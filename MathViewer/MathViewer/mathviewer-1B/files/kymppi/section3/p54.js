(function (lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p54_1.png",
            id: "p54_1"
        }]
    };

    (lib.p54_1 = function () {
        this.initialize(img.p54_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function () {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("18", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(45, 40);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#958FC5").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(40, 23.5);

        this.text_2 = new cjs.Text("Subtraktion – först ner till 10", "24px 'MyriadPro-Semibold'", "#958FC5");
        this.text_2.setTransform(95, 43);

        this.text_3 = new cjs.Text("kunna lösa uppgifter i subtraktion 0 till 20, med tiotalsövergång", "9px 'Myriad Pro'");
        this.text_3.setTransform(70, 655.5);

        this.text = new cjs.Text("54", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(38, 655);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28, 660.8, 1, 1.1);

        this.instance = new lib.p54_1();
        this.instance.setTransform(33, 52, 0.46, 0.46);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.text_3, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function () {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 510, 180, 10);
        this.roundRect1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(25, 0).lineTo(305, 0);
        this.Line_1.setTransform(60, 30);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 12; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 100;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 110;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 120;
                }
                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }

        } this.shape_group1.setTransform(3, 0);

        this.text_1 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_1.setTransform(178, 33);

        this.text_2 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(284, 33);

        var ToBeAdded = [];
        var arryVal = [2, 4, 6, 5, 7, 3, 2, 4, 6, 5];
        var tmpArry = ["11  –  1  –  ", "12  –  2  –  "];
        var i = 0;
        var j = 0;
        var temp_Line1 = new cjs.Shape();
        var tmprow = 27;
        var tmpcol = 162;
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var row = 0; row < 5; row++) {
                var rowSpace = row;
                if (col == 1) {
                    tmpcol = 217;
                    j = 1;
                }
                var tempText = new cjs.Text(tmpArry[j] + arryVal[i] + "  =", "16px 'Myriad Pro'");
                tempText.setTransform(50 + (216 * columnSpace), 62 + (28 * rowSpace));

                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(147 + (columnSpace * tmpcol), 70 + (rowSpace * tmprow)).lineTo(180 + (columnSpace * tmpcol), 70 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };

        this.addChild(this.text_q1, this.text_q2, this.roundRect1, this.Line_1, this.shape_group1, this.text_1, this.text_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 200);

    (lib.Symbol3 = function () {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 510, 182, 10);
        this.roundRect1.setTransform(0, 0);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(25, 0).lineTo(345, 0);
        this.Line_1.setTransform(60, 30);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 14; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 100;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 110;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 120;
                }
                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }

        } this.shape_group1.setTransform(3, 0);

        this.text_1 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_1.setTransform(178, 33);

        this.text_2 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(284, 33);

        var ToBeAdded = [];
        var arryVal = [3, 2, 4, 6, 5, 2, 3, 1, 4, 5];
        var tmpArry = ["13  –  3  –  ", "14  –  4  –  "];
        var i = 0;
        var j = 0;
        var temp_Line1 = new cjs.Shape();
        var tmprow = 27;
        var tmpcol = 162;
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var row = 0; row < 5; row++) {
                var rowSpace = row;
                if (col == 1) {
                    tmpcol = 217;
                    j = 1;
                }
                var tempText = new cjs.Text(tmpArry[j] + arryVal[i] + "  =", "16px 'Myriad Pro'");
                tempText.setTransform(50 + (216 * columnSpace), 62 + (28 * rowSpace));

                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(147 + (columnSpace * tmpcol), 70 + (rowSpace * tmprow)).lineTo(180 + (columnSpace * tmpcol), 70 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };

        this.addChild(this.text_q1, this.text_q2, this.roundRect1, this.Line_1, this.shape_group1, this.text_1, this.text_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 200);

    // stage content:
    (lib.pageLib = function () {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 445, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300, 660, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
