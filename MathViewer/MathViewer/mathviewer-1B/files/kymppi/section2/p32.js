(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p32_1.png",
            id: "p32_1"
        }, {
            src: "images/p32_2.png",
            id: "p32_2"
        }, {
            src: "images/p32_3.png",
            id: "p32_3"
        }, {
            src: "images/p32_4.png",
            id: "p32_4"
        }]
    };

    (lib.p32_1 = function() {
        this.initialize(img.p32_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p32_2 = function() {
        this.initialize(img.p32_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p32_3 = function() {
        this.initialize(img.p32_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p32_4 = function() {
        this.initialize(img.p32_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Fler och färre", "bold 36px 'Epic Awesomeness'", "#FAAA33");     
        this.text.setTransform(192, 75);

        this.text_1 = new cjs.Text("10", "28px 'MyriadPro-Semibold'", "#FFFFFF");       
        this.text_1.setTransform(46, 42);

        this.text_2 = new cjs.Text("förstå och kunna använda begreppen fler och färre", "9px 'Myriad Pro'", "#FAAA33");     
        this.text_2.setTransform(83, 657.5);

        this.text_3 = new cjs.Text("32", "13px 'Myriad Pro'", "#FFFFFF");    
        this.text_3.setTransform(40, 656);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(33.1, 660.8, 1.05, 1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(72, 649.7, 215, 12, 20);
        this.text_Rect.setTransform(0, 0);        

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_3, this.text_2, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("Rita och skriv så att det stämmer.", "16px 'Myriad Pro'");    
        this.text_4.setTransform(8, 30);

        this.instance_1 = new lib.p32_1();
        this.instance_1.setTransform(33, 12, 0.47, 0.47);

        this.roundRect = new cjs.Shape();
        this.roundRect.graphics.f("#ffffff").s('#FCD9B0').ss(1.5).drawRoundRect(10, 90, 500, 195, 5);
        this.roundRect.setTransform(0, 0);

        this.text_5 = new cjs.Text("Jag har 2", "14px 'Myriad Pro'");    
        this.text_5.setTransform(87, 53);

        this.text_6 = new cjs.Text("färre än Leo.", "14px 'Myriad Pro'");    
        this.text_6.setTransform(77, 68);

        this.text_7 = new cjs.Text("Jag har 2", "14px 'Myriad Pro'");    
        this.text_7.setTransform(390, 30);

        this.text_8 = new cjs.Text("färre än Leo.", "14px 'Myriad Pro'");  
        this.text_8.setTransform(377, 45);
        //is.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(138 + (25 * columnSpace), 54 + (29 * row)).lineTo(178 + (25 * columnSpace), 54 + (29 * row));
        this.hrLine_1= new cjs.Shape();
        this.hrLine_2 = new cjs.Shape();
        this.hrLine_3 = new cjs.Shape();
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(22,155).lineTo(500,155);
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(22,220).lineTo(500,220);
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(180,100).lineTo(180,277);
        this.hrLine_4.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(348,100).lineTo(348,277);
        // Added green balls
     
        this.instance_2 = new lib.p32_2();
        this.instance_2.setTransform(210, 115, 0.67, 0.67);
        this.instance_3= new lib.p32_2();
        this.instance_3.setTransform(250, 115, 0.67, 0.67);
        this.instance_4 = new lib.p32_2();
        this.instance_4.setTransform(290, 115, 0.67, 0.67);

        this.text_num = new cjs.Text("15", "36px 'UusiTekstausMajema'", "#6C7373");       
        this.text_num.setTransform(245, 198);

        this.text_num1 = new cjs.Text("18", "36px 'UusiTekstausMajema'", "#6C7373");       
        this.text_num1.setTransform(245, 262);
                    

        this.addChild(this.roundRect,this.text_4,this.instance_1,this.text_5,this.text_6,this.text_7,this.text_8,this.hrLine_1,this.hrLine_2,this.hrLine_3,this.hrLine_4);
        this.addChild(this.instance_2,this.instance_3,this.instance_4,this.text_num,this.text_num1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 305);

    (lib.Symbol2 = function() {
        this.initialize();
      
        this.instance_1 = new lib.p32_3();
        this.instance_1.setTransform(24, 15, 0.47, 0.47);

        this.roundRect = new cjs.Shape();
        this.roundRect.graphics.f("#ffffff").s('#FCD9B0').ss(1.5).drawRoundRect(10,83, 500, 195, 5);
        this.roundRect.setTransform(0, 0);

        this.text_5 = new cjs.Text("Jag har 3", "14px 'Myriad Pro'");    
        this.text_5.setTransform(86, 41);

        this.text_6 = new cjs.Text("färre än Leo.", "14px 'Myriad Pro'");    
        this.text_6.setTransform(77, 57);

        this.text_7 = new cjs.Text("Jag har 3", "14px 'Myriad Pro'");    
        this.text_7.setTransform(390, 35);

        this.text_8 = new cjs.Text("färre än Leo.", "14px 'Myriad Pro'");  
        this.text_8.setTransform(376, 50);
        //is.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(138 + (25 * columnSpace), 54 + (29 * row)).lineTo(178 + (25 * columnSpace), 54 + (29 * row));
        this.hrLine_1= new cjs.Shape();
        this.hrLine_2 = new cjs.Shape();
        this.hrLine_3 = new cjs.Shape();
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(22,152).lineTo(500,152);
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(22,218).lineTo(500,218);
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(180,92).lineTo(180,270);
        this.hrLine_4.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(348,92).lineTo(348,270);
        // Added green balls
     
        this.instance_2 = new lib.p32_4();
        this.instance_2.setTransform(210, 108, 0.67, 0.67);
        this.instance_3= new lib.p32_4();
        this.instance_3.setTransform(250, 108, 0.67, 0.67);
        this.instance_4 = new lib.p32_4();
        this.instance_4.setTransform(290, 108, 0.67, 0.67);

        this.text_num = new cjs.Text("13", "36px 'UusiTekstausMajema'", "#6C7373");       
        this.text_num.setTransform(245, 197);

        this.text_num1 = new cjs.Text("16", "36px 'UusiTekstausMajema'", "#6C7373");       
        this.text_num1.setTransform(245, 257);
                    

        this.addChild(this.roundRect,this.instance_1,this.text_5,this.text_6,this.text_7,this.text_8,this.hrLine_1,this.hrLine_2,this.hrLine_3,this.hrLine_4);
        this.addChild(this.instance_2,this.instance_3,this.instance_4,this.text_num,this.text_num1);

        
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 285);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 255, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(296.8, 530, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
