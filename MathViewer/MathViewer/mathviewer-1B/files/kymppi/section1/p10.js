(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p10_1.png",
            id: "p8_1"
        }, {
            src: "images/p10_2.png",
            id: "p8_2"
        }]
    };

    // symbols:
   
    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1

        this.text_2 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(40, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);
        
        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(470 + (columnSpace * 32), 29, 27, 27);
            }
        }
        this.textbox_group1.setTransform(-15, 10);

        this.addChild(this.shape, this.text, this.text_1, this.text_2,this.textbox_group1,this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

   

     (lib.Symbol2 = function() {
        this.initialize();
        
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 4);

        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 510, 155, 10);
        this.roundRect1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [94, 268, 434, 94, 268, 434, 94, 268, 434, 94, 268, 434];
        var lineY = [25, 25, 25, 57, 57, 57, 87, 87, 87, 116, 116, 116];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("13  –  1  =", "16px 'Myriad Pro'");
        this.label1.setTransform(37, 60);
        this.label2 = new cjs.Text("13  –  2  =", "16px 'Myriad Pro'");
        this.label2.setTransform(37, 90);
        this.label3 = new cjs.Text("13  –  0  =", "16px 'Myriad Pro'");
        this.label3.setTransform(37, 120);
        this.label4 = new cjs.Text("13  –  3  =", "16px 'Myriad Pro'");
        this.label4.setTransform(37, 150);

        this.label6 = new cjs.Text("10  +  1  =", "16px 'Myriad Pro'");
        this.label6.setTransform(208, 60);
        this.label7 = new cjs.Text("10  +  3  =", "16px 'Myriad Pro'");
        this.label7.setTransform(208, 90);
        this.label8 = new cjs.Text("10  +  0  =", "16px 'Myriad Pro'");
        this.label8.setTransform(208, 120);
        this.label9 = new cjs.Text("10  +  2  =", "16px 'Myriad Pro'");
        this.label9.setTransform(208, 150);

        this.label11 = new cjs.Text("11  +  1  =", "16px 'Myriad Pro'");
        this.label11.setTransform(375, 60);
        this.label12 = new cjs.Text("11  +  2  =", "16px 'Myriad Pro'");
        this.label12.setTransform(375, 90);
        this.label13 = new cjs.Text("13  +  0  =", "16px 'Myriad Pro'");
        this.label13.setTransform(375, 120);
        this.label14 = new cjs.Text("12  +  1  =", "16px 'Myriad Pro'");
        this.label14.setTransform(375, 150);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(51, 20).lineTo(350, 20);
        this.Line_1.setTransform(23, 10);
           

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'","#FFFFFF");
        this.text_2.setTransform(167, 33);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'","#FFFFFF");
        this.text_3.setTransform(274, 33);


       this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 13; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

           if(column<5)
            {
                this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(90 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }
            else if(column<10)
            {
                this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(100 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }
             else
            {
                this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(110 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }

          }
   
        }
        this.shape_group1.setTransform(3, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, 
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1,this.text_2,this.text_3);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");        this.text.setTransform(19, 4);
        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 510, 155, 10);
        this.roundRect1.setTransform(0, 0);


        var lineArr = [];
        var lineX = [94, 268, 434, 94, 268, 434, 94, 268, 434, 94, 268, 434];
        var lineY = [25, 25, 25, 57, 57, 57, 87, 87, 87, 116, 116, 116];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("14  –  2  =", "16px 'Myriad Pro'");
        this.label1.setTransform(37, 60);
        this.label2 = new cjs.Text("14  –  1  =", "16px 'Myriad Pro'");
        this.label2.setTransform(37, 90);
        this.label3 = new cjs.Text("14  –  4  =", "16px 'Myriad Pro'");
        this.label3.setTransform(37, 120);
        this.label4 = new cjs.Text("14  –  3  =", "16px 'Myriad Pro'");
        this.label4.setTransform(37, 150);

        this.label6 = new cjs.Text("10  +  4  =", "16px 'Myriad Pro'");
        this.label6.setTransform(208, 60);
        this.label7 = new cjs.Text("11  +  3  =", "16px 'Myriad Pro'");
        this.label7.setTransform(208, 90);
        this.label8 = new cjs.Text("12  +  2  =", "16px 'Myriad Pro'");
        this.label8.setTransform(208, 120);
        this.label9 = new cjs.Text("11  +  2  =", "16px 'Myriad Pro'");
        this.label9.setTransform(208, 150);

        this.label11 = new cjs.Text("12  +  1  =", "16px 'Myriad Pro'");
        this.label11.setTransform(375, 60);
        this.label12 = new cjs.Text("12  +  2  =", "16px 'Myriad Pro'");
        this.label12.setTransform(375, 90);
        this.label13 = new cjs.Text("11  +  3  =", "16px 'Myriad Pro'");
        this.label13.setTransform(375, 120);
        this.label14 = new cjs.Text("13  +  1  =", "16px 'Myriad Pro'");
        this.label14.setTransform(375, 150);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(51, 20).lineTo(370, 20);
        this.Line_1.setTransform(23, 10);
           
        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'","#FFFFFF");
        this.text_2.setTransform(167, 33);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'","#FFFFFF");
        this.text_3.setTransform(274, 33);


        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 14; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

           if(column<5)
            {
                this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(90 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }
            else if(column<10)
            {
                this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(100 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }
             else
            {
                this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(110 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }

          }
   
        }
        this.shape_group1.setTransform(3, 0);


       this.addChild(this.roundRect1, this.text, this.text_1,this.textbox_group1, this.label1, this.label2, this.label3, this.label4, 
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.text_2,this.text_3);
            for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


 (lib.Symbol4 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");        this.text.setTransform(19, 4);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 4);

          this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 510, 155, 10);
        this.roundRect1.setTransform(0, 0);


        var lineArr = [];
        var lineX = [94, 268, 434, 94, 268, 434, 94, 268, 434, 94, 268, 434];
        var lineY = [25, 25, 25, 57, 57, 57, 87, 87, 87, 116, 116, 116];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("15  –  1  =", "16px 'Myriad Pro'");
        this.label1.setTransform(37, 60);
        this.label2 = new cjs.Text("15  –  5  =", "16px 'Myriad Pro'");
         this.label2.setTransform(37, 90);
        this.label3 = new cjs.Text("15  –  3  =", "16px 'Myriad Pro'");
        this.label3.setTransform(37, 120);
        this.label4 = new cjs.Text("15  –  4  =", "16px 'Myriad Pro'");
        this.label4.setTransform(37, 150);

        this.label6 = new cjs.Text("10  +  5  =", "16px 'Myriad Pro'");
        this.label6.setTransform(208, 60);
        this.label7 = new cjs.Text("11  +  4  =", "16px 'Myriad Pro'");
        this.label7.setTransform(208, 90);
        this.label8 = new cjs.Text("12  +  2  =", "16px 'Myriad Pro'");
        this.label8.setTransform(208, 120);
        this.label9 = new cjs.Text("13  +  2  =", "16px 'Myriad Pro'");
        this.label9.setTransform(208, 150);

        this.label11 = new cjs.Text("11  +  4  =", "16px 'Myriad Pro'");
        this.label11.setTransform(375, 60);
        this.label12 = new cjs.Text("12  +  3  =", "16px 'Myriad Pro'");
        this.label12.setTransform(375, 90);
        this.label13 = new cjs.Text("15  +  0  =", "16px 'Myriad Pro'");
        this.label13.setTransform(375, 120);
        this.label14 = new cjs.Text("14  +  1  =", "16px 'Myriad Pro'");
        this.label14.setTransform(375, 150);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(51, 20).lineTo(390, 20);
        this.Line_1.setTransform(23, 10);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'","#FFFFFF");
        this.text_2.setTransform(167, 33);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'","#FFFFFF");
        this.text_3.setTransform(274, 33);
           
        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'","#FFFFFF");
        this.text_4.setTransform(384, 33);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 15; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

           if(column<5)
            {
                this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(90 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }
            else if(column<10)
            {
                this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(100 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }
             else
            {
                this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(110 + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }

          }
   
        }
        this.shape_group1.setTransform(3, 0);



        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, 
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.text_2,this.text_3,this.text_4);
             for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296, 110, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296, 325, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(296, 520, 1, 1, 0, 0, 0, 255.8, 53.5);


        this.addChild(this.other, this.v1, this.v2, this.v3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
