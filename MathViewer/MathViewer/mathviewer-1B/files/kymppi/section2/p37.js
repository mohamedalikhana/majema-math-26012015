(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p37_1.png",
            id: "p37_1"
        }, {
            src: "images/p37_2.png",
            id: "p37_2"
        }]
    };

    (lib.p37_1 = function() {
        this.initialize(img.p37_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p37_2 = function() {
        this.initialize(img.p37_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();

        this.instance = new lib.p37_1();
        this.instance.setTransform(33, 0, 0.466, 0.466);

        // Layer 1
        this.text_1 = new cjs.Text("kunna lösa uppgifter i addition 0 till 20, med tiotalsövergång", "9px 'Myriad Pro'");
        this.text_1.setTransform(310.5, 661);

        this.text_2 = new cjs.Text("37", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(555, 659);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Vi övar", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.setTransform(95.5, 43);

        this.text_4 = new cjs.Text("12", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(51, 43);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.instance, this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Hur många stjärnor är det tillsammans?", "16px 'Myriad Pro'");
        this.text_q1.setTransform(24, 0);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(7, 10, 256, 117, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 0);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 123);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 123);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 245);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 245);

        this.instance = new lib.p37_2();
        this.instance.setTransform(28, 12.5, 0.472, 0.472);

        this.innerRountrect = new cjs.Shape();
        this.innerRountrect.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(30, 6, 83, 70, 6);
        this.innerRountrect.setTransform(0, 12);

        this.innerRountrect1 = new cjs.Shape();
        this.innerRountrect1.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(160, 6, 83, 47, 6);
        this.innerRountrect1.setTransform(0, 12);

        this.innerRountrect2 = new cjs.Shape();
        this.innerRountrect2.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(423, 250, 53, 47, 6);
        this.innerRountrect2.setTransform(0, 12);

        this.innerRountrect3 = this.innerRountrect.clone(true);
        this.innerRountrect3.setTransform(0, 135);

        this.innerRountrect4 = this.innerRountrect.clone(true);
        this.innerRountrect4.setTransform(0, 257);

        this.innerRountrect5 = this.innerRountrect.clone(true);
        this.innerRountrect5.setTransform(130, 135);

        this.innerRountrect6 = this.innerRountrect.clone(true);
        this.innerRountrect6.setTransform(130, 257);

        this.innerRountrect7 = this.innerRountrect.clone(true);
        this.innerRountrect7.setTransform(260, 12);

        this.innerRountrect8 = this.innerRountrect.clone(true);
        this.innerRountrect8.setTransform(260, 135);

        this.innerRountrect9 = this.innerRountrect.clone(true);
        this.innerRountrect9.setTransform(260, 257);

        this.innerRountrect10 = this.innerRountrect1.clone(true);
        this.innerRountrect10.setTransform(260, 12);

        this.innerRountrect11 = this.innerRountrect.clone(true);
        this.innerRountrect11.setTransform(388, 135);

        var tmptextArr = ['5', '8', '9', '6', '7', '4'];
        var ToBeAdded = [];
        var i = 0;
        this.hrLine_1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                if (i < tmptextArr.length) {
                    var tempText = new cjs.Text("9 " + " + " + tmptextArr[i] + " = ", "16px 'Myriad Pro'");
                    if (row > 0) {
                        tempText.setTransform(92 + (260 * colSpace), 110 + (61 * rowSpace * 2));
                    } else {
                        tempText.setTransform(92 + (260 * colSpace), 110 + (63 * rowSpace));
                    }

                    this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(144 + (260 * colSpace), 115 + (61 * rowSpace * 2)).lineTo(189 + (260 * colSpace), 115 + (61 * rowSpace * 2));
                    this.hrLine_1.setTransform(0, 0);
                    ToBeAdded.push(tempText, this.hrLine_1);
                    i++;


                }

            }
        }


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.innerRountrect, this.innerRountrect1, this.innerRountrect2, this.innerRountrect3, this.innerRountrect4, this.innerRountrect5);
        this.addChild(this.innerRountrect6, this.innerRountrect7, this.innerRountrect8, this.innerRountrect9, this.innerRountrect10, this.innerRountrect11)
        this.addChild(this.text_q1, this.text_q2, this.instance);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 343.6);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 320, 1, 1, 0, 0, 0, 254.6, 53.4);


        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
