var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };



    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 0", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("8", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Räkna högt: 4, 3, 2, 1, 0.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550 + 110, 460 + 20);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();
        this.numbers = new lib.numbers();
        this.numbers.setTransform(400 + 50, 0, 3.5, 4, 0, 0, 0, 0, 0);
        var addImage = [];
        var addImageX = [455, 455, 565, 455, 565, 675, 455, 565, 675, 785];
        var addImageY = [-70, 40, 40, 150, 150, 150, 260, 260, 260, 260];


        for (var i = 10 - 1; i >= 0; i--) {

            var shape = this.blueBall.clone(true);
            shape.setTransform(addImageX[i], addImageY[i] + 80, 0.8, 0.8);
            addImage.push(shape);
        }

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        for (var j = addImage.length - 1; j >= 0; j--) {
            this.addChild(addImage[j]);
        }
        this.addChild(this.numbers, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.numbers = new lib.numbers();
        this.numbers.setTransform(400 + 50, 0, 3.5, 4, 0, 0, 0, 0, 0);

        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();

        var addImage = [];
        var addImageX = [455, 455, 565, 455, 565, 675, 455, 565, 675, 785];
        var addImageY = [-70, 40, 40, 150, 150, 150, 260, 260, 260, 260];


        for (var i = 10 - 1; i >= 0; i--) {

            var shape = this.blueBall.clone(true);
            shape.setTransform(addImageX[i], addImageY[i] + 80, 0.8, 0.8);
            addImage.push(shape);
        }

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.numbers, this.commentTexts);
        this.tweens = [];
        //var b_wait = [1000, 2000, 2000, 3000, 3000, 3000, 4000, 4000, 4000, 4000];
        var b_wait = [1000, 1000, 1000, 1000, 2000, 2000, 2000, 3000, 3000, 4000];
        //        var b_wait=[750,1500,1500,2250,2250,2250,3000,3000,3000,3000].reverse();
        for (var j = addImage.length - 1; j >= 0; j--) {
            this.addChild(addImage[j]);
            this.tweens.push({
                ref: addImage[j],
                alphaFrom: 1,
                alphaTo: 0,
                wait: b_wait[j],
                alphaTimeout: 2000,
            });

        }

        p.tweens = this.tweens;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.numbers = function() {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();
        var numbers = [];
        for (var i = 0; i < 5; i++) {
            var yPos = -15;
            if (i == 2) {
                yPos = -10;
            } else if (i == 3) {
                yPos = -7;
            } else if (i == 4) {
                yPos = -4;
            }
            numbers[i] = new cjs.Text("" + (i), "16px 'Myriad Pro'");
            numbers[i].lineHeight = 19;
            numbers[i].setTransform(-20, (24 * i) + yPos);
        }
        for (var i = 0; i < 5; i++) {
            this.addChild(numbers[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
