(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p86_1.png",
            id: "p86_1"
        }, {
            src: "images/p80_2.png",
            id: "p80_2"
        }]
    };

    (lib.p86_1 = function() {
        this.initialize(img.p86_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p80_2 = function() {
        this.initialize(img.p80_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("86", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(34.5, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28, 660.8);

        this.instance = new lib.p86_1();
        this.instance.setTransform(34, 5, 0.807, 0.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(463 + (columnSpace * 32.5), 26, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Skriv additionen.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 14);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 29;
        this.text_q2.setTransform(0, 14);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 259, 95, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(264, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 125);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(264, 125);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 225);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(264, 225);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 325);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(264, 325);

        //block 1
        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // red ball
                if (row == 0 && column == 1) {
                    continue;
                }
                if (column == 2) {
                    if (row == 0) {
                        columnSpace = 1.045;
                    } else {
                        columnSpace = 1.095;
                    }
                }
                this.shape_group1.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(67 + (columnSpace * 256), 60 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // yellow ball
                if (row == 0 && column == 0) {
                    continue;
                } else if (row == 0 && column == 2) {
                    continue;
                }
                if (column == 1) {
                    if (row == 0) {
                        columnSpace = 0.50;
                    } else {
                        columnSpace = 1;
                    }
                } else if (column == 2) {
                    columnSpace = 11.6;
                }
                this.shape_group2.graphics.f("#FFF679").s("#6E6E70").ss(0.8, 0, 0, 4).arc(115 + (columnSpace * 24), 60 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // blue ball
                if (column == 2) {
                    columnSpace = 1.095;
                }
                this.shape_group3.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(186 + (columnSpace * 257), 60 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group3.setTransform(0, 0);

        //block 2
        this.shape_group4 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // red ball
                if (column == 1) {
                    columnSpace = 0.095;
                } else if (column == 2) {
                    columnSpace = 1.05;
                }
                this.shape_group4.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(62 + (columnSpace * 256), 160 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group4.setTransform(0, 0);

        this.shape_group5 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // yellow ball
                this.shape_group5.graphics.f("#FFF679").s("#6E6E70").ss(0.8, 0, 0, 4).arc(135 + (columnSpace * 260), 160 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group5.setTransform(0, 0);

        this.shape_group6 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // blue ball
                if (column == 0 && row == 0) {
                    continue;
                }
                this.shape_group6.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(189 + (columnSpace * 272), 160 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group6.setTransform(0, 0);

        //block 3
        this.shape_group7 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // red ball
                if (column == 0 && row == 0) {
                    continue;
                } else if (column == 2 && row == 0) {
                    continue;
                }

                if (column == 1) {
                    if (row == 0) {
                        columnSpace = 0.52;
                    } else if (row == 1) {
                        columnSpace = 1;
                    }
                } else if (column == 2) {
                    columnSpace = 11.28;
                } else if (column == 3) {
                    if (row == 0) {
                        columnSpace = 11.75;
                    } else {
                        columnSpace = 12.3;
                    }
                }
                this.shape_group7.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(60.5 + (columnSpace * 24), 260 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group7.setTransform(0, 0);

        this.shape_group8 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // yellow ball
                if (column == 0 && row == 0) {
                    continue;
                }

                if (column == 1) {
                    if (row == 0) {
                        columnSpace = 0.05;
                    } else {
                        columnSpace = 0.1;
                    }
                } else if (column == 2) {
                    columnSpace = 1.085;
                }
                this.shape_group8.graphics.f("#FFF679").s("#6E6E70").ss(0.8, 0, 0, 4).arc(124 + (columnSpace * 257), 260 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group8.setTransform(0, 0);

        this.shape_group9 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // blue ball
                if (column == 0 && row == 0) {
                    continue;
                } else if (column == 1 && row == 0) {
                    continue;
                }

                this.shape_group9.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(189 + (columnSpace * 273), 260 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group9.setTransform(0, 0);

        //block 4
        this.shape_group10 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // red ball
                if (column == 2 && row == 0) {
                    continue;
                }

                if (column == 2) {
                    columnSpace = 11.1;
                } else if (column == 3) {
                    if (row == 0) {
                        columnSpace = 11.6;
                    } else {
                        columnSpace = 12.1;
                    }
                }
                this.shape_group10.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(56 + (columnSpace * 24), 360 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group10.setTransform(0, 0);

        this.shape_group11 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // yellow ball
                if (column == 1 && row == 0) {
                    continue;
                } else if (column == 0 && row == 0) {
                    continue;
                }

                if (column == 2) {
                    if (row == 0) {
                        columnSpace = 1.046;
                    } else {
                        columnSpace = 1.095;
                    }
                }
                this.shape_group11.graphics.f("#FFF679").s("#6E6E70").ss(0.8, 0, 0, 4).arc(129 + (columnSpace * 264), 360 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group11.setTransform(0, 0);

        this.shape_group12 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // blue ball
                if (column == 0 && row == 0) {
                    continue;
                }

                if (column == 1) {
                    if (row == 0) {
                        columnSpace = 0.5;
                    } else {
                        columnSpace = 1;
                    }
                }
                else if(column==2){
                    columnSpace=12.2;
                }

                this.shape_group12.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(175 + (columnSpace * 24), 360 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group12.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['103','203','303','403'];
        var xPos = 27,
            padding,
            yPos,
            rectWidth = 150.5,
            rectHeight = 23,
            boxWidth = 21.5,
            boxHeight = 23,
            numberOfBoxes = 7,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 25;
                if (i == 1) {
                    padding = padding + 47.5;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8);
        this.addChild(this.textbox_group1);
        this.addChild(this.text_q1, this.text_q2, this.shape_group1, this.shape_group2);
        this.addChild(this.shape_group3, this.shape_group4, this.shape_group5, this.shape_group6);
        this.addChild(this.shape_group7, this.shape_group8, this.shape_group9, this.shape_group10, this.shape_group11, this.shape_group12)
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(297, 421, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
