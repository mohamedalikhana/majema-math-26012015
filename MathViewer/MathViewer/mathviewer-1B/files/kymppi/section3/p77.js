(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p77_1.png",
            id: "p77_1"
        }]
    };

    (lib.p77_1 = function() {
        this.initialize(img.p77_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("77", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(580, 660.8);


        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(460 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(4, 0);

        this.text_1 = new cjs.Text("Hur många poäng fick Manuel och Mira tillsammans?", "16px 'Myriad Pro'");
        this.text_1.setTransform(23, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 253, 160, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true)
        this.roundRect2.setTransform(259, 15);

        this.roundRect3 = this.roundRect1.clone(true)
        this.roundRect3.setTransform(0, 182);

        this.roundRect4 = this.roundRect1.clone(true)
        this.roundRect4.setTransform(259, 182);       

        this.instance = new lib.p77_1();
        this.instance.setTransform(15, 21, 0.46, 0.46);

        var ToBeAdded = [];
         var xpos = 60;
        var ypos = 138;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 305
            } else {
                ypos = 138
            }

            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 6; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 325
                        } else {
                            xpos = 60
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 32), 21, 23);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }



        var tmpValtop = "Jag fick";
        var tmpValbottom = "";
        var ypos = 48;
        var xposTop=35;
        var xposBotm=30;
        for (var row = 0; row < 2; row++) {
            var rowSpace = row;
            for (var column = 0; column < 4; column++) {
                var colSpace = column;
                var tempText = new cjs.Text(tmpValtop, "16px 'Myriad Pro'");
                if(column>1){xposTop=48} else {xposTop=35}
                 tempText.setTransform(xposTop + (125 * colSpace), ypos + (166 * rowSpace));
                 if(row==0 && column==0){tmpValbottom="8 poäng."}
                 else if(row==0 && column==1){tmpValbottom="9 poäng."}
                 else if(row==0 && column==2){tmpValbottom="7 poäng."}
                 else if(row==0 && column==3){tmpValbottom="8 poäng."}
                 else if(row==1 && column==0){tmpValbottom="9 poäng."}
                 else if(row==1 && column==1){tmpValbottom="8 poäng."}
                 else if(row==1 && column==2){tmpValbottom="5 poäng."}
                 else if(row==1 && column==3){tmpValbottom="8 poäng."}
                var tempText1 = new cjs.Text(tmpValbottom, "16px 'Myriad Pro'");
                if(column>1){xposBotm=43} else {xposBotm=30}
                tempText1.setTransform(xposBotm + (125 * colSpace), (ypos+17) + (166 * rowSpace));
                ToBeAdded.push(tempText, tempText1);
            }
        }
      

        this.addChild(this.text, this.text_1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,this.instance)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 350);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text.setTransform(18, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(4, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 12, 511, 168, 10);
        this.roundRect1.setTransform(0, 0);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(25, 0).lineTo(473, 0);
        this.Line_1.setTransform(5, 36);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 46;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 56;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 66;
                } else {
                    fillcolor = "#008BD2";
                    xpos = 76;
                }

                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 36 + (row * 19), 7.9, 0, 2 * Math.PI);

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(123, 39);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_3.setTransform(229, 39);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#ffffff");
        this.text_4.setTransform(340, 39);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#ffffff");
        this.text_5.setTransform(450, 39);

        var ToBeAdded = [];
        var arryVal = ["13 – 4 – 4 =","13 – 5 – 5 =","13 – 6 – 6 =","13 – 7 – 6 =","15 – 8 – 7 =","16 – 9 – 6 =","17 – 8 – 8 =","18 – 9 – 9 ="];
        var i = 0;
        var tmpTxt = "";
        var temp_Line1 = new cjs.Shape();
        var tmprow = 28;
        var tmpcol = 225;
        for (var col = 0; col < 2; col++) {           
            var columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text( arryVal[i] , "16px 'Myriad Pro'");
                tempText.setTransform(80 + (225 * columnSpace), 75 + (28 * rowSpace));
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(157 + (columnSpace * tmpcol), 80 + (rowSpace * tmprow)).lineTo(190 + (columnSpace * tmpcol), 80 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1);
        this.addChild(this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(313, 290, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(313, 455, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
