(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p73_1.png",
            id: "p73_1"
        }, {
            src: "images/p73_2.png",
            id: "p73_2"
        }]
    };

    (lib.p73_1 = function() {
        this.initialize(img.p73_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p73_2 = function() {
        this.initialize(img.p73_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("73", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(553, 663);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna och måla svaret.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 511, 295, 10);
        this.roundRect1.setTransform(5, 13);

        this.label1 = new cjs.Text("17 – 9 =", "16px 'Myriad Pro'");
        this.label1.setTransform(43, 44);

        this.label2 = new cjs.Text("14 – 9 =", "16px 'Myriad Pro'");
        this.label2.setTransform(43, 74);

        this.label3 = new cjs.Text("16 – 9 =", "16px 'Myriad Pro'");
        this.label3.setTransform(43, 104);

        this.label4 = new cjs.Text("12 – 8 =", "16px 'Myriad Pro'");
        this.label4.setTransform(43, 134);


        this.label6 = new cjs.Text("20 –  7  =", "16px 'Myriad Pro'");
        this.label6.setTransform(198, 44);

        this.label7 = new cjs.Text("20 – 10 =", "16px 'Myriad Pro'");
        this.label7.setTransform(198, 74);

        this.label8 = new cjs.Text("20 –  9  =", "16px 'Myriad Pro'");
        this.label8.setTransform(198, 104);

        this.label9 = new cjs.Text("20 –  8  =", "16px 'Myriad Pro'");
        this.label9.setTransform(198, 134);


        this.label11 = new cjs.Text("18 – 9 =", "16px 'Myriad Pro'");
        this.label11.setTransform(353, 44);

        this.label12 = new cjs.Text("12 – 6 =", "16px 'Myriad Pro'");
        this.label12.setTransform(353, 74);

        this.label13 = new cjs.Text("11 – 8 =", "16px 'Myriad Pro'");
        this.label13.setTransform(353, 104);

        this.label14 = new cjs.Text("11 – 9 =", "16px 'Myriad Pro'");
        this.label14.setTransform(353, 134);


        // ROW-1 ROUND SHAPES--columns1
        this.hrLine_1 = new cjs.Shape();
        this.shape_group2 = new cjs.Shape();
        var fillcolor = "";
        var columnXpos = 0;
        var MoveToX = 0;
        var LineToX = 0;
        var LineToY = 0;
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {

                if (row < 2 && column == 0) {
                    fillcolor = "#008BD2";
                    columnXpos = 144;
                } else if (row > 1 && column == 0) {
                    fillcolor = "#FFF374";
                    columnXpos = 144;
                } else if (row < 2 && column == 1) {
                    fillcolor = "#FFF374";
                    columnXpos = 283;
                } else if (row > 1 && column == 1) {
                    fillcolor = "#008BD2";
                    columnXpos = 283;
                } else if (column == 2) {
                    columnXpos = 408;
                }
                this.shape_group2.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 25), 44 + (row * 29), 9.5, 0, 2 * Math.PI)

                if (column == 0) {
                    MoveToX = 99;
                    LineToX = 132;
                    LineToY = 52;
                } else if (column == 1) {
                    MoveToX = 237;
                    LineToX = 271;
                    LineToY = 52;
                } else if (column == 2) {
                    MoveToX = 359;
                    LineToX = 394;
                    LineToY = 52;
                }
                this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(MoveToX + (25 * columnSpace), LineToY + (29 * row)).lineTo(LineToX + (25 * columnSpace), LineToY + (29 * row));

            }
        }
        this.shape_group2.setTransform(0, 0);
        this.hrLine_1.setTransform(0, 0);


        var ToBeAdded = [];
        this.textbox_group2 = new cjs.Shape();
        var i = 2;
        var colXpos = 90;
        for (var column = 0; column < 12; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#878787").ss(0.7).drawRect(82 + (columnSpace * 25), 250, 25, 27);
            var temp_label = new cjs.Text(i, "16px 'Myriad Pro'");
            if (i > 9) {
                colXpos = 85
            }
            temp_label.setTransform(colXpos + (columnSpace * 25), 269);
            ToBeAdded.push(temp_label);
            i++
        }
        this.textbox_group2.setTransform(0, 0);

        this.instance = new lib.p73_1();
        this.instance.setTransform(140, 165, 0.46, 0.46);



        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15)
        this.addChild(this.label13, this.shape_group2, this.hrLine_1, this.hrLine_2, this.instance, this.textbox_group2);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 572.3, 270);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(5, 0);

        this.text_1 = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.instance = new lib.p73_2();
        this.instance.setTransform(40, 27, 0.46, 0.46);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 250, 215, 10);
        this.roundRect1.setTransform(10, 13);

        this.roundRect2 = this.roundRect1.clone(true)
        this.roundRect2.setTransform(265, 13);

        this.innerRoundRect = new cjs.Shape();
        this.innerRoundRect.graphics.f("#ffffff").s('#F7B48C').drawRoundRect(0, 0, 190, 122, 6);
        this.innerRoundRect.setTransform(40, 22)

        this.innerRoundRect1 = this.innerRoundRect.clone(true)
        this.innerRoundRect1.setTransform(295, 22)

        var ToBeAdded = [];
        var tmpTxt = "";
        var tmpXpos = 60;
        var xpos = 97;
        for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
            for (var row = 0; row < 4; row++) {
                for (var col = 0; col < 3; col++) {
                    var columnSpace = col;

                    if (col == 0 && row < 3 && numOfcol == 0) {
                        tmpTxt = "+"
                    } else if (col == 0 && row == 3 && numOfcol == 0) {
                        tmpTxt = "–"
                    } else if (col == 1 && row < 4 && numOfcol == 0) {
                        tmpTxt = "="
                    } else if (col == 2 && row == 0 && numOfcol == 0) {
                        tmpTxt = "6"
                    } else if (col == 2 && row == 1 && numOfcol == 0) {
                        tmpTxt = "8"
                    } else if (numOfcol == 1 && col == 0 && row == 0) {
                        tmpTxt = "8"
                    } else if (numOfcol == 1 && col == 0 && row == 3) {
                        tmpTxt = "8"
                    } else if (numOfcol == 1 && col == 1 && row < 4) {
                        tmpTxt = "–"
                    } else if (numOfcol == 1 && col == 2 && row < 4) {
                        tmpTxt = "="
                    } else {
                        continue;
                    }

                    var temp_label = new cjs.Text(tmpTxt, "16px 'Myriad Pro'");
                    if (col == 2 && numOfcol == 0) {
                        tmpXpos = 45
                    } else if (numOfcol == 1) {
                        xpos = 325
                        if (col == 2) {
                            tmpXpos = 42
                        } else {
                            tmpXpos = 25
                        }

                    } else {
                        xpos = 97
                        tmpXpos = 60
                    }
                    temp_label.setTransform(xpos + (columnSpace * tmpXpos), 45 + (row * 28));
                    ToBeAdded.push(temp_label);

                }
            }
        }

         this.textbox_group1 = new cjs.Shape();
        var tmpxpos = 100;
         var tmpxpos1 = 103;
        var txtXpos=75;
        var rectXpos=95;
        for (var row = 0; row < 2; row++) {            
            for (var col = 0; col < 4; col++) {
                var columnSpace = col
               if(col==2) {txtXpos=135;rectXpos=158}
                else if(col==3) {tmpxpos = 98;tmpxpos1=100}
               else {txtXpos=75;rectXpos=95;tmpxpos = 100;tmpxpos1=103}
                this.textbox_group1.graphics.f('#ffffff').s("#9D9D9C").ss(0.9).drawRect(rectXpos + (columnSpace * tmpxpos), 154+(row*29), 20, 23);
                var tempText = new cjs.Text("=", "16px 'Myriad Pro'");
                tempText.setTransform(txtXpos + (columnSpace * tmpxpos1), 170+(row*25));
                ToBeAdded.push(tempText)

            }
        }
       this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.innerRoundRect, this.innerRoundRect1);
        this.addChild(this.text_1, this.textbox_group1, this.InnerroundRect3, this.InnerroundRect4,this.textbox_group1);
        this.addChild(this.text, this.instance);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 150);



    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(307, 274, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(307, 410, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
