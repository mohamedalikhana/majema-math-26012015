(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/s4_5_1.png",
            id: "s4_5_1"
        }, {
            src: "images/s4_5_2.png",
            id: "s4_5_2"
        }, {
            src: "images/s4_5_3.png",
            id: "s4_5_3"
        }, {
            src: "images/s4_5_60.png",
            id: "s4_5_60"
        }]
    };

    (lib.s4_5_1 = function() {
        this.initialize(img.s4_5_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.s4_5_2 = function() {
        this.initialize(img.s4_5_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 877, 307);


    (lib.s4_5_3 = function() {
        this.initialize(img.s4_5_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 953, 566);


    (lib.s4_5_60 = function() {
        this.initialize(img.s4_5_60);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1020, 463);


    (lib.Symbol20 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABpQArAAAfgfQAfgeAAgsQAAgqgfgfQgfgfgrAAQgrAAgeAfQgfAfAAAqQAAAsAfAeQAeAfArAAg");
        this.shape.setTransform(10.6, 10.6);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 23.2, 23.2);


    (lib.Symbol19 = function() {
        this.initialize();
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol18 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol17 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.2, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol16 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.2, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol15 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol14 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol13 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol12 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol11 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol10 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABpQArAAAggfQAegeAAgsQAAgqgegfQgggfgrAAQgrAAgfAfQgfAfAAAqQAAAsAfAeQAfAfArAAg");
        this.shape.setTransform(10.6, 10.6);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 23.2, 23.2);


    (lib.Symbol9 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.2, 10.2);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol8 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol7 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol6 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol5 = function() {
        this.initialize();

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#D74479").ss(1.5, 0, 0, 4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
        this.shape.setTransform(10.3, 10.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 22.5, 22.5);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Räkna till 10", "24px 'MyriadPro-Semibold'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(93.5, 25);

        this.text_1 = new cjs.Text("1", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(57.7, 22);

        this.text_2 = new cjs.Text("5", "bold 20px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 24;
        this.text_2.setTransform(1165.3, 642.4);

        this.pageBottomText = new cjs.Text("kunna räkna antal till 10", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 651);

        this.text_4 = new cjs.Text("6", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 0;
        this.text_4.setTransform(41, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.instance = new lib.s4_5_60();
        this.instance.setTransform(0, 7.4, 0.599, 0.592);
        //this.addChild(this.instance,this.shape_2,this.shape_1,this.shape,this.text_4,this.text_3,this.text_2,this.text_1,this.text);
        this.addChild(this.instance, this.shape_2, this.shape, this.text_4, this.pageBottomText, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol3 = function() {
        this.initialize();


        this.ra6 = new lib.Symbol20();
        this.ra6.setTransform(488.1, 305.3, 1, 1, 0, 0, 0, 10.6, 10.6);

        this.ra5 = new lib.Symbol19();
        this.ra5.setTransform(187.3, 305, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra4 = new lib.Symbol18();
        this.ra4.setTransform(362.6, 192, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra3 = new lib.Symbol17();
        this.ra3.setTransform(145.6, 192, 1, 1, 0, 0, 0, 10.2, 10.3);

        this.ra2 = new lib.Symbol16();
        this.ra2.setTransform(321.4, 81.6, 1, 1, 0, 0, 0, 10.2, 10.3);

        this.ra1 = new lib.Symbol15();
        this.ra1.setTransform(20, 81.6, 1, 1, 0, 0, 0, 10.3, 10.3);


        this.text = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(477.5, 293.1);

        this.text_1 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(441, 293.1);

        this.text_2 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(399.1, 293.1);

        this.text_3 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(357.1, 293.1);

        this.text_4 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(315.2, 293.1);

        this.text_5 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(273.2, 293.1);

        this.text_6 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(218.7, 293.1);

        this.text_7 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(182.2, 293.1);

        this.text_8 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(140.3, 293.1);

        this.text_9 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(98.3, 293.1);

        this.text_10 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(56.4, 293.1);

        this.text_11 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(14.4, 293.1);

        this.text_12 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(478.8, 179.7);

        this.text_13 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 19;
        this.text_13.setTransform(441.1, 179.7);

        this.text_14 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(399.1, 179.7);

        this.text_15 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(357.2, 179.7);

        this.text_16 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(315.2, 179.7);

        this.text_17 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_17.lineHeight = 19;
        this.text_17.setTransform(273.3, 179.7);

        this.text_18 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_18.lineHeight = 19;
        this.text_18.setTransform(218.7, 179.7);

        this.text_19 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_19.lineHeight = 19;
        this.text_19.setTransform(182.3, 179.7);

        this.text_20 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_20.lineHeight = 19;
        this.text_20.setTransform(140.3, 179.7);

        this.text_21 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_21.lineHeight = 19;
        this.text_21.setTransform(98.4, 179.7);

        this.text_22 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_22.lineHeight = 19;
        this.text_22.setTransform(56.4, 179.7);

        this.text_23 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_23.lineHeight = 19;
        this.text_23.setTransform(14.5, 179.7);

        this.text_24 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_24.lineHeight = 19;
        this.text_24.setTransform(478.4, 69.3);

        this.text_25 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_25.lineHeight = 19;
        this.text_25.setTransform(441.9, 69.3);

        this.text_26 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_26.lineHeight = 19;
        this.text_26.setTransform(400, 69.3);

        this.text_27 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_27.lineHeight = 19;
        this.text_27.setTransform(358, 69.3);

        this.text_28 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_28.lineHeight = 19;
        this.text_28.setTransform(316.1, 69.3);

        this.text_29 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_29.lineHeight = 19;
        this.text_29.setTransform(274.1, 69.3);

        this.text_30 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_30.lineHeight = 19;
        this.text_30.setTransform(218.7, 69.3);

        this.text_31 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_31.lineHeight = 19;
        this.text_31.setTransform(182.3, 69.3);

        this.text_32 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_32.lineHeight = 19;
        this.text_32.setTransform(140.3, 69.3);

        this.text_33 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_33.lineHeight = 19;
        this.text_33.setTransform(98.4, 69.3);

        this.text_34 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_34.lineHeight = 19;
        this.text_34.setTransform(56.4, 69.3);

        this.text_35 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_35.lineHeight = 19;
        this.text_35.setTransform(14.5, 69.3);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgcQgdg5hHAAg");
        this.shape.setTransform(383.9, 270.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgcQgdg5hHAAg");
        this.shape_1.setTransform(383.9, 158.7);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4naQgcAAgcAPQg5AcAABHIAALRIACASQADAVAJASQAcA5BHAAMAjxAAAIASgCQAVgEASgIQA5gdAAhHIAArRQAAgdgOgcQgdg5hHAAg");
        this.shape_2.setTransform(383.9, 47.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_3.setTransform(125.9, 270.5);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_4.setTransform(125.9, 158.7);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4naQgcAAgdAPQg4AcAABHIAALRIABASQAEAVAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhHIAArRQAAgdgNgcQgdg5hHAAg");
        this.shape_5.setTransform(125.9, 47.5);

        this.instance = new lib.s4_5_3();
        this.instance.setTransform(16.9, 9, 0.5, 0.5);

        this.addChild(this.instance, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.ra6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.8, 320);


    (lib.Symbol2 = function() {
        this.initialize();


        this.ra4 = new lib.Symbol14();
        this.ra4.setTransform(437.3, 235.3, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra3 = new lib.Symbol13();
        this.ra3.setTransform(27.1, 235.3, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra2 = new lib.Symbol12();
        this.ra2.setTransform(336.1, 121.7, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra1 = new lib.Symbol11();
        this.ra1.setTransform(127, 121.7, 1, 1, 0, 0, 0, 10.3, 10.3);


        this.text = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(481.1, 223.2);

        this.text_1 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(431, 223.2);

        this.text_2 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(380.8, 223.2);

        this.text_3 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(330.6, 223.2);

        this.text_4 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(280.4, 223.2);

        this.text_5 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(221.8, 223.2);

        this.text_6 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(171.6, 223.2);

        this.text_7 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(121.4, 223.2);

        this.text_8 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(71.2, 223.2);

        this.text_9 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(21, 223.2);

        this.text_10 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(481.1, 110.4);

        this.text_11 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(430.9, 110.4);

        this.text_12 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(380.8, 110.4);

        this.text_13 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 19;
        this.text_13.setTransform(330.6, 110.4);

        this.text_14 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(280.4, 110.4);

        this.text_15 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(221.7, 110.4);

        this.text_16 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(171.6, 110.4);

        this.text_17 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_17.lineHeight = 19;
        this.text_17.setTransform(121.4, 110.4);

        this.text_18 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_18.lineHeight = 19;
        this.text_18.setTransform(71.2, 110.4);

        this.text_19 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_19.lineHeight = 19;
        this.text_19.setTransform(21, 110.4);

        this.text_20 = new cjs.Text("Kuin-ka mon-ta? Ym-py-röi lu-ku.", "16px 'Myriad Pro'");
        this.text_20.lineHeight = 19;
        this.text_20.setTransform(19, 1.2);

        this.text_21 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_21.lineHeight = 20;

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgdQgdg4hHAAg");
        this.shape.setTransform(386.4, 201.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACASQADAVAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgcQgdg5hHAAg");
        this.shape_1.setTransform(386.4, 88.3);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgdQgdg4hHAAg");
        this.shape_2.setTransform(128.4, 201.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABASQAEAVAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_3.setTransform(128.4, 88.3);

        this.instance = new lib.s4_5_2();
        this.instance.setTransform(43, 59.2, 0.5, 0.5);

        this.addChild(this.instance, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 250);


    (lib.Symbol1 = function() {
        this.initialize();


        this.ra6 = new lib.Symbol10();
        this.ra6.setTransform(497.7, 331.2, 1, 1, 0, 0, 0, 10.6, 10.6);

        this.ra5 = new lib.Symbol9();
        this.ra5.setTransform(163.8, 330.1, 1, 1, 0, 0, 0, 10.2, 10.2);

        this.ra4 = new lib.Symbol8();
        this.ra4.setTransform(414.6, 220.1, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra3 = new lib.Symbol7();
        this.ra3.setTransform(129.1, 220.1, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra2 = new lib.Symbol6();
        this.ra2.setTransform(386.3, 110, 1, 1, 0, 0, 0, 10.3, 10.3);

        this.ra1 = new lib.Symbol5();
        this.ra1.setTransform(179.4, 110, 1, 1, 0, 0, 0, 10.3, 10.3);


        this.text = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(487.9, 319.4);

        this.text_1 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(466.9, 319.4);

        this.text_2 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(441.6, 319.4);

        this.text_3 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(416.4, 319.4);

        this.text_4 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(391.2, 319.4);

        this.text_5 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(365.9, 319.4);

        this.text_6 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(340.7, 319.4);

        this.text_7 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(315.5, 319.4);

        this.text_8 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(290.2, 319.4);

        this.text_9 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(266.8, 319.4);

        this.text_10 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(229.8, 319.4);

        this.text_11 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(208.8, 319.4);

        this.text_12 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(183.6, 319.4);

        this.text_13 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 19;
        this.text_13.setTransform(158.3, 319.4);

        this.text_14 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(133.1, 319.4);

        this.text_15 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(107.9, 319.4);

        this.text_16 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(82.6, 319.4);

        this.text_17 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_17.lineHeight = 19;
        this.text_17.setTransform(57.4, 319.4);

        this.text_18 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_18.lineHeight = 19;
        this.text_18.setTransform(32.2, 319.4);

        this.text_19 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_19.lineHeight = 19;
        this.text_19.setTransform(9.2, 319.4);

        this.text_20 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_20.lineHeight = 19;
        this.text_20.setTransform(493.1, 208.8);

        this.text_21 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_21.lineHeight = 19;
        this.text_21.setTransform(465, 208.8);

        this.text_22 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_22.lineHeight = 19;
        this.text_22.setTransform(437, 208.8);

        this.text_23 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_23.lineHeight = 19;
        this.text_23.setTransform(408.9, 208.8);

        this.text_24 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_24.lineHeight = 19;
        this.text_24.setTransform(380.9, 208.8);

        this.text_25 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_25.lineHeight = 19;
        this.text_25.setTransform(352.8, 208.8);

        this.text_26 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_26.lineHeight = 19;
        this.text_26.setTransform(324.7, 208.8);

        this.text_27 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_27.lineHeight = 19;
        this.text_27.setTransform(296.7, 208.8);

        this.text_28 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_28.lineHeight = 19;
        this.text_28.setTransform(268.6, 208.8);

        this.text_29 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_29.lineHeight = 19;
        this.text_29.setTransform(234.5, 208.8);

        this.text_30 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_30.lineHeight = 19;
        this.text_30.setTransform(206.5, 208.8);

        this.text_31 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_31.lineHeight = 19;
        this.text_31.setTransform(178.4, 208.8);

        this.text_32 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_32.lineHeight = 19;
        this.text_32.setTransform(150.3, 208.8);

        this.text_33 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_33.lineHeight = 19;
        this.text_33.setTransform(122.3, 208.8);

        this.text_34 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_34.lineHeight = 19;
        this.text_34.setTransform(94.2, 208.8);

        this.text_35 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_35.lineHeight = 19;
        this.text_35.setTransform(66.1, 208.8);

        this.text_36 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_36.lineHeight = 19;
        this.text_36.setTransform(38.1, 208.8);

        this.text_37 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_37.lineHeight = 19;
        this.text_37.setTransform(10, 208.8);

        this.text_38 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_38.lineHeight = 19;
        this.text_38.setTransform(481.3, 98);

        this.text_39 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_39.lineHeight = 19;
        this.text_39.setTransform(430.9, 98);

        this.text_40 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_40.lineHeight = 19;
        this.text_40.setTransform(380.4, 98);

        this.text_41 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_41.lineHeight = 19;
        this.text_41.setTransform(329.9, 98);

        this.text_42 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_42.lineHeight = 19;
        this.text_42.setTransform(279.5, 98);

        this.text_43 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_43.lineHeight = 19;
        this.text_43.setTransform(223, 98);

        this.text_44 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_44.lineHeight = 19;
        this.text_44.setTransform(172.5, 98);

        this.text_45 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_45.lineHeight = 19;
        this.text_45.setTransform(122.1, 98);

        this.text_46 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_46.lineHeight = 19;
        this.text_46.setTransform(71.6, 98);

        this.text_47 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_47.lineHeight = 19;
        this.text_47.setTransform(21.2, 98);

        this.text_48 = new cjs.Text("Hur många ser du i bilden? Ringa in.", "16px 'Myriad Pro'");
        this.text_48.lineHeight = 19;
        this.text_48.setTransform(19, 1.2);

        this.text_49 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_49.lineHeight = 0;
        this.text_49.setTransform(0, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACARQAEAWAIASQAdA4BHAAMAjxAAAQAcAAAdgOQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape.setTransform(386.4, 299.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4naQgdAAgcAOQg5AdAABHIAALRIACARQAEAWAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhHIAArRQAAgdgNgcQgdg5hHAAg");
        this.shape_1.setTransform(386.4, 76.3);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACASQAEAVAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_2.setTransform(386.4, 187.3);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAArRQAAgcgPgcQgcg5hHAAg");
        this.shape_3.setTransform(128.4, 299.5);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4naQgHAAgLACQgVAEgSAIQg5AdAABHIAALRIACARQAEAWAIASQAdA5BHAAMAjxAAAQAdAAAcgOQA5gdAAhHIAArRQAAgdgPgcQgcg5hHAAg");
        this.shape_4.setTransform(128.4, 76.3);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACASQAEAVAIASQAdA5BHAAMAjxAAAQAdAAAcgOQA5gdAAhHIAArRQAAgcgPgcQgcg5hHAAg");
        this.shape_5.setTransform(128.4, 187.3);

        this.instance = new lib.s4_5_1();
        this.instance.setTransform(97.1, 38.6, 0.5, 0.5);

        this.addChild(this.instance, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_49, this.text_48, this.text_47, this.text_46, this.text_45, this.text_44, this.text_43, this.text_42, this.text_41, this.text_40, this.text_39, this.text_38, this.text_37, this.text_36, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.ra6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 348);


    (lib.s4_5 = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 461.9, 1, 1, 0, 0, 0, 256.3, 173.6);
        //this.addChild(this.v1,this.v2,this.v3,this.muu);
        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
