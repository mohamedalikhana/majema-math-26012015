(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p69_1.png",
            id: "p69_1"
        }, {
            src: "images/p69_2.png",
            id: "p69_2"
        }]
    };

    // symbols:

    (lib.p69_1 = function() {
        this.initialize(img.p69_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p69_2 = function() {
        this.initialize(img.p69_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("69", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(577, 661);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(461 + (columnSpace * 33), 35, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Dubblera.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 50);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(10, 50);

        this.instance = new lib.p69_1();
        this.instance.setTransform(340, 77, 0.46, 0.46);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 505, 129, 10);
        this.roundRect1.setTransform(10, 60);

        
        var ToBeAdded = [];
        var i = 2;        
        this.hrLine_1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(i + " + " + i + " = ", "16px 'Myriad Pro'");
                tempText.setTransform(40 + (160 * colSpace), 86 + (28 * rowSpace));
                this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(88 + (160 * colSpace), 90 + (28 * rowSpace)).lineTo(123 + (160 * colSpace), 90 + (28 * rowSpace));
                this.hrLine_1.setTransform(0, 0);
                ToBeAdded.push(tempText, this.hrLine_1);
                i++;

            }
        }

        
        this.text_2 = new cjs.Text("Det är bra att.", "12px 'Myriad Pro'");
        this.text_2.setTransform(362, 100);

        this.text_3 = new cjs.Text("kunna dubblorna.", "12px 'Myriad Pro'");
        this.text_3.setTransform(355, 115);

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5);
        this.addChild(this.text_2,this.text_3);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 107.1);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 505, 127, 10);
        this.roundRect1.setTransform(10, 20);

        this.text = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 10);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(10, 10);

        this.instance = new lib.p69_1();
        this.instance.setTransform(340, 36, 0.46, 0.46);

        var ToBeAdded = [];
        var i = 4;
        var j = 2;
        var xpos = 40;
        this.hrLine_1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(i + " – " + j + " = ", "16px 'Myriad Pro'");
                if (i == 10) {
                    xpos = 32
                } else {
                    xpos = 40
                }
                tempText.setTransform(xpos + (160 * colSpace), 45 + (28 * rowSpace));
                this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(88 + (168 * colSpace), 46 + (29 * rowSpace)).lineTo(123 + (168 * colSpace), 46 + (29 * rowSpace));
                this.hrLine_1.setTransform(0, 0);
                ToBeAdded.push(tempText, this.hrLine_1);
                i = i + 2;
                j++;

            }
        }
        this.text_2 = new cjs.Text("Tänk på dubblorna.", "12px 'Myriad Pro'");
        this.text_2.setTransform(352, 67);

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.text_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 107.1);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Hur många tortillas är det i 2 påsar?", "16px 'Myriad Pro'");
        this.text.setTransform(29, 10);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(10, 10);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 250, 103.2, 10);
        this.roundRect1.setTransform(10, 20);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(266, 20);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(10, 129.5);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(266, 129.5);

        this.instance = new lib.p69_2();
        this.instance.setTransform(26, 32, 0.46, 0.46);

        var ToBeAdded = [];
        var xpos = 110;
        var ypos = 85;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 195
            } else {
                ypos = 85
            }
            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var column = 0; column < 6; column++) {
                    var columnSpace = column;
                    if (numOfcol == 1) {
                        xpos = 370
                    } else {
                        xpos = 110
                    }

                    tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos, 21, 23);
                    ToBeAdded.push(tmp_Rect);

                }
            }
        }

        var i = 6; 
        var tmpval=0;
        var colPos=260;  
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 2; row++) {
                var rowSpace = row;
                tmpval=i;
                if(i==7){tmpval=9}
                if(i==9){tmpval=10}                   
                var tempText = new cjs.Text(tmpval , "16px 'Myriad Pro'");                
                    if (i==9){colPos=250}
                tempText.setTransform(55 + (colPos * colSpace), 72 + (110 * rowSpace));
                var tempText1 = new cjs.Text("Tortillas", "16px 'Myriad Pro'");
                tempText1.setTransform(35 + (colPos * colSpace), 85 + (110 * rowSpace));               
                ToBeAdded.push(tempText,tempText1);
                i++;
            }
        }
   

        this.addChild(this.text, this.text_1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        this.addChild(this.instance);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 250.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(302.3, 70, 1, 1, 0, 0, 0, 254.6, 40);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(302.3, 275, 1, 1, 0, 0, 0, 255.8, 38);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(302.3, 442, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
