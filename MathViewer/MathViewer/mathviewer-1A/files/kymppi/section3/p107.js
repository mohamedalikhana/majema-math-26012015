(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p107_1.png",
            id: "p107_1"
        },{
            src: "images/p107_2.png",
            id: "p107_2"
        }]
    };

    (lib.p107_1 = function() {
        this.initialize(img.p107_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p107_2 = function() {
        this.initialize(img.p107_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("107", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(553, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(118, 26);

        this.text_4 = new cjs.Text("37", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(67, 22);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52, 23.5,1.2,1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 145, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 33, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p107_1();
        this.instance.setTransform(503, 38, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Räkna.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (column == 0 && row > 3) {
                    continue;
                } else if (column == 1 && row > 4) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.69;
                } else if (column == 2) {
                    columnSpace = 1.392;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.8).drawRect(59 + (columnSpace * 258), 29 + (row * 28), 17, 21);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var ToBeAdded = [];
        var arrxPos = [];
        arrxPos = ['85'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "=   9";

            for (var j = 0; j < 4; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 2) {
                        rowSpace = 2.05;
                        text = "=   10";
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "=   10";
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 33 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        arrxPos = [];
        arrxPos = ['25', '157', '330'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "";

            for (var j = 0; j < 4; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 0) {
                        text = "6   +";
                    } else if (j == 1) {
                        text = "2   +";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "4   +";
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "3   +";
                    }
                } else if (i == 1) {
                    if (j == 0) {
                        text = "2   +   3   +   4   =";
                    } else if (j == 1) {
                        text = "1   +   4   +   5   =";
                    } else if (j == 2) {
                        text = "3   +   6   –   5   =";
                        rowSpace = 2.05;
                        xPos = 158;
                    } else if (j == 3) {
                        text = "1   +   9   –   6   =";
                        rowSpace = 3.045;
                    }
                } else if (i == 2) {
                    if (j == 0) {
                        text = "10   –   4   –   3   =";
                    } else if (j == 1) {
                        text = "10   –   5   –   5   =";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "6   +   4   –   5   =";
                        xPos = 336.5;
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "7   +   3   –   6   =";
                        xPos = 335;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 33 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        for (var j = 0; j < 4; j++) {
            var rowSpace = j;

            var temp_Line = new cjs.Shape();
            temp_Line.graphics.s("#949599").ss(1).moveTo(265, 52 + (rowSpace * 27)).lineTo(295, 52 + (rowSpace * 27));
            temp_Line.setTransform(0, 0);

            var temp_Line_2 = new cjs.Shape();
            temp_Line_2.graphics.s("#949599").ss(1).moveTo(443, 52 + (rowSpace * 27)).lineTo(473, 52 + (rowSpace * 27));
            temp_Line_2.setTransform(0, 0);

            ToBeAdded.push(temp_Line, temp_Line_2);
        }

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.shape_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 132, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 25, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p107_1();
        this.instance.setTransform(503, 30, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Skriv talfamiljen.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        var arrTxtbox = ['65', '99'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 96,
            rectHeight = 22,
            boxWidth = 19.2,
            boxHeight = 22,
            numberOfBoxes = 5,
            numberOfRects = 4;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 14;
                if (i == 1) {
                    padding = 16.5;
                } else if (i == 2) {
                    padding = 23.6;
                } else if (i == 3) {
                    padding = 22;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        arrTxtbox = [];
        arrTxtbox = ['72', '321'];

        this.textbox_group2 = new cjs.Shape();
        for (var i = 0; i < arrTxtbox.length; i++) {
            var xPos = parseInt(arrTxtbox[i]);
            var fColor, sColor;

            if (i == 0) {
                fColor = '#D8E9C0';
                sColor = '#88C240';
            } else {
                fColor = '#B4E3ED';
                sColor = '#00B0CA';
            }

            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) {
                    if (column == 2 && row == 2) {
                        continue;
                    }
                    this.textbox_group2.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xPos + (columnSpace * 38), 35, 19, 19);
                }
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.text_1 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 30;
        this.text_1.setTransform(75, 35);
        this.text_2 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 30;
        this.text_2.setTransform(113.5, 35);
        this.text_3 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 30;
        this.text_3.setTransform(151, 35);

        this.text_4 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 30;
        this.text_4.setTransform(324, 35);
        this.text_5 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 30;
        this.text_5.setTransform(362, 35);
        this.text_6 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 30;
        this.text_6.setTransform(400, 35);

        var ToBeAdded = [];
        for (var i = 0; i < 2; i++) {
            var colSpace = i;
            for (var j = 0; j < 2; j++) {
                var rowSpace = j;
                if (i == 1 && j == 1) {
                    colSpace = 0.993;
                }
                var temp_text_1 = new cjs.Text("+", "16px 'Myriad Pro'");
                temp_text_1.lineHeight = -1;
                temp_text_1.setTransform(38 + (colSpace * 249), 70 + (rowSpace * 34));

                var temp_text_2 = new cjs.Text("–", "16px 'Myriad Pro'");
                temp_text_2.lineHeight = -1;
                temp_text_2.setTransform(154 + (colSpace * 247), 70 + (rowSpace * 34));

                ToBeAdded.push(temp_text_1, temp_text_2);
            }
        }

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(244, 34).lineTo(244, 127);
        this.Line_1.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.textbox_group2);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.Line_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 142, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 32, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p107_1();
        this.instance.setTransform(503, 37, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Skriv talen i storleksordning.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        var arrTxtbox = ['34', '193', '359'];

        this.textbox_group1 = new cjs.Shape();
        for (var i = 0; i < arrTxtbox.length; i++) {
            var xPos = parseInt(arrTxtbox[i]);
            var fColor, sColor;

            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 2; row++) {
                    if (row == 0) {
                        fColor = '#FDCDB0';
                        sColor = '#F1652B';
                    } else {
                        fColor = '#B4E3ED';
                        sColor = '#00B0CA';
                    }

                    this.textbox_group1.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xPos + (columnSpace * 38), 32 + (row * 61), 19, 19);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var ToBeAdded = [];
        for (var i = 0; i < arrTxtbox.length; i++) {
            var xPos = parseInt(arrTxtbox[i]);
            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 2; row++) {

                    var temp_Line = new cjs.Shape();
                    temp_Line.graphics.s("#949599").ss(1).moveTo(xPos + (columnSpace * 38), 73 + (row * 61))
                        .lineTo(20 + xPos + (columnSpace * 38), 73 + (row * 61));
                    temp_Line.setTransform(0, 0);

                    ToBeAdded.push(temp_Line);
                }
            }
        }

        var arrtext = ['5', '3', '7', '8', '2', '4', '7', '3', '6'];
        var arrxPos = ['39', '77', '116', '198', '236', '274', '364', '402', '440'];
        for (var j = 0; j < arrxPos.length; j++) {
            var xPos = parseInt(arrxPos[j]);
            var text = arrtext[j];

            var temp_text_1 = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_text_1.lineHeight = -1;
            temp_text_1.setTransform(xPos, 35);

            ToBeAdded.push(temp_text_1);
        }
        arrtext = ['2', '0', '1', '9', '7', '5', '1', '10', '0'];
        for (var j = 0; j < arrxPos.length; j++) {
            var xPos = parseInt(arrxPos[j]);
            var text = arrtext[j];
            if (j == 7) {
                xPos = 397.5;
            }
            var temp_text_1 = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_text_1.lineHeight = -1;
            temp_text_1.setTransform(xPos, 96);

            ToBeAdded.push(temp_text_1);
        }

        for (var i = 0; i < 6; i++) {
            var colSpace = i;
            for (var j = 0; j < 2; j++) {
                var rowSpace = j;
                if (i == 2) {
                    colSpace = 4.1;
                } else if (i == 3) {
                    colSpace = 5.1;
                } else if (i == 4) {
                    colSpace = 8.35;
                } else if (i == 5) {
                    colSpace = 9.3;
                }

                var temp_text_1 = new cjs.Text("<", "16px 'Myriad Pro'");
                temp_text_1.lineHeight = -1;
                temp_text_1.setTransform(57 + (colSpace * 39), 56 + (rowSpace * 60));

                ToBeAdded.push(temp_text_1);
            }
        }

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(161, 28).lineTo(161, 81).moveTo(161, 86).lineTo(161, 138)
            .moveTo(323, 28).lineTo(323, 81).moveTo(323, 86).lineTo(323, 138);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.s("#949599").ss(1).moveTo(10, 83).lineTo(490, 83);
        this.Line_2.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1);
        this.addChild(this.Line_1, this.Line_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol4 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 140, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 25, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p107_1();
        this.instance.setTransform(503, 30, 0.4, 0.4);

        this.instance_2 = new lib.p107_2();
        this.instance_2.setTransform(15, 10, 0.62, 0.63 );

        this.text_q1 = new cjs.Text("Det här är Manuels knappar:", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 10);

        this.text_q2 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 10);

        var arrTxtbox = ['86'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 115.2,
            rectHeight = 22,
            boxWidth = 19.2,
            boxHeight = 22,
            numberOfBoxes = 6,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 98;
                if (i == 1) {
                    padding = 80;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("Mira har dubbelt så många.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 30;
        this.text_1.setTransform(23, 39);
        this.text_2 = new cjs.Text("Leo har 7 fler.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 30;
        this.text_2.setTransform(275, 39);
       
        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(244, 40).lineTo(244, 111);
        this.Line_1.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2,this.instance_2, this.textbox_group1);
        this.addChild(this.text_1, this.text_2, this.Line_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(314, 117, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(314, 267, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(314, 404, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v4 = new lib.Symbol4();
        this.v4.setTransform(314, 551, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
