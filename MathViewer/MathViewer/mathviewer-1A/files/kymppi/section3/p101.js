(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p101_1.png",
            id: "p101_1"
        }, {
            src: "images/p101_2.png",
            id: "p101_2"
        }]
    };

    // symbols:
    (lib.p101_1 = function() {
        this.initialize(img.p101_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p101_2 = function() {
        this.initialize(img.p101_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("101", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 650);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.instance = new lib.p101_1();
        this.instance.setTransform(79, 32, 0.465, 0.445);

        this.addChild(this.shape, this.text, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 517, 135, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Ringa in 2 tal som tillsammans är 10.", "15.5px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 15.5px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#F1662B").ss(0.9).drawRect(30 + (columnSpace * 256), 36.5 + (row * 30), 206, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        //row-1
        var ToBeAdded = [];
        var arrCol1text = ['2', '3', '6', '7', '5', '1'];
        var arrCol2text = ['1', '8', '7', '2', '4', '5'];
        for (var i = 0; i < 6; i++) {
            var colSpace = i;
            var col1text = arrCol1text[i];
            var col2text = arrCol2text[i];

            var temp_text1 = new cjs.Text(col1text, "15.5px 'Myriad Pro'");
            temp_text1.lineHeight = -1;
            temp_text1.setTransform(43 + (colSpace * 34), 42);
            var temp_text2 = new cjs.Text(col2text, "15.5px 'Myriad Pro'");
            temp_text2.lineHeight = -1;
            temp_text2.setTransform(43 + 256 + (colSpace * 34), 42);

            ToBeAdded.push(temp_text1, temp_text2);
        }

        arrCol1text = arrCol2text = []; //row-2
        arrCol1text = ['4', '8', '3', '9', '6', '5'];
        arrCol2text = ['8', '1', '5', '3', '4', '9'];
        for (var i = 0; i < 6; i++) {
            var colSpace = i;
            var col1text = arrCol1text[i];
            var col2text = arrCol2text[i];

            var temp_text1 = new cjs.Text(col1text, "15.5px 'Myriad Pro'");
            temp_text1.lineHeight = -1;
            temp_text1.setTransform(43 + (colSpace * 34), 72);
            var temp_text2 = new cjs.Text(col2text, "15.5px 'Myriad Pro'");
            temp_text2.lineHeight = -1;
            temp_text2.setTransform(43 + 256 + (colSpace * 34), 72);

            ToBeAdded.push(temp_text1, temp_text2);
        }

        arrCol1text = arrCol2text = []; //row-3
        arrCol1text = ['9', '3', '5', '6', '2', '1'];
        arrCol2text = ['6', '9', '5', '7', '4', '2'];
        for (var i = 0; i < 6; i++) {
            var colSpace = i;
            var col1text = arrCol1text[i];
            var col2text = arrCol2text[i];

            var temp_text1 = new cjs.Text(col1text, "15.5px 'Myriad Pro'");
            temp_text1.lineHeight = -1;
            temp_text1.setTransform(43 + (colSpace * 34), 101.5);
            var temp_text2 = new cjs.Text(col2text, "15.5px 'Myriad Pro'");
            temp_text2.lineHeight = -1;
            temp_text2.setTransform(43 + 256 + (colSpace * 34), 101.5);

            ToBeAdded.push(temp_text1, temp_text2);
        }

        arrCol1text = arrCol2text = []; //row-4
        arrCol1text = ['5', '8', '7', '4', '5', '1'];
        arrCol2text = ['9', '5', '2', '7', '6', '5'];
        for (var i = 0; i < 6; i++) {
            var colSpace = i;
            var col1text = arrCol1text[i];
            var col2text = arrCol2text[i];
            if (i == 5) {
                colSpace = 4.95;
            }

            var temp_text1 = new cjs.Text(col1text, "15.5px 'Myriad Pro'");
            temp_text1.lineHeight = -1;
            temp_text1.setTransform(43 + (colSpace * 34), 132);
            var temp_text2 = new cjs.Text(col2text, "15.5px 'Myriad Pro'");
            temp_text2.lineHeight = -1;
            temp_text2.setTransform(43 + 256 + (colSpace * 34), 132);

            ToBeAdded.push(temp_text1, temp_text2);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 325.9);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Räkna och hitta bokstäverna.", "15.5px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 15.5px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p101_2();
        this.instance.setTransform(105, 218, 0.38, 0.385);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 517, 296, 10);
        this.roundRect1.setTransform(0, 25);
        //column-1
        this.label1 = new cjs.Text("10  –  4  =", "15.5px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(20, 48);
        this.label2 = new cjs.Text("10  –  8  =", "15.5px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(20, 74);
        this.label3 = new cjs.Text("2  +  6  =", "15.5px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(27, 100);
        this.label4 = new cjs.Text("5  +  3  =", "15.5px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(27, 125);
        this.label5 = new cjs.Text("10  –  5  =", "15.5px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(20, 153);
        //column-2
        this.label8 = new cjs.Text("1  +  1  –  3  =", "15.5px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(169, 48);
        this.label9 = new cjs.Text("10  –  4  –  4  =", "15.5px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(162, 74);
        this.label10 = new cjs.Text("9  –  3  –  3  =", "15.5px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(169, 100);
        this.label11 = new cjs.Text("10  –  5  –  2  =", "15.5px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(162, 125);
        this.label12 = new cjs.Text("10  –  2  –  3  =", "15.5px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(162, 153);
        this.label13 = new cjs.Text("3  +  1  +  5  =", "15.5px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(167.5, 178);
        //column-3
        this.label15 = new cjs.Text("10  –  2  –  1  =", "15.5px 'Myriad Pro'");
        this.label15.lineHeight = 19;
        this.label15.setTransform(340, 48);
        this.label16 = new cjs.Text("10  –  8  –  1  =", "15.5px 'Myriad Pro'");
        this.label16.lineHeight = 19;
        this.label16.setTransform(340, 74);
        this.label17 = new cjs.Text("3  +  2  +  1  =", "15.5px 'Myriad Pro'");
        this.label17.lineHeight = 19;
        this.label17.setTransform(345.5, 100);
        this.label18 = new cjs.Text("5  –  4  –  1  =", "15.5px 'Myriad Pro'");
        this.label18.lineHeight = 19;
        this.label18.setTransform(345.5, 153);
        this.label19 = new cjs.Text("1  +  2  +  2  =", "15.5px 'Myriad Pro'");
        this.label19.lineHeight = 19;
        this.label19.setTransform(343.5, 178);
        this.label20 = new cjs.Text("3  +  3  +  3  =", "15.5px 'Myriad Pro'");
        this.label20.lineHeight = 19;
        this.label20.setTransform(343.5, 203.5);
        this.label21 = new cjs.Text("8  –  1  –  4  =", "15.5px 'Myriad Pro'");
        this.label21.lineHeight = 19;
        this.label21.setTransform(345.5, 230);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 8; row++) {
                if (column == 0 && row > 4) {
                    continue;
                } else if (column == 1 && row > 5) {
                    continue;
                } else if (column == 2 && row == 3) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.65;
                } else if (column == 2) {
                    columnSpace = 1.36;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#F1662B").ss(0.9).drawRect(120 + (columnSpace * 258), 41 + (row * 26), 20, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 10; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#F1662B").ss(0.9).drawRect(27 + (columnSpace * 28.5), 257, 20, 22);
        }
        this.textbox_group2.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(86, 63).lineTo(111, 63).moveTo(86, 88.5).lineTo(111, 88.5).moveTo(86, 114.5).lineTo(111, 114.5)
            .moveTo(86, 141).lineTo(111, 141).moveTo(86, 167).lineTo(111, 167);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(257, 63).lineTo(282, 63).moveTo(257, 88.5).lineTo(282, 88.5).moveTo(257, 114.5).lineTo(282, 114.5)
            .moveTo(257, 140.5).lineTo(282, 140.5).moveTo(257, 167).lineTo(282, 167).moveTo(257, 192.5).lineTo(282, 192.5);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(435, 63).lineTo(460, 63).moveTo(435, 88.5).lineTo(460, 88.5).moveTo(435, 114.5).lineTo(460, 114.5)
            .moveTo(435, 167).lineTo(460, 167).moveTo(435, 192.5).lineTo(460, 192.5).moveTo(435, 218.5).lineTo(460, 218.5).moveTo(435, 244.5).lineTo(460, 244.5);
        this.hrLine_3.setTransform(0, 0);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(26.5, 286).lineTo(308, 286);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(308, 286, 1.2, 1.2, 265);

        var arrText = ['B', 'C', 'I', 'N', 'P', 'A', 'H', 'O', 'T', 'R'];
        var ToBeAdded = [];

        for (var i = 0; i < arrText.length; i++) {
            var columnSpace = i;
            var text = arrText[i];

            if (i == 2) {
                columnSpace = 2.109;
            } else if (i == 3) {
                columnSpace = 3;
            } else if (i == 4) {
                columnSpace = 4.08;
            } else if (i == 5) {
                columnSpace = 5.1;
            } else if (i == 6) {
                columnSpace = 6.1;
            } else if (i == 7) {
                columnSpace = 7.1;
            } else if (i == 8) {
                columnSpace = 8.1;
            } else if (i == 9) {
                columnSpace = 9.2;
            }

            var temp_label = new cjs.Text(text, "15.5px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(33 + (columnSpace * 28), 262);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 10; i++) {
            var columnSpace = i;
            var text = i.toString();

            if (i == 2) {
                columnSpace = 1.87;
            } else if (i == 3) {
                columnSpace = 2.78;
            } else if (i == 4) {
                columnSpace = 3.75;
            } else if (i == 5) {
                columnSpace = 4.66;
            } else if (i == 6) {
                columnSpace = 5.59;
            } else if (i == 7) {
                columnSpace = 6.49;
            } else if (i == 8) {
                columnSpace = 7.35;
            } else if (i == 9) {
                columnSpace = 8.34;
            }

            var temp_label = new cjs.Text(text, "15.5px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(32 + (columnSpace * 31), 292);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 10; i++) {
            var columnSpace = i;

            if (i == 2) {
                columnSpace = 1.87;
            } else if (i == 3) {
                columnSpace = 2.78;
            } else if (i == 4) {
                columnSpace = 3.75;
            } else if (i == 5) {
                columnSpace = 4.66;
            } else if (i == 6) {
                columnSpace = 5.59;
            } else if (i == 7) {
                columnSpace = 6.49;
            } else if (i == 8) {
                columnSpace = 7.35;
            } else if (i == 9) {
                columnSpace = 8.34;
            }
            var temp_Line = new cjs.Shape();
            temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(36 + (columnSpace * 31), 277).lineTo(36 + (columnSpace * 31), 286);
            temp_Line.setTransform(0, 0);
            ToBeAdded.push(temp_Line);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4);
        this.addChild(this.instance, this.textbox_group1, this.textbox_group2, this.shape_1)
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label8, this.label9, this.label10, this.label11, this.label12, this.label13);
        this.addChild(this.label15, this.label16, this.label17, this.label18, this.label19, this.label20, this.label21);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310, 164, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(310, 368, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
