(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p25_1.png",
            id: "p25_1"
        }, {
            src: "images/p25_2.png",
            id: "p25_2"
        }]
    };


    (lib.p25_1 = function() {
        this.initialize(img.p25_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p25_2 = function() {
        this.initialize(img.p25_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("25", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 657);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.addChild(this.shape_1, this.text_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text   


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Räkna och hitta bokstäverna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 13);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 13);

        this.instance = new lib.p25_1();
        this.instance.setTransform(315, 28, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 510, 260, 10);
        this.roundRect1.setTransform(0, 25);

        //column-1
        this.label1 = new cjs.Text("11  +  4  =", "16px 'Myriad Pro'");
        this.label1.setTransform(13, 57);
        this.label2 = new cjs.Text("11  +  7  =", "16px 'Myriad Pro'");
        this.label2.setTransform(13, 84);
        this.label3 = new cjs.Text("11  +  3  =", "16px 'Myriad Pro'");
        this.label3.setTransform(13, 111);
        this.label4 = new cjs.Text("12  +  4  =", "16px 'Myriad Pro'");
        this.label4.setTransform(13, 137);
        this.label5 = new cjs.Text("12  +  5  =", "16px 'Myriad Pro'");
        this.label5.setTransform(13, 163);

        //column-2
        this.label6 = new cjs.Text("19  –  6  =", "16px 'Myriad Pro'");
        this.label6.setTransform(177, 57);
        this.label7 = new cjs.Text("19  –  8  =", "16px 'Myriad Pro'");
        this.label7.setTransform(177, 84);
        this.label8 = new cjs.Text("18  –  6  =", "16px 'Myriad Pro'");
        this.label8.setTransform(177, 111);
        this.label9 = new cjs.Text("17  –  7  =", "16px 'Myriad Pro'");
        this.label9.setTransform(177, 137);
        this.label10 = new cjs.Text("10  –  3  =", "16px 'Myriad Pro'");
        this.label10.setTransform(177, 163);
        this.label11 = new cjs.Text("10  –  1  =", "16px 'Myriad Pro'");
        this.label11.setTransform(177, 189);
        this.label12 = new cjs.Text(" 9   –  1  =", "16px 'Myriad Pro'");
        this.label12.setTransform(177, 215);

        var lineArr = [];
        var lineX = [72, 72, 72, 72, 72, 234, 234, 234, 234, 234, 234, 234];
        var lineY = [22, 48, 75, 102, 129, 22, 48, 75, 102, 129, 156, 183];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var TxtArr = ['N', 'G', 'K', 'I', 'N', 'Å', 'R', 'S', 'Ö', 'U', 'F', 'T'];
        var TArr = [];
        var TxtlineX = [128, 128, 127, 128, 128, 291, 291, 291, 290, 292, 291, 291];
        var TxtlineY = [58, 84, 111, 138, 165, 57, 84, 111, 138, 165, 192, 219];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var textbox_group1 = new cjs.Shape();

        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 7; row++) {
                if (column == 0 && row > 4) {
                    continue;
                }
                textbox_group1.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(123 + (columnSpace * 163), 40 + (row * 27), 20, 22);
            }
        }
        textbox_group1.setTransform(0, 0);


        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 12; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(24 + (columnSpace * 30.7), 250, 20, 22);
        }
        this.textbox_group2.setTransform(0, 0);


        var arrText = [];

        var ToBeAdded = [];

        for (var i = 0; i < arrText.length; i++) {
            var columnSpace = i;
            var text = arrText[i];

            if (i == 2) {
                columnSpace = 2.109;
            } else if (i == 3) {
                columnSpace = 3.05;
            } else if (i == 4) {
                columnSpace = 4.08;
            } else if (i == 5) {
                columnSpace = 5.1;
            } else if (i == 6) {
                columnSpace = 6.1;
            } else if (i == 7) {
                columnSpace = 7.15;
            } else if (i == 8) {
                columnSpace = 8.17;
            } else if (i == 9) {
                columnSpace = 9.18;
            } else if (i == 10) {
                columnSpace = 10.19;
            } else if (i == 11) {
                columnSpace = 11.25;
            }

            this.temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            this.temp_label.setTransform(35 + (columnSpace * 28), 261.5);
            ToBeAdded.push(this.temp_label);
        }

        for (var i = 0; i < 12; i++) {
            var columnSpace = i;
            var text = (i + 7).toString();
            if (i == 2) {
                columnSpace = 2.1;
            }          
            else if (i == 3) {
                columnSpace = 2.95;
            } 
            else if (i == 8) {
                columnSpace = 8.07;
            }
            else if (i == 9) {
                columnSpace = 9.1;
            }
            else if (i == 10) {
                columnSpace = 10.1;
            }
            else if (i == 11) {
                columnSpace = 11.06;
            } 

            this.temp_label = new cjs.Text(text, "14px 'Myriad Pro'");
            this.temp_label.setTransform(30 + (columnSpace * 30), 245);
            ToBeAdded.push(this.temp_label);
        }


        this.addChild(this.roundRect1, this.text, this.text_1, textbox_group1, this.textbox_group2, this.label1, this.label2, this.label3,
            this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        this.addChild(this.instance)
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 510, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Hur många kronor är kvar?", "16px 'Myriad Pro'");
        this.text.setTransform(14, 10);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(0, 10);

        this.text_2 = new cjs.Text("Kronor från början", "16px 'Myriad Pro'");
        this.text_2.setTransform(13, 43);

        this.text_3 = new cjs.Text("Handlar", "16px 'Myriad Pro'");
        this.text_3.setTransform(250, 43);

        this.text_4 = new cjs.Text("Kronor kvar", "16px 'Myriad Pro'");
        this.text_4.setTransform(410, 43);

        this.text_5 = new cjs.Text("kr", "bold 20px 'UusiTekstausMajema'", "#9D9C9C");
        this.text_5.setTransform(469, 104);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 237, 10);
        this.roundRect1.setTransform(0, 25);

        this.instance = new lib.p25_2();
        this.instance.setTransform(-10, 35, 0.47, 0.47);

        var lineHArr = [];
        var lineHX = [0, 0, 0];
        var lineHY = [11, 81, 152];

        for (var row = 0; row < lineHX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(509, 40);
            hrLine_1.setTransform(lineHX[row], lineHY[row]);
            lineHArr.push(hrLine_1);
        }

        this.hrLine1 = new cjs.Shape();
        this.hrLine1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(57, 40);
        this.hrLine1.setTransform(425, 67);

        this.hrLine2 = new cjs.Shape()
        this.hrLine2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(57, 40);
        this.hrLine2.setTransform(425, 138);

        this.hrLine3 = new cjs.Shape();
        this.hrLine3.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(57, 40);
        this.hrLine3.setTransform(425, 208);

        this.text_6 = new cjs.Text("5 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_6.setTransform(165, 74);
        this.text_6.skewX = 10;
        this.text_6.skewY = 10;

        this.text_7 = new cjs.Text("6 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_7.setTransform(165, 144)
        this.text_7.skewX = -10;
        this.text_7.skewY = -10;

        this.text_8 = new cjs.Text("6 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_8.setTransform(165, 214);
        this.text_8.skewX = -10;
        this.text_8.skewY = -10;

        this.text_9 = new cjs.Text("2 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_9.setTransform(363, 93);
        this.text_9.skewX = -10;
        this.text_9.skewY = -10;

        this.text_10 = new cjs.Text("2 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_10.setTransform(363, 163);
        this.text_10.skewX = -10;
        this.text_10.skewY = -10;

        this.text_11 = new cjs.Text("3 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_11.setTransform(363, 218);
        this.text_11.skewX = -20;
        this.text_11.skewY = -20;


        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(155, 26).lineTo(155, 263);
        this.vrLine_1.setTransform(0, 0);

        this.vrLine_2 = new cjs.Shape();
        this.vrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(397, 26).lineTo(397, 263);
        this.vrLine_2.setTransform(0, 0);

        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.roundRect1, this.instance,
            this.vrLine_1, this.vrLine_2, this.hrLine1, this.hrLine2, this.hrLine3,
            this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11);

        for (var i = 0; i < lineHArr.length; i++) {
            this.addChild(lineHArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 510, 260);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(315, 67, 1, 1, 0, 0, 0, 255.8, 0);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(315, 592, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
