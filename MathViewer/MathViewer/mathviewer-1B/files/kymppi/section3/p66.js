(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p66_1.png",
            id: "p66_1"
        }]
    };

    (lib.p66_1 = function() {
        this.initialize(img.p66_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

         this.text = new cjs.Text("66", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 661);

        this.instance = new lib.p66_1();
        this.instance.setTransform(44, 34, 0.465, 0.465);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(465 + (columnSpace * 32), 36 , 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape,  this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

   
        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#958FC5");      
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Skriv talfamiljen.", "16px 'Myriad Pro'");     
        this.text_1.setTransform(19, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 102, 10);
        this.roundRect1.setTransform(0, 0);    

        this.roundRect2=this.roundRect1.clone(true)
        this.roundRect2.setTransform(0, 109);   

        var ToBeAdded = [];
        var xpos = 20;
        var ypos = 36;
        var tmpxPos = 33;
        fColor = '#F3CAD3';
        sColor = '#D6326A';
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            for (var row = 0; row < 2; row++) {
                for (var column = 0; column < 2; column++) {
                    var columnSpace = (column == 1 && row == 0) ? column + 0.5 : column;
                    if (column == 0 && row == 1) {
                        continue;
                    }
                    if (column == 1 && row == 1 && numOfrow == 0) {
                        tmpxPos = 25;
                        ypos = 51
                    }

                    if (numOfrow == 1) {
                        ypos = 148;
                        tmpxPos = 33;
                        fColor = "#FBD3B9";
                        sColor = '#EB5B1B'
                    }
                    if (column == 1 && row == 1 && numOfrow == 1) {
                        tmpxPos = 25;
                        ypos = 162
                    }
                 
                    tmp_Rect.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xpos + (columnSpace * tmpxPos), ypos + (row * 20), 28, 25);
                    ToBeAdded.push(tmp_Rect);
                }
            }
        }
        var xpos = 30;
        var ypos = 53;
        var tmpxPos = 33;
        var tmpNum = [5, 9, 14, 5, 8, 13]
        var i = 0;
        fColor = '#DFDDF0';
        sColor = '#958FC5';
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            for (var row = 0; row < 2; row++) {
                for (var column = 0; column < 2; column++) {
                    var columnSpace = (column == 1 && row == 0) ? column + 0.5 : column;
                    if (column == 0 && row == 1) {
                        continue;
                    }
                    if (column == 1 && row == 1 && numOfrow == 0) {
                        tmpxPos = 21;
                        ypos = 68
                    }
                    if (numOfrow == 1) {
                        ypos = 165;
                        tmpxPos = 33;
                    }
                    if (column == 1 && row == 1 && numOfrow == 1) {
                        tmpxPos = 20;
                        ypos = 180
                    }
                   
                    var tempText = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                    tempText.setTransform(xpos + (columnSpace * tmpxPos), ypos + (row * 20));
                    ToBeAdded.push(tempText);
                    i++
                }
            }
        }

        var xpos = 120;
        var ypos = 48;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 157
            } else {
                ypos = 48
            }
            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var row = 0; row < 2; row++) {
                    for (var column = 0; column < 6; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 325
                        } else {
                            xpos = 120
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 32), 21, 23);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }          
         for (var row = 0; row < 2; row++) {
            var rowspace = row            
            var tempText = new cjs.Text("summa", "16px 'Myriad Pro'", "#EB602B");
            tempText.setTransform(122 , 40+(rowspace*108));
            var tempText1 =new cjs.Text("differens", "16px 'Myriad Pro'", "#008BDB");
            tempText1.setTransform(327 , 40+(rowspace*108));
            ToBeAdded.push(tempText,tempText1);
            } 
    
        this.addChild(this.roundRect1,this.roundRect2,this.text,this.text_1);     
          for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }  

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");     
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#958FC5");        
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 150, 10);
        this.roundRect1.setTransform(0, 0);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(25, 0).lineTo(342, 0);
        this.Line_1.setTransform(60, 30);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 14; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 100;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 110;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 120;
                }  
                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 25 + (row * 19), 7.9, 0, 2 * Math.PI);
            }

        }this.shape_group1.setTransform(3, 5);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(178, 33);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_3.setTransform(284, 33);

        var ToBeAdded = [];
        var arryVal = [3,5,4,7,3,6,8,9,5,7,9,8];
        var tmpArry=["11  –  ","12  –  ","14  –  "];
        var i = 0;
        var j=0;
        var tmpTxt="";
        var temp_Line1 = new cjs.Shape();
        var tmprow = 28;
        var tmpcol = 158;
        for (var col = 0; col < 3; col++) {
            var columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                 if (col == 1) {
                    tmpcol = 165;
                    j=1;
                } 
                 if (col == 2) {
                    tmpcol = 165;
                    j=2;
                } 
                if (col==2 && row>2){tmpTxt="14  –  "}
                    else {tmpTxt=tmpArry[j]}
                var tempText = new cjs.Text(tmpTxt + arryVal[i] + "  =", "16px 'Myriad Pro'");
                tempText.setTransform(40 + (164 * columnSpace), 63 + (28 * rowSpace));
               
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(105 + (columnSpace * tmpcol), 67 + (rowSpace * tmprow)).lineTo(140 + (columnSpace * tmpcol), 67 + (rowSpace * tmprow));
                ToBeAdded.push(tempText,temp_Line1)
                i++;
            };
        };

       
        this.addChild(this.roundRect1, this.text, this.text_1,this.Line_1,this.shape_group1,this.text_2,this.text_3);
         for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }  

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(300.5, 440, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300.5, 472, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
