(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p71_1.png",
            id: "p71_1"
        }]
    };

    (lib.p71_1 = function() {
        this.initialize(img.p71_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:


    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 7 till 10", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(387, 651);

        this.text_2 = new cjs.Text("71", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(555, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Talet 7", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(111, 25);

        this.text_4 = new cjs.Text("24", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(65, 21.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(51, 23.5, 1.17, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text.lineHeight = 56;
        this.text.setTransform(192.9, 2);

        this.text_1 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_1.lineHeight = 56;
        this.text_1.setTransform(404.8, 2);

        this.text_2 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_2.lineHeight = 56;
        this.text_2.setTransform(155.3, 2);

        this.text_3 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_3.lineHeight = 56;
        this.text_3.setTransform(116.3, 2);

        this.text_4 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_4.lineHeight = 56;
        this.text_4.setTransform(328.2, 2);

        this.text_5 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_5.lineHeight = 56;
        this.text_5.setTransform(78.8, 2);

        this.text_6 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_6.lineHeight = 56;
        this.text_6.setTransform(40.5, 2);

        this.text_7 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_7.lineHeight = 46;
        this.text_7.setTransform(418.6, 103.5);

        this.text_8 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_8.lineHeight = 46;
        this.text_8.setTransform(418.6, 72.8);

        this.text_9 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_9.lineHeight = 46;
        this.text_9.setTransform(418.6, 42.6);

        this.text_10 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_10.lineHeight = 46;
        this.text_10.setTransform(170, 103.8);

        this.text_11 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_11.lineHeight = 46;
        this.text_11.setTransform(170, 72.8);

        this.text_12 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_12.lineHeight = 46;
        this.text_12.setTransform(170, 42.6);

        this.text_13 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_13.lineHeight = 46;
        this.text_13.setTransform(361.9, 103.8);

        this.text_14 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_14.lineHeight = 46;
        this.text_14.setTransform(361.9, 72.8);

        this.text_15 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_15.lineHeight = 46;
        this.text_15.setTransform(361.9, 42.6);

        this.text_16 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_16.lineHeight = 46;
        this.text_16.setTransform(113.4, 103.8);

        this.text_17 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_17.lineHeight = 46;
        this.text_17.setTransform(113.4, 72.8);

        this.text_18 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_18.lineHeight = 46;
        this.text_18.setTransform(113.4, 42.6);

        this.text_19 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_19.lineHeight = 46;
        this.text_19.setTransform(304.9, 103.8);

        this.text_20 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_20.lineHeight = 46;
        this.text_20.setTransform(304.9, 72.8);

        this.text_21 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_21.lineHeight = 46;
        this.text_21.setTransform(304.9, 42.6);

        this.text_22 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_22.lineHeight = 46;
        this.text_22.setTransform(56.3, 103.8);

        this.text_23 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_23.lineHeight = 46;
        this.text_23.setTransform(56.3, 72.8);

        this.text_24 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_24.lineHeight = 46;
        this.text_24.setTransform(56.3, 42.6);

        this.text_25 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_25.lineHeight = 46;
        this.text_25.setTransform(248.6, 103.8);

        this.text_26 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_26.lineHeight = 46;
        this.text_26.setTransform(248.6, 72.8);

        this.text_27 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_27.lineHeight = 46;
        this.text_27.setTransform(248.6, 42.6);

        this.text_28 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_28.lineHeight = 46;
        this.text_28.setTransform(0, 103.8);

        this.text_29 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_29.lineHeight = 46;
        this.text_29.setTransform(0, 72.8);

        this.text_30 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_30.lineHeight = 46;
        this.text_30.setTransform(0, 42.3);

        this.text_31 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_31.lineHeight = 56;
        this.text_31.setTransform(251.7, 2);

        this.text_32 = new cjs.Text("7", "bold 47px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_32.lineHeight = 56;
        this.text_32.setTransform(2.2, 2);

        this.addChild(this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 438.8, 149.4);

    //Part 1
    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("sju", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(50, 79.9);

        this.text_1 = new cjs.Text("7", "bold 80px 'UusiTekstausMajema'");
        this.text_1.lineHeight = 96;
        this.text_1.setTransform(42.1, 5);
        // line arrow above number
        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#000000").ss(0.5, 0, 0, 4).moveTo(50.8, 20.1).lineTo(63.8, 20.1);
        this.shape_38.setTransform(0, 0);
        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_39.setTransform(62.8, 20.1, 1, 1, 270);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(429, 59.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_1.setTransform(424.5, 68, 1, 1, 88);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(429, 45.1);

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(421.8, 26);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(408.1, 30.2);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(398.1, 39.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C6CBCC").s().p("AkPBPQhxhvAAifIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACghxBuQhxBxifAAQieAAhxhxg");
        this.shape_3.setTransform(429, 78.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohmQhohniSAAQiRAAhoBnQhoBmAACTIgfAAQAAifBxhvQBxhxCeAAQCfAABxBxQBxBvAACfg");
        this.shape_4.setTransform(429, 40.1);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(394.5, 53.5);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(399.2, 68);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(409.1, 78.2);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(424.6, 82.8);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(438.9, 78.6);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(450.1, 68.3);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(454.6, 54.2);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(449.9, 39.6);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(439, 30);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(429, 25.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(411.8, 29.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(399.3, 42.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(394.7, 59.4);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(399.3, 76.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAJIAJgR");
        this.shape_10.setTransform(411.8, 89.1);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(429, 93.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAJIgJgR");
        this.shape_12.setTransform(446.1, 89.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKAFIgTgJ");
        this.shape_13.setTransform(458.7, 76.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(463.3, 59.4);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKgEIgTAJ");
        this.shape_15.setTransform(458.7, 42.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(446.1, 29.7);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShnBoQhpBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABpBoQBnBoAACRg");
        this.shape_17.setTransform(429, 59.4);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(429, 59.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(429, 59.4);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(429, 59.4);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQggAAgZAYQgYAYAAAhQAAAiAYAYQAZAYAgAAg");
        this.shape_21.setTransform(258, 31.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_22.setTransform(258, 31.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#0089CA").s("#000000").ss(0.5, 0, 0, 4).arc(0, 0, 8.2, 0, 2 * Math.PI);
        this.shape_52.setTransform(281, 30.7);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_23.setTransform(235.8, 31.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgZAYghAAQghAAgYgYg");
        this.shape_24.setTransform(235.8, 31.1);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_25.setTransform(213.9, 31.1);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQghAAgYgYg");
        this.shape_26.setTransform(213.9, 31.1);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_27.setTransform(190, 31.1);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_28.setTransform(190, 31.1);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao2AAIRsAA");
        this.shape_29.setTransform(235.6, 41.3);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_30.setTransform(270.4, 41.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_31.setTransform(247.2, 41.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_32.setTransform(224.5, 41.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_33.setTransform(202.1, 41.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfQgLAAgLAFQgXAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAIAXgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_34.setTransform(235.8, 41.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAXgMQALgFALAAIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMIgXAFg");
        this.shape_35.setTransform(235.8, 41.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_40.setTransform(273.7, 76.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_41.setTransform(273.7, 76.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_42.setTransform(245.3, 76.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_43.setTransform(245.3, 76.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_44.setTransform(216.5, 76.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_45.setTransform(216.5, 76.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_46.setTransform(188.2, 76.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_47.setTransform(188.2, 76.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#000000").s().p("AgegWIA9AWIg9AXg");
        this.shape_48.setTransform(294.7, 76.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_49.setTransform(236.5, 76.6);

        this.text_14 = new cjs.Text("5", "bold 16px 'Myriad Pro'", "#0089CA");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(266.6, 77.4);

        this.text_15 = new cjs.Text("3", "bold 16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(238.5, 77.4);

        this.text_16 = new cjs.Text("2", "bold 16px 'Myriad Pro'");
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(209.5, 77.4);

        this.text_17 = new cjs.Text("1", "bold 16px 'Myriad Pro'");
        this.text_17.lineHeight = 19;
        this.text_17.setTransform(182, 77.4);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gHZQgdAAgcAOQg5AdAABHIAALQIACARQAEAWAIARQAdA5BHAAMBL/AAAIASgCQAVgEASgIQA5gdAAhGIAArQQAAgdgOgcQgdg5hHAAg");
        this.shape_50.setTransform(254.6, 58.6);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFF173").s().p("Egl/AHaQhHAAgdg5QgIgRgEgWIgCgRIAArQQAAhHA5gdQAcgOAdAAMBL/AAAQBHAAAdA5QAOAcAAAdIAALQQAABGg5AdQgSAIgVAEIgSACg");
        this.shape_51.setTransform(254.6, 58.6);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_61.setTransform(190, 52.6);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_62.setTransform(190, 52.6);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_63.setTransform(214, 52.6);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_64.setTransform(214, 52.6);

        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(130, 66).lineTo(330.5, 66).moveTo(330.5, 66).lineTo(330.5, 64).lineTo(337, 66).lineTo(330.5, 68).lineTo(330.5, 66);
        var numberLineLimit = 7
        for (var dot = 0; dot < 11; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(130 + (20 * dot), 61).lineTo(130 + (20 * dot), 71);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(129, 66).lineTo(130 + (20 * numberLineLimit), 66);

        for (var dot = 0; dot < 11; dot++) {
            var tempText = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 13px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot == 10) ? 9.8 : dot;
            temptext.setTransform(127 + (20 * colSpace), 88);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);



        this.addChild(this.shape_51, this.shape_50, this.shape_39, this.shape_38, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.shape_4, this.shape_3, this.text_4, this.text_3, this.text_2, this.shape_2, this.shape_1, this.shape, this.text_1, this.shape_52, this.text);

        this.addChild(this.shape_61, this.shape_62, this.shape_63, this.shape_64, this.numberLine);

        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 511.3, 107.1);



    //Part 3
    (lib.Symbol5 = function() {
        this.initialize();

        this.text = new cjs.Text("Skriv talen som saknas.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_9 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_9.lineHeight = 27;

        this.textbox_1 = new cjs.Shape();
        var xPos = 44,
            padding = 25,
            yPos = 42,
            rectWidth = 152,
            rectHeight = 22,
            boxWidth = 19,
            boxHeight = 22,
            numberOfBoxes = 8,
            numberOfRects = 2;

        for (var i = 0; i < numberOfRects; i++) {
            if (i == 1) {
                padding = padding + padding;
            }
            var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
            this.textbox_1.graphics.s("#7D7D7D").ss(0.4).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
            for (var j = 1; j < numberOfBoxes; j++) {
                this.textbox_1.graphics.s("#7D7D7D").ss(0.4).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
            }
        }
        this.textbox_1.setTransform(0, 0);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgDcQgHAAgLABQgVAEgSAIQg5AdAABHIAADXIACASQAEAVAIASQAdA4BHAAMBMJAAAIASgBQAVgEASgIQA5gdAAhHIAAjXQAAgcgOgdQgdg4hHAAg");
        this.shape_65.setTransform(257.1, 52.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFFFFF").s().p("EgmEADeQhHAAgdg5QgIgSgEgVIgCgSIAAjXQAAhHA5gcQASgJAVgDQALgDAHAAMBMJAAAQBHAAAdA6QAOAbAAAdIAADXQAABHg5AcQgSAJgVADIgSADg");
        this.shape_66.setTransform(257.1, 52.6);

        this.text_1 = new cjs.Text("0", "34px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(68.3, 33);

        this.text_2 = new cjs.Text("1", "34px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(88, 33);

        this.text_3 = new cjs.Text("5", "34px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(391, 33);

        this.text_4 = new cjs.Text("6", "34px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(410, 33);

        this.addChild(this.shape_66, this.shape_65, this.textbox_1, this.textbox_2, this.textbox_3,
            this.textbox_4, this.textbox_5, this.textbox_6, this.textbox_7, this.textbox_8,
            this.text_1, this.text_2, this.text_3, this.text_4, this.text_9, this.text);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 77.6);


    //Part 4
    (lib.Symbol6 = function() {
        this.initialize();

        this.instance = new lib.p71_1();
        this.instance.setTransform(15, 27, 0.38, 0.35);

        this.text = new cjs.Text("Hur många bollar är det på varje sida?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 182, 10);
        this.shape_65.setTransform(0, 25);

        this.textbox_1 = new cjs.Shape();
        this.textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(30, 40, 22, 23);
        this.textbox_1.setTransform(0, 0);

        this.textbox_2 = new cjs.Shape();
        this.textbox_2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(96, 40, 22, 23);
        this.textbox_2.setTransform(0, 0);

        this.textbox_3 = new cjs.Shape();
        this.textbox_3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(150, 40, 22, 23);
        this.textbox_3.setTransform(0, 0);

        this.textbox_4 = new cjs.Shape();
        this.textbox_4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(217, 40, 22, 23);
        this.textbox_4.setTransform(0, 0);

        this.textbox_5 = new cjs.Shape();
        this.textbox_5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(272, 40, 22, 23);
        this.textbox_5.setTransform(0, 0);

        this.textbox_6 = new cjs.Shape();
        this.textbox_6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(338, 40, 22, 23);
        this.textbox_6.setTransform(0, 0);

        this.textbox_13 = new cjs.Shape();
        this.textbox_13.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(395, 40, 22, 23);
        this.textbox_13.setTransform(0, 0);

        this.textbox_14 = new cjs.Shape();
        this.textbox_14.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(460, 40, 22, 23);
        this.textbox_14.setTransform(0, 0);

        this.textbox_7 = new cjs.Shape();
        this.textbox_7.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(30, 131, 22, 23);
        this.textbox_7.setTransform(0, 0);

        this.textbox_8 = new cjs.Shape();
        this.textbox_8.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(96, 131, 22, 23);
        this.textbox_8.setTransform(0, 0);

        this.textbox_9 = new cjs.Shape();
        this.textbox_9.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(150, 131, 22, 23);
        this.textbox_9.setTransform(0, 0);

        this.textbox_10 = new cjs.Shape();
        this.textbox_10.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(217, 131, 22, 23);
        this.textbox_10.setTransform(0, 0);

        this.textbox_11 = new cjs.Shape();
        this.textbox_11.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(272, 131, 22, 23);
        this.textbox_11.setTransform(0, 0);

        this.textbox_12 = new cjs.Shape();
        this.textbox_12.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(338, 131, 22, 23);
        this.textbox_12.setTransform(0, 0);

        this.textbox_15 = new cjs.Shape();
        this.textbox_15.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(395, 131, 22, 23);
        this.textbox_15.setTransform(0, 0);

        this.textbox_16 = new cjs.Shape();
        this.textbox_16.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(460, 131, 22, 23);
        this.textbox_16.setTransform(0, 0);

        this.text_2 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 24;
        this.text_2.setTransform(68, 35);

        this.text_3 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 24;
        this.text_3.setTransform(189, 35);

        this.text_4 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 24;
        this.text_4.setTransform(311, 35);

        this.text_5 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 24;
        this.text_5.setTransform(432, 35);

        this.text_6 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 24;
        this.text_6.setTransform(68, 128);

        this.text_7 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 24;
        this.text_7.setTransform(189, 128);

        this.text_8 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 24;
        this.text_8.setTransform(311, 128);

        this.text_9 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 24;
        this.text_9.setTransform(432, 128);

        this.addChild(this.shape_65, this.instance, this.text_1, this.textbox_1, this.textbox_2, this.textbox_3,
            this.textbox_4, this.textbox_5, this.textbox_6, this.textbox_7, this.textbox_8, this.textbox_9, this.textbox_10, this.textbox_11, this.textbox_12, this.text_2, this.text_3, this.text_4,
            this.text_5, this.text_6, this.text_7, this.text);

        this.addChild(this.textbox_13, this.textbox_14, this.textbox_15, this.textbox_16, this.text_8, this.text_9);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 200.6);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 2
        // this.ra1 = new lib.Symbol35();
        // this.ra1.setTransform(255.9,74.6,1,1,0,0,0,234.4,74.6);

        // Layer 1
        this.answer = new lib.Symbol4();
        this.answer.setTransform(241.4, 75, 1, 1, 0, 0, 0, 219.4, 74.7);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape.setTransform(149.2, 30.2);

        this.text_1 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(467.4, 12.2);

        this.text_3 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(390.3, 12.2);

        this.text_5 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(315.3, 12.2);

        this.text_7 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(472.1, 109.7);

        this.text_9 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(472.1, 79);

        this.text_11 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(472.1, 48.3);

        this.text_13 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(223.2, 109.7);

        this.text_15 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_15.lineHeight = 12;
        this.text_15.setTransform(223.2, 79);

        this.text_17 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_17.lineHeight = 12;
        this.text_17.setTransform(223.2, 48.3);

        this.text_19 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_19.lineHeight = 12;
        this.text_19.setTransform(415.8, 109.7);

        this.text_21 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_21.lineHeight = 12;
        this.text_21.setTransform(415.8, 79);

        this.text_23 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_23.lineHeight = 12;
        this.text_23.setTransform(415.8, 48.3);

        this.text_25 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_25.lineHeight = 12;
        this.text_25.setTransform(167, 109.7);

        this.text_27 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_27.lineHeight = 12;
        this.text_27.setTransform(167, 79);

        this.text_29 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_29.lineHeight = 12;
        this.text_29.setTransform(167, 48.3);

        this.text_31 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_31.lineHeight = 12;
        this.text_31.setTransform(358.2, 109.7);

        this.text_33 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_33.lineHeight = 12;
        this.text_33.setTransform(358.2, 79);

        this.text_35 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_35.lineHeight = 12;
        this.text_35.setTransform(358.2, 48.3);

        this.text_37 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_37.lineHeight = 12;
        this.text_37.setTransform(110.4, 109.7);

        this.text_39 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_39.lineHeight = 12;
        this.text_39.setTransform(110.4, 79);

        this.text_41 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_41.lineHeight = 12;
        this.text_41.setTransform(110.4, 48.3);

        this.text_43 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_43.lineHeight = 12;
        this.text_43.setTransform(302, 109.7);

        this.text_45 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_45.lineHeight = 12;
        this.text_45.setTransform(302, 79);

        this.text_47 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_47.lineHeight = 12;
        this.text_47.setTransform(302, 48.3);

        this.text_49 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_49.lineHeight = 12;
        this.text_49.setTransform(53.1, 109.7);

        this.text_51 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_51.lineHeight = 12;
        this.text_51.setTransform(53.1, 79);

        this.text_53 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_53.lineHeight = 12;
        this.text_53.setTransform(53.1, 48.3);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_1.setTransform(474.9, 30.2);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_2.setTransform(474.9, 30.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_3.setTransform(226.2, 30.2);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_4.setTransform(226.2, 30.2);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_5.setTransform(436.8, 30.2);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_6.setTransform(436.8, 30.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_7.setTransform(188.2, 30.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_8.setTransform(188.2, 30.2);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_9.setTransform(397.8, 30.2);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_10.setTransform(397.8, 30.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_11.setTransform(149.2, 30.2);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_12.setTransform(360.8, 30.2);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_13.setTransform(360.8, 30.2);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiJCLIEUAAIAAkVIkUAAg");
        this.shape_14.setTransform(112.2, 30.2);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEUAAIAAEVg");
        this.shape_15.setTransform(112.2, 30.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_16.setTransform(322.8, 30.2);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_17.setTransform(322.8, 30.2);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_18.setTransform(74.2, 30.2);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_19.setTransform(74.2, 30.2);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_20.setTransform(479, 124.8);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_21.setTransform(479, 124.8);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_22.setTransform(230.3, 124.8);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_23.setTransform(230.3, 124.8);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_24.setTransform(450.6, 124.8);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_25.setTransform(450.6, 124.8);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_26.setTransform(202, 124.8);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_27.setTransform(202, 124.8);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_28.setTransform(422.3, 124.8);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_29.setTransform(422.3, 124.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_30.setTransform(173.7, 124.8);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_31.setTransform(173.7, 124.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_32.setTransform(393.9, 124.8);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_33.setTransform(393.9, 124.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_34.setTransform(145.3, 124.8);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_35.setTransform(145.3, 124.8);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_36.setTransform(365.6, 124.8);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_37.setTransform(365.6, 124.8);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_38.setTransform(117, 124.8);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_39.setTransform(117, 124.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_40.setTransform(337.2, 124.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_41.setTransform(337.2, 124.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_42.setTransform(88.6, 124.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_43.setTransform(88.6, 124.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_44.setTransform(308.9, 124.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_45.setTransform(308.9, 124.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_46.setTransform(60.3, 124.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_47.setTransform(60.3, 124.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_48.setTransform(280.5, 124.8);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_49.setTransform(280.5, 124.8);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_50.setTransform(31.9, 124.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_51.setTransform(31.9, 124.8);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_52.setTransform(479, 94.1);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_53.setTransform(479, 94.1);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_54.setTransform(230.3, 94.1);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_55.setTransform(230.3, 94.1);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_56.setTransform(450.6, 94.1);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_57.setTransform(450.6, 94.1);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_58.setTransform(202, 94.1);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_59.setTransform(202, 94.1);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_60.setTransform(422.3, 94.1);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_61.setTransform(422.3, 94.1);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_62.setTransform(173.7, 94.1);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_63.setTransform(173.7, 94.1);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_64.setTransform(393.9, 94.1);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_65.setTransform(393.9, 94.1);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_66.setTransform(145.3, 94.1);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_67.setTransform(145.3, 94.1);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_68.setTransform(365.6, 94.1);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_69.setTransform(365.6, 94.1);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_70.setTransform(117, 94.1);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_71.setTransform(117, 94.1);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_72.setTransform(337.2, 94.1);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_73.setTransform(337.2, 94.1);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_74.setTransform(88.6, 94.1);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_75.setTransform(88.6, 94.1);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_76.setTransform(308.9, 94.1);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_77.setTransform(308.9, 94.1);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_78.setTransform(60.3, 94.1);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_79.setTransform(60.3, 94.1);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_80.setTransform(280.5, 94.1);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_81.setTransform(280.5, 94.1);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_82.setTransform(31.9, 94.1);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_83.setTransform(31.9, 94.1);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_84.setTransform(479, 63.5);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_85.setTransform(479, 63.5);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_86.setTransform(230.3, 63.5);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_87.setTransform(230.3, 63.5);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_88.setTransform(450.6, 63.5);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_89.setTransform(450.6, 63.5);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_90.setTransform(202, 63.5);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_91.setTransform(202, 63.5);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_92.setTransform(422.3, 63.5);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_93.setTransform(422.3, 63.5);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_94.setTransform(173.7, 63.5);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_95.setTransform(173.7, 63.5);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_96.setTransform(393.9, 63.5);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_97.setTransform(393.9, 63.5);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_98.setTransform(145.3, 63.5);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_99.setTransform(145.3, 63.5);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_100.setTransform(365.6, 63.5);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_101.setTransform(365.6, 63.5);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_102.setTransform(117, 63.5);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_103.setTransform(117, 63.5);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_104.setTransform(337.2, 63.5);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_105.setTransform(337.2, 63.5);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_106.setTransform(88.6, 63.5);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_107.setTransform(88.6, 63.5);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_108.setTransform(308.9, 63.5);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_109.setTransform(308.9, 63.5);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_110.setTransform(60.3, 63.5);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_111.setTransform(60.3, 63.5);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_112.setTransform(280.5, 63.5);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_113.setTransform(280.5, 63.5);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_114.setTransform(31.9, 63.5);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_115.setTransform(31.9, 63.5);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_116.setTransform(284.8, 30.2);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_117.setTransform(284.8, 30.2);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_118.setTransform(36.2, 30.2);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_119.setTransform(36.2, 30.2);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgLNQgHAAgLACQgVAEgSAIQg5AdAABHIAAS3IACARQAEAWAIASQAdA5BHAAMBMJAAAIASgDQAVgDASgJQA5gcAAhHIAAy3QAAgdgOgcQgdg5hHAAg");
        this.shape_120.setTransform(255.1, 77);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f("#FFFFFF").s().p("EgmEALOQhHgBgdg4QgIgSgEgWIgCgRIAAy3QAAhHA5gdQASgIAVgDQALgCAHgBMBMJAAAQBHABAdA5QAOAbAAAdIAAS3QAABHg5AcQgSAJgVAEIgSACg");
        this.shape_121.setTransform(255.1, 77);

        this.addChild(this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.text_53, this.text_51, this.text_49, this.text_47, this.text_45, this.text_43, this.text_41, this.text_39, this.text_37, this.text_35, this.text_33, this.text_31, this.text_29, this.text_27, this.text_25, this.text_23, this.text_21, this.text_19, this.text_17, this.text_15, this.text_13, this.text_11, this.text_9, this.text_7, this.text_5, this.text_3, this.text_1, this.shape, this.answer);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 512.3, 149.8);


    // stage content:
    (lib.p71 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v4 = new lib.Symbol6();
        this.v4.setTransform(311.8, 462.3, 1, 1, 0, 0, 0, 256.3, 38.8);

        this.v3 = new lib.Symbol5();
        this.v3.setTransform(311.8, 362.3, 1, 1, 0, 0, 0, 256.3, 38.8);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(312.6, 238.2, 1, 1, 0, 0, 0, 255.1, 74.8);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(312.4, 99.9, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
