(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p31_1.png",
            id: "p31_1"
        }, {
            src: "images/p31_2.png",
            id: "p31_2"
        }]
    };

    (lib.p31_1 = function() {
        this.initialize(img.p31_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p31_2 = function() {
        this.initialize(img.p31_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("31", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00B0CA").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol6 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(-2, 49, 521, 525, 13);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p31_1();
        this.instance.setTransform(429, 6, 0.38, 0.38);

        this.instance_2 = new lib.p31_2();
        this.instance_2.setTransform(0, 56, 0.53, 0.525);

        this.text = new cjs.Text("Magiska hatten", "18px 'MyriadPro-Semibold'", "#00B0CA");
        this.text.lineHeight = 20;
        this.text.setTransform(-2, 3);

        this.text_1 = new cjs.Text("Spel för 2 eller fler.", "16px 'Myriad Pro'", "#00B0CA");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(-3, 25);

        this.text_7 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(339, 16);

        this.text_2 = new cjs.Text("Slå tärningen.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(101, 151);

        this.text_3 = new cjs.Text("När du landar på en ruta med en", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(101, 170);

        this.text_4 = new cjs.Text("gå vidare förrän du slagit något av de två talen", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(101, 187);

        this.text_5 = new cjs.Text("som står i rutan.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(101, 204);

        this.text_6 = new cjs.Text("Först i mål vinner.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(101, 222);

        this.text_8 = new cjs.Text("får du inte", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(347, 170);

        this.text_9 = new cjs.Text("1.", "16px 'MyriadPro-Semibold'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(81, 152.3);

        this.text_10 = new cjs.Text("2.", "16px 'MyriadPro-Semibold'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(81, 170.9);

        this.text_11 = new cjs.Text("3.", "16px 'MyriadPro-Semibold'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(81, 223.9);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#00B0CA').drawRoundRect(330, 5, 187, 38, 2);
        this.roundRect2.setTransform(0, 0);

        this.label1 = new cjs.Text("Mål", "18px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(163, 84);
        this.label2 = new cjs.Text("Start", "18px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(317, 84);
        this.label3 = new cjs.Text("3        5", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(9, 96);
        this.label4 = new cjs.Text("2        4", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(10, 223);
        this.label5 = new cjs.Text("4        2", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(6, 417);
        this.label6 = new cjs.Text("3        6", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(10, 482);
        this.label7 = new cjs.Text("2        5", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(139, 546);
        this.label8 = new cjs.Text("2        6", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(267, 543);
        this.label9 = new cjs.Text("1        5", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(393, 543);
        this.label10 = new cjs.Text("5        6", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(460, 484);
        this.label11 = new cjs.Text("3        4", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(463, 294);
        this.label12 = new cjs.Text("1        2", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(454, 164);

        var ToBeAdded = [];
        var arrxPos = ['22', '23', '19', '23', '152', '280', '406', '472','476','465'];
        var arryPos = ['99', '227.5', '421', '486','550','548', '548','488.5','299','167'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var yPos = parseInt(arryPos[i]);
            var temptext=null;
            temptext = new cjs.Text("eller", "12px 'Myriad Pro'");
            temptext.lineHeight = -7;
            temptext.setTransform(xPos, yPos);
            ToBeAdded.push(temptext);
        }

        this.addChild(this.roundRect2, this.roundRect1, this.instance_2);
        this.addChild(this.text_1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6);
        this.addChild(this.text, this.text_2, this.instance, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8);
        this.addChild(this.text_9, this.text_10, this.text_11, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 497, 520);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(304, 280, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
