(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("kunna subtrahera ental från tvåsiffriga tal", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(380, 658);

        this.text_1 = new cjs.Text("135", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(552, 657);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_2 = new cjs.Text("Subtrahera ental", "24px 'MyriadPro-Semibold'", "#00A3C4");
        this.text_2.setTransform(95.5, 42);

        this.text_3 = new cjs.Text("47", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(46, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.shape_2, this.text_3, this.text_2, this.shape_1, this.text_1, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();
        // Block-1

        var ToBeAdded = [];

        var xpos = 32.5;
        var spiral = new lib.spiral(13);
        spiral.setTransform(xpos, 16.4)

        var xpos1 = 287;
        var spiral1 = new lib.spiral(13);
        spiral1.setTransform(xpos1, 16.4);

        this.roundRect2 = new cjs.Shape(); // white block
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(20, 15, 215, 131, 5);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape(); // white block
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(275, 15, 215, 131, 5);
        this.roundRect3.setTransform(0, 0);

        // Main yellow block
        this.roundRect1 = new cjs.Shape(); // white block
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 0, 514, 154, 10);
        this.roundRect1.setTransform(0, 0);

        var xpos = 0;
        for (var col = 0; col < 12; col++) {
            var Line_2 = new cjs.Shape();
            if (col > 7) {
                xpos = 221;
            } else if (col > 2) {
                xpos = 213;
            } else {
                xpos = 39;
            }
            Line_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(45, 31).lineTo(45, 119).lineTo(54, 119).lineTo(54, 31).lineTo(45, 31)
                .moveTo(45, 39.6).lineTo(54, 39.6).moveTo(45, 48.1).lineTo(54, 48.1).moveTo(45, 56.6).lineTo(54, 56.6).moveTo(45, 65.1).lineTo(54, 65.1)
                .moveTo(45, 73.6).lineTo(54, 73.6).moveTo(45, 82.1).lineTo(54, 82.1).moveTo(45, 91.6).lineTo(54, 91.6).moveTo(45, 101.1).lineTo(54, 101.1)
                .moveTo(45, 110.7).lineTo(54, 110.7);
            Line_2.setTransform(xpos + (col * 14.4), 0);
            ToBeAdded.push(Line_2)
        }

        var xpos = 0;
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 4; col++) {
                var Line_3 = new cjs.Shape();

                if (col > 1) {
                    xpos = 300;
                } else {
                    xpos = 17;
                }

                if (col < 1 && row < 1) {
                    continue
                }
                if (col == 1 && row < 2) {
                    continue
                }
                if (col == 2 && row < 2) {
                    continue
                }
                if (col == 3 && row < 3) {
                    continue
                }

                Line_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(119, 62.1).lineTo(119, 71)
                    .lineTo(127, 71).lineTo(127, 62.1).lineTo(119, 62.1)
                Line_3.setTransform(xpos + (col * 13.3), 0 + (row * 12));
                ToBeAdded.push(Line_3)
            }
        }


        var tmptxt = ""
        var xpos = 63
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 2; col++) {
                if (col == 0 && row == 1) {
                    tmptxt = "37 – 5 = 32";
                    xpos = 89;
                    ypos = 130
                } else if (col == 1 && row == 1) {
                    tmptxt = "95 – 5 = 90";
                    xpos = 88;
                    ypos = 130
                } else {
                    xpos = 76;
                    ypos = 170
                }

                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText);
            }
        }

        this.crossLine = new cjs.Shape(); 
        this.crossLine.graphics.f("").s("#DB3A58").setStrokeStyle(1).moveTo(1, 15).lineTo(13, 2)
        this.crossLine.setTransform(133, 70);

         this.crossLine2 = this.crossLine.clone(true);
         this.crossLine2.setTransform(133, 82);

         this.crossLine3 = this.crossLine.clone(true);
         this.crossLine3.setTransform(133, 94);

         this.crossLine4 = this.crossLine.clone(true);
         this.crossLine4.setTransform(147, 82);

         this.crossLine5 = this.crossLine.clone(true);
         this.crossLine5.setTransform(147, 94);


         this.crossLine6 = this.crossLine.clone(true);
         this.crossLine6.setTransform(443, 82);

         this.crossLine7 = this.crossLine.clone(true);
         this.crossLine7.setTransform(443, 94);

         this.crossLine8 = this.crossLine.clone(true);
         this.crossLine8.setTransform(443, 106);

         this.crossLine9 = this.crossLine.clone(true);
         this.crossLine9.setTransform(457, 94);

         this.crossLine10 = this.crossLine.clone(true);
         this.crossLine10.setTransform(457, 105);       

         this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.hrLine, this.hrLine1, this.vrLine1, this.Line_3);
         this.addChild(this.hrLine2, this.hrLine3, this.vrLine2)         
        this.addChild(spiral, spiral1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.crossLine, this.crossLine2, this.crossLine3, this.crossLine4, this.crossLine5,
            this.crossLine6, this.crossLine7, this.crossLine8, this.crossLine9, this.crossLine10);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 530.3, 123.3);

    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 514, 381, 10);
        this.roundRect1.setTransform(0, 11);

        // Draw horizantial  lines

        var ToBeAdded = [];
        var moveToxposition = 137;
        for (var row = 0; row < 2; row++) {
            if (row == 1) {
                moveToxposition = 141;

            } else {
                moveToxposition = 137;
            }

            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(5, moveToxposition + (row * 122)).lineTo(507, moveToxposition + (row * 122));
            ToBeAdded.push(hrLine)
        };

        // Draw vertical lines

        var moveToypos = 13;
        var lineToypos = 123;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 1; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 142;
                    lineToypos = 255;
                } else if (row == 2) {
                    moveToypos = 263;
                    lineToypos = 383;
                } else {
                    moveToypos = 17;
                    lineToypos = 132;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(171 + (col * 170), moveToypos).lineTo(171 + (col * 170), lineToypos)
                    .moveTo(343 + (col * 170), moveToypos).lineTo(343 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }

        //Row-1


        // Draw green boxes

        var xpos = 0;
        for (var col = 0; col < 11; col++) {
            var Line_2 = new cjs.Shape();
            if (col > 5) {
                xpos = 245;
            } else if (col > 1) {
                xpos = 147;
            } else {
                xpos = 4;
            }
            Line_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(33, 20).lineTo(33, 108).lineTo(42, 108).lineTo(42, 20).lineTo(33, 20)
                .moveTo(33, 28.6).lineTo(42, 28.6).moveTo(33, 37.1).lineTo(42, 37.1).moveTo(33, 45.6).lineTo(42, 45.6).moveTo(33, 54.1).lineTo(42, 54.1)
                .moveTo(33, 62.6).lineTo(42, 62.6).moveTo(33, 71.1).lineTo(42, 71.1).moveTo(33, 80.6).lineTo(42, 80.6).moveTo(33, 90.1).lineTo(42, 90.1)
                .moveTo(33, 99.7).lineTo(42, 99.7);
            Line_2.setTransform(xpos + (col * 14.4), 0);
            ToBeAdded.push(Line_2)
        }

        // Draw yellow boxes

        var xpos = 0;
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 6; col++) {
                var Line_3 = new cjs.Shape();

                if (col > 3) {
                    xpos = 276;
                } else if (col > 1) {
                    xpos = 134;
                } else {
                    xpos = -40;
                }

                if (col < 2 && row < 2) {
                    continue
                }
                if (col < 4 && col > 1 && row < 3) {
                    continue
                }
                if (col == 4 && row < 2) {
                    continue
                }
                if (col == 5 && row < 3) {
                    continue
                }

                Line_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(112, 51.1).lineTo(112, 60)
                    .lineTo(120, 60).lineTo(120, 51.1).lineTo(112, 51.1)
                Line_3.setTransform(xpos + (col * 13.3), 0 + (row * 12));
                ToBeAdded.push(Line_3)
            }
        }


        var tmptxt = ""
        var xpos = 0
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 3; col++) {
                if (col == 0 && row == 1) {
                    tmptxt = "26  –  2  =";
                    xpos = 33.5;
                    ypos = 117
                } else if (col == 1 && row == 1) {
                    tmptxt = "44  –  2  =";
                    xpos = -46;
                    ypos = 117
                } else if (col == 2 && row == 1) {
                    tmptxt = "55  –  2  =";
                    xpos = -132;
                    ypos = 117;

                }

                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText);
            }
        }

        //Row-2

        // Draw green boxes

        var xpos = 0;
        for (var col = 0; col < 15; col++) {
            var Line_4 = new cjs.Shape();
            if (col > 9) {
                xpos = 204;
            } else if (col > 5) {
                xpos = 105;
            } else if (col > 4) {
                xpos = 25;
            } else {
                xpos = 16;
            }
            Line_4.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(16, 145).lineTo(16, 233).lineTo(25, 233).lineTo(25, 145).lineTo(16, 145)
                .moveTo(16, 153.6).lineTo(25, 153.6).moveTo(16, 162.1).lineTo(25, 162.1).moveTo(16, 170.6).lineTo(25, 170.6).moveTo(16, 179.1).lineTo(25, 179.1)
                .moveTo(16, 187.6).lineTo(25, 187.6).moveTo(16, 196.1).lineTo(25, 196.1).moveTo(16, 205.6).lineTo(25, 205.6).moveTo(16, 215.1).lineTo(25, 215.1)
                .moveTo(16, 224.7).lineTo(25, 224.7);
            Line_4.setTransform(xpos + (col * 14.4), 0);
            ToBeAdded.push(Line_4)
        }

        // Draw yellow boxes

        var xpos = 0;
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 6; col++) {
                var Line_5 = new cjs.Shape();

                if (col > 3) {
                    xpos = 319;
                } else if (col > 1) {
                    xpos = 175;
                } else {
                    xpos = 63;
                }

                if (col < 1 && row < 3) {
                    continue
                }
                if (col == 1 && row < 4) {
                    continue
                }

                if ((col == 2 || col == 3) && row < 3) {
                    continue
                }
                if ((col == 4 || col == 5) && row < 2) {
                    continue
                }

                Line_5.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(69, 177.1).lineTo(69, 186)
                    .lineTo(77, 186).lineTo(77, 177.1).lineTo(69, 177.1)
                Line_5.setTransform(xpos + (col * 13.3), 0 + (row * 12));
                ToBeAdded.push(Line_5)
            }
        }


        var tmptxt1 = ""
        var xpos = 0
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 3; col++) {
                if (col == 0 && row == 1) {
                    tmptxt1 = "63  –  3  =";
                    xpos = 33.5;
                    ypos = 243
                } else if (col == 1 && row == 1) {
                    tmptxt1 = "44  –  4  =";
                    xpos = -46;
                    ypos = 243
                } else if (col == 2 && row == 1) {
                    tmptxt1 = "56  –  3  =";
                    xpos = -134;
                    ypos = 243;

                }

                var tempText1 = new cjs.Text(tmptxt1, "16px 'Myriad Pro'");
                tempText1.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText1);
            }
        }

        //Row-3

        // Draw green boxes

        var xpos = 0;
        for (var col = 0; col < 21; col++) {
            var Line_6 = new cjs.Shape();

            if (col > 17) {
                xpos = 163;
            } else if (col > 12) {
                xpos = 155;
            } else if (col > 10) {
                xpos = 97;
            } else if (col > 5) {
                xpos = 89;
            } else if (col > 4) {
                xpos = 22;
            } else {
                xpos = 12;
            }
            Line_6.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(16, 271).lineTo(16, 359).lineTo(25, 359).lineTo(25, 271).lineTo(16, 271)
                .moveTo(16, 279.6).lineTo(25, 279.6).moveTo(16, 288.1).lineTo(25, 288.1).moveTo(16, 296.6).lineTo(25, 296.6).moveTo(16, 305.1).lineTo(25, 305.1)
                .moveTo(16, 313.6).lineTo(25, 313.6).moveTo(16, 322.1).lineTo(25, 322.1).moveTo(16, 331.6).lineTo(25, 331.6).moveTo(16, 341.1).lineTo(25, 341.1)
                .moveTo(16, 350.7).lineTo(25, 350.7);
            Line_6.setTransform(xpos + (col * 14.4), 0);
            ToBeAdded.push(Line_6)
        }

        // Draw yellow boxes

        var xpos = 0;
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 6; col++) {
                var Line_7 = new cjs.Shape();

                if (col > 3) {
                    xpos = 292;
                } else if (col > 1) {
                    xpos = 137;
                } else {
                    xpos = -12;
                }

                if (col < 1 && row < 2) {
                    continue
                }

                if (col == 1 && row < 3) {
                    continue
                }
                if ((col == 2 || col == 3) && row < 2) {
                    continue
                }
                if (col == 4 && row < 0) {
                    continue
                }
                if (col == 5 && row < 1) {
                    continue
                }
                Line_7.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(141, 303.1).lineTo(141, 312)
                    .lineTo(149, 312).lineTo(149, 303.1).lineTo(141, 303.1)
                Line_7.setTransform(xpos + (col * 13.3), 0 + (row * 12));
                ToBeAdded.push(Line_7)
            }
        }


        var tmptxt2 = ""
        var xpos = 0
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 3; col++) {
                if (col == 0 && row == 1) {
                    tmptxt2 = "65  –  3  =";
                    xpos = 33.5;
                    ypos = 369
                } else if (col == 1 && row == 1) {
                    tmptxt2 = "76  –  5  =";
                    xpos = -46;
                    ypos = 369
                } else if (col == 2 && row == 1) {
                    tmptxt2 = "89  –  9  =";
                    xpos = -133;
                    ypos = 369;

                }

                var tempText2 = new cjs.Text(tmptxt2, "16px 'Myriad Pro'");
                tempText2.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText2);
            }
        }

        // draw small horizantal lines

        var moveToxpos = 97
        var lineToxpos = 121
        var ypos = 130
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 3; col++) {
                var hrLine1 = new cjs.Shape();
                if (col > 1) {
                    moveToxpos = 95;
                    lineToxpos = 124;
                } else if (col > 0) {
                    moveToxpos = 101;
                    lineToxpos = 128;
                } else {
                    moveToxpos = 97;
                    lineToxpos = 121;
                }
                hrLine1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(moveToxpos + (col * 176), ypos + (row * 126))
                    .lineTo(lineToxpos + (col * 176), ypos + (row * 126));
                ToBeAdded.push(hrLine1)
            }
        }

        this.addChild(this.text, this.text_1, this.roundRect1);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 530.3, 390);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 108, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 296, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
