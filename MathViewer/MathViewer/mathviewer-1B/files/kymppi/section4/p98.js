(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p98_1.png",
            id: "p98_1"
        }]
    };

    (lib.p98_1 = function() {
        this.initialize(img.p98_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("98", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(37, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 0);

        this.text_1 = new cjs.Text(" Rita sträckan. Skriv längden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 285, 10);
        this.roundRect1.setTransform(10, 10);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_1.setTransform(15, 30);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_2.setTransform(15, 85);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_3.setTransform(15, 140);
        1
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_4.setTransform(15, 197);

        var TextArr = [];
        var TArr = ['Exempel 8 cm', '4 cm längre än exemplet', '2 cm kortare än exemplet', '6 cm längre än exemplet', '5 cm kortare än exemplet',
            'cm', 'cm', 'cm', 'cm'
        ];
        var TextX = [213, 31, 31, 31, 31, 480, 480, 480, 480];
        var TextY = [40, 96, 150, 205, 259, 115, 169, 225, 275];

        for (var i = 0; i < TextX.length; i++) {

            var text_1 = new cjs.Text(TArr[i], "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[i], TextY[i]);
            TextArr.push(text_1);
        }

        var hrlineArr = [];
        var hrlineX = [145, 31, 31, 31, 31, 449, 449, 449, 449];
        var hrlineY = [28, 90, 145, 201, 252, 96, 150, 206, 256];

        for (var i = 0; i < hrlineX.length; i++) {
            if (i == 0) {
                var hrLine_1 = new cjs.Shape();
                hrLine_1.graphics.beginStroke("#008BD2").setStrokeStyle(1).moveTo(0, 20).lineTo(225, 20);
                hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
                hrlineArr.push(hrLine_1);
            } else {
                var hrLine_1 = new cjs.Shape();
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(28, 20);
                hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
                hrlineArr.push(hrLine_1);
            }

        }

        var vrlineArr = [];
        var vrlineX = [124, 350, 11, 11, 11, 11];
        var vrlineY = [42, 42, 104, 159, 215, 266];

        for (var i = 0; i < vrlineX.length; i++) {
            if (i == 0 || i == 1) {
                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#008BD2").setStrokeStyle(1).moveTo(20, 0).lineTo(20, 11);
                vrLine_1.setTransform(vrlineX[i], vrlineY[i]);
                vrlineArr.push(vrLine_1);
            } else {
                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(20, 0).lineTo(20, 11);
                vrLine_1.setTransform(vrlineX[i], vrlineY[i]);
                vrlineArr.push(vrLine_1);
            }

        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }
        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < vrlineArr.length; i++) {
            this.addChild(vrlineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 290);

    (lib.drawScale = function(scaleLength, paddingLeft, paddingRight, cropRight) {
        this.initialize();
        var scaleHeight = 126.8;

        var corners = {
            TL: 10,
            TR: 10,
            BR: 10,
            BL: 10
        };

        if (cropRight) {
            corners.TR = 0;
            corners.BR = 0;
            paddingRight = 10;
        }

        this.metricscale = new cjs.Shape();
        this.metricscale.graphics.f("#E2F3FD").s("#959C9D").ss(1).drawRoundRectComplex(0, 0, paddingLeft + (scaleLength * 100) + paddingRight, scaleHeight, corners.TL, corners.TR, corners.BR, corners.BL);
        if (cropRight) {
            this.metricscale.graphics.s('#ffffff').drawRect(paddingLeft + (scaleLength * 100) + paddingRight - 1, 1, 1 * 2, scaleHeight - 1 * 2);
        }
        this.metricscale.setTransform(0, 0);
        this.addChild(this.metricscale);

        for (var i = 0; i <= scaleLength * 10; i++) {
            var height = 31.7;
            if (i % 5 == 0) {
                height = 63.4;
            }

            var round = new cjs.Shape();
            round.graphics.f("#000000").s('#7d7d7d').drawRect(0, 0, 1, height);
            round.setTransform(25 + i * 10, 0);
            this.addChild(round, text);
            if (i % 10 == 0) {
                var ispace = 16;
                var number = i / 10;
                if (number > 9) {
                    ispace = 8;
                }
                var text = new cjs.Text(number + "", "36px 'Myriad Pro'", "#000000");
                text.setTransform(ispace + i * 10, height + (height * 50 / 100));
                this.addChild(text);
            }
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 320.3, 143.6);

    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Hur lång är pennan?", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(10, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 510, 219, 10);
        this.roundRect1.setTransform(10, 10);

        this.instance = new lib.p98_1();
        this.instance.setTransform(93, 30, 0.47, 0.47);

        var scaleLength = 12.5;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = false;
        var bigScale1 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale1.setTransform(40, 61, 0.284, 0.36);

        var scaleLength = 12.5;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = false;
        var bigScale2 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale2.setTransform(40, 162, 0.284, 0.36);

        var hrlineArr = [];
        var hrlineX = [435, 435];
        var hrlineY = [62, 162];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 20).lineTo(27, 20);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        var TextArr = [];
        var TextX = [465, 465];
        var TextY = [81, 181];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("cm", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, bigScale1, bigScale2);

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 220);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287, 117, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(287, 463, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
