(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p81_1.png",
            id: "p81_1"
        }, {
            src: "images/p81_2.png",
            id: "p81_2"
        }, {
            src: "images/p81_3.png",
            id: "p81_3"
        }]
    };

    // symbols:
    (lib.p81_1 = function() {
        this.initialize(img.p81_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p81_2 = function() {
        this.initialize(img.p81_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p81_3 = function() {
        this.initialize(img.p81_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("81", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(558, 648);
        
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Hur många kottar finns kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p81_1();
        this.instance.setTransform(7, 25, 0.45, 0.47);

        this.instance1 = new lib.p81_1();
        this.instance1.setTransform(7, 183, 0.45, 0.47);

        this.instance2 = new lib.p81_2();
        this.instance2.setTransform(275, 23, 0.45, 0.47);

        this.instance3 = new lib.p81_2();
        this.instance3.setTransform(275, 181, 0.45, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 260, 152, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(265, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 183);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(265, 183);

        var arrTxtbox = ['147', '305'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding = 40,
            yPos,
            rectWidth = 110,
            rectHeight = 23,
            boxWidth = 22,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 76;
                if (i == 1) {
                    padding = 118;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_2 = new cjs.Text("Jag tar 4 kottar.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(33, 44);

        this.text_3 = new cjs.Text("Jag tar 5 kottar.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(293, 45);

        this.text_4 = new cjs.Text("Jag tar 3 kottar.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(33, 203);

        this.text_5 = new cjs.Text("Jag tar 6 kottar.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(293, 204);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.text, this.text_1);
        this.addChild(this.instance, this.instance1, this.instance2, this.instance3, this.textbox_group1)
        this.addChild(this.text_2, this.text_3, this.text_4, this.text_5);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p81_3();
        this.instance.setTransform(17, 57, 0.32, 0.32);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 522, 188, 10);
        this.roundRect1.setTransform(0, 20);

        this.text = new cjs.Text("Mössorna finns i 3 färger. Halsdukarna finns i 2 färger.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_2 = new cjs.Text("Måla alla olika sätt som Leo kan klä sig.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(19, 20);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305.3, 438.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(300.3, 93.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
