var defaultLanguage = 'SV';
var languageTranslations = {
    'Previous': {
        'EN': 'Previous',
        'SV': 'Tidigare'
    },
    'Next': {
        'EN': 'Next',
        'SV': 'N\u00E4sta'
    },
    'Return': {
        'EN': 'Return',
        'SV': 'Tillbaka'
    },
    'PreviousExercise': {
        'EN': 'Previous Exercise',
        'SV': 'Föregående'
    },
    'NextExercise': {
        'EN': 'Next Exercise',
        'SV': 'N\u00E4sta'
    },
    'ResetExercise': {
        'EN': 'Reset Exercise',
        'SV': 'Från början'
    },
    'CheckAnswer': {
        'EN': 'Check Answer',
        'SV': 'Kontrollera'
    },
    'NewExercise': {
        'EN': 'New Exercise',
        'SV': 'Fortsätt'
    }
};

$(document).on('click', '.submenu >  li', function(event) {
    $('#' + $(this).data('modalid')).openModal();
    $('#' + $(this).data('modalid')).find('ul.tabs').tabs('select_tab', $(this).data('tabid'));
});
$(document).on('keyup', '.leanModal', function(e) {
    if (e.keyCode === 27) { // ESC key

    }
});
$(document).on('click', '.leanModal', function(e) {
    if (e.keyCode === 27) { // ESC key

    }
});
var mathviewer = {

    initFrames: function() {
        if (window.parent != window) {
            function findParent(target) {
                if (target.window.parent != target.window) {
                    findParent(target.window.parent);
                } else if ($(target.document).find('.d2l-body')[0]) {
                    if ($(target.document).find('#header')[0]) {
                        $(target.document).find('html, body').css('background-color', '#E3E3E3');
                    } else {
                        if ($(target.document).find('.d2l-popup-page')[0]) {
                            $(window.parent.document).find('iframe.dif[name="frmContentObject"]').attr('height', '100%');
                            $(window.parent.document).find('iframe.dif[name="frmContentObject"]').css('height', '100%');
                        } else {
                            $('html').addClass('d2l_fixed');
                            $('html').css('height', '580px');
                        }
                    }
                }
            }
            findParent(window.parent);
        }
    },
    initMaterializeComponents: function() {
        $('ul.tabs').tabs();
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: false, // Does not change width of dropdown to that of the activator
            hover: false, // Activate on hover
            gutter: 20, // Spacing from edge
            belowOrigin: false // Displays dropdown below the button
        });

    },
    getTranslation: function(val, language) {
        language = language || defaultLanguage;
        if (languageTranslations[val]) {
            if (languageTranslations[val][language])
                return languageTranslations[val][language];
        }
        return val;
    },
    androidVersion: function() {
        var ua = navigator.userAgent.toLowerCase();
        return ua.indexOf('android') > -1;
    },
    iOSversion: function() {
        if (/iP(hone|od|ad)/.test(navigator.platform)) {
            var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
            return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
        }
    },
    compatiblitySetup: function() {
        if (window.parent == this) {
            $('html').addClass('orphan');
        }

        var iosVersion = this.iOSversion();
        var verAndroid = this.androidVersion();
        if (iosVersion != undefined && iosVersion[0] == 7) {
            $('html').addClass('ipad ios7');
        } else if (iosVersion == undefined) {
            $('html').addClass('no-touch');
        }
    },
    isTouchDevice: function() {
        var _b = false;
        if (typeof window.Modernizr === 'undefined')
            _b === ('ontouchstart' in window || (window.DocumentTouch && document instanceof DocumentTouch) || window.navigator.msMaxTouchPoints);
        else
            _b = window.navigator.msMaxTouchPoints || Modernizr.touch;
        return _b;
    },
    addToolbarItem: function(title, cssClass, tabid, modalid, clickEventListerner, isHoverRequired, isModal) {
        
        var a = document.createElement('a');
        $(a).attr('title', title);
        $(a).addClass('toolbarButton btn_home ' + cssClass);
        if (isModal) {
            $(a).attr('data-tabid', tabid);
            $(a).attr('data-modalid', modalid);
            $(a).attr('data-target', modalid);
            $(a).leanModal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
                ready: function() {
                    // alert('Modal opened')
                }, // Callback for Modal open
                complete: function() {
                        var tabCnt =$('#'+modalid+' .contentTabs').find('.tabContent').length;
                        for (var tab = 1; tab <= tabCnt; tab++) {
                            var tabID = "#tab" + tab;
                            for (var i = 0; i < $('#'+modalid+' '+ tabID).find('iframe').length; i++) {
                                $('#'+modalid+' '+ tabID).find('iframe')[i].src = "preloader.html";
                            }
                        }
                        for (var i = 0; i < $('#tabs1').find('a').length; i++) {
                            var tab =$('#'+modalid+' #tabs1').find('a')[i];
                            $(tab).removeClass();
                        }
                    } // Callback for Modal close
            });
        }
        if (clickEventListerner != false) {
            $(a).on('click', clickEventListerner);
        }
        if (isHoverRequired) {
            $(a).on({
                mouseenter: function() {
                    if (!$(this).hasClass("disabled")) {
                        var image = $(this).find('img');
                        image.attr('src', image.data('hoverOn'));
                    }
                },
                mouseleave: function() {
                    if (!$(this).hasClass("disabled")) {
                        var image = $(this).find('img');
                        image.attr('src', image.data('hoverOff'));
                    }
                }
            });
        }
        new NoClickDelay(a);
        return a;
    },
    postRenderFunctions: function() {
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 140, // Spacing from edge
            belowOrigin: false // Displays dropdown below the button
        });
    },
    setIconVisibility: function(visibility, icon) {

        if (window.parent) {
            window.parent.setIconVisibility(visibility, icon);
        }
    },
    setImageAudios: function(chapter, imageId) {

        // ;
        //alert(imageId);
        var image = window.parent.document.getElementById("splashImg");
        //c3-img1.png
        image.src = "files/kymppi/" + imageId;

        window.parent.setMedia(chapter);
    },



    setExerciseTabs: function(tabCount, chapter, exerciseNames,modalType) {
        if(!modalType)
        {
            modalType='modalViLar';

        }
        var modal = window.parent.document.getElementById(modalType); // contains attribute data-my-custom-value
        var currentChapter = null;

        if (modal.dataset !== undefined) { // standard approach
            currentChapter = modal.dataset.currentChapter;
            if (currentChapter) {
                currentChapter = parseInt(currentChapter);
            }
            modal.dataset.currentChapter = chapter;
            modal.dataset.slideCount = 0;
        } else {
            currentChapter = modal.getAttribute('data-current-chapter'); // IE approach
            if (currentChapter) {
                currentChapter = parseInt(currentChapter);
            }
            modal.setAttribute('data-current-chapter', chapter);
            modal.setAttribute('data-slide-count', 0);
        }
        var tabs = $(window.parent.document).find('#'+modalType+' .tabs li');

        $(window.parent.document).find('.exercise.resetFrame').hide();
        $(window.parent.document).find('.exercise.prevFrame').addClass('disabled');
        var tabContents = $(window.parent.document).find('#'+modalType+' .tabContent');
        tabs.removeClass('hidden').addClass('hidden');
        tabContents.removeClass('hidden').addClass('hidden');
        var numberOfTabs = tabCount;
        for (var i = 0; i < numberOfTabs; i++) {
            if (numberOfTabs == 1) { // do not set tab-name when only one tab
                $(tabs[i]).find('a').html("");
            } else {
                $(tabs[i]).find('a').html(exerciseNames[i]);
            }
            $(tabs[i]).removeClass('hidden').removeClass('shown').addClass('shown');
            $(tabContents[i]).removeClass('hidden');
        }

        var allTabs = $(window.parent.document).find('#'+modalType+' .tabs');
        var shownTabs = allTabs.find('.shown');
        shownTabs.first().removeClass('noLeftBorder').removeClass('noRightBorder');
        shownTabs.last().removeClass('noLeftBorder').removeClass('noRightBorder');
        if (numberOfTabs == 1) {
            shownTabs.first().addClass('noLeftBorder noRightBorder');
        } else {
            shownTabs.first().addClass('noLeftBorder');
            shownTabs.last().addClass('noRightBorder');
        }

    },

    setExerciseSlideCount: function(Cnt) {
        var modalViLar = window.parent.document.getElementById('modalViLar'); // contains attribute data-my-custom-value

        if (modalViLar.dataset !== undefined) { // standard approach
            modalViLar.dataset.slideCount = Cnt;
        } else {
            modalViLar.setAttribute('data-slide-count', Cnt); // IE approach
        }
    }
}

function popupFunctions() {
    if (!$(this).hasClass("disabled")) {
        var modalid='#' + $(this).data('modalid');
        // $('#' + $(this).data('modalid')).openModal();
        $(modalid).find('ul.tabs').tabs('select_tab', $(this).data('tabid'));
        var visibleTabContents = $(modalid+' .tabContent').not('.hidden');
        var currentChapter = $(modalid).data('current-chapter');
        var exerciseNames = [];
        $.each($(modalid+' .tabs li').not('.hidden'), function(i, e) {
            exerciseNames.push($(e).find('a').html())
        });
        var chapterFrameCount = $(modalid).data('chapter-frame-count');
        //setExerciseTabs: function(tabCount, chapter, exerciseNames, frameCount)
        // mathviewer.setExerciseTabs(visibleTabContents.length, currentChapter, exerciseNames, chapterFrameCount);
        // mathviewer.setImageAudios(visibleTabContents.length, currentChapter,page);
    }
}
