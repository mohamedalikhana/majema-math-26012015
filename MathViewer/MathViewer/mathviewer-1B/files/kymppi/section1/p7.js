(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p94_1.png",
            id: "p94_1"
        }]
    };

    (lib.p94_1 = function() {
        this.initialize(img.p94_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("7", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(557, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(470 + (columnSpace * 32), 29, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape_1, this.text_4, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol2 = function() {
        this.initialize();

        this.text1 = new cjs.Text(" Hur många?", "16px 'Myriad Pro'");
        this.text1.setTransform(19, 4);

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text.setTransform(5, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 510, 230, 10);
        this.roundRect1.setTransform(0, 0);

        // ROW-1 ROUND SHAPES

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                this.shape_group1.graphics.f("#20B14A").s("#000000").ss(0.8, 0, 0, 4).arc(60 + (columnSpace * 25), 35 + (row * 19), 8.2, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);


        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                }
                this.shape_group2.graphics.f("#20B14A").s("#000000").ss(0.8, 0, 0, 4).arc(200 + (columnSpace * 25), 35 + (row * 19), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group2.setTransform(0, 0);



        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                }

                this.shape_group3.graphics.f("#20B14A").s("#000000").ss(0.8, 0, 0, 4).arc(255 + (columnSpace * 25), 35 + (row * 19), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group3.setTransform(0, 0);


        this.shape_group4 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                } else if (row == 2) {
                    columnSpace = column;
                }

                this.shape_group4.graphics.f("#20B14A").s("#000000").ss(0.8, 0, 0, 4).arc(395 + (columnSpace * 25), 35 + (row * 15), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group4.setTransform(0, 0);


        // ROW-2 ROUND SHAPES


        this.shape_group5 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                this.shape_group5.graphics.f("#008BD3").s("#000000").ss(0.8, 0, 0, 4).arc(35 + (columnSpace * 25), 104 + (row * 19), 8.2, 0, 2 * Math.PI);
            }
        }
        this.shape_group5.setTransform(0, 0);

        this.shape_group6 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 0 && row == 0) {
                    continue;
                } else if (column == 1 && row == 0) {
                    columnSpace = 0.5;
                } else if (row == 1) {
                    columnSpace = column;
                }

                this.shape_group6.graphics.f("#008BD3").s("#000000").ss(0.8, 0, 0, 4).arc(92 + (columnSpace * 25), 106 + (row * 18), 8.2, 0, 2 * Math.PI);
            }
        }
        this.shape_group6.setTransform(0, 0);


        this.shape_group7 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                }

                this.shape_group7.graphics.f("#008BD3").s("#000000").ss(0.8, 0, 0, 4).arc(190 + (columnSpace * 25), 104 + (row * 17), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group7.setTransform(0, 0);


        this.shape_group8 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                }

                this.shape_group8.graphics.f("#008BD3").s("#000000").ss(0.8, 0, 0, 4).arc(240 + (columnSpace * 25), 104 + (row * 17), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group8.setTransform(0, 0);


        this.shape_group9 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                }
                this.shape_group9.graphics.f("#008BD3").s("#000000").ss(0.8, 0, 0, 4).arc(290 + (columnSpace * 25), 104 + (row * 17), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group9.setTransform(0, 0);


        this.shape_group10 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                this.shape_group10.graphics.f("#008BD3").s("#000000").ss(0.8, 0, 0, 4).arc(365 + (columnSpace * 25), 104 + (row * 21), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group10.setTransform(0, 0);


        this.shape_group11 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                this.shape_group11.graphics.f("#008BD3").s("#000000").ss(0.8, 0, 0, 4).arc(430 + (columnSpace * 25), 104 + (row * 21), 8.2, 0, 2 * Math.PI);
            }
        }
        this.shape_group11.setTransform(0, 0);

        // ROW-3 ROUND SHAPES

        this.shape_group12 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                } else if (row == 2) {
                    columnSpace = column;
                }

                this.shape_group12.graphics.f("#D51217").s("#000000").ss(0.8, 0, 0, 4).arc(30 + (columnSpace * 25), 185 + (row * 15), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group12.setTransform(0, 0);

        this.shape_group13 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                this.shape_group13.graphics.f("#D51217").s("#000000").ss(0.8, 0, 0, 4).arc(93 + (columnSpace * 23), 185 + (row * 23), 8.2, 0, 2 * Math.PI);
            }
        }
        this.shape_group13.setTransform(0, 0);

        this.shape_group14 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                } else if (row == 2) {
                    columnSpace = column;
                }

                this.shape_group14.graphics.f("#D51217").s("#000000").ss(0.8, 0, 0, 4).arc(190 + (columnSpace * 25), 185 + (row * 15), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group14.setTransform(0, 0);



        this.shape_group15 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                } else if (row == 2) {
                    columnSpace = column;
                }

                this.shape_group15.graphics.f("#D51217").s("#000000").ss(0.8, 0, 0, 4).arc(250 + (columnSpace * 25), 185 + (row * 15), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group15.setTransform(0, 0);



        this.shape_group16 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 0 && row == 1) {
                    continue;
                } else if (column == 1 && row == 1) {
                    columnSpace = 0.5;
                }

                this.shape_group16.graphics.f("#D51217").s("#000000").ss(0.8, 0, 0, 4).arc(310 + (columnSpace * 25), 200 + (row * 19), 8.2, 0, 2 * Math.PI);
            }
        }

        this.shape_group16.setTransform(0, 0);


        this.shape_group17 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                this.shape_group17.graphics.f("#D51217").s("#000000").ss(0.8, 0, 0, 4).arc(350 + (columnSpace * 28), 185 + (row * 21), 8.2, 0, 2 * Math.PI);
            }
        }
        this.shape_group17.setTransform(0, 0);


        this.shape_group18 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                this.shape_group18.graphics.f("#D51217").s("#000000").ss(0.8, 0, 0, 4).arc(412 + (columnSpace * 25), 185 + (row * 21), 8.2, 0, 2 * Math.PI);
            }
        }
        this.shape_group18.setTransform(0, 0);


        var textArr = [];
        var lineX = [115, 280, 460, 115, 280, 460, 115, 280, 460];
        var lineY = [43, 43, 43, 117, 117, 117, 195, 195, 195];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = null;
            hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(12, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            textArr.push(hrLine_1);
        }


        this.hrRule = new cjs.Shape();
        this.hrRule.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 90).lineTo(507, 90);
        this.hrRule.setTransform(0, 0);

        this.hrRule_2 = new cjs.Shape();
        this.hrRule_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 165).lineTo(507, 165);
        this.hrRule_2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170.8, 20).lineTo(170.8, 85);
        this.Line_1.setTransform(0, 0);


        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(335.8, 20).lineTo(335.8, 85);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170.8, 95).lineTo(170.8, 159);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(335.8, 95).lineTo(335.8, 159);
        this.Line_4.setTransform(0, 0);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(335.8, 172).lineTo(335.8, 240);
        this.Line_5.setTransform(0, 0);

        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170.8, 172).lineTo(170.8, 240);
        this.Line_6.setTransform(0, 0);

        this.addChild(this.roundRect1, this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5,
            this.shape_group6, this.shape_group7, this.shape_group8, this.shape_group9, this.shape_group10, this.shape_group11, this.shape_group12,
            this.shape_group13, this.shape_group14, this.shape_group15, this.shape_group16, this.shape_group17, this.shape_group18,
            this.textbox_group1, this.hrRule, this.hrRule_2);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6);

        this.addChild(this.text, this.text1, this.Line_1, this.Line_3, this.Line_5, this.Line_2, this.Line_4, this.Line_6);
        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 4);
        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 510, 120, 10);
        this.roundRect1.setTransform(0, 0);


        var lineArr = [];
        var lineX = [109, 272, 432, 109, 272, 432, 109, 272, 432, 109, 272, 432];
        var lineY = [3, 3, 3, 33, 33, 33, 61, 61, 61, 88, 88, 88];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(38, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("5  +  1  =", "16px 'Myriad Pro'");
        this.label1.setTransform(57, 36);
        this.label2 = new cjs.Text("6  +  1  =", "16px 'Myriad Pro'");
        this.label2.setTransform(57, 65);
        this.label3 = new cjs.Text("7  +  1  =", "16px 'Myriad Pro'");
        this.label3.setTransform(57, 92);
        this.label4 = new cjs.Text("8  +  1  =", "16px 'Myriad Pro'");
        this.label4.setTransform(57, 120);

        this.label6 = new cjs.Text("9  +  1  =", "16px 'Myriad Pro'");
        this.label6.setTransform(220, 36);
        this.label7 = new cjs.Text("4  +  2  =", "16px 'Myriad Pro'");
        this.label7.setTransform(220, 65);
        this.label8 = new cjs.Text("5  +  2  =", "16px 'Myriad Pro'");
        this.label8.setTransform(220, 92);
        this.label9 = new cjs.Text("6  +  2  =", "16px 'Myriad Pro'");
        this.label9.setTransform(220, 120);

        this.label11 = new cjs.Text("7  +  2  =", "16px 'Myriad Pro'");
        this.label11.setTransform(380, 36);
        this.label12 = new cjs.Text("8  +  2  =", "16px 'Myriad Pro'");
        this.label12.setTransform(380, 65);
        this.label13 = new cjs.Text("5  +  3  =", "16px 'Myriad Pro'");
        this.label13.setTransform(380, 92);
        this.label14 = new cjs.Text("5  +  4  =", "16px 'Myriad Pro'");
        this.label14.setTransform(380, 120);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    (lib.Symbol4 = function() {
        this.initialize();
        this.text = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 4);
        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 510, 120, 10);
        this.roundRect1.setTransform(0, 0);


        var lineArr = [];
        var lineX = [109, 273, 438, 109, 273, 438, 109, 273, 438, 109, 273, 438];
        var lineY = [3, 3, 3, 33, 33, 33, 61, 61, 61, 88, 88, 88];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = null;
            hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(35, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("7  –  2  =", "16px 'Myriad Pro'");
        this.label1.setTransform(57, 38);
        this.label2 = new cjs.Text("7  –  5  =", "16px 'Myriad Pro'");
        this.label2.setTransform(57, 67);
        this.label3 = new cjs.Text("7  –  6  =", "16px 'Myriad Pro'");
        this.label3.setTransform(57, 95);
        this.label4 = new cjs.Text("7  –  4  =", "16px 'Myriad Pro'");
        this.label4.setTransform(57, 123);

        this.label6 = new cjs.Text("8  –  3  =", "16px 'Myriad Pro'");
        this.label6.setTransform(220, 38);
        this.label7 = new cjs.Text("8  –  5  =", "16px 'Myriad Pro'");
        this.label7.setTransform(220, 67);
        this.label8 = new cjs.Text("8  –  4  =", "16px 'Myriad Pro'");
        this.label8.setTransform(220, 95);
        this.label9 = new cjs.Text("8  –  6  =", "16px 'Myriad Pro'");
        this.label9.setTransform(220, 123);

        this.label11 = new cjs.Text("9  –  4  =", "16px 'Myriad Pro'");
        this.label11.setTransform(385, 38);
        this.label12 = new cjs.Text("9  –  5  =", "16px 'Myriad Pro'");
        this.label12.setTransform(385, 67);
        this.label13 = new cjs.Text("9  –  3  =", "16px 'Myriad Pro'");
        this.label13.setTransform(385, 95);
        this.label14 = new cjs.Text("9  –  6  =", "16px 'Myriad Pro'");
        this.label14.setTransform(385, 123);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(308, 282, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(308, 337, 1, 1, 0, 0, 0, 255.8, 0);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(308, 500, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
