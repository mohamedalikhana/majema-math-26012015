(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1219,
	height: 678,
	fps: 20,
	color: "#FFFFFF",
	manifest: [
		{src:"images/s22_23_1.png", id:"s22_23_1"},
		{src:"images/s22_23_10.png", id:"s22_23_10"},
		{src:"images/s22_23_11.png", id:"s22_23_11"},
		{src:"images/s22_23_12.png", id:"s22_23_12"},
		{src:"images/s22_23_14.png", id:"s22_23_14"},
		{src:"images/s22_23_15.png", id:"s22_23_15"},
		{src:"images/s22_23_9.png", id:"s22_23_9"},
		{src:"images/p22_1.png", id:"s18_19_2"},
	]
};
// symbols:
(lib.s22_23_1 = function() {
	this.initialize(img.s22_23_1);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,1020,406);


(lib.s22_23_10 = function() {
	this.initialize(img.s22_23_10);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,1274,630);


(lib.s22_23_11 = function() {
	this.initialize(img.s22_23_11);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,1274,630);


(lib.s22_23_12 = function() {
	this.initialize(img.s22_23_12);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,1274,630);


(lib.s22_23_14 = function() {
	this.initialize(img.s22_23_14);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,1013,400);


(lib.s22_23_15 = function() {
	this.initialize(img.s22_23_15);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,179,146);

(lib.s18_19_2 = function() {
	this.initialize(img.s18_19_2);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,929,391);


(lib.s22_23_9 = function() {
	this.initialize(img.s22_23_9);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,1274,630);


(lib.Symbol23 = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("22", "13px 'Myriad Pro'", "#FFFFFF");
	this.text.lineHeight = 18;
	this.text.setTransform(38.4,6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
	this.shape.setTransform(31.1,18.4);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,1218.9,35.1);





(lib.Symbol2 = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("6", "16px 'Myriad Pro'");
	this.text.lineHeight = 19;
	this.text.setTransform(162.4,33);

	this.text_1 = new cjs.Text("5", "16px 'Myriad Pro'");
	this.text_1.lineHeight = 19;
	this.text_1.setTransform(110.6,33);

	this.text_2 = new cjs.Text("4", "16px 'Myriad Pro'");
	this.text_2.lineHeight = 19;
	this.text_2.setTransform(58.8,33);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("EgmEgQwQgdAAgcAOQg5AcAABHIAAd/IACARQAEAVAIASQAdA5BHAAMBMJAAAIASgCQAVgDASgJQA5gcAAhHIAA9/QAAgcgOgcQgdg5hHAAg");
	this.shape.setTransform(257.1,170);

	this.text_3 = new cjs.Text("3", "16px 'Myriad Pro'");
	this.text_3.lineHeight = 19;
	this.text_3.setTransform(7,33);

	this.text_4 = new cjs.Text("Måla.", "16px 'Myriad Pro'");
	this.text_4.lineHeight = 19;
	this.text_4.setTransform(19.2,5.2);

	this.text_5 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#00A5C0");
	this.text_5.lineHeight = 20;
	this.text_5.setTransform(0,4);

	this.instance = new lib.s22_23_9();
	this.instance.setTransform(155.6,30,0.035,0.035);

	this.instance_1 = new lib.s22_23_10();
	this.instance_1.setTransform(51.4,30,0.035,0.035);

	this.instance_2 = new lib.s22_23_11();
	this.instance_2.setTransform(104.3,30,0.035,0.035);

	this.instance_3 = new lib.s22_23_12();
	this.instance_3.setTransform(0,30,0.035,0.035);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#25A449").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
	this.shape_1.setTransform(188.4,41);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF173").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
	this.shape_2.setTransform(136,41);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0089CA").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
	this.shape_3.setTransform(83.5,41);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C51930").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
	this.shape_4.setTransform(32,41);

	this.instance_4 = new lib.s22_23_1();
	this.instance_4.setTransform(1.6,70,0.5,0.5);

	this.addChild(this.instance_4,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance_3,this.instance_2,this.instance_1,this.instance,this.text_5,this.text_4,this.text_3,this.shape,this.ra1,this.text_2,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,248.5);


(lib.Symbol101 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.fontfix = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.text_2 = new cjs.Text("Mira får 1 kula mer än Leo. Visa.", "16px 'Myriad Pro'");
	this.text_2.lineHeight = 22;
	this.text_2.setTransform(5,6.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2}]}).wait(1));

	this.text = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#00A5C0");
	this.text.lineHeight = 20;
	this.text.setTransform(-15,5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.virtualBounds = new cjs.Rectangle(5,0,485.3,25.3);




(lib.Symbol11 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4pgQgcAAgdAOQg4AdAABHIAAPeIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhGIAAveQAAgdgNgcQgdg5hHAAg");
	this.shape.setTransform(386.6,227.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4pgQgcAAgdAOQg4AcAABIIAAPdIABASQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gdAAhHIAAvdQAAgdgNgcQgdg5hHAAg");
	this.shape_1.setTransform(386.6,95.4);

	this.instance = new lib.Symbol101();
	this.instance.setTransform(224,13.6,1,1,0,0,0,208.6,12.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnLjKQgdAAgcAPQg5AcAABHIAACyIACARQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEARgJQA5gcAAhGIAAiyQAAgdgOgcQgdg5hGAAg");
	this.shape_2.setTransform(447.9,259.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnLjKQgdAAgcAPQg5AcAABHIAACyIACARQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEASgJQA4gcAAhGIAAiyQAAgdgOgcQgcg5hHAAg");
	this.shape_3.setTransform(324.8,259.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnMjKQgcAAgcAPQg5AcAABHIAACyIACARQADAVAJASQAcA5BHAAIOYAAIASgCQAWgEARgJQA5gcAAhGIAAiyQAAgdgOgcQgdg5hHAAg");
	this.shape_4.setTransform(190.1,259.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnMjKQgcAAgcAPQg5AcAABHIAACyIACARQAEAVAIASQAdA5BGAAIOZAAIARgCQAWgEARgJQA5gcAAhGIAAiyQAAgdgOgcQgdg5hGAAg");
	this.shape_5.setTransform(66.9,259.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnLjJQgdAAgcAOQg5AcAABHIAACxIACASQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEARgIQA5gdAAhHIAAixQAAgcgOgcQgdg5hGAAg");
	this.shape_6.setTransform(447.9,127.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnLjJQgdAAgcAOQg5AcAABHIAACxIACASQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEASgIQA4gdAAhHIAAixQAAgcgOgcQgcg5hHAAg");
	this.shape_7.setTransform(324.8,127.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnMjJQgcAAgcAOQg5AcAABHIAACxIACASQADAVAJASQAcA5BHAAIOYAAIASgCQAWgEARgIQA5gdAAhHIAAixQAAgcgOgcQgdg5hHAAg");
	this.shape_8.setTransform(190.1,127.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AnMjJQgcAAgcAOQg5AcAABHIAACxIACASQAEAVAIASQAdA5BGAAIOZAAIARgCQAWgEARgIQA5gdAAhHIAAixQAAgcgOgcQgdg5hGAAg");
	this.shape_9.setTransform(66.9,127.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4pgQgcAAgdAOQg4AdAABHIAAPeIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhGIAAveQAAgdgNgcQgdg5hHAAg");
	this.shape_10.setTransform(127.9,227.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4pgQgcAAgdAOQg4AcAABIIAAPdIABASQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gdAAhHIAAvdQAAgdgNgcQgdg5hHAAg");
	this.shape_11.setTransform(127.9,95.4);

	this.instance_1 = new lib.s18_19_2();
	this.instance_1.setTransform(27.3,42.5,0.5,0.5);
	
	this.addChild(this.instance_1,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.text,this.ra1,this.ra2,this.ra3,this.ra4,this.instance,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.5,289.9);

// stage content:
(lib.p22 = function() {
	this.initialize();

	// Layer 1
	this.other = new lib.Symbol23();
	this.other.setTransform(609.5,660.2,1,1,0,0,0,609.5,17.5);

	this.v3 = new lib.Symbol11();
	this.v3.setTransform(296.5,490,1,1,0,0,0,256.3,144.6);
	
	this.v1 = new lib.Symbol2();
	this.v1.setTransform(296.5,174.3,1,1,0,0,0,256.3,123.9);

	this.addChild(this.v1,this.v3,this.other);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(609.5,389.4,1218.9,627.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;