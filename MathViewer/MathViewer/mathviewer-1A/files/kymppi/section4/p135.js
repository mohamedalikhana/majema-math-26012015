(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p135_1.png",
            id: "p135_1"
        }, {
            src: "images/p135_2.png",
            id: "p135_2"
        }]
    };

    (lib.p135_1 = function() {
        this.initialize(img.p135_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p135_2 = function() {
        this.initialize(img.p135_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("47", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(67, 24);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8490C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(56, 23.5, 1.09, 1.1);

        this.text_2 = new cjs.Text("Stapeldiagram", "24px 'MyriadPro-Semibold'", "#8490C8");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(113.5, 27);

        this.pageBottomText = new cjs.Text("kunna läsa av och göra eget stapeldiagram", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(382, 649);

        this.text = new cjs.Text("135", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(560.5, 646);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(587.5, 660.8, 1, 1.15);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#E0E4F2").s('#8490C8').drawRoundRect(0, 10, 515, 246, 10);
        this.roundRect1.setTransform(0, 17);

        this.instance = new lib.p135_1();
        this.instance.setTransform(82, 201, 0.47, 0.47);

        this.text_h1 = new cjs.Text("Rösta på ditt favoritdjur", "15px 'MyriadPro-Semibold'");
        this.text_h1.lineHeight = 19;
        this.text_h1.setTransform(173, 39);

        this.text_y1 = new cjs.Text("Antal röster", "12px 'Myriad Pro'");
        this.text_y1.lineHeight = 19;
        this.text_y1.setTransform(17, 45);

        this.text_x1 = new cjs.Text("Djur", "12px 'Myriad Pro'");
        this.text_x1.lineHeight = 19;
        this.text_x1.setTransform(447, 190);

        this.text_1 = new cjs.Text("Polly", "14px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(95, 253);
        this.text_2 = new cjs.Text("Kitty", "14px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(169, 253);
        this.text_3 = new cjs.Text("Molly", "14px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(239, 253);
        this.text_4 = new cjs.Text("Skutt", "14px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(312, 253);
        this.text_5 = new cjs.Text("Gnagy", "14px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(380, 253);

        var ToBeAdded = [];
        for (var num = 0; num < 11; num++) {
            var xPos = 69;
            var temptext = new cjs.Text("" + num, "12px 'Myriad Pro'");
            temptext.lineHeight = -1;
            if (num == 10) {
                xPos = 62;
                num = 10.15;
            }
            temptext.setTransform(xPos, 194 + (-13.3 * num));
            ToBeAdded.push(temptext);
        }

        this.shape_arrow1 = new cjs.Shape();
        this.shape_arrow1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow1.setTransform(85.8, 52, 1, 1, 175);

        this.shape_arrow2 = new cjs.Shape();
        this.shape_arrow2.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow2.setTransform(438, 199.8, 1, 1, 265);

        this.Line_bg1 = new cjs.Shape();
        this.Line_bg1.graphics.f("#ffffff").beginStroke("#ffffff").setStrokeStyle(0.7).moveTo(86, 65).lineTo(86, 200).lineTo(414, 200).lineTo(414, 65).lineTo(86, 65);
        this.Line_bg1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(86, 55).lineTo(86, 200).moveTo(80.5, 200).lineTo(436, 200)
            .moveTo(96, 65).lineTo(96, 200).moveTo(120, 65).lineTo(120, 200)
            .moveTo(168, 65).lineTo(168, 200).moveTo(192, 65).lineTo(192, 200)
            .moveTo(243.6, 65).lineTo(243.6, 200).moveTo(267.6, 65).lineTo(267.6, 200).moveTo(316, 65).lineTo(316, 200).moveTo(340, 65).lineTo(340, 200).moveTo(390, 65).lineTo(390, 200).moveTo(414, 65).lineTo(414, 200);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#949599").setStrokeStyle(0.7).moveTo(80.5, 186.5).lineTo(414, 186.5).moveTo(80.5, 173).lineTo(414, 173).moveTo(80.5, 159.5).lineTo(414, 159.5).moveTo(80.5, 146).lineTo(414, 146).moveTo(80.5, 119).lineTo(414, 119).moveTo(80.5, 105.5).lineTo(414, 105.5).moveTo(80.5, 92).lineTo(414, 92).moveTo(80.5, 78.5).lineTo(414, 78.5);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(80.5, 65).lineTo(422, 65).moveTo(80.5, 132.5).lineTo(422, 132.5);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7)
            .moveTo(96, 79).lineTo(96, 200).lineTo(120, 200).lineTo(120, 79).lineTo(96, 79)
            .moveTo(168, 119.5).lineTo(168, 200).lineTo(192, 200).lineTo(192, 119.5).lineTo(168, 119.5)
            .moveTo(243.6, 65).lineTo(243.6, 200).lineTo(267.6, 200).lineTo(267.6, 65).lineTo(243.6, 65)
            .moveTo(316, 106).lineTo(316, 200).lineTo(340, 200).lineTo(340, 106).lineTo(316, 106)
            .moveTo(390, 146).lineTo(390, 200).lineTo(414, 200).lineTo(414, 146).lineTo(390, 146);
        this.Line_4.setTransform(0, 0);

        this.addChild(this.roundRect1, this.instance, this.text_h1, this.text_y1, this.text_x1, this.text_1, this.Line_bg1, this.Line_4, this.Line_1, this.Line_2, this.Line_3, this.Line_5, this.text_2, this.text_3, this.text_4, this.text_5, this.shape_arrow1, this.shape_arrow2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i])
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 280);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.text_q2 = new cjs.Text("Hur många fler röster fick", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20, 1.13);

        this.instance_2 = new lib.p135_2();
        this.instance_2.setTransform(23.5, 28.5, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 93, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 15);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 112);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 112);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 208.9);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 208.9);

        var arrTxtbox = ['89', '185', '282.2'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 120,
            rectHeight = 22,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 6,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 66.5;
                if (i == 1) {
                    padding = 104;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("9", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(66, 79.3);
        this.text_2 = new cjs.Text("–", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(87, 79.3);
        this.text_3 = new cjs.Text("6", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(107, 79.3);
        this.text_4 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(128, 79.3);

        var ToBeAdded = [];
        for (var col = 0; col < 2; col++) {
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                var colSpace = col;
                var temptext = new cjs.Text("än", "16px 'Myriad Pro'");
                temptext.lineHeight = -1;
                temptext.setTransform(122 + (263.7 * colSpace), 52 + (95.5 * rowSpace));
                ToBeAdded.push(temptext);
            }
        }

        this.addChild(this.text, this.text_q2, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 300);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(308, 252, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol6();
        this.v2.setTransform(308, 536, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
