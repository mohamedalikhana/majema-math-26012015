var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };
    var transformX = transformY = 0.195;
    var moveX = 480 + 70,
        moveY = -9;
    var startX = ((399.3333) * transformX) + moveX,
        startY = ((236.6666) * transformY) + moveY;
    var endX = ((75.3334) * transformX) + moveX,
        endY = ((1705.0000) * transformY) + moveY;
    var pencil = null;

    var startX2 = ((74.6666) * transformX) + moveX,
        startY2 = ((1705.0000) * transformY) + moveY,
        endX2 = ((74.0000) * transformX) + moveX,
        endY2 = ((1705.0000) * transformY) + moveY;
    var altStartX2 = startX,
        altStartY2 = startY,
        altEndX2 = endX,
        altEndY2 = endY;
    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [
        new createjs.Point(((399.3333) * transformX) + moveX, ((236.6666) * transformY) + moveY),
        new createjs.Point(((399.6667) * transformX) + moveX, ((237.3334) * transformY) + moveY),
        new createjs.Point(((400.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((400.6666) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((401.3334) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((402.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((402.0000) * transformX) + moveX, ((238.3333) * transformY) + moveY),
        new createjs.Point(((402.0000) * transformX) + moveX, ((238.6667) * transformY) + moveY),
        new createjs.Point(((402.0000) * transformX) + moveX, ((239.0000) * transformY) + moveY),
        new createjs.Point(((403.3649) * transformX) + moveX, ((239.9246) * transformY) + moveY),
        new createjs.Point(((403.7622) * transformX) + moveX, ((239.6913) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((240.0000) * transformY) + moveY),
        new createjs.Point(((406.3333) * transformX) + moveX, ((239.0001) * transformY) + moveY),
        new createjs.Point(((406.6667) * transformX) + moveX, ((237.9999) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((237.0000) * transformY) + moveY),
        new createjs.Point(((407.6666) * transformX) + moveX, ((236.6667) * transformY) + moveY),
        new createjs.Point(((408.3334) * transformX) + moveX, ((236.3333) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((235.3334) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((234.6666) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((234.0000) * transformY) + moveY),


        new createjs.Point(((409.9999) * transformX) + moveX, ((233.3334) * transformY) + moveY),
        new createjs.Point(((411.0001) * transformX) + moveX, ((232.6666) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((232.0000) * transformY) + moveY),


        new createjs.Point(((412.0000) * transformX) + moveX, ((231.3334) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((230.6666) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((230.0000) * transformY) + moveY),


        new createjs.Point(((412.6666) * transformX) + moveX, ((229.6667) * transformY) + moveY),
        new createjs.Point(((413.3334) * transformX) + moveX, ((229.3333) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((229.0000) * transformY) + moveY),


        new createjs.Point(((414.0000) * transformX) + moveX, ((228.3334) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((227.6666) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((227.0000) * transformY) + moveY),


        new createjs.Point(((414.9999) * transformX) + moveX, ((226.3334) * transformY) + moveY),
        new createjs.Point(((416.0001) * transformX) + moveX, ((225.6666) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((225.0000) * transformY) + moveY),


        new createjs.Point(((417.0000) * transformX) + moveX, ((224.3334) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((223.6666) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((223.0000) * transformY) + moveY),


        new createjs.Point(((417.9999) * transformX) + moveX, ((222.3334) * transformY) + moveY),
        new createjs.Point(((419.0001) * transformX) + moveX, ((221.6666) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),


        new createjs.Point(((420.0000) * transformX) + moveX, ((220.3334) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((219.6666) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((219.0000) * transformY) + moveY),


        new createjs.Point(((420.9999) * transformX) + moveX, ((218.3334) * transformY) + moveY),
        new createjs.Point(((422.0001) * transformX) + moveX, ((217.6666) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((217.0000) * transformY) + moveY),


        new createjs.Point(((423.0000) * transformX) + moveX, ((216.3334) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((215.6666) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((215.0000) * transformY) + moveY),


        new createjs.Point(((423.9999) * transformX) + moveX, ((214.3334) * transformY) + moveY),
        new createjs.Point(((425.0001) * transformX) + moveX, ((213.6666) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((213.0000) * transformY) + moveY),


        new createjs.Point(((426.0000) * transformX) + moveX, ((212.3334) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((211.6666) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((211.0000) * transformY) + moveY),


        new createjs.Point(((426.6666) * transformX) + moveX, ((210.6667) * transformY) + moveY),
        new createjs.Point(((427.3334) * transformX) + moveX, ((210.3333) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((210.0000) * transformY) + moveY),


        new createjs.Point(((428.0000) * transformX) + moveX, ((209.6667) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((209.3333) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((209.0000) * transformY) + moveY),


        new createjs.Point(((428.6666) * transformX) + moveX, ((208.6667) * transformY) + moveY),
        new createjs.Point(((429.3334) * transformX) + moveX, ((208.3333) * transformY) + moveY),
        new createjs.Point(((430.0000) * transformX) + moveX, ((208.0000) * transformY) + moveY),


        new createjs.Point(((430.0000) * transformX) + moveX, ((207.3334) * transformY) + moveY),
        new createjs.Point(((430.0000) * transformX) + moveX, ((206.6666) * transformY) + moveY),
        new createjs.Point(((430.0000) * transformX) + moveX, ((206.0000) * transformY) + moveY),


        new createjs.Point(((431.3332) * transformX) + moveX, ((205.0001) * transformY) + moveY),
        new createjs.Point(((432.6668) * transformX) + moveX, ((203.9999) * transformY) + moveY),
        new createjs.Point(((434.0000) * transformX) + moveX, ((203.0000) * transformY) + moveY),


        new createjs.Point(((434.0000) * transformX) + moveX, ((202.3334) * transformY) + moveY),
        new createjs.Point(((434.0000) * transformX) + moveX, ((201.6666) * transformY) + moveY),
        new createjs.Point(((434.0000) * transformX) + moveX, ((201.0000) * transformY) + moveY),


        new createjs.Point(((434.9999) * transformX) + moveX, ((200.3334) * transformY) + moveY),
        new createjs.Point(((436.0001) * transformX) + moveX, ((199.6666) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((199.0000) * transformY) + moveY),


        new createjs.Point(((437.0000) * transformX) + moveX, ((198.6667) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((198.3333) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((198.0000) * transformY) + moveY),


        new createjs.Point(((437.6666) * transformX) + moveX, ((197.6667) * transformY) + moveY),
        new createjs.Point(((438.3334) * transformX) + moveX, ((197.3333) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),


        new createjs.Point(((439.0000) * transformX) + moveX, ((196.3334) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((195.6666) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),


        new createjs.Point(((440.9998) * transformX) + moveX, ((193.3335) * transformY) + moveY),
        new createjs.Point(((443.0002) * transformX) + moveX, ((191.6665) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((190.0000) * transformY) + moveY),


        new createjs.Point(((445.0000) * transformX) + moveX, ((189.3334) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((188.6666) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),


        new createjs.Point(((447.6664) * transformX) + moveX, ((185.6669) * transformY) + moveY),
        new createjs.Point(((450.3336) * transformX) + moveX, ((183.3331) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((181.0000) * transformY) + moveY),


        new createjs.Point(((453.0000) * transformX) + moveX, ((180.3334) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((179.6666) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((179.0000) * transformY) + moveY),


        new createjs.Point(((456.3330) * transformX) + moveX, ((176.0003) * transformY) + moveY),
        new createjs.Point(((459.6670) * transformX) + moveX, ((172.9997) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((170.0000) * transformY) + moveY),


        new createjs.Point(((467.9995) * transformX) + moveX, ((164.6672) * transformY) + moveY),
        new createjs.Point(((473.0005) * transformX) + moveX, ((159.3328) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((154.0000) * transformY) + moveY),


        new createjs.Point(((478.6666) * transformX) + moveX, ((154.0000) * transformY) + moveY),
        new createjs.Point(((479.3334) * transformX) + moveX, ((154.0000) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((154.0000) * transformY) + moveY),


        new createjs.Point(((481.9998) * transformX) + moveX, ((151.6669) * transformY) + moveY),
        new createjs.Point(((484.0002) * transformX) + moveX, ((149.3331) * transformY) + moveY),
        new createjs.Point(((486.0000) * transformX) + moveX, ((147.0000) * transformY) + moveY),


        new createjs.Point(((486.6666) * transformX) + moveX, ((147.0000) * transformY) + moveY),
        new createjs.Point(((487.3334) * transformX) + moveX, ((147.0000) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((147.0000) * transformY) + moveY),


        new createjs.Point(((489.6665) * transformX) + moveX, ((145.0002) * transformY) + moveY),
        new createjs.Point(((491.3335) * transformX) + moveX, ((142.9998) * transformY) + moveY),
        new createjs.Point(((493.0000) * transformX) + moveX, ((141.0000) * transformY) + moveY),


        new createjs.Point(((493.6666) * transformX) + moveX, ((141.0000) * transformY) + moveY),
        new createjs.Point(((494.3334) * transformX) + moveX, ((141.0000) * transformY) + moveY),
        new createjs.Point(((495.0000) * transformX) + moveX, ((141.0000) * transformY) + moveY),


        new createjs.Point(((495.9999) * transformX) + moveX, ((139.6668) * transformY) + moveY),
        new createjs.Point(((497.0001) * transformX) + moveX, ((138.3332) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((137.0000) * transformY) + moveY),


        new createjs.Point(((498.6666) * transformX) + moveX, ((137.0000) * transformY) + moveY),
        new createjs.Point(((499.3334) * transformX) + moveX, ((137.0000) * transformY) + moveY),
        new createjs.Point(((500.0000) * transformX) + moveX, ((137.0000) * transformY) + moveY),


        new createjs.Point(((500.6666) * transformX) + moveX, ((136.0001) * transformY) + moveY),
        new createjs.Point(((501.3334) * transformX) + moveX, ((134.9999) * transformY) + moveY),
        new createjs.Point(((502.0000) * transformX) + moveX, ((134.0000) * transformY) + moveY),


        new createjs.Point(((502.6666) * transformX) + moveX, ((134.0000) * transformY) + moveY),
        new createjs.Point(((503.3334) * transformX) + moveX, ((134.0000) * transformY) + moveY),
        new createjs.Point(((504.0000) * transformX) + moveX, ((134.0000) * transformY) + moveY),


        new createjs.Point(((504.6666) * transformX) + moveX, ((133.0001) * transformY) + moveY),
        new createjs.Point(((505.3334) * transformX) + moveX, ((131.9999) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((131.0000) * transformY) + moveY),


        new createjs.Point(((506.6666) * transformX) + moveX, ((131.0000) * transformY) + moveY),
        new createjs.Point(((507.3334) * transformX) + moveX, ((131.0000) * transformY) + moveY),
        new createjs.Point(((508.0000) * transformX) + moveX, ((131.0000) * transformY) + moveY),


        new createjs.Point(((508.6666) * transformX) + moveX, ((130.0001) * transformY) + moveY),
        new createjs.Point(((509.3334) * transformX) + moveX, ((128.9999) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((128.0000) * transformY) + moveY),


        new createjs.Point(((510.6666) * transformX) + moveX, ((128.0000) * transformY) + moveY),
        new createjs.Point(((511.3334) * transformX) + moveX, ((128.0000) * transformY) + moveY),
        new createjs.Point(((512.0000) * transformX) + moveX, ((128.0000) * transformY) + moveY),


        new createjs.Point(((512.3333) * transformX) + moveX, ((127.3334) * transformY) + moveY),
        new createjs.Point(((512.6667) * transformX) + moveX, ((126.6666) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((126.0000) * transformY) + moveY),


        new createjs.Point(((513.6666) * transformX) + moveX, ((126.0000) * transformY) + moveY),
        new createjs.Point(((514.3334) * transformX) + moveX, ((126.0000) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((126.0000) * transformY) + moveY),


        new createjs.Point(((515.3333) * transformX) + moveX, ((125.3334) * transformY) + moveY),
        new createjs.Point(((515.6667) * transformX) + moveX, ((124.6666) * transformY) + moveY),
        new createjs.Point(((516.0000) * transformX) + moveX, ((124.0000) * transformY) + moveY),


        new createjs.Point(((516.6666) * transformX) + moveX, ((124.0000) * transformY) + moveY),
        new createjs.Point(((517.3334) * transformX) + moveX, ((124.0000) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((124.0000) * transformY) + moveY),


        new createjs.Point(((518.3333) * transformX) + moveX, ((123.3334) * transformY) + moveY),
        new createjs.Point(((518.6667) * transformX) + moveX, ((122.6666) * transformY) + moveY),
        new createjs.Point(((519.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),


        new createjs.Point(((519.6666) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((520.3334) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((521.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),


        new createjs.Point(((521.3333) * transformX) + moveX, ((121.3334) * transformY) + moveY),
        new createjs.Point(((521.6667) * transformX) + moveX, ((120.6666) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((120.0000) * transformY) + moveY),


        new createjs.Point(((522.6666) * transformX) + moveX, ((120.0000) * transformY) + moveY),
        new createjs.Point(((523.3334) * transformX) + moveX, ((120.0000) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((120.0000) * transformY) + moveY),


        new createjs.Point(((524.3333) * transformX) + moveX, ((119.3334) * transformY) + moveY),
        new createjs.Point(((524.6667) * transformX) + moveX, ((118.6666) * transformY) + moveY),
        new createjs.Point(((525.0000) * transformX) + moveX, ((118.0000) * transformY) + moveY),


        new createjs.Point(((526.3332) * transformX) + moveX, ((117.6667) * transformY) + moveY),
        new createjs.Point(((527.6668) * transformX) + moveX, ((117.3333) * transformY) + moveY),
        new createjs.Point(((529.0000) * transformX) + moveX, ((117.0000) * transformY) + moveY),


        new createjs.Point(((529.3333) * transformX) + moveX, ((116.3334) * transformY) + moveY),
        new createjs.Point(((529.6667) * transformX) + moveX, ((115.6666) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),


        new createjs.Point(((531.3332) * transformX) + moveX, ((114.6667) * transformY) + moveY),
        new createjs.Point(((532.6668) * transformX) + moveX, ((114.3333) * transformY) + moveY),
        new createjs.Point(((534.0000) * transformX) + moveX, ((114.0000) * transformY) + moveY),


        new createjs.Point(((534.3333) * transformX) + moveX, ((113.3334) * transformY) + moveY),
        new createjs.Point(((534.6667) * transformX) + moveX, ((112.6666) * transformY) + moveY),
        new createjs.Point(((535.0000) * transformX) + moveX, ((112.0000) * transformY) + moveY),


        new createjs.Point(((535.6666) * transformX) + moveX, ((112.0000) * transformY) + moveY),
        new createjs.Point(((536.3334) * transformX) + moveX, ((112.0000) * transformY) + moveY),
        new createjs.Point(((537.0000) * transformX) + moveX, ((112.0000) * transformY) + moveY),


        new createjs.Point(((537.0000) * transformX) + moveX, ((111.6667) * transformY) + moveY),
        new createjs.Point(((537.0000) * transformX) + moveX, ((111.3333) * transformY) + moveY),
        new createjs.Point(((537.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),


        new createjs.Point(((537.6666) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((538.3334) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),


        new createjs.Point(((539.0000) * transformX) + moveX, ((110.6667) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((110.3333) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((110.0000) * transformY) + moveY),


        new createjs.Point(((539.6666) * transformX) + moveX, ((110.0000) * transformY) + moveY),
        new createjs.Point(((540.3334) * transformX) + moveX, ((110.0000) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((110.0000) * transformY) + moveY),


        new createjs.Point(((541.0000) * transformX) + moveX, ((109.6667) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((109.3333) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),


        new createjs.Point(((541.6666) * transformX) + moveX, ((109.0000) * transformY) + moveY),
        new createjs.Point(((542.3334) * transformX) + moveX, ((109.0000) * transformY) + moveY),
        new createjs.Point(((543.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),


        new createjs.Point(((543.3333) * transformX) + moveX, ((108.3334) * transformY) + moveY),
        new createjs.Point(((543.6667) * transformX) + moveX, ((107.6666) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((107.0000) * transformY) + moveY),


        new createjs.Point(((545.3332) * transformX) + moveX, ((106.6667) * transformY) + moveY),
        new createjs.Point(((546.6668) * transformX) + moveX, ((106.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((106.0000) * transformY) + moveY),


        new createjs.Point(((548.0000) * transformX) + moveX, ((105.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((105.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((105.0000) * transformY) + moveY),


        new createjs.Point(((549.9998) * transformX) + moveX, ((104.3334) * transformY) + moveY),
        new createjs.Point(((552.0002) * transformX) + moveX, ((103.6666) * transformY) + moveY),
        new createjs.Point(((554.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),


        new createjs.Point(((554.0000) * transformX) + moveX, ((102.6667) * transformY) + moveY),
        new createjs.Point(((554.0000) * transformX) + moveX, ((102.3333) * transformY) + moveY),
        new createjs.Point(((554.0000) * transformX) + moveX, ((102.0000) * transformY) + moveY),


        new createjs.Point(((554.9999) * transformX) + moveX, ((102.0000) * transformY) + moveY),
        new createjs.Point(((556.0001) * transformX) + moveX, ((102.0000) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((102.0000) * transformY) + moveY),


        new createjs.Point(((557.0000) * transformX) + moveX, ((101.6667) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((101.3333) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),


        new createjs.Point(((557.6666) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((558.3334) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),


        new createjs.Point(((559.0000) * transformX) + moveX, ((100.6667) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((100.3333) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((100.0000) * transformY) + moveY),


        new createjs.Point(((560.3332) * transformX) + moveX, ((99.6667) * transformY) + moveY),
        new createjs.Point(((561.6668) * transformX) + moveX, ((99.3333) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((99.0000) * transformY) + moveY),


        new createjs.Point(((563.0000) * transformX) + moveX, ((98.6667) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((98.3333) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((98.0000) * transformY) + moveY),


        new createjs.Point(((563.9999) * transformX) + moveX, ((98.0000) * transformY) + moveY),
        new createjs.Point(((565.0001) * transformX) + moveX, ((98.0000) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((98.0000) * transformY) + moveY),


        new createjs.Point(((566.0000) * transformX) + moveX, ((97.6667) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((97.3333) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((97.0000) * transformY) + moveY),


        new createjs.Point(((566.6666) * transformX) + moveX, ((97.0000) * transformY) + moveY),
        new createjs.Point(((567.3334) * transformX) + moveX, ((97.0000) * transformY) + moveY),
        new createjs.Point(((568.0000) * transformX) + moveX, ((97.0000) * transformY) + moveY),


        new createjs.Point(((568.0000) * transformX) + moveX, ((96.6667) * transformY) + moveY),
        new createjs.Point(((568.0000) * transformX) + moveX, ((96.3333) * transformY) + moveY),
        new createjs.Point(((568.0000) * transformX) + moveX, ((96.0000) * transformY) + moveY),


        new createjs.Point(((568.9999) * transformX) + moveX, ((96.0000) * transformY) + moveY),
        new createjs.Point(((570.0001) * transformX) + moveX, ((96.0000) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((96.0000) * transformY) + moveY),


        new createjs.Point(((571.0000) * transformX) + moveX, ((95.6667) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((95.3333) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((95.0000) * transformY) + moveY),


        new createjs.Point(((571.6666) * transformX) + moveX, ((95.0000) * transformY) + moveY),
        new createjs.Point(((572.3334) * transformX) + moveX, ((95.0000) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((95.0000) * transformY) + moveY),


        new createjs.Point(((573.0000) * transformX) + moveX, ((94.6667) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((94.3333) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),


        new createjs.Point(((574.9998) * transformX) + moveX, ((93.6667) * transformY) + moveY),
        new createjs.Point(((577.0002) * transformX) + moveX, ((93.3333) * transformY) + moveY),
        new createjs.Point(((579.0000) * transformX) + moveX, ((93.0000) * transformY) + moveY),


        new createjs.Point(((579.0000) * transformX) + moveX, ((92.6667) * transformY) + moveY),
        new createjs.Point(((579.0000) * transformX) + moveX, ((92.3333) * transformY) + moveY),
        new createjs.Point(((579.0000) * transformX) + moveX, ((92.0000) * transformY) + moveY),


        new createjs.Point(((579.9999) * transformX) + moveX, ((92.0000) * transformY) + moveY),
        new createjs.Point(((581.0001) * transformX) + moveX, ((92.0000) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((92.0000) * transformY) + moveY),


        new createjs.Point(((582.0000) * transformX) + moveX, ((91.6667) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((91.3333) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),


        new createjs.Point(((582.9999) * transformX) + moveX, ((91.0000) * transformY) + moveY),
        new createjs.Point(((584.0001) * transformX) + moveX, ((91.0000) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),


        new createjs.Point(((585.0000) * transformX) + moveX, ((90.6667) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((90.3333) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((90.0000) * transformY) + moveY),


        new createjs.Point(((585.9999) * transformX) + moveX, ((90.0000) * transformY) + moveY),
        new createjs.Point(((587.0001) * transformX) + moveX, ((90.0000) * transformY) + moveY),
        new createjs.Point(((588.0000) * transformX) + moveX, ((90.0000) * transformY) + moveY),


        new createjs.Point(((588.0000) * transformX) + moveX, ((89.6667) * transformY) + moveY),
        new createjs.Point(((588.0000) * transformX) + moveX, ((89.3333) * transformY) + moveY),
        new createjs.Point(((588.0000) * transformX) + moveX, ((89.0000) * transformY) + moveY),


        new createjs.Point(((588.9999) * transformX) + moveX, ((89.0000) * transformY) + moveY),
        new createjs.Point(((590.0001) * transformX) + moveX, ((89.0000) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((89.0000) * transformY) + moveY),


        new createjs.Point(((591.0000) * transformX) + moveX, ((88.6667) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((88.3333) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),


        new createjs.Point(((593.3331) * transformX) + moveX, ((87.6667) * transformY) + moveY),
        new createjs.Point(((595.6669) * transformX) + moveX, ((87.3333) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((87.0000) * transformY) + moveY),


        new createjs.Point(((598.0000) * transformX) + moveX, ((86.6667) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((86.3333) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((86.0000) * transformY) + moveY),


        new createjs.Point(((600.6664) * transformX) + moveX, ((85.6667) * transformY) + moveY),
        new createjs.Point(((603.3336) * transformX) + moveX, ((85.3333) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((85.0000) * transformY) + moveY),


        new createjs.Point(((606.0000) * transformX) + moveX, ((84.6667) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((84.3333) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),


        new createjs.Point(((609.3330) * transformX) + moveX, ((83.6667) * transformY) + moveY),
        new createjs.Point(((612.6670) * transformX) + moveX, ((83.3333) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((83.0000) * transformY) + moveY),


        new createjs.Point(((616.0000) * transformX) + moveX, ((82.6667) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((82.3333) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((82.0000) * transformY) + moveY),


        new createjs.Point(((619.9996) * transformX) + moveX, ((81.6667) * transformY) + moveY),
        new createjs.Point(((624.0004) * transformX) + moveX, ((81.3333) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((81.0000) * transformY) + moveY),


        new createjs.Point(((638.2451) * transformX) + moveX, ((78.0433) * transformY) + moveY),
        new createjs.Point(((652.0892) * transformX) + moveX, ((78.0018) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),


        new createjs.Point(((669.9995) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((675.0005) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),


        new createjs.Point(((680.0000) * transformX) + moveX, ((78.3333) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((78.6667) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((79.0000) * transformY) + moveY),


        new createjs.Point(((685.6661) * transformX) + moveX, ((79.3333) * transformY) + moveY),
        new createjs.Point(((691.3339) * transformX) + moveX, ((79.6667) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((80.0000) * transformY) + moveY),


        new createjs.Point(((697.0000) * transformX) + moveX, ((80.3333) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((80.6667) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((81.0000) * transformY) + moveY),


        new createjs.Point(((698.9998) * transformX) + moveX, ((81.0000) * transformY) + moveY),
        new createjs.Point(((701.0002) * transformX) + moveX, ((81.0000) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((81.0000) * transformY) + moveY),


        new createjs.Point(((703.0000) * transformX) + moveX, ((81.3333) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((81.6667) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((82.0000) * transformY) + moveY),


        new createjs.Point(((704.6665) * transformX) + moveX, ((82.0000) * transformY) + moveY),
        new createjs.Point(((706.3335) * transformX) + moveX, ((82.0000) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((82.0000) * transformY) + moveY),


        new createjs.Point(((708.0000) * transformX) + moveX, ((82.3333) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((82.6667) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((83.0000) * transformY) + moveY),


        new createjs.Point(((709.6665) * transformX) + moveX, ((83.0000) * transformY) + moveY),
        new createjs.Point(((711.3335) * transformX) + moveX, ((83.0000) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((83.0000) * transformY) + moveY),


        new createjs.Point(((713.0000) * transformX) + moveX, ((83.3333) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((83.6667) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),


        new createjs.Point(((714.3332) * transformX) + moveX, ((84.0000) * transformY) + moveY),
        new createjs.Point(((715.6668) * transformX) + moveX, ((84.0000) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),


        new createjs.Point(((717.0000) * transformX) + moveX, ((84.3333) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((84.6667) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((85.0000) * transformY) + moveY),


        new createjs.Point(((718.3332) * transformX) + moveX, ((85.0000) * transformY) + moveY),
        new createjs.Point(((719.6668) * transformX) + moveX, ((85.0000) * transformY) + moveY),
        new createjs.Point(((721.0000) * transformX) + moveX, ((85.0000) * transformY) + moveY),


        new createjs.Point(((721.0000) * transformX) + moveX, ((85.3333) * transformY) + moveY),
        new createjs.Point(((721.0000) * transformX) + moveX, ((85.6667) * transformY) + moveY),
        new createjs.Point(((721.0000) * transformX) + moveX, ((86.0000) * transformY) + moveY),


        new createjs.Point(((721.9999) * transformX) + moveX, ((86.0000) * transformY) + moveY),
        new createjs.Point(((723.0001) * transformX) + moveX, ((86.0000) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((86.0000) * transformY) + moveY),


        new createjs.Point(((724.0000) * transformX) + moveX, ((86.3333) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((86.6667) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((87.0000) * transformY) + moveY),


        new createjs.Point(((724.9999) * transformX) + moveX, ((87.0000) * transformY) + moveY),
        new createjs.Point(((726.0001) * transformX) + moveX, ((87.0000) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((87.0000) * transformY) + moveY),


        new createjs.Point(((727.0000) * transformX) + moveX, ((87.3333) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((87.6667) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),


        new createjs.Point(((727.9999) * transformX) + moveX, ((88.0000) * transformY) + moveY),
        new createjs.Point(((729.0001) * transformX) + moveX, ((88.0000) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),


        new createjs.Point(((730.0000) * transformX) + moveX, ((88.3333) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((88.6667) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((89.0000) * transformY) + moveY),


        new createjs.Point(((730.9999) * transformX) + moveX, ((89.0000) * transformY) + moveY),
        new createjs.Point(((732.0001) * transformX) + moveX, ((89.0000) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((89.0000) * transformY) + moveY),


        new createjs.Point(((733.0000) * transformX) + moveX, ((89.3333) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((89.6667) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((90.0000) * transformY) + moveY),


        new createjs.Point(((734.9998) * transformX) + moveX, ((90.3333) * transformY) + moveY),
        new createjs.Point(((737.0002) * transformX) + moveX, ((90.6667) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),


        new createjs.Point(((739.0000) * transformX) + moveX, ((91.3333) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((91.6667) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((92.0000) * transformY) + moveY),


        new createjs.Point(((739.6666) * transformX) + moveX, ((92.0000) * transformY) + moveY),
        new createjs.Point(((740.3334) * transformX) + moveX, ((92.0000) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((92.0000) * transformY) + moveY),


        new createjs.Point(((741.0000) * transformX) + moveX, ((92.3333) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((92.6667) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((93.0000) * transformY) + moveY),


        new createjs.Point(((741.9999) * transformX) + moveX, ((93.0000) * transformY) + moveY),
        new createjs.Point(((743.0001) * transformX) + moveX, ((93.0000) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((93.0000) * transformY) + moveY),


        new createjs.Point(((744.0000) * transformX) + moveX, ((93.3333) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((93.6667) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),


        new createjs.Point(((744.6666) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((745.3334) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((746.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),


        new createjs.Point(((746.0000) * transformX) + moveX, ((94.3333) * transformY) + moveY),
        new createjs.Point(((746.0000) * transformX) + moveX, ((94.6667) * transformY) + moveY),
        new createjs.Point(((746.0000) * transformX) + moveX, ((95.0000) * transformY) + moveY),


        new createjs.Point(((746.9999) * transformX) + moveX, ((95.0000) * transformY) + moveY),
        new createjs.Point(((748.0001) * transformX) + moveX, ((95.0000) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((95.0000) * transformY) + moveY),


        new createjs.Point(((749.0000) * transformX) + moveX, ((95.3333) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((95.6667) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((96.0000) * transformY) + moveY),


        new createjs.Point(((750.9998) * transformX) + moveX, ((96.6666) * transformY) + moveY),
        new createjs.Point(((753.0002) * transformX) + moveX, ((97.3334) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((98.0000) * transformY) + moveY),


        new createjs.Point(((755.0000) * transformX) + moveX, ((98.3333) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((98.6667) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((99.0000) * transformY) + moveY),


        new createjs.Point(((755.6666) * transformX) + moveX, ((99.0000) * transformY) + moveY),
        new createjs.Point(((756.3334) * transformX) + moveX, ((99.0000) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((99.0000) * transformY) + moveY),


        new createjs.Point(((757.0000) * transformX) + moveX, ((99.3333) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((99.6667) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((100.0000) * transformY) + moveY),


        new createjs.Point(((757.6666) * transformX) + moveX, ((100.0000) * transformY) + moveY),
        new createjs.Point(((758.3334) * transformX) + moveX, ((100.0000) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((100.0000) * transformY) + moveY),


        new createjs.Point(((759.0000) * transformX) + moveX, ((100.3333) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((100.6667) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),


        new createjs.Point(((759.6666) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((760.3334) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((761.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),


        new createjs.Point(((761.0000) * transformX) + moveX, ((101.3333) * transformY) + moveY),
        new createjs.Point(((761.0000) * transformX) + moveX, ((101.6667) * transformY) + moveY),
        new createjs.Point(((761.0000) * transformX) + moveX, ((102.0000) * transformY) + moveY),


        new createjs.Point(((761.6666) * transformX) + moveX, ((102.0000) * transformY) + moveY),
        new createjs.Point(((762.3334) * transformX) + moveX, ((102.0000) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((102.0000) * transformY) + moveY),


        new createjs.Point(((763.0000) * transformX) + moveX, ((102.3333) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((102.6667) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),


        new createjs.Point(((763.6666) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((764.3334) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),


        new createjs.Point(((765.0000) * transformX) + moveX, ((103.3333) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((103.6667) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((104.0000) * transformY) + moveY),


        new createjs.Point(((765.6666) * transformX) + moveX, ((104.0000) * transformY) + moveY),
        new createjs.Point(((766.3334) * transformX) + moveX, ((104.0000) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((104.0000) * transformY) + moveY),


        new createjs.Point(((767.0000) * transformX) + moveX, ((104.3333) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((104.6667) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((105.0000) * transformY) + moveY),


        new createjs.Point(((767.6666) * transformX) + moveX, ((105.0000) * transformY) + moveY),
        new createjs.Point(((768.3334) * transformX) + moveX, ((105.0000) * transformY) + moveY),
        new createjs.Point(((769.0000) * transformX) + moveX, ((105.0000) * transformY) + moveY),


        new createjs.Point(((769.3333) * transformX) + moveX, ((105.6666) * transformY) + moveY),
        new createjs.Point(((769.6667) * transformX) + moveX, ((106.3334) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((107.0000) * transformY) + moveY),


        new createjs.Point(((771.9998) * transformX) + moveX, ((107.6666) * transformY) + moveY),
        new createjs.Point(((774.0002) * transformX) + moveX, ((108.3334) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),


        new createjs.Point(((776.3333) * transformX) + moveX, ((109.6666) * transformY) + moveY),
        new createjs.Point(((776.6667) * transformX) + moveX, ((110.3334) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),


        new createjs.Point(((777.6666) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((778.3334) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),


        new createjs.Point(((779.3333) * transformX) + moveX, ((111.6666) * transformY) + moveY),
        new createjs.Point(((779.6667) * transformX) + moveX, ((112.3334) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((113.0000) * transformY) + moveY),


        new createjs.Point(((780.6666) * transformX) + moveX, ((113.0000) * transformY) + moveY),
        new createjs.Point(((781.3334) * transformX) + moveX, ((113.0000) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((113.0000) * transformY) + moveY),


        new createjs.Point(((782.3333) * transformX) + moveX, ((113.6666) * transformY) + moveY),
        new createjs.Point(((782.6667) * transformX) + moveX, ((114.3334) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),


        new createjs.Point(((783.6666) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((784.3334) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),


        new createjs.Point(((785.3333) * transformX) + moveX, ((115.6666) * transformY) + moveY),
        new createjs.Point(((785.6667) * transformX) + moveX, ((116.3334) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((117.0000) * transformY) + moveY),


        new createjs.Point(((786.6666) * transformX) + moveX, ((117.0000) * transformY) + moveY),
        new createjs.Point(((787.3334) * transformX) + moveX, ((117.0000) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((117.0000) * transformY) + moveY),


        new createjs.Point(((788.3333) * transformX) + moveX, ((117.6666) * transformY) + moveY),
        new createjs.Point(((788.6667) * transformX) + moveX, ((118.3334) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((119.0000) * transformY) + moveY),


        new createjs.Point(((789.6666) * transformX) + moveX, ((119.0000) * transformY) + moveY),
        new createjs.Point(((790.3334) * transformX) + moveX, ((119.0000) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((119.0000) * transformY) + moveY),


        new createjs.Point(((791.6666) * transformX) + moveX, ((119.9999) * transformY) + moveY),
        new createjs.Point(((792.3334) * transformX) + moveX, ((121.0001) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),


        new createjs.Point(((793.6666) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((794.3334) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((795.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),


        new createjs.Point(((795.6666) * transformX) + moveX, ((122.9999) * transformY) + moveY),
        new createjs.Point(((796.3334) * transformX) + moveX, ((124.0001) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((125.0000) * transformY) + moveY),


        new createjs.Point(((797.6666) * transformX) + moveX, ((125.0000) * transformY) + moveY),
        new createjs.Point(((798.3334) * transformX) + moveX, ((125.0000) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((125.0000) * transformY) + moveY),


        new createjs.Point(((800.3332) * transformX) + moveX, ((126.6665) * transformY) + moveY),
        new createjs.Point(((801.6668) * transformX) + moveX, ((128.3335) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((130.0000) * transformY) + moveY),


        new createjs.Point(((803.6666) * transformX) + moveX, ((130.0000) * transformY) + moveY),
        new createjs.Point(((804.3334) * transformX) + moveX, ((130.0000) * transformY) + moveY),
        new createjs.Point(((805.0000) * transformX) + moveX, ((130.0000) * transformY) + moveY),


        new createjs.Point(((806.6665) * transformX) + moveX, ((131.9998) * transformY) + moveY),
        new createjs.Point(((808.3335) * transformX) + moveX, ((134.0002) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((136.0000) * transformY) + moveY),


        new createjs.Point(((810.6666) * transformX) + moveX, ((136.0000) * transformY) + moveY),
        new createjs.Point(((811.3334) * transformX) + moveX, ((136.0000) * transformY) + moveY),
        new createjs.Point(((812.0000) * transformX) + moveX, ((136.0000) * transformY) + moveY),


        new createjs.Point(((814.9997) * transformX) + moveX, ((139.3330) * transformY) + moveY),
        new createjs.Point(((818.0003) * transformX) + moveX, ((142.6670) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((146.0000) * transformY) + moveY),


        new createjs.Point(((824.3330) * transformX) + moveX, ((148.9997) * transformY) + moveY),
        new createjs.Point(((827.6670) * transformX) + moveX, ((152.0003) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((155.0000) * transformY) + moveY),


        new createjs.Point(((831.0000) * transformX) + moveX, ((155.6666) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((156.3334) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((157.0000) * transformY) + moveY),


        new createjs.Point(((833.3331) * transformX) + moveX, ((158.9998) * transformY) + moveY),
        new createjs.Point(((835.6669) * transformX) + moveX, ((161.0002) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((163.0000) * transformY) + moveY),


        new createjs.Point(((838.0000) * transformX) + moveX, ((163.6666) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((164.3334) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((165.0000) * transformY) + moveY),


        new createjs.Point(((839.3332) * transformX) + moveX, ((165.9999) * transformY) + moveY),
        new createjs.Point(((840.6668) * transformX) + moveX, ((167.0001) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((168.0000) * transformY) + moveY),


        new createjs.Point(((842.0000) * transformX) + moveX, ((168.6666) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((169.3334) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((170.0000) * transformY) + moveY),


        new createjs.Point(((842.9999) * transformX) + moveX, ((170.6666) * transformY) + moveY),
        new createjs.Point(((844.0001) * transformX) + moveX, ((171.3334) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((172.0000) * transformY) + moveY),


        new createjs.Point(((845.0000) * transformX) + moveX, ((172.6666) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((173.3334) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((174.0000) * transformY) + moveY),


        new createjs.Point(((845.9999) * transformX) + moveX, ((174.6666) * transformY) + moveY),
        new createjs.Point(((847.0001) * transformX) + moveX, ((175.3334) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((176.0000) * transformY) + moveY),


        new createjs.Point(((848.0000) * transformX) + moveX, ((176.6666) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((177.3334) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((178.0000) * transformY) + moveY),


        new createjs.Point(((848.9999) * transformX) + moveX, ((178.6666) * transformY) + moveY),
        new createjs.Point(((850.0001) * transformX) + moveX, ((179.3334) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((180.0000) * transformY) + moveY),


        new createjs.Point(((851.0000) * transformX) + moveX, ((180.6666) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((181.3334) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((182.0000) * transformY) + moveY),


        new createjs.Point(((851.6666) * transformX) + moveX, ((182.3333) * transformY) + moveY),
        new createjs.Point(((852.3334) * transformX) + moveX, ((182.6667) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((183.0000) * transformY) + moveY),


        new createjs.Point(((853.0000) * transformX) + moveX, ((183.6666) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((184.3334) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((185.0000) * transformY) + moveY),


        new createjs.Point(((853.9999) * transformX) + moveX, ((185.6666) * transformY) + moveY),
        new createjs.Point(((855.0001) * transformX) + moveX, ((186.3334) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((187.0000) * transformY) + moveY),


        new createjs.Point(((856.0000) * transformX) + moveX, ((187.6666) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((188.3334) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((189.0000) * transformY) + moveY),


        new createjs.Point(((856.6666) * transformX) + moveX, ((189.3333) * transformY) + moveY),
        new createjs.Point(((857.3334) * transformX) + moveX, ((189.6667) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((190.0000) * transformY) + moveY),


        new createjs.Point(((858.3333) * transformX) + moveX, ((191.3332) * transformY) + moveY),
        new createjs.Point(((858.6667) * transformX) + moveX, ((192.6668) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((194.0000) * transformY) + moveY),


        new createjs.Point(((859.3333) * transformX) + moveX, ((194.0000) * transformY) + moveY),
        new createjs.Point(((859.6667) * transformX) + moveX, ((194.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((194.0000) * transformY) + moveY),


        new createjs.Point(((860.3333) * transformX) + moveX, ((194.9999) * transformY) + moveY),
        new createjs.Point(((860.6667) * transformX) + moveX, ((196.0001) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),


        new createjs.Point(((861.6666) * transformX) + moveX, ((197.3333) * transformY) + moveY),
        new createjs.Point(((862.3334) * transformX) + moveX, ((197.6667) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((198.0000) * transformY) + moveY),


        new createjs.Point(((863.3333) * transformX) + moveX, ((199.3332) * transformY) + moveY),
        new createjs.Point(((863.6667) * transformX) + moveX, ((200.6668) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((202.0000) * transformY) + moveY),


        new createjs.Point(((864.6666) * transformX) + moveX, ((202.3333) * transformY) + moveY),
        new createjs.Point(((865.3334) * transformX) + moveX, ((202.6667) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((203.0000) * transformY) + moveY),


        new createjs.Point(((866.6666) * transformX) + moveX, ((204.9998) * transformY) + moveY),
        new createjs.Point(((867.3334) * transformX) + moveX, ((207.0002) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((209.0000) * transformY) + moveY),


        new createjs.Point(((868.6666) * transformX) + moveX, ((209.3333) * transformY) + moveY),
        new createjs.Point(((869.3334) * transformX) + moveX, ((209.6667) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((210.0000) * transformY) + moveY),


        new createjs.Point(((870.0000) * transformX) + moveX, ((210.6666) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((211.3334) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((212.0000) * transformY) + moveY),


        new createjs.Point(((870.3333) * transformX) + moveX, ((212.0000) * transformY) + moveY),
        new createjs.Point(((870.6667) * transformX) + moveX, ((212.0000) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((212.0000) * transformY) + moveY),


        new createjs.Point(((871.0000) * transformX) + moveX, ((212.6666) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((213.3334) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((214.0000) * transformY) + moveY),


        new createjs.Point(((871.3333) * transformX) + moveX, ((214.0000) * transformY) + moveY),
        new createjs.Point(((871.6667) * transformX) + moveX, ((214.0000) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((214.0000) * transformY) + moveY),


        new createjs.Point(((872.0000) * transformX) + moveX, ((214.6666) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((215.3334) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((216.0000) * transformY) + moveY),


        new createjs.Point(((872.3333) * transformX) + moveX, ((216.0000) * transformY) + moveY),
        new createjs.Point(((872.6667) * transformX) + moveX, ((216.0000) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((216.0000) * transformY) + moveY),


        new createjs.Point(((873.0000) * transformX) + moveX, ((216.6666) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((217.3334) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((218.0000) * transformY) + moveY),


        new createjs.Point(((873.3333) * transformX) + moveX, ((218.0000) * transformY) + moveY),
        new createjs.Point(((873.6667) * transformX) + moveX, ((218.0000) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((218.0000) * transformY) + moveY),


        new createjs.Point(((874.0000) * transformX) + moveX, ((218.6666) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((219.3334) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((220.0000) * transformY) + moveY),


        new createjs.Point(((874.3333) * transformX) + moveX, ((220.0000) * transformY) + moveY),
        new createjs.Point(((874.6667) * transformX) + moveX, ((220.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((220.0000) * transformY) + moveY),


        new createjs.Point(((875.0000) * transformX) + moveX, ((220.6666) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((221.3334) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((222.0000) * transformY) + moveY),


        new createjs.Point(((875.3333) * transformX) + moveX, ((222.0000) * transformY) + moveY),
        new createjs.Point(((875.6667) * transformX) + moveX, ((222.0000) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((222.0000) * transformY) + moveY),


        new createjs.Point(((876.0000) * transformX) + moveX, ((222.6666) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((223.3334) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((224.0000) * transformY) + moveY),


        new createjs.Point(((876.3333) * transformX) + moveX, ((224.0000) * transformY) + moveY),
        new createjs.Point(((876.6667) * transformX) + moveX, ((224.0000) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((224.0000) * transformY) + moveY),


        new createjs.Point(((877.0000) * transformX) + moveX, ((224.6666) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((225.3334) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((226.0000) * transformY) + moveY),


        new createjs.Point(((877.3333) * transformX) + moveX, ((226.0000) * transformY) + moveY),
        new createjs.Point(((877.6667) * transformX) + moveX, ((226.0000) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((226.0000) * transformY) + moveY),


        new createjs.Point(((878.0000) * transformX) + moveX, ((226.6666) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((227.3334) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((228.0000) * transformY) + moveY),


        new createjs.Point(((878.3333) * transformX) + moveX, ((228.0000) * transformY) + moveY),
        new createjs.Point(((878.6667) * transformX) + moveX, ((228.0000) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((228.0000) * transformY) + moveY),


        new createjs.Point(((879.0000) * transformX) + moveX, ((228.6666) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((229.3334) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((230.0000) * transformY) + moveY),


        new createjs.Point(((879.3333) * transformX) + moveX, ((230.0000) * transformY) + moveY),
        new createjs.Point(((879.6667) * transformX) + moveX, ((230.0000) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((230.0000) * transformY) + moveY),


        new createjs.Point(((880.6666) * transformX) + moveX, ((231.9998) * transformY) + moveY),
        new createjs.Point(((881.3334) * transformX) + moveX, ((234.0002) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),


        new createjs.Point(((882.3333) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((882.6667) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),


        new createjs.Point(((883.0000) * transformX) + moveX, ((236.9999) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((238.0001) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((239.0000) * transformY) + moveY),


        new createjs.Point(((883.3333) * transformX) + moveX, ((239.0000) * transformY) + moveY),
        new createjs.Point(((883.6667) * transformX) + moveX, ((239.0000) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((239.0000) * transformY) + moveY),


        new createjs.Point(((884.3333) * transformX) + moveX, ((240.3332) * transformY) + moveY),
        new createjs.Point(((884.6667) * transformX) + moveX, ((241.6668) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((243.0000) * transformY) + moveY),


        new createjs.Point(((885.3333) * transformX) + moveX, ((243.0000) * transformY) + moveY),
        new createjs.Point(((885.6667) * transformX) + moveX, ((243.0000) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((243.0000) * transformY) + moveY),


        new createjs.Point(((886.0000) * transformX) + moveX, ((243.9999) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((245.0001) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((246.0000) * transformY) + moveY),


        new createjs.Point(((886.3333) * transformX) + moveX, ((246.0000) * transformY) + moveY),
        new createjs.Point(((886.6667) * transformX) + moveX, ((246.0000) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((246.0000) * transformY) + moveY),


        new createjs.Point(((887.0000) * transformX) + moveX, ((246.6666) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((247.3334) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),


        new createjs.Point(((887.3333) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((887.6667) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),


        new createjs.Point(((888.3333) * transformX) + moveX, ((249.9998) * transformY) + moveY),
        new createjs.Point(((888.6667) * transformX) + moveX, ((252.0002) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((254.0000) * transformY) + moveY),


        new createjs.Point(((889.3333) * transformX) + moveX, ((254.0000) * transformY) + moveY),
        new createjs.Point(((889.6667) * transformX) + moveX, ((254.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((254.0000) * transformY) + moveY),


        new createjs.Point(((890.0000) * transformX) + moveX, ((254.6666) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((255.3334) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((256.0000) * transformY) + moveY),


        new createjs.Point(((890.3333) * transformX) + moveX, ((256.0000) * transformY) + moveY),
        new createjs.Point(((890.6667) * transformX) + moveX, ((256.0000) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((256.0000) * transformY) + moveY),


        new createjs.Point(((894.9996) * transformX) + moveX, ((269.3320) * transformY) + moveY),
        new createjs.Point(((899.0004) * transformX) + moveX, ((282.6680) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((296.0000) * transformY) + moveY),


        new createjs.Point(((905.3331) * transformX) + moveX, ((309.3320) * transformY) + moveY),
        new createjs.Point(((907.6669) * transformX) + moveX, ((322.6680) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((336.0000) * transformY) + moveY),


        new createjs.Point(((910.0000) * transformX) + moveX, ((339.3330) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((342.6670) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((346.0000) * transformY) + moveY),


        new createjs.Point(((910.3333) * transformX) + moveX, ((346.0000) * transformY) + moveY),
        new createjs.Point(((910.6667) * transformX) + moveX, ((346.0000) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((346.0000) * transformY) + moveY),


        new createjs.Point(((911.0000) * transformX) + moveX, ((351.3328) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((356.6672) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((362.0000) * transformY) + moveY),


        new createjs.Point(((911.3333) * transformX) + moveX, ((362.0000) * transformY) + moveY),
        new createjs.Point(((911.6667) * transformX) + moveX, ((362.0000) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((362.0000) * transformY) + moveY),


        new createjs.Point(((913.4429) * transformX) + moveX, ((367.1850) * transformY) + moveY),
        new createjs.Point(((912.0929) * transformX) + moveX, ((390.1226) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((394.0000) * transformY) + moveY),


        new createjs.Point(((911.0000) * transformX) + moveX, ((398.3329) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((402.6671) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((407.0000) * transformY) + moveY),


        new createjs.Point(((910.3334) * transformX) + moveX, ((415.3325) * transformY) + moveY),
        new createjs.Point(((909.6666) * transformX) + moveX, ((423.6675) * transformY) + moveY),
        new createjs.Point(((909.0000) * transformX) + moveX, ((432.0000) * transformY) + moveY),


        new createjs.Point(((907.3168) * transformX) + moveX, ((437.8218) * transformY) + moveY),
        new createjs.Point(((906.6983) * transformX) + moveX, ((446.2907) * transformY) + moveY),
        new createjs.Point(((905.0000) * transformX) + moveX, ((452.0000) * transformY) + moveY),


        new createjs.Point(((905.0000) * transformX) + moveX, ((453.9998) * transformY) + moveY),
        new createjs.Point(((905.0000) * transformX) + moveX, ((456.0002) * transformY) + moveY),
        new createjs.Point(((905.0000) * transformX) + moveX, ((458.0000) * transformY) + moveY),


        new createjs.Point(((904.6667) * transformX) + moveX, ((458.0000) * transformY) + moveY),
        new createjs.Point(((904.3333) * transformX) + moveX, ((458.0000) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((458.0000) * transformY) + moveY),


        new createjs.Point(((904.0000) * transformX) + moveX, ((459.6665) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((461.3335) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((463.0000) * transformY) + moveY),


        new createjs.Point(((903.6667) * transformX) + moveX, ((463.0000) * transformY) + moveY),
        new createjs.Point(((903.3333) * transformX) + moveX, ((463.0000) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((463.0000) * transformY) + moveY),


        new createjs.Point(((903.0000) * transformX) + moveX, ((464.6665) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((466.3335) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((468.0000) * transformY) + moveY),


        new createjs.Point(((902.6667) * transformX) + moveX, ((468.0000) * transformY) + moveY),
        new createjs.Point(((902.3333) * transformX) + moveX, ((468.0000) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((468.0000) * transformY) + moveY),


        new createjs.Point(((902.0000) * transformX) + moveX, ((469.3332) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((470.6668) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((472.0000) * transformY) + moveY),


        new createjs.Point(((901.6667) * transformX) + moveX, ((472.0000) * transformY) + moveY),
        new createjs.Point(((901.3333) * transformX) + moveX, ((472.0000) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((472.0000) * transformY) + moveY),


        new createjs.Point(((901.0000) * transformX) + moveX, ((473.6665) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((475.3335) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((477.0000) * transformY) + moveY),


        new createjs.Point(((900.6667) * transformX) + moveX, ((477.0000) * transformY) + moveY),
        new createjs.Point(((900.3333) * transformX) + moveX, ((477.0000) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((477.0000) * transformY) + moveY),


        new createjs.Point(((899.6667) * transformX) + moveX, ((479.6664) * transformY) + moveY),
        new createjs.Point(((899.3333) * transformX) + moveX, ((482.3336) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((485.0000) * transformY) + moveY),


        new createjs.Point(((898.6667) * transformX) + moveX, ((485.0000) * transformY) + moveY),
        new createjs.Point(((898.3333) * transformX) + moveX, ((485.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((485.0000) * transformY) + moveY),


        new createjs.Point(((897.3334) * transformX) + moveX, ((488.6663) * transformY) + moveY),
        new createjs.Point(((896.6666) * transformX) + moveX, ((492.3337) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((496.0000) * transformY) + moveY),


        new createjs.Point(((895.6667) * transformX) + moveX, ((496.0000) * transformY) + moveY),
        new createjs.Point(((895.3333) * transformX) + moveX, ((496.0000) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((496.0000) * transformY) + moveY),


        new createjs.Point(((895.0000) * transformX) + moveX, ((496.9999) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((498.0001) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),


        new createjs.Point(((894.6667) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((894.3333) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),


        new createjs.Point(((894.0000) * transformX) + moveX, ((499.9999) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((501.0001) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),


        new createjs.Point(((893.6667) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((893.3333) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),


        new createjs.Point(((893.0000) * transformX) + moveX, ((502.9999) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((504.0001) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((505.0000) * transformY) + moveY),


        new createjs.Point(((892.6667) * transformX) + moveX, ((505.0000) * transformY) + moveY),
        new createjs.Point(((892.3333) * transformX) + moveX, ((505.0000) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((505.0000) * transformY) + moveY),


        new createjs.Point(((892.0000) * transformX) + moveX, ((505.9999) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((507.0001) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),


        new createjs.Point(((891.6667) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((891.3333) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),


        new createjs.Point(((891.0000) * transformX) + moveX, ((508.9999) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((510.0001) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((511.0000) * transformY) + moveY),


        new createjs.Point(((890.6667) * transformX) + moveX, ((511.0000) * transformY) + moveY),
        new createjs.Point(((890.3333) * transformX) + moveX, ((511.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((511.0000) * transformY) + moveY),


        new createjs.Point(((889.3334) * transformX) + moveX, ((513.9997) * transformY) + moveY),
        new createjs.Point(((888.6666) * transformX) + moveX, ((517.0003) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((520.0000) * transformY) + moveY),


        new createjs.Point(((887.6667) * transformX) + moveX, ((520.0000) * transformY) + moveY),
        new createjs.Point(((887.3333) * transformX) + moveX, ((520.0000) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((520.0000) * transformY) + moveY),


        new createjs.Point(((887.0000) * transformX) + moveX, ((520.9999) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((522.0001) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((523.0000) * transformY) + moveY),


        new createjs.Point(((886.6667) * transformX) + moveX, ((523.0000) * transformY) + moveY),
        new createjs.Point(((886.3333) * transformX) + moveX, ((523.0000) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((523.0000) * transformY) + moveY),


        new createjs.Point(((886.0000) * transformX) + moveX, ((523.6666) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((524.3334) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((525.0000) * transformY) + moveY),


        new createjs.Point(((885.6667) * transformX) + moveX, ((525.0000) * transformY) + moveY),
        new createjs.Point(((885.3333) * transformX) + moveX, ((525.0000) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((525.0000) * transformY) + moveY),


        new createjs.Point(((884.6667) * transformX) + moveX, ((526.9998) * transformY) + moveY),
        new createjs.Point(((884.3333) * transformX) + moveX, ((529.0002) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((531.0000) * transformY) + moveY),


        new createjs.Point(((883.6667) * transformX) + moveX, ((531.0000) * transformY) + moveY),
        new createjs.Point(((883.3333) * transformX) + moveX, ((531.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((531.0000) * transformY) + moveY),


        new createjs.Point(((882.6667) * transformX) + moveX, ((532.3332) * transformY) + moveY),
        new createjs.Point(((882.3333) * transformX) + moveX, ((533.6668) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((535.0000) * transformY) + moveY),


        new createjs.Point(((881.6667) * transformX) + moveX, ((535.0000) * transformY) + moveY),
        new createjs.Point(((881.3333) * transformX) + moveX, ((535.0000) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((535.0000) * transformY) + moveY),


        new createjs.Point(((881.0000) * transformX) + moveX, ((535.9999) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((537.0001) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((538.0000) * transformY) + moveY),


        new createjs.Point(((880.6667) * transformX) + moveX, ((538.0000) * transformY) + moveY),
        new createjs.Point(((880.3333) * transformX) + moveX, ((538.0000) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((538.0000) * transformY) + moveY),


        new createjs.Point(((880.0000) * transformX) + moveX, ((538.6666) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((539.3334) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((540.0000) * transformY) + moveY),


        new createjs.Point(((879.6667) * transformX) + moveX, ((540.0000) * transformY) + moveY),
        new createjs.Point(((879.3333) * transformX) + moveX, ((540.0000) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((540.0000) * transformY) + moveY),


        new createjs.Point(((879.0000) * transformX) + moveX, ((540.9999) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((542.0001) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((543.0000) * transformY) + moveY),


        new createjs.Point(((878.6667) * transformX) + moveX, ((543.0000) * transformY) + moveY),
        new createjs.Point(((878.3333) * transformX) + moveX, ((543.0000) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((543.0000) * transformY) + moveY),


        new createjs.Point(((877.3334) * transformX) + moveX, ((544.9998) * transformY) + moveY),
        new createjs.Point(((876.6666) * transformX) + moveX, ((547.0002) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((549.0000) * transformY) + moveY),


        new createjs.Point(((875.6667) * transformX) + moveX, ((549.0000) * transformY) + moveY),
        new createjs.Point(((875.3333) * transformX) + moveX, ((549.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((549.0000) * transformY) + moveY),


        new createjs.Point(((875.0000) * transformX) + moveX, ((549.6666) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((550.3334) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((551.0000) * transformY) + moveY),


        new createjs.Point(((874.6667) * transformX) + moveX, ((551.0000) * transformY) + moveY),
        new createjs.Point(((874.3333) * transformX) + moveX, ((551.0000) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((551.0000) * transformY) + moveY),


        new createjs.Point(((874.0000) * transformX) + moveX, ((551.6666) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((552.3334) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((553.0000) * transformY) + moveY),


        new createjs.Point(((873.6667) * transformX) + moveX, ((553.0000) * transformY) + moveY),
        new createjs.Point(((873.3333) * transformX) + moveX, ((553.0000) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((553.0000) * transformY) + moveY),


        new createjs.Point(((873.0000) * transformX) + moveX, ((553.9999) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((555.0001) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((556.0000) * transformY) + moveY),


        new createjs.Point(((872.6667) * transformX) + moveX, ((556.0000) * transformY) + moveY),
        new createjs.Point(((872.3333) * transformX) + moveX, ((556.0000) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((556.0000) * transformY) + moveY),


        new createjs.Point(((871.3334) * transformX) + moveX, ((557.9998) * transformY) + moveY),
        new createjs.Point(((870.6666) * transformX) + moveX, ((560.0002) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((562.0000) * transformY) + moveY),


        new createjs.Point(((869.6667) * transformX) + moveX, ((562.0000) * transformY) + moveY),
        new createjs.Point(((869.3333) * transformX) + moveX, ((562.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((562.0000) * transformY) + moveY),


        new createjs.Point(((868.6667) * transformX) + moveX, ((563.3332) * transformY) + moveY),
        new createjs.Point(((868.3333) * transformX) + moveX, ((564.6668) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((566.0000) * transformY) + moveY),


        new createjs.Point(((867.3334) * transformX) + moveX, ((566.3333) * transformY) + moveY),
        new createjs.Point(((866.6666) * transformX) + moveX, ((566.6667) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((567.0000) * transformY) + moveY),


        new createjs.Point(((866.0000) * transformX) + moveX, ((567.6666) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((568.3334) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((569.0000) * transformY) + moveY),


        new createjs.Point(((865.6667) * transformX) + moveX, ((569.0000) * transformY) + moveY),
        new createjs.Point(((865.3333) * transformX) + moveX, ((569.0000) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((569.0000) * transformY) + moveY),


        new createjs.Point(((865.0000) * transformX) + moveX, ((569.6666) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((570.3334) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((571.0000) * transformY) + moveY),


        new createjs.Point(((864.6667) * transformX) + moveX, ((571.0000) * transformY) + moveY),
        new createjs.Point(((864.3333) * transformX) + moveX, ((571.0000) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((571.0000) * transformY) + moveY),


        new createjs.Point(((864.0000) * transformX) + moveX, ((571.6666) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((572.3334) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((573.0000) * transformY) + moveY),


        new createjs.Point(((863.6667) * transformX) + moveX, ((573.0000) * transformY) + moveY),
        new createjs.Point(((863.3333) * transformX) + moveX, ((573.0000) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((573.0000) * transformY) + moveY),


        new createjs.Point(((863.0000) * transformX) + moveX, ((573.6666) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((574.3334) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((575.0000) * transformY) + moveY),


        new createjs.Point(((862.6667) * transformX) + moveX, ((575.0000) * transformY) + moveY),
        new createjs.Point(((862.3333) * transformX) + moveX, ((575.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((575.0000) * transformY) + moveY),


        new createjs.Point(((862.0000) * transformX) + moveX, ((575.6666) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((576.3334) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((577.0000) * transformY) + moveY),


        new createjs.Point(((861.3334) * transformX) + moveX, ((577.3333) * transformY) + moveY),
        new createjs.Point(((860.6666) * transformX) + moveX, ((577.6667) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((578.0000) * transformY) + moveY),


        new createjs.Point(((859.3334) * transformX) + moveX, ((579.9998) * transformY) + moveY),
        new createjs.Point(((858.6666) * transformX) + moveX, ((582.0002) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((584.0000) * transformY) + moveY),


        new createjs.Point(((857.3334) * transformX) + moveX, ((584.3333) * transformY) + moveY),
        new createjs.Point(((856.6666) * transformX) + moveX, ((584.6667) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((585.0000) * transformY) + moveY),


        new createjs.Point(((855.6667) * transformX) + moveX, ((586.3332) * transformY) + moveY),
        new createjs.Point(((855.3333) * transformX) + moveX, ((587.6668) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((589.0000) * transformY) + moveY),


        new createjs.Point(((854.3334) * transformX) + moveX, ((589.3333) * transformY) + moveY),
        new createjs.Point(((853.6666) * transformX) + moveX, ((589.6667) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((590.0000) * transformY) + moveY),


        new createjs.Point(((853.0000) * transformX) + moveX, ((590.6666) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((591.3334) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((592.0000) * transformY) + moveY),


        new createjs.Point(((852.6667) * transformX) + moveX, ((592.0000) * transformY) + moveY),
        new createjs.Point(((852.3333) * transformX) + moveX, ((592.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((592.0000) * transformY) + moveY),


        new createjs.Point(((852.0000) * transformX) + moveX, ((592.6666) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((593.3334) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((594.0000) * transformY) + moveY),


        new createjs.Point(((851.3334) * transformX) + moveX, ((594.3333) * transformY) + moveY),
        new createjs.Point(((850.6666) * transformX) + moveX, ((594.6667) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((595.0000) * transformY) + moveY),


        new createjs.Point(((850.0000) * transformX) + moveX, ((595.6666) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((596.3334) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((597.0000) * transformY) + moveY),


        new createjs.Point(((849.3334) * transformX) + moveX, ((597.3333) * transformY) + moveY),
        new createjs.Point(((848.6666) * transformX) + moveX, ((597.6667) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((598.0000) * transformY) + moveY),


        new createjs.Point(((848.0000) * transformX) + moveX, ((598.6666) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((599.3334) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((600.0000) * transformY) + moveY),


        new createjs.Point(((847.3334) * transformX) + moveX, ((600.3333) * transformY) + moveY),
        new createjs.Point(((846.6666) * transformX) + moveX, ((600.6667) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((601.0000) * transformY) + moveY),


        new createjs.Point(((846.0000) * transformX) + moveX, ((601.6666) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((602.3334) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((603.0000) * transformY) + moveY),


        new createjs.Point(((845.3334) * transformX) + moveX, ((603.3333) * transformY) + moveY),
        new createjs.Point(((844.6666) * transformX) + moveX, ((603.6667) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((604.0000) * transformY) + moveY),


        new createjs.Point(((844.0000) * transformX) + moveX, ((604.6666) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((605.3334) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((606.0000) * transformY) + moveY),


        new createjs.Point(((843.3334) * transformX) + moveX, ((606.3333) * transformY) + moveY),
        new createjs.Point(((842.6666) * transformX) + moveX, ((606.6667) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((607.0000) * transformY) + moveY),


        new createjs.Point(((842.0000) * transformX) + moveX, ((607.6666) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((608.3334) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((609.0000) * transformY) + moveY),


        new createjs.Point(((841.6667) * transformX) + moveX, ((609.0000) * transformY) + moveY),
        new createjs.Point(((841.3333) * transformX) + moveX, ((609.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((609.0000) * transformY) + moveY),


        new createjs.Point(((840.6667) * transformX) + moveX, ((609.9999) * transformY) + moveY),
        new createjs.Point(((840.3333) * transformX) + moveX, ((611.0001) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((612.0000) * transformY) + moveY),


        new createjs.Point(((839.3334) * transformX) + moveX, ((612.3333) * transformY) + moveY),
        new createjs.Point(((838.6666) * transformX) + moveX, ((612.6667) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((613.0000) * transformY) + moveY),


        new createjs.Point(((838.0000) * transformX) + moveX, ((613.6666) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((614.3334) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((615.0000) * transformY) + moveY),


        new createjs.Point(((837.0001) * transformX) + moveX, ((615.6666) * transformY) + moveY),
        new createjs.Point(((835.9999) * transformX) + moveX, ((616.3334) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((617.0000) * transformY) + moveY),


        new createjs.Point(((835.0000) * transformX) + moveX, ((617.6666) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((618.3334) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((619.0000) * transformY) + moveY),


        new createjs.Point(((834.0001) * transformX) + moveX, ((619.6666) * transformY) + moveY),
        new createjs.Point(((832.9999) * transformX) + moveX, ((620.3334) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),


        new createjs.Point(((832.0000) * transformX) + moveX, ((621.6666) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((622.3334) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((623.0000) * transformY) + moveY),


        new createjs.Point(((831.0001) * transformX) + moveX, ((623.6666) * transformY) + moveY),
        new createjs.Point(((829.9999) * transformX) + moveX, ((624.3334) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((625.0000) * transformY) + moveY),


        new createjs.Point(((829.0000) * transformX) + moveX, ((625.6666) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((626.3334) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((627.0000) * transformY) + moveY),


        new createjs.Point(((828.0001) * transformX) + moveX, ((627.6666) * transformY) + moveY),
        new createjs.Point(((826.9999) * transformX) + moveX, ((628.3334) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((629.0000) * transformY) + moveY),


        new createjs.Point(((826.0000) * transformX) + moveX, ((629.6666) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((630.3334) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((631.0000) * transformY) + moveY),


        new createjs.Point(((825.3334) * transformX) + moveX, ((631.3333) * transformY) + moveY),
        new createjs.Point(((824.6666) * transformX) + moveX, ((631.6667) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((632.0000) * transformY) + moveY),


        new createjs.Point(((824.0000) * transformX) + moveX, ((632.3333) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((632.6667) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((633.0000) * transformY) + moveY),


        new createjs.Point(((823.3334) * transformX) + moveX, ((633.3333) * transformY) + moveY),
        new createjs.Point(((822.6666) * transformX) + moveX, ((633.6667) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((634.0000) * transformY) + moveY),


        new createjs.Point(((822.0000) * transformX) + moveX, ((634.6666) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((635.3334) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((636.0000) * transformY) + moveY),


        new createjs.Point(((820.6668) * transformX) + moveX, ((636.9999) * transformY) + moveY),
        new createjs.Point(((819.3332) * transformX) + moveX, ((638.0001) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((639.0000) * transformY) + moveY),


        new createjs.Point(((818.0000) * transformX) + moveX, ((639.6666) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((640.3334) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((641.0000) * transformY) + moveY),


        new createjs.Point(((816.3335) * transformX) + moveX, ((642.3332) * transformY) + moveY),
        new createjs.Point(((814.6665) * transformX) + moveX, ((643.6668) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((645.0000) * transformY) + moveY),


        new createjs.Point(((813.0000) * transformX) + moveX, ((645.6666) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((646.3334) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((647.0000) * transformY) + moveY),


        new createjs.Point(((811.0002) * transformX) + moveX, ((648.6665) * transformY) + moveY),
        new createjs.Point(((808.9998) * transformX) + moveX, ((650.3335) * transformY) + moveY),
        new createjs.Point(((807.0000) * transformX) + moveX, ((652.0000) * transformY) + moveY),


        new createjs.Point(((807.0000) * transformX) + moveX, ((652.6666) * transformY) + moveY),
        new createjs.Point(((807.0000) * transformX) + moveX, ((653.3334) * transformY) + moveY),
        new createjs.Point(((807.0000) * transformX) + moveX, ((654.0000) * transformY) + moveY),


        new createjs.Point(((803.3337) * transformX) + moveX, ((657.3330) * transformY) + moveY),
        new createjs.Point(((799.6663) * transformX) + moveX, ((660.6670) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((664.0000) * transformY) + moveY),


        new createjs.Point(((796.0000) * transformX) + moveX, ((664.6666) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((665.3334) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((666.0000) * transformY) + moveY),


        new createjs.Point(((793.3336) * transformX) + moveX, ((668.3331) * transformY) + moveY),
        new createjs.Point(((790.6664) * transformX) + moveX, ((670.6669) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((673.0000) * transformY) + moveY),


        new createjs.Point(((782.3339) * transformX) + moveX, ((678.9994) * transformY) + moveY),
        new createjs.Point(((776.6661) * transformX) + moveX, ((685.0006) * transformY) + moveY),
        new createjs.Point(((771.0000) * transformX) + moveX, ((691.0000) * transformY) + moveY),


        new createjs.Point(((770.3334) * transformX) + moveX, ((691.0000) * transformY) + moveY),
        new createjs.Point(((769.6666) * transformX) + moveX, ((691.0000) * transformY) + moveY),
        new createjs.Point(((769.0000) * transformX) + moveX, ((691.0000) * transformY) + moveY),


        new createjs.Point(((765.6670) * transformX) + moveX, ((694.6663) * transformY) + moveY),
        new createjs.Point(((762.3330) * transformX) + moveX, ((698.3337) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((702.0000) * transformY) + moveY),


        new createjs.Point(((758.3334) * transformX) + moveX, ((702.0000) * transformY) + moveY),
        new createjs.Point(((757.6666) * transformX) + moveX, ((702.0000) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((702.0000) * transformY) + moveY),


        new createjs.Point(((755.0002) * transformX) + moveX, ((704.3331) * transformY) + moveY),
        new createjs.Point(((752.9998) * transformX) + moveX, ((706.6669) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((709.0000) * transformY) + moveY),


        new createjs.Point(((750.3334) * transformX) + moveX, ((709.0000) * transformY) + moveY),
        new createjs.Point(((749.6666) * transformX) + moveX, ((709.0000) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((709.0000) * transformY) + moveY),


        new createjs.Point(((747.3335) * transformX) + moveX, ((710.9998) * transformY) + moveY),
        new createjs.Point(((745.6665) * transformX) + moveX, ((713.0002) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((715.0000) * transformY) + moveY),


        new createjs.Point(((743.3334) * transformX) + moveX, ((715.0000) * transformY) + moveY),
        new createjs.Point(((742.6666) * transformX) + moveX, ((715.0000) * transformY) + moveY),
        new createjs.Point(((742.0000) * transformX) + moveX, ((715.0000) * transformY) + moveY),


        new createjs.Point(((740.6668) * transformX) + moveX, ((716.6665) * transformY) + moveY),
        new createjs.Point(((739.3332) * transformX) + moveX, ((718.3335) * transformY) + moveY),
        new createjs.Point(((738.0000) * transformX) + moveX, ((720.0000) * transformY) + moveY),


        new createjs.Point(((737.3334) * transformX) + moveX, ((720.0000) * transformY) + moveY),
        new createjs.Point(((736.6666) * transformX) + moveX, ((720.0000) * transformY) + moveY),
        new createjs.Point(((736.0000) * transformX) + moveX, ((720.0000) * transformY) + moveY),


        new createjs.Point(((735.0001) * transformX) + moveX, ((721.3332) * transformY) + moveY),
        new createjs.Point(((733.9999) * transformX) + moveX, ((722.6668) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((724.0000) * transformY) + moveY),


        new createjs.Point(((732.3334) * transformX) + moveX, ((724.0000) * transformY) + moveY),
        new createjs.Point(((731.6666) * transformX) + moveX, ((724.0000) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((724.0000) * transformY) + moveY),


        new createjs.Point(((730.0001) * transformX) + moveX, ((725.3332) * transformY) + moveY),
        new createjs.Point(((728.9999) * transformX) + moveX, ((726.6668) * transformY) + moveY),
        new createjs.Point(((728.0000) * transformX) + moveX, ((728.0000) * transformY) + moveY),


        new createjs.Point(((727.3334) * transformX) + moveX, ((728.0000) * transformY) + moveY),
        new createjs.Point(((726.6666) * transformX) + moveX, ((728.0000) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((728.0000) * transformY) + moveY),


        new createjs.Point(((725.0001) * transformX) + moveX, ((729.3332) * transformY) + moveY),
        new createjs.Point(((723.9999) * transformX) + moveX, ((730.6668) * transformY) + moveY),
        new createjs.Point(((723.0000) * transformX) + moveX, ((732.0000) * transformY) + moveY),


        new createjs.Point(((722.3334) * transformX) + moveX, ((732.0000) * transformY) + moveY),
        new createjs.Point(((721.6666) * transformX) + moveX, ((732.0000) * transformY) + moveY),
        new createjs.Point(((721.0000) * transformX) + moveX, ((732.0000) * transformY) + moveY),


        new createjs.Point(((720.6667) * transformX) + moveX, ((732.6666) * transformY) + moveY),
        new createjs.Point(((720.3333) * transformX) + moveX, ((733.3334) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((734.0000) * transformY) + moveY),


        new createjs.Point(((719.6667) * transformX) + moveX, ((734.0000) * transformY) + moveY),
        new createjs.Point(((719.3333) * transformX) + moveX, ((734.0000) * transformY) + moveY),
        new createjs.Point(((719.0000) * transformX) + moveX, ((734.0000) * transformY) + moveY),


        new createjs.Point(((718.6667) * transformX) + moveX, ((734.6666) * transformY) + moveY),
        new createjs.Point(((718.3333) * transformX) + moveX, ((735.3334) * transformY) + moveY),
        new createjs.Point(((718.0000) * transformX) + moveX, ((736.0000) * transformY) + moveY),


        new createjs.Point(((717.3334) * transformX) + moveX, ((736.0000) * transformY) + moveY),
        new createjs.Point(((716.6666) * transformX) + moveX, ((736.0000) * transformY) + moveY),
        new createjs.Point(((716.0000) * transformX) + moveX, ((736.0000) * transformY) + moveY),


        new createjs.Point(((715.3334) * transformX) + moveX, ((736.9999) * transformY) + moveY),
        new createjs.Point(((714.6666) * transformX) + moveX, ((738.0001) * transformY) + moveY),
        new createjs.Point(((714.0000) * transformX) + moveX, ((739.0000) * transformY) + moveY),


        new createjs.Point(((713.3334) * transformX) + moveX, ((739.0000) * transformY) + moveY),
        new createjs.Point(((712.6666) * transformX) + moveX, ((739.0000) * transformY) + moveY),
        new createjs.Point(((712.0000) * transformX) + moveX, ((739.0000) * transformY) + moveY),


        new createjs.Point(((711.3334) * transformX) + moveX, ((739.9999) * transformY) + moveY),
        new createjs.Point(((710.6666) * transformX) + moveX, ((741.0001) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((742.0000) * transformY) + moveY),


        new createjs.Point(((709.3334) * transformX) + moveX, ((742.0000) * transformY) + moveY),
        new createjs.Point(((708.6666) * transformX) + moveX, ((742.0000) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((742.0000) * transformY) + moveY),


        new createjs.Point(((707.3334) * transformX) + moveX, ((742.9999) * transformY) + moveY),
        new createjs.Point(((706.6666) * transformX) + moveX, ((744.0001) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((745.0000) * transformY) + moveY),


        new createjs.Point(((705.3334) * transformX) + moveX, ((745.0000) * transformY) + moveY),
        new createjs.Point(((704.6666) * transformX) + moveX, ((745.0000) * transformY) + moveY),
        new createjs.Point(((704.0000) * transformX) + moveX, ((745.0000) * transformY) + moveY),


        new createjs.Point(((703.3334) * transformX) + moveX, ((745.9999) * transformY) + moveY),
        new createjs.Point(((702.6666) * transformX) + moveX, ((747.0001) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((748.0000) * transformY) + moveY),


        new createjs.Point(((701.3334) * transformX) + moveX, ((748.0000) * transformY) + moveY),
        new createjs.Point(((700.6666) * transformX) + moveX, ((748.0000) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((748.0000) * transformY) + moveY),


        new createjs.Point(((699.3334) * transformX) + moveX, ((748.9999) * transformY) + moveY),
        new createjs.Point(((698.6666) * transformX) + moveX, ((750.0001) * transformY) + moveY),
        new createjs.Point(((698.0000) * transformX) + moveX, ((751.0000) * transformY) + moveY),


        new createjs.Point(((697.0001) * transformX) + moveX, ((751.3333) * transformY) + moveY),
        new createjs.Point(((695.9999) * transformX) + moveX, ((751.6667) * transformY) + moveY),
        new createjs.Point(((695.0000) * transformX) + moveX, ((752.0000) * transformY) + moveY),


        new createjs.Point(((694.6667) * transformX) + moveX, ((752.6666) * transformY) + moveY),
        new createjs.Point(((694.3333) * transformX) + moveX, ((753.3334) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((754.0000) * transformY) + moveY),


        new createjs.Point(((693.3334) * transformX) + moveX, ((754.0000) * transformY) + moveY),
        new createjs.Point(((692.6666) * transformX) + moveX, ((754.0000) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((754.0000) * transformY) + moveY),


        new createjs.Point(((691.6667) * transformX) + moveX, ((754.6666) * transformY) + moveY),
        new createjs.Point(((691.3333) * transformX) + moveX, ((755.3334) * transformY) + moveY),
        new createjs.Point(((691.0000) * transformX) + moveX, ((756.0000) * transformY) + moveY),


        new createjs.Point(((690.3334) * transformX) + moveX, ((756.0000) * transformY) + moveY),
        new createjs.Point(((689.6666) * transformX) + moveX, ((756.0000) * transformY) + moveY),
        new createjs.Point(((689.0000) * transformX) + moveX, ((756.0000) * transformY) + moveY),


        new createjs.Point(((688.3334) * transformX) + moveX, ((756.9999) * transformY) + moveY),
        new createjs.Point(((687.6666) * transformX) + moveX, ((758.0001) * transformY) + moveY),
        new createjs.Point(((687.0000) * transformX) + moveX, ((759.0000) * transformY) + moveY),


        new createjs.Point(((686.3334) * transformX) + moveX, ((759.0000) * transformY) + moveY),
        new createjs.Point(((685.6666) * transformX) + moveX, ((759.0000) * transformY) + moveY),
        new createjs.Point(((685.0000) * transformX) + moveX, ((759.0000) * transformY) + moveY),


        new createjs.Point(((684.6667) * transformX) + moveX, ((759.6666) * transformY) + moveY),
        new createjs.Point(((684.3333) * transformX) + moveX, ((760.3334) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((761.0000) * transformY) + moveY),


        new createjs.Point(((683.3334) * transformX) + moveX, ((761.0000) * transformY) + moveY),
        new createjs.Point(((682.6666) * transformX) + moveX, ((761.0000) * transformY) + moveY),
        new createjs.Point(((682.0000) * transformX) + moveX, ((761.0000) * transformY) + moveY),


        new createjs.Point(((681.3334) * transformX) + moveX, ((761.9999) * transformY) + moveY),
        new createjs.Point(((680.6666) * transformX) + moveX, ((763.0001) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((764.0000) * transformY) + moveY),


        new createjs.Point(((679.3334) * transformX) + moveX, ((764.0000) * transformY) + moveY),
        new createjs.Point(((678.6666) * transformX) + moveX, ((764.0000) * transformY) + moveY),
        new createjs.Point(((678.0000) * transformX) + moveX, ((764.0000) * transformY) + moveY),


        new createjs.Point(((677.6667) * transformX) + moveX, ((764.6666) * transformY) + moveY),
        new createjs.Point(((677.3333) * transformX) + moveX, ((765.3334) * transformY) + moveY),
        new createjs.Point(((677.0000) * transformX) + moveX, ((766.0000) * transformY) + moveY),


        new createjs.Point(((676.3334) * transformX) + moveX, ((766.0000) * transformY) + moveY),
        new createjs.Point(((675.6666) * transformX) + moveX, ((766.0000) * transformY) + moveY),
        new createjs.Point(((675.0000) * transformX) + moveX, ((766.0000) * transformY) + moveY),


        new createjs.Point(((675.0000) * transformX) + moveX, ((766.3333) * transformY) + moveY),
        new createjs.Point(((675.0000) * transformX) + moveX, ((766.6667) * transformY) + moveY),
        new createjs.Point(((675.0000) * transformX) + moveX, ((767.0000) * transformY) + moveY),


        new createjs.Point(((674.0001) * transformX) + moveX, ((767.3333) * transformY) + moveY),
        new createjs.Point(((672.9999) * transformX) + moveX, ((767.6667) * transformY) + moveY),
        new createjs.Point(((672.0000) * transformX) + moveX, ((768.0000) * transformY) + moveY),


        new createjs.Point(((671.6667) * transformX) + moveX, ((768.6666) * transformY) + moveY),
        new createjs.Point(((671.3333) * transformX) + moveX, ((769.3334) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((770.0000) * transformY) + moveY),


        new createjs.Point(((670.3334) * transformX) + moveX, ((770.0000) * transformY) + moveY),
        new createjs.Point(((669.6666) * transformX) + moveX, ((770.0000) * transformY) + moveY),
        new createjs.Point(((669.0000) * transformX) + moveX, ((770.0000) * transformY) + moveY),


        new createjs.Point(((668.3334) * transformX) + moveX, ((770.9999) * transformY) + moveY),
        new createjs.Point(((667.6666) * transformX) + moveX, ((772.0001) * transformY) + moveY),
        new createjs.Point(((667.0000) * transformX) + moveX, ((773.0000) * transformY) + moveY),


        new createjs.Point(((666.3334) * transformX) + moveX, ((773.0000) * transformY) + moveY),
        new createjs.Point(((665.6666) * transformX) + moveX, ((773.0000) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((773.0000) * transformY) + moveY),


        new createjs.Point(((664.6667) * transformX) + moveX, ((773.6666) * transformY) + moveY),
        new createjs.Point(((664.3333) * transformX) + moveX, ((774.3334) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),


        new createjs.Point(((663.0001) * transformX) + moveX, ((775.3333) * transformY) + moveY),
        new createjs.Point(((661.9999) * transformX) + moveX, ((775.6667) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((776.0000) * transformY) + moveY),


        new createjs.Point(((661.0000) * transformX) + moveX, ((776.3333) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((776.6667) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),


        new createjs.Point(((660.3334) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((659.6666) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((659.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),


        new createjs.Point(((658.6667) * transformX) + moveX, ((777.6666) * transformY) + moveY),
        new createjs.Point(((658.3333) * transformX) + moveX, ((778.3334) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((779.0000) * transformY) + moveY),


        new createjs.Point(((657.3334) * transformX) + moveX, ((779.0000) * transformY) + moveY),
        new createjs.Point(((656.6666) * transformX) + moveX, ((779.0000) * transformY) + moveY),
        new createjs.Point(((656.0000) * transformX) + moveX, ((779.0000) * transformY) + moveY),


        new createjs.Point(((655.6667) * transformX) + moveX, ((779.6666) * transformY) + moveY),
        new createjs.Point(((655.3333) * transformX) + moveX, ((780.3334) * transformY) + moveY),
        new createjs.Point(((655.0000) * transformX) + moveX, ((781.0000) * transformY) + moveY),


        new createjs.Point(((654.3334) * transformX) + moveX, ((781.0000) * transformY) + moveY),
        new createjs.Point(((653.6666) * transformX) + moveX, ((781.0000) * transformY) + moveY),
        new createjs.Point(((653.0000) * transformX) + moveX, ((781.0000) * transformY) + moveY),


        new createjs.Point(((652.6667) * transformX) + moveX, ((781.6666) * transformY) + moveY),
        new createjs.Point(((652.3333) * transformX) + moveX, ((782.3334) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((783.0000) * transformY) + moveY),


        new createjs.Point(((651.3334) * transformX) + moveX, ((783.0000) * transformY) + moveY),
        new createjs.Point(((650.6666) * transformX) + moveX, ((783.0000) * transformY) + moveY),
        new createjs.Point(((650.0000) * transformX) + moveX, ((783.0000) * transformY) + moveY),


        new createjs.Point(((649.6667) * transformX) + moveX, ((783.6666) * transformY) + moveY),
        new createjs.Point(((649.3333) * transformX) + moveX, ((784.3334) * transformY) + moveY),
        new createjs.Point(((649.0000) * transformX) + moveX, ((785.0000) * transformY) + moveY),


        new createjs.Point(((648.3334) * transformX) + moveX, ((785.0000) * transformY) + moveY),
        new createjs.Point(((647.6666) * transformX) + moveX, ((785.0000) * transformY) + moveY),
        new createjs.Point(((647.0000) * transformX) + moveX, ((785.0000) * transformY) + moveY),


        new createjs.Point(((646.6667) * transformX) + moveX, ((785.6666) * transformY) + moveY),
        new createjs.Point(((646.3333) * transformX) + moveX, ((786.3334) * transformY) + moveY),
        new createjs.Point(((646.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),


        new createjs.Point(((645.3334) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((644.6666) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((644.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),


        new createjs.Point(((643.6667) * transformX) + moveX, ((787.6666) * transformY) + moveY),
        new createjs.Point(((643.3333) * transformX) + moveX, ((788.3334) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((789.0000) * transformY) + moveY),


        new createjs.Point(((641.6668) * transformX) + moveX, ((789.3333) * transformY) + moveY),
        new createjs.Point(((640.3332) * transformX) + moveX, ((789.6667) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((790.0000) * transformY) + moveY),


        new createjs.Point(((638.6667) * transformX) + moveX, ((790.6666) * transformY) + moveY),
        new createjs.Point(((638.3333) * transformX) + moveX, ((791.3334) * transformY) + moveY),
        new createjs.Point(((638.0000) * transformX) + moveX, ((792.0000) * transformY) + moveY),


        new createjs.Point(((637.3334) * transformX) + moveX, ((792.0000) * transformY) + moveY),
        new createjs.Point(((636.6666) * transformX) + moveX, ((792.0000) * transformY) + moveY),
        new createjs.Point(((636.0000) * transformX) + moveX, ((792.0000) * transformY) + moveY),


        new createjs.Point(((635.6667) * transformX) + moveX, ((792.6666) * transformY) + moveY),
        new createjs.Point(((635.3333) * transformX) + moveX, ((793.3334) * transformY) + moveY),
        new createjs.Point(((635.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),


        new createjs.Point(((634.3334) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((633.6666) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((633.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),


        new createjs.Point(((632.6667) * transformX) + moveX, ((794.6666) * transformY) + moveY),
        new createjs.Point(((632.3333) * transformX) + moveX, ((795.3334) * transformY) + moveY),
        new createjs.Point(((632.0000) * transformX) + moveX, ((796.0000) * transformY) + moveY),


        new createjs.Point(((631.3334) * transformX) + moveX, ((796.0000) * transformY) + moveY),
        new createjs.Point(((630.6666) * transformX) + moveX, ((796.0000) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((796.0000) * transformY) + moveY),


        new createjs.Point(((629.6667) * transformX) + moveX, ((796.6666) * transformY) + moveY),
        new createjs.Point(((629.3333) * transformX) + moveX, ((797.3334) * transformY) + moveY),
        new createjs.Point(((629.0000) * transformX) + moveX, ((798.0000) * transformY) + moveY),


        new createjs.Point(((627.6668) * transformX) + moveX, ((798.3333) * transformY) + moveY),
        new createjs.Point(((626.3332) * transformX) + moveX, ((798.6667) * transformY) + moveY),
        new createjs.Point(((625.0000) * transformX) + moveX, ((799.0000) * transformY) + moveY),


        new createjs.Point(((624.6667) * transformX) + moveX, ((799.6666) * transformY) + moveY),
        new createjs.Point(((624.3333) * transformX) + moveX, ((800.3334) * transformY) + moveY),
        new createjs.Point(((624.0000) * transformX) + moveX, ((801.0000) * transformY) + moveY),


        new createjs.Point(((623.3334) * transformX) + moveX, ((801.0000) * transformY) + moveY),
        new createjs.Point(((622.6666) * transformX) + moveX, ((801.0000) * transformY) + moveY),
        new createjs.Point(((622.0000) * transformX) + moveX, ((801.0000) * transformY) + moveY),


        new createjs.Point(((621.6667) * transformX) + moveX, ((801.6666) * transformY) + moveY),
        new createjs.Point(((621.3333) * transformX) + moveX, ((802.3334) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((803.0000) * transformY) + moveY),


        new createjs.Point(((620.3334) * transformX) + moveX, ((803.0000) * transformY) + moveY),
        new createjs.Point(((619.6666) * transformX) + moveX, ((803.0000) * transformY) + moveY),
        new createjs.Point(((619.0000) * transformX) + moveX, ((803.0000) * transformY) + moveY),


        new createjs.Point(((618.6667) * transformX) + moveX, ((803.6666) * transformY) + moveY),
        new createjs.Point(((618.3333) * transformX) + moveX, ((804.3334) * transformY) + moveY),
        new createjs.Point(((618.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),


        new createjs.Point(((616.6668) * transformX) + moveX, ((805.3333) * transformY) + moveY),
        new createjs.Point(((615.3332) * transformX) + moveX, ((805.6667) * transformY) + moveY),
        new createjs.Point(((614.0000) * transformX) + moveX, ((806.0000) * transformY) + moveY),


        new createjs.Point(((613.6667) * transformX) + moveX, ((806.6666) * transformY) + moveY),
        new createjs.Point(((613.3333) * transformX) + moveX, ((807.3334) * transformY) + moveY),
        new createjs.Point(((613.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),


        new createjs.Point(((612.3334) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((611.6666) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),


        new createjs.Point(((610.6667) * transformX) + moveX, ((808.6666) * transformY) + moveY),
        new createjs.Point(((610.3333) * transformX) + moveX, ((809.3334) * transformY) + moveY),
        new createjs.Point(((610.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),


        new createjs.Point(((608.6668) * transformX) + moveX, ((810.3333) * transformY) + moveY),
        new createjs.Point(((607.3332) * transformX) + moveX, ((810.6667) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((811.0000) * transformY) + moveY),


        new createjs.Point(((606.0000) * transformX) + moveX, ((811.3333) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((811.6667) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((812.0000) * transformY) + moveY),


        new createjs.Point(((605.0001) * transformX) + moveX, ((812.3333) * transformY) + moveY),
        new createjs.Point(((603.9999) * transformX) + moveX, ((812.6667) * transformY) + moveY),
        new createjs.Point(((603.0000) * transformX) + moveX, ((813.0000) * transformY) + moveY),


        new createjs.Point(((602.6667) * transformX) + moveX, ((813.6666) * transformY) + moveY),
        new createjs.Point(((602.3333) * transformX) + moveX, ((814.3334) * transformY) + moveY),
        new createjs.Point(((602.0000) * transformX) + moveX, ((815.0000) * transformY) + moveY),


        new createjs.Point(((601.3334) * transformX) + moveX, ((815.0000) * transformY) + moveY),
        new createjs.Point(((600.6666) * transformX) + moveX, ((815.0000) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((815.0000) * transformY) + moveY),


        new createjs.Point(((600.0000) * transformX) + moveX, ((815.3333) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((815.6667) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((816.0000) * transformY) + moveY),


        new createjs.Point(((599.3334) * transformX) + moveX, ((816.0000) * transformY) + moveY),
        new createjs.Point(((598.6666) * transformX) + moveX, ((816.0000) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((816.0000) * transformY) + moveY),


        new createjs.Point(((597.6667) * transformX) + moveX, ((816.6666) * transformY) + moveY),
        new createjs.Point(((597.3333) * transformX) + moveX, ((817.3334) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),


        new createjs.Point(((595.6668) * transformX) + moveX, ((818.3333) * transformY) + moveY),
        new createjs.Point(((594.3332) * transformX) + moveX, ((818.6667) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((819.0000) * transformY) + moveY),


        new createjs.Point(((593.0000) * transformX) + moveX, ((819.3333) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((819.6667) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((820.0000) * transformY) + moveY),


        new createjs.Point(((592.0001) * transformX) + moveX, ((820.3333) * transformY) + moveY),
        new createjs.Point(((590.9999) * transformX) + moveX, ((820.6667) * transformY) + moveY),
        new createjs.Point(((590.0000) * transformX) + moveX, ((821.0000) * transformY) + moveY),


        new createjs.Point(((589.6667) * transformX) + moveX, ((821.6666) * transformY) + moveY),
        new createjs.Point(((589.3333) * transformX) + moveX, ((822.3334) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((823.0000) * transformY) + moveY),


        new createjs.Point(((587.6668) * transformX) + moveX, ((823.3333) * transformY) + moveY),
        new createjs.Point(((586.3332) * transformX) + moveX, ((823.6667) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((824.0000) * transformY) + moveY),


        new createjs.Point(((584.6667) * transformX) + moveX, ((824.6666) * transformY) + moveY),
        new createjs.Point(((584.3333) * transformX) + moveX, ((825.3334) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((826.0000) * transformY) + moveY),


        new createjs.Point(((582.6668) * transformX) + moveX, ((826.3333) * transformY) + moveY),
        new createjs.Point(((581.3332) * transformX) + moveX, ((826.6667) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((827.0000) * transformY) + moveY),


        new createjs.Point(((579.6667) * transformX) + moveX, ((827.6666) * transformY) + moveY),
        new createjs.Point(((579.3333) * transformX) + moveX, ((828.3334) * transformY) + moveY),
        new createjs.Point(((579.0000) * transformX) + moveX, ((829.0000) * transformY) + moveY),


        new createjs.Point(((577.6668) * transformX) + moveX, ((829.3333) * transformY) + moveY),
        new createjs.Point(((576.3332) * transformX) + moveX, ((829.6667) * transformY) + moveY),
        new createjs.Point(((575.0000) * transformX) + moveX, ((830.0000) * transformY) + moveY),


        new createjs.Point(((574.6667) * transformX) + moveX, ((830.6666) * transformY) + moveY),
        new createjs.Point(((574.3333) * transformX) + moveX, ((831.3334) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((832.0000) * transformY) + moveY),


        new createjs.Point(((572.6668) * transformX) + moveX, ((832.3333) * transformY) + moveY),
        new createjs.Point(((571.3332) * transformX) + moveX, ((832.6667) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((833.0000) * transformY) + moveY),


        new createjs.Point(((569.6667) * transformX) + moveX, ((833.6666) * transformY) + moveY),
        new createjs.Point(((569.3333) * transformX) + moveX, ((834.3334) * transformY) + moveY),
        new createjs.Point(((569.0000) * transformX) + moveX, ((835.0000) * transformY) + moveY),


        new createjs.Point(((567.6668) * transformX) + moveX, ((835.3333) * transformY) + moveY),
        new createjs.Point(((566.3332) * transformX) + moveX, ((835.6667) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((836.0000) * transformY) + moveY),

        //start joining part of num-3
        new createjs.Point(((564.6667) * transformX) + moveX, ((836.6666) * transformY) + moveY),
        new createjs.Point(((564.3333) * transformX) + moveX, ((837.3334) * transformY) + moveY),
        new createjs.Point(((564.0000) * transformX) + moveX, ((838.0000) * transformY) + moveY),


        new createjs.Point(((562.6668) * transformX) + moveX, ((838.3333) * transformY) + moveY),
        new createjs.Point(((561.3332) * transformX) + moveX, ((838.6667) * transformY) + moveY),
        new createjs.Point(((560.0000) * transformX) + moveX, ((839.0000) * transformY) + moveY),


        new createjs.Point(((559.6667) * transformX) + moveX, ((839.6666) * transformY) + moveY),
        new createjs.Point(((559.3333) * transformX) + moveX, ((840.3334) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((841.0000) * transformY) + moveY),


        new createjs.Point(((558.3334) * transformX) + moveX, ((841.0000) * transformY) + moveY),
        new createjs.Point(((557.6666) * transformX) + moveX, ((841.0000) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((841.0000) * transformY) + moveY),

        // joining part of num-3

        new createjs.Point(((500.3334) * transformX) + moveX, ((869) * transformY) + moveY),
        new createjs.Point(((495.3334) * transformX) + moveX, ((869) * transformY) + moveY),
        new createjs.Point(((490.3334) * transformX) + moveX, ((869) * transformY) + moveY),

        new createjs.Point(((485.3334) * transformX) + moveX, ((869) * transformY) + moveY),
        new createjs.Point(((480.3334) * transformX) + moveX, ((869) * transformY) + moveY),
        new createjs.Point(((475.6666) * transformX) + moveX, ((871.0000) * transformY) + moveY),

        new createjs.Point(((470.6666) * transformX) + moveX, ((871.0000) * transformY) + moveY),
        new createjs.Point(((465.6666) * transformX) + moveX, ((871.0000) * transformY) + moveY),
        new createjs.Point(((460.6666) * transformX) + moveX, ((871.0000) * transformY) + moveY),

        new createjs.Point(((455.0000) * transformX) + moveX, ((873) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((873) * transformY) + moveY),
        new createjs.Point(((455.0000) * transformX) + moveX, ((877) * transformY) + moveY),

        new createjs.Point(((460.6666) * transformX) + moveX, ((880.0000) * transformY) + moveY),
        new createjs.Point(((485.6666) * transformX) + moveX, ((881.0000) * transformY) + moveY),
        new createjs.Point(((500.6666) * transformX) + moveX, ((882.0000) * transformY) + moveY),

        new createjs.Point(((515.6666) * transformX) + moveX, ((885.0000) * transformY) + moveY),
        new createjs.Point(((525.6666) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((535.6666) * transformX) + moveX, ((890.0000) * transformY) + moveY),

        // joining part of num-3

        new createjs.Point(((544.3333) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((544.6667) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((896.0000) * transformY) + moveY),


        new createjs.Point(((545.0000) * transformX) + moveX, ((896.3333) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((896.6667) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((897.0000) * transformY) + moveY),


        new createjs.Point(((551.6296) * transformX) + moveX, ((896.8945) * transformY) + moveY),
        new createjs.Point(((558.8401) * transformX) + moveX, ((898.4406) * transformY) + moveY),
        new createjs.Point(((564.0000) * transformX) + moveX, ((900.0000) * transformY) + moveY),


        new createjs.Point(((565.9998) * transformX) + moveX, ((900.0000) * transformY) + moveY),
        new createjs.Point(((568.0002) * transformX) + moveX, ((900.0000) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((900.0000) * transformY) + moveY),


        new createjs.Point(((570.0000) * transformX) + moveX, ((900.3333) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((900.6667) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((901.0000) * transformY) + moveY),


        new createjs.Point(((571.9998) * transformX) + moveX, ((901.0000) * transformY) + moveY),
        new createjs.Point(((574.0002) * transformX) + moveX, ((901.0000) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((901.0000) * transformY) + moveY),


        new createjs.Point(((576.0000) * transformX) + moveX, ((901.3333) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((901.6667) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((902.0000) * transformY) + moveY),


        new createjs.Point(((577.9998) * transformX) + moveX, ((902.0000) * transformY) + moveY),
        new createjs.Point(((580.0002) * transformX) + moveX, ((902.0000) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((902.0000) * transformY) + moveY),


        new createjs.Point(((582.0000) * transformX) + moveX, ((902.3333) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((902.6667) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((903.0000) * transformY) + moveY),


        new createjs.Point(((583.9998) * transformX) + moveX, ((903.0000) * transformY) + moveY),
        new createjs.Point(((586.0002) * transformX) + moveX, ((903.0000) * transformY) + moveY),
        new createjs.Point(((588.0000) * transformX) + moveX, ((903.0000) * transformY) + moveY),


        new createjs.Point(((588.0000) * transformX) + moveX, ((903.3333) * transformY) + moveY),
        new createjs.Point(((588.0000) * transformX) + moveX, ((903.6667) * transformY) + moveY),
        new createjs.Point(((588.0000) * transformX) + moveX, ((904.0000) * transformY) + moveY),


        new createjs.Point(((589.6665) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((591.3335) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((904.0000) * transformY) + moveY),


        new createjs.Point(((593.0000) * transformX) + moveX, ((904.3333) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((904.6667) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((905.0000) * transformY) + moveY),


        new createjs.Point(((596.9996) * transformX) + moveX, ((905.3333) * transformY) + moveY),
        new createjs.Point(((601.0004) * transformX) + moveX, ((905.6667) * transformY) + moveY),
        new createjs.Point(((605.0000) * transformX) + moveX, ((906.0000) * transformY) + moveY),


        new createjs.Point(((616.1454) * transformX) + moveX, ((909.4430) * transformY) + moveY),
        new createjs.Point(((629.9816) * transformX) + moveX, ((910.5300) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((914.0000) * transformY) + moveY),


        new createjs.Point(((642.6665) * transformX) + moveX, ((914.0000) * transformY) + moveY),
        new createjs.Point(((644.3335) * transformX) + moveX, ((914.0000) * transformY) + moveY),
        new createjs.Point(((646.0000) * transformX) + moveX, ((914.0000) * transformY) + moveY),


        new createjs.Point(((646.0000) * transformX) + moveX, ((914.3333) * transformY) + moveY),
        new createjs.Point(((646.0000) * transformX) + moveX, ((914.6667) * transformY) + moveY),
        new createjs.Point(((646.0000) * transformX) + moveX, ((915.0000) * transformY) + moveY),


        new createjs.Point(((647.6665) * transformX) + moveX, ((915.0000) * transformY) + moveY),
        new createjs.Point(((649.3335) * transformX) + moveX, ((915.0000) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((915.0000) * transformY) + moveY),


        new createjs.Point(((651.0000) * transformX) + moveX, ((915.3333) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((915.6667) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((916.0000) * transformY) + moveY),


        new createjs.Point(((652.3332) * transformX) + moveX, ((916.0000) * transformY) + moveY),
        new createjs.Point(((653.6668) * transformX) + moveX, ((916.0000) * transformY) + moveY),
        new createjs.Point(((655.0000) * transformX) + moveX, ((916.0000) * transformY) + moveY),


        new createjs.Point(((655.0000) * transformX) + moveX, ((916.3333) * transformY) + moveY),
        new createjs.Point(((655.0000) * transformX) + moveX, ((916.6667) * transformY) + moveY),
        new createjs.Point(((655.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),


        new createjs.Point(((656.3332) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((657.6668) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((659.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),


        new createjs.Point(((659.0000) * transformX) + moveX, ((917.3333) * transformY) + moveY),
        new createjs.Point(((659.0000) * transformX) + moveX, ((917.6667) * transformY) + moveY),
        new createjs.Point(((659.0000) * transformX) + moveX, ((918.0000) * transformY) + moveY),


        new createjs.Point(((660.6665) * transformX) + moveX, ((918.0000) * transformY) + moveY),
        new createjs.Point(((662.3335) * transformX) + moveX, ((918.0000) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((918.0000) * transformY) + moveY),


        new createjs.Point(((664.0000) * transformX) + moveX, ((918.3333) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((918.6667) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((919.0000) * transformY) + moveY),


        new createjs.Point(((670.6660) * transformX) + moveX, ((920.3332) * transformY) + moveY),
        new createjs.Point(((677.3340) * transformX) + moveX, ((921.6668) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((923.0000) * transformY) + moveY),


        new createjs.Point(((695.7548) * transformX) + moveX, ((927.0393) * transformY) + moveY),
        new createjs.Point(((708.8581) * transformX) + moveX, ((930.5549) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((935.0000) * transformY) + moveY),


        new createjs.Point(((720.9999) * transformX) + moveX, ((935.0000) * transformY) + moveY),
        new createjs.Point(((722.0001) * transformX) + moveX, ((935.0000) * transformY) + moveY),
        new createjs.Point(((723.0000) * transformX) + moveX, ((935.0000) * transformY) + moveY),


        new createjs.Point(((723.0000) * transformX) + moveX, ((935.3333) * transformY) + moveY),
        new createjs.Point(((723.0000) * transformX) + moveX, ((935.6667) * transformY) + moveY),
        new createjs.Point(((723.0000) * transformX) + moveX, ((936.0000) * transformY) + moveY),


        new createjs.Point(((723.9999) * transformX) + moveX, ((936.0000) * transformY) + moveY),
        new createjs.Point(((725.0001) * transformX) + moveX, ((936.0000) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((936.0000) * transformY) + moveY),


        new createjs.Point(((726.0000) * transformX) + moveX, ((936.3333) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((936.6667) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((937.0000) * transformY) + moveY),


        new createjs.Point(((726.9999) * transformX) + moveX, ((937.0000) * transformY) + moveY),
        new createjs.Point(((728.0001) * transformX) + moveX, ((937.0000) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((937.0000) * transformY) + moveY),


        new createjs.Point(((729.0000) * transformX) + moveX, ((937.3333) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((937.6667) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((938.0000) * transformY) + moveY),


        new createjs.Point(((729.9999) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((731.0001) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((732.0000) * transformX) + moveX, ((938.0000) * transformY) + moveY),


        new createjs.Point(((732.0000) * transformX) + moveX, ((938.3333) * transformY) + moveY),
        new createjs.Point(((732.0000) * transformX) + moveX, ((938.6667) * transformY) + moveY),
        new createjs.Point(((732.0000) * transformX) + moveX, ((939.0000) * transformY) + moveY),


        new createjs.Point(((732.6666) * transformX) + moveX, ((939.0000) * transformY) + moveY),
        new createjs.Point(((733.3334) * transformX) + moveX, ((939.0000) * transformY) + moveY),
        new createjs.Point(((734.0000) * transformX) + moveX, ((939.0000) * transformY) + moveY),


        new createjs.Point(((734.0000) * transformX) + moveX, ((939.3333) * transformY) + moveY),
        new createjs.Point(((734.0000) * transformX) + moveX, ((939.6667) * transformY) + moveY),
        new createjs.Point(((734.0000) * transformX) + moveX, ((940.0000) * transformY) + moveY),


        new createjs.Point(((734.9999) * transformX) + moveX, ((940.0000) * transformY) + moveY),
        new createjs.Point(((736.0001) * transformX) + moveX, ((940.0000) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((940.0000) * transformY) + moveY),


        new createjs.Point(((737.0000) * transformX) + moveX, ((940.3333) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((940.6667) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((941.0000) * transformY) + moveY),


        new createjs.Point(((738.6665) * transformX) + moveX, ((941.3333) * transformY) + moveY),
        new createjs.Point(((740.3335) * transformX) + moveX, ((941.6667) * transformY) + moveY),
        new createjs.Point(((742.0000) * transformX) + moveX, ((942.0000) * transformY) + moveY),


        new createjs.Point(((742.0000) * transformX) + moveX, ((942.3333) * transformY) + moveY),
        new createjs.Point(((742.0000) * transformX) + moveX, ((942.6667) * transformY) + moveY),
        new createjs.Point(((742.0000) * transformX) + moveX, ((943.0000) * transformY) + moveY),


        new createjs.Point(((742.6666) * transformX) + moveX, ((943.0000) * transformY) + moveY),
        new createjs.Point(((743.3334) * transformX) + moveX, ((943.0000) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((943.0000) * transformY) + moveY),


        new createjs.Point(((744.0000) * transformX) + moveX, ((943.3333) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((943.6667) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((944.0000) * transformY) + moveY),


        new createjs.Point(((744.9999) * transformX) + moveX, ((944.0000) * transformY) + moveY),
        new createjs.Point(((746.0001) * transformX) + moveX, ((944.0000) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((944.0000) * transformY) + moveY),


        new createjs.Point(((747.0000) * transformX) + moveX, ((944.3333) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((944.6667) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((945.0000) * transformY) + moveY),


        new createjs.Point(((748.3332) * transformX) + moveX, ((945.3333) * transformY) + moveY),
        new createjs.Point(((749.6668) * transformX) + moveX, ((945.6667) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((946.0000) * transformY) + moveY),


        new createjs.Point(((751.0000) * transformX) + moveX, ((946.3333) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((946.6667) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((947.0000) * transformY) + moveY),


        new createjs.Point(((751.9999) * transformX) + moveX, ((947.0000) * transformY) + moveY),
        new createjs.Point(((753.0001) * transformX) + moveX, ((947.0000) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((947.0000) * transformY) + moveY),


        new createjs.Point(((754.0000) * transformX) + moveX, ((947.3333) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((947.6667) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((948.0000) * transformY) + moveY),


        new createjs.Point(((755.9998) * transformX) + moveX, ((948.6666) * transformY) + moveY),
        new createjs.Point(((758.0002) * transformX) + moveX, ((949.3334) * transformY) + moveY),
        new createjs.Point(((760.0000) * transformX) + moveX, ((950.0000) * transformY) + moveY),


        new createjs.Point(((760.0000) * transformX) + moveX, ((950.3333) * transformY) + moveY),
        new createjs.Point(((760.0000) * transformX) + moveX, ((950.6667) * transformY) + moveY),
        new createjs.Point(((760.0000) * transformX) + moveX, ((951.0000) * transformY) + moveY),


        new createjs.Point(((760.6666) * transformX) + moveX, ((951.0000) * transformY) + moveY),
        new createjs.Point(((761.3334) * transformX) + moveX, ((951.0000) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((951.0000) * transformY) + moveY),


        new createjs.Point(((762.0000) * transformX) + moveX, ((951.3333) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((951.6667) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),


        new createjs.Point(((762.6666) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((763.3334) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),


        new createjs.Point(((764.0000) * transformX) + moveX, ((952.3333) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((952.6667) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((953.0000) * transformY) + moveY),


        new createjs.Point(((764.6666) * transformX) + moveX, ((953.0000) * transformY) + moveY),
        new createjs.Point(((765.3334) * transformX) + moveX, ((953.0000) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((953.0000) * transformY) + moveY),


        new createjs.Point(((766.0000) * transformX) + moveX, ((953.3333) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((953.6667) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((954.0000) * transformY) + moveY),


        new createjs.Point(((766.6666) * transformX) + moveX, ((954.0000) * transformY) + moveY),
        new createjs.Point(((767.3334) * transformX) + moveX, ((954.0000) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((954.0000) * transformY) + moveY),


        new createjs.Point(((768.0000) * transformX) + moveX, ((954.3333) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((954.6667) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((955.0000) * transformY) + moveY),


        new createjs.Point(((768.6666) * transformX) + moveX, ((955.0000) * transformY) + moveY),
        new createjs.Point(((769.3334) * transformX) + moveX, ((955.0000) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((955.0000) * transformY) + moveY),


        new createjs.Point(((770.0000) * transformX) + moveX, ((955.3333) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((955.6667) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((956.0000) * transformY) + moveY),


        new createjs.Point(((770.6666) * transformX) + moveX, ((956.0000) * transformY) + moveY),
        new createjs.Point(((771.3334) * transformX) + moveX, ((956.0000) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((956.0000) * transformY) + moveY),


        new createjs.Point(((772.0000) * transformX) + moveX, ((956.3333) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((956.6667) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((957.0000) * transformY) + moveY),


        new createjs.Point(((772.6666) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((773.3334) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((957.0000) * transformY) + moveY),


        new createjs.Point(((774.0000) * transformX) + moveX, ((957.3333) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((957.6667) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((958.0000) * transformY) + moveY),


        new createjs.Point(((774.6666) * transformX) + moveX, ((958.0000) * transformY) + moveY),
        new createjs.Point(((775.3334) * transformX) + moveX, ((958.0000) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((958.0000) * transformY) + moveY),


        new createjs.Point(((776.3333) * transformX) + moveX, ((958.6666) * transformY) + moveY),
        new createjs.Point(((776.6667) * transformX) + moveX, ((959.3334) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((960.0000) * transformY) + moveY),


        new createjs.Point(((778.9998) * transformX) + moveX, ((960.6666) * transformY) + moveY),
        new createjs.Point(((781.0002) * transformX) + moveX, ((961.3334) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((962.0000) * transformY) + moveY),


        new createjs.Point(((783.3333) * transformX) + moveX, ((962.6666) * transformY) + moveY),
        new createjs.Point(((783.6667) * transformX) + moveX, ((963.3334) * transformY) + moveY),
        new createjs.Point(((784.0000) * transformX) + moveX, ((964.0000) * transformY) + moveY),


        new createjs.Point(((785.3332) * transformX) + moveX, ((964.3333) * transformY) + moveY),
        new createjs.Point(((786.6668) * transformX) + moveX, ((964.6667) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((965.0000) * transformY) + moveY),


        new createjs.Point(((788.3333) * transformX) + moveX, ((965.6666) * transformY) + moveY),
        new createjs.Point(((788.6667) * transformX) + moveX, ((966.3334) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),


        new createjs.Point(((789.6666) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((790.3334) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),


        new createjs.Point(((791.3333) * transformX) + moveX, ((967.6666) * transformY) + moveY),
        new createjs.Point(((791.6667) * transformX) + moveX, ((968.3334) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((969.0000) * transformY) + moveY),


        new createjs.Point(((792.6666) * transformX) + moveX, ((969.0000) * transformY) + moveY),
        new createjs.Point(((793.3334) * transformX) + moveX, ((969.0000) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((969.0000) * transformY) + moveY),


        new createjs.Point(((794.3333) * transformX) + moveX, ((969.6666) * transformY) + moveY),
        new createjs.Point(((794.6667) * transformX) + moveX, ((970.3334) * transformY) + moveY),
        new createjs.Point(((795.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),


        new createjs.Point(((795.6666) * transformX) + moveX, ((971.0000) * transformY) + moveY),
        new createjs.Point(((796.3334) * transformX) + moveX, ((971.0000) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),


        new createjs.Point(((797.0000) * transformX) + moveX, ((971.3333) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((971.6667) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((972.0000) * transformY) + moveY),


        new createjs.Point(((797.9999) * transformX) + moveX, ((972.3333) * transformY) + moveY),
        new createjs.Point(((799.0001) * transformX) + moveX, ((972.6667) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((973.0000) * transformY) + moveY),


        new createjs.Point(((800.3333) * transformX) + moveX, ((973.6666) * transformY) + moveY),
        new createjs.Point(((800.6667) * transformX) + moveX, ((974.3334) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),


        new createjs.Point(((801.6666) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((802.3334) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),


        new createjs.Point(((803.6666) * transformX) + moveX, ((975.9999) * transformY) + moveY),
        new createjs.Point(((804.3334) * transformX) + moveX, ((977.0001) * transformY) + moveY),
        new createjs.Point(((805.0000) * transformX) + moveX, ((978.0000) * transformY) + moveY),


        new createjs.Point(((805.6666) * transformX) + moveX, ((978.0000) * transformY) + moveY),
        new createjs.Point(((806.3334) * transformX) + moveX, ((978.0000) * transformY) + moveY),
        new createjs.Point(((807.0000) * transformX) + moveX, ((978.0000) * transformY) + moveY),


        new createjs.Point(((807.6666) * transformX) + moveX, ((978.9999) * transformY) + moveY),
        new createjs.Point(((808.3334) * transformX) + moveX, ((980.0001) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((981.0000) * transformY) + moveY),


        new createjs.Point(((809.6666) * transformX) + moveX, ((981.0000) * transformY) + moveY),
        new createjs.Point(((810.3334) * transformX) + moveX, ((981.0000) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((981.0000) * transformY) + moveY),


        new createjs.Point(((811.9999) * transformX) + moveX, ((982.3332) * transformY) + moveY),
        new createjs.Point(((813.0001) * transformX) + moveX, ((983.6668) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((985.0000) * transformY) + moveY),


        new createjs.Point(((814.6666) * transformX) + moveX, ((985.0000) * transformY) + moveY),
        new createjs.Point(((815.3334) * transformX) + moveX, ((985.0000) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((985.0000) * transformY) + moveY),


        new createjs.Point(((817.9998) * transformX) + moveX, ((987.3331) * transformY) + moveY),
        new createjs.Point(((820.0002) * transformX) + moveX, ((989.6669) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((992.0000) * transformY) + moveY),


        new createjs.Point(((822.6666) * transformX) + moveX, ((992.0000) * transformY) + moveY),
        new createjs.Point(((823.3334) * transformX) + moveX, ((992.0000) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((992.0000) * transformY) + moveY),


        new createjs.Point(((824.9999) * transformX) + moveX, ((993.3332) * transformY) + moveY),
        new createjs.Point(((826.0001) * transformX) + moveX, ((994.6668) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((996.0000) * transformY) + moveY),


        new createjs.Point(((829.9997) * transformX) + moveX, ((998.6664) * transformY) + moveY),
        new createjs.Point(((833.0003) * transformX) + moveX, ((1001.3336) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1004.0000) * transformY) + moveY),


        new createjs.Point(((836.0000) * transformX) + moveX, ((1004.6666) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1005.3334) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1006.0000) * transformY) + moveY),


        new createjs.Point(((838.3331) * transformX) + moveX, ((1007.9998) * transformY) + moveY),
        new createjs.Point(((840.6669) * transformX) + moveX, ((1010.0002) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1012.0000) * transformY) + moveY),


        new createjs.Point(((843.3333) * transformX) + moveX, ((1012.9999) * transformY) + moveY),
        new createjs.Point(((843.6667) * transformX) + moveX, ((1014.0001) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1015.0000) * transformY) + moveY),


        new createjs.Point(((844.9999) * transformX) + moveX, ((1015.6666) * transformY) + moveY),
        new createjs.Point(((846.0001) * transformX) + moveX, ((1016.3334) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),


        new createjs.Point(((847.0000) * transformX) + moveX, ((1017.6666) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1018.3334) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1019.0000) * transformY) + moveY),


        new createjs.Point(((847.9999) * transformX) + moveX, ((1019.6666) * transformY) + moveY),
        new createjs.Point(((849.0001) * transformX) + moveX, ((1020.3334) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1021.0000) * transformY) + moveY),


        new createjs.Point(((850.3333) * transformX) + moveX, ((1021.9999) * transformY) + moveY),
        new createjs.Point(((850.6667) * transformX) + moveX, ((1023.0001) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1024.0000) * transformY) + moveY),


        new createjs.Point(((851.6666) * transformX) + moveX, ((1024.3333) * transformY) + moveY),
        new createjs.Point(((852.3334) * transformX) + moveX, ((1024.6667) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1025.0000) * transformY) + moveY),


        new createjs.Point(((853.0000) * transformX) + moveX, ((1025.6666) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1026.3334) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1027.0000) * transformY) + moveY),


        new createjs.Point(((853.6666) * transformX) + moveX, ((1027.3333) * transformY) + moveY),
        new createjs.Point(((854.3334) * transformX) + moveX, ((1027.6667) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1028.0000) * transformY) + moveY),


        new createjs.Point(((855.0000) * transformX) + moveX, ((1028.6666) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1029.3334) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1030.0000) * transformY) + moveY),


        new createjs.Point(((855.6666) * transformX) + moveX, ((1030.3333) * transformY) + moveY),
        new createjs.Point(((856.3334) * transformX) + moveX, ((1030.6667) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1031.0000) * transformY) + moveY),


        new createjs.Point(((857.0000) * transformX) + moveX, ((1031.6666) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1032.3334) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1033.0000) * transformY) + moveY),


        new createjs.Point(((857.6666) * transformX) + moveX, ((1033.3333) * transformY) + moveY),
        new createjs.Point(((858.3334) * transformX) + moveX, ((1033.6667) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1034.0000) * transformY) + moveY),


        new createjs.Point(((859.0000) * transformX) + moveX, ((1034.6666) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1035.3334) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1036.0000) * transformY) + moveY),


        new createjs.Point(((859.6666) * transformX) + moveX, ((1036.3333) * transformY) + moveY),
        new createjs.Point(((860.3334) * transformX) + moveX, ((1036.6667) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1037.0000) * transformY) + moveY),


        new createjs.Point(((861.0000) * transformX) + moveX, ((1037.6666) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1038.3334) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1039.0000) * transformY) + moveY),


        new createjs.Point(((861.6666) * transformX) + moveX, ((1039.3333) * transformY) + moveY),
        new createjs.Point(((862.3334) * transformX) + moveX, ((1039.6667) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((1040.0000) * transformY) + moveY),


        new createjs.Point(((863.6666) * transformX) + moveX, ((1041.9998) * transformY) + moveY),
        new createjs.Point(((864.3334) * transformX) + moveX, ((1044.0002) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1046.0000) * transformY) + moveY),


        new createjs.Point(((865.6666) * transformX) + moveX, ((1046.3333) * transformY) + moveY),
        new createjs.Point(((866.3334) * transformX) + moveX, ((1046.6667) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1047.0000) * transformY) + moveY),


        new createjs.Point(((867.0000) * transformX) + moveX, ((1047.6666) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1048.3334) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1049.0000) * transformY) + moveY),


        new createjs.Point(((867.3333) * transformX) + moveX, ((1049.0000) * transformY) + moveY),
        new createjs.Point(((867.6667) * transformX) + moveX, ((1049.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1049.0000) * transformY) + moveY),


        new createjs.Point(((868.0000) * transformX) + moveX, ((1049.6666) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1050.3334) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1051.0000) * transformY) + moveY),


        new createjs.Point(((868.3333) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((868.6667) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1051.0000) * transformY) + moveY),


        new createjs.Point(((869.0000) * transformX) + moveX, ((1051.6666) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1052.3334) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1053.0000) * transformY) + moveY),


        new createjs.Point(((869.3333) * transformX) + moveX, ((1053.0000) * transformY) + moveY),
        new createjs.Point(((869.6667) * transformX) + moveX, ((1053.0000) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1053.0000) * transformY) + moveY),


        new createjs.Point(((870.0000) * transformX) + moveX, ((1053.6666) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1054.3334) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1055.0000) * transformY) + moveY),


        new createjs.Point(((870.3333) * transformX) + moveX, ((1055.0000) * transformY) + moveY),
        new createjs.Point(((870.6667) * transformX) + moveX, ((1055.0000) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1055.0000) * transformY) + moveY),


        new createjs.Point(((871.0000) * transformX) + moveX, ((1055.6666) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1056.3334) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1057.0000) * transformY) + moveY),


        new createjs.Point(((871.3333) * transformX) + moveX, ((1057.0000) * transformY) + moveY),
        new createjs.Point(((871.6667) * transformX) + moveX, ((1057.0000) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1057.0000) * transformY) + moveY),


        new createjs.Point(((872.0000) * transformX) + moveX, ((1057.6666) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1058.3334) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),


        new createjs.Point(((872.3333) * transformX) + moveX, ((1059.0000) * transformY) + moveY),
        new createjs.Point(((872.6667) * transformX) + moveX, ((1059.0000) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),


        new createjs.Point(((873.6666) * transformX) + moveX, ((1060.9998) * transformY) + moveY),
        new createjs.Point(((874.3334) * transformX) + moveX, ((1063.0002) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1065.0000) * transformY) + moveY),


        new createjs.Point(((875.3333) * transformX) + moveX, ((1065.0000) * transformY) + moveY),
        new createjs.Point(((875.6667) * transformX) + moveX, ((1065.0000) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1065.0000) * transformY) + moveY),


        new createjs.Point(((876.0000) * transformX) + moveX, ((1065.9999) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1067.0001) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),


        new createjs.Point(((876.3333) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((876.6667) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),


        new createjs.Point(((877.3333) * transformX) + moveX, ((1069.3332) * transformY) + moveY),
        new createjs.Point(((877.6667) * transformX) + moveX, ((1070.6668) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1072.0000) * transformY) + moveY),


        new createjs.Point(((878.3333) * transformX) + moveX, ((1072.0000) * transformY) + moveY),
        new createjs.Point(((878.6667) * transformX) + moveX, ((1072.0000) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1072.0000) * transformY) + moveY),


        new createjs.Point(((879.0000) * transformX) + moveX, ((1072.9999) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1074.0001) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1075.0000) * transformY) + moveY),


        new createjs.Point(((879.3333) * transformX) + moveX, ((1075.0000) * transformY) + moveY),
        new createjs.Point(((879.6667) * transformX) + moveX, ((1075.0000) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1075.0000) * transformY) + moveY),


        new createjs.Point(((880.0000) * transformX) + moveX, ((1075.6666) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1076.3334) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1077.0000) * transformY) + moveY),


        new createjs.Point(((880.3333) * transformX) + moveX, ((1077.0000) * transformY) + moveY),
        new createjs.Point(((880.6667) * transformX) + moveX, ((1077.0000) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1077.0000) * transformY) + moveY),


        new createjs.Point(((881.3333) * transformX) + moveX, ((1078.9998) * transformY) + moveY),
        new createjs.Point(((881.6667) * transformX) + moveX, ((1081.0002) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1083.0000) * transformY) + moveY),


        new createjs.Point(((882.3333) * transformX) + moveX, ((1083.0000) * transformY) + moveY),
        new createjs.Point(((882.6667) * transformX) + moveX, ((1083.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1083.0000) * transformY) + moveY),


        new createjs.Point(((883.0000) * transformX) + moveX, ((1083.6666) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1084.3334) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1085.0000) * transformY) + moveY),


        new createjs.Point(((883.3333) * transformX) + moveX, ((1085.0000) * transformY) + moveY),
        new createjs.Point(((883.6667) * transformX) + moveX, ((1085.0000) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1085.0000) * transformY) + moveY),


        new createjs.Point(((884.3333) * transformX) + moveX, ((1086.9998) * transformY) + moveY),
        new createjs.Point(((884.6667) * transformX) + moveX, ((1089.0002) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),


        new createjs.Point(((885.3333) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((885.6667) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),


        new createjs.Point(((886.0000) * transformX) + moveX, ((1091.9999) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1093.0001) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1094.0000) * transformY) + moveY),


        new createjs.Point(((886.3333) * transformX) + moveX, ((1094.0000) * transformY) + moveY),
        new createjs.Point(((886.6667) * transformX) + moveX, ((1094.0000) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1094.0000) * transformY) + moveY),


        new createjs.Point(((887.0000) * transformX) + moveX, ((1094.9999) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1096.0001) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),


        new createjs.Point(((887.3333) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((887.6667) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),


        new createjs.Point(((888.3333) * transformX) + moveX, ((1099.3331) * transformY) + moveY),
        new createjs.Point(((888.6667) * transformX) + moveX, ((1101.6669) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),


        new createjs.Point(((889.3333) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((889.6667) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),


        new createjs.Point(((890.3333) * transformX) + moveX, ((1106.3331) * transformY) + moveY),
        new createjs.Point(((890.6667) * transformX) + moveX, ((1108.6669) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1111.0000) * transformY) + moveY),


        new createjs.Point(((891.3333) * transformX) + moveX, ((1111.0000) * transformY) + moveY),
        new createjs.Point(((891.6667) * transformX) + moveX, ((1111.0000) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((1111.0000) * transformY) + moveY),


        new createjs.Point(((892.3333) * transformX) + moveX, ((1113.6664) * transformY) + moveY),
        new createjs.Point(((892.6667) * transformX) + moveX, ((1116.3336) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),


        new createjs.Point(((893.3333) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((893.6667) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),


        new createjs.Point(((894.0000) * transformX) + moveX, ((1120.6665) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((1122.3335) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((1124.0000) * transformY) + moveY),


        new createjs.Point(((894.3333) * transformX) + moveX, ((1124.0000) * transformY) + moveY),
        new createjs.Point(((894.6667) * transformX) + moveX, ((1124.0000) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((1124.0000) * transformY) + moveY),


        new createjs.Point(((895.0000) * transformX) + moveX, ((1125.3332) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((1126.6668) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((1128.0000) * transformY) + moveY),


        new createjs.Point(((895.3333) * transformX) + moveX, ((1128.0000) * transformY) + moveY),
        new createjs.Point(((895.6667) * transformX) + moveX, ((1128.0000) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((1128.0000) * transformY) + moveY),


        new createjs.Point(((896.0000) * transformX) + moveX, ((1129.6665) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((1131.3335) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((1133.0000) * transformY) + moveY),


        new createjs.Point(((896.3333) * transformX) + moveX, ((1133.0000) * transformY) + moveY),
        new createjs.Point(((896.6667) * transformX) + moveX, ((1133.0000) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1133.0000) * transformY) + moveY),


        new createjs.Point(((897.0000) * transformX) + moveX, ((1134.6665) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1136.3335) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1138.0000) * transformY) + moveY),


        new createjs.Point(((897.3333) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((897.6667) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1138.0000) * transformY) + moveY),


        new createjs.Point(((898.0000) * transformX) + moveX, ((1139.9998) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1142.0002) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1144.0000) * transformY) + moveY),


        new createjs.Point(((898.3333) * transformX) + moveX, ((1144.0000) * transformY) + moveY),
        new createjs.Point(((898.6667) * transformX) + moveX, ((1144.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1144.0000) * transformY) + moveY),


        new createjs.Point(((899.0000) * transformX) + moveX, ((1145.9998) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1148.0002) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1150.0000) * transformY) + moveY),


        new createjs.Point(((899.3333) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((899.6667) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1150.0000) * transformY) + moveY),


        new createjs.Point(((900.0000) * transformX) + moveX, ((1152.3331) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1154.6669) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1157.0000) * transformY) + moveY),


        new createjs.Point(((900.3333) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((900.6667) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1157.0000) * transformY) + moveY),


        new createjs.Point(((901.0000) * transformX) + moveX, ((1159.6664) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1162.3336) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1165.0000) * transformY) + moveY),


        new createjs.Point(((901.3333) * transformX) + moveX, ((1165.0000) * transformY) + moveY),
        new createjs.Point(((901.6667) * transformX) + moveX, ((1165.0000) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1165.0000) * transformY) + moveY),


        new createjs.Point(((902.0000) * transformX) + moveX, ((1167.9997) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1171.0003) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1174.0000) * transformY) + moveY),


        new createjs.Point(((902.3333) * transformX) + moveX, ((1174.0000) * transformY) + moveY),
        new createjs.Point(((902.6667) * transformX) + moveX, ((1174.0000) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((1174.0000) * transformY) + moveY),


        new createjs.Point(((903.0000) * transformX) + moveX, ((1177.6663) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((1181.3337) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((1185.0000) * transformY) + moveY),


        new createjs.Point(((903.3333) * transformX) + moveX, ((1185.0000) * transformY) + moveY),
        new createjs.Point(((903.6667) * transformX) + moveX, ((1185.0000) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((1185.0000) * transformY) + moveY),


        new createjs.Point(((904.0000) * transformX) + moveX, ((1190.3328) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((1195.6672) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((1201.0000) * transformY) + moveY),


        new createjs.Point(((906.8442) * transformX) + moveX, ((1211.2479) * transformY) + moveY),
        new createjs.Point(((904.9964) * transformX) + moveX, ((1229.7107) * transformY) + moveY),
        new createjs.Point(((905.0000) * transformX) + moveX, ((1242.0000) * transformY) + moveY),


        new createjs.Point(((905.0020) * transformX) + moveX, ((1248.8426) * transformY) + moveY),
        new createjs.Point(((905.4850) * transformX) + moveX, ((1257.7654) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((1263.0000) * transformY) + moveY),


        new createjs.Point(((904.0000) * transformX) + moveX, ((1267.9995) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((1273.0005) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((1278.0000) * transformY) + moveY),


        new createjs.Point(((903.6667) * transformX) + moveX, ((1278.0000) * transformY) + moveY),
        new createjs.Point(((903.3333) * transformX) + moveX, ((1278.0000) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((1278.0000) * transformY) + moveY),


        new createjs.Point(((903.0000) * transformX) + moveX, ((1281.6663) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((1285.3337) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((1289.0000) * transformY) + moveY),


        new createjs.Point(((902.6667) * transformX) + moveX, ((1289.0000) * transformY) + moveY),
        new createjs.Point(((902.3333) * transformX) + moveX, ((1289.0000) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1289.0000) * transformY) + moveY),


        new createjs.Point(((902.0000) * transformX) + moveX, ((1291.9997) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1295.0003) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1298.0000) * transformY) + moveY),


        new createjs.Point(((901.6667) * transformX) + moveX, ((1298.0000) * transformY) + moveY),
        new createjs.Point(((901.3333) * transformX) + moveX, ((1298.0000) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1298.0000) * transformY) + moveY),


        new createjs.Point(((900.0001) * transformX) + moveX, ((1307.6657) * transformY) + moveY),
        new createjs.Point(((898.9999) * transformX) + moveX, ((1317.3343) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1327.0000) * transformY) + moveY),


        new createjs.Point(((896.6389) * transformX) + moveX, ((1331.4950) * transformY) + moveY),
        new createjs.Point(((895.4038) * transformX) + moveX, ((1338.5191) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((1343.0000) * transformY) + moveY),


        new createjs.Point(((894.0000) * transformX) + moveX, ((1344.6665) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((1346.3335) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((1348.0000) * transformY) + moveY),


        new createjs.Point(((893.6667) * transformX) + moveX, ((1348.0000) * transformY) + moveY),
        new createjs.Point(((893.3333) * transformX) + moveX, ((1348.0000) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1348.0000) * transformY) + moveY),


        new createjs.Point(((893.0000) * transformX) + moveX, ((1349.6665) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1351.3335) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1353.0000) * transformY) + moveY),


        new createjs.Point(((892.6667) * transformX) + moveX, ((1353.0000) * transformY) + moveY),
        new createjs.Point(((892.3333) * transformX) + moveX, ((1353.0000) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((1353.0000) * transformY) + moveY),


        new createjs.Point(((891.6667) * transformX) + moveX, ((1355.9997) * transformY) + moveY),
        new createjs.Point(((891.3333) * transformX) + moveX, ((1359.0003) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1362.0000) * transformY) + moveY),


        new createjs.Point(((890.6667) * transformX) + moveX, ((1362.0000) * transformY) + moveY),
        new createjs.Point(((890.3333) * transformX) + moveX, ((1362.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1362.0000) * transformY) + moveY),


        new createjs.Point(((890.0000) * transformX) + moveX, ((1363.3332) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1364.6668) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1366.0000) * transformY) + moveY),


        new createjs.Point(((889.6667) * transformX) + moveX, ((1366.0000) * transformY) + moveY),
        new createjs.Point(((889.3333) * transformX) + moveX, ((1366.0000) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1366.0000) * transformY) + moveY),


        new createjs.Point(((889.0000) * transformX) + moveX, ((1367.3332) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1368.6668) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1370.0000) * transformY) + moveY),


        new createjs.Point(((888.6667) * transformX) + moveX, ((1370.0000) * transformY) + moveY),
        new createjs.Point(((888.3333) * transformX) + moveX, ((1370.0000) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((1370.0000) * transformY) + moveY),


        new createjs.Point(((888.0000) * transformX) + moveX, ((1371.3332) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((1372.6668) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((1374.0000) * transformY) + moveY),


        new createjs.Point(((887.6667) * transformX) + moveX, ((1374.0000) * transformY) + moveY),
        new createjs.Point(((887.3333) * transformX) + moveX, ((1374.0000) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1374.0000) * transformY) + moveY),


        new createjs.Point(((887.0000) * transformX) + moveX, ((1375.3332) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1376.6668) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1378.0000) * transformY) + moveY),


        new createjs.Point(((886.6667) * transformX) + moveX, ((1378.0000) * transformY) + moveY),
        new createjs.Point(((886.3333) * transformX) + moveX, ((1378.0000) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1378.0000) * transformY) + moveY),


        new createjs.Point(((886.0000) * transformX) + moveX, ((1378.9999) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1380.0001) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1381.0000) * transformY) + moveY),


        new createjs.Point(((885.6667) * transformX) + moveX, ((1381.0000) * transformY) + moveY),
        new createjs.Point(((885.3333) * transformX) + moveX, ((1381.0000) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1381.0000) * transformY) + moveY),


        new createjs.Point(((885.0000) * transformX) + moveX, ((1382.3332) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1383.6668) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1385.0000) * transformY) + moveY),


        new createjs.Point(((884.6667) * transformX) + moveX, ((1385.0000) * transformY) + moveY),
        new createjs.Point(((884.3333) * transformX) + moveX, ((1385.0000) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1385.0000) * transformY) + moveY),


        new createjs.Point(((884.0000) * transformX) + moveX, ((1386.3332) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1387.6668) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1389.0000) * transformY) + moveY),


        new createjs.Point(((883.6667) * transformX) + moveX, ((1389.0000) * transformY) + moveY),
        new createjs.Point(((883.3333) * transformX) + moveX, ((1389.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1389.0000) * transformY) + moveY),


        new createjs.Point(((883.0000) * transformX) + moveX, ((1389.9999) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1391.0001) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1392.0000) * transformY) + moveY),


        new createjs.Point(((882.6667) * transformX) + moveX, ((1392.0000) * transformY) + moveY),
        new createjs.Point(((882.3333) * transformX) + moveX, ((1392.0000) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1392.0000) * transformY) + moveY),


        new createjs.Point(((882.0000) * transformX) + moveX, ((1393.3332) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1394.6668) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1396.0000) * transformY) + moveY),


        new createjs.Point(((881.6667) * transformX) + moveX, ((1396.0000) * transformY) + moveY),
        new createjs.Point(((881.3333) * transformX) + moveX, ((1396.0000) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1396.0000) * transformY) + moveY),


        new createjs.Point(((881.0000) * transformX) + moveX, ((1396.9999) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1398.0001) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1399.0000) * transformY) + moveY),


        new createjs.Point(((880.6667) * transformX) + moveX, ((1399.0000) * transformY) + moveY),
        new createjs.Point(((880.3333) * transformX) + moveX, ((1399.0000) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1399.0000) * transformY) + moveY),


        new createjs.Point(((880.0000) * transformX) + moveX, ((1399.9999) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1401.0001) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1402.0000) * transformY) + moveY),


        new createjs.Point(((879.6667) * transformX) + moveX, ((1402.0000) * transformY) + moveY),
        new createjs.Point(((879.3333) * transformX) + moveX, ((1402.0000) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1402.0000) * transformY) + moveY),


        new createjs.Point(((879.0000) * transformX) + moveX, ((1402.9999) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1404.0001) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1405.0000) * transformY) + moveY),


        new createjs.Point(((878.6667) * transformX) + moveX, ((1405.0000) * transformY) + moveY),
        new createjs.Point(((878.3333) * transformX) + moveX, ((1405.0000) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1405.0000) * transformY) + moveY),


        new createjs.Point(((878.0000) * transformX) + moveX, ((1405.9999) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1407.0001) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1408.0000) * transformY) + moveY),


        new createjs.Point(((877.6667) * transformX) + moveX, ((1408.0000) * transformY) + moveY),
        new createjs.Point(((877.3333) * transformX) + moveX, ((1408.0000) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1408.0000) * transformY) + moveY),


        new createjs.Point(((876.3334) * transformX) + moveX, ((1411.3330) * transformY) + moveY),
        new createjs.Point(((875.6666) * transformX) + moveX, ((1414.6670) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1418.0000) * transformY) + moveY),


        new createjs.Point(((874.6667) * transformX) + moveX, ((1418.0000) * transformY) + moveY),
        new createjs.Point(((874.3333) * transformX) + moveX, ((1418.0000) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1418.0000) * transformY) + moveY),


        new createjs.Point(((874.0000) * transformX) + moveX, ((1418.6666) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1419.3334) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1420.0000) * transformY) + moveY),


        new createjs.Point(((873.6667) * transformX) + moveX, ((1420.0000) * transformY) + moveY),
        new createjs.Point(((873.3333) * transformX) + moveX, ((1420.0000) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((1420.0000) * transformY) + moveY),


        new createjs.Point(((873.0000) * transformX) + moveX, ((1420.9999) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((1422.0001) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((1423.0000) * transformY) + moveY),


        new createjs.Point(((872.6667) * transformX) + moveX, ((1423.0000) * transformY) + moveY),
        new createjs.Point(((872.3333) * transformX) + moveX, ((1423.0000) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1423.0000) * transformY) + moveY),


        new createjs.Point(((872.0000) * transformX) + moveX, ((1423.9999) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1425.0001) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),


        new createjs.Point(((871.6667) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((871.3333) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),


        new createjs.Point(((871.0000) * transformX) + moveX, ((1426.9999) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1428.0001) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1429.0000) * transformY) + moveY),


        new createjs.Point(((870.6667) * transformX) + moveX, ((1429.0000) * transformY) + moveY),
        new createjs.Point(((870.3333) * transformX) + moveX, ((1429.0000) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1429.0000) * transformY) + moveY),


        new createjs.Point(((870.0000) * transformX) + moveX, ((1429.9999) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1431.0001) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1432.0000) * transformY) + moveY),


        new createjs.Point(((869.6667) * transformX) + moveX, ((1432.0000) * transformY) + moveY),
        new createjs.Point(((869.3333) * transformX) + moveX, ((1432.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1432.0000) * transformY) + moveY),


        new createjs.Point(((869.0000) * transformX) + moveX, ((1432.6666) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1433.3334) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1434.0000) * transformY) + moveY),


        new createjs.Point(((868.6667) * transformX) + moveX, ((1434.0000) * transformY) + moveY),
        new createjs.Point(((868.3333) * transformX) + moveX, ((1434.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1434.0000) * transformY) + moveY),


        new createjs.Point(((867.6667) * transformX) + moveX, ((1435.9998) * transformY) + moveY),
        new createjs.Point(((867.3333) * transformX) + moveX, ((1438.0002) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1440.0000) * transformY) + moveY),


        new createjs.Point(((866.6667) * transformX) + moveX, ((1440.0000) * transformY) + moveY),
        new createjs.Point(((866.3333) * transformX) + moveX, ((1440.0000) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((1440.0000) * transformY) + moveY),


        new createjs.Point(((866.0000) * transformX) + moveX, ((1440.6666) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((1441.3334) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((1442.0000) * transformY) + moveY),


        new createjs.Point(((865.6667) * transformX) + moveX, ((1442.0000) * transformY) + moveY),
        new createjs.Point(((865.3333) * transformX) + moveX, ((1442.0000) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1442.0000) * transformY) + moveY),


        new createjs.Point(((865.0000) * transformX) + moveX, ((1442.9999) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1444.0001) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1445.0000) * transformY) + moveY),


        new createjs.Point(((864.6667) * transformX) + moveX, ((1445.0000) * transformY) + moveY),
        new createjs.Point(((864.3333) * transformX) + moveX, ((1445.0000) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1445.0000) * transformY) + moveY),


        new createjs.Point(((864.0000) * transformX) + moveX, ((1445.6666) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1446.3334) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1447.0000) * transformY) + moveY),


        new createjs.Point(((863.6667) * transformX) + moveX, ((1447.0000) * transformY) + moveY),
        new createjs.Point(((863.3333) * transformX) + moveX, ((1447.0000) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((1447.0000) * transformY) + moveY),


        new createjs.Point(((863.0000) * transformX) + moveX, ((1447.9999) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((1449.0001) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((1450.0000) * transformY) + moveY),


        new createjs.Point(((862.6667) * transformX) + moveX, ((1450.0000) * transformY) + moveY),
        new createjs.Point(((862.3333) * transformX) + moveX, ((1450.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1450.0000) * transformY) + moveY),


        new createjs.Point(((862.0000) * transformX) + moveX, ((1450.6666) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1451.3334) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1452.0000) * transformY) + moveY),


        new createjs.Point(((861.6667) * transformX) + moveX, ((1452.0000) * transformY) + moveY),
        new createjs.Point(((861.3333) * transformX) + moveX, ((1452.0000) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1452.0000) * transformY) + moveY),


        new createjs.Point(((861.0000) * transformX) + moveX, ((1452.9999) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1454.0001) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1455.0000) * transformY) + moveY),


        new createjs.Point(((860.6667) * transformX) + moveX, ((1455.0000) * transformY) + moveY),
        new createjs.Point(((860.3333) * transformX) + moveX, ((1455.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1455.0000) * transformY) + moveY),


        new createjs.Point(((859.6667) * transformX) + moveX, ((1456.3332) * transformY) + moveY),
        new createjs.Point(((859.3333) * transformX) + moveX, ((1457.6668) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1459.0000) * transformY) + moveY),


        new createjs.Point(((858.6667) * transformX) + moveX, ((1459.0000) * transformY) + moveY),
        new createjs.Point(((858.3333) * transformX) + moveX, ((1459.0000) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1459.0000) * transformY) + moveY),


        new createjs.Point(((858.0000) * transformX) + moveX, ((1459.9999) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1461.0001) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1462.0000) * transformY) + moveY),


        new createjs.Point(((857.6667) * transformX) + moveX, ((1462.0000) * transformY) + moveY),
        new createjs.Point(((857.3333) * transformX) + moveX, ((1462.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1462.0000) * transformY) + moveY),


        new createjs.Point(((856.6667) * transformX) + moveX, ((1463.3332) * transformY) + moveY),
        new createjs.Point(((856.3333) * transformX) + moveX, ((1464.6668) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1466.0000) * transformY) + moveY),


        new createjs.Point(((855.6667) * transformX) + moveX, ((1466.0000) * transformY) + moveY),
        new createjs.Point(((855.3333) * transformX) + moveX, ((1466.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1466.0000) * transformY) + moveY),


        new createjs.Point(((855.0000) * transformX) + moveX, ((1466.9999) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1468.0001) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1469.0000) * transformY) + moveY),


        new createjs.Point(((854.6667) * transformX) + moveX, ((1469.0000) * transformY) + moveY),
        new createjs.Point(((854.3333) * transformX) + moveX, ((1469.0000) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1469.0000) * transformY) + moveY),


        new createjs.Point(((854.0000) * transformX) + moveX, ((1469.6666) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1470.3334) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1471.0000) * transformY) + moveY),


        new createjs.Point(((853.6667) * transformX) + moveX, ((1471.0000) * transformY) + moveY),
        new createjs.Point(((853.3333) * transformX) + moveX, ((1471.0000) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1471.0000) * transformY) + moveY),


        new createjs.Point(((852.3334) * transformX) + moveX, ((1472.9998) * transformY) + moveY),
        new createjs.Point(((851.6666) * transformX) + moveX, ((1475.0002) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1477.0000) * transformY) + moveY),


        new createjs.Point(((850.6667) * transformX) + moveX, ((1477.0000) * transformY) + moveY),
        new createjs.Point(((850.3333) * transformX) + moveX, ((1477.0000) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1477.0000) * transformY) + moveY),


        new createjs.Point(((850.0000) * transformX) + moveX, ((1477.9999) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1479.0001) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1480.0000) * transformY) + moveY),


        new createjs.Point(((849.6667) * transformX) + moveX, ((1480.0000) * transformY) + moveY),
        new createjs.Point(((849.3333) * transformX) + moveX, ((1480.0000) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1480.0000) * transformY) + moveY),


        new createjs.Point(((849.0000) * transformX) + moveX, ((1480.6666) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1481.3334) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1482.0000) * transformY) + moveY),


        new createjs.Point(((848.6667) * transformX) + moveX, ((1482.0000) * transformY) + moveY),
        new createjs.Point(((848.3333) * transformX) + moveX, ((1482.0000) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((1482.0000) * transformY) + moveY),


        new createjs.Point(((847.6667) * transformX) + moveX, ((1483.3332) * transformY) + moveY),
        new createjs.Point(((847.3333) * transformX) + moveX, ((1484.6668) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1486.0000) * transformY) + moveY),


        new createjs.Point(((846.6667) * transformX) + moveX, ((1486.0000) * transformY) + moveY),
        new createjs.Point(((846.3333) * transformX) + moveX, ((1486.0000) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1486.0000) * transformY) + moveY),


        new createjs.Point(((846.0000) * transformX) + moveX, ((1486.6666) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1487.3334) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1488.0000) * transformY) + moveY),


        new createjs.Point(((845.6667) * transformX) + moveX, ((1488.0000) * transformY) + moveY),
        new createjs.Point(((845.3333) * transformX) + moveX, ((1488.0000) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1488.0000) * transformY) + moveY),


        new createjs.Point(((845.0000) * transformX) + moveX, ((1488.6666) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1489.3334) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1490.0000) * transformY) + moveY),


        new createjs.Point(((844.6667) * transformX) + moveX, ((1490.0000) * transformY) + moveY),
        new createjs.Point(((844.3333) * transformX) + moveX, ((1490.0000) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1490.0000) * transformY) + moveY),


        new createjs.Point(((844.0000) * transformX) + moveX, ((1490.6666) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1491.3334) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1492.0000) * transformY) + moveY),


        new createjs.Point(((843.6667) * transformX) + moveX, ((1492.0000) * transformY) + moveY),
        new createjs.Point(((843.3333) * transformX) + moveX, ((1492.0000) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1492.0000) * transformY) + moveY),


        new createjs.Point(((843.0000) * transformX) + moveX, ((1492.6666) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1493.3334) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1494.0000) * transformY) + moveY),


        new createjs.Point(((842.6667) * transformX) + moveX, ((1494.0000) * transformY) + moveY),
        new createjs.Point(((842.3333) * transformX) + moveX, ((1494.0000) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1494.0000) * transformY) + moveY),


        new createjs.Point(((842.0000) * transformX) + moveX, ((1494.6666) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1495.3334) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1496.0000) * transformY) + moveY),


        new createjs.Point(((841.6667) * transformX) + moveX, ((1496.0000) * transformY) + moveY),
        new createjs.Point(((841.3333) * transformX) + moveX, ((1496.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1496.0000) * transformY) + moveY),


        new createjs.Point(((841.0000) * transformX) + moveX, ((1496.6666) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1497.3334) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1498.0000) * transformY) + moveY),


        new createjs.Point(((840.6667) * transformX) + moveX, ((1498.0000) * transformY) + moveY),
        new createjs.Point(((840.3333) * transformX) + moveX, ((1498.0000) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1498.0000) * transformY) + moveY),


        new createjs.Point(((840.0000) * transformX) + moveX, ((1498.6666) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1499.3334) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1500.0000) * transformY) + moveY),


        new createjs.Point(((839.6667) * transformX) + moveX, ((1500.0000) * transformY) + moveY),
        new createjs.Point(((839.3333) * transformX) + moveX, ((1500.0000) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1500.0000) * transformY) + moveY),


        new createjs.Point(((839.0000) * transformX) + moveX, ((1500.6666) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1501.3334) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1502.0000) * transformY) + moveY),


        new createjs.Point(((838.6667) * transformX) + moveX, ((1502.0000) * transformY) + moveY),
        new createjs.Point(((838.3333) * transformX) + moveX, ((1502.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1502.0000) * transformY) + moveY),


        new createjs.Point(((838.0000) * transformX) + moveX, ((1502.6666) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1503.3334) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1504.0000) * transformY) + moveY),


        new createjs.Point(((837.6667) * transformX) + moveX, ((1504.0000) * transformY) + moveY),
        new createjs.Point(((837.3333) * transformX) + moveX, ((1504.0000) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1504.0000) * transformY) + moveY),


        new createjs.Point(((837.0000) * transformX) + moveX, ((1504.6666) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1505.3334) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1506.0000) * transformY) + moveY),


        new createjs.Point(((836.6667) * transformX) + moveX, ((1506.0000) * transformY) + moveY),
        new createjs.Point(((836.3333) * transformX) + moveX, ((1506.0000) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1506.0000) * transformY) + moveY),


        new createjs.Point(((836.0000) * transformX) + moveX, ((1506.6666) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1507.3334) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1508.0000) * transformY) + moveY),


        new createjs.Point(((835.3334) * transformX) + moveX, ((1508.3333) * transformY) + moveY),
        new createjs.Point(((834.6666) * transformX) + moveX, ((1508.6667) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1509.0000) * transformY) + moveY),


        new createjs.Point(((833.6667) * transformX) + moveX, ((1510.3332) * transformY) + moveY),
        new createjs.Point(((833.3333) * transformX) + moveX, ((1511.6668) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1513.0000) * transformY) + moveY),


        new createjs.Point(((832.6667) * transformX) + moveX, ((1513.0000) * transformY) + moveY),
        new createjs.Point(((832.3333) * transformX) + moveX, ((1513.0000) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1513.0000) * transformY) + moveY),


        new createjs.Point(((832.0000) * transformX) + moveX, ((1513.6666) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1514.3334) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1515.0000) * transformY) + moveY),


        new createjs.Point(((831.6667) * transformX) + moveX, ((1515.0000) * transformY) + moveY),
        new createjs.Point(((831.3333) * transformX) + moveX, ((1515.0000) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1515.0000) * transformY) + moveY),


        new createjs.Point(((831.0000) * transformX) + moveX, ((1515.6666) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1516.3334) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1517.0000) * transformY) + moveY),


        new createjs.Point(((830.6667) * transformX) + moveX, ((1517.0000) * transformY) + moveY),
        new createjs.Point(((830.3333) * transformX) + moveX, ((1517.0000) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1517.0000) * transformY) + moveY),


        new createjs.Point(((830.0000) * transformX) + moveX, ((1517.6666) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1518.3334) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1519.0000) * transformY) + moveY),


        new createjs.Point(((829.3334) * transformX) + moveX, ((1519.3333) * transformY) + moveY),
        new createjs.Point(((828.6666) * transformX) + moveX, ((1519.6667) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1520.0000) * transformY) + moveY),


        new createjs.Point(((827.3334) * transformX) + moveX, ((1521.9998) * transformY) + moveY),
        new createjs.Point(((826.6666) * transformX) + moveX, ((1524.0002) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1526.0000) * transformY) + moveY),


        new createjs.Point(((825.3334) * transformX) + moveX, ((1526.3333) * transformY) + moveY),
        new createjs.Point(((824.6666) * transformX) + moveX, ((1526.6667) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((1527.0000) * transformY) + moveY),


        new createjs.Point(((823.6667) * transformX) + moveX, ((1528.3332) * transformY) + moveY),
        new createjs.Point(((823.3333) * transformX) + moveX, ((1529.6668) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((1531.0000) * transformY) + moveY),


        new createjs.Point(((822.3334) * transformX) + moveX, ((1531.3333) * transformY) + moveY),
        new createjs.Point(((821.6666) * transformX) + moveX, ((1531.6667) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1532.0000) * transformY) + moveY),


        new createjs.Point(((820.6667) * transformX) + moveX, ((1533.3332) * transformY) + moveY),
        new createjs.Point(((820.3333) * transformX) + moveX, ((1534.6668) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1536.0000) * transformY) + moveY),


        new createjs.Point(((819.3334) * transformX) + moveX, ((1536.3333) * transformY) + moveY),
        new createjs.Point(((818.6666) * transformX) + moveX, ((1536.6667) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((1537.0000) * transformY) + moveY),


        new createjs.Point(((817.6667) * transformX) + moveX, ((1538.3332) * transformY) + moveY),
        new createjs.Point(((817.3333) * transformX) + moveX, ((1539.6668) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((1541.0000) * transformY) + moveY),


        new createjs.Point(((816.3334) * transformX) + moveX, ((1541.3333) * transformY) + moveY),
        new createjs.Point(((815.6666) * transformX) + moveX, ((1541.6667) * transformY) + moveY),
        new createjs.Point(((815.0000) * transformX) + moveX, ((1542.0000) * transformY) + moveY),


        new createjs.Point(((814.6667) * transformX) + moveX, ((1543.3332) * transformY) + moveY),
        new createjs.Point(((814.3333) * transformX) + moveX, ((1544.6668) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((1546.0000) * transformY) + moveY),


        new createjs.Point(((813.3334) * transformX) + moveX, ((1546.3333) * transformY) + moveY),
        new createjs.Point(((812.6666) * transformX) + moveX, ((1546.6667) * transformY) + moveY),
        new createjs.Point(((812.0000) * transformX) + moveX, ((1547.0000) * transformY) + moveY),


        new createjs.Point(((812.0000) * transformX) + moveX, ((1547.6666) * transformY) + moveY),
        new createjs.Point(((812.0000) * transformX) + moveX, ((1548.3334) * transformY) + moveY),
        new createjs.Point(((812.0000) * transformX) + moveX, ((1549.0000) * transformY) + moveY),


        new createjs.Point(((811.3334) * transformX) + moveX, ((1549.3333) * transformY) + moveY),
        new createjs.Point(((810.6666) * transformX) + moveX, ((1549.6667) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((1550.0000) * transformY) + moveY),


        new createjs.Point(((809.6667) * transformX) + moveX, ((1550.9999) * transformY) + moveY),
        new createjs.Point(((809.3333) * transformX) + moveX, ((1552.0001) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((1553.0000) * transformY) + moveY),


        new createjs.Point(((808.6667) * transformX) + moveX, ((1553.0000) * transformY) + moveY),
        new createjs.Point(((808.3333) * transformX) + moveX, ((1553.0000) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1553.0000) * transformY) + moveY),


        new createjs.Point(((807.6667) * transformX) + moveX, ((1554.3332) * transformY) + moveY),
        new createjs.Point(((807.3333) * transformX) + moveX, ((1555.6668) * transformY) + moveY),
        new createjs.Point(((807.0000) * transformX) + moveX, ((1557.0000) * transformY) + moveY),


        new createjs.Point(((806.6667) * transformX) + moveX, ((1557.0000) * transformY) + moveY),
        new createjs.Point(((806.3333) * transformX) + moveX, ((1557.0000) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1557.0000) * transformY) + moveY),


        new createjs.Point(((805.6667) * transformX) + moveX, ((1557.9999) * transformY) + moveY),
        new createjs.Point(((805.3333) * transformX) + moveX, ((1559.0001) * transformY) + moveY),
        new createjs.Point(((805.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),


        new createjs.Point(((804.3334) * transformX) + moveX, ((1560.3333) * transformY) + moveY),
        new createjs.Point(((803.6666) * transformX) + moveX, ((1560.6667) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1561.0000) * transformY) + moveY),


        new createjs.Point(((803.0000) * transformX) + moveX, ((1561.6666) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1562.3334) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1563.0000) * transformY) + moveY),


        new createjs.Point(((802.0001) * transformX) + moveX, ((1563.6666) * transformY) + moveY),
        new createjs.Point(((800.9999) * transformX) + moveX, ((1564.3334) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((1565.0000) * transformY) + moveY),


        new createjs.Point(((800.0000) * transformX) + moveX, ((1565.6666) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((1566.3334) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((1567.0000) * transformY) + moveY),


        new createjs.Point(((799.3334) * transformX) + moveX, ((1567.3333) * transformY) + moveY),
        new createjs.Point(((798.6666) * transformX) + moveX, ((1567.6667) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((1568.0000) * transformY) + moveY),


        new createjs.Point(((798.0000) * transformX) + moveX, ((1568.6666) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((1569.3334) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((1570.0000) * transformY) + moveY),


        new createjs.Point(((797.6667) * transformX) + moveX, ((1570.0000) * transformY) + moveY),
        new createjs.Point(((797.3333) * transformX) + moveX, ((1570.0000) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1570.0000) * transformY) + moveY),


        new createjs.Point(((796.6667) * transformX) + moveX, ((1570.9999) * transformY) + moveY),
        new createjs.Point(((796.3333) * transformX) + moveX, ((1572.0001) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((1573.0000) * transformY) + moveY),


        new createjs.Point(((795.3334) * transformX) + moveX, ((1573.3333) * transformY) + moveY),
        new createjs.Point(((794.6666) * transformX) + moveX, ((1573.6667) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1574.0000) * transformY) + moveY),


        new createjs.Point(((794.0000) * transformX) + moveX, ((1574.6666) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1575.3334) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1576.0000) * transformY) + moveY),


        new createjs.Point(((793.3334) * transformX) + moveX, ((1576.3333) * transformY) + moveY),
        new createjs.Point(((792.6666) * transformX) + moveX, ((1576.6667) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((1577.0000) * transformY) + moveY),


        new createjs.Point(((791.6667) * transformX) + moveX, ((1577.9999) * transformY) + moveY),
        new createjs.Point(((791.3333) * transformX) + moveX, ((1579.0001) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1580.0000) * transformY) + moveY),


        new createjs.Point(((790.0001) * transformX) + moveX, ((1580.6666) * transformY) + moveY),
        new createjs.Point(((788.9999) * transformX) + moveX, ((1581.3334) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1582.0000) * transformY) + moveY),


        new createjs.Point(((788.0000) * transformX) + moveX, ((1582.6666) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1583.3334) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1584.0000) * transformY) + moveY),


        new createjs.Point(((787.3334) * transformX) + moveX, ((1584.3333) * transformY) + moveY),
        new createjs.Point(((786.6666) * transformX) + moveX, ((1584.6667) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1585.0000) * transformY) + moveY),


        new createjs.Point(((786.0000) * transformX) + moveX, ((1585.6666) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1586.3334) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1587.0000) * transformY) + moveY),


        new createjs.Point(((785.0001) * transformX) + moveX, ((1587.6666) * transformY) + moveY),
        new createjs.Point(((783.9999) * transformX) + moveX, ((1588.3334) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1589.0000) * transformY) + moveY),


        new createjs.Point(((783.0000) * transformX) + moveX, ((1589.6666) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1590.3334) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1591.0000) * transformY) + moveY),


        new createjs.Point(((782.0001) * transformX) + moveX, ((1591.6666) * transformY) + moveY),
        new createjs.Point(((780.9999) * transformX) + moveX, ((1592.3334) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((1593.0000) * transformY) + moveY),


        new createjs.Point(((780.0000) * transformX) + moveX, ((1593.6666) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((1594.3334) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((1595.0000) * transformY) + moveY),


        new createjs.Point(((778.6668) * transformX) + moveX, ((1595.9999) * transformY) + moveY),
        new createjs.Point(((777.3332) * transformX) + moveX, ((1597.0001) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((1598.0000) * transformY) + moveY),


        new createjs.Point(((776.0000) * transformX) + moveX, ((1598.6666) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((1599.3334) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((1600.0000) * transformY) + moveY),


        new createjs.Point(((774.6668) * transformX) + moveX, ((1600.9999) * transformY) + moveY),
        new createjs.Point(((773.3332) * transformX) + moveX, ((1602.0001) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),


        new createjs.Point(((772.0000) * transformX) + moveX, ((1603.6666) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((1604.3334) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((1605.0000) * transformY) + moveY),


        new createjs.Point(((770.6668) * transformX) + moveX, ((1605.9999) * transformY) + moveY),
        new createjs.Point(((769.3332) * transformX) + moveX, ((1607.0001) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((1608.0000) * transformY) + moveY),


        new createjs.Point(((768.0000) * transformX) + moveX, ((1608.6666) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((1609.3334) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((1610.0000) * transformY) + moveY),


        new createjs.Point(((767.3334) * transformX) + moveX, ((1610.3333) * transformY) + moveY),
        new createjs.Point(((766.6666) * transformX) + moveX, ((1610.6667) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((1611.0000) * transformY) + moveY),


        new createjs.Point(((765.3334) * transformX) + moveX, ((1611.9999) * transformY) + moveY),
        new createjs.Point(((764.6666) * transformX) + moveX, ((1613.0001) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((1614.0000) * transformY) + moveY),


        new createjs.Point(((763.6667) * transformX) + moveX, ((1614.0000) * transformY) + moveY),
        new createjs.Point(((763.3333) * transformX) + moveX, ((1614.0000) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1614.0000) * transformY) + moveY),


        new createjs.Point(((763.0000) * transformX) + moveX, ((1614.6666) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1615.3334) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),


        new createjs.Point(((761.3335) * transformX) + moveX, ((1617.3332) * transformY) + moveY),
        new createjs.Point(((759.6665) * transformX) + moveX, ((1618.6668) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((1620.0000) * transformY) + moveY),


        new createjs.Point(((758.0000) * transformX) + moveX, ((1620.6666) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((1621.3334) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((1622.0000) * transformY) + moveY),


        new createjs.Point(((755.6669) * transformX) + moveX, ((1623.9998) * transformY) + moveY),
        new createjs.Point(((753.3331) * transformX) + moveX, ((1626.0002) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((1628.0000) * transformY) + moveY),


        new createjs.Point(((751.0000) * transformX) + moveX, ((1628.6666) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((1629.3334) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((1630.0000) * transformY) + moveY),


        new createjs.Point(((747.6670) * transformX) + moveX, ((1632.9997) * transformY) + moveY),
        new createjs.Point(((744.3330) * transformX) + moveX, ((1636.0003) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((1639.0000) * transformY) + moveY),


        new createjs.Point(((741.0000) * transformX) + moveX, ((1639.6666) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((1640.3334) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((1641.0000) * transformY) + moveY),


        new createjs.Point(((737.3337) * transformX) + moveX, ((1644.3330) * transformY) + moveY),
        new createjs.Point(((733.6663) * transformX) + moveX, ((1647.6670) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((1651.0000) * transformY) + moveY),


        new createjs.Point(((722.7038) * transformX) + moveX, ((1658.2933) * transformY) + moveY),
        new createjs.Point(((716.5164) * transformX) + moveX, ((1665.8413) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((1672.0000) * transformY) + moveY),


        new createjs.Point(((705.3336) * transformX) + moveX, ((1674.9997) * transformY) + moveY),
        new createjs.Point(((702.6664) * transformX) + moveX, ((1678.0003) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((1681.0000) * transformY) + moveY),


        new createjs.Point(((699.3334) * transformX) + moveX, ((1681.0000) * transformY) + moveY),
        new createjs.Point(((698.6666) * transformX) + moveX, ((1681.0000) * transformY) + moveY),
        new createjs.Point(((698.0000) * transformX) + moveX, ((1681.0000) * transformY) + moveY),


        new createjs.Point(((696.0002) * transformX) + moveX, ((1683.3331) * transformY) + moveY),
        new createjs.Point(((693.9998) * transformX) + moveX, ((1685.6669) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((1688.0000) * transformY) + moveY),


        new createjs.Point(((691.3334) * transformX) + moveX, ((1688.0000) * transformY) + moveY),
        new createjs.Point(((690.6666) * transformX) + moveX, ((1688.0000) * transformY) + moveY),
        new createjs.Point(((690.0000) * transformX) + moveX, ((1688.0000) * transformY) + moveY),


        new createjs.Point(((688.3335) * transformX) + moveX, ((1689.9998) * transformY) + moveY),
        new createjs.Point(((686.6665) * transformX) + moveX, ((1692.0002) * transformY) + moveY),
        new createjs.Point(((685.0000) * transformX) + moveX, ((1694.0000) * transformY) + moveY),


        new createjs.Point(((684.3334) * transformX) + moveX, ((1694.0000) * transformY) + moveY),
        new createjs.Point(((683.6666) * transformX) + moveX, ((1694.0000) * transformY) + moveY),
        new createjs.Point(((683.0000) * transformX) + moveX, ((1694.0000) * transformY) + moveY),


        new createjs.Point(((682.0001) * transformX) + moveX, ((1695.3332) * transformY) + moveY),
        new createjs.Point(((680.9999) * transformX) + moveX, ((1696.6668) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((1698.0000) * transformY) + moveY),


        new createjs.Point(((679.3334) * transformX) + moveX, ((1698.0000) * transformY) + moveY),
        new createjs.Point(((678.6666) * transformX) + moveX, ((1698.0000) * transformY) + moveY),
        new createjs.Point(((678.0000) * transformX) + moveX, ((1698.0000) * transformY) + moveY),


        new createjs.Point(((677.0001) * transformX) + moveX, ((1699.3332) * transformY) + moveY),
        new createjs.Point(((675.9999) * transformX) + moveX, ((1700.6668) * transformY) + moveY),
        new createjs.Point(((675.0000) * transformX) + moveX, ((1702.0000) * transformY) + moveY),


        new createjs.Point(((674.3334) * transformX) + moveX, ((1702.0000) * transformY) + moveY),
        new createjs.Point(((673.6666) * transformX) + moveX, ((1702.0000) * transformY) + moveY),
        new createjs.Point(((673.0000) * transformX) + moveX, ((1702.0000) * transformY) + moveY),


        new createjs.Point(((672.0001) * transformX) + moveX, ((1703.3332) * transformY) + moveY),
        new createjs.Point(((670.9999) * transformX) + moveX, ((1704.6668) * transformY) + moveY),
        new createjs.Point(((670.0000) * transformX) + moveX, ((1706.0000) * transformY) + moveY),


        new createjs.Point(((669.3334) * transformX) + moveX, ((1706.0000) * transformY) + moveY),
        new createjs.Point(((668.6666) * transformX) + moveX, ((1706.0000) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((1706.0000) * transformY) + moveY),


        new createjs.Point(((667.3334) * transformX) + moveX, ((1706.9999) * transformY) + moveY),
        new createjs.Point(((666.6666) * transformX) + moveX, ((1708.0001) * transformY) + moveY),
        new createjs.Point(((666.0000) * transformX) + moveX, ((1709.0000) * transformY) + moveY),


        new createjs.Point(((665.3334) * transformX) + moveX, ((1709.0000) * transformY) + moveY),
        new createjs.Point(((664.6666) * transformX) + moveX, ((1709.0000) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((1709.0000) * transformY) + moveY),


        new createjs.Point(((663.0001) * transformX) + moveX, ((1710.3332) * transformY) + moveY),
        new createjs.Point(((661.9999) * transformX) + moveX, ((1711.6668) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((1713.0000) * transformY) + moveY),


        new createjs.Point(((660.0001) * transformX) + moveX, ((1713.3333) * transformY) + moveY),
        new createjs.Point(((658.9999) * transformX) + moveX, ((1713.6667) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((1714.0000) * transformY) + moveY),


        new createjs.Point(((657.6667) * transformX) + moveX, ((1714.6666) * transformY) + moveY),
        new createjs.Point(((657.3333) * transformX) + moveX, ((1715.3334) * transformY) + moveY),
        new createjs.Point(((657.0000) * transformX) + moveX, ((1716.0000) * transformY) + moveY),


        new createjs.Point(((656.3334) * transformX) + moveX, ((1716.0000) * transformY) + moveY),
        new createjs.Point(((655.6666) * transformX) + moveX, ((1716.0000) * transformY) + moveY),
        new createjs.Point(((655.0000) * transformX) + moveX, ((1716.0000) * transformY) + moveY),


        new createjs.Point(((654.6667) * transformX) + moveX, ((1716.6666) * transformY) + moveY),
        new createjs.Point(((654.3333) * transformX) + moveX, ((1717.3334) * transformY) + moveY),
        new createjs.Point(((654.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),


        new createjs.Point(((653.3334) * transformX) + moveX, ((1718.0000) * transformY) + moveY),
        new createjs.Point(((652.6666) * transformX) + moveX, ((1718.0000) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),


        new createjs.Point(((651.3334) * transformX) + moveX, ((1718.9999) * transformY) + moveY),
        new createjs.Point(((650.6666) * transformX) + moveX, ((1720.0001) * transformY) + moveY),
        new createjs.Point(((650.0000) * transformX) + moveX, ((1721.0000) * transformY) + moveY),


        new createjs.Point(((649.3334) * transformX) + moveX, ((1721.0000) * transformY) + moveY),
        new createjs.Point(((648.6666) * transformX) + moveX, ((1721.0000) * transformY) + moveY),
        new createjs.Point(((648.0000) * transformX) + moveX, ((1721.0000) * transformY) + moveY),


        new createjs.Point(((647.6667) * transformX) + moveX, ((1721.6666) * transformY) + moveY),
        new createjs.Point(((647.3333) * transformX) + moveX, ((1722.3334) * transformY) + moveY),
        new createjs.Point(((647.0000) * transformX) + moveX, ((1723.0000) * transformY) + moveY),


        new createjs.Point(((646.3334) * transformX) + moveX, ((1723.0000) * transformY) + moveY),
        new createjs.Point(((645.6666) * transformX) + moveX, ((1723.0000) * transformY) + moveY),
        new createjs.Point(((645.0000) * transformX) + moveX, ((1723.0000) * transformY) + moveY),


        new createjs.Point(((644.3334) * transformX) + moveX, ((1723.9999) * transformY) + moveY),
        new createjs.Point(((643.6666) * transformX) + moveX, ((1725.0001) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((1726.0000) * transformY) + moveY),


        new createjs.Point(((642.3334) * transformX) + moveX, ((1726.0000) * transformY) + moveY),
        new createjs.Point(((641.6666) * transformX) + moveX, ((1726.0000) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((1726.0000) * transformY) + moveY),


        new createjs.Point(((640.6667) * transformX) + moveX, ((1726.6666) * transformY) + moveY),
        new createjs.Point(((640.3333) * transformX) + moveX, ((1727.3334) * transformY) + moveY),
        new createjs.Point(((640.0000) * transformX) + moveX, ((1728.0000) * transformY) + moveY),


        new createjs.Point(((639.0001) * transformX) + moveX, ((1728.3333) * transformY) + moveY),
        new createjs.Point(((637.9999) * transformX) + moveX, ((1728.6667) * transformY) + moveY),
        new createjs.Point(((637.0000) * transformX) + moveX, ((1729.0000) * transformY) + moveY),


        new createjs.Point(((637.0000) * transformX) + moveX, ((1729.3333) * transformY) + moveY),
        new createjs.Point(((637.0000) * transformX) + moveX, ((1729.6667) * transformY) + moveY),
        new createjs.Point(((637.0000) * transformX) + moveX, ((1730.0000) * transformY) + moveY),


        new createjs.Point(((636.3334) * transformX) + moveX, ((1730.0000) * transformY) + moveY),
        new createjs.Point(((635.6666) * transformX) + moveX, ((1730.0000) * transformY) + moveY),
        new createjs.Point(((635.0000) * transformX) + moveX, ((1730.0000) * transformY) + moveY),


        new createjs.Point(((634.6667) * transformX) + moveX, ((1730.6666) * transformY) + moveY),
        new createjs.Point(((634.3333) * transformX) + moveX, ((1731.3334) * transformY) + moveY),
        new createjs.Point(((634.0000) * transformX) + moveX, ((1732.0000) * transformY) + moveY),


        new createjs.Point(((633.3334) * transformX) + moveX, ((1732.0000) * transformY) + moveY),
        new createjs.Point(((632.6666) * transformX) + moveX, ((1732.0000) * transformY) + moveY),
        new createjs.Point(((632.0000) * transformX) + moveX, ((1732.0000) * transformY) + moveY),


        new createjs.Point(((631.6667) * transformX) + moveX, ((1732.6666) * transformY) + moveY),
        new createjs.Point(((631.3333) * transformX) + moveX, ((1733.3334) * transformY) + moveY),
        new createjs.Point(((631.0000) * transformX) + moveX, ((1734.0000) * transformY) + moveY),


        new createjs.Point(((630.3334) * transformX) + moveX, ((1734.0000) * transformY) + moveY),
        new createjs.Point(((629.6666) * transformX) + moveX, ((1734.0000) * transformY) + moveY),
        new createjs.Point(((629.0000) * transformX) + moveX, ((1734.0000) * transformY) + moveY),


        new createjs.Point(((628.6667) * transformX) + moveX, ((1734.6666) * transformY) + moveY),
        new createjs.Point(((628.3333) * transformX) + moveX, ((1735.3334) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),


        new createjs.Point(((627.3334) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((626.6666) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),


        new createjs.Point(((625.6667) * transformX) + moveX, ((1736.6666) * transformY) + moveY),
        new createjs.Point(((625.3333) * transformX) + moveX, ((1737.3334) * transformY) + moveY),
        new createjs.Point(((625.0000) * transformX) + moveX, ((1738.0000) * transformY) + moveY),


        new createjs.Point(((623.6668) * transformX) + moveX, ((1738.3333) * transformY) + moveY),
        new createjs.Point(((622.3332) * transformX) + moveX, ((1738.6667) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((1739.0000) * transformY) + moveY),


        new createjs.Point(((620.6667) * transformX) + moveX, ((1739.6666) * transformY) + moveY),
        new createjs.Point(((620.3333) * transformX) + moveX, ((1740.3334) * transformY) + moveY),
        new createjs.Point(((620.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),


        new createjs.Point(((619.3334) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((618.6666) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((618.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),


        new createjs.Point(((617.6667) * transformX) + moveX, ((1741.6666) * transformY) + moveY),
        new createjs.Point(((617.3333) * transformX) + moveX, ((1742.3334) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),


        new createjs.Point(((615.6668) * transformX) + moveX, ((1743.3333) * transformY) + moveY),
        new createjs.Point(((614.3332) * transformX) + moveX, ((1743.6667) * transformY) + moveY),
        new createjs.Point(((613.0000) * transformX) + moveX, ((1744.0000) * transformY) + moveY),


        new createjs.Point(((612.6667) * transformX) + moveX, ((1744.6666) * transformY) + moveY),
        new createjs.Point(((612.3333) * transformX) + moveX, ((1745.3334) * transformY) + moveY),
        new createjs.Point(((612.0000) * transformX) + moveX, ((1746.0000) * transformY) + moveY),


        new createjs.Point(((610.6668) * transformX) + moveX, ((1746.3333) * transformY) + moveY),
        new createjs.Point(((609.3332) * transformX) + moveX, ((1746.6667) * transformY) + moveY),
        new createjs.Point(((608.0000) * transformX) + moveX, ((1747.0000) * transformY) + moveY),


        new createjs.Point(((607.6667) * transformX) + moveX, ((1747.6666) * transformY) + moveY),
        new createjs.Point(((607.3333) * transformX) + moveX, ((1748.3334) * transformY) + moveY),
        new createjs.Point(((607.0000) * transformX) + moveX, ((1749.0000) * transformY) + moveY),


        new createjs.Point(((605.6668) * transformX) + moveX, ((1749.3333) * transformY) + moveY),
        new createjs.Point(((604.3332) * transformX) + moveX, ((1749.6667) * transformY) + moveY),
        new createjs.Point(((603.0000) * transformX) + moveX, ((1750.0000) * transformY) + moveY),


        new createjs.Point(((602.6667) * transformX) + moveX, ((1750.6666) * transformY) + moveY),
        new createjs.Point(((602.3333) * transformX) + moveX, ((1751.3334) * transformY) + moveY),
        new createjs.Point(((602.0000) * transformX) + moveX, ((1752.0000) * transformY) + moveY),


        new createjs.Point(((601.3334) * transformX) + moveX, ((1752.0000) * transformY) + moveY),
        new createjs.Point(((600.6666) * transformX) + moveX, ((1752.0000) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1752.0000) * transformY) + moveY),


        new createjs.Point(((600.0000) * transformX) + moveX, ((1752.3333) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1752.6667) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1753.0000) * transformY) + moveY),


        new createjs.Point(((599.3334) * transformX) + moveX, ((1753.0000) * transformY) + moveY),
        new createjs.Point(((598.6666) * transformX) + moveX, ((1753.0000) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((1753.0000) * transformY) + moveY),


        new createjs.Point(((598.0000) * transformX) + moveX, ((1753.3333) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((1753.6667) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((1754.0000) * transformY) + moveY),


        new createjs.Point(((597.3334) * transformX) + moveX, ((1754.0000) * transformY) + moveY),
        new createjs.Point(((596.6666) * transformX) + moveX, ((1754.0000) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((1754.0000) * transformY) + moveY),


        new createjs.Point(((596.0000) * transformX) + moveX, ((1754.3333) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((1754.6667) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),


        new createjs.Point(((595.3334) * transformX) + moveX, ((1755.0000) * transformY) + moveY),
        new createjs.Point(((594.6666) * transformX) + moveX, ((1755.0000) * transformY) + moveY),
        new createjs.Point(((594.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),


        new createjs.Point(((593.6667) * transformX) + moveX, ((1755.6666) * transformY) + moveY),
        new createjs.Point(((593.3333) * transformX) + moveX, ((1756.3334) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((1757.0000) * transformY) + moveY),


        new createjs.Point(((592.3334) * transformX) + moveX, ((1757.0000) * transformY) + moveY),
        new createjs.Point(((591.6666) * transformX) + moveX, ((1757.0000) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((1757.0000) * transformY) + moveY),


        new createjs.Point(((591.0000) * transformX) + moveX, ((1757.3333) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((1757.6667) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),


        new createjs.Point(((590.3334) * transformX) + moveX, ((1758.0000) * transformY) + moveY),
        new createjs.Point(((589.6666) * transformX) + moveX, ((1758.0000) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),


        new createjs.Point(((589.0000) * transformX) + moveX, ((1758.3333) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1758.6667) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1759.0000) * transformY) + moveY),


        new createjs.Point(((588.3334) * transformX) + moveX, ((1759.0000) * transformY) + moveY),
        new createjs.Point(((587.6666) * transformX) + moveX, ((1759.0000) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1759.0000) * transformY) + moveY),


        new createjs.Point(((587.0000) * transformX) + moveX, ((1759.3333) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1759.6667) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),


        new createjs.Point(((586.3334) * transformX) + moveX, ((1760.0000) * transformY) + moveY),
        new createjs.Point(((585.6666) * transformX) + moveX, ((1760.0000) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),


        new createjs.Point(((585.0000) * transformX) + moveX, ((1760.3333) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1760.6667) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1761.0000) * transformY) + moveY),


        new createjs.Point(((583.6668) * transformX) + moveX, ((1761.3333) * transformY) + moveY),
        new createjs.Point(((582.3332) * transformX) + moveX, ((1761.6667) * transformY) + moveY),
        new createjs.Point(((581.0000) * transformX) + moveX, ((1762.0000) * transformY) + moveY),


        new createjs.Point(((580.6667) * transformX) + moveX, ((1762.6666) * transformY) + moveY),
        new createjs.Point(((580.3333) * transformX) + moveX, ((1763.3334) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1764.0000) * transformY) + moveY),


        new createjs.Point(((578.0002) * transformX) + moveX, ((1764.6666) * transformY) + moveY),
        new createjs.Point(((575.9998) * transformX) + moveX, ((1765.3334) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((1766.0000) * transformY) + moveY),


        new createjs.Point(((574.0000) * transformX) + moveX, ((1766.3333) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((1766.6667) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((1767.0000) * transformY) + moveY),


        new createjs.Point(((573.3334) * transformX) + moveX, ((1767.0000) * transformY) + moveY),
        new createjs.Point(((572.6666) * transformX) + moveX, ((1767.0000) * transformY) + moveY),
        new createjs.Point(((572.0000) * transformX) + moveX, ((1767.0000) * transformY) + moveY),


        new createjs.Point(((572.0000) * transformX) + moveX, ((1767.3333) * transformY) + moveY),
        new createjs.Point(((572.0000) * transformX) + moveX, ((1767.6667) * transformY) + moveY),
        new createjs.Point(((572.0000) * transformX) + moveX, ((1768.0000) * transformY) + moveY),


        new createjs.Point(((570.0002) * transformX) + moveX, ((1768.6666) * transformY) + moveY),
        new createjs.Point(((567.9998) * transformX) + moveX, ((1769.3334) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((1770.0000) * transformY) + moveY),


        new createjs.Point(((566.0000) * transformX) + moveX, ((1770.3333) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((1770.6667) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((1771.0000) * transformY) + moveY),


        new createjs.Point(((565.0001) * transformX) + moveX, ((1771.0000) * transformY) + moveY),
        new createjs.Point(((563.9999) * transformX) + moveX, ((1771.0000) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((1771.0000) * transformY) + moveY),


        new createjs.Point(((563.0000) * transformX) + moveX, ((1771.3333) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((1771.6667) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((1772.0000) * transformY) + moveY),


        new createjs.Point(((562.3334) * transformX) + moveX, ((1772.0000) * transformY) + moveY),
        new createjs.Point(((561.6666) * transformX) + moveX, ((1772.0000) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1772.0000) * transformY) + moveY),


        new createjs.Point(((561.0000) * transformX) + moveX, ((1772.3333) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1772.6667) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1773.0000) * transformY) + moveY),


        new createjs.Point(((560.3334) * transformX) + moveX, ((1773.0000) * transformY) + moveY),
        new createjs.Point(((559.6666) * transformX) + moveX, ((1773.0000) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1773.0000) * transformY) + moveY),


        new createjs.Point(((559.0000) * transformX) + moveX, ((1773.3333) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1773.6667) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1774.0000) * transformY) + moveY),


        new createjs.Point(((557.0002) * transformX) + moveX, ((1774.6666) * transformY) + moveY),
        new createjs.Point(((554.9998) * transformX) + moveX, ((1775.3334) * transformY) + moveY),
        new createjs.Point(((553.0000) * transformX) + moveX, ((1776.0000) * transformY) + moveY),


        new createjs.Point(((553.0000) * transformX) + moveX, ((1776.3333) * transformY) + moveY),
        new createjs.Point(((553.0000) * transformX) + moveX, ((1776.6667) * transformY) + moveY),
        new createjs.Point(((553.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),


        new createjs.Point(((552.0001) * transformX) + moveX, ((1777.0000) * transformY) + moveY),
        new createjs.Point(((550.9999) * transformX) + moveX, ((1777.0000) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),


        new createjs.Point(((550.0000) * transformX) + moveX, ((1777.3333) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1777.6667) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),


        new createjs.Point(((549.3334) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((548.6666) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),


        new createjs.Point(((548.0000) * transformX) + moveX, ((1778.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1778.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1779.0000) * transformY) + moveY),


        new createjs.Point(((546.6668) * transformX) + moveX, ((1779.3333) * transformY) + moveY),
        new createjs.Point(((545.3332) * transformX) + moveX, ((1779.6667) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),


        new createjs.Point(((544.0000) * transformX) + moveX, ((1780.3333) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1780.6667) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),


        new createjs.Point(((543.0001) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((541.9999) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),


        new createjs.Point(((541.0000) * transformX) + moveX, ((1781.3333) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((1781.6667) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),


        new createjs.Point(((540.3334) * transformX) + moveX, ((1782.0000) * transformY) + moveY),
        new createjs.Point(((539.6666) * transformX) + moveX, ((1782.0000) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),


        new createjs.Point(((539.0000) * transformX) + moveX, ((1782.3333) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1782.6667) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),


        new createjs.Point(((538.0001) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((536.9999) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),


        new createjs.Point(((536.0000) * transformX) + moveX, ((1783.3333) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((1783.6667) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((1784.0000) * transformY) + moveY),


        new createjs.Point(((534.6668) * transformX) + moveX, ((1784.3333) * transformY) + moveY),
        new createjs.Point(((533.3332) * transformX) + moveX, ((1784.6667) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),


        new createjs.Point(((532.0000) * transformX) + moveX, ((1785.3333) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1785.6667) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1786.0000) * transformY) + moveY),


        new createjs.Point(((513.0019) * transformX) + moveX, ((1792.3327) * transformY) + moveY),
        new createjs.Point(((493.9981) * transformX) + moveX, ((1798.6673) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),


        new createjs.Point(((473.6668) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((472.3332) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),


        new createjs.Point(((471.0000) * transformX) + moveX, ((1805.3333) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((1805.6667) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((1806.0000) * transformY) + moveY),


        new createjs.Point(((469.6668) * transformX) + moveX, ((1806.0000) * transformY) + moveY),
        new createjs.Point(((468.3332) * transformX) + moveX, ((1806.0000) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((1806.0000) * transformY) + moveY),


        new createjs.Point(((467.0000) * transformX) + moveX, ((1806.3333) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((1806.6667) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),


        new createjs.Point(((465.6668) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((464.3332) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),


        new createjs.Point(((463.0000) * transformX) + moveX, ((1807.3333) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((1807.6667) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),


        new createjs.Point(((461.6668) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((460.3332) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),


        new createjs.Point(((459.0000) * transformX) + moveX, ((1808.3333) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1808.6667) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),


        new createjs.Point(((457.3335) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((455.6665) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),


        new createjs.Point(((454.0000) * transformX) + moveX, ((1809.3333) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((1809.6667) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),


        new createjs.Point(((452.6668) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((451.3332) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),


        new createjs.Point(((450.0000) * transformX) + moveX, ((1810.3333) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((1810.6667) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),


        new createjs.Point(((448.3335) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((446.6665) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),


        new createjs.Point(((445.0000) * transformX) + moveX, ((1811.3333) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((1811.6667) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),


        new createjs.Point(((443.3335) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((441.6665) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),


        new createjs.Point(((440.0000) * transformX) + moveX, ((1812.3333) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((1812.6667) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),


        new createjs.Point(((438.3335) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((436.6665) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((435.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),


        new createjs.Point(((435.0000) * transformX) + moveX, ((1813.3333) * transformY) + moveY),
        new createjs.Point(((435.0000) * transformX) + moveX, ((1813.6667) * transformY) + moveY),
        new createjs.Point(((435.0000) * transformX) + moveX, ((1814.0000) * transformY) + moveY),


        new createjs.Point(((431.0004) * transformX) + moveX, ((1814.3333) * transformY) + moveY),
        new createjs.Point(((426.9996) * transformX) + moveX, ((1814.6667) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((1815.0000) * transformY) + moveY),


        new createjs.Point(((423.0000) * transformX) + moveX, ((1815.3333) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((1815.6667) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),


        new createjs.Point(((420.6669) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((418.3331) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((416.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),


        new createjs.Point(((416.0000) * transformX) + moveX, ((1816.3333) * transformY) + moveY),
        new createjs.Point(((416.0000) * transformX) + moveX, ((1816.6667) * transformY) + moveY),
        new createjs.Point(((416.0000) * transformX) + moveX, ((1817.0000) * transformY) + moveY),


        new createjs.Point(((413.3336) * transformX) + moveX, ((1817.0000) * transformY) + moveY),
        new createjs.Point(((410.6664) * transformX) + moveX, ((1817.0000) * transformY) + moveY),
        new createjs.Point(((408.0000) * transformX) + moveX, ((1817.0000) * transformY) + moveY),


        new createjs.Point(((408.0000) * transformX) + moveX, ((1817.3333) * transformY) + moveY),
        new createjs.Point(((408.0000) * transformX) + moveX, ((1817.6667) * transformY) + moveY),
        new createjs.Point(((408.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),


        new createjs.Point(((405.0003) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((401.9997) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),


        new createjs.Point(((399.0000) * transformX) + moveX, ((1818.3333) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((1818.6667) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),


        new createjs.Point(((390.0009) * transformX) + moveX, ((1819.3333) * transformY) + moveY),
        new createjs.Point(((380.9991) * transformX) + moveX, ((1819.6667) * transformY) + moveY),
        new createjs.Point(((372.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),


        new createjs.Point(((363.8877) * transformX) + moveX, ((1822.2423) * transformY) + moveY),
        new createjs.Point(((339.0956) * transformX) + moveX, ((1822.2957) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),


        new createjs.Point(((325.3339) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((319.6661) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),


        new createjs.Point(((314.0000) * transformX) + moveX, ((1819.6667) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((1819.3333) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),


        new createjs.Point(((310.0004) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((305.9996) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),


        new createjs.Point(((302.0000) * transformX) + moveX, ((1818.6667) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((1818.3333) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),


        new createjs.Point(((299.0003) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((295.9997) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),


        new createjs.Point(((285.4637) * transformX) + moveX, ((1815.8008) * transformY) + moveY),
        new createjs.Point(((275.2310) * transformX) + moveX, ((1815.2632) * transformY) + moveY),
        new createjs.Point(((268.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),


        new createjs.Point(((266.3335) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((264.6665) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),


        new createjs.Point(((263.0000) * transformX) + moveX, ((1812.6667) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((1812.3333) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),


        new createjs.Point(((261.3335) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((259.6665) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),


        new createjs.Point(((258.0000) * transformX) + moveX, ((1811.6667) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((1811.3333) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),


        new createjs.Point(((256.6668) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((255.3332) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),


        new createjs.Point(((254.0000) * transformX) + moveX, ((1810.6667) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1810.3333) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),


        new createjs.Point(((252.6668) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((251.3332) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((250.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),


        new createjs.Point(((250.0000) * transformX) + moveX, ((1809.6667) * transformY) + moveY),
        new createjs.Point(((250.0000) * transformX) + moveX, ((1809.3333) * transformY) + moveY),
        new createjs.Point(((250.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),


        new createjs.Point(((248.6668) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((247.3332) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),


        new createjs.Point(((246.0000) * transformX) + moveX, ((1808.6667) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1808.3333) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),


        new createjs.Point(((244.6668) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((243.3332) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),


        new createjs.Point(((242.0000) * transformX) + moveX, ((1807.6667) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((1807.3333) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),


        new createjs.Point(((241.0001) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((239.9999) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),


        new createjs.Point(((239.0000) * transformX) + moveX, ((1806.6667) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1806.3333) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1806.0000) * transformY) + moveY),


        new createjs.Point(((237.6668) * transformX) + moveX, ((1806.0000) * transformY) + moveY),
        new createjs.Point(((236.3332) * transformX) + moveX, ((1806.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1806.0000) * transformY) + moveY),


        new createjs.Point(((235.0000) * transformX) + moveX, ((1805.6667) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1805.3333) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),


        new createjs.Point(((234.0001) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((232.9999) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((232.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),


        new createjs.Point(((232.0000) * transformX) + moveX, ((1804.6667) * transformY) + moveY),
        new createjs.Point(((232.0000) * transformX) + moveX, ((1804.3333) * transformY) + moveY),
        new createjs.Point(((232.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),


        new createjs.Point(((231.0001) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((229.9999) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),


        new createjs.Point(((229.0000) * transformX) + moveX, ((1803.6667) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1803.3333) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),


        new createjs.Point(((228.0001) * transformX) + moveX, ((1803.0000) * transformY) + moveY),
        new createjs.Point(((226.9999) * transformX) + moveX, ((1803.0000) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),


        new createjs.Point(((226.0000) * transformX) + moveX, ((1802.6667) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((1802.3333) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),


        new createjs.Point(((225.0001) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((223.9999) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),


        new createjs.Point(((223.0000) * transformX) + moveX, ((1801.6667) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1801.3333) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1801.0000) * transformY) + moveY),


        new createjs.Point(((222.0001) * transformX) + moveX, ((1801.0000) * transformY) + moveY),
        new createjs.Point(((220.9999) * transformX) + moveX, ((1801.0000) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((1801.0000) * transformY) + moveY),


        new createjs.Point(((220.0000) * transformX) + moveX, ((1800.6667) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((1800.3333) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((1800.0000) * transformY) + moveY),


        new createjs.Point(((218.0002) * transformX) + moveX, ((1799.6667) * transformY) + moveY),
        new createjs.Point(((215.9998) * transformX) + moveX, ((1799.3333) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1799.0000) * transformY) + moveY),


        new createjs.Point(((214.0000) * transformX) + moveX, ((1798.6667) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1798.3333) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),


        new createjs.Point(((213.3334) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((212.6666) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((212.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),


        new createjs.Point(((212.0000) * transformX) + moveX, ((1797.6667) * transformY) + moveY),
        new createjs.Point(((212.0000) * transformX) + moveX, ((1797.3333) * transformY) + moveY),
        new createjs.Point(((212.0000) * transformX) + moveX, ((1797.0000) * transformY) + moveY),


        new createjs.Point(((211.0001) * transformX) + moveX, ((1797.0000) * transformY) + moveY),
        new createjs.Point(((209.9999) * transformX) + moveX, ((1797.0000) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((1797.0000) * transformY) + moveY),


        new createjs.Point(((209.0000) * transformX) + moveX, ((1796.6667) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((1796.3333) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),


        new createjs.Point(((208.3334) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((207.6666) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),


        new createjs.Point(((207.0000) * transformX) + moveX, ((1795.6667) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((1795.3333) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((1795.0000) * transformY) + moveY),


        new createjs.Point(((206.0001) * transformX) + moveX, ((1795.0000) * transformY) + moveY),
        new createjs.Point(((204.9999) * transformX) + moveX, ((1795.0000) * transformY) + moveY),
        new createjs.Point(((204.0000) * transformX) + moveX, ((1795.0000) * transformY) + moveY),


        new createjs.Point(((204.0000) * transformX) + moveX, ((1794.6667) * transformY) + moveY),
        new createjs.Point(((204.0000) * transformX) + moveX, ((1794.3333) * transformY) + moveY),
        new createjs.Point(((204.0000) * transformX) + moveX, ((1794.0000) * transformY) + moveY),


        new createjs.Point(((202.6668) * transformX) + moveX, ((1793.6667) * transformY) + moveY),
        new createjs.Point(((201.3332) * transformX) + moveX, ((1793.3333) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1793.0000) * transformY) + moveY),


        new createjs.Point(((200.0000) * transformX) + moveX, ((1792.6667) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1792.3333) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1792.0000) * transformY) + moveY),


        new createjs.Point(((199.0001) * transformX) + moveX, ((1792.0000) * transformY) + moveY),
        new createjs.Point(((197.9999) * transformX) + moveX, ((1792.0000) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((1792.0000) * transformY) + moveY),


        new createjs.Point(((197.0000) * transformX) + moveX, ((1791.6667) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((1791.3333) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((1791.0000) * transformY) + moveY),


        new createjs.Point(((195.0002) * transformX) + moveX, ((1790.3334) * transformY) + moveY),
        new createjs.Point(((192.9998) * transformX) + moveX, ((1789.6666) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((1789.0000) * transformY) + moveY),


        new createjs.Point(((191.0000) * transformX) + moveX, ((1788.6667) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((1788.3333) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((1788.0000) * transformY) + moveY),


        new createjs.Point(((190.3334) * transformX) + moveX, ((1788.0000) * transformY) + moveY),
        new createjs.Point(((189.6666) * transformX) + moveX, ((1788.0000) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((1788.0000) * transformY) + moveY),


        new createjs.Point(((189.0000) * transformX) + moveX, ((1787.6667) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((1787.3333) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((1787.0000) * transformY) + moveY),


        new createjs.Point(((188.3334) * transformX) + moveX, ((1787.0000) * transformY) + moveY),
        new createjs.Point(((187.6666) * transformX) + moveX, ((1787.0000) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((1787.0000) * transformY) + moveY),


        new createjs.Point(((187.0000) * transformX) + moveX, ((1786.6667) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((1786.3333) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((1786.0000) * transformY) + moveY),


        new createjs.Point(((186.3334) * transformX) + moveX, ((1786.0000) * transformY) + moveY),
        new createjs.Point(((185.6666) * transformX) + moveX, ((1786.0000) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((1786.0000) * transformY) + moveY),


        new createjs.Point(((185.0000) * transformX) + moveX, ((1785.6667) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((1785.3333) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),


        new createjs.Point(((184.3334) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((183.6666) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),


        new createjs.Point(((183.0000) * transformX) + moveX, ((1784.6667) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((1784.3333) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((1784.0000) * transformY) + moveY),


        new createjs.Point(((182.3334) * transformX) + moveX, ((1784.0000) * transformY) + moveY),
        new createjs.Point(((181.6666) * transformX) + moveX, ((1784.0000) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((1784.0000) * transformY) + moveY),


        new createjs.Point(((181.0000) * transformX) + moveX, ((1783.6667) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((1783.3333) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),


        new createjs.Point(((180.3334) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((179.6666) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),


        new createjs.Point(((179.0000) * transformX) + moveX, ((1782.6667) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((1782.3333) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),


        new createjs.Point(((178.3334) * transformX) + moveX, ((1782.0000) * transformY) + moveY),
        new createjs.Point(((177.6666) * transformX) + moveX, ((1782.0000) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),


        new createjs.Point(((177.0000) * transformX) + moveX, ((1781.6667) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((1781.3333) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),


        new createjs.Point(((176.3334) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((175.6666) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),


        new createjs.Point(((175.0000) * transformX) + moveX, ((1780.6667) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((1780.3333) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),


        new createjs.Point(((174.3334) * transformX) + moveX, ((1780.0000) * transformY) + moveY),
        new createjs.Point(((173.6666) * transformX) + moveX, ((1780.0000) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),


        new createjs.Point(((173.0000) * transformX) + moveX, ((1779.6667) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((1779.3333) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((1779.0000) * transformY) + moveY),


        new createjs.Point(((172.3334) * transformX) + moveX, ((1779.0000) * transformY) + moveY),
        new createjs.Point(((171.6666) * transformX) + moveX, ((1779.0000) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((1779.0000) * transformY) + moveY),


        new createjs.Point(((170.6667) * transformX) + moveX, ((1778.3334) * transformY) + moveY),
        new createjs.Point(((170.3333) * transformX) + moveX, ((1777.6666) * transformY) + moveY),
        new createjs.Point(((170.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),


        new createjs.Point(((168.0002) * transformX) + moveX, ((1776.3334) * transformY) + moveY),
        new createjs.Point(((165.9998) * transformX) + moveX, ((1775.6666) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((1775.0000) * transformY) + moveY),


        new createjs.Point(((163.6667) * transformX) + moveX, ((1774.3334) * transformY) + moveY),
        new createjs.Point(((163.3333) * transformX) + moveX, ((1773.6666) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((1773.0000) * transformY) + moveY),


        new createjs.Point(((161.6668) * transformX) + moveX, ((1772.6667) * transformY) + moveY),
        new createjs.Point(((160.3332) * transformX) + moveX, ((1772.3333) * transformY) + moveY),
        new createjs.Point(((159.0000) * transformX) + moveX, ((1772.0000) * transformY) + moveY),


        new createjs.Point(((158.6667) * transformX) + moveX, ((1771.3334) * transformY) + moveY),
        new createjs.Point(((158.3333) * transformX) + moveX, ((1770.6666) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1770.0000) * transformY) + moveY),


        new createjs.Point(((156.6668) * transformX) + moveX, ((1769.6667) * transformY) + moveY),
        new createjs.Point(((155.3332) * transformX) + moveX, ((1769.3333) * transformY) + moveY),
        new createjs.Point(((154.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),


        new createjs.Point(((153.6667) * transformX) + moveX, ((1768.3334) * transformY) + moveY),
        new createjs.Point(((153.3333) * transformX) + moveX, ((1767.6666) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1767.0000) * transformY) + moveY),


        new createjs.Point(((152.3334) * transformX) + moveX, ((1767.0000) * transformY) + moveY),
        new createjs.Point(((151.6666) * transformX) + moveX, ((1767.0000) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1767.0000) * transformY) + moveY),


        new createjs.Point(((150.6667) * transformX) + moveX, ((1766.3334) * transformY) + moveY),
        new createjs.Point(((150.3333) * transformX) + moveX, ((1765.6666) * transformY) + moveY),
        new createjs.Point(((150.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),


        new createjs.Point(((149.3334) * transformX) + moveX, ((1765.0000) * transformY) + moveY),
        new createjs.Point(((148.6666) * transformX) + moveX, ((1765.0000) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),


        new createjs.Point(((148.0000) * transformX) + moveX, ((1764.6667) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((1764.3333) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((1764.0000) * transformY) + moveY),


        new createjs.Point(((147.0001) * transformX) + moveX, ((1763.6667) * transformY) + moveY),
        new createjs.Point(((145.9999) * transformX) + moveX, ((1763.3333) * transformY) + moveY),
        new createjs.Point(((145.0000) * transformX) + moveX, ((1763.0000) * transformY) + moveY),


        new createjs.Point(((144.6667) * transformX) + moveX, ((1762.3334) * transformY) + moveY),
        new createjs.Point(((144.3333) * transformX) + moveX, ((1761.6666) * transformY) + moveY),
        new createjs.Point(((144.0000) * transformX) + moveX, ((1761.0000) * transformY) + moveY),


        new createjs.Point(((143.3334) * transformX) + moveX, ((1761.0000) * transformY) + moveY),
        new createjs.Point(((142.6666) * transformX) + moveX, ((1761.0000) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((1761.0000) * transformY) + moveY),


        new createjs.Point(((141.6667) * transformX) + moveX, ((1760.3334) * transformY) + moveY),
        new createjs.Point(((141.3333) * transformX) + moveX, ((1759.6666) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((1759.0000) * transformY) + moveY),


        new createjs.Point(((140.3334) * transformX) + moveX, ((1759.0000) * transformY) + moveY),
        new createjs.Point(((139.6666) * transformX) + moveX, ((1759.0000) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((1759.0000) * transformY) + moveY),


        new createjs.Point(((138.3334) * transformX) + moveX, ((1758.0001) * transformY) + moveY),
        new createjs.Point(((137.6666) * transformX) + moveX, ((1756.9999) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1756.0000) * transformY) + moveY),


        new createjs.Point(((136.3334) * transformX) + moveX, ((1756.0000) * transformY) + moveY),
        new createjs.Point(((135.6666) * transformX) + moveX, ((1756.0000) * transformY) + moveY),
        new createjs.Point(((135.0000) * transformX) + moveX, ((1756.0000) * transformY) + moveY),


        new createjs.Point(((134.6667) * transformX) + moveX, ((1755.3334) * transformY) + moveY),
        new createjs.Point(((134.3333) * transformX) + moveX, ((1754.6666) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((1754.0000) * transformY) + moveY),


        new createjs.Point(((133.3334) * transformX) + moveX, ((1754.0000) * transformY) + moveY),
        new createjs.Point(((132.6666) * transformX) + moveX, ((1754.0000) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((1754.0000) * transformY) + moveY),


        new createjs.Point(((131.3334) * transformX) + moveX, ((1753.0001) * transformY) + moveY),
        new createjs.Point(((130.6666) * transformX) + moveX, ((1751.9999) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1751.0000) * transformY) + moveY),


        new createjs.Point(((129.3334) * transformX) + moveX, ((1751.0000) * transformY) + moveY),
        new createjs.Point(((128.6666) * transformX) + moveX, ((1751.0000) * transformY) + moveY),
        new createjs.Point(((128.0000) * transformX) + moveX, ((1751.0000) * transformY) + moveY),


        new createjs.Point(((127.3334) * transformX) + moveX, ((1750.0001) * transformY) + moveY),
        new createjs.Point(((126.6666) * transformX) + moveX, ((1748.9999) * transformY) + moveY),
        new createjs.Point(((126.0000) * transformX) + moveX, ((1748.0000) * transformY) + moveY),


        new createjs.Point(((125.3334) * transformX) + moveX, ((1748.0000) * transformY) + moveY),
        new createjs.Point(((124.6666) * transformX) + moveX, ((1748.0000) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((1748.0000) * transformY) + moveY),


        new createjs.Point(((123.0001) * transformX) + moveX, ((1746.6668) * transformY) + moveY),
        new createjs.Point(((121.9999) * transformX) + moveX, ((1745.3332) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1744.0000) * transformY) + moveY),


        new createjs.Point(((120.0001) * transformX) + moveX, ((1743.6667) * transformY) + moveY),
        new createjs.Point(((118.9999) * transformX) + moveX, ((1743.3333) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),


        new createjs.Point(((117.6667) * transformX) + moveX, ((1742.3334) * transformY) + moveY),
        new createjs.Point(((117.3333) * transformX) + moveX, ((1741.6666) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),


        new createjs.Point(((116.3334) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((115.6666) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),


        new createjs.Point(((113.6668) * transformX) + moveX, ((1739.3335) * transformY) + moveY),
        new createjs.Point(((112.3332) * transformX) + moveX, ((1737.6665) * transformY) + moveY),
        new createjs.Point(((111.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),


        new createjs.Point(((110.3334) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((109.6666) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),


        new createjs.Point(((107.6668) * transformX) + moveX, ((1734.3335) * transformY) + moveY),
        new createjs.Point(((106.3332) * transformX) + moveX, ((1732.6665) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),


        new createjs.Point(((104.3334) * transformX) + moveX, ((1731.0000) * transformY) + moveY),
        new createjs.Point(((103.6666) * transformX) + moveX, ((1731.0000) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),


        new createjs.Point(((101.3335) * transformX) + moveX, ((1729.0002) * transformY) + moveY),
        new createjs.Point(((99.6665) * transformX) + moveX, ((1726.9998) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),


        new createjs.Point(((97.3334) * transformX) + moveX, ((1725.0000) * transformY) + moveY),
        new createjs.Point(((96.6666) * transformX) + moveX, ((1725.0000) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),


        new createjs.Point(((94.0002) * transformX) + moveX, ((1722.6669) * transformY) + moveY),
        new createjs.Point(((91.9998) * transformX) + moveX, ((1720.3331) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),


        new createjs.Point(((89.3334) * transformX) + moveX, ((1718.0000) * transformY) + moveY),
        new createjs.Point(((88.6666) * transformX) + moveX, ((1718.0000) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),


        new createjs.Point(((84.0004) * transformX) + moveX, ((1713.6671) * transformY) + moveY),
        new createjs.Point(((79.9996) * transformX) + moveX, ((1709.3329) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1705.0000) * transformY) + moveY),


        new createjs.Point(((75.3334) * transformX) + moveX, ((1705.0000) * transformY) + moveY),

    ];

    var points2 = [
        new createjs.Point(startX2, startY2),
        new createjs.Point(endX2, endY2)
    ];
    var points1Final = points;

    var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath, motionPath2);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final), getMotionPathFromPoints(points2Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 3", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("4", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();


        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 60).call(function() {
            setTimeout(function() {
                pause();
            }, 500);
        })).on('change', (function(event) {

            if (currentMotionStep == 1) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        this.timeline.addTween(cjs.Tween.get(bar2).wait(40).to({
            guide: {
                path: motionPath2
            }
        }, 20).wait(50)).on('change', (function(event) {
            if (currentMotionStep == 2) {
                bar2 = drawCharacter(thisStage, bar2, true, event);
            }
        }));


    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];

                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }
            
            if (thisbar.x === startX && thisbar.y === startY) {
                Stage1.shape.visible = true;
            } else if (thisbar.x === 726.28 && thisbar.y === 81.15748432968871) { // finish drawing 1/3 of line
                Stage1.shape.visible = false;
            }

            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.2)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.paused = false;
            thisStage.movie.play();
        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);
        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }

    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145);
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
