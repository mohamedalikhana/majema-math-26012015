(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p115_1.png",
            id: "p115_1"
        }]
    };

    (lib.p115_1 = function() {
        this.initialize(img.p115_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_2 = new cjs.Text("115", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(555, 656);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.instance = new lib.p115_1();
        this.instance.setTransform(417, 340, 0.46, 0.46);

        this.addChild(this.shape_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();
        this.text = new cjs.Text("Använd talen 2, 3 och 4.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 15);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 15);

        this.shape1 = new cjs.Shape();
        this.shape1.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 75, 10);
        this.shape1.setTransform(0, 25);
        var ToBeAdded = [];
        //Rectangle box and text
        var xpos = 275;
        var ypos = 35;
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 2; row++) {
            for (var column = 0; column < 6; column++) {
                var columnSpace = column;
                if (column > 3) {
                    xpos = 365
                } else if (column > 1) {
                    xpos = 320
                } else {
                    xpos = 275
                }
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 30), 21, 23);

                ToBeAdded.push(tmp_Rect);
            }
        }

        var tmptxt = "Skriv alla tvåsiffriga tal"
        for (var row = 0; row < 2; row++) {
            if (row == 1) {
                tmptxt = "som är mindre än 40."
            }
            var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
            tempText.setTransform(20, 55 + (row * 23));
            ToBeAdded.push(tempText);
        }

        var j = 0;
        for (var col = 0; col < 2; col++) {
            columnSpace = col
            var tempText = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
            tempText.setTransform(276 + (columnSpace * 23), 57);
            ToBeAdded.push(tempText);
        }

        this.addChild(this.shape1, this.text, this.text_1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 95);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("Använd talen 3, 4 och 5.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 15);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 15);

        this.shape1 = new cjs.Shape();
        this.shape1.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 75, 10);
        this.shape1.setTransform(0, 25);
        var ToBeAdded = [];
        //Rectangle box and text
        var xpos = 275;
        var ypos = 35;
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 2; row++) {
            for (var column = 0; column < 6; column++) {
                var columnSpace = column;
                if (column > 3) {
                    xpos = 365
                } else if (column > 1) {
                    xpos = 320
                } else {
                    xpos = 275
                }
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 30), 21, 23);

                ToBeAdded.push(tmp_Rect);
            }
        }

        var tmptxt = "Skriv alla tvåsiffriga tal"
        for (var row = 0; row < 2; row++) {
            if (row == 1) {
                tmptxt = "som är större än 40."
            }
            var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
            tempText.setTransform(20, 55 + (row * 23));
            ToBeAdded.push(tempText);
        }


        this.addChild(this.shape1, this.text, this.text_1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 95);

    //Part 3
    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Använd talen 3, 4 och 5.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 15);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 15);

        this.shape1 = new cjs.Shape();
        this.shape1.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 75, 10);
        this.shape1.setTransform(0, 25);

        var ToBeAdded = [];
        //Rectangle box and text
        var xpos = 275;
        var ypos = 35;
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 2; row++) {
            for (var column = 0; column < 6; column++) {
                var columnSpace = column;
                if (column > 3) {
                    xpos = 365
                } else if (column > 1) {
                    xpos = 320
                } else {
                    xpos = 275
                }
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 30), 21, 23);

                ToBeAdded.push(tmp_Rect);
            }
        }

        var tmptxt = "Skriv alla tvåsiffriga tal"
        for (var row = 0; row < 2; row++) {
            if (row == 1) {
                tmptxt = "som är mindre än 50."
            }
            var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
            tempText.setTransform(20, 55 + (row * 23));
            ToBeAdded.push(tempText);
        }
        this.addChild(this.text_1, this.text, this.shape1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 95);

    //Part 4
    (lib.Symbol5 = function() {
        this.initialize();

        // this.instance = new lib.p71_1();
        // this.instance.setTransform(15, 27, 0.38, 0.35);

        this.text = new cjs.Text("Ringa in talen som passar.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 15);

        this.text_1 = new cjs.Text("7.", "bold 16px 'Myriad Pro'", "#00A3C4");;
        this.text_1.setTransform(0, 15);

        this.shape1 = new cjs.Shape();
        this.shape1.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 187, 10);
        this.shape1.setTransform(0, 25);

        var ToBeAdded = [];

        for (var row = 0; row < 4; row++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(22, 75 + (row * 32)).lineTo(490, 75 + (row * 32));
            ToBeAdded.push(hrLine)
        };

        var tmptxt = ["Det är 4 ental.", "14", "22", "34", "41", "45", " 4",
            "Det är 4 tiotal.", "24", "43", "34", "49", "40", " 4",
            "Det är 2 ental.", "20", "12", "42", "25", "23", " 2",
            "Det är inga ental.", " 1", "10", "11", "20", "31", "40",
            "Det är inga tiotal.", "10", "20", " 2", " 5", "50", " 4"
        ]
        var i = 0;
        var xpos=20
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 7; col++) {
                if(col>0){xpos=145}else {xpos=20}
                var tempText = new cjs.Text(tmptxt[i], "16px 'Myriad Pro'");
                tempText.setTransform(xpos+(col*52), 60 + (row * 34));
                ToBeAdded.push(tempText);
                i++
            };
        }

        this.addChild(this.shape1, this.text, this.instance, this.text_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535.3, 200.6);





    // stage content:
    (lib.p71 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v4 = new lib.Symbol5();
        this.v4.setTransform(311.8, 465, 1, 1, 0, 0, 0, 256.3, 38.8);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(311.8, 297.3, 1, 1, 0, 0, 0, 256.3, 38.8);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(312.6, 222, 1, 1, 0, 0, 0, 255.1, 74.8);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(312.4, 89.9, 1, 1, 0, 0, 0, 254.6, 53.4);

        // this.addChild(this.v1, this.other);
        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
