(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p74_1.png",
            id: "p74_1"
        }, {
            src: "images/p74_2.png",
            id: "p74_2"
        }, {
            src: "images/p74_3.png",
            id: "p74_3"
        }]
    };

    (lib.p74_1 = function() {
        this.initialize(img.p74_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p74_2 = function() {
        this.initialize(img.p74_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p74_3 = function() {
        this.initialize(img.p74_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Likhetstecknet", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(166, 40);

        this.text_1 = new cjs.Text("25", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(42, 22);

        this.pageBottomText = new cjs.Text("förstå och kunna använda symbolen =", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(85.5, 650.8);

        this.text_4 = new cjs.Text("74", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(38.5, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 170, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").ss(0.5, 0, 0, 4).arc(423, 62, 13.5, 0, 2 * Math.PI);
        this.shape_1.setTransform(0, 0);

        this.text5 = new cjs.Text("=", "bold 32px 'Epic Awesomeness'", "#FFFFFF");
        this.text5.lineHeight = 29;
        this.text5.setTransform(412, 40);

        this.addChild(this.shape_1, this.text5, this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.pageBottomText, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_48 = new cjs.Text("Rita bollar så att Tuss har lika många som Boss. Skriv tal så att det stämmer.", "16px 'Myriad Pro'");
        this.text_48.lineHeight = 19;
        this.text_48.setTransform(1, 7.6);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 45, 500, 230, 10);
        this.round_Rect1.setTransform(0, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(20, 145).lineTo(495, 145).moveTo(20, 210).lineTo(495, 210);
        this.line1.setTransform(0, 0);

        this.line2 = new cjs.Shape();
        var temp;
        for (var i = 45; i < 275; i++) {
            if (i == 45) {
                this.line2.graphics.f("#BDBEC0").s("#BDBEC0").ss(0.5).arc(250, i, 0.5, 0, 2 * Math.PI);
                temp = i + 3;
            } else if (i == temp) {
                this.line2.graphics.f("#BDBEC0").s("#BDBEC0").ss(0.5).arc(250, i, 0.5, 0, 2 * Math.PI);
                temp = i + 3;
            }
        }
        this.line2.setTransform(0, 0);

        this.instance = new lib.p74_1();
        this.instance.setTransform(80, 30, 0.30, 0.30);

        this.instance_2 = new lib.p74_2();
        this.instance_2.setTransform(360, 30, 0.4, 0.4);

        var arrVal = ['75', '110', '145', '291', '326'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var xVal = parseInt(arrVal[index]);

            this.instance_3 = new lib.p74_3();
            this.instance_3.setTransform(xVal, 86, 0.3, 0.3);
            ToBeAdded.push(this.instance_3);
        }

        var arrVal = [];
        var arrVal = ['40', '75', '110', '145', '180', '275', '310', '345'];

        for (var index = 0; index < arrVal.length; index++) {
            var xVal = parseInt(arrVal[index]);

            this.instance_3 = new lib.p74_3();
            this.instance_3.setTransform(xVal, 151, 0.3, 0.3);
            ToBeAdded.push(this.instance_3);
        }

        var arrVal = [];
        var arrVal = ['56', '91', '126', '161', '310'];

        for (var index = 0; index < arrVal.length; index++) {
            var xVal = parseInt(arrVal[index]);

            this.instance_3 = new lib.p74_3();
            this.instance_3.setTransform(xVal, 216, 0.3, 0.3);
            ToBeAdded.push(this.instance_3);
        }

        arrVal = []
        arrVal = ['Boss', 'Tuss'];

        for (var index = 0; index < arrVal.length; index++) {

            this.tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            this.tempLabel.lineHeight = 19;
            this.tempLabel.setTransform(43 + (index * 368), 60);
            ToBeAdded.push(this.tempLabel);
        }

        for (var index = 0; index < 3; index++) {

            var tempLabel = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
            tempLabel.lineHeight = 30;
            tempLabel.setTransform(365, 104 + (index * 63));
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            var colMultiple = 200;
            if (column == 2) {
                colMultiple = 152;
            }
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(110 + (columnSpace * colMultiple), 114 + (row * 65), 22, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(111, 104.5);

        this.text_2 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(311, 104.5);

        this.text_3 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(235, 85, 1.2, 1.2);

        this.text_4 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_4.lineHeight = 29;
        this.text_4.setTransform(235, 155, 1.2, 1.2);

        this.text_5 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_5.lineHeight = 29;
        this.text_5.setTransform(235, 225, 1.2, 1.2);

        this.addChild(this.round_Rect1, this.line1, this.text_48, this.instance, this.textbox_group1, this.text_1, this.text_2, this.line2);
        this.addChild(this.instance_2, this.text_3, this.text_4, this.text_5);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 300);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_48 = new cjs.Text("Skriv tal så att det stämmer.", "16px 'Myriad Pro'");
        this.text_48.lineHeight = 19;
        this.text_48.setTransform(1, 7.6);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 45, 500, 214, 10);
        this.round_Rect1.setTransform(0, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(20, 118).lineTo(495, 118).moveTo(20, 153).lineTo(495, 153).moveTo(20, 186).lineTo(495, 186).moveTo(20, 221).lineTo(495, 221);
        this.line1.setTransform(0, 0);

        this.line2 = new cjs.Shape();
        var temp;
        for (var i = 45; i < 260; i++) {
            if (i == 45) {
                this.line2.graphics.f("#BDBEC0").s("#BDBEC0").ss(0.5).arc(250, i, 0.5, 0, 2 * Math.PI);
                temp = i + 3;
            } else if (i == temp) {
                this.line2.graphics.f("#BDBEC0").s("#BDBEC0").ss(0.5).arc(250, i, 0.5, 0, 2 * Math.PI);
                temp = i + 3;
            }
        }
        this.line2.setTransform(0, 0);

        this.instance = new lib.p74_1();
        this.instance.setTransform(80, 30, 0.30, 0.30);

        this.instance_2 = new lib.p74_2();
        this.instance_2.setTransform(360, 30, 0.4, 0.4);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            var colMultiple = 200;
            if (column == 2) {
                colMultiple = 152;
            }
            for (var row = 0; row < 5; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(110 + (columnSpace * colMultiple), 90 + (row * 34), 22, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(110, 80);

        this.text_2 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(310, 80);

        this.text_3 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(110, 114);

        this.text_4 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(310, 114);

        this.text_5 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 45;
        this.text_5.setTransform(110, 148);

        this.text_6 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 45;
        this.text_6.setTransform(310, 148);

        this.text_7 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 45;
        this.text_7.setTransform(110, 182);

        this.text_8 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 45;
        this.text_8.setTransform(310, 182);

        this.text_9 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 45;
        this.text_9.setTransform(110, 216);

        this.text_10 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 45;
        this.text_10.setTransform(310, 216);

        this.text_11 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_11.lineHeight = 29;
        this.text_11.setTransform(235, 73, 1.2, 1.2);

        this.text_12 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_12.lineHeight = 29;
        this.text_12.setTransform(235, 107, 1.2, 1.2);

        this.text_13 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_13.lineHeight = 29;
        this.text_13.setTransform(235, 143, 1.2, 1.2);

        this.text_14 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_14.lineHeight = 29;
        this.text_14.setTransform(235, 177, 1.2, 1.2);

        this.text_15 = new cjs.Text("=", "bold 32px 'Arial'", "#FAAA33");
        this.text_15.lineHeight = 29;
        this.text_15.setTransform(235, 213, 1.2, 1.2);

        var ToBeAdded = [];
        for (var index = 0; index < 5; index++) {

            var tempLabel = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
            tempLabel.lineHeight = 30;
            tempLabel.setTransform(365, 78 + (index * 35));
            ToBeAdded.push(tempLabel);
        }

        this.addChild(this.round_Rect1, this.line1, this.text_48, this.instance, this.textbox_group1, this.text_1, this.text_2, this.line2);

        this.addChild(this.instance_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10);
        this.addChild(this.text_11, this.text_12, this.text_13, this.text_14, this.text_15);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(302, 270, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(302, 550, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
