(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p83_1.png",
            id: "p83_1"
        }]
    };

    (lib.p83_1 = function() {
        this.initialize(img.p83_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("83", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);     

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(18, 20);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   
// 
        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Rita visare.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 330, 10);
        this.roundRect1.setTransform(10, 20);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_1.setTransform(10, 98);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_2.setTransform(10, 204);

        this.Symbolclock = new lib.Symboldottedclock();
        this.Symbolclock.setTransform(72, 72, 0.83, 0.83);

        this.Symbolclock_1 = new lib.Symboldottedclock();
        this.Symbolclock_1.setTransform(200, 72, 0.83, 0.83);

        this.Symbolclock_2 = new lib.Symboldottedclock();
        this.Symbolclock_2.setTransform(329, 72, 0.83, 0.83);

        this.Symbolclock_3 = new lib.Symboldottedclock();
        this.Symbolclock_3.setTransform(455, 72, 0.83, 0.83);

        this.Symbolclock_4 = new lib.Symboldottedclock();
        this.Symbolclock_4.setTransform(72, 181, 0.83, 0.83);

        this.Symbolclock_5 = new lib.Symboldottedclock();
        this.Symbolclock_5.setTransform(200, 181, 0.83, 0.83);

        this.Symbolclock_6 = new lib.Symboldottedclock();
        this.Symbolclock_6.setTransform(329, 181, 0.83, 0.83);

        this.Symbolclock_7 = new lib.Symboldottedclock();
        this.Symbolclock_7.setTransform(455, 181, 0.83, 0.83);

        this.Symbolclock_8 = new lib.Symboldottedclock();
        this.Symbolclock_8.setTransform(72, 286, 0.83, 0.83);

        this.Symbolclock_9 = new lib.Symboldottedclock();
        this.Symbolclock_9.setTransform(200, 286, 0.83, 0.83);

        this.Symbolclock_10 = new lib.Symboldottedclock();
        this.Symbolclock_10.setTransform(329, 286, 0.83, 0.83);

        this.Symbolclock_11 = new lib.Symboldottedclock();
        this.Symbolclock_11.setTransform(455, 286, 0.83, 0.83);

        var TextArr = [];
        var TArr = [5, 7, 4, 10, 9, 8, 11, 6, 1, 3, 2, 12];
        var TextX = [21, 147, 273, 399, 21, 147, 273, 399, 21, 147, 273, 399];
        var TextY = [130, 130, 130, 130, 235, 235, 235, 235, 340, 340, 340, 340];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("Klockan är"+" "+TArr[row]+".", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.hrLine_1, this.hrLine_2,
            this.Symbolclock, this.Symbolclock_1, this.Symbolclock_2, this.Symbolclock_3, this.Symbolclock_4, this.Symbolclock_5,
            this.Symbolclock_6, this.Symbolclock_7, this.Symbolclock_8, this.Symbolclock_9, this.Symbolclock_10, this.Symbolclock_11);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 350);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Vilken klocka stämmer? Ringa in.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(10, 0);

        this.text_2 = new cjs.Text("Jag ska åka", "15px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(95, 43);

        this.text_3 = new cjs.Text("klockan 4.", "15px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(95, 60);

        this.text_4 = new cjs.Text("Jag ska åka", "15px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(95, 114);

        this.text_5 = new cjs.Text("klockan 9.", "15px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(95, 132);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 510, 180, 10);
        this.roundRect1.setTransform(10, 12);

        this.instance = new lib.p83_1();
        this.instance.setTransform(15, 14, 0.48, 0.48);

        this.Symbolclock_1 = new lib.Symboldotclock();
        this.Symbolclock_1.setTransform(230, 60, 0.83, 0.83);

        this.Symbolclock_2 = new lib.Symboldotclock();
        this.Symbolclock_2.setTransform(340, 60, 0.83, 0.83);

        this.Symbolclock_3 = new lib.Symboldotclock();
        this.Symbolclock_3.setTransform(450, 60, 0.83, 0.83);

        this.Symbolclock_4 = new lib.Symboldotclock();
        this.Symbolclock_4.setTransform(230, 145, 0.83, 0.83);

        this.Symbolclock_5 = new lib.Symboldotclock();
        this.Symbolclock_5.setTransform(340, 145, 0.83, 0.83);

        this.Symbolclock_6 = new lib.Symboldotclock();
        this.Symbolclock_6.setTransform(450, 145, 0.83, 0.83);

        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.text_2, this.text_3, this.text_4, this.text_5,
            this.Symbolclock_1, this.Symbolclock_2, this.Symbolclock_3, this.Symbolclock_4, this.Symbolclock_5, this.Symbolclock_6);


        //row-1

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(240, 60.5, 1.2, 0.7, 89);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(230, 46.5, 1.2, 0.9);
        this.text_dot1 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot1.setTransform(224.2, 58.4);

        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(348, 65, 1.2, 0.7, 299);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(340, 46.5, 1.2, 0.9);
        this.text_dot2 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot2.setTransform(334, 58.4);

        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(454, 67.5, 1.2, 0.7, 329);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(450, 46.5, 1.2, 0.9);
        this.text_dot3 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot3.setTransform(444, 58.4);

        //row-2

        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(220, 145.5, 1.2, 0.7, 272);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(230.5, 132.1, 1.2, 0.9);
        this.text_dot4 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot4.setTransform(224, 144.5);

        this.shape_9 = new cjs.Shape(); // clock handle red
        this.shape_9.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_9.setTransform(332.5, 141, 1.2, 0.7, 480);
        this.shape_10 = new cjs.Shape(); // clock handle blue
        this.shape_10.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_10.setTransform(340.5, 132, 1.2, 0.9);
        this.text_dot5 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot5.setTransform(334.5, 144.5);

        this.shape_11 = new cjs.Shape(); // clock handle red
        this.shape_11.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_11.setTransform(460, 145.5, 1.2, 0.7, 90);
        this.shape_12 = new cjs.Shape(); // clock handle blue
        this.shape_12.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_12.setTransform(450.5, 132, 1.2, 0.9);
        this.text_dot6 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot6.setTransform(444.5, 144.5);

        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10, this.shape_11, this.shape_12, this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4, this.text_dot5, this.text_dot6)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 180);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(304, 103, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(304, 498, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
