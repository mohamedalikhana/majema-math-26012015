﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MathViewer.Startup))]
namespace MathViewer
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
