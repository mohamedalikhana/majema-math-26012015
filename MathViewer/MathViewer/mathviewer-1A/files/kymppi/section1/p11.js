(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p11_1.png",
            id: "p11_1"
        }, {
            src: "images/p11_2.png",
            id: "p11_2"
        }, {
            src: "images/p11_3.png",
            id: "p11_3"
        }, ]
    };

    // symbols:
    (lib.p11_1 = function() {
        this.initialize(img.p11_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 962, 99);


    (lib.p11_2 = function() {
        this.initialize(img.p11_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 983, 148);


    (lib.p11_3 = function() {
        this.initialize(img.p11_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 914, 148);


    (lib.Symbol31 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text = new cjs.Text("Ringa in antalet. Rita lika många cirklar.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(0, 5.2);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 433.7, 25.3);


    (lib.Symbol30 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("11", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(555, 7);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(580, 18.4);

        this.addChild(this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 35.1);


    (lib.Symbol15 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABqQAsAAAfgfQAfggAAgrQAAgqgfggQgfgfgsAAQgqAAgfAfQgfAgAAAqQAAArAfAgQAfAfAqAAg");
        this.shape.setTransform(127.9, 10.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_1.setTransform(122.7, 45.3);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_2.setTransform(94.9, 74.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_3.setTransform(94.9, 45.3);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_4.setTransform(66, 74.2);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_5.setTransform(66, 45.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_6.setTransform(37.7, 74.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_7.setTransform(37.7, 45.3);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAdgdQAdgcAAgpQAAgngdgdQgdgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_8.setTransform(9.9, 74.2);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAdgdQAdgcAAgpQAAgngdgdQgdgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_9.setTransform(9.9, 45.3);

        this.addChild(this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 140.5, 86.1);


    (lib.Symbol14 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABqQArAAAfgfQAfggAAgrQAAgqgfggQgfgfgrAAQgrAAgfAfQgfAgAAAqQAAArAfAgQAfAfArAAg");
        this.shape.setTransform(171.5, 10.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAdgdQAdgcAAgpQAAgngdgdQgdgdgpAAQgnAAgeAdQgcAdAAAnQAAApAcAcQAeAdAnAAg");
        this.shape_1.setTransform(122.7, 74.2);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAdgdQAdgcAAgpQAAgngdgdQgdgdgpAAQgnAAgeAdQgcAdAAAnQAAApAcAcQAeAdAnAAg");
        this.shape_2.setTransform(122.7, 45.3);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_3.setTransform(94.9, 74.2);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_4.setTransform(94.9, 45.3);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_5.setTransform(66, 74.2);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_6.setTransform(66, 45.3);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_7.setTransform(37.7, 74.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
        this.shape_8.setTransform(37.7, 45.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_9.setTransform(9.9, 74.2);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
        this.shape_10.setTransform(9.9, 45.3);

        this.addChild(this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 184.1, 86.1);


    (lib.Symbol13 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABpQArAAAfgfQAfgeAAgsQAAgqgfgfQgfgfgrAAQgrAAgfAfQgfAfAAAqQAAAsAfAeQAfAfArAAg");
        this.shape.setTransform(45.5, 10.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
        this.shape_1.setTransform(122.7, 47);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQApAAAdgdQAdgdAAgpQAAgngdgeQgdgcgpAAQgnAAgeAcQgcAeAAAnQAAApAcAdQAeAdAnAAg");
        this.shape_2.setTransform(94.9, 47);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
        this.shape_3.setTransform(66, 47);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgdAAgoQAAgogdgcQgcgdgpAAQgnAAgdAdQgdAcAAAoQAAAoAdAdQAdAdAnAAg");
        this.shape_4.setTransform(37.6, 74.2);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
        this.shape_5.setTransform(37.6, 47);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgdAAgoQAAgogdgcQgdgdgoAAQgoAAgcAdQgdAcAAAoQAAAoAdAdQAcAdAoAAg");
        this.shape_6.setTransform(9.9, 74.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
        this.shape_7.setTransform(9.9, 47);

        this.addChild(this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 134.6, 86.1);


    (lib.Symbol12 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABpQArAAAfgfQAfgeAAgsQAAgqgfgfQgfgfgrAAQgrAAgfAfQgfAfAAAqQAAAsAfAeQAfAfArAAg");
        this.shape.setTransform(10.6, 10.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQApAAAdgdQAdgdAAgpQAAgngdgeQgdgcgpAAQgnAAgeAcQgcAeAAAnQAAApAcAdQAeAdAnAAg");
        this.shape_1.setTransform(128, 46);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
        this.shape_2.setTransform(100.2, 46);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
        this.shape_3.setTransform(71.3, 46);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
        this.shape_4.setTransform(43, 46);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgdAAgoQAAgogdgcQgcgdgpAAQgnAAgdAdQgdAcAAAoQAAAoAdAdQAdAdAnAAg");
        this.shape_5.setTransform(15.2, 73.2);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
        this.shape_6.setTransform(15.2, 46);

        this.addChild(this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 139.9, 85.1);


    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgdAAgoQAAgogdgcQgcgdgpAAQgnAAgdAdQgdAcAAAoQAAAoAdAdQAdAdAnAAg");
        this.shape.setTransform(122.7, 42.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAdgdQAdgdAAgoQAAgogdgcQgdgdgpAAQgnAAgdAdQgdAcAAAoQAAAoAdAdQAdAdAnAAg");
        this.shape_1.setTransform(94.9, 42.6);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgdAAgoQAAgogdgcQgcgdgpAAQgnAAgdAdQgdAcAAAoQAAAoAdAdQAdAdAnAAg");
        this.shape_2.setTransform(66, 42.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQApAAAcgdQAdgdAAgoQAAgogdgcQgcgdgpAAQgnAAgdAdQgdAcAAAoQAAAoAdAdQAdAdAnAAg");
        this.shape_3.setTransform(37.6, 42.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABiQAoAAAdgdQAdgdAAgoQAAgogdgcQgdgdgoAAQgoAAgcAdQgdAcAAAoQAAAoAdAdQAcAdAoAAg");
        this.shape_4.setTransform(9.9, 42.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#DE3B7A").ss(1.5, 0, 0, 4).p("AAABpQArAAAfgfQAfgeAAgsQAAgqgfgfQgfgfgrAAQgrAAgeAfQgfAfAAAqQAAAsAfAeQAeAfArAAg");
        this.shape_5.setTransform(163.1, 10.6);

        this.addChild(this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 175.7, 54.5);


    (lib.Symbol5 = function() {
        this.initialize();

        // Layer 2
        this.ra2 = new lib.Symbol15();
        this.ra2.setTransform(389.7, 124.2, 1, 1, 0, 0, 0, 69.3, 42.1);

        this.ra1 = new lib.Symbol14();
        this.ra1.setTransform(148.5, 124.2, 1, 1, 0, 0, 0, 91, 42.1);

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape.setTransform(180.5, 170);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_1.setTransform(152, 170);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_2.setTransform(123.7, 170);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_3.setTransform(95.4, 170);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_4.setTransform(66.9, 170);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_5.setTransform(194.6, 155.8);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_6.setTransform(166.2, 155.8);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_7.setTransform(180.5, 141.6);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_8.setTransform(137.9, 155.8);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_9.setTransform(152, 141.6);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_10.setTransform(109.6, 155.8);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_11.setTransform(123.7, 141.6);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_12.setTransform(81.2, 155.8);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_13.setTransform(95.4, 141.6);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_14.setTransform(52.9, 155.8);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_15.setTransform(66.9, 141.6);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_16.setTransform(194.6, 127.4);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_17.setTransform(166.2, 127.4);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_18.setTransform(180.5, 113.3);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_19.setTransform(137.9, 127.4);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_20.setTransform(152, 113.3);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_21.setTransform(109.6, 127.4);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_22.setTransform(123.7, 113.3);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_23.setTransform(81.2, 127.4);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_24.setTransform(95.4, 113.3);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_25.setTransform(52.9, 127.4);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_26.setTransform(66.9, 113.3);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_27.setTransform(442.7, 170);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_28.setTransform(414.2, 170);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_29.setTransform(385.9, 170);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_30.setTransform(357.5, 170);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_31.setTransform(329.1, 170);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_32.setTransform(456.8, 155.8);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_33.setTransform(428.4, 155.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_34.setTransform(442.7, 141.6);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_35.setTransform(400.1, 155.8);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_36.setTransform(414.2, 141.6);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_37.setTransform(371.7, 155.8);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_38.setTransform(385.9, 141.6);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_39.setTransform(343.4, 155.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_40.setTransform(357.5, 141.6);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_41.setTransform(315, 155.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_42.setTransform(329.1, 141.6);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_43.setTransform(456.8, 127.4);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_44.setTransform(428.4, 127.4);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_45.setTransform(442.7, 113.3);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_46.setTransform(400.1, 127.4);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_47.setTransform(414.2, 113.3);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_48.setTransform(371.7, 127.4);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_49.setTransform(385.9, 113.3);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_50.setTransform(343.4, 127.4);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_51.setTransform(357.5, 113.3);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_52.setTransform(315, 127.4);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_53.setTransform(329.1, 113.3);

        this.text = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(479.7, 81.8);

        this.text_1 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(442.2, 81.8);

        this.text_2 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(400.6, 81.8);

        this.text_3 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(358.9, 81.8);

        this.text_4 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(317.2, 81.8);

        this.text_5 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(275.6, 81.8);

        this.text_6 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(218.3, 81.8);

        this.text_7 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(180.9, 81.8);

        this.text_8 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(139.2, 81.8);

        this.text_9 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(97.6, 81.8);

        this.text_10 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(55.9, 81.8);

        this.text_11 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(14.3, 81.8);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#818888").ss(0.5, 0, 0, 4).p("Ax4ttQgHAAgLACQgVADgSAJQg5AcAABHIAAX5IACARQAEAWAIASQAdA4BHAAMAjxAAAQAcAAAdgOQA4gcAAhHIAA35QAAgcgNgcQgdg5hHAAg");
        this.shape_54.setTransform(383.9, 87.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#818888").ss(0.5, 0, 0, 4).p("Ax4ttQgHAAgLACQgVADgSAJQg5AcAABHIAAX5IACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAA35QAAgcgPgcQgcg5hHAAg");
        this.shape_55.setTransform(125.9, 87.9);

        this.instance = new lib.p11_3();
        this.instance.setTransform(34.9, 6.6, 0.5, 0.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFFFFF").s().p("Ax4NuQhHAAgdg4QgIgSgEgWIgBgRIAA35QAAhHA4gcQASgJAWgDQAKgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAX5QAABHg5AcQgcAOgdAAg");
        this.shape_56.setTransform(383.9, 87.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("Ax4NuQhHAAgdg4QgIgSgDgWIgCgRIAA35QAAhHA4gcQASgJAVgDQALgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAX5QAABHg5AcQgcAOgdAAg");
        this.shape_57.setTransform(125.9, 87.9);

        this.addChild(this.shape_57, this.shape_56, this.instance, this.shape_55, this.shape_54, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.ra1, this.ra2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.8, 177.8);


    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 2
        this.ra2 = new lib.Symbol13();
        this.ra2.setTransform(383.1, 123.5, 1, 1, 0, 0, 0, 66.3, 42.1);

        this.ra1 = new lib.Symbol12();
        this.ra1.setTransform(121.2, 124, 1, 1, 0, 0, 0, 69, 41.6);

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape.setTransform(180.7, 169.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_1.setTransform(152.2, 169.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_2.setTransform(123.9, 169.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_3.setTransform(95.5, 169.5);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_4.setTransform(67.1, 169.5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_5.setTransform(194.8, 155.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_6.setTransform(166.4, 155.3);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_7.setTransform(180.7, 141.1);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_8.setTransform(138.1, 155.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_9.setTransform(152.2, 141.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_10.setTransform(109.7, 155.3);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_11.setTransform(123.9, 141.1);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_12.setTransform(81.4, 155.3);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_13.setTransform(95.5, 141.1);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_14.setTransform(53, 155.3);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_15.setTransform(67.1, 141.1);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_16.setTransform(194.8, 126.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_17.setTransform(166.4, 126.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_18.setTransform(180.7, 112.8);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_19.setTransform(138.1, 126.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_20.setTransform(152.2, 112.8);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_21.setTransform(109.7, 126.9);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_22.setTransform(123.9, 112.8);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_23.setTransform(81.4, 126.9);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_24.setTransform(95.5, 112.8);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_25.setTransform(53, 126.9);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_26.setTransform(67.1, 112.8);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_27.setTransform(439.3, 169.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_28.setTransform(410.9, 169.5);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_29.setTransform(382.5, 169.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_30.setTransform(354.2, 169.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_31.setTransform(325.7, 169.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_32.setTransform(453.4, 155.3);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_33.setTransform(425.1, 155.3);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_34.setTransform(439.3, 141.1);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_35.setTransform(396.7, 155.3);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_36.setTransform(410.9, 141.1);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_37.setTransform(368.4, 155.3);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_38.setTransform(382.5, 141.1);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_39.setTransform(340, 155.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_40.setTransform(354.2, 141.1);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_41.setTransform(311.7, 155.3);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_42.setTransform(325.7, 141.1);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_43.setTransform(453.4, 126.9);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_44.setTransform(425.1, 126.9);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_45.setTransform(439.3, 112.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_46.setTransform(396.7, 126.9);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_47.setTransform(410.9, 112.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_48.setTransform(368.4, 126.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_49.setTransform(382.5, 112.8);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_50.setTransform(340, 126.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_51.setTransform(354.2, 112.8);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_52.setTransform(311.7, 126.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_53.setTransform(325.7, 112.8);

        this.text = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(476.8, 81.4);

        this.text_1 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(439.4, 81.4);

        this.text_2 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(397.7, 81.4);

        this.text_3 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(356.1, 81.4);

        this.text_4 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(314.4, 81.4);

        this.text_5 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(272.7, 81.4);

        this.text_6 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(218.3, 81.4);

        this.text_7 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(180.9, 81.4);

        this.text_8 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(139.2, 81.4);

        this.text_9 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(97.6, 81.4);

        this.text_10 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(55.9, 81.4);

        this.text_11 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(14.3, 81.4);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#818888").ss(0.5, 0, 0, 4).p("Ax4ttQgdAAgcAOQg5AcAABHIAAX5IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAA35QAAgcgNgcQgdg5hHAAg");
        this.shape_54.setTransform(383.9, 87.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#818888").ss(0.5, 0, 0, 4).p("Ax4ttQgHAAgLACQgVADgSAJQg5AcAABHIAAX5IACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAA35QAAgcgPgcQgcg5hHAAg");
        this.shape_55.setTransform(125.9, 87.9);

        this.instance = new lib.p11_2();
        this.instance.setTransform(8.1, 7.4, 0.5, 0.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFFFFF").s().p("Ax4NuQhHAAgdg4QgIgSgEgWIgBgRIAA35QAAhHA4gcQAcgOAdAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAX5QAABHg5AcQgSAJgWADIgRACg");
        this.shape_56.setTransform(383.9, 87.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("Ax4NuQhHAAgdg4QgIgSgDgWIgCgRIAA35QAAhHA4gcQASgJAVgDQALgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAX5QAABHg5AcQgcAOgdAAg");
        this.shape_57.setTransform(125.9, 87.9);

        this.addChild(this.shape_57, this.shape_56, this.instance, this.shape_55, this.shape_54, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.ra1, this.ra2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.8, 177.8);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 2
        this.ra1 = new lib.Symbol11();
        this.ra1.setTransform(410.5, 142.9, 1, 1, 0, 0, 0, 86.8, 26.3);

        // Layer 1
        this.instance = new lib.p11_1();
        this.instance.setTransform(24, 49.8, 0.5, 0.5);

        this.instance_1 = new lib.Symbol31();
        this.instance_1.setTransform(234.8, 12.9, 1, 1, 0, 0, 0, 215.8, 11.9);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#6C7373").ss(1, 0, 0, 4).p("AAABlQApAAAegeQAegeAAgpQAAgpgegdQgegegpAAQgpAAgdAeQgeAdAAApQAAApAeAeQAdAeApAAg");
        this.shape.setTransform(175.8, 125.7);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#6C7373").ss(1, 0, 0, 4).p("AAABlQAqAAAdgeQAegdAAgqQAAgogegeQgdgegqAAQgoAAgeAeQgeAeAAAoQAAAqAeAdQAeAeAoAAg");
        this.shape_1.setTransform(155.6, 158.2);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#6C7373").ss(1, 0, 0, 4).p("AAABlQApAAAegeQAegdAAgqQAAgogegeQgegegpAAQgoAAgeAeQgeAeAAAoQAAAqAeAdQAeAeAoAAg");
        this.shape_2.setTransform(126.6, 158.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#6C7373").ss(1, 0, 0, 4).p("AAABlQAqAAAdgeQAegdAAgqQAAgogegeQgdgegqAAQgoAAgeAeQgeAeAAAoQAAAqAeAdQAeAeAoAAg");
        this.shape_3.setTransform(98.9, 158.2);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#6C7373").ss(1, 0, 0, 4).p("AAABlQApAAAegeQAegdAAgqQAAgogegeQgegegpAAQgpAAgdAeQgeAeAAAoQAAAqAeAdQAdAeApAAg");
        this.shape_4.setTransform(69.7, 158.2);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_5.setTransform(446.1, 201.2);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_6.setTransform(417.7, 201.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_7.setTransform(389.3, 201.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_8.setTransform(361, 201.2);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_9.setTransform(332.5, 201.2);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_10.setTransform(460.2, 187);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_11.setTransform(431.9, 187);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_12.setTransform(446.1, 172.8);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_13.setTransform(403.5, 187);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_14.setTransform(417.7, 172.8);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_15.setTransform(375.2, 187);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_16.setTransform(389.3, 172.8);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_17.setTransform(346.8, 187);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_18.setTransform(361, 172.8);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_19.setTransform(318.5, 187);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_20.setTransform(332.5, 172.8);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_21.setTransform(460.2, 158.6);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_22.setTransform(431.9, 158.6);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_23.setTransform(446.1, 144.5);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_24.setTransform(403.5, 158.6);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_25.setTransform(417.7, 144.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_26.setTransform(375.2, 158.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_27.setTransform(389.3, 144.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_28.setTransform(346.8, 158.6);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_29.setTransform(361, 144.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_30.setTransform(318.5, 158.6);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_31.setTransform(332.5, 144.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_32.setTransform(183.5, 201.2);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_33.setTransform(155.1, 201.2);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_34.setTransform(126.7, 201.2);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_35.setTransform(98.4, 201.2);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_36.setTransform(69.9, 201.2);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_37.setTransform(197.6, 187);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_38.setTransform(169.3, 187);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_39.setTransform(183.5, 172.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_40.setTransform(140.9, 187);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_41.setTransform(155.1, 172.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_42.setTransform(112.6, 187);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_43.setTransform(126.7, 172.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_44.setTransform(84.2, 187);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_45.setTransform(98.4, 172.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_46.setTransform(55.9, 187);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_47.setTransform(69.9, 172.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_48.setTransform(197.6, 158.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_49.setTransform(169.3, 158.6);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_50.setTransform(183.5, 144.5);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_51.setTransform(140.9, 158.6);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_52.setTransform(155.1, 144.5);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_53.setTransform(112.6, 158.6);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_54.setTransform(126.7, 144.5);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_55.setTransform(84.2, 158.6);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
        this.shape_56.setTransform(98.4, 144.5);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
        this.shape_57.setTransform(55.9, 158.6);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
        this.shape_58.setTransform(69.9, 144.5);

        this.text = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text.lineHeight = 25;
        this.text.setTransform(480.7, 117.8);

        this.text_1 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 25;
        this.text_1.setTransform(430.7, 117.8);

        this.text_2 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(381.7, 117.8);

        this.text_3 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(331.7, 117.8);

        this.text_4 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(281.7, 117.8);

        this.text_5 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(218.7, 117.8);

        this.text_6 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 25;
        this.text_6.setTransform(168.7, 117.8);

        this.text_7 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(118.7, 117.8);

        this.text_8 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(69.7, 117.8);

        this.text_9 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(19.7, 117.8);

        this.text_10 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_10.lineHeight = 20;
        this.text_10.setTransform(0, 5);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#818888").ss(0.5, 0, 0, 4).p("Ax4ttQgdAAgcAOQg5AcAABHIAAX5IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAA35QAAgcgNgcQgdg5hHAAg");
        this.shape_59.setTransform(386.4, 120);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFFFFF").s().p("Ax4NuQhHAAgdg4QgIgSgEgWIgBgRIAA35QAAhHA4gcQAcgOAdAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAX5QAABHg5AcQgSAJgWADIgRACg");
        this.shape_60.setTransform(386.4, 120);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#818888").ss(0.5, 0, 0, 4).p("Ax4ttQgdAAgcAOQg5AcAABHIAAX5IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA5gcAAhHIAA35QAAgcgPgcQgcg5hHAAg");
        this.shape_61.setTransform(128.4, 120);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFFFFF").s().p("Ax4NuQhHAAgdg4QgIgSgDgWIgCgRIAA35QAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAX5QAABHg5AcQgSAJgVADIgSACg");
        this.shape_62.setTransform(128.4, 120);

        this.addChild(this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.instance_1, this.instance, this.ra1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 208.9);


    // stage content:
    (lib.p11 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol30();
        this.other.setTransform(609.5, 660.2, 1, 1, 0, 0, 0, 609.5, 17.5);

        this.v3 = new lib.Symbol5();
        this.v3.setTransform(312.8, 547.4, 1, 1, 0, 0, 0, 254.8, 87.9);

        this.v2 = new lib.Symbol4();
        this.v2.setTransform(312.8, 358.2, 1, 1, 0, 0, 0, 254.8, 87.9);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(311.8, 155, 1, 1, 0, 0, 0, 256.3, 104.1);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 389.6, 1218.9, 627.1);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
