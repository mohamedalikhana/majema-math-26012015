(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/s28_29_1.png",
            id: "s28_29_1"
        }, {
            src: "images/s28_29_2.png",
            id: "s28_29_2"
        }, {
            src: "images/p26_1.png",
            id: "p26_1"
        }, {
            src: "images/p26_2.png",
            id: "p26_2"
        }]
    };
    // symbols:
    (lib.s28_29_1 = function() {
        this.initialize(img.s28_29_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.s28_29_2 = function() {
        this.initialize(img.s28_29_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p26_1 = function() {
        this.initialize(img.p26_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p26_2 = function() {
        this.initialize(img.p26_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 0 till 4", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(67.5, 651);

        this.text_1 = new cjs.Text("26", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 18;
        this.text_1.setTransform(37, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.text_2 = new cjs.Text("Talet 0", "24px 'MyriadPro-Semibold'", "#00A5C0");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(93.5, 25);

        this.text_3 = new cjs.Text("8", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.lineHeight = 34;
        this.text_3.setTransform(56.7, 22);

        this.instance = new lib.s28_29_2();
        this.instance.setTransform(42.5, 394.9, 0.9, 0.9);

        this.text_4 = new cjs.Text("29", "bold 20px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 24;
        this.text_4.setTransform(1159.7, 642.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_3.setTransform(43.6, 23.5);

        this.addChild(this.shape_3, this.shape_2, this.shape_1, this.text_4, this.instance, this.text_3, this.text_2, this.shape, this.text_1, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol11 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text = new cjs.Text("kuin yk-si.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(0, 21.6);

        this.text_1 = new cjs.Text("yh-tä suu-ri", "bold 16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(66.5, 0);

        this.text_2 = new cjs.Text("Yk-si on ", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(0, 0.6);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }, {
                t: this.text_1
            }, {
                t: this.text
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 153.6, 46.3);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.shape_324 = new cjs.Shape();
        this.shape_324.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_324.setTransform(227.5, 41.4);

        this.shape_325 = new cjs.Shape();
        this.shape_325.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_325.setTransform(262.3, 41.6);

        this.shape_326 = new cjs.Shape();
        this.shape_326.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_326.setTransform(239.1, 41.6);

        this.shape_327 = new cjs.Shape();
        this.shape_327.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_327.setTransform(216.5, 41.6);

        this.shape_328 = new cjs.Shape();
        this.shape_328.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_328.setTransform(194, 41.6);

        this.shape_329 = new cjs.Shape();
        this.shape_329.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfQgLAAgMAFQgWAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAIAXgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_329.setTransform(227.8, 41.4);

        this.shape_330 = new cjs.Shape();
        this.shape_330.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAWgMQAMgFALAAIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMIgXAFg");
        this.shape_330.setTransform(227.8, 41.4);

        this.shape_331 = new cjs.Shape();
        this.shape_331.graphics.f("#000000").s().p("AgEgeIAhAeIg5Afg");
        this.shape_331.setTransform(50.7, 27.2);

        this.shape_332 = new cjs.Shape();
        this.shape_332.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("ABDgZQhUAAguA1");
        this.shape_332.setTransform(57.3, 24);

        this.text = new cjs.Text("0 knutar", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(376.3, 79.3);

        this.text_1 = new cjs.Text("1 knut", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(384.3, 41.3);

        this.text_2 = new cjs.Text("noll", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(44.8, 86);

        this.text_3 = new cjs.Text("0", "bold 80px 'UusiTekstausMajema'");
        this.text_3.lineHeight = 96;
        this.text_3.setTransform(41.6, 4.8);

        this.shape_333 = new cjs.Shape();
        this.shape_333.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgFAFAAAFQAAAGAFAFQAFAEAFAAg");
        this.shape_333.setTransform(285.5, 76.9);

        this.shape_334 = new cjs.Shape();
        this.shape_334.graphics.f("#000000").s().p("AgKALQgEgFgBgGQABgFAEgFQAFgEAFAAQAGAAAFAEQAEAFABAFQgBAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_334.setTransform(285.5, 76.9);

        this.shape_335 = new cjs.Shape();
        this.shape_335.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_335.setTransform(256.7, 76.9);

        this.shape_336 = new cjs.Shape();
        this.shape_336.graphics.f("#000000").s().p("AgKALQgFgFAAgGQAAgFAFgFQAFgEAFAAQAGAAAFAEQAEAFABAFQgBAGgEAFQgEAEgHAAQgFAAgFgEg");
        this.shape_336.setTransform(256.7, 76.9);

        this.shape_337 = new cjs.Shape();
        this.shape_337.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_337.setTransform(228.3, 76.9);

        this.shape_338 = new cjs.Shape();
        this.shape_338.graphics.f("#000000").s().p("AgKALQgFgFAAgGQAAgFAFgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgEAEgHAAQgFAAgFgEg");
        this.shape_338.setTransform(228.3, 76.9);

        this.shape_339 = new cjs.Shape();
        this.shape_339.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_339.setTransform(199.5, 76.9);

        this.shape_340 = new cjs.Shape();
        this.shape_340.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_340.setTransform(199.5, 76.9);

        this.shape_341 = new cjs.Shape();
        this.shape_341.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_341.setTransform(171.2, 76.9);

        this.shape_342 = new cjs.Shape();
        this.shape_342.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_342.setTransform(171.2, 76.9);

        this.shape_343 = new cjs.Shape();
        this.shape_343.graphics.f("#000000").s().p("AgegWIA9AWIg9AXg");
        this.shape_343.setTransform(301.7, 76.7);

        this.shape_344 = new cjs.Shape();
        this.shape_344.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AqtAAIVbAA");
        this.shape_344.setTransform(231.5, 76.7);

        this.text_4 = new cjs.Text("4", "bold 16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(278.6, 77.5);

        this.text_5 = new cjs.Text("3", "bold 16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(249.5, 77.5);

        this.text_6 = new cjs.Text("2", "bold 16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(221.5, 77.5);

        this.text_7 = new cjs.Text("1", "bold 16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(192.5, 77.5);

        this.text_8 = new cjs.Text("0", "bold 16px 'Myriad Pro'", "#0089CA");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(165, 77.5);

        this.shape_345 = new cjs.Shape();
        this.shape_345.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gHZQgdAAgcAOQg5AdAABHIAALQIACARQAEAWAIARQAdA5BHAAMBL/AAAIASgCQAVgEASgIQA5gdAAhGIAArQQAAgdgOgcQgdg5hHAAg");
        this.shape_345.setTransform(254.6, 58.7);

        this.shape_346 = new cjs.Shape();
        this.shape_346.graphics.f("#FFF173").s().p("Egl/AHaQhHAAgdg5QgIgRgEgWIgCgRIAArQQAAhHA5gdQAcgOAdAAMBL/AAAQBHAAAdA5QAOAcAAAdIAALQQAABGg5AdQgSAIgVAEIgSACg");
        this.shape_346.setTransform(254.6, 58.7);

        this.instance = new lib.p26_1();
        this.instance.setTransform(366, 18, 0.4, 0.4);

        this.instance_2 = new lib.p26_2();
        this.instance_2.setTransform(362, 67, 0.4, 0.4);

        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(130, 66).lineTo(330.5, 66).moveTo(330.5, 66).lineTo(330.5, 64).lineTo(337, 66).lineTo(330.5, 68).lineTo(330.5, 66);
        var numberLineLimit = 0
        for (var dot = 0; dot < 11; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(130 + (20 * dot), 61).lineTo(130 + (20 * dot), 71);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(129, 66).lineTo(130 + (20 * numberLineLimit), 66);

        for (var dot = 0; dot < 11; dot++) {
            var temptext = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 13px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot == 10) ? 9.8 : dot;
            temptext.setTransform(127 + (20 * colSpace), 88);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);

        this.addChild(this.shape_346, this.shape_345, this.text_3, this.text_2, this.text_1, this.text, this.shape_332, this.shape_331, this.shape_330, this.shape_329, this.shape_328, this.shape_327, this.shape_326, this.shape_325, this.shape_324, this.shape_323, this.shape_322, this.shape_321, this.shape_320, this.shape_319, this.shape_318, this.shape_317, this.shape_316, this.shape_315, this.shape_314, this.shape_313, this.shape_312, this.shape_311, this.shape_310, this.shape_309, this.shape_308, this.shape_307, this.shape_306, this.shape_305, this.shape_304, this.shape_303, this.shape_302, this.shape_301, this.shape_300, this.shape_299, this.shape_298, this.shape_297, this.shape_296, this.shape_295, this.shape_294, this.shape_293, this.shape_292, this.shape_291, this.shape_290, this.shape_289, this.shape_288, this.shape_287, this.shape_286, this.shape_285, this.shape_284, this.shape_283, this.shape_282, this.shape_281, this.shape_280, this.shape_279, this.shape_278, this.shape_277, this.shape_276, this.shape_275, this.shape_274, this.shape_273, this.shape_272, this.shape_271, this.shape_270, this.shape_269, this.shape_268, this.shape_267, this.shape_266, this.shape_265, this.shape_264, this.shape_263, this.shape_262, this.shape_261, this.shape_260, this.shape_259, this.shape_258, this.shape_257, this.shape_256, this.shape_255, this.shape_254, this.shape_253, this.shape_252, this.shape_251, this.shape_250, this.shape_249, this.shape_248, this.shape_247, this.shape_246, this.shape_245, this.shape_244, this.shape_243, this.shape_242, this.shape_241, this.shape_240, this.shape_239, this.shape_238, this.shape_237, this.shape_236, this.shape_235, this.shape_234, this.shape_233, this.shape_232, this.shape_231, this.shape_230, this.shape_229, this.shape_228, this.shape_227, this.shape_226, this.shape_225, this.shape_224, this.shape_223, this.shape_222, this.shape_221, this.shape_220, this.shape_219, this.shape_218, this.shape_217, this.shape_216, this.shape_215, this.shape_214, this.shape_213, this.shape_212, this.shape_211, this.shape_210, this.shape_209, this.shape_208, this.shape_207, this.shape_206, this.shape_205, this.shape_204, this.shape_203, this.shape_202, this.shape_201, this.shape_200, this.shape_199, this.shape_198, this.shape_197, this.shape_196, this.shape_195, this.shape_194, this.shape_193, this.shape_192, this.shape_191, this.shape_190, this.shape_189, this.shape_188, this.shape_187, this.shape_186, this.shape_185, this.shape_184, this.shape_183, this.shape_182, this.shape_181, this.shape_180, this.shape_179, this.shape_178, this.shape_177, this.shape_176, this.shape_175, this.shape_174, this.shape_173, this.shape_172, this.shape_171, this.shape_170, this.shape_169, this.shape_168, this.shape_167, this.shape_166, this.shape_165, this.shape_164, this.shape_163, this.shape_162, this.shape_161, this.shape_160, this.shape_159, this.shape_158, this.shape_157, this.shape_156, this.shape_155, this.shape_154, this.shape_153, this.shape_152, this.shape_151, this.shape_150, this.shape_149, this.shape_148, this.shape_147, this.shape_146, this.shape_145, this.shape_144, this.shape_143, this.shape_142, this.shape_141, this.shape_140, this.shape_139, this.shape_138, this.shape_137, this.shape_136, this.shape_135, this.shape_134, this.shape_133, this.shape_132, this.shape_131, this.shape_130, this.shape_129, this.shape_128, this.shape_127, this.shape_126, this.shape_125, this.shape_124, this.shape_123, this.shape_122, this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape);
        this.addChild(this.instance, this.instance_2, this.numberLine);
        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 511.3, 107.2);


    (lib.Symbol6 = function() {
        this.initialize();

        // Layer 2


        // Layer 1
        this.instance = new lib.s28_29_1();
        this.instance.setTransform(372.2, 31.1, 0.197, 0.197);

        this.text = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(272.7, 119.9);

        this.text_1 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(233, 119.9);

        this.text_2 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(173.2, 119.9);

        this.text_3 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(133.5, 119.9);

        this.text_4 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(73.9, 119.9);

        this.text_5 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(34.2, 119.9);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape.setTransform(60.1, 142.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_1.setTransform(70.1, 131.2);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_2.setTransform(50.2, 131.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_3.setTransform(60.1, 119.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_4.setTransform(159.4, 142.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_5.setTransform(169.4, 131.2);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_6.setTransform(149.5, 131.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_7.setTransform(159.4, 119.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_8.setTransform(258.9, 142.6);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_9.setTransform(268.9, 131.2);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_10.setTransform(249, 131.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_11.setTransform(258.9, 119.9);

        this.text_6 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(272.2, 91.7);

        this.text_7 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(232.5, 91.7);

        this.text_8 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(173.2, 91.7);

        this.text_9 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(133.5, 91.7);

        this.text_10 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(73.9, 91.7);

        this.text_11 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(34.2, 91.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_12.setTransform(60.1, 114.4);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_13.setTransform(70.1, 103);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_14.setTransform(50.2, 103);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_15.setTransform(60.1, 91.7);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_16.setTransform(159.4, 114.4);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_17.setTransform(169.4, 103);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_18.setTransform(149.5, 103);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_19.setTransform(159.4, 91.7);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_20.setTransform(258.4, 114.4);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_21.setTransform(268.4, 103);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_22.setTransform(248.5, 103);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_23.setTransform(258.4, 91.7);

        this.text_12 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(272.2, 63.6);

        this.text_13 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 19;
        this.text_13.setTransform(232.5, 63.6);

        this.text_14 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(173.2, 63.6);

        this.text_15 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(133.5, 63.6);

        this.text_16 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(73.9, 63.6);

        this.text_17 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_17.lineHeight = 19;
        this.text_17.setTransform(34.2, 63.6);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_24.setTransform(60.1, 86.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_25.setTransform(70.1, 74.8);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_26.setTransform(50.2, 74.8);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_27.setTransform(60.1, 63.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_28.setTransform(159.4, 86.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_29.setTransform(169.4, 74.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_30.setTransform(149.5, 74.8);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_31.setTransform(159.4, 63.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_32.setTransform(258.4, 86.2);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_33.setTransform(268.4, 74.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_34.setTransform(248.5, 74.8);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_35.setTransform(258.4, 63.5);

        this.text_18 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_18.lineHeight = 19;
        this.text_18.setTransform(272.5, 35.4);

        this.text_19 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_19.lineHeight = 19;
        this.text_19.setTransform(232.8, 35.4);

        this.text_20 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_20.lineHeight = 19;
        this.text_20.setTransform(172.8, 35.4);

        this.text_21 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_21.lineHeight = 19;
        this.text_21.setTransform(133.1, 35.4);

        this.text_22 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_22.lineHeight = 19;
        this.text_22.setTransform(73.5, 35.4);

        this.text_23 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_23.lineHeight = 19;
        this.text_23.setTransform(33.8, 35.4);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_36.setTransform(259.2, 58);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_37.setTransform(269.1, 46.7);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_38.setTransform(249.3, 46.7);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_39.setTransform(259.2, 35.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_40.setTransform(159.4, 58);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_41.setTransform(169.4, 46.7);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_42.setTransform(149.5, 46.7);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_43.setTransform(159.4, 35.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_44.setTransform(60.1, 58);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_45.setTransform(70.1, 46.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjd");
        this.shape_46.setTransform(50.2, 46.7);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5).p("AhkAAIDJAA");
        this.shape_47.setTransform(60.1, 35.3);

        this.text_24 = new cjs.Text("Ver-taa.", "16px 'Myriad Pro'");
        this.text_24.lineHeight = 19;
        this.text_24.setTransform(19, 0.6);

        this.text_25 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#00A5C0");
        this.text_25.lineHeight = 19;

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgJYQgdAAgcAOQg5AdAABHIAAPOIACARQAEAWAIASQAdA4BHAAMBMJAAAIASgCQAVgDASgJQA5gcAAhHIAAvOQAAgdgOgcQgdg5hHAAg");
        this.shape_48.setTransform(257.1, 87.1);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#FFFFFF").s().p("EgmEAJZQhHAAgdg5QgIgRgEgWIgCgRIAAvPQAAhHA5gcQAcgOAdAAMBMJAAAQBHAAAdA5QAOAcAAAcIAAPPQAABGg5AdQgSAIgVAEIgSACg");
        this.shape_49.setTransform(257.1, 87.1);

        this.addChild(this.shape_49, this.shape_48, this.text_25, this.text_24, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.instance, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.ra6, this.ra7, this.ra8, this.ra9, this.ra10, this.ra11, this.ra12);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 154.6);


    (lib.Symbol5 = function() {
        this.initialize();

        // Layer 2


        // Layer 1
        this.text = new cjs.Text(">", "bold 35.5px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 46;
        this.text.setTransform(73.8, 68.9);

        this.text_1 = new cjs.Text("2", "bold 35.5px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 46;
        this.text_1.setTransform(34.6, 70.9);

        this.text_2 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 46;
        this.text_2.setTransform(110.7, 70.9);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape.setTransform(480.2, 69.1);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_1.setTransform(480.2, 69.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_2.setTransform(454.9, 69.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_3.setTransform(454.9, 69.1);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_4.setTransform(298.7, 69.8);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_5.setTransform(298.7, 69.8);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_6.setTransform(467, 47.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_7.setTransform(467, 47.5);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_8.setTransform(467, 256.2);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_9.setTransform(467, 256.2);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_10.setTransform(312.5, 256.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_11.setTransform(312.5, 256.2);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_12.setTransform(480, 165.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_13.setTransform(480, 165.1);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_14.setTransform(287.7, 256.2);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_15.setTransform(287.7, 256.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_16.setTransform(455.2, 165.1);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_17.setTransform(455.2, 165.1);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_18.setTransform(121.5, 256.2);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_19.setTransform(121.5, 256.2);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_20.setTransform(121.5, 165.1);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_21.setTransform(121.5, 165.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_22.setTransform(467, 232.6);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_23.setTransform(467, 232.6);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_24.setTransform(312.5, 232.6);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_25.setTransform(312.5, 232.6);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_26.setTransform(480, 141.4);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_27.setTransform(480, 141.4);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_28.setTransform(287.7, 232.6);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_29.setTransform(287.7, 232.6);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_30.setTransform(455.2, 141.4);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_31.setTransform(455.2, 141.4);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_32.setTransform(121.5, 232.6);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_33.setTransform(121.5, 232.6);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_34.setTransform(121.5, 141.4);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_35.setTransform(121.5, 141.4);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_36.setTransform(390.4, 234.2);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_37.setTransform(390.4, 234.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_38.setTransform(390.4, 47.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_39.setTransform(390.4, 47.5);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_40.setTransform(222.8, 47.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_41.setTransform(222.8, 47.5);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_42.setTransform(389.3, 142.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_43.setTransform(389.3, 142.4);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_44.setTransform(222.8, 142.4);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_45.setTransform(222.8, 142.4);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_46.setTransform(31.9, 232.6);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_47.setTransform(31.9, 232.6);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_48.setTransform(211.9, 232.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_49.setTransform(211.9, 232.6);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_50.setTransform(57.4, 232.6);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_51.setTransform(57.4, 232.6);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_52.setTransform(237.4, 232.6);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_53.setTransform(237.4, 232.6);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_54.setTransform(44.9, 142.4);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_55.setTransform(44.9, 142.4);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_56.setTransform(43.9, 47.5);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_57.setTransform(43.9, 47.5);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_58.setTransform(390.4, 256.6);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_59.setTransform(390.4, 256.6);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_60.setTransform(390.4, 69.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_61.setTransform(390.4, 69.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_62.setTransform(222.8, 69.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_63.setTransform(222.8, 69.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_64.setTransform(404.3, 164.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_65.setTransform(404.3, 164.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_66.setTransform(235.8, 164.9);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_67.setTransform(235.8, 164.9);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_68.setTransform(369.6, 245.6);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_69.setTransform(369.6, 245.6);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_70.setTransform(376.3, 164.9);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_71.setTransform(376.3, 164.9);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_72.setTransform(207.8, 164.9);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_73.setTransform(207.8, 164.9);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_74.setTransform(31.9, 255);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_75.setTransform(31.9, 255);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_76.setTransform(211.9, 255);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_77.setTransform(211.9, 255);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_78.setTransform(57.4, 255);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_79.setTransform(57.4, 255);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_80.setTransform(237.4, 255);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_81.setTransform(237.4, 255);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_82.setTransform(44.9, 164.9);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_83.setTransform(44.9, 164.9);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_84.setTransform(43.9, 69.9);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_85.setTransform(43.9, 69.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_86.setTransform(391.1, 282.7);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_87.setTransform(391.1, 282.7);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_88.setTransform(391.1, 190.4);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_89.setTransform(391.1, 190.4);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_90.setTransform(391.1, 96.2);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_91.setTransform(391.1, 96.2);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_92.setTransform(222.5, 282.7);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_93.setTransform(222.5, 282.7);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_94.setTransform(222.5, 190.4);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_95.setTransform(222.5, 190.4);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_96.setTransform(222.5, 96.2);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_97.setTransform(222.5, 96.2);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_98.setTransform(44.6, 282.7);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_99.setTransform(44.6, 282.7);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_100.setTransform(44.6, 190.4);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_101.setTransform(44.6, 190.4);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_102.setTransform(44.6, 96.2);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_103.setTransform(44.6, 96.2);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_104.setTransform(467, 282.7);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_105.setTransform(467, 282.7);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_106.setTransform(467, 190.4);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_107.setTransform(467, 190.4);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_108.setTransform(467, 96.2);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_109.setTransform(467, 96.2);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_110.setTransform(298.4, 282.7);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_111.setTransform(298.4, 282.7);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_112.setTransform(298.4, 190.4);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_113.setTransform(298.4, 190.4);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_114.setTransform(298.4, 96.2);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_115.setTransform(298.4, 96.2);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_116.setTransform(120.5, 282.7);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_117.setTransform(120.5, 282.7);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_118.setTransform(120.5, 190.4);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_119.setTransform(120.5, 190.4);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_120.setTransform(120.5, 96.2);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_121.setTransform(120.5, 96.2);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_122.setTransform(429.5, 282.7);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_123.setTransform(429.5, 282.7);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_124.setTransform(429.5, 190.4);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_125.setTransform(429.5, 190.4);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_126.setTransform(429.5, 96.2);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_127.setTransform(429.5, 96.2);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_128.setTransform(260.8, 282.7);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_129.setTransform(260.8, 282.7);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_130.setTransform(260.8, 190.4);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_131.setTransform(260.8, 190.4);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_132.setTransform(260.8, 96.2);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_133.setTransform(260.8, 96.2);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_134.setTransform(82.9, 282.7);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_135.setTransform(82.9, 282.7);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_136.setTransform(82.9, 190.4);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_137.setTransform(82.9, 190.4);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_138.setTransform(82.9, 96.2);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_139.setTransform(82.9, 96.2);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAPQg4AcAABHIAAJtIABASQAEAWAIARQAdA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgNgcQgdg5hHAAg");
        this.shape_140.setTransform(256.9, 70.5);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgdg5QgIgRgDgWIgCgSIAAptQAAhHA4gcQAdgOAcgBIWfAAQBHABAcA4QAOAdABAcIAAJtQgBBHg4AcQgSAJgWADIgRADg");
        this.shape_141.setTransform(256.9, 70.5);

        this.shape_142 = new cjs.Shape();
        this.shape_142.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAOQg4AdAABHIAAJtIABASQAEAVAIASQAdA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgNgcQgdg5hHAAg");
        this.shape_142.setTransform(256.9, 255.7);

        this.shape_143 = new cjs.Shape();
        this.shape_143.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgdg5QgIgSgDgVIgCgSIAAptQAAhHA4gdQAdgOAcAAIWfAAQBHAAAcA5QAOAcABAdIAAJtQgBBHg4AdQgSAIgWAEIgRACg");
        this.shape_143.setTransform(256.9, 255.7);

        this.shape_144 = new cjs.Shape();
        this.shape_144.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAOQg4AdAABHIAAJtIABASQAEAVAIASQAdA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgNgcQgdg5hHAAg");
        this.shape_144.setTransform(256.9, 163.5);

        this.shape_145 = new cjs.Shape();
        this.shape_145.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgdg5QgIgSgDgVIgCgSIAAptQAAhHA4gdQAdgOAcAAIWfAAQBHAAAcA5QAOAcABAdIAAJtQgBBHg4AdQgSAIgWAEIgRACg");
        this.shape_145.setTransform(256.9, 163.5);

        this.shape_146 = new cjs.Shape();
        this.shape_146.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAPQg4AcAABHIAAJtIACASQADAWAJARQAcA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgOgcQgcg5hHAAg");
        this.shape_146.setTransform(428.9, 70.5);

        this.shape_147 = new cjs.Shape();
        this.shape_147.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgcg5QgJgRgDgWIgCgSIAAptQAAhHA4gcQAdgOAcgBIWfAAQBHABAcA4QAOAdAAAcIAAJtQAABHg4AcQgSAJgWADIgRADg");
        this.shape_147.setTransform(428.9, 70.5);

        this.shape_148 = new cjs.Shape();
        this.shape_148.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAOQg4AdAABHIAAJtIACASQADAVAJASQAcA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgOgcQgcg5hHAAg");
        this.shape_148.setTransform(428.9, 255.7);

        this.shape_149 = new cjs.Shape();
        this.shape_149.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgcg5QgJgSgDgVIgCgSIAAptQAAhHA4gdQAdgOAcAAIWfAAQBHAAAcA5QAOAcAAAdIAAJtQAABHg4AdQgSAIgWAEIgRACg");
        this.shape_149.setTransform(428.9, 255.7);

        this.shape_150 = new cjs.Shape();
        this.shape_150.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAOQg4AdAABHIAAJtIACASQADAVAJASQAcA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgOgcQgcg5hHAAg");
        this.shape_150.setTransform(428.9, 163.5);

        this.shape_151 = new cjs.Shape();
        this.shape_151.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgcg5QgJgSgDgVIgCgSIAAptQAAhHA4gdQAdgOAcAAIWfAAQBHAAAcA5QAOAcAAAdIAAJtQAABHg4AdQgSAIgWAEIgRACg");
        this.shape_151.setTransform(428.9, 163.5);

        this.shape_152 = new cjs.Shape();
        this.shape_152.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAPQg4AcAABHIAAJtIABASQAEAWAIARQAdA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgNgcQgdg5hHAAg");
        this.shape_152.setTransform(85.4, 70.5);

        this.shape_153 = new cjs.Shape();
        this.shape_153.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgcg5QgJgRgEgWIgCgSIAAptQAAhHA5gcQAdgOAcgBIWfAAQBHABAdA4QANAdAAAcIAAJtQAABHg4AcQgSAJgVADIgSADg");
        this.shape_153.setTransform(85.4, 70.5);

        this.shape_154 = new cjs.Shape();
        this.shape_154.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAOQg4AdAABHIAAJtIABASQAEAVAIASQAdA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgNgcQgdg5hHAAg");
        this.shape_154.setTransform(85.4, 255.7);

        this.shape_155 = new cjs.Shape();
        this.shape_155.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgcg5QgJgSgEgVIgCgSIAAptQAAhHA5gdQAdgOAcAAIWfAAQBHAAAdA5QANAcAAAdIAAJtQAABHg4AdQgSAIgVAEIgSACg");
        this.shape_155.setTransform(85.4, 255.7);

        this.shape_156 = new cjs.Shape();
        this.shape_156.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmoQgcAAgdAOQg4AdAABHIAAJtIABASQAEAVAIASQAdA5BHAAIWfAAIARgCQAWgEASgIQA4gdAAhHIAAptQAAgdgNgcQgdg5hHAAg");
        this.shape_156.setTransform(85.4, 163.5);

        this.shape_157 = new cjs.Shape();
        this.shape_157.graphics.f("#FFFFFF").s().p("ArPGpQhHAAgcg5QgJgSgEgVIgCgSIAAptQAAhHA5gdQAdgOAcAAIWfAAQBHAAAdA5QANAcAAAdIAAJtQAABHg4AdQgSAIgVAEIgSACg");
        this.shape_157.setTransform(85.4, 163.5);

        this.text_3 = new cjs.Text("Kuin-ka mon-ta pal-lo-a? Ver-taa ja mer-kit-se < tai > tai = .", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(19, 0.6);

        this.text_4 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#00A5C0");
        this.text_4.lineHeight = 19;

        this.addChild(this.text_4, this.text_3, this.shape_157, this.shape_156, this.shape_155, this.shape_154, this.shape_153, this.shape_152, this.shape_151, this.shape_150, this.shape_149, this.shape_148, this.shape_147, this.shape_146, this.shape_145, this.shape_144, this.shape_143, this.shape_142, this.shape_141, this.shape_140, this.shape_139, this.shape_138, this.shape_137, this.shape_136, this.shape_135, this.shape_134, this.shape_133, this.shape_132, this.shape_131, this.shape_130, this.shape_129, this.shape_128, this.shape_127, this.shape_126, this.shape_125, this.shape_124, this.shape_123, this.shape_122, this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_2, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.ra6, this.ra7, this.ra8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 307.7);


    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.Symbol12();
        this.instance.setTransform(424.4, 87.8, 1, 1, 0, 0, 0, 72.2, 23.1);

        this.instance_1 = new lib.Symbol11();
        this.instance_1.setTransform(241.5, 77.3, 1, 1, 0, 0, 0, 62.4, 12.6);

        this.text = new cjs.Text("kuin yk-si.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(6.1, 86.2);

        this.text_1 = new cjs.Text("pie-nem-pi", "bold 16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(80.6, 64.7);

        this.text_2 = new cjs.Text("Nol-la on ", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(6.1, 65.2);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape.setTransform(384, 29.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_1.setTransform(384, 29.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQgmAAgbAcQgcAbAAAmQAAAmAcAcQAbAcAmAAg");
        this.shape_2.setTransform(409.8, 29.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAbgcAmAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQgmAAgbgcg");
        this.shape_3.setTransform(409.8, 29.5);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape_4.setTransform(455.1, 29.5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#C51930").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_5.setTransform(455.1, 29.5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQgmAAgbAcQgcAbAAAmQAAAmAcAcQAbAcAmAAg");
        this.shape_6.setTransform(278.7, 29.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#C51930").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAbgcAmAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQgmAAgbgcg");
        this.shape_7.setTransform(278.7, 29.5);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape_8.setTransform(226.2, 29.5);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_9.setTransform(226.2, 29.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape_10.setTransform(100.1, 29.5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#C51930").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_11.setTransform(100.1, 29.5);

        this.text_3 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(449.9, 39.6);

        this.text_4 = new cjs.Text(">", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(420.8, 39.6);

        this.text_5 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(390.6, 39.6);

        this.text_6 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(273.1, 39.6);

        this.text_7 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(246.4, 39.6);

        this.text_8 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(220.5, 39.6);

        this.text_9 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(93.4, 39.6);

        this.text_10 = new cjs.Text("<", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(69.1, 39.6);

        this.text_11 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(42.2, 39.6);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_12.setTransform(322.8, 4.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_13.setTransform(306.7, 4.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_14.setTransform(291, 4.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_15.setTransform(275.6, 4.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_16.setTransform(260, 4.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_17.setTransform(243.9, 4.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_18.setTransform(228.4, 4.8);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_19.setTransform(212.9, 4.8);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_20.setTransform(197.4, 4.8);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_21.setTransform(181.9, 4.8);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_22.setTransform(245.2, 11.3);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_23.setTransform(245.2, 11.3);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_24.setTransform(324.2, 11.3);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAJgIAJAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_25.setTransform(324.2, 11.3);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_26.setTransform(308.1, 11.3);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_27.setTransform(308.1, 11.3);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_28.setTransform(230.1, 11.3);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_29.setTransform(230.1, 11.3);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_30.setTransform(292.4, 11.3);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_31.setTransform(292.4, 11.3);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_32.setTransform(214.4, 11.3);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_33.setTransform(214.4, 11.3);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_34.setTransform(276.9, 11.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_35.setTransform(276.9, 11.3);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_36.setTransform(198.9, 11.3);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_37.setTransform(198.9, 11.3);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_38.setTransform(261.2, 11.3);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAJgIAJAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_39.setTransform(261.2, 11.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_40.setTransform(183.2, 11.3);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_41.setTransform(183.2, 11.3);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArxoeQgOAAgOAHQgdAOAAAjIAAPNIAHAdQAPAcAjAAIXjAAIAcgHQAcgOAAgkIAAvNQAAgOgHgOQgNgcgkAAg");
        this.shape_42.setTransform(254, 57.9);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFFFFF").s().p("ArxIgQgjAAgOgcIgIgdIAAvNQABgjAcgOQAOgIAOABIXjAAQAjgBAPAdQAGAOAAAOIAAPNQAAAkgcAOIgcAHg");
        this.shape_43.setTransform(254, 57.9);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(497.2, 4.9);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(481.1, 4.9);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_46.setTransform(465.5, 4.9);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_47.setTransform(450, 4.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_48.setTransform(434.4, 4.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_49.setTransform(418.3, 4.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_50.setTransform(402.8, 4.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_51.setTransform(387.3, 4.8);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_52.setTransform(371.8, 4.8);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_53.setTransform(356.4, 4.8);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgJAAgJAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_54.setTransform(419.6, 11.3);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAJgIAJAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_55.setTransform(419.6, 11.3);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_56.setTransform(498.6, 11.3);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_57.setTransform(498.6, 11.3);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_58.setTransform(482.5, 11.3);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_59.setTransform(482.5, 11.3);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_60.setTransform(404.5, 11.3);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_61.setTransform(404.5, 11.3);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_62.setTransform(466.8, 11.3);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_63.setTransform(466.8, 11.3);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_64.setTransform(388.8, 11.3);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_65.setTransform(388.8, 11.3);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgJAAgJAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_66.setTransform(451.3, 11.3);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_67.setTransform(451.3, 11.3);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_68.setTransform(373.3, 11.3);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_69.setTransform(373.3, 11.3);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_70.setTransform(435.6, 11.3);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_71.setTransform(435.6, 11.3);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgJAAgJAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_72.setTransform(357.6, 11.3);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAJgIAJAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_73.setTransform(357.6, 11.3);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArxoeQgOAAgOAHQgdAOAAAjIAAPNIAHAdQAOAcAkAAIXiAAIAdgHQAcgOAAgkIAAvNQAAgOgHgOQgOgcgkAAg");
        this.shape_74.setTransform(428.4, 57.9);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFFFFF").s().p("ArxIgQgjAAgPgcIgHgdIAAvNQAAgjAdgOQAOgIAOABIXiAAQAkgBAPAdQAGAOABAOIAAPNQAAAkgdAOIgdAHg");
        this.shape_75.setTransform(428.4, 57.9);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(149.9, 4.9);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(133.8, 4.9);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(118.2, 4.9);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(102.7, 4.9);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_80.setTransform(87.1, 4.9);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_81.setTransform(71, 4.9);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_82.setTransform(55.5, 4.8);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_83.setTransform(40, 4.8);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_84.setTransform(24.5, 4.8);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_85.setTransform(9.1, 4.8);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgJAAgJAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_86.setTransform(72.3, 11.3);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAJgIAJAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_87.setTransform(72.3, 11.3);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(151.3, 11.3);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(151.3, 11.3);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_90.setTransform(135.2, 11.3);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAJgIAJAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_91.setTransform(135.2, 11.3);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(57.2, 11.3);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(57.2, 11.3);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_94.setTransform(119.5, 11.3);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_95.setTransform(119.5, 11.3);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(41.5, 11.3);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(41.5, 11.3);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_98.setTransform(104, 11.3);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_99.setTransform(104, 11.3);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_100.setTransform(26, 11.3);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_101.setTransform(26, 11.3);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_102.setTransform(88.3, 11.3);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_103.setTransform(88.3, 11.3);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgJAAgJAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_104.setTransform(10.3, 11.3);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFFFFF").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_105.setTransform(10.3, 11.3);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArxoeQgOAAgOAHQgdAOAAAjIAAPNIAIAdQANAcAkAAIXiAAIAdgHQAdgOAAgkIAAvNQAAgOgIgOQgOgcgkAAg");
        this.shape_106.setTransform(81.1, 57.9);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFFFFF").s().p("ArxIgQgjAAgPgcIgGgdIAAvNQAAgjAcgOQAOgIAOABIXiAAQAkgBAOAdQAIAOAAAOIAAPNQgBAkgcAOIgdAHg");
        this.shape_107.setTransform(81.1, 57.9);

        this.addChild(this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_2, this.text_1, this.text, this.instance_1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.5, 114.3);


    (lib.Symbol3 = function() {
        this.initialize();



        // Layer 1
        this.text = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 44;
        this.text.setTransform(314, 29.8);

        this.text_1 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 44;
        this.text_1.setTransform(95, 29.8);

        this.text_3 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 44;
        this.text_3.setTransform(174.4, 29.8);

        this.text_4 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 44;
        this.text_4.setTransform(56, 29.8);

        this.text_5 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 44;
        this.text_5.setTransform(334, 29.8);

        this.text_6 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 44;
        this.text_6.setTransform(75.7, 29.8);

        this.text_7 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 44;
        this.text_7.setTransform(16.1, 29.8);

        this.text_8 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 44;
        this.text_8.setTransform(413.7, 29.8);

        this.text_9 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 44;
        this.text_9.setTransform(154.1, 29.8);

        this.text_10 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 44;
        this.text_10.setTransform(36.7, 29.8);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape.setTransform(462.8, 62.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_1.setTransform(472.6, 51.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_2.setTransform(452.8, 51.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_3.setTransform(462.8, 39.8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_4.setTransform(442.9, 62.5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_5.setTransform(433, 51.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_6.setTransform(442.9, 39.8);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_7.setTransform(304, 62.5);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_8.setTransform(313.9, 51.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_9.setTransform(294.1, 51.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_10.setTransform(304, 39.8);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_11.setTransform(224.7, 62.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_12.setTransform(204.8, 62.5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_13.setTransform(234.5, 51.1);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_14.setTransform(214.7, 51.1);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_15.setTransform(224.7, 39.8);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_16.setTransform(194.9, 51.1);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_17.setTransform(204.8, 39.8);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_18.setTransform(105.7, 62.5);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_19.setTransform(85.7, 62.5);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_20.setTransform(115.5, 51.1);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_21.setTransform(95.6, 51.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_22.setTransform(105.7, 39.8);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_23.setTransform(75.8, 51.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_24.setTransform(85.7, 39.8);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_25.setTransform(423, 62.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_26.setTransform(403.2, 62.5);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_27.setTransform(383.2, 62.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_28.setTransform(413.1, 51.1);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_29.setTransform(423, 39.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_30.setTransform(393.3, 51.1);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_31.setTransform(403.2, 39.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_32.setTransform(373.4, 51.1);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_33.setTransform(383.2, 39.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_34.setTransform(343.8, 62.5);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_35.setTransform(323.8, 62.5);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_36.setTransform(353.6, 51.1);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_37.setTransform(333.8, 51.1);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_38.setTransform(343.8, 39.8);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_39.setTransform(323.8, 39.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_40.setTransform(284.1, 62.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_41.setTransform(264.2, 62.5);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_42.setTransform(274.2, 51.1);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_43.setTransform(284.1, 39.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_44.setTransform(254.4, 51.1);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_45.setTransform(264.2, 39.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_46.setTransform(184.9, 62.5);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_47.setTransform(165.1, 62.5);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_48.setTransform(145.1, 62.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_49.setTransform(175, 51.1);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_50.setTransform(184.9, 39.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_51.setTransform(155.2, 51.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_52.setTransform(165.1, 39.8);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_53.setTransform(135.3, 51.1);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_54.setTransform(145.1, 39.8);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_55.setTransform(65.9, 62.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_56.setTransform(46, 62.5);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_57.setTransform(26.1, 62.5);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_58.setTransform(56, 51.1);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_59.setTransform(65.9, 39.8);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_60.setTransform(36.1, 51.1);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_61.setTransform(46, 39.8);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_62.setTransform(16.3, 51.1);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_63.setTransform(26.1, 39.8);

        this.text_11 = new cjs.Text("Skriv talen som saknas.", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(19, 5.2);

        this.text_12 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#00A5C0");
        this.text_12.lineHeight = 25;
        this.text_12.setTransform(0, 5);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgDcQgHAAgLABQgVAEgSAIQg5AdAABHIAADXIACASQAEAVAIASQAdA4BHAAMBMJAAAIASgBQAVgEASgIQA5gdAAhHIAAjXQAAgcgOgdQgdg4hHAAg");
        this.shape_64.setTransform(256.1, 49.2);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("EgmEADeQhHAAgdg5QgIgSgEgVIgCgSIAAjXQAAhHA5gcQASgJAVgEQALgCAHAAMBMJAAAQBHAAAdA6QAOAcAAAcIAADXQAABHg5AcQgSAJgVADIgSADg");
        this.shape_65.setTransform(256.1, 49.2);

        this.addChild(this.shape_65, this.shape_64, this.text_12, this.text_11, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.ra6, this.ra7, this.ra8, this.ra9);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75.9);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 2

        // Layer 1
        this.text = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text.lineHeight = 56;
        this.text.setTransform(214.8, 2.8);

        this.text_1 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_1.lineHeight = 56;
        this.text_1.setTransform(426.7, 2.8);

        this.text_2 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_2.lineHeight = 56;
        this.text_2.setTransform(177.2, 2.8);

        this.text_3 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_3.lineHeight = 56;
        this.text_3.setTransform(138.3, 2.8);

        this.text_4 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_4.lineHeight = 56;
        this.text_4.setTransform(350.2, 2.8);

        this.text_5 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_5.lineHeight = 56;
        this.text_5.setTransform(100.7, 2.8);

        this.text_6 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_6.lineHeight = 56;
        this.text_6.setTransform(62.4, 2.8);

        this.text_7 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_7.lineHeight = 46;
        this.text_7.setTransform(440.5, 103);

        this.text_8 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_8.lineHeight = 46;
        this.text_8.setTransform(440.5, 72.8);

        this.text_9 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_9.lineHeight = 46;
        this.text_9.setTransform(440.5, 42.1);

        this.text_10 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_10.lineHeight = 46;
        this.text_10.setTransform(192, 103);

        this.text_11 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_11.lineHeight = 46;
        this.text_11.setTransform(192, 72.8);

        this.text_12 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_12.lineHeight = 46;
        this.text_12.setTransform(192, 42.1);

        this.text_13 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_13.lineHeight = 46;
        this.text_13.setTransform(383.9, 103);

        this.text_14 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_14.lineHeight = 46;
        this.text_14.setTransform(383.9, 72.8);

        this.text_15 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_15.lineHeight = 46;
        this.text_15.setTransform(383.9, 42.1);

        this.text_16 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_16.lineHeight = 46;
        this.text_16.setTransform(135.3, 103);

        this.text_17 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_17.lineHeight = 46;
        this.text_17.setTransform(135.3, 72.8);

        this.text_18 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_18.lineHeight = 46;
        this.text_18.setTransform(135.3, 42.1);

        this.text_19 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_19.lineHeight = 46;
        this.text_19.setTransform(326.8, 103);

        this.text_20 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_20.lineHeight = 46;
        this.text_20.setTransform(326.8, 72.8);

        this.text_21 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_21.lineHeight = 46;
        this.text_21.setTransform(326.8, 42.1);

        this.text_22 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_22.lineHeight = 46;
        this.text_22.setTransform(78.3, 103);

        this.text_23 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_23.lineHeight = 46;
        this.text_23.setTransform(78.3, 72.8);

        this.text_24 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_24.lineHeight = 46;
        this.text_24.setTransform(78.3, 42.1);

        this.text_25 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_25.lineHeight = 46;
        this.text_25.setTransform(270.5, 103);

        this.text_26 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_26.lineHeight = 46;
        this.text_26.setTransform(270.5, 72.8);

        this.text_27 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_27.lineHeight = 46;
        this.text_27.setTransform(270.5, 42.1);

        this.text_28 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_28.lineHeight = 46;
        this.text_28.setTransform(22, 103);

        this.text_29 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_29.lineHeight = 46;
        this.text_29.setTransform(22, 72.8);

        this.text_30 = new cjs.Text("0", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_30.lineHeight = 46;
        this.text_30.setTransform(22, 42.1);

        this.text_31 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_31.lineHeight = 56;
        this.text_31.setTransform(273.6, 2.8);

        this.text_32 = new cjs.Text("0", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_32.lineHeight = 56;
        this.text_32.setTransform(24.2, 2.8);

        this.text_33 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_33.lineHeight = 12;
        this.text_33.setTransform(474.4, 109.3);

        this.text_34 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_34.lineHeight = 12;
        this.text_34.setTransform(474.4, 78.6);

        this.text_35 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_35.lineHeight = 12;
        this.text_35.setTransform(474.4, 48.4);

        this.text_36 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_36.lineHeight = 12;
        this.text_36.setTransform(225.5, 109.6);

        this.text_37 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_37.lineHeight = 12;
        this.text_37.setTransform(225.5, 78.6);

        this.text_38 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_38.lineHeight = 12;
        this.text_38.setTransform(225.5, 48.4);

        this.text_39 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_39.lineHeight = 12;
        this.text_39.setTransform(418.2, 109.6);

        this.text_40 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_40.lineHeight = 12;
        this.text_40.setTransform(418.2, 78.6);

        this.text_41 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_41.lineHeight = 12;
        this.text_41.setTransform(418.2, 48.4);

        this.text_42 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_42.lineHeight = 12;
        this.text_42.setTransform(169.3, 109.6);

        this.text_43 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_43.lineHeight = 12;
        this.text_43.setTransform(169.3, 78.6);

        this.text_44 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_44.lineHeight = 12;
        this.text_44.setTransform(361, 109.6);

        this.text_45 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_45.lineHeight = 12;
        this.text_45.setTransform(361, 78.6);

        this.text_46 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_46.lineHeight = 12;
        this.text_46.setTransform(361, 48.4);

        this.text_47 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_47.lineHeight = 12;
        this.text_47.setTransform(112.1, 109.6);

        this.text_48 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_48.lineHeight = 12;
        this.text_48.setTransform(112.1, 78.6);

        this.text_49 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_49.lineHeight = 12;
        this.text_49.setTransform(169.1, 48.4);

        this.text_50 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_50.lineHeight = 12;
        this.text_50.setTransform(55.4, 48.4);

        this.text_51 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_51.lineHeight = 12;
        this.text_51.setTransform(112.1, 48.4);

        this.text_52 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_52.lineHeight = 12;
        this.text_52.setTransform(304.8, 109.6);

        this.text_53 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_53.lineHeight = 12;
        this.text_53.setTransform(304.8, 78.6);

        this.text_54 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_54.lineHeight = 12;
        this.text_54.setTransform(304.8, 48.4);

        this.text_55 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_55.lineHeight = 12;
        this.text_55.setTransform(55.9, 109.6);

        this.text_56 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_56.lineHeight = 12;
        this.text_56.setTransform(55.9, 78.6);

        this.text_57 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_57.lineHeight = 12;
        this.text_57.setTransform(469.2, 12.3);

        this.text_58 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_58.lineHeight = 12;
        this.text_58.setTransform(392.1, 12.3);

        this.text_59 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_59.lineHeight = 12;
        this.text_59.setTransform(317.1, 12.3);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape.setTransform(474.9, 29.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_1.setTransform(474.9, 29.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_2.setTransform(226.2, 29.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_3.setTransform(226.2, 29.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_4.setTransform(436.8, 29.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_5.setTransform(436.8, 29.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_6.setTransform(188.2, 29.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_7.setTransform(188.2, 29.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_8.setTransform(397.8, 29.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_9.setTransform(397.8, 29.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_10.setTransform(149.2, 29.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_11.setTransform(149.2, 29.9);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_12.setTransform(360.8, 29.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_13.setTransform(360.8, 29.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiJCLIEUAAIAAkVIkUAAg");
        this.shape_14.setTransform(112.2, 29.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEUAAIAAEVg");
        this.shape_15.setTransform(112.2, 29.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_16.setTransform(322.8, 29.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_17.setTransform(322.8, 29.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_18.setTransform(74.2, 29.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_19.setTransform(74.2, 29.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_20.setTransform(479, 124.5);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_21.setTransform(479, 124.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_22.setTransform(230.3, 124.5);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_23.setTransform(230.3, 124.5);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_24.setTransform(450.6, 124.5);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_25.setTransform(450.6, 124.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_26.setTransform(202, 124.5);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_27.setTransform(202, 124.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_28.setTransform(422.3, 124.5);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_29.setTransform(422.3, 124.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_30.setTransform(173.7, 124.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_31.setTransform(173.7, 124.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_32.setTransform(393.9, 124.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_33.setTransform(393.9, 124.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_34.setTransform(145.3, 124.5);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_35.setTransform(145.3, 124.5);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_36.setTransform(365.6, 124.5);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_37.setTransform(365.6, 124.5);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_38.setTransform(117, 124.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_39.setTransform(117, 124.5);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_40.setTransform(337.2, 124.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_41.setTransform(337.2, 124.5);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_42.setTransform(88.6, 124.5);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_43.setTransform(88.6, 124.5);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_44.setTransform(308.9, 124.5);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_45.setTransform(308.9, 124.5);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_46.setTransform(60.3, 124.5);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_47.setTransform(60.3, 124.5);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_48.setTransform(280.5, 124.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_49.setTransform(280.5, 124.5);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_50.setTransform(31.9, 124.5);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_51.setTransform(31.9, 124.5);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_52.setTransform(479, 93.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_53.setTransform(479, 93.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_54.setTransform(230.3, 93.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_55.setTransform(230.3, 93.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_56.setTransform(450.6, 93.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_57.setTransform(450.6, 93.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_58.setTransform(202, 93.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_59.setTransform(202, 93.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_60.setTransform(422.3, 93.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_61.setTransform(422.3, 93.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_62.setTransform(173.7, 93.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_63.setTransform(173.7, 93.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_64.setTransform(393.9, 93.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_65.setTransform(393.9, 93.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_66.setTransform(145.3, 93.9);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_67.setTransform(145.3, 93.9);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_68.setTransform(365.6, 93.9);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_69.setTransform(365.6, 93.9);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_70.setTransform(117, 93.9);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_71.setTransform(117, 93.9);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_72.setTransform(337.2, 93.9);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_73.setTransform(337.2, 93.9);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_74.setTransform(88.6, 93.9);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_75.setTransform(88.6, 93.9);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_76.setTransform(308.9, 93.9);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_77.setTransform(308.9, 93.9);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_78.setTransform(60.3, 93.9);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_79.setTransform(60.3, 93.9);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_80.setTransform(280.5, 93.9);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_81.setTransform(280.5, 93.9);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_82.setTransform(31.9, 93.9);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_83.setTransform(31.9, 93.9);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_84.setTransform(479, 63.3);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_85.setTransform(479, 63.3);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_86.setTransform(230.3, 63.3);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_87.setTransform(230.3, 63.3);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_88.setTransform(450.6, 63.3);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_89.setTransform(450.6, 63.3);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_90.setTransform(202, 63.3);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_91.setTransform(202, 63.3);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_92.setTransform(422.3, 63.3);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_93.setTransform(422.3, 63.3);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_94.setTransform(173.7, 63.3);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_95.setTransform(173.7, 63.3);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_96.setTransform(393.9, 63.3);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_97.setTransform(393.9, 63.3);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_98.setTransform(145.3, 63.3);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_99.setTransform(145.3, 63.3);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_100.setTransform(365.6, 63.3);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_101.setTransform(365.6, 63.3);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_102.setTransform(117, 63.3);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_103.setTransform(117, 63.3);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_104.setTransform(337.2, 63.3);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_105.setTransform(337.2, 63.3);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_106.setTransform(88.6, 63.3);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_107.setTransform(88.6, 63.3);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_108.setTransform(308.9, 63.3);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_109.setTransform(308.9, 63.3);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_110.setTransform(60.3, 63.3);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_111.setTransform(60.3, 63.3);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_112.setTransform(280.5, 63.3);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_113.setTransform(280.5, 63.3);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_114.setTransform(31.9, 63.3);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
        this.shape_115.setTransform(31.9, 63.3);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_116.setTransform(284.8, 29.9);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_117.setTransform(284.8, 29.9);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_118.setTransform(36.2, 29.9);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_119.setTransform(36.2, 29.9);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgLMQgHAAgLACQgVADgSAJQg5AcAABHIAAS4IACARQAEAWAIARQAdA5BHAAMBMJAAAIASgCQAVgEASgIQA5gdAAhGIAAy4QAAgcgOgcQgdg5hHAAg");
        this.shape_120.setTransform(255.1, 76.7);

        this.addChild(this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_59, this.text_58, this.text_57, this.text_56, this.text_55, this.text_54, this.text_53, this.text_52, this.text_51, this.text_50, this.text_49, this.text_48, this.text_47, this.text_46, this.text_45, this.text_44, this.text_43, this.text_42, this.text_41, this.text_40, this.text_39, this.text_38, this.text_37, this.text_36, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.ra1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 512.3, 149.5);


    // stage content:
    (lib.p26 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(296.3, 354.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(297.6, 234.4, 1, 1, 0, 0, 0, 255.1, 74.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297.9, 99.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
