(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p18_1.png",
            id: "p18_1"
        }, {
            src: "images/p18_2.png",
            id: "p18_2"
        }, {
            src: "images/p18_3.png",
            id: "p18_3"
        }, {
            src: "images/p18_4.png",
            id: "p18_4"
        }]
    };

    (lib.p18_1 = function() {
        this.initialize(img.p18_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p18_2 = function() {
        this.initialize(img.p18_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p18_3 = function() {
        this.initialize(img.p18_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p18_4 = function() {
        this.initialize(img.p18_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("I klassrummet", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(188, 47.6);

        this.text_1 = new cjs.Text("5", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(54, 21);

        this.pageBottomText = new cjs.Text("kunna räkna antal till 10", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(85.5, 651.2);

        this.text_4 = new cjs.Text("18", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(38.5, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 115, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.pageBottomText, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Räkna saker i klassrummet. Visa antalet med tal eller streck.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(57, 7.6);

        this.instance = new lib.p18_1();
        this.instance.setTransform(3, -11, 0.64, 0.64);

        this.text_2 = new cjs.Text("Sak", "16px 'UusiTekstausMajema'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(10, 51);

        this.text_3 = new cjs.Text("Antal", "16px 'UusiTekstausMajema'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(107, 101);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 70, 98, 110, 5);
        this.round_Rect1.setTransform(0, 0);
        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(94, 119, 142, 55, 5);
        this.round_Rect2.setTransform(0, 0);

        this.round_Rect3 = this.round_Rect1.clone(true);
        this.round_Rect3.setTransform(270, 0);
        this.round_Rect4 = this.round_Rect2.clone(true);
        this.round_Rect4.setTransform(270, 0);

        this.instance_1 = new lib.p18_2();
        this.instance_1.setTransform(19, 78, 0.42, 0.42);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(108, 165).lineTo(224, 165).moveTo(378, 165).lineTo(494, 165);
        this.line1.setTransform(0, 0);

        this.addChild(this.text_1, this.instance, this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4,
            this.instance_1, this.line1, this.text_2, this.text_3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 220);

    (lib.Symbol2 = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 30, 98, 110, 5);
        this.round_Rect1.setTransform(0, 0);
        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(94, 79, 142, 55, 5);
        this.round_Rect2.setTransform(0, 0);

        this.round_Rect3 = this.round_Rect1.clone(true);
        this.round_Rect3.setTransform(270, 0);
        this.round_Rect4 = this.round_Rect2.clone(true);
        this.round_Rect4.setTransform(270, 0);

        this.instance_1 = new lib.p18_3();
        this.instance_1.setTransform(15, 36, 0.42, 0.41);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(108, 125).lineTo(224, 125).moveTo(378, 125).lineTo(494, 125);
        this.line1.setTransform(0, 0);

        this.addChild(this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4,
            this.instance_1, this.line1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 220);

    (lib.Symbol3 = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 30, 98, 110, 5);
        this.round_Rect1.setTransform(0, 0);
        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(94, 79, 142, 55, 5);
        this.round_Rect2.setTransform(0, 0);

        this.round_Rect3 = this.round_Rect1.clone(true);
        this.round_Rect3.setTransform(270, 0);
        this.round_Rect4 = this.round_Rect2.clone(true);
        this.round_Rect4.setTransform(270, 0);

        this.instance_1 = new lib.p18_4();
        this.instance_1.setTransform(15, 53, 0.42, 0.42);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(108, 125).lineTo(224, 125).moveTo(378, 125).lineTo(494, 125);
        this.line1.setTransform(0, 0);

        this.addChild(this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4,
            this.instance_1, this.line1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 220);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(302, 270, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(302, 468, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(302, 627, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
