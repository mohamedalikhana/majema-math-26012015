var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c9_ex1_1.png",
            id: "c23_ex1_1"
        }, {
            src: "images/c23_ex1_2.png",
            id: "c23_ex1_2"
        }, {
            src: "images/c23_ex1_3.png",
            id: "c23_ex1_3"
        }]
    };

    (lib.c23_ex1_1 = function() {
        this.initialize(img.c23_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.c23_ex1_2 = function() {
        this.initialize(img.c23_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);
    (lib.c23_ex1_3 = function() {
        this.initialize(img.c23_ex1_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 51, 42);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Testa dina kunskaper", "bold 24px 'Myriad Pro'", "#87C140");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("23", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#87C140").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1_1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(-2, 49, 521, 525, 13);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Kycklingspelet", "bold 40px 'MyriadPro-Semibold'", "#87C140");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text("Spel för 2 eller fler.", "35px 'Myriad Pro'", "#87C140");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(0, 45);

        this.text_2 = new cjs.Text("1.", "32px 'MyriadPro-Semibold'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(0, 140);
        this.text_3 = new cjs.Text("Slå tärningen.", "32px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(35, 140);

        this.text_4 = new cjs.Text("2.", "32px 'MyriadPro-Semibold'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(0, 190);
        this.text_5 = new cjs.Text("Subtrahera tärningstalet från 6", "32px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(35, 190);

        this.text_6 = new cjs.Text("och gå lika många steg som svaret.", "32px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(35, 240);

        this.text_8 = new cjs.Text("3.", "32px 'MyriadPro-Semibold'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(0, 290);
        this.text_9 = new cjs.Text("Om du landar på ", "32px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(35, 290);
        this.dice = new lib.c23_ex1_3();
        this.dice.setTransform(270, 287, 0.75, 0.75)
        this.text_10 = new cjs.Text("får du slå en extra gång.", "32px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(315, 290);

        this.text_11 = new cjs.Text("4.", "32px 'MyriadPro-Semibold'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(0, 350);
        this.text_12 = new cjs.Text("Först i mål vinner.", "32px 'Myriad Pro'");
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(35, 350);

        this.addChild(this.text_1, this.text, this.text_2, this.text_3, this.dice, this.text_4, this.text_5, this.text_6, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1_2 = function() {
        this.initialize();

        this.instance = new lib.c23_ex1_1();
        this.instance.setTransform(429, 18, 0.31, 0.31);

        this.instance_2 = new lib.c23_ex1_2();
        this.instance_2.setTransform(0, 56, 0.53, 0.525);

        this.text_7 = new cjs.Text("Ni behöver", "13.67px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(347, 29);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#00B0CA').drawRoundRect(334, 17, 168, 32.5, 2);
        this.roundRect2.setTransform(0, 0);

        this.label1 = new cjs.Text("Mål", "18px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(130, 78);
        this.label2 = new cjs.Text("Start", "18px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(330, 78);
        this.addChild(this.roundRect2, this.instance_2);
        this.addChild(this.label1, this.label2);
        this.addChild(this.instance, this.text_7);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage1_1();
        stage1_1.setTransform(0, 0);
        var stage1_2 = new lib.Stage1_2();
        stage1_2.setTransform(1228 / 2 + 100, -120, 1.17, 1.17);
        // this.roundRect1 = new cjs.Shape();
        // this.roundRect1.graphics.f("#ffffff").s('#00B0CA').drawRoundRect(0, -70, 1228 / 2, 625, 0);
        // this.roundRect1.setTransform(0, 0);
        // this.roundRect2 = new cjs.Shape();
        // this.roundRect2.graphics.f("#ffffff").s('#00B0CA').drawRoundRect(0, -70, 1228 / 2, 625, 0);
        // this.roundRect2.setTransform(1228 / 2, 0);
        this.addChild(stage1_1, stage1_2)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
