(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p48_1.png",
            id: "p48_1"
        }, {
            src: "images/p48_2.png",
            id: "p48_2"
        }]
    };

    (lib.p48_1 = function() {
        this.initialize(img.p48_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p48_2 = function() {
        this.initialize(img.p48_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("kunna läsa av ett stapeldiagram", "9px 'Myriad Pro'");
        this.text_1.setTransform(70, 660);

        this.text_2 = new cjs.Text("48", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(37.5, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31, 660.8);

        this.text_3 = new cjs.Text("Stapeldiagram", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.setTransform(95.5, 43);

        this.text_4 = new cjs.Text("16", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(48, 43);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 8, 527, 320, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p48_1();
        this.instance.setTransform(20, 44, 0.48, 0.52);

        this.Line_bg1 = new cjs.Shape();
        this.Line_bg1.graphics.f("#ffffff").beginStroke("#7d7d7d").setStrokeStyle(0.7).moveTo(45, 59).lineTo(45, 217).lineTo(335, 217).lineTo(335, 59).lineTo(45, 59);
        this.Line_bg1.setTransform(0, 0);

        this.text_y1 = new cjs.Text("Antal", "12px 'Myriad Pro'");
        this.text_y1.setTransform(9, 48);

        this.text_h1 = new cjs.Text("Pappas foton av djur", "15px 'MyriadPro-Semibold'");
        this.text_h1.setTransform(42, 32);

        this.text_1 = new cjs.Text("varg", "16px 'Myriad Pro'");
        this.text_1.setTransform(48, 285);
        this.text_2 = new cjs.Text("björn", "16px 'Myriad Pro'");
        this.text_2.setTransform(110, 285);
        this.text_3 = new cjs.Text("räv", "16px 'Myriad Pro'");
        this.text_3.setTransform(180, 285);
        this.text_4 = new cjs.Text("lodjur", "16px 'Myriad Pro'");
        this.text_4.setTransform(235, 285);
        this.text_5 = new cjs.Text("hare", "16px 'Myriad Pro'");
        this.text_5.setTransform(310, 285);

        var ToBeAdded = [];
        for (var num = 0; num < 11; num++) {
            var xPos = 29;
            var temptext = new cjs.Text("" + num, "12px 'Myriad Pro'");
            if (num == 10) {
                xPos = 22;
                num = 10.15;
            }
            temptext.setTransform(xPos, 219 + (-15.48 * num));
            ToBeAdded.push(temptext);
        }

        var tmpline = new cjs.Shape();
        var tmpcol = 64;

        for (var col = 0; col < 5; col++) {
            var columnSpace = col;
            for (var row = 0; row < 1; row++) {
                if (col == 4) {
                    tmpcol = 66
                } else {
                    tmpcol = 64
                }
                var rowSpace = row;
                tmpline.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(47 + (columnSpace * tmpcol), 310).lineTo(85 + (columnSpace * tmpcol), 310);
                ToBeAdded.push(tmpline);
            }
        }


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").f("#000000").setStrokeStyle(0.7).moveTo(45, 50).lineTo(45, 217).moveTo(37, 217).lineTo(352, 217)
            .moveTo(57, 59).lineTo(57, 217).moveTo(81, 59).lineTo(81, 217)
            .moveTo(120, 59).lineTo(120, 217).moveTo(144, 59).lineTo(144, 217)
            .moveTo(183, 59).lineTo(183, 217).moveTo(207, 59).lineTo(207, 217)
            .moveTo(246, 59).lineTo(246, 217).moveTo(270, 59).lineTo(270, 217)
            .moveTo(309, 59).lineTo(309, 217).moveTo(335, 59).lineTo(335, 217)
            .moveTo(352, 217).lineTo(351, 215).lineTo(357, 217).lineTo(351, 219).lineTo(352, 217)
            .moveTo(45, 50).lineTo(43, 51).lineTo(45, 45).lineTo(47, 51).lineTo(45, 50)

        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#949599").setStrokeStyle(0.7).moveTo(37, 201).lineTo(335, 201).moveTo(37, 186).lineTo(335, 186)
            .moveTo(37, 170).lineTo(335, 170).moveTo(37, 154).lineTo(335, 154).moveTo(37, 138).lineTo(335, 138).moveTo(37, 122).lineTo(335, 122)
            .moveTo(37, 106).lineTo(335, 106).moveTo(37, 90).lineTo(335, 90).moveTo(37, 74).lineTo(335, 74);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        166
        this.Line_3.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(37, 59).lineTo(340, 59).moveTo(37, 137.8).lineTo(340, 137.8);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7)
            .moveTo(80, 90).lineTo(80, 217).lineTo(57, 217).lineTo(57, 90)
            .moveTo(120, 74).lineTo(120, 217).lineTo(144, 217).lineTo(144, 74)
            .moveTo(184, 122).lineTo(184, 217).lineTo(207, 217).lineTo(207, 122)
            .moveTo(246, 154).lineTo(246, 217).lineTo(270, 217).lineTo(270, 154)
            .moveTo(309, 106).lineTo(309, 217).lineTo(335, 217).lineTo(335, 106)

        this.Line_4.setTransform(0, 0);
        144

        this.addChild(this.roundRect1, this.instance);
        this.addChild(this.Line_bg1, this.Line_4, this.Line_1, this.Line_2, this.Line_3)
        this.addChild(this.text_y1, this.text_h1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535.3, 300);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Hur många foton har pappa tagit tillsammans av djuren?", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 16);

        this.text_2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_2.setTransform(0, 16);


        this.roundRect = new cjs.Shape();
        this.roundRect.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 14, 260, 110, 10);
        this.roundRect.setTransform(0, 15);

        this.roundRect1 = this.roundRect.clone(true);
        this.roundRect1.setTransform(0, 130);

        this.roundRect2 = this.roundRect.clone(true);
        this.roundRect2.setTransform(266, 15);

        this.roundRect3 = this.roundRect.clone(true);
        this.roundRect3.setTransform(266, 130);

        this.instance = new lib.p48_2();
        this.instance.setTransform(25, 45, 0.47, 0.43);

        this.textbox_group1 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var row = 0; row < 2; row++) {

            for (var col = 0; col < 6; col++) {
                this.textbox_group1.graphics.s("#000000").ss(0.3).drawRect((67 + Xpos) + (col * 20), (108 + Ypos) + (row * 23), 20, 22);
            }
            Ypos = Ypos + 92;
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var row = 0; row < 2; row++) {

            for (var col = 0; col < 6; col++) {
                this.textbox_group2.graphics.s("#000000").ss(0.3).drawRect((330 + Xpos) + (col * 20), (108 + Ypos) + (row * 23), 20, 22);
            }
            Ypos = Ypos + 92;
        }
        this.textbox_group1.setTransform(0, 0);


        this.addChild(this.text_1, this.text_2, this.roundRect, this.roundRect1, this.roundRect2, this.roundRect3, this.instance, this.textbox_group1, this.textbox_group2);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 180.2);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(302.5, 100, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(302.5, 438, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
