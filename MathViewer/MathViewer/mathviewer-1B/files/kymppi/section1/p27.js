(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p27_BG.png",
            id: "p27_BG"
        }]
    };

    (lib.p27_BG = function() {
        this.initialize(img.p27_BG);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1

        this.pageTitle = new cjs.Text("Kluring", "18px 'MyriadPro'", "#8FBC21");
        this.pageTitle.setTransform(45, 65);

        this.text_1 = new cjs.Text("27", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(18, 10);

        this.addChild(this.pageTitle, this.shape, this.text, this.text_1, this.text_2, this.textbox_group1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.dotMatrixRect = function() {
        this.initialize();

        var width = 152,
            height = 242;
        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#90BD22").ss(0.8).drawRect(0, 0, width, height);
        this.rect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#90BD22").setStrokeStyle(0.5).moveTo(0, height / 2).lineTo(width, height / 2);
        this.hrLine_1.setTransform(0, 0);

        var ToBeAdded = [];
        var colSpace = 17.04;
        var rowSpace = 17.04;
        var startX = 7;
        var startY = 11;
        var dotCount = 0;
        for (var col = 0; col < 9; col++) {

            for (var row = 0; row < 14; row++) {

                var dot = null;
                dot = new cjs.Shape();
                dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 1.25);
                dot.setTransform(startX + (col * colSpace), startY + (row * rowSpace));
                dot.id = dotCount;
                dotCount = dotCount + 1;

                ToBeAdded.push(dot);
            }
        }

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);

        }
        this.dots = ToBeAdded;
        this.addChild(this.rect1, this.hrLine_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);



    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(6, 24, 520, 295, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text(" Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 48);

        this.text_1 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(10, 48);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(60, 65);

        this.rectimg_1 = new lib.dotMatrixRect();
        this.rectimg_1.setTransform(334, 65);

        // Left base level

        this.Line_1 = new cjs.Shape(); // blue
        this.Line_1.graphics.f('#008BD2').s("#9D9D9D").ss(1.2).moveTo(100, 179).lineTo(118, 179).lineTo(118, 144).lineTo(101, 144).lineTo(101, 179);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape(); // green
        this.Line_2.graphics.f('#13A538').s("#9D9D9D").ss(1.2).moveTo(118, 179).lineTo(153, 179).lineTo(153, 144).lineTo(118, 144);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape(); // blue
        this.Line_3.graphics.f('#008BD2').s("#9D9D9D").ss(1.2).moveTo(153, 179).lineTo(170, 179).lineTo(170, 144).lineTo(153, 144);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape(); // right yellow
        this.Line_4.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(171, 179).lineTo(203, 144).lineTo(171, 144);
        this.Line_4.setTransform(0, 0);

        this.Line_5 = new cjs.Shape(); // left yellow
        this.Line_5.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(100, 179).lineTo(67, 144).lineTo(100, 144);
        this.Line_5.setTransform(0, 0);

        // Left  1st level

        this.Line_6 = new cjs.Shape(); // left green
        this.Line_6.graphics.f('#13A538').s("#9D9D9D").ss(1.2).moveTo(101, 144).lineTo(101, 127).lineTo(118, 127).lineTo(118, 144);
        this.Line_6.setTransform(0, 0);

        this.Line_7 = new cjs.Shape(); // right green
        this.Line_7.graphics.f('#13A538').s("#9D9D9D").ss(1.2).moveTo(153, 144).lineTo(153, 127).lineTo(170, 127).lineTo(170, 144);
        this.Line_7.setTransform(0, 0);

        this.Line_8 = new cjs.Shape(); // blue square
        this.Line_8.graphics.f('#008BD2').s("#9D9D9D").ss(1.2).moveTo(118, 144).lineTo(118, 110).lineTo(153, 110).lineTo(153, 144);
        this.Line_8.setTransform(0, 0);

        this.Line_9 = new cjs.Shape(); // triangle
        this.Line_9.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(118, 144).lineTo(136, 110).lineTo(152, 144);
        this.Line_9.setTransform(0, 0);

        this.Line_10 = new cjs.Shape(); // top level triangle
        this.Line_10.graphics.f('#D51317').s("#9D9D9D").ss(1.2).moveTo(118, 110).lineTo(136, 75).lineTo(153, 110);
        this.Line_10.setTransform(0, 0);

        // Right  1st level

        this.Line_11 = new cjs.Shape(); // left green
        this.Line_11.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(375, 179).lineTo(375, 127).lineTo(357, 127).lineTo(340, 109).lineTo(375, 93).lineTo(393, 93)
            .lineTo(393, 110.5).lineTo(461, 110.5).lineTo(461, 179).lineTo(443, 179).lineTo(443, 144.5).lineTo(393, 144.5).lineTo(393, 179).lineTo(375, 179);
        this.Line_11.setTransform(0, 0);

        this.Line_12 = new cjs.Shape();
        this.Line_12.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[20].x, this.rectimg_1.dots[20].y).lineTo(this.rectimg_1.dots[34].x, this.rectimg_1.dots[34].y).lineTo(this.rectimg_1.dots[33].x, this.rectimg_1.dots[33].y).lineTo(this.rectimg_1.dots[20].x, this.rectimg_1.dots[20].y);

        this.Line_13 = new cjs.Shape();
        this.Line_13.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[76].x, this.rectimg_1.dots[76].y).lineTo(this.rectimg_1.dots[90].x, this.rectimg_1.dots[90].y).lineTo(this.rectimg_1.dots[89].x, this.rectimg_1.dots[89].y).lineTo(this.rectimg_1.dots[76].x, this.rectimg_1.dots[76].y);

        this.Line_14 = new cjs.Shape();
        this.Line_14.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[28].x, this.rectimg_1.dots[28].y).lineTo(this.rectimg_1.dots[29].x, this.rectimg_1.dots[29].y).lineTo(this.rectimg_1.dots[43].x, this.rectimg_1.dots[43].y).lineTo(this.rectimg_1.dots[28].x, this.rectimg_1.dots[28].y);

        this.Line_15 = new cjs.Shape();
        this.Line_15.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[85].x, this.rectimg_1.dots[85].y).lineTo(this.rectimg_1.dots[99].x, this.rectimg_1.dots[99].y).lineTo(this.rectimg_1.dots[100].x, this.rectimg_1.dots[100].y).lineTo(this.rectimg_1.dots[85].x, this.rectimg_1.dots[85].y);

        this.Line_16 = new cjs.Shape();
        this.Line_16.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[32].x, this.rectimg_1.dots[32].y).lineTo(this.rectimg_1.dots[45].x, this.rectimg_1.dots[45].y).lineTo(this.rectimg_1.dots[46].x, this.rectimg_1.dots[46].y).lineTo(this.rectimg_1.dots[32].x, this.rectimg_1.dots[32].y);

        this.Line_17 = new cjs.Shape();
        this.Line_17.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[88].x, this.rectimg_1.dots[88].y).lineTo(this.rectimg_1.dots[102].x, this.rectimg_1.dots[102].y).lineTo(this.rectimg_1.dots[101].x, this.rectimg_1.dots[101].y).lineTo(this.rectimg_1.dots[88].x, this.rectimg_1.dots[88].y);

        // left bottom rect

        this.Line_18 = new cjs.Shape();
        this.Line_18.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(118, 229).lineTo(135, 195).lineTo(152, 229);

        // right bottom rect

        this.Line_19 = new cjs.Shape();
        this.Line_19.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(376, 211).lineTo(340, 230).lineTo(349, 235);

        this.arc1 = new cjs.Shape();
        this.arc1.graphics.f('#000000').s().ss(1.2).mt(this.rectimg_1.dots[2].x, this.rectimg_1.dots[2].y).arc(this.rectimg_1.dots[2].x, this.rectimg_1.dots[2].y, 5, -Math.PI / 6, Math.PI / 4, false);

        this.dot_1 = new cjs.Shape();
        this.dot_1.graphics.f('#000000').ss().s().drawCircle(this.rectimg_1.dots[30].x, this.rectimg_1.dots[30].y, 2.5);


        this.dot_2 = new cjs.Shape();
        this.dot_2.graphics.f('#000000').ss().s().drawCircle(this.rectimg_1.dots[37].x, this.rectimg_1.dots[37].y, 2.5);


        this.arc2 = new cjs.Shape();
        this.arc2.graphics.f('#000000').s().ss(1.2).mt(this.rectimg_1.dots[9].x, this.rectimg_1.dots[9].y).arc(this.rectimg_1.dots[9].x, this.rectimg_1.dots[9].y, 5, -Math.PI / 6, Math.PI / 4, false);

        this.rectimg_1.addChild(this.Line_12, this.Line_13, this.Line_14, this.Line_15, this.Line_16, this.Line_17, this.arc1, this.arc2, this.dot_1, this.dot_2);

        this.addChild(this.roundRect1, this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5,
            this.Line_6, this.Line_7, this.Line_8, this.Line_9, this.Line_10, this.rectimg, this.Line_11,
            this.Line_18, this.Line_19, this.rectimg_1, this.text, this.text_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 515.3, 350);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Vilken är figuren?", "16px 'Myriad Pro'");
        this.text.setTransform(29, 22);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(10, 22);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 0, 520, 260, 10);
        this.roundRect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(5, 150).lineTo(519, 150);
        this.hrLine_1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [51, 185, 310, 51, 185, 310];
        var lineY = [25, 25, 25, 137, 137, 137];

        for (var row = 0; row < lineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(80, 20).lineTo(80, 117);
            vrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(vrLine_1);
        }

        var TextArr = [];
        var TextX = [20, 150, 285, 395, 20, 150, 285, 395];
        var TextY = [57, 57, 57, 57, 170, 170, 170, 170];

        for (var row = 0; row < TextX.length; row++) {

            if (row == 3 || row == 7) {
                var text_1 = new cjs.Text("Den finns inte här.", "16px 'Myriad Pro'", "#000000");
                text_1.setTransform(TextX[row], TextY[row]);
                TextArr.push(text_1);
            } else {
                var text_1 = new cjs.Text("Den finns här.", "16px 'Myriad Pro'", "#000000");
                text_1.setTransform(TextX[row], TextY[row]);
                TextArr.push(text_1);
            }

        }


        var TriArr = [];
        var TriX = [15, 144, 279, 15, 144, 279];
        var TriY = [25, 25, 25, 137, 137, 137];

        for (var row = 0; row < TriX.length; row++) {
            var triangle_1 = new cjs.Shape();
            triangle_1.graphics.f('').s("#008BD2").ss(1.2).moveTo(5, 112).lineTo(100, 112).lineTo(53, 42).lineTo(5, 112);
            triangle_1.setTransform(TriX[row], TriY[row]);
            lineArr.push(triangle_1);
        }

        this.triangle_2 = new cjs.Shape();
        this.triangle_2.graphics.f('').s("#D51317").ss(1.2).moveTo(5, 42).lineTo(100, 42).lineTo(53, 112).lineTo(5, 42);
        this.triangle_2.setTransform(392, 25);

        this.triangle_3 = new cjs.Shape();
        this.triangle_3.graphics.f('').s("#D51317").ss(1.2).moveTo(5, 42).lineTo(100, 42).lineTo(53, 112).lineTo(5, 42);
        this.triangle_3.setTransform(392, 137);

        this.text_2 = new cjs.Text("Figuren", "12px 'Myriad Pro'");
        this.text_2.setTransform(475, 105);

        this.text_3 = new cjs.Text("är:", "12px 'Myriad Pro'");
        this.text_3.setTransform(487, 117);

        this.text_4 = new cjs.Text("Figuren", "12px 'Myriad Pro'");
        this.text_4.setTransform(475, 215);

        this.text_5 = new cjs.Text("är:", "12px 'Myriad Pro'");
        this.text_5.setTransform(487, 227);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group1.graphics.f("#D6326A").s("#000000").ss(0.8, 0, 0, 4).arc(41 + (columnSpace * 20.5), 38 + (row * 19), 6.7, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group1.graphics.f('#00A3C4').s("#000000").ss(0.7).drawRoundRect(57, 32, 13, 13, 0);
                } else if (column == 0 && row == 1) {
                    this.shape_group1.graphics.f('#90BD22').s("#000000").ss(1.2).moveTo(32, 65).lineTo(48, 65).lineTo(40, 52).lineTo(32, 65);
                } else {

                    this.shape_group1.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group1.setTransform(14, 67);


        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group2.graphics.f("#D6326A").s("#000000").ss(0.8, 0, 0, 4).arc(41 + (columnSpace * 20.5), 38 + (row * 19), 6.7, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group2.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 38 + (row * 19), 6.7, 0, 2 * Math.PI);

                } else if (column == 0 && row == 1) {
                    this.shape_group2.graphics.f('#90BD22').s("#000000").ss(1.2).moveTo(32, 65).lineTo(48, 65).lineTo(40, 52).lineTo(32, 65);
                } else {
                    this.shape_group2.graphics.f('#FFF374').s("#000000").ss(0.7).drawRoundRect(57, 52, 13, 13, 0);
                }

            }

        }
        this.shape_group2.setTransform(144, 67);

        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group3.graphics.f('#00A3C4').s("#000000").ss(0.7).drawRoundRect(37, 32, 13, 13, 0);
                } else if (column == 1 && row == 0) {
                    this.shape_group3.graphics.f('#90BD22').s("#000000").ss(1.2).moveTo(57, 45).lineTo(73, 45).lineTo(65, 31).lineTo(57, 45);
                } else if (column == 0 && row == 1) {
                    this.shape_group3.graphics.f("#D6326A").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);
                } else {

                    this.shape_group3.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group3.setTransform(279, 67);

        this.shape_group4 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group4.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 39 + (row * 19), 6.7, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group4.graphics.f("#D6326A").s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 39 + (row * 19), 6.7, 0, 2 * Math.PI);

                } else if (column == 0 && row == 1) {
                    this.shape_group4.graphics.f('#00A3C4').s("#000000").ss(1.2).moveTo(34, 64).lineTo(51, 64).lineTo(43, 50).lineTo(34, 64);
                } else {
                    this.shape_group4.graphics.f('#00A3C4').s("#000000").ss(0.7).drawRoundRect(59, 51, 13, 13, 0);
                }

            }

        }
        this.shape_group4.setTransform(392, 40);

        this.shape_group5 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group5.graphics.f("#D6326A").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group5.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);

                } else if (column == 0 && row == 1) {
                    this.shape_group5.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(37, 52, 13, 13, 0);
                } else {
                    this.shape_group5.graphics.f('#EB5B1B').s("#000000").ss(0.7).drawRoundRect(60, 52, 13, 13, 0);
                }

            }

        }
        this.shape_group5.setTransform(12, 179);

        this.shape_group6 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group6.graphics.f('#EB5B1B').s("#000000").ss(0.7).drawRoundRect(40, 33, 13, 13, 0);
                } else if (column == 1 && row == 0) {
                    this.shape_group6.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(46 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);

                } else if (column == 0 && row == 1) {
                    this.shape_group6.graphics.f("#00A3C4").s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);
                } else {
                    this.shape_group6.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(60, 52, 13, 13, 0);
                }


            }

        }
        this.shape_group6.setTransform(142, 179);

        this.shape_group7 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group7.graphics.f("#90BD22").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 39 + (row * 19), 6.7, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group7.graphics.f('#EB5B1B').s("#000000").ss(0.7).drawRoundRect(59, 33, 13, 13, 0);
                } else if (column == 0 && row == 1) {
                    this.shape_group7.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(37, 52, 13, 13, 0);
                } else {

                    this.shape_group7.graphics.f("#00A3C4").s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 40 + (row * 19), 6.7, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group7.setTransform(278, 179);


        this.shape_group8 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group8.graphics.f('#00A3C4').s("#000000").ss(0.7).drawRoundRect(37, 32, 13, 13, 0);
                } else if (column == 1 && row == 0) {
                    this.shape_group8.graphics.f("#00A3C4").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 38 + (row * 19), 6.7, 0, 2 * Math.PI);

                } else if (column == 0 && row == 1) {
                    this.shape_group8.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(37, 52, 13, 13, 0);
                } else {
                    this.shape_group8.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 39 + (row * 19), 6.7, 0, 2 * Math.PI);

                }
            }

        }

        this.shape_group8.setTransform(392, 153);


        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(39, 40);
        this.hrLine_2.setTransform(477, 97);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(39, 40);
        this.hrLine_3.setTransform(477, 207);

        this.addChild(this.roundRect1, this.hrLine_1, this.triangle_2, this.triangle_3, this.text, this.text_1, this.text_2, this.text_3,
            this.text_4, this.text_5, this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5,
            this.shape_group6, this.shape_group7, this.shape_group8,this.hrLine_2,this.hrLine_3);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }
        for (var i = 0; i < TriArr.length; i++) {
            this.addChild(TriArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 260);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(299, 90, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300, 432, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
