var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        // ishideButtons: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };
    var transformX = transformY = 0.184;
    var moveX = 480 + 70,
        moveY = 6;
    var startX = ((476.0000) * transformX) + moveX,
        startY = ((41.3333) * transformY) + moveY;
    var endX = ((476.0000) * transformX) + moveX,
        endY = ((42.8667) * transformY) + moveY;
    var pencil = null;
    //  new createjs.Point(((86.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY)

    var startX2 = ((476.0000) * transformX) + moveX,
        startY2 = ((41.3333) * transformY) + moveY;
    var endX2 = ((450.0000) * transformX) + moveX,
        endY2 = ((43.8667) * transformY) + moveY;
    var altStartX2 = startX,
        altStartY2 = startY,
        altEndX2 = endX,
        altEndY2 = endY;
    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [

        new createjs.Point(startX, startY),
        //new createjs.Point(((476.0000) * transformX) + moveX, ((41.3333) * transformY) + moveY),
        new createjs.Point(((476.0000) * transformX) + moveX, ((41.6667) * transformY) + moveY),
        new createjs.Point(((476.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((475.0001) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((473.9999) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((473.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((473.0000) * transformX) + moveX, ((42.3333) * transformY) + moveY),
        new createjs.Point(((473.0000) * transformX) + moveX, ((42.6667) * transformY) + moveY),
        new createjs.Point(((473.0000) * transformX) + moveX, ((43.0000) * transformY) + moveY),

        new createjs.Point(((471.0002) * transformX) + moveX, ((43.6666) * transformY) + moveY),
        new createjs.Point(((468.9998) * transformX) + moveX, ((44.3334) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((45.0000) * transformY) + moveY),

        new createjs.Point(((467.0000) * transformX) + moveX, ((45.3333) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((45.6667) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((466.3334) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((465.6666) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((465.0000) * transformX) + moveX, ((46.3333) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((46.6667) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((464.3334) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((463.6666) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((463.0000) * transformX) + moveX, ((47.3333) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((47.6667) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((48.0000) * transformY) + moveY),

        new createjs.Point(((462.3334) * transformX) + moveX, ((48.0000) * transformY) + moveY),
        new createjs.Point(((461.6666) * transformX) + moveX, ((48.0000) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((48.0000) * transformY) + moveY),

        new createjs.Point(((461.0000) * transformX) + moveX, ((48.3333) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((48.6667) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((49.0000) * transformY) + moveY),

        new createjs.Point(((460.3334) * transformX) + moveX, ((49.0000) * transformY) + moveY),
        new createjs.Point(((459.6666) * transformX) + moveX, ((49.0000) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((49.0000) * transformY) + moveY),

        new createjs.Point(((459.0000) * transformX) + moveX, ((49.3333) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((49.6667) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((50.0000) * transformY) + moveY),

        new createjs.Point(((458.3334) * transformX) + moveX, ((50.0000) * transformY) + moveY),
        new createjs.Point(((457.6666) * transformX) + moveX, ((50.0000) * transformY) + moveY),
        new createjs.Point(((457.0000) * transformX) + moveX, ((50.0000) * transformY) + moveY),

        new createjs.Point(((457.0000) * transformX) + moveX, ((50.3333) * transformY) + moveY),
        new createjs.Point(((457.0000) * transformX) + moveX, ((50.6667) * transformY) + moveY),
        new createjs.Point(((457.0000) * transformX) + moveX, ((51.0000) * transformY) + moveY),

        new createjs.Point(((456.3334) * transformX) + moveX, ((51.0000) * transformY) + moveY),
        new createjs.Point(((455.6666) * transformX) + moveX, ((51.0000) * transformY) + moveY),
        new createjs.Point(((455.0000) * transformX) + moveX, ((51.0000) * transformY) + moveY),

        new createjs.Point(((454.6667) * transformX) + moveX, ((51.6666) * transformY) + moveY),
        new createjs.Point(((454.3333) * transformX) + moveX, ((52.3334) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((53.0000) * transformY) + moveY),

        new createjs.Point(((453.3334) * transformX) + moveX, ((53.0000) * transformY) + moveY),
        new createjs.Point(((452.6666) * transformX) + moveX, ((53.0000) * transformY) + moveY),
        new createjs.Point(((452.0000) * transformX) + moveX, ((53.0000) * transformY) + moveY),

        new createjs.Point(((452.0000) * transformX) + moveX, ((53.3333) * transformY) + moveY),
        new createjs.Point(((452.0000) * transformX) + moveX, ((53.6667) * transformY) + moveY),
        new createjs.Point(((452.0000) * transformX) + moveX, ((54.0000) * transformY) + moveY),

        new createjs.Point(((451.3334) * transformX) + moveX, ((54.0000) * transformY) + moveY),
        new createjs.Point(((450.6666) * transformX) + moveX, ((54.0000) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((54.0000) * transformY) + moveY),

        new createjs.Point(((450.0000) * transformX) + moveX, ((54.3333) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((54.6667) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((55.0000) * transformY) + moveY),

        new createjs.Point(((449.3334) * transformX) + moveX, ((55.0000) * transformY) + moveY),
        new createjs.Point(((448.6666) * transformX) + moveX, ((55.0000) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((55.0000) * transformY) + moveY),

        new createjs.Point(((448.0000) * transformX) + moveX, ((55.3333) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((55.6667) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((447.3334) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((446.6666) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((446.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((446.0000) * transformX) + moveX, ((56.3333) * transformY) + moveY),
        new createjs.Point(((446.0000) * transformX) + moveX, ((56.6667) * transformY) + moveY),
        new createjs.Point(((446.0000) * transformX) + moveX, ((57.0000) * transformY) + moveY),

        new createjs.Point(((445.3334) * transformX) + moveX, ((57.0000) * transformY) + moveY),
        new createjs.Point(((444.6666) * transformX) + moveX, ((57.0000) * transformY) + moveY),
        new createjs.Point(((444.0000) * transformX) + moveX, ((57.0000) * transformY) + moveY),

        new createjs.Point(((443.6667) * transformX) + moveX, ((57.6666) * transformY) + moveY),
        new createjs.Point(((443.3333) * transformX) + moveX, ((58.3334) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((442.3334) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((441.6666) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((441.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((441.0000) * transformX) + moveX, ((59.3333) * transformY) + moveY),
        new createjs.Point(((441.0000) * transformX) + moveX, ((59.6667) * transformY) + moveY),
        new createjs.Point(((441.0000) * transformX) + moveX, ((60.0000) * transformY) + moveY),

        new createjs.Point(((440.3334) * transformX) + moveX, ((60.0000) * transformY) + moveY),
        new createjs.Point(((439.6666) * transformX) + moveX, ((60.0000) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((60.0000) * transformY) + moveY),

        new createjs.Point(((438.6667) * transformX) + moveX, ((60.6666) * transformY) + moveY),
        new createjs.Point(((438.3333) * transformX) + moveX, ((61.3334) * transformY) + moveY),
        new createjs.Point(((438.0000) * transformX) + moveX, ((62.0000) * transformY) + moveY),

        new createjs.Point(((436.0002) * transformX) + moveX, ((62.6666) * transformY) + moveY),
        new createjs.Point(((433.9998) * transformX) + moveX, ((63.3334) * transformY) + moveY),
        new createjs.Point(((432.0000) * transformX) + moveX, ((64.0000) * transformY) + moveY),

        new createjs.Point(((431.6667) * transformX) + moveX, ((64.6666) * transformY) + moveY),
        new createjs.Point(((431.3333) * transformX) + moveX, ((65.3334) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((430.3334) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((429.6666) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((428.6667) * transformX) + moveX, ((66.6666) * transformY) + moveY),
        new createjs.Point(((428.3333) * transformX) + moveX, ((67.3334) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((68.0000) * transformY) + moveY),

        new createjs.Point(((426.6668) * transformX) + moveX, ((68.3333) * transformY) + moveY),
        new createjs.Point(((425.3332) * transformX) + moveX, ((68.6667) * transformY) + moveY),
        new createjs.Point(((424.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((423.6667) * transformX) + moveX, ((69.6666) * transformY) + moveY),
        new createjs.Point(((423.3333) * transformX) + moveX, ((70.3334) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((71.0000) * transformY) + moveY),

        new createjs.Point(((422.3334) * transformX) + moveX, ((71.0000) * transformY) + moveY),
        new createjs.Point(((421.6666) * transformX) + moveX, ((71.0000) * transformY) + moveY),
        new createjs.Point(((421.0000) * transformX) + moveX, ((71.0000) * transformY) + moveY),

        new createjs.Point(((420.6667) * transformX) + moveX, ((71.6666) * transformY) + moveY),
        new createjs.Point(((420.3333) * transformX) + moveX, ((72.3334) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((73.0000) * transformY) + moveY),

        new createjs.Point(((419.3334) * transformX) + moveX, ((73.0000) * transformY) + moveY),
        new createjs.Point(((418.6666) * transformX) + moveX, ((73.0000) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((73.0000) * transformY) + moveY),

        new createjs.Point(((417.6667) * transformX) + moveX, ((73.6666) * transformY) + moveY),
        new createjs.Point(((417.3333) * transformX) + moveX, ((74.3334) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((75.0000) * transformY) + moveY),

        new createjs.Point(((416.3334) * transformX) + moveX, ((75.0000) * transformY) + moveY),
        new createjs.Point(((415.6666) * transformX) + moveX, ((75.0000) * transformY) + moveY),
        new createjs.Point(((415.0000) * transformX) + moveX, ((75.0000) * transformY) + moveY),

        new createjs.Point(((414.6667) * transformX) + moveX, ((75.6666) * transformY) + moveY),
        new createjs.Point(((414.3333) * transformX) + moveX, ((76.3334) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((77.0000) * transformY) + moveY),

        new createjs.Point(((413.3334) * transformX) + moveX, ((77.0000) * transformY) + moveY),
        new createjs.Point(((412.6666) * transformX) + moveX, ((77.0000) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((77.0000) * transformY) + moveY),

        new createjs.Point(((412.0000) * transformX) + moveX, ((77.3333) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((77.6667) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((411.0001) * transformX) + moveX, ((78.3333) * transformY) + moveY),
        new createjs.Point(((409.9999) * transformX) + moveX, ((78.6667) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((79.0000) * transformY) + moveY),

        new createjs.Point(((408.6667) * transformX) + moveX, ((79.6666) * transformY) + moveY),
        new createjs.Point(((408.3333) * transformX) + moveX, ((80.3334) * transformY) + moveY),
        new createjs.Point(((408.0000) * transformX) + moveX, ((81.0000) * transformY) + moveY),

        new createjs.Point(((407.3334) * transformX) + moveX, ((81.0000) * transformY) + moveY),
        new createjs.Point(((406.6666) * transformX) + moveX, ((81.0000) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((81.0000) * transformY) + moveY),

        new createjs.Point(((405.6667) * transformX) + moveX, ((81.6666) * transformY) + moveY),
        new createjs.Point(((405.3333) * transformX) + moveX, ((82.3334) * transformY) + moveY),
        new createjs.Point(((405.0000) * transformX) + moveX, ((83.0000) * transformY) + moveY),

        new createjs.Point(((404.3334) * transformX) + moveX, ((83.0000) * transformY) + moveY),
        new createjs.Point(((403.6666) * transformX) + moveX, ((83.0000) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((83.0000) * transformY) + moveY),

        new createjs.Point(((402.3334) * transformX) + moveX, ((83.9999) * transformY) + moveY),
        new createjs.Point(((401.6666) * transformX) + moveX, ((85.0001) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((86.0000) * transformY) + moveY),

        new createjs.Point(((400.3334) * transformX) + moveX, ((86.0000) * transformY) + moveY),
        new createjs.Point(((399.6666) * transformX) + moveX, ((86.0000) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((86.0000) * transformY) + moveY),

        new createjs.Point(((398.6667) * transformX) + moveX, ((86.6666) * transformY) + moveY),
        new createjs.Point(((398.3333) * transformX) + moveX, ((87.3334) * transformY) + moveY),
        new createjs.Point(((398.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),

        new createjs.Point(((397.3334) * transformX) + moveX, ((88.0000) * transformY) + moveY),
        new createjs.Point(((396.6666) * transformX) + moveX, ((88.0000) * transformY) + moveY),
        new createjs.Point(((396.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),

        new createjs.Point(((395.3334) * transformX) + moveX, ((88.9999) * transformY) + moveY),
        new createjs.Point(((394.6666) * transformX) + moveX, ((90.0001) * transformY) + moveY),
        new createjs.Point(((394.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),

        new createjs.Point(((393.3334) * transformX) + moveX, ((91.0000) * transformY) + moveY),
        new createjs.Point(((392.6666) * transformX) + moveX, ((91.0000) * transformY) + moveY),
        new createjs.Point(((392.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),

        new createjs.Point(((391.3334) * transformX) + moveX, ((91.9999) * transformY) + moveY),
        new createjs.Point(((390.6666) * transformX) + moveX, ((93.0001) * transformY) + moveY),
        new createjs.Point(((390.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((389.3334) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((388.6666) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((388.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((387.3334) * transformX) + moveX, ((94.9999) * transformY) + moveY),
        new createjs.Point(((386.6666) * transformX) + moveX, ((96.0001) * transformY) + moveY),
        new createjs.Point(((386.0000) * transformX) + moveX, ((97.0000) * transformY) + moveY),

        new createjs.Point(((385.3334) * transformX) + moveX, ((97.0000) * transformY) + moveY),
        new createjs.Point(((384.6666) * transformX) + moveX, ((97.0000) * transformY) + moveY),
        new createjs.Point(((384.0000) * transformX) + moveX, ((97.0000) * transformY) + moveY),

        new createjs.Point(((383.0001) * transformX) + moveX, ((98.3332) * transformY) + moveY),
        new createjs.Point(((381.9999) * transformX) + moveX, ((99.6668) * transformY) + moveY),
        new createjs.Point(((381.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),

        new createjs.Point(((380.3334) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((379.6666) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((379.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),

        new createjs.Point(((378.0001) * transformX) + moveX, ((102.3332) * transformY) + moveY),
        new createjs.Point(((376.9999) * transformX) + moveX, ((103.6668) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((105.0000) * transformY) + moveY),

        new createjs.Point(((375.3334) * transformX) + moveX, ((105.0000) * transformY) + moveY),
        new createjs.Point(((374.6666) * transformX) + moveX, ((105.0000) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((105.0000) * transformY) + moveY),

        new createjs.Point(((373.0001) * transformX) + moveX, ((106.3332) * transformY) + moveY),
        new createjs.Point(((371.9999) * transformX) + moveX, ((107.6668) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),

        new createjs.Point(((370.3334) * transformX) + moveX, ((109.0000) * transformY) + moveY),
        new createjs.Point(((369.6666) * transformX) + moveX, ((109.0000) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),

        new createjs.Point(((368.3334) * transformX) + moveX, ((109.9999) * transformY) + moveY),
        new createjs.Point(((367.6666) * transformX) + moveX, ((111.0001) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((112.0000) * transformY) + moveY),

        new createjs.Point(((366.0001) * transformX) + moveX, ((112.6666) * transformY) + moveY),
        new createjs.Point(((364.9999) * transformX) + moveX, ((113.3334) * transformY) + moveY),
        new createjs.Point(((364.0000) * transformX) + moveX, ((114.0000) * transformY) + moveY),

        new createjs.Point(((364.0000) * transformX) + moveX, ((114.3333) * transformY) + moveY),
        new createjs.Point(((364.0000) * transformX) + moveX, ((114.6667) * transformY) + moveY),
        new createjs.Point(((364.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),

        new createjs.Point(((363.3334) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((362.6666) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),

        new createjs.Point(((360.0002) * transformX) + moveX, ((117.3331) * transformY) + moveY),
        new createjs.Point(((357.9998) * transformX) + moveX, ((119.6669) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),

        new createjs.Point(((355.3334) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((354.6666) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),

        new createjs.Point(((350.6670) * transformX) + moveX, ((125.6663) * transformY) + moveY),
        new createjs.Point(((347.3330) * transformX) + moveX, ((129.3337) * transformY) + moveY),
        new createjs.Point(((344.0000) * transformX) + moveX, ((133.0000) * transformY) + moveY),

        new createjs.Point(((343.3334) * transformX) + moveX, ((133.0000) * transformY) + moveY),
        new createjs.Point(((342.6666) * transformX) + moveX, ((133.0000) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((133.0000) * transformY) + moveY),

        new createjs.Point(((339.6669) * transformX) + moveX, ((135.6664) * transformY) + moveY),
        new createjs.Point(((337.3331) * transformX) + moveX, ((138.3336) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((141.0000) * transformY) + moveY),

        new createjs.Point(((329.3339) * transformX) + moveX, ((146.3328) * transformY) + moveY),
        new createjs.Point(((323.6661) * transformX) + moveX, ((151.6672) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((157.0000) * transformY) + moveY),

        new createjs.Point(((318.0000) * transformX) + moveX, ((157.6666) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((158.3334) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((159.0000) * transformY) + moveY),

        new createjs.Point(((315.0003) * transformX) + moveX, ((161.6664) * transformY) + moveY),
        new createjs.Point(((311.9997) * transformX) + moveX, ((164.3336) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((167.0000) * transformY) + moveY),

        new createjs.Point(((309.0000) * transformX) + moveX, ((167.3333) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((167.6667) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((168.0000) * transformY) + moveY),

        new createjs.Point(((308.3334) * transformX) + moveX, ((168.3333) * transformY) + moveY),
        new createjs.Point(((307.6666) * transformX) + moveX, ((168.6667) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((169.0000) * transformY) + moveY),

        new createjs.Point(((307.0000) * transformX) + moveX, ((169.6666) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((170.3334) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),

        new createjs.Point(((304.3336) * transformX) + moveX, ((173.3331) * transformY) + moveY),
        new createjs.Point(((301.6664) * transformX) + moveX, ((175.6669) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((178.0000) * transformY) + moveY),

        new createjs.Point(((299.0000) * transformX) + moveX, ((178.6666) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((179.3334) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((180.0000) * transformY) + moveY),

        new createjs.Point(((297.0002) * transformX) + moveX, ((181.6665) * transformY) + moveY),
        new createjs.Point(((294.9998) * transformX) + moveX, ((183.3335) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((185.0000) * transformY) + moveY),

        new createjs.Point(((293.0000) * transformX) + moveX, ((185.6666) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((186.3334) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((187.0000) * transformY) + moveY),

        new createjs.Point(((292.3334) * transformX) + moveX, ((187.3333) * transformY) + moveY),
        new createjs.Point(((291.6666) * transformX) + moveX, ((187.6667) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),

        new createjs.Point(((291.0000) * transformX) + moveX, ((188.3333) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((188.6667) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((189.0000) * transformY) + moveY),

        new createjs.Point(((290.0001) * transformX) + moveX, ((189.6666) * transformY) + moveY),
        new createjs.Point(((288.9999) * transformX) + moveX, ((190.3334) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((191.0000) * transformY) + moveY),

        new createjs.Point(((288.0000) * transformX) + moveX, ((191.6666) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((192.3334) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((193.0000) * transformY) + moveY),

        new createjs.Point(((286.6668) * transformX) + moveX, ((193.9999) * transformY) + moveY),
        new createjs.Point(((285.3332) * transformX) + moveX, ((195.0001) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((196.0000) * transformY) + moveY),

        new createjs.Point(((284.0000) * transformX) + moveX, ((196.6666) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((197.3334) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((198.0000) * transformY) + moveY),

        new createjs.Point(((282.6668) * transformX) + moveX, ((198.9999) * transformY) + moveY),
        new createjs.Point(((281.3332) * transformX) + moveX, ((200.0001) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((201.0000) * transformY) + moveY),

        new createjs.Point(((280.0000) * transformX) + moveX, ((201.6666) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((202.3334) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((203.0000) * transformY) + moveY),

        new createjs.Point(((278.6668) * transformX) + moveX, ((203.9999) * transformY) + moveY),
        new createjs.Point(((277.3332) * transformX) + moveX, ((205.0001) * transformY) + moveY),
        new createjs.Point(((276.0000) * transformX) + moveX, ((206.0000) * transformY) + moveY),

        new createjs.Point(((276.0000) * transformX) + moveX, ((206.6666) * transformY) + moveY),
        new createjs.Point(((276.0000) * transformX) + moveX, ((207.3334) * transformY) + moveY),
        new createjs.Point(((276.0000) * transformX) + moveX, ((208.0000) * transformY) + moveY),

        new createjs.Point(((275.0001) * transformX) + moveX, ((208.6666) * transformY) + moveY),
        new createjs.Point(((273.9999) * transformX) + moveX, ((209.3334) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((210.0000) * transformY) + moveY),

        new createjs.Point(((273.0000) * transformX) + moveX, ((210.6666) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((211.3334) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((212.0000) * transformY) + moveY),

        new createjs.Point(((271.6668) * transformX) + moveX, ((212.9999) * transformY) + moveY),
        new createjs.Point(((270.3332) * transformX) + moveX, ((214.0001) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((215.0000) * transformY) + moveY),

        new createjs.Point(((269.0000) * transformX) + moveX, ((215.6666) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((216.3334) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((217.0000) * transformY) + moveY),

        new createjs.Point(((268.0001) * transformX) + moveX, ((217.6666) * transformY) + moveY),
        new createjs.Point(((266.9999) * transformX) + moveX, ((218.3334) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((219.0000) * transformY) + moveY),

        new createjs.Point(((266.0000) * transformX) + moveX, ((219.6666) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((220.3334) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),

        new createjs.Point(((265.0001) * transformX) + moveX, ((221.6666) * transformY) + moveY),
        new createjs.Point(((263.9999) * transformX) + moveX, ((222.3334) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((223.0000) * transformY) + moveY),

        new createjs.Point(((263.0000) * transformX) + moveX, ((223.6666) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((224.3334) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((225.0000) * transformY) + moveY),

        new createjs.Point(((262.3334) * transformX) + moveX, ((225.3333) * transformY) + moveY),
        new createjs.Point(((261.6666) * transformX) + moveX, ((225.6667) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((226.0000) * transformY) + moveY),

        new createjs.Point(((261.0000) * transformX) + moveX, ((226.6666) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((227.3334) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((228.0000) * transformY) + moveY),

        new createjs.Point(((260.3334) * transformX) + moveX, ((228.3333) * transformY) + moveY),
        new createjs.Point(((259.6666) * transformX) + moveX, ((228.6667) * transformY) + moveY),
        new createjs.Point(((259.0000) * transformX) + moveX, ((229.0000) * transformY) + moveY),

        new createjs.Point(((258.6667) * transformX) + moveX, ((229.9999) * transformY) + moveY),
        new createjs.Point(((258.3333) * transformX) + moveX, ((231.0001) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((232.0000) * transformY) + moveY),

        new createjs.Point(((257.0001) * transformX) + moveX, ((232.6666) * transformY) + moveY),
        new createjs.Point(((255.9999) * transformX) + moveX, ((233.3334) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((234.0000) * transformY) + moveY),

        new createjs.Point(((255.0000) * transformX) + moveX, ((234.6666) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((235.3334) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),

        new createjs.Point(((254.6667) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((254.3333) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),

        new createjs.Point(((253.6667) * transformX) + moveX, ((236.9999) * transformY) + moveY),
        new createjs.Point(((253.3333) * transformX) + moveX, ((238.0001) * transformY) + moveY),
        new createjs.Point(((253.0000) * transformX) + moveX, ((239.0000) * transformY) + moveY),

        new createjs.Point(((252.3334) * transformX) + moveX, ((239.3333) * transformY) + moveY),
        new createjs.Point(((251.6666) * transformX) + moveX, ((239.6667) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((240.0000) * transformY) + moveY),

        new createjs.Point(((251.0000) * transformX) + moveX, ((240.6666) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((241.3334) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((242.0000) * transformY) + moveY),

        new createjs.Point(((250.0001) * transformX) + moveX, ((242.6666) * transformY) + moveY),
        new createjs.Point(((248.9999) * transformX) + moveX, ((243.3334) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((244.0000) * transformY) + moveY),

        new createjs.Point(((248.0000) * transformX) + moveX, ((244.6666) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((245.3334) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((246.0000) * transformY) + moveY),

        new createjs.Point(((247.3334) * transformX) + moveX, ((246.3333) * transformY) + moveY),
        new createjs.Point(((246.6666) * transformX) + moveX, ((246.6667) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((247.0000) * transformY) + moveY),

        new createjs.Point(((245.6667) * transformX) + moveX, ((247.9999) * transformY) + moveY),
        new createjs.Point(((245.3333) * transformX) + moveX, ((249.0001) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((250.0000) * transformY) + moveY),

        new createjs.Point(((244.6667) * transformX) + moveX, ((250.0000) * transformY) + moveY),
        new createjs.Point(((244.3333) * transformX) + moveX, ((250.0000) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((250.0000) * transformY) + moveY),

        new createjs.Point(((244.0000) * transformX) + moveX, ((250.6666) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((251.3334) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((252.0000) * transformY) + moveY),

        new createjs.Point(((243.3334) * transformX) + moveX, ((252.3333) * transformY) + moveY),
        new createjs.Point(((242.6666) * transformX) + moveX, ((252.6667) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((253.0000) * transformY) + moveY),

        new createjs.Point(((242.0000) * transformX) + moveX, ((253.6666) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((254.3334) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((255.0000) * transformY) + moveY),

        new createjs.Point(((241.3334) * transformX) + moveX, ((255.3333) * transformY) + moveY),
        new createjs.Point(((240.6666) * transformX) + moveX, ((255.6667) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((256.0000) * transformY) + moveY),

        new createjs.Point(((240.0000) * transformX) + moveX, ((256.6666) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((257.3334) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((258.0000) * transformY) + moveY),

        new createjs.Point(((239.3334) * transformX) + moveX, ((258.3333) * transformY) + moveY),
        new createjs.Point(((238.6666) * transformX) + moveX, ((258.6667) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((259.0000) * transformY) + moveY),

        new createjs.Point(((238.0000) * transformX) + moveX, ((259.6666) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((260.3334) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((261.0000) * transformY) + moveY),

        new createjs.Point(((237.3334) * transformX) + moveX, ((261.3333) * transformY) + moveY),
        new createjs.Point(((236.6666) * transformX) + moveX, ((261.6667) * transformY) + moveY),
        new createjs.Point(((236.0000) * transformX) + moveX, ((262.0000) * transformY) + moveY),

        new createjs.Point(((236.0000) * transformX) + moveX, ((262.6666) * transformY) + moveY),
        new createjs.Point(((236.0000) * transformX) + moveX, ((263.3334) * transformY) + moveY),
        new createjs.Point(((236.0000) * transformX) + moveX, ((264.0000) * transformY) + moveY),

        new createjs.Point(((235.3334) * transformX) + moveX, ((264.3333) * transformY) + moveY),
        new createjs.Point(((234.6666) * transformX) + moveX, ((264.6667) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((265.0000) * transformY) + moveY),

        new createjs.Point(((234.0000) * transformX) + moveX, ((265.6666) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((266.3334) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((267.0000) * transformY) + moveY),

        new createjs.Point(((233.3334) * transformX) + moveX, ((267.3333) * transformY) + moveY),
        new createjs.Point(((232.6666) * transformX) + moveX, ((267.6667) * transformY) + moveY),
        new createjs.Point(((232.0000) * transformX) + moveX, ((268.0000) * transformY) + moveY),

        new createjs.Point(((231.6667) * transformX) + moveX, ((269.3332) * transformY) + moveY),
        new createjs.Point(((231.3333) * transformX) + moveX, ((270.6668) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((272.0000) * transformY) + moveY),

        new createjs.Point(((230.6667) * transformX) + moveX, ((272.0000) * transformY) + moveY),
        new createjs.Point(((230.3333) * transformX) + moveX, ((272.0000) * transformY) + moveY),
        new createjs.Point(((230.0000) * transformX) + moveX, ((272.0000) * transformY) + moveY),

        new createjs.Point(((229.6667) * transformX) + moveX, ((272.9999) * transformY) + moveY),
        new createjs.Point(((229.3333) * transformX) + moveX, ((274.0001) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((275.0000) * transformY) + moveY),

        new createjs.Point(((228.3334) * transformX) + moveX, ((275.3333) * transformY) + moveY),
        new createjs.Point(((227.6666) * transformX) + moveX, ((275.6667) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((276.0000) * transformY) + moveY),

        new createjs.Point(((226.6667) * transformX) + moveX, ((277.3332) * transformY) + moveY),
        new createjs.Point(((226.3333) * transformX) + moveX, ((278.6668) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((280.0000) * transformY) + moveY),

        new createjs.Point(((225.6667) * transformX) + moveX, ((280.0000) * transformY) + moveY),
        new createjs.Point(((225.3333) * transformX) + moveX, ((280.0000) * transformY) + moveY),
        new createjs.Point(((225.0000) * transformX) + moveX, ((280.0000) * transformY) + moveY),

        new createjs.Point(((224.6667) * transformX) + moveX, ((280.9999) * transformY) + moveY),
        new createjs.Point(((224.3333) * transformX) + moveX, ((282.0001) * transformY) + moveY),
        new createjs.Point(((224.0000) * transformX) + moveX, ((283.0000) * transformY) + moveY),

        new createjs.Point(((223.3334) * transformX) + moveX, ((283.3333) * transformY) + moveY),
        new createjs.Point(((222.6666) * transformX) + moveX, ((283.6667) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((284.0000) * transformY) + moveY),

        new createjs.Point(((221.6667) * transformX) + moveX, ((285.3332) * transformY) + moveY),
        new createjs.Point(((221.3333) * transformX) + moveX, ((286.6668) * transformY) + moveY),
        new createjs.Point(((221.0000) * transformX) + moveX, ((288.0000) * transformY) + moveY),

        new createjs.Point(((220.3334) * transformX) + moveX, ((288.3333) * transformY) + moveY),
        new createjs.Point(((219.6666) * transformX) + moveX, ((288.6667) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((289.0000) * transformY) + moveY),

        new createjs.Point(((218.6667) * transformX) + moveX, ((290.3332) * transformY) + moveY),
        new createjs.Point(((218.3333) * transformX) + moveX, ((291.6668) * transformY) + moveY),
        new createjs.Point(((218.0000) * transformX) + moveX, ((293.0000) * transformY) + moveY),

        new createjs.Point(((217.3334) * transformX) + moveX, ((293.3333) * transformY) + moveY),
        new createjs.Point(((216.6666) * transformX) + moveX, ((293.6667) * transformY) + moveY),
        new createjs.Point(((216.0000) * transformX) + moveX, ((294.0000) * transformY) + moveY),

        new createjs.Point(((215.6667) * transformX) + moveX, ((295.3332) * transformY) + moveY),
        new createjs.Point(((215.3333) * transformX) + moveX, ((296.6668) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((298.0000) * transformY) + moveY),

        new createjs.Point(((214.3334) * transformX) + moveX, ((298.3333) * transformY) + moveY),
        new createjs.Point(((213.6666) * transformX) + moveX, ((298.6667) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((299.0000) * transformY) + moveY),

        new createjs.Point(((212.6667) * transformX) + moveX, ((300.3332) * transformY) + moveY),
        new createjs.Point(((212.3333) * transformX) + moveX, ((301.6668) * transformY) + moveY),
        new createjs.Point(((212.0000) * transformX) + moveX, ((303.0000) * transformY) + moveY),

        new createjs.Point(((211.3334) * transformX) + moveX, ((303.3333) * transformY) + moveY),
        new createjs.Point(((210.6666) * transformX) + moveX, ((303.6667) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((304.0000) * transformY) + moveY),

        new createjs.Point(((210.0000) * transformX) + moveX, ((304.6666) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((305.3334) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((306.0000) * transformY) + moveY),

        new createjs.Point(((209.6667) * transformX) + moveX, ((306.0000) * transformY) + moveY),
        new createjs.Point(((209.3333) * transformX) + moveX, ((306.0000) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((306.0000) * transformY) + moveY),

        new createjs.Point(((209.0000) * transformX) + moveX, ((306.6666) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((307.3334) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((308.0000) * transformY) + moveY),

        new createjs.Point(((208.6667) * transformX) + moveX, ((308.0000) * transformY) + moveY),
        new createjs.Point(((208.3333) * transformX) + moveX, ((308.0000) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((308.0000) * transformY) + moveY),

        new createjs.Point(((208.0000) * transformX) + moveX, ((308.6666) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((309.3334) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((310.0000) * transformY) + moveY),

        new createjs.Point(((207.3334) * transformX) + moveX, ((310.3333) * transformY) + moveY),
        new createjs.Point(((206.6666) * transformX) + moveX, ((310.6667) * transformY) + moveY),
        new createjs.Point(((206.0000) * transformX) + moveX, ((311.0000) * transformY) + moveY),

        new createjs.Point(((205.3334) * transformX) + moveX, ((312.9998) * transformY) + moveY),
        new createjs.Point(((204.6666) * transformX) + moveX, ((315.0002) * transformY) + moveY),
        new createjs.Point(((204.0000) * transformX) + moveX, ((317.0000) * transformY) + moveY),

        new createjs.Point(((203.3334) * transformX) + moveX, ((317.3333) * transformY) + moveY),
        new createjs.Point(((202.6666) * transformX) + moveX, ((317.6667) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((318.0000) * transformY) + moveY),

        new createjs.Point(((201.6667) * transformX) + moveX, ((319.3332) * transformY) + moveY),
        new createjs.Point(((201.3333) * transformX) + moveX, ((320.6668) * transformY) + moveY),
        new createjs.Point(((201.0000) * transformX) + moveX, ((322.0000) * transformY) + moveY),

        new createjs.Point(((200.6667) * transformX) + moveX, ((322.0000) * transformY) + moveY),
        new createjs.Point(((200.3333) * transformX) + moveX, ((322.0000) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((322.0000) * transformY) + moveY),

        new createjs.Point(((199.3334) * transformX) + moveX, ((323.9998) * transformY) + moveY),
        new createjs.Point(((198.6666) * transformX) + moveX, ((326.0002) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((328.0000) * transformY) + moveY),

        new createjs.Point(((197.3334) * transformX) + moveX, ((328.3333) * transformY) + moveY),
        new createjs.Point(((196.6666) * transformX) + moveX, ((328.6667) * transformY) + moveY),
        new createjs.Point(((196.0000) * transformX) + moveX, ((329.0000) * transformY) + moveY),

        new createjs.Point(((196.0000) * transformX) + moveX, ((329.6666) * transformY) + moveY),
        new createjs.Point(((196.0000) * transformX) + moveX, ((330.3334) * transformY) + moveY),
        new createjs.Point(((196.0000) * transformX) + moveX, ((331.0000) * transformY) + moveY),

        new createjs.Point(((195.6667) * transformX) + moveX, ((331.0000) * transformY) + moveY),
        new createjs.Point(((195.3333) * transformX) + moveX, ((331.0000) * transformY) + moveY),
        new createjs.Point(((195.0000) * transformX) + moveX, ((331.0000) * transformY) + moveY),

        new createjs.Point(((195.0000) * transformX) + moveX, ((331.6666) * transformY) + moveY),
        new createjs.Point(((195.0000) * transformX) + moveX, ((332.3334) * transformY) + moveY),
        new createjs.Point(((195.0000) * transformX) + moveX, ((333.0000) * transformY) + moveY),

        new createjs.Point(((194.6667) * transformX) + moveX, ((333.0000) * transformY) + moveY),
        new createjs.Point(((194.3333) * transformX) + moveX, ((333.0000) * transformY) + moveY),
        new createjs.Point(((194.0000) * transformX) + moveX, ((333.0000) * transformY) + moveY),

        new createjs.Point(((194.0000) * transformX) + moveX, ((333.6666) * transformY) + moveY),
        new createjs.Point(((194.0000) * transformX) + moveX, ((334.3334) * transformY) + moveY),
        new createjs.Point(((194.0000) * transformX) + moveX, ((335.0000) * transformY) + moveY),

        new createjs.Point(((193.6667) * transformX) + moveX, ((335.0000) * transformY) + moveY),
        new createjs.Point(((193.3333) * transformX) + moveX, ((335.0000) * transformY) + moveY),
        new createjs.Point(((193.0000) * transformX) + moveX, ((335.0000) * transformY) + moveY),

        new createjs.Point(((193.0000) * transformX) + moveX, ((335.6666) * transformY) + moveY),
        new createjs.Point(((193.0000) * transformX) + moveX, ((336.3334) * transformY) + moveY),
        new createjs.Point(((193.0000) * transformX) + moveX, ((337.0000) * transformY) + moveY),

        new createjs.Point(((192.6667) * transformX) + moveX, ((337.0000) * transformY) + moveY),
        new createjs.Point(((192.3333) * transformX) + moveX, ((337.0000) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((337.0000) * transformY) + moveY),

        new createjs.Point(((192.0000) * transformX) + moveX, ((337.6666) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((338.3334) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),

        new createjs.Point(((191.6667) * transformX) + moveX, ((339.0000) * transformY) + moveY),
        new createjs.Point(((191.3333) * transformX) + moveX, ((339.0000) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),

        new createjs.Point(((191.0000) * transformX) + moveX, ((339.6666) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((340.3334) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((341.0000) * transformY) + moveY),

        new createjs.Point(((190.3334) * transformX) + moveX, ((341.3333) * transformY) + moveY),
        new createjs.Point(((189.6666) * transformX) + moveX, ((341.6667) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((342.0000) * transformY) + moveY),

        new createjs.Point(((188.6667) * transformX) + moveX, ((343.3332) * transformY) + moveY),
        new createjs.Point(((188.3333) * transformX) + moveX, ((344.6668) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((346.0000) * transformY) + moveY),

        new createjs.Point(((187.6667) * transformX) + moveX, ((346.0000) * transformY) + moveY),
        new createjs.Point(((187.3333) * transformX) + moveX, ((346.0000) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((346.0000) * transformY) + moveY),

        new createjs.Point(((187.0000) * transformX) + moveX, ((346.6666) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((347.3334) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((348.0000) * transformY) + moveY),

        new createjs.Point(((186.6667) * transformX) + moveX, ((348.0000) * transformY) + moveY),
        new createjs.Point(((186.3333) * transformX) + moveX, ((348.0000) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((348.0000) * transformY) + moveY),

        new createjs.Point(((186.0000) * transformX) + moveX, ((348.6666) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((349.3334) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((350.0000) * transformY) + moveY),

        new createjs.Point(((185.6667) * transformX) + moveX, ((350.0000) * transformY) + moveY),
        new createjs.Point(((185.3333) * transformX) + moveX, ((350.0000) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((350.0000) * transformY) + moveY),

        new createjs.Point(((185.0000) * transformX) + moveX, ((350.6666) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((351.3334) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((352.0000) * transformY) + moveY),

        new createjs.Point(((184.6667) * transformX) + moveX, ((352.0000) * transformY) + moveY),
        new createjs.Point(((184.3333) * transformX) + moveX, ((352.0000) * transformY) + moveY),
        new createjs.Point(((184.0000) * transformX) + moveX, ((352.0000) * transformY) + moveY),

        new createjs.Point(((184.0000) * transformX) + moveX, ((352.6666) * transformY) + moveY),
        new createjs.Point(((184.0000) * transformX) + moveX, ((353.3334) * transformY) + moveY),
        new createjs.Point(((184.0000) * transformX) + moveX, ((354.0000) * transformY) + moveY),

        new createjs.Point(((183.6667) * transformX) + moveX, ((354.0000) * transformY) + moveY),
        new createjs.Point(((183.3333) * transformX) + moveX, ((354.0000) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((354.0000) * transformY) + moveY),

        new createjs.Point(((183.0000) * transformX) + moveX, ((354.6666) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((355.3334) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((356.0000) * transformY) + moveY),

        new createjs.Point(((182.6667) * transformX) + moveX, ((356.0000) * transformY) + moveY),
        new createjs.Point(((182.3333) * transformX) + moveX, ((356.0000) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((356.0000) * transformY) + moveY),

        new createjs.Point(((181.6667) * transformX) + moveX, ((357.3332) * transformY) + moveY),
        new createjs.Point(((181.3333) * transformX) + moveX, ((358.6668) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((360.0000) * transformY) + moveY),

        new createjs.Point(((180.6667) * transformX) + moveX, ((360.0000) * transformY) + moveY),
        new createjs.Point(((180.3333) * transformX) + moveX, ((360.0000) * transformY) + moveY),
        new createjs.Point(((180.0000) * transformX) + moveX, ((360.0000) * transformY) + moveY),

        new createjs.Point(((179.6667) * transformX) + moveX, ((361.3332) * transformY) + moveY),
        new createjs.Point(((179.3333) * transformX) + moveX, ((362.6668) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((364.0000) * transformY) + moveY),

        new createjs.Point(((178.6667) * transformX) + moveX, ((364.0000) * transformY) + moveY),
        new createjs.Point(((178.3333) * transformX) + moveX, ((364.0000) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((364.0000) * transformY) + moveY),

        new createjs.Point(((177.6667) * transformX) + moveX, ((365.3332) * transformY) + moveY),
        new createjs.Point(((177.3333) * transformX) + moveX, ((366.6668) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((368.0000) * transformY) + moveY),

        new createjs.Point(((176.6667) * transformX) + moveX, ((368.0000) * transformY) + moveY),
        new createjs.Point(((176.3333) * transformX) + moveX, ((368.0000) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((368.0000) * transformY) + moveY),

        new createjs.Point(((176.0000) * transformX) + moveX, ((368.6666) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((369.3334) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((370.0000) * transformY) + moveY),

        new createjs.Point(((175.6667) * transformX) + moveX, ((370.0000) * transformY) + moveY),
        new createjs.Point(((175.3333) * transformX) + moveX, ((370.0000) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((370.0000) * transformY) + moveY),

        new createjs.Point(((175.0000) * transformX) + moveX, ((370.6666) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((371.3334) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((372.0000) * transformY) + moveY),

        new createjs.Point(((174.6667) * transformX) + moveX, ((372.0000) * transformY) + moveY),
        new createjs.Point(((174.3333) * transformX) + moveX, ((372.0000) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((372.0000) * transformY) + moveY),

        new createjs.Point(((174.0000) * transformX) + moveX, ((372.6666) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((373.3334) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((374.0000) * transformY) + moveY),

        new createjs.Point(((173.6667) * transformX) + moveX, ((374.0000) * transformY) + moveY),
        new createjs.Point(((173.3333) * transformX) + moveX, ((374.0000) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((374.0000) * transformY) + moveY),

        new createjs.Point(((173.0000) * transformX) + moveX, ((374.6666) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((375.3334) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((376.0000) * transformY) + moveY),

        new createjs.Point(((172.6667) * transformX) + moveX, ((376.0000) * transformY) + moveY),
        new createjs.Point(((172.3333) * transformX) + moveX, ((376.0000) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((376.0000) * transformY) + moveY),

        new createjs.Point(((171.3334) * transformX) + moveX, ((377.9998) * transformY) + moveY),
        new createjs.Point(((170.6666) * transformX) + moveX, ((380.0002) * transformY) + moveY),
        new createjs.Point(((170.0000) * transformX) + moveX, ((382.0000) * transformY) + moveY),

        new createjs.Point(((169.6667) * transformX) + moveX, ((382.0000) * transformY) + moveY),
        new createjs.Point(((169.3333) * transformX) + moveX, ((382.0000) * transformY) + moveY),
        new createjs.Point(((169.0000) * transformX) + moveX, ((382.0000) * transformY) + moveY),

        new createjs.Point(((168.6667) * transformX) + moveX, ((383.3332) * transformY) + moveY),
        new createjs.Point(((168.3333) * transformX) + moveX, ((384.6668) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((386.0000) * transformY) + moveY),

        new createjs.Point(((167.6667) * transformX) + moveX, ((386.0000) * transformY) + moveY),
        new createjs.Point(((167.3333) * transformX) + moveX, ((386.0000) * transformY) + moveY),
        new createjs.Point(((167.0000) * transformX) + moveX, ((386.0000) * transformY) + moveY),

        new createjs.Point(((167.0000) * transformX) + moveX, ((386.9999) * transformY) + moveY),
        new createjs.Point(((167.0000) * transformX) + moveX, ((388.0001) * transformY) + moveY),
        new createjs.Point(((167.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((166.6667) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((166.3333) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((166.0000) * transformX) + moveX, ((389.6666) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((390.3334) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((391.0000) * transformY) + moveY),

        new createjs.Point(((165.6667) * transformX) + moveX, ((391.0000) * transformY) + moveY),
        new createjs.Point(((165.3333) * transformX) + moveX, ((391.0000) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((391.0000) * transformY) + moveY),

        new createjs.Point(((165.0000) * transformX) + moveX, ((391.6666) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((392.3334) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((393.0000) * transformY) + moveY),

        new createjs.Point(((164.6667) * transformX) + moveX, ((393.0000) * transformY) + moveY),
        new createjs.Point(((164.3333) * transformX) + moveX, ((393.0000) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((393.0000) * transformY) + moveY),

        new createjs.Point(((163.3334) * transformX) + moveX, ((394.9998) * transformY) + moveY),
        new createjs.Point(((162.6666) * transformX) + moveX, ((397.0002) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((399.0000) * transformY) + moveY),

        new createjs.Point(((161.6667) * transformX) + moveX, ((399.0000) * transformY) + moveY),
        new createjs.Point(((161.3333) * transformX) + moveX, ((399.0000) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((399.0000) * transformY) + moveY),

        new createjs.Point(((161.0000) * transformX) + moveX, ((399.9999) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((401.0001) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((402.0000) * transformY) + moveY),

        new createjs.Point(((160.6667) * transformX) + moveX, ((402.0000) * transformY) + moveY),
        new createjs.Point(((160.3333) * transformX) + moveX, ((402.0000) * transformY) + moveY),
        new createjs.Point(((160.0000) * transformX) + moveX, ((402.0000) * transformY) + moveY),

        new createjs.Point(((160.0000) * transformX) + moveX, ((402.6666) * transformY) + moveY),
        new createjs.Point(((160.0000) * transformX) + moveX, ((403.3334) * transformY) + moveY),
        new createjs.Point(((160.0000) * transformX) + moveX, ((404.0000) * transformY) + moveY),

        new createjs.Point(((159.6667) * transformX) + moveX, ((404.0000) * transformY) + moveY),
        new createjs.Point(((159.3333) * transformX) + moveX, ((404.0000) * transformY) + moveY),
        new createjs.Point(((159.0000) * transformX) + moveX, ((404.0000) * transformY) + moveY),

        new createjs.Point(((158.3334) * transformX) + moveX, ((405.9998) * transformY) + moveY),
        new createjs.Point(((157.6666) * transformX) + moveX, ((408.0002) * transformY) + moveY),
        new createjs.Point(((157.0000) * transformX) + moveX, ((410.0000) * transformY) + moveY),

        new createjs.Point(((156.6667) * transformX) + moveX, ((410.0000) * transformY) + moveY),
        new createjs.Point(((156.3333) * transformX) + moveX, ((410.0000) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((410.0000) * transformY) + moveY),

        new createjs.Point(((156.0000) * transformX) + moveX, ((410.9999) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((412.0001) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((413.0000) * transformY) + moveY),

        new createjs.Point(((155.6667) * transformX) + moveX, ((413.0000) * transformY) + moveY),
        new createjs.Point(((155.3333) * transformX) + moveX, ((413.0000) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((413.0000) * transformY) + moveY),

        new createjs.Point(((155.0000) * transformX) + moveX, ((413.6666) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((414.3334) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((415.0000) * transformY) + moveY),

        new createjs.Point(((154.6667) * transformX) + moveX, ((415.0000) * transformY) + moveY),
        new createjs.Point(((154.3333) * transformX) + moveX, ((415.0000) * transformY) + moveY),
        new createjs.Point(((154.0000) * transformX) + moveX, ((415.0000) * transformY) + moveY),

        new createjs.Point(((153.6667) * transformX) + moveX, ((416.3332) * transformY) + moveY),
        new createjs.Point(((153.3333) * transformX) + moveX, ((417.6668) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((419.0000) * transformY) + moveY),

        new createjs.Point(((152.6667) * transformX) + moveX, ((419.0000) * transformY) + moveY),
        new createjs.Point(((152.3333) * transformX) + moveX, ((419.0000) * transformY) + moveY),
        new createjs.Point(((152.0000) * transformX) + moveX, ((419.0000) * transformY) + moveY),

        new createjs.Point(((152.0000) * transformX) + moveX, ((419.9999) * transformY) + moveY),
        new createjs.Point(((152.0000) * transformX) + moveX, ((421.0001) * transformY) + moveY),
        new createjs.Point(((152.0000) * transformX) + moveX, ((422.0000) * transformY) + moveY),

        new createjs.Point(((151.6667) * transformX) + moveX, ((422.0000) * transformY) + moveY),
        new createjs.Point(((151.3333) * transformX) + moveX, ((422.0000) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((422.0000) * transformY) + moveY),

        new createjs.Point(((150.6667) * transformX) + moveX, ((423.3332) * transformY) + moveY),
        new createjs.Point(((150.3333) * transformX) + moveX, ((424.6668) * transformY) + moveY),
        new createjs.Point(((150.0000) * transformX) + moveX, ((426.0000) * transformY) + moveY),

        new createjs.Point(((149.6667) * transformX) + moveX, ((426.0000) * transformY) + moveY),
        new createjs.Point(((149.3333) * transformX) + moveX, ((426.0000) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((426.0000) * transformY) + moveY),

        new createjs.Point(((149.0000) * transformX) + moveX, ((426.9999) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((428.0001) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((429.0000) * transformY) + moveY),

        new createjs.Point(((148.6667) * transformX) + moveX, ((429.0000) * transformY) + moveY),
        new createjs.Point(((148.3333) * transformX) + moveX, ((429.0000) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((429.0000) * transformY) + moveY),

        new createjs.Point(((147.6667) * transformX) + moveX, ((430.3332) * transformY) + moveY),
        new createjs.Point(((147.3333) * transformX) + moveX, ((431.6668) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((433.0000) * transformY) + moveY),

        new createjs.Point(((146.6667) * transformX) + moveX, ((433.0000) * transformY) + moveY),
        new createjs.Point(((146.3333) * transformX) + moveX, ((433.0000) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((433.0000) * transformY) + moveY),

        new createjs.Point(((146.0000) * transformX) + moveX, ((433.9999) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((435.0001) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((436.0000) * transformY) + moveY),

        new createjs.Point(((145.6667) * transformX) + moveX, ((436.0000) * transformY) + moveY),
        new createjs.Point(((145.3333) * transformX) + moveX, ((436.0000) * transformY) + moveY),
        new createjs.Point(((145.0000) * transformX) + moveX, ((436.0000) * transformY) + moveY),

        new createjs.Point(((144.6667) * transformX) + moveX, ((437.3332) * transformY) + moveY),
        new createjs.Point(((144.3333) * transformX) + moveX, ((438.6668) * transformY) + moveY),
        new createjs.Point(((144.0000) * transformX) + moveX, ((440.0000) * transformY) + moveY),

        new createjs.Point(((143.6667) * transformX) + moveX, ((440.0000) * transformY) + moveY),
        new createjs.Point(((143.3333) * transformX) + moveX, ((440.0000) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((440.0000) * transformY) + moveY),

        new createjs.Point(((143.0000) * transformX) + moveX, ((440.9999) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((442.0001) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((443.0000) * transformY) + moveY),

        new createjs.Point(((142.6667) * transformX) + moveX, ((443.0000) * transformY) + moveY),
        new createjs.Point(((142.3333) * transformX) + moveX, ((443.0000) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((443.0000) * transformY) + moveY),

        new createjs.Point(((142.0000) * transformX) + moveX, ((443.6666) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((444.3334) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((445.0000) * transformY) + moveY),

        new createjs.Point(((141.6667) * transformX) + moveX, ((445.0000) * transformY) + moveY),
        new createjs.Point(((141.3333) * transformX) + moveX, ((445.0000) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((445.0000) * transformY) + moveY),

        new createjs.Point(((141.0000) * transformX) + moveX, ((445.9999) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((447.0001) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((448.0000) * transformY) + moveY),

        new createjs.Point(((140.6667) * transformX) + moveX, ((448.0000) * transformY) + moveY),
        new createjs.Point(((140.3333) * transformX) + moveX, ((448.0000) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((448.0000) * transformY) + moveY),

        new createjs.Point(((140.0000) * transformX) + moveX, ((448.6666) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((449.3334) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((450.0000) * transformY) + moveY),

        new createjs.Point(((139.6667) * transformX) + moveX, ((450.0000) * transformY) + moveY),
        new createjs.Point(((139.3333) * transformX) + moveX, ((450.0000) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((450.0000) * transformY) + moveY),

        new createjs.Point(((138.6667) * transformX) + moveX, ((451.6665) * transformY) + moveY),
        new createjs.Point(((138.3333) * transformX) + moveX, ((453.3335) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((137.6667) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((137.3333) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((137.0000) * transformX) + moveX, ((455.9999) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((457.0001) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((458.0000) * transformY) + moveY),

        new createjs.Point(((136.6667) * transformX) + moveX, ((458.0000) * transformY) + moveY),
        new createjs.Point(((136.3333) * transformX) + moveX, ((458.0000) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((458.0000) * transformY) + moveY),

        new createjs.Point(((136.0000) * transformX) + moveX, ((458.6666) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((459.3334) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((460.0000) * transformY) + moveY),

        new createjs.Point(((135.6667) * transformX) + moveX, ((460.0000) * transformY) + moveY),
        new createjs.Point(((135.3333) * transformX) + moveX, ((460.0000) * transformY) + moveY),
        new createjs.Point(((135.0000) * transformX) + moveX, ((460.0000) * transformY) + moveY),

        new createjs.Point(((135.0000) * transformX) + moveX, ((460.9999) * transformY) + moveY),
        new createjs.Point(((135.0000) * transformX) + moveX, ((462.0001) * transformY) + moveY),
        new createjs.Point(((135.0000) * transformX) + moveX, ((463.0000) * transformY) + moveY),

        new createjs.Point(((134.6667) * transformX) + moveX, ((463.0000) * transformY) + moveY),
        new createjs.Point(((134.3333) * transformX) + moveX, ((463.0000) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((463.0000) * transformY) + moveY),

        new createjs.Point(((134.0000) * transformX) + moveX, ((463.6666) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((464.3334) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((465.0000) * transformY) + moveY),

        new createjs.Point(((133.6667) * transformX) + moveX, ((465.0000) * transformY) + moveY),
        new createjs.Point(((133.3333) * transformX) + moveX, ((465.0000) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((465.0000) * transformY) + moveY),

        new createjs.Point(((133.0000) * transformX) + moveX, ((465.9999) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((467.0001) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((468.0000) * transformY) + moveY),

        new createjs.Point(((132.6667) * transformX) + moveX, ((468.0000) * transformY) + moveY),
        new createjs.Point(((132.3333) * transformX) + moveX, ((468.0000) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((468.0000) * transformY) + moveY),

        new createjs.Point(((132.0000) * transformX) + moveX, ((468.6666) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((469.3334) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((470.0000) * transformY) + moveY),

        new createjs.Point(((131.6667) * transformX) + moveX, ((470.0000) * transformY) + moveY),
        new createjs.Point(((131.3333) * transformX) + moveX, ((470.0000) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((470.0000) * transformY) + moveY),

        new createjs.Point(((131.0000) * transformX) + moveX, ((470.9999) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((472.0001) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((473.0000) * transformY) + moveY),

        new createjs.Point(((130.6667) * transformX) + moveX, ((473.0000) * transformY) + moveY),
        new createjs.Point(((130.3333) * transformX) + moveX, ((473.0000) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((473.0000) * transformY) + moveY),

        new createjs.Point(((130.0000) * transformX) + moveX, ((473.6666) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((474.3334) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((475.0000) * transformY) + moveY),

        new createjs.Point(((129.6667) * transformX) + moveX, ((475.0000) * transformY) + moveY),
        new createjs.Point(((129.3333) * transformX) + moveX, ((475.0000) * transformY) + moveY),
        new createjs.Point(((129.0000) * transformX) + moveX, ((475.0000) * transformY) + moveY),

        new createjs.Point(((128.6667) * transformX) + moveX, ((476.9998) * transformY) + moveY),
        new createjs.Point(((128.3333) * transformX) + moveX, ((479.0002) * transformY) + moveY),
        new createjs.Point(((128.0000) * transformX) + moveX, ((481.0000) * transformY) + moveY),

        new createjs.Point(((127.6667) * transformX) + moveX, ((481.0000) * transformY) + moveY),
        new createjs.Point(((127.3333) * transformX) + moveX, ((481.0000) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((481.0000) * transformY) + moveY),

        new createjs.Point(((127.0000) * transformX) + moveX, ((481.6666) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((482.3334) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((483.0000) * transformY) + moveY),

        new createjs.Point(((126.6667) * transformX) + moveX, ((483.0000) * transformY) + moveY),
        new createjs.Point(((126.3333) * transformX) + moveX, ((483.0000) * transformY) + moveY),
        new createjs.Point(((126.0000) * transformX) + moveX, ((483.0000) * transformY) + moveY),

        new createjs.Point(((125.6667) * transformX) + moveX, ((484.9998) * transformY) + moveY),
        new createjs.Point(((125.3333) * transformX) + moveX, ((487.0002) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((124.6667) * transformX) + moveX, ((489.0000) * transformY) + moveY),
        new createjs.Point(((124.3333) * transformX) + moveX, ((489.0000) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((124.0000) * transformX) + moveX, ((489.6666) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((490.3334) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((123.6667) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((123.3333) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((123.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((122.6667) * transformX) + moveX, ((492.9998) * transformY) + moveY),
        new createjs.Point(((122.3333) * transformX) + moveX, ((495.0002) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((497.0000) * transformY) + moveY),

        new createjs.Point(((121.6667) * transformX) + moveX, ((497.0000) * transformY) + moveY),
        new createjs.Point(((121.3333) * transformX) + moveX, ((497.0000) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((497.0000) * transformY) + moveY),

        new createjs.Point(((121.0000) * transformX) + moveX, ((497.6666) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((498.3334) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),

        new createjs.Point(((120.6667) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((120.3333) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),

        new createjs.Point(((119.6667) * transformX) + moveX, ((500.9998) * transformY) + moveY),
        new createjs.Point(((119.3333) * transformX) + moveX, ((503.0002) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((505.0000) * transformY) + moveY),

        new createjs.Point(((118.6667) * transformX) + moveX, ((505.0000) * transformY) + moveY),
        new createjs.Point(((118.3333) * transformX) + moveX, ((505.0000) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((505.0000) * transformY) + moveY),

        new createjs.Point(((118.0000) * transformX) + moveX, ((505.9999) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((507.0001) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((117.6667) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((117.3333) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((117.0000) * transformX) + moveX, ((508.6666) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((509.3334) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((116.6667) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((116.3333) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((116.0000) * transformX) + moveX, ((510.9999) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((512.0001) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((513.0000) * transformY) + moveY),

        new createjs.Point(((115.6667) * transformX) + moveX, ((513.0000) * transformY) + moveY),
        new createjs.Point(((115.3333) * transformX) + moveX, ((513.0000) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((513.0000) * transformY) + moveY),

        new createjs.Point(((115.0000) * transformX) + moveX, ((513.9999) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((515.0001) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((114.6667) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((114.3333) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((114.0000) * transformX) + moveX, ((516.9999) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((518.0001) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((519.0000) * transformY) + moveY),

        new createjs.Point(((113.6667) * transformX) + moveX, ((519.0000) * transformY) + moveY),
        new createjs.Point(((113.3333) * transformX) + moveX, ((519.0000) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((519.0000) * transformY) + moveY),

        new createjs.Point(((113.0000) * transformX) + moveX, ((519.9999) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((521.0001) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((522.0000) * transformY) + moveY),

        new createjs.Point(((112.6667) * transformX) + moveX, ((522.0000) * transformY) + moveY),
        new createjs.Point(((112.3333) * transformX) + moveX, ((522.0000) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((522.0000) * transformY) + moveY),

        new createjs.Point(((112.0000) * transformX) + moveX, ((522.9999) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((524.0001) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((525.0000) * transformY) + moveY),

        new createjs.Point(((111.6667) * transformX) + moveX, ((525.0000) * transformY) + moveY),
        new createjs.Point(((111.3333) * transformX) + moveX, ((525.0000) * transformY) + moveY),
        new createjs.Point(((111.0000) * transformX) + moveX, ((525.0000) * transformY) + moveY),

        new createjs.Point(((111.0000) * transformX) + moveX, ((525.6666) * transformY) + moveY),
        new createjs.Point(((111.0000) * transformX) + moveX, ((526.3334) * transformY) + moveY),
        new createjs.Point(((111.0000) * transformX) + moveX, ((527.0000) * transformY) + moveY),

        new createjs.Point(((110.6667) * transformX) + moveX, ((527.0000) * transformY) + moveY),
        new createjs.Point(((110.3333) * transformX) + moveX, ((527.0000) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((527.0000) * transformY) + moveY),

        new createjs.Point(((109.3334) * transformX) + moveX, ((529.9997) * transformY) + moveY),
        new createjs.Point(((108.6666) * transformX) + moveX, ((533.0003) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((536.0000) * transformY) + moveY),

        new createjs.Point(((107.6667) * transformX) + moveX, ((536.0000) * transformY) + moveY),
        new createjs.Point(((107.3333) * transformX) + moveX, ((536.0000) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((536.0000) * transformY) + moveY),

        new createjs.Point(((107.0000) * transformX) + moveX, ((536.9999) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((538.0001) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((539.0000) * transformY) + moveY),

        new createjs.Point(((106.6667) * transformX) + moveX, ((539.0000) * transformY) + moveY),
        new createjs.Point(((106.3333) * transformX) + moveX, ((539.0000) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((539.0000) * transformY) + moveY),

        new createjs.Point(((106.0000) * transformX) + moveX, ((539.9999) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((541.0001) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((542.0000) * transformY) + moveY),

        new createjs.Point(((105.6667) * transformX) + moveX, ((542.0000) * transformY) + moveY),
        new createjs.Point(((105.3333) * transformX) + moveX, ((542.0000) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((542.0000) * transformY) + moveY),

        new createjs.Point(((105.0000) * transformX) + moveX, ((542.9999) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((544.0001) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((545.0000) * transformY) + moveY),

        new createjs.Point(((104.6667) * transformX) + moveX, ((545.0000) * transformY) + moveY),
        new createjs.Point(((104.3333) * transformX) + moveX, ((545.0000) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((545.0000) * transformY) + moveY),

        new createjs.Point(((104.0000) * transformX) + moveX, ((545.9999) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((547.0001) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((548.0000) * transformY) + moveY),

        new createjs.Point(((103.6667) * transformX) + moveX, ((548.0000) * transformY) + moveY),
        new createjs.Point(((103.3333) * transformX) + moveX, ((548.0000) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((548.0000) * transformY) + moveY),

        new createjs.Point(((103.0000) * transformX) + moveX, ((548.9999) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((550.0001) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((551.0000) * transformY) + moveY),

        new createjs.Point(((102.6667) * transformX) + moveX, ((551.0000) * transformY) + moveY),
        new createjs.Point(((102.3333) * transformX) + moveX, ((551.0000) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((551.0000) * transformY) + moveY),

        new createjs.Point(((102.0000) * transformX) + moveX, ((551.9999) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((553.0001) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((554.0000) * transformY) + moveY),

        new createjs.Point(((101.6667) * transformX) + moveX, ((554.0000) * transformY) + moveY),
        new createjs.Point(((101.3333) * transformX) + moveX, ((554.0000) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((554.0000) * transformY) + moveY),

        new createjs.Point(((101.0000) * transformX) + moveX, ((555.3332) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((556.6668) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((558.0000) * transformY) + moveY),

        new createjs.Point(((100.6667) * transformX) + moveX, ((558.0000) * transformY) + moveY),
        new createjs.Point(((100.3333) * transformX) + moveX, ((558.0000) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((558.0000) * transformY) + moveY),

        new createjs.Point(((100.0000) * transformX) + moveX, ((558.9999) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((560.0001) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((561.0000) * transformY) + moveY),

        new createjs.Point(((99.6667) * transformX) + moveX, ((561.0000) * transformY) + moveY),
        new createjs.Point(((99.3333) * transformX) + moveX, ((561.0000) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((561.0000) * transformY) + moveY),

        new createjs.Point(((99.0000) * transformX) + moveX, ((561.9999) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((563.0001) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((564.0000) * transformY) + moveY),

        new createjs.Point(((98.6667) * transformX) + moveX, ((564.0000) * transformY) + moveY),
        new createjs.Point(((98.3333) * transformX) + moveX, ((564.0000) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((564.0000) * transformY) + moveY),

        new createjs.Point(((98.0000) * transformX) + moveX, ((564.9999) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((566.0001) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((567.0000) * transformY) + moveY),

        new createjs.Point(((97.6667) * transformX) + moveX, ((567.0000) * transformY) + moveY),
        new createjs.Point(((97.3333) * transformX) + moveX, ((567.0000) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((567.0000) * transformY) + moveY),

        new createjs.Point(((97.0000) * transformX) + moveX, ((567.9999) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((569.0001) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((570.0000) * transformY) + moveY),

        new createjs.Point(((96.6667) * transformX) + moveX, ((570.0000) * transformY) + moveY),
        new createjs.Point(((96.3333) * transformX) + moveX, ((570.0000) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((570.0000) * transformY) + moveY),

        new createjs.Point(((96.0000) * transformX) + moveX, ((570.9999) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((572.0001) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((573.0000) * transformY) + moveY),

        new createjs.Point(((95.6667) * transformX) + moveX, ((573.0000) * transformY) + moveY),
        new createjs.Point(((95.3333) * transformX) + moveX, ((573.0000) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((573.0000) * transformY) + moveY),

        new createjs.Point(((94.0001) * transformX) + moveX, ((577.3329) * transformY) + moveY),
        new createjs.Point(((92.9999) * transformX) + moveX, ((581.6671) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((586.0000) * transformY) + moveY),

        new createjs.Point(((91.6667) * transformX) + moveX, ((586.0000) * transformY) + moveY),
        new createjs.Point(((91.3333) * transformX) + moveX, ((586.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((586.0000) * transformY) + moveY),

        new createjs.Point(((90.3334) * transformX) + moveX, ((589.3330) * transformY) + moveY),
        new createjs.Point(((89.6666) * transformX) + moveX, ((592.6670) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((596.0000) * transformY) + moveY),

        new createjs.Point(((88.6667) * transformX) + moveX, ((596.0000) * transformY) + moveY),
        new createjs.Point(((88.3333) * transformX) + moveX, ((596.0000) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((596.0000) * transformY) + moveY),

        new createjs.Point(((86.3335) * transformX) + moveX, ((602.9993) * transformY) + moveY),
        new createjs.Point(((84.6665) * transformX) + moveX, ((610.0007) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((617.0000) * transformY) + moveY),

        new createjs.Point(((82.6667) * transformX) + moveX, ((617.0000) * transformY) + moveY),
        new createjs.Point(((82.3333) * transformX) + moveX, ((617.0000) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((617.0000) * transformY) + moveY),

        new createjs.Point(((82.0000) * transformX) + moveX, ((618.3332) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((619.6668) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),

        new createjs.Point(((81.6667) * transformX) + moveX, ((621.0000) * transformY) + moveY),
        new createjs.Point(((81.3333) * transformX) + moveX, ((621.0000) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),

        new createjs.Point(((81.0000) * transformX) + moveX, ((621.9999) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((623.0001) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((624.0000) * transformY) + moveY),

        new createjs.Point(((80.6667) * transformX) + moveX, ((624.0000) * transformY) + moveY),
        new createjs.Point(((80.3333) * transformX) + moveX, ((624.0000) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((624.0000) * transformY) + moveY),

        new createjs.Point(((79.3334) * transformX) + moveX, ((627.6663) * transformY) + moveY),
        new createjs.Point(((78.6666) * transformX) + moveX, ((631.3337) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((635.0000) * transformY) + moveY),

        new createjs.Point(((77.6667) * transformX) + moveX, ((635.0000) * transformY) + moveY),
        new createjs.Point(((77.3333) * transformX) + moveX, ((635.0000) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((635.0000) * transformY) + moveY),

        new createjs.Point(((76.3334) * transformX) + moveX, ((638.6663) * transformY) + moveY),
        new createjs.Point(((75.6666) * transformX) + moveX, ((642.3337) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((646.0000) * transformY) + moveY),

        new createjs.Point(((74.6667) * transformX) + moveX, ((646.0000) * transformY) + moveY),
        new createjs.Point(((74.3333) * transformX) + moveX, ((646.0000) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((646.0000) * transformY) + moveY),

        new createjs.Point(((74.0000) * transformX) + moveX, ((647.3332) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((648.6668) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((650.0000) * transformY) + moveY),

        new createjs.Point(((73.6667) * transformX) + moveX, ((650.0000) * transformY) + moveY),
        new createjs.Point(((73.3333) * transformX) + moveX, ((650.0000) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((650.0000) * transformY) + moveY),

        new createjs.Point(((72.6667) * transformX) + moveX, ((652.6664) * transformY) + moveY),
        new createjs.Point(((72.3333) * transformX) + moveX, ((655.3336) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((658.0000) * transformY) + moveY),

        new createjs.Point(((68.6086) * transformX) + moveX, ((667.9079) * transformY) + moveY),
        new createjs.Point(((66.3681) * transformX) + moveX, ((680.0844) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((690.0000) * transformY) + moveY),

        new createjs.Point(((62.6667) * transformX) + moveX, ((692.9997) * transformY) + moveY),
        new createjs.Point(((62.3333) * transformX) + moveX, ((696.0003) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((699.0000) * transformY) + moveY),

        new createjs.Point(((56.5439) * transformX) + moveX, ((715.7894) * transformY) + moveY),
        new createjs.Point(((54.3471) * transformX) + moveX, ((735.9313) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((753.0000) * transformY) + moveY),

        new createjs.Point(((49.0000) * transformX) + moveX, ((754.6665) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((756.3335) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((758.0000) * transformY) + moveY),

        new createjs.Point(((47.1363) * transformX) + moveX, ((763.9212) * transformY) + moveY),
        new createjs.Point(((45.8064) * transformX) + moveX, ((772.0582) * transformY) + moveY),
        new createjs.Point(((44.0000) * transformX) + moveX, ((778.0000) * transformY) + moveY),

        new createjs.Point(((44.0000) * transformX) + moveX, ((779.9998) * transformY) + moveY),
        new createjs.Point(((44.0000) * transformX) + moveX, ((782.0002) * transformY) + moveY),
        new createjs.Point(((44.0000) * transformX) + moveX, ((784.0000) * transformY) + moveY),

        new createjs.Point(((42.1224) * transformX) + moveX, ((790.1387) * transformY) + moveY),
        new createjs.Point(((40.8806) * transformX) + moveX, ((798.8054) * transformY) + moveY),
        new createjs.Point(((39.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),

        new createjs.Point(((37.3335) * transformX) + moveX, ((816.9988) * transformY) + moveY),
        new createjs.Point(((35.6665) * transformX) + moveX, ((829.0012) * transformY) + moveY),
        new createjs.Point(((34.0000) * transformX) + moveX, ((841.0000) * transformY) + moveY),

        new createjs.Point(((29.8641) * transformX) + moveX, ((854.9730) * transformY) + moveY),
        new createjs.Point(((30.1388) * transformX) + moveX, ((872.8205) * transformY) + moveY),
        new createjs.Point(((26.0000) * transformX) + moveX, ((887.0000) * transformY) + moveY),

        new createjs.Point(((26.0000) * transformX) + moveX, ((889.3331) * transformY) + moveY),
        new createjs.Point(((26.0000) * transformX) + moveX, ((891.6669) * transformY) + moveY),
        new createjs.Point(((26.0000) * transformX) + moveX, ((894.0000) * transformY) + moveY),

        new createjs.Point(((25.6667) * transformX) + moveX, ((894.0000) * transformY) + moveY),
        new createjs.Point(((25.3333) * transformX) + moveX, ((894.0000) * transformY) + moveY),
        new createjs.Point(((25.0000) * transformX) + moveX, ((894.0000) * transformY) + moveY),

        new createjs.Point(((25.0000) * transformX) + moveX, ((896.6664) * transformY) + moveY),
        new createjs.Point(((25.0000) * transformX) + moveX, ((899.3336) * transformY) + moveY),
        new createjs.Point(((25.0000) * transformX) + moveX, ((902.0000) * transformY) + moveY),

        new createjs.Point(((24.6667) * transformX) + moveX, ((902.0000) * transformY) + moveY),
        new createjs.Point(((24.3333) * transformX) + moveX, ((902.0000) * transformY) + moveY),
        new createjs.Point(((24.0000) * transformX) + moveX, ((902.0000) * transformY) + moveY),

        new createjs.Point(((24.0000) * transformX) + moveX, ((904.3331) * transformY) + moveY),
        new createjs.Point(((24.0000) * transformX) + moveX, ((906.6669) * transformY) + moveY),
        new createjs.Point(((24.0000) * transformX) + moveX, ((909.0000) * transformY) + moveY),

        new createjs.Point(((23.6667) * transformX) + moveX, ((909.0000) * transformY) + moveY),
        new createjs.Point(((23.3333) * transformX) + moveX, ((909.0000) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((909.0000) * transformY) + moveY),

        new createjs.Point(((23.0000) * transformX) + moveX, ((911.6664) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((914.3336) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),

        new createjs.Point(((22.6667) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((22.3333) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),

        new createjs.Point(((22.0000) * transformX) + moveX, ((919.6664) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((922.3336) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((925.0000) * transformY) + moveY),

        new createjs.Point(((21.6667) * transformX) + moveX, ((925.0000) * transformY) + moveY),
        new createjs.Point(((21.3333) * transformX) + moveX, ((925.0000) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((925.0000) * transformY) + moveY),

        new createjs.Point(((21.0000) * transformX) + moveX, ((927.9997) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((931.0003) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((934.0000) * transformY) + moveY),

        new createjs.Point(((20.6667) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((20.3333) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((934.0000) * transformY) + moveY),

        new createjs.Point(((20.0000) * transformX) + moveX, ((936.6664) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((939.3336) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((942.0000) * transformY) + moveY),

        new createjs.Point(((19.6667) * transformX) + moveX, ((942.0000) * transformY) + moveY),
        new createjs.Point(((19.3333) * transformX) + moveX, ((942.0000) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((942.0000) * transformY) + moveY),

        new createjs.Point(((19.0000) * transformX) + moveX, ((944.9997) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((948.0003) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((951.0000) * transformY) + moveY),

        new createjs.Point(((18.6667) * transformX) + moveX, ((951.0000) * transformY) + moveY),
        new createjs.Point(((18.3333) * transformX) + moveX, ((951.0000) * transformY) + moveY),
        new createjs.Point(((18.0000) * transformX) + moveX, ((951.0000) * transformY) + moveY),

        new createjs.Point(((17.0001) * transformX) + moveX, ((964.6653) * transformY) + moveY),
        new createjs.Point(((15.9999) * transformX) + moveX, ((978.3347) * transformY) + moveY),
        new createjs.Point(((15.0000) * transformX) + moveX, ((992.0000) * transformY) + moveY),

        new createjs.Point(((14.6667) * transformX) + moveX, ((992.0000) * transformY) + moveY),
        new createjs.Point(((14.3333) * transformX) + moveX, ((992.0000) * transformY) + moveY),
        new createjs.Point(((14.0000) * transformX) + moveX, ((992.0000) * transformY) + moveY),

        new createjs.Point(((14.0000) * transformX) + moveX, ((995.6663) * transformY) + moveY),
        new createjs.Point(((14.0000) * transformX) + moveX, ((999.3337) * transformY) + moveY),
        new createjs.Point(((14.0000) * transformX) + moveX, ((1003.0000) * transformY) + moveY),

        new createjs.Point(((13.6667) * transformX) + moveX, ((1003.0000) * transformY) + moveY),
        new createjs.Point(((13.3333) * transformX) + moveX, ((1003.0000) * transformY) + moveY),
        new createjs.Point(((13.0000) * transformX) + moveX, ((1003.0000) * transformY) + moveY),

        new createjs.Point(((13.0000) * transformX) + moveX, ((1006.9996) * transformY) + moveY),
        new createjs.Point(((13.0000) * transformX) + moveX, ((1011.0004) * transformY) + moveY),
        new createjs.Point(((13.0000) * transformX) + moveX, ((1015.0000) * transformY) + moveY),

        new createjs.Point(((12.6667) * transformX) + moveX, ((1015.0000) * transformY) + moveY),
        new createjs.Point(((12.3333) * transformX) + moveX, ((1015.0000) * transformY) + moveY),
        new createjs.Point(((12.0000) * transformX) + moveX, ((1015.0000) * transformY) + moveY),

        new createjs.Point(((12.0000) * transformX) + moveX, ((1019.6662) * transformY) + moveY),
        new createjs.Point(((12.0000) * transformX) + moveX, ((1024.3338) * transformY) + moveY),
        new createjs.Point(((12.0000) * transformX) + moveX, ((1029.0000) * transformY) + moveY),

        new createjs.Point(((11.6667) * transformX) + moveX, ((1029.0000) * transformY) + moveY),
        new createjs.Point(((11.3333) * transformX) + moveX, ((1029.0000) * transformY) + moveY),
        new createjs.Point(((11.0000) * transformX) + moveX, ((1029.0000) * transformY) + moveY),

        new createjs.Point(((11.0000) * transformX) + moveX, ((1033.6662) * transformY) + moveY),
        new createjs.Point(((11.0000) * transformX) + moveX, ((1038.3338) * transformY) + moveY),
        new createjs.Point(((11.0000) * transformX) + moveX, ((1043.0000) * transformY) + moveY),

        new createjs.Point(((10.6667) * transformX) + moveX, ((1043.0000) * transformY) + moveY),
        new createjs.Point(((10.3333) * transformX) + moveX, ((1043.0000) * transformY) + moveY),
        new createjs.Point(((10.0000) * transformX) + moveX, ((1043.0000) * transformY) + moveY),

        new createjs.Point(((9.6667) * transformX) + moveX, ((1051.3325) * transformY) + moveY),
        new createjs.Point(((9.3333) * transformX) + moveX, ((1059.6675) * transformY) + moveY),
        new createjs.Point(((9.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),

        new createjs.Point(((9.0000) * transformX) + moveX, ((1071.6663) * transformY) + moveY),
        new createjs.Point(((9.0000) * transformX) + moveX, ((1075.3337) * transformY) + moveY),
        new createjs.Point(((9.0000) * transformX) + moveX, ((1079.0000) * transformY) + moveY),

        new createjs.Point(((8.6667) * transformX) + moveX, ((1079.0000) * transformY) + moveY),
        new createjs.Point(((8.3333) * transformX) + moveX, ((1079.0000) * transformY) + moveY),
        new createjs.Point(((8.0000) * transformX) + moveX, ((1079.0000) * transformY) + moveY),

        new createjs.Point(((8.0000) * transformX) + moveX, ((1087.3325) * transformY) + moveY),
        new createjs.Point(((8.0000) * transformX) + moveX, ((1095.6675) * transformY) + moveY),
        new createjs.Point(((8.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),

        new createjs.Point(((7.6667) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((7.3333) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((7.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),

        new createjs.Point(((7.0000) * transformX) + moveX, ((1122.9981) * transformY) + moveY),
        new createjs.Point(((7.0000) * transformX) + moveX, ((1142.0019) * transformY) + moveY),
        new createjs.Point(((7.0000) * transformX) + moveX, ((1161.0000) * transformY) + moveY),

        new createjs.Point(((7.0000) * transformX) + moveX, ((1177.6650) * transformY) + moveY),
        new createjs.Point(((7.0000) * transformX) + moveX, ((1194.3350) * transformY) + moveY),
        new createjs.Point(((7.0000) * transformX) + moveX, ((1211.0000) * transformY) + moveY),

        new createjs.Point(((7.3333) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((7.6667) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((8.0000) * transformX) + moveX, ((1211.0000) * transformY) + moveY),

        new createjs.Point(((8.0000) * transformX) + moveX, ((1214.6663) * transformY) + moveY),
        new createjs.Point(((8.0000) * transformX) + moveX, ((1218.3337) * transformY) + moveY),
        new createjs.Point(((8.0000) * transformX) + moveX, ((1222.0000) * transformY) + moveY),

        new createjs.Point(((8.3333) * transformX) + moveX, ((1230.6658) * transformY) + moveY),
        new createjs.Point(((8.6667) * transformX) + moveX, ((1239.3342) * transformY) + moveY),
        new createjs.Point(((9.0000) * transformX) + moveX, ((1248.0000) * transformY) + moveY),

        new createjs.Point(((9.3333) * transformX) + moveX, ((1248.0000) * transformY) + moveY),
        new createjs.Point(((9.6667) * transformX) + moveX, ((1248.0000) * transformY) + moveY),
        new createjs.Point(((10.0000) * transformX) + moveX, ((1248.0000) * transformY) + moveY),

        new createjs.Point(((10.0000) * transformX) + moveX, ((1252.3329) * transformY) + moveY),
        new createjs.Point(((10.0000) * transformX) + moveX, ((1256.6671) * transformY) + moveY),
        new createjs.Point(((10.0000) * transformX) + moveX, ((1261.0000) * transformY) + moveY),

        new createjs.Point(((10.3333) * transformX) + moveX, ((1261.0000) * transformY) + moveY),
        new createjs.Point(((10.6667) * transformX) + moveX, ((1261.0000) * transformY) + moveY),
        new createjs.Point(((11.0000) * transformX) + moveX, ((1261.0000) * transformY) + moveY),

        new createjs.Point(((11.0000) * transformX) + moveX, ((1264.9996) * transformY) + moveY),
        new createjs.Point(((11.0000) * transformX) + moveX, ((1269.0004) * transformY) + moveY),
        new createjs.Point(((11.0000) * transformX) + moveX, ((1273.0000) * transformY) + moveY),

        new createjs.Point(((11.3333) * transformX) + moveX, ((1273.0000) * transformY) + moveY),
        new createjs.Point(((11.6667) * transformX) + moveX, ((1273.0000) * transformY) + moveY),
        new createjs.Point(((12.0000) * transformX) + moveX, ((1273.0000) * transformY) + moveY),

        new createjs.Point(((12.0000) * transformX) + moveX, ((1276.6663) * transformY) + moveY),
        new createjs.Point(((12.0000) * transformX) + moveX, ((1280.3337) * transformY) + moveY),
        new createjs.Point(((12.0000) * transformX) + moveX, ((1284.0000) * transformY) + moveY),

        new createjs.Point(((12.3333) * transformX) + moveX, ((1284.0000) * transformY) + moveY),
        new createjs.Point(((12.6667) * transformX) + moveX, ((1284.0000) * transformY) + moveY),
        new createjs.Point(((13.0000) * transformX) + moveX, ((1284.0000) * transformY) + moveY),

        new createjs.Point(((13.3333) * transformX) + moveX, ((1290.6660) * transformY) + moveY),
        new createjs.Point(((13.6667) * transformX) + moveX, ((1297.3340) * transformY) + moveY),
        new createjs.Point(((14.0000) * transformX) + moveX, ((1304.0000) * transformY) + moveY),

        new createjs.Point(((17.3977) * transformX) + moveX, ((1315.7460) * transformY) + moveY),
        new createjs.Point(((16.5971) * transformX) + moveX, ((1331.3511) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((1343.0000) * transformY) + moveY),

        new createjs.Point(((20.9999) * transformX) + moveX, ((1351.6658) * transformY) + moveY),
        new createjs.Point(((22.0001) * transformX) + moveX, ((1360.3342) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((1369.0000) * transformY) + moveY),

        new createjs.Point(((27.8820) * transformX) + moveX, ((1385.3689) * transformY) + moveY),
        new createjs.Point(((28.8969) * transformX) + moveX, ((1405.1479) * transformY) + moveY),
        new createjs.Point(((34.0000) * transformX) + moveX, ((1421.0000) * transformY) + moveY),

        new createjs.Point(((34.0000) * transformX) + moveX, ((1422.6665) * transformY) + moveY),
        new createjs.Point(((34.0000) * transformX) + moveX, ((1424.3335) * transformY) + moveY),
        new createjs.Point(((34.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),

        new createjs.Point(((34.3333) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((34.6667) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((35.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),

        new createjs.Point(((35.0000) * transformX) + moveX, ((1427.6665) * transformY) + moveY),
        new createjs.Point(((35.0000) * transformX) + moveX, ((1429.3335) * transformY) + moveY),
        new createjs.Point(((35.0000) * transformX) + moveX, ((1431.0000) * transformY) + moveY),

        new createjs.Point(((35.3333) * transformX) + moveX, ((1431.0000) * transformY) + moveY),
        new createjs.Point(((35.6667) * transformX) + moveX, ((1431.0000) * transformY) + moveY),
        new createjs.Point(((36.0000) * transformX) + moveX, ((1431.0000) * transformY) + moveY),

        new createjs.Point(((36.0000) * transformX) + moveX, ((1432.3332) * transformY) + moveY),
        new createjs.Point(((36.0000) * transformX) + moveX, ((1433.6668) * transformY) + moveY),
        new createjs.Point(((36.0000) * transformX) + moveX, ((1435.0000) * transformY) + moveY),

        new createjs.Point(((36.3333) * transformX) + moveX, ((1435.0000) * transformY) + moveY),
        new createjs.Point(((36.6667) * transformX) + moveX, ((1435.0000) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1435.0000) * transformY) + moveY),

        new createjs.Point(((37.0000) * transformX) + moveX, ((1436.3332) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1437.6668) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1439.0000) * transformY) + moveY),

        new createjs.Point(((37.3333) * transformX) + moveX, ((1439.0000) * transformY) + moveY),
        new createjs.Point(((37.6667) * transformX) + moveX, ((1439.0000) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1439.0000) * transformY) + moveY),

        new createjs.Point(((38.0000) * transformX) + moveX, ((1440.6665) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1442.3335) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1444.0000) * transformY) + moveY),

        new createjs.Point(((45.3499) * transformX) + moveX, ((1466.5376) * transformY) + moveY),
        new createjs.Point(((49.5771) * transformX) + moveX, ((1491.7092) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1513.0000) * transformY) + moveY),

        new createjs.Point(((58.3333) * transformX) + moveX, ((1515.3331) * transformY) + moveY),
        new createjs.Point(((58.6667) * transformX) + moveX, ((1517.6669) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1520.0000) * transformY) + moveY),

        new createjs.Point(((59.3333) * transformX) + moveX, ((1520.0000) * transformY) + moveY),
        new createjs.Point(((59.6667) * transformX) + moveX, ((1520.0000) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1520.0000) * transformY) + moveY),

        new createjs.Point(((60.0000) * transformX) + moveX, ((1520.9999) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1522.0001) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1523.0000) * transformY) + moveY),

        new createjs.Point(((60.3333) * transformX) + moveX, ((1523.0000) * transformY) + moveY),
        new createjs.Point(((60.6667) * transformX) + moveX, ((1523.0000) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1523.0000) * transformY) + moveY),

        new createjs.Point(((61.0000) * transformX) + moveX, ((1523.9999) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1525.0001) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1526.0000) * transformY) + moveY),

        new createjs.Point(((61.3333) * transformX) + moveX, ((1526.0000) * transformY) + moveY),
        new createjs.Point(((61.6667) * transformX) + moveX, ((1526.0000) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1526.0000) * transformY) + moveY),

        new createjs.Point(((62.0000) * transformX) + moveX, ((1526.9999) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1528.0001) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1529.0000) * transformY) + moveY),

        new createjs.Point(((62.3333) * transformX) + moveX, ((1529.0000) * transformY) + moveY),
        new createjs.Point(((62.6667) * transformX) + moveX, ((1529.0000) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1529.0000) * transformY) + moveY),

        new createjs.Point(((63.0000) * transformX) + moveX, ((1529.9999) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1531.0001) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1532.0000) * transformY) + moveY),

        new createjs.Point(((63.3333) * transformX) + moveX, ((1532.0000) * transformY) + moveY),
        new createjs.Point(((63.6667) * transformX) + moveX, ((1532.0000) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1532.0000) * transformY) + moveY),

        new createjs.Point(((64.3333) * transformX) + moveX, ((1533.9998) * transformY) + moveY),
        new createjs.Point(((64.6667) * transformX) + moveX, ((1536.0002) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1538.0000) * transformY) + moveY),

        new createjs.Point(((65.3333) * transformX) + moveX, ((1538.0000) * transformY) + moveY),
        new createjs.Point(((65.6667) * transformX) + moveX, ((1538.0000) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1538.0000) * transformY) + moveY),

        new createjs.Point(((66.0000) * transformX) + moveX, ((1538.6666) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1539.3334) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1540.0000) * transformY) + moveY),

        new createjs.Point(((66.3333) * transformX) + moveX, ((1540.0000) * transformY) + moveY),
        new createjs.Point(((66.6667) * transformX) + moveX, ((1540.0000) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1540.0000) * transformY) + moveY),

        new createjs.Point(((67.0000) * transformX) + moveX, ((1540.9999) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1542.0001) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1543.0000) * transformY) + moveY),

        new createjs.Point(((67.3333) * transformX) + moveX, ((1543.0000) * transformY) + moveY),
        new createjs.Point(((67.6667) * transformX) + moveX, ((1543.0000) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1543.0000) * transformY) + moveY),

        new createjs.Point(((68.0000) * transformX) + moveX, ((1543.9999) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1545.0001) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1546.0000) * transformY) + moveY),

        new createjs.Point(((68.3333) * transformX) + moveX, ((1546.0000) * transformY) + moveY),
        new createjs.Point(((68.6667) * transformX) + moveX, ((1546.0000) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((1546.0000) * transformY) + moveY),

        new createjs.Point(((69.0000) * transformX) + moveX, ((1546.9999) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((1548.0001) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((1549.0000) * transformY) + moveY),

        new createjs.Point(((69.3333) * transformX) + moveX, ((1549.0000) * transformY) + moveY),
        new createjs.Point(((69.6667) * transformX) + moveX, ((1549.0000) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1549.0000) * transformY) + moveY),

        new createjs.Point(((70.0000) * transformX) + moveX, ((1549.9999) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1551.0001) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1552.0000) * transformY) + moveY),

        new createjs.Point(((70.3333) * transformX) + moveX, ((1552.0000) * transformY) + moveY),
        new createjs.Point(((70.6667) * transformX) + moveX, ((1552.0000) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((1552.0000) * transformY) + moveY),

        new createjs.Point(((71.0000) * transformX) + moveX, ((1552.6666) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((1553.3334) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((1554.0000) * transformY) + moveY),

        new createjs.Point(((71.3333) * transformX) + moveX, ((1554.0000) * transformY) + moveY),
        new createjs.Point(((71.6667) * transformX) + moveX, ((1554.0000) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1554.0000) * transformY) + moveY),

        new createjs.Point(((72.3333) * transformX) + moveX, ((1555.9998) * transformY) + moveY),
        new createjs.Point(((72.6667) * transformX) + moveX, ((1558.0002) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),

        new createjs.Point(((73.3333) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((73.6667) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),

        new createjs.Point(((74.0000) * transformX) + moveX, ((1560.6666) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1561.3334) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1562.0000) * transformY) + moveY),

        new createjs.Point(((74.3333) * transformX) + moveX, ((1562.0000) * transformY) + moveY),
        new createjs.Point(((74.6667) * transformX) + moveX, ((1562.0000) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1562.0000) * transformY) + moveY),

        new createjs.Point(((75.0000) * transformX) + moveX, ((1562.9999) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1564.0001) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1565.0000) * transformY) + moveY),

        new createjs.Point(((75.3333) * transformX) + moveX, ((1565.0000) * transformY) + moveY),
        new createjs.Point(((75.6667) * transformX) + moveX, ((1565.0000) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1565.0000) * transformY) + moveY),

        new createjs.Point(((76.0000) * transformX) + moveX, ((1565.6666) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1566.3334) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1567.0000) * transformY) + moveY),

        new createjs.Point(((76.3333) * transformX) + moveX, ((1567.0000) * transformY) + moveY),
        new createjs.Point(((76.6667) * transformX) + moveX, ((1567.0000) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1567.0000) * transformY) + moveY),

        new createjs.Point(((77.3333) * transformX) + moveX, ((1568.9998) * transformY) + moveY),
        new createjs.Point(((77.6667) * transformX) + moveX, ((1571.0002) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((1573.0000) * transformY) + moveY),

        new createjs.Point(((78.3333) * transformX) + moveX, ((1573.0000) * transformY) + moveY),
        new createjs.Point(((78.6667) * transformX) + moveX, ((1573.0000) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1573.0000) * transformY) + moveY),

        new createjs.Point(((79.0000) * transformX) + moveX, ((1573.6666) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1574.3334) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1575.0000) * transformY) + moveY),

        new createjs.Point(((79.3333) * transformX) + moveX, ((1575.0000) * transformY) + moveY),
        new createjs.Point(((79.6667) * transformX) + moveX, ((1575.0000) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1575.0000) * transformY) + moveY),

        new createjs.Point(((80.0000) * transformX) + moveX, ((1575.9999) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1577.0001) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),

        new createjs.Point(((80.3333) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((80.6667) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),

        new createjs.Point(((81.3333) * transformX) + moveX, ((1579.3332) * transformY) + moveY),
        new createjs.Point(((81.6667) * transformX) + moveX, ((1580.6668) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1582.0000) * transformY) + moveY),

        new createjs.Point(((82.3333) * transformX) + moveX, ((1582.0000) * transformY) + moveY),
        new createjs.Point(((82.6667) * transformX) + moveX, ((1582.0000) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1582.0000) * transformY) + moveY),

        new createjs.Point(((83.0000) * transformX) + moveX, ((1582.9999) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1584.0001) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1585.0000) * transformY) + moveY),

        new createjs.Point(((83.3333) * transformX) + moveX, ((1585.0000) * transformY) + moveY),
        new createjs.Point(((83.6667) * transformX) + moveX, ((1585.0000) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1585.0000) * transformY) + moveY),

        new createjs.Point(((84.0000) * transformX) + moveX, ((1585.6666) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1586.3334) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1587.0000) * transformY) + moveY),

        new createjs.Point(((84.3333) * transformX) + moveX, ((1587.0000) * transformY) + moveY),
        new createjs.Point(((84.6667) * transformX) + moveX, ((1587.0000) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1587.0000) * transformY) + moveY),

        new createjs.Point(((85.0000) * transformX) + moveX, ((1587.9999) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1589.0001) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1590.0000) * transformY) + moveY),

        new createjs.Point(((85.3333) * transformX) + moveX, ((1590.0000) * transformY) + moveY),
        new createjs.Point(((85.6667) * transformX) + moveX, ((1590.0000) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((1590.0000) * transformY) + moveY),

        new createjs.Point(((86.3333) * transformX) + moveX, ((1591.3332) * transformY) + moveY),
        new createjs.Point(((86.6667) * transformX) + moveX, ((1592.6668) * transformY) + moveY),
        new createjs.Point(((87.0000) * transformX) + moveX, ((1594.0000) * transformY) + moveY),

        new createjs.Point(((87.3333) * transformX) + moveX, ((1594.0000) * transformY) + moveY),
        new createjs.Point(((87.6667) * transformX) + moveX, ((1594.0000) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1594.0000) * transformY) + moveY),

        new createjs.Point(((88.0000) * transformX) + moveX, ((1594.9999) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1596.0001) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1597.0000) * transformY) + moveY),

        new createjs.Point(((88.3333) * transformX) + moveX, ((1597.0000) * transformY) + moveY),
        new createjs.Point(((88.6667) * transformX) + moveX, ((1597.0000) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1597.0000) * transformY) + moveY),

        new createjs.Point(((89.0000) * transformX) + moveX, ((1597.6666) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1598.3334) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1599.0000) * transformY) + moveY),

        new createjs.Point(((89.3333) * transformX) + moveX, ((1599.0000) * transformY) + moveY),
        new createjs.Point(((89.6667) * transformX) + moveX, ((1599.0000) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1599.0000) * transformY) + moveY),

        new createjs.Point(((90.0000) * transformX) + moveX, ((1599.6666) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1600.3334) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1601.0000) * transformY) + moveY),

        new createjs.Point(((90.3333) * transformX) + moveX, ((1601.0000) * transformY) + moveY),
        new createjs.Point(((90.6667) * transformX) + moveX, ((1601.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1601.0000) * transformY) + moveY),

        new createjs.Point(((91.0000) * transformX) + moveX, ((1601.6666) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1602.3334) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),

        new createjs.Point(((91.3333) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((91.6667) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),

        new createjs.Point(((92.0000) * transformX) + moveX, ((1603.9999) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((1605.0001) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((1606.0000) * transformY) + moveY),

        new createjs.Point(((92.3333) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((92.6667) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1606.0000) * transformY) + moveY),

        new createjs.Point(((93.0000) * transformX) + moveX, ((1606.6666) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1607.3334) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1608.0000) * transformY) + moveY),

        new createjs.Point(((93.3333) * transformX) + moveX, ((1608.0000) * transformY) + moveY),
        new createjs.Point(((93.6667) * transformX) + moveX, ((1608.0000) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1608.0000) * transformY) + moveY),

        new createjs.Point(((94.0000) * transformX) + moveX, ((1608.6666) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1609.3334) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1610.0000) * transformY) + moveY),

        new createjs.Point(((94.3333) * transformX) + moveX, ((1610.0000) * transformY) + moveY),
        new createjs.Point(((94.6667) * transformX) + moveX, ((1610.0000) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((1610.0000) * transformY) + moveY),

        new createjs.Point(((95.6666) * transformX) + moveX, ((1611.9998) * transformY) + moveY),
        new createjs.Point(((96.3334) * transformX) + moveX, ((1614.0002) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),

        new createjs.Point(((97.3333) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((97.6667) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),

        new createjs.Point(((98.0000) * transformX) + moveX, ((1616.9999) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1618.0001) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),

        new createjs.Point(((98.3333) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((98.6667) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),

        new createjs.Point(((99.6666) * transformX) + moveX, ((1620.9998) * transformY) + moveY),
        new createjs.Point(((100.3334) * transformX) + moveX, ((1623.0002) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1625.0000) * transformY) + moveY),

        new createjs.Point(((101.3333) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((101.6667) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1625.0000) * transformY) + moveY),

        new createjs.Point(((102.0000) * transformX) + moveX, ((1625.6666) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1626.3334) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1627.0000) * transformY) + moveY),

        new createjs.Point(((102.3333) * transformX) + moveX, ((1627.0000) * transformY) + moveY),
        new createjs.Point(((102.6667) * transformX) + moveX, ((1627.0000) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((1627.0000) * transformY) + moveY),

        new createjs.Point(((103.0000) * transformX) + moveX, ((1627.6666) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((1628.3334) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((1629.0000) * transformY) + moveY),

        new createjs.Point(((103.3333) * transformX) + moveX, ((1629.0000) * transformY) + moveY),
        new createjs.Point(((103.6667) * transformX) + moveX, ((1629.0000) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1629.0000) * transformY) + moveY),

        new createjs.Point(((104.0000) * transformX) + moveX, ((1629.6666) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1630.3334) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),

        new createjs.Point(((104.3333) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((104.6667) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),

        new createjs.Point(((105.0000) * transformX) + moveX, ((1631.6666) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((1632.3334) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((1633.0000) * transformY) + moveY),

        new createjs.Point(((105.3333) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((105.6667) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((1633.0000) * transformY) + moveY),

        new createjs.Point(((106.0000) * transformX) + moveX, ((1633.6666) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((1634.3334) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((1635.0000) * transformY) + moveY),

        new createjs.Point(((106.3333) * transformX) + moveX, ((1635.0000) * transformY) + moveY),
        new createjs.Point(((106.6667) * transformX) + moveX, ((1635.0000) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1635.0000) * transformY) + moveY),

        new createjs.Point(((107.0000) * transformX) + moveX, ((1635.6666) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1636.3334) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1637.0000) * transformY) + moveY),

        new createjs.Point(((107.3333) * transformX) + moveX, ((1637.0000) * transformY) + moveY),
        new createjs.Point(((107.6667) * transformX) + moveX, ((1637.0000) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((1637.0000) * transformY) + moveY),

        new createjs.Point(((108.0000) * transformX) + moveX, ((1637.6666) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((1638.3334) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((1639.0000) * transformY) + moveY),

        new createjs.Point(((108.3333) * transformX) + moveX, ((1639.0000) * transformY) + moveY),
        new createjs.Point(((108.6667) * transformX) + moveX, ((1639.0000) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((1639.0000) * transformY) + moveY),

        new createjs.Point(((109.0000) * transformX) + moveX, ((1639.6666) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((1640.3334) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((1641.0000) * transformY) + moveY),

        new createjs.Point(((109.3333) * transformX) + moveX, ((1641.0000) * transformY) + moveY),
        new createjs.Point(((109.6667) * transformX) + moveX, ((1641.0000) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1641.0000) * transformY) + moveY),

        new createjs.Point(((110.0000) * transformX) + moveX, ((1641.6666) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1642.3334) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1643.0000) * transformY) + moveY),

        new createjs.Point(((110.6666) * transformX) + moveX, ((1643.3333) * transformY) + moveY),
        new createjs.Point(((111.3334) * transformX) + moveX, ((1643.6667) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((1644.0000) * transformY) + moveY),

        new createjs.Point(((112.0000) * transformX) + moveX, ((1644.6666) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((1645.3334) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((1646.0000) * transformY) + moveY),

        new createjs.Point(((112.3333) * transformX) + moveX, ((1646.0000) * transformY) + moveY),
        new createjs.Point(((112.6667) * transformX) + moveX, ((1646.0000) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((1646.0000) * transformY) + moveY),

        new createjs.Point(((113.0000) * transformX) + moveX, ((1646.6666) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((1647.3334) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((1648.0000) * transformY) + moveY),

        new createjs.Point(((113.3333) * transformX) + moveX, ((1648.0000) * transformY) + moveY),
        new createjs.Point(((113.6667) * transformX) + moveX, ((1648.0000) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1648.0000) * transformY) + moveY),

        new createjs.Point(((114.0000) * transformX) + moveX, ((1648.6666) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1649.3334) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1650.0000) * transformY) + moveY),

        new createjs.Point(((114.3333) * transformX) + moveX, ((1650.0000) * transformY) + moveY),
        new createjs.Point(((114.6667) * transformX) + moveX, ((1650.0000) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((1650.0000) * transformY) + moveY),

        new createjs.Point(((115.0000) * transformX) + moveX, ((1650.6666) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((1651.3334) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((1652.0000) * transformY) + moveY),

        new createjs.Point(((115.3333) * transformX) + moveX, ((1652.0000) * transformY) + moveY),
        new createjs.Point(((115.6667) * transformX) + moveX, ((1652.0000) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((1652.0000) * transformY) + moveY),

        new createjs.Point(((116.0000) * transformX) + moveX, ((1652.6666) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((1653.3334) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((1654.0000) * transformY) + moveY),

        new createjs.Point(((116.6666) * transformX) + moveX, ((1654.3333) * transformY) + moveY),
        new createjs.Point(((117.3334) * transformX) + moveX, ((1654.6667) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1655.0000) * transformY) + moveY),

        new createjs.Point(((118.0000) * transformX) + moveX, ((1655.6666) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1656.3334) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1657.0000) * transformY) + moveY),

        new createjs.Point(((118.3333) * transformX) + moveX, ((1657.0000) * transformY) + moveY),
        new createjs.Point(((118.6667) * transformX) + moveX, ((1657.0000) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((1657.0000) * transformY) + moveY),

        new createjs.Point(((119.0000) * transformX) + moveX, ((1657.6666) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((1658.3334) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((1659.0000) * transformY) + moveY),

        new createjs.Point(((119.3333) * transformX) + moveX, ((1659.0000) * transformY) + moveY),
        new createjs.Point(((119.6667) * transformX) + moveX, ((1659.0000) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((1659.0000) * transformY) + moveY),

        new createjs.Point(((120.0000) * transformX) + moveX, ((1659.6666) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((1660.3334) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),

        new createjs.Point(((120.3333) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((120.6667) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),

        new createjs.Point(((121.0000) * transformX) + moveX, ((1661.6666) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1662.3334) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1663.0000) * transformY) + moveY),

        new createjs.Point(((121.6666) * transformX) + moveX, ((1663.3333) * transformY) + moveY),
        new createjs.Point(((122.3334) * transformX) + moveX, ((1663.6667) * transformY) + moveY),
        new createjs.Point(((123.0000) * transformX) + moveX, ((1664.0000) * transformY) + moveY),

        new createjs.Point(((123.6666) * transformX) + moveX, ((1665.9998) * transformY) + moveY),
        new createjs.Point(((124.3334) * transformX) + moveX, ((1668.0002) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((1670.0000) * transformY) + moveY),

        new createjs.Point(((125.6666) * transformX) + moveX, ((1670.3333) * transformY) + moveY),
        new createjs.Point(((126.3334) * transformX) + moveX, ((1670.6667) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1671.0000) * transformY) + moveY),

        new createjs.Point(((127.3333) * transformX) + moveX, ((1672.3332) * transformY) + moveY),
        new createjs.Point(((127.6667) * transformX) + moveX, ((1673.6668) * transformY) + moveY),
        new createjs.Point(((128.0000) * transformX) + moveX, ((1675.0000) * transformY) + moveY),

        new createjs.Point(((128.6666) * transformX) + moveX, ((1675.3333) * transformY) + moveY),
        new createjs.Point(((129.3334) * transformX) + moveX, ((1675.6667) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1676.0000) * transformY) + moveY),

        new createjs.Point(((130.0000) * transformX) + moveX, ((1676.6666) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1677.3334) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1678.0000) * transformY) + moveY),

        new createjs.Point(((130.3333) * transformX) + moveX, ((1678.0000) * transformY) + moveY),
        new createjs.Point(((130.6667) * transformX) + moveX, ((1678.0000) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1678.0000) * transformY) + moveY),

        new createjs.Point(((131.0000) * transformX) + moveX, ((1678.6666) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1679.3334) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1680.0000) * transformY) + moveY),

        new createjs.Point(((131.6666) * transformX) + moveX, ((1680.3333) * transformY) + moveY),
        new createjs.Point(((132.3334) * transformX) + moveX, ((1680.6667) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((1681.0000) * transformY) + moveY),

        new createjs.Point(((133.0000) * transformX) + moveX, ((1681.6666) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((1682.3334) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((1683.0000) * transformY) + moveY),

        new createjs.Point(((133.6666) * transformX) + moveX, ((1683.3333) * transformY) + moveY),
        new createjs.Point(((134.3334) * transformX) + moveX, ((1683.6667) * transformY) + moveY),
        new createjs.Point(((135.0000) * transformX) + moveX, ((1684.0000) * transformY) + moveY),

        new createjs.Point(((135.3333) * transformX) + moveX, ((1685.3332) * transformY) + moveY),
        new createjs.Point(((135.6667) * transformX) + moveX, ((1686.6668) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((1688.0000) * transformY) + moveY),

        new createjs.Point(((136.6666) * transformX) + moveX, ((1688.3333) * transformY) + moveY),
        new createjs.Point(((137.3334) * transformX) + moveX, ((1688.6667) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((1689.0000) * transformY) + moveY),

        new createjs.Point(((138.0000) * transformX) + moveX, ((1689.6666) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((1690.3334) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((1691.0000) * transformY) + moveY),

        new createjs.Point(((138.6666) * transformX) + moveX, ((1691.3333) * transformY) + moveY),
        new createjs.Point(((139.3334) * transformX) + moveX, ((1691.6667) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1692.0000) * transformY) + moveY),

        new createjs.Point(((140.0000) * transformX) + moveX, ((1692.6666) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1693.3334) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1694.0000) * transformY) + moveY),

        new createjs.Point(((140.6666) * transformX) + moveX, ((1694.3333) * transformY) + moveY),
        new createjs.Point(((141.3334) * transformX) + moveX, ((1694.6667) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((1695.0000) * transformY) + moveY),

        new createjs.Point(((142.0000) * transformX) + moveX, ((1695.6666) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((1696.3334) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((1697.0000) * transformY) + moveY),

        new createjs.Point(((142.3333) * transformX) + moveX, ((1697.0000) * transformY) + moveY),
        new createjs.Point(((142.6667) * transformX) + moveX, ((1697.0000) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1697.0000) * transformY) + moveY),

        new createjs.Point(((143.3333) * transformX) + moveX, ((1697.9999) * transformY) + moveY),
        new createjs.Point(((143.6667) * transformX) + moveX, ((1699.0001) * transformY) + moveY),
        new createjs.Point(((144.0000) * transformX) + moveX, ((1700.0000) * transformY) + moveY),

        new createjs.Point(((144.6666) * transformX) + moveX, ((1700.3333) * transformY) + moveY),
        new createjs.Point(((145.3334) * transformX) + moveX, ((1700.6667) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1701.0000) * transformY) + moveY),

        new createjs.Point(((146.0000) * transformX) + moveX, ((1701.6666) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1702.3334) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1703.0000) * transformY) + moveY),

        new createjs.Point(((146.6666) * transformX) + moveX, ((1703.3333) * transformY) + moveY),
        new createjs.Point(((147.3334) * transformX) + moveX, ((1703.6667) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((1704.0000) * transformY) + moveY),

        new createjs.Point(((148.0000) * transformX) + moveX, ((1704.6666) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((1705.3334) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((1706.0000) * transformY) + moveY),

        new createjs.Point(((148.9999) * transformX) + moveX, ((1706.6666) * transformY) + moveY),
        new createjs.Point(((150.0001) * transformX) + moveX, ((1707.3334) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1708.0000) * transformY) + moveY),

        new createjs.Point(((151.0000) * transformX) + moveX, ((1708.6666) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1709.3334) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1710.0000) * transformY) + moveY),

        new createjs.Point(((151.6666) * transformX) + moveX, ((1710.3333) * transformY) + moveY),
        new createjs.Point(((152.3334) * transformX) + moveX, ((1710.6667) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1711.0000) * transformY) + moveY),

        new createjs.Point(((153.0000) * transformX) + moveX, ((1711.6666) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1712.3334) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1713.0000) * transformY) + moveY),

        new createjs.Point(((153.9999) * transformX) + moveX, ((1713.6666) * transformY) + moveY),
        new createjs.Point(((155.0001) * transformX) + moveX, ((1714.3334) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((1715.0000) * transformY) + moveY),

        new createjs.Point(((156.0000) * transformX) + moveX, ((1715.6666) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((1716.3334) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((1717.0000) * transformY) + moveY),

        new createjs.Point(((156.6666) * transformX) + moveX, ((1717.3333) * transformY) + moveY),
        new createjs.Point(((157.3334) * transformX) + moveX, ((1717.6667) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),

        new createjs.Point(((158.0000) * transformX) + moveX, ((1718.6666) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1719.3334) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1720.0000) * transformY) + moveY),

        new createjs.Point(((158.9999) * transformX) + moveX, ((1720.6666) * transformY) + moveY),
        new createjs.Point(((160.0001) * transformX) + moveX, ((1721.3334) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((1722.0000) * transformY) + moveY),

        new createjs.Point(((161.0000) * transformX) + moveX, ((1722.6666) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((1723.3334) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((1724.0000) * transformY) + moveY),

        new createjs.Point(((161.9999) * transformX) + moveX, ((1724.6666) * transformY) + moveY),
        new createjs.Point(((163.0001) * transformX) + moveX, ((1725.3334) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((1726.0000) * transformY) + moveY),

        new createjs.Point(((164.0000) * transformX) + moveX, ((1726.6666) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((1727.3334) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((1728.0000) * transformY) + moveY),

        new createjs.Point(((165.3332) * transformX) + moveX, ((1728.9999) * transformY) + moveY),
        new createjs.Point(((166.6668) * transformX) + moveX, ((1730.0001) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),

        new createjs.Point(((168.0000) * transformX) + moveX, ((1731.6666) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1732.3334) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1733.0000) * transformY) + moveY),

        new createjs.Point(((169.3332) * transformX) + moveX, ((1733.9999) * transformY) + moveY),
        new createjs.Point(((170.6668) * transformX) + moveX, ((1735.0001) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),

        new createjs.Point(((172.0000) * transformX) + moveX, ((1736.6666) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((1737.3334) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((1738.0000) * transformY) + moveY),

        new createjs.Point(((173.3332) * transformX) + moveX, ((1738.9999) * transformY) + moveY),
        new createjs.Point(((174.6668) * transformX) + moveX, ((1740.0001) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),

        new createjs.Point(((176.0000) * transformX) + moveX, ((1741.6666) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((1742.3334) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),

        new createjs.Point(((177.9998) * transformX) + moveX, ((1744.6665) * transformY) + moveY),
        new createjs.Point(((180.0002) * transformX) + moveX, ((1746.3335) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((1748.0000) * transformY) + moveY),

        new createjs.Point(((182.0000) * transformX) + moveX, ((1748.6666) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((1749.3334) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((1750.0000) * transformY) + moveY),

        new createjs.Point(((184.3331) * transformX) + moveX, ((1751.9998) * transformY) + moveY),
        new createjs.Point(((186.6669) * transformX) + moveX, ((1754.0002) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((1756.0000) * transformY) + moveY),

        new createjs.Point(((189.0000) * transformX) + moveX, ((1756.6666) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((1757.3334) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),

        new createjs.Point(((192.9996) * transformX) + moveX, ((1761.6663) * transformY) + moveY),
        new createjs.Point(((197.0004) * transformX) + moveX, ((1765.3337) * transformY) + moveY),
        new createjs.Point(((201.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),

        new createjs.Point(((208.6159) * transformX) + moveX, ((1776.6123) * transformY) + moveY),
        new createjs.Point(((215.1174) * transformX) + moveX, ((1784.5781) * transformY) + moveY),
        new createjs.Point(((224.0000) * transformX) + moveX, ((1791.0000) * transformY) + moveY),

        new createjs.Point(((225.9998) * transformX) + moveX, ((1793.3331) * transformY) + moveY),
        new createjs.Point(((228.0002) * transformX) + moveX, ((1795.6669) * transformY) + moveY),
        new createjs.Point(((230.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),

        new createjs.Point(((230.6666) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((231.3334) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((232.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),

        new createjs.Point(((233.3332) * transformX) + moveX, ((1799.6665) * transformY) + moveY),
        new createjs.Point(((234.6668) * transformX) + moveX, ((1801.3335) * transformY) + moveY),
        new createjs.Point(((236.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),

        new createjs.Point(((236.6666) * transformX) + moveX, ((1803.0000) * transformY) + moveY),
        new createjs.Point(((237.3334) * transformX) + moveX, ((1803.0000) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),

        new createjs.Point(((238.9999) * transformX) + moveX, ((1804.3332) * transformY) + moveY),
        new createjs.Point(((240.0001) * transformX) + moveX, ((1805.6668) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((241.6666) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((242.3334) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((243.9999) * transformX) + moveX, ((1808.3332) * transformY) + moveY),
        new createjs.Point(((245.0001) * transformX) + moveX, ((1809.6668) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),

        new createjs.Point(((246.6666) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((247.3334) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),

        new createjs.Point(((248.6666) * transformX) + moveX, ((1811.9999) * transformY) + moveY),
        new createjs.Point(((249.3334) * transformX) + moveX, ((1813.0001) * transformY) + moveY),
        new createjs.Point(((250.0000) * transformX) + moveX, ((1814.0000) * transformY) + moveY),

        new createjs.Point(((250.6666) * transformX) + moveX, ((1814.0000) * transformY) + moveY),
        new createjs.Point(((251.3334) * transformX) + moveX, ((1814.0000) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((1814.0000) * transformY) + moveY),

        new createjs.Point(((252.6666) * transformX) + moveX, ((1814.9999) * transformY) + moveY),
        new createjs.Point(((253.3334) * transformX) + moveX, ((1816.0001) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1817.0000) * transformY) + moveY),

        new createjs.Point(((254.6666) * transformX) + moveX, ((1817.0000) * transformY) + moveY),
        new createjs.Point(((255.3334) * transformX) + moveX, ((1817.0000) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((1817.0000) * transformY) + moveY),

        new createjs.Point(((256.6666) * transformX) + moveX, ((1817.9999) * transformY) + moveY),
        new createjs.Point(((257.3334) * transformX) + moveX, ((1819.0001) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),

        new createjs.Point(((258.6666) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((259.3334) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),

        new createjs.Point(((260.3333) * transformX) + moveX, ((1820.6666) * transformY) + moveY),
        new createjs.Point(((260.6667) * transformX) + moveX, ((1821.3334) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((1822.0000) * transformY) + moveY),

        new createjs.Point(((261.6666) * transformX) + moveX, ((1822.0000) * transformY) + moveY),
        new createjs.Point(((262.3334) * transformX) + moveX, ((1822.0000) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((1822.0000) * transformY) + moveY),

        new createjs.Point(((263.6666) * transformX) + moveX, ((1822.9999) * transformY) + moveY),
        new createjs.Point(((264.3334) * transformX) + moveX, ((1824.0001) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((1825.0000) * transformY) + moveY),

        new createjs.Point(((265.6666) * transformX) + moveX, ((1825.0000) * transformY) + moveY),
        new createjs.Point(((266.3334) * transformX) + moveX, ((1825.0000) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((1825.0000) * transformY) + moveY),

        new createjs.Point(((267.3333) * transformX) + moveX, ((1825.6666) * transformY) + moveY),
        new createjs.Point(((267.6667) * transformX) + moveX, ((1826.3334) * transformY) + moveY),
        new createjs.Point(((268.0000) * transformX) + moveX, ((1827.0000) * transformY) + moveY),

        new createjs.Point(((268.9999) * transformX) + moveX, ((1827.3333) * transformY) + moveY),
        new createjs.Point(((270.0001) * transformX) + moveX, ((1827.6667) * transformY) + moveY),
        new createjs.Point(((271.0000) * transformX) + moveX, ((1828.0000) * transformY) + moveY),

        new createjs.Point(((271.0000) * transformX) + moveX, ((1828.3333) * transformY) + moveY),
        new createjs.Point(((271.0000) * transformX) + moveX, ((1828.6667) * transformY) + moveY),
        new createjs.Point(((271.0000) * transformX) + moveX, ((1829.0000) * transformY) + moveY),

        new createjs.Point(((271.6666) * transformX) + moveX, ((1829.0000) * transformY) + moveY),
        new createjs.Point(((272.3334) * transformX) + moveX, ((1829.0000) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((1829.0000) * transformY) + moveY),

        new createjs.Point(((273.3333) * transformX) + moveX, ((1829.6666) * transformY) + moveY),
        new createjs.Point(((273.6667) * transformX) + moveX, ((1830.3334) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1831.0000) * transformY) + moveY),

        new createjs.Point(((275.3332) * transformX) + moveX, ((1831.3333) * transformY) + moveY),
        new createjs.Point(((276.6668) * transformX) + moveX, ((1831.6667) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((1832.0000) * transformY) + moveY),

        new createjs.Point(((278.0000) * transformX) + moveX, ((1832.3333) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((1832.6667) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((1833.0000) * transformY) + moveY),

        new createjs.Point(((278.9999) * transformX) + moveX, ((1833.3333) * transformY) + moveY),
        new createjs.Point(((280.0001) * transformX) + moveX, ((1833.6667) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),

        new createjs.Point(((281.3333) * transformX) + moveX, ((1834.6666) * transformY) + moveY),
        new createjs.Point(((281.6667) * transformX) + moveX, ((1835.3334) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((1836.0000) * transformY) + moveY),

        new createjs.Point(((282.6666) * transformX) + moveX, ((1836.0000) * transformY) + moveY),
        new createjs.Point(((283.3334) * transformX) + moveX, ((1836.0000) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((1836.0000) * transformY) + moveY),

        new createjs.Point(((284.0000) * transformX) + moveX, ((1836.3333) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((1836.6667) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((1837.0000) * transformY) + moveY),

        new createjs.Point(((284.6666) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((285.3334) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((1837.0000) * transformY) + moveY),

        new createjs.Point(((286.3333) * transformX) + moveX, ((1837.6666) * transformY) + moveY),
        new createjs.Point(((286.6667) * transformX) + moveX, ((1838.3334) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((1839.0000) * transformY) + moveY),

        new createjs.Point(((288.3332) * transformX) + moveX, ((1839.3333) * transformY) + moveY),
        new createjs.Point(((289.6668) * transformX) + moveX, ((1839.6667) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1840.0000) * transformY) + moveY),

        new createjs.Point(((291.3333) * transformX) + moveX, ((1840.6666) * transformY) + moveY),
        new createjs.Point(((291.6667) * transformX) + moveX, ((1841.3334) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((1842.0000) * transformY) + moveY),

        new createjs.Point(((293.9998) * transformX) + moveX, ((1842.6666) * transformY) + moveY),
        new createjs.Point(((296.0002) * transformX) + moveX, ((1843.3334) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),

        new createjs.Point(((298.3333) * transformX) + moveX, ((1844.6666) * transformY) + moveY),
        new createjs.Point(((298.6667) * transformX) + moveX, ((1845.3334) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((299.6666) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((300.3334) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((301.0000) * transformX) + moveX, ((1846.3333) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((1846.6667) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),

        new createjs.Point(((301.6666) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((302.3334) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),

        new createjs.Point(((303.0000) * transformX) + moveX, ((1847.3333) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((1847.6667) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((303.6666) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((304.3334) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((305.0000) * transformX) + moveX, ((1848.3333) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1848.6667) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1849.0000) * transformY) + moveY),

        new createjs.Point(((305.6666) * transformX) + moveX, ((1849.0000) * transformY) + moveY),
        new createjs.Point(((306.3334) * transformX) + moveX, ((1849.0000) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((1849.0000) * transformY) + moveY),

        new createjs.Point(((307.0000) * transformX) + moveX, ((1849.3333) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((1849.6667) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((307.6666) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((308.3334) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((309.0000) * transformX) + moveX, ((1850.3333) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((1850.6667) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((1851.0000) * transformY) + moveY),

        new createjs.Point(((309.6666) * transformX) + moveX, ((1851.0000) * transformY) + moveY),
        new createjs.Point(((310.3334) * transformX) + moveX, ((1851.0000) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((1851.0000) * transformY) + moveY),

        new createjs.Point(((311.0000) * transformX) + moveX, ((1851.3333) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((1851.6667) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),

        new createjs.Point(((312.3332) * transformX) + moveX, ((1852.3333) * transformY) + moveY),
        new createjs.Point(((313.6668) * transformX) + moveX, ((1852.6667) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((1853.0000) * transformY) + moveY),

        new createjs.Point(((315.0000) * transformX) + moveX, ((1853.3333) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((1853.6667) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((315.6666) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((316.3334) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((317.0000) * transformX) + moveX, ((1854.3333) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((1854.6667) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),

        new createjs.Point(((317.6666) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((318.3334) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),

        new createjs.Point(((319.0000) * transformX) + moveX, ((1855.3333) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((1855.6667) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),

        new createjs.Point(((319.6666) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((320.3334) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),

        new createjs.Point(((321.0000) * transformX) + moveX, ((1856.3333) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((1856.6667) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((1857.0000) * transformY) + moveY),

        new createjs.Point(((321.6666) * transformX) + moveX, ((1857.0000) * transformY) + moveY),
        new createjs.Point(((322.3334) * transformX) + moveX, ((1857.0000) * transformY) + moveY),
        new createjs.Point(((323.0000) * transformX) + moveX, ((1857.0000) * transformY) + moveY),

        new createjs.Point(((323.0000) * transformX) + moveX, ((1857.3333) * transformY) + moveY),
        new createjs.Point(((323.0000) * transformX) + moveX, ((1857.6667) * transformY) + moveY),
        new createjs.Point(((323.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),

        new createjs.Point(((323.6666) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((324.3334) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),

        new createjs.Point(((325.0000) * transformX) + moveX, ((1858.3333) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((1858.6667) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),

        new createjs.Point(((326.3332) * transformX) + moveX, ((1859.3333) * transformY) + moveY),
        new createjs.Point(((327.6668) * transformX) + moveX, ((1859.6667) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),

        new createjs.Point(((329.0000) * transformX) + moveX, ((1860.3333) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1860.6667) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),

        new createjs.Point(((329.6666) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((330.3334) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),

        new createjs.Point(((331.0000) * transformX) + moveX, ((1861.3333) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((1861.6667) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),

        new createjs.Point(((331.9999) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((333.0001) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),

        new createjs.Point(((334.0000) * transformX) + moveX, ((1862.3333) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((1862.6667) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),

        new createjs.Point(((335.3332) * transformX) + moveX, ((1863.3333) * transformY) + moveY),
        new createjs.Point(((336.6668) * transformX) + moveX, ((1863.6667) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),

        new createjs.Point(((338.0000) * transformX) + moveX, ((1864.3333) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((1864.6667) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),

        new createjs.Point(((338.9999) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((340.0001) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),

        new createjs.Point(((341.0000) * transformX) + moveX, ((1865.3333) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((1865.6667) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),

        new createjs.Point(((341.6666) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((342.3334) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),

        new createjs.Point(((343.0000) * transformX) + moveX, ((1866.3333) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((1866.6667) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),

        new createjs.Point(((343.9999) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((345.0001) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),

        new createjs.Point(((346.0000) * transformX) + moveX, ((1867.3333) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((1867.6667) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((346.6666) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((347.3334) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((348.0000) * transformX) + moveX, ((1868.3333) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((1868.6667) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),

        new createjs.Point(((348.9999) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((350.0001) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((351.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),

        new createjs.Point(((351.0000) * transformX) + moveX, ((1869.3333) * transformY) + moveY),
        new createjs.Point(((351.0000) * transformX) + moveX, ((1869.6667) * transformY) + moveY),
        new createjs.Point(((351.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),

        new createjs.Point(((351.6666) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((352.3334) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((353.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),

        new createjs.Point(((353.0000) * transformX) + moveX, ((1870.3333) * transformY) + moveY),
        new createjs.Point(((353.0000) * transformX) + moveX, ((1870.6667) * transformY) + moveY),
        new createjs.Point(((353.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),

        new createjs.Point(((354.9998) * transformX) + moveX, ((1871.3333) * transformY) + moveY),
        new createjs.Point(((357.0002) * transformX) + moveX, ((1871.6667) * transformY) + moveY),
        new createjs.Point(((359.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),

        new createjs.Point(((359.0000) * transformX) + moveX, ((1872.3333) * transformY) + moveY),
        new createjs.Point(((359.0000) * transformX) + moveX, ((1872.6667) * transformY) + moveY),
        new createjs.Point(((359.0000) * transformX) + moveX, ((1873.0000) * transformY) + moveY),

        new createjs.Point(((360.9998) * transformX) + moveX, ((1873.3333) * transformY) + moveY),
        new createjs.Point(((363.0002) * transformX) + moveX, ((1873.6667) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((1874.0000) * transformY) + moveY),

        new createjs.Point(((365.0000) * transformX) + moveX, ((1874.3333) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((1874.6667) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),

        new createjs.Point(((365.6666) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((366.3334) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),

        new createjs.Point(((367.0000) * transformX) + moveX, ((1875.3333) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((1875.6667) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),

        new createjs.Point(((367.9999) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((369.0001) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((370.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),

        new createjs.Point(((370.0000) * transformX) + moveX, ((1876.3333) * transformY) + moveY),
        new createjs.Point(((370.0000) * transformX) + moveX, ((1876.6667) * transformY) + moveY),
        new createjs.Point(((370.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),

        new createjs.Point(((371.3332) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((372.6668) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),

        new createjs.Point(((374.0000) * transformX) + moveX, ((1877.3333) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((1877.6667) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),

        new createjs.Point(((374.9999) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((376.0001) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((377.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),

        new createjs.Point(((377.0000) * transformX) + moveX, ((1878.3333) * transformY) + moveY),
        new createjs.Point(((377.0000) * transformX) + moveX, ((1878.6667) * transformY) + moveY),
        new createjs.Point(((377.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),

        new createjs.Point(((377.9999) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((379.0001) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),

        new createjs.Point(((380.0000) * transformX) + moveX, ((1879.3333) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((1879.6667) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((380.9999) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((382.0001) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((383.0000) * transformX) + moveX, ((1880.3333) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((1880.6667) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((1881.0000) * transformY) + moveY),

        new createjs.Point(((385.3331) * transformX) + moveX, ((1881.3333) * transformY) + moveY),
        new createjs.Point(((387.6669) * transformX) + moveX, ((1881.6667) * transformY) + moveY),
        new createjs.Point(((390.0000) * transformX) + moveX, ((1882.0000) * transformY) + moveY),

        new createjs.Point(((390.0000) * transformX) + moveX, ((1882.3333) * transformY) + moveY),
        new createjs.Point(((390.0000) * transformX) + moveX, ((1882.6667) * transformY) + moveY),
        new createjs.Point(((390.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),

        new createjs.Point(((391.3332) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((392.6668) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((394.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),

        new createjs.Point(((394.0000) * transformX) + moveX, ((1883.3333) * transformY) + moveY),
        new createjs.Point(((394.0000) * transformX) + moveX, ((1883.6667) * transformY) + moveY),
        new createjs.Point(((394.0000) * transformX) + moveX, ((1884.0000) * transformY) + moveY),

        new createjs.Point(((395.3332) * transformX) + moveX, ((1884.0000) * transformY) + moveY),
        new createjs.Point(((396.6668) * transformX) + moveX, ((1884.0000) * transformY) + moveY),
        new createjs.Point(((398.0000) * transformX) + moveX, ((1884.0000) * transformY) + moveY),

        new createjs.Point(((398.0000) * transformX) + moveX, ((1884.3333) * transformY) + moveY),
        new createjs.Point(((398.0000) * transformX) + moveX, ((1884.6667) * transformY) + moveY),
        new createjs.Point(((398.0000) * transformX) + moveX, ((1885.0000) * transformY) + moveY),

        new createjs.Point(((399.3332) * transformX) + moveX, ((1885.0000) * transformY) + moveY),
        new createjs.Point(((400.6668) * transformX) + moveX, ((1885.0000) * transformY) + moveY),
        new createjs.Point(((402.0000) * transformX) + moveX, ((1885.0000) * transformY) + moveY),

        new createjs.Point(((402.0000) * transformX) + moveX, ((1885.3333) * transformY) + moveY),
        new createjs.Point(((402.0000) * transformX) + moveX, ((1885.6667) * transformY) + moveY),
        new createjs.Point(((402.0000) * transformX) + moveX, ((1886.0000) * transformY) + moveY),

        new createjs.Point(((403.3332) * transformX) + moveX, ((1886.0000) * transformY) + moveY),
        new createjs.Point(((404.6668) * transformX) + moveX, ((1886.0000) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((1886.0000) * transformY) + moveY),

        new createjs.Point(((406.0000) * transformX) + moveX, ((1886.3333) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((1886.6667) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((1887.0000) * transformY) + moveY),

        new createjs.Point(((420.6652) * transformX) + moveX, ((1889.6664) * transformY) + moveY),
        new createjs.Point(((435.3348) * transformX) + moveX, ((1892.3336) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((1895.0000) * transformY) + moveY),

        new createjs.Point(((452.6664) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((455.3336) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((458.0000) * transformX) + moveX, ((1895.0000) * transformY) + moveY),

        new createjs.Point(((458.0000) * transformX) + moveX, ((1895.3333) * transformY) + moveY),
        new createjs.Point(((458.0000) * transformX) + moveX, ((1895.6667) * transformY) + moveY),
        new createjs.Point(((458.0000) * transformX) + moveX, ((1896.0000) * transformY) + moveY),

        new createjs.Point(((460.9997) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((464.0003) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((1896.0000) * transformY) + moveY),

        new createjs.Point(((467.0000) * transformX) + moveX, ((1896.3333) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((1896.6667) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),

        new createjs.Point(((470.6663) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((474.3337) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),

        new createjs.Point(((478.0000) * transformX) + moveX, ((1897.3333) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1897.6667) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1898.0000) * transformY) + moveY),

        new createjs.Point(((483.6661) * transformX) + moveX, ((1898.0000) * transformY) + moveY),
        new createjs.Point(((489.3339) * transformX) + moveX, ((1898.0000) * transformY) + moveY),
        new createjs.Point(((495.0000) * transformX) + moveX, ((1898.0000) * transformY) + moveY),

        new createjs.Point(((495.0000) * transformX) + moveX, ((1898.3333) * transformY) + moveY),
        new createjs.Point(((495.0000) * transformX) + moveX, ((1898.6667) * transformY) + moveY),
        new createjs.Point(((495.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),

        new createjs.Point(((499.8197) * transformX) + moveX, ((1900.3693) * transformY) + moveY),
        new createjs.Point(((507.7858) * transformX) + moveX, ((1897.1566) * transformY) + moveY),
        new createjs.Point(((511.0000) * transformX) + moveX, ((1900.0000) * transformY) + moveY),

        // 2nd part
        new createjs.Point(((511.0000) * transformX) + moveX, ((1900.0000) * transformY) + moveY),

        new createjs.Point(((517.2108) * transformX) + moveX, ((1900.1318) * transformY) + moveY),
        new createjs.Point(((525.3846) * transformX) + moveX, ((1900.3183) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),

        new createjs.Point(((535.3328) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((540.6672) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),

        new createjs.Point(((546.0000) * transformX) + moveX, ((1898.6667) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((1898.3333) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((1898.0000) * transformY) + moveY),

        new createjs.Point(((554.9991) * transformX) + moveX, ((1897.6667) * transformY) + moveY),
        new createjs.Point(((564.0009) * transformX) + moveX, ((1897.3333) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),

        new createjs.Point(((573.0000) * transformX) + moveX, ((1896.6667) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((1896.3333) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((1896.0000) * transformY) + moveY),

        new createjs.Point(((576.3330) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((579.6670) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((583.0000) * transformX) + moveX, ((1896.0000) * transformY) + moveY),

        new createjs.Point(((583.0000) * transformX) + moveX, ((1895.6667) * transformY) + moveY),
        new createjs.Point(((583.0000) * transformX) + moveX, ((1895.3333) * transformY) + moveY),
        new createjs.Point(((583.0000) * transformX) + moveX, ((1895.0000) * transformY) + moveY),

        new createjs.Point(((585.3331) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((587.6669) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((590.0000) * transformX) + moveX, ((1895.0000) * transformY) + moveY),

        new createjs.Point(((590.0000) * transformX) + moveX, ((1894.6667) * transformY) + moveY),
        new createjs.Point(((590.0000) * transformX) + moveX, ((1894.3333) * transformY) + moveY),
        new createjs.Point(((590.0000) * transformX) + moveX, ((1894.0000) * transformY) + moveY),

        new createjs.Point(((592.3331) * transformX) + moveX, ((1894.0000) * transformY) + moveY),
        new createjs.Point(((594.6669) * transformX) + moveX, ((1894.0000) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1894.0000) * transformY) + moveY),

        new createjs.Point(((614.9982) * transformX) + moveX, ((1890.3337) * transformY) + moveY),
        new createjs.Point(((633.0018) * transformX) + moveX, ((1886.6663) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),

        new createjs.Point(((651.0000) * transformX) + moveX, ((1882.6667) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((1882.3333) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((1882.0000) * transformY) + moveY),

        new createjs.Point(((653.3331) * transformX) + moveX, ((1881.6667) * transformY) + moveY),
        new createjs.Point(((655.6669) * transformX) + moveX, ((1881.3333) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((1881.0000) * transformY) + moveY),

        new createjs.Point(((658.0000) * transformX) + moveX, ((1880.6667) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((1880.3333) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((658.9999) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((660.0001) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((661.0000) * transformX) + moveX, ((1879.6667) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((1879.3333) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),

        new createjs.Point(((664.3330) * transformX) + moveX, ((1878.3334) * transformY) + moveY),
        new createjs.Point(((667.6670) * transformX) + moveX, ((1877.6666) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),

        new createjs.Point(((671.0000) * transformX) + moveX, ((1876.6667) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((1876.3333) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),

        new createjs.Point(((675.3329) * transformX) + moveX, ((1875.0001) * transformY) + moveY),
        new createjs.Point(((679.6671) * transformX) + moveX, ((1873.9999) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((1873.0000) * transformY) + moveY),

        new createjs.Point(((684.0000) * transformX) + moveX, ((1872.6667) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((1872.3333) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),

        new createjs.Point(((684.6666) * transformX) + moveX, ((1872.0000) * transformY) + moveY),
        new createjs.Point(((685.3334) * transformX) + moveX, ((1872.0000) * transformY) + moveY),
        new createjs.Point(((686.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),

        new createjs.Point(((686.0000) * transformX) + moveX, ((1871.6667) * transformY) + moveY),
        new createjs.Point(((686.0000) * transformX) + moveX, ((1871.3333) * transformY) + moveY),
        new createjs.Point(((686.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),

        new createjs.Point(((688.9997) * transformX) + moveX, ((1870.3334) * transformY) + moveY),
        new createjs.Point(((692.0003) * transformX) + moveX, ((1869.6666) * transformY) + moveY),
        new createjs.Point(((695.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),

        new createjs.Point(((695.0000) * transformX) + moveX, ((1868.6667) * transformY) + moveY),
        new createjs.Point(((695.0000) * transformX) + moveX, ((1868.3333) * transformY) + moveY),
        new createjs.Point(((695.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((695.6666) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((696.3334) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((697.0000) * transformX) + moveX, ((1867.6667) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((1867.3333) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),

        new createjs.Point(((698.9998) * transformX) + moveX, ((1866.6667) * transformY) + moveY),
        new createjs.Point(((701.0002) * transformX) + moveX, ((1866.3333) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),

        new createjs.Point(((703.0000) * transformX) + moveX, ((1865.6667) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((1865.3333) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),

        new createjs.Point(((704.6665) * transformX) + moveX, ((1864.6667) * transformY) + moveY),
        new createjs.Point(((706.3335) * transformX) + moveX, ((1864.3333) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),

        new createjs.Point(((708.0000) * transformX) + moveX, ((1863.6667) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((1863.3333) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),

        new createjs.Point(((708.6666) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((709.3334) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),

        new createjs.Point(((710.0000) * transformX) + moveX, ((1862.6667) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((1862.3333) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),

        new createjs.Point(((710.9999) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((712.0001) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),

        new createjs.Point(((713.0000) * transformX) + moveX, ((1861.6667) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((1861.3333) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),

        new createjs.Point(((714.3332) * transformX) + moveX, ((1860.6667) * transformY) + moveY),
        new createjs.Point(((715.6668) * transformX) + moveX, ((1860.3333) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),

        new createjs.Point(((717.0000) * transformX) + moveX, ((1859.6667) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((1859.3333) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),

        new createjs.Point(((717.9999) * transformX) + moveX, ((1859.0000) * transformY) + moveY),
        new createjs.Point(((719.0001) * transformX) + moveX, ((1859.0000) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),

        new createjs.Point(((720.0000) * transformX) + moveX, ((1858.6667) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((1858.3333) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),

        new createjs.Point(((720.6666) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((721.3334) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),

        new createjs.Point(((722.0000) * transformX) + moveX, ((1857.6667) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((1857.3333) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((1857.0000) * transformY) + moveY),

        new createjs.Point(((723.3332) * transformX) + moveX, ((1856.6667) * transformY) + moveY),
        new createjs.Point(((724.6668) * transformX) + moveX, ((1856.3333) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),

        new createjs.Point(((726.0000) * transformX) + moveX, ((1855.6667) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((1855.3333) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),

        new createjs.Point(((726.9999) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((728.0001) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),

        new createjs.Point(((729.0000) * transformX) + moveX, ((1854.6667) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((1854.3333) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((729.6666) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((730.3334) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((731.0000) * transformX) + moveX, ((1853.6667) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((1853.3333) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((1853.0000) * transformY) + moveY),

        new createjs.Point(((732.3332) * transformX) + moveX, ((1852.6667) * transformY) + moveY),
        new createjs.Point(((733.6668) * transformX) + moveX, ((1852.3333) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),

        new createjs.Point(((735.0000) * transformX) + moveX, ((1851.6667) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((1851.3333) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((1851.0000) * transformY) + moveY),

        new createjs.Point(((736.3332) * transformX) + moveX, ((1850.6667) * transformY) + moveY),
        new createjs.Point(((737.6668) * transformX) + moveX, ((1850.3333) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((739.0000) * transformX) + moveX, ((1849.6667) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((1849.3333) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((1849.0000) * transformY) + moveY),

        new createjs.Point(((739.6666) * transformX) + moveX, ((1849.0000) * transformY) + moveY),
        new createjs.Point(((740.3334) * transformX) + moveX, ((1849.0000) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((1849.0000) * transformY) + moveY),

        new createjs.Point(((741.0000) * transformX) + moveX, ((1848.6667) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((1848.3333) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((741.6666) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((742.3334) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((743.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((743.0000) * transformX) + moveX, ((1847.6667) * transformY) + moveY),
        new createjs.Point(((743.0000) * transformX) + moveX, ((1847.3333) * transformY) + moveY),
        new createjs.Point(((743.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),

        new createjs.Point(((743.6666) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((744.3334) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),

        new createjs.Point(((745.0000) * transformX) + moveX, ((1846.6667) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((1846.3333) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((745.6666) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((746.3334) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((747.0000) * transformX) + moveX, ((1845.6667) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((1845.3333) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),

        new createjs.Point(((747.6666) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((748.3334) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),

        new createjs.Point(((749.0000) * transformX) + moveX, ((1844.6667) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((1844.3333) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),

        new createjs.Point(((749.6666) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((750.3334) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),

        new createjs.Point(((751.0000) * transformX) + moveX, ((1843.6667) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((1843.3333) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((1843.0000) * transformY) + moveY),

        new createjs.Point(((751.6666) * transformX) + moveX, ((1843.0000) * transformY) + moveY),
        new createjs.Point(((752.3334) * transformX) + moveX, ((1843.0000) * transformY) + moveY),
        new createjs.Point(((753.0000) * transformX) + moveX, ((1843.0000) * transformY) + moveY),

        new createjs.Point(((753.0000) * transformX) + moveX, ((1842.6667) * transformY) + moveY),
        new createjs.Point(((753.0000) * transformX) + moveX, ((1842.3333) * transformY) + moveY),
        new createjs.Point(((753.0000) * transformX) + moveX, ((1842.0000) * transformY) + moveY),

        new createjs.Point(((753.6666) * transformX) + moveX, ((1842.0000) * transformY) + moveY),
        new createjs.Point(((754.3334) * transformX) + moveX, ((1842.0000) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((1842.0000) * transformY) + moveY),

        new createjs.Point(((755.0000) * transformX) + moveX, ((1841.6667) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((1841.3333) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),

        new createjs.Point(((755.6666) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((756.3334) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),

        new createjs.Point(((757.0000) * transformX) + moveX, ((1840.6667) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((1840.3333) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((1840.0000) * transformY) + moveY),

        new createjs.Point(((757.6666) * transformX) + moveX, ((1840.0000) * transformY) + moveY),
        new createjs.Point(((758.3334) * transformX) + moveX, ((1840.0000) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((1840.0000) * transformY) + moveY),

        new createjs.Point(((759.0000) * transformX) + moveX, ((1839.6667) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((1839.3333) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((1839.0000) * transformY) + moveY),

        new createjs.Point(((759.6666) * transformX) + moveX, ((1839.0000) * transformY) + moveY),
        new createjs.Point(((760.3334) * transformX) + moveX, ((1839.0000) * transformY) + moveY),
        new createjs.Point(((761.0000) * transformX) + moveX, ((1839.0000) * transformY) + moveY),

        new createjs.Point(((761.3333) * transformX) + moveX, ((1838.3334) * transformY) + moveY),
        new createjs.Point(((761.6667) * transformX) + moveX, ((1837.6666) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((1837.0000) * transformY) + moveY),

        new createjs.Point(((762.6666) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((763.3334) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((1837.0000) * transformY) + moveY),

        new createjs.Point(((764.0000) * transformX) + moveX, ((1836.6667) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((1836.3333) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((1836.0000) * transformY) + moveY),

        new createjs.Point(((764.6666) * transformX) + moveX, ((1836.0000) * transformY) + moveY),
        new createjs.Point(((765.3334) * transformX) + moveX, ((1836.0000) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((1836.0000) * transformY) + moveY),

        new createjs.Point(((766.0000) * transformX) + moveX, ((1835.6667) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((1835.3333) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((1835.0000) * transformY) + moveY),

        new createjs.Point(((766.6666) * transformX) + moveX, ((1835.0000) * transformY) + moveY),
        new createjs.Point(((767.3334) * transformX) + moveX, ((1835.0000) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((1835.0000) * transformY) + moveY),

        new createjs.Point(((768.0000) * transformX) + moveX, ((1834.6667) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((1834.3333) * transformY) + moveY),
        new createjs.Point(((768.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),

        new createjs.Point(((768.6666) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((769.3334) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),

        new createjs.Point(((770.3333) * transformX) + moveX, ((1833.3334) * transformY) + moveY),
        new createjs.Point(((770.6667) * transformX) + moveX, ((1832.6666) * transformY) + moveY),
        new createjs.Point(((771.0000) * transformX) + moveX, ((1832.0000) * transformY) + moveY),

        new createjs.Point(((772.9998) * transformX) + moveX, ((1831.3334) * transformY) + moveY),
        new createjs.Point(((775.0002) * transformX) + moveX, ((1830.6666) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((1830.0000) * transformY) + moveY),

        new createjs.Point(((777.3333) * transformX) + moveX, ((1829.3334) * transformY) + moveY),
        new createjs.Point(((777.6667) * transformX) + moveX, ((1828.6666) * transformY) + moveY),
        new createjs.Point(((778.0000) * transformX) + moveX, ((1828.0000) * transformY) + moveY),

        new createjs.Point(((779.3332) * transformX) + moveX, ((1827.6667) * transformY) + moveY),
        new createjs.Point(((780.6668) * transformX) + moveX, ((1827.3333) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((1827.0000) * transformY) + moveY),

        new createjs.Point(((782.0000) * transformX) + moveX, ((1826.6667) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((1826.3333) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((1826.0000) * transformY) + moveY),

        new createjs.Point(((782.9999) * transformX) + moveX, ((1825.6667) * transformY) + moveY),
        new createjs.Point(((784.0001) * transformX) + moveX, ((1825.3333) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((1825.0000) * transformY) + moveY),

        new createjs.Point(((785.3333) * transformX) + moveX, ((1824.3334) * transformY) + moveY),
        new createjs.Point(((785.6667) * transformX) + moveX, ((1823.6666) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1823.0000) * transformY) + moveY),

        new createjs.Point(((787.3332) * transformX) + moveX, ((1822.6667) * transformY) + moveY),
        new createjs.Point(((788.6668) * transformX) + moveX, ((1822.3333) * transformY) + moveY),
        new createjs.Point(((790.0000) * transformX) + moveX, ((1822.0000) * transformY) + moveY),

        new createjs.Point(((790.3333) * transformX) + moveX, ((1821.3334) * transformY) + moveY),
        new createjs.Point(((790.6667) * transformX) + moveX, ((1820.6666) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),

        new createjs.Point(((791.6666) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((792.3334) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),

        new createjs.Point(((793.3333) * transformX) + moveX, ((1819.3334) * transformY) + moveY),
        new createjs.Point(((793.6667) * transformX) + moveX, ((1818.6666) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),

        new createjs.Point(((794.6666) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((795.3334) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),

        new createjs.Point(((796.3333) * transformX) + moveX, ((1817.3334) * transformY) + moveY),
        new createjs.Point(((796.6667) * transformX) + moveX, ((1816.6666) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),

        new createjs.Point(((798.3332) * transformX) + moveX, ((1815.6667) * transformY) + moveY),
        new createjs.Point(((799.6668) * transformX) + moveX, ((1815.3333) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1815.0000) * transformY) + moveY),

        new createjs.Point(((801.3333) * transformX) + moveX, ((1814.3334) * transformY) + moveY),
        new createjs.Point(((801.6667) * transformX) + moveX, ((1813.6666) * transformY) + moveY),
        new createjs.Point(((802.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),

        new createjs.Point(((802.6666) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((803.3334) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((804.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),

        new createjs.Point(((804.6666) * transformX) + moveX, ((1812.0001) * transformY) + moveY),
        new createjs.Point(((805.3334) * transformX) + moveX, ((1810.9999) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((806.6666) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((807.3334) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((808.3333) * transformX) + moveX, ((1809.3334) * transformY) + moveY),
        new createjs.Point(((808.6667) * transformX) + moveX, ((1808.6666) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),

        new createjs.Point(((809.6666) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((810.3334) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),

        new createjs.Point(((811.0000) * transformX) + moveX, ((1807.6667) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1807.3333) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((811.9999) * transformX) + moveX, ((1806.6667) * transformY) + moveY),
        new createjs.Point(((813.0001) * transformX) + moveX, ((1806.3333) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((1806.0000) * transformY) + moveY),

        new createjs.Point(((814.3333) * transformX) + moveX, ((1805.3334) * transformY) + moveY),
        new createjs.Point(((814.6667) * transformX) + moveX, ((1804.6666) * transformY) + moveY),
        new createjs.Point(((815.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),

        new createjs.Point(((815.6666) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((816.3334) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),

        new createjs.Point(((817.3333) * transformX) + moveX, ((1803.3334) * transformY) + moveY),
        new createjs.Point(((817.6667) * transformX) + moveX, ((1802.6666) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),

        new createjs.Point(((818.9999) * transformX) + moveX, ((1801.6667) * transformY) + moveY),
        new createjs.Point(((820.0001) * transformX) + moveX, ((1801.3333) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1801.0000) * transformY) + moveY),

        new createjs.Point(((821.6666) * transformX) + moveX, ((1800.0001) * transformY) + moveY),
        new createjs.Point(((822.3334) * transformX) + moveX, ((1798.9999) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),

        new createjs.Point(((823.6666) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((824.3334) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((825.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),

        new createjs.Point(((825.3333) * transformX) + moveX, ((1797.3334) * transformY) + moveY),
        new createjs.Point(((825.6667) * transformX) + moveX, ((1796.6666) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),

        new createjs.Point(((826.6666) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((827.3334) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),

        new createjs.Point(((828.9999) * transformX) + moveX, ((1794.6668) * transformY) + moveY),
        new createjs.Point(((830.0001) * transformX) + moveX, ((1793.3332) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1792.0000) * transformY) + moveY),

        new createjs.Point(((831.6666) * transformX) + moveX, ((1792.0000) * transformY) + moveY),
        new createjs.Point(((832.3334) * transformX) + moveX, ((1792.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1792.0000) * transformY) + moveY),

        new createjs.Point(((833.6666) * transformX) + moveX, ((1791.0001) * transformY) + moveY),
        new createjs.Point(((834.3334) * transformX) + moveX, ((1789.9999) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((1789.0000) * transformY) + moveY),

        new createjs.Point(((835.6666) * transformX) + moveX, ((1789.0000) * transformY) + moveY),
        new createjs.Point(((836.3334) * transformX) + moveX, ((1789.0000) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1789.0000) * transformY) + moveY),

        new createjs.Point(((837.9999) * transformX) + moveX, ((1787.6668) * transformY) + moveY),
        new createjs.Point(((839.0001) * transformX) + moveX, ((1786.3332) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((840.6666) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((841.3334) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((842.3333) * transformX) + moveX, ((1784.3334) * transformY) + moveY),
        new createjs.Point(((842.6667) * transformX) + moveX, ((1783.6666) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),

        new createjs.Point(((843.3333) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((843.6667) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),

        new createjs.Point(((844.3333) * transformX) + moveX, ((1782.3334) * transformY) + moveY),
        new createjs.Point(((844.6667) * transformX) + moveX, ((1781.6666) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),

        new createjs.Point(((845.6666) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((846.3334) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),

        new createjs.Point(((847.9999) * transformX) + moveX, ((1779.6668) * transformY) + moveY),
        new createjs.Point(((849.0001) * transformX) + moveX, ((1778.3332) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),

        new createjs.Point(((850.6666) * transformX) + moveX, ((1777.0000) * transformY) + moveY),
        new createjs.Point(((851.3334) * transformX) + moveX, ((1777.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),

        new createjs.Point(((853.3332) * transformX) + moveX, ((1775.3335) * transformY) + moveY),
        new createjs.Point(((854.6668) * transformX) + moveX, ((1773.6665) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1772.0000) * transformY) + moveY),

        new createjs.Point(((856.6666) * transformX) + moveX, ((1772.0000) * transformY) + moveY),
        new createjs.Point(((857.3334) * transformX) + moveX, ((1772.0000) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1772.0000) * transformY) + moveY),

        new createjs.Point(((859.9998) * transformX) + moveX, ((1769.6669) * transformY) + moveY),
        new createjs.Point(((862.0002) * transformX) + moveX, ((1767.3331) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),

        new createjs.Point(((864.6666) * transformX) + moveX, ((1765.0000) * transformY) + moveY),
        new createjs.Point(((865.3334) * transformX) + moveX, ((1765.0000) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),

        new createjs.Point(((868.3331) * transformX) + moveX, ((1762.3336) * transformY) + moveY),
        new createjs.Point(((870.6669) * transformX) + moveX, ((1759.6664) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((1757.0000) * transformY) + moveY),

        new createjs.Point(((873.9999) * transformX) + moveX, ((1756.6667) * transformY) + moveY),
        new createjs.Point(((875.0001) * transformX) + moveX, ((1756.3333) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1756.0000) * transformY) + moveY),

        new createjs.Point(((880.3329) * transformX) + moveX, ((1751.3338) * transformY) + moveY),
        new createjs.Point(((884.6671) * transformX) + moveX, ((1746.6662) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1742.0000) * transformY) + moveY),

        new createjs.Point(((897.8905) * transformX) + moveX, ((1733.1063) * transformY) + moveY),
        new createjs.Point(((907.5201) * transformX) + moveX, ((1725.3436) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((1715.0000) * transformY) + moveY),

        new createjs.Point(((918.3330) * transformX) + moveX, ((1712.0003) * transformY) + moveY),
        new createjs.Point(((921.6670) * transformX) + moveX, ((1708.9997) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((1706.0000) * transformY) + moveY),

        new createjs.Point(((925.0000) * transformX) + moveX, ((1705.3334) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((1704.6666) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((1704.0000) * transformY) + moveY),

        new createjs.Point(((926.9998) * transformX) + moveX, ((1702.3335) * transformY) + moveY),
        new createjs.Point(((929.0002) * transformX) + moveX, ((1700.6665) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((1699.0000) * transformY) + moveY),

        new createjs.Point(((931.0000) * transformX) + moveX, ((1698.3334) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((1697.6666) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((1697.0000) * transformY) + moveY),

        new createjs.Point(((932.9998) * transformX) + moveX, ((1695.3335) * transformY) + moveY),
        new createjs.Point(((935.0002) * transformX) + moveX, ((1693.6665) * transformY) + moveY),
        new createjs.Point(((937.0000) * transformX) + moveX, ((1692.0000) * transformY) + moveY),

        new createjs.Point(((937.0000) * transformX) + moveX, ((1691.3334) * transformY) + moveY),
        new createjs.Point(((937.0000) * transformX) + moveX, ((1690.6666) * transformY) + moveY),
        new createjs.Point(((937.0000) * transformX) + moveX, ((1690.0000) * transformY) + moveY),

        new createjs.Point(((938.6665) * transformX) + moveX, ((1688.6668) * transformY) + moveY),
        new createjs.Point(((940.3335) * transformX) + moveX, ((1687.3332) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((1686.0000) * transformY) + moveY),

        new createjs.Point(((942.0000) * transformX) + moveX, ((1685.3334) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((1684.6666) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((1684.0000) * transformY) + moveY),

        new createjs.Point(((943.3332) * transformX) + moveX, ((1683.0001) * transformY) + moveY),
        new createjs.Point(((944.6668) * transformX) + moveX, ((1681.9999) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((1681.0000) * transformY) + moveY),

        new createjs.Point(((946.0000) * transformX) + moveX, ((1680.3334) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((1679.6666) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((1679.0000) * transformY) + moveY),

        new createjs.Point(((947.6665) * transformX) + moveX, ((1677.6668) * transformY) + moveY),
        new createjs.Point(((949.3335) * transformX) + moveX, ((1676.3332) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((1675.0000) * transformY) + moveY),

        new createjs.Point(((951.0000) * transformX) + moveX, ((1674.3334) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((1673.6666) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((1673.0000) * transformY) + moveY),

        new createjs.Point(((951.6666) * transformX) + moveX, ((1672.6667) * transformY) + moveY),
        new createjs.Point(((952.3334) * transformX) + moveX, ((1672.3333) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((1672.0000) * transformY) + moveY),

        new createjs.Point(((953.3333) * transformX) + moveX, ((1671.0001) * transformY) + moveY),
        new createjs.Point(((953.6667) * transformX) + moveX, ((1669.9999) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((1669.0000) * transformY) + moveY),

        new createjs.Point(((955.3332) * transformX) + moveX, ((1668.0001) * transformY) + moveY),
        new createjs.Point(((956.6668) * transformX) + moveX, ((1666.9999) * transformY) + moveY),
        new createjs.Point(((958.0000) * transformX) + moveX, ((1666.0000) * transformY) + moveY),

        new createjs.Point(((958.0000) * transformX) + moveX, ((1665.3334) * transformY) + moveY),
        new createjs.Point(((958.0000) * transformX) + moveX, ((1664.6666) * transformY) + moveY),
        new createjs.Point(((958.0000) * transformX) + moveX, ((1664.0000) * transformY) + moveY),

        new createjs.Point(((958.9999) * transformX) + moveX, ((1663.3334) * transformY) + moveY),
        new createjs.Point(((960.0001) * transformX) + moveX, ((1662.6666) * transformY) + moveY),
        new createjs.Point(((961.0000) * transformX) + moveX, ((1662.0000) * transformY) + moveY),

        new createjs.Point(((961.0000) * transformX) + moveX, ((1661.3334) * transformY) + moveY),
        new createjs.Point(((961.0000) * transformX) + moveX, ((1660.6666) * transformY) + moveY),
        new createjs.Point(((961.0000) * transformX) + moveX, ((1660.0000) * transformY) + moveY),

        new createjs.Point(((961.9999) * transformX) + moveX, ((1659.3334) * transformY) + moveY),
        new createjs.Point(((963.0001) * transformX) + moveX, ((1658.6666) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((1658.0000) * transformY) + moveY),

        new createjs.Point(((964.0000) * transformX) + moveX, ((1657.3334) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((1656.6666) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((1656.0000) * transformY) + moveY),

        new createjs.Point(((964.9999) * transformX) + moveX, ((1655.3334) * transformY) + moveY),
        new createjs.Point(((966.0001) * transformX) + moveX, ((1654.6666) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((1654.0000) * transformY) + moveY),

        new createjs.Point(((967.0000) * transformX) + moveX, ((1653.3334) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((1652.6666) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((1652.0000) * transformY) + moveY),

        new createjs.Point(((967.9999) * transformX) + moveX, ((1651.3334) * transformY) + moveY),
        new createjs.Point(((969.0001) * transformX) + moveX, ((1650.6666) * transformY) + moveY),
        new createjs.Point(((970.0000) * transformX) + moveX, ((1650.0000) * transformY) + moveY),

        new createjs.Point(((970.3333) * transformX) + moveX, ((1649.0001) * transformY) + moveY),
        new createjs.Point(((970.6667) * transformX) + moveX, ((1647.9999) * transformY) + moveY),
        new createjs.Point(((971.0000) * transformX) + moveX, ((1647.0000) * transformY) + moveY),

        new createjs.Point(((971.6666) * transformX) + moveX, ((1646.6667) * transformY) + moveY),
        new createjs.Point(((972.3334) * transformX) + moveX, ((1646.3333) * transformY) + moveY),
        new createjs.Point(((973.0000) * transformX) + moveX, ((1646.0000) * transformY) + moveY),

        new createjs.Point(((973.0000) * transformX) + moveX, ((1645.3334) * transformY) + moveY),
        new createjs.Point(((973.0000) * transformX) + moveX, ((1644.6666) * transformY) + moveY),
        new createjs.Point(((973.0000) * transformX) + moveX, ((1644.0000) * transformY) + moveY),

        new createjs.Point(((973.6666) * transformX) + moveX, ((1643.6667) * transformY) + moveY),
        new createjs.Point(((974.3334) * transformX) + moveX, ((1643.3333) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((1643.0000) * transformY) + moveY),

        new createjs.Point(((975.0000) * transformX) + moveX, ((1642.3334) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((1641.6666) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((1641.0000) * transformY) + moveY),

        new createjs.Point(((975.9999) * transformX) + moveX, ((1640.3334) * transformY) + moveY),
        new createjs.Point(((977.0001) * transformX) + moveX, ((1639.6666) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((1639.0000) * transformY) + moveY),

        new createjs.Point(((978.0000) * transformX) + moveX, ((1638.3334) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((1637.6666) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((1637.0000) * transformY) + moveY),

        new createjs.Point(((978.3333) * transformX) + moveX, ((1637.0000) * transformY) + moveY),
        new createjs.Point(((978.6667) * transformX) + moveX, ((1637.0000) * transformY) + moveY),
        new createjs.Point(((979.0000) * transformX) + moveX, ((1637.0000) * transformY) + moveY),

        new createjs.Point(((979.3333) * transformX) + moveX, ((1636.0001) * transformY) + moveY),
        new createjs.Point(((979.6667) * transformX) + moveX, ((1634.9999) * transformY) + moveY),
        new createjs.Point(((980.0000) * transformX) + moveX, ((1634.0000) * transformY) + moveY),

        new createjs.Point(((980.6666) * transformX) + moveX, ((1633.6667) * transformY) + moveY),
        new createjs.Point(((981.3334) * transformX) + moveX, ((1633.3333) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((1633.0000) * transformY) + moveY),

        new createjs.Point(((982.0000) * transformX) + moveX, ((1632.3334) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((1631.6666) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),

        new createjs.Point(((982.9999) * transformX) + moveX, ((1630.3334) * transformY) + moveY),
        new createjs.Point(((984.0001) * transformX) + moveX, ((1629.6666) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((1629.0000) * transformY) + moveY),

        new createjs.Point(((985.0000) * transformX) + moveX, ((1628.3334) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((1627.6666) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((1627.0000) * transformY) + moveY),

        new createjs.Point(((985.6666) * transformX) + moveX, ((1626.6667) * transformY) + moveY),
        new createjs.Point(((986.3334) * transformX) + moveX, ((1626.3333) * transformY) + moveY),
        new createjs.Point(((987.0000) * transformX) + moveX, ((1626.0000) * transformY) + moveY),

        new createjs.Point(((987.3333) * transformX) + moveX, ((1625.0001) * transformY) + moveY),
        new createjs.Point(((987.6667) * transformX) + moveX, ((1623.9999) * transformY) + moveY),
        new createjs.Point(((988.0000) * transformX) + moveX, ((1623.0000) * transformY) + moveY),

        new createjs.Point(((988.3333) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((988.6667) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((989.0000) * transformX) + moveX, ((1623.0000) * transformY) + moveY),

        new createjs.Point(((989.0000) * transformX) + moveX, ((1622.3334) * transformY) + moveY),
        new createjs.Point(((989.0000) * transformX) + moveX, ((1621.6666) * transformY) + moveY),
        new createjs.Point(((989.0000) * transformX) + moveX, ((1621.0000) * transformY) + moveY),

        new createjs.Point(((989.6666) * transformX) + moveX, ((1620.6667) * transformY) + moveY),
        new createjs.Point(((990.3334) * transformX) + moveX, ((1620.3333) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((1620.0000) * transformY) + moveY),

        new createjs.Point(((991.0000) * transformX) + moveX, ((1619.3334) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((1618.6666) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((1618.0000) * transformY) + moveY),

        new createjs.Point(((991.6666) * transformX) + moveX, ((1617.6667) * transformY) + moveY),
        new createjs.Point(((992.3334) * transformX) + moveX, ((1617.3333) * transformY) + moveY),
        new createjs.Point(((993.0000) * transformX) + moveX, ((1617.0000) * transformY) + moveY),

        new createjs.Point(((993.0000) * transformX) + moveX, ((1616.3334) * transformY) + moveY),
        new createjs.Point(((993.0000) * transformX) + moveX, ((1615.6666) * transformY) + moveY),
        new createjs.Point(((993.0000) * transformX) + moveX, ((1615.0000) * transformY) + moveY),

        new createjs.Point(((993.6666) * transformX) + moveX, ((1614.6667) * transformY) + moveY),
        new createjs.Point(((994.3334) * transformX) + moveX, ((1614.3333) * transformY) + moveY),
        new createjs.Point(((995.0000) * transformX) + moveX, ((1614.0000) * transformY) + moveY),

        new createjs.Point(((995.0000) * transformX) + moveX, ((1613.3334) * transformY) + moveY),
        new createjs.Point(((995.0000) * transformX) + moveX, ((1612.6666) * transformY) + moveY),
        new createjs.Point(((995.0000) * transformX) + moveX, ((1612.0000) * transformY) + moveY),

        new createjs.Point(((995.6666) * transformX) + moveX, ((1611.6667) * transformY) + moveY),
        new createjs.Point(((996.3334) * transformX) + moveX, ((1611.3333) * transformY) + moveY),
        new createjs.Point(((997.0000) * transformX) + moveX, ((1611.0000) * transformY) + moveY),

        new createjs.Point(((997.0000) * transformX) + moveX, ((1610.3334) * transformY) + moveY),
        new createjs.Point(((997.0000) * transformX) + moveX, ((1609.6666) * transformY) + moveY),
        new createjs.Point(((997.0000) * transformX) + moveX, ((1609.0000) * transformY) + moveY),

        new createjs.Point(((997.6666) * transformX) + moveX, ((1608.6667) * transformY) + moveY),
        new createjs.Point(((998.3334) * transformX) + moveX, ((1608.3333) * transformY) + moveY),
        new createjs.Point(((999.0000) * transformX) + moveX, ((1608.0000) * transformY) + moveY),

        new createjs.Point(((999.0000) * transformX) + moveX, ((1607.3334) * transformY) + moveY),
        new createjs.Point(((999.0000) * transformX) + moveX, ((1606.6666) * transformY) + moveY),
        new createjs.Point(((999.0000) * transformX) + moveX, ((1606.0000) * transformY) + moveY),

        new createjs.Point(((999.6666) * transformX) + moveX, ((1605.6667) * transformY) + moveY),
        new createjs.Point(((1000.3334) * transformX) + moveX, ((1605.3333) * transformY) + moveY),
        new createjs.Point(((1001.0000) * transformX) + moveX, ((1605.0000) * transformY) + moveY),

        new createjs.Point(((1001.3333) * transformX) + moveX, ((1603.6668) * transformY) + moveY),
        new createjs.Point(((1001.6667) * transformX) + moveX, ((1602.3332) * transformY) + moveY),
        new createjs.Point(((1002.0000) * transformX) + moveX, ((1601.0000) * transformY) + moveY),

        new createjs.Point(((1002.3333) * transformX) + moveX, ((1601.0000) * transformY) + moveY),
        new createjs.Point(((1002.6667) * transformX) + moveX, ((1601.0000) * transformY) + moveY),
        new createjs.Point(((1003.0000) * transformX) + moveX, ((1601.0000) * transformY) + moveY),

        new createjs.Point(((1003.3333) * transformX) + moveX, ((1600.0001) * transformY) + moveY),
        new createjs.Point(((1003.6667) * transformX) + moveX, ((1598.9999) * transformY) + moveY),
        new createjs.Point(((1004.0000) * transformX) + moveX, ((1598.0000) * transformY) + moveY),

        new createjs.Point(((1004.6666) * transformX) + moveX, ((1597.6667) * transformY) + moveY),
        new createjs.Point(((1005.3334) * transformX) + moveX, ((1597.3333) * transformY) + moveY),
        new createjs.Point(((1006.0000) * transformX) + moveX, ((1597.0000) * transformY) + moveY),

        new createjs.Point(((1006.0000) * transformX) + moveX, ((1596.3334) * transformY) + moveY),
        new createjs.Point(((1006.0000) * transformX) + moveX, ((1595.6666) * transformY) + moveY),
        new createjs.Point(((1006.0000) * transformX) + moveX, ((1595.0000) * transformY) + moveY),

        new createjs.Point(((1006.6666) * transformX) + moveX, ((1594.6667) * transformY) + moveY),
        new createjs.Point(((1007.3334) * transformX) + moveX, ((1594.3333) * transformY) + moveY),
        new createjs.Point(((1008.0000) * transformX) + moveX, ((1594.0000) * transformY) + moveY),

        new createjs.Point(((1008.0000) * transformX) + moveX, ((1593.3334) * transformY) + moveY),
        new createjs.Point(((1008.0000) * transformX) + moveX, ((1592.6666) * transformY) + moveY),
        new createjs.Point(((1008.0000) * transformX) + moveX, ((1592.0000) * transformY) + moveY),

        new createjs.Point(((1008.3333) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((1008.6667) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((1009.0000) * transformX) + moveX, ((1592.0000) * transformY) + moveY),

        new createjs.Point(((1009.0000) * transformX) + moveX, ((1591.3334) * transformY) + moveY),
        new createjs.Point(((1009.0000) * transformX) + moveX, ((1590.6666) * transformY) + moveY),
        new createjs.Point(((1009.0000) * transformX) + moveX, ((1590.0000) * transformY) + moveY),

        new createjs.Point(((1009.6666) * transformX) + moveX, ((1589.6667) * transformY) + moveY),
        new createjs.Point(((1010.3334) * transformX) + moveX, ((1589.3333) * transformY) + moveY),
        new createjs.Point(((1011.0000) * transformX) + moveX, ((1589.0000) * transformY) + moveY),

        new createjs.Point(((1011.3333) * transformX) + moveX, ((1587.6668) * transformY) + moveY),
        new createjs.Point(((1011.6667) * transformX) + moveX, ((1586.3332) * transformY) + moveY),
        new createjs.Point(((1012.0000) * transformX) + moveX, ((1585.0000) * transformY) + moveY),

        new createjs.Point(((1012.6666) * transformX) + moveX, ((1584.6667) * transformY) + moveY),
        new createjs.Point(((1013.3334) * transformX) + moveX, ((1584.3333) * transformY) + moveY),
        new createjs.Point(((1014.0000) * transformX) + moveX, ((1584.0000) * transformY) + moveY),

        new createjs.Point(((1014.3333) * transformX) + moveX, ((1582.6668) * transformY) + moveY),
        new createjs.Point(((1014.6667) * transformX) + moveX, ((1581.3332) * transformY) + moveY),
        new createjs.Point(((1015.0000) * transformX) + moveX, ((1580.0000) * transformY) + moveY),

        new createjs.Point(((1015.6666) * transformX) + moveX, ((1579.6667) * transformY) + moveY),
        new createjs.Point(((1016.3334) * transformX) + moveX, ((1579.3333) * transformY) + moveY),
        new createjs.Point(((1017.0000) * transformX) + moveX, ((1579.0000) * transformY) + moveY),

        new createjs.Point(((1017.3333) * transformX) + moveX, ((1577.6668) * transformY) + moveY),
        new createjs.Point(((1017.6667) * transformX) + moveX, ((1576.3332) * transformY) + moveY),
        new createjs.Point(((1018.0000) * transformX) + moveX, ((1575.0000) * transformY) + moveY),

        new createjs.Point(((1018.6666) * transformX) + moveX, ((1574.6667) * transformY) + moveY),
        new createjs.Point(((1019.3334) * transformX) + moveX, ((1574.3333) * transformY) + moveY),
        new createjs.Point(((1020.0000) * transformX) + moveX, ((1574.0000) * transformY) + moveY),

        new createjs.Point(((1020.3333) * transformX) + moveX, ((1572.6668) * transformY) + moveY),
        new createjs.Point(((1020.6667) * transformX) + moveX, ((1571.3332) * transformY) + moveY),
        new createjs.Point(((1021.0000) * transformX) + moveX, ((1570.0000) * transformY) + moveY),

        new createjs.Point(((1021.6666) * transformX) + moveX, ((1569.6667) * transformY) + moveY),
        new createjs.Point(((1022.3334) * transformX) + moveX, ((1569.3333) * transformY) + moveY),
        new createjs.Point(((1023.0000) * transformX) + moveX, ((1569.0000) * transformY) + moveY),

        new createjs.Point(((1023.6666) * transformX) + moveX, ((1567.0002) * transformY) + moveY),
        new createjs.Point(((1024.3334) * transformX) + moveX, ((1564.9998) * transformY) + moveY),
        new createjs.Point(((1025.0000) * transformX) + moveX, ((1563.0000) * transformY) + moveY),

        new createjs.Point(((1025.6666) * transformX) + moveX, ((1562.6667) * transformY) + moveY),
        new createjs.Point(((1026.3334) * transformX) + moveX, ((1562.3333) * transformY) + moveY),
        new createjs.Point(((1027.0000) * transformX) + moveX, ((1562.0000) * transformY) + moveY),

        new createjs.Point(((1027.0000) * transformX) + moveX, ((1561.3334) * transformY) + moveY),
        new createjs.Point(((1027.0000) * transformX) + moveX, ((1560.6666) * transformY) + moveY),
        new createjs.Point(((1027.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),

        new createjs.Point(((1027.3333) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((1027.6667) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((1028.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),

        new createjs.Point(((1028.0000) * transformX) + moveX, ((1559.3334) * transformY) + moveY),
        new createjs.Point(((1028.0000) * transformX) + moveX, ((1558.6666) * transformY) + moveY),
        new createjs.Point(((1028.0000) * transformX) + moveX, ((1558.0000) * transformY) + moveY),

        new createjs.Point(((1028.3333) * transformX) + moveX, ((1558.0000) * transformY) + moveY),
        new createjs.Point(((1028.6667) * transformX) + moveX, ((1558.0000) * transformY) + moveY),
        new createjs.Point(((1029.0000) * transformX) + moveX, ((1558.0000) * transformY) + moveY),

        new createjs.Point(((1029.0000) * transformX) + moveX, ((1557.3334) * transformY) + moveY),
        new createjs.Point(((1029.0000) * transformX) + moveX, ((1556.6666) * transformY) + moveY),
        new createjs.Point(((1029.0000) * transformX) + moveX, ((1556.0000) * transformY) + moveY),

        new createjs.Point(((1029.6666) * transformX) + moveX, ((1555.6667) * transformY) + moveY),
        new createjs.Point(((1030.3334) * transformX) + moveX, ((1555.3333) * transformY) + moveY),
        new createjs.Point(((1031.0000) * transformX) + moveX, ((1555.0000) * transformY) + moveY),

        new createjs.Point(((1031.6666) * transformX) + moveX, ((1553.0002) * transformY) + moveY),
        new createjs.Point(((1032.3334) * transformX) + moveX, ((1550.9998) * transformY) + moveY),
        new createjs.Point(((1033.0000) * transformX) + moveX, ((1549.0000) * transformY) + moveY),

        new createjs.Point(((1033.6666) * transformX) + moveX, ((1548.6667) * transformY) + moveY),
        new createjs.Point(((1034.3334) * transformX) + moveX, ((1548.3333) * transformY) + moveY),
        new createjs.Point(((1035.0000) * transformX) + moveX, ((1548.0000) * transformY) + moveY),

        new createjs.Point(((1035.0000) * transformX) + moveX, ((1547.3334) * transformY) + moveY),
        new createjs.Point(((1035.0000) * transformX) + moveX, ((1546.6666) * transformY) + moveY),
        new createjs.Point(((1035.0000) * transformX) + moveX, ((1546.0000) * transformY) + moveY),

        new createjs.Point(((1035.3333) * transformX) + moveX, ((1546.0000) * transformY) + moveY),
        new createjs.Point(((1035.6667) * transformX) + moveX, ((1546.0000) * transformY) + moveY),
        new createjs.Point(((1036.0000) * transformX) + moveX, ((1546.0000) * transformY) + moveY),

        new createjs.Point(((1036.0000) * transformX) + moveX, ((1545.3334) * transformY) + moveY),
        new createjs.Point(((1036.0000) * transformX) + moveX, ((1544.6666) * transformY) + moveY),
        new createjs.Point(((1036.0000) * transformX) + moveX, ((1544.0000) * transformY) + moveY),

        new createjs.Point(((1036.3333) * transformX) + moveX, ((1544.0000) * transformY) + moveY),
        new createjs.Point(((1036.6667) * transformX) + moveX, ((1544.0000) * transformY) + moveY),
        new createjs.Point(((1037.0000) * transformX) + moveX, ((1544.0000) * transformY) + moveY),

        new createjs.Point(((1037.0000) * transformX) + moveX, ((1543.3334) * transformY) + moveY),
        new createjs.Point(((1037.0000) * transformX) + moveX, ((1542.6666) * transformY) + moveY),
        new createjs.Point(((1037.0000) * transformX) + moveX, ((1542.0000) * transformY) + moveY),

        new createjs.Point(((1037.3333) * transformX) + moveX, ((1542.0000) * transformY) + moveY),
        new createjs.Point(((1037.6667) * transformX) + moveX, ((1542.0000) * transformY) + moveY),
        new createjs.Point(((1038.0000) * transformX) + moveX, ((1542.0000) * transformY) + moveY),

        new createjs.Point(((1038.0000) * transformX) + moveX, ((1541.3334) * transformY) + moveY),
        new createjs.Point(((1038.0000) * transformX) + moveX, ((1540.6666) * transformY) + moveY),
        new createjs.Point(((1038.0000) * transformX) + moveX, ((1540.0000) * transformY) + moveY),

        new createjs.Point(((1038.3333) * transformX) + moveX, ((1540.0000) * transformY) + moveY),
        new createjs.Point(((1038.6667) * transformX) + moveX, ((1540.0000) * transformY) + moveY),
        new createjs.Point(((1039.0000) * transformX) + moveX, ((1540.0000) * transformY) + moveY),

        new createjs.Point(((1039.3333) * transformX) + moveX, ((1538.6668) * transformY) + moveY),
        new createjs.Point(((1039.6667) * transformX) + moveX, ((1537.3332) * transformY) + moveY),
        new createjs.Point(((1040.0000) * transformX) + moveX, ((1536.0000) * transformY) + moveY),

        new createjs.Point(((1040.6666) * transformX) + moveX, ((1535.6667) * transformY) + moveY),
        new createjs.Point(((1041.3334) * transformX) + moveX, ((1535.3333) * transformY) + moveY),
        new createjs.Point(((1042.0000) * transformX) + moveX, ((1535.0000) * transformY) + moveY),

        new createjs.Point(((1042.6666) * transformX) + moveX, ((1533.0002) * transformY) + moveY),
        new createjs.Point(((1043.3334) * transformX) + moveX, ((1530.9998) * transformY) + moveY),
        new createjs.Point(((1044.0000) * transformX) + moveX, ((1529.0000) * transformY) + moveY),

        new createjs.Point(((1044.3333) * transformX) + moveX, ((1529.0000) * transformY) + moveY),
        new createjs.Point(((1044.6667) * transformX) + moveX, ((1529.0000) * transformY) + moveY),
        new createjs.Point(((1045.0000) * transformX) + moveX, ((1529.0000) * transformY) + moveY),

        new createjs.Point(((1045.3333) * transformX) + moveX, ((1527.6668) * transformY) + moveY),
        new createjs.Point(((1045.6667) * transformX) + moveX, ((1526.3332) * transformY) + moveY),
        new createjs.Point(((1046.0000) * transformX) + moveX, ((1525.0000) * transformY) + moveY),

        new createjs.Point(((1046.3333) * transformX) + moveX, ((1525.0000) * transformY) + moveY),
        new createjs.Point(((1046.6667) * transformX) + moveX, ((1525.0000) * transformY) + moveY),
        new createjs.Point(((1047.0000) * transformX) + moveX, ((1525.0000) * transformY) + moveY),

        new createjs.Point(((1047.3333) * transformX) + moveX, ((1523.6668) * transformY) + moveY),
        new createjs.Point(((1047.6667) * transformX) + moveX, ((1522.3332) * transformY) + moveY),
        new createjs.Point(((1048.0000) * transformX) + moveX, ((1521.0000) * transformY) + moveY),

        new createjs.Point(((1048.3333) * transformX) + moveX, ((1521.0000) * transformY) + moveY),
        new createjs.Point(((1048.6667) * transformX) + moveX, ((1521.0000) * transformY) + moveY),
        new createjs.Point(((1049.0000) * transformX) + moveX, ((1521.0000) * transformY) + moveY),

        new createjs.Point(((1049.0000) * transformX) + moveX, ((1520.3334) * transformY) + moveY),
        new createjs.Point(((1049.0000) * transformX) + moveX, ((1519.6666) * transformY) + moveY),
        new createjs.Point(((1049.0000) * transformX) + moveX, ((1519.0000) * transformY) + moveY),

        new createjs.Point(((1049.3333) * transformX) + moveX, ((1519.0000) * transformY) + moveY),
        new createjs.Point(((1049.6667) * transformX) + moveX, ((1519.0000) * transformY) + moveY),
        new createjs.Point(((1050.0000) * transformX) + moveX, ((1519.0000) * transformY) + moveY),

        new createjs.Point(((1050.0000) * transformX) + moveX, ((1518.3334) * transformY) + moveY),
        new createjs.Point(((1050.0000) * transformX) + moveX, ((1517.6666) * transformY) + moveY),
        new createjs.Point(((1050.0000) * transformX) + moveX, ((1517.0000) * transformY) + moveY),

        new createjs.Point(((1050.3333) * transformX) + moveX, ((1517.0000) * transformY) + moveY),
        new createjs.Point(((1050.6667) * transformX) + moveX, ((1517.0000) * transformY) + moveY),
        new createjs.Point(((1051.0000) * transformX) + moveX, ((1517.0000) * transformY) + moveY),

        new createjs.Point(((1051.0000) * transformX) + moveX, ((1516.3334) * transformY) + moveY),
        new createjs.Point(((1051.0000) * transformX) + moveX, ((1515.6666) * transformY) + moveY),
        new createjs.Point(((1051.0000) * transformX) + moveX, ((1515.0000) * transformY) + moveY),

        new createjs.Point(((1051.3333) * transformX) + moveX, ((1515.0000) * transformY) + moveY),
        new createjs.Point(((1051.6667) * transformX) + moveX, ((1515.0000) * transformY) + moveY),
        new createjs.Point(((1052.0000) * transformX) + moveX, ((1515.0000) * transformY) + moveY),

        new createjs.Point(((1052.0000) * transformX) + moveX, ((1514.3334) * transformY) + moveY),
        new createjs.Point(((1052.0000) * transformX) + moveX, ((1513.6666) * transformY) + moveY),
        new createjs.Point(((1052.0000) * transformX) + moveX, ((1513.0000) * transformY) + moveY),

        new createjs.Point(((1052.3333) * transformX) + moveX, ((1513.0000) * transformY) + moveY),
        new createjs.Point(((1052.6667) * transformX) + moveX, ((1513.0000) * transformY) + moveY),
        new createjs.Point(((1053.0000) * transformX) + moveX, ((1513.0000) * transformY) + moveY),

        new createjs.Point(((1053.0000) * transformX) + moveX, ((1512.3334) * transformY) + moveY),
        new createjs.Point(((1053.0000) * transformX) + moveX, ((1511.6666) * transformY) + moveY),
        new createjs.Point(((1053.0000) * transformX) + moveX, ((1511.0000) * transformY) + moveY),

        new createjs.Point(((1053.3333) * transformX) + moveX, ((1511.0000) * transformY) + moveY),
        new createjs.Point(((1053.6667) * transformX) + moveX, ((1511.0000) * transformY) + moveY),
        new createjs.Point(((1054.0000) * transformX) + moveX, ((1511.0000) * transformY) + moveY),

        new createjs.Point(((1054.6666) * transformX) + moveX, ((1509.0002) * transformY) + moveY),
        new createjs.Point(((1055.3334) * transformX) + moveX, ((1506.9998) * transformY) + moveY),
        new createjs.Point(((1056.0000) * transformX) + moveX, ((1505.0000) * transformY) + moveY),

        new createjs.Point(((1056.3333) * transformX) + moveX, ((1505.0000) * transformY) + moveY),
        new createjs.Point(((1056.6667) * transformX) + moveX, ((1505.0000) * transformY) + moveY),
        new createjs.Point(((1057.0000) * transformX) + moveX, ((1505.0000) * transformY) + moveY),

        new createjs.Point(((1057.3333) * transformX) + moveX, ((1503.6668) * transformY) + moveY),
        new createjs.Point(((1057.6667) * transformX) + moveX, ((1502.3332) * transformY) + moveY),
        new createjs.Point(((1058.0000) * transformX) + moveX, ((1501.0000) * transformY) + moveY),

        new createjs.Point(((1058.3333) * transformX) + moveX, ((1501.0000) * transformY) + moveY),
        new createjs.Point(((1058.6667) * transformX) + moveX, ((1501.0000) * transformY) + moveY),
        new createjs.Point(((1059.0000) * transformX) + moveX, ((1501.0000) * transformY) + moveY),

        new createjs.Point(((1059.3333) * transformX) + moveX, ((1499.6668) * transformY) + moveY),
        new createjs.Point(((1059.6667) * transformX) + moveX, ((1498.3332) * transformY) + moveY),
        new createjs.Point(((1060.0000) * transformX) + moveX, ((1497.0000) * transformY) + moveY),

        new createjs.Point(((1060.3333) * transformX) + moveX, ((1497.0000) * transformY) + moveY),
        new createjs.Point(((1060.6667) * transformX) + moveX, ((1497.0000) * transformY) + moveY),
        new createjs.Point(((1061.0000) * transformX) + moveX, ((1497.0000) * transformY) + moveY),

        new createjs.Point(((1061.0000) * transformX) + moveX, ((1496.3334) * transformY) + moveY),
        new createjs.Point(((1061.0000) * transformX) + moveX, ((1495.6666) * transformY) + moveY),
        new createjs.Point(((1061.0000) * transformX) + moveX, ((1495.0000) * transformY) + moveY),

        new createjs.Point(((1061.3333) * transformX) + moveX, ((1495.0000) * transformY) + moveY),
        new createjs.Point(((1061.6667) * transformX) + moveX, ((1495.0000) * transformY) + moveY),
        new createjs.Point(((1062.0000) * transformX) + moveX, ((1495.0000) * transformY) + moveY),

        new createjs.Point(((1062.0000) * transformX) + moveX, ((1494.3334) * transformY) + moveY),
        new createjs.Point(((1062.0000) * transformX) + moveX, ((1493.6666) * transformY) + moveY),
        new createjs.Point(((1062.0000) * transformX) + moveX, ((1493.0000) * transformY) + moveY),

        new createjs.Point(((1062.3333) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((1062.6667) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((1063.0000) * transformX) + moveX, ((1493.0000) * transformY) + moveY),

        new createjs.Point(((1063.0000) * transformX) + moveX, ((1492.3334) * transformY) + moveY),
        new createjs.Point(((1063.0000) * transformX) + moveX, ((1491.6666) * transformY) + moveY),
        new createjs.Point(((1063.0000) * transformX) + moveX, ((1491.0000) * transformY) + moveY),

        new createjs.Point(((1063.3333) * transformX) + moveX, ((1491.0000) * transformY) + moveY),
        new createjs.Point(((1063.6667) * transformX) + moveX, ((1491.0000) * transformY) + moveY),
        new createjs.Point(((1064.0000) * transformX) + moveX, ((1491.0000) * transformY) + moveY),

        new createjs.Point(((1064.0000) * transformX) + moveX, ((1490.3334) * transformY) + moveY),
        new createjs.Point(((1064.0000) * transformX) + moveX, ((1489.6666) * transformY) + moveY),
        new createjs.Point(((1064.0000) * transformX) + moveX, ((1489.0000) * transformY) + moveY),

        new createjs.Point(((1064.3333) * transformX) + moveX, ((1489.0000) * transformY) + moveY),
        new createjs.Point(((1064.6667) * transformX) + moveX, ((1489.0000) * transformY) + moveY),
        new createjs.Point(((1065.0000) * transformX) + moveX, ((1489.0000) * transformY) + moveY),

        new createjs.Point(((1065.0000) * transformX) + moveX, ((1488.3334) * transformY) + moveY),
        new createjs.Point(((1065.0000) * transformX) + moveX, ((1487.6666) * transformY) + moveY),
        new createjs.Point(((1065.0000) * transformX) + moveX, ((1487.0000) * transformY) + moveY),

        new createjs.Point(((1065.3333) * transformX) + moveX, ((1487.0000) * transformY) + moveY),
        new createjs.Point(((1065.6667) * transformX) + moveX, ((1487.0000) * transformY) + moveY),
        new createjs.Point(((1066.0000) * transformX) + moveX, ((1487.0000) * transformY) + moveY),

        new createjs.Point(((1066.3333) * transformX) + moveX, ((1485.6668) * transformY) + moveY),
        new createjs.Point(((1066.6667) * transformX) + moveX, ((1484.3332) * transformY) + moveY),
        new createjs.Point(((1067.0000) * transformX) + moveX, ((1483.0000) * transformY) + moveY),

        new createjs.Point(((1067.3333) * transformX) + moveX, ((1483.0000) * transformY) + moveY),
        new createjs.Point(((1067.6667) * transformX) + moveX, ((1483.0000) * transformY) + moveY),
        new createjs.Point(((1068.0000) * transformX) + moveX, ((1483.0000) * transformY) + moveY),

        new createjs.Point(((1068.0000) * transformX) + moveX, ((1482.3334) * transformY) + moveY),
        new createjs.Point(((1068.0000) * transformX) + moveX, ((1481.6666) * transformY) + moveY),
        new createjs.Point(((1068.0000) * transformX) + moveX, ((1481.0000) * transformY) + moveY),

        new createjs.Point(((1068.3333) * transformX) + moveX, ((1481.0000) * transformY) + moveY),
        new createjs.Point(((1068.6667) * transformX) + moveX, ((1481.0000) * transformY) + moveY),
        new createjs.Point(((1069.0000) * transformX) + moveX, ((1481.0000) * transformY) + moveY),

        new createjs.Point(((1069.0000) * transformX) + moveX, ((1480.0001) * transformY) + moveY),
        new createjs.Point(((1069.0000) * transformX) + moveX, ((1478.9999) * transformY) + moveY),
        new createjs.Point(((1069.0000) * transformX) + moveX, ((1478.0000) * transformY) + moveY),

        new createjs.Point(((1069.3333) * transformX) + moveX, ((1478.0000) * transformY) + moveY),
        new createjs.Point(((1069.6667) * transformX) + moveX, ((1478.0000) * transformY) + moveY),
        new createjs.Point(((1070.0000) * transformX) + moveX, ((1478.0000) * transformY) + moveY),

        new createjs.Point(((1070.0000) * transformX) + moveX, ((1477.3334) * transformY) + moveY),
        new createjs.Point(((1070.0000) * transformX) + moveX, ((1476.6666) * transformY) + moveY),
        new createjs.Point(((1070.0000) * transformX) + moveX, ((1476.0000) * transformY) + moveY),

        new createjs.Point(((1070.3333) * transformX) + moveX, ((1476.0000) * transformY) + moveY),
        new createjs.Point(((1070.6667) * transformX) + moveX, ((1476.0000) * transformY) + moveY),
        new createjs.Point(((1071.0000) * transformX) + moveX, ((1476.0000) * transformY) + moveY),

        new createjs.Point(((1071.6666) * transformX) + moveX, ((1474.0002) * transformY) + moveY),
        new createjs.Point(((1072.3334) * transformX) + moveX, ((1471.9998) * transformY) + moveY),
        new createjs.Point(((1073.0000) * transformX) + moveX, ((1470.0000) * transformY) + moveY),

        new createjs.Point(((1073.3333) * transformX) + moveX, ((1470.0000) * transformY) + moveY),
        new createjs.Point(((1073.6667) * transformX) + moveX, ((1470.0000) * transformY) + moveY),
        new createjs.Point(((1074.0000) * transformX) + moveX, ((1470.0000) * transformY) + moveY),

        new createjs.Point(((1074.0000) * transformX) + moveX, ((1469.0001) * transformY) + moveY),
        new createjs.Point(((1074.0000) * transformX) + moveX, ((1467.9999) * transformY) + moveY),
        new createjs.Point(((1074.0000) * transformX) + moveX, ((1467.0000) * transformY) + moveY),

        new createjs.Point(((1074.3333) * transformX) + moveX, ((1467.0000) * transformY) + moveY),
        new createjs.Point(((1074.6667) * transformX) + moveX, ((1467.0000) * transformY) + moveY),
        new createjs.Point(((1075.0000) * transformX) + moveX, ((1467.0000) * transformY) + moveY),

        new createjs.Point(((1075.0000) * transformX) + moveX, ((1466.3334) * transformY) + moveY),
        new createjs.Point(((1075.0000) * transformX) + moveX, ((1465.6666) * transformY) + moveY),
        new createjs.Point(((1075.0000) * transformX) + moveX, ((1465.0000) * transformY) + moveY),

        new createjs.Point(((1075.3333) * transformX) + moveX, ((1465.0000) * transformY) + moveY),
        new createjs.Point(((1075.6667) * transformX) + moveX, ((1465.0000) * transformY) + moveY),
        new createjs.Point(((1076.0000) * transformX) + moveX, ((1465.0000) * transformY) + moveY),

        new createjs.Point(((1076.3333) * transformX) + moveX, ((1463.6668) * transformY) + moveY),
        new createjs.Point(((1076.6667) * transformX) + moveX, ((1462.3332) * transformY) + moveY),
        new createjs.Point(((1077.0000) * transformX) + moveX, ((1461.0000) * transformY) + moveY),

        new createjs.Point(((1077.3333) * transformX) + moveX, ((1461.0000) * transformY) + moveY),
        new createjs.Point(((1077.6667) * transformX) + moveX, ((1461.0000) * transformY) + moveY),
        new createjs.Point(((1078.0000) * transformX) + moveX, ((1461.0000) * transformY) + moveY),

        new createjs.Point(((1078.0000) * transformX) + moveX, ((1460.0001) * transformY) + moveY),
        new createjs.Point(((1078.0000) * transformX) + moveX, ((1458.9999) * transformY) + moveY),
        new createjs.Point(((1078.0000) * transformX) + moveX, ((1458.0000) * transformY) + moveY),

        new createjs.Point(((1078.3333) * transformX) + moveX, ((1458.0000) * transformY) + moveY),
        new createjs.Point(((1078.6667) * transformX) + moveX, ((1458.0000) * transformY) + moveY),
        new createjs.Point(((1079.0000) * transformX) + moveX, ((1458.0000) * transformY) + moveY),

        new createjs.Point(((1079.0000) * transformX) + moveX, ((1457.3334) * transformY) + moveY),
        new createjs.Point(((1079.0000) * transformX) + moveX, ((1456.6666) * transformY) + moveY),
        new createjs.Point(((1079.0000) * transformX) + moveX, ((1456.0000) * transformY) + moveY),

        new createjs.Point(((1079.3333) * transformX) + moveX, ((1456.0000) * transformY) + moveY),
        new createjs.Point(((1079.6667) * transformX) + moveX, ((1456.0000) * transformY) + moveY),
        new createjs.Point(((1080.0000) * transformX) + moveX, ((1456.0000) * transformY) + moveY),

        new createjs.Point(((1080.3333) * transformX) + moveX, ((1454.6668) * transformY) + moveY),
        new createjs.Point(((1080.6667) * transformX) + moveX, ((1453.3332) * transformY) + moveY),
        new createjs.Point(((1081.0000) * transformX) + moveX, ((1452.0000) * transformY) + moveY),

        new createjs.Point(((1081.3333) * transformX) + moveX, ((1452.0000) * transformY) + moveY),
        new createjs.Point(((1081.6667) * transformX) + moveX, ((1452.0000) * transformY) + moveY),
        new createjs.Point(((1082.0000) * transformX) + moveX, ((1452.0000) * transformY) + moveY),

        new createjs.Point(((1082.0000) * transformX) + moveX, ((1451.0001) * transformY) + moveY),
        new createjs.Point(((1082.0000) * transformX) + moveX, ((1449.9999) * transformY) + moveY),
        new createjs.Point(((1082.0000) * transformX) + moveX, ((1449.0000) * transformY) + moveY),

        new createjs.Point(((1082.3333) * transformX) + moveX, ((1449.0000) * transformY) + moveY),
        new createjs.Point(((1082.6667) * transformX) + moveX, ((1449.0000) * transformY) + moveY),
        new createjs.Point(((1083.0000) * transformX) + moveX, ((1449.0000) * transformY) + moveY),

        new createjs.Point(((1083.3333) * transformX) + moveX, ((1447.6668) * transformY) + moveY),
        new createjs.Point(((1083.6667) * transformX) + moveX, ((1446.3332) * transformY) + moveY),
        new createjs.Point(((1084.0000) * transformX) + moveX, ((1445.0000) * transformY) + moveY),

        new createjs.Point(((1084.3333) * transformX) + moveX, ((1445.0000) * transformY) + moveY),
        new createjs.Point(((1084.6667) * transformX) + moveX, ((1445.0000) * transformY) + moveY),
        new createjs.Point(((1085.0000) * transformX) + moveX, ((1445.0000) * transformY) + moveY),

        new createjs.Point(((1085.0000) * transformX) + moveX, ((1444.0001) * transformY) + moveY),
        new createjs.Point(((1085.0000) * transformX) + moveX, ((1442.9999) * transformY) + moveY),
        new createjs.Point(((1085.0000) * transformX) + moveX, ((1442.0000) * transformY) + moveY),

        new createjs.Point(((1085.3333) * transformX) + moveX, ((1442.0000) * transformY) + moveY),
        new createjs.Point(((1085.6667) * transformX) + moveX, ((1442.0000) * transformY) + moveY),
        new createjs.Point(((1086.0000) * transformX) + moveX, ((1442.0000) * transformY) + moveY),

        new createjs.Point(((1086.3333) * transformX) + moveX, ((1440.6668) * transformY) + moveY),
        new createjs.Point(((1086.6667) * transformX) + moveX, ((1439.3332) * transformY) + moveY),
        new createjs.Point(((1087.0000) * transformX) + moveX, ((1438.0000) * transformY) + moveY),

        new createjs.Point(((1087.3333) * transformX) + moveX, ((1438.0000) * transformY) + moveY),
        new createjs.Point(((1087.6667) * transformX) + moveX, ((1438.0000) * transformY) + moveY),
        new createjs.Point(((1088.0000) * transformX) + moveX, ((1438.0000) * transformY) + moveY),

        new createjs.Point(((1088.0000) * transformX) + moveX, ((1437.0001) * transformY) + moveY),
        new createjs.Point(((1088.0000) * transformX) + moveX, ((1435.9999) * transformY) + moveY),
        new createjs.Point(((1088.0000) * transformX) + moveX, ((1435.0000) * transformY) + moveY),

        new createjs.Point(((1088.3333) * transformX) + moveX, ((1435.0000) * transformY) + moveY),
        new createjs.Point(((1088.6667) * transformX) + moveX, ((1435.0000) * transformY) + moveY),
        new createjs.Point(((1089.0000) * transformX) + moveX, ((1435.0000) * transformY) + moveY),

        new createjs.Point(((1089.3333) * transformX) + moveX, ((1433.3335) * transformY) + moveY),
        new createjs.Point(((1089.6667) * transformX) + moveX, ((1431.6665) * transformY) + moveY),
        new createjs.Point(((1090.0000) * transformX) + moveX, ((1430.0000) * transformY) + moveY),

        new createjs.Point(((1090.3333) * transformX) + moveX, ((1430.0000) * transformY) + moveY),
        new createjs.Point(((1090.6667) * transformX) + moveX, ((1430.0000) * transformY) + moveY),
        new createjs.Point(((1091.0000) * transformX) + moveX, ((1430.0000) * transformY) + moveY),

        new createjs.Point(((1091.3333) * transformX) + moveX, ((1428.6668) * transformY) + moveY),
        new createjs.Point(((1091.6667) * transformX) + moveX, ((1427.3332) * transformY) + moveY),
        new createjs.Point(((1092.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),

        new createjs.Point(((1092.3333) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((1092.6667) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((1093.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),

        new createjs.Point(((1093.0000) * transformX) + moveX, ((1425.0001) * transformY) + moveY),
        new createjs.Point(((1093.0000) * transformX) + moveX, ((1423.9999) * transformY) + moveY),
        new createjs.Point(((1093.0000) * transformX) + moveX, ((1423.0000) * transformY) + moveY),

        new createjs.Point(((1093.3333) * transformX) + moveX, ((1423.0000) * transformY) + moveY),
        new createjs.Point(((1093.6667) * transformX) + moveX, ((1423.0000) * transformY) + moveY),
        new createjs.Point(((1094.0000) * transformX) + moveX, ((1423.0000) * transformY) + moveY),

        new createjs.Point(((1094.0000) * transformX) + moveX, ((1422.3334) * transformY) + moveY),
        new createjs.Point(((1094.0000) * transformX) + moveX, ((1421.6666) * transformY) + moveY),
        new createjs.Point(((1094.0000) * transformX) + moveX, ((1421.0000) * transformY) + moveY),

        new createjs.Point(((1094.3333) * transformX) + moveX, ((1421.0000) * transformY) + moveY),
        new createjs.Point(((1094.6667) * transformX) + moveX, ((1421.0000) * transformY) + moveY),
        new createjs.Point(((1095.0000) * transformX) + moveX, ((1421.0000) * transformY) + moveY),

        new createjs.Point(((1095.0000) * transformX) + moveX, ((1420.0001) * transformY) + moveY),
        new createjs.Point(((1095.0000) * transformX) + moveX, ((1418.9999) * transformY) + moveY),
        new createjs.Point(((1095.0000) * transformX) + moveX, ((1418.0000) * transformY) + moveY),

        new createjs.Point(((1095.3333) * transformX) + moveX, ((1418.0000) * transformY) + moveY),
        new createjs.Point(((1095.6667) * transformX) + moveX, ((1418.0000) * transformY) + moveY),
        new createjs.Point(((1096.0000) * transformX) + moveX, ((1418.0000) * transformY) + moveY),

        new createjs.Point(((1096.0000) * transformX) + moveX, ((1417.3334) * transformY) + moveY),
        new createjs.Point(((1096.0000) * transformX) + moveX, ((1416.6666) * transformY) + moveY),
        new createjs.Point(((1096.0000) * transformX) + moveX, ((1416.0000) * transformY) + moveY),

        new createjs.Point(((1096.3333) * transformX) + moveX, ((1416.0000) * transformY) + moveY),
        new createjs.Point(((1096.6667) * transformX) + moveX, ((1416.0000) * transformY) + moveY),
        new createjs.Point(((1097.0000) * transformX) + moveX, ((1416.0000) * transformY) + moveY),

        new createjs.Point(((1097.0000) * transformX) + moveX, ((1415.0001) * transformY) + moveY),
        new createjs.Point(((1097.0000) * transformX) + moveX, ((1413.9999) * transformY) + moveY),
        new createjs.Point(((1097.0000) * transformX) + moveX, ((1413.0000) * transformY) + moveY),

        new createjs.Point(((1097.3333) * transformX) + moveX, ((1413.0000) * transformY) + moveY),
        new createjs.Point(((1097.6667) * transformX) + moveX, ((1413.0000) * transformY) + moveY),
        new createjs.Point(((1098.0000) * transformX) + moveX, ((1413.0000) * transformY) + moveY),

        new createjs.Point(((1098.0000) * transformX) + moveX, ((1412.3334) * transformY) + moveY),
        new createjs.Point(((1098.0000) * transformX) + moveX, ((1411.6666) * transformY) + moveY),
        new createjs.Point(((1098.0000) * transformX) + moveX, ((1411.0000) * transformY) + moveY),

        new createjs.Point(((1098.3333) * transformX) + moveX, ((1411.0000) * transformY) + moveY),
        new createjs.Point(((1098.6667) * transformX) + moveX, ((1411.0000) * transformY) + moveY),
        new createjs.Point(((1099.0000) * transformX) + moveX, ((1411.0000) * transformY) + moveY),

        new createjs.Point(((1099.3333) * transformX) + moveX, ((1409.0002) * transformY) + moveY),
        new createjs.Point(((1099.6667) * transformX) + moveX, ((1406.9998) * transformY) + moveY),
        new createjs.Point(((1100.0000) * transformX) + moveX, ((1405.0000) * transformY) + moveY),

        new createjs.Point(((1100.3333) * transformX) + moveX, ((1405.0000) * transformY) + moveY),
        new createjs.Point(((1100.6667) * transformX) + moveX, ((1405.0000) * transformY) + moveY),
        new createjs.Point(((1101.0000) * transformX) + moveX, ((1405.0000) * transformY) + moveY),

        new createjs.Point(((1101.0000) * transformX) + moveX, ((1404.3334) * transformY) + moveY),
        new createjs.Point(((1101.0000) * transformX) + moveX, ((1403.6666) * transformY) + moveY),
        new createjs.Point(((1101.0000) * transformX) + moveX, ((1403.0000) * transformY) + moveY),

        new createjs.Point(((1101.3333) * transformX) + moveX, ((1403.0000) * transformY) + moveY),
        new createjs.Point(((1101.6667) * transformX) + moveX, ((1403.0000) * transformY) + moveY),
        new createjs.Point(((1102.0000) * transformX) + moveX, ((1403.0000) * transformY) + moveY),

        new createjs.Point(((1102.0000) * transformX) + moveX, ((1402.0001) * transformY) + moveY),
        new createjs.Point(((1102.0000) * transformX) + moveX, ((1400.9999) * transformY) + moveY),
        new createjs.Point(((1102.0000) * transformX) + moveX, ((1400.0000) * transformY) + moveY),

        new createjs.Point(((1102.3333) * transformX) + moveX, ((1400.0000) * transformY) + moveY),
        new createjs.Point(((1102.6667) * transformX) + moveX, ((1400.0000) * transformY) + moveY),
        new createjs.Point(((1103.0000) * transformX) + moveX, ((1400.0000) * transformY) + moveY),

        new createjs.Point(((1103.0000) * transformX) + moveX, ((1399.3334) * transformY) + moveY),
        new createjs.Point(((1103.0000) * transformX) + moveX, ((1398.6666) * transformY) + moveY),
        new createjs.Point(((1103.0000) * transformX) + moveX, ((1398.0000) * transformY) + moveY),

        new createjs.Point(((1103.3333) * transformX) + moveX, ((1398.0000) * transformY) + moveY),
        new createjs.Point(((1103.6667) * transformX) + moveX, ((1398.0000) * transformY) + moveY),
        new createjs.Point(((1104.0000) * transformX) + moveX, ((1398.0000) * transformY) + moveY),

        new createjs.Point(((1104.0000) * transformX) + moveX, ((1397.0001) * transformY) + moveY),
        new createjs.Point(((1104.0000) * transformX) + moveX, ((1395.9999) * transformY) + moveY),
        new createjs.Point(((1104.0000) * transformX) + moveX, ((1395.0000) * transformY) + moveY),

        new createjs.Point(((1104.3333) * transformX) + moveX, ((1395.0000) * transformY) + moveY),
        new createjs.Point(((1104.6667) * transformX) + moveX, ((1395.0000) * transformY) + moveY),
        new createjs.Point(((1105.0000) * transformX) + moveX, ((1395.0000) * transformY) + moveY),

        new createjs.Point(((1105.0000) * transformX) + moveX, ((1394.0001) * transformY) + moveY),
        new createjs.Point(((1105.0000) * transformX) + moveX, ((1392.9999) * transformY) + moveY),
        new createjs.Point(((1105.0000) * transformX) + moveX, ((1392.0000) * transformY) + moveY),

        new createjs.Point(((1116.6305) * transformX) + moveX, ((1365.4354) * transformY) + moveY),
        new createjs.Point(((1124.8692) * transformX) + moveX, ((1335.0262) * transformY) + moveY),
        new createjs.Point(((1135.0000) * transformX) + moveX, ((1307.0000) * transformY) + moveY),

        new createjs.Point(((1135.0000) * transformX) + moveX, ((1305.6668) * transformY) + moveY),
        new createjs.Point(((1135.0000) * transformX) + moveX, ((1304.3332) * transformY) + moveY),
        new createjs.Point(((1135.0000) * transformX) + moveX, ((1303.0000) * transformY) + moveY),

        new createjs.Point(((1135.3333) * transformX) + moveX, ((1303.0000) * transformY) + moveY),
        new createjs.Point(((1135.6667) * transformX) + moveX, ((1303.0000) * transformY) + moveY),
        new createjs.Point(((1136.0000) * transformX) + moveX, ((1303.0000) * transformY) + moveY),

        new createjs.Point(((1136.0000) * transformX) + moveX, ((1302.0001) * transformY) + moveY),
        new createjs.Point(((1136.0000) * transformX) + moveX, ((1300.9999) * transformY) + moveY),
        new createjs.Point(((1136.0000) * transformX) + moveX, ((1300.0000) * transformY) + moveY),

        new createjs.Point(((1136.3333) * transformX) + moveX, ((1300.0000) * transformY) + moveY),
        new createjs.Point(((1136.6667) * transformX) + moveX, ((1300.0000) * transformY) + moveY),
        new createjs.Point(((1137.0000) * transformX) + moveX, ((1300.0000) * transformY) + moveY),

        new createjs.Point(((1137.0000) * transformX) + moveX, ((1298.6668) * transformY) + moveY),
        new createjs.Point(((1137.0000) * transformX) + moveX, ((1297.3332) * transformY) + moveY),
        new createjs.Point(((1137.0000) * transformX) + moveX, ((1296.0000) * transformY) + moveY),

        new createjs.Point(((1137.3333) * transformX) + moveX, ((1296.0000) * transformY) + moveY),
        new createjs.Point(((1137.6667) * transformX) + moveX, ((1296.0000) * transformY) + moveY),
        new createjs.Point(((1138.0000) * transformX) + moveX, ((1296.0000) * transformY) + moveY),

        new createjs.Point(((1138.0000) * transformX) + moveX, ((1295.0001) * transformY) + moveY),
        new createjs.Point(((1138.0000) * transformX) + moveX, ((1293.9999) * transformY) + moveY),
        new createjs.Point(((1138.0000) * transformX) + moveX, ((1293.0000) * transformY) + moveY),

        new createjs.Point(((1138.3333) * transformX) + moveX, ((1293.0000) * transformY) + moveY),
        new createjs.Point(((1138.6667) * transformX) + moveX, ((1293.0000) * transformY) + moveY),
        new createjs.Point(((1139.0000) * transformX) + moveX, ((1293.0000) * transformY) + moveY),

        new createjs.Point(((1139.0000) * transformX) + moveX, ((1292.0001) * transformY) + moveY),
        new createjs.Point(((1139.0000) * transformX) + moveX, ((1290.9999) * transformY) + moveY),
        new createjs.Point(((1139.0000) * transformX) + moveX, ((1290.0000) * transformY) + moveY),

        new createjs.Point(((1139.3333) * transformX) + moveX, ((1290.0000) * transformY) + moveY),
        new createjs.Point(((1139.6667) * transformX) + moveX, ((1290.0000) * transformY) + moveY),
        new createjs.Point(((1140.0000) * transformX) + moveX, ((1290.0000) * transformY) + moveY),

        new createjs.Point(((1140.3333) * transformX) + moveX, ((1287.6669) * transformY) + moveY),
        new createjs.Point(((1140.6667) * transformX) + moveX, ((1285.3331) * transformY) + moveY),
        new createjs.Point(((1141.0000) * transformX) + moveX, ((1283.0000) * transformY) + moveY),

        new createjs.Point(((1141.3333) * transformX) + moveX, ((1283.0000) * transformY) + moveY),
        new createjs.Point(((1141.6667) * transformX) + moveX, ((1283.0000) * transformY) + moveY),
        new createjs.Point(((1142.0000) * transformX) + moveX, ((1283.0000) * transformY) + moveY),

        new createjs.Point(((1142.3333) * transformX) + moveX, ((1280.6669) * transformY) + moveY),
        new createjs.Point(((1142.6667) * transformX) + moveX, ((1278.3331) * transformY) + moveY),
        new createjs.Point(((1143.0000) * transformX) + moveX, ((1276.0000) * transformY) + moveY),

        new createjs.Point(((1143.3333) * transformX) + moveX, ((1276.0000) * transformY) + moveY),
        new createjs.Point(((1143.6667) * transformX) + moveX, ((1276.0000) * transformY) + moveY),
        new createjs.Point(((1144.0000) * transformX) + moveX, ((1276.0000) * transformY) + moveY),

        new createjs.Point(((1144.6666) * transformX) + moveX, ((1272.3337) * transformY) + moveY),
        new createjs.Point(((1145.3334) * transformX) + moveX, ((1268.6663) * transformY) + moveY),
        new createjs.Point(((1146.0000) * transformX) + moveX, ((1265.0000) * transformY) + moveY),

        new createjs.Point(((1146.3333) * transformX) + moveX, ((1265.0000) * transformY) + moveY),
        new createjs.Point(((1146.6667) * transformX) + moveX, ((1265.0000) * transformY) + moveY),
        new createjs.Point(((1147.0000) * transformX) + moveX, ((1265.0000) * transformY) + moveY),

        new createjs.Point(((1147.0000) * transformX) + moveX, ((1263.6668) * transformY) + moveY),
        new createjs.Point(((1147.0000) * transformX) + moveX, ((1262.3332) * transformY) + moveY),
        new createjs.Point(((1147.0000) * transformX) + moveX, ((1261.0000) * transformY) + moveY),

        new createjs.Point(((1147.3333) * transformX) + moveX, ((1261.0000) * transformY) + moveY),
        new createjs.Point(((1147.6667) * transformX) + moveX, ((1261.0000) * transformY) + moveY),
        new createjs.Point(((1148.0000) * transformX) + moveX, ((1261.0000) * transformY) + moveY),

        new createjs.Point(((1148.0000) * transformX) + moveX, ((1259.6668) * transformY) + moveY),
        new createjs.Point(((1148.0000) * transformX) + moveX, ((1258.3332) * transformY) + moveY),
        new createjs.Point(((1148.0000) * transformX) + moveX, ((1257.0000) * transformY) + moveY),

        new createjs.Point(((1148.3333) * transformX) + moveX, ((1257.0000) * transformY) + moveY),
        new createjs.Point(((1148.6667) * transformX) + moveX, ((1257.0000) * transformY) + moveY),
        new createjs.Point(((1149.0000) * transformX) + moveX, ((1257.0000) * transformY) + moveY),

        new createjs.Point(((1149.0000) * transformX) + moveX, ((1255.6668) * transformY) + moveY),
        new createjs.Point(((1149.0000) * transformX) + moveX, ((1254.3332) * transformY) + moveY),
        new createjs.Point(((1149.0000) * transformX) + moveX, ((1253.0000) * transformY) + moveY),

        new createjs.Point(((1149.3333) * transformX) + moveX, ((1253.0000) * transformY) + moveY),
        new createjs.Point(((1149.6667) * transformX) + moveX, ((1253.0000) * transformY) + moveY),
        new createjs.Point(((1150.0000) * transformX) + moveX, ((1253.0000) * transformY) + moveY),

        new createjs.Point(((1150.0000) * transformX) + moveX, ((1251.6668) * transformY) + moveY),
        new createjs.Point(((1150.0000) * transformX) + moveX, ((1250.3332) * transformY) + moveY),
        new createjs.Point(((1150.0000) * transformX) + moveX, ((1249.0000) * transformY) + moveY),

        new createjs.Point(((1157.4181) * transformX) + moveX, ((1228.3790) * transformY) + moveY),
        new createjs.Point(((1160.1294) * transformX) + moveX, ((1203.3694) * transformY) + moveY),
        new createjs.Point(((1167.0000) * transformX) + moveX, ((1182.0000) * transformY) + moveY),

        new createjs.Point(((1167.3333) * transformX) + moveX, ((1178.6670) * transformY) + moveY),
        new createjs.Point(((1167.6667) * transformX) + moveX, ((1175.3330) * transformY) + moveY),
        new createjs.Point(((1168.0000) * transformX) + moveX, ((1172.0000) * transformY) + moveY),

        new createjs.Point(((1170.3728) * transformX) + moveX, ((1164.4554) * transformY) + moveY),
        new createjs.Point(((1171.6989) * transformX) + moveX, ((1154.5721) * transformY) + moveY),
        new createjs.Point(((1174.0000) * transformX) + moveX, ((1147.0000) * transformY) + moveY),

        new createjs.Point(((1174.0000) * transformX) + moveX, ((1145.0002) * transformY) + moveY),
        new createjs.Point(((1174.0000) * transformX) + moveX, ((1142.9998) * transformY) + moveY),
        new createjs.Point(((1174.0000) * transformX) + moveX, ((1141.0000) * transformY) + moveY),

        new createjs.Point(((1178.2847) * transformX) + moveX, ((1126.9637) * transformY) + moveY),
        new createjs.Point(((1178.7967) * transformX) + moveX, ((1109.2800) * transformY) + moveY),
        new createjs.Point(((1183.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),

        new createjs.Point(((1183.3333) * transformX) + moveX, ((1090.3338) * transformY) + moveY),
        new createjs.Point(((1183.6667) * transformX) + moveX, ((1085.6662) * transformY) + moveY),
        new createjs.Point(((1184.0000) * transformX) + moveX, ((1081.0000) * transformY) + moveY),

        new createjs.Point(((1187.8863) * transformX) + moveX, ((1067.8225) * transformY) + moveY),
        new createjs.Point(((1187.1438) * transformX) + moveX, ((1050.4436) * transformY) + moveY),
        new createjs.Point(((1191.0000) * transformX) + moveX, ((1037.0000) * transformY) + moveY),

        new createjs.Point(((1191.0000) * transformX) + moveX, ((1034.0003) * transformY) + moveY),
        new createjs.Point(((1191.0000) * transformX) + moveX, ((1030.9997) * transformY) + moveY),
        new createjs.Point(((1191.0000) * transformX) + moveX, ((1028.0000) * transformY) + moveY),

        new createjs.Point(((1191.3333) * transformX) + moveX, ((1028.0000) * transformY) + moveY),
        new createjs.Point(((1191.6667) * transformX) + moveX, ((1028.0000) * transformY) + moveY),
        new createjs.Point(((1192.0000) * transformX) + moveX, ((1028.0000) * transformY) + moveY),

        new createjs.Point(((1192.0000) * transformX) + moveX, ((1025.3336) * transformY) + moveY),
        new createjs.Point(((1192.0000) * transformX) + moveX, ((1022.6664) * transformY) + moveY),
        new createjs.Point(((1192.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),

        new createjs.Point(((1192.3333) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((1192.6667) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((1193.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),

        new createjs.Point(((1194.3332) * transformX) + moveX, ((1002.3351) * transformY) + moveY),
        new createjs.Point(((1195.6668) * transformX) + moveX, ((984.6649) * transformY) + moveY),
        new createjs.Point(((1197.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),

        new createjs.Point(((1197.3333) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((1197.6667) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((1198.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),

        new createjs.Point(((1198.0000) * transformX) + moveX, ((962.6671) * transformY) + moveY),
        new createjs.Point(((1198.0000) * transformX) + moveX, ((958.3329) * transformY) + moveY),
        new createjs.Point(((1198.0000) * transformX) + moveX, ((954.0000) * transformY) + moveY),

        new createjs.Point(((1202.0685) * transformX) + moveX, ((939.5957) * transformY) + moveY),
        new createjs.Point(((1199.9982) * transformX) + moveX, ((917.5707) * transformY) + moveY),
        new createjs.Point(((1200.0000) * transformX) + moveX, ((900.0000) * transformY) + moveY),

        new createjs.Point(((1200.0000) * transformX) + moveX, ((859.0041) * transformY) + moveY),
        new createjs.Point(((1200.0000) * transformX) + moveX, ((817.9959) * transformY) + moveY),
        new createjs.Point(((1200.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((1200.0000) * transformX) + moveX, ((749.6694) * transformY) + moveY),
        new createjs.Point(((1200.0000) * transformX) + moveX, ((722.3306) * transformY) + moveY),
        new createjs.Point(((1200.0000) * transformX) + moveX, ((695.0000) * transformY) + moveY),

        new createjs.Point(((1199.6667) * transformX) + moveX, ((695.0000) * transformY) + moveY),
        new createjs.Point(((1199.3333) * transformX) + moveX, ((695.0000) * transformY) + moveY),
        new createjs.Point(((1199.0000) * transformX) + moveX, ((695.0000) * transformY) + moveY),

        new createjs.Point(((1198.6667) * transformX) + moveX, ((690.3338) * transformY) + moveY),
        new createjs.Point(((1198.3333) * transformX) + moveX, ((685.6662) * transformY) + moveY),
        new createjs.Point(((1198.0000) * transformX) + moveX, ((681.0000) * transformY) + moveY),

        new createjs.Point(((1198.0000) * transformX) + moveX, ((677.3337) * transformY) + moveY),
        new createjs.Point(((1198.0000) * transformX) + moveX, ((673.6663) * transformY) + moveY),
        new createjs.Point(((1198.0000) * transformX) + moveX, ((670.0000) * transformY) + moveY),

        new createjs.Point(((1197.6667) * transformX) + moveX, ((670.0000) * transformY) + moveY),
        new createjs.Point(((1197.3333) * transformX) + moveX, ((670.0000) * transformY) + moveY),
        new createjs.Point(((1197.0000) * transformX) + moveX, ((670.0000) * transformY) + moveY),

        new createjs.Point(((1197.0000) * transformX) + moveX, ((665.3338) * transformY) + moveY),
        new createjs.Point(((1197.0000) * transformX) + moveX, ((660.6662) * transformY) + moveY),
        new createjs.Point(((1197.0000) * transformX) + moveX, ((656.0000) * transformY) + moveY),

        new createjs.Point(((1196.6667) * transformX) + moveX, ((656.0000) * transformY) + moveY),
        new createjs.Point(((1196.3333) * transformX) + moveX, ((656.0000) * transformY) + moveY),
        new createjs.Point(((1196.0000) * transformX) + moveX, ((656.0000) * transformY) + moveY),

        new createjs.Point(((1196.0000) * transformX) + moveX, ((652.0004) * transformY) + moveY),
        new createjs.Point(((1196.0000) * transformX) + moveX, ((647.9996) * transformY) + moveY),
        new createjs.Point(((1196.0000) * transformX) + moveX, ((644.0000) * transformY) + moveY),

        new createjs.Point(((1195.6667) * transformX) + moveX, ((644.0000) * transformY) + moveY),
        new createjs.Point(((1195.3333) * transformX) + moveX, ((644.0000) * transformY) + moveY),
        new createjs.Point(((1195.0000) * transformX) + moveX, ((644.0000) * transformY) + moveY),

        new createjs.Point(((1195.0000) * transformX) + moveX, ((643.6667) * transformY) + moveY),
        new createjs.Point(((1195.0000) * transformX) + moveX, ((643.3333) * transformY) + moveY),
        new createjs.Point(((1195.0000) * transformX) + moveX, ((643.0000) * transformY) + moveY),

        new createjs.Point(((1195.0000) * transformX) + moveX, ((639.3337) * transformY) + moveY),
        new createjs.Point(((1195.0000) * transformX) + moveX, ((635.6663) * transformY) + moveY),
        new createjs.Point(((1195.0000) * transformX) + moveX, ((632.0000) * transformY) + moveY),

        new createjs.Point(((1194.6667) * transformX) + moveX, ((632.0000) * transformY) + moveY),
        new createjs.Point(((1194.3333) * transformX) + moveX, ((632.0000) * transformY) + moveY),
        new createjs.Point(((1194.0000) * transformX) + moveX, ((632.0000) * transformY) + moveY),

        new createjs.Point(((1194.0000) * transformX) + moveX, ((628.3337) * transformY) + moveY),
        new createjs.Point(((1194.0000) * transformX) + moveX, ((624.6663) * transformY) + moveY),
        new createjs.Point(((1194.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),

        new createjs.Point(((1193.6667) * transformX) + moveX, ((621.0000) * transformY) + moveY),
        new createjs.Point(((1193.3333) * transformX) + moveX, ((621.0000) * transformY) + moveY),
        new createjs.Point(((1193.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),

        new createjs.Point(((1193.0000) * transformX) + moveX, ((617.6670) * transformY) + moveY),
        new createjs.Point(((1193.0000) * transformX) + moveX, ((614.3330) * transformY) + moveY),
        new createjs.Point(((1193.0000) * transformX) + moveX, ((611.0000) * transformY) + moveY),

        new createjs.Point(((1192.6667) * transformX) + moveX, ((611.0000) * transformY) + moveY),
        new createjs.Point(((1192.3333) * transformX) + moveX, ((611.0000) * transformY) + moveY),
        new createjs.Point(((1192.0000) * transformX) + moveX, ((611.0000) * transformY) + moveY),

        new createjs.Point(((1192.0000) * transformX) + moveX, ((607.6670) * transformY) + moveY),
        new createjs.Point(((1192.0000) * transformX) + moveX, ((604.3330) * transformY) + moveY),
        new createjs.Point(((1192.0000) * transformX) + moveX, ((601.0000) * transformY) + moveY),

        new createjs.Point(((1191.6667) * transformX) + moveX, ((601.0000) * transformY) + moveY),
        new createjs.Point(((1191.3333) * transformX) + moveX, ((601.0000) * transformY) + moveY),
        new createjs.Point(((1191.0000) * transformX) + moveX, ((601.0000) * transformY) + moveY),

        new createjs.Point(((1191.0000) * transformX) + moveX, ((598.0003) * transformY) + moveY),
        new createjs.Point(((1191.0000) * transformX) + moveX, ((594.9997) * transformY) + moveY),
        new createjs.Point(((1191.0000) * transformX) + moveX, ((592.0000) * transformY) + moveY),

        new createjs.Point(((1187.4935) * transformX) + moveX, ((579.8740) * transformY) + moveY),
        new createjs.Point(((1188.4895) * transformX) + moveX, ((563.9495) * transformY) + moveY),
        new createjs.Point(((1185.0000) * transformX) + moveX, ((552.0000) * transformY) + moveY),

        new createjs.Point(((1184.6667) * transformX) + moveX, ((547.3338) * transformY) + moveY),
        new createjs.Point(((1184.3333) * transformX) + moveX, ((542.6662) * transformY) + moveY),
        new createjs.Point(((1184.0000) * transformX) + moveX, ((538.0000) * transformY) + moveY),

        new createjs.Point(((1182.2655) * transformX) + moveX, ((532.1296) * transformY) + moveY),
        new createjs.Point(((1181.7431) * transformX) + moveX, ((523.8669) * transformY) + moveY),
        new createjs.Point(((1180.0000) * transformX) + moveX, ((518.0000) * transformY) + moveY),

        new createjs.Point(((1180.0000) * transformX) + moveX, ((516.0002) * transformY) + moveY),
        new createjs.Point(((1180.0000) * transformX) + moveX, ((513.9998) * transformY) + moveY),
        new createjs.Point(((1180.0000) * transformX) + moveX, ((512.0000) * transformY) + moveY),

        new createjs.Point(((1179.6667) * transformX) + moveX, ((512.0000) * transformY) + moveY),
        new createjs.Point(((1179.3333) * transformX) + moveX, ((512.0000) * transformY) + moveY),
        new createjs.Point(((1179.0000) * transformX) + moveX, ((512.0000) * transformY) + moveY),

        new createjs.Point(((1179.0000) * transformX) + moveX, ((510.0002) * transformY) + moveY),
        new createjs.Point(((1179.0000) * transformX) + moveX, ((507.9998) * transformY) + moveY),
        new createjs.Point(((1179.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((1178.6667) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((1178.3333) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((1178.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((1178.0000) * transformX) + moveX, ((504.0002) * transformY) + moveY),
        new createjs.Point(((1178.0000) * transformX) + moveX, ((501.9998) * transformY) + moveY),
        new createjs.Point(((1178.0000) * transformX) + moveX, ((500.0000) * transformY) + moveY),

        new createjs.Point(((1177.6667) * transformX) + moveX, ((500.0000) * transformY) + moveY),
        new createjs.Point(((1177.3333) * transformX) + moveX, ((500.0000) * transformY) + moveY),
        new createjs.Point(((1177.0000) * transformX) + moveX, ((500.0000) * transformY) + moveY),

        new createjs.Point(((1177.0000) * transformX) + moveX, ((498.3335) * transformY) + moveY),
        new createjs.Point(((1177.0000) * transformX) + moveX, ((496.6665) * transformY) + moveY),
        new createjs.Point(((1177.0000) * transformX) + moveX, ((495.0000) * transformY) + moveY),

        new createjs.Point(((1176.6667) * transformX) + moveX, ((495.0000) * transformY) + moveY),
        new createjs.Point(((1176.3333) * transformX) + moveX, ((495.0000) * transformY) + moveY),
        new createjs.Point(((1176.0000) * transformX) + moveX, ((495.0000) * transformY) + moveY),

        new createjs.Point(((1176.0000) * transformX) + moveX, ((493.0002) * transformY) + moveY),
        new createjs.Point(((1176.0000) * transformX) + moveX, ((490.9998) * transformY) + moveY),
        new createjs.Point(((1176.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((1172.2840) * transformX) + moveX, ((476.9151) * transformY) + moveY),
        new createjs.Point(((1170.8304) * transformX) + moveX, ((461.8785) * transformY) + moveY),
        new createjs.Point(((1167.0000) * transformX) + moveX, ((450.0000) * transformY) + moveY),

        new createjs.Point(((1167.0000) * transformX) + moveX, ((448.3335) * transformY) + moveY),
        new createjs.Point(((1167.0000) * transformX) + moveX, ((446.6665) * transformY) + moveY),
        new createjs.Point(((1167.0000) * transformX) + moveX, ((445.0000) * transformY) + moveY),

        new createjs.Point(((1166.6667) * transformX) + moveX, ((445.0000) * transformY) + moveY),
        new createjs.Point(((1166.3333) * transformX) + moveX, ((445.0000) * transformY) + moveY),
        new createjs.Point(((1166.0000) * transformX) + moveX, ((445.0000) * transformY) + moveY),

        new createjs.Point(((1166.0000) * transformX) + moveX, ((443.6668) * transformY) + moveY),
        new createjs.Point(((1166.0000) * transformX) + moveX, ((442.3332) * transformY) + moveY),
        new createjs.Point(((1166.0000) * transformX) + moveX, ((441.0000) * transformY) + moveY),

        new createjs.Point(((1165.6667) * transformX) + moveX, ((441.0000) * transformY) + moveY),
        new createjs.Point(((1165.3333) * transformX) + moveX, ((441.0000) * transformY) + moveY),
        new createjs.Point(((1165.0000) * transformX) + moveX, ((441.0000) * transformY) + moveY),

        new createjs.Point(((1165.0000) * transformX) + moveX, ((439.3335) * transformY) + moveY),
        new createjs.Point(((1165.0000) * transformX) + moveX, ((437.6665) * transformY) + moveY),
        new createjs.Point(((1165.0000) * transformX) + moveX, ((436.0000) * transformY) + moveY),

        new createjs.Point(((1160.2318) * transformX) + moveX, ((421.3770) * transformY) + moveY),
        new createjs.Point(((1157.0932) * transformX) + moveX, ((404.1424) * transformY) + moveY),
        new createjs.Point(((1152.0000) * transformX) + moveX, ((390.0000) * transformY) + moveY),

        new createjs.Point(((1152.0000) * transformX) + moveX, ((388.6668) * transformY) + moveY),
        new createjs.Point(((1152.0000) * transformX) + moveX, ((387.3332) * transformY) + moveY),
        new createjs.Point(((1152.0000) * transformX) + moveX, ((386.0000) * transformY) + moveY),

        new createjs.Point(((1151.6667) * transformX) + moveX, ((386.0000) * transformY) + moveY),
        new createjs.Point(((1151.3333) * transformX) + moveX, ((386.0000) * transformY) + moveY),
        new createjs.Point(((1151.0000) * transformX) + moveX, ((386.0000) * transformY) + moveY),

        new createjs.Point(((1151.0000) * transformX) + moveX, ((385.0001) * transformY) + moveY),
        new createjs.Point(((1151.0000) * transformX) + moveX, ((383.9999) * transformY) + moveY),
        new createjs.Point(((1151.0000) * transformX) + moveX, ((383.0000) * transformY) + moveY),

        new createjs.Point(((1150.6667) * transformX) + moveX, ((383.0000) * transformY) + moveY),
        new createjs.Point(((1150.3333) * transformX) + moveX, ((383.0000) * transformY) + moveY),
        new createjs.Point(((1150.0000) * transformX) + moveX, ((383.0000) * transformY) + moveY),

        new createjs.Point(((1150.0000) * transformX) + moveX, ((381.6668) * transformY) + moveY),
        new createjs.Point(((1150.0000) * transformX) + moveX, ((380.3332) * transformY) + moveY),
        new createjs.Point(((1150.0000) * transformX) + moveX, ((379.0000) * transformY) + moveY),

        new createjs.Point(((1149.6667) * transformX) + moveX, ((379.0000) * transformY) + moveY),
        new createjs.Point(((1149.3333) * transformX) + moveX, ((379.0000) * transformY) + moveY),
        new createjs.Point(((1149.0000) * transformX) + moveX, ((379.0000) * transformY) + moveY),

        new createjs.Point(((1148.3334) * transformX) + moveX, ((375.6670) * transformY) + moveY),
        new createjs.Point(((1147.6666) * transformX) + moveX, ((372.3330) * transformY) + moveY),
        new createjs.Point(((1147.0000) * transformX) + moveX, ((369.0000) * transformY) + moveY),

        new createjs.Point(((1146.6667) * transformX) + moveX, ((369.0000) * transformY) + moveY),
        new createjs.Point(((1146.3333) * transformX) + moveX, ((369.0000) * transformY) + moveY),
        new createjs.Point(((1146.0000) * transformX) + moveX, ((369.0000) * transformY) + moveY),

        new createjs.Point(((1146.0000) * transformX) + moveX, ((368.0001) * transformY) + moveY),
        new createjs.Point(((1146.0000) * transformX) + moveX, ((366.9999) * transformY) + moveY),
        new createjs.Point(((1146.0000) * transformX) + moveX, ((366.0000) * transformY) + moveY),

        new createjs.Point(((1145.6667) * transformX) + moveX, ((366.0000) * transformY) + moveY),
        new createjs.Point(((1145.3333) * transformX) + moveX, ((366.0000) * transformY) + moveY),
        new createjs.Point(((1145.0000) * transformX) + moveX, ((366.0000) * transformY) + moveY),

        new createjs.Point(((1145.0000) * transformX) + moveX, ((365.0001) * transformY) + moveY),
        new createjs.Point(((1145.0000) * transformX) + moveX, ((363.9999) * transformY) + moveY),
        new createjs.Point(((1145.0000) * transformX) + moveX, ((363.0000) * transformY) + moveY),

        new createjs.Point(((1144.6667) * transformX) + moveX, ((363.0000) * transformY) + moveY),
        new createjs.Point(((1144.3333) * transformX) + moveX, ((363.0000) * transformY) + moveY),
        new createjs.Point(((1144.0000) * transformX) + moveX, ((363.0000) * transformY) + moveY),

        new createjs.Point(((1144.0000) * transformX) + moveX, ((362.0001) * transformY) + moveY),
        new createjs.Point(((1144.0000) * transformX) + moveX, ((360.9999) * transformY) + moveY),
        new createjs.Point(((1144.0000) * transformX) + moveX, ((360.0000) * transformY) + moveY),

        new createjs.Point(((1143.6667) * transformX) + moveX, ((360.0000) * transformY) + moveY),
        new createjs.Point(((1143.3333) * transformX) + moveX, ((360.0000) * transformY) + moveY),
        new createjs.Point(((1143.0000) * transformX) + moveX, ((360.0000) * transformY) + moveY),

        new createjs.Point(((1142.6667) * transformX) + moveX, ((358.0002) * transformY) + moveY),
        new createjs.Point(((1142.3333) * transformX) + moveX, ((355.9998) * transformY) + moveY),
        new createjs.Point(((1142.0000) * transformX) + moveX, ((354.0000) * transformY) + moveY),

        new createjs.Point(((1141.6667) * transformX) + moveX, ((354.0000) * transformY) + moveY),
        new createjs.Point(((1141.3333) * transformX) + moveX, ((354.0000) * transformY) + moveY),
        new createjs.Point(((1141.0000) * transformX) + moveX, ((354.0000) * transformY) + moveY),

        new createjs.Point(((1140.6667) * transformX) + moveX, ((351.6669) * transformY) + moveY),
        new createjs.Point(((1140.3333) * transformX) + moveX, ((349.3331) * transformY) + moveY),
        new createjs.Point(((1140.0000) * transformX) + moveX, ((347.0000) * transformY) + moveY),

        new createjs.Point(((1139.6667) * transformX) + moveX, ((347.0000) * transformY) + moveY),
        new createjs.Point(((1139.3333) * transformX) + moveX, ((347.0000) * transformY) + moveY),
        new createjs.Point(((1139.0000) * transformX) + moveX, ((347.0000) * transformY) + moveY),

        new createjs.Point(((1139.0000) * transformX) + moveX, ((346.3334) * transformY) + moveY),
        new createjs.Point(((1139.0000) * transformX) + moveX, ((345.6666) * transformY) + moveY),
        new createjs.Point(((1139.0000) * transformX) + moveX, ((345.0000) * transformY) + moveY),

        new createjs.Point(((1138.6667) * transformX) + moveX, ((345.0000) * transformY) + moveY),
        new createjs.Point(((1138.3333) * transformX) + moveX, ((345.0000) * transformY) + moveY),
        new createjs.Point(((1138.0000) * transformX) + moveX, ((345.0000) * transformY) + moveY),

        new createjs.Point(((1138.0000) * transformX) + moveX, ((344.0001) * transformY) + moveY),
        new createjs.Point(((1138.0000) * transformX) + moveX, ((342.9999) * transformY) + moveY),
        new createjs.Point(((1138.0000) * transformX) + moveX, ((342.0000) * transformY) + moveY),

        new createjs.Point(((1137.6667) * transformX) + moveX, ((342.0000) * transformY) + moveY),
        new createjs.Point(((1137.3333) * transformX) + moveX, ((342.0000) * transformY) + moveY),
        new createjs.Point(((1137.0000) * transformX) + moveX, ((342.0000) * transformY) + moveY),

        new createjs.Point(((1137.0000) * transformX) + moveX, ((341.0001) * transformY) + moveY),
        new createjs.Point(((1137.0000) * transformX) + moveX, ((339.9999) * transformY) + moveY),
        new createjs.Point(((1137.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),

        new createjs.Point(((1136.6667) * transformX) + moveX, ((339.0000) * transformY) + moveY),
        new createjs.Point(((1136.3333) * transformX) + moveX, ((339.0000) * transformY) + moveY),
        new createjs.Point(((1136.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),

        new createjs.Point(((1136.0000) * transformX) + moveX, ((338.0001) * transformY) + moveY),
        new createjs.Point(((1136.0000) * transformX) + moveX, ((336.9999) * transformY) + moveY),
        new createjs.Point(((1136.0000) * transformX) + moveX, ((336.0000) * transformY) + moveY),

        new createjs.Point(((1135.6667) * transformX) + moveX, ((336.0000) * transformY) + moveY),
        new createjs.Point(((1135.3333) * transformX) + moveX, ((336.0000) * transformY) + moveY),
        new createjs.Point(((1135.0000) * transformX) + moveX, ((336.0000) * transformY) + moveY),

        new createjs.Point(((1135.0000) * transformX) + moveX, ((335.0001) * transformY) + moveY),
        new createjs.Point(((1135.0000) * transformX) + moveX, ((333.9999) * transformY) + moveY),
        new createjs.Point(((1135.0000) * transformX) + moveX, ((333.0000) * transformY) + moveY),

        new createjs.Point(((1134.6667) * transformX) + moveX, ((333.0000) * transformY) + moveY),
        new createjs.Point(((1134.3333) * transformX) + moveX, ((333.0000) * transformY) + moveY),
        new createjs.Point(((1134.0000) * transformX) + moveX, ((333.0000) * transformY) + moveY),

        new createjs.Point(((1134.0000) * transformX) + moveX, ((332.0001) * transformY) + moveY),
        new createjs.Point(((1134.0000) * transformX) + moveX, ((330.9999) * transformY) + moveY),
        new createjs.Point(((1134.0000) * transformX) + moveX, ((330.0000) * transformY) + moveY),

        new createjs.Point(((1133.6667) * transformX) + moveX, ((330.0000) * transformY) + moveY),
        new createjs.Point(((1133.3333) * transformX) + moveX, ((330.0000) * transformY) + moveY),
        new createjs.Point(((1133.0000) * transformX) + moveX, ((330.0000) * transformY) + moveY),

        new createjs.Point(((1133.0000) * transformX) + moveX, ((329.3334) * transformY) + moveY),
        new createjs.Point(((1133.0000) * transformX) + moveX, ((328.6666) * transformY) + moveY),
        new createjs.Point(((1133.0000) * transformX) + moveX, ((328.0000) * transformY) + moveY),

        new createjs.Point(((1132.6667) * transformX) + moveX, ((328.0000) * transformY) + moveY),
        new createjs.Point(((1132.3333) * transformX) + moveX, ((328.0000) * transformY) + moveY),
        new createjs.Point(((1132.0000) * transformX) + moveX, ((328.0000) * transformY) + moveY),

        new createjs.Point(((1131.6667) * transformX) + moveX, ((326.0002) * transformY) + moveY),
        new createjs.Point(((1131.3333) * transformX) + moveX, ((323.9998) * transformY) + moveY),
        new createjs.Point(((1131.0000) * transformX) + moveX, ((322.0000) * transformY) + moveY),

        new createjs.Point(((1130.6667) * transformX) + moveX, ((322.0000) * transformY) + moveY),
        new createjs.Point(((1130.3333) * transformX) + moveX, ((322.0000) * transformY) + moveY),
        new createjs.Point(((1130.0000) * transformX) + moveX, ((322.0000) * transformY) + moveY),

        new createjs.Point(((1130.0000) * transformX) + moveX, ((321.3334) * transformY) + moveY),
        new createjs.Point(((1130.0000) * transformX) + moveX, ((320.6666) * transformY) + moveY),
        new createjs.Point(((1130.0000) * transformX) + moveX, ((320.0000) * transformY) + moveY),

        new createjs.Point(((1129.6667) * transformX) + moveX, ((320.0000) * transformY) + moveY),
        new createjs.Point(((1129.3333) * transformX) + moveX, ((320.0000) * transformY) + moveY),
        new createjs.Point(((1129.0000) * transformX) + moveX, ((320.0000) * transformY) + moveY),

        new createjs.Point(((1128.6667) * transformX) + moveX, ((318.0002) * transformY) + moveY),
        new createjs.Point(((1128.3333) * transformX) + moveX, ((315.9998) * transformY) + moveY),
        new createjs.Point(((1128.0000) * transformX) + moveX, ((314.0000) * transformY) + moveY),

        new createjs.Point(((1127.6667) * transformX) + moveX, ((314.0000) * transformY) + moveY),
        new createjs.Point(((1127.3333) * transformX) + moveX, ((314.0000) * transformY) + moveY),
        new createjs.Point(((1127.0000) * transformX) + moveX, ((314.0000) * transformY) + moveY),

        new createjs.Point(((1126.6667) * transformX) + moveX, ((312.3335) * transformY) + moveY),
        new createjs.Point(((1126.3333) * transformX) + moveX, ((310.6665) * transformY) + moveY),
        new createjs.Point(((1126.0000) * transformX) + moveX, ((309.0000) * transformY) + moveY),

        new createjs.Point(((1125.6667) * transformX) + moveX, ((309.0000) * transformY) + moveY),
        new createjs.Point(((1125.3333) * transformX) + moveX, ((309.0000) * transformY) + moveY),
        new createjs.Point(((1125.0000) * transformX) + moveX, ((309.0000) * transformY) + moveY),

        new createjs.Point(((1125.0000) * transformX) + moveX, ((308.3334) * transformY) + moveY),
        new createjs.Point(((1125.0000) * transformX) + moveX, ((307.6666) * transformY) + moveY),
        new createjs.Point(((1125.0000) * transformX) + moveX, ((307.0000) * transformY) + moveY),

        new createjs.Point(((1124.6667) * transformX) + moveX, ((307.0000) * transformY) + moveY),
        new createjs.Point(((1124.3333) * transformX) + moveX, ((307.0000) * transformY) + moveY),
        new createjs.Point(((1124.0000) * transformX) + moveX, ((307.0000) * transformY) + moveY),

        new createjs.Point(((1124.0000) * transformX) + moveX, ((306.0001) * transformY) + moveY),
        new createjs.Point(((1124.0000) * transformX) + moveX, ((304.9999) * transformY) + moveY),
        new createjs.Point(((1124.0000) * transformX) + moveX, ((304.0000) * transformY) + moveY),

        new createjs.Point(((1123.6667) * transformX) + moveX, ((304.0000) * transformY) + moveY),
        new createjs.Point(((1123.3333) * transformX) + moveX, ((304.0000) * transformY) + moveY),
        new createjs.Point(((1123.0000) * transformX) + moveX, ((304.0000) * transformY) + moveY),

        new createjs.Point(((1123.0000) * transformX) + moveX, ((303.3334) * transformY) + moveY),
        new createjs.Point(((1123.0000) * transformX) + moveX, ((302.6666) * transformY) + moveY),
        new createjs.Point(((1123.0000) * transformX) + moveX, ((302.0000) * transformY) + moveY),

        new createjs.Point(((1122.6667) * transformX) + moveX, ((302.0000) * transformY) + moveY),
        new createjs.Point(((1122.3333) * transformX) + moveX, ((302.0000) * transformY) + moveY),
        new createjs.Point(((1122.0000) * transformX) + moveX, ((302.0000) * transformY) + moveY),

        new createjs.Point(((1122.0000) * transformX) + moveX, ((301.0001) * transformY) + moveY),
        new createjs.Point(((1122.0000) * transformX) + moveX, ((299.9999) * transformY) + moveY),
        new createjs.Point(((1122.0000) * transformX) + moveX, ((299.0000) * transformY) + moveY),

        new createjs.Point(((1121.6667) * transformX) + moveX, ((299.0000) * transformY) + moveY),
        new createjs.Point(((1121.3333) * transformX) + moveX, ((299.0000) * transformY) + moveY),
        new createjs.Point(((1121.0000) * transformX) + moveX, ((299.0000) * transformY) + moveY),

        new createjs.Point(((1120.6667) * transformX) + moveX, ((297.6668) * transformY) + moveY),
        new createjs.Point(((1120.3333) * transformX) + moveX, ((296.3332) * transformY) + moveY),
        new createjs.Point(((1120.0000) * transformX) + moveX, ((295.0000) * transformY) + moveY),

        new createjs.Point(((1119.6667) * transformX) + moveX, ((295.0000) * transformY) + moveY),
        new createjs.Point(((1119.3333) * transformX) + moveX, ((295.0000) * transformY) + moveY),
        new createjs.Point(((1119.0000) * transformX) + moveX, ((295.0000) * transformY) + moveY),

        new createjs.Point(((1119.0000) * transformX) + moveX, ((294.0001) * transformY) + moveY),
        new createjs.Point(((1119.0000) * transformX) + moveX, ((292.9999) * transformY) + moveY),
        new createjs.Point(((1119.0000) * transformX) + moveX, ((292.0000) * transformY) + moveY),

        new createjs.Point(((1118.6667) * transformX) + moveX, ((292.0000) * transformY) + moveY),
        new createjs.Point(((1118.3333) * transformX) + moveX, ((292.0000) * transformY) + moveY),
        new createjs.Point(((1118.0000) * transformX) + moveX, ((292.0000) * transformY) + moveY),

        new createjs.Point(((1117.6667) * transformX) + moveX, ((290.6668) * transformY) + moveY),
        new createjs.Point(((1117.3333) * transformX) + moveX, ((289.3332) * transformY) + moveY),
        new createjs.Point(((1117.0000) * transformX) + moveX, ((288.0000) * transformY) + moveY),

        new createjs.Point(((1116.6667) * transformX) + moveX, ((288.0000) * transformY) + moveY),
        new createjs.Point(((1116.3333) * transformX) + moveX, ((288.0000) * transformY) + moveY),
        new createjs.Point(((1116.0000) * transformX) + moveX, ((288.0000) * transformY) + moveY),

        new createjs.Point(((1116.0000) * transformX) + moveX, ((287.0001) * transformY) + moveY),
        new createjs.Point(((1116.0000) * transformX) + moveX, ((285.9999) * transformY) + moveY),
        new createjs.Point(((1116.0000) * transformX) + moveX, ((285.0000) * transformY) + moveY),

        new createjs.Point(((1115.6667) * transformX) + moveX, ((285.0000) * transformY) + moveY),
        new createjs.Point(((1115.3333) * transformX) + moveX, ((285.0000) * transformY) + moveY),
        new createjs.Point(((1115.0000) * transformX) + moveX, ((285.0000) * transformY) + moveY),

        new createjs.Point(((1115.0000) * transformX) + moveX, ((284.3334) * transformY) + moveY),
        new createjs.Point(((1115.0000) * transformX) + moveX, ((283.6666) * transformY) + moveY),
        new createjs.Point(((1115.0000) * transformX) + moveX, ((283.0000) * transformY) + moveY),

        new createjs.Point(((1114.6667) * transformX) + moveX, ((283.0000) * transformY) + moveY),
        new createjs.Point(((1114.3333) * transformX) + moveX, ((283.0000) * transformY) + moveY),
        new createjs.Point(((1114.0000) * transformX) + moveX, ((283.0000) * transformY) + moveY),

        new createjs.Point(((1113.6667) * transformX) + moveX, ((281.6668) * transformY) + moveY),
        new createjs.Point(((1113.3333) * transformX) + moveX, ((280.3332) * transformY) + moveY),
        new createjs.Point(((1113.0000) * transformX) + moveX, ((279.0000) * transformY) + moveY),

        new createjs.Point(((1112.6667) * transformX) + moveX, ((279.0000) * transformY) + moveY),
        new createjs.Point(((1112.3333) * transformX) + moveX, ((279.0000) * transformY) + moveY),
        new createjs.Point(((1112.0000) * transformX) + moveX, ((279.0000) * transformY) + moveY),

        new createjs.Point(((1112.0000) * transformX) + moveX, ((278.0001) * transformY) + moveY),
        new createjs.Point(((1112.0000) * transformX) + moveX, ((276.9999) * transformY) + moveY),
        new createjs.Point(((1112.0000) * transformX) + moveX, ((276.0000) * transformY) + moveY),

        new createjs.Point(((1111.6667) * transformX) + moveX, ((276.0000) * transformY) + moveY),
        new createjs.Point(((1111.3333) * transformX) + moveX, ((276.0000) * transformY) + moveY),
        new createjs.Point(((1111.0000) * transformX) + moveX, ((276.0000) * transformY) + moveY),

        new createjs.Point(((1111.0000) * transformX) + moveX, ((275.3334) * transformY) + moveY),
        new createjs.Point(((1111.0000) * transformX) + moveX, ((274.6666) * transformY) + moveY),
        new createjs.Point(((1111.0000) * transformX) + moveX, ((274.0000) * transformY) + moveY),

        new createjs.Point(((1110.6667) * transformX) + moveX, ((274.0000) * transformY) + moveY),
        new createjs.Point(((1110.3333) * transformX) + moveX, ((274.0000) * transformY) + moveY),
        new createjs.Point(((1110.0000) * transformX) + moveX, ((274.0000) * transformY) + moveY),

        new createjs.Point(((1109.6667) * transformX) + moveX, ((272.6668) * transformY) + moveY),
        new createjs.Point(((1109.3333) * transformX) + moveX, ((271.3332) * transformY) + moveY),
        new createjs.Point(((1109.0000) * transformX) + moveX, ((270.0000) * transformY) + moveY),

        new createjs.Point(((1108.6667) * transformX) + moveX, ((270.0000) * transformY) + moveY),
        new createjs.Point(((1108.3333) * transformX) + moveX, ((270.0000) * transformY) + moveY),
        new createjs.Point(((1108.0000) * transformX) + moveX, ((270.0000) * transformY) + moveY),

        new createjs.Point(((1107.6667) * transformX) + moveX, ((268.6668) * transformY) + moveY),
        new createjs.Point(((1107.3333) * transformX) + moveX, ((267.3332) * transformY) + moveY),
        new createjs.Point(((1107.0000) * transformX) + moveX, ((266.0000) * transformY) + moveY),

        new createjs.Point(((1106.6667) * transformX) + moveX, ((266.0000) * transformY) + moveY),
        new createjs.Point(((1106.3333) * transformX) + moveX, ((266.0000) * transformY) + moveY),
        new createjs.Point(((1106.0000) * transformX) + moveX, ((266.0000) * transformY) + moveY),

        new createjs.Point(((1106.0000) * transformX) + moveX, ((265.3334) * transformY) + moveY),
        new createjs.Point(((1106.0000) * transformX) + moveX, ((264.6666) * transformY) + moveY),
        new createjs.Point(((1106.0000) * transformX) + moveX, ((264.0000) * transformY) + moveY),

        new createjs.Point(((1105.6667) * transformX) + moveX, ((264.0000) * transformY) + moveY),
        new createjs.Point(((1105.3333) * transformX) + moveX, ((264.0000) * transformY) + moveY),
        new createjs.Point(((1105.0000) * transformX) + moveX, ((264.0000) * transformY) + moveY),

        new createjs.Point(((1105.0000) * transformX) + moveX, ((263.3334) * transformY) + moveY),
        new createjs.Point(((1105.0000) * transformX) + moveX, ((262.6666) * transformY) + moveY),
        new createjs.Point(((1105.0000) * transformX) + moveX, ((262.0000) * transformY) + moveY),

        new createjs.Point(((1104.6667) * transformX) + moveX, ((262.0000) * transformY) + moveY),
        new createjs.Point(((1104.3333) * transformX) + moveX, ((262.0000) * transformY) + moveY),
        new createjs.Point(((1104.0000) * transformX) + moveX, ((262.0000) * transformY) + moveY),

        new createjs.Point(((1104.0000) * transformX) + moveX, ((261.3334) * transformY) + moveY),
        new createjs.Point(((1104.0000) * transformX) + moveX, ((260.6666) * transformY) + moveY),
        new createjs.Point(((1104.0000) * transformX) + moveX, ((260.0000) * transformY) + moveY),

        new createjs.Point(((1103.6667) * transformX) + moveX, ((260.0000) * transformY) + moveY),
        new createjs.Point(((1103.3333) * transformX) + moveX, ((260.0000) * transformY) + moveY),
        new createjs.Point(((1103.0000) * transformX) + moveX, ((260.0000) * transformY) + moveY),

        new createjs.Point(((1103.0000) * transformX) + moveX, ((259.3334) * transformY) + moveY),
        new createjs.Point(((1103.0000) * transformX) + moveX, ((258.6666) * transformY) + moveY),
        new createjs.Point(((1103.0000) * transformX) + moveX, ((258.0000) * transformY) + moveY),

        new createjs.Point(((1102.6667) * transformX) + moveX, ((258.0000) * transformY) + moveY),
        new createjs.Point(((1102.3333) * transformX) + moveX, ((258.0000) * transformY) + moveY),
        new createjs.Point(((1102.0000) * transformX) + moveX, ((258.0000) * transformY) + moveY),

        new createjs.Point(((1101.6667) * transformX) + moveX, ((256.6668) * transformY) + moveY),
        new createjs.Point(((1101.3333) * transformX) + moveX, ((255.3332) * transformY) + moveY),
        new createjs.Point(((1101.0000) * transformX) + moveX, ((254.0000) * transformY) + moveY),

        new createjs.Point(((1100.6667) * transformX) + moveX, ((254.0000) * transformY) + moveY),
        new createjs.Point(((1100.3333) * transformX) + moveX, ((254.0000) * transformY) + moveY),
        new createjs.Point(((1100.0000) * transformX) + moveX, ((254.0000) * transformY) + moveY),

        new createjs.Point(((1099.6667) * transformX) + moveX, ((252.6668) * transformY) + moveY),
        new createjs.Point(((1099.3333) * transformX) + moveX, ((251.3332) * transformY) + moveY),
        new createjs.Point(((1099.0000) * transformX) + moveX, ((250.0000) * transformY) + moveY),

        new createjs.Point(((1098.6667) * transformX) + moveX, ((250.0000) * transformY) + moveY),
        new createjs.Point(((1098.3333) * transformX) + moveX, ((250.0000) * transformY) + moveY),
        new createjs.Point(((1098.0000) * transformX) + moveX, ((250.0000) * transformY) + moveY),

        new createjs.Point(((1098.0000) * transformX) + moveX, ((249.3334) * transformY) + moveY),
        new createjs.Point(((1098.0000) * transformX) + moveX, ((248.6666) * transformY) + moveY),
        new createjs.Point(((1098.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),

        new createjs.Point(((1097.6667) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((1097.3333) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((1097.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),

        new createjs.Point(((1097.0000) * transformX) + moveX, ((247.3334) * transformY) + moveY),
        new createjs.Point(((1097.0000) * transformX) + moveX, ((246.6666) * transformY) + moveY),
        new createjs.Point(((1097.0000) * transformX) + moveX, ((246.0000) * transformY) + moveY),

        new createjs.Point(((1096.6667) * transformX) + moveX, ((246.0000) * transformY) + moveY),
        new createjs.Point(((1096.3333) * transformX) + moveX, ((246.0000) * transformY) + moveY),
        new createjs.Point(((1096.0000) * transformX) + moveX, ((246.0000) * transformY) + moveY),

        new createjs.Point(((1096.0000) * transformX) + moveX, ((245.3334) * transformY) + moveY),
        new createjs.Point(((1096.0000) * transformX) + moveX, ((244.6666) * transformY) + moveY),
        new createjs.Point(((1096.0000) * transformX) + moveX, ((244.0000) * transformY) + moveY),

        new createjs.Point(((1095.6667) * transformX) + moveX, ((244.0000) * transformY) + moveY),
        new createjs.Point(((1095.3333) * transformX) + moveX, ((244.0000) * transformY) + moveY),
        new createjs.Point(((1095.0000) * transformX) + moveX, ((244.0000) * transformY) + moveY),

        new createjs.Point(((1095.0000) * transformX) + moveX, ((243.3334) * transformY) + moveY),
        new createjs.Point(((1095.0000) * transformX) + moveX, ((242.6666) * transformY) + moveY),
        new createjs.Point(((1095.0000) * transformX) + moveX, ((242.0000) * transformY) + moveY),

        new createjs.Point(((1094.6667) * transformX) + moveX, ((242.0000) * transformY) + moveY),
        new createjs.Point(((1094.3333) * transformX) + moveX, ((242.0000) * transformY) + moveY),
        new createjs.Point(((1094.0000) * transformX) + moveX, ((242.0000) * transformY) + moveY),

        new createjs.Point(((1093.6667) * transformX) + moveX, ((240.6668) * transformY) + moveY),
        new createjs.Point(((1093.3333) * transformX) + moveX, ((239.3332) * transformY) + moveY),
        new createjs.Point(((1093.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),

        new createjs.Point(((1092.3334) * transformX) + moveX, ((237.6667) * transformY) + moveY),
        new createjs.Point(((1091.6666) * transformX) + moveX, ((237.3333) * transformY) + moveY),
        new createjs.Point(((1091.0000) * transformX) + moveX, ((237.0000) * transformY) + moveY),

        new createjs.Point(((1091.0000) * transformX) + moveX, ((236.3334) * transformY) + moveY),
        new createjs.Point(((1091.0000) * transformX) + moveX, ((235.6666) * transformY) + moveY),
        new createjs.Point(((1091.0000) * transformX) + moveX, ((235.0000) * transformY) + moveY),

        new createjs.Point(((1090.6667) * transformX) + moveX, ((235.0000) * transformY) + moveY),
        new createjs.Point(((1090.3333) * transformX) + moveX, ((235.0000) * transformY) + moveY),
        new createjs.Point(((1090.0000) * transformX) + moveX, ((235.0000) * transformY) + moveY),

        new createjs.Point(((1090.0000) * transformX) + moveX, ((234.3334) * transformY) + moveY),
        new createjs.Point(((1090.0000) * transformX) + moveX, ((233.6666) * transformY) + moveY),
        new createjs.Point(((1090.0000) * transformX) + moveX, ((233.0000) * transformY) + moveY),

        new createjs.Point(((1089.6667) * transformX) + moveX, ((233.0000) * transformY) + moveY),
        new createjs.Point(((1089.3333) * transformX) + moveX, ((233.0000) * transformY) + moveY),
        new createjs.Point(((1089.0000) * transformX) + moveX, ((233.0000) * transformY) + moveY),

        new createjs.Point(((1089.0000) * transformX) + moveX, ((232.3334) * transformY) + moveY),
        new createjs.Point(((1089.0000) * transformX) + moveX, ((231.6666) * transformY) + moveY),
        new createjs.Point(((1089.0000) * transformX) + moveX, ((231.0000) * transformY) + moveY),

        new createjs.Point(((1088.6667) * transformX) + moveX, ((231.0000) * transformY) + moveY),
        new createjs.Point(((1088.3333) * transformX) + moveX, ((231.0000) * transformY) + moveY),
        new createjs.Point(((1088.0000) * transformX) + moveX, ((231.0000) * transformY) + moveY),

        new createjs.Point(((1088.0000) * transformX) + moveX, ((230.3334) * transformY) + moveY),
        new createjs.Point(((1088.0000) * transformX) + moveX, ((229.6666) * transformY) + moveY),
        new createjs.Point(((1088.0000) * transformX) + moveX, ((229.0000) * transformY) + moveY),

        new createjs.Point(((1087.3334) * transformX) + moveX, ((228.6667) * transformY) + moveY),
        new createjs.Point(((1086.6666) * transformX) + moveX, ((228.3333) * transformY) + moveY),
        new createjs.Point(((1086.0000) * transformX) + moveX, ((228.0000) * transformY) + moveY),

        new createjs.Point(((1085.3334) * transformX) + moveX, ((226.0002) * transformY) + moveY),
        new createjs.Point(((1084.6666) * transformX) + moveX, ((223.9998) * transformY) + moveY),
        new createjs.Point(((1084.0000) * transformX) + moveX, ((222.0000) * transformY) + moveY),

        new createjs.Point(((1083.3334) * transformX) + moveX, ((221.6667) * transformY) + moveY),
        new createjs.Point(((1082.6666) * transformX) + moveX, ((221.3333) * transformY) + moveY),
        new createjs.Point(((1082.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),

        new createjs.Point(((1081.6667) * transformX) + moveX, ((219.6668) * transformY) + moveY),
        new createjs.Point(((1081.3333) * transformX) + moveX, ((218.3332) * transformY) + moveY),
        new createjs.Point(((1081.0000) * transformX) + moveX, ((217.0000) * transformY) + moveY),

        new createjs.Point(((1080.3334) * transformX) + moveX, ((216.6667) * transformY) + moveY),
        new createjs.Point(((1079.6666) * transformX) + moveX, ((216.3333) * transformY) + moveY),
        new createjs.Point(((1079.0000) * transformX) + moveX, ((216.0000) * transformY) + moveY),

        new createjs.Point(((1079.0000) * transformX) + moveX, ((215.3334) * transformY) + moveY),
        new createjs.Point(((1079.0000) * transformX) + moveX, ((214.6666) * transformY) + moveY),
        new createjs.Point(((1079.0000) * transformX) + moveX, ((214.0000) * transformY) + moveY),

        new createjs.Point(((1078.6667) * transformX) + moveX, ((214.0000) * transformY) + moveY),
        new createjs.Point(((1078.3333) * transformX) + moveX, ((214.0000) * transformY) + moveY),
        new createjs.Point(((1078.0000) * transformX) + moveX, ((214.0000) * transformY) + moveY),

        new createjs.Point(((1078.0000) * transformX) + moveX, ((213.3334) * transformY) + moveY),
        new createjs.Point(((1078.0000) * transformX) + moveX, ((212.6666) * transformY) + moveY),
        new createjs.Point(((1078.0000) * transformX) + moveX, ((212.0000) * transformY) + moveY),

        new createjs.Point(((1077.3334) * transformX) + moveX, ((211.6667) * transformY) + moveY),
        new createjs.Point(((1076.6666) * transformX) + moveX, ((211.3333) * transformY) + moveY),
        new createjs.Point(((1076.0000) * transformX) + moveX, ((211.0000) * transformY) + moveY),

        new createjs.Point(((1076.0000) * transformX) + moveX, ((210.3334) * transformY) + moveY),
        new createjs.Point(((1076.0000) * transformX) + moveX, ((209.6666) * transformY) + moveY),
        new createjs.Point(((1076.0000) * transformX) + moveX, ((209.0000) * transformY) + moveY),

        new createjs.Point(((1075.3334) * transformX) + moveX, ((208.6667) * transformY) + moveY),
        new createjs.Point(((1074.6666) * transformX) + moveX, ((208.3333) * transformY) + moveY),
        new createjs.Point(((1074.0000) * transformX) + moveX, ((208.0000) * transformY) + moveY),

        new createjs.Point(((1073.6667) * transformX) + moveX, ((206.6668) * transformY) + moveY),
        new createjs.Point(((1073.3333) * transformX) + moveX, ((205.3332) * transformY) + moveY),
        new createjs.Point(((1073.0000) * transformX) + moveX, ((204.0000) * transformY) + moveY),

        new createjs.Point(((1072.3334) * transformX) + moveX, ((203.6667) * transformY) + moveY),
        new createjs.Point(((1071.6666) * transformX) + moveX, ((203.3333) * transformY) + moveY),
        new createjs.Point(((1071.0000) * transformX) + moveX, ((203.0000) * transformY) + moveY),

        new createjs.Point(((1071.0000) * transformX) + moveX, ((202.3334) * transformY) + moveY),
        new createjs.Point(((1071.0000) * transformX) + moveX, ((201.6666) * transformY) + moveY),
        new createjs.Point(((1071.0000) * transformX) + moveX, ((201.0000) * transformY) + moveY),

        new createjs.Point(((1070.3334) * transformX) + moveX, ((200.6667) * transformY) + moveY),
        new createjs.Point(((1069.6666) * transformX) + moveX, ((200.3333) * transformY) + moveY),
        new createjs.Point(((1069.0000) * transformX) + moveX, ((200.0000) * transformY) + moveY),

        new createjs.Point(((1069.0000) * transformX) + moveX, ((199.3334) * transformY) + moveY),
        new createjs.Point(((1069.0000) * transformX) + moveX, ((198.6666) * transformY) + moveY),
        new createjs.Point(((1069.0000) * transformX) + moveX, ((198.0000) * transformY) + moveY),

        new createjs.Point(((1068.3334) * transformX) + moveX, ((197.6667) * transformY) + moveY),
        new createjs.Point(((1067.6666) * transformX) + moveX, ((197.3333) * transformY) + moveY),
        new createjs.Point(((1067.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),

        new createjs.Point(((1067.0000) * transformX) + moveX, ((196.3334) * transformY) + moveY),
        new createjs.Point(((1067.0000) * transformX) + moveX, ((195.6666) * transformY) + moveY),
        new createjs.Point(((1067.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),

        new createjs.Point(((1066.6667) * transformX) + moveX, ((195.0000) * transformY) + moveY),
        new createjs.Point(((1066.3333) * transformX) + moveX, ((195.0000) * transformY) + moveY),
        new createjs.Point(((1066.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),

        new createjs.Point(((1065.6667) * transformX) + moveX, ((194.0001) * transformY) + moveY),
        new createjs.Point(((1065.3333) * transformX) + moveX, ((192.9999) * transformY) + moveY),
        new createjs.Point(((1065.0000) * transformX) + moveX, ((192.0000) * transformY) + moveY),

        new createjs.Point(((1064.3334) * transformX) + moveX, ((191.6667) * transformY) + moveY),
        new createjs.Point(((1063.6666) * transformX) + moveX, ((191.3333) * transformY) + moveY),
        new createjs.Point(((1063.0000) * transformX) + moveX, ((191.0000) * transformY) + moveY),

        new createjs.Point(((1063.0000) * transformX) + moveX, ((190.3334) * transformY) + moveY),
        new createjs.Point(((1063.0000) * transformX) + moveX, ((189.6666) * transformY) + moveY),
        new createjs.Point(((1063.0000) * transformX) + moveX, ((189.0000) * transformY) + moveY),

        new createjs.Point(((1062.3334) * transformX) + moveX, ((188.6667) * transformY) + moveY),
        new createjs.Point(((1061.6666) * transformX) + moveX, ((188.3333) * transformY) + moveY),
        new createjs.Point(((1061.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),

        new createjs.Point(((1061.0000) * transformX) + moveX, ((187.3334) * transformY) + moveY),
        new createjs.Point(((1061.0000) * transformX) + moveX, ((186.6666) * transformY) + moveY),
        new createjs.Point(((1061.0000) * transformX) + moveX, ((186.0000) * transformY) + moveY),

        new createjs.Point(((1060.0001) * transformX) + moveX, ((185.3334) * transformY) + moveY),
        new createjs.Point(((1058.9999) * transformX) + moveX, ((184.6666) * transformY) + moveY),
        new createjs.Point(((1058.0000) * transformX) + moveX, ((184.0000) * transformY) + moveY),

        new createjs.Point(((1058.0000) * transformX) + moveX, ((183.3334) * transformY) + moveY),
        new createjs.Point(((1058.0000) * transformX) + moveX, ((182.6666) * transformY) + moveY),
        new createjs.Point(((1058.0000) * transformX) + moveX, ((182.0000) * transformY) + moveY),

        new createjs.Point(((1057.3334) * transformX) + moveX, ((181.6667) * transformY) + moveY),
        new createjs.Point(((1056.6666) * transformX) + moveX, ((181.3333) * transformY) + moveY),
        new createjs.Point(((1056.0000) * transformX) + moveX, ((181.0000) * transformY) + moveY),

        new createjs.Point(((1056.0000) * transformX) + moveX, ((180.3334) * transformY) + moveY),
        new createjs.Point(((1056.0000) * transformX) + moveX, ((179.6666) * transformY) + moveY),
        new createjs.Point(((1056.0000) * transformX) + moveX, ((179.0000) * transformY) + moveY),

        new createjs.Point(((1055.0001) * transformX) + moveX, ((178.3334) * transformY) + moveY),
        new createjs.Point(((1053.9999) * transformX) + moveX, ((177.6666) * transformY) + moveY),
        new createjs.Point(((1053.0000) * transformX) + moveX, ((177.0000) * transformY) + moveY),

        new createjs.Point(((1053.0000) * transformX) + moveX, ((176.3334) * transformY) + moveY),
        new createjs.Point(((1053.0000) * transformX) + moveX, ((175.6666) * transformY) + moveY),
        new createjs.Point(((1053.0000) * transformX) + moveX, ((175.0000) * transformY) + moveY),

        new createjs.Point(((1052.0001) * transformX) + moveX, ((174.3334) * transformY) + moveY),
        new createjs.Point(((1050.9999) * transformX) + moveX, ((173.6666) * transformY) + moveY),
        new createjs.Point(((1050.0000) * transformX) + moveX, ((173.0000) * transformY) + moveY),

        new createjs.Point(((1050.0000) * transformX) + moveX, ((172.3334) * transformY) + moveY),
        new createjs.Point(((1050.0000) * transformX) + moveX, ((171.6666) * transformY) + moveY),
        new createjs.Point(((1050.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),

        new createjs.Point(((1049.0001) * transformX) + moveX, ((170.3334) * transformY) + moveY),
        new createjs.Point(((1047.9999) * transformX) + moveX, ((169.6666) * transformY) + moveY),
        new createjs.Point(((1047.0000) * transformX) + moveX, ((169.0000) * transformY) + moveY),

        new createjs.Point(((1047.0000) * transformX) + moveX, ((168.3334) * transformY) + moveY),
        new createjs.Point(((1047.0000) * transformX) + moveX, ((167.6666) * transformY) + moveY),
        new createjs.Point(((1047.0000) * transformX) + moveX, ((167.0000) * transformY) + moveY),

        new createjs.Point(((1045.6668) * transformX) + moveX, ((166.0001) * transformY) + moveY),
        new createjs.Point(((1044.3332) * transformX) + moveX, ((164.9999) * transformY) + moveY),
        new createjs.Point(((1043.0000) * transformX) + moveX, ((164.0000) * transformY) + moveY),

        new createjs.Point(((1043.0000) * transformX) + moveX, ((163.3334) * transformY) + moveY),
        new createjs.Point(((1043.0000) * transformX) + moveX, ((162.6666) * transformY) + moveY),
        new createjs.Point(((1043.0000) * transformX) + moveX, ((162.0000) * transformY) + moveY),

        new createjs.Point(((1041.6668) * transformX) + moveX, ((161.0001) * transformY) + moveY),
        new createjs.Point(((1040.3332) * transformX) + moveX, ((159.9999) * transformY) + moveY),
        new createjs.Point(((1039.0000) * transformX) + moveX, ((159.0000) * transformY) + moveY),

        new createjs.Point(((1039.0000) * transformX) + moveX, ((158.3334) * transformY) + moveY),
        new createjs.Point(((1039.0000) * transformX) + moveX, ((157.6666) * transformY) + moveY),
        new createjs.Point(((1039.0000) * transformX) + moveX, ((157.0000) * transformY) + moveY),

        new createjs.Point(((1037.3335) * transformX) + moveX, ((155.6668) * transformY) + moveY),
        new createjs.Point(((1035.6665) * transformX) + moveX, ((154.3332) * transformY) + moveY),
        new createjs.Point(((1034.0000) * transformX) + moveX, ((153.0000) * transformY) + moveY),

        new createjs.Point(((1034.0000) * transformX) + moveX, ((152.3334) * transformY) + moveY),
        new createjs.Point(((1034.0000) * transformX) + moveX, ((151.6666) * transformY) + moveY),
        new createjs.Point(((1034.0000) * transformX) + moveX, ((151.0000) * transformY) + moveY),

        new createjs.Point(((1031.6669) * transformX) + moveX, ((149.0002) * transformY) + moveY),
        new createjs.Point(((1029.3331) * transformX) + moveX, ((146.9998) * transformY) + moveY),
        new createjs.Point(((1027.0000) * transformX) + moveX, ((145.0000) * transformY) + moveY),

        new createjs.Point(((1027.0000) * transformX) + moveX, ((144.3334) * transformY) + moveY),
        new createjs.Point(((1027.0000) * transformX) + moveX, ((143.6666) * transformY) + moveY),
        new createjs.Point(((1027.0000) * transformX) + moveX, ((143.0000) * transformY) + moveY),

        new createjs.Point(((1022.6671) * transformX) + moveX, ((139.0004) * transformY) + moveY),
        new createjs.Point(((1018.3329) * transformX) + moveX, ((134.9996) * transformY) + moveY),
        new createjs.Point(((1014.0000) * transformX) + moveX, ((131.0000) * transformY) + moveY),

        new createjs.Point(((1014.0000) * transformX) + moveX, ((130.3334) * transformY) + moveY),
        new createjs.Point(((1014.0000) * transformX) + moveX, ((129.6666) * transformY) + moveY),
        new createjs.Point(((1014.0000) * transformX) + moveX, ((129.0000) * transformY) + moveY),

        new createjs.Point(((1013.3334) * transformX) + moveX, ((128.6667) * transformY) + moveY),
        new createjs.Point(((1012.6666) * transformX) + moveX, ((128.3333) * transformY) + moveY),
        new createjs.Point(((1012.0000) * transformX) + moveX, ((128.0000) * transformY) + moveY),

        new createjs.Point(((1010.3335) * transformX) + moveX, ((126.0002) * transformY) + moveY),
        new createjs.Point(((1008.6665) * transformX) + moveX, ((123.9998) * transformY) + moveY),
        new createjs.Point(((1007.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),

        new createjs.Point(((1006.3334) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((1005.6666) * transformX) + moveX, ((122.0000) * transformY) + moveY),
        new createjs.Point(((1005.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),

        new createjs.Point(((1001.0004) * transformX) + moveX, ((117.6671) * transformY) + moveY),
        new createjs.Point(((996.9996) * transformX) + moveX, ((113.3329) * transformY) + moveY),
        new createjs.Point(((993.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),

        new createjs.Point(((992.3334) * transformX) + moveX, ((109.0000) * transformY) + moveY),
        new createjs.Point(((991.6666) * transformX) + moveX, ((109.0000) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),

        new createjs.Point(((989.3335) * transformX) + moveX, ((107.0002) * transformY) + moveY),
        new createjs.Point(((987.6665) * transformX) + moveX, ((104.9998) * transformY) + moveY),
        new createjs.Point(((986.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),

        new createjs.Point(((985.3334) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((984.6666) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((984.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),

        new createjs.Point(((982.6668) * transformX) + moveX, ((101.3335) * transformY) + moveY),
        new createjs.Point(((981.3332) * transformX) + moveX, ((99.6665) * transformY) + moveY),
        new createjs.Point(((980.0000) * transformX) + moveX, ((98.0000) * transformY) + moveY),

        new createjs.Point(((979.3334) * transformX) + moveX, ((98.0000) * transformY) + moveY),
        new createjs.Point(((978.6666) * transformX) + moveX, ((98.0000) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((98.0000) * transformY) + moveY),

        new createjs.Point(((977.0001) * transformX) + moveX, ((96.6668) * transformY) + moveY),
        new createjs.Point(((975.9999) * transformX) + moveX, ((95.3332) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((974.3334) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((973.6666) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((973.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((972.0001) * transformX) + moveX, ((92.6668) * transformY) + moveY),
        new createjs.Point(((970.9999) * transformX) + moveX, ((91.3332) * transformY) + moveY),
        new createjs.Point(((970.0000) * transformX) + moveX, ((90.0000) * transformY) + moveY),

        new createjs.Point(((969.3334) * transformX) + moveX, ((90.0000) * transformY) + moveY),
        new createjs.Point(((968.6666) * transformX) + moveX, ((90.0000) * transformY) + moveY),
        new createjs.Point(((968.0000) * transformX) + moveX, ((90.0000) * transformY) + moveY),

        new createjs.Point(((967.3334) * transformX) + moveX, ((89.0001) * transformY) + moveY),
        new createjs.Point(((966.6666) * transformX) + moveX, ((87.9999) * transformY) + moveY),
        new createjs.Point(((966.0000) * transformX) + moveX, ((87.0000) * transformY) + moveY),

        new createjs.Point(((965.3334) * transformX) + moveX, ((87.0000) * transformY) + moveY),
        new createjs.Point(((964.6666) * transformX) + moveX, ((87.0000) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((87.0000) * transformY) + moveY),

        new createjs.Point(((963.3334) * transformX) + moveX, ((86.0001) * transformY) + moveY),
        new createjs.Point(((962.6666) * transformX) + moveX, ((84.9999) * transformY) + moveY),
        new createjs.Point(((962.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),

        new createjs.Point(((961.3334) * transformX) + moveX, ((84.0000) * transformY) + moveY),
        new createjs.Point(((960.6666) * transformX) + moveX, ((84.0000) * transformY) + moveY),
        new createjs.Point(((960.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),

        new createjs.Point(((959.6667) * transformX) + moveX, ((83.3334) * transformY) + moveY),
        new createjs.Point(((959.3333) * transformX) + moveX, ((82.6666) * transformY) + moveY),
        new createjs.Point(((959.0000) * transformX) + moveX, ((82.0000) * transformY) + moveY),

        new createjs.Point(((958.3334) * transformX) + moveX, ((82.0000) * transformY) + moveY),
        new createjs.Point(((957.6666) * transformX) + moveX, ((82.0000) * transformY) + moveY),
        new createjs.Point(((957.0000) * transformX) + moveX, ((82.0000) * transformY) + moveY),

        new createjs.Point(((956.3334) * transformX) + moveX, ((81.0001) * transformY) + moveY),
        new createjs.Point(((955.6666) * transformX) + moveX, ((79.9999) * transformY) + moveY),
        new createjs.Point(((955.0000) * transformX) + moveX, ((79.0000) * transformY) + moveY),

        new createjs.Point(((954.3334) * transformX) + moveX, ((79.0000) * transformY) + moveY),
        new createjs.Point(((953.6666) * transformX) + moveX, ((79.0000) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((79.0000) * transformY) + moveY),

        new createjs.Point(((952.6667) * transformX) + moveX, ((78.3334) * transformY) + moveY),
        new createjs.Point(((952.3333) * transformX) + moveX, ((77.6666) * transformY) + moveY),
        new createjs.Point(((952.0000) * transformX) + moveX, ((77.0000) * transformY) + moveY),

        new createjs.Point(((951.3334) * transformX) + moveX, ((77.0000) * transformY) + moveY),
        new createjs.Point(((950.6666) * transformX) + moveX, ((77.0000) * transformY) + moveY),
        new createjs.Point(((950.0000) * transformX) + moveX, ((77.0000) * transformY) + moveY),

        new createjs.Point(((949.6667) * transformX) + moveX, ((76.3334) * transformY) + moveY),
        new createjs.Point(((949.3333) * transformX) + moveX, ((75.6666) * transformY) + moveY),
        new createjs.Point(((949.0000) * transformX) + moveX, ((75.0000) * transformY) + moveY),

        new createjs.Point(((948.3334) * transformX) + moveX, ((75.0000) * transformY) + moveY),
        new createjs.Point(((947.6666) * transformX) + moveX, ((75.0000) * transformY) + moveY),
        new createjs.Point(((947.0000) * transformX) + moveX, ((75.0000) * transformY) + moveY),

        new createjs.Point(((946.6667) * transformX) + moveX, ((74.3334) * transformY) + moveY),
        new createjs.Point(((946.3333) * transformX) + moveX, ((73.6666) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((73.0000) * transformY) + moveY),

        new createjs.Point(((945.3334) * transformX) + moveX, ((73.0000) * transformY) + moveY),
        new createjs.Point(((944.6666) * transformX) + moveX, ((73.0000) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((73.0000) * transformY) + moveY),

        new createjs.Point(((943.6667) * transformX) + moveX, ((72.3334) * transformY) + moveY),
        new createjs.Point(((943.3333) * transformX) + moveX, ((71.6666) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((71.0000) * transformY) + moveY),

        new createjs.Point(((941.6668) * transformX) + moveX, ((70.6667) * transformY) + moveY),
        new createjs.Point(((940.3332) * transformX) + moveX, ((70.3333) * transformY) + moveY),
        new createjs.Point(((939.0000) * transformX) + moveX, ((70.0000) * transformY) + moveY),

        new createjs.Point(((939.0000) * transformX) + moveX, ((69.6667) * transformY) + moveY),
        new createjs.Point(((939.0000) * transformX) + moveX, ((69.3333) * transformY) + moveY),
        new createjs.Point(((939.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((938.0001) * transformX) + moveX, ((68.6667) * transformY) + moveY),
        new createjs.Point(((936.9999) * transformX) + moveX, ((68.3333) * transformY) + moveY),
        new createjs.Point(((936.0000) * transformX) + moveX, ((68.0000) * transformY) + moveY),

        new createjs.Point(((935.6667) * transformX) + moveX, ((67.3334) * transformY) + moveY),
        new createjs.Point(((935.3333) * transformX) + moveX, ((66.6666) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((934.3334) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((933.6666) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((933.0000) * transformX) + moveX, ((65.6667) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((65.3333) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((65.0000) * transformY) + moveY),

        new createjs.Point(((932.3334) * transformX) + moveX, ((65.0000) * transformY) + moveY),
        new createjs.Point(((931.6666) * transformX) + moveX, ((65.0000) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((65.0000) * transformY) + moveY),

        new createjs.Point(((930.6667) * transformX) + moveX, ((64.3334) * transformY) + moveY),
        new createjs.Point(((930.3333) * transformX) + moveX, ((63.6666) * transformY) + moveY),
        new createjs.Point(((930.0000) * transformX) + moveX, ((63.0000) * transformY) + moveY),

        new createjs.Point(((928.0002) * transformX) + moveX, ((62.3334) * transformY) + moveY),
        new createjs.Point(((925.9998) * transformX) + moveX, ((61.6666) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((61.0000) * transformY) + moveY),

        new createjs.Point(((923.6667) * transformX) + moveX, ((60.3334) * transformY) + moveY),
        new createjs.Point(((923.3333) * transformX) + moveX, ((59.6666) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((922.3334) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((921.6666) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((921.0000) * transformX) + moveX, ((58.6667) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((58.3333) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((58.0000) * transformY) + moveY),

        new createjs.Point(((920.3334) * transformX) + moveX, ((58.0000) * transformY) + moveY),
        new createjs.Point(((919.6666) * transformX) + moveX, ((58.0000) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((58.0000) * transformY) + moveY),

        new createjs.Point(((919.0000) * transformX) + moveX, ((57.6667) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((57.3333) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((57.0000) * transformY) + moveY),

        new createjs.Point(((918.3334) * transformX) + moveX, ((57.0000) * transformY) + moveY),
        new createjs.Point(((917.6666) * transformX) + moveX, ((57.0000) * transformY) + moveY),
        new createjs.Point(((917.0000) * transformX) + moveX, ((57.0000) * transformY) + moveY),

        new createjs.Point(((917.0000) * transformX) + moveX, ((56.6667) * transformY) + moveY),
        new createjs.Point(((917.0000) * transformX) + moveX, ((56.3333) * transformY) + moveY),
        new createjs.Point(((917.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((916.3334) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((915.6666) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((892.8176) * transformX) + moveX, ((42.0674) * transformY) + moveY),
        new createjs.Point(((865.5124) * transformX) + moveX, ((32.5560) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((837.6668) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((836.3332) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((835.0000) * transformX) + moveX, ((22.6667) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((22.3333) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((834.0001) * transformX) + moveX, ((22.0000) * transformY) + moveY),
        new createjs.Point(((832.9999) * transformX) + moveX, ((22.0000) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((832.0000) * transformX) + moveX, ((21.6667) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((21.3333) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((831.0001) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((829.9999) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((829.0000) * transformX) + moveX, ((20.6667) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((20.3333) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((20.0000) * transformY) + moveY),

        new createjs.Point(((826.3336) * transformX) + moveX, ((19.6667) * transformY) + moveY),
        new createjs.Point(((823.6664) * transformX) + moveX, ((19.3333) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((19.0000) * transformY) + moveY),

        new createjs.Point(((821.0000) * transformX) + moveX, ((18.6667) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((18.3333) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((820.0001) * transformX) + moveX, ((18.0000) * transformY) + moveY),
        new createjs.Point(((818.9999) * transformX) + moveX, ((18.0000) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((818.0000) * transformX) + moveX, ((17.6667) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((17.3333) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((17.0000) * transformY) + moveY),

        new createjs.Point(((816.6668) * transformX) + moveX, ((17.0000) * transformY) + moveY),
        new createjs.Point(((815.3332) * transformX) + moveX, ((17.0000) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((17.0000) * transformY) + moveY),

        new createjs.Point(((814.0000) * transformX) + moveX, ((16.6667) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((16.3333) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((812.6668) * transformX) + moveX, ((16.0000) * transformY) + moveY),
        new createjs.Point(((811.3332) * transformX) + moveX, ((16.0000) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((810.0000) * transformX) + moveX, ((15.6667) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((15.3333) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((807.0003) * transformX) + moveX, ((14.6667) * transformY) + moveY),
        new createjs.Point(((803.9997) * transformX) + moveX, ((14.3333) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((14.0000) * transformY) + moveY),

        new createjs.Point(((801.0000) * transformX) + moveX, ((13.6667) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((13.3333) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((13.0000) * transformY) + moveY),

        new createjs.Point(((799.3335) * transformX) + moveX, ((13.0000) * transformY) + moveY),
        new createjs.Point(((797.6665) * transformX) + moveX, ((13.0000) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((13.0000) * transformY) + moveY),

        new createjs.Point(((796.0000) * transformX) + moveX, ((12.6667) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((12.3333) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((794.6668) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((793.3332) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((792.0000) * transformX) + moveX, ((11.6667) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((11.3333) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((11.0000) * transformY) + moveY),

        new createjs.Point(((790.3335) * transformX) + moveX, ((11.0000) * transformY) + moveY),
        new createjs.Point(((788.6665) * transformX) + moveX, ((11.0000) * transformY) + moveY),
        new createjs.Point(((787.0000) * transformX) + moveX, ((11.0000) * transformY) + moveY),

        new createjs.Point(((787.0000) * transformX) + moveX, ((10.6667) * transformY) + moveY),
        new createjs.Point(((787.0000) * transformX) + moveX, ((10.3333) * transformY) + moveY),
        new createjs.Point(((787.0000) * transformX) + moveX, ((10.0000) * transformY) + moveY),

        new createjs.Point(((785.0002) * transformX) + moveX, ((10.0000) * transformY) + moveY),
        new createjs.Point(((782.9998) * transformX) + moveX, ((10.0000) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((10.0000) * transformY) + moveY),

        new createjs.Point(((781.0000) * transformX) + moveX, ((9.6667) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((9.3333) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((779.3335) * transformX) + moveX, ((9.0000) * transformY) + moveY),
        new createjs.Point(((777.6665) * transformX) + moveX, ((9.0000) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((776.0000) * transformX) + moveX, ((8.6667) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((8.3333) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((8.0000) * transformY) + moveY),

        new createjs.Point(((774.0002) * transformX) + moveX, ((8.0000) * transformY) + moveY),
        new createjs.Point(((771.9998) * transformX) + moveX, ((8.0000) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((8.0000) * transformY) + moveY),

        new createjs.Point(((770.0000) * transformX) + moveX, ((7.6667) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((7.3333) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((7.0000) * transformY) + moveY),

        new createjs.Point(((767.6669) * transformX) + moveX, ((7.0000) * transformY) + moveY),
        new createjs.Point(((765.3331) * transformX) + moveX, ((7.0000) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((7.0000) * transformY) + moveY),

        new createjs.Point(((763.0000) * transformX) + moveX, ((6.6667) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((6.3333) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((6.0000) * transformY) + moveY),

        new createjs.Point(((760.6669) * transformX) + moveX, ((6.0000) * transformY) + moveY),
        new createjs.Point(((758.3331) * transformX) + moveX, ((6.0000) * transformY) + moveY),
        new createjs.Point(((756.0000) * transformX) + moveX, ((6.0000) * transformY) + moveY),

        new createjs.Point(((756.0000) * transformX) + moveX, ((5.6667) * transformY) + moveY),
        new createjs.Point(((756.0000) * transformX) + moveX, ((5.3333) * transformY) + moveY),
        new createjs.Point(((756.0000) * transformX) + moveX, ((5.0000) * transformY) + moveY),

        new createjs.Point(((753.3336) * transformX) + moveX, ((5.0000) * transformY) + moveY),
        new createjs.Point(((750.6664) * transformX) + moveX, ((5.0000) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((5.0000) * transformY) + moveY),

        new createjs.Point(((748.0000) * transformX) + moveX, ((4.6667) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((4.3333) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((4.0000) * transformY) + moveY),

        new createjs.Point(((745.3336) * transformX) + moveX, ((4.0000) * transformY) + moveY),
        new createjs.Point(((742.6664) * transformX) + moveX, ((4.0000) * transformY) + moveY),
        new createjs.Point(((740.0000) * transformX) + moveX, ((4.0000) * transformY) + moveY),

        new createjs.Point(((740.0000) * transformX) + moveX, ((3.6667) * transformY) + moveY),
        new createjs.Point(((740.0000) * transformX) + moveX, ((3.3333) * transformY) + moveY),
        new createjs.Point(((740.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((736.6670) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((733.3330) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((730.0000) * transformX) + moveX, ((2.6667) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((2.3333) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((725.6671) * transformX) + moveX, ((2.0000) * transformY) + moveY),
        new createjs.Point(((721.3329) * transformX) + moveX, ((2.0000) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((702.1079) * transformX) + moveX, ((-2.1623) * transformY) + moveY),
        new createjs.Point(((679.1245) * transformX) + moveX, ((0.0054) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY),

        new createjs.Point(((648.9486) * transformX) + moveX, ((-0.0036) * transformY) + moveY),
        new createjs.Point(((635.2242) * transformX) + moveX, ((-0.6405) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((617.0009) * transformX) + moveX, ((2.6666) * transformY) + moveY),
        new createjs.Point(((607.9991) * transformX) + moveX, ((3.3334) * transformY) + moveY),
        new createjs.Point(((599.0000) * transformX) + moveX, ((4.0000) * transformY) + moveY),

        new createjs.Point(((593.7967) * transformX) + moveX, ((5.5358) * transformY) + moveY),
        new createjs.Point(((586.1806) * transformX) + moveX, ((6.4034) * transformY) + moveY),
        new createjs.Point(((581.0000) * transformX) + moveX, ((8.0000) * transformY) + moveY),

        new createjs.Point(((579.3335) * transformX) + moveX, ((8.0000) * transformY) + moveY),
        new createjs.Point(((577.6665) * transformX) + moveX, ((8.0000) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((8.0000) * transformY) + moveY),

        new createjs.Point(((576.0000) * transformX) + moveX, ((8.3333) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((8.6667) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((574.3335) * transformX) + moveX, ((9.0000) * transformY) + moveY),
        new createjs.Point(((572.6665) * transformX) + moveX, ((9.0000) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((571.0000) * transformX) + moveX, ((9.3333) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((9.6667) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((10.0000) * transformY) + moveY),

        new createjs.Point(((569.6668) * transformX) + moveX, ((10.0000) * transformY) + moveY),
        new createjs.Point(((568.3332) * transformX) + moveX, ((10.0000) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((10.0000) * transformY) + moveY),

        new createjs.Point(((567.0000) * transformX) + moveX, ((10.3333) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((10.6667) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((11.0000) * transformY) + moveY),

        new createjs.Point(((565.3335) * transformX) + moveX, ((11.0000) * transformY) + moveY),
        new createjs.Point(((563.6665) * transformX) + moveX, ((11.0000) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((11.0000) * transformY) + moveY),

        new createjs.Point(((562.0000) * transformX) + moveX, ((11.3333) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((11.6667) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((558.0004) * transformX) + moveX, ((12.6666) * transformY) + moveY),
        new createjs.Point(((553.9996) * transformX) + moveX, ((13.3334) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((14.0000) * transformY) + moveY),

        new createjs.Point(((550.0000) * transformX) + moveX, ((14.3333) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((14.6667) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((548.6668) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((547.3332) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((546.0000) * transformX) + moveX, ((15.3333) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((15.6667) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((543.6669) * transformX) + moveX, ((16.3333) * transformY) + moveY),
        new createjs.Point(((541.3331) * transformX) + moveX, ((16.6667) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((17.0000) * transformY) + moveY),

        new createjs.Point(((539.0000) * transformX) + moveX, ((17.3333) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((17.6667) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((536.6669) * transformX) + moveX, ((18.3333) * transformY) + moveY),
        new createjs.Point(((534.3331) * transformX) + moveX, ((18.6667) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((19.0000) * transformY) + moveY),

        new createjs.Point(((532.0000) * transformX) + moveX, ((19.3333) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((19.6667) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((20.0000) * transformY) + moveY),

        new createjs.Point(((531.0001) * transformX) + moveX, ((20.0000) * transformY) + moveY),
        new createjs.Point(((529.9999) * transformX) + moveX, ((20.0000) * transformY) + moveY),
        new createjs.Point(((529.0000) * transformX) + moveX, ((20.0000) * transformY) + moveY),

        new createjs.Point(((529.0000) * transformX) + moveX, ((20.3333) * transformY) + moveY),
        new createjs.Point(((529.0000) * transformX) + moveX, ((20.6667) * transformY) + moveY),
        new createjs.Point(((529.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((528.0001) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((526.9999) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((526.0000) * transformX) + moveX, ((21.3333) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((21.6667) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((525.0001) * transformX) + moveX, ((22.0000) * transformY) + moveY),
        new createjs.Point(((523.9999) * transformX) + moveX, ((22.0000) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((523.0000) * transformX) + moveX, ((22.3333) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((22.6667) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((520.0003) * transformX) + moveX, ((23.6666) * transformY) + moveY),
        new createjs.Point(((516.9997) * transformX) + moveX, ((24.3334) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((25.0000) * transformY) + moveY),

        new createjs.Point(((514.0000) * transformX) + moveX, ((25.3333) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((25.6667) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((26.0000) * transformY) + moveY),

        new createjs.Point(((513.3334) * transformX) + moveX, ((26.0000) * transformY) + moveY),
        new createjs.Point(((512.6666) * transformX) + moveX, ((26.0000) * transformY) + moveY),
        new createjs.Point(((512.0000) * transformX) + moveX, ((26.0000) * transformY) + moveY),

        new createjs.Point(((512.0000) * transformX) + moveX, ((26.3333) * transformY) + moveY),
        new createjs.Point(((512.0000) * transformX) + moveX, ((26.6667) * transformY) + moveY),
        new createjs.Point(((512.0000) * transformX) + moveX, ((27.0000) * transformY) + moveY),

        new createjs.Point(((510.0002) * transformX) + moveX, ((27.3333) * transformY) + moveY),
        new createjs.Point(((507.9998) * transformX) + moveX, ((27.6667) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((28.0000) * transformY) + moveY),

        new createjs.Point(((506.0000) * transformX) + moveX, ((28.3333) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((28.6667) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((505.3334) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((504.6666) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((504.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((504.0000) * transformX) + moveX, ((29.3333) * transformY) + moveY),
        new createjs.Point(((504.0000) * transformX) + moveX, ((29.6667) * transformY) + moveY),
        new createjs.Point(((504.0000) * transformX) + moveX, ((30.0000) * transformY) + moveY),

        new createjs.Point(((503.0001) * transformX) + moveX, ((30.0000) * transformY) + moveY),
        new createjs.Point(((501.9999) * transformX) + moveX, ((30.0000) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((30.0000) * transformY) + moveY),

        new createjs.Point(((501.0000) * transformX) + moveX, ((30.3333) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((30.6667) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((499.3335) * transformX) + moveX, ((31.3333) * transformY) + moveY),
        new createjs.Point(((497.6665) * transformX) + moveX, ((31.6667) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((32.0000) * transformY) + moveY),

        new createjs.Point(((496.0000) * transformX) + moveX, ((32.3333) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((32.6667) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((33.0000) * transformY) + moveY),

        new createjs.Point(((495.3334) * transformX) + moveX, ((33.0000) * transformY) + moveY),
        new createjs.Point(((494.6666) * transformX) + moveX, ((33.0000) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((33.0000) * transformY) + moveY),

        new createjs.Point(((494.0000) * transformX) + moveX, ((33.3333) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((33.6667) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((493.0001) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((491.9999) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((491.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((491.0000) * transformX) + moveX, ((34.3333) * transformY) + moveY),
        new createjs.Point(((491.0000) * transformX) + moveX, ((34.6667) * transformY) + moveY),
        new createjs.Point(((491.0000) * transformX) + moveX, ((35.0000) * transformY) + moveY),

        new createjs.Point(((489.6668) * transformX) + moveX, ((35.3333) * transformY) + moveY),
        new createjs.Point(((488.3332) * transformX) + moveX, ((35.6667) * transformY) + moveY),
        new createjs.Point(((487.0000) * transformX) + moveX, ((36.0000) * transformY) + moveY),

        new createjs.Point(((487.0000) * transformX) + moveX, ((36.3333) * transformY) + moveY),
        new createjs.Point(((487.0000) * transformX) + moveX, ((36.6667) * transformY) + moveY),
        new createjs.Point(((487.0000) * transformX) + moveX, ((37.0000) * transformY) + moveY),

        new createjs.Point(((486.0001) * transformX) + moveX, ((37.0000) * transformY) + moveY),
        new createjs.Point(((484.9999) * transformX) + moveX, ((37.0000) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((37.0000) * transformY) + moveY),

        new createjs.Point(((484.0000) * transformX) + moveX, ((37.3333) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((37.6667) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((483.3334) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((482.6666) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((482.0000) * transformX) + moveX, ((38.3333) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((38.6667) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((39.0000) * transformY) + moveY),

        new createjs.Point(((480.0002) * transformX) + moveX, ((39.6666) * transformY) + moveY),
        new createjs.Point(((477.9998) * transformX) + moveX, ((40.3334) * transformY) + moveY),
        new createjs.Point(((476.0000) * transformX) + moveX, ((41.0000) * transformY) + moveY),
        new createjs.Point(endX, endY)

    ];

    // var points2 = [
    //     new createjs.Point(startX2, startY2),
    //     new createjs.Point(endX2, endY2)
    // ];
    var points1Final = points;

    // var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    // var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 0", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("8", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        loop = false;
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 60).call(function() {
            gotoLastAndStop(Stage1);
            Stage1.shape.visible = false;
        })).on('change', (function(event) {
            if (currentMotionStep == 1 && !stopped) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        // this.timeline.addTween(cjs.Tween.get(bar2).wait(40).to({
        //     guide: {
        //         path: motionPath2
        //     }
        // }, 20).wait(50).call(function() {
        //     gotoLastAndStop(Stage1);
        //     Stage1.shape.visible = false;
        // })).on('change', (function(event) {
        //     if (currentMotionStep == 2) {
        //         bar2 = drawCharacter(thisStage, bar2, true, event);
        //     }
        // }));



    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;
    var stopped = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];
                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }
            
            if (thisbar.x == 552.024 && thisbar.y == 197.5047113212554) { // finished plotting 1/3 the number
                Stage1.shape.visible = false;
            } else if (thisbar.x == startX && thisbar.y == startY) { // plot 1st point
                Stage1.shape.visible = true;
            }

            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();
        thisStage.answerText = new cjs.Text("3 är lika med 3.", "36px 'Myriad Pro'")
        thisStage.answerText.lineHeight = 19;
        thisStage.answerText.textAlign = 'center';
        thisStage.answerText.setTransform(550, 340);

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.32)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            if (stopped) {
                Stage1.shape.visible = true;
            }
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.paused = false;
            stopped = false;
            thisStage.movie.play();

        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            stopped = false;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);

        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }


    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        // thisStage.pencil=thisStage.movie.pencil;
        // thisStage.addChild(thisStage.pencil);
        if (thisStage.pencil) {
            thisStage.removeChild(thisStage.pencil);
        }
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function gotoLastAndStop(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        stopped = true;
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);


        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        thisStage.pencil = thisStage.movie.pencil;
        thisStage.addChild(thisStage.pencil);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145);
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
