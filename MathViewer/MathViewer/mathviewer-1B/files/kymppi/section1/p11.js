(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p11_1.png",
            id: "p11_1"
        }, {
            src: "images/p11_2.png",
            id: "p11_2"
        }]
    };

    (lib.p11_1 = function() {
        this.initialize(img.p11_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p11_2 = function() {
        this.initialize(img.p11_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {

        this.text = new cjs.Text("11", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(551, 658);


        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);


        this.addChild(this.shape_1, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p11_1();
        this.instance.setTransform(0, 52, 0.47, 0.47);

        this.text = new cjs.Text("Hur många kronor är det?", "16px 'Myriad Pro'");
        this.text.setTransform(21, 40);

        this.text_1 = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(2, 40);

        this.text_2 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#9D9C9C");
        this.text_2.setTransform(147, 131);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(5, 30, 164, 85, 10);
        this.roundRect1.setTransform(0, 27);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#7d7d7d').drawRoundRect(5, 78, 164, 85, 10);
        this.roundRect2.setTransform(0, 70);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("").s('#7d7d7d').drawRoundRect(5, 118, 164, 87, 10);
        this.roundRect3.setTransform(0, 120);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("").s('#7d7d7d').drawRoundRect(174, 32, 164, 85, 10);
        this.roundRect4.setTransform(0, 25);

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("").s('#7d7d7d').drawRoundRect(174, 122, 164, 87, 10);
        this.roundRect5.setTransform(0, 25);

        this.roundRect6 = new cjs.Shape();
        this.roundRect6.graphics.f("").s('#7d7d7d').drawRoundRect(174, 215, 164, 87, 10);
        this.roundRect6.setTransform(0, 25);

        this.roundRect7 = new cjs.Shape();
        this.roundRect7.graphics.f("").s('#7d7d7d').drawRoundRect(343, 32, 164, 85, 10);
        this.roundRect7.setTransform(0, 25);

        this.roundRect8 = new cjs.Shape();
        this.roundRect8.graphics.f("").s('#7d7d7d').drawRoundRect(343, 122, 164, 87, 10);
        this.roundRect8.setTransform(0, 25);

        this.roundRect9 = new cjs.Shape();
        this.roundRect9.graphics.f("").s('#7d7d7d').drawRoundRect(343, 215, 164, 87, 10);
        this.roundRect9.setTransform(0, 25);


        var lineArr = [];
        var lineX = [105, 272, 437, 105, 272, 437, 105, 272, 437, ];
        var lineY = [93, 93, 93, 184, 184, 184, 275, 275, 275];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(55, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }


        this.addChild(this.text, this.text_1, this.text_2, this.instance, this.roundRect1, this.roundRect2, this.roundRect3,
            this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.roundRect9);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }




    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 330);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p11_2();
        this.instance.setTransform(7.5, 35, 0.47, 0.47);

        this.text = new cjs.Text("Likadana leksaker har samma pris.", "16px 'Myriad Pro'");
        this.text.setTransform(23, 0);

        this.text_1 = new cjs.Text("7.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(4, 0);

        this.text_2 = new cjs.Text("Vad kostar varje leksak?", "16px 'Myriad Pro'");
        this.text_2.setTransform(23, 20);

        //outer rect

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(4, 0, 249, 99, 10);
        this.roundRect1.setTransform(0, 32);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#7d7d7d').drawRoundRect(4, 61, 249, 102, 10);
        this.roundRect2.setTransform(0, 75);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("").s('#7d7d7d').drawRoundRect(258, 2, 254, 99, 10);
        this.roundRect3.setTransform(0, 30);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("").s('#7d7d7d').drawRoundRect(258, 105, 253, 102, 10);
        this.roundRect4.setTransform(0, 30);

        //inner green rect

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("").s('#90BD22').drawRoundRect(4, 32, 240, 68, 10);
        this.roundRect5.setTransform(5, 25);

        this.roundRect6 = new cjs.Shape();
        this.roundRect6.graphics.f("").s('#90BD22').drawRoundRect(9, 89, 240, 68, 10);
        this.roundRect6.setTransform(0, 75);

        this.roundRect7 = new cjs.Shape();
        this.roundRect7.graphics.f("").s('#90BD22').drawRoundRect(263, 32, 243, 68, 10);
        this.roundRect7.setTransform(0, 25);

        this.roundRect8 = new cjs.Shape();
        this.roundRect8.graphics.f("").s('#90BD22').drawRoundRect(263, 89, 243, 68, 10);
        this.roundRect8.setTransform(0, 75);



        this.text_3 = new cjs.Text("10 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(74, 50);

        this.text_4 = new cjs.Text("10 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(330, 50);

        this.text_5 = new cjs.Text("10 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(74, 157);

        this.text_6 = new cjs.Text("10 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(330, 157);


        this.text_7 = new cjs.Text("4 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_7.setTransform(15, 75);
        this.text_7.skewX = 20;
        this.text_7.skewY = 20;


        this.text_8 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_8.setTransform(158, 83);
        this.text_8.skewX = -20;
        this.text_8.skewY = -20;


        this.text_9 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_9.setTransform(228, 80);
        this.text_9.skewX = -20;
        this.text_9.skewY = -20;

        this.text_10 = new cjs.Text("6 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_10.setTransform(313, 80);
        this.text_10.skewX = -20;
        this.text_10.skewY = -20;


        this.text_11 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_11.setTransform(407, 96);
        this.text_11.skewX = -20;
        this.text_11.skewY = -20;


        this.text_12 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_12.setTransform(487, 95);
        this.text_12.skewX = -20;
        this.text_12.skewY = -20;


        this.text_13 = new cjs.Text("2 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_13.setTransform(35, 179);
        this.text_13.skewX = 20;
        this.text_13.skewY = 20;


        this.text_14 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_14.setTransform(157, 188);
        this.text_14.skewX = -20;
        this.text_14.skewY = -20;

        this.text_15 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_15.setTransform(223, 188);
        this.text_15.skewX = -20;
        this.text_15.skewY = -20;


        this.text_16 = new cjs.Text("8 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_16.setTransform(294,180);
        this.text_16.skewX = 20;
        this.text_16.skewY = 20;

        this.text_17 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_17.setTransform(402, 194);
        this.text_17.skewX = -20;
        this.text_17.skewY = -20;


        this.text_18 = new cjs.Text("kr", "bold 16px 'UusiTekstausMajema'");
        this.text_18.setTransform(485, 194);
        this.text_18.skewX = -20;
        this.text_18.skewY = -20;

        
        this.addChild(this.text, this.text_1, this.text_2, this.instance, this.roundRect1, this.roundRect2, this.roundRect3,
            this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.text_3, this.text_4,
            this.text_5, this.text_6, this.text_7,this.text_8,this.text_9,this.text_10,
            this.text_11,this.text_12,this.text_13,this.text_14,this.text_15,this.text_16,this.text_17,this.text_18);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 280);


    (lib.p6 = function() {
        this.initialize();


        this.v2 = new lib.Symbol3();
        this.v2.setTransform(310, 435, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(310, 85, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
