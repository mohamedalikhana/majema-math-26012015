(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p103_1.png",
            id: "p103_1"
        }, {
            src: "images/p103_2.png",
            id: "p103_2"
        }, {
            src: "images/p103_3.png",
            id: "p103_3"
        }]
    };

    (lib.p103_1 = function() {
        this.initialize(img.p103_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p103_2 = function() {
        this.initialize(img.p103_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p103_3 = function() {
        this.initialize(img.p103_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("103", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(553, 650);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);

        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        var adjustX = 5;
        var adjustY = 12;

        this.text_1 = new cjs.Text("Här är dubbelt så många", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(48 - adjustX, 0 + adjustY);

        this.text_2 = new cjs.Text("som", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(250 - adjustX, 0 + adjustY);

        this.text_3 = new cjs.Text(".", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(303 - adjustX, 0 + adjustY);

        this.instance_1 = new lib.p103_1();
        this.instance_1.setTransform(0 - adjustX, -20 + adjustY, 0.369, 0.369);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(0, 35, 508, 132, 10);
        this.round_Rect1.setTransform(0 - adjustX, 0 + adjustY);

        this.instance_2 = new lib.p103_2();
        this.instance_2.setTransform(32 - adjustX, 54 + adjustY, 0.37, 0.37);

        this.addChild(this.text_1, this.instance_1, this.round_Rect1, this.instance_2, this.text_2, this.text_3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 175);


    (lib.Symbol2 = function() {
        this.initialize();

        var adjustX = 9;
        var adjustY = 5;

        this.text_1 = new cjs.Text("Rita dubbelt så många", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0 - adjustX, 7.6 + adjustY);

        this.instance_1 = new lib.p103_3();
        this.instance_1.setTransform(155 - adjustX, -10 + adjustY, 0.369, 0.369);

        this.text_2 = new cjs.Text("som", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(183 - adjustX, 7.6 + adjustY);

        this.text_3 = new cjs.Text(". Rita på 2 olika sätt.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(235 - adjustX, 7.6 + adjustY);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 38, 508, 132, 10);
        this.round_Rect1.setTransform(0 - adjustX, 0 + adjustY);

        this.round_Rect2 = this.round_Rect1.clone(true);
        this.round_Rect2.setTransform(0 - adjustX, 143);

        this.text_4 = new cjs.Text("Visa och berätta för en kamrat.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(0 - adjustX, 328 + adjustY);

        this.addChild(this.text_1, this.instance_1, this.text_2, this.text_3, this.round_Rect1, this.round_Rect2, this.text_4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 343);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(312.8, 266, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(316.8, 461, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
