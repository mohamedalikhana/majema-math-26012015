(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("förstå begreppet talfamilj", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(438.5, 651);

        this.text_2 = new cjs.Text("79", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(555, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Talfamiljer", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(95.5, 25);

        this.text_4 = new cjs.Text("27", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(51, 22);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_0 = new cjs.Text("En talfamilj är 3 tal som du kan använda till 2 additioner och 2 subtraktioner.", "16px 'Myriad Pro'");
        this.text_0.lineHeight = 19;
        this.text_0.setTransform(12, 8);

        this.text = new cjs.Text("5", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 45;
        this.text.setTransform(107, 86);

        this.text_1 = new cjs.Text("+", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(128, 86);

        this.text_2 = new cjs.Text("3", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(147, 86);

        this.text_3 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(168, 86);

        this.text_4 = new cjs.Text("8", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(187, 86);

        this.text_5 = new cjs.Text("8", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 45;
        this.text_5.setTransform(310, 86);

        this.text_6 = new cjs.Text("–", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 45;
        this.text_6.setTransform(331, 86);

        this.text_7 = new cjs.Text("5", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 45;
        this.text_7.setTransform(350, 86);

        this.text_8 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 45;
        this.text_8.setTransform(371, 86);

        this.text_9 = new cjs.Text("3", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 45;
        this.text_9.setTransform(390, 86);

        this.text_10 = new cjs.Text("3", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 45;
        this.text_10.setTransform(107, 116);

        this.text_11 = new cjs.Text("+", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_11.lineHeight = 45;
        this.text_11.setTransform(128, 116);

        this.text_12 = new cjs.Text("5", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_12.lineHeight = 45;
        this.text_12.setTransform(147, 116);

        this.text_13 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_13.lineHeight = 45;
        this.text_13.setTransform(168, 116);

        this.text_14 = new cjs.Text("8", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_14.lineHeight = 45;
        this.text_14.setTransform(187, 116);

        this.text_15 = new cjs.Text("8", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_15.lineHeight = 45;
        this.text_15.setTransform(310, 116);

        this.text_16 = new cjs.Text("–", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_16.lineHeight = 45;
        this.text_16.setTransform(331, 116);

        this.text_17 = new cjs.Text("3", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_17.lineHeight = 45;
        this.text_17.setTransform(350, 116);

        this.text_18 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_18.lineHeight = 45;
        this.text_18.setTransform(371, 116);

        this.text_19 = new cjs.Text("5", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_19.lineHeight = 45;
        this.text_19.setTransform(390, 116);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) { // red ball
                this.shape_group1.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(169 + (columnSpace * 25), 75 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) { // blue ball
                this.shape_group2.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(305 + (columnSpace * 25), 75 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['96', '126'];
        var xPos = 82,
            padding,
            yPos,
            rectWidth = 100,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 25;
                if (i == 1) {
                    padding = padding + 39;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);


        // Block-1
        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(240.9, 37.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_37.setTransform(226.1, 37.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(210.9, 37.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_39.setTransform(195.3, 37.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(179.9, 37.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(164.2, 37.4);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(148.2, 37.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(132.7, 37.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(117.2, 37.3);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(99.6, 37.3);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_47.setTransform(242.2, 43.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_48.setTransform(242.2, 43.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_49.setTransform(227.5, 43.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_50.setTransform(227.5, 43.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_51.setTransform(149.5, 43.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_52.setTransform(149.5, 43.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_53.setTransform(212.4, 43.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_54.setTransform(212.4, 43.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_55.setTransform(134.3, 43.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_56.setTransform(134.3, 43.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_57.setTransform(196.7, 43.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_58.setTransform(196.7, 43.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_59.setTransform(118.6, 43.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_60.setTransform(118.6, 43.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_61.setTransform(181.2, 43.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_62.setTransform(181.2, 43.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_63.setTransform(101.2, 43.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_64.setTransform(101.2, 43.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_65.setTransform(165.5, 43.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_66.setTransform(165.5, 43.9);

        // Block-2
        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_114.setTransform(417.7, 37.4);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_70.setTransform(399.9, 37.4);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_71.setTransform(383.1, 37.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_72.setTransform(367.9, 37.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_73.setTransform(352.3, 37.4);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_74.setTransform(336.9, 37.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_75.setTransform(321.2, 37.4);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(305.2, 37.4);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(289.7, 37.3);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(274.2, 37.3);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(258.6, 37.3);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_80.setTransform(401.2, 43.9);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_81.setTransform(401.2, 43.9);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_82.setTransform(384.5, 43.9);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_83.setTransform(384.5, 43.9);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_84.setTransform(306.5, 43.9);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_85.setTransform(306.5, 43.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_86.setTransform(369.4, 43.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_87.setTransform(369.4, 43.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(291.3, 43.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(291.3, 43.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_90.setTransform(353.7, 43.9);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_91.setTransform(353.7, 43.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(275.6, 43.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(275.6, 43.9);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_94.setTransform(338.2, 43.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_95.setTransform(338.2, 43.9);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(260.2, 43.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(260.2, 43.9);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_98.setTransform(322.5, 43.9);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_99.setTransform(322.5, 43.9);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_111.setTransform(418.5, 43.9);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_112.setTransform(418.5, 43.9);

        // Inner White block
        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(92, 36, 338, 124, 5);
        this.shape_100.setTransform(0, 0);

        // Main yellow block

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFF679").s("#959C9D").ss(1).drawRoundRect(5, 0, 525, 169, 10);
        this.shape_115.setTransform(0, 0);

        this.addChild(this.shape_115, this.shape_100, this.shape_112, this.shape_111, this.shape_114);
        this.addChild(this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text);
        this.addChild(this.text_0, this.shape_group1, this.shape_group2, this.textbox_group1, this.text_6, this.text_7, this.text_8, this.text_9);
        this.addChild(this.text_10, this.text_11, this.text_12, this.text_13, this.text_14, this.text_15, this.text_16, this.text_17, this.text_18, this.text_19);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Skriv en talfamilj.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 14);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 14);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 259, 127, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(264, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 157);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(264, 157);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 289);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(264, 289);

        var arrTxtbox = ['100', '130', '232', '262', '363', '393'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 100,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 4;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 18;
                if (i == 1) {
                    padding = 22;
                } else if (i == 2) {
                    padding = 27;
                } else if (i == 3) {
                    padding = 27;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("##707070").ss(0.4).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("##707070").ss(0.4).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 45;
        this.text.setTransform(18, 90);

        this.text_1 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(39, 90);

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(58, 90);

        this.text_3 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(79, 90);

        this.text_4 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(18, 120);

        this.text_5 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 45;
        this.text_5.setTransform(39, 120);

        this.text_6 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 45;
        this.text_6.setTransform(58, 120);

        this.text_7 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 45;
        this.text_7.setTransform(79, 120);

        this.text_8 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 45;
        this.text_8.setTransform(144, 90);

        this.text_9 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 45;
        this.text_9.setTransform(165, 90);

        this.text_10 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 45;
        this.text_10.setTransform(184, 90);

        this.text_11 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_11.lineHeight = 45;
        this.text_11.setTransform(205, 90);

        this.text_12 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_12.lineHeight = 45;
        this.text_12.setTransform(144, 120);

        this.text_13 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_13.lineHeight = 45;
        this.text_13.setTransform(165, 120);

        this.text_14 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_14.lineHeight = 45;
        this.text_14.setTransform(184, 120);

        this.text_15 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_15.lineHeight = 45;
        this.text_15.setTransform(205, 120);

        this.text_16 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_16.lineHeight = 45;
        this.text_16.setTransform(281, 90);

        this.text_17 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_17.lineHeight = 45;
        this.text_17.setTransform(302, 90);

        this.text_18 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_18.lineHeight = 45;
        this.text_18.setTransform(281, 120);

        this.text_19 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_19.lineHeight = 45;
        this.text_19.setTransform(302, 120);

        this.text_20 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_20.lineHeight = 45;
        this.text_20.setTransform(408, 90);

        this.text_21 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_21.lineHeight = 45;
        this.text_21.setTransform(429, 90);

        this.text_22 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_22.lineHeight = 45;
        this.text_22.setTransform(408, 120);

        this.text_23 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_23.lineHeight = 45;
        this.text_23.setTransform(429, 120);

        this.text_24 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_24.lineHeight = 45;
        this.text_24.setTransform(408, 222);

        this.text_25 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_25.lineHeight = 45;
        this.text_25.setTransform(429, 222);

        this.text_26 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_26.lineHeight = 45;
        this.text_26.setTransform(408, 252);

        this.text_27 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_27.lineHeight = 45;
        this.text_27.setTransform(429, 252);

        this.text_28 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_28.lineHeight = 45;
        this.text_28.setTransform(144, 222);

        this.text_29 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_29.lineHeight = 45;
        this.text_29.setTransform(165, 222);

        this.text_30 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_30.lineHeight = 45;
        this.text_30.setTransform(144, 252);

        this.text_31 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_31.lineHeight = 45;
        this.text_31.setTransform(165, 252);

        var arrPos = ['70', '348'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrPos.length; i++) {
            var xPos = parseInt(arrPos[i]);
            var columnLimit = 2;
            if (i == 1) {
                columnLimit = 1;
            }

            for (var column = 0; column < columnLimit; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // red ball
                    this.shape_group1.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(xPos + (columnSpace * 22), 70 + (row * 23), 9, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        arrPos = [];
        arrPos = ['126', '382'];
        this.shape_group2 = new cjs.Shape();
        for (var i = 0; i < arrPos.length; i++) {
            var xPos = parseInt(arrPos[i]);
            var columnLimit = 3;
            if (i == 1) {
                columnLimit = 4;
            }
            for (var column = 0; column < columnLimit; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // blue ball
                    this.shape_group2.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(xPos + (columnSpace * 21), 70 + (row * 23), 9, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.box_ball_1 = new cjs.Shape();
        this.box_ball_1.graphics.s("##707070").ss(0.4).drawRect(58, 59, 121, 22);
        this.box_ball_1.setTransform(0, 0);

        this.box_ball_2 = new cjs.Shape();
        this.box_ball_2.graphics.s("##707070").ss(0.4).drawRect(336, 59, 121, 22);
        this.box_ball_2.setTransform(0, 0);

        arrPos = [];
        arrPos = ['70', '335'];
        this.shape_group3 = new cjs.Shape();
        for (var i = 0; i < arrPos.length; i++) {
            var xPos = parseInt(arrPos[i]);
            var columnLimit = 2;
            if (i == 1) {
                columnLimit = 1;
            }

            for (var column = 0; column < columnLimit; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // red ball
                    this.shape_group3.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(xPos + (columnSpace * 22), 200 + (row * 23), 9, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group3.setTransform(0, 0);

        arrPos = [];
        arrPos = ['126', '370'];
        this.shape_group4 = new cjs.Shape();
        for (var i = 0; i < arrPos.length; i++) {
            var xPos = parseInt(arrPos[i]);
            var columnLimit = 4;
            if (i == 1) {
                columnLimit = 5;
            }
            for (var column = 0; column < columnLimit; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // blue ball
                    this.shape_group4.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(xPos + (columnSpace * 21), 200 + (row * 23), 9, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group4.setTransform(0, 0);

        this.box_ball_3 = new cjs.Shape();
        this.box_ball_3.graphics.s("##707070").ss(0.4).drawRect(58, 189, 142, 22);
        this.box_ball_3.setTransform(0, 0);

        this.box_ball_4 = new cjs.Shape();
        this.box_ball_4.graphics.s("##707070").ss(0.4).drawRect(324, 189, 142, 22);
        this.box_ball_4.setTransform(0, 0);

        arrPos = [];
        arrPos = ['58', '321'];
        this.shape_group5 = new cjs.Shape();
        for (var i = 0; i < arrPos.length; i++) {
            var xPos = parseInt(arrPos[i]);
            var columnLimit = 3;
            if (i == 1) {
                columnLimit = 2;
            }

            for (var column = 0; column < columnLimit; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // red ball
                    this.shape_group5.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(xPos + (columnSpace * 22), 330 + (row * 23), 9, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group5.setTransform(0, 0);

        arrPos = [];
        arrPos = ['136', '377'];
        this.shape_group6 = new cjs.Shape();
        for (var i = 0; i < arrPos.length; i++) {
            var xPos = parseInt(arrPos[i]);
            var columnLimit = 4;
            if (i == 1) {
                columnLimit = 5;
            }
            for (var column = 0; column < columnLimit; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // blue ball
                    this.shape_group6.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(xPos + (columnSpace * 21), 330 + (row * 23), 9, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group6.setTransform(0, 0);

        this.box_ball_5 = new cjs.Shape();
        this.box_ball_5.graphics.s("##707070").ss(0.4).drawRect(47, 319, 163, 22);
        this.box_ball_5.setTransform(0, 0);

        this.box_ball_6 = new cjs.Shape();
        this.box_ball_6.graphics.s("##707070").ss(0.4).drawRect(310, 319, 162, 22);
        this.box_ball_6.setTransform(0, 0);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.text, this.text_1);
        this.addChild(this.textbox_group1, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7);
        this.addChild(this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14, this.text_15);
        this.addChild(this.text_16, this.text_17, this.text_18, this.text_19, this.text_20, this.text_21, this.text_22, this.text_23);
        this.addChild(this.text_24, this.text_25, this.text_26, this.text_27, this.text_28, this.text_29, this.text_30, this.text_31);
        this.addChild(this.text_q1, this.text_q2, this.shape_group1, this.shape_group2, this.box_ball_1, this.box_ball_2);
        this.addChild(this.shape_group3, this.shape_group4, this.box_ball_3, this.box_ball_4, this.shape_group5, this.shape_group6, this.box_ball_5, this.box_ball_6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(295, 106, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(300, 265, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
