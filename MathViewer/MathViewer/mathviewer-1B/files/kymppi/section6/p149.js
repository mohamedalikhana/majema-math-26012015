(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p149_1.png",
            id: "p149_1"
        }, {
            src: "images/p149_2.png",
            id: "p149_2"

        }]
    };

    (lib.p149_1 = function() {
        this.initialize(img.p149_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p149_2 = function() {
        this.initialize(img.p149_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("kunna tolka enkla koder", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(446, 658);

        this.text_1 = new cjs.Text("149", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(552, 657);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#1A863A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_2 = new cjs.Text("Mot programmering 2", "24px 'MyriadPro-Semibold'", "#1A863A");
        this.text_2.setTransform(107, 42);

        this.text_3 = new cjs.Text("52", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(59, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#1A863A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(49, 23.5,1.15,1);

        this.instance = new lib.p149_1();
        this.instance.setTransform(67, 18, 0.47, 0.467);

        this.addChild(this.instance, this.shape_2, this.text_3, this.text_2, this.shape_1, this.text_1, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();
        // Block-1         

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#1A863A");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Hitta starttalet i hundrarutan.", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(15, 0);

        this.text_2 = new cjs.Text("Följ instruktionen. Skriv bokstaven du kommer till.", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(18, 18);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s("#959C9D").ss(1).drawRoundRect(0, 0, 512, 413, 10);
        this.roundRect1.setTransform(0, 32);

        this.instance = new lib.p149_2();
        this.instance.setTransform(367, 353, 0.47, 0.467);

        var crosswordBox_group = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#FFFFFF").s("#1A863A").ss(1).drawRect(0, 0, 255, 255, 0);
        this.Rect1.setTransform(0, 0);

        crosswordBox_group.addChild(this.Rect1);
        crosswordBox_group.setTransform(232, 47);

        var specialTexts = [1, 2, 3, 4, 5, 6, 7, 8, 'N', 10, 11, 12, 'O', 14, 15, 16, 'E', 18, 19, 20, 21, 22, 23, 24, 'M', 26, 27, 28, 29, 30,
            31, 'S', 33, 34, 35, 36, 37, 38, 'R', 40, 'D', 42, 43, 'U', 45, 'T', 47, 48, 49, 50, 51, 52, 53, 54, 55, 'Ä', 57, 58, 59, "A",
            61, 62, 'M', 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 'E', 78, 79, 'D', 81, 'U', 83, 'R', 85, 86, 87, 88, 'N', 90,
            'K', 92, 93, 94, 95, 'N', 97, 98, "R", 100
        ];

        var count = 0;
        var specialCount = 0;
        for (var column = 0; column < 10; column++) {
            for (var row = 0; row < 10; row++) {
                count = count + 1;
                var fillColor = '#ffffff';
                var fillText = count;
                fillText = specialTexts[specialCount];
                if (isNaN(fillText)) {
                    fillColor = '#CAD7BE';
                }
                specialCount = specialCount + 1;


                var rectContainer = new lib.FillBoxWithText("" + fillText, fillColor)
                rectContainer.setTransform(25.5 * row, 25.5 * column);
                crosswordBox_group.addChild(rectContainer);
            }
        }

        var hrlineX = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        var hrlineY = [25.5, 51, 76.5, 102, 127.5, 153, 178.5, 204, 229.5];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#1A863A").setStrokeStyle(1).moveTo(0, 0).lineTo(255, 0);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            crosswordBox_group.addChild(hrLine_1);
        }

        var vrlineX = [25.5, 51, 76.5, 102, 127.5, 153, 178.5, 204, 229.5];

        var vrlineY = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#1A863A").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 255);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            crosswordBox_group.addChild(vrLine_1);
        }

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.instance, crosswordBox_group);


        //container started

        this.text_4 = new cjs.Text("–", "bold 16px 'Myriad Pro'");
        this.text_4.setTransform(130, 155);
        this.addChild(this.text_4);


        var wordBox_group = new cjs.Container();

        var firstColFillText = ['27', '6', '8'];

        var count = 0;
        for (var col = 0; col < 3; col++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#FFF374';
                var fillText = firstColFillText[count];
                var rectContainer = new lib.wordRectangle(fillText, fillColor)
                rectContainer.setTransform(36.8 * col, 140 * row);
                wordBox_group.addChild(rectContainer, this.verticalRectangle, this.horline, this.horline2, this.horline3);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group);

        var wordBox_group2 = new cjs.Container();

        var specialTexts = [];
        specialTexts = ['2', '1', '4'];
        var arrowPos = ['up', 'down', 'down']
        var count = 0;
        for (var column = 0; column < 3; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle2(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37 * column, 140 * row);
                wordBox_group2.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group2);


        var wordBox_group3 = new cjs.Container();

        specialTexts = ['2', '3', '2'];
        var arrowPos = ['right', 'left', 'left']
        var count = 0;
        for (var column = 0; column < 3; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle3(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37.5 * column, 140 * row);
                wordBox_group3.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group3);

        var wordBox_group4 = new cjs.Container();

        var firstColFillText = ['40', '45', '58', '88'];

        var count = 0;

        for (var col = 0; col < 4; col++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#FFF374';
                var fillText = firstColFillText[count];
                var rectContainer = new lib.wordRectangle4(fillText, fillColor)
                rectContainer.setTransform(36.8 * col, 140 * row);
                wordBox_group4.addChild(rectContainer, this.verticalRectangle, this.horline, this.horline2, this.horline3);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group4);

        var wordBox_group5 = new cjs.Container();

        var specialTexts = [];
        specialTexts = ['2', '4', '4', '2'];
        var arrowPos = ['up', 'down', 'down', 'right']
        var count = 0;
        for (var column = 0; column < 4; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle5(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37 * column, 140 * row);
                wordBox_group5.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group5);


        var wordBox_group6 = new cjs.Container();

        specialTexts = ['3', '1', '2', '3'];
        var arrowPos = ['left', 'left', 'left', 'up']
        var count = 0;
        for (var column = 0; column < 4; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle6(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37.5 * column, 140 * row);
                wordBox_group6.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group6);


        var wordBox_group7 = new cjs.Container();

        var firstColFillText = ['100', '65', '62', '51', '57', '45', '37'];
        var count = 0;
        for (var col = 0; col < 7; col++) {
            for (var row = 0; row < 1; row++) {

                var startX = 0,
                    startY = 0;

                if (col < 2) {
                    startX = 0, startY = 0;
                }
                if (col > 1) {
                    startX = 25, startY = 0;
                }


                var fillColor = '#FFF374';
                var fillText = firstColFillText[count];
                var rectContainer = new lib.wordRectangle7(fillText, fillColor)
                rectContainer.setTransform(startX + 36.8 * col, 140 * row);
                wordBox_group7.addChild(rectContainer, this.verticalRectangle, this.horline, this.horline2, this.horline3);
                count = count + 1;
            }
        }
        this.addChild(wordBox_group7);

        var wordBox_group8 = new cjs.Container();

        var specialTexts = [];
        specialTexts = ['4', '3', '1', '4', '3', '3', '3'];
        var arrowPos = ['up', 'down', 'left', 'up', 'right', 'down', 'up']
        var count = 0;
        for (var column = 0; column < 7; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle8(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37 * column, 140 * row);
                wordBox_group8.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group8);


        var wordBox_group9 = new cjs.Container();

        specialTexts = ['4', '4', '3', '2', '2', '2', '2'];
        var arrowPos = ['left', 'right', 'down', 'right', 'down', 'right', 'right']
        var count = 0;
        for (var column = 0; column < 7; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle9(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37 * column, 140 * row);
                wordBox_group9.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group9);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 550.3, 440);

    (lib.wordRectangle = function(fillText, fillColor, row_group) {
        this.initialize();

        var vertRect_group = new cjs.Container();

        this.verticalRectangle = new cjs.Shape();
        this.verticalRectangle.graphics.f("").s('#7d7d7d').drawRect(0, 0, 31, 95);
        this.verticalRectangle.setTransform(0, 0);

        this.horline = new cjs.Shape();
        this.horline.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline.setTransform(0, 23);

        this.horline2 = new cjs.Shape();
        this.horline2.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline2.setTransform(0, 46);

        this.horline3 = new cjs.Shape();
        this.horline3.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline3.setTransform(0, 69);

        vertRect_group.addChild(this.verticalRectangle, this.horline, this.horline2, this.horline3);
        vertRect_group.setTransform(19, 70);

        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, (31 - 1), (92 / 4) - 1);
        rect.setTransform(19, 70);

        var Xpos = 0,
            Ypos = 0;
        if (fillText < 10) {
            Xpos = 28, Ypos = 87;
        }
        if (fillText > 9) {
            Xpos = 23, Ypos = 87;
        }
        if (fillText > 99) {
            Xpos = 18, Ypos = 87;
        }

        this.text = new cjs.Text(" " + fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(Xpos, Ypos);

        this.addChild(vertRect_group, rect, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle2 = function(fillText, fillColor, position, count, row_group) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 4) {
            startX = 0;
            startY = 0;
        }
        if (count > 3 && count < 11) {
            startX = -2;
            startY = 0;
        }


        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 37, 109);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);

        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 25, 105);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 23, 105);
                break;
            case 'up':
                Arrow.setTransform(startX + 29, 111);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 31, 98);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }
        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle3 = function(fillText, fillColor, position, count, row_group) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 5) {
            startX = 0;
            startY = 0;
        }
        if (count > 4 && count < 9) {
            startX = -2;
            startY = 0;
        }
        if (count > 8) {
            startX = -6;
            startY = 0;
        }

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 35, 132);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);
        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 35, 128);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 21, 128);
                break;
            case 'up':
                Arrow.setTransform(startX + 27, 134);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 27, 119);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }

        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.wordRectangle4 = function(fillText, fillColor) {
        this.initialize();

        var vertRect_group = new cjs.Container();

        this.verticalRectangle = new cjs.Shape();
        this.verticalRectangle.graphics.f("").s('#7d7d7d').drawRect(0, 0, 31, 95);
        this.verticalRectangle.setTransform(0, 0);

        this.horline = new cjs.Shape();
        this.horline.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline.setTransform(0, 23);

        this.horline2 = new cjs.Shape();
        this.horline2.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline2.setTransform(0, 46);

        this.horline3 = new cjs.Shape();
        this.horline3.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline3.setTransform(0, 69);

        vertRect_group.addChild(this.verticalRectangle, this.horline, this.horline2, this.horline3);
        vertRect_group.setTransform(19, 195);

        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, (31 - 1), (92 / 4) - 1);
        rect.setTransform(19, 195);

        var Xpos = 0,
            Ypos = 0;
        if (fillText < 10) {
            Xpos = 28, Ypos = 212;
        }
        if (fillText > 9) {
            Xpos = 23, Ypos = 212;
        }
        if (fillText > 99) {
            Xpos = 18, Ypos = 212;
        }

        this.text = new cjs.Text(" " + fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(Xpos, Ypos);

        this.addChild(vertRect_group, rect, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle5 = function(fillText, fillColor, position, count) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 4) {
            startX = 0;
            startY = 0;
        }
        if (count > 3 && count < 11) {
            startX = -2;
            startY = 0;
        }


        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 37, 234);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);

        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 25, 230);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 23, 230);
                break;
            case 'up':
                Arrow.setTransform(startX + 29, 236);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 31, 223);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }
        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle6 = function(fillText, fillColor, position, count) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 5) {
            startX = 0;
            startY = 0;
        }
        if (count > 4 && count < 9) {
            startX = -2;
            startY = 0;
        }
        if (count > 8) {
            startX = -6;
            startY = 0;
        }

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 35, 257);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);
        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 35, 253);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 21, 253);
                break;
            case 'up':
                Arrow.setTransform(startX + 27, 259);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 27, 244);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }

        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.wordRectangle7 = function(fillText, fillColor) {
        this.initialize();

        var vertRect_group = new cjs.Container();

        this.verticalRectangle = new cjs.Shape();
        this.verticalRectangle.graphics.f("").s('#7d7d7d').drawRect(0, 0, 31, 95);
        this.verticalRectangle.setTransform(0, 0);

        this.horline = new cjs.Shape();
        this.horline.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline.setTransform(0, 23);

        this.horline2 = new cjs.Shape();
        this.horline2.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline2.setTransform(0, 46);

        this.horline3 = new cjs.Shape();
        this.horline3.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline3.setTransform(0, 69);

        vertRect_group.addChild(this.verticalRectangle, this.horline, this.horline2, this.horline3);
        vertRect_group.setTransform(19, 321);

        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, (31 - 1), (92 / 4) - 1);
        rect.setTransform(19, 321);

        var Xpos = 0,
            Ypos = 0;
        if (fillText < 10) {
            Xpos = 28, Ypos = 338;
        }
        if (fillText > 9) {
            Xpos = 23, Ypos = 338;
        }
        if (fillText > 99) {
            Xpos = 18, Ypos = 338;
        }

        this.text = new cjs.Text(" " + fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(Xpos, Ypos);

        this.addChild(vertRect_group, rect, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.wordRectangle8 = function(fillText, fillColor, position, count) {
        this.initialize();


        var startX = 0,
            startY = 0;

        if (count < 2) {
            startX = 0;
            startY = 0;
        }
        if (count > 1 && count < 7) {
            startX = 25;
            startY = 0;
        }
       

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 37, 360);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);

        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 37, 356);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 23, 356);
                break;
            case 'up':
                Arrow.setTransform(startX + 31, 362);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 30, 348);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }
        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle9 = function(fillText, fillColor, position, count) {
        this.initialize();

        var startX = 0,
            startY = 0;

      if (count < 2) {
            startX = 1;
            startY = 0;
        }
        if (count > 1 && count < 7) {
            startX = 25;
            startY = 0;
        }
       

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 35, 383);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);
        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 36, 379);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 20, 379);
                break;
            case 'up':
                Arrow.setTransform(startX + 27, 385);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 27, 370);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }

        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.FillBoxWithText = function(fillText, fillColor) {
        this.initialize();
        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0, 0, 25.2, 25.2);
        var text = new cjs.Text('' + fillText, "14px 'Myriad Pro'", "#000000");
        text.setTransform(12, 20)
        text.textAlign = 'center';
        this.addChild(rect, text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 245, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
