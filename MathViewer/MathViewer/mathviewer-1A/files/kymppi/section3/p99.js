(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p99_1.png",
            id: "p99_1"
        }, {
            src: "images/p99_2.png",
            id: "p99_2"
        }]
    };

    (lib.p99_1 = function() {
        this.initialize(img.p99_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p99_2 = function() {
        this.initialize(img.p99_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:


    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 7 till 10", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 5;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(388, 651);

        this.text_2 = new cjs.Text("99", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(557, 650);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Talet 10", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(115.5, 25);

        this.text_4 = new cjs.Text("34", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(66.7, 22);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52.6, 23.5, 1.2, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Part 1
    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("tio", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(45, 123.5);

        this.text_col1 = new cjs.Text("tiotal", "10px 'Myriad Pro'");
        this.text_col1.lineHeight = 19;
        this.text_col1.setTransform(20, 26);

        this.text_col2 = new cjs.Text("ental", "10px 'Myriad Pro'");
        this.text_col2.lineHeight = 19;
        this.text_col2.setTransform(66, 26);

        this.text_1 = new cjs.Text("0", "bold 80px 'UusiTekstausMajema'");
        this.text_1.lineHeight = 96;
        this.text_1.setTransform(61, 37);

        this.text_no1 = new cjs.Text("1", "bold 80px 'UusiTekstausMajema'");
        this.text_no1.lineHeight = 96;
        this.text_no1.setTransform(14, 37);

        this.shape_rect1 = new cjs.Shape();
        this.shape_rect1.graphics.f('#ffffff').s("#000000").ss(0.5).drawRect(9, 23, 97, 97);
        this.shape_rect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(106, 40);
        this.hrLine_1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(55, 23).lineTo(55, 120);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(29, 58).lineTo(27, 66);
        this.Line_2.setTransform(0, 0);

        this.arrow_Line2 = new cjs.Shape();
        this.arrow_Line2.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.arrow_Line2.setTransform(26.5, 69, 1, 1, 365);

        // arc above number
        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.s("#000000").ss(1).arc(85, 80.5, 27, 10.7 * Math.PI / 2, 4.65);
        this.shape_38.setTransform(0, 0);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_39.setTransform(71, 58.5, 1, 1, 400);

        this.shape = new cjs.Shape(); // clock center dot
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(429, 65.4);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_1.setTransform(419, 59.4, 1, 1, 182);

        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(429, 51.1);

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(421.8, 32);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(408.1, 36.2);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(398.1, 45.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C6CBCC").s().p("AkPBPQhxhvAAifIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACghxBuQhxBxifAAQieAAhxhxg");
        this.shape_3.setTransform(429, 84.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohmQhohniSAAQiRAAhoBnQhoBmAACTIgfAAQAAifBxhvQBxhxCeAAQCfAABxBxQBxBvAACfg");
        this.shape_4.setTransform(429, 46.1);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(394.5, 59.5);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(399.2, 74);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(409.1, 84.2);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(424.6, 88.8);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(438.9, 84.6);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(450.1, 74.3);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(454.6, 60.2);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(449.9, 45.6);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(439, 36);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(429, 31.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(411.8, 35.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(399.3, 48.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(394.7, 65.4);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(399.3, 82.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAJIAJgR");
        this.shape_10.setTransform(411.8, 95.1);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(429, 99.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAJIgJgR");
        this.shape_12.setTransform(446.1, 95.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKAFIgTgJ");
        this.shape_13.setTransform(458.7, 82.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(463.3, 65.4);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKgEIgTAJ");
        this.shape_15.setTransform(458.7, 48.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(446.1, 35.7);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShnBoQhpBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABpBoQBnBoAACRg");
        this.shape_17.setTransform(429, 65.4);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(429, 65.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(429, 65.4);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(429, 65.4);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQggAAgZAYQgYAYAAAhQAAAiAYAYQAZAYAgAAg");
        this.shape_21.setTransform(273, 51.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_22.setTransform(273, 51.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#0089CA").s("#000000").ss(0.5, 0, 0, 4).arc(0, 0, 8.2, 0, 2 * Math.PI);
        this.shape_52.setTransform(296, 50.7);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_23.setTransform(250.8, 51.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgZAYghAAQghAAgYgYg");
        this.shape_24.setTransform(250.8, 51.1);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_25.setTransform(228.9, 51.1);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQghAAgYgYg");
        this.shape_26.setTransform(228.9, 51.1);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_27.setTransform(205, 51.1);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_28.setTransform(205, 51.1);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao2AAIRsAA");
        this.shape_29.setTransform(250.6, 61.3);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_30.setTransform(285.4, 61.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_31.setTransform(262.2, 61.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_32.setTransform(239.5, 61.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_33.setTransform(217.1, 61.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfQgLAAgLAFQgXAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAIAXgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_34.setTransform(250.8, 61.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAXgMQALgFALAAIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMIgXAFg");
        this.shape_35.setTransform(250.8, 61.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_40.setTransform(288.7, 96.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_41.setTransform(288.7, 96.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_42.setTransform(260.3, 96.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_43.setTransform(260.3, 96.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_44.setTransform(231.5, 96.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_45.setTransform(231.5, 96.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_46.setTransform(203.2, 96.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_47.setTransform(203.2, 96.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#000000").s().p("AgegWIA9AWIg9AXg");
        this.shape_48.setTransform(309.7, 96.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_49.setTransform(251.5, 96.6);

        // Main Yellow block
        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFF173").s("").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 509, 134, 10);
        this.shape_51.setTransform(0, 12);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_61.setTransform(205, 72.6);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_62.setTransform(205, 72.6);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_63.setTransform(229, 72.6);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_64.setTransform(229, 72.6);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_65.setTransform(251, 72.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_66.setTransform(251, 72.6);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_67.setTransform(273, 72.6);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_68.setTransform(273, 72.6);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_69.setTransform(296, 72.6);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_70.setTransform(296, 72.6);


        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(150, 97).lineTo(350.5, 97).moveTo(350.5, 97).lineTo(350.5, 95).lineTo(357, 97).lineTo(350.5, 99).lineTo(350.5, 97);
        var numberLineLimit = 10
        for (var dot = 0; dot < 11; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(147 + (20 * dot), 92).lineTo(147 + (20 * dot), 102);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(146, 97).lineTo(147 + (20 * numberLineLimit), 97);

        for (var dot = 0; dot < 11; dot++) {
            var temptext = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 13px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot == 10) ? 9.8 : dot;
            temptext.setTransform(144 + (20 * colSpace), 119);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);

        this.addChild(this.shape_51, this.shape_rect1, this.hrLine_1, this.Line_1, this.Line_2, this.arrow_Line2, this.text_no1, this.text_col1, this.text_col2);
        this.addChild(this.shape_39, this.shape_38, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.shape_4, this.shape_3, this.text_4, this.text_3, this.text_2, this.shape_2, this.shape_1, this.shape, this.text_1, this.shape_52, this.text);

        this.addChild(this.shape_61, this.shape_62, this.shape_63, this.shape_64, this.shape_65, this.shape_66, this.shape_67, this.shape_68, this.numberLine);
        this.addChild(this.shape_69, this.shape_70);

        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 511.3, 107.1);

    //Part 4
    (lib.Symbol6 = function() {
        this.initialize();

        this.instance = new lib.p99_1();
        this.instance.setTransform(0, 24, 0.40, 0.40);

        this.instance_2 = new lib.p99_2();
        this.instance_2.setTransform(0, 243, 0.40, 0.40);

        this.text = new cjs.Text("Hur många bollar är det på varje sida?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 303, 10);
        this.shape_65.setTransform(0, 25);

        this.textbox_1 = new cjs.Shape();
        this.textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(15, 42.6, 23, 25);
        this.textbox_1.setTransform(0, 0);
        this.textbox_2 = new cjs.Shape();
        this.textbox_2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(82, 42.6, 33.5, 25);
        this.textbox_2.setTransform(0, 0);
        this.textbox_3 = new cjs.Shape();
        this.textbox_3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(143, 42.6, 23, 25);
        this.textbox_3.setTransform(0, 0);
        this.textbox_4 = new cjs.Shape();
        this.textbox_4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(213, 42.6, 23, 25);
        this.textbox_4.setTransform(0, 0);
        this.textbox_5 = new cjs.Shape();
        this.textbox_5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(271, 42.6, 23, 25);
        this.textbox_5.setTransform(0, 0);
        this.textbox_6 = new cjs.Shape();
        this.textbox_6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(341, 42.6, 23, 25);
        this.textbox_6.setTransform(0, 0);
        this.textbox_13 = new cjs.Shape();
        this.textbox_13.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(399.5, 42.6, 23, 25);
        this.textbox_13.setTransform(0, 0);
        this.textbox_14 = new cjs.Shape();
        this.textbox_14.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(469, 42.6, 23, 25);
        this.textbox_14.setTransform(0, 0);

        this.textbox_7 = new cjs.Shape();
        this.textbox_7.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(15, 147, 23, 25);
        this.textbox_7.setTransform(0, 0);
        this.textbox_8 = new cjs.Shape();
        this.textbox_8.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(86, 147, 23, 25);
        this.textbox_8.setTransform(0, 0);
        this.textbox_9 = new cjs.Shape();
        this.textbox_9.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(143, 147, 23, 25);
        this.textbox_9.setTransform(0, 0);
        this.textbox_10 = new cjs.Shape();
        this.textbox_10.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(213, 147, 23, 25);
        this.textbox_10.setTransform(0, 0);
        this.textbox_11 = new cjs.Shape();
        this.textbox_11.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(271, 147, 23, 25);
        this.textbox_11.setTransform(0, 0);
        this.textbox_12 = new cjs.Shape();
        this.textbox_12.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(341, 147, 23, 25);
        this.textbox_12.setTransform(0, 0);
        this.textbox_15 = new cjs.Shape();
        this.textbox_15.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(399.5, 147, 23, 25);
        this.textbox_15.setTransform(0, 0);
        this.textbox_16 = new cjs.Shape();
        this.textbox_16.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(469, 147, 23, 25);
        this.textbox_16.setTransform(0, 0);

        this.textbox_17 = new cjs.Shape();
        this.textbox_17.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(15, 242, 23, 25);
        this.textbox_17.setTransform(0, 0);
        this.textbox_18 = new cjs.Shape();
        this.textbox_18.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(86, 242, 23, 25);
        this.textbox_18.setTransform(0, 0);
        this.textbox_19 = new cjs.Shape();
        this.textbox_19.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(143, 242, 23, 25);
        this.textbox_19.setTransform(0, 0);
        this.textbox_20 = new cjs.Shape();
        this.textbox_20.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(213, 242, 23, 25);
        this.textbox_20.setTransform(0, 0);
        this.textbox_21 = new cjs.Shape();
        this.textbox_21.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(263.5, 242, 34, 25);
        this.textbox_21.setTransform(0, 0);
        this.textbox_22 = new cjs.Shape();
        this.textbox_22.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(341, 242, 23, 25);
        this.textbox_22.setTransform(0, 0);

        this.textbox_Line_1 = new cjs.Shape();
        this.textbox_Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.7).moveTo(280.5, 242).lineTo(280.5, 267).moveTo(99, 42.6).lineTo(99, 67.6);
        this.textbox_Line_1.setTransform(0, 0);


        this.text_2 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_2.lineHeight = 24;
        this.text_2.setTransform(52, 35);
        this.text_3 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_3.lineHeight = 24;
        this.text_3.setTransform(179, 35);
        this.text_4 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_4.lineHeight = 24;
        this.text_4.setTransform(307, 35);
        this.text_5 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_5.lineHeight = 24;
        this.text_5.setTransform(436, 35);

        this.text_6 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_6.lineHeight = 24;
        this.text_6.setTransform(52, 143);
        this.text_7 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_7.lineHeight = 24;
        this.text_7.setTransform(179, 143);
        this.text_8 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_8.lineHeight = 24;
        this.text_8.setTransform(307, 143);
        this.text_9 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_9.lineHeight = 24;
        this.text_9.setTransform(436, 143);

        this.text_10 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_10.lineHeight = 24;
        this.text_10.setTransform(52, 236);
        this.text_11 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_11.lineHeight = 24;
        this.text_11.setTransform(179, 236);
        this.text_12 = new cjs.Text("10", "15.5px 'Myriad Pro'");
        this.text_12.lineHeight = 24;
        this.text_12.setTransform(307, 236);

        this.addChild(this.shape_65, this.instance, this.instance_2, this.text_1);
        this.addChild(this.textbox_1, this.textbox_2, this.textbox_3,
            this.textbox_4, this.textbox_5, this.textbox_6, this.textbox_7, this.textbox_8, this.textbox_9, this.textbox_10, this.textbox_11, this.textbox_12, this.text_2, this.text_3, this.text_4,
            this.text_5, this.text_6, this.text_7, this.text);

        this.addChild(this.textbox_13, this.textbox_14, this.textbox_15, this.textbox_16, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12);
        this.addChild(this.textbox_17, this.textbox_18, this.textbox_19, this.textbox_20, this.textbox_21, this.textbox_22, this.textbox_Line_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 320.6);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 90, 10);
        this.round_Rect1.setTransform(0, 20);

        var arrTxtbox = ['34', '71'];

        this.textbox_group1 = new cjs.Shape();
        var xPos,
            padding,
            yPos,
            rectWidth = 40,
            rectHeight = 22,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 2,
            numberOfRects = 7;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                if (i == 0) {
                    padding = 0;
                    xPos = 19;
                } else {
                    padding = 32;
                    xPos = -15;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var ToBeAdded = [];
        var arrxPos = ['22.5', '92.5', '164.5', '236.5', '308.5', '380.5', '452.5'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var temp_text1 = new cjs.Text("1", "34px 'UusiTekstausMajema'", "#BCC1C2");
            temp_text1.lineHeight = 40;
            temp_text1.setTransform(xPos, 25);

            var temp_text2 = new cjs.Text("0", "34px 'UusiTekstausMajema'", "#BCC1C2");
            temp_text2.lineHeight = 40;
            temp_text2.setTransform(xPos + 20, 25);
            ToBeAdded.push(temp_text1, temp_text2);
        }

        arrxPos = [];
        arrxPos = ['22.5', '164.5', '308.5', '450.5'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var temp_text1 = new cjs.Text("1", "34px 'UusiTekstausMajema'", "#BCC1C2");
            temp_text1.lineHeight = 40;
            temp_text1.setTransform(xPos, 62);

            var temp_text2 = new cjs.Text("0", "34px 'UusiTekstausMajema'", "#BCC1C2");
            temp_text2.lineHeight = 40;
            temp_text2.setTransform(xPos + 20, 62);
            ToBeAdded.push(temp_text1, temp_text2);
        }

        arrxPos = [];
        arrxPos = ['97', '241', '384.5'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var temp_text1 = new cjs.Text("•", "17px 'Myriad Pro'", "#BCC1C2");
            temp_text1.lineHeight = -1;
            temp_text1.setTransform(xPos, 64);

            var temp_text2 = new cjs.Text("•", "17px 'Myriad Pro'", "#BCC1C2");
            temp_text2.lineHeight = -1;
            temp_text2.setTransform(xPos + 22, 64);

            ToBeAdded.push(temp_text1, temp_text2);
        }

        this.addChild(this.round_Rect1, this.textbox_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 512.3, 149.8);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v4 = new lib.Symbol6();
        this.v4.setTransform(315, 345.3, 1, 1, 0, 0, 0, 256.3, 38.8);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(315, 256.2, 1, 1, 0, 0, 0, 255.1, 74.8);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(315, 95.9, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
