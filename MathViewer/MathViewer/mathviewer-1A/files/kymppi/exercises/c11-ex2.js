var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };

    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 5", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("11", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Räkna högt: 1, 2 … 5.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550 + 110, 460);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.numbers = new lib.numbers();
        this.numbers.setTransform(320 + 70, 0, 3.5, 4, 0, 0, 0, 0, 0);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        this.addChild(this.numbers, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.numbers = new lib.numbers();
        this.numbers.setTransform(320 + 70, 0, 3.5, 4, 0, 0, 0, 0, 0);

        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();

        var addImage = [];
        var addImageX = ['425', '425', '525', '425', '525', '625', '425', '525', '625', '725', '425', '525', '625', '725', '825'];
        var addImageY = ['-55', '45', '45', '145', '145', '145', '245', '245', '245', '245', '345', '345', '345', '345', '345'];

        for (var i = 0; i < 15; i++) {
            var shape = this.blueBall.clone(true);
            shape.setTransform(parseInt(addImageX[i]) + 20, addImageY[i], 0.7, 0.7);
            addImage.push(shape);
        }

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.numbers, this.commentTexts);
        this.tweens = [];

        // var b_wait = [750, 1500, 1500, 2250, 2250, 2250, 3000, 3000, 3000, 3000, 3750, 3750, 3750, 3750, 3750, 4500, 4500, 4500, 4500, 4500, 4500, 5250, 5250, 5250, 5250, 5250, 5250, 5250];
        var b_wait = [1000, 2000, 2000, 3000, 3000, 3000, 4000, 4000, 4000, 4000, 5000, 5000, 5000, 5000, 5000];
        for (var j = 0; j < addImage.length; j++) {
            this.addChild(addImage[j]);
            this.tweens.push({
                ref: addImage[j],
                alphaFrom: 0,
                alphaTo: 1,
                wait: b_wait[j],
                alphaTimeout: 2000
            });
        }

        p.tweens = this.tweens;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.numbers = function() {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();
        var numbers = [];
        var arrNumHeight = [-8, 18, 43, 68, 93];
        for (var i = 0; i < 5; i++) {
            numbers[i] = new cjs.Text("" + (i + 1), "11px 'Myriad Pro'");
            numbers[i].lineHeight = 19;
            numbers[i].setTransform(0, arrNumHeight[i]);
        }
        for (var i = 0; i < 5; i++) {
            this.addChild(numbers[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
