(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p55_1.png",
            id: "p55_1"
        }]
    };

    (lib.p55_1 = function() {
        this.initialize(img.p55_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("55", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(577, 661);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(461 + (columnSpace * 33), 35, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(19, 35);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_q2.setTransform(0, 35);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 254, 132, 10);
        this.roundRect1.setTransform(0, 33);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 33);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 172);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 172);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 312);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 312);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 452);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(260, 452);

        this.instance = new lib.p55_1();
        this.instance.setTransform(25, 70, 0.47, 0.47);
         var ToBeAdded = [];
        var tmpcol = 260;
        for (var row = 0; row < 4; row++) {
            var rowSpace = row;
            for (var col = 0; col < 3; col++) {
                var columnSpace = col;
                if (col == 2 && row < 3) {
                    continue;
                }
                if (col == 2 && row > 2) {
                    tmpcol = 185
                } else {
                    tmpcol = 260
                }
                var text_1 = new cjs.Text("10 st", "16px 'Myriad Pro'", "#000000");
                text_1.setTransform(35 + (columnSpace * tmpcol), 107 + (rowSpace * 140));
                ToBeAdded.push(text_1);
            }
        }
// draw arc  in 1st  row in left
        this.shape_group5 = new cjs.Shape();
        var fillcolor = "#958FC5";
        var columnXpos = 160;        
            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) {
                    if(row==2 && column>0){continue} 
                    this.shape_group5.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 27), 95 + (row * 29), 9.5, 0, 2 * Math.PI)
                }
            }
 // draw arc  in 1st  row in right
        this.shape_group6 = new cjs.Shape();
        var fillcolor = "#958FC5";
        var columnXpos = 425;        
            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                for (var row = 0; row < 2; row++) {
                    if(row==2 && column>0){continue} 
                    this.shape_group6.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 27), 85 + (row * 29), 9.5, 0, 2 * Math.PI)
                }
            }
        
// draw arc  in 2nd row 
        this.shape_group2 = new cjs.Shape();
        var fillcolor = "#958FC5";
        var columnXpos = 170;
        for (var numofCol = 0; numofCol < 2; numofCol++) {
            for (var column = 0; column < 4; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    if (numofCol == 1) {
                        columnXpos = 425
                    }
                    if (numofCol == 0 && row == 2) {
                        continue
                    }
                    if (numofCol == 0 && column == 3) {
                        continue
                    }
                    if (numofCol == 0 && column == 2 && row == 1) {
                        continue
                    }
                    if (numofCol == 1 && row == 2) {
                        continue
                    }
                    if (numofCol == 1 && column == 3) {
                        continue
                    }

                    this.shape_group2.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 27), 220 + (row * 29), 9.5, 0, 2 * Math.PI)
                }
            }
        }

// draw arc  in 3rd row
        this.shape_group3 = new cjs.Shape();
        var fillcolor = "#958FC5";
        var columnXpos = 140;
        for (var numofCol = 0; numofCol < 2; numofCol++) {
            for (var column = 0; column < 4; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    if (numofCol == 1) {
                        columnXpos = 400
                    }
                    if(numofCol==0){columnXpos=145}
                    if (numofCol == 0 && row == 2) {
                        continue
                    }                    
                    if (numofCol == 0 && column == 3 && row == 1) {
                        continue
                    }
                    if (numofCol == 1 && row == 2) {
                        continue
                    }                   

                    this.shape_group3.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 27), 365 + (row * 29), 9.5, 0, 2 * Math.PI)
                }
            }
        }
// draw arc  in 4th row
        this.shape_group4 = new cjs.Shape();
        var fillcolor = "#958FC5";
        var columnXpos = 145    ;        
            for (var column = 0; column < 4; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    if(row==2 && column>0){continue} 
                    this.shape_group4.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 27), 493 + (row * 29), 9.5, 0, 2 * Math.PI)
                }
            }

        var arryVal=["13  –  3  –  3 =","15  –  5  –  4 =","17  –  7  –  3 =","19  –  9  –  6 =","14  –  4  –  3 =","16  –  6  –  2 =","18  –  8  –  4 =","20  – 10 –  8 ="];
        var tmprow = 27;
        var tmpcol = 312;
        var i = 0;
        var temp_Line1 = new cjs.Shape();
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;             
                var tempText = new cjs.Text(arryVal[i], "16px 'Myriad Pro'");
                tempText.setTransform(63 + (262 * columnSpace), 160 + (140 * rowSpace));
                if (col == 1) {
                    tmpcol = 262
                } else if (col == 2) {
                    tmpcol = 165
                } else {
                    tmpcol = 162
                }
                if (row > 0) {
                    tmprow = 140
                } else {
                    tmprow = 27
                }                
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(157 + (columnSpace * tmpcol), 165 + (rowSpace * tmprow)).lineTo(190 + (columnSpace * tmpcol), 165 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };
      

        this.addChild(this.text_q1, this.text_q2);
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8);
        this.addChild(this.instance, this.textbox_group1, this.shape_group1, this.shape_group2,this.shape_group3,this.shape_group4, this.shape_group5, this.shape_group6);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 550.2);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(310, 259, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
