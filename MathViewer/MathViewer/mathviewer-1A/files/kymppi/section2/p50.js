(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p50_1.png",
            id: "p50_1"
        }, {
            src: "images/p50_2.png",
            id: "p50_2"
        }]
    };

    (lib.p50_1 = function() {
        this.initialize(img.p50_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p50_2 = function() {
        this.initialize(img.p50_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("50", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 18;
        this.text_1.setTransform(38, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#88C13F").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.shape_2, this.shape_1, this.shape, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p50_1();
        this.instance.setTransform(-10, 25, 0.38, 0.37);

        this.text_1 = new cjs.Text("Hur många stannar kvar?", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(12, 0);

        this.text_2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_2.lineHeight = 27;
        this.text_2.setTransform(-8, 0);

        var textArr = [];
        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var tempRect=null;
                tempRect = new cjs.Shape();
                tempRect.graphics.s("#7D7D7D").ss(1).drawRoundRect(-10, 7, 258, 132, 10);
                tempRect.setTransform(2 + (column * 263), 18 + (row * 138));
                textArr.push(tempRect);
            }
        }

        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var temptext=null;
                temptext = new cjs.Text("stannar kvar:", "16px 'Myriad Pro'");
                temptext.lineHeight = 3;
                temptext.setTransform(121 + (column * 262), 137 + (row * 138));
                textArr.push(temptext);
            }
        }

        this.tempBox1 = new cjs.Shape();
        this.tempBox1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox1.setTransform(211, 128.5);

        this.tempBox2 = new cjs.Shape();
        this.tempBox2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox2.setTransform(472, 128.5);

        this.tempBox3 = new cjs.Shape();
        this.tempBox3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox3.setTransform(211, 267);

        this.tempBox4 = new cjs.Shape();
        this.tempBox4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox4.setTransform(473, 268);

        textArr.push(this.tempBox1);
        textArr.push(this.tempBox2);
        textArr.push(this.tempBox3);
        textArr.push(this.tempBox4);

        this.text_3 = new cjs.Text("1 går iväg.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(86, 33.5);
        this.text_4 = new cjs.Text("1 går iväg.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(347, 33.5);
        this.text_5 = new cjs.Text("2 går iväg.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(86, 175);
        this.text_6 = new cjs.Text("2 går iväg.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(346.5, 175);

        textArr.push(this.text_3);
        textArr.push(this.text_4);
        textArr.push(this.text_5);
        textArr.push(this.text_6);

        this.addChild(this.text_1, this.text_2, this.instance);

        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 280);

    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p50_2();
        this.instance.setTransform(-10, 15, 0.38, 0.37);

        var textArr = [];
        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var tempRect=null;
                tempRect = new cjs.Shape();
                tempRect.graphics.s("#7D7D7D").ss(1).drawRoundRect(-10, 0, 258, 132, 10);
                tempRect.setTransform(2 + (column * 263), 18 + (row * 138));
                textArr.push(tempRect);
            }
        }

        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var temptext=null;
                temptext = new cjs.Text("stannar kvar:", "16px 'Myriad Pro'");
                temptext.lineHeight = -1;
                temptext.setTransform(121 + (column * 262), 127 + (row * 138));
                textArr.push(temptext);
            }
        }

        this.tempBox1 = new cjs.Shape();
        this.tempBox1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox1.setTransform(212, 119.5);

        this.tempBox2 = new cjs.Shape();
        this.tempBox2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox2.setTransform(473, 119.5);

        this.tempBox3 = new cjs.Shape();
        this.tempBox3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox3.setTransform(212.5, 257.7);

        this.tempBox4 = new cjs.Shape();
        this.tempBox4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 21);
        this.tempBox4.setTransform(473, 259);

        textArr.push(this.tempBox1);
        textArr.push(this.tempBox2);
        textArr.push(this.tempBox3);
        textArr.push(this.tempBox4);

        this.text_3 = new cjs.Text("3 går iväg.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(86, 26);
        this.text_4 = new cjs.Text("3 går iväg.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(347, 26);
        this.text_5 = new cjs.Text("4 går iväg.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(86, 164.5);
        this.text_6 = new cjs.Text("4 går iväg.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(346.5, 164.5);

        textArr.push(this.text_3);
        textArr.push(this.text_4);
        textArr.push(this.text_5);
        textArr.push(this.text_6);

        this.addChild(this.instance);

        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 280);


    (lib.p50 = function() {
        this.initialize();
        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305, 348, 1, 1, 0, 0, 0, 256.3, 0);
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(305, 284.5, 1, 1, 0, 0, 0, 256.3, 220);
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
