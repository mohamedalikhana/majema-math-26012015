(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1219,
	height: 678,
	fps: 20,
	color: "#FFFFFF",
	manifest: [
		{src:"images/s24_25_1.png", id:"s24_25_1"},
		{src:"images/s24_25_2.png", id:"s24_25_2"},
		{src:"images/s24_25_94.png", id:"s24_25_94"},
		{src:"images/s24_25_95.png", id:"s24_25_95"},
		{src:"images/s24_25_96.png", id:"s24_25_96"}
	]
};
// symbols:
(lib.s24_25_1 = function() {
	this.initialize(img.s24_25_1);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,902,292);


(lib.s24_25_2 = function() {
	this.initialize(img.s24_25_2);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,974,344);


(lib.s24_25_94 = function() {
	this.initialize(img.s24_25_94);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,29,29);


(lib.s24_25_95 = function() {
	this.initialize(img.s24_25_95);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,21,21);


(lib.s24_25_96 = function() {
	this.initialize(img.s24_25_96);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,658,248);


(lib.Symbol16 = function() {
	this.initialize();

	// Layer 1
	this.text_4 = new cjs.Text("24", "13px 'Myriad Pro'", "#FFFFFF");
	this.text_4.lineHeight = 18;
	this.text_4.setTransform(39,648);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
	this.shape_1.setTransform(31.1,660.8);

	this.instance = new lib.s24_25_96();
	this.instance.setTransform(42,0,0.779,0.779);

	this.addChild(this.instance,this.shape_2,this.shape_1,this.shape,this.text_4,this.text_3,this.text_2,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,1218.9,677.5);

//Title Text
(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.fontfix = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.text_2 = new cjs.Text("Måla 1 cirkel mer än antalet djur.", "16px 'Myriad Pro'");
	this.text_2.lineHeight = 19;
	this.text_2.setTransform(4,5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.virtualBounds = new cjs.Rectangle(0,0,341.3,25.3);

(lib.Symbol6 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.s24_25_2();
	this.instance.setTransform(18.4,237.6,0.5,0.5);

	this.instance_1 = new lib.s24_25_1();
	this.instance_1.setTransform(49.6,42.1,0.5,0.5);

	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(193,13.6,1,1,0,0,0,178.6,12.6);

	this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A5C0");
	this.text.lineHeight = 20;
	this.text.setTransform(0,5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape.setTransform(494.4,420.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAcAcAmAAg");
	this.shape_1.setTransform(494.4,315.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_2.setTransform(494.4,212.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAcAcAmAAg");
	this.shape_3.setTransform(494.4,109.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape_4.setTransform(236.4,420.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_5.setTransform(236.4,315.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_6.setTransform(236.4,212.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_7.setTransform(236.4,109.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_8.setTransform(369.8,420.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_9.setTransform(369.8,315.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
	this.shape_10.setTransform(369.8,212.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_11.setTransform(369.8,109.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_12.setTransform(111.9,420.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_13.setTransform(111.9,315.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
	this.shape_14.setTransform(111.9,212.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_15.setTransform(111.9,109.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape_16.setTransform(470.9,420.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_17.setTransform(470.9,315.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_18.setTransform(470.9,212.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_19.setTransform(470.9,109.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape_20.setTransform(213,420.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_21.setTransform(213,315.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_22.setTransform(213,212.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_23.setTransform(213,109.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_24.setTransform(346.4,420.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_25.setTransform(346.4,315.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
	this.shape_26.setTransform(346.4,212.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_27.setTransform(346.4,109.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape_28.setTransform(88.4,420.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_29.setTransform(88.4,315.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_30.setTransform(88.4,212.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_31.setTransform(88.4,109.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_32.setTransform(448.3,420.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgdgdgnAAQgmAAgdAdQgcAbAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_33.setTransform(448.3,315.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAnAAAdgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
	this.shape_34.setTransform(448.3,212.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgdgdgnAAQgmAAgdAdQgcAbAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_35.setTransform(448.3,109.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_36.setTransform(190.4,420.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgbQgdgdgnAAQgmAAgdAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_37.setTransform(190.4,315.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAnAAAdgdQAcgbAAgoQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
	this.shape_38.setTransform(190.4,212.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgbQgdgdgnAAQgmAAgdAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_39.setTransform(190.4,109.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_40.setTransform(323.8,420.6);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgcQgcgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_41.setTransform(323.8,315.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
	this.shape_42.setTransform(323.8,212.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgcQgcgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_43.setTransform(323.8,109.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_44.setTransform(65.9,420.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgcQgbgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_45.setTransform(65.9,315.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgdQAdgbAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
	this.shape_46.setTransform(65.9,212.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgcQgbgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_47.setTransform(65.9,109.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_48.setTransform(426.2,420.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgcQgbgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_49.setTransform(426.2,315.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgdQAdgbAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
	this.shape_50.setTransform(426.2,212.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgcQgbgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_51.setTransform(426.2,109.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_52.setTransform(168.2,420.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgcQgcgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_53.setTransform(168.2,315.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
	this.shape_54.setTransform(168.2,212.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgcQgcgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_55.setTransform(168.2,109.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_56.setTransform(301.6,420.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_57.setTransform(301.6,315.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
	this.shape_58.setTransform(301.6,212.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_59.setTransform(301.6,109.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_60.setTransform(43.7,420.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgcQgbgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_61.setTransform(43.7,315.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgdQAdgbAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
	this.shape_62.setTransform(43.7,212.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgcQgbgdgoAAQgnAAgcAdQgcAbAAAnQAAAoAcAcQAcAcAnAAg");
	this.shape_63.setTransform(43.7,109.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape_64.setTransform(403.5,420.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_65.setTransform(403.5,315.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_66.setTransform(403.5,212.8);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_67.setTransform(403.5,109.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape_68.setTransform(145.6,420.6);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_69.setTransform(145.6,315.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_70.setTransform(145.6,212.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_71.setTransform(145.6,109.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAcQAcAcAmAAg");
	this.shape_72.setTransform(279,420.6);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_73.setTransform(279,315.8);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
	this.shape_74.setTransform(279,212.8);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgcAdQgdAcAAAmQAAAoAdAcQAbAcAnAAg");
	this.shape_75.setTransform(279,109.1);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAdAcAmAAg");
	this.shape_76.setTransform(21,420.6);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_77.setTransform(21,315.8);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
	this.shape_78.setTransform(21,212.8);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#959C9D").ss(0.6,0,0,4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgbQgcgdgoAAQgmAAgdAdQgcAcAAAmQAAAoAcAcQAcAcAnAAg");
	this.shape_79.setTransform(21,109.1);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgdQgdg4hHAAg");
	this.shape_80.setTransform(128.4,77.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAdA4QANAdAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
	this.shape_81.setTransform(128.4,77.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgdQgdg4hHAAg");
	this.shape_82.setTransform(386.4,77.7);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAcA4QAOAdAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
	this.shape_83.setTransform(386.4,77.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4naQgcAAgdAPQg4AcAABHIAALRIABASQAEAVAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhHIAArRQAAgdgNgcQgdg5hHAAg");
	this.shape_84.setTransform(128.4,181.1);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgPAcAAMAjxAAAQBHABAdA5QANAbAAAdIAALRQAABHg4AcQgSAJgVAEIgSABg");
	this.shape_85.setTransform(128.4,181.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4naQgcAAgcAPQg5AcAABHIAALRIACASQADAVAJASQAcA5BHAAMAjxAAAIASgCQAVgEASgIQA5gdAAhHIAArRQAAgdgOgcQgdg5hHAAg");
	this.shape_86.setTransform(386.4,181.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgPAcAAMAjxAAAQBHABAcA5QAOAbAAAdIAALRQAABHg4AcQgSAJgVAEIgSABg");
	this.shape_87.setTransform(386.4,181.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
	this.shape_88.setTransform(128.4,284.3);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAdA5QANAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
	this.shape_89.setTransform(128.4,284.3);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgcQgdg5hHAAg");
	this.shape_90.setTransform(386.4,284.3);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAcA5QAOAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
	this.shape_91.setTransform(386.4,284.3);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#959C9D").s().p("Ax4HdQhJAAgdg6QgJgSgDgWIgCgSIAArRQAAhIA6gdQAdgPAdAAMAjxAAAQBJAAAcA6QAPAdAAAdIAALRQAABIg6AdQgSAJgVAEIgTACgAyJnVQgWAEgRAIQg3AcAABFIAALRIACARQADAVAJARQAcA4BFAAMAj2AAAIANgCQAVgEAQgIQA4gcAAhFIAArWIgBgMQgEgWgJgQQgcg4hFAAMgj1AAAg");
	this.shape_92.setTransform(128.4,388.1);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAdA5QANAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
	this.shape_93.setTransform(128.4,388.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgcQgdg5hHAAg");
	this.shape_94.setTransform(386.4,388.1);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAcA5QAOAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
	this.shape_95.setTransform(386.4,388.1);

	this.addChild(this.shape_95,this.shape_94,this.shape_93,this.shape_92,this.shape_91,this.shape_90,this.shape_89,this.shape_88,this.shape_87,this.shape_86,this.shape_85,this.shape_84,this.shape_83,this.shape_82,this.shape_81,this.shape_80,this.shape_79,this.shape_78,this.shape_77,this.shape_76,this.shape_75,this.shape_74,this.shape_73,this.shape_72,this.shape_71,this.shape_70,this.shape_69,this.shape_68,this.shape_67,this.shape_66,this.shape_65,this.shape_64,this.shape_63,this.shape_62,this.shape_61,this.shape_60,this.shape_59,this.shape_58,this.shape_57,this.shape_56,this.shape_55,this.shape_54,this.shape_53,this.shape_52,this.shape_51,this.shape_50,this.shape_49,this.shape_48,this.shape_47,this.shape_46,this.shape_45,this.shape_44,this.shape_43,this.shape_42,this.shape_41,this.shape_40,this.shape_39,this.shape_38,this.shape_37,this.shape_36,this.shape_35,this.shape_34,this.shape_33,this.shape_32,this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.text,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,436.6);

// stage content:
(lib.p24 = function() {
	this.initialize();

	// Layer 1
	this.other = new lib.Symbol16();
	this.other.setTransform(609.5,339,1,1,0,0,0,609.5,338.7);

	this.v1 = new lib.Symbol6();
	this.v1.setTransform(295.8,417.5,1,1,0,0,0,256.3,217.9);

	this.addChild(this.v1,this.other);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(609.5,339.3,1218.9,677.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;