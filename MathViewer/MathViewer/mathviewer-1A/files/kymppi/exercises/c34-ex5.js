var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };

    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 10", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("34", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Säg högt: Klockan är 1, 2, 3 … 10.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(640, 470);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Klocken = function() {
        this.initialize();
        // arc above num

        //this.shape.alpha=0.1;
        //red line/ hour needle

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(28.1, 0);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(14.4, 4.1);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(4.4, 13.1);

        this.ringLayer = new cjs.Shape();
        this.ringLayer.graphics.f("#C6CBCC").s().p("AkPBPQhxhuAAigIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACfhxBvQhxBxifAAQieAAhxhxg");
        this.ringLayer.setTransform(35.2, 52.6);

        this.outerRingLayer = new cjs.Shape();
        this.outerRingLayer.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohmQhohniSAAQiRAAhoBnQhoBmAACTIgfAAQAAifBxhvQBxhxCeAAQCfAABxBxQBxBvAACfg");
        this.outerRingLayer.setTransform(35.2, 14.1);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(0.8, 27.5);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(5.5, 42);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(15.3, 52.1);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(30.8, 56.8);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(45.2, 52.5);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(56.3, 42.3);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(60.9, 28.2);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(56.1, 13.6);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(45.3, 3.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(35.3, -0.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(18.1, 3.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(5.6, 16.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(1, 33.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(5.6, 50.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAKIAJgT");
        this.shape_10.setTransform(18.1, 63);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(35.3, 67.6);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAKIgJgT");
        this.shape_12.setTransform(52.4, 63);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJAFIgRgJ");
        this.shape_13.setTransform(64.9, 50.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(69.5, 33.3);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJgEIgRAJ");
        this.shape_15.setTransform(64.9, 16.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(52.4, 3.6);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShoBoQhoBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRg");
        this.shape_17.setTransform(35.2, 33.3);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(35.2, 33.3);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(35.2, 33.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(35.2, 33.3);
        this.addChild(this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.outerRingLayer, this.ringLayer, this.text_4, this.text_3, this.text_2, this.hour, this.minute, this.shape);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 500.1);

    (lib.Stage1 = function() {
        this.initialize();

        this.clock = new lib.Klocken();
        this.clock.setTransform(430, 0, 6, 6, 0, 0, 0, 0, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(430 + (35.2 * 6), 33.3 * 6, 6, 6);
        this.hour = new cjs.Shape();
        this.hour.graphics.f("#D7172F").s().drawRect(0, 0, 2, 20);
        this.hour.setTransform(430 + (35.2 * 6), 33.3 * 6, 6, 6);
        this.hour.regX = 1;
        this.hour.regY = 0;
        this.hour.rotation = 180;
        //this.hour.alpha=1;
        //blue line/ minute needle
        this.minute = new cjs.Shape();
        this.minute.graphics.f("#0066A6").s().drawRect(0, 0, 2, 28);
        this.minute.setTransform(430 + (35.2 * 6), 33.3 * 6, 6, 6);
        this.minute.regX = 1;
        this.minute.regY = 0;
        this.minute.rotation = 180;

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        this.addChild(this.clock, this.commentTexts, this.minute, this.hour, this.shape);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {

        this.initialize();

        this.Stage1 = new lib.Stage1();
        this.addChild(this.Stage1);

        this.tweens = [];

        var timeout = 1000;
        var reduceTime = 20000 - timeout;

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 2000 - (reduceTime * 0),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 2000 - (reduceTime * 0),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + (30 * 1),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 22500 - (reduceTime * 1),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 22500 - (reduceTime * 1),
            alphaTimeout: timeout,
            rotationFrom: 180 + 30,
            rotationTo: 180 + (30 * 2),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 43000 - (reduceTime * 2),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 43000 - (reduceTime * 2),
            alphaTimeout: timeout,
            rotationFrom: 180 + 60,
            rotationTo: 180 + (30 * 3),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 63500 - (reduceTime * 3),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 63500 - (reduceTime * 3),
            alphaTimeout: timeout,
            rotationFrom: 180 + 90,
            rotationTo: 180 + (30 * 4),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 84000 - (reduceTime * 4),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 84000 - (reduceTime * 4),
            alphaTimeout: timeout,
            rotationFrom: 180 + 120,
            rotationTo: 180 + (30 * 5),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 104500 - (reduceTime * 5),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 104500 - (reduceTime * 5),
            alphaTimeout: timeout,
            rotationFrom: 180 + 150,
            rotationTo: 180 + (30 * 6),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 125000 - (reduceTime * 6),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 125000 - (reduceTime * 6),
            alphaTimeout: timeout,
            rotationFrom: 180 + 180,
            rotationTo: 180 + (30 * 7),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 145500 - (reduceTime * 7),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 145500 - (reduceTime * 7),
            alphaTimeout: timeout,
            rotationFrom: 180 + 210,
            rotationTo: 180 + (30 * 8),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 166000 - (reduceTime * 8),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 166000 - (reduceTime * 8),
            alphaTimeout: timeout,
            rotationFrom: 180 + 240,
            rotationTo: 180 + (30 * 9),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.minute,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 186500 - (reduceTime * 9),
            alphaTimeout: timeout,
            rotationFrom: 180,
            rotationTo: 180 + 360,
            override: false
        });
        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 186500 - (reduceTime * 9),
            alphaTimeout: timeout,
            rotationFrom: 180 + 270,
            rotationTo: 180 + (30 * 10),
            override: false
        });

        this.tweens.push({
            ref: this.Stage1.hour,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 186500 - (reduceTime * 9),
            override: false
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.other, this.stage1, this.stage2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
