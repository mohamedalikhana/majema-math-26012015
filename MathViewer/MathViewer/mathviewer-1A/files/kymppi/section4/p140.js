(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p124_1.png",
            id: "p124_1"
        }, {
            src: "images/p124_2.png",
            id: "p124_2"
        }]
    };

    (lib.p124_1 = function() {
        this.initialize(img.p124_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p124_2 = function() {
        this.initialize(img.p124_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("49", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(57, 22);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8490C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(52, 23.5);

        this.text_2 = new cjs.Text("Klockan – hel timme", "24px 'MyriadPro-Semibold'", "#8490C8");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(103, 26);

        this.pageBottomText = new cjs.Text("kunna klockan – hel timme", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(79, 649.5);

        this.text = new cjs.Text("140", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(44, 647);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(39.5, 660.8, 1, 1.1);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.SymbolClock = function() {
        this.initialize();

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#C8C9CB").s("#000000").ss(0.6, 0, 0, 4).arc(0, 0, 42, 0, 2 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#ffffff").s("#000000").ss(0.4, 0, 0, 4).arc(0, 0, 37.7, 0, 2 * Math.PI);
        this.shape_group2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(0, -38).lineTo(0, -35).moveTo(0, 35).lineTo(0, 38).moveTo(35, 0).lineTo(38, 0).moveTo(-35, 0).lineTo(-38, 0).moveTo(18, -33).lineTo(16.5, -30).moveTo(33, -19).lineTo(30, -17).moveTo(-30, 17.5).lineTo(-32.5, 19).moveTo(-17, 30).lineTo(-19, 32.5).moveTo(-31, -16).lineTo(-33.5, -18).moveTo(-19, -33).lineTo(-17, -30).moveTo(33, 17.5).lineTo(30.5, 16).moveTo(20, 31.5).lineTo(18.2, 29);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(10, -31);
        this.text_2 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(21.8, -20);
        this.text_3 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(27, -6);
        this.text_4 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(21.8, 10);
        this.text_5 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(10, 20);
        this.text_6 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(-4.5, 25);
        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(-19, 20);
        this.text_8 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(-31, 10);
        this.text_9 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(-36, -6);
        this.text_10 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(-32, -20);
        this.text_11 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(-20, -31);
        this.text_12 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(-7, -35.5);
        //center dot
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.lineHeight = 63;
        this.text_dot.setTransform(-6.5, -32);

        this.addChild(this.shape_group1, this.shape_group2, this.Line_1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_dot)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Klockan är 2.", "16px 'Myriad Pro'");
        this.text.lineHeight = 40;
        this.text.setTransform(71, 10);

        this.text_1 = new cjs.Text("Minutvisaren", "16px 'MyriadPro-Semibold'", "#0095DA");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(203, 49);

        this.text_2 = new cjs.Text("Timvisaren", "16px 'MyriadPro-Semibold'", "#DA2129");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(203, 71);

        this.text_3 = new cjs.Text("pekar på 12.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(298, 47);

        this.text_4 = new cjs.Text("pekar på 2.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(283, 69);

        // Main yellow block
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 1, 509, 126.5, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance_clock1 = new lib.SymbolClock();
        this.instance_clock1.setTransform(110, 76, 1, 1);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_1.setTransform(120, 70, 1.3, 1, 127);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(110, 61.1, 1.2, 1.1);
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.lineHeight = 63;
        this.text_dot.setTransform(103.5, 44);

        this.addChild(this.roundRect1, this.text, this.instance_clock1, this.shape_1, this.shape_2, this.text_dot, this.text_1, this.text_2, this.text_3, this.text_4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.3, 143.6);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Vad är klockan?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 509, 415, 10);
        this.roundRect1.setTransform(0, 12);
        //row-1
        this.instance_clock1 = new lib.SymbolClock();
        this.instance_clock1.setTransform(73, 72, 0.84, 0.84);
        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(82, 71.5, 1.2, 0.7, 89);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(73, 59.1, 1.2, 0.9);
        this.text_dot1 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot1.lineHeight = 53;
        this.text_dot1.setTransform(67, 42.5);

        this.instance_clock2 = new lib.SymbolClock();
        this.instance_clock2.setTransform(232, 72, 0.84, 0.84);
        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(228, 78.5, 1.2, 0.7, 209);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(232, 59.1, 1.2, 0.9);
        this.text_dot2 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot2.lineHeight = 53;
        this.text_dot2.setTransform(226, 42.5);

        this.instance_clock3 = new lib.SymbolClock();
        this.instance_clock3.setTransform(409, 72, 0.84, 0.84);
        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(413, 63.5, 1.2, 0.7, 206);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(409, 59.1, 1.2, 0.9);
        this.text_dot3 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot3.lineHeight = 53;
        this.text_dot3.setTransform(403, 42.5);
        //row-2
        this.instance_clock4 = new lib.SymbolClock();
        this.instance_clock4.setTransform(73, 175, 0.84, 0.84);
        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(82, 179, 1.2, 0.7, 116);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(73, 162.1, 1.2, 0.9);
        this.text_dot4 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot4.lineHeight = 53;
        this.text_dot4.setTransform(67, 145.5);

        this.instance_clock5 = new lib.SymbolClock();
        this.instance_clock5.setTransform(232, 175, 0.84, 0.84);
        this.shape_9 = new cjs.Shape(); // clock handle red
        this.shape_9.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_9.setTransform(239, 170.5, 1.2, 0.7, 57);
        this.shape_10 = new cjs.Shape(); // clock handle blue
        this.shape_10.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_10.setTransform(232, 162.1, 1.2, 0.9);
        this.text_dot5 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot5.lineHeight = 53;
        this.text_dot5.setTransform(226, 145.5);

        this.instance_clock6 = new lib.SymbolClock();
        this.instance_clock6.setTransform(409, 175, 0.84, 0.84);
        this.shape_11 = new cjs.Shape(); // clock handle red
        this.shape_11.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_11.setTransform(413, 180.5, 1.2, 0.7, 148);
        this.shape_12 = new cjs.Shape(); // clock handle blue
        this.shape_12.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_12.setTransform(409, 162.1, 1.2, 0.9);
        this.text_dot6 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot6.lineHeight = 53;
        this.text_dot6.setTransform(403, 145.5);
        //row-3
        this.instance_clock7 = new lib.SymbolClock();
        this.instance_clock7.setTransform(73, 276, 0.84, 0.84);
        this.shape_13 = new cjs.Shape(); // clock handle red
        this.shape_13.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_13.setTransform(73, 285, 1.2, 0.7);
        this.shape_14 = new cjs.Shape(); // clock handle blue
        this.shape_14.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_14.setTransform(73, 263.1, 1.2, 0.9);
        this.text_dot7 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot7.lineHeight = 53;
        this.text_dot7.setTransform(67, 246.5);

        this.instance_clock8 = new lib.SymbolClock();
        this.instance_clock8.setTransform(232, 276, 0.84, 0.84);
        this.shape_15 = new cjs.Shape(); // clock handle red
        this.shape_15.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_15.setTransform(223.5, 275.5, 1.2, 0.7, 90);
        this.shape_16 = new cjs.Shape(); // clock handle blue
        this.shape_16.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_16.setTransform(232, 263.1, 1.2, 0.9);
        this.text_dot8 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot8.lineHeight = 53;
        this.text_dot8.setTransform(226, 246.5);

        this.instance_clock9 = new lib.SymbolClock();
        this.instance_clock9.setTransform(409, 276, 0.84, 0.84);
        this.shape_17 = new cjs.Shape(); // clock handle red
        this.shape_17.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_17.setTransform(401, 281, 1.2, 0.7, 238);
        this.shape_18 = new cjs.Shape(); // clock handle blue
        this.shape_18.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_18.setTransform(409, 263.1, 1.2, 0.9);
        this.text_dot9 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot9.lineHeight = 53;
        this.text_dot9.setTransform(403, 246.5);
        //row-4
        this.instance_clock10 = new lib.SymbolClock();
        this.instance_clock10.setTransform(73, 377, 0.84, 0.84);
        this.shape_19 = new cjs.Shape(); // clock handle red
        this.shape_19.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_19.setTransform(73, 368, 1.2, 0.7);
        this.shape_20 = new cjs.Shape(); // clock handle blue
        this.shape_20.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_20.setTransform(73, 364.1, 1.2, 0.9);
        this.text_dot10 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot10.lineHeight = 53;
        this.text_dot10.setTransform(67, 347.5);

        this.instance_clock11 = new lib.SymbolClock();
        this.instance_clock11.setTransform(232, 377, 0.84, 0.84);
        this.shape_21 = new cjs.Shape(); // clock handle red
        this.shape_21.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_21.setTransform(228.5, 369.5, 1.2, 0.7, 150);
        this.shape_22 = new cjs.Shape(); // clock handle blue
        this.shape_22.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_22.setTransform(232, 364.1, 1.2, 0.9);
        this.text_dot11 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot11.lineHeight = 53;
        this.text_dot11.setTransform(226, 347.5);

        this.instance_clock12 = new lib.SymbolClock();
        this.instance_clock12.setTransform(409, 377, 0.84, 0.84);
        this.shape_23 = new cjs.Shape(); // clock handle red
        this.shape_23.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_23.setTransform(401, 372, 1.2, 0.7, 295);
        this.shape_24 = new cjs.Shape(); // clock handle blue
        this.shape_24.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_24.setTransform(409, 364.1, 1.2, 0.9);
        this.text_dot12 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot12.lineHeight = 53;
        this.text_dot12.setTransform(403, 347.5);

        var ToBeAdded = [];
        for (var col = 0; col < 3; col++) {
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var colSpace = col;
                if (col == 2) {
                    colSpace = 2.1;
                }
                if (row == 2) {
                    rowSpace = 1.97;
                } else if (row == 3) {
                    rowSpace = 2.93;
                }
                var temptext = new cjs.Text("Klockan är             .", "16px 'Myriad Pro'");
                temptext.lineHeight = -1;
                temptext.setTransform(20 + (161 * colSpace), 111.5 + (103.5 * rowSpace));
                if (row == 3 && col == 2) {
                    rowSpace = 2.95;
                    colSpace = 2.115;
                }

                var tempLine_1 = new cjs.Shape();
                tempLine_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(92 + (161 * colSpace), 125 + (103.5 * rowSpace)).lineTo(132 + (161 * colSpace), 125 + (103.5 * rowSpace));
                tempLine_1.setTransform(0, 0);
                ToBeAdded.push(tempLine_1, temptext);
            }
        }



        this.addChild(this.text_q1, this.text, this.roundRect1, this.instance_clock1, this.instance_clock2, this.instance_clock3, this.instance_clock4, this.instance_clock5, this.instance_clock6, this.instance_clock7, this.instance_clock8, this.instance_clock9, this.instance_clock10, this.instance_clock11, this.instance_clock12);
        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10, this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_20, this.shape_19, this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4, this.text_dot5, this.text_dot6, this.text_dot7, this.text_dot8, this.text_dot9, this.text_dot10, this.text_dot11, this.text_dot12)
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 400);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(299, 276, 1, 1, 0, 0, 0, 256.3, 217.9);
        this.v2 = new lib.Symbol6();
        this.v2.setTransform(299, 413, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
