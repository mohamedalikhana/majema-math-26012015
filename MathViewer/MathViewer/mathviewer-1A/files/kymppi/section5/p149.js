(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p149_1.png",
            id: "p149_1"
        }, {
            src: "images/p149_2.png",
            id: "p149_2"
        }]
    };

    (lib.p149_1 = function() {
        this.initialize(img.p149_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p149_2 = function() {
        this.initialize(img.p149_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("52", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(67, 22);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D83770").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(56, 23.5, 1.09, 1);

        this.text_2 = new cjs.Text("Mot programmering 2", "24px 'MyriadPro-Semibold'", "#D83770");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(113.5, 25);

        this.pageBottomText = new cjs.Text("kunna tolka enkla koder", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(457, 651);

        this.text = new cjs.Text("149", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(560, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(588, 660.8);

        this.instance = new lib.p149_1();
        this.instance.setTransform(115, 50, 0.474, 0.474);

        this.addChild(this.instance, this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D83770");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.text_q1 = new cjs.Text("Rita robotens väg. Pilen visar riktningen och talet visar antalet steg.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(20, 1.2);
        this.text_q2 = new cjs.Text("Färglägg cirkeln du kommer till.", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20, 20);

        this.instance_2 = new lib.p149_2();
        this.instance_2.setTransform(5, 48, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 45, 514.5, 366, 10);
        this.roundRect1.setTransform(0, 0);

        var ToBeAdded = [];
        var arrText = ['3', '2', '2', '4', '5', '3', '3', '5', '3', '3', '4', '5', '3', '1'];
        var loopCnt = 0;
        var txtYpos = ['115', '267'];
        var addrow = 3;
        for (var i = 0; i < txtYpos.length; i++) {
            if (i == 1) {
                addrow = 4;
            }
            var yPos = parseInt(txtYpos[i]);
            for (var col = 0; col < 2; col++) {

                for (var row = 0; row < addrow; row++) {
                    var text = arrText[loopCnt];
                    var rowSpace = row;
                    var colSpace = col;
                    if (i == 1 && col == 1 && row == 2) {
                        colSpace = 0.996;
                    }
                    if (row == 2) {
                        rowSpace = 2.05;
                    }
                    if (i == 1 && col == 1) {
                        yPos = 246.5;
                    }

                    if (loopCnt == 12) {
                        colSpace = 0.999;
                    }


                    var temptextN = new cjs.Text(text, "16px 'Myriad Pro'");
                    temptextN.lineHeight = -1;
                    temptextN.setTransform(35 + (430 * colSpace), yPos + (21.5 * rowSpace));
                    if (row == 2 && col == 1) {
                        rowSpace = 1.99;
                        colSpace = 1.009;
                    }
                    ToBeAdded.push(temptextN);
                    loopCnt++;
                }
            }
        }

        for (var col = 0; col < 17; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var fcolor = '#6D6E70';

                if (row == 0 && col == 16) {
                    fcolor = '#DA2129';
                } else if (row == 0 && col == 1) {
                    fcolor = '#DA2129';
                } else if (row == 6 && col == 16) {
                    fcolor = '#DA2129';
                } else if (row == 7 && col == 0) {
                    fcolor = '#DA2129';
                }
                var temp_text = new cjs.Text("•", "bold 20.2px 'Myriad Pro'", fcolor);
                temp_text.lineHeight = -1;
                var xPos = 73 + (colSpace * 21.2);
                var yPos = 106 + (rowSpace * 21.2);
                temp_text.setTransform(xPos, yPos);

                temp_round = new cjs.Shape();
                temp_round.graphics.s("#6D6E70").ss(0.8, 0, 0, 4).arc(xPos + 3.5, yPos + 9.8, 9, 0, 2 * Math.PI);
                temp_round.setTransform(0, 0);
                if ((row == 2 && col == 3) || (row == 5 && (col == 3 || col == 9 || col == 15)) || (row == 9 && (col == 0 || col == 6)) || (row == 10 && col == 9) || (row == 12 && (col == 9 || col == 15))) {
                    ToBeAdded.push(temp_round);
                }


                ToBeAdded.push(temp_text);
            }
        }

        this.line_1 = new cjs.Shape();
        this.line_1.graphics.s("#000000").ss(0.5).moveTo(70, 110).lineTo(423, 110).lineTo(423, 399).lineTo(70, 399).lineTo(70, 110);
        this.line_1.setTransform(0, 0);

        this.line_arrow1 = new cjs.Shape();
        this.line_arrow1.graphics.s("#D83770").f("#D83770").ss(0.9).moveTo(79, 137).lineTo(86, 137).lineTo(85, 135).lineTo(90, 137).lineTo(85, 139).lineTo(86, 137);
        this.line_arrow1.setTransform(234.6, 37.5);
        this.line_arrow1.skewX = 90;
        this.line_arrow1.skewY = 90;

        this.line_arrow2 = this.line_arrow1.clone(true);;
        this.line_arrow2.setTransform(0, 127);

        this.line_arrow3 = this.line_arrow1.clone(true);;
        this.line_arrow3.setTransform(492, 380);
        this.line_arrow3.skewX = -180;
        this.line_arrow3.skewY = -180;

        this.line_arrow4 = this.line_arrow1.clone(true);;
        this.line_arrow4.setTransform(492, 252.5);
        this.line_arrow4.skewX = -180;
        this.line_arrow4.skewY = -180;

        // this.line_arrow4 = this.line_arrow1.clone(true);;
        // this.line_arrow4.setTransform(531, 39);
        // this.line_arrow4.skewX = 90;
        // this.line_arrow4.skewY = 90;

        var arrYpos = ['110', '262'];
        this.textbox_group1 = new cjs.Shape();
        for (var i = 0; i < arrYpos.length; i++) {
            var yPos = parseInt(arrYpos[i]);
            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                for (var row = 0; row < 4; row++) {
                    var rowSpace = row;
                    if (i == 1 && column == 1) {
                        yPos = 242;
                    } else if (i == 0 && row == 3) {
                        continue;
                    }
                    this.textbox_group1.graphics.f('#ffffff').s("#D83770").ss(0.8).drawRect(14 + (columnSpace * 430), yPos + (rowSpace * 22), 36, 22);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.line_Barrow1 = new cjs.Shape();
        this.line_Barrow1.graphics.s("#000000").f("#000000").ss(0.9).moveTo(18, 122).lineTo(28, 122).lineTo(27, 120.5).lineTo(31, 122).lineTo(27, 123.5).lineTo(28, 122);
        this.line_Barrow1.setTransform(0, 21.5);
        this.line_Barrow2 = this.line_Barrow1.clone(true);
        this.line_Barrow2.setTransform(430, 44);
        this.line_Barrow3 = this.line_Barrow1.clone(true);
        this.line_Barrow3.setTransform(0, 152.5);
        this.line_Barrow4 = this.line_Barrow1.clone(true);
        this.line_Barrow4.setTransform(0, 197.5);

        this.line_Barrow5 = this.line_Barrow1.clone(true);
        this.line_Barrow5.setTransform(148, 96.5);
        this.line_Barrow5.skewX = 90;
        this.line_Barrow5.skewY = 90;
        this.line_Barrow6 = this.line_Barrow1.clone(true);
        this.line_Barrow6.setTransform(148, 140.5);
        this.line_Barrow6.skewX = 90;
        this.line_Barrow6.skewY = 90;
        this.line_Barrow7 = this.line_Barrow1.clone(true);
        this.line_Barrow7.setTransform(148, 270.5);
        this.line_Barrow7.skewX = 90;
        this.line_Barrow7.skewY = 90;
        this.line_Barrow8 = this.line_Barrow1.clone(true);
        this.line_Barrow8.setTransform(577, 118);
        this.line_Barrow8.skewX = 90;
        this.line_Barrow8.skewY = 90;
        this.line_Barrow9 = this.line_Barrow1.clone(true);
        this.line_Barrow9.setTransform(577, 249.5);
        this.line_Barrow9.skewX = 90;
        this.line_Barrow9.skewY = 90;
        this.line_Barrow10 = this.line_Barrow1.clone(true);
        this.line_Barrow10.setTransform(577, 294.5);
        this.line_Barrow10.skewX = 90;
        this.line_Barrow10.skewY = 90;


        this.line_Barrow11 = this.line_Barrow1.clone(true);
        this.line_Barrow11.setTransform(480, 244);
        this.line_Barrow11.skewX = 180;
        this.line_Barrow11.skewY = 180;
        this.line_Barrow12 = this.line_Barrow1.clone(true);
        this.line_Barrow12.setTransform(480, 376.5);
        this.line_Barrow12.skewX = 180;
        this.line_Barrow12.skewY = 180;
        this.line_Barrow13 = this.line_Barrow1.clone(true);
        this.line_Barrow13.setTransform(480, 421);
        this.line_Barrow13.skewX = 180;
        this.line_Barrow13.skewY = 180;

        this.line_Barrow14 = this.line_Barrow1.clone(true);
        this.line_Barrow14.setTransform(-96, 364);
        this.line_Barrow14.skewX = 270;
        this.line_Barrow14.skewY = 270;

        this.addChild(this.text, this.text_q1, this.text_q2, this.roundRect1);
        this.addChild(this.line_1, this.line_arrow1, this.line_arrow2, this.line_arrow3, this.line_arrow4);
        this.addChild(this.instance_2, this.textbox_group1);
        this.addChild(this.line_Barrow1, this.line_Barrow2, this.line_Barrow3, this.line_Barrow4, this.line_Barrow5,
            this.line_Barrow6, this.line_Barrow7, this.line_Barrow8, this.line_Barrow9, this.line_Barrow10,
            this.line_Barrow11, this.line_Barrow12, this.line_Barrow13, this.line_Barrow14);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);



        this.v1 = new lib.Symbol6();
        this.v1.setTransform(307, 441, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
