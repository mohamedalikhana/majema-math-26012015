(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p6_1.png",            
            id: "p6_1"
        }, {
            src: "images/p6_2.png",
            id: "p6_2"
        }]
    };

    (lib.p6_1 = function() {
        this.initialize(img.p6_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p6_2 = function() {
        this.initialize(img.p6_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {


        this.pageTitle = new cjs.Text("Vi repeterar talen 0 till 12", "24px 'MyriadPro-Semibold'", "#90BD22");
        this.pageTitle.setTransform(96, 40);

        this.text_1 = new cjs.Text("1", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(56, 40);

        this.text_2 = new cjs.Text("6", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(40, 658);

        this.pageFooterTextRight = new cjs.Text("förstå och kunna använda talen 0 till 20", "9px 'Myriad Pro'");
        this.pageFooterTextRight.setTransform(70, 659);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(44, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.pageTitle, this.shape_2, this.shape_1, this.shape, this.text_1, this.text_2,
            this.pageFooterTextRight);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p6_1();
        this.instance.setTransform(0, 52, 0.47, 0.46);

        this.text = new cjs.Text("Bind ihop talen 0 till 12.", "16px 'Myriad Pro'");
        this.text.setTransform(21, 40);

        this.text_1 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(2, 40);
        
        this.addChild(this.text, this.text_1, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 375);


    (lib.Symbol3 = function() {
        this.initialize();
        this.instance = new lib.p6_2();
        this.instance.setTransform(35, 55, 0.47, 0.47);

        this.text = new cjs.Text("Skriv talen som kommer före och efter.", "16px 'Myriad Pro'");
        this.text.setTransform(23, 39.5);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(4, 39.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(5, 0, ((1042) - 15) * 0.50, ((396) - 20) * 0.50, 10);
        this.roundRect1.setTransform(0, 51);

        var textArr = [];
        var lineX = [42, 70, 98, 210, 238, 266, 378, 406, 434, 42, 70, 98, 210, 238, 266, 378, 406, 434, 37, 70, 105, 205, 238, 272, 373, 404, 440];
        var lineY = [70, 70, 70, 70, 70, 70, 70, 70, 70, 130, 130, 130, 130, 130, 130, 130, 130, 130, 190, 190, 190, 190, 190, 190, 190, 190, 190];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = null;
            hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(33, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            textArr.push(hrLine_1);
        }

        
        var number=[1,3,5,4,6,8,9,10,11];
        var numX=[84,249,421,84,249,421,84,244,413];
        var numY=[110,110,110,170,170,170,230,230,230];

        for (var j = 0; j < numX.length; j++) 
        {
            var text_2 = null;
            text_2 = new cjs.Text(number[j],"bold 32px 'UusiTekstausMajema'", "#6C7373");
            text_2.setTransform(numX[j],numY[j]);
            textArr.push(text_2);
        }


        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.text_2);
        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

       

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 270);



    (lib.p6 = function() {
        this.initialize();



        this.v2 = new lib.Symbol3();
        this.v2.setTransform(293, 430, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(293, 82, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.addChild(this.other, this.v2, this.v1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
