(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p63_1.png",
            id: "p63_1"
        }]
    };

    // symbols:

    (lib.p63_1 = function() {
        this.initialize(img.p63_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();


        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(469 + (i * 32), 37, 27, 27);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageNoRight = new cjs.Text("63", "13px 'Myriad Pro'", "#FFFFFF");
        this.pageNoRight.lineHeight = 18;
        this.pageNoRight.setTransform(555, 648);
        this.pageNoRightBackground = new cjs.Shape();
        this.pageNoRightBackground.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.pageNoRightBackground.setTransform(579, 660.8);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoRightBackground, this.pageNoRight, this.top_textbox_group);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p63_1();
        this.instance.setTransform(15, 35, 0.37, 0.37);
        this.text = new cjs.Text("Hur många kex finns kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(21, 8);
        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(2, 8);

        this.textBoxGroup1 = new cjs.Shape();

        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 115, 20, 23);
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(338 + (i * 20), 115, 20, 23);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(338 + (i * 20), 238, 20, 23);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 238, 20, 23);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(338 + (i * 20), 361, 20, 23);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 361, 20, 23);
        }
        this.textBoxGroup6.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((1394 * 0.5) - 10) * 0.365, ((1004 * (1 / 3)) - 16) * 0.365, 10);
        this.roundRect1.setTransform(3, 31);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1394 * 0.5) + 8) * 0.37, 31);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 154);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1394 * 0.5) + 8) * 0.37, 154);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(3, 277);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((1394 * 0.5) + 8) * 0.37, 277);

        var textArr = [];

        this.text_3 = new cjs.Text("Jag äter 2.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 22;
        this.text_3.setTransform(121, 49);
        this.text_4 = new cjs.Text("Jag äter 3.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 22;
        this.text_4.setTransform(370, 49);
        this.text_5 = new cjs.Text("Jag äter 2.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 22;
        this.text_5.setTransform(121, 173);
        this.text_6 = new cjs.Text("Jag äter 4.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 22;
        this.text_6.setTransform(370, 173);
        this.text_7 = new cjs.Text("Jag äter 4.", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 22;
        this.text_7.setTransform(121, 298);
        this.text_8 = new cjs.Text("Jag äter 2.", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 22;
        this.text_8.setTransform(370, 298);

        textArr.push(this.text_3);
        textArr.push(this.text_4);
        textArr.push(this.text_5);
        textArr.push(this.text_6);
        textArr.push(this.text_7);
        textArr.push(this.text_8);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5,
            this.roundRect6, this.instance, this.textBoxGroup1, this.textBoxGroup2, this.textBoxGroup3,
            this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6, this.text, this.text_1);

        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);


    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text("Räkna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(21, 2);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(2, 2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(2.5, 25, 510, 151.5, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = (column == 2) ? 1.971 : column;
            for (var row = 0; row < 5; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(105 + (columnSpace * 158), 34 + (row * 28), 20, 22.5);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("3  –  1  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(40, 38);
        this.label2 = new cjs.Text("4  –  1  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(40, 66.5);
        this.label3 = new cjs.Text("5  –  1  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(40, 93);
        this.label4 = new cjs.Text("6  –  1  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(40, 122);
        this.label5 = new cjs.Text("2  –  1  =  ", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(40, 149.5);


        this.label6 = new cjs.Text("2  +  1  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(197, 38);
        this.label7 = new cjs.Text("3  +  1  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(197, 66.5);
        this.label8 = new cjs.Text("4  +  1  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(197, 93);
        this.label9 = new cjs.Text("5  +  1  =  ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(197, 122);
        this.label10 = new cjs.Text("1  +  1  =  ", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(197, 149.5);

        this.label11 = new cjs.Text("6  –  2  =  ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(352, 38);
        this.label12 = new cjs.Text("5  –  2  =  ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(352, 66.5);
        this.label13 = new cjs.Text("3  +  2  =  ", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(352, 93);
        this.label14 = new cjs.Text("3  –  2  =  ", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(352, 122);
        this.label15 = new cjs.Text("4  +  2  =  ", "16px 'Myriad Pro'");
        this.label15.lineHeight = 19;
        this.label15.setTransform(352, 149.5);
        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(311.3, 107.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(311.3, 497.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
