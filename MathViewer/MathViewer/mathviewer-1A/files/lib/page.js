if (window.parent == this) {
    $('html').addClass('orphan');
}







var iosVersion = iOSversion();
if (iosVersion != undefined && iosVersion[0] == 7) {
    $('html').addClass('ipad ios7');
} else if (iosVersion == undefined) {
    $('html').addClass('no-touch');
}

function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    }
}

function isTouchDevice() {
    var _b = false;
    if (typeof window.Modernizr === 'undefined')
        _b === ("ontouchstart" in window || (window.DocumentTouch && document instanceof DocumentTouch) || window.navigator.msMaxTouchPoints);
    else
        _b = window.navigator.msMaxTouchPoints || Modernizr.touch;
    return _b;
}


var stageProperties = {
    width: 610,
    height: 678
};

var sideFrame = true;
var sideFrameColor = '#999999';

var max_scale_ratio = 3.5;
var max_zoom_level = 3;

var highlighting = true;
var frameHighlighting = false;
var frameHighlightColor = '#ff0000';

var isActive = true;
var isClickable = true;
var isKeyboardEnabled = true;
var solutionForAll = false;

var currentAnswers = true;

var output = false;
var outputPosition = 'topright';
var outputTitle = 'eKymppi';

var activation = true;

var debugJS = false;


var loaded;
var canvasProperties;
var spreadProperties;
var middlePoint;
var update, frequentUpdate = true;
var pageLRM;
var pageCount;
var position;
var stageInfo = {
    x: 0,
    y: 0,
    scaling: 1,
    ratio: 1
};
var stageInfoBeforeStep;
var sideFrameMC;


var steps, stepsCount;
var btn_navigate1, btn_navigate2, btn_spread, btn_return;



var features = [];



if (getVariable('print') === '-1') {
    output = false;
} else if (getVariable('print') === '0') {
    stageProperties.width = 595;
    stageProperties.height = 842;
    sideFrame = false;
    output = true;
} else if (getVariable('print') === '1') {
    stageProperties.width = 842;
    stageProperties.height = 595;
    sideFrame = false;
    output = true;
    outputPosition = 'horizontal';
}

if (getVariable('properties') !== 'false') {
    stageProperties.width = parseFloat(getVariable('properties').split('x')[0]);
    stageProperties.height = parseFloat(getVariable('properties').split('x')[1]);
    if (stageProperties.width > stageProperties.height)
        outputPosition = 'horizontal';
}
if (getVariable('sideFrame') !== 'false') {
    sideFrame = getVariable('sideFrame') == '1' ? true : false;
}
if (getVariable('frameHighlight') !== 'false') {
    frameHighlighting = getVariable('frameHighlight') == '1' ? true : false;
}


function loadStep1() {
    debug('loadStep1: load');

    if (this["lib"] === undefined)
        setTimeout(loadStep1, 100);
    else {
        debug('loadStep1: done');

        $('#container').css('visibility', 'hidden');

        if (lib.properties) {
            if (lib.properties.manifest.length === 0)
                lib.properties.manifest = [{
                    id: 'dummy',
                    src: 'dummy.png'
                }];
        }
        init();
        loadStep2();
    }
}

function loadStep2() {
    debug('loadStep2: load');

    if (stage == null)
        setTimeout(loadStep2, 100);
    else {
        debug('loadStep2: done');


        createjs_fontfix(exportRoot);

        canvasReady();

        setTimeout(loadStep3, 250);
    }
}

function loadStep3() {
    debug('loadStep3: done');

    $('#container').css('visibility', 'visible');

    loaded = true;

    if (typeof window.parent.iframeloaded !== 'undefined')
        window.parent.iframeloaded(window);
}

function canvasReady() {

    if (exportRoot.isExercise === true) {
        sideFrame = false;
        stageProperties = {
            width: 1200,
            height: 500
        };
    } else {
        sideFrame = true;
        stageProperties = {
            width: 610,
            height: 678
        };
    }
    middlePoint = {
        x: stageProperties.width / 2,
        y: stageProperties.height / 2
    };
    spreadProperties = {
        width: stageProperties.width,
        height: stageProperties.height
    };


    createjs.Ticker.removeEventListener("tick", stage);
    createjs.Touch.enable(stage);
    createjs.Ticker.addEventListener('tick', tick);
    stage.enableMouseOver(10);


    var a, aa, item, obj;

    steps = [{
        object: exportRoot,
        x: 0,
        y: 0,
        width: stageProperties.width,
        height: stageProperties.height,
        scaling: 1,
        info: {}
    }];
    if (exportRoot.other) {
        if (exportRoot.other.pageBottomText) {
            var xPos = exportRoot.other.pageBottomText.x;
            exportRoot.other.pageBottomText.cursor = 'pointer';
            exportRoot.other.pageBottomText.addEventListener("mouseover", function() {
                exportRoot.other.pageBottomText.font = "16px 'Myriad Pro'";
                if (exportRoot.other.pageBottomText.pos == "right") {
                    var transformX = 130;
                    if (xPos >= 440) {
                        transformX = 77;
                    } else if (xPos >= 390) {
                        transformX = 115;
                    } else if (xPos >= 380) {
                        transformX = 120;
                    } else if (xPos >= 370) {
                        transformX = 130;
                    } else if (xPos >= 360) {
                        transformX = 140;
                    } else if (xPos < 320) {
                        transformX = 180;
                    }
                    exportRoot.other.pageBottomText.x = exportRoot.other.pageBottomText.x - transformX;
                }
                stage.update();
            });

            exportRoot.other.pageBottomText.addEventListener("mouseout", function() {
                exportRoot.other.pageBottomText.font = "9px 'Myriad Pro'";
                if (exportRoot.other.pageBottomText.pos == "right") {
                    var transformX = 130;
                    if (xPos >= 440) {
                        transformX = 77;
                    } else if (xPos >= 390) {
                        transformX = 115;
                    } else if (xPos >= 380) {
                        transformX = 120;
                    } else if (xPos >= 370) {
                        transformX = 130;
                    } else if (xPos >= 360) {
                        transformX = 140;
                    } else if (xPos < 320) {
                        transformX = 180;
                    }
                    exportRoot.other.pageBottomText.x = exportRoot.other.pageBottomText.x + transformX;
                }
                stage.update();
            });
        }
    }
    for (a = 1; a <= exportRoot.getNumChildren(); a++) {
        item = exportRoot["v" + a.toString()];

        if (item === undefined) {
            continue;
        }

        item.addEventListener("click", stepFunctions);
        if (highlighting) {
            item.addEventListener("rollover", stepRoll);
            item.addEventListener("rollout", stepRoll);
        }
        item.cursor = 'pointer';
        item.x -= item.regX;
        item.regX = 0;
        item.y -= item.regY;
        item.regY = 0;

        var info = {};
        if (exportRoot.v_info) {
            $.each(exportRoot.v_info, function(_a, _obj) {
                if (_obj.v === "v" + a.toString()) {
                    info = _obj;
                }
            });
        }
        obj = {
            object: item,
            x: item.x,
            y: item.y,
            width: item.virtualBounds.width,
            height: item.virtualBounds.height,
            rotation: item.rotation,
            scaling: item.scaleX,
            info: info
        };

        if (item['background']) {
            if (item['background'].timeline)
                item['background'].gotoAndStop(0);
        } else {
            var gr = new createjs.Graphics();
            gr.beginFill('rgba(255,255,255,.01)');
            gr.drawRoundRect(-5, -5, obj.width + 10, obj.height + 10, 5);
            gr.endFill();

            var s = new createjs.Shape(gr);

            var background = new createjs.MovieClip();
            background.id = 'background';
            background.addChild(s);
            item.addChildAt(background, 0);

            gr = new createjs.Graphics();
            gr.beginStroke(frameHighlightColor);
            gr.setStrokeStyle(1);
            gr.drawRoundRect(-5, -5, obj.width + 10, obj.height + 10, 10);
            gr.endStroke();

            s = new createjs.Shape(gr);

            var frame = new createjs.Shape(gr);
            frame.id = 'frame';
            item.addChildAt(frame, 1);

            obj.frame = frame;
            frame.visible = frameHighlighting;
        }

        steps.push(obj);
    }

    for (a = 1; a <= exportRoot.getNumChildren(); a++) {
        item = exportRoot["vl" + a.toString()];

        if (item === undefined)
            continue;

        if (item.virtualBounds === undefined)
            continue;

        item.addEventListener("click", stepFunctions);
        if (highlighting) {
            item.addEventListener("rollover", stepRoll);
            item.addEventListener("rollout", stepRoll);
        }
        item.cursor = 'pointer';
        item.x -= item.regX;
        item.regX = 0;
        item.y -= item.regY;
        item.regY = 0;

        obj = {
            object: item,
            x: item.x,
            y: item.y,
            width: item.virtualBounds.width,
            height: item.virtualBounds.height,
            rotation: item.rotation,
            scaling: item.scaleX
        };

        if (item['background']) {
            if (item['background'].timeline)
                item['background'].gotoAndStop(0);
        } else {
            var gr = new createjs.Graphics();
            gr.beginFill('rgba(255,255,255,.01)');
            gr.drawRoundRect(-5, -5, obj.width + 10, obj.height + 10, 5);
            gr.endFill();

            var s = new createjs.Shape(gr);

            var background = new createjs.MovieClip();
            background.id = 'background';
            background.addChild(s);
            item.addChildAt(background, 0);

            gr = new createjs.Graphics();
            gr.beginStroke(frameHighlightColor);
            gr.setStrokeStyle(1);
            gr.drawRoundRect(-5, -5, obj.width + 10, obj.height + 10, 10);
            gr.endStroke();

            s = new createjs.Shape(gr);

            var frame = new createjs.Shape(gr);
            frame.id = 'frame';
            item.addChildAt(frame, 1);

            obj.frame = frame;
            frame.visible = frameHighlighting;
        }

        obj.step = steps[a];
        obj.step.extraStep = obj;
    }

    pageLRM = 1;
    for (a = 0; a < exportRoot.getNumChildren(); a++) {
        item = exportRoot.getChildAt(a);
        if (item.x < stageProperties.width / 2 - spreadProperties.width / 4 - 10) {
            pageLRM = 2;
            break;
        }
    }

    if (sideFrame === true) {
        gr = new createjs.Graphics();
        gr.setStrokeStyle(1);
        gr.beginStroke(sideFrameColor);
        if (pageLRM === 1) {
            gr.rect(spreadProperties.width / 4, 0, spreadProperties.width / 2, spreadProperties.height);
        } else {
            //var stageProperties = {    width: 1200,    height: 500};
            gr.rect(0, 0, spreadProperties.width, spreadProperties.height);
            gr.moveTo(spreadProperties.width / 2, 0);
            //gr.lineTo(spreadProperties.width/2, spreadProperties.height);
        }

        sideFrameMC = new createjs.Shape(gr);
        sideFrameMC.x = (stageProperties.width - spreadProperties.width) / 2;
        sideFrameMC.y = (stageProperties.height - spreadProperties.height) / 2;
        stage.addChild(sideFrameMC);
    }

    var buttons = document.createElement('div');
    $(buttons).attr('id', 'buttons');
    $('body').append(buttons);

    features = [
        new Solutions({
            featured: isActive,
            all: solutionForAll,
            click: isClickable,
            key: isKeyboardEnabled
        }),
        new Answers({
            featured: currentAnswers
        }),
        new Show(),
        new Result({
            inUse: output,
            position: outputPosition,
            title: outputTitle
        })
    ];

    $.each(steps, function(a, obj) {
        $.each(features, function(t, customFunction) {
            customFunction.baseSetting(steps[a]);
        });
    });

    var exerciseClass = exportRoot.isExercise ? ' exercise' : '';
    btn_navigate1 = document.createElement('a');
    $(btn_navigate1).text('<');
    $(btn_navigate1).attr('title', mathviewer.getTranslation('Previous', defaultLanguage));
    $(btn_navigate1).addClass('btn navigation navigate_prev' + exerciseClass);
    $(btn_navigate1).on('click', navigation_functions);
    new NoClickDelay(btn_navigate1);
    $('body').append(btn_navigate1);
    $(btn_navigate1).hide();

    btn_navigate2 = document.createElement('a');
    $(btn_navigate2).text('>');
    $(btn_navigate2).attr('title', mathviewer.getTranslation('Next', defaultLanguage));
    $(btn_navigate2).addClass('btn navigation navigate_next' + exerciseClass);
    $(btn_navigate2).on('click', navigation_functions);
    new NoClickDelay(btn_navigate2);
    $('body').append(btn_navigate2);
    $(btn_navigate2).hide();

    btn_spread = document.createElement('a');
    $(btn_spread).text(mathviewer.getTranslation('Return', defaultLanguage));
    $(btn_spread).addClass('button navigate_spread' + exerciseClass);
    $(btn_spread).on('click', navigation_functions);
    new NoClickDelay(btn_spread);
    $('#buttons').append(btn_spread);
    $(btn_spread).hide();

    if (getVariable('return') === '1') {
        btn_return = document.createElement('a');
        $(btn_return).text(mathviewer.getTranslation('Return', defaultLanguage));
        $(btn_return).addClass('button' + exerciseClass);
        $(btn_return).on('click', return_functions);
        new NoClickDelay(btn_return);
        $('#buttons').append(btn_return);
        $(btn_return).hide();
    }

    if (isTouchDevice()) {
        $('#canvas')[0].addEventListener("touchstart", touchStart);
        $('#canvas')[0].addEventListener("touchmove", touchMove);
        $('#canvas')[0].addEventListener("touchend", touchEnd);

        $('#canvas')[0].addEventListener("MSPointerDown", touchStart);
        $('#canvas')[0].addEventListener("MSPointerMove", touchMove);
        $('#canvas')[0].addEventListener("MSPointerUp", touchEnd);
    }

    if (window.parent === this) {
        window.addEventListener('orientationchange', updateProperties);
        window.onresize = updateProperties;
    }
    updateProperties();

    if (getVariable('disabled') === '1') {
        $(buttons).hide();
    } else {
        if (btn_return)
            $(btn_return).show();

        stepsCount = 0;
        step = steps[0];
        setSteps();
    }
}



function activate(bool) {
    if (bool) {
        if (activation)
            return;

        activation = true;

        if (stage) {
            createjs.Touch.enable(stage);
            createjs.Ticker.addEventListener('tick', tick);
            stage.enableMouseOver(10);

            if (isTouchDevice()) {
                $('#canvas')[0].addEventListener("touchstart", touchStart);
                $('#canvas')[0].addEventListener("touchmove", touchMove);
                $('#canvas')[0].addEventListener("touchend", touchEnd);

                $('#canvas')[0].addEventListener("MSPointerDown", touchStart);
                $('#canvas')[0].addEventListener("MSPointerMove", touchMove);
                $('#canvas')[0].addEventListener("MSPointerUp", touchEnd);
            }
        }
    } else {
        if (!activation)
            return;

        activation = false;

        if (stage) {
            createjs.Touch.disable(stage);
            createjs.Ticker.removeEventListener('tick', tick);
            stage.enableMouseOver(0);

            if (isTouchDevice()) {
                $('#canvas')[0].removeEventListener("touchstart", touchStart);
                $('#canvas')[0].removeEventListener("touchmove", touchMove);
                $('#canvas')[0].removeEventListener("touchend", touchEnd);

                $('#canvas')[0].removeEventListener("MSPointerDown", touchStart);
                $('#canvas')[0].removeEventListener("MSPointerMove", touchMove);
                $('#canvas')[0].removeEventListener("MSPointerUp", touchEnd);
            }
        }
    }
}

var touch = false;
var touchDirection = '';
var touchScaleStart = 1;
var touchPointsStart = {};
var touchPointsNow = {};
var touchPointStart;
var touchPointNow;
var touchFingers = 0;
var touchTimeout = undefined;

function touchStart(e) {
    if (e.preventManipulation)
        e.preventManipulation();
    if (e.preventDefault)
        e.preventDefault();

    if (touchDirection != '' || touchFingers === 2 || createjs.Tween.hasActiveTweens())
        return;

    var touchPoints = (typeof e.changedTouches != 'undefined') ? e.changedTouches : [e];
    $.each(touchPoints, function(a, tp) {
        var tp_id = (typeof tp.identifier != 'undefined') ? tp.identifier : (typeof tp.pointerId != 'undefined') ? tp.pointerId : 1;
        touchPointsStart[tp_id] = touchPointsNow[tp_id] = {
            x: tp.clientX * stageInfo.ratio,
            y: tp.clientY * stageInfo.ratio
        };
    });

    if (touchTimeout === undefined)
        touchTimeout = setTimeout(doTouch, 100);

    function doTouch() {
        touchTimeout = undefined;

        var points = score(touchPointsStart);
        touchFingers = points.length;

        var p = {};

        if (touchFingers === 1)
            p = {
                x: points[0].x,
                y: points[0].y
            };
        else if (touchFingers === 2)
            p = {
                x: Math.min(points[0].x, points[1].x) + (Math.max(points[0].x, points[1].x) - Math.min(points[0].x, points[1].x)) / 2,
                y: Math.min(points[0].y, points[1].y) + (Math.max(points[0].y, points[1].y) - Math.min(points[0].y, points[1].y)) / 2
            };
        else
            return;

        touch = true;
        touchDirection = '';
        touchScaleStart = stage.scaleX;
        touchPointStart = p;
        touchPointNow = p;

        if (step === null || touchFingers === 2)
            cacheMode(true, true);
        else
            cacheMode(true, false);
    }
}

function touchMove(e) {
    if (e.preventManipulation)
        e.preventManipulation();
    if (e.preventDefault)
        e.preventDefault();


    if (!touch)
        return;

    var touchPoints = (typeof e.changedTouches != 'undefined') ? e.changedTouches : [e];
    $.each(touchPoints, function(a, tp) {
        var tp_id = (typeof tp.identifier != 'undefined') ? tp.identifier : (typeof tp.pointerId != 'undefined') ? tp.pointerId : 1;
        touchPointsNow[tp_id] = {
            x: tp.clientX * stageInfo.ratio,
            y: tp.clientY * stageInfo.ratio
        };
    });

    var points1 = score(touchPointsStart);
    var points2 = score(touchPointsNow);

    var p = {};

    if (touchFingers == 1) {
        p = points2[0];
    } else if (touchFingers == 2) {
        p = {
            x: Math.min(points2[0].x, points2[1].x) + (Math.max(points2[0].x, points2[1].x) - Math.min(points2[0].x, points2[1].x)) / 2,
            y: Math.min(points2[0].y, points2[1].y) + (Math.max(points2[0].y, points2[1].y) - Math.min(points2[0].y, points2[1].y)) / 2
        };
    } else
        return;

    var x, y, s, dist0, dist1;

    x = stage.x;
    y = stage.y;
    s = stage.scaleX;

    if (touchFingers == 2) {
        dist0 = getDistance(points1[0], points1[1]);
        dist1 = getDistance(points2[0], points2[1]);
        s = dist1 / dist0 * touchScaleStart;
        s = Math.max(stageInfo.scaling, Math.min(stageInfo.scaling * max_zoom_level, s));
    }

    x += (p.x - touchPointNow.x) + ((-x + p.x) - (-x + p.x) * s / stage.scaleX);
    y += (p.y - touchPointNow.y) + ((-y + p.y) - (-y + p.y) * s / stage.scaleY);

    if (stage.scaleX === stageInfo.scaling && touchFingers === 1 && stepsCount > 0) {


        steps[stepsCount].object.x = steps[stepsCount].isoX + (p.x - touchPointStart.x) / stage.scaleX;

        if (stepsCount > 1) {
            steps[stepsCount - 1].object.x = (steps[stepsCount].object.x + steps[stepsCount].width * steps[stepsCount].object.scaleX / 2) - (canvasProperties.width / stage.scaleX) / 2 - steps[stepsCount - 1].width * steps[stepsCount - 1].object.scaleX;
            if (!steps[stepsCount - 1].object.visible) {
                steps[stepsCount - 1].object.visible = true;
                cacheMode(true, false, (steps[stepsCount - 1].object));
            }
        }


        if (stepsCount < steps.length - 1) {
            steps[stepsCount + 1].object.x = (steps[stepsCount].object.x + steps[stepsCount].width * steps[stepsCount].object.scaleX / 2) + (canvasProperties.width / stage.scaleX) / 2;
            if (!steps[stepsCount + 1].object.visible) {
                steps[stepsCount + 1].object.visible = true;
                cacheMode(true, false, (steps[stepsCount + 1].object));
            }
        }

        touchDirection = 'none';
        if (p.x - touchPointStart.x > 100)
            touchDirection = 'prev';
        else if (p.x - touchPointStart.x < -100)
            touchDirection = 'next';
        x = stage.x;
        y = stage.y;
        s = stage.scaleX;
    } else {
        x = Math.min(Math.max(0, (canvasProperties.width - stageProperties.width * s) / 2), Math.max(canvasProperties.width - stageProperties.width * s, x));
        y = Math.min(Math.max(0, (canvasProperties.height - stageProperties.height * s) / 2), Math.max(canvasProperties.height - stageProperties.height * s, y));
    }

    stage.x = x;
    stage.y = y;
    stage.scaleX = stage.scaleY = s;
    touchPointNow = p;
    update = true;
}

function touchEnd(e) {
    if (e.preventManipulation)
        e.preventManipulation();
    if (e.preventDefault)
        e.preventDefault();


    if (!touch) {
        touchCancel(e);
        return;
    }

    var touchPoints = (typeof e.changedTouches != 'undefined') ? e.changedTouches : [e];
    $.each(touchPoints, function(a, tp) {
        var tp_id = (typeof tp.identifier != 'undefined') ? tp.identifier : (typeof tp.pointerId != 'undefined') ? tp.pointerId : 1;
        delete touchPointsStart[tp_id];
        delete touchPointsNow[tp_id];
    });

    var points = score(touchPointsNow);
    touchFingers = points.length;

    if (touchFingers === 2) {
        touchCancel(e);
        return;
    }

    if (stepsCount > 0) {
        if (touchDirection === 'prev' && stepsCount > 1)
            updateStep(stepsCount - 1);
        else if (touchDirection === 'next' && stepsCount < steps.length - 1)
            updateStep(stepsCount + 1);
        else
            updateStep(stepsCount);
    }


    touchCancel(e);
}

function touchCancel(e) {
    touchDirection = "";
    touchScaleStart = 1;
    touchPointsStart = {};
    touchPointsNow = {};
    touchPointStart = null;
    touchPointNow = null;
    touchFingers = 0;

    if (touchTimeout) {
        clearTimeout(touchTimeout);
        touchTimeout = undefined;
    }

    setTimeout(function() {
        touch = false;
    }, 50);

    if (!createjs.Tween.hasActiveTweens())
        cacheMode(false);


    update = true;
}

function updatePage(s, tween) {
    if (position === "topright") {
        var scale = Math.min(canvasProperties.width / (stageProperties.width / 2), canvasProperties.height / stageProperties.height);

        if (s != pageCount) {
            pageCount = s;
            if (pageCount === 1)
                stageInfo.x += spreadProperties.width * scale / 2;
            else
                stageInfo.x -= spreadProperties.width * scale / 2;

            if (tween === false)
                stage.x = stageInfo.x;
            else {
                createjs.Tween.get(stage).to({
                    x: stageInfo.x
                }, 500, createjs.Ease.sineOut);
                createjs.Tween.get({}).wait(500).call(tweenReady);
            }
        } else {
            if (tween === false)
                stage.x = stageInfo.x;
            else {
                createjs.Tween.get(stage).to({
                    x: stageInfo.x
                }, 250, createjs.Ease.sineOut);
                createjs.Tween.get({}).wait(250).call(tweenReady);
            }
        }
    } else if (position === "horizontal") {
        if (tween === false)
            stage.x = stageInfo.x;
        else {
            createjs.Tween.get(stage).to({
                x: stageInfo.x
            }, 250, createjs.Ease.sineOut);
            createjs.Tween.get({}).wait(250).call(tweenReady);
        }
    }


    function tweenReady() {
        cacheMode(false);
    }
}

function updateStep(v) {
    var a, w, x;

    w = steps[v].isoX - steps[v].object.x;
    for (a = Math.max(v - 1, 1); a <= Math.min(v + 1, steps.length - 1); a++) {
        x = steps[a].object.x + w;
        createjs.Tween.get(steps[a].object).to({
            x: x
        }, 500, createjs.Ease.sineOut);
    }
    createjs.Tween.get({}).wait(500).call(tweenReady, [v - stepsCount]);


    function tweenReady(i) {
        var a;

        for (a = Math.max(stepsCount - 1, 1); a <= Math.min(stepsCount + 1, steps.length - 1); a++) {
            steps[a].object.visible = (a === stepsCount);
        }

        if (i !== 0) {
            closeStep(false);
            stepsCount += i;
            setSteps(false);
        }


        cacheMode(false);
    }
}


function setNavigations() {
    if (stepsCount === 0) {
        $(btn_navigate1).hide();
        $(btn_navigate2).hide();
        $(btn_spread).hide();
    } else {
        $(btn_navigate1).show();
        $(btn_navigate2).show();
        $(btn_spread).show();

        if (stepsCount === 1)
            $(btn_navigate1).hide();
        if (stepsCount === steps.length - 1)
            $(btn_navigate2).hide();
    }
}

function hideNavigation() {
    $(btn_navigate1).hide();
    $(btn_navigate2).hide();
    $(btn_spread).hide();
}

function navigation_functions(e) {

    if (exportRoot.isExercise) {
        return exerciseNavigationFunctions(e);
    }
    switch (e.target) {
        case btn_navigate1:
            closeStep();
            stepsCount--;
            break;
        case btn_navigate2:
            closeStep();
            stepsCount++;
            break;
        case btn_spread:
            closeStep(true);
            stepsCount = 0;
            break;
    }

    setSteps();
}

function exerciseNavigationFunctions(e) {
    //alert();
    switch (e.target) {
        case btn_navigate1:
            closeStep();
            stepsCount--;
            break;
        case btn_navigate2:
            closeStep();
            stepsCount++;
            break;
        case btn_spread:
            closeStep(true);
            stepsCount = 0;
            break;
    }
    setSteps();
    return true;
}

function stepFunctions(e) {
    if (touch === true || stepsCount > 0)
        return;

    closeStep(true);

    var v;
    if (e.currentTarget != undefined)
        v = e.currentTarget;
    else if (e.target != undefined)
        v = e.target;
    else
        v = e;

    $.each(steps, function(a, obj) {
        if (obj.object === v)
            stepsCount = a;
        else if (obj.extraStep) {
            if (obj.extraStep.object === v)
                stepsCount = a;
        }
    });
    setSteps();


    update = true;
}

function stepRoll(e) {
    if (touch === true || stepsCount > 0)
        return;

    var v, v_obj

    if (e.currentTarget != undefined)
        v = e.currentTarget;
    else if (e.target != undefined)
        v = e.target;
    else
        v = e;

    $.each(steps, function(a, obj) {
        if (obj.object === v) {
            if (obj.extraStep)
                v_obj = [obj, obj.extraStep];
            else
                v_obj = obj;
        } else if (obj.extraStep) {
            if (obj.extraStep.object === v)
                v_obj = [obj, obj.extraStep];
        }
    });

    if (!v_obj) {
        v_obj = {
            object: v
        };
        if (e.type === 'rollover') {
            v_obj.x = v.x;
            v_obj.y = v.y;
            v_obj.width = v.virtualBounds.width;
            v_obj.scaling = 1;
        } else if (e.type === 'rollout') {
            v_obj.x = v.x + 2;
            v_obj.y = v.y + 2;
            v_obj.width = v.virtualBounds.width + 4;
            v_obj.scaling = 1;
        }
    }

    if ($.isArray(v_obj) === false) {
        v_obj = [v_obj];
    }

    switch (e.type) {
        case 'rollover':
            $.each(v_obj, function(a, obj) {
                obj.object.scaleX = obj.object.scaleY = (obj.scaling * (obj.width + 14) / obj.width);
                obj.object.x -= 4;
                obj.object.y -= 2;
            });
            break;
        case 'rollout':
            $.each(v_obj, function(a, obj) {
                obj.object.scaleX = obj.object.scaleY = obj.scaling;
                obj.object.x = obj.x;
                obj.object.y = obj.y;
            });
            break;
    }


    update = true;
}

function setSteps(all) {
    var a, item;


    steps[stepsCount].object.visible = true;
    if (stepsCount > 0) {
        steps[stepsCount].object.removeEventListener("click", stepFunctions);
        steps[stepsCount].object.cursor = null;
    }


    setNavigations();

    backgroundFormat();

    $.each(features, function(t, customFunction) {
        customFunction.open(stepsCount);
    });

    if (all === false)
        return;

    if (stepsCount > 0) {
        for (a = 0; a < exportRoot.getNumChildren(); a++) {
            item = exportRoot.getChildAt(a);

            if (item === steps[stepsCount].object)
                continue;

            if (item.contains === undefined)
                continue;

            item.visible = false;
        }

        steps[stepsCount].object.visible = true;
    }


    if (sideFrameMC && stepsCount === 0)
        sideFrameMC.visible = true;
    else if (sideFrameMC && stepsCount > 0)
        sideFrameMC.visible = false;

    if (btn_return && stepsCount === 0)
        $(btn_return).show();
    else if (btn_return && stepsCount > 0)
        $(btn_return).hide();

    if (!stageInfoBeforeStep)

        stageInfoBeforeStep = {
        x: stage.x,
        y: stage.y,
        scaling: stage.scaleX
    };
    ////console.log(stageInfoBeforeStep)

    updateProperties();
}

function closeStep(all) {
    var a, item;


    hideNavigation();

    backgroundClose();

    $.each(features, function(t, customFunction) {
        customFunction.close(stepsCount);
    });

    //
    if (stepsCount > 0) {
        steps[stepsCount].object.addEventListener("click", stepFunctions);
        steps[stepsCount].object.cursor = 'pointer';
    }

    if (all === false) {
        steps[stepsCount].object.visible = false;
        return;
    }

    for (a = 0; a < exportRoot.getNumChildren(); a++) {
        item = exportRoot.getChildAt(a);

        if (item.contains === undefined)
            continue;

        item.visible = true;
    }


    updateProperties();

    if (all === true) {
        if (stageInfoBeforeStep) {
            if (stageInfoBeforeStep.scaling == stageInfo.scaling) {
                setTimeout(function() {
                    stage.x = stageInfoBeforeStep.x;
                    stage.y = stageInfoBeforeStep.y;
                    stage.scaleX = stage.scaleY = stageInfoBeforeStep.scaling;
                    stageInfoBeforeStep = null;
                    update = true;
                }, 0);
            }
        }
    }


    update = true;
}

function updateStep2(v, pienena) {
    //console.log("")
    if (stepsCount === 0 || pienena === true) {
        v.object.scaleX = v.object.scaleY = v.scaling;
        v.object.x = v.x;
        v.object.y = v.y;
        v.object.rotation = v.rotation;

        if (v.frame && frameHighlighting === true)
            v.frame.visible = true;
    } else {
        var ps = parseFloat($('#buttons').attr('scaling') || 1);
        if (v.info.iso === true)
            v.object.scaleX = v.object.scaleY = Math.min(max_scale_ratio, Math.min((canvasProperties.width - 110 * ps) / v.width, (canvasProperties.height) / v.height) / stage.scaleX);
        else
            v.object.scaleX = v.object.scaleY = Math.min(max_scale_ratio, Math.min((canvasProperties.width - 110 * ps) / v.width, (canvasProperties.height - 20 * stage.scaleX - 60 * ps) / v.height) / stage.scaleX);
        v.object.x = middlePoint.x - (v.width * v.object.scaleX) / 2;
        v.object.y = Math.min(80, middlePoint.y - (v.height * v.object.scaleY) / 2 - 20);
        v.object.rotation = 0;

        v.isoX = v.object.x;
        v.isoY = v.object.y;

        if (v.frame)
            v.frame.visible = false;
    }
    update = true;
}

function backgroundFormat() {
    var item = steps[stepsCount].object;
    if (item['background']) {
        if (item['background'].timeline) {
            item['background'].gotoAndStop(1);
        }
    }
}

function backgroundClose() {
    var item = steps[stepsCount].object;
    if (item['background']) {
        if (item['background'].timeline) {
            item['background'].gotoAndStop(0);
        }
    }
}


function return_functions(e) {
    if (getVariable('backurl') != 'false') {
        var url = getVariable('backurl') + '.html?folder=' + getVariable('folder') + '&id=' + getVariable('id');
        if (typeof window.parent.extraOpen != 'undefined') {
            $('#buttons, .navigation').hide();
            window.parent.extraOpen(url);
        }
    } else {
        history.back();
        return false;
    }
}

function cacheMode(value, cacheStage, item) {
    var a, s;
    if (value) {
        if (item != undefined) {
            item.cache(-1, -1, item.virtualBounds.width + 2, item.virtualBounds.height + 2, item.scaleX * 1.5);
        } else if (cacheStage == true) {
            stage.cache(0, 0, stageProperties.width, stageProperties.height, 1);
        } else {
            for (a = 0; a < exportRoot.getNumChildren(); a++) {
                item = exportRoot.getChildAt(a);

                if (item.contains === undefined)
                    continue;

                if (item.visible === false || item.virtualBounds === undefined)
                    continue;

                item.cache(-1, -1, item.virtualBounds.width + 2, item.virtualBounds.height + 2, item.scaleX * 1.5);
            }
        }
    } else {
        stage.uncache();
        for (a = 0; a < exportRoot.getNumChildren(); a++) {
            item = exportRoot.getChildAt(a);
            item.uncache();
        }
    }


    update = true;
}

setTimeout(function() {
    frequentUpdate = false;
}, 60000);

function tick(e) {
    if (frequentUpdate) {
        update = true;
    }
    if (update || createjs.Tween.hasActiveTweens()) {
        update = false;
        stage.update(e);
    }
}

function updateProperties(myProperties) {
    if (canvas == undefined || stage == undefined) {
        setTimeout(updateProperties, 100);
        return;
    }


    canvasProperties = screenProperties();
    if (myProperties != undefined) {
        if (myProperties.width != undefined)
            canvasProperties = myProperties;
    }

    var context = canvas.getContext('2d');
    var devicePixelRatio = window.devicePixelRatio || 1,
        backingStoreRatio = context.webkitBackingStorePixelRatio ||
        context.mozBackingStorePixelRatio ||
        context.msBackingStorePixelRatio ||
        context.oBackingStorePixelRatio ||
        context.backingStorePixelRatio || 1;
    stageInfo.ratio = devicePixelRatio / backingStoreRatio;
    canvasProperties.width *= stageInfo.ratio;
    canvasProperties.height *= stageInfo.ratio;

    canvas.width = canvasProperties.width;
    canvas.height = canvasProperties.height;
    canvas.style.width = canvasProperties.width / stageInfo.ratio + 'px';
    canvas.style.height = canvasProperties.height / stageInfo.ratio + 'px';

    if (canvasProperties.width > canvasProperties.height || stepsCount > 0) {
        if (position != 'horizontal' && stepsCount === 0)
            stageInfoBeforeStep = null;

        if (stepsCount === 0)
            position = 'horizontal';

        stageInfo.scaling = Math.min(canvasProperties.width / stageProperties.width, canvasProperties.height / stageProperties.height);

        stageInfo.x = (canvasProperties.width - stageProperties.width * stageInfo.scaling) / 2;
        stageInfo.y = (canvasProperties.height - stageProperties.height * stageInfo.scaling) / 2;
    } else {
        if (position != 'topright' && stepsCount === 0)
            stageInfoBeforeStep = null;

        if (stepsCount === 0)
            position = 'topright';

        stageInfo.scaling = Math.min(canvasProperties.width / (stageProperties.width / 2), canvasProperties.height / stageProperties.height);

        if (pageLRM == 1) {
            stageInfo.x = -(stageProperties.width - spreadProperties.width) * stageInfo.scaling / 4 + (canvasProperties.width - stageProperties.width * stageInfo.scaling / 2) / 2 - spreadProperties.width / 4 * stageInfo.scaling;
            stageInfo.y = (canvasProperties.height - stageProperties.height * stageInfo.scaling) / 2;
        } else {
            if (pageCount == null)
                pageCount = 1;

            if (pageCount == 1) {
                stageInfo.x = 0;
                stageInfo.y = (canvasProperties.height - stageProperties.height * stageInfo.scaling) / 2;
            } else {
                stageInfo.x = canvasProperties.width - stageProperties.width * stageInfo.scaling;
                stageInfo.y = (canvasProperties.height - stageProperties.height * stageInfo.scaling) / 2;
            }
        }
    }

    stage.x = stageInfo.x;
    stage.y = stageInfo.y;
    stage.scaleX = stage.scaleY = stageInfo.scaling;

    var m = screenProperties();
    var scaling = m.height / 580;

    m.width /= scaling;
    m.height /= scaling;

    $('#buttons').attr('scaling', scaling);
    $('#buttons').css({
        'font-size': 15 * scaling + 'px'
    });
    $('.navigation').css({
        'font-size': 20 * scaling + 'px'
    });

    $('a.button').css({
        'font-size': 15 * scaling + 'px'
    });
    $('a.btn').css({
        'font-size': 20 * scaling + 'px'
    });

    if (steps) {
        for (a = 1; a < steps.length; a++) {
            updateStep2(steps[a]);
        }
    }


    update = true;
}

function screenProperties() {
    if (window.parent != this) {
        if (typeof window.parent.iframeProperties === 'function')
            return window.parent.iframeProperties(window);
    }


    var w = window.innerWidth ? window.innerWidth : $(window).width();
    var h = window.innerHeight ? window.innerHeight : $(window).height();

    return {
        width: w,
        height: h
    };
}



function score(ass_arr) {
    var points = [];
    $.each(ass_arr, function(val, key) {
        points.push(key);
    });
    return points;
}

function getDistance(p1, p2) {
    var xs = 0;
    var ys = 0;

    xs = p2.x - p1.x;
    xs = xs * xs;

    ys = p2.y - p1.y;
    ys = ys * ys;
    //d=sqrt((x2-x1)^2+(y2-y1)^2);
    return Math.sqrt(xs + ys);
}

function getVariable(str) {
    var a, tempArr = window.location.href.split("?");
    if (tempArr.length == 1)
        return 'false';
    tempArr = tempArr[1].split("&");
    for (a = 0; a < tempArr.length; a++) {
        if (tempArr[a].indexOf(str + "=") > -1)
            return tempArr[a].split(str + "=")[1];
    }
    return 'false';
}

function debug(str) {
    if (debugJS !== true)
        return;

    var t = $('#trace')[0];
    if (t === undefined) {
        t = document.createElement('div');
        $(t).attr('id', 'trace');
        $('body').append(t);
        $(t).css({
            'color': '#000000',
            'position': 'absolute',
            'left': '0',
            'top': '0'
        });
    }
    $(t).html(str.toString());
}


function NoClickDelay(el) {
    this.element = typeof el == 'object' ? el : document.getElementById(el);
    if (window.Touch)
        this.element.addEventListener('touchstart', this, false);
}
NoClickDelay.prototype = {
    handleEvent: function(e) {
        switch (e.type) {
            case 'touchstart':
                this.onTouchStart(e);
                break;
            case 'touchmove':
                this.onTouchMove(e);
                break;
            case 'touchend':
                this.onTouchEnd(e);
                break;
        }
    },
    onTouchStart: function(e) {
        e.preventDefault();
        this.moved = false;
        this.x = e.targetTouches[0].clientX;
        this.y = e.targetTouches[0].clientY;

        this.theTarget = document.elementFromPoint(this.x, this.y);
        if (this.theTarget.nodeType == 3) this.theTarget = theTarget.parentNode;
        this.theTarget.className += ' hover';
        this.element.addEventListener('touchmove', this, false);
        this.element.addEventListener('touchend', this, false);

        var theEvent = document.createEvent('MouseEvents');
        theEvent.initEvent('mousedown', true, true);
        this.theTarget.dispatchEvent(theEvent);
    },
    onTouchMove: function(e) {
        var x = e.targetTouches[0].clientX;
        var y = e.targetTouches[0].clientY;
        if (Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2)) > 50) {
            this.moved = true;
            this.theTarget.className = this.theTarget.className.replace(/ hover/gi, '');
        } else {
            if (this.moved == true) {
                this.moved = false;
                this.theTarget.className += ' hover';
            }
        }
    },
    onTouchEnd: function(e) {
        this.element.removeEventListener('touchmove', this, false);
        this.element.removeEventListener('touchend', this, false);
        if (!this.moved && this.theTarget) {
            this.theTarget.className = this.theTarget.className.replace(/ hover/gi, '');
            var theEvent = document.createEvent('MouseEvents');
            theEvent.initEvent('click', true, true);
            this.theTarget.dispatchEvent(theEvent);

            theEvent = document.createEvent('MouseEvents');
            theEvent.initEvent('mouseup', true, true);
            this.theTarget.dispatchEvent(theEvent);
        }
        this.theTarget = undefined;
    }
};
NoClickDelay.prototype.offset = function() {
    var _x = _y = 0;
    var obj = document;
    if (obj.offsetParent) {
        do {
            _x += obj.offsetLeft;
            _y += obj.offsetTop;
        } while (obj = obj.offsetParent);
    }
    return {
        x: _x,
        y: _y
    };
}

$(document).ready(loadStep1);
