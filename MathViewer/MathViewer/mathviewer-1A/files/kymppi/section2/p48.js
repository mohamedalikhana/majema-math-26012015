(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p48_1.png",
            id: "p48_1"
        }, {
            src: "images/p48_2.png",
            id: "p48_2"
        }]
    };

    (lib.p48_1 = function() {
        this.initialize(img.p48_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p48_2 = function() {
        this.initialize(img.p48_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.pageTitle = new cjs.Text("Vi lär oss subtraktion", "24px 'MyriadPro-Semibold'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(93, 25.5);

        this.text_1 = new cjs.Text("16", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(44, 22.5);

        this.text_2 = new cjs.Text("48", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(38, 648);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med subtraktion 0 till 6", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#88C13F").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#88C13F").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.instance = new lib.p48_1();
        this.instance.setTransform(39, 60, 0.39, 0.40);

        this.addChild(this.pageTitle, this.shape_2, this.shape_1, this.shape, this.text_1, this.text_2, this.instance,
            this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Mira tar 1 bok. Hur många finns kvar?", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(24, -15);

        this.text_2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_2.lineHeight = 27;
        this.text_2.setTransform(4, -15);

        this.instance = new lib.p48_2();
        this.instance.setTransform(35, 0, 0.18, 0.18);

        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 105, 10);
        this.img_Rect1.setTransform(0, 2);

        this.img_Rect2 = new cjs.Shape();
        this.img_Rect2.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 105, 10);
        this.img_Rect2.setTransform(0, 114);

        this.img_Rect3 = new cjs.Shape();
        this.img_Rect3.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 105, 10);
        this.img_Rect3.setTransform(0, 228);

        this.img_Rect4 = new cjs.Shape();
        this.img_Rect4.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 105, 10);
        this.img_Rect4.setTransform(268, 2);

        this.img_Rect5 = new cjs.Shape();
        this.img_Rect5.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 105, 10);
        this.img_Rect5.setTransform(268, 114);

        this.img_Rect6 = new cjs.Shape();
        this.img_Rect6.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 105, 10);
        this.img_Rect6.setTransform(268, 228);

        var textArr = [];
        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 3; row++) {
                var temptext = null;
                temptext = new cjs.Text("finns kvar:", "17px 'Myriad Pro'");
                temptext.lineHeight = -5;
                temptext.setTransform(148 + (column * 270), 93.5 + (row * 112));
                textArr.push(temptext);
            }
        }

        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 3; row++) {
                this.tempBox = new cjs.Shape();
                this.tempBox.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
                this.tempBox.setTransform(226 + (column * 270), 88 + (row * 112.5));
                textArr.push(this.tempBox);
            }
        }

        this.addChild(this.text_1, this.text_2, this.instance, this.img_Rect1, this.img_Rect2, this.img_Rect3,
            this.img_Rect4, this.img_Rect5, this.img_Rect6, this.text_3, this.textBox1);
        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 350);

    (lib.p48 = function() {
        this.initialize();
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(289.8, 270, 1, 1, 0, 0, 0, 256.3, -20);
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
