(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p92_1.png",
            id: "p92_1"
        }]
    };

    // symbols:
    (lib.p92_1 = function() {
        this.initialize(img.p92_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("92", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(37, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Skriv talet som saknas så att svaret stämmer.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 1);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 169, 297, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(176, 0, 158, 297, 10);
        this.roundRect2.setTransform(0, 25);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(341, 0, 169, 297, 10);
        this.roundRect3.setTransform(0, 25);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f("#F1662B").beginStroke("#000000").setStrokeStyle(0.8).moveTo(74, 66).lineTo(74, 30).lineTo(99, 50).lineTo(74, 60);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f("#F1662B").beginStroke("#000000").setStrokeStyle(0.8).moveTo(247, 66).lineTo(247, 30).lineTo(272, 50).lineTo(247, 60);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.f("#F1662B").beginStroke("#000000").setStrokeStyle(0.8).moveTo(408, 66).lineTo(408, 30).lineTo(433, 50).lineTo(408, 60);
        this.Line_3.setTransform(0, 0);

        this.text1 = new cjs.Text("8", "15px 'Myriad Pro'", "#FFFFFF");
        this.text1.lineHeight = 19;
        this.text1.setTransform(76, 41);
        this.text2 = new cjs.Text("0", "15px 'Myriad Pro'", "#FFFFFF");
        this.text2.lineHeight = 19;
        this.text2.setTransform(249, 41);
        this.text3 = new cjs.Text("1", "15px 'Myriad Pro'", "#FFFFFF");
        this.text3.lineHeight = 19;
        this.text3.setTransform(410, 41);

        this.label1 = new cjs.Text("1   +   2   +", "16px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(39, 79);
        this.label2 = new cjs.Text("1   +   4   +", "16px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(39, 111);
        this.label3 = new cjs.Text("2   +   2   +", "16px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(39, 141);
        this.label4 = new cjs.Text("3   +   1   +", "16px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(39, 171);
        this.label5 = new cjs.Text("4   +   1   +", "16px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(39, 202);
        this.label6 = new cjs.Text("4   +   2   +", "16px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(39, 232);
        this.label7 = new cjs.Text("5   +   1   +", "16px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(39, 263);
        this.label22 = new cjs.Text("5   +   2   +", "16px 'Myriad Pro'");
        this.label22.lineHeight = 30;
        this.label22.setTransform(39, 293);

        this.label8 = new cjs.Text("7   –   1   –", "16px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(212, 79);
        this.label9 = new cjs.Text("7   –   3   –", "16px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(212, 111);
        this.label10 = new cjs.Text("7   –   4   –", "16px 'Myriad Pro'");
        this.label10.lineHeight = 30;
        this.label10.setTransform(212, 141);
        this.label11 = new cjs.Text("8   –   2   –", "16px 'Myriad Pro'");
        this.label11.lineHeight = 30;
        this.label11.setTransform(212, 171);
        this.label12 = new cjs.Text("8   –   4   –", "16px 'Myriad Pro'");
        this.label12.lineHeight = 30;
        this.label12.setTransform(212, 202);
        this.label13 = new cjs.Text("8   –   3   –", "16px 'Myriad Pro'");
        this.label13.lineHeight = 30;
        this.label13.setTransform(212, 232);
        this.label14 = new cjs.Text("8   –   6   –", "16px 'Myriad Pro'");
        this.label14.lineHeight = 30;
        this.label14.setTransform(212, 263);
        this.label23 = new cjs.Text("8   –   5   –", "16px 'Myriad Pro'");
        this.label23.lineHeight = 30;
        this.label23.setTransform(212, 293);

        this.label15 = new cjs.Text("7   –   1   –", "16px 'Myriad Pro'");
        this.label15.lineHeight = 30;
        this.label15.setTransform(380, 79);
        this.label16 = new cjs.Text("7   –   3   –", "16px 'Myriad Pro'");
        this.label16.lineHeight = 30;
        this.label16.setTransform(380, 111);
        this.label17 = new cjs.Text("7   –   2   –", "16px 'Myriad Pro'");
        this.label17.lineHeight = 30;
        this.label17.setTransform(380, 141);
        this.label18 = new cjs.Text("8   –   1   –", "16px 'Myriad Pro'");
        this.label18.lineHeight = 30;
        this.label18.setTransform(380, 171);
        this.label19 = new cjs.Text("8   –   3   –", "16px 'Myriad Pro'");
        this.label19.lineHeight = 30;
        this.label19.setTransform(380, 202);
        this.label20 = new cjs.Text("8   –   2   –", "16px 'Myriad Pro'");
        this.label20.lineHeight = 30;
        this.label20.setTransform(380, 232);
        this.label21 = new cjs.Text("8   –   5   –", "16px 'Myriad Pro'");
        this.label21.lineHeight = 30;
        this.label21.setTransform(380, 263);
        this.label24 = new cjs.Text("8   –   4   –", "16px 'Myriad Pro'");
        this.label24.lineHeight = 30;
        this.label24.setTransform(380, 293);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 8; row++) {
                if (column == 1) {
                    columnSpace = 0.657;
                } else if (column == 2) {
                    columnSpace = 1.310;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(116 + (columnSpace * 258), 75 + (row * 30.5), 20, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.text, this.text_1, this.Line_1, this.Line_2, this.Line_3);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7,this.label22,this.label23,this.label24);
        this.addChild(this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14);
        this.addChild(this.label15, this.label16, this.label17, this.label18, this.label19, this.label20, this.label21, this.text1, this.text2, this.text3);
        this.addChild(this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p92_1();
        this.instance.setTransform(1, 37, 0.44, 0.44);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 511.1, 248, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Jämför. Skriv >, < eller =.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 3);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 3);

        this.label1 = new cjs.Text("4   –   1", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(185, 39);
        this.label1a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label1a.lineHeight = 19;
        this.label1a.setTransform(263, 39);
        this.label2 = new cjs.Text("4   –   2", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(185, 69);
        this.label2a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label2a.lineHeight = 19;
        this.label2a.setTransform(263, 69);
        this.label3 = new cjs.Text("2   +   1", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(185, 98.5);
        this.label3a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label3a.lineHeight = 19;
        this.label3a.setTransform(263, 98.5);
        this.label4 = new cjs.Text("2   +   2", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(185, 129);
        this.label4a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label4a.lineHeight = 19;
        this.label4a.setTransform(263, 129);
        this.label5 = new cjs.Text("3   –   2", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(227, 157);
        this.label5a = new cjs.Text("3   –   1", "16px 'Myriad Pro'");
        this.label5a.lineHeight = 19;
        this.label5a.setTransform(149, 157);
        this.label6 = new cjs.Text("3   +   2", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(227, 188);
        this.label6a = new cjs.Text("3   +   1", "16px 'Myriad Pro'");
        this.label6a.lineHeight = 19;
        this.label6a.setTransform(149, 188);
        this.label7 = new cjs.Text("3   –   0", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19; 
        this.label7.setTransform(227, 218);
        this.label7a = new cjs.Text("3   +   0", "16px 'Myriad Pro'");
        this.label7a.lineHeight = 19; 
        this.label7a.setTransform(149, 218);
        this.label8 = new cjs.Text("3   –   3", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(227, 247);
        this.label8a = new cjs.Text("3   +   3", "16px 'Myriad Pro'");
        this.label8a.lineHeight = 19;
        this.label8a.setTransform(149, 247);

        this.label9 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(385, 39);
        this.label9a = new cjs.Text("5   –   1", "16px 'Myriad Pro'");
        this.label9a.lineHeight = 19;
        this.label9a.setTransform(426, 39);
        this.label10 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(385, 69);
        this.label10a = new cjs.Text("5   +   2", "16px 'Myriad Pro'");
        this.label10a.lineHeight = 19;
        this.label10a.setTransform(426, 69);
        this.label11 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(385, 98.5);
        this.label11a = new cjs.Text("6   –   1", "16px 'Myriad Pro'");
        this.label11a.lineHeight = 19;
        this.label11a.setTransform(426, 98.5);
        this.label12 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(385, 129);
        this.label12a = new cjs.Text("7   –   1", "16px 'Myriad Pro'");
        this.label12a.lineHeight = 19;
        this.label12a.setTransform(426, 129);
        this.label13 = new cjs.Text("4   –   1", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(426, 157);
        this.label13a = new cjs.Text("4   –   0", "16px 'Myriad Pro'");
        this.label13a.lineHeight = 19;
        this.label13a.setTransform(349, 157);
        this.label14 = new cjs.Text("4   +   1", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(426, 188);
        this.label14a = new cjs.Text("4   +   0", "16px 'Myriad Pro'");
        this.label14a.lineHeight = 19;
        this.label14a.setTransform(349, 188);
        this.label15 = new cjs.Text("3   +   2", "16px 'Myriad Pro'");
        this.label15.lineHeight = 19; 
        this.label15.setTransform(426, 218);
        this.label15a = new cjs.Text("4   +   2", "16px 'Myriad Pro'");
        this.label15a.lineHeight = 19; 
        this.label15a.setTransform(349, 218);
        this.label16 = new cjs.Text("5   –   2", "16px 'Myriad Pro'");
        this.label16.lineHeight = 19;
        this.label16.setTransform(426, 247);
        this.label16a = new cjs.Text("4   –   2", "16px 'Myriad Pro'");
        this.label16a.lineHeight = 19;
        this.label16a.setTransform(349, 247);

        this.label17 = new cjs.Text("Kom ihåg!", "16px 'Myriad Pro'");
        this.label17.lineHeight = 19;
        this.label17.setTransform(35, 58);
        this.label18 = new cjs.Text("2 > 1", "16px 'Myriad Pro'");
        this.label18.lineHeight = 19;
        this.label18.setTransform(52, 81);
        this.label19 = new cjs.Text("0 < 1", "16px 'Myriad Pro'");
        this.label19.lineHeight = 19;
        this.label19.setTransform(52, 102);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 1) {
                    columnSpace = 0.63;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(239 + (columnSpace * 258), 33.5 + (row * 29.5), 20, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 1) {
                    columnSpace = 0.775;
                }
                this.textbox_group2.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(203 + (columnSpace * 258), 153.5 + (row * 29.5), 20, 24);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.instance);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
        this.addChild(this.label1a, this.label2a, this.label3a, this.label4a, this.label5a, this.label6a, this.label7a, this.label8a);
        this.addChild(this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15, this.label16);
        this.addChild(this.label9a, this.label10a, this.label11a, this.label12a, this.label13a, this.label14a, this.label15a, this.label16a);
        this.addChild(this.label17, this.label18, this.label19);
        this.addChild(this.textbox_group1,this.textbox_group2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 540, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296.7, 401.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.7, 89, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
