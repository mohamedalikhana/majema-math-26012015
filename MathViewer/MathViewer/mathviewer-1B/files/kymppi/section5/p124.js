(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p124_1.png",
            id: "p124_1"
        }]
    };

    (lib.p124_1 = function() {
        this.initialize(img.p124_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("förstå talens placering i hundrarutan", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 658);

        this.text_1 = new cjs.Text("124", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(35, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.text_2 = new cjs.Text("Hundrarutan", "24px 'MyriadPro-Semibold'", "#00A3C4");
        this.text_2.setTransform(95.5, 42);

        this.text_3 = new cjs.Text("43", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(46, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.instance = new lib.p124_1();
        this.instance.setTransform(33, 0, 0.47, 0.467);

        this.addChild(this.instance, this.shape_2, this.text_3, this.text_2, this.shape_1, this.text_1, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();
        // Block-1         

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Para ihop tal och bokstav", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text("i hundrarutan.", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(17, 22);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s("#959C9D").ss(1).drawRoundRect(0, 0, 294, 377, 10);
        this.roundRect1.setTransform(0, 35);

        var numberBox_group = new cjs.Container();

        var TArr = ['100', '12', '78', '1', '56', '89', '45', '34', '67', '23'];

        var TxtlineX = [2, 36, 70, 104, 134, 167, 5, 39, 71, 103];

        var TxtlineY = [-5, -5, -5, -5, -5, -5, 45, 45, 45, 45];

        for (var i = 0; i < TxtlineX.length; i++) {
            var temp_label2;
            temp_label2 = new cjs.Text(TArr[i] + '', "16px 'Myriad Pro'", "#000000");
            temp_label2.setTransform(TxtlineX[i], TxtlineY[i]);
            numberBox_group.addChild(temp_label2);
        }

        var boxcount = 10;
        var TxtBox_X = [0, 32, 64, 96, 128, 160, 0, 32, 64, 96];
        var TxtBox_Y = [1, 1, 1, 1, 1, 1, 50, 50, 50, 50];

        for (var col = 0; col < boxcount; col++) {

            var boxfillcolor = "#FFFFFF";
            var boxborder = "#00A3C4";
            var boxheight = 23;
            var boxwidth = 26;

            var TxtBox_1 = new cjs.Shape();
            TxtBox_1.graphics.f(boxfillcolor).s(boxborder).ss(0.7).drawRoundRect(0, 0, boxwidth, boxheight, 0);
            TxtBox_1.setTransform(TxtBox_X[col], TxtBox_Y[col]);
            numberBox_group.addChild(TxtBox_1);
        }

        numberBox_group.setTransform(18, 59);



        var crosswordBox_group = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#FFFFFF").s("#00A3C4").ss(1).drawRect(0, 0, 255, 255, 0);
        this.Rect1.setTransform(0, 0);

        crosswordBox_group.addChild(this.Rect1);
        crosswordBox_group.setTransform(18, 146);

        var specialTexts = ['K', 'U', 'D', 'U', 'H', 'E', 'N', 'R', 'L', 'N'];
        var count = 0;
        var specialCount = 0;
        for (var column = 0; column < 10; column++) {
            for (var row = 0; row < 10; row++) {
                count = count + 1;
                var fillColor = '#ffffff';
                var fillText = count;
                if (row === column) {
                    fillColor = '#fff374';
                    fillText = specialTexts[specialCount]
                    specialCount = specialCount + 1;
                }
                var rectContainer = new lib.FillBoxWithText(fillText, fillColor)
                rectContainer.setTransform(25.5 * row, 25.5 * column);
                crosswordBox_group.addChild(rectContainer);
            }
        }

        var hrlineX = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        var hrlineY = [25.5, 51, 76.5, 102, 127.5, 153, 178.5, 204, 229.5];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#00A3C4").setStrokeStyle(1).moveTo(0, 0).lineTo(255, 0);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            crosswordBox_group.addChild(hrLine_1);
        }

        var vrlineX = [25.5, 51, 76.5, 102, 127.5, 153, 178.5, 204, 229.5];

        var vrlineY = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#00A3C4").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 255);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            crosswordBox_group.addChild(vrLine_1);
        }

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, numberBox_group, crosswordBox_group);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 290.3, 380);


    (lib.FillBoxWithText = function(fillText, fillColor) {
        this.initialize();
        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0, 0, 25.2, 25.2);
        var text = new cjs.Text('' + fillText, "16px 'Myriad Pro'", "#000000");
        text.setTransform(12, 20)
        text.textAlign = 'center';
        this.addChild(rect, text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Skriv talet som", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text("kommer före.", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(17, 20);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 230, 151, 10);
        this.roundRect1.setTransform(0, 33);

        var containerBox = new cjs.Container();

        var hrlineX = [20, 105, 20, 105, 20, 105, 20, 105];

        var hrlineY = [71, 71, 101, 101, 131, 131, 161, 161];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(35, 0);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            containerBox.addChild(hrLine_1);
        }

        var TArr = ['22', '31', '45', '51', '69', '71', '88', '100'];

        var TxtlineX = [58, 143, 58, 143, 58, 143, 58, 143];

        var TxtlineY = [65, 65, 95, 95, 125, 125, 155, 155];

        for (var i = 0; i < TxtlineX.length; i++) {
            var temp_label3;
            temp_label3 = new cjs.Text(TArr[i] + '', "16px 'Myriad Pro'", "#000000");
            temp_label3.setTransform(TxtlineX[i], TxtlineY[i]);
            containerBox.addChild(temp_label3);
        }
        containerBox.setTransform(0, 0);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, containerBox);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 230.3, 240);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Skriv talet som", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text("kommer efter.", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(17, 20);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 230, 157, 10);
        this.roundRect1.setTransform(0, 33);

        var containerBox = new cjs.Container();

        var hrlineX = [40, 125, 40, 125, 40, 125, 40, 125];

        var hrlineY = [76, 76, 106, 106, 136, 136, 166, 166];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(35, 0);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            containerBox.addChild(hrLine_1);
        }

        var TArr = ['20', '30', '40', '55', '60', '79', '89', '99'];

        var TxtlineX = [19, 104, 19, 104, 19, 104, 19, 104];

        var TxtlineY = [70, 70, 100, 100, 130, 130, 160, 160];

        for (var i = 0; i < TxtlineX.length; i++) {
            var temp_label3;
            temp_label3 = new cjs.Text(TArr[i] + '', "16px 'Myriad Pro'", "#000000");
            temp_label3.setTransform(TxtlineX[i], TxtlineY[i]);
            containerBox.addChild(temp_label3);
        }
        containerBox.setTransform(0, 0);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, containerBox);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 230.3, 235);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 275, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(601, 275, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(601, 497, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
