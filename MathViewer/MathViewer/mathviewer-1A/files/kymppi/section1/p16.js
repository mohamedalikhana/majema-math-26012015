(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 609.5,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/s16_17_1.png",
            id: "s16_17_1"
        }, {
            src: "images/s16_17_2.png",
            id: "s16_17_2"
        }, {
            src: "images/s16_17_3.png",
            id: "s16_17_3"
        }, {
            src: "images/s16_17_4.png",
            id: "s16_17_4"
        }]
    };


    // symbols:
    //Image1
    (lib.s16_17_1 = function() {
        this.initialize(img.s16_17_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 750, 86);

    //Image2
    (lib.s16_17_2 = function() {
        this.initialize(img.s16_17_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 750, 89);

    //Image3
    (lib.s16_17_3 = function() {
        this.initialize(img.s16_17_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 726, 256);

    //Image1
    (lib.s16_17_4 = function() {
        this.initialize(img.s16_17_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 724, 260);


    (lib.Symbol22 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Luku 3", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(94.5, 18.1);

        this.text_4 = new cjs.Text("16", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(39, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A5C0").s().p("Ak1CmIAAlLIIzAAQAjAAAOAcQAIAPAAAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.addChild(this.shape_2, this.shape_1, this.text_4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol7 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1.2);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape.setTransform(418.9, 267.3);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_1.setTransform(418.9, 267.3);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_2.setTransform(333.8, 267.3);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_3.setTransform(333.8, 267.3);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_4.setTransform(248.8, 267.3);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_5.setTransform(248.8, 267.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_6.setTransform(163.7, 267.3);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_7.setTransform(163.7, 267.3);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_8.setTransform(78.7, 267.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_9.setTransform(78.7, 267.3);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgT6QgcAAgcAOQg5AcAABHMAAAAkSIACASQADAWAJARQAcA5BHAAMBMFAAAIARgCQAWgDASgJQA4gdAAhHMAAAgkSQAAgcgOgdQgcg4hHAAg");
        this.shape_10.setTransform(257.4, 159.1);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.bf(img.s16_17_4, null, new cjs.Matrix2D(0.704, 0, 0, 0.704, -254.8, -92.2)).s().p("EgnzAOMIAA6zQAFg8AzgaQAdgOAcAAMBMFAAAQBHAAAcA5QANAaABAbIAAapg");
        this.shape_11.setTransform(257.4, 122.4);

        this.instance = new lib.s16_17_1();
        this.instance.setTransform(60, 210.2, 0.5, 0.5);

        this.addChild(this.instance, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 291.4);


    (lib.Symbol6 = function() {
        this.initialize();
        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.bf(img.s16_17_3, null, new cjs.Matrix2D(0.704, 0, 0, 0.704, -256, -90.6)).s().p("EgnzAN/IAA6MQAAhHA5gdQAcgOAcABMBMFAAAQBHAAAcA4QAOAcAAAdIAAaMg");
        this.shape.setTransform(257.4, 121);

        this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1.2);

        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_1.setTransform(418.9, 267.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_2.setTransform(418.9, 267.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_3.setTransform(333.8, 267.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_4.setTransform(333.8, 267.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_5.setTransform(248.8, 267.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_6.setTransform(248.8, 267.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_7.setTransform(163.7, 267.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_8.setTransform(163.7, 267.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_9.setTransform(78.7, 267.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_10.setTransform(78.7, 267.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgT7QgcAAgcAOQg5AdAABHMAAAAkTIACASQADAVAJASQAcA5BHAAMBMFAAAIARgCQAWgEASgIQA4gdAAhHMAAAgkTQAAgdgOgcQgcg5hHAAg");
        this.shape_11.setTransform(257.4, 159);

        this.instance = new lib.s16_17_2();
        this.instance.setTransform(60, 210.4, 0.5, 0.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFFFFF").s().p("EgmCAT8QhHAAgcg5QgJgSgDgVIgCgSMAAAgkTQAAhHA5gdQAcgOAcAAMBMFAAAQBHAAAcA5QAOAcAAAdMAAAAkTQAABHg4AdQgSAIgWAEIgRACg");
        this.shape_12.setTransform(257.4, 159);

        this.addChild(this.shape_12, this.instance, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.text_1, this.text, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 291.3);


    (lib.Symbol4 = function() {
        this.initialize();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 77.4);



    // stage content:
    (lib.p16 = function() {
        this.initialize();
        // Layer 1
        this.other = new lib.Symbol22();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol7();
        this.v2.setTransform(296.9, 494.4, 1, 1, 0, 0, 0, 256.6, 143.5);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(296.9, 194.6, 1, 1, 0, 0, 0, 256.6, 144.7);

        this.addChild(this.v1, this.v2, this.other);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
