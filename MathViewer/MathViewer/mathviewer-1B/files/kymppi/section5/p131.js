(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p130_1.png",
            id: "p130_1"
        }]
    };

    (lib.p130_1 = function() {
        this.initialize(img.p130_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("131", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 655);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_1.setTransform(0, 0);

        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.shape_2, this.shape_1, this.shapeArc1, this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.symRect = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Talet är", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(23, 186);

        this.text_2 = new cjs.Text("8", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_2.setTransform(94, 185);

        this.text_3 = new cjs.Text(".", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(132, 187);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 501, 190, 10);
        this.round_Rect1.setTransform(0, 17);

        var hrlineArr = [];
        var hrlineX = [26, 26, 26, 75];
        var hrlineY = [68, 109, 150, 188];

        for (var i = 0; i < hrlineX.length; i++) {
            var hrLine_1 = new cjs.Shape();
            if (i == 3) {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(58, 0);
            } else {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(450, 0);
            }
            hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
            hrlineArr.push(hrLine_1);
        }

        this.addChild(this.round_Rect1, this.text, this.text_1, this.text_2, this.text_3);

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p130_1();
        this.instance.setTransform(-5, 0, 0.47, 0.47);

        this.text = new cjs.Text("Skriv en talgåta till talet 8.", "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(48, 37);

        this.content_Rect = new lib.symRect();
        this.content_Rect.setTransform(0, 84);

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 20, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(20, 0);
        this.arrow.setTransform(463, 67, 0.3, 0.3);
        this.arrow.rotation = -50;

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(453, 0);
        this.linescale.setTransform(16, 67);
        this.addChild(this.linescale);

        for (var i = 0; i < 13; i++) {
            var height = 10;
            var colspace = 35;

            var vline = new cjs.Shape();
            vline.graphics.f("").s('#000000').ss(1).drawRect(0, 0, 1, height);
            vline.setTransform(31 + i * colspace, 57);

            this.addChild(vline, this.arrow);

            var ispace = 35;
            var number = i;
            if (number > 9) {
                ispace = 34.7;
            }
            var text = new cjs.Text(number + "", "bold 16px 'Myriad Pro'", "#000000");
            text.setTransform(27 + i * ispace, 84);
            this.addChild(text);

        };

        this.addChild(this.instance, this.content_Rect, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("Bestäm ett tal. Skriv en talgåta till talet.", "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(0, 0);

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 20, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(20, 0);
        this.arrow.setTransform(463, 30, 0.3, 0.3);
        this.arrow.rotation = -50;

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(453, 0);
        this.linescale.setTransform(16, 30);
        this.addChild(this.linescale);

        for (var i = 0; i < 51; i++) {
            var height = 5;
            var height_settransform = 25;
            var colspace = 8.45;

            if (i % 5 == 0) {
                height = 9;
                height_settransform = 21;
            }

            var vline = new cjs.Shape();
            vline.graphics.f("").s('#000000').ss(1).drawRect(0, 0, 1, height);
            vline.setTransform(31 + i * colspace, height_settransform);

            this.addChild(vline, this.arrow);

            if (i % 5 == 0) {
                var ispace = 8.46;
                var number = i;
                var textContainer = new cjs.Container();

                var text = new cjs.Text(number + "", "bold 16px 'Myriad Pro'", "#000000");
                text.textAlign = "center";
                text.setTransform(31 + i * ispace, 47);

                textContainer.addChild(text);
                this.addChild(textContainer);
            }

        };

        this.content_Rect = new lib.symRect();
        this.content_Rect.setTransform(0, 53);

        this.addChild(this.text, this.content_Rect, this.vrLine_1, this.vrLine_2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 260);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(315, 217, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(315, 550, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
