(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p112_1.png",
            id: "p112_1"
        }]
    };

    // symbols:
    (lib.p112_1 = function() {
        this.initialize(img.p112_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);




    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1

        this.text = new cjs.Text("112", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.2);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Bind ihop talen 1 till 50.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(10, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(10, 11, 515, 311, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p112_1();
        this.instance.setTransform(10.3, 17.2, 0.472, 0.46);


        this.addChild(this.text, this.text_1, this.roundRect1, this.instance);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 570.3, 310);


    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(10, 11, 515, 210, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Para ihop tal och bokstav på tallinjen.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(10, 0);

        var vrRect = new cjs.Shape();
        var ToBeAdded = [];
        var rectColspace=90
        for (var row = 0; row < 4; row++) {
            var rowSpace = row;
            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                if(column>2){rectColspace=92}else{rectColspace=90}
                vrRect.graphics.f('#ffffff').s("#00A3C4").drawRoundRect(80 + (columnSpace * rectColspace), 25 + (rowSpace * 30), 21, 23, 0);
                vrRect.setTransform(0, 0);
                ToBeAdded.push(vrRect);
            }
        }

 //draw small arrow
        var moveToX = 53
        var moveToY = 37
        var lineToX = 73
        var lineToY = 37
        var colspace = 90
        var rowspace = 30
        var txtColspace=90.1
        var tmpChar = ['O', 'K', 'M', 'T', 'P', 'O', 'B', 'O', 'Å', 'R', 'P', 'D', 'R', 'S', 'V', 'M', 'Å', 'K', 'P', 'E']
        var tmpNum = [31, 27, 32, 44, 49, 28, 33, 34, 42, 43, 38, 36, 35, 46, 41, 29, 39, 47, 50, 48]
        var i = 0
        var arrowLine = new cjs.Shape();
        for (var row = 0; row < 4; row++) {
            for (var col = 0; col < 5; col++) {
                if(col>2){colspace=92;txtColspace=92}else{colspace=90;txtColspace=90.1}
                arrowLine.graphics.s("#000000").ss(0.5).moveTo(moveToX + (col * colspace), moveToY + (row * rowspace))
                    .lineTo(lineToX + (col * colspace), lineToY + (row * rowspace))
                    .moveTo((moveToX + 16) + (col * colspace), (lineToY + 4) + (row * rowspace))
                    .lineTo(lineToX + (col * colspace), lineToY + (row * rowspace)).lineTo((moveToX + 16) + (col * colspace), (lineToY - 4) + (row * rowspace))

                var tempText = new cjs.Text(tmpChar[i], "16px 'Myriad Pro'");
                tempText.setTransform(85 + (col * txtColspace), 42 + (row * 30));
                var txtNum = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                txtNum.setTransform(30 + (col * txtColspace), 42 + (row * 30));
                ToBeAdded.push(arrowLine, tempText, txtNum)
                i++
            }
        }


        var xpos = 23
        var hrRect = new cjs.Shape();
        var rowSpace = row;
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            if (column > 14) {
                xpos = 93
            } else if (column > 10) {
                xpos = 75
            } else if (column > 8) {
                xpos = 57
            } else if (column > 2) {
                xpos = 40
            } else {
                xpos = 23
            }
            hrRect.graphics.f('#ffffff').s("#00A3C4").drawRoundRect(xpos + (columnSpace * 20.5), 160, 15, 18, 0);
            hrRect.setTransform(0, 0);
            ToBeAdded.push(hrRect);
        }
        //draw arrow line

        var lineToypos = 34
        var ypos = 146;
        var temp_hrLine1 = new cjs.Shape();
        var temp_vrLine1 = new cjs.Shape();
        for (var col = 0; col < 24; col++) {
            temp_hrLine1.graphics.s("#000000").f("#000000").ss(0.1).moveTo(23, (43 + ypos)).lineTo(505, (43 + ypos))
                .moveTo(505, (43 + ypos)).lineTo(502, (39 + ypos)).lineTo(511, (43 + ypos)).lineTo(502, (47 + ypos))
            if (col == 3 || col == 8 || col == 13 || col == 18 || col == 23) {
                lineToypos = 33
            } else {
                lineToypos = 36
            }
            temp_vrLine1.graphics.s("#000000").ss(0.5).moveTo(30 + (col * 20), (43 + ypos))
                .lineTo(30 + (col * 20), (lineToypos + ypos));
            ToBeAdded.push(temp_hrLine1, temp_vrLine1)
        }
       
        var fontColor = ""
        var bottomNum = [27, 30, 35, 40, 45, 50]

        var j = 0;
        var xpos = 20
        for (var col = 0; col < 6; col++) {
            if (col == 1 || col == 3 || col == 5) {
                fontColor = "#00ABDC"
            } else {
                fontColor = "#000000"
            }
            var txtNum = new cjs.Text(bottomNum[j], "16px 'MyriadPro-Semibold'", fontColor);
            if (col == 2) {
                xpos = 60
            } else if (col == 3) {
                xpos = 100
            } else if (col == 4) {
                xpos = 140
            } else if (col == 5) {
                xpos = 180
            } else {
                xpos = 20
            }
            txtNum.setTransform(xpos + (col * 60), 90 + (row * 30));
            var txtspecial = new cjs.Text("!", "28px 'UusiTekstausMajema'", "#6C7373");
            txtspecial.setTransform(500, 180);
            ToBeAdded.push(arrowLine, tempText, txtNum, txtspecial)
            j++
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance, this.instance1);



    }).prototype = p = new cjs.Container();
   // p.virtualBounds = new cjs.Rectangle(0, 0, 530, 280.9);
   p.virtualBounds = new cjs.Rectangle(0, 0, 553.3, 210);

    // stage content: 513.3, 340
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(287, 122, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(287, 452, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
