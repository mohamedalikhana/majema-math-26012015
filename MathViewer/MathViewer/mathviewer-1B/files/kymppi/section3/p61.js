(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p61_1.png",
            id: "p61_1"
        }, {
            src: "images/p61_2.png",
            id: "p61_2"
        },{
            src: "images/p74_3.png",
            id: "p74_3"
        }]
    };

    (lib.p61_1 = function() {
        this.initialize(img.p61_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p61_2 = function() {
        this.initialize(img.p61_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

     (lib.p74_3 = function() {
        this.initialize(img.p74_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);



    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("61", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(555, 658);


        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);


        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 20).lineTo(0, 20).moveTo(590, 30).lineTo(590, 660).moveTo(590, 660).lineTo(0, 660);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(580, 30, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(590, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);



        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 400);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p61_2();
        this.instance.setTransform(87, 40, 0.46, 0.46);

        this.text_1 = new cjs.Text("Skriv talen i talföljden. Skriv regeln.", "16px 'Myriad Pro'");
        this.text_1.setTransform(18, 0)
        var ToBeAdded = [];

         //Arrow Shape Loop
        this.shape_group2 = new cjs.Shape();
        
        this.hrLine_1 = new cjs.Shape();
        var arcYpos = 49.7
        var arrowYpos = 33.6
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 8; col++) {
                var colSpace = col;
                if (row == 1) {
                    arcYpos = 41;
                    arrowYpos = 66.6
                } 
                else if (row == 2) {
                    arcYpos = 32;
                    arrowYpos = 98.4
                } 
                else {
                    arcYpos = 49.7
                    arrowYpos = 33.3
                }      
         this.shape_group2.graphics.f("#FFFFFF").s("#000000").ss(0.4, 0, 0, 4).arc(130+(colSpace*50) , arcYpos+(row*50) , 32, 4.3, 4.99)

                this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(139 + (colSpace * 50), arrowYpos + (row * 50)).lineTo(139 + (colSpace * 50), (arrowYpos + 5) + (row * 50)).lineTo(134 + (colSpace * 50), (arrowYpos + 5) + (row * 50))

            }
        }
        this.shape_group2.setTransform(0, 0, 1, 2);
        this.hrLine_1.setTransform(0, 0);

        //draw line under the text

        this.hrLine_2 = new cjs.Shape();
         for (var row = 0; row < 3; row++) {
            var rowspace = row;
            var tempText1 = new cjs.Text("Regel", "16px 'Myriad Pro'");
            tempText1.setTransform(27 , 45+(rowspace*85));
             this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(22 , 78+(rowspace*85)).lineTo(67 , 78+(rowspace*85));
            ToBeAdded.push(tempText1)
        }
         this.hrLine_2.setTransform(0, 0);

        
         //Arrow Shape Loop
        this.shape_group3 = new cjs.Shape();
        this.hrLine_3 = new cjs.Shape();
        var arcYpos = 46.9
        var arrowYpos = 24.5

        for (var row = 0; row < 3; row++) {
            var rowSpace = row;
            this.shape_group3.graphics.f("#FFFFFF").s("#000000").ss(0.8, 0, 0, 4).arc(45 , arcYpos+ (rowSpace * 60), 32, 4.1, 5.3)

            this.hrLine_3.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(63 , arrowYpos+ (rowSpace * 83.5)).lineTo(63 , (arrowYpos + 5)+ (rowSpace * 83.5)).lineTo(58 , (arrowYpos + 5)+ (rowSpace * 83.5))

        }
        this.shape_group3.setTransform(0, 0, 1, 1.4);
        this.hrLine_3.setTransform(0, 0);

        //Set Number in Box
        var tmpval=[1,3,5,3,7,11,19,17,15]
        var i=0;
        var xpos=93
        for (var row = 0; row < 3; row++) {
    
         for (var col = 0; col < 3; col++) {
            var columnSpace = col;
            var tempText1 = new cjs.Text(tmpval[i],"36px 'UusiTekstausMajema'", "#6C7373");
            if(row==2){xpos=86}else {xpos=93}
            tempText1.setTransform(xpos + (columnSpace * 49.7), 65+(row*82));
            ToBeAdded.push(tempText1)
            i++;
        }
    }

        this.addChild(this.instance,this.text_1,  this.hrLine_1, this.hrLine_2, this.shape_group2,this.hrLine_1,this.hrLine_3, this.shape_group3);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 250);


    (lib.Symbol3 = function() {
        this.initialize();
        this.text_1 = new cjs.Text("Gör början på en talföljd.", "16px 'Myriad Pro'");
        this.text_1.setTransform(65, 10);
        this.text_2 = new cjs.Text("Låt en kamrat fortsätta och sedan skriva regeln.", "16px 'Myriad Pro'");
        this.text_2.setTransform(65, 30);

        

        this.instance1 = new lib.p74_3();
        this.instance1.setTransform(18, 0, 0.46, 0.46)

        this.instance = new lib.p61_1();
        this.instance.setTransform(87, 75, 0.46, 0.46)

        var ToBeAdded = [];

        this.shape_group2 = new cjs.Shape();
        
        this.hrLine_1 = new cjs.Shape();
        var arcYpos = 67
        var arrowYpos = 68
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 8; col++) {
                var colSpace = col;
                if (row == 1) {
                    arcYpos = 58;
                    arrowYpos = 100.6
                } 
                else if (row == 2) {
                    arcYpos = 48.6;
                    arrowYpos = 131.7
                } 
                else {
                    arcYpos = 67
                    arrowYpos = 68
                }      
         this.shape_group2.graphics.f("#FFFFFF").s("#000000").ss(0.4, 0, 0, 4).arc(130+(colSpace*50) , arcYpos+(row*50) , 32, 4.3, 4.99)

                this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(139 + (colSpace * 50), arrowYpos + (row * 50)).lineTo(139 + (colSpace * 50), (arrowYpos + 5) + (row * 50)).lineTo(134 + (colSpace * 50), (arrowYpos + 5) + (row * 50))

            }
        }
        this.shape_group2.setTransform(0, 0, 1, 2);
        this.hrLine_1.setTransform(0, 0);

        //draw line under the text

        this.hrLine_2 = new cjs.Shape();
         for (var row = 0; row < 3; row++) {
            var rowspace = row;
            var tempText1 = new cjs.Text("Regel", "16px 'Myriad Pro'");
            tempText1.setTransform(27 , 77+(rowspace*85));
             this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(22 , 110+(rowspace*85)).lineTo(67 , 110+(rowspace*85));
            ToBeAdded.push(tempText1)
        }
         this.hrLine_2.setTransform(0, 0);

        
         //Arrow Shape Loop
        this.shape_group3 = new cjs.Shape();
        this.hrLine_3 = new cjs.Shape();
        var arcYpos = 67.6
        var arrowYpos = 53.5

        for (var row = 0; row < 3; row++) {
            var rowSpace = row;
            this.shape_group3.graphics.f("#FFFFFF").s("#000000").ss(0.8, 0, 0, 4).arc(45 , arcYpos+ (rowSpace * 60), 32, 4.1, 5.3)

            this.hrLine_3.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(63 , arrowYpos+ (rowSpace * 83.5)).lineTo(63 , (arrowYpos + 5)+ (rowSpace * 83.5)).lineTo(58 , (arrowYpos + 5)+ (rowSpace * 83.5))

        }
        this.shape_group3.setTransform(0, 0, 1, 1.4);
        this.hrLine_3.setTransform(0, 0);

       
        this.addChild(this.text_1,this.text_2, this.instance,this.instance1, this.hrLine_1, this.hrLine_2, this.shape_group2,this.hrLine_1,this.hrLine_3, this.shape_group3);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 250);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290.8, 235, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(290.8, 525, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
