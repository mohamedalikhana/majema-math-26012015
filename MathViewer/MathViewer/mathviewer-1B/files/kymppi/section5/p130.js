(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p130_1.png",
            id: "p130_1"
        }, {
            src: "images/p130_2.png",
            id: "p130_2"
        }]
    };

    (lib.p130_1 = function() {
        this.initialize(img.p130_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p130_2 = function() {
        this.initialize(img.p130_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Talgåtor", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(235, 73);

        this.text_1 = new cjs.Text("45", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(40, 43);

        this.pageBottomText = new cjs.Text("ha fått arbeta med problemlösning", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(90, 658);

        this.text_2 = new cjs.Text("130", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(32, 659);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 170, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_2, this.pageBottomText, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p130_1();
        this.instance.setTransform(0, 17, 0.47, 0.47);

        this.text_1 = new cjs.Text("Vilket är talet?", "16px 'Myriad Pro'");
        this.text_1.setTransform(55, 55);

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 20, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(20, 0);
        this.arrow.setTransform(495, 82, 0.3, 0.3);
        this.arrow.rotation = -50;

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(453, 0);
        this.linescale.setTransform(47, 82);
        this.addChild(this.linescale);

        for (var i = 0; i < 13; i++) {
            var height = 10;
            var colspace = 35;

            var vline = new cjs.Shape();
            vline.graphics.f("").s('#000000').ss(1).drawRect(0, 0, 1, height);
            vline.setTransform(60 + i * colspace, 72);

            this.addChild(vline, this.arrow);

            var ispace = 35;
            var number = i;
            if (number > 9) {
                ispace = 34.7;
            }
            var text = new cjs.Text(number + "", "bold 16px 'Myriad Pro'", "#000000");
            text.setTransform(56 + i * ispace, 99);
            this.addChild(text);

        };

        this.text_2 = new cjs.Text("Talet är 1 korv!", "14px 'Myriad Pro");
        this.text_2.setTransform(250, 500);

        this.round_Rect = new cjs.Shape();
        this.round_Rect.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 200, 126, 7);
        this.round_Rect.setTransform(61, 118);

        this.round_Rect1 = this.round_Rect.clone(true);
        this.round_Rect1.setTransform(293, 118);

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 20, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(20, 0);
        this.arrow.setTransform(495, 289, 0.3, 0.3);
        this.arrow.rotation = -50;

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(453, 0);
        this.linescale.setTransform(47, 289);
        this.addChild(this.linescale);

        for (var i = 0; i < 51; i++) {
            var height = 5;
            var height_settransform = 284;
            var colspace = 8.45;

            if (i % 5 == 0) {
                height = 9;
                height_settransform = 280;
            }

            var vline = new cjs.Shape();
            vline.graphics.f("").s('#000000').ss(1).drawRect(0, 0, 1, height);
            vline.setTransform(60 + i * colspace, height_settransform);

            this.addChild(vline, this.arrow);

            if (i % 5 == 0) {
                var ispace = 8.46;
                var number = i;
                var textContainer = new cjs.Container();

                var text = new cjs.Text(number + "", "bold 16px 'Myriad Pro'", "#000000");
                text.textAlign = "center";
                text.setTransform(60 + i * ispace, 306);

                textContainer.addChild(text);
                this.addChild(textContainer);
            }

        };

        this.round_Rect2 = this.round_Rect.clone(true);
        this.round_Rect2.setTransform(61, 328);

        this.round_Rect3 = this.round_Rect.clone(true);
        this.round_Rect3.setTransform(293, 328);

        this.instance_1 = new lib.p130_2();
        this.instance_1.setTransform(39, 439, 0.47, 0.47);

        var TArr = [];

        var TxtArr = ['större än 5', 'udda', 'mindre än 9', 'Talet är', '.', 'mindre än 8', 'jämnt', 'större än 4', 'Talet är', '.',
            '2 tiotal', 'udda', 'mindre än 23', 'Talet är', '.', '3 ental', 'fler tiotal än ental', 'mindre än 50', 'Talet är', '.'
        ];

        var TxtlineX = [80, 80, 80, 80, 199, 315, 315, 315, 315, 437,
            80, 80, 80, 80, 199, 315, 315, 315, 315, 435
        ];
        var TxtlineY = [145, 172, 197, 225, 225, 145, 172, 197, 225, 225,
            357, 381, 405, 429, 429, 357, 381, 405, 429, 428
        ];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var hrlineArr = [];
        var hrlineX = [132, 370, 132, 370];
        var hrlineY = [225, 225, 429, 429];

        for (var i = 0; i < hrlineX.length; i++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(65, 0);
            hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
            hrlineArr.push(hrLine_1);
        }

        this.addChild(this.instance, this.instance, this.text_1, this.round_Rect, this.round_Rect1, this.round_Rect2,
            this.round_Rect3, this.instance_1, this.text_2);

        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }
        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 575, 560);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(302, 235, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
