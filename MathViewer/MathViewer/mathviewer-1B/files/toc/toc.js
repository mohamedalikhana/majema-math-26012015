if (window.parent == this) {
    $('html').addClass('orphan');
}
var iosVersion = iOSversion();
if (iosVersion != undefined && iosVersion[0] == 7) {
    $('html').addClass('ipad ios7');
} else if (iosVersion == undefined) {
    $('html').addClass('no-touch');
}

function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    }
}

function isTouchDevice() {
    var _b = false;
    if (typeof window.Modernizr === 'undefined')
        _b === ("ontouchstart" in window || (window.DocumentTouch && document instanceof DocumentTouch) || window.navigator.msMaxTouchPoints);
    else
        _b = window.navigator.msMaxTouchPoints || Modernizr.touch;
    return _b;
}

var fileProperties = {
    width: 1024,
    height: 650
};
var sectionCount = getVariable('section') == 'false' ? -1 : parseFloat(getVariable('section')) - 1;
var loaded = false;

function init() {
    links(pages);

    updateProperties();

    loaded = true;

    if (window.parent != this) {
        window.parent.pageloaded(window);
    }
}

function links(titletable) {
    var titles = [];

    //Info
    var info = {
        name: '',
        product: 'Innehåll',
        book: ''
    };

    searchTitles(0, titles, 0);
    titles.splice(0, 1);

    titles.push({
        n: '',
        o: 'Samtalsbild',
        color: '#8E59B2',
        childColor: '#EEE6F4',
        columnColor: '#EEE6F4',
        links: [{
            n: '',
            o: 'Ordningstal',
            t: 'samtalsbild/p152.html'
        }]
    });

    addMenus();

    function searchTitles(level, links, index) {
        var a, obj;
        for (a = index; a < titletable.length; a++) {
            obj = titletable[a];
            if (obj.a === level) {
                links.push(obj);
                if (a + 1 < titletable.length) {
                    if (titletable[a + 1].a > obj.a) {
                        obj.links = [];
                        a = searchTitles(titletable[a + 1].a, obj.links, a + 1);
                    }
                }
            } else
                return a - 1;
        }
        return titletable.length;
    }


    function addMenus() {
        var text = "<table><tr class='menu'><td></td><td colspan=4 class='menu_02'>" + info.product + "</td><td></td></tr><tr class='break'><td colspan=5></td></tr><tr><td class='column'  style='position:relative;background-color:" + titles[0].columnColor + ";'>";
        $.each(titles, function(a, objA) {
            var number1 = objA.n;
            if (number1 === undefined || number1 === '')
                number1 = '&nbsp;';

            var title1 = objA.o;
            var color = objA.color || '';
            var childColor = objA.childColor || '';
            var columnColor = objA.columnColor || '';
            var numChildColor = objA.color || '';

            if (a == 6) {
                //text += "<br>";

            } else if (a > 0 && a < 6) {
                text += "</td><td class='column' style='position:relative;background-color:" + columnColor + ";'>";
            }


            text += "<table class='column_table'>"
            text += "<tr><td class='title title" + (a + 1) + "' style='background-color:" + color + "'>";
            text += "<p class='number'>" + number1 + "</p>";
            text += "</td><td class='title title" + (a + 1) + "' style='background-color:" + color + "'>";
            text += "<p class='text'>" + title1 + "</p>";
            text += "</td></tr>";

            $.each(objA.links, function(b, objB) {

                // if(objB.o !== undefined && objB.o !== ''){
                var number2 = objB.n;
                if (number2 === undefined || number2 === '')
                    number2 = '';

                var title2 = objB.o;

                var file = objB.t;

                if (title2 == 'Lärandemål') { // for Lärandemål
                    var fileURL = objB.u;
                    var className = "trLarandemal" + number2;
                    text += "<tr class='" + className + "' style='top:93%'><td style='background-color:" + childColor + "'>";
                    text += "</td><td>";
                    if (title2 !== undefined) {
                        text += "<p class='number'><a fileUrl='" + fileURL + "' file='" + file + "' onClick='javascript:window.parent.showPopup(this);'><span style='color:" + numChildColor + "'>" + title2 + "</span></a></p>";
                    }
                    text += "</td></tr>";
                } else if (objB.t === '') {

                    text += "<tr><td style='background-color:" + childColor + "'>";
                    text += "<p class='number'><span style='color:" + numChildColor + "'>" + number2 + "</span></p>";
                    text += "</td><td style='background-color:" + childColor + "'>";
                    if (title2 !== undefined) {
                        text += "<p class='link'><span style='color:#ccc'>" + title2 + "</span></p>";
                    }
                    text += "</td></tr>";

                } else {

                    if (objB.o !== undefined && objB.o !== '') {
                        text += "<tr><td style='background-color:" + childColor + "'>";
                        text += "<p class='number' style='color:" + numChildColor + "'>" + number2 + "</p>";
                        text += "</td><td style='background-color:" + childColor + "'>";
                        text += "<p class='link'><a file=" + file + ">" + title2 + "</a></p>";
                        text += "</td></tr>";
                    } else {
                        text += "<tr><td style='background-color:" + childColor + "'>";
                        text += "</td><td style='background-color:" + childColor + "'>";
                        text += "</td></tr>";
                    }

                }
                // }
            });
            text += "</table>";
        });
        text += "</td></tr></table>";
        document.getElementById("links").innerHTML = text;

        $('#links').find('a').on('click', openFile);
    }


    function openFile(e) {
        var file = $(e.currentTarget).attr('file');

        if (file === '')
            return;

        if (file == 'handouts' || file == 'generalHandouts' || file == 'ope') {
            if (file == 'handouts')
                window.parent.extraOpen(_handouts.t, 0, _handouts.o);
            else if (file == 'generalHandouts')
                window.parent.extraOpen(_generalHandouts.t, 0, _generalHandouts.o);
            else if (file == 'ope')
                window.parent.extraOpen(_ope.t, 0, _ope.o);
        } else
            window.parent.downloadPage(file);
    }
}




if (window.parent === this) {
    window.addEventListener('orientationchange', updateProperties);
    window.onresize = updateProperties;
} else if ($(parent.window.document).find('.d2l-popup-page').length > 0) {
    $(parent.window.document).find('.d2l-popup-body').css('overflowX', 'hidden');
    $(parent.window.document).find('.d2l-popup-body').css('overflowY', 'hidden');

    //window.addEventListener('orientationchange', updateProperties);
    window.onresize = updateProperties;

    parent.window.addEventListener('orientationchange', updateProperties);
    parent.window.onresize = updateProperties;
}

function updateProperties(e) {
    if ($('#content')[0] === undefined)
        return;


    var m = screenProperties();

    $('#container').css('width', m.width + 'px');
    $('#container').css('height', m.height + 'px');

    var scaling = Math.min(m.width / fileProperties.width, m.height / fileProperties.height);
    scaling = Math.min(1.0, scaling);
    m.width /= scaling;
    m.height /= scaling;
    scaleObject($('#content')[0], scaling);
    $('#content').css('width', m.width + 'px');
    $('#content').css('height', m.height + 'px');

    window.scrollTo(0, 0);
}

function screenProperties() {
    if (window.parent != this) {
        var m;

        if (typeof window.parent.iframeProperties === 'function')
            return window.parent.iframeProperties(win);
    }

    var w = fileProperties.width;
    var h = fileProperties.height;

    if ($(parent.window.document).find('.d2l-popup-page').length > 0) {
        w = $(parent.window.document).find('.d2l-popup-body').width();
        h = $(parent.window.document).find('.d2l-popup-body').height();
    } else if ($(parent.window.document).find('.d2l-body').length > 0) {} else {
        if (document.body && document.body.offsetWidth) {
            w = document.body.offsetWidth;
            h = document.body.offsetHeight;
        }
        if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
            w = document.documentElement.offsetWidth;
            h = document.documentElement.offsetHeight;
        }
        if (window.innerWidth && window.innerHeight) {
            w = window.innerWidth;
            h = window.innerHeight;
        }
    }
    return {
        width: w,
        height: h
    };
}

function scaleObject(obj, scaling, x, y) {
    if (!obj)
        return;

    if (x == undefined)
        x = 0;
    if (y == undefined)
        y = 0;

    obj.style.transform = "scale(" + scaling + ")";
    obj.style.transformOrigin = (x + " " + y).toString();

    obj.style.webkitTransform = "scale(" + scaling + ")";
    obj.style.webkitTransformOrigin = (x + " " + y).toString();

    obj.style.MozTransform = "scale(" + scaling + ")";
    obj.style.MozTransformOrigin = (x + " " + y).toString();

    obj.style.OTransform = "scale(" + scaling + ")";
    obj.style.OTransformOrigin = (x + " " + y).toString();

    obj.style.msTransform = "scale(" + scaling + ")";
    obj.style.msTransformOrigin = (x + " " + y).toString();
}


function getVariable(str) {
    var a, tempArr = window.location.href.split("?");
    if (tempArr.length == 1)
        return 'false';
    tempArr = tempArr[1].split("&");
    for (a = 0; a < tempArr.length; a++) {
        if (tempArr[a].indexOf(str + "=") > -1)
            return tempArr[a].split(str + "=")[1];
    }
    return 'false';
}


function NoClickDelay(el) {
    this.element = typeof el == 'object' ? el : document.getElementById(el);
    if (window.Touch)
        this.element.addEventListener('touchstart', this, false);
}
NoClickDelay.prototype = {
    handleEvent: function(e) {
        switch (e.type) {
            case 'touchstart':
                this.onTouchStart(e);
                break;
            case 'touchmove':
                this.onTouchMove(e);
                break;
            case 'touchend':
                this.onTouchEnd(e);
                break;
        }
    },
    onTouchStart: function(e) {
        e.preventDefault();
        this.moved = false;
        this.x = e.targetTouches[0].clientX;
        this.y = e.targetTouches[0].clientY;

        this.theTarget = document.elementFromPoint(this.x, this.y);
        if (this.theTarget.nodeType == 3) this.theTarget = theTarget.parentNode;
        this.theTarget.className += ' hover';
        this.element.addEventListener('touchmove', this, false);
        this.element.addEventListener('touchend', this, false);

        var theEvent = document.createEvent('MouseEvents');
        theEvent.initEvent('mousedown', true, true);
        this.theTarget.dispatchEvent(theEvent);
    },
    onTouchMove: function(e) {
        var x = e.targetTouches[0].clientX;
        var y = e.targetTouches[0].clientY;
        if (Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2)) > 50) {
            this.moved = true;
            this.theTarget.className = this.theTarget.className.replace(/ hover/gi, '');
        } else {
            if (this.moved == true) {
                this.moved = false;
                this.theTarget.className += ' hover';
            }
        }
    },
    onTouchEnd: function(e) {
        this.element.removeEventListener('touchmove', this, false);
        this.element.removeEventListener('touchend', this, false);
        if (!this.moved && this.theTarget) {
            this.theTarget.className = this.theTarget.className.replace(/ hover/gi, '');
            var theEvent = document.createEvent('MouseEvents');
            theEvent.initEvent('click', true, true);
            this.theTarget.dispatchEvent(theEvent);

            theEvent = document.createEvent('MouseEvents');
            theEvent.initEvent('mouseup', true, true);
            this.theTarget.dispatchEvent(theEvent);
        }
        this.theTarget = undefined;
    }
};
NoClickDelay.prototype.offset = function() {
    var _x = _y = 0;
    var obj = document;

    if (obj.offsetParent) {
        do {
            _x += obj.offsetLeft;
            _y += obj.offsetTop;
        } while (obj = obj.offsetParent);
    }
    return {
        x: _x,
        y: _y
    };
}



$(document).ready(init);
