(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
                src: "images/s6_7_1.png",
                id: "s6_7_1"
            }, {
                src: "images/s6_7_2.png",
                id: "s6_7_2"
            },

        ]
    };

    (lib.s6_7_1 = function() {
        this.initialize(img.s6_7_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 936, 307);


    (lib.s6_7_2 = function() {
        this.initialize(img.s6_7_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 950, 603);


    (lib.s6_7_3 = function() {
        this.initialize(img.s6_7_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 964, 429);


    (lib.s6_7_35 = function() {
        this.initialize(img.s6_7_35);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 179, 146);


    (lib.Symbol20 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("8", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(40, 6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 18.4);

        this.addChild(this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 35.1);




    (lib.Symbol13 = function() {
        this.initialize();


        // Layer 1
        this.text = new cjs.Text("1 ", "bold 18px 'Myriad Pro'");
        this.text.lineHeight = 22;
        this.text.setTransform(497.2, 7.9);

        this.instance = new lib.s6_7_35();
        this.instance.setTransform(480.2, 0, 0.25, 0.25);

        this.text_1 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(481, 277);

        this.text_2 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(443.2, 277);

        this.text_3 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(401.3, 277);

        this.text_4 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(359.3, 277);

        this.text_5 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(317.4, 277);

        this.text_6 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(275.4, 277);

        this.text_7 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(223.3, 277);

        this.text_8 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(185.5, 277);

        this.text_9 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(143.6, 277);

        this.text_10 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(101.6, 277);

        this.text_11 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(59.7, 277);

        this.text_12 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(17.7, 277);

        this.text_13 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 19;
        this.text_13.setTransform(481, 182.9);

        this.text_14 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(443.3, 182.9);

        this.text_15 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(401.3, 182.9);

        this.text_16 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(359.4, 182.9);

        this.text_17 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_17.lineHeight = 19;
        this.text_17.setTransform(317.4, 182.9);

        this.text_18 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_18.lineHeight = 19;
        this.text_18.setTransform(275.5, 182.9);

        this.text_19 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_19.lineHeight = 19;
        this.text_19.setTransform(223.3, 182.9);

        this.text_20 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_20.lineHeight = 19;
        this.text_20.setTransform(185.6, 182.9);

        this.text_21 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.text_21.lineHeight = 19;
        this.text_21.setTransform(143.6, 182.9);

        this.text_22 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_22.lineHeight = 19;
        this.text_22.setTransform(101.6, 182.9);

        this.text_23 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_23.lineHeight = 19;
        this.text_23.setTransform(59.7, 182.9);

        this.text_24 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_24.lineHeight = 19;
        this.text_24.setTransform(17.7, 182.9);

        this.text_25 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_25.lineHeight = 19;
        this.text_25.setTransform(480.1, 101.5);

        this.text_26 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_26.lineHeight = 19;
        this.text_26.setTransform(430, 101.5);

        this.text_27 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_27.lineHeight = 19;
        this.text_27.setTransform(379.8, 101.5);

        this.text_28 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_28.lineHeight = 19;
        this.text_28.setTransform(329.6, 101.5);

        this.text_29 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_29.lineHeight = 19;
        this.text_29.setTransform(279.4, 101.5);

        this.text_30 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_30.lineHeight = 19;
        this.text_30.setTransform(222.5, 101.5);

        this.text_31 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_31.lineHeight = 19;
        this.text_31.setTransform(172.4, 101.5);

        this.text_32 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_32.lineHeight = 19;
        this.text_32.setTransform(122.2, 101.5);

        this.text_33 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_33.lineHeight = 19;
        this.text_33.setTransform(72, 101.5);

        this.text_34 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_34.lineHeight = 19;
        this.text_34.setTransform(21.8, 101.5);

        this.text_35 = new cjs.Text(" Kuin-ka mon-ta? Ym-py-röi lu-ku.", "16px 'Myriad Pro'");
        this.text_35.lineHeight = 19;
        this.text_35.setTransform(20.8, 38.7);

        this.text_36 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_36.lineHeight = 20;
        this.text_36.setTransform(6.5, 37.1);

        this.text_37 = new cjs.Text("Kotitehtävä", "bold 18px 'Myriad Pro'", "#FFFFFF");
        this.text_37.lineHeight = 22;
        this.text_37.setTransform(6.2, 7.9);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAmkIAANJ");
        this.shape.setTransform(258.7, 256.2);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAloIAALR");
        this.shape_1.setTransform(258.7, 170.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAkSIAAIl");
        this.shape_2.setTransform(258.7, 97.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgnNAAAMBObAAA");
        this.shape_3.setTransform(256.9, 210.2);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgnNAAAMBObAAA");
        this.shape_4.setTransform(256.9, 129.3);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#000000").s().p("EgmtABxQg4AAgVgsQgHgOgCgQIgCgOIAAgxQAAg2ArgXQAXgLAWAAMBNcAAAQA3AAAVAsQALAWAAAWIAAAxQAAA3gsAWQgOAHgQACIgNACgEgmxgBqIgJABQgNABgQAIQgpAVAAAzIAAA1IABAJQADAQAGANQAVApA0AAMBNcAAAIANgCQANgCAPgHQApgVAAgzIAAg1IgBgJQgCgPgHgOQgVgpgzAAMhNcAAAg");
        this.shape_5.setTransform(256.6, 21.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#00A5C0").s().p("EgmuABuQg1AAgVgqIgKgrIAAgxQAAg1AqgWQAWgKAUAAMBNcAAAQA1AAAWAqQAKAWAAAVIAAAxQAAA1gqAWIgrAKg");
        this.shape_6.setTransform(256.7, 21.3);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#959C9D").s().p("EgmEAWZQhJAAgdg6QgJgSgDgWIgCgSMAAAgpJQAAhJA6gdQAdgOAdAAMBMJAAAQBJAAAdA6QAOAdAAAdMAAAApJQAABIg6AeQgSAIgVAEIgTACgEgmVgWSQgWAFgRAIQg3AbAABGMAAAApJIACARQADAVAJASQAcA3BFAAMBMOAAAIANgBQAWgFAQgIQA3gbAAhGMAAAgpOIgBgMQgEgXgJgQQgcg3hFAAMhMOAAAg");
        this.shape_7.setTransform(257.9, 159);

        this.instance_1 = new lib.s6_7_3();
        this.instance_1.setTransform(16.3, 61.9, 0.5, 0.5);

        this.addChild(this.instance_1, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_37, this.text_36, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.instance, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 525, 302.3);


    (lib.Symbol6 = function() {
        this.initialize();



        this.text = new cjs.Text(" Piir-rä li-sää niin, et-tä on 10.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(14.4, 1.6);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhZAAICzAA");
        this.shape.setTransform(284.2, 252.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AAAhcIAAC5");
        this.shape_1.setTransform(283.9, 253.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhZAAICzAA");
        this.shape_2.setTransform(26, 252.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AAAhcIAAC5");
        this.shape_3.setTransform(25.7, 253.4);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhZAAICzAA");
        this.shape_4.setTransform(56.9, 220.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AAAhcIAAC5");
        this.shape_5.setTransform(56.6, 221.7);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhZAAICzAA");
        this.shape_6.setTransform(284.2, 220.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AAAhcIAAC5");
        this.shape_7.setTransform(283.9, 221.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag/BAIB/h/");
        this.shape_8.setTransform(76.5, 137);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_9.setTransform(76.8, 137.8);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag/BAIB/h/");
        this.shape_10.setTransform(310.1, 168.6);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_11.setTransform(310.4, 169.3);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag+BAIB+h/");
        this.shape_12.setTransform(49.5, 168.6);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_13.setTransform(49.8, 169.3);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag/BAIB/h/");
        this.shape_14.setTransform(310.1, 137);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_15.setTransform(310.4, 137.8);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag+BAIB+h/");
        this.shape_16.setTransform(49.5, 137);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_17.setTransform(49.8, 137.8);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag/BAIB/h/");
        this.shape_18.setTransform(283.8, 168.6);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_19.setTransform(284.2, 169.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag/BAIB/h/");
        this.shape_20.setTransform(23.2, 168.6);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_21.setTransform(23.5, 169.3);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag/BAIB/h/");
        this.shape_22.setTransform(283.8, 137);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_23.setTransform(284.2, 137.8);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("Ag/BAIB/h/");
        this.shape_24.setTransform(23.2, 137);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhBhBICDCD");
        this.shape_25.setTransform(23.5, 137.8);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AhZAAICzAA");
        this.shape_26.setTransform(26, 220.9);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#6C7373").ss(1.7, 0, 0, 4).p("AAAhcIAAC5");
        this.shape_27.setTransform(25.7, 221.7);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAgggQAfgfAAgsQAAgrgfgfQgggggsAAQgrAAgfAgQggAfAAArQAAAsAgAfQAfAgArAAg");
        this.shape_28.setTransform(312, 87.4);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAgggQAfgfAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAfQAgAgArAAg");
        this.shape_29.setTransform(53, 87.4);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAgggQAfgfAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAfQAgAgArAAg");
        this.shape_30.setTransform(282.8, 87.4);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAgggQAfgfAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAfQAgAgArAAg");
        this.shape_31.setTransform(23.8, 87.4);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAggfQAfggAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAgQAgAfArAAg");
        this.shape_32.setTransform(136.5, 56.3);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAggfQAfggAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAgQAgAfArAAg");
        this.shape_33.setTransform(311.8, 56.3);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAfgfQAgggAAgsQAAgrgggfQgfgggsAAQgrAAggAgQgfAfAAArQAAAsAfAgQAgAfArAAg");
        this.shape_34.setTransform(52.9, 56.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAgggQAfgfAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAfQAgAgArAAg");
        this.shape_35.setTransform(341.8, 86.3);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAggfQAfggAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAgQAgAfArAAg");
        this.shape_36.setTransform(341.8, 56.3);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAggfQAfggAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAgQAgAfArAAg");
        this.shape_37.setTransform(80.8, 56.3);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAggfQAfggAAgsQAAgrgfgfQgggggsAAQgrAAgfAgQggAfAAArQAAAsAgAgQAfAfArAAg");
        this.shape_38.setTransform(108.5, 56.3);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAggfQAfggAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAgQAgAfArAAg");
        this.shape_39.setTransform(282.8, 56.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#6C7373").ss(1.3, 0, 0, 4).p("AAABrQAsAAAggfQAfggAAgsQAAgrgfgfQgggggsAAQgrAAggAgQgfAfAAArQAAAsAfAgQAgAfArAAg");
        this.shape_40.setTransform(23.8, 56.3);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4l0QgcAAgcAOQg5AdAABGIAAIHIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAAoHQAAgcgOgcQgdg5hHAAg");
        this.shape_41.setTransform(385.9, 235.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4l0QgcAAgcAOQg5AcAABHIAAIHIACARQADAVAJASQAcA5BHAAMAjxAAAIASgCQAVgEASgIQA5gdAAhGIAAoHQAAgcgOgcQgdg5hHAAg");
        this.shape_42.setTransform(385.9, 152);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4l0QgcAAgcAOQg5AcAABHIAAIHIACARQADAVAJASQAcA5BHAAMAjxAAAIASgCQAVgEASgIQA5gdAAhGIAAoHQAAgcgOgcQgdg5hHAAg");
        this.shape_43.setTransform(385.9, 69.5);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4l0QgcAAgdAOQg4AdAABGIAAIHIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAAoHQAAgcgNgcQgdg5hHAAg");
        this.shape_44.setTransform(127.9, 235.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4l0QgcAAgdAOQg4AcAABHIAAIHIABARQAEAVAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhGIAAoHQAAgcgNgcQgdg5hHAAg");
        this.shape_45.setTransform(127.9, 152);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4l0QgcAAgdAOQg4AcAABHIAAIHIABARQAEAVAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhGIAAoHQAAgcgNgcQgdg5hHAAg");
        this.shape_46.setTransform(127.9, 69.5);

        this.addChild(this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.8, 274.2);


    (lib.Symbol2 = function() {
        this.initialize();



        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgcAOAAAjIAACBIAHAcQAOAcAjAAICXAAQAOAAAOgHQAcgOAAgjIAAiBQAAgNgGgOQgPgdgjAAg");
        this.shape.setTransform(18.6, 275.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgdAPAAAjIAACAIAHAcQAOAcAkAAICWAAQAPAAAOgHQAcgOAAgjIAAiAQAAgOgHgOQgOgdgkAAg");
        this.shape_1.setTransform(278.4, 162.4);

        this.text = new cjs.Text("10", "20px 'Myriad Pro'");
        this.text.lineHeight = 30;
        this.text.setTransform(265, 265.4);

        this.text_1 = new cjs.Text("6", "20px 'Myriad Pro'");
        this.text_1.lineHeight = 30;
        this.text_1.setTransform(11.7, 265.4);

        this.text_2 = new cjs.Text("9", "20px 'Myriad Pro'");
        this.text_2.lineHeight = 30;
        this.text_2.setTransform(271.8, 152.1);

        this.text_3 = new cjs.Text("8", "20px 'Myriad Pro'");
        this.text_3.lineHeight = 30;
        this.text_3.setTransform(11.7, 152.1);

        this.text_4 = new cjs.Text("6", "20px 'Myriad Pro'");
        this.text_4.lineHeight = 30;
        this.text_4.setTransform(270.3, 39.4);

        this.text_5 = new cjs.Text("7", "20px 'Myriad Pro'");
        this.text_5.lineHeight = 30;
        this.text_5.setTransform(11.7, 39.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgdAOAAAjIAACAIAHAdQAOAcAkAAICWAAQAPAAAOgHQAcgOAAgkIAAiAQAAgNgHgOQgOgdgkAAg");
        this.shape_2.setTransform(278.4, 49.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgdAOAAAjIAACBIAHAcQAOAcAkAAICWAAQAPAAAOgHQAcgOAAgjIAAiBQAAgNgHgOQgOgdgkAAg");
        this.shape_3.setTransform(278.4, 275.4);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgcAOAAAjIAACAIAHAdQAOAcAjAAICXAAQAOAAAOgHQAcgOAAgkIAAiAQAAgNgGgOQgPgdgjAAg");
        this.shape_4.setTransform(18.6, 49.8);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgcAPAAAjIAACAIAHAcQAOAcAjAAICXAAQAOAAAOgHQAcgOAAgjIAAiAQAAgOgGgOQgPgdgjAAg");
        this.shape_5.setTransform(18.6, 162.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgdAAgcAOQg5AcAABHIAALRIACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_6.setTransform(383.9, 47.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACASQAEAVAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_7.setTransform(383.9, 160.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nrQgHAAgLACQgVAEgSAIQg5AdAABHIAALzIACARQAEAWAIASQAdA4BHAAMAjxAAAQAcAAAdgOQA4gcAAhHIAArzQAAgdgNgcQgdg5hHAAg");
        this.shape_8.setTransform(383.9, 274.8);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAArRQAAgcgPgcQgcg5hHAAg");
        this.shape_9.setTransform(125.9, 47.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACASQAEAVAIASQAdA5BHAAMAjxAAAQAdAAAcgOQA5gdAAhHIAArRQAAgcgPgcQgcg5hHAAg");
        this.shape_10.setTransform(125.9, 160.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nrQgHAAgLACQgVAEgSAIQg5AdAABHIAALzIACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAArzQAAgdgPgcQgcg5hHAAg");
        this.shape_11.setTransform(125.9, 274.8);

        this.instance = new lib.s6_7_2();
        this.instance.setTransform(31.9, 10.6, 0.5, 0.5);

        this.addChild(this.instance, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.8, 326);


    (lib.Symbol1 = function() {
        this.initialize();



        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#6C7373").ss(1.5, 0, 0, 4).p("AmxjUQgyAAgyAZQhkAyAAB9IADA5QAGAlAQAfQAyBkB9AAINjAAIAfgDQAmgGAfgQQBkgyAAh9IAAgZQAAgygZgyQgyhkh9AAg");
        this.shape.setTransform(102, 79.7);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgcAPAAAjIAACAIAHAcQAOAcAjAAICXAAQAOAAAOgHQAcgOAAgjIAAiAQAAgOgGgOQgPgdgjAAg");
        this.shape_1.setTransform(21.1, 81.9);

        this.text = new cjs.Text("5", "20px 'Myriad Pro'");
        this.text.lineHeight = 30;
        this.text.setTransform(272.8, 184.9);

        this.text_1 = new cjs.Text("2", "20px 'Myriad Pro'");
        this.text_1.lineHeight = 30;
        this.text_1.setTransform(14.2, 184.9);

        this.text_2 = new cjs.Text("4", "20px 'Myriad Pro'");
        this.text_2.lineHeight = 30;
        this.text_2.setTransform(272.1, 72.9);

        this.text_3 = new cjs.Text("3", "20px 'Myriad Pro'");
        this.text_3.lineHeight = 30;
        this.text_3.setTransform(14.2, 72.9);

        this.text_4 = new cjs.Text(" Ringa in antalet.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(14.4, 5.2);

        this.text_5 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_5.lineHeight = 20;
        this.text_5.setTransform(0, 4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4QgOAAgOAHQgdAOAAAkIAACAIAHAcQAOAcAkAAICWAAIAdgHQAcgOAAgjIAAiAQAAgOgHgPQgOgcgkAAg");
        this.shape_2.setTransform(280.9, 195.4);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4QgOAAgOAHQgdAPAAAjIAACAIAHAcQAOAcAkAAICWAAIAdgHQAcgOAAgjIAAiAQAAgOgHgOQgOgdgkAAg");
        this.shape_3.setTransform(280.9, 81.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AhLh4IgcAHQgcAOAAAkIAACAIAHAcQAOAcAjAAICXAAQAOAAAOgHQAcgOAAgjIAAiAQAAgOgGgPQgPgcgjAAg");
        this.shape_4.setTransform(21.1, 195.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgdAAgcAOQg5AcAABHIAALRIACASQAEAVAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_5.setTransform(386.4, 193.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgdAAgcAOQg5AcAABHIAALRIACASQAEAVAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_6.setTransform(386.4, 79.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgHAAgLACQgVADgSAJQg5AcAABHIAALRIACASQAEAVAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAArRQAAgcgPgcQgcg5hHAAg");
        this.shape_7.setTransform(128.4, 193.1);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgdAAgcAOQg5AcAABHIAALRIACASQAEAVAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA5gcAAhHIAArRQAAgcgPgcQgcg5hHAAg");
        this.shape_8.setTransform(128.4, 79.6);

        this.instance = new lib.s6_7_1();
        this.instance.setTransform(43.5, 61.2, 0.5, 0.5);

        this.addChild(this.instance, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 241.6);


    // stage content:
    (lib.s6_7 = function() {
        this.initialize();

        // Layer 1
        this.muu = new lib.Symbol20();
        this.muu.setTransform(609.5, 659.9, 1, 1, 0, 0, 0, 609.5, 17.5);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(297.8, 472.4, 1, 1, 0, 0, 0, 254.8, 162);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 171, 1, 1, 0, 0, 0, 256.3, 120.4);

        this.addChild(this.v1, this.v2, this.muu);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 389.6, 1218.9, 626.9);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
