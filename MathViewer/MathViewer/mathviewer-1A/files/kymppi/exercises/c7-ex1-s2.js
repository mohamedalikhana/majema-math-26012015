(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };


    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.BallStack = function(ball, count, scaleX, scaleY, ratio) {

        this.initialize();
        ballCloned = ball.clone();
        ballCloned.setTransform(0, 0, scaleX, scaleY);
        var balls = [];
        for (var i = 0; i < count; i++) {
            var ballTemp = ballCloned.clone(true);
            ballTemp.setTransform(0, 0, 2, 2);
            this.addChild(ballTemp);
            balls.push(ballTemp);
        };
        //1000
        switch (count) {
            case 1:
                balls[0].setTransform(50 * ratio, 50 * ratio, scaleX, scaleY);
                break;
            case 2:
                balls[0].setTransform(20 * ratio, 50 * ratio, scaleX, scaleY);
                balls[1].setTransform(80 * ratio, 50 * ratio, scaleX, scaleY);
                break;
            case 3:
                balls[0].setTransform(20 * ratio, 50 * ratio, scaleX, scaleY);
                balls[1].setTransform(80 * ratio, 50 * ratio, scaleX, scaleY);
                balls[2].setTransform(50 * ratio, 0 * ratio, scaleX, scaleY);
                break;
            case 4:
                balls[0].setTransform(0 * ratio, 0 * ratio, scaleX, scaleY);
                balls[1].setTransform(100 * ratio, 0 * ratio, scaleX, scaleY);
                balls[2].setTransform(0 * ratio, 100 * ratio, scaleX, scaleY);
                balls[3].setTransform(100 * ratio, 100 * ratio, scaleX, scaleY);
                break;
            case 5:
                balls[0].setTransform(0, 0, scaleX, scaleY);
                balls[1].setTransform(100 * ratio, 0, scaleX, scaleY);
                balls[2].setTransform(0, 100 * ratio, scaleX, scaleY);
                balls[3].setTransform(50 * ratio, 50 * ratio, scaleX, scaleY);
                balls[4].setTransform(100 * ratio, 100 * ratio, scaleX, scaleY);
                break;
            case 6:
                balls[0].setTransform(10 * ratio, 0 * ratio, scaleX, scaleY);
                balls[1].setTransform(10 * ratio, 50 * ratio, scaleX, scaleY);
                balls[2].setTransform(10 * ratio, 100 * ratio, scaleX, scaleY);
                balls[3].setTransform(90 * ratio, 0 * ratio, scaleX, scaleY);
                balls[4].setTransform(90 * ratio, 50 * ratio, scaleX, scaleY);
                balls[5].setTransform(90 * ratio, 100 * ratio, scaleX, scaleY);

                break;
        }
        //this.setTransform(0,0,0.25,0.25);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.CommentText = function() {
        this.initialize();
        var font = "24px 'Myriad Pro'";
        this.comments = [];

        // this.comments.push("Vilken färg på bollar finns det flest av?");
        // this.comments.push("Säg högt: 3 är större än 2.");

        this.comments.push("Vilken färg på bollar finns det minst av?");
        this.comments.push("Säg högt: 2 är mindre än 3.");


        this.hintText = new cjs.Text("", font, "#00B4EA")
        this.hintText.textAlign = 'center';
        this.hintText.lineHeight = 19;

        this.addChild(this.hintText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.Stage0 = function() {
        this.initialize();
        this.questionText = new cjs.Text("Hur många bollar?", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -40);
        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();
        var ballScaleX = 3,
            ballScaleY = 3;
        this.balls = new lib.BallStack(this.blueBall, 2, ballScaleX, ballScaleY, 7);
        this.balls.setTransform(335, 40, 0.30, 0.30, 0, 0, 0, 0, 0);
        this.balls1 = new lib.BallStack(this.redBall, 3, ballScaleX, ballScaleY, 7);
        this.balls1.setTransform(335 + 350, 40, 0.30, 0.30, 0, 0, 0, 0, 0);
        this.answers = new lib.answers();
        this.answers.setTransform(480, 285);
        this.textFields = new lib.textFields(100);
        this.textFields.setTransform(460, 280, 3.5, 3.5, 0, 0, 0, 0, 0);
        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText.text = this.commentTexts.comments[0];
        this.commentTexts.setTransform(663, 450);
        this.textBeneath = new lib.textBeneath();
        this.textBeneath.setTransform(548, 380);
        this.addChild(this.balls, this.balls1, this.textFields, this.questionText, this.commentTexts, this.answers, this.textBeneath);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.questionText.text = "Hur många bollar?";
        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.questionText.text = "Hur många bollar?";
        this.stage0.answers.text_1.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.questionText.text = "Hur många bollar?";
        this.stage0.answers.text_1.visible = true;
        this.stage0.answers.text_3.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

   

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.questionText.text = "Hur många bollar?";
        this.stage0.answers.text_1.visible = true;
        this.stage0.answers.text_3.visible = true;
        this.stage0.answers.text_2.visible = false;
this.stage0.textBeneath.text0.text = this.stage0.textBeneath.texts[0];
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.textBeneath.text0,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.questionText.text = "Hur många bollar?";
        this.stage0.answers.text_1.visible = true;
        this.stage0.answers.text_3.visible = true;
        this.stage0.answers.text_2.visible = true;
        this.stage0.textBeneath.text0.text = this.stage0.textBeneath.texts[0];
        this.stage0.commentTexts.hintText.text = this.stage0.commentTexts.comments[1];
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.commentTexts.hintText,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 3000,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Större än och mindre än", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("7", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.textBeneath = function() {
        this.initialize();
        this.texts = [];
        //this.texts.push("3 är större än 2.");
        this.texts.push("2 är mindre än 3.");
        var font = "36px 'Myriad Pro'";
        this.text0 = new cjs.Text(" ", font);
        this.text0.lineHeight = 19;
        this.text0.setTransform(0, 0);
        this.addChild(this.text0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    (lib.textFields = function(space) {
        this.initialize();

        this.textbox_1 = new cjs.Shape();
        this.textbox_1.graphics.f('#ffffff').s('#000000').ss(0.7).drawRoundRect(0, 0, 20, 20, 2);
        this.textbox_1.setTransform(space, 0);


        this.textbox_3 = new cjs.Shape();
        this.textbox_3.graphics.f('#ffffff').s('#000000').ss(0.7).drawRoundRect(0, 0, 20, 20, 2);
        this.textbox_3.setTransform(0, 0);

        this.addChild(this.textbox_3, this.textbox_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.answers = function() {
        this.initialize();
        var font = "60px 'Myriad Pro'";
        // this.text_1 = new cjs.Text("3", font);
        // this.text_1.visible = false;
        // this.text_1.lineHeight = 19;
        // this.text_1.setTransform(0, 0);

        // this.text_2 = new cjs.Text(">", font);
        // this.text_2.visible = false;
        // this.text_2.lineHeight = 19;
        // this.text_2.setTransform(175, 0);

        // this.text_3 = new cjs.Text("2", font);
        // this.text_3.visible = false;
        // this.text_3.lineHeight = 19;
        // this.text_3.setTransform(350, 0);

        this.text_1 = new cjs.Text("2", font);
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("<", font);
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(175, 0);

        this.text_3 = new cjs.Text("3", font);
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(350, 0);
        this.addChild(this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        
        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
