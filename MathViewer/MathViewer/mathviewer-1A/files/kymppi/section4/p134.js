(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p134_1.png",
            id: "p134_1"
        }, {
            src: "images/p134_2.png",
            id: "p134_2"
        }]
    };

    // symbols:

    (lib.p134_2 = function() {
        this.initialize(img.p134_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p134_1 = function() {
        this.initialize(img.p134_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("134", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(33.4, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Hitta vägen med rätt svar.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(-1, 0);

        this.instance = new lib.p134_1();
        this.instance.setTransform(22, 25, 0.312, 0.31);

        this.label1 = new cjs.Text("10 –3", "16px 'Myriad Pro'");
        this.label1.lineHeight = 21;
        this.label1.setTransform(47, 43);
        this.label2 = new cjs.Text("9 – 4", "16px 'Myriad Pro'");
        this.label2.lineHeight = 21;
        this.label2.setTransform(128, 43);
        this.label3 = new cjs.Text("4 + 6", "16px 'Myriad Pro'");
        this.label3.lineHeight = 21;
        this.label3.setTransform(208, 43);
        this.label4 = new cjs.Text("8 – 5", "16px 'Myriad Pro'");
        this.label4.lineHeight = 21;
        this.label4.setTransform(290, 43);
        this.label5 = new cjs.Text("10 + 0", "16px 'Myriad Pro'");
        this.label5.lineHeight = 21;
        this.label5.setTransform(364, 43);

        this.label6 = new cjs.Text("7 – 3", "16px 'Myriad Pro'");
        this.label6.lineHeight = 21;
        this.label6.setTransform(48, 89);
        this.label7 = new cjs.Text("3 + 7", "16px 'Myriad Pro'");
        this.label7.lineHeight = 21;
        this.label7.setTransform(128, 89);
        this.label8 = new cjs.Text("6 – 3", "16px 'Myriad Pro'");
        this.label8.lineHeight = 21;
        this.label8.setTransform(208, 89);
        this.label9 = new cjs.Text("2 + 8", "16px 'Myriad Pro'");
        this.label9.lineHeight = 21;
        this.label9.setTransform(290, 89);
        this.label10 = new cjs.Text("3 – 0", "16px 'Myriad Pro'");
        this.label10.lineHeight = 21;
        this.label10.setTransform(368, 89);

        this.label11 = new cjs.Text("1 + 9", "16px 'Myriad Pro'");
        this.label11.lineHeight = 21;
        this.label11.setTransform(48, 133);
        this.label12 = new cjs.Text("10 – 7", "16px 'Myriad Pro'");
        this.label12.lineHeight = 21;
        this.label12.setTransform(124, 133);
        this.label13 = new cjs.Text("10 – 8", "16px 'Myriad Pro'");
        this.label13.lineHeight = 21;
        this.label13.setTransform(204, 133);
        this.label14 = new cjs.Text("8 – 1", "16px 'Myriad Pro'");
        this.label14.lineHeight = 21;
        this.label14.setTransform(290, 133);
        this.label15 = new cjs.Text("9 – 3", "16px 'Myriad Pro'");
        this.label15.lineHeight = 21;
        this.label15.setTransform(368, 133);

        this.label16 = new cjs.Text("2 + 5", "16px 'Myriad Pro'");
        this.label16.lineHeight = 21;
        this.label16.setTransform(48, 178);
        this.label17 = new cjs.Text("5 + 5", "16px 'Myriad Pro'");
        this.label17.lineHeight = 21;
        this.label17.setTransform(128, 178);
        this.label18 = new cjs.Text("4 – 1", "16px 'Myriad Pro'");
        this.label18.lineHeight = 21;
        this.label18.setTransform(208, 178);
        this.label19 = new cjs.Text("9 – 7", "16px 'Myriad Pro'");
        this.label19.lineHeight = 21;
        this.label19.setTransform(290, 178);
        this.label20 = new cjs.Text("5 + 3", "16px 'Myriad Pro'");
        this.label20.lineHeight = 21;
        this.label20.setTransform(368, 178);

        this.label21 = new cjs.Text("8 – 5", "16px 'Myriad Pro'");
        this.label21.lineHeight = 21;
        this.label21.setTransform(48, 223);
        this.label22 = new cjs.Text("12 – 1", "16px 'Myriad Pro'");
        this.label22.lineHeight = 21;
        this.label22.setTransform(124, 223);
        this.label23 = new cjs.Text("12 – 2", "16px 'Myriad Pro'");
        this.label23.lineHeight = 21;
        this.label23.setTransform(204, 223);
        this.label24 = new cjs.Text("5 – 2", "16px 'Myriad Pro'");
        this.label24.lineHeight = 21;
        this.label24.setTransform(290, 223);
        this.label25 = new cjs.Text("8 + 2", "16px 'Myriad Pro'");
        this.label25.lineHeight = 21;
        this.label25.setTransform(368, 223);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#ffffff').drawRoundRect(0, 0, 460, 285, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label6, this.label7, this.label8, this.label9, this.label10);
        this.addChild(this.label11, this.label12, this.label13, this.label14, this.label15);
        this.addChild(this.label16, this.label17, this.label18, this.label19, this.label20);
        this.addChild(this.label21, this.label22, this.label23, this.label24, this.label25);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p134_2();
        this.instance.setTransform(180, 190, 0.4, 0.4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 5, 515, 237, 10);
        this.roundRect1.setTransform(0, 20);

        this.text = new cjs.Text("Skriv + eller – så att det stämmer.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);


        var text_label = [];
        var text_1 = ['0', '10', '6', '4'];
        var text_2 = ['12', '8', '0', '4'];

        for (var label = 0; label < 4; label++) {
            var temp1 = new cjs.Text("5", "16px 'Myriad Pro'");
            temp1.lineHeight = -1;
            temp1.setTransform(40, 37 + label * 27);
            text_label.push(temp1);

            var temp2 = new cjs.Text("6", "16px 'Myriad Pro'");
            temp2.lineHeight = -1;
            temp2.setTransform(40, 154 + label * 27);
            text_label.push(temp2);

            this.textbox_group1 = new cjs.Shape();
            this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(55, 33, 18, 21);
            this.textbox_group1.setTransform(0, label * 27);
            text_label.push(this.textbox_group1);

            this.textbox_group2 = new cjs.Shape();
            this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(55, 150, 18, 21);
            this.textbox_group2.setTransform(0, label * 27);
            text_label.push(this.textbox_group2);

            var temp3 = new cjs.Text("2", "16px 'Myriad Pro'");
            temp3.lineHeight = -1;
            temp3.setTransform(80.5, 37 + label * 27);
            text_label.push(temp3);

            var temp4 = new cjs.Text("4", "16px 'Myriad Pro'");
            temp4.lineHeight = -1;
            temp4.setTransform(80.5, 154 + label * 27);
            text_label.push(temp4);

            this.textbox_group3 = new cjs.Shape();
            this.textbox_group3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(95.5, 33, 18, 21);
            this.textbox_group3.setTransform(0, label * 27);
            text_label.push(this.textbox_group3);

            this.textbox_group4 = new cjs.Shape();
            this.textbox_group4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(95.5, 150, 18, 21);
            this.textbox_group4.setTransform(0, label * 27);
            text_label.push(this.textbox_group4);

            var temp5 = new cjs.Text("3", "16px 'Myriad Pro'");
            temp5.lineHeight = -1;
            temp5.setTransform(120.5, 37 + label * 27);
            text_label.push(temp5);

            var temp6 = new cjs.Text("2", "16px 'Myriad Pro'");
            temp6.lineHeight = -1;
            temp6.setTransform(120.5, 154 + label * 27);
            text_label.push(temp6);

            var temp6 = new cjs.Text("=", "16px 'Myriad Pro'");
            temp6.lineHeight = -1;
            temp6.setTransform(136, 37 + label * 27);
            text_label.push(temp6);

            var temp7 = new cjs.Text("=", "16px 'Myriad Pro'");
            temp7.lineHeight = -1;
            temp7.setTransform(136, 154 + label * 27);
            text_label.push(temp7);

            var temp8 = new cjs.Text(text_1[label], "16px 'Myriad Pro'");
            temp8.lineHeight = -1;
            temp8.setTransform(153, 37 + label * 27);
            text_label.push(temp8);

            var temp9 = new cjs.Text(text_2[label], "16px 'Myriad Pro'");
            temp9.lineHeight = -1;
            temp9.setTransform(153, 154 + label * 27);
            text_label.push(temp9);
        }

        var text_label2 = [];
        var text_3 = ['11', '5', '9', '7'];
        var text_4 = ['11', '3', '9', '5'];

        for (var label = 0; label < 4; label++) {
            var temp1 = new cjs.Text("8", "16px 'Myriad Pro'");
            temp1.lineHeight = -1;
            temp1.setTransform(337, 37 + label * 27);
            text_label2.push(temp1);

            var temp2 = new cjs.Text("7", "16px 'Myriad Pro'");
            temp2.lineHeight = -1;
            temp2.setTransform(337, 154 + label * 27);
            text_label2.push(temp2);

            this.textbox_group1 = new cjs.Shape();
            this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(352, 33, 18, 21);
            this.textbox_group1.setTransform(0, label * 27);
            text_label2.push(this.textbox_group1);

            this.textbox_group2 = new cjs.Shape();
            this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(352, 150, 18, 21);
            this.textbox_group2.setTransform(0, label * 27);
            text_label2.push(this.textbox_group2);

            var temp3 = new cjs.Text("2", "16px 'Myriad Pro'");
            temp3.lineHeight = -1;
            temp3.setTransform(377.5, 37 + label * 27);
            text_label2.push(temp3);

            var temp4 = new cjs.Text("3", "16px 'Myriad Pro'");
            temp4.lineHeight = -1;
            temp4.setTransform(377.5, 154 + label * 27);
            text_label2.push(temp4);

            this.textbox_group3 = new cjs.Shape();
            this.textbox_group3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(392.5, 33, 18, 21);
            this.textbox_group3.setTransform(0, label * 27);
            text_label2.push(this.textbox_group3);

            this.textbox_group4 = new cjs.Shape();
            this.textbox_group4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(392.5, 150, 18, 21);
            this.textbox_group4.setTransform(0, label * 27);
            text_label2.push(this.textbox_group4);

            var temp5 = new cjs.Text("1", "16px 'Myriad Pro'");
            temp5.lineHeight = -1;
            temp5.setTransform(417.5, 37 + label * 27);
            text_label2.push(temp5);

            var temp6 = new cjs.Text("1", "16px 'Myriad Pro'");
            temp6.lineHeight = -1;
            temp6.setTransform(417.5, 154 + label * 27);
            text_label2.push(temp6);

            var temp6 = new cjs.Text("=", "16px 'Myriad Pro'");
            temp6.lineHeight = -1;
            temp6.setTransform(433, 37 + label * 27);
            text_label2.push(temp6);

            var temp7 = new cjs.Text("=", "16px 'Myriad Pro'");
            temp7.lineHeight = -1;
            temp7.setTransform(433, 154 + label * 27);
            text_label2.push(temp7);

            var temp8 = new cjs.Text(text_3[label], "16px 'Myriad Pro'");
            temp8.lineHeight = -1;
            temp8.setTransform(450, 37 + label * 27);
            text_label2.push(temp8);

            var temp9 = new cjs.Text(text_4[label], "16px 'Myriad Pro'");
            temp9.lineHeight = -1;
            temp9.setTransform(450, 154 + label * 27);
            text_label2.push(temp9);
        }



        this.addChild(this.roundRect1, this.text, this.text_1);
        this.addChild(this.instance);

        for (var rect = 0; rect < text_label.length; rect++) {
            this.addChild(text_label[rect]);
        }

        for (var rect = 0; rect < text_label2.length; rect++) {
            this.addChild(text_label2[rect]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(295.3, 101, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295.3, 412, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
