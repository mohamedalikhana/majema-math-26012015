(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p112_1.png",
            id: "p112_1"
        }, {
            src: "images/p112_2.png",
            id: "p112_2"
        }]
    };

    (lib.p112_1 = function() {
        this.initialize(img.p112_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p112_2 = function() {
        this.initialize(img.p112_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("112", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(35, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31, 660.5);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 514.5, 250, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Hur många kronor är det kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.hrRule_1 = new cjs.Shape();
        this.hrRule_1.graphics.beginStroke("#707070").setStrokeStyle(0.8).moveTo(0, 60.5).lineTo(512.8, 60.5).moveTo(0, 116).lineTo(512.8, 116).moveTo(0, 169).lineTo(512.8, 169)
            .moveTo(0, 222).lineTo(512.8, 222);
        this.hrRule_1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#707070").setStrokeStyle(0.8).moveTo(150, 26).lineTo(150, 276).moveTo(264, 26).lineTo(264, 276).moveTo(376, 26).lineTo(376, 276);
        this.Line_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("Pengar från början", "15px 'MyriadPro-Semibold'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(11, 38);

        this.text_3 = new cjs.Text("Får", "15px 'MyriadPro-Semibold'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(196, 38);

        this.text_4 = new cjs.Text("Köper för", "15px 'MyriadPro-Semibold'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(285, 38);

        this.text_5 = new cjs.Text("Är kvar till slut", "15px 'MyriadPro-Semibold'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(398, 38);

        var arrVal = ['2 kr', '4 kr', '3 kr', '5 kr', '6 kr', '2 kr', '1 kr', '7 kr'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            var yPos = 81;
            if (index == 1) {
                colSpace = 0.65;
                yPos = 81;
            } else if (index == 2) {
                colSpace = 0;
                yPos = 136;
            } else if (index == 3) {
                colSpace = 0.65;
                yPos = 136;
            } else if (index == 4) {
                colSpace = 0;
                yPos = 189;
            } else if (index == 5) {
                colSpace = 0.65;
                yPos = 189;
            } else if (index == 6) {
                colSpace = 0;
                yPos = 241;
            } else if (index == 7) {
                colSpace = 0.65;
                yPos = 241;
            } else if (index == 8) {
                colSpace = 0;
                yPos = 276;
            }

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(193 + (colSpace * 175.5), yPos);
            ToBeAdded.push(tempLabel);
        }

        for (var i = 0; i < 4; i++) {
            this.line1 = new cjs.Shape();
            this.line1.graphics.beginStroke("#707070").setStrokeStyle(0.8).moveTo(16, 20).lineTo(60, 20);
            this.line1.setTransform(400, 73 + (54 * i));
            ToBeAdded.push(this.line1);
        }

        for (var i = 0; i < 4; i++) {
            var tempLabel = new cjs.Text('kr', "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(462, 81 + (i * 54));
            ToBeAdded.push(tempLabel);
        }

        this.instance = new lib.p112_1();
        this.instance.setTransform(3, 69, 0.47, 0.47);

        this.addChild(this.roundRect1, this.text, this.text_1, this.hrRule_1);
        this.addChild(this.Line_1, this.text_2, this.text_3, this.text_4, this.text_5, this.instance);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 325.9);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("Hur många kan du köpa av varje?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p112_2();
        this.instance.setTransform(0, 29, 0.47, 0.47);
        // outer rectangles
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 96, 117, 10);
        this.roundRect1.setTransform(0, 0);
        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 123);
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(102, 26, 413.5, 117, 10);
        this.roundRect2.setTransform(0, 0);
        this.roundRect4 = this.roundRect2.clone(true);
        this.roundRect4.setTransform(0, 123);
        // inner rectangles
        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(108.5, 31, 95, 107, 10);
        this.roundRect5.setTransform(0, 0);
        this.roundRect6 = this.roundRect5.clone(true);
        this.roundRect6.setTransform(0, 123);
        this.roundRect7 = this.roundRect5.clone(true);
        this.roundRect7.setTransform(101.5, 0);
        this.roundRect8 = this.roundRect5.clone(true);
        this.roundRect8.setTransform(101.5, 123);
        this.roundRect9 = this.roundRect5.clone(true);
        this.roundRect9.setTransform(203, 0);
        this.roundRect10 = this.roundRect5.clone(true);
        this.roundRect10.setTransform(203, 123);
        this.roundRect11 = this.roundRect5.clone(true);
        this.roundRect11.setTransform(304.5, 0);
        this.roundRect12 = this.roundRect5.clone(true);
        this.roundRect12.setTransform(304.5, 123);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                var rowSpace = row;
                if(column==2){
                    columnSpace = 2.04;
                } else if(column==3){
                    columnSpace = 3.11;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(148 + (columnSpace * 98), 111 + (rowSpace * 122), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        // row-1
        this.text_2 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_2.lineHeight = 15;
        this.text_2.setTransform(167, 50);
        this.text_2.skewX=-26;
        this.text_2.skewY=-26;
        this.text_3 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_3.lineHeight = 15;
        this.text_3.setTransform(277, 66);
        this.text_3.skewX=13;
        this.text_3.skewY=13;
        this.text_4 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_4.lineHeight = 15;
        this.text_4.setTransform(365, 57);
        this.text_4.skewX=-17;
        this.text_4.skewY=-17;
        this.text_5 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_5.lineHeight = 15;
        this.text_5.setTransform(463, 46.5);
        this.text_5.skewX=-26;
        this.text_5.skewY=-26;
         // row-2
        this.text_6 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_6.lineHeight = 15;
        this.text_6.setTransform(166, 178.5);
        this.text_6.skewX=-11;
        this.text_6.skewY=-11;
        this.text_7 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 15;
        this.text_7.setTransform(265, 168.5);
        this.text_7.skewX=-18;
        this.text_7.skewY=-18;
        this.text_8 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_8.lineHeight = 15;
        this.text_8.setTransform(371, 173.5);
        this.text_8.skewX=-27;
        this.text_8.skewY=-27;
        this.text_9 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 15;
        this.text_9.setTransform(474, 173);
        this.text_9.skewX=-9;
        this.text_9.skewY=-9;

        this.addChild(this.text, this.text_1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        this.addChild(this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.roundRect9, this.roundRect10, this.roundRect11, this.roundRect12);
        this.addChild(this.instance, this.textbox_group1,this.text_2,this.text_3,this.text_4,this.text_5,this.text_6,this.text_7,this.text_8,this.text_9);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297.5, 116, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(298.5, 406, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
