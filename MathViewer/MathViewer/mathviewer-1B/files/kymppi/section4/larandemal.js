var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: []
    };

    (lib.Stage1_1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B62A5A').ss(3).drawRoundRect(210, 0, 1150, 420, 13);
        this.roundRect1.setTransform(-125, -80);

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#B62A5A").s("#B62A5A").ss(0.5, 0, 0, 4).arc(210, 0, 30, 0, 30 * Math.PI);
        this.shape_group1.setTransform(-125, -80);

        this.text_No = new cjs.Text("4", "35px 'MyriadPro-Semibold'", "#ffffff");
        this.text_No.setTransform(75, -70);

        this.textRect = new cjs.Shape();
        this.textRect.graphics.f("#ffffff").s('#B62A5A').ss(3).drawRoundRect(330, -25, 435, 50, 19);
        this.textRect.setTransform(-125, -80);

        this.text_1 = new cjs.Text("GEOMETRI OCH MÄTNING", "bold 28px 'Myriad Pro'", "#B62A5A");
        this.text_1.setTransform(255, -68);

        this.text_2 = new cjs.Text("•  kunna klockan - hel och halv timme", "35px 'Myriad Pro'", "#B62A5A");
        this.text_2.setTransform(205, 0);

        this.text_3 = new cjs.Text("•  kunna uppskatta tid", "35px 'Myriad Pro'", "#B62A5A");
        this.text_3.setTransform(205, 55);

        this.text_4 = new cjs.Text("•  kunna mäta längd med klossar och rutor", "35px 'Myriad Pro'", "#B62A5A");
        this.text_4.setTransform(205, 110);

        this.text_5 = new cjs.Text("•  kunna mäta längd i centimeter", "35px 'Myriad Pro'", "#B62A5A");
       this.text_5.setTransform(205, 165);

        this.text_6 = new cjs.Text("•  kunna beskriva egenskaper hos trianglar, fyrhörningar och cirklar", "35px 'Myriad Pro'", "#B62A5A");
        this.text_6.setTransform(205, 220);

        this.text_7 = new cjs.Text("•  ha fått arbeta med måttenheter som användes förr", "35px 'Myriad Pro'", "#B62A5A");
        this.text_7.setTransform(205, 275);

        this.addChild(this.roundRect1, this.shape_group1, this.text_No, this.textRect, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5,
         this.text_6, this.text_7);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage1_1();
        stage1_1.setTransform(0, 30);

        this.addChild(stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
