var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: []
    };

    (lib.Stage1_1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#81B53C').ss(3).drawRoundRect(210, 0, 950, 320, 13);
        this.roundRect1.setTransform(-25, -25);

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#81B53C").s("#81B53C").ss(0.5, 0, 0, 4).arc(210, 0, 30, 0, 30 * Math.PI);
        this.shape_group1.setTransform(-25, -25);

        this.text_No = new cjs.Text("1", "35px 'MyriadPro-Semibold'", "#ffffff");
        this.text_No.setTransform(175, -15);

        this.textRect = new cjs.Shape();
        this.textRect.graphics.f("#ffffff").s('#81B53C').ss(3).drawRoundRect(330, -25, 300, 50, 19);
        this.textRect.setTransform(-25, -25);

        this.text_1 = new cjs.Text("TALEN 0 TILL 20", "bold 28px 'Myriad Pro'", "#81B53C");
        this.text_1.setTransform(355, -13);

        this.text_2 = new cjs.Text("•  förstå och kunna använda talen 0 till 20", "35px 'Myriad Pro'", "#81B53C");
        this.text_2.setTransform(305, 60);

        this.text_3 = new cjs.Text("•  ha fått arbeta med talsymboler som användes förr", "35px 'Myriad Pro'", "#81B53C");
        this.text_3.setTransform(305, 120);      

        this.text_5 = new cjs.Text("•  kunna skilja på udda och jämna tal", "35px 'Myriad Pro'", "#81B53C");
        this.text_5.setTransform(305, 180);

         this.text_6 = new cjs.Text("•  ha fått arbeta med att lösa textuppgifter", "35px 'Myriad Pro'", "#81B53C");
        this.text_6.setTransform(305, 240);

        this.addChild(this.roundRect1, this.shape_group1, this.text_No, this.textRect, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_8);
    
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage1_1();
        stage1_1.setTransform(0, 20);

        this.addChild(stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
