var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        // ishideButtons: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };
    var transformX = transformY = 0.187;
    var moveX = 480 + 70,
        moveY = 6;
    var startX = ((86.0000) * transformX) + moveX,
        startY = ((0) * transformY) + moveY;
    var endX = ((195.2003) * transformX) + moveX,
        endY = ((1885.7513) * transformY) + moveY;
    var pencil = null;
    //  new createjs.Point(((86.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY)

    var startX2 = ((0) * transformX) + moveX,
        startY2 = ((0) * transformY) + moveY,
        endX2 = ((0) * transformX) + moveX,
        endY2 = ((0) * transformY) + moveY;
    var altStartX2 = startX,
        altStartY2 = startY,
        altEndX2 = endX,
        altEndY2 = endY;
    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [

        new createjs.Point(((86.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((371.9714) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((658.0286) * transformX) + moveX, ((0.0000) * transformY) + moveY),

        new createjs.Point(((944.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((28.9971) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((58.0029) * transformY) + moveY),

        new createjs.Point(((944.0000) * transformX) + moveX, ((87.0000) * transformY) + moveY),
        new createjs.Point(((943.6667) * transformX) + moveX, ((87.0000) * transformY) + moveY),
        new createjs.Point(((943.3333) * transformX) + moveX, ((87.0000) * transformY) + moveY),

        new createjs.Point(((943.0000) * transformX) + moveX, ((87.0000) * transformY) + moveY),
        new createjs.Point(((942.6667) * transformX) + moveX, ((88.3332) * transformY) + moveY),
        new createjs.Point(((942.3333) * transformX) + moveX, ((89.6668) * transformY) + moveY),

        new createjs.Point(((942.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),
        new createjs.Point(((941.6667) * transformX) + moveX, ((91.0000) * transformY) + moveY),
        new createjs.Point(((941.3333) * transformX) + moveX, ((91.0000) * transformY) + moveY),

        new createjs.Point(((941.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((91.9999) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((93.0001) * transformY) + moveY),

        new createjs.Point(((941.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((940.6667) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((940.3333) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((940.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((940.0000) * transformX) + moveX, ((94.6666) * transformY) + moveY),
        new createjs.Point(((940.0000) * transformX) + moveX, ((95.3334) * transformY) + moveY),

        new createjs.Point(((940.0000) * transformX) + moveX, ((96.0000) * transformY) + moveY),
        new createjs.Point(((939.6667) * transformX) + moveX, ((96.0000) * transformY) + moveY),
        new createjs.Point(((939.3333) * transformX) + moveX, ((96.0000) * transformY) + moveY),

        new createjs.Point(((939.0000) * transformX) + moveX, ((96.0000) * transformY) + moveY),
        new createjs.Point(((939.0000) * transformX) + moveX, ((96.9999) * transformY) + moveY),
        new createjs.Point(((939.0000) * transformX) + moveX, ((98.0001) * transformY) + moveY),

        new createjs.Point(((939.0000) * transformX) + moveX, ((99.0000) * transformY) + moveY),
        new createjs.Point(((938.6667) * transformX) + moveX, ((99.0000) * transformY) + moveY),
        new createjs.Point(((938.3333) * transformX) + moveX, ((99.0000) * transformY) + moveY),

        new createjs.Point(((938.0000) * transformX) + moveX, ((99.0000) * transformY) + moveY),
        new createjs.Point(((937.6667) * transformX) + moveX, ((100.3332) * transformY) + moveY),
        new createjs.Point(((937.3333) * transformX) + moveX, ((101.6668) * transformY) + moveY),

        new createjs.Point(((937.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((936.6667) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((936.3333) * transformX) + moveX, ((103.0000) * transformY) + moveY),

        new createjs.Point(((936.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((936.0000) * transformX) + moveX, ((103.9999) * transformY) + moveY),
        new createjs.Point(((936.0000) * transformX) + moveX, ((105.0001) * transformY) + moveY),

        new createjs.Point(((936.0000) * transformX) + moveX, ((106.0000) * transformY) + moveY),
        new createjs.Point(((935.6667) * transformX) + moveX, ((106.0000) * transformY) + moveY),
        new createjs.Point(((935.3333) * transformX) + moveX, ((106.0000) * transformY) + moveY),

        new createjs.Point(((935.0000) * transformX) + moveX, ((106.0000) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((106.6666) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((107.3334) * transformY) + moveY),

        new createjs.Point(((935.0000) * transformX) + moveX, ((108.0000) * transformY) + moveY),
        new createjs.Point(((934.6667) * transformX) + moveX, ((108.0000) * transformY) + moveY),
        new createjs.Point(((934.3333) * transformX) + moveX, ((108.0000) * transformY) + moveY),

        new createjs.Point(((934.0000) * transformX) + moveX, ((108.0000) * transformY) + moveY),
        new createjs.Point(((934.0000) * transformX) + moveX, ((108.9999) * transformY) + moveY),
        new createjs.Point(((934.0000) * transformX) + moveX, ((110.0001) * transformY) + moveY),

        new createjs.Point(((934.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((933.6667) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((933.3333) * transformX) + moveX, ((111.0000) * transformY) + moveY),

        new createjs.Point(((933.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((932.6667) * transformX) + moveX, ((112.3332) * transformY) + moveY),
        new createjs.Point(((932.3333) * transformX) + moveX, ((113.6668) * transformY) + moveY),

        new createjs.Point(((932.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((931.6667) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((931.3333) * transformX) + moveX, ((115.0000) * transformY) + moveY),

        new createjs.Point(((931.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((115.9999) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((117.0001) * transformY) + moveY),

        new createjs.Point(((931.0000) * transformX) + moveX, ((118.0000) * transformY) + moveY),
        new createjs.Point(((930.6667) * transformX) + moveX, ((118.0000) * transformY) + moveY),
        new createjs.Point(((930.3333) * transformX) + moveX, ((118.0000) * transformY) + moveY),

        new createjs.Point(((930.0000) * transformX) + moveX, ((118.0000) * transformY) + moveY),
        new createjs.Point(((930.0000) * transformX) + moveX, ((118.6666) * transformY) + moveY),
        new createjs.Point(((930.0000) * transformX) + moveX, ((119.3334) * transformY) + moveY),

        new createjs.Point(((930.0000) * transformX) + moveX, ((120.0000) * transformY) + moveY),
        new createjs.Point(((929.6667) * transformX) + moveX, ((120.0000) * transformY) + moveY),
        new createjs.Point(((929.3333) * transformX) + moveX, ((120.0000) * transformY) + moveY),

        new createjs.Point(((929.0000) * transformX) + moveX, ((120.0000) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((120.9999) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((122.0001) * transformY) + moveY),

        new createjs.Point(((929.0000) * transformX) + moveX, ((123.0000) * transformY) + moveY),
        new createjs.Point(((928.6667) * transformX) + moveX, ((123.0000) * transformY) + moveY),
        new createjs.Point(((928.3333) * transformX) + moveX, ((123.0000) * transformY) + moveY),

        new createjs.Point(((928.0000) * transformX) + moveX, ((123.0000) * transformY) + moveY),
        new createjs.Point(((927.6667) * transformX) + moveX, ((124.3332) * transformY) + moveY),
        new createjs.Point(((927.3333) * transformX) + moveX, ((125.6668) * transformY) + moveY),

        new createjs.Point(((927.0000) * transformX) + moveX, ((127.0000) * transformY) + moveY),
        new createjs.Point(((926.6667) * transformX) + moveX, ((127.0000) * transformY) + moveY),
        new createjs.Point(((926.3333) * transformX) + moveX, ((127.0000) * transformY) + moveY),

        new createjs.Point(((926.0000) * transformX) + moveX, ((127.0000) * transformY) + moveY),
        new createjs.Point(((926.0000) * transformX) + moveX, ((127.9999) * transformY) + moveY),
        new createjs.Point(((926.0000) * transformX) + moveX, ((129.0001) * transformY) + moveY),

        new createjs.Point(((926.0000) * transformX) + moveX, ((130.0000) * transformY) + moveY),
        new createjs.Point(((925.6667) * transformX) + moveX, ((130.0000) * transformY) + moveY),
        new createjs.Point(((925.3333) * transformX) + moveX, ((130.0000) * transformY) + moveY),

        new createjs.Point(((925.0000) * transformX) + moveX, ((130.0000) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((130.6666) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((131.3334) * transformY) + moveY),

        new createjs.Point(((925.0000) * transformX) + moveX, ((132.0000) * transformY) + moveY),
        new createjs.Point(((924.6667) * transformX) + moveX, ((132.0000) * transformY) + moveY),
        new createjs.Point(((924.3333) * transformX) + moveX, ((132.0000) * transformY) + moveY),

        new createjs.Point(((924.0000) * transformX) + moveX, ((132.0000) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((132.9999) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((134.0001) * transformY) + moveY),

        new createjs.Point(((924.0000) * transformX) + moveX, ((135.0000) * transformY) + moveY),
        new createjs.Point(((923.6667) * transformX) + moveX, ((135.0000) * transformY) + moveY),
        new createjs.Point(((923.3333) * transformX) + moveX, ((135.0000) * transformY) + moveY),

        new createjs.Point(((923.0000) * transformX) + moveX, ((135.0000) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((135.6666) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((136.3334) * transformY) + moveY),

        new createjs.Point(((923.0000) * transformX) + moveX, ((137.0000) * transformY) + moveY),
        new createjs.Point(((922.6667) * transformX) + moveX, ((137.0000) * transformY) + moveY),
        new createjs.Point(((922.3333) * transformX) + moveX, ((137.0000) * transformY) + moveY),

        new createjs.Point(((922.0000) * transformX) + moveX, ((137.0000) * transformY) + moveY),
        new createjs.Point(((921.0001) * transformX) + moveX, ((140.3330) * transformY) + moveY),
        new createjs.Point(((919.9999) * transformX) + moveX, ((143.6670) * transformY) + moveY),

        new createjs.Point(((919.0000) * transformX) + moveX, ((147.0000) * transformY) + moveY),
        new createjs.Point(((918.6667) * transformX) + moveX, ((147.0000) * transformY) + moveY),
        new createjs.Point(((918.3333) * transformX) + moveX, ((147.0000) * transformY) + moveY),

        new createjs.Point(((918.0000) * transformX) + moveX, ((147.0000) * transformY) + moveY),
        new createjs.Point(((917.6667) * transformX) + moveX, ((148.3332) * transformY) + moveY),
        new createjs.Point(((917.3333) * transformX) + moveX, ((149.6668) * transformY) + moveY),

        new createjs.Point(((917.0000) * transformX) + moveX, ((151.0000) * transformY) + moveY),
        new createjs.Point(((916.6667) * transformX) + moveX, ((151.0000) * transformY) + moveY),
        new createjs.Point(((916.3333) * transformX) + moveX, ((151.0000) * transformY) + moveY),

        new createjs.Point(((916.0000) * transformX) + moveX, ((151.0000) * transformY) + moveY),
        new createjs.Point(((916.0000) * transformX) + moveX, ((151.9999) * transformY) + moveY),
        new createjs.Point(((916.0000) * transformX) + moveX, ((153.0001) * transformY) + moveY),

        new createjs.Point(((916.0000) * transformX) + moveX, ((154.0000) * transformY) + moveY),
        new createjs.Point(((915.6667) * transformX) + moveX, ((154.0000) * transformY) + moveY),
        new createjs.Point(((915.3333) * transformX) + moveX, ((154.0000) * transformY) + moveY),

        new createjs.Point(((915.0000) * transformX) + moveX, ((154.0000) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((154.6666) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((155.3334) * transformY) + moveY),

        new createjs.Point(((915.0000) * transformX) + moveX, ((156.0000) * transformY) + moveY),
        new createjs.Point(((914.6667) * transformX) + moveX, ((156.0000) * transformY) + moveY),
        new createjs.Point(((914.3333) * transformX) + moveX, ((156.0000) * transformY) + moveY),

        new createjs.Point(((914.0000) * transformX) + moveX, ((156.0000) * transformY) + moveY),
        new createjs.Point(((914.0000) * transformX) + moveX, ((156.9999) * transformY) + moveY),
        new createjs.Point(((914.0000) * transformX) + moveX, ((158.0001) * transformY) + moveY),

        new createjs.Point(((914.0000) * transformX) + moveX, ((159.0000) * transformY) + moveY),
        new createjs.Point(((913.6667) * transformX) + moveX, ((159.0000) * transformY) + moveY),
        new createjs.Point(((913.3333) * transformX) + moveX, ((159.0000) * transformY) + moveY),

        new createjs.Point(((913.0000) * transformX) + moveX, ((159.0000) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((159.6666) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((160.3334) * transformY) + moveY),

        new createjs.Point(((913.0000) * transformX) + moveX, ((161.0000) * transformY) + moveY),
        new createjs.Point(((912.6667) * transformX) + moveX, ((161.0000) * transformY) + moveY),
        new createjs.Point(((912.3333) * transformX) + moveX, ((161.0000) * transformY) + moveY),

        new createjs.Point(((912.0000) * transformX) + moveX, ((161.0000) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((161.9999) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((163.0001) * transformY) + moveY),

        new createjs.Point(((912.0000) * transformX) + moveX, ((164.0000) * transformY) + moveY),
        new createjs.Point(((911.6667) * transformX) + moveX, ((164.0000) * transformY) + moveY),
        new createjs.Point(((911.3333) * transformX) + moveX, ((164.0000) * transformY) + moveY),

        new createjs.Point(((911.0000) * transformX) + moveX, ((164.0000) * transformY) + moveY),
        new createjs.Point(((910.6667) * transformX) + moveX, ((165.3332) * transformY) + moveY),
        new createjs.Point(((910.3333) * transformX) + moveX, ((166.6668) * transformY) + moveY),

        new createjs.Point(((910.0000) * transformX) + moveX, ((168.0000) * transformY) + moveY),
        new createjs.Point(((909.6667) * transformX) + moveX, ((168.0000) * transformY) + moveY),
        new createjs.Point(((909.3333) * transformX) + moveX, ((168.0000) * transformY) + moveY),

        new createjs.Point(((909.0000) * transformX) + moveX, ((168.0000) * transformY) + moveY),
        new createjs.Point(((909.0000) * transformX) + moveX, ((168.9999) * transformY) + moveY),
        new createjs.Point(((909.0000) * transformX) + moveX, ((170.0001) * transformY) + moveY),

        new createjs.Point(((909.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),
        new createjs.Point(((908.6667) * transformX) + moveX, ((171.0000) * transformY) + moveY),
        new createjs.Point(((908.3333) * transformX) + moveX, ((171.0000) * transformY) + moveY),

        new createjs.Point(((908.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),
        new createjs.Point(((908.0000) * transformX) + moveX, ((171.6666) * transformY) + moveY),
        new createjs.Point(((908.0000) * transformX) + moveX, ((172.3334) * transformY) + moveY),

        new createjs.Point(((908.0000) * transformX) + moveX, ((173.0000) * transformY) + moveY),
        new createjs.Point(((907.6667) * transformX) + moveX, ((173.0000) * transformY) + moveY),
        new createjs.Point(((907.3333) * transformX) + moveX, ((173.0000) * transformY) + moveY),

        new createjs.Point(((907.0000) * transformX) + moveX, ((173.0000) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((173.9999) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((175.0001) * transformY) + moveY),

        new createjs.Point(((907.0000) * transformX) + moveX, ((176.0000) * transformY) + moveY),
        new createjs.Point(((906.6667) * transformX) + moveX, ((176.0000) * transformY) + moveY),
        new createjs.Point(((906.3333) * transformX) + moveX, ((176.0000) * transformY) + moveY),

        new createjs.Point(((906.0000) * transformX) + moveX, ((176.0000) * transformY) + moveY),
        new createjs.Point(((905.6667) * transformX) + moveX, ((177.3332) * transformY) + moveY),
        new createjs.Point(((905.3333) * transformX) + moveX, ((178.6668) * transformY) + moveY),

        new createjs.Point(((905.0000) * transformX) + moveX, ((180.0000) * transformY) + moveY),
        new createjs.Point(((904.6667) * transformX) + moveX, ((180.0000) * transformY) + moveY),
        new createjs.Point(((904.3333) * transformX) + moveX, ((180.0000) * transformY) + moveY),

        new createjs.Point(((904.0000) * transformX) + moveX, ((180.0000) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((180.9999) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((182.0001) * transformY) + moveY),

        new createjs.Point(((904.0000) * transformX) + moveX, ((183.0000) * transformY) + moveY),
        new createjs.Point(((903.6667) * transformX) + moveX, ((183.0000) * transformY) + moveY),
        new createjs.Point(((903.3333) * transformX) + moveX, ((183.0000) * transformY) + moveY),

        new createjs.Point(((903.0000) * transformX) + moveX, ((183.0000) * transformY) + moveY),
        new createjs.Point(((902.6667) * transformX) + moveX, ((184.6665) * transformY) + moveY),
        new createjs.Point(((902.3333) * transformX) + moveX, ((186.3335) * transformY) + moveY),

        new createjs.Point(((902.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),
        new createjs.Point(((901.6667) * transformX) + moveX, ((188.0000) * transformY) + moveY),
        new createjs.Point(((901.3333) * transformX) + moveX, ((188.0000) * transformY) + moveY),

        new createjs.Point(((901.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),
        new createjs.Point(((900.6667) * transformX) + moveX, ((189.3332) * transformY) + moveY),
        new createjs.Point(((900.3333) * transformX) + moveX, ((190.6668) * transformY) + moveY),

        new createjs.Point(((900.0000) * transformX) + moveX, ((192.0000) * transformY) + moveY),
        new createjs.Point(((899.6667) * transformX) + moveX, ((192.0000) * transformY) + moveY),
        new createjs.Point(((899.3333) * transformX) + moveX, ((192.0000) * transformY) + moveY),

        new createjs.Point(((899.0000) * transformX) + moveX, ((192.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((192.9999) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((194.0001) * transformY) + moveY),

        new createjs.Point(((899.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),
        new createjs.Point(((898.6667) * transformX) + moveX, ((195.0000) * transformY) + moveY),
        new createjs.Point(((898.3333) * transformX) + moveX, ((195.0000) * transformY) + moveY),

        new createjs.Point(((898.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((195.6666) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((196.3334) * transformY) + moveY),

        new createjs.Point(((898.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),
        new createjs.Point(((897.6667) * transformX) + moveX, ((197.0000) * transformY) + moveY),
        new createjs.Point(((897.3333) * transformX) + moveX, ((197.0000) * transformY) + moveY),

        new createjs.Point(((897.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((197.9999) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((199.0001) * transformY) + moveY),

        new createjs.Point(((897.0000) * transformX) + moveX, ((200.0000) * transformY) + moveY),
        new createjs.Point(((896.6667) * transformX) + moveX, ((200.0000) * transformY) + moveY),
        new createjs.Point(((896.3333) * transformX) + moveX, ((200.0000) * transformY) + moveY),

        new createjs.Point(((896.0000) * transformX) + moveX, ((200.0000) * transformY) + moveY),
        new createjs.Point(((895.6667) * transformX) + moveX, ((201.3332) * transformY) + moveY),
        new createjs.Point(((895.3333) * transformX) + moveX, ((202.6668) * transformY) + moveY),

        new createjs.Point(((895.0000) * transformX) + moveX, ((204.0000) * transformY) + moveY),
        new createjs.Point(((894.6667) * transformX) + moveX, ((204.0000) * transformY) + moveY),
        new createjs.Point(((894.3333) * transformX) + moveX, ((204.0000) * transformY) + moveY),

        new createjs.Point(((894.0000) * transformX) + moveX, ((204.0000) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((204.9999) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((206.0001) * transformY) + moveY),

        new createjs.Point(((894.0000) * transformX) + moveX, ((207.0000) * transformY) + moveY),
        new createjs.Point(((893.6667) * transformX) + moveX, ((207.0000) * transformY) + moveY),
        new createjs.Point(((893.3333) * transformX) + moveX, ((207.0000) * transformY) + moveY),

        new createjs.Point(((893.0000) * transformX) + moveX, ((207.0000) * transformY) + moveY),
        new createjs.Point(((892.6667) * transformX) + moveX, ((208.6665) * transformY) + moveY),
        new createjs.Point(((892.3333) * transformX) + moveX, ((210.3335) * transformY) + moveY),

        new createjs.Point(((892.0000) * transformX) + moveX, ((212.0000) * transformY) + moveY),
        new createjs.Point(((891.6667) * transformX) + moveX, ((212.0000) * transformY) + moveY),
        new createjs.Point(((891.3333) * transformX) + moveX, ((212.0000) * transformY) + moveY),

        new createjs.Point(((891.0000) * transformX) + moveX, ((212.0000) * transformY) + moveY),
        new createjs.Point(((890.6667) * transformX) + moveX, ((213.3332) * transformY) + moveY),
        new createjs.Point(((890.3333) * transformX) + moveX, ((214.6668) * transformY) + moveY),

        new createjs.Point(((890.0000) * transformX) + moveX, ((216.0000) * transformY) + moveY),
        new createjs.Point(((889.6667) * transformX) + moveX, ((216.0000) * transformY) + moveY),
        new createjs.Point(((889.3333) * transformX) + moveX, ((216.0000) * transformY) + moveY),

        new createjs.Point(((889.0000) * transformX) + moveX, ((216.0000) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((216.9999) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((218.0001) * transformY) + moveY),

        new createjs.Point(((889.0000) * transformX) + moveX, ((219.0000) * transformY) + moveY),
        new createjs.Point(((888.6667) * transformX) + moveX, ((219.0000) * transformY) + moveY),
        new createjs.Point(((888.3333) * transformX) + moveX, ((219.0000) * transformY) + moveY),

        new createjs.Point(((888.0000) * transformX) + moveX, ((219.0000) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((219.6666) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((220.3334) * transformY) + moveY),

        new createjs.Point(((888.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),
        new createjs.Point(((887.6667) * transformX) + moveX, ((221.0000) * transformY) + moveY),
        new createjs.Point(((887.3333) * transformX) + moveX, ((221.0000) * transformY) + moveY),

        new createjs.Point(((887.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((221.9999) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((223.0001) * transformY) + moveY),

        new createjs.Point(((887.0000) * transformX) + moveX, ((224.0000) * transformY) + moveY),
        new createjs.Point(((886.6667) * transformX) + moveX, ((224.0000) * transformY) + moveY),
        new createjs.Point(((886.3333) * transformX) + moveX, ((224.0000) * transformY) + moveY),

        new createjs.Point(((886.0000) * transformX) + moveX, ((224.0000) * transformY) + moveY),
        new createjs.Point(((885.6667) * transformX) + moveX, ((225.3332) * transformY) + moveY),
        new createjs.Point(((885.3333) * transformX) + moveX, ((226.6668) * transformY) + moveY),

        new createjs.Point(((885.0000) * transformX) + moveX, ((228.0000) * transformY) + moveY),
        new createjs.Point(((884.6667) * transformX) + moveX, ((228.0000) * transformY) + moveY),
        new createjs.Point(((884.3333) * transformX) + moveX, ((228.0000) * transformY) + moveY),

        new createjs.Point(((884.0000) * transformX) + moveX, ((228.0000) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((228.9999) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((230.0001) * transformY) + moveY),

        new createjs.Point(((884.0000) * transformX) + moveX, ((231.0000) * transformY) + moveY),
        new createjs.Point(((883.6667) * transformX) + moveX, ((231.0000) * transformY) + moveY),
        new createjs.Point(((883.3333) * transformX) + moveX, ((231.0000) * transformY) + moveY),

        new createjs.Point(((883.0000) * transformX) + moveX, ((231.0000) * transformY) + moveY),
        new createjs.Point(((882.6667) * transformX) + moveX, ((232.6665) * transformY) + moveY),
        new createjs.Point(((882.3333) * transformX) + moveX, ((234.3335) * transformY) + moveY),

        new createjs.Point(((882.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((881.6667) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((881.3333) * transformX) + moveX, ((236.0000) * transformY) + moveY),

        new createjs.Point(((881.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((236.6666) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((237.3334) * transformY) + moveY),

        new createjs.Point(((881.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((880.6667) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((880.3333) * transformX) + moveX, ((238.0000) * transformY) + moveY),

        new createjs.Point(((880.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((238.9999) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((240.0001) * transformY) + moveY),

        new createjs.Point(((880.0000) * transformX) + moveX, ((241.0000) * transformY) + moveY),
        new createjs.Point(((879.6667) * transformX) + moveX, ((241.0000) * transformY) + moveY),
        new createjs.Point(((879.3333) * transformX) + moveX, ((241.0000) * transformY) + moveY),

        new createjs.Point(((879.0000) * transformX) + moveX, ((241.0000) * transformY) + moveY),
        new createjs.Point(((878.6667) * transformX) + moveX, ((242.3332) * transformY) + moveY),
        new createjs.Point(((878.3333) * transformX) + moveX, ((243.6668) * transformY) + moveY),

        new createjs.Point(((878.0000) * transformX) + moveX, ((245.0000) * transformY) + moveY),
        new createjs.Point(((877.6667) * transformX) + moveX, ((245.0000) * transformY) + moveY),
        new createjs.Point(((877.3333) * transformX) + moveX, ((245.0000) * transformY) + moveY),

        new createjs.Point(((877.0000) * transformX) + moveX, ((245.0000) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((245.9999) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((247.0001) * transformY) + moveY),

        new createjs.Point(((877.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((876.6667) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((876.3333) * transformX) + moveX, ((248.0000) * transformY) + moveY),

        new createjs.Point(((876.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((248.6666) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((249.3334) * transformY) + moveY),

        new createjs.Point(((876.0000) * transformX) + moveX, ((250.0000) * transformY) + moveY),
        new createjs.Point(((875.6667) * transformX) + moveX, ((250.0000) * transformY) + moveY),
        new createjs.Point(((875.3333) * transformX) + moveX, ((250.0000) * transformY) + moveY),

        new createjs.Point(((875.0000) * transformX) + moveX, ((250.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((250.9999) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((252.0001) * transformY) + moveY),

        new createjs.Point(((875.0000) * transformX) + moveX, ((253.0000) * transformY) + moveY),
        new createjs.Point(((874.6667) * transformX) + moveX, ((253.0000) * transformY) + moveY),
        new createjs.Point(((874.3333) * transformX) + moveX, ((253.0000) * transformY) + moveY),

        new createjs.Point(((874.0000) * transformX) + moveX, ((253.0000) * transformY) + moveY),
        new createjs.Point(((873.6667) * transformX) + moveX, ((254.3332) * transformY) + moveY),
        new createjs.Point(((873.3333) * transformX) + moveX, ((255.6668) * transformY) + moveY),

        new createjs.Point(((873.0000) * transformX) + moveX, ((257.0000) * transformY) + moveY),
        new createjs.Point(((872.6667) * transformX) + moveX, ((257.0000) * transformY) + moveY),
        new createjs.Point(((872.3333) * transformX) + moveX, ((257.0000) * transformY) + moveY),

        new createjs.Point(((872.0000) * transformX) + moveX, ((257.0000) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((257.9999) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((259.0001) * transformY) + moveY),

        new createjs.Point(((872.0000) * transformX) + moveX, ((260.0000) * transformY) + moveY),
        new createjs.Point(((871.6667) * transformX) + moveX, ((260.0000) * transformY) + moveY),
        new createjs.Point(((871.3333) * transformX) + moveX, ((260.0000) * transformY) + moveY),

        new createjs.Point(((871.0000) * transformX) + moveX, ((260.0000) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((260.6666) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((261.3334) * transformY) + moveY),

        new createjs.Point(((871.0000) * transformX) + moveX, ((262.0000) * transformY) + moveY),
        new createjs.Point(((870.6667) * transformX) + moveX, ((262.0000) * transformY) + moveY),
        new createjs.Point(((870.3333) * transformX) + moveX, ((262.0000) * transformY) + moveY),

        new createjs.Point(((870.0000) * transformX) + moveX, ((262.0000) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((262.9999) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((264.0001) * transformY) + moveY),

        new createjs.Point(((870.0000) * transformX) + moveX, ((265.0000) * transformY) + moveY),
        new createjs.Point(((869.6667) * transformX) + moveX, ((265.0000) * transformY) + moveY),
        new createjs.Point(((869.3333) * transformX) + moveX, ((265.0000) * transformY) + moveY),

        new createjs.Point(((869.0000) * transformX) + moveX, ((265.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((265.6666) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((266.3334) * transformY) + moveY),

        new createjs.Point(((869.0000) * transformX) + moveX, ((267.0000) * transformY) + moveY),
        new createjs.Point(((868.6667) * transformX) + moveX, ((267.0000) * transformY) + moveY),
        new createjs.Point(((868.3333) * transformX) + moveX, ((267.0000) * transformY) + moveY),

        new createjs.Point(((868.0000) * transformX) + moveX, ((267.0000) * transformY) + moveY),
        new createjs.Point(((867.0001) * transformX) + moveX, ((270.3330) * transformY) + moveY),
        new createjs.Point(((865.9999) * transformX) + moveX, ((273.6670) * transformY) + moveY),

        new createjs.Point(((865.0000) * transformX) + moveX, ((277.0000) * transformY) + moveY),
        new createjs.Point(((864.6667) * transformX) + moveX, ((277.0000) * transformY) + moveY),
        new createjs.Point(((864.3333) * transformX) + moveX, ((277.0000) * transformY) + moveY),

        new createjs.Point(((864.0000) * transformX) + moveX, ((277.0000) * transformY) + moveY),
        new createjs.Point(((863.6667) * transformX) + moveX, ((278.3332) * transformY) + moveY),
        new createjs.Point(((863.3333) * transformX) + moveX, ((279.6668) * transformY) + moveY),

        new createjs.Point(((863.0000) * transformX) + moveX, ((281.0000) * transformY) + moveY),
        new createjs.Point(((862.6667) * transformX) + moveX, ((281.0000) * transformY) + moveY),
        new createjs.Point(((862.3333) * transformX) + moveX, ((281.0000) * transformY) + moveY),

        new createjs.Point(((862.0000) * transformX) + moveX, ((281.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((281.9999) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((283.0001) * transformY) + moveY),

        new createjs.Point(((862.0000) * transformX) + moveX, ((284.0000) * transformY) + moveY),
        new createjs.Point(((861.6667) * transformX) + moveX, ((284.0000) * transformY) + moveY),
        new createjs.Point(((861.3333) * transformX) + moveX, ((284.0000) * transformY) + moveY),

        new createjs.Point(((861.0000) * transformX) + moveX, ((284.0000) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((284.6666) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((285.3334) * transformY) + moveY),

        new createjs.Point(((861.0000) * transformX) + moveX, ((286.0000) * transformY) + moveY),
        new createjs.Point(((860.6667) * transformX) + moveX, ((286.0000) * transformY) + moveY),
        new createjs.Point(((860.3333) * transformX) + moveX, ((286.0000) * transformY) + moveY),

        new createjs.Point(((860.0000) * transformX) + moveX, ((286.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((286.9999) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((288.0001) * transformY) + moveY),

        new createjs.Point(((860.0000) * transformX) + moveX, ((289.0000) * transformY) + moveY),
        new createjs.Point(((859.6667) * transformX) + moveX, ((289.0000) * transformY) + moveY),
        new createjs.Point(((859.3333) * transformX) + moveX, ((289.0000) * transformY) + moveY),

        new createjs.Point(((859.0000) * transformX) + moveX, ((289.0000) * transformY) + moveY),
        new createjs.Point(((858.6667) * transformX) + moveX, ((290.3332) * transformY) + moveY),
        new createjs.Point(((858.3333) * transformX) + moveX, ((291.6668) * transformY) + moveY),

        new createjs.Point(((858.0000) * transformX) + moveX, ((293.0000) * transformY) + moveY),
        new createjs.Point(((857.6667) * transformX) + moveX, ((293.0000) * transformY) + moveY),
        new createjs.Point(((857.3333) * transformX) + moveX, ((293.0000) * transformY) + moveY),

        new createjs.Point(((857.0000) * transformX) + moveX, ((293.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((293.9999) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((295.0001) * transformY) + moveY),

        new createjs.Point(((857.0000) * transformX) + moveX, ((296.0000) * transformY) + moveY),
        new createjs.Point(((856.6667) * transformX) + moveX, ((296.0000) * transformY) + moveY),
        new createjs.Point(((856.3333) * transformX) + moveX, ((296.0000) * transformY) + moveY),

        new createjs.Point(((856.0000) * transformX) + moveX, ((296.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((296.6666) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((297.3334) * transformY) + moveY),

        new createjs.Point(((856.0000) * transformX) + moveX, ((298.0000) * transformY) + moveY),
        new createjs.Point(((855.6667) * transformX) + moveX, ((298.0000) * transformY) + moveY),
        new createjs.Point(((855.3333) * transformX) + moveX, ((298.0000) * transformY) + moveY),

        new createjs.Point(((855.0000) * transformX) + moveX, ((298.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((298.9999) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((300.0001) * transformY) + moveY),

        new createjs.Point(((855.0000) * transformX) + moveX, ((301.0000) * transformY) + moveY),
        new createjs.Point(((854.6667) * transformX) + moveX, ((301.0000) * transformY) + moveY),
        new createjs.Point(((854.3333) * transformX) + moveX, ((301.0000) * transformY) + moveY),

        new createjs.Point(((854.0000) * transformX) + moveX, ((301.0000) * transformY) + moveY),
        new createjs.Point(((853.6667) * transformX) + moveX, ((302.3332) * transformY) + moveY),
        new createjs.Point(((853.3333) * transformX) + moveX, ((303.6668) * transformY) + moveY),

        new createjs.Point(((853.0000) * transformX) + moveX, ((305.0000) * transformY) + moveY),
        new createjs.Point(((852.6667) * transformX) + moveX, ((305.0000) * transformY) + moveY),
        new createjs.Point(((852.3333) * transformX) + moveX, ((305.0000) * transformY) + moveY),

        new createjs.Point(((852.0000) * transformX) + moveX, ((305.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((305.9999) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((307.0001) * transformY) + moveY),

        new createjs.Point(((852.0000) * transformX) + moveX, ((308.0000) * transformY) + moveY),
        new createjs.Point(((851.6667) * transformX) + moveX, ((308.0000) * transformY) + moveY),
        new createjs.Point(((851.3333) * transformX) + moveX, ((308.0000) * transformY) + moveY),

        new createjs.Point(((851.0000) * transformX) + moveX, ((308.0000) * transformY) + moveY),
        new createjs.Point(((850.6667) * transformX) + moveX, ((309.6665) * transformY) + moveY),
        new createjs.Point(((850.3333) * transformX) + moveX, ((311.3335) * transformY) + moveY),

        new createjs.Point(((850.0000) * transformX) + moveX, ((313.0000) * transformY) + moveY),
        new createjs.Point(((849.6667) * transformX) + moveX, ((313.0000) * transformY) + moveY),
        new createjs.Point(((849.3333) * transformX) + moveX, ((313.0000) * transformY) + moveY),

        new createjs.Point(((849.0000) * transformX) + moveX, ((313.0000) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((313.6666) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((314.3334) * transformY) + moveY),

        new createjs.Point(((849.0000) * transformX) + moveX, ((315.0000) * transformY) + moveY),
        new createjs.Point(((848.6667) * transformX) + moveX, ((315.0000) * transformY) + moveY),
        new createjs.Point(((848.3333) * transformX) + moveX, ((315.0000) * transformY) + moveY),

        new createjs.Point(((848.0000) * transformX) + moveX, ((315.0000) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((315.9999) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((317.0001) * transformY) + moveY),

        new createjs.Point(((848.0000) * transformX) + moveX, ((318.0000) * transformY) + moveY),
        new createjs.Point(((847.6667) * transformX) + moveX, ((318.0000) * transformY) + moveY),
        new createjs.Point(((847.3333) * transformX) + moveX, ((318.0000) * transformY) + moveY),

        new createjs.Point(((847.0000) * transformX) + moveX, ((318.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((318.6666) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((319.3334) * transformY) + moveY),

        new createjs.Point(((847.0000) * transformX) + moveX, ((320.0000) * transformY) + moveY),
        new createjs.Point(((846.6667) * transformX) + moveX, ((320.0000) * transformY) + moveY),
        new createjs.Point(((846.3333) * transformX) + moveX, ((320.0000) * transformY) + moveY),

        new createjs.Point(((846.0000) * transformX) + moveX, ((320.0000) * transformY) + moveY),
        new createjs.Point(((845.0001) * transformX) + moveX, ((323.3330) * transformY) + moveY),
        new createjs.Point(((843.9999) * transformX) + moveX, ((326.6670) * transformY) + moveY),

        new createjs.Point(((843.0000) * transformX) + moveX, ((330.0000) * transformY) + moveY),
        new createjs.Point(((842.6667) * transformX) + moveX, ((330.0000) * transformY) + moveY),
        new createjs.Point(((842.3333) * transformX) + moveX, ((330.0000) * transformY) + moveY),

        new createjs.Point(((842.0000) * transformX) + moveX, ((330.0000) * transformY) + moveY),
        new createjs.Point(((841.6667) * transformX) + moveX, ((331.3332) * transformY) + moveY),
        new createjs.Point(((841.3333) * transformX) + moveX, ((332.6668) * transformY) + moveY),

        new createjs.Point(((841.0000) * transformX) + moveX, ((334.0000) * transformY) + moveY),
        new createjs.Point(((840.6667) * transformX) + moveX, ((334.0000) * transformY) + moveY),
        new createjs.Point(((840.3333) * transformX) + moveX, ((334.0000) * transformY) + moveY),

        new createjs.Point(((840.0000) * transformX) + moveX, ((334.0000) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((334.9999) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((336.0001) * transformY) + moveY),

        new createjs.Point(((840.0000) * transformX) + moveX, ((337.0000) * transformY) + moveY),
        new createjs.Point(((839.6667) * transformX) + moveX, ((337.0000) * transformY) + moveY),
        new createjs.Point(((839.3333) * transformX) + moveX, ((337.0000) * transformY) + moveY),

        new createjs.Point(((839.0000) * transformX) + moveX, ((337.0000) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((337.6666) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((338.3334) * transformY) + moveY),

        new createjs.Point(((839.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),
        new createjs.Point(((838.6667) * transformX) + moveX, ((339.0000) * transformY) + moveY),
        new createjs.Point(((838.3333) * transformX) + moveX, ((339.0000) * transformY) + moveY),

        new createjs.Point(((838.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((339.9999) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((341.0001) * transformY) + moveY),

        new createjs.Point(((838.0000) * transformX) + moveX, ((342.0000) * transformY) + moveY),
        new createjs.Point(((837.6667) * transformX) + moveX, ((342.0000) * transformY) + moveY),
        new createjs.Point(((837.3333) * transformX) + moveX, ((342.0000) * transformY) + moveY),

        new createjs.Point(((837.0000) * transformX) + moveX, ((342.0000) * transformY) + moveY),
        new createjs.Point(((836.6667) * transformX) + moveX, ((343.3332) * transformY) + moveY),
        new createjs.Point(((836.3333) * transformX) + moveX, ((344.6668) * transformY) + moveY),

        new createjs.Point(((836.0000) * transformX) + moveX, ((346.0000) * transformY) + moveY),
        new createjs.Point(((835.6667) * transformX) + moveX, ((346.0000) * transformY) + moveY),
        new createjs.Point(((835.3333) * transformX) + moveX, ((346.0000) * transformY) + moveY),

        new createjs.Point(((835.0000) * transformX) + moveX, ((346.0000) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((346.9999) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((348.0001) * transformY) + moveY),

        new createjs.Point(((835.0000) * transformX) + moveX, ((349.0000) * transformY) + moveY),
        new createjs.Point(((834.6667) * transformX) + moveX, ((349.0000) * transformY) + moveY),
        new createjs.Point(((834.3333) * transformX) + moveX, ((349.0000) * transformY) + moveY),

        new createjs.Point(((834.0000) * transformX) + moveX, ((349.0000) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((349.6666) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((350.3334) * transformY) + moveY),

        new createjs.Point(((834.0000) * transformX) + moveX, ((351.0000) * transformY) + moveY),
        new createjs.Point(((833.6667) * transformX) + moveX, ((351.0000) * transformY) + moveY),
        new createjs.Point(((833.3333) * transformX) + moveX, ((351.0000) * transformY) + moveY),

        new createjs.Point(((833.0000) * transformX) + moveX, ((351.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((351.9999) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((353.0001) * transformY) + moveY),

        new createjs.Point(((833.0000) * transformX) + moveX, ((354.0000) * transformY) + moveY),
        new createjs.Point(((832.6667) * transformX) + moveX, ((354.0000) * transformY) + moveY),
        new createjs.Point(((832.3333) * transformX) + moveX, ((354.0000) * transformY) + moveY),

        new createjs.Point(((832.0000) * transformX) + moveX, ((354.0000) * transformY) + moveY),
        new createjs.Point(((831.6667) * transformX) + moveX, ((355.3332) * transformY) + moveY),
        new createjs.Point(((831.3333) * transformX) + moveX, ((356.6668) * transformY) + moveY),

        new createjs.Point(((831.0000) * transformX) + moveX, ((358.0000) * transformY) + moveY),
        new createjs.Point(((830.6667) * transformX) + moveX, ((358.0000) * transformY) + moveY),
        new createjs.Point(((830.3333) * transformX) + moveX, ((358.0000) * transformY) + moveY),

        new createjs.Point(((830.0000) * transformX) + moveX, ((358.0000) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((358.9999) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((360.0001) * transformY) + moveY),

        new createjs.Point(((830.0000) * transformX) + moveX, ((361.0000) * transformY) + moveY),
        new createjs.Point(((829.6667) * transformX) + moveX, ((361.0000) * transformY) + moveY),
        new createjs.Point(((829.3333) * transformX) + moveX, ((361.0000) * transformY) + moveY),

        new createjs.Point(((829.0000) * transformX) + moveX, ((361.0000) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((361.6666) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((362.3334) * transformY) + moveY),

        new createjs.Point(((829.0000) * transformX) + moveX, ((363.0000) * transformY) + moveY),
        new createjs.Point(((828.6667) * transformX) + moveX, ((363.0000) * transformY) + moveY),
        new createjs.Point(((828.3333) * transformX) + moveX, ((363.0000) * transformY) + moveY),

        new createjs.Point(((828.0000) * transformX) + moveX, ((363.0000) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((363.9999) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((365.0001) * transformY) + moveY),

        new createjs.Point(((828.0000) * transformX) + moveX, ((366.0000) * transformY) + moveY),
        new createjs.Point(((827.6667) * transformX) + moveX, ((366.0000) * transformY) + moveY),
        new createjs.Point(((827.3333) * transformX) + moveX, ((366.0000) * transformY) + moveY),

        new createjs.Point(((827.0000) * transformX) + moveX, ((366.0000) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((366.6666) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((367.3334) * transformY) + moveY),

        new createjs.Point(((827.0000) * transformX) + moveX, ((368.0000) * transformY) + moveY),
        new createjs.Point(((826.6667) * transformX) + moveX, ((368.0000) * transformY) + moveY),
        new createjs.Point(((826.3333) * transformX) + moveX, ((368.0000) * transformY) + moveY),

        new createjs.Point(((826.0000) * transformX) + moveX, ((368.0000) * transformY) + moveY),
        new createjs.Point(((825.0001) * transformX) + moveX, ((371.3330) * transformY) + moveY),
        new createjs.Point(((823.9999) * transformX) + moveX, ((374.6670) * transformY) + moveY),

        new createjs.Point(((823.0000) * transformX) + moveX, ((378.0000) * transformY) + moveY),
        new createjs.Point(((822.6667) * transformX) + moveX, ((378.0000) * transformY) + moveY),
        new createjs.Point(((822.3333) * transformX) + moveX, ((378.0000) * transformY) + moveY),

        new createjs.Point(((822.0000) * transformX) + moveX, ((378.0000) * transformY) + moveY),
        new createjs.Point(((821.6667) * transformX) + moveX, ((379.3332) * transformY) + moveY),
        new createjs.Point(((821.3333) * transformX) + moveX, ((380.6668) * transformY) + moveY),

        new createjs.Point(((821.0000) * transformX) + moveX, ((382.0000) * transformY) + moveY),
        new createjs.Point(((820.6667) * transformX) + moveX, ((382.0000) * transformY) + moveY),
        new createjs.Point(((820.3333) * transformX) + moveX, ((382.0000) * transformY) + moveY),

        new createjs.Point(((820.0000) * transformX) + moveX, ((382.0000) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((382.9999) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((384.0001) * transformY) + moveY),

        new createjs.Point(((820.0000) * transformX) + moveX, ((385.0000) * transformY) + moveY),
        new createjs.Point(((819.6667) * transformX) + moveX, ((385.0000) * transformY) + moveY),
        new createjs.Point(((819.3333) * transformX) + moveX, ((385.0000) * transformY) + moveY),

        new createjs.Point(((819.0000) * transformX) + moveX, ((385.0000) * transformY) + moveY),
        new createjs.Point(((818.6667) * transformX) + moveX, ((386.6665) * transformY) + moveY),
        new createjs.Point(((818.3333) * transformX) + moveX, ((388.3335) * transformY) + moveY),

        new createjs.Point(((818.0000) * transformX) + moveX, ((390.0000) * transformY) + moveY),
        new createjs.Point(((817.6667) * transformX) + moveX, ((390.0000) * transformY) + moveY),
        new createjs.Point(((817.3333) * transformX) + moveX, ((390.0000) * transformY) + moveY),

        new createjs.Point(((817.0000) * transformX) + moveX, ((390.0000) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((390.6666) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((391.3334) * transformY) + moveY),

        new createjs.Point(((817.0000) * transformX) + moveX, ((392.0000) * transformY) + moveY),
        new createjs.Point(((816.6667) * transformX) + moveX, ((392.0000) * transformY) + moveY),
        new createjs.Point(((816.3333) * transformX) + moveX, ((392.0000) * transformY) + moveY),

        new createjs.Point(((816.0000) * transformX) + moveX, ((392.0000) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((392.9999) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((394.0001) * transformY) + moveY),

        new createjs.Point(((816.0000) * transformX) + moveX, ((395.0000) * transformY) + moveY),
        new createjs.Point(((785.6797) * transformX) + moveX, ((462.4323) * transformY) + moveY),
        new createjs.Point(((759.3766) * transformX) + moveX, ((533.6027) * transformY) + moveY),

        new createjs.Point(((729.0000) * transformX) + moveX, ((601.0000) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((601.9999) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((603.0001) * transformY) + moveY),

        new createjs.Point(((729.0000) * transformX) + moveX, ((604.0000) * transformY) + moveY),
        new createjs.Point(((728.6667) * transformX) + moveX, ((604.0000) * transformY) + moveY),
        new createjs.Point(((728.3333) * transformX) + moveX, ((604.0000) * transformY) + moveY),

        new createjs.Point(((728.0000) * transformX) + moveX, ((604.0000) * transformY) + moveY),
        new createjs.Point(((727.6667) * transformX) + moveX, ((605.6665) * transformY) + moveY),
        new createjs.Point(((727.3333) * transformX) + moveX, ((607.3335) * transformY) + moveY),

        new createjs.Point(((727.0000) * transformX) + moveX, ((609.0000) * transformY) + moveY),
        new createjs.Point(((726.6667) * transformX) + moveX, ((609.0000) * transformY) + moveY),
        new createjs.Point(((726.3333) * transformX) + moveX, ((609.0000) * transformY) + moveY),

        new createjs.Point(((726.0000) * transformX) + moveX, ((609.0000) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((609.6666) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((610.3334) * transformY) + moveY),

        new createjs.Point(((726.0000) * transformX) + moveX, ((611.0000) * transformY) + moveY),
        new createjs.Point(((725.6667) * transformX) + moveX, ((611.0000) * transformY) + moveY),
        new createjs.Point(((725.3333) * transformX) + moveX, ((611.0000) * transformY) + moveY),

        new createjs.Point(((725.0000) * transformX) + moveX, ((611.0000) * transformY) + moveY),
        new createjs.Point(((725.0000) * transformX) + moveX, ((611.9999) * transformY) + moveY),
        new createjs.Point(((725.0000) * transformX) + moveX, ((613.0001) * transformY) + moveY),

        new createjs.Point(((725.0000) * transformX) + moveX, ((614.0000) * transformY) + moveY),
        new createjs.Point(((724.6667) * transformX) + moveX, ((614.0000) * transformY) + moveY),
        new createjs.Point(((724.3333) * transformX) + moveX, ((614.0000) * transformY) + moveY),

        new createjs.Point(((724.0000) * transformX) + moveX, ((614.0000) * transformY) + moveY),
        new createjs.Point(((723.6667) * transformX) + moveX, ((615.3332) * transformY) + moveY),
        new createjs.Point(((723.3333) * transformX) + moveX, ((616.6668) * transformY) + moveY),

        new createjs.Point(((723.0000) * transformX) + moveX, ((618.0000) * transformY) + moveY),
        new createjs.Point(((722.6667) * transformX) + moveX, ((618.0000) * transformY) + moveY),
        new createjs.Point(((722.3333) * transformX) + moveX, ((618.0000) * transformY) + moveY),

        new createjs.Point(((722.0000) * transformX) + moveX, ((618.0000) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((618.9999) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((620.0001) * transformY) + moveY),

        new createjs.Point(((722.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),
        new createjs.Point(((721.6667) * transformX) + moveX, ((621.0000) * transformY) + moveY),
        new createjs.Point(((721.3333) * transformX) + moveX, ((621.0000) * transformY) + moveY),

        new createjs.Point(((721.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),
        new createjs.Point(((720.6667) * transformX) + moveX, ((622.6665) * transformY) + moveY),
        new createjs.Point(((720.3333) * transformX) + moveX, ((624.3335) * transformY) + moveY),

        new createjs.Point(((720.0000) * transformX) + moveX, ((626.0000) * transformY) + moveY),
        new createjs.Point(((719.6667) * transformX) + moveX, ((626.0000) * transformY) + moveY),
        new createjs.Point(((719.3333) * transformX) + moveX, ((626.0000) * transformY) + moveY),

        new createjs.Point(((719.0000) * transformX) + moveX, ((626.0000) * transformY) + moveY),
        new createjs.Point(((718.6667) * transformX) + moveX, ((627.3332) * transformY) + moveY),
        new createjs.Point(((718.3333) * transformX) + moveX, ((628.6668) * transformY) + moveY),

        new createjs.Point(((718.0000) * transformX) + moveX, ((630.0000) * transformY) + moveY),
        new createjs.Point(((717.6667) * transformX) + moveX, ((630.0000) * transformY) + moveY),
        new createjs.Point(((717.3333) * transformX) + moveX, ((630.0000) * transformY) + moveY),

        new createjs.Point(((717.0000) * transformX) + moveX, ((630.0000) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((630.9999) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((632.0001) * transformY) + moveY),

        new createjs.Point(((717.0000) * transformX) + moveX, ((633.0000) * transformY) + moveY),
        new createjs.Point(((716.6667) * transformX) + moveX, ((633.0000) * transformY) + moveY),
        new createjs.Point(((716.3333) * transformX) + moveX, ((633.0000) * transformY) + moveY),

        new createjs.Point(((716.0000) * transformX) + moveX, ((633.0000) * transformY) + moveY),
        new createjs.Point(((716.0000) * transformX) + moveX, ((633.6666) * transformY) + moveY),
        new createjs.Point(((716.0000) * transformX) + moveX, ((634.3334) * transformY) + moveY),

        new createjs.Point(((716.0000) * transformX) + moveX, ((635.0000) * transformY) + moveY),
        new createjs.Point(((715.6667) * transformX) + moveX, ((635.0000) * transformY) + moveY),
        new createjs.Point(((715.3333) * transformX) + moveX, ((635.0000) * transformY) + moveY),

        new createjs.Point(((715.0000) * transformX) + moveX, ((635.0000) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((635.9999) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((637.0001) * transformY) + moveY),

        new createjs.Point(((715.0000) * transformX) + moveX, ((638.0000) * transformY) + moveY),
        new createjs.Point(((714.6667) * transformX) + moveX, ((638.0000) * transformY) + moveY),
        new createjs.Point(((714.3333) * transformX) + moveX, ((638.0000) * transformY) + moveY),

        new createjs.Point(((714.0000) * transformX) + moveX, ((638.0000) * transformY) + moveY),
        new createjs.Point(((713.6667) * transformX) + moveX, ((639.3332) * transformY) + moveY),
        new createjs.Point(((713.3333) * transformX) + moveX, ((640.6668) * transformY) + moveY),

        new createjs.Point(((713.0000) * transformX) + moveX, ((642.0000) * transformY) + moveY),
        new createjs.Point(((712.6667) * transformX) + moveX, ((642.0000) * transformY) + moveY),
        new createjs.Point(((712.3333) * transformX) + moveX, ((642.0000) * transformY) + moveY),

        new createjs.Point(((712.0000) * transformX) + moveX, ((642.0000) * transformY) + moveY),
        new createjs.Point(((712.0000) * transformX) + moveX, ((642.9999) * transformY) + moveY),
        new createjs.Point(((712.0000) * transformX) + moveX, ((644.0001) * transformY) + moveY),

        new createjs.Point(((712.0000) * transformX) + moveX, ((645.0000) * transformY) + moveY),
        new createjs.Point(((711.6667) * transformX) + moveX, ((645.0000) * transformY) + moveY),
        new createjs.Point(((711.3333) * transformX) + moveX, ((645.0000) * transformY) + moveY),

        new createjs.Point(((711.0000) * transformX) + moveX, ((645.0000) * transformY) + moveY),
        new createjs.Point(((710.6667) * transformX) + moveX, ((646.6665) * transformY) + moveY),
        new createjs.Point(((710.3333) * transformX) + moveX, ((648.3335) * transformY) + moveY),

        new createjs.Point(((710.0000) * transformX) + moveX, ((650.0000) * transformY) + moveY),
        new createjs.Point(((709.6667) * transformX) + moveX, ((650.0000) * transformY) + moveY),
        new createjs.Point(((709.3333) * transformX) + moveX, ((650.0000) * transformY) + moveY),

        new createjs.Point(((709.0000) * transformX) + moveX, ((650.0000) * transformY) + moveY),
        new createjs.Point(((708.6667) * transformX) + moveX, ((651.3332) * transformY) + moveY),
        new createjs.Point(((708.3333) * transformX) + moveX, ((652.6668) * transformY) + moveY),

        new createjs.Point(((708.0000) * transformX) + moveX, ((654.0000) * transformY) + moveY),
        new createjs.Point(((707.6667) * transformX) + moveX, ((654.0000) * transformY) + moveY),
        new createjs.Point(((707.3333) * transformX) + moveX, ((654.0000) * transformY) + moveY),

        new createjs.Point(((707.0000) * transformX) + moveX, ((654.0000) * transformY) + moveY),
        new createjs.Point(((707.0000) * transformX) + moveX, ((654.9999) * transformY) + moveY),
        new createjs.Point(((707.0000) * transformX) + moveX, ((656.0001) * transformY) + moveY),

        new createjs.Point(((707.0000) * transformX) + moveX, ((657.0000) * transformY) + moveY),
        new createjs.Point(((706.6667) * transformX) + moveX, ((657.0000) * transformY) + moveY),
        new createjs.Point(((706.3333) * transformX) + moveX, ((657.0000) * transformY) + moveY),

        new createjs.Point(((706.0000) * transformX) + moveX, ((657.0000) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((657.6666) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((658.3334) * transformY) + moveY),

        new createjs.Point(((706.0000) * transformX) + moveX, ((659.0000) * transformY) + moveY),
        new createjs.Point(((705.6667) * transformX) + moveX, ((659.0000) * transformY) + moveY),
        new createjs.Point(((705.3333) * transformX) + moveX, ((659.0000) * transformY) + moveY),

        new createjs.Point(((705.0000) * transformX) + moveX, ((659.0000) * transformY) + moveY),
        new createjs.Point(((705.0000) * transformX) + moveX, ((659.9999) * transformY) + moveY),
        new createjs.Point(((705.0000) * transformX) + moveX, ((661.0001) * transformY) + moveY),

        new createjs.Point(((705.0000) * transformX) + moveX, ((662.0000) * transformY) + moveY),
        new createjs.Point(((704.6667) * transformX) + moveX, ((662.0000) * transformY) + moveY),
        new createjs.Point(((704.3333) * transformX) + moveX, ((662.0000) * transformY) + moveY),

        new createjs.Point(((704.0000) * transformX) + moveX, ((662.0000) * transformY) + moveY),
        new createjs.Point(((703.6667) * transformX) + moveX, ((663.3332) * transformY) + moveY),
        new createjs.Point(((703.3333) * transformX) + moveX, ((664.6668) * transformY) + moveY),

        new createjs.Point(((703.0000) * transformX) + moveX, ((666.0000) * transformY) + moveY),
        new createjs.Point(((702.6667) * transformX) + moveX, ((666.0000) * transformY) + moveY),
        new createjs.Point(((702.3333) * transformX) + moveX, ((666.0000) * transformY) + moveY),

        new createjs.Point(((702.0000) * transformX) + moveX, ((666.0000) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((666.9999) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((668.0001) * transformY) + moveY),

        new createjs.Point(((702.0000) * transformX) + moveX, ((669.0000) * transformY) + moveY),
        new createjs.Point(((701.6667) * transformX) + moveX, ((669.0000) * transformY) + moveY),
        new createjs.Point(((701.3333) * transformX) + moveX, ((669.0000) * transformY) + moveY),

        new createjs.Point(((701.0000) * transformX) + moveX, ((669.0000) * transformY) + moveY),
        new createjs.Point(((701.0000) * transformX) + moveX, ((669.6666) * transformY) + moveY),
        new createjs.Point(((701.0000) * transformX) + moveX, ((670.3334) * transformY) + moveY),

        new createjs.Point(((701.0000) * transformX) + moveX, ((671.0000) * transformY) + moveY),
        new createjs.Point(((700.6667) * transformX) + moveX, ((671.0000) * transformY) + moveY),
        new createjs.Point(((700.3333) * transformX) + moveX, ((671.0000) * transformY) + moveY),

        new createjs.Point(((700.0000) * transformX) + moveX, ((671.0000) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((671.9999) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((673.0001) * transformY) + moveY),

        new createjs.Point(((700.0000) * transformX) + moveX, ((674.0000) * transformY) + moveY),
        new createjs.Point(((699.6667) * transformX) + moveX, ((674.0000) * transformY) + moveY),
        new createjs.Point(((699.3333) * transformX) + moveX, ((674.0000) * transformY) + moveY),

        new createjs.Point(((699.0000) * transformX) + moveX, ((674.0000) * transformY) + moveY),
        new createjs.Point(((698.6667) * transformX) + moveX, ((675.3332) * transformY) + moveY),
        new createjs.Point(((698.3333) * transformX) + moveX, ((676.6668) * transformY) + moveY),

        new createjs.Point(((698.0000) * transformX) + moveX, ((678.0000) * transformY) + moveY),
        new createjs.Point(((697.6667) * transformX) + moveX, ((678.0000) * transformY) + moveY),
        new createjs.Point(((697.3333) * transformX) + moveX, ((678.0000) * transformY) + moveY),

        new createjs.Point(((697.0000) * transformX) + moveX, ((678.0000) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((678.9999) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((680.0001) * transformY) + moveY),

        new createjs.Point(((697.0000) * transformX) + moveX, ((681.0000) * transformY) + moveY),
        new createjs.Point(((696.6667) * transformX) + moveX, ((681.0000) * transformY) + moveY),
        new createjs.Point(((696.3333) * transformX) + moveX, ((681.0000) * transformY) + moveY),

        new createjs.Point(((696.0000) * transformX) + moveX, ((681.0000) * transformY) + moveY),
        new createjs.Point(((695.6667) * transformX) + moveX, ((682.6665) * transformY) + moveY),
        new createjs.Point(((695.3333) * transformX) + moveX, ((684.3335) * transformY) + moveY),

        new createjs.Point(((695.0000) * transformX) + moveX, ((686.0000) * transformY) + moveY),
        new createjs.Point(((694.6667) * transformX) + moveX, ((686.0000) * transformY) + moveY),
        new createjs.Point(((694.3333) * transformX) + moveX, ((686.0000) * transformY) + moveY),

        new createjs.Point(((694.0000) * transformX) + moveX, ((686.0000) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((686.6666) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((687.3334) * transformY) + moveY),

        new createjs.Point(((694.0000) * transformX) + moveX, ((688.0000) * transformY) + moveY),
        new createjs.Point(((693.6667) * transformX) + moveX, ((688.0000) * transformY) + moveY),
        new createjs.Point(((693.3333) * transformX) + moveX, ((688.0000) * transformY) + moveY),

        new createjs.Point(((693.0000) * transformX) + moveX, ((688.0000) * transformY) + moveY),
        new createjs.Point(((693.0000) * transformX) + moveX, ((688.9999) * transformY) + moveY),
        new createjs.Point(((693.0000) * transformX) + moveX, ((690.0001) * transformY) + moveY),

        new createjs.Point(((693.0000) * transformX) + moveX, ((691.0000) * transformY) + moveY),
        new createjs.Point(((692.6667) * transformX) + moveX, ((691.0000) * transformY) + moveY),
        new createjs.Point(((692.3333) * transformX) + moveX, ((691.0000) * transformY) + moveY),

        new createjs.Point(((692.0000) * transformX) + moveX, ((691.0000) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((691.6666) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((692.3334) * transformY) + moveY),

        new createjs.Point(((692.0000) * transformX) + moveX, ((693.0000) * transformY) + moveY),
        new createjs.Point(((691.6667) * transformX) + moveX, ((693.0000) * transformY) + moveY),
        new createjs.Point(((691.3333) * transformX) + moveX, ((693.0000) * transformY) + moveY),

        new createjs.Point(((691.0000) * transformX) + moveX, ((693.0000) * transformY) + moveY),
        new createjs.Point(((690.0001) * transformX) + moveX, ((696.3330) * transformY) + moveY),
        new createjs.Point(((688.9999) * transformX) + moveX, ((699.6670) * transformY) + moveY),

        new createjs.Point(((688.0000) * transformX) + moveX, ((703.0000) * transformY) + moveY),
        new createjs.Point(((687.6667) * transformX) + moveX, ((703.0000) * transformY) + moveY),
        new createjs.Point(((687.3333) * transformX) + moveX, ((703.0000) * transformY) + moveY),

        new createjs.Point(((687.0000) * transformX) + moveX, ((703.0000) * transformY) + moveY),
        new createjs.Point(((686.6667) * transformX) + moveX, ((704.3332) * transformY) + moveY),
        new createjs.Point(((686.3333) * transformX) + moveX, ((705.6668) * transformY) + moveY),

        new createjs.Point(((686.0000) * transformX) + moveX, ((707.0000) * transformY) + moveY),
        new createjs.Point(((685.6667) * transformX) + moveX, ((707.0000) * transformY) + moveY),
        new createjs.Point(((685.3333) * transformX) + moveX, ((707.0000) * transformY) + moveY),

        new createjs.Point(((685.0000) * transformX) + moveX, ((707.0000) * transformY) + moveY),
        new createjs.Point(((685.0000) * transformX) + moveX, ((707.9999) * transformY) + moveY),
        new createjs.Point(((685.0000) * transformX) + moveX, ((709.0001) * transformY) + moveY),

        new createjs.Point(((685.0000) * transformX) + moveX, ((710.0000) * transformY) + moveY),
        new createjs.Point(((684.6667) * transformX) + moveX, ((710.0000) * transformY) + moveY),
        new createjs.Point(((684.3333) * transformX) + moveX, ((710.0000) * transformY) + moveY),

        new createjs.Point(((684.0000) * transformX) + moveX, ((710.0000) * transformY) + moveY),
        new createjs.Point(((683.6667) * transformX) + moveX, ((711.6665) * transformY) + moveY),
        new createjs.Point(((683.3333) * transformX) + moveX, ((713.3335) * transformY) + moveY),

        new createjs.Point(((683.0000) * transformX) + moveX, ((715.0000) * transformY) + moveY),
        new createjs.Point(((682.6667) * transformX) + moveX, ((715.0000) * transformY) + moveY),
        new createjs.Point(((682.3333) * transformX) + moveX, ((715.0000) * transformY) + moveY),

        new createjs.Point(((682.0000) * transformX) + moveX, ((715.0000) * transformY) + moveY),
        new createjs.Point(((681.6667) * transformX) + moveX, ((716.3332) * transformY) + moveY),
        new createjs.Point(((681.3333) * transformX) + moveX, ((717.6668) * transformY) + moveY),

        new createjs.Point(((681.0000) * transformX) + moveX, ((719.0000) * transformY) + moveY),
        new createjs.Point(((680.6667) * transformX) + moveX, ((719.0000) * transformY) + moveY),
        new createjs.Point(((680.3333) * transformX) + moveX, ((719.0000) * transformY) + moveY),

        new createjs.Point(((680.0000) * transformX) + moveX, ((719.0000) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((719.9999) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((721.0001) * transformY) + moveY),

        new createjs.Point(((680.0000) * transformX) + moveX, ((722.0000) * transformY) + moveY),
        new createjs.Point(((679.6667) * transformX) + moveX, ((722.0000) * transformY) + moveY),
        new createjs.Point(((679.3333) * transformX) + moveX, ((722.0000) * transformY) + moveY),

        new createjs.Point(((679.0000) * transformX) + moveX, ((722.0000) * transformY) + moveY),
        new createjs.Point(((679.0000) * transformX) + moveX, ((722.6666) * transformY) + moveY),
        new createjs.Point(((679.0000) * transformX) + moveX, ((723.3334) * transformY) + moveY),

        new createjs.Point(((679.0000) * transformX) + moveX, ((724.0000) * transformY) + moveY),
        new createjs.Point(((678.6667) * transformX) + moveX, ((724.0000) * transformY) + moveY),
        new createjs.Point(((678.3333) * transformX) + moveX, ((724.0000) * transformY) + moveY),

        new createjs.Point(((678.0000) * transformX) + moveX, ((724.0000) * transformY) + moveY),
        new createjs.Point(((678.0000) * transformX) + moveX, ((724.9999) * transformY) + moveY),
        new createjs.Point(((678.0000) * transformX) + moveX, ((726.0001) * transformY) + moveY),

        new createjs.Point(((678.0000) * transformX) + moveX, ((727.0000) * transformY) + moveY),
        new createjs.Point(((677.6667) * transformX) + moveX, ((727.0000) * transformY) + moveY),
        new createjs.Point(((677.3333) * transformX) + moveX, ((727.0000) * transformY) + moveY),

        new createjs.Point(((677.0000) * transformX) + moveX, ((727.0000) * transformY) + moveY),
        new createjs.Point(((676.6667) * transformX) + moveX, ((728.3332) * transformY) + moveY),
        new createjs.Point(((676.3333) * transformX) + moveX, ((729.6668) * transformY) + moveY),

        new createjs.Point(((676.0000) * transformX) + moveX, ((731.0000) * transformY) + moveY),
        new createjs.Point(((675.6667) * transformX) + moveX, ((731.0000) * transformY) + moveY),
        new createjs.Point(((675.3333) * transformX) + moveX, ((731.0000) * transformY) + moveY),

        new createjs.Point(((675.0000) * transformX) + moveX, ((731.0000) * transformY) + moveY),
        new createjs.Point(((675.0000) * transformX) + moveX, ((731.9999) * transformY) + moveY),
        new createjs.Point(((675.0000) * transformX) + moveX, ((733.0001) * transformY) + moveY),

        new createjs.Point(((675.0000) * transformX) + moveX, ((734.0000) * transformY) + moveY),
        new createjs.Point(((674.6667) * transformX) + moveX, ((734.0000) * transformY) + moveY),
        new createjs.Point(((674.3333) * transformX) + moveX, ((734.0000) * transformY) + moveY),

        new createjs.Point(((674.0000) * transformX) + moveX, ((734.0000) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((734.6666) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((735.3334) * transformY) + moveY),

        new createjs.Point(((674.0000) * transformX) + moveX, ((736.0000) * transformY) + moveY),
        new createjs.Point(((673.6667) * transformX) + moveX, ((736.0000) * transformY) + moveY),
        new createjs.Point(((673.3333) * transformX) + moveX, ((736.0000) * transformY) + moveY),

        new createjs.Point(((673.0000) * transformX) + moveX, ((736.0000) * transformY) + moveY),
        new createjs.Point(((673.0000) * transformX) + moveX, ((736.9999) * transformY) + moveY),
        new createjs.Point(((673.0000) * transformX) + moveX, ((738.0001) * transformY) + moveY),

        new createjs.Point(((673.0000) * transformX) + moveX, ((739.0000) * transformY) + moveY),
        new createjs.Point(((672.6667) * transformX) + moveX, ((739.0000) * transformY) + moveY),
        new createjs.Point(((672.3333) * transformX) + moveX, ((739.0000) * transformY) + moveY),

        new createjs.Point(((672.0000) * transformX) + moveX, ((739.0000) * transformY) + moveY),
        new createjs.Point(((671.6667) * transformX) + moveX, ((740.3332) * transformY) + moveY),
        new createjs.Point(((671.3333) * transformX) + moveX, ((741.6668) * transformY) + moveY),

        new createjs.Point(((671.0000) * transformX) + moveX, ((743.0000) * transformY) + moveY),
        new createjs.Point(((670.6667) * transformX) + moveX, ((743.0000) * transformY) + moveY),
        new createjs.Point(((670.3333) * transformX) + moveX, ((743.0000) * transformY) + moveY),

        new createjs.Point(((670.0000) * transformX) + moveX, ((743.0000) * transformY) + moveY),
        new createjs.Point(((670.0000) * transformX) + moveX, ((743.9999) * transformY) + moveY),
        new createjs.Point(((670.0000) * transformX) + moveX, ((745.0001) * transformY) + moveY),

        new createjs.Point(((670.0000) * transformX) + moveX, ((746.0000) * transformY) + moveY),
        new createjs.Point(((669.6667) * transformX) + moveX, ((746.0000) * transformY) + moveY),
        new createjs.Point(((669.3333) * transformX) + moveX, ((746.0000) * transformY) + moveY),

        new createjs.Point(((669.0000) * transformX) + moveX, ((746.0000) * transformY) + moveY),
        new createjs.Point(((669.0000) * transformX) + moveX, ((746.6666) * transformY) + moveY),
        new createjs.Point(((669.0000) * transformX) + moveX, ((747.3334) * transformY) + moveY),

        new createjs.Point(((669.0000) * transformX) + moveX, ((748.0000) * transformY) + moveY),
        new createjs.Point(((668.6667) * transformX) + moveX, ((748.0000) * transformY) + moveY),
        new createjs.Point(((668.3333) * transformX) + moveX, ((748.0000) * transformY) + moveY),

        new createjs.Point(((668.0000) * transformX) + moveX, ((748.0000) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((748.9999) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((750.0001) * transformY) + moveY),

        new createjs.Point(((668.0000) * transformX) + moveX, ((751.0000) * transformY) + moveY),
        new createjs.Point(((667.6667) * transformX) + moveX, ((751.0000) * transformY) + moveY),
        new createjs.Point(((667.3333) * transformX) + moveX, ((751.0000) * transformY) + moveY),

        new createjs.Point(((667.0000) * transformX) + moveX, ((751.0000) * transformY) + moveY),
        new createjs.Point(((666.6667) * transformX) + moveX, ((752.3332) * transformY) + moveY),
        new createjs.Point(((666.3333) * transformX) + moveX, ((753.6668) * transformY) + moveY),

        new createjs.Point(((666.0000) * transformX) + moveX, ((755.0000) * transformY) + moveY),
        new createjs.Point(((665.6667) * transformX) + moveX, ((755.0000) * transformY) + moveY),
        new createjs.Point(((665.3333) * transformX) + moveX, ((755.0000) * transformY) + moveY),

        new createjs.Point(((665.0000) * transformX) + moveX, ((755.0000) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((755.9999) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((757.0001) * transformY) + moveY),

        new createjs.Point(((665.0000) * transformX) + moveX, ((758.0000) * transformY) + moveY),
        new createjs.Point(((664.6667) * transformX) + moveX, ((758.0000) * transformY) + moveY),
        new createjs.Point(((664.3333) * transformX) + moveX, ((758.0000) * transformY) + moveY),

        new createjs.Point(((664.0000) * transformX) + moveX, ((758.0000) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((758.6666) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((759.3334) * transformY) + moveY),

        new createjs.Point(((664.0000) * transformX) + moveX, ((760.0000) * transformY) + moveY),
        new createjs.Point(((663.6667) * transformX) + moveX, ((760.0000) * transformY) + moveY),
        new createjs.Point(((663.3333) * transformX) + moveX, ((760.0000) * transformY) + moveY),

        new createjs.Point(((663.0000) * transformX) + moveX, ((760.0000) * transformY) + moveY),
        new createjs.Point(((663.0000) * transformX) + moveX, ((760.9999) * transformY) + moveY),
        new createjs.Point(((663.0000) * transformX) + moveX, ((762.0001) * transformY) + moveY),

        new createjs.Point(((663.0000) * transformX) + moveX, ((763.0000) * transformY) + moveY),
        new createjs.Point(((662.6667) * transformX) + moveX, ((763.0000) * transformY) + moveY),
        new createjs.Point(((662.3333) * transformX) + moveX, ((763.0000) * transformY) + moveY),

        new createjs.Point(((662.0000) * transformX) + moveX, ((763.0000) * transformY) + moveY),
        new createjs.Point(((661.6667) * transformX) + moveX, ((764.6665) * transformY) + moveY),
        new createjs.Point(((661.3333) * transformX) + moveX, ((766.3335) * transformY) + moveY),

        new createjs.Point(((661.0000) * transformX) + moveX, ((768.0000) * transformY) + moveY),
        new createjs.Point(((660.6667) * transformX) + moveX, ((768.0000) * transformY) + moveY),
        new createjs.Point(((660.3333) * transformX) + moveX, ((768.0000) * transformY) + moveY),

        new createjs.Point(((660.0000) * transformX) + moveX, ((768.0000) * transformY) + moveY),
        new createjs.Point(((659.6667) * transformX) + moveX, ((769.3332) * transformY) + moveY),
        new createjs.Point(((659.3333) * transformX) + moveX, ((770.6668) * transformY) + moveY),

        new createjs.Point(((659.0000) * transformX) + moveX, ((772.0000) * transformY) + moveY),
        new createjs.Point(((658.6667) * transformX) + moveX, ((772.0000) * transformY) + moveY),
        new createjs.Point(((658.3333) * transformX) + moveX, ((772.0000) * transformY) + moveY),

        new createjs.Point(((658.0000) * transformX) + moveX, ((772.0000) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((772.9999) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((774.0001) * transformY) + moveY),

        new createjs.Point(((658.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),
        new createjs.Point(((657.6667) * transformX) + moveX, ((775.0000) * transformY) + moveY),
        new createjs.Point(((657.3333) * transformX) + moveX, ((775.0000) * transformY) + moveY),

        new createjs.Point(((657.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),
        new createjs.Point(((657.0000) * transformX) + moveX, ((775.6666) * transformY) + moveY),
        new createjs.Point(((657.0000) * transformX) + moveX, ((776.3334) * transformY) + moveY),

        new createjs.Point(((657.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((656.6667) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((656.3333) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((656.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((656.0000) * transformX) + moveX, ((777.9999) * transformY) + moveY),
        new createjs.Point(((656.0000) * transformX) + moveX, ((779.0001) * transformY) + moveY),

        new createjs.Point(((656.0000) * transformX) + moveX, ((780.0000) * transformY) + moveY),
        new createjs.Point(((627.4560) * transformX) + moveX, ((843.4789) * transformY) + moveY),
        new createjs.Point(((602.5955) * transformX) + moveX, ((910.5544) * transformY) + moveY),

        new createjs.Point(((574.0000) * transformX) + moveX, ((974.0000) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((974.9999) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((976.0001) * transformY) + moveY),

        new createjs.Point(((574.0000) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((573.6667) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((573.3333) * transformX) + moveX, ((977.0000) * transformY) + moveY),

        new createjs.Point(((573.0000) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((572.6667) * transformX) + moveX, ((978.6665) * transformY) + moveY),
        new createjs.Point(((572.3333) * transformX) + moveX, ((980.3335) * transformY) + moveY),

        new createjs.Point(((572.0000) * transformX) + moveX, ((982.0000) * transformY) + moveY),
        new createjs.Point(((571.6667) * transformX) + moveX, ((982.0000) * transformY) + moveY),
        new createjs.Point(((571.3333) * transformX) + moveX, ((982.0000) * transformY) + moveY),

        new createjs.Point(((571.0000) * transformX) + moveX, ((982.0000) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((982.6666) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((983.3334) * transformY) + moveY),

        new createjs.Point(((571.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((570.6667) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((570.3333) * transformX) + moveX, ((984.0000) * transformY) + moveY),

        new createjs.Point(((570.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((984.9999) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((986.0001) * transformY) + moveY),

        new createjs.Point(((570.0000) * transformX) + moveX, ((987.0000) * transformY) + moveY),
        new createjs.Point(((569.6667) * transformX) + moveX, ((987.0000) * transformY) + moveY),
        new createjs.Point(((569.3333) * transformX) + moveX, ((987.0000) * transformY) + moveY),

        new createjs.Point(((569.0000) * transformX) + moveX, ((987.0000) * transformY) + moveY),
        new createjs.Point(((568.6667) * transformX) + moveX, ((988.3332) * transformY) + moveY),
        new createjs.Point(((568.3333) * transformX) + moveX, ((989.6668) * transformY) + moveY),

        new createjs.Point(((568.0000) * transformX) + moveX, ((991.0000) * transformY) + moveY),
        new createjs.Point(((567.6667) * transformX) + moveX, ((991.0000) * transformY) + moveY),
        new createjs.Point(((567.3333) * transformX) + moveX, ((991.0000) * transformY) + moveY),

        new createjs.Point(((567.0000) * transformX) + moveX, ((991.0000) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((991.9999) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((993.0001) * transformY) + moveY),

        new createjs.Point(((567.0000) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((566.6667) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((566.3333) * transformX) + moveX, ((994.0000) * transformY) + moveY),

        new createjs.Point(((566.0000) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((994.6666) * transformY) + moveY),
        new createjs.Point(((566.0000) * transformX) + moveX, ((995.3334) * transformY) + moveY),

        new createjs.Point(((566.0000) * transformX) + moveX, ((996.0000) * transformY) + moveY),
        new createjs.Point(((565.6667) * transformX) + moveX, ((996.0000) * transformY) + moveY),
        new createjs.Point(((565.3333) * transformX) + moveX, ((996.0000) * transformY) + moveY),

        new createjs.Point(((565.0000) * transformX) + moveX, ((996.0000) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((996.9999) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((998.0001) * transformY) + moveY),

        new createjs.Point(((565.0000) * transformX) + moveX, ((999.0000) * transformY) + moveY),
        new createjs.Point(((564.6667) * transformX) + moveX, ((999.0000) * transformY) + moveY),
        new createjs.Point(((564.3333) * transformX) + moveX, ((999.0000) * transformY) + moveY),

        new createjs.Point(((564.0000) * transformX) + moveX, ((999.0000) * transformY) + moveY),
        new createjs.Point(((563.6667) * transformX) + moveX, ((1000.3332) * transformY) + moveY),
        new createjs.Point(((563.3333) * transformX) + moveX, ((1001.6668) * transformY) + moveY),

        new createjs.Point(((563.0000) * transformX) + moveX, ((1003.0000) * transformY) + moveY),
        new createjs.Point(((562.6667) * transformX) + moveX, ((1003.0000) * transformY) + moveY),
        new createjs.Point(((562.3333) * transformX) + moveX, ((1003.0000) * transformY) + moveY),

        new createjs.Point(((562.0000) * transformX) + moveX, ((1003.0000) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((1003.9999) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((1005.0001) * transformY) + moveY),

        new createjs.Point(((562.0000) * transformX) + moveX, ((1006.0000) * transformY) + moveY),
        new createjs.Point(((561.6667) * transformX) + moveX, ((1006.0000) * transformY) + moveY),
        new createjs.Point(((561.3333) * transformX) + moveX, ((1006.0000) * transformY) + moveY),

        new createjs.Point(((561.0000) * transformX) + moveX, ((1006.0000) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1006.6666) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1007.3334) * transformY) + moveY),

        new createjs.Point(((561.0000) * transformX) + moveX, ((1008.0000) * transformY) + moveY),
        new createjs.Point(((560.6667) * transformX) + moveX, ((1008.0000) * transformY) + moveY),
        new createjs.Point(((560.3333) * transformX) + moveX, ((1008.0000) * transformY) + moveY),

        new createjs.Point(((560.0000) * transformX) + moveX, ((1008.0000) * transformY) + moveY),
        new createjs.Point(((560.0000) * transformX) + moveX, ((1008.9999) * transformY) + moveY),
        new createjs.Point(((560.0000) * transformX) + moveX, ((1010.0001) * transformY) + moveY),

        new createjs.Point(((560.0000) * transformX) + moveX, ((1011.0000) * transformY) + moveY),
        new createjs.Point(((559.6667) * transformX) + moveX, ((1011.0000) * transformY) + moveY),
        new createjs.Point(((559.3333) * transformX) + moveX, ((1011.0000) * transformY) + moveY),

        new createjs.Point(((559.0000) * transformX) + moveX, ((1011.0000) * transformY) + moveY),
        new createjs.Point(((558.6667) * transformX) + moveX, ((1012.3332) * transformY) + moveY),
        new createjs.Point(((558.3333) * transformX) + moveX, ((1013.6668) * transformY) + moveY),

        new createjs.Point(((558.0000) * transformX) + moveX, ((1015.0000) * transformY) + moveY),
        new createjs.Point(((557.6667) * transformX) + moveX, ((1015.0000) * transformY) + moveY),
        new createjs.Point(((557.3333) * transformX) + moveX, ((1015.0000) * transformY) + moveY),

        new createjs.Point(((557.0000) * transformX) + moveX, ((1015.0000) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1015.9999) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1017.0001) * transformY) + moveY),

        new createjs.Point(((557.0000) * transformX) + moveX, ((1018.0000) * transformY) + moveY),
        new createjs.Point(((556.6667) * transformX) + moveX, ((1018.0000) * transformY) + moveY),
        new createjs.Point(((556.3333) * transformX) + moveX, ((1018.0000) * transformY) + moveY),

        new createjs.Point(((556.0000) * transformX) + moveX, ((1018.0000) * transformY) + moveY),
        new createjs.Point(((556.0000) * transformX) + moveX, ((1018.6666) * transformY) + moveY),
        new createjs.Point(((556.0000) * transformX) + moveX, ((1019.3334) * transformY) + moveY),

        new createjs.Point(((556.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((555.6667) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((555.3333) * transformX) + moveX, ((1020.0000) * transformY) + moveY),

        new createjs.Point(((555.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((1020.9999) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((1022.0001) * transformY) + moveY),

        new createjs.Point(((555.0000) * transformX) + moveX, ((1023.0000) * transformY) + moveY),
        new createjs.Point(((554.6667) * transformX) + moveX, ((1023.0000) * transformY) + moveY),
        new createjs.Point(((554.3333) * transformX) + moveX, ((1023.0000) * transformY) + moveY),

        new createjs.Point(((554.0000) * transformX) + moveX, ((1023.0000) * transformY) + moveY),
        new createjs.Point(((553.6667) * transformX) + moveX, ((1024.3332) * transformY) + moveY),
        new createjs.Point(((553.3333) * transformX) + moveX, ((1025.6668) * transformY) + moveY),

        new createjs.Point(((553.0000) * transformX) + moveX, ((1027.0000) * transformY) + moveY),
        new createjs.Point(((552.6667) * transformX) + moveX, ((1027.0000) * transformY) + moveY),
        new createjs.Point(((552.3333) * transformX) + moveX, ((1027.0000) * transformY) + moveY),

        new createjs.Point(((552.0000) * transformX) + moveX, ((1027.0000) * transformY) + moveY),
        new createjs.Point(((552.0000) * transformX) + moveX, ((1027.9999) * transformY) + moveY),
        new createjs.Point(((552.0000) * transformX) + moveX, ((1029.0001) * transformY) + moveY),

        new createjs.Point(((552.0000) * transformX) + moveX, ((1030.0000) * transformY) + moveY),
        new createjs.Point(((551.6667) * transformX) + moveX, ((1030.0000) * transformY) + moveY),
        new createjs.Point(((551.3333) * transformX) + moveX, ((1030.0000) * transformY) + moveY),

        new createjs.Point(((551.0000) * transformX) + moveX, ((1030.0000) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1030.6666) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1031.3334) * transformY) + moveY),

        new createjs.Point(((551.0000) * transformX) + moveX, ((1032.0000) * transformY) + moveY),
        new createjs.Point(((550.6667) * transformX) + moveX, ((1032.0000) * transformY) + moveY),
        new createjs.Point(((550.3333) * transformX) + moveX, ((1032.0000) * transformY) + moveY),

        new createjs.Point(((550.0000) * transformX) + moveX, ((1032.0000) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1032.9999) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1034.0001) * transformY) + moveY),

        new createjs.Point(((550.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),
        new createjs.Point(((549.6667) * transformX) + moveX, ((1035.0000) * transformY) + moveY),
        new createjs.Point(((549.3333) * transformX) + moveX, ((1035.0000) * transformY) + moveY),

        new createjs.Point(((549.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),
        new createjs.Point(((549.0000) * transformX) + moveX, ((1035.6666) * transformY) + moveY),
        new createjs.Point(((549.0000) * transformX) + moveX, ((1036.3334) * transformY) + moveY),

        new createjs.Point(((549.0000) * transformX) + moveX, ((1037.0000) * transformY) + moveY),
        new createjs.Point(((548.6667) * transformX) + moveX, ((1037.0000) * transformY) + moveY),
        new createjs.Point(((548.3333) * transformX) + moveX, ((1037.0000) * transformY) + moveY),

        new createjs.Point(((548.0000) * transformX) + moveX, ((1037.0000) * transformY) + moveY),
        new createjs.Point(((547.0001) * transformX) + moveX, ((1040.3330) * transformY) + moveY),
        new createjs.Point(((545.9999) * transformX) + moveX, ((1043.6670) * transformY) + moveY),

        new createjs.Point(((545.0000) * transformX) + moveX, ((1047.0000) * transformY) + moveY),
        new createjs.Point(((544.6667) * transformX) + moveX, ((1047.0000) * transformY) + moveY),
        new createjs.Point(((544.3333) * transformX) + moveX, ((1047.0000) * transformY) + moveY),

        new createjs.Point(((544.0000) * transformX) + moveX, ((1047.0000) * transformY) + moveY),
        new createjs.Point(((543.6667) * transformX) + moveX, ((1048.3332) * transformY) + moveY),
        new createjs.Point(((543.3333) * transformX) + moveX, ((1049.6668) * transformY) + moveY),

        new createjs.Point(((543.0000) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((542.6667) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((542.3333) * transformX) + moveX, ((1051.0000) * transformY) + moveY),

        new createjs.Point(((542.0000) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1051.9999) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1053.0001) * transformY) + moveY),

        new createjs.Point(((542.0000) * transformX) + moveX, ((1054.0000) * transformY) + moveY),
        new createjs.Point(((541.6667) * transformX) + moveX, ((1054.0000) * transformY) + moveY),
        new createjs.Point(((541.3333) * transformX) + moveX, ((1054.0000) * transformY) + moveY),

        new createjs.Point(((541.0000) * transformX) + moveX, ((1054.0000) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((1054.6666) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((1055.3334) * transformY) + moveY),

        new createjs.Point(((541.0000) * transformX) + moveX, ((1056.0000) * transformY) + moveY),
        new createjs.Point(((540.6667) * transformX) + moveX, ((1056.0000) * transformY) + moveY),
        new createjs.Point(((540.3333) * transformX) + moveX, ((1056.0000) * transformY) + moveY),

        new createjs.Point(((540.0000) * transformX) + moveX, ((1056.0000) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((1056.9999) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((1058.0001) * transformY) + moveY),

        new createjs.Point(((540.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),
        new createjs.Point(((539.6667) * transformX) + moveX, ((1059.0000) * transformY) + moveY),
        new createjs.Point(((539.3333) * transformX) + moveX, ((1059.0000) * transformY) + moveY),

        new createjs.Point(((539.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1059.6666) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1060.3334) * transformY) + moveY),

        new createjs.Point(((539.0000) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((538.6667) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((538.3333) * transformX) + moveX, ((1061.0000) * transformY) + moveY),

        new createjs.Point(((538.0000) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((1061.9999) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((1063.0001) * transformY) + moveY),

        new createjs.Point(((538.0000) * transformX) + moveX, ((1064.0000) * transformY) + moveY),
        new createjs.Point(((537.6667) * transformX) + moveX, ((1064.0000) * transformY) + moveY),
        new createjs.Point(((537.3333) * transformX) + moveX, ((1064.0000) * transformY) + moveY),

        new createjs.Point(((537.0000) * transformX) + moveX, ((1064.0000) * transformY) + moveY),
        new createjs.Point(((536.6667) * transformX) + moveX, ((1065.3332) * transformY) + moveY),
        new createjs.Point(((536.3333) * transformX) + moveX, ((1066.6668) * transformY) + moveY),

        new createjs.Point(((536.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((535.6667) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((535.3333) * transformX) + moveX, ((1068.0000) * transformY) + moveY),

        new createjs.Point(((535.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((535.0000) * transformX) + moveX, ((1068.9999) * transformY) + moveY),
        new createjs.Point(((535.0000) * transformX) + moveX, ((1070.0001) * transformY) + moveY),

        new createjs.Point(((535.0000) * transformX) + moveX, ((1071.0000) * transformY) + moveY),
        new createjs.Point(((534.6667) * transformX) + moveX, ((1071.0000) * transformY) + moveY),
        new createjs.Point(((534.3333) * transformX) + moveX, ((1071.0000) * transformY) + moveY),

        new createjs.Point(((534.0000) * transformX) + moveX, ((1071.0000) * transformY) + moveY),
        new createjs.Point(((534.0000) * transformX) + moveX, ((1071.6666) * transformY) + moveY),
        new createjs.Point(((534.0000) * transformX) + moveX, ((1072.3334) * transformY) + moveY),

        new createjs.Point(((534.0000) * transformX) + moveX, ((1073.0000) * transformY) + moveY),
        new createjs.Point(((533.6667) * transformX) + moveX, ((1073.0000) * transformY) + moveY),
        new createjs.Point(((533.3333) * transformX) + moveX, ((1073.0000) * transformY) + moveY),

        new createjs.Point(((533.0000) * transformX) + moveX, ((1073.0000) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((1073.9999) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((1075.0001) * transformY) + moveY),

        new createjs.Point(((533.0000) * transformX) + moveX, ((1076.0000) * transformY) + moveY),
        new createjs.Point(((532.6667) * transformX) + moveX, ((1076.0000) * transformY) + moveY),
        new createjs.Point(((532.3333) * transformX) + moveX, ((1076.0000) * transformY) + moveY),

        new createjs.Point(((532.0000) * transformX) + moveX, ((1076.0000) * transformY) + moveY),
        new createjs.Point(((531.6667) * transformX) + moveX, ((1077.3332) * transformY) + moveY),
        new createjs.Point(((531.3333) * transformX) + moveX, ((1078.6668) * transformY) + moveY),

        new createjs.Point(((531.0000) * transformX) + moveX, ((1080.0000) * transformY) + moveY),
        new createjs.Point(((530.6667) * transformX) + moveX, ((1080.0000) * transformY) + moveY),
        new createjs.Point(((530.3333) * transformX) + moveX, ((1080.0000) * transformY) + moveY),

        new createjs.Point(((530.0000) * transformX) + moveX, ((1080.0000) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((1080.9999) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((1082.0001) * transformY) + moveY),

        new createjs.Point(((530.0000) * transformX) + moveX, ((1083.0000) * transformY) + moveY),
        new createjs.Point(((529.6667) * transformX) + moveX, ((1083.0000) * transformY) + moveY),
        new createjs.Point(((529.3333) * transformX) + moveX, ((1083.0000) * transformY) + moveY),

        new createjs.Point(((529.0000) * transformX) + moveX, ((1083.0000) * transformY) + moveY),
        new createjs.Point(((528.6667) * transformX) + moveX, ((1084.6665) * transformY) + moveY),
        new createjs.Point(((528.3333) * transformX) + moveX, ((1086.3335) * transformY) + moveY),

        new createjs.Point(((528.0000) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((527.6667) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((527.3333) * transformX) + moveX, ((1088.0000) * transformY) + moveY),

        new createjs.Point(((527.0000) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((526.6667) * transformX) + moveX, ((1089.3332) * transformY) + moveY),
        new createjs.Point(((526.3333) * transformX) + moveX, ((1090.6668) * transformY) + moveY),

        new createjs.Point(((526.0000) * transformX) + moveX, ((1092.0000) * transformY) + moveY),
        new createjs.Point(((525.6667) * transformX) + moveX, ((1092.0000) * transformY) + moveY),
        new createjs.Point(((525.3333) * transformX) + moveX, ((1092.0000) * transformY) + moveY),

        new createjs.Point(((525.0000) * transformX) + moveX, ((1092.0000) * transformY) + moveY),
        new createjs.Point(((525.0000) * transformX) + moveX, ((1092.9999) * transformY) + moveY),
        new createjs.Point(((525.0000) * transformX) + moveX, ((1094.0001) * transformY) + moveY),

        new createjs.Point(((525.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((524.6667) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((524.3333) * transformX) + moveX, ((1095.0000) * transformY) + moveY),

        new createjs.Point(((524.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((1095.6666) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((1096.3334) * transformY) + moveY),

        new createjs.Point(((524.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((523.6667) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((523.3333) * transformX) + moveX, ((1097.0000) * transformY) + moveY),

        new createjs.Point(((523.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((1097.9999) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((1099.0001) * transformY) + moveY),

        new createjs.Point(((523.0000) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((522.6667) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((522.3333) * transformX) + moveX, ((1100.0000) * transformY) + moveY),

        new createjs.Point(((522.0000) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((521.6667) * transformX) + moveX, ((1101.3332) * transformY) + moveY),
        new createjs.Point(((521.3333) * transformX) + moveX, ((1102.6668) * transformY) + moveY),

        new createjs.Point(((521.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((520.6667) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((520.3333) * transformX) + moveX, ((1104.0000) * transformY) + moveY),

        new createjs.Point(((520.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((520.0000) * transformX) + moveX, ((1104.9999) * transformY) + moveY),
        new createjs.Point(((520.0000) * transformX) + moveX, ((1106.0001) * transformY) + moveY),

        new createjs.Point(((520.0000) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((519.6667) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((519.3333) * transformX) + moveX, ((1107.0000) * transformY) + moveY),

        new createjs.Point(((519.0000) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((518.6667) * transformX) + moveX, ((1108.6665) * transformY) + moveY),
        new createjs.Point(((518.3333) * transformX) + moveX, ((1110.3335) * transformY) + moveY),

        new createjs.Point(((518.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((517.6667) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((517.3333) * transformX) + moveX, ((1112.0000) * transformY) + moveY),

        new createjs.Point(((517.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((516.6667) * transformX) + moveX, ((1113.3332) * transformY) + moveY),
        new createjs.Point(((516.3333) * transformX) + moveX, ((1114.6668) * transformY) + moveY),

        new createjs.Point(((516.0000) * transformX) + moveX, ((1116.0000) * transformY) + moveY),
        new createjs.Point(((515.6667) * transformX) + moveX, ((1116.0000) * transformY) + moveY),
        new createjs.Point(((515.3333) * transformX) + moveX, ((1116.0000) * transformY) + moveY),

        new createjs.Point(((515.0000) * transformX) + moveX, ((1116.0000) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1116.9999) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1118.0001) * transformY) + moveY),

        new createjs.Point(((515.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((514.6667) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((514.3333) * transformX) + moveX, ((1119.0000) * transformY) + moveY),

        new createjs.Point(((514.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((1119.6666) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((1120.3334) * transformY) + moveY),

        new createjs.Point(((514.0000) * transformX) + moveX, ((1121.0000) * transformY) + moveY),
        new createjs.Point(((513.6667) * transformX) + moveX, ((1121.0000) * transformY) + moveY),
        new createjs.Point(((513.3333) * transformX) + moveX, ((1121.0000) * transformY) + moveY),

        new createjs.Point(((513.0000) * transformX) + moveX, ((1121.0000) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1121.9999) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1123.0001) * transformY) + moveY),

        new createjs.Point(((513.0000) * transformX) + moveX, ((1124.0000) * transformY) + moveY),
        new createjs.Point(((512.6667) * transformX) + moveX, ((1124.0000) * transformY) + moveY),
        new createjs.Point(((512.3333) * transformX) + moveX, ((1124.0000) * transformY) + moveY),

        new createjs.Point(((512.0000) * transformX) + moveX, ((1124.0000) * transformY) + moveY),
        new createjs.Point(((511.6667) * transformX) + moveX, ((1125.3332) * transformY) + moveY),
        new createjs.Point(((511.3333) * transformX) + moveX, ((1126.6668) * transformY) + moveY),

        new createjs.Point(((511.0000) * transformX) + moveX, ((1128.0000) * transformY) + moveY),
        new createjs.Point(((510.6667) * transformX) + moveX, ((1128.0000) * transformY) + moveY),
        new createjs.Point(((510.3333) * transformX) + moveX, ((1128.0000) * transformY) + moveY),

        new createjs.Point(((510.0000) * transformX) + moveX, ((1128.0000) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((1128.9999) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((1130.0001) * transformY) + moveY),

        new createjs.Point(((510.0000) * transformX) + moveX, ((1131.0000) * transformY) + moveY),
        new createjs.Point(((509.6667) * transformX) + moveX, ((1131.0000) * transformY) + moveY),
        new createjs.Point(((509.3333) * transformX) + moveX, ((1131.0000) * transformY) + moveY),

        new createjs.Point(((509.0000) * transformX) + moveX, ((1131.0000) * transformY) + moveY),
        new createjs.Point(((508.6667) * transformX) + moveX, ((1132.6665) * transformY) + moveY),
        new createjs.Point(((508.3333) * transformX) + moveX, ((1134.3335) * transformY) + moveY),

        new createjs.Point(((508.0000) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((507.6667) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((507.3333) * transformX) + moveX, ((1136.0000) * transformY) + moveY),

        new createjs.Point(((507.0000) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((507.0000) * transformX) + moveX, ((1136.6666) * transformY) + moveY),
        new createjs.Point(((507.0000) * transformX) + moveX, ((1137.3334) * transformY) + moveY),

        new createjs.Point(((507.0000) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((506.6667) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((506.3333) * transformX) + moveX, ((1138.0000) * transformY) + moveY),

        new createjs.Point(((506.0000) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((1138.9999) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((1140.0001) * transformY) + moveY),

        new createjs.Point(((506.0000) * transformX) + moveX, ((1141.0000) * transformY) + moveY),
        new createjs.Point(((505.6667) * transformX) + moveX, ((1141.0000) * transformY) + moveY),
        new createjs.Point(((505.3333) * transformX) + moveX, ((1141.0000) * transformY) + moveY),

        new createjs.Point(((505.0000) * transformX) + moveX, ((1141.0000) * transformY) + moveY),
        new createjs.Point(((504.6667) * transformX) + moveX, ((1142.3332) * transformY) + moveY),
        new createjs.Point(((504.3333) * transformX) + moveX, ((1143.6668) * transformY) + moveY),

        new createjs.Point(((504.0000) * transformX) + moveX, ((1145.0000) * transformY) + moveY),
        new createjs.Point(((503.6667) * transformX) + moveX, ((1145.0000) * transformY) + moveY),
        new createjs.Point(((503.3333) * transformX) + moveX, ((1145.0000) * transformY) + moveY),

        new createjs.Point(((503.0000) * transformX) + moveX, ((1145.0000) * transformY) + moveY),
        new createjs.Point(((503.0000) * transformX) + moveX, ((1145.9999) * transformY) + moveY),
        new createjs.Point(((503.0000) * transformX) + moveX, ((1147.0001) * transformY) + moveY),

        new createjs.Point(((503.0000) * transformX) + moveX, ((1148.0000) * transformY) + moveY),
        new createjs.Point(((502.6667) * transformX) + moveX, ((1148.0000) * transformY) + moveY),
        new createjs.Point(((502.3333) * transformX) + moveX, ((1148.0000) * transformY) + moveY),

        new createjs.Point(((502.0000) * transformX) + moveX, ((1148.0000) * transformY) + moveY),
        new createjs.Point(((502.0000) * transformX) + moveX, ((1148.6666) * transformY) + moveY),
        new createjs.Point(((502.0000) * transformX) + moveX, ((1149.3334) * transformY) + moveY),

        new createjs.Point(((502.0000) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((501.6667) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((501.3333) * transformX) + moveX, ((1150.0000) * transformY) + moveY),

        new createjs.Point(((501.0000) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((1150.9999) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((1152.0001) * transformY) + moveY),

        new createjs.Point(((501.0000) * transformX) + moveX, ((1153.0000) * transformY) + moveY),
        new createjs.Point(((500.6667) * transformX) + moveX, ((1153.0000) * transformY) + moveY),
        new createjs.Point(((500.3333) * transformX) + moveX, ((1153.0000) * transformY) + moveY),

        new createjs.Point(((500.0000) * transformX) + moveX, ((1153.0000) * transformY) + moveY),
        new createjs.Point(((499.6667) * transformX) + moveX, ((1154.3332) * transformY) + moveY),
        new createjs.Point(((499.3333) * transformX) + moveX, ((1155.6668) * transformY) + moveY),

        new createjs.Point(((499.0000) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((498.6667) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((498.3333) * transformX) + moveX, ((1157.0000) * transformY) + moveY),

        new createjs.Point(((498.0000) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((1157.9999) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((1159.0001) * transformY) + moveY),

        new createjs.Point(((498.0000) * transformX) + moveX, ((1160.0000) * transformY) + moveY),
        new createjs.Point(((497.6667) * transformX) + moveX, ((1160.0000) * transformY) + moveY),
        new createjs.Point(((497.3333) * transformX) + moveX, ((1160.0000) * transformY) + moveY),

        new createjs.Point(((497.0000) * transformX) + moveX, ((1160.0000) * transformY) + moveY),
        new createjs.Point(((497.0000) * transformX) + moveX, ((1160.6666) * transformY) + moveY),
        new createjs.Point(((497.0000) * transformX) + moveX, ((1161.3334) * transformY) + moveY),

        new createjs.Point(((497.0000) * transformX) + moveX, ((1162.0000) * transformY) + moveY),
        new createjs.Point(((496.6667) * transformX) + moveX, ((1162.0000) * transformY) + moveY),
        new createjs.Point(((496.3333) * transformX) + moveX, ((1162.0000) * transformY) + moveY),

        new createjs.Point(((496.0000) * transformX) + moveX, ((1162.0000) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((1162.9999) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((1164.0001) * transformY) + moveY),

        new createjs.Point(((496.0000) * transformX) + moveX, ((1165.0000) * transformY) + moveY),
        new createjs.Point(((495.6667) * transformX) + moveX, ((1165.0000) * transformY) + moveY),
        new createjs.Point(((495.3333) * transformX) + moveX, ((1165.0000) * transformY) + moveY),

        new createjs.Point(((495.0000) * transformX) + moveX, ((1165.0000) * transformY) + moveY),
        new createjs.Point(((495.0000) * transformX) + moveX, ((1165.6666) * transformY) + moveY),
        new createjs.Point(((495.0000) * transformX) + moveX, ((1166.3334) * transformY) + moveY),

        new createjs.Point(((495.0000) * transformX) + moveX, ((1167.0000) * transformY) + moveY),
        new createjs.Point(((494.6667) * transformX) + moveX, ((1167.0000) * transformY) + moveY),
        new createjs.Point(((494.3333) * transformX) + moveX, ((1167.0000) * transformY) + moveY),

        new createjs.Point(((494.0000) * transformX) + moveX, ((1167.0000) * transformY) + moveY),
        new createjs.Point(((493.0001) * transformX) + moveX, ((1170.3330) * transformY) + moveY),
        new createjs.Point(((491.9999) * transformX) + moveX, ((1173.6670) * transformY) + moveY),

        new createjs.Point(((491.0000) * transformX) + moveX, ((1177.0000) * transformY) + moveY),
        new createjs.Point(((490.6667) * transformX) + moveX, ((1177.0000) * transformY) + moveY),
        new createjs.Point(((490.3333) * transformX) + moveX, ((1177.0000) * transformY) + moveY),

        new createjs.Point(((490.0000) * transformX) + moveX, ((1177.0000) * transformY) + moveY),
        new createjs.Point(((489.6667) * transformX) + moveX, ((1178.3332) * transformY) + moveY),
        new createjs.Point(((489.3333) * transformX) + moveX, ((1179.6668) * transformY) + moveY),

        new createjs.Point(((489.0000) * transformX) + moveX, ((1181.0000) * transformY) + moveY),
        new createjs.Point(((488.6667) * transformX) + moveX, ((1181.0000) * transformY) + moveY),
        new createjs.Point(((488.3333) * transformX) + moveX, ((1181.0000) * transformY) + moveY),

        new createjs.Point(((488.0000) * transformX) + moveX, ((1181.0000) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((1181.9999) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((1183.0001) * transformY) + moveY),

        new createjs.Point(((488.0000) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((487.6667) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((487.3333) * transformX) + moveX, ((1184.0000) * transformY) + moveY),

        new createjs.Point(((487.0000) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((486.6667) * transformX) + moveX, ((1185.6665) * transformY) + moveY),
        new createjs.Point(((486.3333) * transformX) + moveX, ((1187.3335) * transformY) + moveY),

        new createjs.Point(((486.0000) * transformX) + moveX, ((1189.0000) * transformY) + moveY),
        new createjs.Point(((485.6667) * transformX) + moveX, ((1189.0000) * transformY) + moveY),
        new createjs.Point(((485.3333) * transformX) + moveX, ((1189.0000) * transformY) + moveY),

        new createjs.Point(((485.0000) * transformX) + moveX, ((1189.0000) * transformY) + moveY),
        new createjs.Point(((484.6667) * transformX) + moveX, ((1190.3332) * transformY) + moveY),
        new createjs.Point(((484.3333) * transformX) + moveX, ((1191.6668) * transformY) + moveY),

        new createjs.Point(((484.0000) * transformX) + moveX, ((1193.0000) * transformY) + moveY),
        new createjs.Point(((483.6667) * transformX) + moveX, ((1193.0000) * transformY) + moveY),
        new createjs.Point(((483.3333) * transformX) + moveX, ((1193.0000) * transformY) + moveY),

        new createjs.Point(((483.0000) * transformX) + moveX, ((1193.0000) * transformY) + moveY),
        new createjs.Point(((483.0000) * transformX) + moveX, ((1193.9999) * transformY) + moveY),
        new createjs.Point(((483.0000) * transformX) + moveX, ((1195.0001) * transformY) + moveY),

        new createjs.Point(((483.0000) * transformX) + moveX, ((1196.0000) * transformY) + moveY),
        new createjs.Point(((482.6667) * transformX) + moveX, ((1196.0000) * transformY) + moveY),
        new createjs.Point(((482.3333) * transformX) + moveX, ((1196.0000) * transformY) + moveY),

        new createjs.Point(((482.0000) * transformX) + moveX, ((1196.0000) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((1196.6666) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((1197.3334) * transformY) + moveY),

        new createjs.Point(((482.0000) * transformX) + moveX, ((1198.0000) * transformY) + moveY),
        new createjs.Point(((481.6667) * transformX) + moveX, ((1198.0000) * transformY) + moveY),
        new createjs.Point(((481.3333) * transformX) + moveX, ((1198.0000) * transformY) + moveY),

        new createjs.Point(((481.0000) * transformX) + moveX, ((1198.0000) * transformY) + moveY),
        new createjs.Point(((481.0000) * transformX) + moveX, ((1198.9999) * transformY) + moveY),
        new createjs.Point(((481.0000) * transformX) + moveX, ((1200.0001) * transformY) + moveY),

        new createjs.Point(((481.0000) * transformX) + moveX, ((1201.0000) * transformY) + moveY),
        new createjs.Point(((480.6667) * transformX) + moveX, ((1201.0000) * transformY) + moveY),
        new createjs.Point(((480.3333) * transformX) + moveX, ((1201.0000) * transformY) + moveY),

        new createjs.Point(((480.0000) * transformX) + moveX, ((1201.0000) * transformY) + moveY),
        new createjs.Point(((479.6667) * transformX) + moveX, ((1202.3332) * transformY) + moveY),
        new createjs.Point(((479.3333) * transformX) + moveX, ((1203.6668) * transformY) + moveY),

        new createjs.Point(((479.0000) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((478.6667) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((478.3333) * transformX) + moveX, ((1205.0000) * transformY) + moveY),

        new createjs.Point(((478.0000) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1205.9999) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1207.0001) * transformY) + moveY),

        new createjs.Point(((478.0000) * transformX) + moveX, ((1208.0000) * transformY) + moveY),
        new createjs.Point(((477.6667) * transformX) + moveX, ((1208.0000) * transformY) + moveY),
        new createjs.Point(((477.3333) * transformX) + moveX, ((1208.0000) * transformY) + moveY),

        new createjs.Point(((477.0000) * transformX) + moveX, ((1208.0000) * transformY) + moveY),
        new createjs.Point(((476.6667) * transformX) + moveX, ((1209.6665) * transformY) + moveY),
        new createjs.Point(((476.3333) * transformX) + moveX, ((1211.3335) * transformY) + moveY),

        new createjs.Point(((476.0000) * transformX) + moveX, ((1213.0000) * transformY) + moveY),
        new createjs.Point(((475.6667) * transformX) + moveX, ((1213.0000) * transformY) + moveY),
        new createjs.Point(((475.3333) * transformX) + moveX, ((1213.0000) * transformY) + moveY),

        new createjs.Point(((475.0000) * transformX) + moveX, ((1213.0000) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((1213.6666) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((1214.3334) * transformY) + moveY),

        new createjs.Point(((475.0000) * transformX) + moveX, ((1215.0000) * transformY) + moveY),
        new createjs.Point(((474.6667) * transformX) + moveX, ((1215.0000) * transformY) + moveY),
        new createjs.Point(((474.3333) * transformX) + moveX, ((1215.0000) * transformY) + moveY),

        new createjs.Point(((474.0000) * transformX) + moveX, ((1215.0000) * transformY) + moveY),
        new createjs.Point(((474.0000) * transformX) + moveX, ((1215.9999) * transformY) + moveY),
        new createjs.Point(((474.0000) * transformX) + moveX, ((1217.0001) * transformY) + moveY),

        new createjs.Point(((474.0000) * transformX) + moveX, ((1218.0000) * transformY) + moveY),
        new createjs.Point(((473.6667) * transformX) + moveX, ((1218.0000) * transformY) + moveY),
        new createjs.Point(((473.3333) * transformX) + moveX, ((1218.0000) * transformY) + moveY),

        new createjs.Point(((473.0000) * transformX) + moveX, ((1218.0000) * transformY) + moveY),
        new createjs.Point(((472.6667) * transformX) + moveX, ((1219.3332) * transformY) + moveY),
        new createjs.Point(((472.3333) * transformX) + moveX, ((1220.6668) * transformY) + moveY),

        new createjs.Point(((472.0000) * transformX) + moveX, ((1222.0000) * transformY) + moveY),
        new createjs.Point(((471.6667) * transformX) + moveX, ((1222.0000) * transformY) + moveY),
        new createjs.Point(((471.3333) * transformX) + moveX, ((1222.0000) * transformY) + moveY),

        new createjs.Point(((471.0000) * transformX) + moveX, ((1222.0000) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((1222.9999) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((1224.0001) * transformY) + moveY),

        new createjs.Point(((471.0000) * transformX) + moveX, ((1225.0000) * transformY) + moveY),
        new createjs.Point(((470.6667) * transformX) + moveX, ((1225.0000) * transformY) + moveY),
        new createjs.Point(((470.3333) * transformX) + moveX, ((1225.0000) * transformY) + moveY),

        new createjs.Point(((470.0000) * transformX) + moveX, ((1225.0000) * transformY) + moveY),
        new createjs.Point(((470.0000) * transformX) + moveX, ((1225.6666) * transformY) + moveY),
        new createjs.Point(((470.0000) * transformX) + moveX, ((1226.3334) * transformY) + moveY),

        new createjs.Point(((470.0000) * transformX) + moveX, ((1227.0000) * transformY) + moveY),
        new createjs.Point(((469.6667) * transformX) + moveX, ((1227.0000) * transformY) + moveY),
        new createjs.Point(((469.3333) * transformX) + moveX, ((1227.0000) * transformY) + moveY),

        new createjs.Point(((469.0000) * transformX) + moveX, ((1227.0000) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((1227.9999) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((1229.0001) * transformY) + moveY),

        new createjs.Point(((469.0000) * transformX) + moveX, ((1230.0000) * transformY) + moveY),
        new createjs.Point(((468.6667) * transformX) + moveX, ((1230.0000) * transformY) + moveY),
        new createjs.Point(((468.3333) * transformX) + moveX, ((1230.0000) * transformY) + moveY),

        new createjs.Point(((468.0000) * transformX) + moveX, ((1230.0000) * transformY) + moveY),
        new createjs.Point(((467.6667) * transformX) + moveX, ((1231.3332) * transformY) + moveY),
        new createjs.Point(((467.3333) * transformX) + moveX, ((1232.6668) * transformY) + moveY),

        new createjs.Point(((467.0000) * transformX) + moveX, ((1234.0000) * transformY) + moveY),
        new createjs.Point(((466.6667) * transformX) + moveX, ((1234.0000) * transformY) + moveY),
        new createjs.Point(((466.3333) * transformX) + moveX, ((1234.0000) * transformY) + moveY),

        new createjs.Point(((466.0000) * transformX) + moveX, ((1234.0000) * transformY) + moveY),
        new createjs.Point(((466.0000) * transformX) + moveX, ((1234.9999) * transformY) + moveY),
        new createjs.Point(((466.0000) * transformX) + moveX, ((1236.0001) * transformY) + moveY),

        new createjs.Point(((466.0000) * transformX) + moveX, ((1237.0000) * transformY) + moveY),
        new createjs.Point(((465.6667) * transformX) + moveX, ((1237.0000) * transformY) + moveY),
        new createjs.Point(((465.3333) * transformX) + moveX, ((1237.0000) * transformY) + moveY),

        new createjs.Point(((465.0000) * transformX) + moveX, ((1237.0000) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((1237.6666) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((1238.3334) * transformY) + moveY),

        new createjs.Point(((465.0000) * transformX) + moveX, ((1239.0000) * transformY) + moveY),
        new createjs.Point(((464.6667) * transformX) + moveX, ((1239.0000) * transformY) + moveY),
        new createjs.Point(((464.3333) * transformX) + moveX, ((1239.0000) * transformY) + moveY),

        new createjs.Point(((464.0000) * transformX) + moveX, ((1239.0000) * transformY) + moveY),
        new createjs.Point(((464.0000) * transformX) + moveX, ((1239.9999) * transformY) + moveY),
        new createjs.Point(((464.0000) * transformX) + moveX, ((1241.0001) * transformY) + moveY),

        new createjs.Point(((464.0000) * transformX) + moveX, ((1242.0000) * transformY) + moveY),
        new createjs.Point(((463.6667) * transformX) + moveX, ((1242.0000) * transformY) + moveY),
        new createjs.Point(((463.3333) * transformX) + moveX, ((1242.0000) * transformY) + moveY),

        new createjs.Point(((463.0000) * transformX) + moveX, ((1242.0000) * transformY) + moveY),
        new createjs.Point(((462.6667) * transformX) + moveX, ((1243.3332) * transformY) + moveY),
        new createjs.Point(((462.3333) * transformX) + moveX, ((1244.6668) * transformY) + moveY),

        new createjs.Point(((462.0000) * transformX) + moveX, ((1246.0000) * transformY) + moveY),
        new createjs.Point(((461.6667) * transformX) + moveX, ((1246.0000) * transformY) + moveY),
        new createjs.Point(((461.3333) * transformX) + moveX, ((1246.0000) * transformY) + moveY),

        new createjs.Point(((461.0000) * transformX) + moveX, ((1246.0000) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((1246.9999) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((1248.0001) * transformY) + moveY),

        new createjs.Point(((461.0000) * transformX) + moveX, ((1249.0000) * transformY) + moveY),
        new createjs.Point(((460.6667) * transformX) + moveX, ((1249.0000) * transformY) + moveY),
        new createjs.Point(((460.3333) * transformX) + moveX, ((1249.0000) * transformY) + moveY),

        new createjs.Point(((460.0000) * transformX) + moveX, ((1249.0000) * transformY) + moveY),
        new createjs.Point(((460.0000) * transformX) + moveX, ((1249.6666) * transformY) + moveY),
        new createjs.Point(((460.0000) * transformX) + moveX, ((1250.3334) * transformY) + moveY),

        new createjs.Point(((460.0000) * transformX) + moveX, ((1251.0000) * transformY) + moveY),
        new createjs.Point(((459.6667) * transformX) + moveX, ((1251.0000) * transformY) + moveY),
        new createjs.Point(((459.3333) * transformX) + moveX, ((1251.0000) * transformY) + moveY),

        new createjs.Point(((459.0000) * transformX) + moveX, ((1251.0000) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1251.9999) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1253.0001) * transformY) + moveY),

        new createjs.Point(((459.0000) * transformX) + moveX, ((1254.0000) * transformY) + moveY),
        new createjs.Point(((458.6667) * transformX) + moveX, ((1254.0000) * transformY) + moveY),
        new createjs.Point(((458.3333) * transformX) + moveX, ((1254.0000) * transformY) + moveY),

        new createjs.Point(((458.0000) * transformX) + moveX, ((1254.0000) * transformY) + moveY),
        new createjs.Point(((457.6667) * transformX) + moveX, ((1255.3332) * transformY) + moveY),
        new createjs.Point(((457.3333) * transformX) + moveX, ((1256.6668) * transformY) + moveY),

        new createjs.Point(((457.0000) * transformX) + moveX, ((1258.0000) * transformY) + moveY),
        new createjs.Point(((456.6667) * transformX) + moveX, ((1258.0000) * transformY) + moveY),
        new createjs.Point(((456.3333) * transformX) + moveX, ((1258.0000) * transformY) + moveY),

        new createjs.Point(((456.0000) * transformX) + moveX, ((1258.0000) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1258.9999) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1260.0001) * transformY) + moveY),

        new createjs.Point(((456.0000) * transformX) + moveX, ((1261.0000) * transformY) + moveY),
        new createjs.Point(((455.6667) * transformX) + moveX, ((1261.0000) * transformY) + moveY),
        new createjs.Point(((455.3333) * transformX) + moveX, ((1261.0000) * transformY) + moveY),

        new createjs.Point(((455.0000) * transformX) + moveX, ((1261.0000) * transformY) + moveY),
        new createjs.Point(((455.0000) * transformX) + moveX, ((1261.6666) * transformY) + moveY),
        new createjs.Point(((455.0000) * transformX) + moveX, ((1262.3334) * transformY) + moveY),

        new createjs.Point(((455.0000) * transformX) + moveX, ((1263.0000) * transformY) + moveY),
        new createjs.Point(((454.6667) * transformX) + moveX, ((1263.0000) * transformY) + moveY),
        new createjs.Point(((454.3333) * transformX) + moveX, ((1263.0000) * transformY) + moveY),

        new createjs.Point(((454.0000) * transformX) + moveX, ((1263.0000) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((1263.9999) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((1265.0001) * transformY) + moveY),

        new createjs.Point(((454.0000) * transformX) + moveX, ((1266.0000) * transformY) + moveY),
        new createjs.Point(((453.6667) * transformX) + moveX, ((1266.0000) * transformY) + moveY),
        new createjs.Point(((453.3333) * transformX) + moveX, ((1266.0000) * transformY) + moveY),

        new createjs.Point(((453.0000) * transformX) + moveX, ((1266.0000) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1266.6666) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1267.3334) * transformY) + moveY),

        new createjs.Point(((453.0000) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((452.6667) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((452.3333) * transformX) + moveX, ((1268.0000) * transformY) + moveY),

        new createjs.Point(((452.0000) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((451.0001) * transformX) + moveX, ((1271.3330) * transformY) + moveY),
        new createjs.Point(((449.9999) * transformX) + moveX, ((1274.6670) * transformY) + moveY),

        new createjs.Point(((449.0000) * transformX) + moveX, ((1278.0000) * transformY) + moveY),
        new createjs.Point(((448.6667) * transformX) + moveX, ((1278.0000) * transformY) + moveY),
        new createjs.Point(((448.3333) * transformX) + moveX, ((1278.0000) * transformY) + moveY),

        new createjs.Point(((448.0000) * transformX) + moveX, ((1278.0000) * transformY) + moveY),
        new createjs.Point(((447.6667) * transformX) + moveX, ((1279.3332) * transformY) + moveY),
        new createjs.Point(((447.3333) * transformX) + moveX, ((1280.6668) * transformY) + moveY),

        new createjs.Point(((447.0000) * transformX) + moveX, ((1282.0000) * transformY) + moveY),
        new createjs.Point(((446.6667) * transformX) + moveX, ((1282.0000) * transformY) + moveY),
        new createjs.Point(((446.3333) * transformX) + moveX, ((1282.0000) * transformY) + moveY),

        new createjs.Point(((446.0000) * transformX) + moveX, ((1282.0000) * transformY) + moveY),
        new createjs.Point(((446.0000) * transformX) + moveX, ((1282.9999) * transformY) + moveY),
        new createjs.Point(((446.0000) * transformX) + moveX, ((1284.0001) * transformY) + moveY),

        new createjs.Point(((446.0000) * transformX) + moveX, ((1285.0000) * transformY) + moveY),
        new createjs.Point(((445.6667) * transformX) + moveX, ((1285.0000) * transformY) + moveY),
        new createjs.Point(((445.3333) * transformX) + moveX, ((1285.0000) * transformY) + moveY),

        new createjs.Point(((445.0000) * transformX) + moveX, ((1285.0000) * transformY) + moveY),
        new createjs.Point(((444.6667) * transformX) + moveX, ((1286.6665) * transformY) + moveY),
        new createjs.Point(((444.3333) * transformX) + moveX, ((1288.3335) * transformY) + moveY),

        new createjs.Point(((444.0000) * transformX) + moveX, ((1290.0000) * transformY) + moveY),
        new createjs.Point(((443.6667) * transformX) + moveX, ((1290.0000) * transformY) + moveY),
        new createjs.Point(((443.3333) * transformX) + moveX, ((1290.0000) * transformY) + moveY),

        new createjs.Point(((443.0000) * transformX) + moveX, ((1290.0000) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((1290.6666) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((1291.3334) * transformY) + moveY),

        new createjs.Point(((443.0000) * transformX) + moveX, ((1292.0000) * transformY) + moveY),
        new createjs.Point(((442.6667) * transformX) + moveX, ((1292.0000) * transformY) + moveY),
        new createjs.Point(((442.3333) * transformX) + moveX, ((1292.0000) * transformY) + moveY),

        new createjs.Point(((442.0000) * transformX) + moveX, ((1292.0000) * transformY) + moveY),
        new createjs.Point(((442.0000) * transformX) + moveX, ((1292.9999) * transformY) + moveY),
        new createjs.Point(((442.0000) * transformX) + moveX, ((1294.0001) * transformY) + moveY),

        new createjs.Point(((442.0000) * transformX) + moveX, ((1295.0000) * transformY) + moveY),
        new createjs.Point(((441.6667) * transformX) + moveX, ((1295.0000) * transformY) + moveY),
        new createjs.Point(((441.3333) * transformX) + moveX, ((1295.0000) * transformY) + moveY),

        new createjs.Point(((441.0000) * transformX) + moveX, ((1295.0000) * transformY) + moveY),
        new createjs.Point(((440.6667) * transformX) + moveX, ((1296.3332) * transformY) + moveY),
        new createjs.Point(((440.3333) * transformX) + moveX, ((1297.6668) * transformY) + moveY),

        new createjs.Point(((440.0000) * transformX) + moveX, ((1299.0000) * transformY) + moveY),
        new createjs.Point(((439.6667) * transformX) + moveX, ((1299.0000) * transformY) + moveY),
        new createjs.Point(((439.3333) * transformX) + moveX, ((1299.0000) * transformY) + moveY),

        new createjs.Point(((439.0000) * transformX) + moveX, ((1299.0000) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((1299.9999) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((1301.0001) * transformY) + moveY),

        new createjs.Point(((439.0000) * transformX) + moveX, ((1302.0000) * transformY) + moveY),
        new createjs.Point(((438.6667) * transformX) + moveX, ((1302.0000) * transformY) + moveY),
        new createjs.Point(((438.3333) * transformX) + moveX, ((1302.0000) * transformY) + moveY),

        new createjs.Point(((438.0000) * transformX) + moveX, ((1302.0000) * transformY) + moveY),
        new createjs.Point(((438.0000) * transformX) + moveX, ((1302.6666) * transformY) + moveY),
        new createjs.Point(((438.0000) * transformX) + moveX, ((1303.3334) * transformY) + moveY),

        new createjs.Point(((438.0000) * transformX) + moveX, ((1304.0000) * transformY) + moveY),
        new createjs.Point(((437.6667) * transformX) + moveX, ((1304.0000) * transformY) + moveY),
        new createjs.Point(((437.3333) * transformX) + moveX, ((1304.0000) * transformY) + moveY),

        new createjs.Point(((437.0000) * transformX) + moveX, ((1304.0000) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((1304.9999) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((1306.0001) * transformY) + moveY),

        new createjs.Point(((437.0000) * transformX) + moveX, ((1307.0000) * transformY) + moveY),
        new createjs.Point(((436.6667) * transformX) + moveX, ((1307.0000) * transformY) + moveY),
        new createjs.Point(((436.3333) * transformX) + moveX, ((1307.0000) * transformY) + moveY),

        new createjs.Point(((436.0000) * transformX) + moveX, ((1307.0000) * transformY) + moveY),
        new createjs.Point(((435.6667) * transformX) + moveX, ((1308.3332) * transformY) + moveY),
        new createjs.Point(((435.3333) * transformX) + moveX, ((1309.6668) * transformY) + moveY),

        new createjs.Point(((435.0000) * transformX) + moveX, ((1311.0000) * transformY) + moveY),
        new createjs.Point(((434.6667) * transformX) + moveX, ((1311.0000) * transformY) + moveY),
        new createjs.Point(((434.3333) * transformX) + moveX, ((1311.0000) * transformY) + moveY),

        new createjs.Point(((434.0000) * transformX) + moveX, ((1311.0000) * transformY) + moveY),
        new createjs.Point(((434.0000) * transformX) + moveX, ((1311.9999) * transformY) + moveY),
        new createjs.Point(((434.0000) * transformX) + moveX, ((1313.0001) * transformY) + moveY),

        new createjs.Point(((434.0000) * transformX) + moveX, ((1314.0000) * transformY) + moveY),
        new createjs.Point(((433.6667) * transformX) + moveX, ((1314.0000) * transformY) + moveY),
        new createjs.Point(((433.3333) * transformX) + moveX, ((1314.0000) * transformY) + moveY),

        new createjs.Point(((433.0000) * transformX) + moveX, ((1314.0000) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((1314.6666) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((1315.3334) * transformY) + moveY),

        new createjs.Point(((433.0000) * transformX) + moveX, ((1316.0000) * transformY) + moveY),
        new createjs.Point(((432.6667) * transformX) + moveX, ((1316.0000) * transformY) + moveY),
        new createjs.Point(((432.3333) * transformX) + moveX, ((1316.0000) * transformY) + moveY),

        new createjs.Point(((432.0000) * transformX) + moveX, ((1316.0000) * transformY) + moveY),
        new createjs.Point(((432.0000) * transformX) + moveX, ((1316.9999) * transformY) + moveY),
        new createjs.Point(((432.0000) * transformX) + moveX, ((1318.0001) * transformY) + moveY),

        new createjs.Point(((432.0000) * transformX) + moveX, ((1319.0000) * transformY) + moveY),
        new createjs.Point(((431.6667) * transformX) + moveX, ((1319.0000) * transformY) + moveY),
        new createjs.Point(((431.3333) * transformX) + moveX, ((1319.0000) * transformY) + moveY),

        new createjs.Point(((431.0000) * transformX) + moveX, ((1319.0000) * transformY) + moveY),
        new createjs.Point(((430.6667) * transformX) + moveX, ((1320.3332) * transformY) + moveY),
        new createjs.Point(((430.3333) * transformX) + moveX, ((1321.6668) * transformY) + moveY),

        new createjs.Point(((430.0000) * transformX) + moveX, ((1323.0000) * transformY) + moveY),
        new createjs.Point(((429.6667) * transformX) + moveX, ((1323.0000) * transformY) + moveY),
        new createjs.Point(((429.3333) * transformX) + moveX, ((1323.0000) * transformY) + moveY),

        new createjs.Point(((429.0000) * transformX) + moveX, ((1323.0000) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((1323.9999) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((1325.0001) * transformY) + moveY),

        new createjs.Point(((429.0000) * transformX) + moveX, ((1326.0000) * transformY) + moveY),
        new createjs.Point(((428.6667) * transformX) + moveX, ((1326.0000) * transformY) + moveY),
        new createjs.Point(((428.3333) * transformX) + moveX, ((1326.0000) * transformY) + moveY),

        new createjs.Point(((428.0000) * transformX) + moveX, ((1326.0000) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((1326.6666) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((1327.3334) * transformY) + moveY),

        new createjs.Point(((428.0000) * transformX) + moveX, ((1328.0000) * transformY) + moveY),
        new createjs.Point(((427.6667) * transformX) + moveX, ((1328.0000) * transformY) + moveY),
        new createjs.Point(((427.3333) * transformX) + moveX, ((1328.0000) * transformY) + moveY),

        new createjs.Point(((427.0000) * transformX) + moveX, ((1328.0000) * transformY) + moveY),
        new createjs.Point(((427.0000) * transformX) + moveX, ((1328.9999) * transformY) + moveY),
        new createjs.Point(((427.0000) * transformX) + moveX, ((1330.0001) * transformY) + moveY),

        new createjs.Point(((427.0000) * transformX) + moveX, ((1331.0000) * transformY) + moveY),
        new createjs.Point(((426.6667) * transformX) + moveX, ((1331.0000) * transformY) + moveY),
        new createjs.Point(((426.3333) * transformX) + moveX, ((1331.0000) * transformY) + moveY),

        new createjs.Point(((426.0000) * transformX) + moveX, ((1331.0000) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((1331.6666) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((1332.3334) * transformY) + moveY),

        new createjs.Point(((426.0000) * transformX) + moveX, ((1333.0000) * transformY) + moveY),
        new createjs.Point(((425.6667) * transformX) + moveX, ((1333.0000) * transformY) + moveY),
        new createjs.Point(((425.3333) * transformX) + moveX, ((1333.0000) * transformY) + moveY),

        new createjs.Point(((425.0000) * transformX) + moveX, ((1333.0000) * transformY) + moveY),
        new createjs.Point(((424.0001) * transformX) + moveX, ((1336.3330) * transformY) + moveY),
        new createjs.Point(((422.9999) * transformX) + moveX, ((1339.6670) * transformY) + moveY),

        new createjs.Point(((422.0000) * transformX) + moveX, ((1343.0000) * transformY) + moveY),
        new createjs.Point(((421.6667) * transformX) + moveX, ((1343.0000) * transformY) + moveY),
        new createjs.Point(((421.3333) * transformX) + moveX, ((1343.0000) * transformY) + moveY),

        new createjs.Point(((421.0000) * transformX) + moveX, ((1343.0000) * transformY) + moveY),
        new createjs.Point(((420.6667) * transformX) + moveX, ((1344.3332) * transformY) + moveY),
        new createjs.Point(((420.3333) * transformX) + moveX, ((1345.6668) * transformY) + moveY),

        new createjs.Point(((420.0000) * transformX) + moveX, ((1347.0000) * transformY) + moveY),
        new createjs.Point(((419.6667) * transformX) + moveX, ((1347.0000) * transformY) + moveY),
        new createjs.Point(((419.3333) * transformX) + moveX, ((1347.0000) * transformY) + moveY),

        new createjs.Point(((419.0000) * transformX) + moveX, ((1347.0000) * transformY) + moveY),
        new createjs.Point(((419.0000) * transformX) + moveX, ((1347.9999) * transformY) + moveY),
        new createjs.Point(((419.0000) * transformX) + moveX, ((1349.0001) * transformY) + moveY),

        new createjs.Point(((419.0000) * transformX) + moveX, ((1350.0000) * transformY) + moveY),
        new createjs.Point(((418.6667) * transformX) + moveX, ((1350.0000) * transformY) + moveY),
        new createjs.Point(((418.3333) * transformX) + moveX, ((1350.0000) * transformY) + moveY),

        new createjs.Point(((418.0000) * transformX) + moveX, ((1350.0000) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((1350.6666) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((1351.3334) * transformY) + moveY),

        new createjs.Point(((418.0000) * transformX) + moveX, ((1352.0000) * transformY) + moveY),
        new createjs.Point(((417.6667) * transformX) + moveX, ((1352.0000) * transformY) + moveY),
        new createjs.Point(((417.3333) * transformX) + moveX, ((1352.0000) * transformY) + moveY),

        new createjs.Point(((417.0000) * transformX) + moveX, ((1352.0000) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((1352.9999) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((1354.0001) * transformY) + moveY),

        new createjs.Point(((417.0000) * transformX) + moveX, ((1355.0000) * transformY) + moveY),
        new createjs.Point(((387.7005) * transformX) + moveX, ((1420.1589) * transformY) + moveY),
        new createjs.Point(((362.3451) * transformX) + moveX, ((1488.8946) * transformY) + moveY),

        new createjs.Point(((333.0000) * transformX) + moveX, ((1554.0000) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((1554.9999) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((1556.0001) * transformY) + moveY),

        new createjs.Point(((333.0000) * transformX) + moveX, ((1557.0000) * transformY) + moveY),
        new createjs.Point(((332.6667) * transformX) + moveX, ((1557.0000) * transformY) + moveY),
        new createjs.Point(((332.3333) * transformX) + moveX, ((1557.0000) * transformY) + moveY),

        new createjs.Point(((332.0000) * transformX) + moveX, ((1557.0000) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1557.6666) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1558.3334) * transformY) + moveY),

        new createjs.Point(((332.0000) * transformX) + moveX, ((1559.0000) * transformY) + moveY),
        new createjs.Point(((331.6667) * transformX) + moveX, ((1559.0000) * transformY) + moveY),
        new createjs.Point(((331.3333) * transformX) + moveX, ((1559.0000) * transformY) + moveY),

        new createjs.Point(((331.0000) * transformX) + moveX, ((1559.0000) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((1559.9999) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((1561.0001) * transformY) + moveY),

        new createjs.Point(((331.0000) * transformX) + moveX, ((1562.0000) * transformY) + moveY),
        new createjs.Point(((330.6667) * transformX) + moveX, ((1562.0000) * transformY) + moveY),
        new createjs.Point(((330.3333) * transformX) + moveX, ((1562.0000) * transformY) + moveY),

        new createjs.Point(((330.0000) * transformX) + moveX, ((1562.0000) * transformY) + moveY),
        new createjs.Point(((329.6667) * transformX) + moveX, ((1563.3332) * transformY) + moveY),
        new createjs.Point(((329.3333) * transformX) + moveX, ((1564.6668) * transformY) + moveY),

        new createjs.Point(((329.0000) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((328.6667) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((328.3333) * transformX) + moveX, ((1566.0000) * transformY) + moveY),

        new createjs.Point(((328.0000) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((1566.9999) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((1568.0001) * transformY) + moveY),

        new createjs.Point(((328.0000) * transformX) + moveX, ((1569.0000) * transformY) + moveY),
        new createjs.Point(((327.6667) * transformX) + moveX, ((1569.0000) * transformY) + moveY),
        new createjs.Point(((327.3333) * transformX) + moveX, ((1569.0000) * transformY) + moveY),

        new createjs.Point(((327.0000) * transformX) + moveX, ((1569.0000) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((1569.6666) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((1570.3334) * transformY) + moveY),

        new createjs.Point(((327.0000) * transformX) + moveX, ((1571.0000) * transformY) + moveY),
        new createjs.Point(((326.6667) * transformX) + moveX, ((1571.0000) * transformY) + moveY),
        new createjs.Point(((326.3333) * transformX) + moveX, ((1571.0000) * transformY) + moveY),

        new createjs.Point(((326.0000) * transformX) + moveX, ((1571.0000) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((1571.9999) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((1573.0001) * transformY) + moveY),

        new createjs.Point(((326.0000) * transformX) + moveX, ((1574.0000) * transformY) + moveY),
        new createjs.Point(((325.6667) * transformX) + moveX, ((1574.0000) * transformY) + moveY),
        new createjs.Point(((325.3333) * transformX) + moveX, ((1574.0000) * transformY) + moveY),

        new createjs.Point(((325.0000) * transformX) + moveX, ((1574.0000) * transformY) + moveY),
        new createjs.Point(((324.6667) * transformX) + moveX, ((1575.3332) * transformY) + moveY),
        new createjs.Point(((324.3333) * transformX) + moveX, ((1576.6668) * transformY) + moveY),

        new createjs.Point(((324.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((323.6667) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((323.3333) * transformX) + moveX, ((1578.0000) * transformY) + moveY),

        new createjs.Point(((323.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((323.0000) * transformX) + moveX, ((1578.9999) * transformY) + moveY),
        new createjs.Point(((323.0000) * transformX) + moveX, ((1580.0001) * transformY) + moveY),

        new createjs.Point(((323.0000) * transformX) + moveX, ((1581.0000) * transformY) + moveY),
        new createjs.Point(((322.6667) * transformX) + moveX, ((1581.0000) * transformY) + moveY),
        new createjs.Point(((322.3333) * transformX) + moveX, ((1581.0000) * transformY) + moveY),

        new createjs.Point(((322.0000) * transformX) + moveX, ((1581.0000) * transformY) + moveY),
        new createjs.Point(((321.6667) * transformX) + moveX, ((1582.6665) * transformY) + moveY),
        new createjs.Point(((321.3333) * transformX) + moveX, ((1584.3335) * transformY) + moveY),

        new createjs.Point(((321.0000) * transformX) + moveX, ((1586.0000) * transformY) + moveY),
        new createjs.Point(((320.6667) * transformX) + moveX, ((1586.0000) * transformY) + moveY),
        new createjs.Point(((320.3333) * transformX) + moveX, ((1586.0000) * transformY) + moveY),

        new createjs.Point(((320.0000) * transformX) + moveX, ((1586.0000) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1586.6666) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1587.3334) * transformY) + moveY),

        new createjs.Point(((320.0000) * transformX) + moveX, ((1588.0000) * transformY) + moveY),
        new createjs.Point(((319.6667) * transformX) + moveX, ((1588.0000) * transformY) + moveY),
        new createjs.Point(((319.3333) * transformX) + moveX, ((1588.0000) * transformY) + moveY),

        new createjs.Point(((319.0000) * transformX) + moveX, ((1588.0000) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((1588.9999) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((1590.0001) * transformY) + moveY),

        new createjs.Point(((319.0000) * transformX) + moveX, ((1591.0000) * transformY) + moveY),
        new createjs.Point(((318.6667) * transformX) + moveX, ((1591.0000) * transformY) + moveY),
        new createjs.Point(((318.3333) * transformX) + moveX, ((1591.0000) * transformY) + moveY),

        new createjs.Point(((318.0000) * transformX) + moveX, ((1591.0000) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((1591.6666) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((1592.3334) * transformY) + moveY),

        new createjs.Point(((318.0000) * transformX) + moveX, ((1593.0000) * transformY) + moveY),
        new createjs.Point(((317.6667) * transformX) + moveX, ((1593.0000) * transformY) + moveY),
        new createjs.Point(((317.3333) * transformX) + moveX, ((1593.0000) * transformY) + moveY),

        new createjs.Point(((317.0000) * transformX) + moveX, ((1593.0000) * transformY) + moveY),
        new createjs.Point(((316.0001) * transformX) + moveX, ((1596.3330) * transformY) + moveY),
        new createjs.Point(((314.9999) * transformX) + moveX, ((1599.6670) * transformY) + moveY),

        new createjs.Point(((314.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((313.6667) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((313.3333) * transformX) + moveX, ((1603.0000) * transformY) + moveY),

        new createjs.Point(((313.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((312.6667) * transformX) + moveX, ((1604.3332) * transformY) + moveY),
        new createjs.Point(((312.3333) * transformX) + moveX, ((1605.6668) * transformY) + moveY),

        new createjs.Point(((312.0000) * transformX) + moveX, ((1607.0000) * transformY) + moveY),
        new createjs.Point(((311.6667) * transformX) + moveX, ((1607.0000) * transformY) + moveY),
        new createjs.Point(((311.3333) * transformX) + moveX, ((1607.0000) * transformY) + moveY),

        new createjs.Point(((311.0000) * transformX) + moveX, ((1607.0000) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((1607.9999) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((1609.0001) * transformY) + moveY),

        new createjs.Point(((311.0000) * transformX) + moveX, ((1610.0000) * transformY) + moveY),
        new createjs.Point(((310.6667) * transformX) + moveX, ((1610.0000) * transformY) + moveY),
        new createjs.Point(((310.3333) * transformX) + moveX, ((1610.0000) * transformY) + moveY),

        new createjs.Point(((310.0000) * transformX) + moveX, ((1610.0000) * transformY) + moveY),
        new createjs.Point(((309.6667) * transformX) + moveX, ((1611.6665) * transformY) + moveY),
        new createjs.Point(((309.3333) * transformX) + moveX, ((1613.3335) * transformY) + moveY),

        new createjs.Point(((309.0000) * transformX) + moveX, ((1615.0000) * transformY) + moveY),
        new createjs.Point(((308.6667) * transformX) + moveX, ((1615.0000) * transformY) + moveY),
        new createjs.Point(((308.3333) * transformX) + moveX, ((1615.0000) * transformY) + moveY),

        new createjs.Point(((308.0000) * transformX) + moveX, ((1615.0000) * transformY) + moveY),
        new createjs.Point(((307.6667) * transformX) + moveX, ((1616.3332) * transformY) + moveY),
        new createjs.Point(((307.3333) * transformX) + moveX, ((1617.6668) * transformY) + moveY),

        new createjs.Point(((307.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((306.6667) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((306.3333) * transformX) + moveX, ((1619.0000) * transformY) + moveY),

        new createjs.Point(((306.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((1619.9999) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((1621.0001) * transformY) + moveY),

        new createjs.Point(((306.0000) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((305.6667) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((305.3333) * transformX) + moveX, ((1622.0000) * transformY) + moveY),

        new createjs.Point(((305.0000) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1622.6666) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1623.3334) * transformY) + moveY),

        new createjs.Point(((305.0000) * transformX) + moveX, ((1624.0000) * transformY) + moveY),
        new createjs.Point(((304.6667) * transformX) + moveX, ((1624.0000) * transformY) + moveY),
        new createjs.Point(((304.3333) * transformX) + moveX, ((1624.0000) * transformY) + moveY),

        new createjs.Point(((304.0000) * transformX) + moveX, ((1624.0000) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((1624.9999) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((1626.0001) * transformY) + moveY),

        new createjs.Point(((304.0000) * transformX) + moveX, ((1627.0000) * transformY) + moveY),
        new createjs.Point(((286.0922) * transformX) + moveX, ((1666.8219) * transformY) + moveY),
        new createjs.Point(((269.9597) * transformX) + moveX, ((1709.1849) * transformY) + moveY),

        new createjs.Point(((252.0000) * transformX) + moveX, ((1749.0000) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((1749.9999) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((1751.0001) * transformY) + moveY),

        new createjs.Point(((252.0000) * transformX) + moveX, ((1752.0000) * transformY) + moveY),
        new createjs.Point(((251.6667) * transformX) + moveX, ((1752.0000) * transformY) + moveY),
        new createjs.Point(((251.3333) * transformX) + moveX, ((1752.0000) * transformY) + moveY),

        new createjs.Point(((251.0000) * transformX) + moveX, ((1752.0000) * transformY) + moveY),
        new createjs.Point(((250.6667) * transformX) + moveX, ((1753.6665) * transformY) + moveY),
        new createjs.Point(((250.3333) * transformX) + moveX, ((1755.3335) * transformY) + moveY),

        new createjs.Point(((250.0000) * transformX) + moveX, ((1757.0000) * transformY) + moveY),
        new createjs.Point(((249.6667) * transformX) + moveX, ((1757.0000) * transformY) + moveY),
        new createjs.Point(((249.3333) * transformX) + moveX, ((1757.0000) * transformY) + moveY),

        new createjs.Point(((249.0000) * transformX) + moveX, ((1757.0000) * transformY) + moveY),
        new createjs.Point(((248.6667) * transformX) + moveX, ((1758.3332) * transformY) + moveY),
        new createjs.Point(((248.3333) * transformX) + moveX, ((1759.6668) * transformY) + moveY),

        new createjs.Point(((248.0000) * transformX) + moveX, ((1761.0000) * transformY) + moveY),
        new createjs.Point(((247.6667) * transformX) + moveX, ((1761.0000) * transformY) + moveY),
        new createjs.Point(((247.3333) * transformX) + moveX, ((1761.0000) * transformY) + moveY),

        new createjs.Point(((247.0000) * transformX) + moveX, ((1761.0000) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((1761.9999) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((1763.0001) * transformY) + moveY),

        new createjs.Point(((247.0000) * transformX) + moveX, ((1764.0000) * transformY) + moveY),
        new createjs.Point(((246.6667) * transformX) + moveX, ((1764.0000) * transformY) + moveY),
        new createjs.Point(((246.3333) * transformX) + moveX, ((1764.0000) * transformY) + moveY),

        new createjs.Point(((246.0000) * transformX) + moveX, ((1764.0000) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1764.6666) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1765.3334) * transformY) + moveY),

        new createjs.Point(((246.0000) * transformX) + moveX, ((1766.0000) * transformY) + moveY),
        new createjs.Point(((245.6667) * transformX) + moveX, ((1766.0000) * transformY) + moveY),
        new createjs.Point(((245.3333) * transformX) + moveX, ((1766.0000) * transformY) + moveY),

        new createjs.Point(((245.0000) * transformX) + moveX, ((1766.0000) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((1766.9999) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((1768.0001) * transformY) + moveY),

        new createjs.Point(((245.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),
        new createjs.Point(((244.6667) * transformX) + moveX, ((1769.0000) * transformY) + moveY),
        new createjs.Point(((244.3333) * transformX) + moveX, ((1769.0000) * transformY) + moveY),

        new createjs.Point(((244.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),
        new createjs.Point(((243.6667) * transformX) + moveX, ((1770.3332) * transformY) + moveY),
        new createjs.Point(((243.3333) * transformX) + moveX, ((1771.6668) * transformY) + moveY),

        new createjs.Point(((243.0000) * transformX) + moveX, ((1773.0000) * transformY) + moveY),
        new createjs.Point(((242.6667) * transformX) + moveX, ((1773.0000) * transformY) + moveY),
        new createjs.Point(((242.3333) * transformX) + moveX, ((1773.0000) * transformY) + moveY),

        new createjs.Point(((242.0000) * transformX) + moveX, ((1773.0000) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((1773.9999) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((1775.0001) * transformY) + moveY),

        new createjs.Point(((242.0000) * transformX) + moveX, ((1776.0000) * transformY) + moveY),
        new createjs.Point(((241.6667) * transformX) + moveX, ((1776.0000) * transformY) + moveY),
        new createjs.Point(((241.3333) * transformX) + moveX, ((1776.0000) * transformY) + moveY),

        new createjs.Point(((241.0000) * transformX) + moveX, ((1776.0000) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1776.6666) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1777.3334) * transformY) + moveY),

        new createjs.Point(((241.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((240.6667) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((240.3333) * transformX) + moveX, ((1778.0000) * transformY) + moveY),

        new createjs.Point(((240.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((1778.9999) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((1780.0001) * transformY) + moveY),

        new createjs.Point(((240.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((239.6667) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((239.3333) * transformX) + moveX, ((1781.0000) * transformY) + moveY),

        new createjs.Point(((239.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((238.6667) * transformX) + moveX, ((1782.3332) * transformY) + moveY),
        new createjs.Point(((238.3333) * transformX) + moveX, ((1783.6668) * transformY) + moveY),

        new createjs.Point(((238.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((237.6667) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((237.3333) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((237.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((1785.9999) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((1787.0001) * transformY) + moveY),

        new createjs.Point(((237.0000) * transformX) + moveX, ((1788.0000) * transformY) + moveY),
        new createjs.Point(((236.6667) * transformX) + moveX, ((1788.0000) * transformY) + moveY),
        new createjs.Point(((236.3333) * transformX) + moveX, ((1788.0000) * transformY) + moveY),

        new createjs.Point(((236.0000) * transformX) + moveX, ((1788.0000) * transformY) + moveY),
        new createjs.Point(((236.0000) * transformX) + moveX, ((1788.6666) * transformY) + moveY),
        new createjs.Point(((236.0000) * transformX) + moveX, ((1789.3334) * transformY) + moveY),

        new createjs.Point(((236.0000) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((235.6667) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((235.3333) * transformX) + moveX, ((1790.0000) * transformY) + moveY),

        new createjs.Point(((235.0000) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1790.9999) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1792.0001) * transformY) + moveY),

        new createjs.Point(((235.0000) * transformX) + moveX, ((1793.0000) * transformY) + moveY),
        new createjs.Point(((234.6667) * transformX) + moveX, ((1793.0000) * transformY) + moveY),
        new createjs.Point(((234.3333) * transformX) + moveX, ((1793.0000) * transformY) + moveY),

        new createjs.Point(((234.0000) * transformX) + moveX, ((1793.0000) * transformY) + moveY),
        new createjs.Point(((233.6667) * transformX) + moveX, ((1794.3332) * transformY) + moveY),
        new createjs.Point(((233.3333) * transformX) + moveX, ((1795.6668) * transformY) + moveY),

        new createjs.Point(((233.0000) * transformX) + moveX, ((1797.0000) * transformY) + moveY),
        new createjs.Point(((232.6667) * transformX) + moveX, ((1797.0000) * transformY) + moveY),
        new createjs.Point(((232.3333) * transformX) + moveX, ((1797.0000) * transformY) + moveY),

        new createjs.Point(((232.0000) * transformX) + moveX, ((1797.0000) * transformY) + moveY),
        new createjs.Point(((232.0000) * transformX) + moveX, ((1797.9999) * transformY) + moveY),
        new createjs.Point(((232.0000) * transformX) + moveX, ((1799.0001) * transformY) + moveY),

        new createjs.Point(((232.0000) * transformX) + moveX, ((1800.0000) * transformY) + moveY),
        new createjs.Point(((231.6667) * transformX) + moveX, ((1800.0000) * transformY) + moveY),
        new createjs.Point(((231.3333) * transformX) + moveX, ((1800.0000) * transformY) + moveY),

        new createjs.Point(((231.0000) * transformX) + moveX, ((1800.0000) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1800.6666) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1801.3334) * transformY) + moveY),

        new createjs.Point(((231.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((230.6667) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((230.3333) * transformX) + moveX, ((1802.0000) * transformY) + moveY),

        new createjs.Point(((230.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((230.0000) * transformX) + moveX, ((1802.9999) * transformY) + moveY),
        new createjs.Point(((230.0000) * transformX) + moveX, ((1804.0001) * transformY) + moveY),

        new createjs.Point(((230.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((229.6667) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((229.3333) * transformX) + moveX, ((1805.0000) * transformY) + moveY),

        new createjs.Point(((229.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((228.6667) * transformX) + moveX, ((1806.3332) * transformY) + moveY),
        new createjs.Point(((228.3333) * transformX) + moveX, ((1807.6668) * transformY) + moveY),

        new createjs.Point(((228.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((227.6667) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((227.3333) * transformX) + moveX, ((1809.0000) * transformY) + moveY),

        new createjs.Point(((227.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1809.9999) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1811.0001) * transformY) + moveY),

        new createjs.Point(((227.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((226.6667) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((226.3333) * transformX) + moveX, ((1812.0000) * transformY) + moveY),

        new createjs.Point(((226.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((225.6667) * transformX) + moveX, ((1813.6665) * transformY) + moveY),
        new createjs.Point(((225.3333) * transformX) + moveX, ((1815.3335) * transformY) + moveY),

        new createjs.Point(((225.0000) * transformX) + moveX, ((1817.0000) * transformY) + moveY),
        new createjs.Point(((224.6667) * transformX) + moveX, ((1817.0000) * transformY) + moveY),
        new createjs.Point(((224.3333) * transformX) + moveX, ((1817.0000) * transformY) + moveY),

        new createjs.Point(((224.0000) * transformX) + moveX, ((1817.0000) * transformY) + moveY),
        new createjs.Point(((224.0000) * transformX) + moveX, ((1817.6666) * transformY) + moveY),
        new createjs.Point(((224.0000) * transformX) + moveX, ((1818.3334) * transformY) + moveY),

        new createjs.Point(((224.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((223.6667) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((223.3333) * transformX) + moveX, ((1819.0000) * transformY) + moveY),

        new createjs.Point(((223.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1819.9999) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1821.0001) * transformY) + moveY),

        new createjs.Point(((223.0000) * transformX) + moveX, ((1822.0000) * transformY) + moveY),
        new createjs.Point(((222.6667) * transformX) + moveX, ((1822.0000) * transformY) + moveY),
        new createjs.Point(((222.3333) * transformX) + moveX, ((1822.0000) * transformY) + moveY),

        new createjs.Point(((222.0000) * transformX) + moveX, ((1822.0000) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((1822.6666) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((1823.3334) * transformY) + moveY),

        new createjs.Point(((222.0000) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((221.6667) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((221.3333) * transformX) + moveX, ((1824.0000) * transformY) + moveY),

        new createjs.Point(((221.0000) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((220.0001) * transformX) + moveX, ((1827.3330) * transformY) + moveY),
        new createjs.Point(((218.9999) * transformX) + moveX, ((1830.6670) * transformY) + moveY),

        new createjs.Point(((218.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((217.6667) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((217.3333) * transformX) + moveX, ((1834.0000) * transformY) + moveY),

        new createjs.Point(((217.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((216.6667) * transformX) + moveX, ((1835.3332) * transformY) + moveY),
        new createjs.Point(((216.3333) * transformX) + moveX, ((1836.6668) * transformY) + moveY),

        new createjs.Point(((216.0000) * transformX) + moveX, ((1838.0000) * transformY) + moveY),
        new createjs.Point(((215.6667) * transformX) + moveX, ((1838.0000) * transformY) + moveY),
        new createjs.Point(((215.3333) * transformX) + moveX, ((1838.0000) * transformY) + moveY),

        new createjs.Point(((215.0000) * transformX) + moveX, ((1838.0000) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((1838.9999) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((1840.0001) * transformY) + moveY),

        new createjs.Point(((215.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((214.6667) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((214.3333) * transformX) + moveX, ((1841.0000) * transformY) + moveY),

        new createjs.Point(((214.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((213.6667) * transformX) + moveX, ((1842.6665) * transformY) + moveY),
        new createjs.Point(((213.3333) * transformX) + moveX, ((1844.3335) * transformY) + moveY),

        new createjs.Point(((213.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((212.6667) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((212.3333) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((212.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((211.6667) * transformX) + moveX, ((1847.3332) * transformY) + moveY),
        new createjs.Point(((211.3333) * transformX) + moveX, ((1848.6668) * transformY) + moveY),

        new createjs.Point(((211.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((210.6667) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((210.3333) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((210.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((1850.9999) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((1852.0001) * transformY) + moveY),

        new createjs.Point(((210.0000) * transformX) + moveX, ((1853.0000) * transformY) + moveY),
        new createjs.Point(((209.6667) * transformX) + moveX, ((1853.0000) * transformY) + moveY),
        new createjs.Point(((209.3333) * transformX) + moveX, ((1853.0000) * transformY) + moveY),

        new createjs.Point(((209.0000) * transformX) + moveX, ((1853.0000) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((1853.6666) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((1854.3334) * transformY) + moveY),

        new createjs.Point(((209.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((208.6667) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((208.3333) * transformX) + moveX, ((1855.0000) * transformY) + moveY),

        new createjs.Point(((208.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((1855.9999) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((1857.0001) * transformY) + moveY),

        new createjs.Point(((208.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((201.9622) * transformX) + moveX, ((1871.3856) * transformY) + moveY),
        new createjs.Point(((195.2003) * transformX) + moveX, ((1885.7513) * transformY) + moveY)

    ];

    var points2 = [
        new createjs.Point(startX2, startY2),
        new createjs.Point(endX2, endY2)
    ];
    var points1Final = points;

    var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath, motionPath2);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final), getMotionPathFromPoints(points2Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 7", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("24", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        loop = false;
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 60)).on('change', (function(event) {

            if (currentMotionStep == 1) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        this.timeline.addTween(cjs.Tween.get(bar2).wait(40).to({
            guide: {
                path: motionPath2
            }
        }, 20).wait(50).call(function() {
            gotoLastAndStop(Stage1);
            Stage1.shape.visible = false;
        })).on('change', (function(event) {
            if (currentMotionStep == 2) {
                bar2 = drawCharacter(thisStage, bar2, true, event);
            }
        }));



    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];
                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }

            if (thisbar.x == 726.528) { // finished plotting horizontal line
                Stage1.shape.visible = false;
            } else if (thisbar.x == startX && thisbar.y == startY) { // plot 1st point
                Stage1.shape.visible = true;
            }

            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.32)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.paused = false;
            thisStage.movie.play();
        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);

        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }


    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        // thisStage.pencil=thisStage.movie.pencil;
        // thisStage.addChild(thisStage.pencil);
        if (thisStage.pencil) {
            thisStage.removeChild(thisStage.pencil);
        }
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function gotoLastAndStop(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);


        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        thisStage.pencil = thisStage.movie.pencil;
        thisStage.addChild(thisStage.pencil);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145);
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
