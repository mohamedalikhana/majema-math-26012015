(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p56_1.png",
            id: "p56_1"
        }, {
            src: "images/p56_2.png",
            id: "p56_2"
        }]
    };

    // symbols:
    (lib.p56_1 = function() {
        this.initialize(img.p56_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p56_2 = function() {
        this.initialize(img.p56_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("56", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.5, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Räkna och hitta bokstäverna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p56_1();
        this.instance.setTransform(173, 158, 0.47, 0.49);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 508, 367, 10);
        this.roundRect1.setTransform(0, 13);

        var ToBeAdded = [];

        var tmpArry = ['Ö', 'T', 'A', 'N', 'R', 'G', 'O', 'T', 'N', 'S', 'R', 'H', 'L', 'E', 'A', 'E', 'O']
        var tmpText = ["9  +  3  =", "9  +  7  =", "9  +  9  =", "8  +  3  =", "8  +  6  =", "7  +  6  =", "7  +  8  =", "8  +  9  =", "13  –  3  –  1  =", "12  –  1  –  1  =", "14  –  4  –  3  =", "17  –  7  –  5  =", "18  –  8  –  8  =", "15  –  5  –  2  =", "16  –  6  –  4  =", "16  –  6  –  7  =", "15  –  5  –  6  ="]
        var i = 0;
        var tmpXval = 240
        var temp_Line1 = new cjs.Shape();
        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 10; row++) {
                if (column == 1 && row > 2) {
                    continue;
                } else if (column == 2 && row > 5) {
                    continue;
                }
                if (column == 0 && row == 3) {
                    continue;
                }
                if (column == 0 && row == 7) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.662;
                } else if (column == 2) {
                    columnSpace = 1.31;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#958FC5").ss(0.7).drawRect(123 + (columnSpace * 268), 28 + (row * 29.1), 19,23);
                var temp_label = new cjs.Text(tmpArry[i], "16px 'Myriad Pro'");
                temp_label.setTransform(128 + (columnSpace * 267), 46 + (row * 29.1));
                var temp_labe2 = new cjs.Text(tmpText[i], "16px 'Myriad Pro'");
                if (column == 1) {
                    tmpXval = 215
                } else {
                    tmpXval = 240
                }
                temp_labe2.setTransform(25 + (columnSpace * tmpXval), 44 + (row * 29.1));
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(84 + (columnSpace * 270), 47 + (row * 29.1)).lineTo(115 + (columnSpace * 270), 47 + (row * 29.1));
                ToBeAdded.push(temp_label, temp_labe2, temp_Line1);
                i++;
            }
        }
        this.textbox_group1.setTransform(0, 0);
//Horizontal box line 1 
        this.textbox_group2 = new cjs.Shape();
        var i=2;
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#8490C8").ss(0.7).drawRect(18 + (columnSpace * 24), 346, 19,23);
            var temp_label = new cjs.Text(i, "16px 'Myriad Pro'");
            temp_label.setTransform(23 + (columnSpace * 25), 340);
            ToBeAdded.push(temp_label);
            i++
        }
        this.textbox_group2.setTransform(0, 0);
//Horizontal box Line 2
        this.textbox_group3 = new cjs.Shape();
        var i=5;
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            this.textbox_group3.graphics.f('#ffffff').s("#8490C8").ss(0.7).drawRect(103 + (columnSpace * 24), 346, 19,23);
            var temp_label = new cjs.Text(i, "16px 'Myriad Pro'");
            temp_label.setTransform(107 + (columnSpace * 25), 340);
            ToBeAdded.push(temp_label);
            i++
        }
        this.textbox_group3.setTransform(0, 0);

//Horizontal box Line 3
        this.textbox_group4 = new cjs.Shape();
        var i=8;
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            this.textbox_group4.graphics.f('#ffffff').s("#8490C8").ss(0.7).drawRect(187 + (columnSpace * 24), 346, 19,23);
            var temp_label = new cjs.Text(i, "16px 'Myriad Pro'");
            temp_label.setTransform(191 + (columnSpace * 25), 340);
            ToBeAdded.push(temp_label);
            i++
        }
        this.textbox_group4.setTransform(0, 0);

//Horizontal box Line 4
        this.textbox_group5 = new cjs.Shape();
        var i=10;
        for (var column = 0; column < 9; column++) {
            var columnSpace = column;
            this.textbox_group5.graphics.f('#ffffff').s("#8490C8").ss(0.7).drawRect(248 + (columnSpace * 24), 346, 19, 23);
            var temp_label = new cjs.Text(i, "16px 'Myriad Pro'");
            temp_label.setTransform(248 + (columnSpace * 24), 340);
            ToBeAdded.push(temp_label);
            i++
        }
        this.textbox_group5.setTransform(0, 0);


//this.textbox_group4.graphics.f('#ffffff').s("#8490C8").ss(0.7).drawRect(192 + (columnSpace * 25), 346, 21, 24);

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.textbox_group1, this.textbox_group2,this.textbox_group3,this.textbox_group4,this.textbox_group5);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 350);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text_q2.setTransform(19, 0);

        this.text_q1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_q1.setTransform(0, 0);

        this.instance = new lib.p56_2();
        this.instance.setTransform(15, 33, 0.48, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 250, 170, 10);
        this.roundRect1.setTransform(0, 13);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(257, 13);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#FAD3BC').drawRoundRect(0, 0, 212, 100, 8);
        this.roundRect3.setTransform(19, 26);

        this.roundRect4 = this.roundRect3.clone(true);
        this.roundRect4.setTransform(275, 26);


        var ToBeAdded = [];
        var i = 0;
        var tmpcolspace = 72;
        var tmprow = [' 9     –', '10    –', '        +', '–', '=', '=', '=', '', '']
        for (var col = 0; col < 4; col++) {
            var columnSpace = col
            for (var row = 0; row < 3; row++) {
                var rowSpace = row
                var tempText = new cjs.Text(tmprow[i], "16px 'Myriad Pro'");
                if (col == 1) {
                    tmpcolspace = 84
                } else {
                    tmpcolspace = 72
                }
                tempText.setTransform(35 + (tmpcolspace * columnSpace), 47 + (30 * rowSpace));
                ToBeAdded.push(tempText)
                i++
            }
        }

        var i = 0;
        var tmpcolspace = 60;
        var tmprow = ['+', '+', '–', '+', '=', '–', '=  9', '', '=']
        for (var col = 0; col < 3; col++) {
            var columnSpace = col
            for (var row = 0; row < 3; row++) {
                var rowSpace = row
                var tempText = new cjs.Text(tmprow[i], "16px 'Myriad Pro'");
                if (col == 2) {
                    tmpcolspace = 58
                } else {
                    tmpcolspace = 60
                }
                tempText.setTransform(320 + (tmpcolspace * columnSpace), 47 + (30 * rowSpace));
                ToBeAdded.push(tempText)
                i++
            }
        }

        this.textbox_group1 = new cjs.Shape();
        var tmpxpos = 76;
        for (var row = 0; row < 1; row++) {
            var rowSpace = row
            for (var col = 0; col < 6; col++) {
                var columnSpace = col
                if (col > 2) {
                    tmpxpos = 82
                } else {
                    tmpxpos = 75
                }
                this.textbox_group1.graphics.f('#ffffff').s("#9D9D9C").ss(0.9).drawRect(61 + (columnSpace * tmpxpos), 139, 20, 23);
                var tempText = new cjs.Text("=", "16px 'Myriad Pro'");
                tempText.setTransform(48 + (columnSpace * tmpxpos), 156);
                ToBeAdded.push(tempText)

            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text_q1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.textbox_group1, this.text_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 540, 190.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300, 490, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 104.7, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
