(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p88_1.png",
            id: "p88_1"
        }]
    };

    (lib.p88_1 = function() {
        this.initialize(img.p88_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Tid", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(272, 73);

        this.text_1 = new cjs.Text("30", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(40, 43);

        this.text_3 = new cjs.Text("kunna uppskatta tid", "9px 'Myriad Pro'", "#FAAA33");
        this.text_3.setTransform(100.5, 658);

        this.text_4 = new cjs.Text("88", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(39, 659);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 135, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.SymbolRect = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 203, 140, 5);
        this.round_Rect1.setTransform(0, 5);

        this.text_Rect1 = new cjs.Shape();
        this.text_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(0, 0, 114, 13, 20);
        this.text_Rect1.setTransform(49, 0);

        this.Symbolclock_1 = new lib.Symboldottedclock();
        this.Symbolclock_1.setTransform(105, 63, 0.85, 0.85);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(110, 40);
        this.hrLine_1.setTransform(50, 90);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#FFFFFF").setStrokeStyle(5.5).moveTo(0, 0).lineTo(0, 20);
        this.vrLine_1.setTransform(0, 65);

        this.text_dot2 = new cjs.Text(".", "48px 'Myriad Pro'", "#F8AC23");
        this.text_dot2.setTransform(-7, 74);

        this.addChild(this.round_Rect1, this.text_Rect1, this.Symbolclock_1, this.hrLine_1, this.vrLine_1, this.text_dot2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p88_1();
        this.instance.setTransform(0, 1, 0.47, 0.47);

        this.text_1 = new cjs.Text("Ringa in 3 saker som Mira kan göra under en lördag.", "16px 'Myriad Pro'");
        this.text_1.setTransform(60, 43);

        this.text_2 = new cjs.Text("Dra streck till förmiddag, eftermiddag eller kväll.", "16px 'Myriad Pro");
        this.text_2.setTransform(60, 66);

        this.text_3 = new cjs.Text("Rita visare och skriv klockslag som passar.", "16px 'Myriad Pro");
        this.text_3.setTransform(60, 89);

        this.text_4 = new cjs.Text("F Ö R M I D D A G", "9px 'Myriad Pro", "#F8AB22");
        this.text_4.setTransform(377, 106);

        this.text_5 = new cjs.Text("E F T E R M I D D A G", "9px 'Myriad Pro", "#F8AB22");
        this.text_5.setTransform(372, 267);

        this.text_6 = new cjs.Text("K V Ä L L", "9px 'Myriad Pro", "#F8AB22");
        this.text_6.setTransform(392, 427);

        this.round_Rect1 = new lib.SymbolRect();
        this.round_Rect1.setTransform(305, 96);

        this.round_Rect2 = new lib.SymbolRect();
        this.round_Rect2.setTransform(305, 257);

        this.round_Rect3 = new lib.SymbolRect();
        this.round_Rect3.setTransform(305, 417);

        this.addChild(this.instance, this.text_1, this.text_2, this.text_3, this.round_Rect1, this.round_Rect2, this.round_Rect3,
            this.text_4, this.text_5, this.text_6);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 570);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(302, 245, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
