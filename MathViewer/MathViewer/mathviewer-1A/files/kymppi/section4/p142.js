(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p142_1.png",
            id: "p142_1"
        }]
    };

    // symbols:
    (lib.p142_1 = function() {
        this.initialize(img.p142_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("142", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(36, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.5, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.SymbolClock = function() {
        this.initialize();

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#C8C9CB").s("#000000").ss(0.6, 0, 0, 4).arc(0, 0, 42, 0, 2 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#ffffff").s("#000000").ss(0.4, 0, 0, 4).arc(0, 0, 37.7, 0, 2 * Math.PI);
        this.shape_group2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(0, -38).lineTo(0, -35).moveTo(0, 35).lineTo(0, 38).moveTo(35, 0).lineTo(38, 0).moveTo(-35, 0).lineTo(-38, 0).moveTo(18, -33).lineTo(16.5, -30).moveTo(33, -19).lineTo(30, -17).moveTo(-30, 17.5).lineTo(-32.5, 19).moveTo(-17, 30).lineTo(-19, 32.5).moveTo(-31, -16).lineTo(-33.5, -18).moveTo(-19, -33).lineTo(-17, -30).moveTo(33, 17.5).lineTo(30.5, 16).moveTo(20, 31.5).lineTo(18.2, 29);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(10, -31);
        this.text_2 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(21.8, -20);
        this.text_3 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(27, -6);
        this.text_4 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(21.8, 10);
        this.text_5 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(10, 20);
        this.text_6 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(-4.5, 25);
        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(-19, 20);
        this.text_8 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(-31, 10);
        this.text_9 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(-36, -6);
        this.text_10 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(-32, -20);
        this.text_11 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(-20, -31);
        this.text_12 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(-7, -35.5);
        //center dot
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.lineHeight = 63;
        this.text_dot.setTransform(-6.5, -32);

        this.addChild(this.shape_group1, this.shape_group2, this.Line_1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_dot)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.SymbolClock_2 = function() {
        this.initialize();

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#C8C9CB").s("#000000").ss(0.6, 0, 0, 4).arc(0, 0, 42, 0, 2 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#ffffff").s("#000000").ss(0.4, 0, 0, 4).arc(0, 0, 37.7, 0, 2 * Math.PI);
        this.shape_group2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(0, -38).lineTo(0, -35).moveTo(0, 35).lineTo(0, 38).moveTo(35, 0).lineTo(38, 0).moveTo(-35, 0).lineTo(-38, 0).moveTo(18, -33).lineTo(16.5, -30).moveTo(33, -19).lineTo(30, -17).moveTo(-30, 17.5).lineTo(-32.5, 19).moveTo(-17, 30).lineTo(-19, 32.5).moveTo(-31, -16).lineTo(-33.5, -18).moveTo(-19, -33).lineTo(-17, -30).moveTo(33, 17.5).lineTo(30.5, 16).moveTo(20, 31.5).lineTo(18.2, 29);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(10, -31);
        this.text_2 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(21.8, -20);
        this.text_3 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(27, -6);
        this.text_4 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(21.8, 10);
        this.text_5 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(10, 20);
        this.text_6 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(-4.5, 25);
        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(-19, 20);
        this.text_8 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(-31, 10);
        this.text_9 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(-36, -6);
        this.text_10 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(-32, -20);
        this.text_11 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(-20, -31);
        this.text_12 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(-7, -35.5);
        //center dot
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.lineHeight = 63;
        this.text_dot.setTransform(-6.5, -32);

        this.dotted = new lib.DottedCircle(30, 12);
        this.dotted.setTransform(0, 0, 0.18, 0.18);

        this.addChild(this.shape_group1, this.shape_group2, this.Line_1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_dot, this.dotted)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    (lib.Symbol1 = function() {
        this.initialize();
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 251, 317, 10);
        this.roundRect1.setTransform(0, 25);
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(261, 0, 251, 317, 10);
        this.roundRect2.setTransform(0, 25);

        this.text = new cjs.Text("Vad är klockan om 1 timme? Rita visare och skriv tiden.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_q1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 0);

        this.text_h2 = new cjs.Text("Nu", "15px 'MyriadPro-Semibold'");
        this.text_h2.lineHeight = 19;
        this.text_h2.setTransform(44, 35);
        this.text_h3 = new cjs.Text("Om 1 timme", "15px 'MyriadPro-Semibold'");
        this.text_h3.lineHeight = 19;
        this.text_h3.setTransform(133, 35);

        this.text_h4 = new cjs.Text("Nu", "15px 'MyriadPro-Semibold'");
        this.text_h4.lineHeight = 19;
        this.text_h4.setTransform(308, 35);
        this.text_h5 = new cjs.Text("Om 1 timme", "15px 'MyriadPro-Semibold'");
        this.text_h5.lineHeight = 19;
        this.text_h5.setTransform(400, 35);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(0, 54.5).lineTo(251, 54.5).moveTo(0, 148.5).lineTo(245, 148.5)
            .moveTo(0, 245.5).lineTo(245, 245.5).moveTo(261, 54.5).lineTo(512, 54.5).moveTo(261, 148.5).lineTo(506, 148.5)
            .moveTo(261, 245.5).lineTo(506, 245.5);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(115, 25).lineTo(115, 338).moveTo(375, 25).lineTo(375, 338);
        this.hrLine_2.setTransform(0, 0);

        // row-1
        this.instance_clock1 = new lib.SymbolClock();
        this.instance_clock1.setTransform(55, 98, 0.83, 0.83);
        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(62, 93, 1, 0.7, 238);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(55, 84.3, 1, 0.85);
        this.text_dot1 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot1.lineHeight = 35;
        this.text_dot1.setTransform(50, 78);
        this.instance_clock2 = new lib.SymbolClock_2();
        this.instance_clock2.setTransform(177, 95, 0.78, 0.78);
        this.instance_clock3 = new lib.SymbolClock();
        this.instance_clock3.setTransform(322, 98, 0.83, 0.83);
        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(330, 97.5, 1, 0.7, 90);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(322, 84.3, 1, 0.85);
        this.text_dot2 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot2.lineHeight = 35;
        this.text_dot2.setTransform(317, 78);
        this.instance_clock4 = new lib.SymbolClock_2();
        this.instance_clock4.setTransform(437, 95, 0.78, 0.78);
        // // row-2   
        this.instance_clock5 = new lib.SymbolClock();
        this.instance_clock5.setTransform(55, 193, 0.83, 0.83);
        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(60, 201, 1, 0.7, 150);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(55, 179.3, 1, 0.85);
        this.text_dot3 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot3.lineHeight = 35;
        this.text_dot3.setTransform(50, 173);
        this.instance_clock6 = new lib.SymbolClock_2();
        this.instance_clock6.setTransform(177, 188.5, 0.78, 0.78);
        this.instance_clock7 = new lib.SymbolClock();
        this.instance_clock7.setTransform(322, 193, 0.83, 0.83);
        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(322, 202.3, 1, 0.7, 180);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(322, 179.3, 1, 0.85);
        this.text_dot4 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot4.lineHeight = 35;
        this.text_dot4.setTransform(317, 173);
        this.instance_clock8 = new lib.SymbolClock_2();
        this.instance_clock8.setTransform(437, 188.5, 0.78, 0.78);
        // // row-3
        this.instance_clock9 = new lib.SymbolClock();
        this.instance_clock9.setTransform(55, 290, 0.83, 0.83);
        this.shape_9 = new cjs.Shape(); // clock handle red
        this.shape_9.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_9.setTransform(50.5, 298, 1, 0.7, 208);
        this.shape_10 = new cjs.Shape(); // clock handle blue
        this.shape_10.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_10.setTransform(55, 276.3, 1, 0.85);
        this.text_dot5 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot5.lineHeight = 35;
        this.text_dot5.setTransform(50, 270);
        this.instance_clock10 = new lib.SymbolClock_2();
        this.instance_clock10.setTransform(177, 285.5, 0.78, 0.78);
        this.instance_clock11 = new lib.SymbolClock();
        this.instance_clock11.setTransform(322, 290, 0.83, 0.83);
        this.shape_11 = new cjs.Shape(); // clock handle red
        this.shape_11.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_11.setTransform(315, 294.3, 1, 0.7, 235);
        this.shape_12 = new cjs.Shape(); // clock handle blue
        this.shape_12.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_12.setTransform(322, 276.3, 1, 0.85);
        this.text_dot6 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot6.lineHeight = 35;
        this.text_dot6.setTransform(317, 270);
        this.instance_clock12 = new lib.SymbolClock_2();
        this.instance_clock12.setTransform(437, 285.5, 0.78, 0.78);

        var ToBeAdded = [];
        for (var col = 0; col < 2; col++) {
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                var colSpace = col;
                if (col == 2) {
                    colSpace = 2.1;
                }
                if (row == 2) {
                    rowSpace = 1.97;
                }
                var temptext = new cjs.Text("Klockan är          .", "16px 'Myriad Pro'");
                temptext.lineHeight = -1;
                temptext.setTransform(129 + (261 * colSpace), 127.5 + (97.5 * rowSpace));
                if (row == 2 && col == 1) {
                    rowSpace = 1.99;
                    colSpace = 1.009;
                }

                var tempLine_1 = new cjs.Shape();
                tempLine_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(201 + (261 * colSpace), 141 + (97.5 * rowSpace)).lineTo(232 + (261 * colSpace), 141 + (97.5 * rowSpace));
                tempLine_1.setTransform(0, 0);
                ToBeAdded.push(tempLine_1, temptext);
            }
        }

        this.addChild(this.roundRect1, this.roundRect2, this.text, this.text_q1, this.instance, this.hrLine_1, this.hrLine_2);
        this.addChild(this.instance_clock1, this.instance_clock2, this.instance_clock3, this.instance_clock4, this.instance_clock5, this.instance_clock6, this.instance_clock7, this.instance_clock8, this.instance_clock9, this.instance_clock10, this.instance_clock11, this.instance_clock12);
        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10, this.shape_11, this.shape_12, this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4, this.text_dot5, this.text_dot6);
        this.addChild(this.text_h2, this.text_h3, this.text_h4, this.text_h5);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 310);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många likadana armband kan du göra av pärlorna?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(19, 1);

        this.text_q1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 1);

        this.instance = new lib.p142_1();
        this.instance.setTransform(0, 35.5, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 24, 512, 188, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#BBD992').drawRoundRect(40, 34, 172, 79, 7);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#BBD992').drawRoundRect(280, 34, 209, 79, 7);
        this.roundRect3.setTransform(0, 0);

        // row-1
        this.text_1 = new cjs.Text("armband", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 22;
        this.text_1.setTransform(104, 184);
        this.text_2 = new cjs.Text("armband", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 22;
        this.text_2.setTransform(356, 184);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(259, 36).lineTo(259, 212);
        this.hrLine_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                var rowSpace = row;
                this.textbox_group1.graphics.f('#ffffff').s("#6D6E70").ss(0.8).drawRect(82.5 + (columnSpace * 250), 177 + (rowSpace * 32.5), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text_q1, this.roundRect1, this.roundRect2, this.roundRect3);
        this.addChild(this.instance, this.textbox_group1, this.text_1, this.text_2, this.hrLine_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 540, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(298, 113.7, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(298, 462, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
