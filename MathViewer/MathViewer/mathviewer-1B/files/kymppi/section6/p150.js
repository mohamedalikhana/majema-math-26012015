(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p150_1.png",
            id: "p150_1"
        }]
    };

    (lib.p150_1 = function() {
        this.initialize(img.p150_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("150", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#1A863A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(2, 10);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#1A863A");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text("  Följ instruktionen. Vem kommer du till?", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 261, 10);
        this.roundRect1.setTransform(0, 13);

        var crosswordBox_group = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s("#1A863A").ss(1).drawRect(0, 0, 337, 283, 0);
        this.Rect1.setTransform(0, 0);

        crosswordBox_group.addChild(this.Rect1);

        var hrlineX = [0, 0, 0, 0, 0];

        var hrlineY = [47.166, 94.333, 141.498, 188.664, 235.83];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#1A863A").setStrokeStyle(1).moveTo(0, 0).lineTo(337, 0);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            crosswordBox_group.addChild(hrLine_1);
        }

        var vrlineX = [48.14, 96.285, 144.42, 192.56, 240.7, 288.84];

        var vrlineY = [0, 0, 0, 0, 0, 0];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#1A863A").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 283);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            crosswordBox_group.addChild(vrLine_1);
        }


        var TxtlineX = [58.5, 156, 298, 110, 255, 11.5, 110, 298, 203, 159, 302];
        var TxtlineY = [44, 44, 44, 92, 92, 185, 185, 185, 231, 279, 279];
        var TxtArrNos = ['Mika', 'Tom', 'Jonas', 'Lea', 'Ella', 'Sam', 'Ida', 'Anna', 'Otto', 'Mia', 'Kai'];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text('' + TxtArrNos[i], "12px 'Myriad Pro'", "9D9C9C");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            crosswordBox_group.addChild(this.temp_label);
        }

        crosswordBox_group.setTransform(0, 287);

        this.butterfly = new lib.p150_1();
        this.butterfly.setTransform(8, 287.3, 0.47, 0.47);

        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.roundRect1, this.butterfly, crosswordBox_group);

        var wordBox_group = new cjs.Container();

        var firstColFillText = ['Mia', 'Tom', 'Otto', 'Anna', 'Jonas', 'Ella', 'Sam', 'Anna', 'Ida'];
        var count = 0;
        for (var col = 0; col < 3; col++) {
            for (var row = 0; row < 3; row++) {

                var fillColor = '#FFF374';
                var fillText = firstColFillText[count];
                var rectContainer = new lib.wordRectangle(fillText, fillColor)
                rectContainer.setTransform(165 * col, 85 * row);
                wordBox_group.addChild(rectContainer, this.verticalRectangle, this.horline, this.horline2, this.horline3);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group);

        var wordBox_group2 = new cjs.Container();

        var specialTexts = [];
        specialTexts = ['2', '1', '4', '2', '2', '2', '2', '3', '1'];
        var arrowPos = ['up', 'down', 'left', 'left', 'left', 'left', 'up', 'left', 'left']
        var count = 0;
        for (var column = 0; column < 3; column++) {
            for (var row = 0; row < 3; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle2(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(167 * column, 85.5 * row);
                wordBox_group2.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group2);


        var wordBox_group3 = new cjs.Container();

        specialTexts = ['1', '2', '1', '1', '4', '4', '2', '2', '3'];
        var arrowPos = ['left', 'right', 'up', 'down', 'down', 'down', 'right', 'down', 'up']
        var count = 0;
        for (var column = 0; column < 3; column++) {
            for (var row = 0; row < 3; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle3(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(167 * column, 85.5 * row);
                wordBox_group3.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group3);

        var lineArr = [];
        var lineX = [75, 242, 408, 75, 242, 408, 75, 242, 408];
        var lineY = [46, 46, 46, 132, 132, 132, 217, 217, 217];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 40).lineTo(57, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 650, 570);

    (lib.wordRectangle = function(fillText, fillColor, row_group) {
        this.initialize();

        var vertRect_group = new cjs.Container();

        this.verticalRectangle = new cjs.Shape();
        this.verticalRectangle.graphics.f("").s('#7d7d7d').drawRect(0, 0, 45, 60);
        this.verticalRectangle.setTransform(0, 0);

        this.horline = new cjs.Shape();
        this.horline.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(45, 0);
        this.horline.setTransform(0, 20);

        this.horline2 = new cjs.Shape();
        this.horline2.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(45, 0);
        this.horline2.setTransform(0, 40);

        vertRect_group.addChild(this.verticalRectangle, this.horline, this.horline2, this.horline3);
        vertRect_group.setTransform(19, 27);

        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, (45 - 1), (60 / 3) - 1);
        rect.setTransform(19, 27);

        var Xpos = 39,
            Ypos = 43;


        this.text = new cjs.Text(" " + fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(Xpos, Ypos);
        this.text.textAlign = 'center';
        this.addChild(vertRect_group, rect, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.wordRectangle2 = function(fillText, fillColor, position, count, row_group) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 4) {
            startX = 0;
            startY = 0;
        }
        if (count > 3 && count < 11) {
            startX = -2;
            startY = 0;
        }


        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 46, 62);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);

        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 42, 57);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 23, 57);
                break;
            case 'up':
                Arrow.setTransform(startX + 36, 65);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 36, 50);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }
        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle3 = function(fillText, fillColor, position, count, row_group) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 5) {
            startX = 0;
            startY = 0;
        }
        if (count > 4 && count < 9) {
            startX = -2;
            startY = 0;
        }
        if (count > 8) {
            startX = -6;
            startY = 0;
        }

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 45, 82);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);
        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 45, 78);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 31, 78);
                break;
            case 'up':
                Arrow.setTransform(startX + 37, 84);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 37, 69);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }

        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 100, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
