(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p54_1.png",
            id: "p54_1"
        }, {
            src: "images/p54_2.png",
            id: "p54_2"
        }]
    };

    (lib.p54_1 = function() {
        this.initialize(img.p54_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p54_2 = function() {
        this.initialize(img.p54_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.pageTitle = new cjs.Text("Öva subtraktion", "24px 'MyriadPro-Semibold'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(92, 25.2);

        this.text_1 = new cjs.Text("18", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(43.7, 22.2);

        this.text_2 = new cjs.Text("54", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(32, 648);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med subtraktion 0 till 6", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(66, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#88C13F").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#88C13F").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(28.5, 660.8, 0.9, 1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.instance = new lib.p54_1();
        this.instance.setTransform(34, 60, 0.39, 0.39);

        this.addChild(this.pageTitle, this.shape_2, this.shape_1, this.shape, this.text_1, this.text_2, this.instance,
            this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        var textArr = [];
        this.text_1 = new cjs.Text("Hur många finns kvar?", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(25, 0, 1, 1, 0, 1, 0);

        this.text_2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_2.lineHeight = 27;
        this.text_2.setTransform(5, 0);

        this.text_3 = new cjs.Text("Mira tar 2.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 22;
        this.text_3.setTransform(100, 28);
        this.text_4 = new cjs.Text("Mira tar 2.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 22;
        this.text_4.setTransform(368, 28);
        this.text_5 = new cjs.Text("Mira tar 3.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 22;
        this.text_5.setTransform(100, 145);
        this.text_6 = new cjs.Text("Mira tar 3.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 22;
        this.text_6.setTransform(368, 145);
        this.text_7 = new cjs.Text("Mira tar 4.", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 22;
        this.text_7.setTransform(100, 262);
        this.text_8 = new cjs.Text("Mira tar 4.", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 22;
        this.text_8.setTransform(368, 262);

        textArr.push(this.text_1);
        textArr.push(this.text_2);
        textArr.push(this.text_3);
        textArr.push(this.text_4);
        textArr.push(this.text_5);
        textArr.push(this.text_6);
        textArr.push(this.text_7);
        textArr.push(this.text_8);

        var textToBeDisplayed1 = ['3', '–', '2', '=', ' '];
        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(85 + (i * 20), 103, 20, 23);
            this.labelTemp1 = new cjs.Text(textToBeDisplayed1[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp1.lineHeight = 40;
            this.labelTemp1.setTransform(88 + (i * 20), 93.7);
            textToBeDisplayed1[i] = this.labelTemp1;
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(354 + (i * 20), 103, 20, 23);
        }
        this.textBoxGroup2.setTransform(0, 0);


        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(85 + (i * 20), 219, 20, 23);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(354 + (i * 20), 219, 20, 23);
        }
        this.textBoxGroup4.setTransform(0, 0);


        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(85 + (i * 20), 336.5, 20, 23);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(354 + (i * 20), 336.5, 20, 23);
        }
        this.textBoxGroup6.setTransform(0, 0);

        textArr.push(this.textBoxGroup1);
        textArr.push(this.textBoxGroup2);
        textArr.push(this.textBoxGroup3);
        textArr.push(this.textBoxGroup4);
        textArr.push(this.textBoxGroup5);
        textArr.push(this.textBoxGroup6);

        this.instance = new lib.p54_2();
        this.instance.setTransform(13, 15, 0.38, 0.38);


        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 110, 10);
        this.img_Rect1.setTransform(0, 16);

        this.img_Rect2 = new cjs.Shape();
        this.img_Rect2.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 110, 10);
        this.img_Rect2.setTransform(0, 133);

        this.img_Rect3 = new cjs.Shape();
        this.img_Rect3.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 110, 10);
        this.img_Rect3.setTransform(0, 250);

        this.img_Rect4 = new cjs.Shape();
        this.img_Rect4.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 110, 10);
        this.img_Rect4.setTransform(268, 16);

        this.img_Rect5 = new cjs.Shape();
        this.img_Rect5.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 110, 10);
        this.img_Rect5.setTransform(268, 133);

        this.img_Rect6 = new cjs.Shape();
        this.img_Rect6.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 7, 260, 110, 10);
        this.img_Rect6.setTransform(268, 250);




        this.addChild(this.instance, this.img_Rect1, this.img_Rect2, this.img_Rect3,
            this.img_Rect4, this.img_Rect5, this.img_Rect6);
        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed1[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 360);


    (lib.p54 = function() {
        this.initialize();
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(284.8, 245, 1, 1, 0, 0, 0, 256.3, -20);
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
