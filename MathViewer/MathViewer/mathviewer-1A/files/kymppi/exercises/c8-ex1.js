var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c8_ex1_1.png",
            id: "c8_ex1_1"
        }, {
            src: "images/c8_ex1_2.png",
            id: "c8_ex1_2"
        }]
    };



    (lib.c8_ex1_1 = function() {
        this.initialize(img.c8_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c8_ex1_2 = function() {
        this.initialize(img.c8_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 0", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("8", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();
        this.ropeKnots = new cjs.Container();
        this.ropeKnots1 = new lib.c8_ex1_1();
        this.ropeKnots1.setTransform(0, 10)

        this.ropeKnots2 = new lib.c8_ex1_1();
        this.ropeKnots2.setTransform(197, 7.5)
        this.ropeKnots.addChild(this.ropeKnots1, this.ropeKnots2);
        this.ropeKnots.setTransform(365, 101);

        this.ropeNoKnots = new cjs.Container();
        this.ropeNoKnots1 = new lib.c8_ex1_2();
        this.ropeNoKnots1.setTransform(0, 11.5)
        this.ropeNoKnots1.alpha = 0;
        this.ropeNoKnots2 = new lib.c8_ex1_2();
        this.ropeNoKnots2.setTransform(197, 8.5)
        this.ropeNoKnots2.alpha = 0;
        this.ropeNoKnots.addChild(this.ropeNoKnots2, this.ropeNoKnots1);
        this.ropeNoKnots.setTransform(365, 101);

        this.questionText = new cjs.Text("En fågel till kommer.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(570, 0);
    
        this.addChild(this.ropeKnots, this.ropeNoKnots);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();

            this.answers = new lib.answers();
        this.answers.text_1.visible = true;
        this.answers.setTransform(190, 20, 3, 3, 0, 0, 0, 0, 0);
        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });



        


        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage2 = new lib.Stage2();
        this.stage2.stage1.ropeKnots2.alpha = 0;
        this.stage2.stage1.ropeNoKnots2.alpha = 1;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage2.stage1.ropeKnots2,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500+1000+2000,
            alphaTimeout: 1000
        });
        this.tweens.push({
            ref: this.stage2.stage1.ropeNoKnots2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 200+1000+2000,
            alphaTimeout: 1500
        });
        this.tweens.push({
            ref: this.stage2.answers.text_1,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 500
        });
        

        // this.tweens.push({
        //     ref: this.stage2.stage1.ropeNoKnots2,
        //     alphaFrom: 1,
        //     alphaTo: 0,
        //     wait: 500,
        //     alphaTimeout: 2000
        // });

        p.tweens = this.tweens;
        this.addChild(this.stage2, this.ropeKnots);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage3 = new lib.Stage3();
        this.stage3.stage2.stage1.ropeNoKnots2.alpha = 1;
        this.stage3.stage2.stage1.ropeNoKnots1.alpha = 0;
        this.stage3.stage2.stage1.ropeKnots1.alpha = 1;
        this.stage3.stage2.stage1.ropeKnots2.alpha = 0;
        this.stage3.stage2.answers.text_1.visible=false;
        this.stage3.stage2.answers.text_2.visible=true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage3.stage2.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
 (lib.Stage5 = function() {
        this.initialize();
        this.stage4 = new lib.Stage4();
        this.stage4.stage3.stage2.stage1.ropeNoKnots2.alpha = 1;
        this.stage4.stage3.stage2.stage1.ropeNoKnots1.alpha = 1;
        this.stage4.stage3.stage2.stage1.ropeKnots1.alpha = 1;
        this.stage4.stage3.stage2.stage1.ropeKnots2.alpha = 0;
        this.stage4.stage3.stage2.answers.text_1.visible=false;
        this.stage4.stage3.stage2.answers.text_2.visible=true;
        this.tweens = [];
       this.tweens.push({
            ref: this.stage4.stage3.stage2.stage1.ropeKnots1,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500+3000,
            alphaTimeout: 1000
        });
        this.tweens.push({
            ref: this.stage4.stage3.stage2.stage1.ropeNoKnots1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 200+3000,
            alphaTimeout: 1500
        });
        this.tweens.push({
            ref: this.stage4.stage3.stage2.answers.text_2,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 500
        });

        p.tweens = this.tweens;
        this.addChild(this.stage4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
(lib.Stage6 = function() {
        this.initialize();
        this.stage5 = new lib.Stage5();
        this.stage5.stage4.stage3.stage2.stage1.ropeNoKnots2.alpha = 1;
        this.stage5.stage4.stage3.stage2.stage1.ropeNoKnots1.alpha = 1;
        this.stage5.stage4.stage3.stage2.stage1.ropeKnots1.alpha = 0;
        this.stage5.stage4.stage3.stage2.stage1.ropeKnots2.alpha = 0;
        this.stage5.stage4.stage3.stage2.answers.text_2.visible=false;
        this.stage5.stage4.stage3.stage2.answers.text_3.visible=true;
        this.tweens = [];
       
        this.tweens.push({
            ref: this.stage5.stage4.stage3.stage2.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 500
        });

        p.tweens = this.tweens;
        this.addChild(this.stage5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("2 knutar", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(95, 80);

        this.text_2 = new cjs.Text("1 knut", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(95, 80);

        this.text_3 = new cjs.Text("0 knutar", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(95, 80);



        this.addChild(this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage6 = new lib.Stage6();
        this.stage6.visible = false;
        this.stage6.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4,this.stage5, this.stage6);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
