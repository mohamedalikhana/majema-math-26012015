(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p62_1.png",
            id: "p62_1"
        }, {
            src: "images/p62_2.png",
            id: "p62_2"
        }]
    };

    (lib.p62_1 = function() {
        this.initialize(img.p62_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p62_2 = function() {
        this.initialize(img.p62_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.pageTitle = new cjs.Text("Vi övar", "24px 'MyriadPro-Semibold'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(93.5, 45);

        this.text_1 = new cjs.Text("21", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46.2, 41);

        this.text_2 = new cjs.Text("62", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(37, 668);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med addition och subtraktion 0 till 6", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 671);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#88C13F").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 43);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#88C13F").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 680.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.instance = new lib.p62_1();
        this.instance.setTransform(33, 72, 0.382, 0.41);

        this.addChild(this.pageTitle, this.shape_2, this.shape_1, this.shape, this.text_1, this.text_2, this.instance,
            this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        var textArr = [];
        this.text_1 = new cjs.Text("Hur många kex är det tillsammans?", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(28, 1, 1, 1, 0, 1, 0);

        this.text_2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(8, 1);

        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(81 + (i * 20), 95, 20, 21);
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(81 + (i * 20), 200, 20, 21);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(81 + (i * 20), 309, 20, 21);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(343 + (i * 20), 95, 20, 21);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(343 + (i * 20), 200, 20, 21);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(343 + (i * 20), 309, 20, 21);
        }
        this.textBoxGroup6.setTransform(0, 0);

        this.instance = new lib.p62_2();
        this.instance.setTransform(15, 25, 0.35, 0.35);

        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(10, 10, 250, 99, 10);
        this.img_Rect1.setTransform(0, 14);
        this.img_Rect4 = new cjs.Shape();
        this.img_Rect4.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 10, 250, 99, 10);
        this.img_Rect4.setTransform(258, 14);

        this.img_Rect2 = new cjs.Shape();
        this.img_Rect2.graphics.s("#D0CECE").ss(1).drawRoundRect(10, 5, 250, 99, 10);
        this.img_Rect2.setTransform(0, 125);
        this.img_Rect5 = new cjs.Shape();
        this.img_Rect5.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 5, 250, 99, 10);
        this.img_Rect5.setTransform(258, 125);

        this.img_Rect3 = new cjs.Shape();
        this.img_Rect3.graphics.s("#D0CECE").ss(1).drawRoundRect(10, 0, 250, 99, 10);
        this.img_Rect3.setTransform(0, 238);
        this.img_Rect6 = new cjs.Shape();
        this.img_Rect6.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 0, 250, 99, 10);
        this.img_Rect6.setTransform(258, 238);


        this.addChild(this.instance, this.img_Rect1, this.img_Rect2, this.img_Rect3,
            this.img_Rect4, this.img_Rect5, this.img_Rect6, this.text_1, this.text_2, this.textBoxGroup1, this.textBoxGroup2,
            this.textBoxGroup3, this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6);
        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 320);


    (lib.p54 = function() {
        this.initialize();
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(284.8, 278, 1, 1, 0, 0, 0, 253.3, -20);
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 319, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
