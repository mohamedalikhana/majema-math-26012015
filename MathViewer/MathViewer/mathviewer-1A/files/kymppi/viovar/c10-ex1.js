var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes
    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }, {
            src: "../exercises/images/hittaBase.png",
            id: "hittaBase"
        }, {
            src: "../viovar/images/c10_ex1_1.png",
            id: "sprites"
        }, {
            src: "../viovar/images/bat.png",
            id: "bat"
        }]
    };

    lib.init_Magic_Wand();

    var iconProperties = {
        x: 850 + 150,
        y: 350,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 150,
        wrongY: 345 - 15
    };

    var objects = [];



    (lib.balloon = function() {
        this.initialize(img.balloon);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.bat = function() {
        this.initialize(img.bat);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.present = function() {
        this.initialize(img.present);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.cup = function() {
        this.initialize(img.cup);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.icecream = function() {
        this.initialize(img.icecream);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.spoon = function() {
        this.initialize(img.spoon);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.tick = function() {
        this.initialize(img.tick);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function generateImages(images, imageCount, X, Y, scaleX, scaleY, image) {
        X = 280, Y = 100;
        if (imageCount < 5) {
            //X=X+(100*((5-imageCount-1)));
            switch (imageCount) {
                case 1:
                    X = X + 200;
                    break;
                case 2:
                    X = X + 150;
                    break;
                case 3:
                    X = X + 100;
                    break;
                case 4:
                    X = X + 50;
                    break;
            }

        }
        for (var i = 0; i < imageCount; i++) {
            var iteration = i;
            if (5 <= i) {
                iteration = i - 5;
                Y = 200;
            }
            var tempImage = image.clone(true);
            tempImage.setTransform(X + (100 * iteration), Y, 0.5, 0.5);
            images.addChild(tempImage);
        }
        return images;
    }
    (lib.dice = function(count, properties, number) {
        this.initialize();
        //console.log(ballWidth);
        var ballWidth = properties.width;
        thisStage = this;
        this.square = new cjs.Shape();
        this.square.graphics.f("#ffffff").ss(0.5).s('black').drawRoundRect(0, 0, properties.width, properties.height, 6);
        ballWidth = (properties.width + properties.height) / 1.35;
        this.addChild(this.square);
        switch (count) {
            case 1:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 2:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 2 / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;

            case 3:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 4:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 5:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 6:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 9:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case -1:
                this.number = new cjs.Text('' + number, "72px 'Myriad Pro'", '#000000');
                this.number.textAlign = 'center';
                this.number.setTransform(30, 5)
                this.addChild(this.number);
                break;
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.CommentText = function() {
        this.initialize();
        this.hintText1 = new cjs.Text("Klicka på talet som passar.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 500);
        this.hintText2 = new cjs.Text("Vi räknar de blå bollarna.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText2.textAlign = 'center';
        this.hintText2.lineHeight = 19;
        this.hintText2.visible = false;
        this.hintText2.setTransform(550, 400);
        this.hintText3 = new cjs.Text("Vi säger tillsammans: 3 är lika med 3.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText3.textAlign = 'center';
        this.hintText3.lineHeight = 19;
        this.hintText3.visible = false;
        this.hintText3.setTransform(550, 400);
        this.hintText4 = new cjs.Text("Hur kan vi se att det är lika många?", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText4.textAlign = 'center';
        this.hintText4.lineHeight = 19;
        this.hintText4.visible = false;
        this.hintText4.setTransform(550, 400);
        this.addChild(this.hintText1, this.hintText2, this.hintText3, this.hintText4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;

        var data = {
            images: [img.sprites],
            frames: {
                width: 170,
                height: 170
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        for (var i = 0; i < 5; i++) {
            var tempObject = new createjs.Sprite(spriteSheet);
            tempObject.gotoAndStop(i);
            //            this.addChild(tempObject);
            objects.push(tempObject);
        }
        this.answerText = new cjs.Text("3 är lika med 3.", "36px 'Myriad Pro'")
        this.answerText.lineHeight = 19;
        this.answerText.textAlign = 'center';
        this.answerText.setTransform(663, 340);
        this.questionText = new cjs.Text("Hur många är hälften?", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -50);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        //this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.imageCount = 3;

        var startX = 375;
        var startY = 125;

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        var circles = [];
        var circleCount = 6;
        var startX = 45;
        thisStage.currentAnswer = 0;


        var imageX = 475,
            imageY = -20,
            scaleX = 0.8,
            scaleY = 0.8,
            boxesX = 455,
            boxesY = 360;
        var myStage = this;
        var imagePositions = [];
        imagePositions[2] = [];
        imagePositions[4] = [];
        imagePositions[6] = [];
        imagePositions[8] = [];

        imagePositions[2].push({
            x1: 50,
            y1: 50,
            x2: 250,
            y2: 250
        });
        imagePositions[2].push({
            x1: 50,
            y1: 150,
            x2: 250,
            y2: 150
        });
        imagePositions[2].push({
            x1: 150,
            y1: 50,
            x2: 150,
            y2: 250
        });
        imagePositions[4].push({
            x1: 50,
            y1: 50,
            x2: 50,
            y2: 250,
            x3: 250,
            y3: 50,
            x4: 250,
            y4: 250
        });
        imagePositions[6].push({
            x1: 50,
            y1: 50,
            x2: 50,
            y2: 250,
            x3: 250,
            y3: 50,
            x4: 250,
            y4: 250,
            x5: 450,
            y5: 50,
            x6: 450,
            y6: 250
        });
        imagePositions[8].push({
            x1: 50,
            y1: 50,
            x2: 50,
            y2: 250,
            x3: 250,
            y3: 50,
            x4: 250,
            y4: 250,
            x5: 450,
            y5: 50,
            x6: 450,
            y6: 250,
            x7: 650,
            y7: 50,
            x8: 650,
            y8: 250
        });



        console.log(imagePositions)
        var boxes = [];
        var boxeCount = 4;
        for (var i = 0; i < boxeCount; i++) {
            var box = new lib.dice(-1, {
                width: 60,
                height: 70
            }, i + 1);

            box.setTransform(boxesX + (110 * i), boxesY, 1.4, 1.4);
            box.value = i + 1;
            myStage.addChild(box);
            boxes.push(box);
            box.addEventListener('click', checkAnswer);
        };

        var imageCount = 0;

        function checkAnswer(e) {
            if (e.currentTarget.value == imageCount / 2) {
                myStage.onAnswerCorrect(e.currentTarget);
            } else {

                myStage.onAnswerIncorrect(e.currentTarget);
            }

        }

        myStage.addChild(this.commentTexts);
        exerciseCount = 0;
        // objects = shuffle(objects);

        this.onNewExercise = function(e) {
            if (exerciseCount < 5) {
                for (var i = 0; i < boxes.length; i++) {
                    boxes[i].number.color = "#000000";
                };
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;
                myStage.removeChild(myStage.imageSets);
                myStage.imageSets = new cjs.Container();
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;
                var ImgCnts = [4, 2, 6, 4, 8];
                var images = [];
                imageCount = ImgCnts[exerciseCount];

                for (var i = 0; i < imageCount; i++) {
                    images.push(objects[exerciseCount].clone())
                };
                if (imageCount > 0) {
                    var randomForm = (imageCount == 2) ? Math.floor((Math.random() * (imagePositions[imageCount].length))) : 0;

                    console.log(randomForm);
                    var randomPosition = Math.floor((Math.random() * imageCount) + 1) - 1 || Math.floor((Math.random() * imageCount) + 1) - 1 || 1;
                    switch (imageCount) {
                        case 2:
                            images[0].x = imagePositions[imageCount][randomForm].x1;
                            images[0].y = imagePositions[imageCount][randomForm].y1;
                            images[1].x = imagePositions[imageCount][randomForm].x2;
                            images[1].y = imagePositions[imageCount][randomForm].y2;
                            break;
                        case 4:
                            images[0].x = imagePositions[imageCount][randomForm].x1;
                            images[0].y = imagePositions[imageCount][randomForm].y1;
                            images[1].x = imagePositions[imageCount][randomForm].x2;
                            images[1].y = imagePositions[imageCount][randomForm].y2;
                            images[2].x = imagePositions[imageCount][randomForm].x3;
                            images[2].y = imagePositions[imageCount][randomForm].y3;
                            images[3].x = imagePositions[imageCount][randomForm].x4;
                            images[3].y = imagePositions[imageCount][randomForm].y4;
                            break;
                        case 6:
                            images[0].x = imagePositions[imageCount][randomForm].x1;
                            images[0].y = imagePositions[imageCount][randomForm].y1;
                            images[1].x = imagePositions[imageCount][randomForm].x2;
                            images[1].y = imagePositions[imageCount][randomForm].y2;
                            images[2].x = imagePositions[imageCount][randomForm].x3;
                            images[2].y = imagePositions[imageCount][randomForm].y3;
                            images[3].x = imagePositions[imageCount][randomForm].x4;
                            images[3].y = imagePositions[imageCount][randomForm].y4;
                            images[4].x = imagePositions[imageCount][randomForm].x5;
                            images[4].y = imagePositions[imageCount][randomForm].y5;
                            images[5].x = imagePositions[imageCount][randomForm].x6;
                            images[5].y = imagePositions[imageCount][randomForm].y6;
                            break;
                        case 8:
                            images[0].x = imagePositions[imageCount][randomForm].x1;
                            images[0].y = imagePositions[imageCount][randomForm].y1;
                            images[1].x = imagePositions[imageCount][randomForm].x2;
                            images[1].y = imagePositions[imageCount][randomForm].y2;
                            images[2].x = imagePositions[imageCount][randomForm].x3;
                            images[2].y = imagePositions[imageCount][randomForm].y3;
                            images[3].x = imagePositions[imageCount][randomForm].x4;
                            images[3].y = imagePositions[imageCount][randomForm].y4;
                            images[4].x = imagePositions[imageCount][randomForm].x5;
                            images[4].y = imagePositions[imageCount][randomForm].y5;
                            images[5].x = imagePositions[imageCount][randomForm].x6;
                            images[5].y = imagePositions[imageCount][randomForm].y6;
                            images[6].x = imagePositions[imageCount][randomForm].x7;
                            images[6].y = imagePositions[imageCount][randomForm].y7;
                            images[7].x = imagePositions[imageCount][randomForm].x8;
                            images[7].y = imagePositions[imageCount][randomForm].y8;
                            break;
                    }



                }
                for (var i = 0; i < imageCount; i++) {

                    myStage.imageSets.addChild(images[i])
                };

                var xTransform = (imageCount == 8) ? -155 : (imageCount == 6) ? -80 : 0; // tranform Xpos when object count exceeds 4

                myStage.imageSets.setTransform(imageX + xTransform, imageY, scaleX, scaleY)
                myStage.addChild(myStage.imageSets);

                new lib.Magic_Wand(myStage, false);

                exerciseCount = exerciseCount + 1;
            }
            if (typeof btn_newExercise != 'undefined') {
                $(btn_newExercise).hide();
            }
        };
        var showIncorrectAnswerTween = null;
        var currentTimeoutAnimation = null;
        this.onAnswerCorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = true;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            if (exerciseCount < 5) {
                currentTimeoutAnimation = setTimeout(function() {
                    $(btn_newExercise).removeClass('disabled');
                    $(btn_newExercise).show();
                }, 1000)
            }
            stage.update();
        };
        this.onAnswerIncorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            box.number.color = "#ff0000";

            stage.update();
            showIncorrectAnswerTween = cjs.Tween.get(box.number).wait(4000).call(function(e) {
                box.number.color = "#000000";
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                stage.update();
            });
            $(btn_newExercise).hide();
        };
        this.onCheckAnswer = function() {
            myStage.correctIcon.visible = true;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            // for (var i = 0; i < circles.length; i++) {
            //     var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
            //         resetFillStyle[1] = "#ffffff";
            // }
            var answered = false;
            var selectedAnswers = [];
            for (var i = 0; i < myStage.dices.children.length; i++) {
                var resetFillStyle = myStage.dices.children[i].square.graphics._fillInstructions[0].params;
                if (resetFillStyle[1] === "#ffff00") {
                    selectedAnswers.push(myStage.dices.children[i]);
                }
            }
            for (var i = 0; i < selectedAnswers.length; i++) {
                if (selectedAnswers[i].value === myStage.answer) {
                    currentAnswers[1] = myStage.answer;
                    continue;
                } else {
                    currentAnswers[1] = 0;
                    break;
                }
            }

        };
        this.onIncorrectAnswer = function() {
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = true;
            // for (var i = 0; i < circles.length; i++) {
            //     var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
            //         resetFillStyle[1] = "#ffffff";
            // }
            cjs.Tween.get(myStage.dices).wait(500).call(function(e) {
                for (var i = 0; i < myStage.dices.children.length; i++) {
                    var resetFillStyle = myStage.dices.children[i].square.graphics._fillInstructions[0].params;
                    if (resetFillStyle[1] === "#ffff00") {
                        resetFillStyle[1] = "#ffffff";
                    }
                }
                stage.update();
            });

        };

        this.onNewExercise();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Hälften", "bold 24px 'Myriad Pro'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);

        this.text_1 = new cjs.Text("10", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(45, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)



        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
