(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p132_1.png",
            id: "p132_1"
        }, {
            src: "images/p132_2.png",
            id: "p132_2"
        }]
    };

    (lib.p132_1 = function() {
        this.initialize(img.p132_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p132_2 = function() {
        this.initialize(img.p132_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("46", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(57, 22);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8490C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(52, 23.5);

        this.text_2 = new cjs.Text("Talet 12", "24px 'MyriadPro-Semibold'", "#8490C8");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(103, 26);

        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 11 och 12", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(79, 649.5);

        this.text = new cjs.Text("132", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(44, 647);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(39.5, 660.8, 1, 1.1);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Part 1
    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("tolv", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(39, 123.5);

        this.text_col1 = new cjs.Text("tiotal", "10px 'Myriad Pro'");
        this.text_col1.lineHeight = 19;
        this.text_col1.setTransform(20, 30);

        this.text_col2 = new cjs.Text("ental", "10px 'Myriad Pro'");
        this.text_col2.lineHeight = 19;
        this.text_col2.setTransform(66, 30);

        this.text_1 = new cjs.Text("2", "bold 80px 'UusiTekstausMajema'");
        this.text_1.lineHeight = 96;
        this.text_1.setTransform(61, 30, 1, 1.1);

        this.text_no1 = new cjs.Text("1", "bold 80px 'UusiTekstausMajema'");
        this.text_no1.lineHeight = 96;
        this.text_no1.setTransform(14, 30, 1, 1.1);

        this.shape_rect1 = new cjs.Shape();
        this.shape_rect1.graphics.f('#ffffff').s("#000000").ss(0.5).drawRect(9, 23, 97, 97);
        this.shape_rect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 47).lineTo(106, 47);
        this.hrLine_1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(55, 23).lineTo(55, 120);
        this.Line_1.setTransform(0, 0);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#000000").s().p("");
        this.shape_39.setTransform(71, 58.5, 1, 1, 400);

        //start clock

        this.shape = new cjs.Shape(); // clock center dot
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(444, 79.4);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_1.setTransform(444, 71, 1, 1, 240);

        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(444, 65.1);

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(436.8, 46);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(423.1, 50.2);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(413.1, 59.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C6CBCC").s().p("AkPBPQhxhvAAifIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACghxBuQhxBxifAAQieAAhxhxg");
        this.shape_3.setTransform(444, 98.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohmQhohniSAAQiRAAhoBnQhoBmAACTIgfAAQAAifBxhvQBxhxCeAAQCfAABxBxQBxBvAACfg");
        this.shape_4.setTransform(444, 60.1);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(409.5, 73.5);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(414.2, 88);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(424.1, 98.2);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(439.6, 102.8);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(453.9, 98.6);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(465.1, 88.3);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(469.6, 74.2);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(464.9, 59.6);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(454, 50);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(444, 45.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(426.8, 49.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(414.3, 62.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(409.7, 79.4);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(414.3, 96.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAJIAJgR");
        this.shape_10.setTransform(426.8, 109.1);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(444, 113.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAJIgJgR");
        this.shape_12.setTransform(461.1, 109.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKAFIgTgJ");
        this.shape_13.setTransform(473.7, 96.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(478.3, 79.4);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKgEIgTAJ");
        this.shape_15.setTransform(473.7, 62.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(461.1, 49.7);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShnBoQhpBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABpBoQBnBoAACRg");
        this.shape_17.setTransform(444, 79.4);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(444, 79.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(444, 79.4);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(444, 79.4);

        //End clock       

        //  Rec

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQggAAgZAYQgYAYAAAhQAAAiAYAYQAZAYAgAAg");
        this.shape_21.setTransform(214, 51.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_22.setTransform(214, 51.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#0089CA").s("#000000").ss(0.5, 0, 0, 4).arc(0, 0, 8.2, 0, 2 * Math.PI);
        this.shape_52.setTransform(237, 50.7);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_23.setTransform(191.8, 51.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgZAYghAAQghAAgYgYg");
        this.shape_24.setTransform(191.8, 51.1);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_25.setTransform(169.9, 51.1);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQghAAgYgYg");
        this.shape_26.setTransform(169.9, 51.1);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_27.setTransform(146, 51.1);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_28.setTransform(146, 51.1);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao2AAIRsAA");
        this.shape_29.setTransform(191.6, 61.3);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_30.setTransform(226.4, 61.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_31.setTransform(203.2, 61.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_32.setTransform(180.5, 61.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_33.setTransform(158.1, 61.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfQgLAAgLAFQgXAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAIAXgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_34.setTransform(191.8, 61.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAXgMQALgFALAAIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMIgXAFg");
        this.shape_35.setTransform(191.8, 61.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_40.setTransform(229.7, 96.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_41.setTransform(229.7, 96.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_42.setTransform(201.3, 96.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_43.setTransform(201.3, 96.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_44.setTransform(172.5, 96.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_45.setTransform(172.5, 96.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_46.setTransform(144.2, 96.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_47.setTransform(144.2, 96.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#000000").s().p("AgegWIA9AWIg9AXg");
        this.shape_48.setTransform(250.7, 96.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_49.setTransform(192.5, 96.6);


        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_62.setTransform(146, 72.6);


        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_64.setTransform(170, 72.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_66.setTransform(192, 72.6);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_68.setTransform(214, 72.6);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_70.setTransform(237, 72.6);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_61.setTransform(146, 72.6);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_63.setTransform(170, 72.6);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_65.setTransform(192, 72.6);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_67.setTransform(214, 72.6);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_69.setTransform(237, 72.6);


        // line two
        this.shapetwo_29 = new cjs.Shape();
        this.shapetwo_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao2AAIRsAA");
        this.shapetwo_29.setTransform(321.6, 61.3);

        this.shapetwo_30 = new cjs.Shape();
        this.shapetwo_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_30.setTransform(356.4, 61.5);

        this.shapetwo_31 = new cjs.Shape();
        this.shapetwo_31.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_31.setTransform(333.2, 61.5);

        this.shapetwo_32 = new cjs.Shape();
        this.shapetwo_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_32.setTransform(310.5, 61.5);

        this.shapetwo_33 = new cjs.Shape();
        this.shapetwo_33.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_33.setTransform(288.1, 61.5);

        this.shapetwo_35 = new cjs.Shape();
        this.shapetwo_35.graphics.f("#0089CA").s("#000000").ss(0.5).p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shapetwo_35.setTransform(276, 51.1);

        this.shapetwo_36 = new cjs.Shape();
        this.shapetwo_36.graphics.f("#0089CA").s("#000000").ss(0.5).p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shapetwo_36.setTransform(299, 51.1);


        this.shapetwo_34 = new cjs.Shape();
        this.shapetwo_34.graphics.f("#ffffff").s("#000000").ss(0.5).drawRoundRect(0, 0, 113, 44, 5);
        this.shapetwo_34.setTransform(265, 40);

        // Main Yellow block
        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFF173").s("").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 509, 134, 10);
        this.shape_51.setTransform(0, 12);



        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(129, 91).lineTo(329.5 + 60, 91).moveTo(329.5 + 60, 91).lineTo(329.5 + 60, 89).lineTo(336 + 60, 91).lineTo(329.5 + 60, 93).lineTo(329.5 + 60, 91);
        var numberLineLimit = 12
        for (var dot = 0; dot < 13; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(126 + (21.8 * dot), 86).lineTo(126 + (21.8 * dot), 96);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(125, 91).lineTo(126 + (21.8 * numberLineLimit), 91);

        for (var dot = 0; dot < 13; dot++) {
            var temptext = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 13px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot > 9) ? (dot - 0.2) : dot;
            temptext.setTransform(122.5 + (21.8 * colSpace), 113);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);

        this.addChild(this.shape_51, this.shape_rect1, this.hrLine_1, this.Line_1, this.text_no1, this.text_col1, this.text_col2);
        this.addChild(this.shape_39, this.shape_38, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.shape_4, this.shape_3, this.text_4, this.text_3, this.text_2, this.shape_2, this.shape_1, this.shape, this.text_1, this.shape_52, this.text);

        this.addChild(this.numberLine, this.shape_61, this.shape_62, this.shape_63, this.shape_64, this.shape_65, this.shape_66, this.shape_67, this.shape_68);
        this.addChild(this.shape_69, this.shape_70);
        this.addChild(this.shapetwo_34, this.shapetwo_29, this.shapetwo_30, this.shapetwo_31, this.shapetwo_32,
            this.shapetwo_33, this.shapetwo_35, this.shapetwo_36);
        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 511.3, 107.1);


    (lib.Symbol3 = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 25, 510, 405, 10);
        this.round_Rect1.setTransform(0, 0);

        this.text = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(23, 0);

        this.text_11 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_11.lineHeight = 27;
        this.text_11.setTransform(4, 0);


        var colGrp = ['32', '200', '375'];

        this.shape_group1 = new cjs.Shape();
        for (var colIndex = 0; colIndex < colGrp.length; colIndex++) {
            var rowStartVal = parseInt(colGrp[colIndex]);

            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                for (var row = 0; row < 4; row++) {

                    if (row == 0 || row == 2) {
                        this.shape_group1.graphics.f("#24B24B").s("#616161").ss(0.8, 0, 0, 4).arc(rowStartVal + (columnSpace * 18), 54 + (row * 101), 7.5, 0, 2 * Math.PI);
                        this.shape_group1.graphics.f("#24B24B").s("#616161").ss(0.8, 0, 0, 4).arc(rowStartVal + (columnSpace * 18), 72 + (row * 101), 7.5, 0, 2 * Math.PI);


                    } else {
                        this.shape_group1.graphics.f("#FFF795").s("#616161").ss(0.8, 0, 0, 4).arc(rowStartVal + (columnSpace * 18), 54 + (row * 101), 7.5, 0, 2 * Math.PI);
                        this.shape_group1.graphics.f("#FFF795").s("#616161").ss(0.8, 0, 0, 4).arc(rowStartVal + (columnSpace * 18), 72 + (row * 101), 7.5, 0, 2 * Math.PI);
                    }
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        var rowGrp = ['130', '301', '477'];

        this.shape_group2 = new cjs.Shape();
        for (var rowIndex = 0; rowIndex < rowGrp.length; rowIndex++) {
            var colStartVal = parseInt(rowGrp[rowIndex]);

            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                for (var row = 0; row < 4; row++) {

                    if (column == 1 && row == 1 || row == 3) {
                        this.shape_group2.graphics.f("#FFF795").s("#616161").ss(0.8, 0, 0, 4).arc(colStartVal + (columnSpace * 18), 54 + (row * 101), 7.5, 0, 2 * Math.PI);
                    } else if (column == 1) {

                    } else if (column == 0 && row == 0 || row == 2) {
                        this.shape_group2.graphics.f("#24B24B").s("#616161").ss(0.8, 0, 0, 4).arc(colStartVal + (columnSpace * 18), 54 + (row * 101), 7.5, 0, 2 * Math.PI);
                    } else {
                        this.shape_group2.graphics.f("#FFF795").s("#616161").ss(0.8, 0, 0, 4).arc(colStartVal + (columnSpace * 18), 54 + (row * 101), 7.5, 0, 2 * Math.PI);
                    }
                }
            }
        }
        this.shape_group2.setTransform(0, 0);



        this.label1 = new cjs.Text("11 – 1 = ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 25;
        this.label1.setTransform(20, 102);
        this.label2 = new cjs.Text("12 – 4 = ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 25;
        this.label2.setTransform(20, 203);
        this.label3 = new cjs.Text("11 – 1 – 3  = ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 25;
        this.label3.setTransform(20, 304);
        this.label4 = new cjs.Text("12 – 2 – 2 = ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 25;
        this.label4.setTransform(20, 405);

        this.label5 = new cjs.Text("11 – 10 = ", "16px 'Myriad Pro'");
        this.label5.lineHeight = 25;
        this.label5.setTransform(189, 102);
        this.label6 = new cjs.Text("12 – 2 = ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 25;
        this.label6.setTransform(189, 203);
        this.label7 = new cjs.Text("11 – 1 – 5 = ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 25;
        this.label7.setTransform(189, 304);
        this.label8 = new cjs.Text("12 – 2 – 4 = ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 25;
        this.label8.setTransform(189, 405);

        this.label9 = new cjs.Text("11 – 0 = ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 25;
        this.label9.setTransform(364, 102);
        this.label10 = new cjs.Text("12 – 10 = ", "16px 'Myriad Pro'");
        this.label10.lineHeight = 25;
        this.label10.setTransform(364, 203);
        this.label11 = new cjs.Text("11 – 1 – 6 = ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 25;
        this.label11.setTransform(364, 304);
        this.label12 = new cjs.Text("12 – 2 – 5 = ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 25;
        this.label12.setTransform(364, 405);



        this.hrRule = new cjs.Shape();
        this.hrRule.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 127).lineTo(507, 127);
        this.hrRule.setTransform(0, 0);

        this.hrRule_2 = new cjs.Shape();
        this.hrRule_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 228).lineTo(507, 228);
        this.hrRule_2.setTransform(0, 0);

        this.hrRule_3 = new cjs.Shape();
        this.hrRule_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 329).lineTo(507, 329);
        this.hrRule_3.setTransform(0, 0);



        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170, 32).lineTo(170, 123);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(343, 32).lineTo(343, 123);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170, 133).lineTo(170, 224);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(343, 133).lineTo(343, 224);
        this.Line_4.setTransform(0, 0);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170, 232).lineTo(170, 325);
        this.Line_5.setTransform(0, 0);

        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(343, 232).lineTo(343, 325);
        this.Line_6.setTransform(0, 0);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170, 332).lineTo(170, 427);
        this.Line_7.setTransform(0, 0);

        this.Line_8 = new cjs.Shape();
        this.Line_8.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(343, 332).lineTo(343, 427);
        this.Line_8.setTransform(0, 0);

        //text line

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_2.setTransform(70, 75);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_3.setTransform(247, 75);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_4.setTransform(415, 75);

        this.hrLine_5 = new cjs.Shape();
        this.hrLine_5.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_5.setTransform(70, 175);

        this.hrLine_6 = new cjs.Shape();
        this.hrLine_6.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_6.setTransform(239, 175);

        this.hrLine_7 = new cjs.Shape();
        this.hrLine_7.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_7.setTransform(421, 175);

        this.hrLine_8 = new cjs.Shape();
        this.hrLine_8.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_8.setTransform(94, 276);

        this.hrLine_9 = new cjs.Shape();
        this.hrLine_9.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_9.setTransform(264, 276);

        this.hrLine_10 = new cjs.Shape();
        this.hrLine_10.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_10.setTransform(440, 276);

        this.hrLine_11 = new cjs.Shape();
        this.hrLine_11.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_11.setTransform(94, 378);

        this.hrLine_12 = new cjs.Shape();
        this.hrLine_12.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_12.setTransform(264, 378);

        this.hrLine_13 = new cjs.Shape();
        this.hrLine_13.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(60, 40);
        this.hrLine_13.setTransform(440, 378);


        this.addChild(this.round_Rect1, this.text, this.text_11);
        this.addChild(this.shape_group1, this.shape_group2, this.hrRule, this.hrRule_2, this.hrRule_3);

        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12);

        this.addChild(this.text, this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5,
            this.Line_6, this.Line_7, this.Line_8, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.hrLine_5, this.hrLine_6, this.hrLine_7, this.hrLine_8, this.hrLine_9, this.hrLine_10, this.hrLine_11, this.hrLine_12, this.hrLine_13);





    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 570.3, 410.8);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);



        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 275.2, 1, 1, 0, 0, 0, 255.1, 74.8);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(298, 95.9, 1, 1, 0, 0, 0, 254.6, 53.4);


        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
