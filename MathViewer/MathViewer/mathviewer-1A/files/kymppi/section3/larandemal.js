var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: []
    };

    (lib.Stage1_1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FF6025').ss(3).drawRoundRect(210, 0, 1100, 460, 13);
        this.roundRect1.setTransform(0, 0);

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#FF6025").s("#FF6025").ss(0.5, 0, 0, 4).arc(210, 0, 30, 0, 30 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.text_No = new cjs.Text("3", "35px 'MyriadPro-Semibold'", "#ffffff");
        this.text_No.lineHeight = 19;
        this.text_No.setTransform(200, -15);

        this.textRect = new cjs.Shape();
        this.textRect.graphics.f("#ffffff").s('#FF6025').ss(3).drawRoundRect(330, -25, 295, 50, 19);
        this.textRect.setTransform(0, 0);

        this.text_1 = new cjs.Text("TALEN 7 TILL 10", "bold 28px 'Myriad Pro'", "#FF6025");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(380, -12);

        this.text_2 = new cjs.Text("•  förstå och kunna använda talen 7 till 10", "35px 'Myriad Pro'", "#FF6025");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(330, 75);

        this.text_3 = new cjs.Text("•  kunna lösa uppgifter med addition 0 till 10", "35px 'Myriad Pro'", "#FF6025");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(330, 125);

        this.text_4 = new cjs.Text("•  kunna lösa uppgifter med subtraktion 0 till 10", "35px 'Myriad Pro'", "#FF6025");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(330, 175);

        this.text_5 = new cjs.Text("•  förstå och kunna använda den kommutativa lagen i addition", "35px 'Myriad Pro'", "#FF6025");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(330, 225);

        this.text_6 = new cjs.Text("•  förstå begreppet talfamilj", "35px 'Myriad Pro'", "#FF6025");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(330, 275);

        this.text_7 = new cjs.Text("•  förstå och kunna fortsätta ett påbörjat mönster", "35px 'Myriad Pro'", "#FF6025");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(330, 325);

        this.text_8 = new cjs.Text("•  förstå och kunna använda begreppet dubbelt", "35px 'Myriad Pro'", "#FF6025");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(330, 375);

        this.addChild(this.roundRect1, this.shape_group1, this.text_No, this.textRect, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage1_1();
        stage1_1.setTransform(-100, -70);

        this.addChild(stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
