(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p72_1.png",
            id: "p72_1"
        },
        {
            src: "images/p72_2.png",
            id: "p72_2"
        }
        ]
    };


    (lib.p72_1 = function() {
        this.initialize(img.p72_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p72_2 = function() {
        this.initialize(img.p72_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 127, 140);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text_4 = new cjs.Text("72", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(39, 650);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.instance = new lib.p72_1();
        this.instance.setTransform(43, 0, 0.81, 0.779);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(453 + (columnSpace * 33), 28 , 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape_2, this.shape_1, this.shape, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(5, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 25, 510, 240, 10);
        this.roundRect1.setTransform(0, 0);

        var colGrp = ['20', '190', '365'];

        this.shape_group1 = new cjs.Shape();
        for (var colIndex = 0; colIndex < colGrp.length; colIndex++) {
            var rowStartVal = parseInt(colGrp[colIndex]);

            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    if (colIndex == 2 && row == 2) {
                        continue;
                    }
                    this.shape_group1.graphics.f("#FFF795").s("#000000").ss(0.8, 0, 0, 4).arc(rowStartVal + (columnSpace * 20), 50 + (row * 75), 8.2, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        var rowGrp = ['130', '301', '477'];

        this.shape_group2 = new cjs.Shape();
        for (var rowIndex = 0; rowIndex < rowGrp.length; rowIndex++) {
            var colStartVal = parseInt(rowGrp[rowIndex]);

            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    if (rowIndex == 2 && row == 2) {
                        continue;
                    }
                    this.shape_group2.graphics.f("#FFF795").s("#000000").ss(0.8, 0, 0, 4).arc(colStartVal + (columnSpace * 20), 50 + (row * 75), 8.2, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                if(column==2){
                    columnSpace = 1.97;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(104 + (columnSpace * 178), 73 + (row * 75), 19, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("7  –  1  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 27;
        this.label1.setTransform(42, 77);
        this.label2 = new cjs.Text("7  –  4  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 27;
        this.label2.setTransform(42, 151);
        this.label3 = new cjs.Text("7  –  7  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 27;
        this.label3.setTransform(42, 227);

        this.label4 = new cjs.Text("7  –  0  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 27;
        this.label4.setTransform(220, 77);
        this.label5 = new cjs.Text("7  –  3  =  ", "16px 'Myriad Pro'");
        this.label5.lineHeight = 27;
        this.label5.setTransform(220, 151);
        this.label6 = new cjs.Text("7  –  6  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 27;
        this.label6.setTransform(220, 227);

        this.label7 = new cjs.Text("7  –  2  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 27;
        this.label7.setTransform(392, 77);
        this.label8 = new cjs.Text("7  –  5  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 27;
        this.label8.setTransform(392, 151);

        this.hrRule = new cjs.Shape();
        this.hrRule.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 105).lineTo(507, 105);
        this.hrRule.setTransform(0, 0);

        this.hrRule_2 = new cjs.Shape();
        this.hrRule_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 182).lineTo(507, 182);
        this.hrRule_2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170, 33).lineTo(170, 100);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(343, 33).lineTo(343, 100);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170, 110).lineTo(170, 178);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(343, 110).lineTo(343, 178);
        this.Line_4.setTransform(0, 0);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(170, 187).lineTo(170, 262);
        this.Line_5.setTransform(0, 0);

        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(343, 187).lineTo(343, 262);
        this.Line_6.setTransform(0, 0);

        this.instance = new lib.p72_2();
        this.instance.setTransform(393, 190,0.5,0.5);

        this.addChild(this.roundRect1, this.shape_group1, this.shape_group2, this.textbox_group1, this.hrRule, this.hrRule_2);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);

        this.addChild(this.instance_2, this.text, this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 6.3);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(5, 6.3);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 31, 510, 130, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(98 + (columnSpace * 150), 40 + (row * 29), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("6  +  1  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(35, 43);
        this.label2 = new cjs.Text("3  +  4  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(35, 73);
        this.label3 = new cjs.Text("5  +  2  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(35, 103);
        this.label4 = new cjs.Text("5  +  1  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(35, 131);

        this.label6 = new cjs.Text("2  +  5  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(185, 43);
        this.label7 = new cjs.Text("4  +  3  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(185, 73);
        this.label8 = new cjs.Text("3  +  3  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(185, 103);
        this.label9 = new cjs.Text("1  +  6  =  ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(185, 131);

        this.label11 = new cjs.Text("7  –  5  =  ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(336, 43);
        this.label12 = new cjs.Text("7  –  2  =  ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(336, 73);
        this.label13 = new cjs.Text("7  –  4  =  ", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(336, 103);
        this.label14 = new cjs.Text("7  –  6  =  ", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(336, 131);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.p72 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(294, 421, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 475, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
