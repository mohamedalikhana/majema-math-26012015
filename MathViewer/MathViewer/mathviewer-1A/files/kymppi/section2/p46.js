(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p46_1.png",
            id: "p46_1"
        }, {
            src: "images/p46_2.png",
            id: "p46_2"
        }]
    };

    (lib.p46_1 = function() {
        this.initialize(img.p46_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p46_2 = function() {
        this.initialize(img.p46_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Räknehändelser", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 40;
        this.text.setTransform(147, 38.6);

        this.text_0 = new cjs.Text("+", "bold 32px 'Epic Awesomeness'", "#ffffff");
        this.text_0.lineHeight = 40;
        this.text_0.setTransform(418, 38.6);

        this.text_1 = new cjs.Text("15", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(41, 21);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med addition 0 till 6", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(80.5, 654.7);

        this.text_4 = new cjs.Text("46", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(33, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 20).lineTo(610, 20).moveTo(20, 20).lineTo(20, 660).moveTo(20, 660).lineTo(610, 660);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(70, 653.7, 183, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FAAA33").ss(0.5, 0, 0, 4).arc(0, 0, 13.5, 0, 2 * Math.PI);
        this.shape_5.setTransform(429, 61);

        this.text_5 = new cjs.Text("Hitta på räknehändelser till bilden.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(34, 100);

        this.instance = new lib.p46_1();
        this.instance.setTransform(34, 103, 0.53, 0.53);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.shape_5, this.text_Rect, this.text_4, this.pageBottomText,
            this.text_1, this.text_5, this.instance, this.text, this.text_0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Hitta på räknehändelser till additionerna.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 5);

        this.instance = new lib.p46_2();
        this.instance.setTransform(45, 38, 0.098, 0.098);

        this.instance1 = new lib.p46_2();
        this.instance1.setTransform(45, (980 * 0.12) + 30, 0.098, 0.098);

        this.instance2 = new lib.p46_2();
        this.instance2.setTransform((1780 * 0.12) + 68, 38, 0.098, 0.098);

        this.instance3 = new lib.p46_2();
        this.instance3.setTransform((1780 * 0.12) + 68, (980 * 0.12) + 30, 0.098, 0.098);

        this.labelTemp1 = new cjs.Text("3", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp1.lineHeight = 34;
        this.labelTemp1.setTransform(81, 66);
        this.labelTemp2 = new cjs.Text("+", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp2.lineHeight = 34;
        this.labelTemp2.setTransform(102, 66);
        this.labelTemp3 = new cjs.Text("1", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp3.lineHeight = 34;
        this.labelTemp3.setTransform(121, 66);
        this.labelTemp4 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp4.lineHeight = 34;
        this.labelTemp4.setTransform(141, 66);
        this.labelTemp5 = new cjs.Text("4", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp5.lineHeight = 34;
        this.labelTemp5.setTransform(160, 66);

        this.labelTemp6 = new cjs.Text("2", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp6.lineHeight = 34;
        this.labelTemp6.setTransform(320, 66);
        this.labelTemp7 = new cjs.Text("+", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp7.lineHeight = 34;
        this.labelTemp7.setTransform(341, 66);
        this.labelTemp8 = new cjs.Text("3", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp8.lineHeight = 34;
        this.labelTemp8.setTransform(360, 66);
        this.labelTemp9 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp9.lineHeight = 34;
        this.labelTemp9.setTransform(380, 66);
        this.labelTemp10 = new cjs.Text("5", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp10.lineHeight = 34;
        this.labelTemp10.setTransform(399, 66);

        this.labelTemp11 = new cjs.Text("1", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp11.lineHeight = 34;
        this.labelTemp11.setTransform(81, 173);
        this.labelTemp12 = new cjs.Text("+", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp12.lineHeight = 34;
        this.labelTemp12.setTransform(102, 173);
        this.labelTemp13 = new cjs.Text("2", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp13.lineHeight = 34;
        this.labelTemp13.setTransform(121, 173);
        this.labelTemp14 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp14.lineHeight = 34;
        this.labelTemp14.setTransform(141, 173);
        this.labelTemp15 = new cjs.Text("3", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp15.lineHeight = 34;
        this.labelTemp15.setTransform(160, 173);

        this.labelTemp16 = new cjs.Text("0", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp16.lineHeight = 34;
        this.labelTemp16.setTransform(320, 173);
        this.labelTemp17 = new cjs.Text("+", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp17.lineHeight = 34;
        this.labelTemp17.setTransform(341, 173);
        this.labelTemp18 = new cjs.Text("2", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp18.lineHeight = 34;
        this.labelTemp18.setTransform(360, 173);
        this.labelTemp19 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp19.lineHeight = 34;
        this.labelTemp19.setTransform(380, 173);
        this.labelTemp20 = new cjs.Text("2", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp20.lineHeight = 34;
        this.labelTemp20.setTransform(399, 173);

        this.addChild(this.text_1, this.instance, this.instance1, this.instance2, this.instance3);
        this.addChild(this.labelTemp1, this.labelTemp2, this.labelTemp3, this.labelTemp4, this.labelTemp5, this.labelTemp6, this.labelTemp7, this.labelTemp8, this.labelTemp9, this.labelTemp10, this.labelTemp11, this.labelTemp12, this.labelTemp13, this.labelTemp14, this.labelTemp15, this.labelTemp16, this.labelTemp17, this.labelTemp18, this.labelTemp19, this.labelTemp20)
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 260);


    (lib.p46 = function() {
        this.initialize();
        this.v2 = new lib.Symbol2();
        this.v2.setTransform(296.8, 270, 1, 1, 0, 0, 0, 256.3, -100);
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
