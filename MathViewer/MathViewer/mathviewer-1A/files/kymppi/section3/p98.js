(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p98_1.png",
            id: "p98_1"
        }, {
            src: "images/p98_2.png",
            id: "p98_2"
        }]
    };

    // symbols:

    (lib.p98_2 = function() {
        this.initialize(img.p98_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p98_1 = function() {
        this.initialize(img.p98_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("98", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(33.4, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1

         var adjustX = 5;
        var adjustY = 7;

        this.text = new cjs.Text("Hitta vägen med rätt svar.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19-adjustX, 0-adjustY);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0-adjustX, 0-adjustY);

        this.instance = new lib.p98_1();
        this.instance.setTransform(22-adjustX, 25-adjustY, 0.312, 0.31);

        this.label1 = new cjs.Text("2 + 5", "16px 'Myriad Pro'");
        this.label1.lineHeight = 23;
        this.label1.setTransform(50-adjustX, 43-adjustY);
        this.label2 = new cjs.Text("5 + 3", "16px 'Myriad Pro'");
        this.label2.lineHeight = 23;
        this.label2.setTransform(129-adjustX, 43-adjustY);
        this.label3 = new cjs.Text("6 + 3", "16px 'Myriad Pro'");
        this.label3.lineHeight = 23;
        this.label3.setTransform(208-adjustX, 43-adjustY);
        this.label4 = new cjs.Text("2 + 6", "16px 'Myriad Pro'");
        this.label4.lineHeight = 23;
        this.label4.setTransform(288-adjustX, 43-adjustY);
        this.label5 = new cjs.Text("9 + 0", "16px 'Myriad Pro'");
        this.label5.lineHeight = 23;
        this.label5.setTransform(366-adjustX, 43-adjustY);

        this.label6 = new cjs.Text("8 + 0", "16px 'Myriad Pro'");
        this.label6.lineHeight = 23;
        this.label6.setTransform(50-adjustX, 87-adjustY);
        this.label7 = new cjs.Text("2 + 7", "16px 'Myriad Pro'");
        this.label7.lineHeight = 23;
        this.label7.setTransform(129-adjustX, 87-adjustY);
        this.label8 = new cjs.Text("4 + 3", "16px 'Myriad Pro'");
        this.label8.lineHeight = 23;
        this.label8.setTransform(208-adjustX, 87-adjustY);
        this.label9 = new cjs.Text("7 + 2", "16px 'Myriad Pro'");
        this.label9.lineHeight = 23;
        this.label9.setTransform(288-adjustX, 87-adjustY);
        this.label10 = new cjs.Text("3 + 5", "16px 'Myriad Pro'");
        this.label10.lineHeight = 23;
        this.label10.setTransform(366-adjustX, 87-adjustY);

        this.label11 = new cjs.Text("4 + 5", "16px 'Myriad Pro'");
        this.label11.lineHeight = 23;
        this.label11.setTransform(50-adjustX, 132-adjustY);
        this.label12 = new cjs.Text("1 + 7", "16px 'Myriad Pro'");
        this.label12.lineHeight = 23;
        this.label12.setTransform(129-adjustX, 132-adjustY);
        this.label13 = new cjs.Text("5 + 2", "16px 'Myriad Pro'");
        this.label13.lineHeight = 23;
        this.label13.setTransform(208-adjustX, 132-adjustY);
        this.label14 = new cjs.Text("7 + 1", "16px 'Myriad Pro'");
        this.label14.lineHeight = 23;
        this.label14.setTransform(288-adjustX, 132-adjustY);
        this.label15 = new cjs.Text("4 + 4", "16px 'Myriad Pro'");
        this.label15.lineHeight = 23;
        this.label15.setTransform(366-adjustX, 132-adjustY);

        this.label16 = new cjs.Text("1 + 6", "16px 'Myriad Pro'");
        this.label16.lineHeight = 23;
        this.label16.setTransform(50-adjustX, 177-adjustY);
        this.label17 = new cjs.Text("8 + 1", "16px 'Myriad Pro'");
        this.label17.lineHeight = 23;
        this.label17.setTransform(129-adjustX, 177-adjustY);
        this.label18 = new cjs.Text("3 + 4", "16px 'Myriad Pro'");
        this.label18.lineHeight = 23;
        this.label18.setTransform(208-adjustX, 177-adjustY);
        this.label19 = new cjs.Text("0 + 8", "16px 'Myriad Pro'");
        this.label19.lineHeight = 23;
        this.label19.setTransform(288-adjustX, 177-adjustY);
        this.label20 = new cjs.Text("6 + 2", "16px 'Myriad Pro'");
        this.label20.lineHeight = 23;
        this.label20.setTransform(366-adjustX, 177-adjustY);

        this.label21 = new cjs.Text("4 + 3", "16px 'Myriad Pro'");
        this.label21.lineHeight = 23;
        this.label21.setTransform(50-adjustX, 221-adjustY);
        this.label22 = new cjs.Text("3 + 5", "16px 'Myriad Pro'");
        this.label22.lineHeight = 23;
        this.label22.setTransform(129-adjustX, 221-adjustY);
        this.label23 = new cjs.Text("5 + 4", "16px 'Myriad Pro'");
        this.label23.lineHeight = 23;
        this.label23.setTransform(208-adjustX, 221-adjustY);
        this.label24 = new cjs.Text("1 + 6", "16px 'Myriad Pro'");
        this.label24.lineHeight = 23;
        this.label24.setTransform(288-adjustX, 221-adjustY);
        this.label25 = new cjs.Text("6 + 3", "16px 'Myriad Pro'");
        this.label25.lineHeight = 23;
        this.label25.setTransform(366-adjustX, 221-adjustY);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#ffffff').drawRoundRect(0, 0, 460, 285, 10);
        this.roundRect1.setTransform(0-adjustX, 20-adjustY);

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label6, this.label7, this.label8, this.label9, this.label10);
        this.addChild(this.label11, this.label12, this.label13, this.label14, this.label15);
        this.addChild(this.label16, this.label17, this.label18, this.label19, this.label20);
        this.addChild(this.label21, this.label22, this.label23, this.label24, this.label25);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 526, 300);

    (lib.Symbol3 = function() {
        this.initialize();

        var adjustX = 5;
        var adjustY = 7;


        this.instance = new lib.p98_2();
        this.instance.setTransform(25-adjustX, 42-adjustY, 0.4, 0.4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 5, 264, 207, 10);
        this.roundRect1.setTransform(0-adjustX, 20-adjustY);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(271-adjustX, 20-adjustY);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#FBBD99').drawRoundRect(23, 16, 214, 104, 10);
        this.roundRect3.setTransform(0-adjustX, 20-adjustY);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s('#FBBD99').drawRoundRect(23, 16, 209, 104, 10);
        this.roundRect4.setTransform(279-adjustX, 20-adjustY);

        this.text = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19-adjustX, 0-adjustY);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0-adjustX, 0-adjustY);

        this.label1 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(320-adjustX, 76-adjustY);

        this.label2 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(320-adjustX, 42.5-adjustY);

        this.label5 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(202-adjustX, 42.5-adjustY);

        this.label6 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(202-adjustX, 76-adjustY);

        this.label7 = new cjs.Text("8", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(320-adjustX, 109-adjustY);

        this.label8 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(480-adjustX, 109-adjustY);

        var arrEqual = ['43', '76', '110'];
        var ToBeAdded = [];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(455-adjustX, row-adjustY);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['43', '76', '110'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(179-adjustX, row-adjustY);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['43', '76', '110'];
        var arrxPos = ['69', '125', '344', '402'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "";
            if (i < 2) {
                text = "+";
            } else {
                text = "–";
            }
            for (var index = 0; index < arrEqual.length; index++) {
                var row = parseInt(arrEqual[index]);

                var tempLabel = new cjs.Text(text, "16px 'Myriad Pro'");
                tempLabel.lineHeight = -1;
                tempLabel.setTransform(xPos-adjustX, row-adjustY);
                ToBeAdded.push(tempLabel);
            }
        }

        arrEqual = [];
        arrEqual = ['166', '197'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(57-adjustX, row-adjustY);
            ToBeAdded.push(tempLabel);
        }


        arrEqual = [];
        arrEqual = ['167', '196'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(338-adjustX, row-adjustY);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['172', '446'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(row-adjustX, 167-adjustY);
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1 && column == 1) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(72 + (columnSpace * 115), 164 + (row * 32), 19, 22);
            }
        }
        this.textbox_group1.setTransform(0-adjustX, 0-adjustY);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1 && column == 1) {
                    continue;
                }
                this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(353 + (columnSpace * 110), 164 + (row * 32), 19, 22);
            }
        }
        this.textbox_group2.setTransform(0-adjustX, 0-adjustY);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.text, this.text_1, this.label1, this.label2);
        this.addChild(this.label5, this.label6, this.textbox_group1, this.textbox_group2, this.label7, this.label8);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 526, 221);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297.3, 85, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(297.3, 412, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
