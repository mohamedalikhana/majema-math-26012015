(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p29_1.png",
            id: "p29_1"
        }, {
            src: "images/p29_2.png",
            id: "p29_2"
        }, {
            src: "images/p29_3.png",
            id: "p29_3"
        }]
    };

    (lib.p29_1 = function() {
        this.initialize(img.p29_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p29_2 = function() {
        this.initialize(img.p29_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p29_3 = function() {
        this.initialize(img.p29_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("29", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(555, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00B0CA").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#00B0CA");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(118, 26);

        this.text_4 = new cjs.Text("9", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(75, 22);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00B0CA").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52, 23.5, 1.2, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 491, 101, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 10, 40, 82, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p29_1();
        this.instance.setTransform(495.5, 14, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Hur många?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#00B0CA");
        this.text_q2.lineHeight = 26;
        this.text_q2.setTransform(4, 6);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.8).drawRect(40 + (columnSpace * 99), 67.5, 20, 23);
        }
        this.textbox_group1.setTransform(0, 0);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 0) {
                    continue;
                } // blue ball
                this.shape_group1.graphics.f("#0095DA").s("#000000").ss(0.8, 0, 0, 4).arc(40 + (columnSpace * 19.5), 32 + (row * 18), 8.4, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 1 && row == 0) {
                    continue;
                } else if (column == 0) {
                    if (row == 0) {
                        columnSpace = 0.5;
                    } else {
                        columnSpace = column;
                    }
                } // red ball
                this.shape_group2.graphics.f("#DA2129").s("#000000").ss(0.8, 0, 0, 4).arc(140 + (columnSpace * 19.5), 32 + (row * 18), 8.4, 0, 2 * Math.PI);
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // yellow ball
                this.shape_group3.graphics.f("#FFF679").s("#000000").ss(0.8, 0, 0, 4).arc(338 + (columnSpace * 19.5), 32 + (row * 20), 8.4, 0, 2 * Math.PI);
            }
        }
        this.shape_group3.setTransform(0, 0);

        this.shape_group4 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 0) {
                    continue;
                } else if (row == 1 && column == 1) {
                    continue;
                } // green ball
                this.shape_group4.graphics.f("#20B14A").s("#000000").ss(0.8, 0, 0, 4).arc(446 + (columnSpace * 19.5), 32 + (row * 18), 8.4, 0, 2 * Math.PI);
            }
        }
        this.shape_group4.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.shape_group1);
        this.addChild(this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 491, 138, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 29, 40, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p29_1();
        this.instance.setTransform(495.5, 34, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Rita 1 cirkel mer än antalet kottar.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 9);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#00B0CA");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 9);

        var arrTxtbox = ['79', '102'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 105,
            rectHeight = 23,
            boxWidth = 21,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 3;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 26;
                if (i == 1) {
                    padding = 39.5;
                } else if (i == 2) {
                    padding = 47;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(157.5, 34).lineTo(157.5, 134).moveTo(320, 34).lineTo(320, 134);
        this.Line_1.setTransform(0, 0);

        this.instance_2 = new lib.p29_2();
        this.instance_2.setTransform(22, 27, 0.42, 0.42);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.Line_1, this.instance_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 491, 138, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 29, 40, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p29_1();
        this.instance.setTransform(495.5, 34, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Rita 1 cirkel mindre än antalet löv.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 9);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#00B0CA");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 9);

        var arrTxtbox = ['79', '102'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 105,
            rectHeight = 23,
            boxWidth = 21,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 3;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 26;
                if (i == 1) {
                    padding = 39.5;
                } else if (i == 2) {
                    padding = 47;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(157.5, 34).lineTo(157.5, 134).moveTo(320, 34).lineTo(320, 134);
        this.Line_1.setTransform(0, 0);

        this.instance_2 = new lib.p29_3();
        this.instance_2.setTransform(28, 27, 0.42, 0.42);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.Line_1, this.instance_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol4 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 491, 140, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 25, 40, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p29_1();
        this.instance.setTransform(495.5, 30, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Jämför. Skriv >, < eller =.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 10);

        this.text_q2 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#00B0CA");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 10);

        var ToBeAdded = [];
        var arrxPos = ['29', '192', '352'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            this.textbox_group1 = new cjs.Shape();
            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.8).drawRect(xPos + (columnSpace * 39), 106, 20, 23);
            }
            this.textbox_group1.setTransform(0, 0);
            ToBeAdded.push(this.textbox_group1);
        }

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 1) {
                    continue;
                } // blue ball
                this.shape_group1.graphics.f("#0095DA").s("#000000").ss(0.8, 0, 0, 4).arc(38 + (columnSpace * 19.5), 66 + (row * 20), 8.4, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) { // blue ball
                this.shape_group2.graphics.f("#0095DA").s("#000000").ss(0.8, 0, 0, 4).arc(192 + (columnSpace * 22), 66 + (row * 20.3), 8.4, 0, 2 * Math.PI);
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) { // blue ball
                this.shape_group3.graphics.f("#0095DA").s("#000000").ss(0.8, 0, 0, 4).arc(362 + (columnSpace * 22), 45 + (row * 20.3), 8.4, 0, 2 * Math.PI);
            }
        }
        this.shape_group3.setTransform(0, 0);

        arrxPos = [];
        arrxPos = ['103','268','429'];

        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            this.temp_shape = new cjs.Shape();
            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                for (var row = 0; row < 2; row++) {
                    if (column == 1 && row == 0) {
                        continue;
                    } else if (column == 0) {
                        if (row == 0) {
                            columnSpace = 0.5;
                        } else {
                            columnSpace = column;
                        }
                    } // red ball
                    this.temp_shape.graphics.f("#DA2129").s("#000000").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * 25), 66 + (row * 20.5), 8.4, 0, 2 * Math.PI);
                }
            }
            this.temp_shape.setTransform(0, 0);
            ToBeAdded.push(this.temp_shape);
        }

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(157.5, 43).lineTo(157.5, 136).moveTo(320, 43).lineTo(320, 136);
        this.Line_1.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1);
        this.addChild(this.shape_group1, this.shape_group2, this.shape_group3, this.Line_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(315, 136, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(315, 250, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(315, 400, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v4 = new lib.Symbol4();
        this.v4.setTransform(315, 551, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
