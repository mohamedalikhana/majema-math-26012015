(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p95_1.png",
            id: "p95_1"
        }]
    };

    (lib.p95_1 = function() {
        this.initialize(img.p95_1);     
    }).prototype = p = new cjs.Bitmap()
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("95", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);
   
    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 2);

        this.text_1 = new cjs.Text(" Mät och räkna ut hur långt myran ska gå.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#9D9D9C').drawRoundRect(0, 0, 510, 548, 10);
        this.roundRect1.setTransform(10, 17);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_1.setTransform(14, 71);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_2.setTransform(14, 161);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_3.setTransform(14, 248);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(501, 40);
        this.hrLine_4.setTransform(14, 353);

        this.hrLine_5 = new cjs.Shape();
        this.hrLine_5.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(66, 102).lineTo(64, 97).lineTo(65, 100).lineTo(245, 35).lineTo(343, 87).lineTo(340, 89).lineTo(345, 85)
        this.hrLine_5.setTransform(0, 0);

        this.hrLine_6 = new cjs.Shape();
        this.hrLine_6.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(66, 129).lineTo(69, 124).lineTo(68, 127).lineTo(277, 197).lineTo(277, 118).lineTo(273, 118).lineTo(281, 118);
        this.hrLine_6.setTransform(0, 0);

        this.hrLine_7 = new cjs.Shape();
        this.hrLine_7.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(70, 268).lineTo(76, 268).lineTo(73, 268).lineTo(98, 218).lineTo(337, 265).lineTo(338, 262).lineTo(335, 268);
        this.hrLine_7.setTransform(0, 0);

        this.hrLine_8 = new cjs.Shape();
        this.hrLine_8.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(74, 358).lineTo(80, 362).lineTo(77, 360).lineTo(165, 299).lineTo(300, 319).lineTo(200, 359).lineTo(199, 356).lineTo(201, 362);
        this.hrLine_7.setTransform(0, 0);
        this.hrLine_8.setTransform(0, 0);

        this.hrLine_9 = new cjs.Shape();
        this.hrLine_9.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(65, 513).lineTo(71, 517).lineTo(68, 515).lineTo(130, 457).lineTo(220, 515).lineTo(230, 464)
            .lineTo(357, 407).lineTo(436, 433).lineTo(395, 495).lineTo(392, 493).lineTo(398, 497);
        this.hrLine_7.setTransform(0, 0);
        this.hrLine_9.setTransform(0, 0);

        var hrlineArr = [];
        var hrlineX = [94, 300, 410, 105, 283, 411, 85, 211, 411, 61, 181, 411, 231, 91, 141, 231, 236, 336, 426, 411];
        var hrlineY = [48, 38, 53, 157, 145, 146, 233, 215, 240, 307, 307, 327, 342, 492, 437, 477, 412, 422, 457, 518];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(26, 20);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        var TextArr = [];
        var TextX = [123, 329, 439, 134, 312, 440, 114, 240, 440, 90, 210, 440, 260, 120, 170, 260, 265, 365, 455, 440];
        var TextY = [66, 56, 71, 175, 163, 164, 251, 233, 258, 325, 325, 345, 360, 510, 455, 495, 430, 440, 475, 536];
        5
        for (var row = 0; row < TextX.length; row++) {

            var text_4 = new cjs.Text("cm", "16px 'Myriad Pro'", "#000000");
            text_4.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_4);
        }

        var TxArr = [];
        var TxX = [372, 372, 372, 372, 372];
        var TxY = [70, 165, 258, 343, 536];
        5
        for (var row = 0; row < TxX.length; row++) {

            var text_4 = new cjs.Text("hela:", "16px 'Myriad Pro'", "#000000");
            text_4.setTransform(TxX[row], TxY[row]);
            TxArr.push(text_4);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4,
            this.hrLine_5, this.hrLine_6, this.hrLine_7, this.hrLine_8, this.hrLine_9);
        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }
        for (var i = 0; i < TxArr.length; i++) {
            this.addChild(TxArr[i]);
        }

        this.antimage = new lib.p95_1();      
        this.antimage.setTransform(21, 73,0.47,0.47);

        this.Symbolimg1 =this.antimage.clone(true); 
        this.Symbolimg1.setTransform(21, 117,0.47,0.47);

        this.Symbolimg2 = this.antimage.clone(true);
        this.Symbolimg2.setTransform(21, 248,0.47,0.47);

        this.Symbolimg3 = this.antimage.clone(true);
        this.Symbolimg3.setTransform(21, 340,0.47,0.47);

        this.Symbolimg4 = this.antimage.clone(true);
        this.Symbolimg4.setTransform(21, 470,0.47,0.47);     

        this.addChild(this.antimage,this.Symbolimg1, this.Symbolimg2, this.Symbolimg3, this.Symbolimg4)


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 580.3, 560);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(304, 110, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
