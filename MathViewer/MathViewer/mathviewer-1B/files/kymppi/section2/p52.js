(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };

    // symbols:

    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("52", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(38, 660);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(453 + (columnSpace * 33), 14, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1, 20, 520, 337, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text.setTransform(22, 40);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(4, 40);

        this.text_2 = new cjs.Text("Kluring", "18px 'MyriadPro-Semibold'", "#F1662B");
        this.text_2.setTransform(0, 11);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('#ffffff').s("#EB5B1B").ss(0.8).drawRect(117, 50, 130, 295);
        this.rect1.setTransform(0, 0);

        this.rect2 = new cjs.Shape();
        this.rect2.graphics.f('#ffffff').s("#EB5B1B").ss(0.8).drawRect(278, 50, 130, 295);
        this.rect2.setTransform(10, 0);

        var ToBeAdded = [];
        for (var col = 0; col < 7; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");

                temp_text.setTransform(125 + (colSpace * 18.6), 64 + (rowSpace * 20.9));
                ToBeAdded.push(temp_text);
            }
        }

        for (var col = 0; col < 7; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");

                temp_text.setTransform(295 + (colSpace * 18.6), 64 + (rowSpace * 20.9));
                ToBeAdded.push(temp_text);
            }
        }

        this.centerLine = new cjs.Shape();
        this.centerLine.graphics.f('#ffffff').s("#EB5B1B").ss(0.7).moveTo(117, 198).lineTo(247, 198)
        this.centerLine.setTransform(0, 0);

        this.centerLine1 = new cjs.Shape();
        this.centerLine1.graphics.f('#ffffff').s("#EB5B1B").ss(0.7).moveTo(278, 198).lineTo(409, 198)
        this.centerLine1.setTransform(10, 0);

        this.redLine_1 = new cjs.Shape(); // box 1 red  shape1
        this.redLine_1.graphics.f('#DA2129').s("#949599").ss(1.2).moveTo(120, 124.5).lineTo(138, 60.5).lineTo(159, 124.5).lineTo(120, 124.5)
            .moveTo(157.5, 124.5).lineTo(175.5, 60.5).lineTo(196.5, 124.5).lineTo(157.5, 124.5)
            .moveTo(195, 124.5).lineTo(213, 60.5).lineTo(232, 124.5).lineTo(195, 124.5)
        this.redLine_1.setTransform(7, 0);

        this.blueLine_1 = new cjs.Shape(); // box 1 blue  shape1
        this.blueLine_1.graphics.f('#008BD2').s("#949599").ss(1.2).moveTo(138.5, 124.5).lineTo(120, 124.5).lineTo(120, 145).lineTo(138.5, 124.5)
            .lineTo(157, 145).lineTo(157, 124.5)
            .moveTo(176, 124.5).lineTo(157, 124.5).lineTo(157, 145).lineTo(176, 124.5)
            .lineTo(194.5, 145).lineTo(194.5, 124.5)
            .moveTo(213, 124.5).lineTo(195, 124.5).lineTo(195, 145).lineTo(213, 124.5)
            .lineTo(231.2, 145).lineTo(231.2, 124.5)
        this.blueLine_1.setTransform(7, 0);

        this.blueLine_2 = new cjs.Shape(); // box 1 blue  shape2
        this.blueLine_2.graphics.f('#008BD2').s("#949599").ss(1.2).moveTo(138, 166).lineTo(157, 145).lineTo(157, 188).lineTo(120, 146).lineTo(120, 188).lineTo(138, 166)
            .moveTo(176, 166).lineTo(195, 145).lineTo(195, 188).lineTo(158, 146).lineTo(158, 188).lineTo(176, 166)
            .moveTo(213, 166).lineTo(232, 145).lineTo(232, 188).lineTo(195, 146).lineTo(195, 188).lineTo(213, 166)
        this.blueLine_2.setTransform(7, 0);

        this.redLine_2 = new cjs.Shape(); // box 1 red  shape2
        this.redLine_2.graphics.f('#DA2129').s("#949599").ss(1.2).moveTo(120, 188).lineTo(158, 188).lineTo(138, 166)
            .moveTo(158, 188).lineTo(196, 188).lineTo(176, 166)
            .moveTo(195, 188).lineTo(233, 188).lineTo(213, 166)
        this.redLine_2.setTransform(7, 0);

        this.yellowLine_1 = new cjs.Shape(); // box 1 yellow  shape1 
        this.yellowLine_1.graphics.f('#FFF374').s("#949599").ss(1.2).moveTo(138, 166).lineTo(157, 145).lineTo(138, 124.5).lineTo(120, 145)
            .moveTo(176, 166).lineTo(195, 145).lineTo(176, 124.5).lineTo(157, 145)
            .moveTo(213, 166).lineTo(232, 145).lineTo(213, 124.5).lineTo(196, 145)
        this.yellowLine_1.setTransform(7, 0);

        this.line1 = new cjs.Shape(); // box 2 Line  
        this.line1.graphics.f('#ffffff').s("#949599").ss(1.2).moveTo(323, 60).lineTo(343, 83).lineTo(363, 60)
        this.line1.setTransform(10, 0);

        this.line2 = new cjs.Shape(); // box 2 gray shape1  
        this.line2.graphics.f('#D3A775').s("#949599").ss(1.2).moveTo(343, 83).lineTo(324, 103).lineTo(361, 103).lineTo(343, 83)
        this.line2.setTransform(10, 0);

        this.redLine3 = new cjs.Shape(); // box 2 red center shape1  
        this.redLine3.graphics.f('#D51317').s("#949599").ss(1.2).moveTo(324, 103).lineTo(324, 145).lineTo(343, 187).lineTo(362, 145).lineTo(361.7, 102)
        this.redLine3.setTransform(10, 0);
        // box 2 left side
        this.blueLine3 = new cjs.Shape(); // box 2 blue shape1  
        this.blueLine3.graphics.f('#008BD2').s("#949599").ss(1.2).moveTo(324, 103).lineTo(285, 60).lineTo(285, 82).lineTo(324, 126)
            .moveTo(324, 126).lineTo(285, 167).lineTo(286, 189).lineTo(324, 147)
        this.blueLine3.setTransform(10, 0);

        this.greenLine1 = new cjs.Shape(); // box 2 Green shape1  
        this.greenLine1.graphics.f('#13A538').s("#949599").ss(1.2).moveTo(285, 102).lineTo(306, 126).lineTo(285, 149).lineTo(285, 102)
        this.greenLine1.setTransform(10, 0);

        this.yellowLine2 = new cjs.Shape(); // box 2 yellow shape1  
        this.yellowLine2.graphics.f('#FFF374').s("#949599").ss(1.2).moveTo(285, 83).lineTo(324, 126).lineTo(285, 166).lineTo(285, 147).lineTo(306, 126).lineTo(285, 103).lineTo(285, 83)
        this.yellowLine2.setTransform(10, 0);

        // box 2 right side
        this.blueLine4 = new cjs.Shape(); // box 2 blue shape1  
        this.blueLine4.graphics.f('#008BD2').s("#949599").ss(1.2).moveTo(362, 103).lineTo(400, 60).lineTo(400, 82).lineTo(362, 126)
            .moveTo(362, 126).lineTo(400, 167).lineTo(401, 189).lineTo(362, 147)
        this.blueLine4.setTransform(10, 0);

        this.greenLine2 = new cjs.Shape(); // box 2 Green shape1  
        this.greenLine2.graphics.f('#13A538').s("#949599").ss(1.2).moveTo(400, 102).lineTo(380, 126).lineTo(400, 149).lineTo(400, 102)
        this.greenLine2.setTransform(10, 0);

        this.yellowLine3 = new cjs.Shape(); // box 2 yellow shape1  
        this.yellowLine3.graphics.f('#FFF374').s("#949599").ss(1.2).moveTo(400, 83).lineTo(362, 126).lineTo(400, 166).lineTo(400, 147).lineTo(380, 126).lineTo(400, 103).lineTo(400, 83)
        this.yellowLine3.setTransform(10, 0);
        362


        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.rect1, this.rect2, this.centerLine, this.centerLine1);
        this.addChild(this.redLine_1, this.blueLine_1, this.blueLine_2, this.redLine_2, this.yellowLine_1, this.line1, this.line2, this.redLine3, this.blueLine3, this.greenLine1, this.yellowLine2)
        this.addChild(this.blueLine4, this.greenLine2, this.yellowLine3)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 510.3, 340.9);

    (lib.Symbol3 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Vilken är figuren?", "16px 'Myriad Pro'");
        this.text.setTransform(25, 22);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(6, 22);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1.5, 3, 520, 235, 10);
        this.roundRect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(5, 133).lineTo(517, 133);
        this.hrLine_1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [51, 185, 310, 51, 185, 310];
        var lineY = [18, 18, 18, 121, 121, 121];
        var linewidth = 108;

        for (var row = 0; row < lineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            if (row > 2) {
                linewidth = 111
            } else {
                linewidth = 108
            }
            vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(80, 20).lineTo(80, linewidth);
            vrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(vrLine_1);
        }

        // var TextArr = [];
        var TextX = [20, 150, 285, 395, 20, 150, 285, 395];
        var TextY = [50, 50, 50, 50, 153, 153, 153, 153];
        var tmptext = "Den finns här.";

        for (var row = 0; row < TextX.length; row++) {
            if (row == 3) {
                tmptext = "Den finns inte här."
            } else if (row == 7) {
                tmptext = "Den finns inte här."
            } else {
                tmptext = "Den finns här."
            }
            var text_1 = new cjs.Text(tmptext, "15.5px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            lineArr.push(text_1);
        }
        //bulue line Triangle
        var TriX = [15, 144, 279, 15, 144, 279];
        var TriY = [20, 20, 20, 125, 125, 125];
        for (var row = 0; row < TriX.length; row++) {
            var triangle_1 = new cjs.Shape();
            triangle_1.graphics.f('').s("#008BD2").ss(1.2).moveTo(3, 100).lineTo(100, 100).lineTo(53, 37).lineTo(3, 100);
            triangle_1.setTransform(TriX[row], TriY[row]);
            lineArr.push(triangle_1);
        }

        this.triangle_2 = new cjs.Shape();
        this.triangle_2.graphics.f('').s("#D51317").ss(1.2).moveTo(5, 37).lineTo(100, 37).lineTo(53, 100).lineTo(5, 37);
        this.triangle_2.setTransform(394, 23);

        this.triangle_3 = new cjs.Shape();110
        this.triangle_3.graphics.f('').s("#D51317").ss(1.2).moveTo(5, 37).lineTo(100, 37).lineTo(53, 100).lineTo(5, 37);
        this.triangle_3.setTransform(394, 125);

        this.text_2 = new cjs.Text("Figuren", "12px 'Myriad Pro'");
        this.text_2.setTransform(473, 99);

        this.text_3 = new cjs.Text("är:", "12px 'Myriad Pro'");
        this.text_3.setTransform(485, 111);

        this.text_4 = new cjs.Text("Figuren", "12px 'Myriad Pro'");
        this.text_4.setTransform(473, 201);

        this.text_5 = new cjs.Text("är:", "12px 'Myriad Pro'");
        this.text_5.setTransform(485, 213);
        var redcolor="#EB5B1B";
        var bluecolor="#00A3C4";
        var greencolor="#90BD22";
        var yellowcolor="#FFF374";

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group1.graphics.f(bluecolor).s("#000000").ss(0.8, 0, 0, 4).arc(44 + (columnSpace * 20.5), 34+ (row * 19), 7.08, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {                  
                    this.shape_group1.graphics.f(redcolor).s("#000000").ss(1.2).moveTo(55, 43).lineTo(74, 43).lineTo(65, 28).lineTo(56, 43);
                } else if (column == 0 && row == 1) {
                   this.shape_group1.graphics.f(yellowcolor).s("#000000").ss(1.2).moveTo(32, 60).lineTo(50, 60).lineTo(42,46).lineTo(32, 60);                  
                } else {

                    this.shape_group1.graphics.f(greencolor).s("#000000").ss(0.7).drawRoundRect(57, 48, 13, 13, 0);
                }
            }
        } this.shape_group1.setTransform(12, 55);
       

          this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group2.graphics.f(bluecolor).s("#000000").ss(0.8, 0, 0, 4).arc(43 + (columnSpace * 20.5), 38 + (row * 19), 7.13, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group2.graphics.f(greencolor).s("#000000").ss(0.7).drawRoundRect(60, 32, 12, 12, 0);

                } else if (column == 0 && row == 1) {
                    this.shape_group2.graphics.f(yellowcolor).s("#000000").ss(1.2).moveTo(32, 62).lineTo(51, 62).lineTo(42, 50).lineTo(33, 62);
                } else {
                     this.shape_group2.graphics.f(redcolor).s("#000000").ss(1.2).moveTo(58, 62).lineTo(77, 62).lineTo(68, 50).lineTo(59, 62);
                }
            }
        }this.shape_group2.setTransform(142, 53);
        

         this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {                    
                    this.shape_group3.graphics.f(yellowcolor).s("#000000").ss(1.2).moveTo(32, 42).lineTo(51, 42).lineTo(42, 30).lineTo(33, 42);
                } else if (column == 1 && row == 0) {                    
                      this.shape_group3.graphics.f(redcolor).s("#000000").ss(0.8, 0, 0, 4).arc(44 + (columnSpace * 20.5), 38 + (row * 19), 7.13, 0, 2 * Math.PI);
                } else if (column == 0 && row == 1) {
                  this.shape_group3.graphics.f(greencolor).s("#000000").ss(0.7).drawRoundRect(59, 49, 12, 12, 0);                  
                } else {              
                     this.shape_group3.graphics.f(bluecolor).s("#000000").ss(1.2).moveTo(32, 62).lineTo(51, 62).lineTo(42, 50).lineTo(33, 62);
                }
            }
        } this.shape_group3.setTransform(278, 53);
       
// red triangle Top
         this.shape_group4 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group4.graphics.f(yellowcolor).s("#000000").ss(1.2).moveTo(32, 42).lineTo(51, 42).lineTo(42,30).lineTo(33,42)                  
                } else if (column == 1 && row == 0) {
                   this.shape_group4.graphics.f(bluecolor).s("#000000").ss(0.8, 0, 0, 4).arc(23 + (columnSpace * 20.5), 54+ (row * 19), 7.13, 0, 2 * Math.PI);
                } else if (column == 0 && row == 1) {
                    this.shape_group4.graphics.f(redcolor).s("#000000").ss(1.2).moveTo(55, 42).lineTo(74, 42).lineTo(65, 30).lineTo(56, 42);
                } else {
                    this.shape_group4.graphics.f(yellowcolor).s("#000000").ss(0.7).drawRoundRect(59, 47, 12, 12, 0);
                }
            }
        } this.shape_group4.setTransform(392, 36);
       


        this.shape_group5 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                 if (column == 0 && row == 0) {
                    this.shape_group5.graphics.f(yellowcolor).s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 38 + (row * 19), 7.13, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group5.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(60, 32, 12, 12, 0);  
                } else if (column == 0 && row == 1) {                   
                    this.shape_group5.graphics.f(bluecolor).s("#000000").ss(0.7).drawRoundRect(40, 49, 12, 12, 0);
                } else {
                     this.shape_group5.graphics.f(redcolor).s("#000000").ss(0.8, 0, 0, 4).arc(46 + (columnSpace * 20.5), 36 + (row * 19), 7.13, 0, 2 * Math.PI);
                }
            }
        }this.shape_group5.setTransform(12, 159);
        

         this.shape_group6 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                     this.shape_group6.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(40, 31, 12, 12, 0);
                } else if (column == 1 && row == 0) {                    
                    this.shape_group6.graphics.f(bluecolor).s("#000000").ss(0.7).drawRoundRect(60, 31, 12, 12, 0);
                } else if (column == 0 && row == 1) {
                   this.shape_group6.graphics.f(yellowcolor).s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 36 + (row * 19), 7.13, 0, 2 * Math.PI);
                } else {
                    this.shape_group6.graphics.f(redcolor).s("#000000").ss(0.8, 0, 0, 4).arc(46 + (columnSpace * 20.5), 36 + (row * 19), 7.13, 0, 2 * Math.PI);
                }


            }

        }this.shape_group6.setTransform(142, 159);
        

         this.shape_group7 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                     this.shape_group7.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(40, 31, 12, 12, 0);
                } else if (column == 1 && row == 0) {                    
                     this.shape_group7.graphics.f(bluecolor).s("#000000").ss(0.8, 0, 0, 4).arc(46 + (columnSpace * 20.5), 36 + (row * 19), 7.13, 0, 2 * Math.PI);
                } else if (column == 0 && row == 1) {
                   this.shape_group7.graphics.f(yellowcolor).s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 36 + (row * 19), 7.13, 0, 2 * Math.PI);
                } else {                   
                    this.shape_group7.graphics.f(bluecolor).s("#000000").ss(0.7).drawRoundRect(60, 49, 12, 12, 0);
                }

            }

        }this.shape_group7.setTransform(275, 159);
        
// red triangle Bottom
         this.shape_group8 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

               if (column == 0 && row == 0) {
                     this.shape_group8.graphics.f('#958FC5').s("#000000").ss(0.7).drawRoundRect(40, 31, 12, 12, 0);
                } else if (column == 1 && row == 0) {                    
                    this.shape_group8.graphics.f(redcolor).s("#000000").ss(0.7).drawRoundRect(60, 31, 12, 12, 0);
                } else if (column == 0 && row == 1) {
                   this.shape_group8.graphics.f(redcolor).s("#000000").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 36 + (row * 19), 7.13, 0, 2 * Math.PI);
                } else {
                    this.shape_group8.graphics.f(yellowcolor).s("#000000").ss(0.8, 0, 0, 4).arc(46 + (columnSpace * 20.5), 36 + (row * 19), 7.13, 0, 2 * Math.PI);
                }
            }
        }this.shape_group8.setTransform(391, 137);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(470, 123).lineTo(510, 123);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(470, 227).lineTo(510, 227);
        this.hrLine_3.setTransform(0, 0);




        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2,this.text_3,this.text_4,this.text_5,this.shape_group7,this.shape_group8,this.hrLine_2,this.hrLine_3);
        this.addChild(this.hrLine_1,this.triangle_2,this.triangle_3,this.shape_group1,this.shape_group2, this.shape_group3,this.shape_group4,this.shape_group5,this.shape_group6);   
        for (var textEl = 0; textEl < lineArr.length; textEl++) {
            this.addChild(lineArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    0, 0, 512.3, 320.9
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 240);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297.3, 90, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(297.3, 438, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
