(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p24_1.png",
            id: "p24_1"
        }]
    };

    (lib.p24_1 = function() {
        this.initialize(img.p24_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1

        this.text = new cjs.Text("24", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(39, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 29, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 15);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Hur mycket kostar de tillsammans?", "16px 'Myriad Pro'");
        this.text.setTransform(19, -2);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, -2);

        this.instance = new lib.p24_1();
        this.instance.setTransform(0, 41.5, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(6, 11, 257, 170, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(6, 193, 257, 170, 10);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(266, 11, 257, 170, 10);
        this.roundRect3.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(266, 193, 257, 170, 10);
        this.roundRect4.setTransform(0, 0);


        this.text_2 = new cjs.Text("Lisa köper sockor och vantar.", "17px 'Myriad Pro'");
        this.text_2.setTransform(15, 32);

        this.text_3 = new cjs.Text("Henrik köper en mössa", "16px 'Myriad Pro'");
        this.text_3.setTransform(275, 32);

        this.text_4 = new cjs.Text("och en halsduk.", "16px 'Myriad Pro'");
        this.text_4.setTransform(275, 50);

        this.text_5 = new cjs.Text("Anna köper en tröja", "16px 'Myriad Pro'");
        this.text_5.setTransform(15, 215);

        this.text_6 = new cjs.Text("och en keps.", "16px 'Myriad Pro'");
        this.text_6.setTransform(15, 233);

        this.text_7 = new cjs.Text("Victor köper en mössa och vantar.", "16px 'Myriad Pro'");
        this.text_7.setTransform(275, 215);


        this.text_8 = new cjs.Text("3 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_8.setTransform(29, 76);
        this.text_8.skewX = 20;
        this.text_8.skewY = 20;

        this.text_9 = new cjs.Text("6 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_9.setTransform(24, 258);
        this.text_9.skewX = 20;
        this.text_9.skewY = 20;

        this.text_10 = new cjs.Text("7 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_10.setTransform(215, 83);
        this.text_10.skewX = -10;
        this.text_10.skewY = -10;

        this.text_11 = new cjs.Text("12 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_11.setTransform(212, 259);
        this.text_11.skewX = 10;
        this.text_11.skewY = 10;

        this.text_12 = new cjs.Text("10 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_12.setTransform(280, 83);
        this.text_12.skewX = -10;
        this.text_12.skewY = -10;

        this.text_13 = new cjs.Text("15 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_13.setTransform(294, 252);
        this.text_13.skewX = 20;
        this.text_13.skewY = 20;

        this.text_14 = new cjs.Text("5 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_14.setTransform(482, 124);
        this.text_14.skewX = 20;
        this.text_14.skewY = 20;

        this.text_15 = new cjs.Text("5 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_15.setTransform(479, 257);
        this.text_15.skewX = -20;
        this.text_15.skewY = -20;

        var RectArr = [];
        var RectX = [25, 285, 25, 285];
        var RectY = [32, 32, 215, 215];

        for (var row = 0; row < RectX.length; row++) {

            var Rect = new cjs.Shape();
            Rect.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(6, 110, 200, 23, 0);
            Rect.setTransform(RectX[row], RectY[row]);
            RectArr.push(Rect);

        }

        var lineArr = [];
        var lineX = [11, 31, 51, 71, 91, 111, 131, 151, 171, 11, 31, 51, 71, 91, 111, 131, 151, 171,
        271,291,311,331,351,371,391,411,431, 271,291,311,331,351,371,391,411,431];
        var lineY = [142, 142, 142, 142, 142, 142, 142, 142, 142, 325, 325, 325, 325, 325, 325, 325, 325, 325,
        142, 142, 142, 142, 142, 142, 142, 142, 142, 325, 325, 325, 325, 325, 325, 325, 325, 325];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 23);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.roundRect1,
            this.roundRect2, this.roundRect3, this.roundRect4, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7,
            this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14, this.text_15);

        for (var i = 0; i < RectArr.length; i++) {
            this.addChild(RectArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 350);


    (lib.Symbol3 = function() {

        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, -2);
        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, -2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 12, 518, 143, 10);
        this.roundRect1.setTransform(0, 0);


        var lineArr = [];
        var lineX = [88, 255, 427, 88, 255, 427, 88, 255, 427, 88, 255, 427];
        var lineY = [30, 30, 30, 57, 57, 57, 82, 82, 82, 106, 106, 106];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(43, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("12  +  6  =", "16px 'Myriad Pro'");
        this.label1.setTransform(31, 65);
        this.label2 = new cjs.Text("12  +  8  =", "16px 'Myriad Pro'");
        this.label2.setTransform(31, 90);
        this.label3 = new cjs.Text("13  +  5  =", "16px 'Myriad Pro'");
        this.label3.setTransform(31, 115);
        this.label4 = new cjs.Text("13  +  7  =", "16px 'Myriad Pro'");
        this.label4.setTransform(31, 140);

        this.label6 = new cjs.Text("20  –  5  =", "16px 'Myriad Pro'");
        this.label6.setTransform(197, 65);
        this.label7 = new cjs.Text("20  –  7  =", "16px 'Myriad Pro'");
        this.label7.setTransform(197, 90);
        this.label8 = new cjs.Text("19  –  6  =", "16px 'Myriad Pro'");
        this.label8.setTransform(197, 115);
        this.label9 = new cjs.Text("18  –  7  =", "16px 'Myriad Pro'");
        this.label9.setTransform(197, 140);

        this.label11 = new cjs.Text("16  +  3  =", "16px 'Myriad Pro'");
        this.label11.setTransform(367, 65);
        this.label12 = new cjs.Text("16  –  3  =", "16px 'Myriad Pro'");
        this.label12.setTransform(367, 90);
        this.label13 = new cjs.Text("18  +  2  =", "16px 'Myriad Pro'");
        this.label13.setTransform(367, 115);
        this.label14 = new cjs.Text("18  –  2  =", "16px 'Myriad Pro'");
        this.label14.setTransform(367, 140);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(5, 20).lineTo(465, 20);
        this.Line_1.setTransform(23, 13);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(124, 36);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_3.setTransform(233, 36);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(345, 36);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_5.setTransform(458, 36);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 10) {
                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(55 + (columnSpace * 20.5), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 15) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(65 + (columnSpace * 20.5), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else {

                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(75 + (columnSpace * 20.5), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 124, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 535, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
