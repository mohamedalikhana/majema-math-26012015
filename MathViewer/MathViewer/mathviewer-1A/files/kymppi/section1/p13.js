(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/s12_13_1.png",
            id: "s12_13_1"
        }, {
            src: "images/s12_13_2.png",
            id: "s12_13_2"
        }, {
            src: "images/s12_13_3.png",
            id: "s12_13_3"
        }, {
            src: "images/s12_13_4.png",
            id: "s12_13_4"
        }]
    };

    // symbols:
    //img1
    (lib.s12_13_1 = function() {
        this.initialize(img.s12_13_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 589, 67);

    //img2
    (lib.s12_13_2 = function() {
        this.initialize(img.s12_13_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 589, 66);

    //img3
    (lib.s12_13_3 = function() {
        this.initialize(img.s12_13_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1020, 370);

    //img4
    (lib.s12_13_4 = function() {
        this.initialize(img.s12_13_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1020, 353);

    //Page Number
    (lib.Symbol17 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("13", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgThQgcAAgcAOQg5AcAABHMAAAAjhIACARQADAWAJASQAcA4BHAAMBMFAAAIARgCQAWgDASgJQA4gcAAhHMAAAgjhQAAgcgOgcQgcg5hHAAg");
        this.shape.setTransform(257.4, 156.7);

        this.instance = new lib.s12_13_3();
        this.instance.setTransform(2.7, 31.7, 0.5, 0.5);

        this.instance_1 = new lib.s12_13_1();
        this.instance_1.setTransform(47.6, 207.2, 0.667, 0.667);

        this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
        this.text.lineHeight = 22;
        this.text.setTransform(19, 5.6);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(0, 4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_1.setTransform(418.9, 263.7);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_2.setTransform(418.9, 263.7);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_3.setTransform(333.8, 263.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_4.setTransform(333.8, 263.7);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_5.setTransform(248.8, 263.7);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_6.setTransform(248.8, 263.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_7.setTransform(163.7, 263.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_8.setTransform(163.7, 263.7);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_9.setTransform(78.7, 263.7);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_10.setTransform(78.7, 263.7);

        this.addChild(this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.instance_1, this.instance, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 286.9);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgTwQgcAAgcAOQg5AdAABHMAAAAj+IACASQADAVAJASQAcA4BHAAMBMFAAAIARgCQAWgDASgJQA4gcAAhHMAAAgj+QAAgdgOgcQgcg5hHAAg");
        this.shape.setTransform(257.2, 159.3);

        this.instance = new lib.s12_13_4();
        this.instance.setTransform(2.4, 33.1, 0.5, 0.5);

        this.instance_1 = new lib.s12_13_2();
        this.instance_1.setTransform(47.6, 209.1, 0.667, 0.667);

        this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 5.2);

        this.text_1 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(0, 4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_1.setTransform(418.9, 265.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_2.setTransform(418.9, 265.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_3.setTransform(333.8, 265.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_4.setTransform(333.8, 265.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_5.setTransform(248.8, 265.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_6.setTransform(248.8, 265.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_7.setTransform(163.7, 265.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_8.setTransform(163.7, 265.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_9.setTransform(78.7, 265.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_10.setTransform(78.7, 265.9);

        this.addChild(this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.instance_1, this.instance, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.1, 289.3);


    // stage content:
    (lib.p13 = function() {
        this.initialize();

        // Layer 1
        // this.v4 = new lib.Symbol21();
        // this.v4.setTransform(297.6,550.2,1,1,0,0,0,255.1,89.2);

        // this.v3 = new lib.Symbol20();
        // this.v3.setTransform(297.9,400,1,1,0,0,0,254.6,52.1);

        // this.v2 = new lib.Symbol19();
        // this.v2.setTransform(297.6,250.2,1,1,0,0,0,255.1,89.5);

        //  this.v1 = new lib.Symbol18();
        // this.v1.setTransform(297.9,100.8,1,1,0,0,0,254.6,52.5);

        this.other = new lib.Symbol17();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.other1 = new lib.Symbol17();
        this.other1.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.other2 = new lib.Symbol17();
        this.other2.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v2 = new lib.Symbol4();
        this.v2.setTransform(312.9, 495.4, 1, 1, 0, 0, 0, 256.6, 143.5);

        this.v1 = new lib.Symbol3();
        this.v1.setTransform(312.9, 195.6, 1, 1, 0, 0, 0, 256.6, 144.7);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
