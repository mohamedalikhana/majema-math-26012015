var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c41_ex1_s1_1.png",
            id: "c41_ex1_s1_1"
        }, {
            src: "images/c41_ex1_s1_2.png",
            id: "c41_ex1_s1_2"
        }, {
            src: "images/c41_ex1_s1_3.png",
            id: "c41_ex1_s1_3"
        }, {
            src: "images/c41_ex1_s2_1.png",
            id: "c41_ex1_s2_1"
        }]
    };



    (lib.c41_ex1_s1_1 = function() {
        this.initialize(img.c41_ex1_s1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c41_ex1_s1_2 = function() {
        this.initialize(img.c41_ex1_s1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c41_ex1_s1_3 = function() {
        this.initialize(img.c41_ex1_s1_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c41_ex1_s2_1 = function() {
        this.initialize(img.c41_ex1_s2_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Hur mycket pengar är kvar?", "bold 24px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("41", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();

        this.img_1kr_1 = new lib.c41_ex1_s1_3();
        this.img_1kr_1.setTransform(380, 60, 0.3, 0.3);
        this.img_1kr_1.visible = false;

        this.img_1kr_2 = new lib.c41_ex1_s1_3();
        this.img_1kr_2.setTransform(530, 60, 0.3, 0.3);
        this.img_1kr_2.visible = false;

        this.img_2kr_1 = new lib.c41_ex1_s1_2();
        this.img_2kr_1.setTransform(450, 50, 0.3, 0.3);

        this.img_2kr_2 = new lib.c41_ex1_s1_2();
        this.img_2kr_2.setTransform(210, 50, 0.3, 0.3);

        this.img_2kr_3 = new lib.c41_ex1_s1_2();
        this.img_2kr_3.setTransform(450, 223, 0.3, 0.3);

        this.img_2kr_4 = new lib.c41_ex1_s1_2();
        this.img_2kr_4.setTransform(210, 223, 0.3, 0.3);

        this.img_obj = new lib.c41_ex1_s2_1();
        this.img_obj.setTransform(860, 20, 0.7, 0.7);

        this.answers = new lib.answers();
        // this.answers.text_4.visible = true;
        // this.answers.text_3.visible = true;
        // this.answers.text_2.visible = true;
        // this.answers.text_1.visible = true;
        this.answers.setTransform(280 - 230, 300 + 100, 4, 4, 0, 0, 0, 0, 0);

        this.instrText1 = new cjs.Text('Hur många kronor är kvar?', "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(550 + 120, -40);

        this.instrText3 = new cjs.Text('5 kr', "40px 'Myriad Pro'");
        this.instrText3.lineHeight = 19;
        this.instrText3.textAlign = 'center';
        this.instrText3.setTransform(1100, 105);
        this.instrText3.skewX = -15;
        this.instrText3.skewY = -15;

        // this.instrText2 = new cjs.Text('Gå tillsammans med klassen igenom priserna på varornaoch se vilka pengar som passar bäst att betala med. Kanske måste vi växla?', "20px 'Myriad Pro'", "#155EAE");
        // this.instrText2.lineHeight = 19;
        // this.instrText2.textAlign = 'center';
        // this.instrText2.setTransform(550 + 105, 480);

        this.addChild(this.img_obj, this.img_1kr_1, this.img_1kr_2, this.img_2kr_1, this.img_2kr_2, this.img_2kr_3, this.img_2kr_4, this.answers, this.instrText1, this.instrText2, this.instrText3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.answers = this.stage0.answers;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.img_1kr_1.visible = true;
        this.stage0.img_1kr_2.visible = true;

        this.answers = this.stage0.answers;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.img_2kr_1,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 300,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.img_1kr_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2500
        });
        this.tweens.push({
            ref: this.stage0.img_1kr_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2500
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.img_2kr_1.visible = false;
        this.stage0.img_1kr_1.visible = true;
        this.stage0.img_1kr_2.visible = true;

        this.answers = this.stage0.answers;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        var thisStage = this;
        var interval;
        this.onNewExercise = function(e) {
            if (navType == 'prev') {
                interval = setInterval(function() {
                    thisStage.stage0.img_2kr_4.setTransform(1040, 250, 0.3, 0.3);
                    thisStage.stage0.img_2kr_3.setTransform(900, 250, 0.3, 0.3);
                    this.stage0.img_1kr_2.setTransform(780, 260, 0.3, 0.3);
                }, 20);
            } else {
                clearInterval(interval);
            }
        };

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_2kr_4,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 210,
                y: 223
            },
            positionTo: {
                x: 900,
                y: 250
            },
            wait: 800,
            alphaTimeout: 4000,
            override: false
        });

        this.tweens.push({
            ref: this.stage0.img_2kr_3,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 450,
                y: 223
            },
            positionTo: {
                x: 1040,
                y: 250
            },
            wait: 800,
            alphaTimeout: 4000,
            override: false
        });

        this.tweens.push({
            ref: this.stage0.img_1kr_2,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 530,
                y: 60
            },
            positionTo: {
                x: 780,
                y: 260
            },
            wait: 3800,
            alphaTimeout: 3500,
            override: false
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.img_2kr_1.visible = false;
        this.stage0.img_2kr_3.visible = true;
        this.stage0.img_2kr_3.setTransform(900, 250, 0.3, 0.3);
        this.stage0.img_2kr_4.visible = true;
        this.stage0.img_2kr_4.setTransform(1040, 250, 0.3, 0.3);
        this.stage0.img_1kr_2.visible = true;
        this.stage0.img_1kr_2.setTransform(780, 260, 0.3, 0.3);
        this.stage0.img_1kr_1.visible = true;

        this.answers = this.stage0.answers;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("8 kr", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(65, 0);

        this.text_2 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(100, 0);

        this.text_3 = new cjs.Text("5 kr", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(120, 0);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(150, 0);

        this.text_5 = new cjs.Text("3 kr", "16px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(170, 0);

        this.addChild(this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
