(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p96_1.png",
            id: "p96_1"
        }]
    };

    (lib.p96_1 = function() {
        this.initialize(img.p96_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("kunna mäta längd i centimeter", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 658);

        this.text_1 = new cjs.Text("96", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(37.5, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.text_2 = new cjs.Text("Vi övar", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_2.setTransform(95.5, 42);

        this.text_3 = new cjs.Text("33", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(46, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.Symbolimg = new lib.Symbolimg();
        this.Symbolimg.setTransform(30, 28);

        this.addChild(this.Symbolimg, this.shape_2, this.text_3, this.text_2, this.shape_1, this.text_1,this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbolimg = function() {
        this.initialize();

        this.instance = new lib.p96_1();
        this.instance.setTransform(0, 0, 0.47, 0.47);
        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    //Part 1
    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 0);

        this.text_1 = new cjs.Text(" Mät figurens sidor och skriv längderna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 403, 10);
        this.roundRect1.setTransform(10, 13);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#008BD2").s('#706F6F').drawRoundRect(0, 0, 56, 140, 0);
        this.Rect1.setTransform(56, 40);

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("#FFF374").s('#706F6F').drawRoundRect(0, 0, 83, 167, 0);
        this.Rect2.setTransform(221, 40);

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("#EB5B1B").s('#706F6F').drawRoundRect(0, 0, 30, 167, 0);
        this.Rect3.setTransform(418, 40);

        this.Rect4 = new cjs.Shape();
        this.Rect4.graphics.f("#8F96CB").s('#706F6F').drawRoundRect(0, 0, 28, 113, 0);
        this.Rect4.setTransform(70, 257);

        this.Rect5 = new cjs.Shape();
        this.Rect5.graphics.f("#D51317").s('#706F6F').drawRoundRect(0, 0, 113, 86, 0);
        this.Rect5.setTransform(174, 269);

        this.Rect6 = new cjs.Shape();
        this.Rect6.graphics.f("#13A538").s('#706F6F').drawRoundRect(0, 0, 85, 86, 0);
        this.Rect6.setTransform(363, 269);

        var hrlineArr = [];
        var hrlineX = [119, 317, 460, 61, 242, 410, 102, 297, 460, 61, 207, 382];
        var hrlineY = [96, 110, 110, 196, 220, 220, 302, 302, 302, 380, 365, 365];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 20).lineTo(27, 20);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        var TextArr = [];
        var TextX = [149, 347, 490, 91, 272, 440, 132, 327, 490, 91, 237, 412];
        var TextY = [115, 129, 129, 215, 239, 239, 321, 321, 321, 399, 384, 384];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("cm", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.Rect1, this.Rect2, this.Rect3, this.Rect4, this.Rect5, this.Rect6);

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 513.3, 410);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287, 276, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
