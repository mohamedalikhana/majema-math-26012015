var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c22_ex1_1.png",
            id: "c22_ex1_1"
        }, {
            src: "images/c22_ex1_2.png",
            id: "c22_ex1_2"
        }]
    };



    (lib.c22_ex1_1 = function() {
        this.initialize(img.c22_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c22_ex1_2 = function() {
        this.initialize(img.c22_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    var answersX = 270,
        answersY = 350;

    function ballZoomTween(ref, animationSpeed, waitTime, position) {
        var modX = 0,
            modY = 0;
        switch (position) {
            case 'topleft':
                modX = -(20 + (0.31 * 20));
                modY = -(20 + 8 + (0.31 * 20));
                break;
            case 'bottomleft':
                modX = -(20 + (0.31 * 20));
                modY = 0 + 8;
                break;
            case 'topcenter':
                modX = -(15 + (0.31 * 15));
                modY = -(20 + 8 + (0.31 * 20));
                break;
            case 'bottomcenter':
                modX = -(15 + (0.31 * 15));
                modY = 0 + 8;
                break;
            case 'topright':
                modX = +(15 + (0.31 * 15));
                modY = -(20 + 8 + (0.31 * 20));
                break;
            case 'bottomright':
                modX = +(15 + (0.31 * 15));
                modY = 0 + 8;
                break;
        }

        return {
            ref: ref,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: ref.x,
                y: ref.y
            },
            positionTo: {
                x: ref.x + modX,
                y: ref.y + modY
            },
            scaleFrom: {
                x: 0.445 + 0.23,
                y: 0.445 + 0.23
            },
            scaleTo: {
                x: 0.55 + 0.23,
                y: 0.55 + 0.23
            },
            wait: waitTime,
            alphaTimeout: animationSpeed,
            loop: true,
            loopCount: 1,
            wiggle: true
        }
    }

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Ett tal saknas", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("22", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function newPoint(x, y, size) {

        var point = new cjs.Shape();
        point.graphics.s('#000000').ss(1).drawRect(x, y, size, size).drawRect(size, y, size, size).drawRect(size, size, size, size).drawRect(x, size, size, size);
        return point;
    }

    (lib.Stage0 = function() {
        this.initialize();
        this.eggRed = new lib.c22_ex1_1();
        this.eggRed.shadow = new cjs.Shadow('rgba(0,0,0,0.6)', 5, 5, 10);
        this.eggYellow = new lib.c22_ex1_2();
        this.eggYellow.shadow = new cjs.Shadow('rgba(0,0,0,0.6)', 5, 5, 10);



        this.instrTexts = [];
        this.instrTexts.push('6 ägg behövs. Hur många saknas?');



        this.instrText1 = new cjs.Text(this.instrTexts[0], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(670, -40);
        this.instrText1.visible = true;


        this.roundRect1 = new lib.cupBoard();
        this.roundRect1.setTransform(433, 22, 1.45, 1.45);
        this.addChild(this.roundRect1);
        this.redBalls = [];
        this.blueBalls = [];

        var columnSpace = 129,
            rowSpace = 130;

        var ballCartonPlacements = [];
        var rows = 2,
            columns = 3;
        var numberOfBalls = 0;
        var start = {
            x: 470,
            y: 50
        };
        for (var column = 0; column < columns; column++) {
            for (var row = 0; row < rows; row++) {
                ballCartonPlacements.push({
                    x: column * columnSpace,
                    y: row * rowSpace
                });
            };

        };

        for (var i = 0; i < 5; i++) {
            var blueBall = this.eggRed.clone(true);
            blueBall.setTransform(start.x + ballCartonPlacements[i].x, start.y + ballCartonPlacements[i].y, 1.13 + 0.2, 1.13 + 0.2);
            this.addChild(blueBall);
            this.blueBalls.push(blueBall);
            numberOfBalls = numberOfBalls + 1;
        }

        for (var j = 0; j < 1; j++) {
            var redBall = this.eggYellow.clone(true);
            redBall.setTransform(start.x + ballCartonPlacements[numberOfBalls + j].x, start.y + ballCartonPlacements[numberOfBalls + j].y, 1.13 + 0.2, 1.13 + 0.2);
            this.addChild(redBall);
            this.redBalls.push(redBall);
        }
        this.redBalls[0].visible = false;
        // this.redBalls[1].visible = false;
        this.answer1 = new cjs.Container();
        this.textbox1 = new cjs.Shape();
        this.textbox1.graphics.f('#ffffff').s("#949599").ss(1).drawRect(0, 0, 60, 70);
        this.textbox1.setTransform(-6, -4, 0.8, 0.8);

        this.answer1.addChild(this.textbox1)
        this.answer1.setTransform(640, 355);
        this.answer1.visible = true;

        this.answers = new lib.answers(60);
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        // this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(550, answersY, 1, 1);

        // this.teacherInstructions = [];

        // this.teacherInstructions.push("Undersök hur många ägg som ryms i kartongen ");
        // this.teacherInstructions.push("och hur många som saknas.");

        // this.teacherInstruction1 = new cjs.Text(this.teacherInstructions[0], "26px 'Myriad Pro'", "#00B4EA")
        // this.teacherInstruction1.textAlign = 'center';
        // this.teacherInstruction1.lineHeight = 19;
        // this.teacherInstruction1.visible = true;
        // this.teacherInstruction1.setTransform(670, 440);

        // this.teacherInstruction2 = this.teacherInstruction1.clone(true);
        // this.teacherInstruction2.text = this.teacherInstructions[1];
        // this.teacherInstruction2.setTransform(670, 470);

        this.addChild(this.instrText1, this.instrText2, this.instrText3, this.answer1, this.answers, this.teacherInstruction1, this.teacherInstruction2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        // this.stage0.instrText1.visible = true;
        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.redBalls[0].visible = true;
        // this.stage0.redBalls[1].visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.redBalls[0],
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });
        // this.tweens.push({
        //     ref: this.stage0.redBalls[1],
        //     alphaFrom: 0,
        //     alphaTo: 1,
        //     wait: 3000,
        //     alphaTimeout: 2000
        // });
        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.redBalls[0].visible = true;
        // this.stage0.redBalls[1].visible = true;
        this.stage0.answers.text_3.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });
        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.cupBoard = function(ball, count) {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();
        var rectProps1 = {
            height: 200,
            width: 300,
            x: 0,
            y: 0
        };

        var rectProps2 = {
            height: 200,
            width: 300,
            x: 0,
            y: 0
        };

        var verticalSpacing = 23,
            horizontalSpacing = 33;
        var skewRectWidth = 10;
        rectProps2.x = rectProps2.x + horizontalSpacing;
        rectProps2.y = rectProps2.y + verticalSpacing;
        rectProps2.height = rectProps2.height - (2 * verticalSpacing);
        rectProps2.width = rectProps2.width - (2 * horizontalSpacing);

        var middleBlock1Points = {
            x1: rectProps1.width / 3,
            y1: rectProps1.y,
            x2: rectProps1.width / 3,
            y2: rectProps1.height,
            x3: (rectProps1.width / 3) + skewRectWidth,
            y3: rectProps1.height - verticalSpacing,
            x4: (rectProps1.width / 3) + skewRectWidth,
            y4: rectProps1.y + verticalSpacing
        }
        var middleBlock2Points = {
            x1: rectProps1.width * 2 / 3,
            y1: rectProps1.y,
            x2: rectProps1.width * 2 / 3,
            y2: rectProps1.height,
            x3: (rectProps1.width * 2 / 3) - skewRectWidth,
            y3: rectProps1.height - verticalSpacing,
            x4: (rectProps1.width * 2 / 3) - skewRectWidth,
            y4: rectProps1.y + verticalSpacing
        }

        var rect1 = new cjs.Shape();
        rect1.graphics.f("#BAD9D7").s('#000000').ss(1).drawRect(rectProps1.x, rectProps1.y, rectProps1.width, rectProps1.height);
        var rect2 = new cjs.Shape();
        rect2.graphics.f("#87B0BB").s('#000000').ss(1).drawRect(rectProps2.x, rectProps2.y, rectProps2.width, rectProps2.height);
        var lineAD = new cjs.Shape();
        lineAD.graphics.s("#000000").ss(0.5).moveTo(rectProps1.x, rectProps1.y).lineTo(rectProps1.width, rectProps1.height);
        var lineBC = new cjs.Shape();
        lineBC.graphics.s("#000000").ss(0.5).moveTo(rectProps1.width, rectProps1.y).lineTo(rectProps1.x, rectProps1.height);

        var middleLine1 = new cjs.Shape();
        middleLine1.graphics.s("#000000").f("#BAD9D7").ss(1).moveTo(middleBlock1Points.x1, middleBlock1Points.y1).lineTo(middleBlock1Points.x2, middleBlock1Points.y2).lineTo(middleBlock1Points.x3, middleBlock1Points.y3).lineTo(middleBlock1Points.x4, middleBlock1Points.y4).lineTo(middleBlock1Points.x1, middleBlock1Points.y1);
        var middleLine2 = new cjs.Shape();
        middleLine2.graphics.s("#000000").f("#BAD9D7").ss(1).moveTo(middleBlock2Points.x1, middleBlock2Points.y1).lineTo(middleBlock2Points.x2, middleBlock2Points.y2).lineTo(middleBlock2Points.x3, middleBlock2Points.y3).lineTo(middleBlock2Points.x4, middleBlock2Points.y4).lineTo(middleBlock2Points.x1, middleBlock2Points.y1);

        var middleLine = new cjs.Shape();
        middleLine.graphics.s("#000000").ss(1).moveTo(rectProps1.x, rectProps1.height / 2).lineTo(rectProps1.width, rectProps1.height / 2);

        this.addChild(rect1, lineAD, lineBC, rect2, middleLine1, middleLine2, middleLine);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage2 = new lib.Stage2();
        this.stage2.visible = true;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage3 = new lib.Stage3();
        this.stage3.visible = true;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0);





        this.addChild(this.other, this.stage1, this.stage2, this.stage3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);
    (lib.answers = function(fontSize) {
        this.initialize();
        this.text_1 = new cjs.Text("5", fontSize + "px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 0);

        this.text_2 = this.text_1.clone(true);
        this.text_2.text = "+";
        this.text_2.setTransform(fontSize - (25 * fontSize / 100), 0);

        this.text_3 = this.text_1.clone(true);
        this.text_3.text = "1";
        this.text_3.setTransform(2 * (fontSize - (25 * fontSize / 100)), 0);

        this.text_4 = this.text_1.clone(true);
        this.text_4.text = "=";
        this.text_4.setTransform(3 * (fontSize - (25 * fontSize / 100)), 0);

        this.text_5 = this.text_1.clone(true);
        this.text_5.text = "6";
        this.text_5.setTransform(4 * (fontSize - (20 * fontSize / 100)), 0);

        this.addChild(this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
