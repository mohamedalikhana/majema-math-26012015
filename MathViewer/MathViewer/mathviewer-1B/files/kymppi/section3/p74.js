(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p74_1.png",
            id: "p74_1"
        }, {
            src: "images/p74_2.png",
            id: "p74_2"
        }, {
            src: "images/p74_3.png",
            id: "p74_3"
        }]
    };

    (lib.p74_1 = function() {
        this.initialize(img.p74_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p74_2 = function() {
        this.initialize(img.p74_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p74_3 = function() {
        this.initialize(img.p74_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

   
    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Problemlösning", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(177, 72);

        this.text_1 = new cjs.Text("25", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(42, 42);

        this.text_3 = new cjs.Text("ha fått arbeta med problemlösning", "9px 'Myriad Pro'", "#FAAA33");
        this.text_3.setTransform(94, 658);

        this.text_4 = new cjs.Text("74", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(42, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(33.1, 660.8, 1.05, 1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(78, 649.7, 168, 12, 20);
        this.text_Rect.setTransform(0, 0);

          this.instance_2 = new lib.p74_2();
        this.instance_2.setTransform(475, 40, 0.46, 0.46);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text_1, this.text, this.instance_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Barnen ska dela lika på pengarna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(60, 30);

        this.text_2 = new cjs.Text("Hur mycket får de var? Rita och skriv.", "16px 'Myriad Pro'");
        this.text_2.setTransform(60, 53);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(0, 0, 510, 470, 10);
        this.round_Rect1.setTransform(15, 87);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(22, 130).lineTo(518, 130)
        this.line1.setTransform(0, 0);

         this.line2 = new cjs.Shape();
        this.line2.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(272, 98).lineTo(272, 550)
        this.line2.setTransform(0, 0);

        this.text_3 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(210, 515);
        this.text_4 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.setTransform(439, 515);

        this.instance = new lib.p74_1();
        this.instance.setTransform(110, 62, 0.46, 0.46);      

        this.instance_3 = new lib.p74_3();
        this.instance_3.setTransform(12, 2, 0.46, 0.46);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.7).moveTo(150, 515).lineTo(210, 515)
            .moveTo(379, 515).lineTo(439, 515);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.round_Rect1, this.text_1, this.text_2, this.instance, this.instance_2,this.instance_3, this.text_3, this.text_4, this.hrLine_1, this.line1,this.line2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 620, 505);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 255, 1, 1, 0, 0, 0, 256.3, 173.6);



        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
