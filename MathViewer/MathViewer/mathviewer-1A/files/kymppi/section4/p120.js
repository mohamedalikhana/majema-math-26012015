(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p120_1.png",
            id: "p120_1"
        }, {
            src: "images/p120_2.png",
            id: "p120_2"
        }]
    };

    // symbols:
    (lib.p120_1 = function() {
        this.initialize(img.p120_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p120_2 = function() {
        this.initialize(img.p120_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("120", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(36, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.5, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Räkna och hitta bokstäverna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p120_1();
        this.instance.setTransform(377.5, 213, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 517, 328, 10);
        this.roundRect1.setTransform(0, 25);
        //column-1
        this.label1 = new cjs.Text("1  +  2  =", "16px 'Myriad Pro'");
        this.label1.lineHeight = 23;
        this.label1.setTransform(27, 44);
        this.label2 = new cjs.Text("10  –  4  =", "16px 'Myriad Pro'");
        this.label2.lineHeight = 23;
        this.label2.setTransform(20, 72);
        this.label3 = new cjs.Text("2  +  5  =", "16px 'Myriad Pro'");
        this.label3.lineHeight = 23;
        this.label3.setTransform(27, 101);
        this.label4 = new cjs.Text("8  –  4  =", "16px 'Myriad Pro'");
        this.label4.lineHeight = 23;
        this.label4.setTransform(27.5, 131);
        //column-2
        this.label6 = new cjs.Text("10  –  9  =", "16px 'Myriad Pro'");
        this.label6.lineHeight = 23;
        this.label6.setTransform(188, 44);
        this.label7 = new cjs.Text("10  –  6  =", "16px 'Myriad Pro'");
        this.label7.lineHeight = 23;
        this.label7.setTransform(188, 72);
        this.label8 = new cjs.Text("1  +  6  =", "16px 'Myriad Pro'");
        this.label8.lineHeight = 23;
        this.label8.setTransform(196, 101);
        this.label10 = new cjs.Text("10  –  5  =", "16px 'Myriad Pro'");
        this.label10.lineHeight = 23;
        this.label10.setTransform(188, 159);
        this.label11 = new cjs.Text("6  –  6 =", "16px 'Myriad Pro'");
        this.label11.lineHeight = 23;
        this.label11.setTransform(196, 189);
        this.label12 = new cjs.Text("0  +  0  =", "16px 'Myriad Pro'");
        this.label12.lineHeight = 23;
        this.label12.setTransform(194, 217);
        //column-3
        this.label15 = new cjs.Text("3  +  0  =", "16px 'Myriad Pro'");
        this.label15.lineHeight = 23;
        this.label15.setTransform(362, 44);
        this.label16 = new cjs.Text("4  +  4  =", "16px 'Myriad Pro'");
        this.label16.lineHeight = 23;
        this.label16.setTransform(362, 72);
        this.label17 = new cjs.Text("10  –  8  =", "16px 'Myriad Pro'");
        this.label17.lineHeight = 23;
        this.label17.setTransform(355, 101);
        this.label18 = new cjs.Text("8  –  8  =", "16px 'Myriad Pro'");
        this.label18.lineHeight = 23;
        this.label18.setTransform(363, 131);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 7; row++) {
                if (column == 0 && row > 3) {
                    continue;
                } else if (column == 2 && row > 3) {
                    continue;
                } else if (column == 1 && row == 3) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.651;
                } else if (column == 2) {
                    columnSpace = 1.299;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#8490C8").ss(0.9).drawRect(129 + (columnSpace * 258), 39 + (row * 29.1), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 9; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#8490C8").ss(0.9).drawRect(34 + (columnSpace * 29), 284, 20, 23);
        }
        this.textbox_group2.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(87, 62).lineTo(123, 62).moveTo(87, 91).lineTo(123, 91).moveTo(87, 120.3).lineTo(123, 120.3)
            .moveTo(87, 149.3).lineTo(123, 149.3);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(255, 62).lineTo(291, 62).moveTo(255, 91).lineTo(291, 91).moveTo(255, 120.3).lineTo(291, 120.3)
            .moveTo(255, 178.3).lineTo(291, 178.3).moveTo(255, 207.3).lineTo(291, 207.3).moveTo(255, 236.3).lineTo(291, 236.3);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(423, 62).lineTo(459, 62).moveTo(423, 91).lineTo(459, 91).moveTo(423, 120.3).lineTo(459, 120.3)
            .moveTo(423, 149.3).lineTo(459, 149.3);
        this.hrLine_3.setTransform(0, 0);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(32, 313).lineTo(297, 313);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(300, 312.5, 1.2, 1.2, 265);

        var arrText = ['T', 'H', 'N', 'M', 'A', 'E', 'I', 'R', 'Y'];
        var ToBeAdded = [];

        for (var i = 0; i < arrText.length; i++) {
            var columnSpace = i;
            var text = arrText[i];
            if (i == 2) {
                columnSpace = 2.05;
            } else if (i == 3) {
                columnSpace = 3.07;
            } else if (i == 4) {
                columnSpace = 4.18;
            } else if (i == 5) {
                columnSpace = 5.27;
            } else if (i == 6) {
                columnSpace = 6.39;
            } else if (i == 7) {
                columnSpace = 7.37;
            } else if (i == 8) {
                columnSpace = 8.43;
            }

            var temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(40 + (columnSpace * 27.5), 290);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 9; i++) {
            var columnSpace = i;
            var text = (i).toString();
            if (i == 2) {
                columnSpace = 2.05;
            } else if (i == 3) {
                columnSpace = 3.07;
            } else if (i == 4) {
                columnSpace = 4.18;
            } else if (i == 5) {
                columnSpace = 5.27;
            } else if (i == 6) {
                columnSpace = 6.39;
            } else if (i == 7) {
                columnSpace = 7.37;
            } else if (i == 8) {
                columnSpace = 8.4;
            }

            var temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(38 + (columnSpace * 27.5), 321);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 9; i++) {
            var columnSpace = i;
            if (i == 2) {
                columnSpace = 2.0;
            } else if (i == 3) {
                columnSpace = 2.98;
            } else if (i == 4) {
                columnSpace = 3.95;
            } else if (i == 5) {
                columnSpace = 4.95;
            } else if (i == 6) {
                columnSpace = 5.92;
            } else if (i == 7) {
                columnSpace = 6.9;
            } else if (i == 8) {
                columnSpace = 7.88;
            }

            var temp_Line = new cjs.Shape();
            temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(43 + (columnSpace * 29.5), 306).lineTo(43 + (columnSpace * 29.5), 313);
            temp_Line.setTransform(0, 0);
            ToBeAdded.push(temp_Line);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4);
        this.addChild(this.instance, this.textbox_group1, this.textbox_group2, this.shape_1)
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7);
        this.addChild(this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14);
        this.addChild(this.label15, this.label16, this.label17, this.label18, this.label19);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 350);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Vad kostar pärlan?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(19, 0);

        this.text_q1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 0);

        this.instance = new lib.p120_2();
        this.instance.setTransform(12, 34, 0.48, 0.48);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 24, 517, 160, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#B5BBDF').drawRoundRect(12, 35, 132, 62, 7);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = this.roundRect2.clone(true);
        this.roundRect3.setTransform(0, 75);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s('#B5BBDF').drawRoundRect(12, 35, 147, 62, 7);
        this.roundRect4.setTransform(251, 0);

        this.roundRect5 = this.roundRect4.clone(true);
        this.roundRect5.setTransform(251, 75);

        // row-1
        this.text_1 = new cjs.Text("5 kr tillsammans", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(23, 40.5);
        this.text_2 = new cjs.Text("7 kr tillsammans", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 20;
        this.text_2.setTransform(280, 40.5);
        // row-2
        this.text_3 = new cjs.Text("6 kr tillsammans", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 20;
        this.text_3.setTransform(23, 115);
        this.text_4 = new cjs.Text("8 kr tillsammans", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 20;
        this.text_4.setTransform(280, 115);

        var ToBeAdded = [];
        var arrYPos = ['43.7', '122'];
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var j = 0; j < arrYPos.length; j++) {
                var yPos = parseInt(arrYPos[j]);
                if (col == 1 && j == 1) {
                    columnSpace = 0.99;
                }
                temp_text = new cjs.Text("1 kr", "16px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(203 + (columnSpace * 264), yPos);
                ToBeAdded.push(temp_text);
            }
        }

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(10, 103.2).lineTo(509, 103.4)
            .moveTo(253, 28).lineTo(253, 100).moveTo(253, 108).lineTo(253, 180);
        this.hrLine_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (row == 2) {
                    rowSpace = 2.43;
                } else if (row == 3) {
                    rowSpace = 3.44;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#6D6E70").ss(0.8).drawRect(199 + (columnSpace * 263), 39 + (rowSpace * 32.5), 34, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text_q1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5);
        this.addChild(this.instance, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.hrLine_1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i])
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 180);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(297, 490, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297, 114.7, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
