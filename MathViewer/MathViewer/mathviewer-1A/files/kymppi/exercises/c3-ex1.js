var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };


    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Räkna högt: 1, 2.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.rectangles = new lib.rectangles();
        this.rectangles.setTransform(190 + 80, 20, 3.5, 4, 0, 0, 0, 0, 0);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        this.addChild(this.rectangles, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.rectangles = new lib.rectangles();
        this.rectangles.setTransform(190 + 80, 20, 3.5, 4, 0, 0, 0, 0, 0);
        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();

        var shape = this.blueBall.clone(true);
        shape.setTransform(300, 45, 0.8, 0.8);
        var shape1 = this.blueBall.clone(true);
        shape1.setTransform(457, 45, 0.8, 0.8);
        shape.alpha = 1;
        shape1.alpha = 1;
        createjs.Tween.get(shape1).to({
            alpha: 0
        }, 0);
        createjs.Tween.get(shape).to({
            alpha: 0
        }, 0);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.rectangles, shape, shape1, this.commentTexts);
        this.tweens = [];
        this.tweens.push({
            ref: shape,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 750,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: shape1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talen1 och 2", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("3", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.rectangles = function() {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();

        for (var j = 0; j < 6; j++) {
            boxes.graphics.f('#ffffff').s('#000000').ss(0.7).moveTo(0 + (30 * j), 0).lineTo(0 + (30 * j), 50);
        };
        for (var i = 0; i < 3; i++) {
            boxes.graphics.f('#ffffff').s('#000000').ss(0.7).moveTo(0, 0 + (25 * i)).lineTo(150, 0 + (25 * i));
        };

        boxes.setTransform(0, 0, 1.5, 1.5);
        this.addChild(boxes);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
