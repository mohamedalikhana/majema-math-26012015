(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p80_1.png",
            id: "p80_1"
        }, {
            src: "images/p80_2.png",
            id: "p80_2"
        }]
    };

    (lib.p80_1 = function() {
        this.initialize(img.p80_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p80_2 = function() {
        this.initialize(img.p80_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("80", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(35, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30.1, 660.8);

        this.instance = new lib.p80_1();
        this.instance.setTransform(30, 26, 0.8, 0.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(456 + (columnSpace * 32), 28, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många löv är det tillsammans?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20, 1);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.instance_2 = new lib.p80_2();
        this.instance_2.setTransform(28, 62, 0.37, 0.36);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 113, 10);
        this.roundRect1.setTransform(0, 17);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 17);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 135);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 135);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 253);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 253);

        var arrTxtbox = ['109', '227', '345'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding = 40,
            yPos,
            rectWidth = 110,
            rectHeight = 23,
            boxWidth = 22,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 72;
                if (i == 1) {
                    padding = 111;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(73, 99);

        this.text_2 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(97, 99);

        this.text_3 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(117, 99);

        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(141, 99);

        var ToBeAdded = [];
        var arryPos = ['35', '153', '272'];
        var arrxPos = ['89', '340'];
        for (var i = 0; i < arryPos.length; i++) {
            var yPos = parseInt(arryPos[i]);
            var text = '';
            if (i == 0) {text='Addera 2.';} else if (i == 1) {text='Addera 3.';} else if (i == 2) {text='Addera 4.';}

            for (var j = 0; j < arrxPos.length; j++) {
                var xPos = parseInt(arrxPos[j]);

                var text_temp = new cjs.Text(text, "16px 'Myriad Pro'");
                text_temp.lineHeight = -1;
                text_temp.setTransform(xPos, yPos);
                ToBeAdded.push(text_temp);
            }
        }

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(610.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(295, 476, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
