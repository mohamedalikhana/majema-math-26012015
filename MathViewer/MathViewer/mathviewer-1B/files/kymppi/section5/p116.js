(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p116_1.png",
            id: "p116_1"
        }, {
            src: "images/p116_2.png",
            id: "p116_2"
        }, {
            src: "images/p116_3.png",
            id: "p116_3"
        }, {
            src: "images/p116_4.png",
            id: "p116_4"
        }, {
            src: "images/p116_5.png",
            id: "p116_5"
        }]
    };

    (lib.p116_1 = function() {
        this.initialize(img.p116_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p116_2 = function() {
        this.initialize(img.p116_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p116_3 = function() {
        this.initialize(img.p116_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    (lib.p116_4 = function() {
        this.initialize(img.p116_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    (lib.p116_5 = function() {
        this.initialize(img.p116_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Problemlösning", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(195, 72);

        this.text_1 = new cjs.Text("40", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(42, 42);

        this.pageBottomText = new cjs.Text("ha fått arbeta med problemlösning", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(94, 658);



        this.text_4 = new cjs.Text("116", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(40, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(33.1, 660.8, 1.05, 1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(78, 649.7, 168, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.instance1 = new lib.p116_1();
        this.instance1.setTransform(115, 95, 0.46, 0.46);

        this.instance2 = new lib.p116_5();
        this.instance2.setTransform(55, 80, 0.46, 0.46);

        this.instance3 = new lib.p116_3();
        this.instance3.setTransform(350, 115, 0.46, 0.46);

        this.instance4 = new lib.p116_4();
        this.instance4.setTransform(380, 240, 0.46, 0.46);

        this.text_6 = new cjs.Text("Mira och Leo kastar pil.", "16px 'Myriad Pro'");
        this.text_6.setTransform(340, 200);

        this.text_5 = new cjs.Text("Mira får 20 poäng med 4 pilar.", "16px 'Myriad Pro'");
        this.text_5.setTransform(340, 220);
        var ToBeAdded = []
        var tmptxt = [9, 8, 1, 3, 5, 6, 4, 2, 7, 10]
        var i = 0
        var xpos = 115
        var ypos = 150

        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 5; col++) {
                if (row == 0 && col == 0) {
                    xpos = 135;
                    ypos = 185
                } else if (row == 0 && col == 2) {
                    ypos = 140
                } else if (row == 0 && col == 4) {
                    xpos = 95;
                    ypos = 185
                } else if (row == 1 && col == 0) {
                    xpos = 145;
                    ypos = 225
                } else if (row == 1 && col == 3) {
                    xpos = 130;
                    ypos = 228
                } else if (row == 1 && col == 4) {
                    xpos = 27;
                    ypos = 195
                } else if (row == 1) {
                    ypos = 250;
                    xpos = 135
                } else {
                    xpos = 115;
                    ypos = 150
                }
                var tempText = new cjs.Text(tmptxt[i], "27.96px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 40), ypos);
                ToBeAdded.push(tempText);
                i++
            }
        };



        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4,  this.pageBottomText, this.text_1, this.text);
        this.addChild(this.instance1, this.instance2, this.instance3, this.instance4, this.text_6, this.text_5)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.wheel = function() {
        this.initialize();
        var ToBeAdded = [];

        this.instance1 = new lib.p116_2();
        this.instance1.setTransform(55, 35, 0.46, 0.46);

        this.tmpContainer = new cjs.Container();

        var tmptxt = [9, 8, 1, 3, 5, 6, 4, 2, 7, 10]
        var i = 0
        var xpos = 58
        var ypos = 75
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 5; col++) {
                if (row == 0 && col == 0) {
                    xpos = 70;
                    ypos = 95
                } else if (row == 0 && col == 2) {
                    ypos = 65
                } else if (row == 0 && col == 4) {
                    xpos = 45;
                    ypos = 95
                } else if (row == 1 && col == 0) {
                    xpos = 75;
                    ypos = 120
                } else if (row == 1 && col == 3) {
                    xpos = 67;
                    ypos = 122
                } else if (row == 1 && col == 4) {
                    xpos = 2;
                    ypos = 102
                } else if (row == 1) {
                    ypos = 135;
                    xpos = 70
                } else {
                    xpos = 58;
                    ypos = 75
                }
                var tempText = new cjs.Text(tmptxt[i], "17.16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 25), ypos);
                ToBeAdded.push(tempText);
                i++
            }
        };

        this.addChild(this.instance1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 620, 505);


    (lib.Symbol1 = function() {
        this.initialize();

         this.text_1 = new cjs.Text("Var på tavlan kan pilarna sitta? Visa olika lösningar.", "16px 'Myriad Pro'");
        this.text_1.setTransform(15, 0);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(0, 0, 510, 322, 10);
        this.round_Rect1.setTransform(15, 10);

        this.addChild(this.round_Rect1,this.text_1);
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 3; col++) {
                var wheel = new lib.wheel();
                wheel.setTransform(0 + (col * 155), 0 + (row * 150));
                this.addChild(wheel)
            };
        };


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 310);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 470, 1, 1, 0, 0, 0, 256.3, 173.6);



        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
