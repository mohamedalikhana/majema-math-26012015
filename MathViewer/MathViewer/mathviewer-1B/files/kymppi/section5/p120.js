(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p120_1.png",
            id: "p120_1"
        }]
    };

    // symbols:
    (lib.p120_1 = function() {
        this.initialize(img.p120_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);




    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1

        this.text = new cjs.Text("120", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.2);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Bind ihop talen 50 till 100.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 65);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 65);       

        this.instance = new lib.p120_1();
        this.instance.setTransform(0, 0, 0.467, 0.46);


        this.addChild(this.instance, this.text, this.text_1);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 553.3, 280);


    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(7, 11, 515, 320, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Para ihop tal och bokstav på tallinjen.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(10, 0);

        var vrRect = new cjs.Shape();
        var ToBeAdded = [];
        var rectColspace = 90
        for (var row = 0; row < 5; row++) {
            var rowSpace = row;
            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                if (column > 2) {
                    rectColspace = 92
                } else {
                    rectColspace = 90
                }
                if (column == 4 && row == 4) {
                    continue
                }
                vrRect.graphics.f('#ffffff').s("#00A3C4").drawRoundRect(80 + (columnSpace * rectColspace), 25 + (rowSpace * 30), 21, 23, 0);
                vrRect.setTransform(0, 0);
                ToBeAdded.push(vrRect);
            }
        }

        //draw small arrow
        var moveToX = 53
        var moveToY = 37
        var lineToX = 73
        var lineToY = 37
        var colspace = 90
        var rowspace = 30
        var txtColspace = 90.1
        var tmpChar = ['P', 'G', 'H', 'G', 'A', 'P', 'B', 'A', 'R', 'L', 'A', 'G', 'Å', 'K', 'E', 'Y', 'P', 'T', 'O', 'H', 'A', 'R', 'F', 'L']
        var tmpNum = [63, 79, 67, 90, 97, 61, 76, 65, 98, 95, 68, 78, 89, 96, 91, 77, 64, 80, 94, 93, 62, 69, 88, 92]
        var i = 0
        var arrowLine = new cjs.Shape();
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 5; col++) {
                if (col > 2) {
                    colspace = 92;
                    txtColspace = 92
                } else {
                    colspace = 90;
                    txtColspace = 90.1
                }
                if (col == 4 && row == 4) {
                    continue;
                }
                arrowLine.graphics.s("#000000").ss(0.5).moveTo(moveToX + (col * colspace), moveToY + (row * rowspace))
                    .lineTo(lineToX + (col * colspace), lineToY + (row * rowspace))
                    .moveTo((moveToX + 16) + (col * colspace), (lineToY + 4) + (row * rowspace))
                    .lineTo(lineToX + (col * colspace), lineToY + (row * rowspace)).lineTo((moveToX + 16) + (col * colspace), (lineToY - 4) + (row * rowspace))

                var tempText = new cjs.Text(tmpChar[i], "16px 'Myriad Pro'");
                tempText.setTransform(85 + (col * txtColspace), 42 + (row * 30));
                var txtNum = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                txtNum.setTransform(30 + (col * txtColspace), 42 + (row * 30));
                ToBeAdded.push(arrowLine, tempText, txtNum)
                i++
            }
        }


        var xpos = 62
        var ypos = 210
        var hrRect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            for (var column = 0; column < 13; column++) {
                var columnSpace = column;  
                if (column > 7 && numOfrow == 0) {
                    xpos = 198
                } else if (column > 4 && numOfrow == 0) {
                    xpos = 82
                } else if (numOfrow == 1) {
                    ypos = 270
                    xpos = 200
                } else {
                    xpos = 62
                }
                
                if(numOfrow==1 && column>10){continue}

                hrRect.graphics.f('#ffffff').s("#00A3C4").drawRoundRect(xpos + (columnSpace * 19.5), ypos, 15, 18, 0);
                hrRect.setTransform(0, 0);
                ToBeAdded.push(hrRect);
            }
        }
        //draw arrow line
        var lineColor = "#000000"
        var lineToypos = 34
        var ypos = 195;
        var temp_hrLine1 = new cjs.Shape();
        var temp_vrLine1 = new cjs.Shape();
        var tmpss=0.5
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            for (var col = 0; col < 21; col++) {
                if (numOfrow == 1) {
                    ypos = 255
                } else {
                    ypos = 195
                }
                temp_hrLine1.graphics.s("#000000").f("#000000").ss(0.1).moveTo(23, (43 + ypos)).lineTo(460, (43 + ypos))
                    .moveTo(460, (43 + ypos)).lineTo(458, (39 + ypos)).lineTo(466, (43 + ypos)).lineTo(458, (47 + ypos))
                if (col == 0 || col == 5 || col == 10 || col == 15 || col == 20) {
                    lineToypos = 33
                    tmpss=1.5
                } else {
                    lineToypos = 36
                    tmpss=0.5
                }
                if (col == 0 || col == 10 || col == 20) {
                    lineColor = "#00ABDC"
                } else {
                    lineColor = "#000000"
                }
                temp_vrLine1.graphics.s(lineColor).ss(tmpss).moveTo(50 + (col * 19.5), (43 + ypos))
                    .lineTo(50 + (col * 19.5), (lineToypos + ypos));
                ToBeAdded.push(temp_hrLine1, temp_vrLine1)
            }
        }
        var fontColor = ""
        var bottomNum = [60, 65, 70, 75, 80, 80, 85, 90, 95, 100]

        var j = 0;
        var xpos = 41
        var ypos = 105
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            for (var col = 0; col < 5; col++) {
                if (col == 0 || col == 2 || col == 4) {
                    fontColor = "#00ABDC"
                } else {
                    fontColor = "#000000"
                }
                if (numOfrow == 1) {
                    ypos = 165
                } else {
                    ypos = 105
                }
                var txtNum = new cjs.Text(bottomNum[j], "16px 'MyriadPro-Semibold'", fontColor);
                txtNum.setTransform(xpos + (col * 97.5), ypos + (row * 30));

                ToBeAdded.push(arrowLine, tempText, txtNum)
                j++
            }
        }
        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance, this.instance1);



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 310);

    // stage content: 513.3, 340
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(292, 57, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(292, 340, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
