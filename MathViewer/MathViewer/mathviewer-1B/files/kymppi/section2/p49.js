(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p49_1.png",
            id: "p49_1"
        }, {
            src: "images/p49_2.png",
            id: "p49_2"
        }]
    };

    (lib.p49_1 = function() {
        this.initialize(img.p49_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p49_2 = function() {
        this.initialize(img.p49_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("49", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(555, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.instance = new lib.p49_1();
        this.instance.setTransform(55, 0, 0.46, 0.46);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(463 + (columnSpace * 32), 24, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape_2, this.shape_1, this.shape, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Hur många foton har pappa tagit tillsammans av djuren?", "16px 'Myriad Pro'");
        this.text_2.setTransform(5, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(5, 0);

        this.roundRect = new cjs.Shape();
        this.roundRect.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 253, 99, 10);
        this.roundRect.setTransform(0, 0);

        this.roundRect1 = this.roundRect.clone(true);
        this.roundRect1.setTransform(0, 103);

        this.roundRect2 = this.roundRect.clone(true);
        this.roundRect2.setTransform(257, 0);

        this.roundRect3 = this.roundRect.clone(true);
        this.roundRect3.setTransform(257, 103);

        this.instance = new lib.p49_2();
        this.instance.setTransform(35, 30, 0.46, 0.46);

        //draw rectangle boxes

        this.textbox_group1 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var row = 0; row < 2; row++) {

            for (var col = 0; col < 8; col++) {
                this.textbox_group1.graphics.s("#000000").ss(0.3).drawRect((53 + Xpos) + (col * 20), (82 + Ypos) + (row * 18), 20, 22);
            }
            Ypos = Ypos + 88;
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var row = 0; row < 2; row++) {

            for (var col = 0; col < 8; col++) {
                this.textbox_group2.graphics.s("#000000").ss(0.3).drawRect((308 + Xpos) + (col * 20), (82 + Ypos) + (row * 18), 20, 22);
            }
            Ypos = Ypos + 88;
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance_2, this.text, this.roundRect, this.roundRect1, this.roundRect2, this.roundRect3, this.instance, this.textbox_group1, this.textbox_group2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 553.3, 206.6);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 5);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(5, 5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 17, 510, 155, 10);
        this.roundRect1.setTransform(0, 0);

        var arryVal = ['6 + 6 =', '6 + 7 =', '7 + 7 =', '7 + 8 =', '8 + 8 =', '8 + 3 =', '8 + 4 =', '8 + 5 =', '8 + 7 =', '8 + 9 =', '4 + 9 =', '3 + 9 =', '5 + 9 =', '6 + 9 =', '7 + 9 ='];
        var ToBeAdded = [];
        var i = 0;
        var tmpline = new cjs.Shape();
        var ypos = 28;
        for (var col = 0; col < 3; col++) {
            var columnSpace = col;
            for (var row = 0; row < 5; row++) {
                var rowSpace = row;
                if (row >2) {
                    ypos = 27;
                } else {
                    ypos = 28;
                }
                var tempText = new cjs.Text(arryVal[i], "16px 'Myriad Pro'");
                tempText.setTransform(48 + (160 * columnSpace), 45 + (27 * rowSpace));
                tmpline.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(98 + (160 * columnSpace), 50 + (ypos * rowSpace)).lineTo(140 + (160 * columnSpace), 50 + (ypos * rowSpace));
                ToBeAdded.push(tempText, tmpline)
                i++;
            };
        };

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550.3, 156);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(306, 441, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(306, 465, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
