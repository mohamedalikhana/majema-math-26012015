(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p102_1.png",
            id: "p102_1"
        }, {
            src: "images/p102_2.png",
            id: "p102_2"
        }]
    };

    (lib.p102_1 = function() {
        this.initialize(img.p102_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p102_2 = function() {
        this.initialize(img.p102_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Dubbelt", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(241, 37);

        this.text_1 = new cjs.Text("35", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(40, 22);

        this.pageBottomText = new cjs.Text("förstå och kunna använda begreppet dubbelt", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(87.5, 651.2);

        this.text_4 = new cjs.Text("102", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(38, 649);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(33.1, 660.8, 1.05, 1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(78, 649.7, 194.1, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.pageBottomText, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        var adjustX=12;
        var adjustY=10;

        this.text_1 = new cjs.Text("Leo har dubbelt så många kakor som Mira. Rita Leos kakor. Skriv antalet.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(8-adjustX, 4-adjustY);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(11, 69, 505, 474, 10);
        this.round_Rect1.setTransform(0-adjustX, 0-adjustY);

        this.hrRule_1 = new cjs.Shape();
        this.hrRule_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(22, 113).lineTo(505, 113).moveTo(22, 198).lineTo(505, 198)
            .moveTo(22, 280).lineTo(505, 280).moveTo(22, 366).lineTo(505, 366).moveTo(22, 449.5).lineTo(505, 449.5);
        this.hrRule_1.setTransform(0-adjustX, 0-adjustY);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#949599").setStrokeStyle(1).moveTo(260.8, 81).lineTo(260.8, 530);
        this.Line_1.setTransform(0-adjustX, 0-adjustY);

        this.instance_1 = new lib.p102_1();
        this.instance_1.setTransform(85-adjustX, 42.5-adjustY, 0.357, 0.357);

        this.instance_2 = new lib.p102_2();
        this.instance_2.setTransform(27-adjustX, 135-adjustY, 0.37, 0.37);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 5; row++) {
                var rowSpace = row;
                if (row == 1) {
                    rowSpace = 1.01;
                } else if (row == 2) {
                    rowSpace = 2.08;
                } else if (row == 3) {
                    rowSpace = 3.11;
                } else if (row == 4) {
                    rowSpace = 4.18;
                }

                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(220 + (columnSpace * 255), 164 + (rowSpace * 80), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0-adjustX, 0-adjustY);

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(220-adjustX, 154-adjustY);

        this.addChild(this.text_1, this.round_Rect1, this.hrRule_1, this.Line_1, this.instance_1, this.instance_2, this.textbox_group1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 529);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(309, 275, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
