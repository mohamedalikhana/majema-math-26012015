(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p45_1.png",
            id: "p45_1"
        }, {
            src: "images/p45_2.png",
            id: "p45_2"
        }]
    };

    (lib.p45_1 = function() {
        this.initialize(img.p45_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p45_2 = function() {
        this.initialize(img.p45_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("45", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(553, 663);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(5, 0);

        this.text_1 = new cjs.Text("Vad kostar pärlan?", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7D7D7D').drawRoundRect(0, 0, 510.5, 230, 10);
        this.roundRect1.setTransform(5, 12);

        this.instance = new lib.p45_2();
        this.instance.setTransform(10, 27, 0.46, 0.46);

        this.label1 = new cjs.Text("16 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label1.setTransform(25, 53);
        this.label2 = new cjs.Text("16 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label2.setTransform(280, 53);
        this.label3 = new cjs.Text("16 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label3.setTransform(25, 160);
        this.label4 = new cjs.Text("18 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label4.setTransform(280, 160);

        this.label5 = new cjs.Text("2 kr", "16.3px 'Myriad Pro'");
        this.label5.setTransform(200, 50);
        this.label6 = new cjs.Text("1 kr", "16.3px 'Myriad Pro'");
        this.label6.setTransform(455, 50);
        this.label7 = new cjs.Text("2 kr", "16.3px 'Myriad Pro'");
        this.label7.setTransform(200, 155);
        this.label8 = new cjs.Text("6 kr", "16.3px 'Myriad Pro'");
        this.label8.setTransform(455, 155);

        this.label5.skewX = 7;
        this.label5.skewY = 7;
        this.label6.skewX = 7;
        this.label6.skewY = 7;
        this.label7.skewX = 7;
        this.label7.skewY = 7;
        this.label8.skewX = 7;
        this.label8.skewY = 7;


        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(10, 125).lineTo(510, 125);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(255, 37).lineTo(255, 109);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(255, 140).lineTo(255, 210);


        this.InnerroundRect1 = new cjs.Shape();
        this.InnerroundRect1.graphics.f("#ffffff").s('#F7B798').ss(2).drawRoundRect(0, 10, 137, 75, 6);
        this.InnerroundRect1.setTransform(17, 23);

        this.InnerroundRect2 = this.InnerroundRect1.clone(true);
        this.InnerroundRect2.setTransform(270, 23);

        this.InnerroundRect3 = this.InnerroundRect1.clone(true);
        this.InnerroundRect3.setTransform(17, 130);

        this.InnerroundRect4 = this.InnerroundRect1.clone(true);
        53
        this.InnerroundRect4.setTransform(270, 130);


        this.addChild(this.roundRect1, this.InnerroundRect1, this.InnerroundRect2);
        this.addChild(this.text_1, this.textbox_group1, this.InnerroundRect3, this.InnerroundRect4, this.hrLine_1, this.hrLine_2, this.hrLine_3);
        this.addChild(this.text, this.instance, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 150);

    (lib.Symbol6 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna och måla svaret.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 14, 511, 267, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        this.textbox_group1.setTransform(0, 0);

        this.instance = new lib.p45_1();
        this.instance.setTransform(345, 30, 0.47, 0.47);

        this.label1 = new cjs.Text("9 + 9 =", "16px 'Myriad Pro'");
        this.label1.setTransform(40, 43);

        this.label2 = new cjs.Text("4 + 4 =", "16px 'Myriad Pro'");
        this.label2.setTransform(40, 73);

        this.label3 = new cjs.Text("7 + 7 =", "16px 'Myriad Pro'");
        this.label3.setTransform(40, 103);

        this.label4 = new cjs.Text("6 + 6 =", "16px 'Myriad Pro'");
        this.label4.setTransform(40, 133);

        this.label5 = new cjs.Text("8 + 8 =", "16px 'Myriad Pro'");
        this.label5.setTransform(40, 163);

        this.label6 = new cjs.Text("8 + 7 =", "16px 'Myriad Pro'");
        this.label6.setTransform(40, 193);


        this.label7 = new cjs.Text("5 + 5 =", "16px 'Myriad Pro'");
        this.label7.setTransform(195, 43);

        this.label8 = new cjs.Text("4 + 5 =", "16px 'Myriad Pro'");
        this.label8.setTransform(195, 73);

        this.label9 = new cjs.Text("5 + 6 =", "16px 'Myriad Pro'");
        this.label9.setTransform(195, 103);

        this.label10 = new cjs.Text("9 + 8 =", "16px 'Myriad Pro'");
        this.label10.setTransform(195, 133);

        this.label11 = new cjs.Text("3 + 4 =", "16px 'Myriad Pro'");
        this.label11.setTransform(195, 163);

        this.label12 = new cjs.Text("7 + 6 =", "16px 'Myriad Pro'");
        this.label12.setTransform(195, 193);

        // ROW-1 ROUND SHAPES--columns1
        this.hrLine_1 = new cjs.Shape();
        this.shape_group2 = new cjs.Shape();
        var fillcolor = "";
        var columnXpos = 0;
        var MoveToX = 0;
        var LineToX = 0;
        var LineToY = 0;
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {

                if (row < 4 && column == 0) {
                    fillcolor = "#008BD2";
                    columnXpos = 144;
                } else if (row > 3 && column == 0) {
                    fillcolor = "#D51317";
                    columnXpos = 144;
                } else if (row < 2 && column == 1) {
                    fillcolor = "#D51317";
                    columnXpos = 275;
                } else if (row > 1 && column == 1) {
                    fillcolor = "#FFF374";
                    columnXpos = 275;
                }
                this.shape_group2.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 25), 42 + (row * 29), 9.5, 0, 2 * Math.PI)

                if (column == 0) {
                    MoveToX = 95;
                    LineToX = 130;
                    LineToY = 51;
                } else if (column == 1) {
                    MoveToX = 225;
                    LineToX = 260;
                    LineToY = 51;
                }
                this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(MoveToX + (25 * columnSpace), LineToY + (29 * row)).lineTo(LineToX + (25 * columnSpace), LineToY + (29 * row));

            }
        }
        this.shape_group2.setTransform(0, 0);
        this.hrLine_1.setTransform(0, 0);


        var arrxPos = [32, 122, 212, 302];
        var arryPos = [230, 230, 230, 230];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arryPos.length; i++) {
            var xPos = arrxPos[i];
            var yPos = arryPos[i];

            for (var row = 0; row < 2; row++) {
                for (var column = 0; column < 2; column++) {
                    var columnSpace = (column == 1 && row == 0) ? column - 0.5 : column;
                    if (column == 0 && row == 0) {
                        continue;
                    }
                    this.shape_group1.graphics.s("#6E6E70").ss(0.8, 0, 0, 6).arc(xPos + (columnSpace * 35), yPos + (row * 21.5), 14, 0, 2 * Math.PI);
                }
            }
        }

        this.shape_group1.setTransform(0, 0);

        var arrxPos = [28, 113, 202, 293];
        var arryPos = [235, 235, 235, 235];
        var arrNum = [8, 7, 9, 11, 10, 12, 14, 13, 15, 17, 16, 18]
        var ToBeAdded = [];
        j = 0;
        //this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arryPos.length; i++) {
            var xPos = arrxPos[i];
            var yPos = arryPos[i];

            for (var row = 0; row < 2; row++) {
                for (var column = 0; column < 2; column++) {
                    var columnSpace = (column == 1 && row == 0) ? column - 0.5 : column;
                    if (column == 0 && row == 0) {
                        continue;
                    }
                    var temp_text = new cjs.Text(arrNum[j], "16px 'Myriad Pro'");
                    temp_text.setTransform(xPos + (columnSpace * 35), yPos + (row * 21.5));
                    ToBeAdded.push(temp_text);
                    j++
                }
            }
        }




        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12);

        this.addChild(this.shape_group1, this.label13, this.shape_group2, this.hrLine_1, this.hrLine_2, this.instance);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 572.3, 250);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(307, 274, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(307, 395, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
