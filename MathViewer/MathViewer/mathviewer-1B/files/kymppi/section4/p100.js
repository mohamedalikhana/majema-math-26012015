(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p100_1.png",
            id: "p100_1"
        }]
    };

    (lib.p100_1 = function() {
        this.initialize(img.p100_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("100", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(443 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 17);

        this.Symbolimg = new lib.Symbolimg();
        this.Symbolimg.setTransform(27, 67);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1, this.Symbolimg);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbolimg = function() {
        this.initialize();

        this.instance = new lib.p100_1();
        this.instance.setTransform(0, 0, 0.465, 0.47);

        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   
        // 
        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(1, 13);

        this.text_1 = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 13);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 200, 10);
        this.roundRect1.setTransform(0, 25);

        var lineArr = [];
        var lineX = [80, 252, 428, 80, 252, 428, 80, 252, 428, 80, 252, 428, 80, 252, 428];
        var lineY = [55, 55, 55, 82, 82, 82, 110, 110, 110, 137, 137, 137, 167, 167, 167];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(35, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("9  +  9  =", "16px 'Myriad Pro'");
        this.label1.setTransform(21, 90);
        this.label2 = new cjs.Text("8  +  8  =", "16px 'Myriad Pro'");
        this.label2.setTransform(21, 118);
        this.label3 = new cjs.Text("7  +  7  =", "16px 'Myriad Pro'");
        this.label3.setTransform(21, 146);
        this.label4 = new cjs.Text("6  +  6  =", "16px 'Myriad Pro'");
        this.label4.setTransform(21, 174);
        this.label5 = new cjs.Text("5  +  7  =", "16px 'Myriad Pro'");
        this.label5.setTransform(21, 202);

        this.label6 = new cjs.Text("4  +  9  =", "16px 'Myriad Pro'");
        this.label6.setTransform(193, 90);
        this.label7 = new cjs.Text("3  +  8  =", "16px 'Myriad Pro'");
        this.label7.setTransform(193, 118);
        this.label8 = new cjs.Text("2  +  9  =", "16px 'Myriad Pro'");
        this.label8.setTransform(193, 146);
        this.label9 = new cjs.Text("11  –  7  =", "16px 'Myriad Pro'");
        this.label9.setTransform(186, 174);
        this.label10 = new cjs.Text("12  –  7  =", "16px 'Myriad Pro'");
        this.label10.setTransform(186, 202);

        this.label11 = new cjs.Text("13  –  5  =", "16px 'Myriad Pro'");
        this.label11.setTransform(362, 90);
        this.label12 = new cjs.Text("14  –  8  =", "16px 'Myriad Pro'");
        this.label12.setTransform(362, 118);
        this.label13 = new cjs.Text("15  –  6  =", "16px 'Myriad Pro'");
        this.label13.setTransform(362, 146);
        this.label14 = new cjs.Text("16  –  7  =", "16px 'Myriad Pro'");
        this.label14.setTransform(362, 174);
        this.label15 = new cjs.Text("17  –  9  =", "16px 'Myriad Pro'");
        this.label15.setTransform(362, 202);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(5, 20).lineTo(465, 20);
        this.Line_1.setTransform(17, 30);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(121.5, 54);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_3.setTransform(227, 54);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(336, 54);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_5.setTransform(446, 54);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.5), 50 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 10) {
                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(52 + (columnSpace * 20.5), 50 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 15) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(59 + (columnSpace * 20.5), 50 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else {

                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(66 + (columnSpace * 20.5), 50 + (row * 19), 7.9, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group1.setTransform(0, 0);

        this.addChild(this.text, this.text_1, this.roundRect1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5,
            this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15,
            this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 220);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290, 450, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
