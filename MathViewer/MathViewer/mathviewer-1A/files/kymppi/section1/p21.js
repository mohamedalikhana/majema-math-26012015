(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1219,
	height: 678,
	fps: 20,
	color: "#FFFFFF",
	manifest: [
		{src:"images/s20_21_1.png", id:"s20_21_1"},
		{src:"images/s20_21_2.png", id:"s20_21_2"},
		{src:"images/s20_21_3.png", id:"s20_21_3"},
		{src:"images/s20_21_4.png", id:"s20_21_4"}
	]
};
// symbols:
(lib.s20_21_1 = function() {
	this.initialize(img.s20_21_1);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,751,95);


(lib.s20_21_2 = function() {
	this.initialize(img.s20_21_2);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,751,95);


(lib.s20_21_3 = function() {
	this.initialize(img.s20_21_3);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,713,239);


(lib.s20_21_4 = function() {
	this.initialize(img.s20_21_4);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,710,239);


(lib.Symbol11 = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("21", "12px 'Myriad Pro'", "#FFFFFF");
	this.text.lineHeight = 18;
	this.text.setTransform(555,648);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
	this.shape.setTransform(579,660.8);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,1218.9,677.5);



(lib.Symbol21 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.s20_21_4, null, new cjs.Matrix2D(0.72,0,0,0.72,-255.9,-86.6)).s().p("EgnzANWIAA46QAAhHA5gcQAcgOAcAAMBMFAAAQBHAAAcA5QAOAcAAAcIAAY6g");
	this.shape.setTransform(257.4,116.8);

	this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
	this.text.lineHeight = 19;
	this.text.setTransform(19,6.2);

	this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#00A5C0");
	this.text_1.lineHeight = 19;
	this.text_1.setTransform(0,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_1.setTransform(418.9,264.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
	this.shape_2.setTransform(418.9,264.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_3.setTransform(333.8,264.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
	this.shape_4.setTransform(333.8,264.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_5.setTransform(248.8,264.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
	this.shape_6.setTransform(248.8,264.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_7.setTransform(163.7,264.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
	this.shape_8.setTransform(163.7,264.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_9.setTransform(78.7,264.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
	this.shape_10.setTransform(78.7,264.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("EgmCgT6QgcAAgcAOQg5AcAABHMAAAAkTIACARQADAVAJASQAcA5BHAAMBMFAAAIARgCQAWgEASgIQA4gcAAhHMAAAgkTQAAgcgOgcQgcg5hHAAg");
	this.shape_11.setTransform(257.4,158.9);

	this.instance = new lib.s20_21_1();
	this.instance.setTransform(66.7,204,0.5,0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("EgmCAT7QhHAAgcg5QgJgRgDgWIgCgRMAAAgkSQAAhIA5gcQAcgOAcAAMBMFAAAQBHAAAcA5QAOAcAAAdMAAAAkSQAABGg4AdQgSAIgWAEIgRACg");
	this.shape_12.setTransform(257.4,158.9);

	this.addChild(this.shape_12,this.instance,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.text_1,this.text,this.ra1,this.ra2,this.ra3,this.ra4,this.ra5,this.shape);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,288.7);


(lib.Symbol20 = function() {
	this.initialize();
	// Layer 1
	this.instance = new lib.s20_21_2();
	this.instance.setTransform(66.7,201.7,0.5,0.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.s20_21_3, null, new cjs.Matrix2D(0.72,0,0,0.72,-256,-86.6)).s().p("EgnzANWIAA46QAAhHA5gcQAcgOAcAAMBMFAAAQBHAAAcA5QAOAcAAAcIAAY6g");
	this.shape.setTransform(257.4,116.6);

	this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
	this.text.lineHeight = 19;
	this.text.setTransform(19,6.2);

	this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#00A5C0");
	this.text_1.lineHeight = 19;
	this.text_1.setTransform(0,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_1.setTransform(418.9,261.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
	this.shape_2.setTransform(418.9,261.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_3.setTransform(333.8,261.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
	this.shape_4.setTransform(333.8,261.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_5.setTransform(248.8,261.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
	this.shape_6.setTransform(248.8,261.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_7.setTransform(163.7,261.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
	this.shape_8.setTransform(163.7,261.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBvIC/AAIAAjdIi/AAg");
	this.shape_9.setTransform(78.7,261.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhfBvIAAjcIC/AAIAADcg");
	this.shape_10.setTransform(78.7,261.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("EgmCgTgQgcAAgcAOQg5AcAABHMAAAAjfIACARQADAWAJASQAcA4BHAAMBMFAAAIARgCQAWgDASgJQA4gcAAhHMAAAgjfQAAgcgOgcQgcg5hHAAg");
	this.shape_11.setTransform(257.4,156.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("EgmCAThQhHAAgcg4QgJgSgDgWIgCgRMAAAgjfQAAhHA5gcQAcgOAcAAMBMFAAAQBHAAAcA5QAOAcAAAcMAAAAjfQAABHg4AcQgSAJgWADIgRACg");
	this.shape_12.setTransform(257.4,156.1);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.text_1,this.text,this.shape,this.instance,this.ra1,this.ra2,this.ra3,this.ra4,this.ra5);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,285.9);


// stage content:
(lib.p21 = function() {
	this.initialize();

	// Layer 1
	this.other = new lib.Symbol11();
	this.other.setTransform(609.5,339,1,1,0,0,0,609.5,338.7);

	this.v2 = new lib.Symbol21();
	this.v2.setTransform(310.8,493.1,1,1,0,0,0,256.6,144.3);

	this.v1 = new lib.Symbol20();
	this.v1.setTransform(310.8,194,1,1,0,0,0,256.6,142.9);

	this.addChild(this.v1,this.v2,this.other);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(609.5,339.3,1218.9,677.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;