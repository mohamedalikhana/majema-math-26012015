var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c47_ex1_1.png",
            id: "c47_ex1_1"
        }, {
            src: "../section4/images/p135_1.png",
            id: "p135_1"
        }]
    };



    (lib.c47_ex1_1 = function() {
        this.initialize(img.c47_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.p135_1 = function() {
        this.initialize(img.p135_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Stapeldiagram", "bold 24px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("47", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.statistics_diagram = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#E0E4F2").s('#8490C8').drawRoundRect(10, 10, 470, 246, 0);
        this.roundRect1.setTransform(0, 17);

        this.instance = new lib.p135_1();
        this.instance.setTransform(82, 201, 0.47, 0.47);

        this.text_h1 = new cjs.Text("Rösta på ditt favoritdjur", "15px 'MyriadPro-Semibold'");
        this.text_h1.lineHeight = 19;
        this.text_h1.setTransform(173, 39);

        this.text_y1 = new cjs.Text("Antal röster", "12px 'Myriad Pro'");
        this.text_y1.lineHeight = 19;
        this.text_y1.setTransform(17, 45);

        this.text_x1 = new cjs.Text("Djur", "12px 'Myriad Pro'");
        this.text_x1.lineHeight = 19;
        this.text_x1.setTransform(447, 190);

        this.text_1 = new cjs.Text("Polly", "14px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(95, 253);
        this.text_2 = new cjs.Text("Kitty", "14px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(169, 253);
        this.text_3 = new cjs.Text("Molly", "14px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(239, 253);
        this.text_4 = new cjs.Text("Skutt", "14px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(312, 253);
        this.text_5 = new cjs.Text("Gnagy", "14px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(380, 253);

        var ToBeAdded = [];
        for (var num = 0; num < 11; num++) {
            var xPos = 69;
            var temptext = new cjs.Text("" + num, "12px 'Myriad Pro'");
            temptext.lineHeight = -1;
            if (num == 10) {
                xPos = 62;
                num = 10.15;
            }
            temptext.setTransform(xPos, 194 + (-13.3 * num));
            ToBeAdded.push(temptext);
        }

        this.shape_arrow1 = new cjs.Shape();
        this.shape_arrow1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow1.setTransform(85.8, 52, 1, 1, 175);

        this.shape_arrow2 = new cjs.Shape();
        this.shape_arrow2.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow2.setTransform(438, 199.8, 1, 1, 265);

        this.Line_bg1 = new cjs.Shape();
        this.Line_bg1.graphics.f("#ffffff").beginStroke("#ffffff").setStrokeStyle(0.7).moveTo(86, 65).lineTo(86, 200).lineTo(414, 200).lineTo(414, 65).lineTo(86, 65);
        this.Line_bg1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(86, 55).lineTo(86, 200).moveTo(80.5, 200).lineTo(436, 200)
            .moveTo(96, 65).lineTo(96, 200).moveTo(120, 65).lineTo(120, 200)
            .moveTo(168, 65).lineTo(168, 200).moveTo(192, 65).lineTo(192, 200)
            .moveTo(243.6, 65).lineTo(243.6, 200).moveTo(267.6, 65).lineTo(267.6, 200).moveTo(316, 65).lineTo(316, 200).moveTo(340, 65).lineTo(340, 200).moveTo(390, 65).lineTo(390, 200).moveTo(414, 65).lineTo(414, 200);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#949599").setStrokeStyle(0.7).moveTo(80.5, 186.5).lineTo(414, 186.5).moveTo(80.5, 173).lineTo(414, 173).moveTo(80.5, 159.5).lineTo(414, 159.5).moveTo(80.5, 146).lineTo(414, 146).moveTo(80.5, 119).lineTo(414, 119).moveTo(80.5, 105.5).lineTo(414, 105.5).moveTo(80.5, 92).lineTo(414, 92).moveTo(80.5, 78.5).lineTo(414, 78.5);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(80.5, 65).lineTo(422, 65).moveTo(80.5, 132.5).lineTo(422, 132.5);
        this.Line_3.setTransform(0, 0);

        this.Line_col_1 = [];
        for (var i = 0; i < 10; i++) {
            var Line = new cjs.Shape();
            Line.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7).moveTo(97, 199.6).lineTo(97, 199.6 - (i * 13.4)).lineTo(120, 199.6 - (i * 13.4)).lineTo(120, 199.6).lineTo(97, 199.6);
            Line.setTransform(0, 0);
            Line.visible = false;
            this.Line_col_1.push(Line);
        }

        this.Line_col_2 = [];
        for (var i = 0; i < 7; i++) {
            var Line = new cjs.Shape();
            Line.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7).moveTo(168.5, 199.6).lineTo(168.5, 199.6 - (i * 13.4)).lineTo(192, 199.6 - (i * 13.4)).lineTo(192, 199.6).lineTo(168.5, 199.6);
            Line.setTransform(0, 0);
            Line.visible = false;
            this.Line_col_2.push(Line);
        }
        this.Line_col_3 = [];
        for (var i = 0; i < 11; i++) {
            var Line = new cjs.Shape();
            Line.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7).moveTo(244.6, 199.6).lineTo(244.6, 199.6 - (i * 13.4)).lineTo(267.6, 199.6 - (i * 13.4)).lineTo(267.6, 199.6).lineTo(244.6, 199.6);
            Line.setTransform(0, 0);
            Line.visible = false;
            this.Line_col_3.push(Line);
        }
        this.Line_col_4 = [];
        for (var i = 0; i < 8; i++) {
            var Line = new cjs.Shape();
            Line.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7).moveTo(317, 199.6).lineTo(317, 199.6 - (i * 13.4)).lineTo(340, 199.6 - (i * 13.4)).lineTo(340, 199.6).lineTo(317, 199.6);
            Line.setTransform(0, 0);
            Line.visible = false;
            this.Line_col_4.push(Line);
        }
        this.Line_col_5 = [];
        for (var i = 0; i < 5; i++) {
            var Line = new cjs.Shape();
            Line.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7).moveTo(391, 199.6).lineTo(391, 199.6 - (i * 13.4)).lineTo(414, 199.6 - (i * 13.4)).lineTo(414, 199.6).lineTo(391, 199.6);
            Line.setTransform(0, 0);
            Line.visible = false;
            this.Line_col_5.push(Line);
        }

        this.addChild(this.roundRect1, this.instance, this.text_h1, this.text_y1, this.text_x1, this.text_1, this.Line_bg1);

        for (var i = 0; i < this.Line_col_1.length; i++) {
            this.addChild(this.Line_col_1[i]);
        }
        for (var i = 0; i < this.Line_col_2.length; i++) {
            this.addChild(this.Line_col_2[i]);
        }
        for (var i = 0; i < this.Line_col_3.length; i++) {
            this.addChild(this.Line_col_3[i]);
        }
        for (var i = 0; i < this.Line_col_4.length; i++) {
            this.addChild(this.Line_col_4[i]);
        }
        for (var i = 0; i < this.Line_col_5.length; i++) {
            this.addChild(this.Line_col_5[i]);
        }

        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_5, this.text_2, this.text_3, this.text_4, this.text_5, this.shape_arrow1, this.shape_arrow2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i])
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 280);

    (lib.Stage0 = function() {
        this.initialize();
        this.image_leo = new lib.c47_ex1_1();
        this.image_leo.setTransform(950, 50);


        this.instrTexts = [];
        this.instrTexts.push('Polly fick 9 röster.');
        this.instrTexts.push('Kitty fick 6 röster.');
        this.instrTexts.push('Molly fick 10 röster.');
        this.instrTexts.push('Skutt fick 7 röster.');
        this.instrTexts.push('Gnagy fick 4 röster.');

        this.instrText1 = new cjs.Text(this.instrTexts[0], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.visible = false;
        this.instrText1.setTransform(1150, 110);

        this.instrText2 = this.instrText1.clone(true);
        this.instrText2.text = this.instrTexts[1];

        this.instrText3 = this.instrText1.clone(true);
        this.instrText3.text = this.instrTexts[2];

        this.instrText4 = this.instrText1.clone(true);
        this.instrText4.text = this.instrTexts[3];

        this.instrText5 = this.instrText1.clone(true);
        this.instrText5.text = this.instrTexts[4];

        this.statistics_diagram = new lib.statistics_diagram();
        this.statistics_diagram.setTransform(-115, -80, 2.1, 2.1);

        this.addChild(this.image_leo, this.instrText1, this.instrText2, this.instrText3, this.instrText4, this.instrText5, this.statistics_diagram);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.image_leo.visible = false;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.image_leo,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 600,
            alphaTimeout: 1400
        });

        this.tweens.push({
            ref: this.stage0.instrText1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        var Line_col_1 = this.stage0.statistics_diagram.Line_col_1;

        for (var i = 0; i < Line_col_1.length; i++) {
            Line_col_1[i].visible = true;
            this.tweens.push({
                ref: Line_col_1[i],
                alphaFrom: 0,
                alphaTo: 1,
                wait: 2500 + (1500 * i),
                alphaTimeout: 1000
            });
        }
        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
        this.stage0.instrText2.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.instrText1,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 300,
            alphaTimeout: 1500
        });

        this.tweens.push({
            ref: this.stage0.instrText2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2300,
            alphaTimeout: 2000
        });

        var Line_col_1 = this.stage0.statistics_diagram.Line_col_1;
        for (var i = 0; i < Line_col_1.length; i++) {
            Line_col_1[i].visible = true;
        }

        var Line_col_2 = this.stage0.statistics_diagram.Line_col_2;
        for (var i = 0; i < Line_col_2.length; i++) {
            Line_col_2[i].visible = true;
            this.tweens.push({
                ref: Line_col_2[i],
                alphaFrom: 0,
                alphaTo: 1,
                wait: 2500 + (1500 * i),
                alphaTimeout: 1000
            });
        }
        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.instrText2,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 300,
            alphaTimeout: 1500
        });

        this.tweens.push({
            ref: this.stage0.instrText3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2300,
            alphaTimeout: 2000
        });

        var Line_col_1 = this.stage0.statistics_diagram.Line_col_1;
        for (var i = 0; i < Line_col_1.length; i++) {
            Line_col_1[i].visible = true;
        }
        var Line_col_2 = this.stage0.statistics_diagram.Line_col_2;
        for (var i = 0; i < Line_col_2.length; i++) {
            Line_col_2[i].visible = true;
        }

        var Line_col_3 = this.stage0.statistics_diagram.Line_col_3;
        for (var i = 0; i < Line_col_3.length; i++) {
            Line_col_3[i].visible = true;
            this.tweens.push({
                ref: Line_col_3[i],
                alphaFrom: 0,
                alphaTo: 1,
                wait: 2500 + (1500 * i),
                alphaTimeout: 1000
            });
        }
        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText3.visible = true;
        this.stage0.instrText4.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.instrText3,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 300,
            alphaTimeout: 1500
        });

        this.tweens.push({
            ref: this.stage0.instrText4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2300,
            alphaTimeout: 2000
        });

        var Line_col_1 = this.stage0.statistics_diagram.Line_col_1;
        for (var i = 0; i < Line_col_1.length; i++) {
            Line_col_1[i].visible = true;
        }
        var Line_col_2 = this.stage0.statistics_diagram.Line_col_2;
        for (var i = 0; i < Line_col_2.length; i++) {
            Line_col_2[i].visible = true;
        }
        var Line_col_3 = this.stage0.statistics_diagram.Line_col_3;
        for (var i = 0; i < Line_col_3.length; i++) {
            Line_col_3[i].visible = true;
        }

        var Line_col_4 = this.stage0.statistics_diagram.Line_col_4;
        for (var i = 0; i < Line_col_4.length; i++) {
            Line_col_4[i].visible = true;
            this.tweens.push({
                ref: Line_col_4[i],
                alphaFrom: 0,
                alphaTo: 1,
                wait: 2500 + (1500 * i),
                alphaTimeout: 1000
            });
        }
        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage6 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText4.visible = true;
        this.stage0.instrText5.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.instrText4,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 300,
            alphaTimeout: 1500
        });

        this.tweens.push({
            ref: this.stage0.instrText5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2300,
            alphaTimeout: 2000
        });

        var Line_col_1 = this.stage0.statistics_diagram.Line_col_1;
        for (var i = 0; i < Line_col_1.length; i++) {
            Line_col_1[i].visible = true;
        }
        var Line_col_2 = this.stage0.statistics_diagram.Line_col_2;
        for (var i = 0; i < Line_col_2.length; i++) {
            Line_col_2[i].visible = true;
        }
        var Line_col_3 = this.stage0.statistics_diagram.Line_col_3;
        for (var i = 0; i < Line_col_3.length; i++) {
            Line_col_3[i].visible = true;
        }
        var Line_col_4 = this.stage0.statistics_diagram.Line_col_4;
        for (var i = 0; i < Line_col_4.length; i++) {
            Line_col_4[i].visible = true;
        }

        var Line_col_5 = this.stage0.statistics_diagram.Line_col_5;
        for (var i = 0; i < Line_col_5.length; i++) {
            Line_col_5[i].visible = true;
            this.tweens.push({
                ref: Line_col_5[i],
                alphaFrom: 0,
                alphaTo: 1,
                wait: 2500 + (1500 * i),
                alphaTimeout: 1000
            });
        }
        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage6 = new lib.Stage6();
        this.stage6.visible = false;
        this.stage6.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
