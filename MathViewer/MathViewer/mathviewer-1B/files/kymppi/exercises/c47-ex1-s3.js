var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: []
    };

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Subtrahera ental", "bold 24px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(100, 25);
        this.text_1 = new cjs.Text("47", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(46, 24);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("48", "16px 'Myriad Pro'");
        this.text_1.textAlign = 'center';
        this.text_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_2.textAlign = 'center';
        this.text_2.setTransform(22, 0);
        this.text_2.visible = false;

        this.text_3 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_3.textAlign = 'center';
        this.text_3.setTransform((22 * 2), 0);
        this.text_3.visible = false;

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.textAlign = 'center';
        this.text_4.setTransform((22 * 3), 0);
        this.text_4.visible = false;

        this.text_5 = new cjs.Text("42", "16px 'Myriad Pro'");
        this.text_5.textAlign = 'center';
        this.text_5.setTransform((22 * 4), 0);
        this.text_5.visible = false;

        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.Stage1_1 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Subtraktion", "40px 'Myriad Pro'")
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(570 + 80, -40);

        this.container1 = new cjs.Container();        

        this.ct1_greenStack = new cjs.Shape(); // green stack
        for (var col = 0; col < 4; col++) {
            for (var row = 0; row < 10; row++) {
                this.ct1_greenStack.graphics.f("#1A8943").s("#000000").ss(1).drawRect(400 + (col * 65), 0 + (row * 30), 30, 30);
            }
        }
        this.ct1_greenStack.setTransform(0, 0);

        this.ct1_yellowCube = new cjs.Shape(); // yellow cube
        for (var col = 0; col < 2; col++) {
            for (var row = 0; row < 4; row++) {
                this.ct1_yellowCube.graphics.f("#FFF679").s("#000000").ss(1).drawRect(750 + (col * 50), 167 + (row * 35), 28, 28);
            }
        }
        this.ct1_yellowCube.setTransform(0, 0);

        this.ct1_Line = new cjs.Shape(); // corss line
        this.ct1_Line.graphics.s("#DB3A58").setStrokeStyle(2).moveTo(745, 199).lineTo(782, 163).moveTo(796, 199).lineTo(833, 163)
            .moveTo(745, 234).lineTo(782, 198).moveTo(796, 234).lineTo(833, 198)
            .moveTo(745, 269).lineTo(782, 233).moveTo(796, 269).lineTo(833, 233);
        this.ct1_Line.setTransform(0, 0);
        this.ct1_Line.visible = false;

        this.container1.addChild(this.ct1_greenStack, this.ct1_yellowCube, this.ct1_Line);

        this.container1.setTransform(0, 25);

        this.answers = new lib.answers();
        this.answers.setTransform(350, 450, 7, 7);

        this.addChild(this.questionText, this.container1, this.answers);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.stage1_1 = new lib.Stage1_1();
        this.stage1_1.container1.visible = true;

        this.addChild(this.stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();

        this.stage1_1 = new lib.Stage1_1();
        this.stage1_1.container1.visible = true;
        this.stage1_1.answers.text_2.visible = true;
        this.stage1_1.answers.text_3.visible = true;
        this.stage1_1.answers.text_4.visible = true;
        this.stage1_1.container1.children[2].visible = true

        this.tweens = [];
        this.tweens.push({
            ref: this.stage1_1.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage1_1.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage1_1.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.stage1_1.container1.children[2],
            alphaFrom: 0,
            alphaTo: 1,
            wait: 3500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();

        this.stage1_1 = new lib.Stage1_1();
        this.stage1_1.container1.visible = true;
        this.stage1_1.answers.text_2.visible = true;
        this.stage1_1.answers.text_3.visible = true;
        this.stage1_1.answers.text_4.visible = true;
        this.stage1_1.answers.text_5.visible = true;
        this.stage1_1.container1.children[2].visible = true

        this.tweens = [];
        this.tweens.push({
            ref: this.stage1_1.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 800,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
