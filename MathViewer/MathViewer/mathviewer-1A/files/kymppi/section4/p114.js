(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p114_1.png",
            id: "p114_1"
        }]
    };

    (lib.p114_1 = function() {
        this.initialize(img.p114_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("114", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(34.5, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(461 + (columnSpace * 33), 32, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Hur mycket kostar leksakerna tillsammans?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 13);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 13);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 258, 130, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(264, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 166);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(264, 166);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 307);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(264, 307);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 448);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(264, 448);

        this.instance = new lib.p114_1();
        this.instance.setTransform(11, 40, 0.47, 0.47);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['133.5','274','415','555'];
        var xPos = 9,
            padding,
            yPos,
            rectWidth = 240,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 12,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 0;
                if (i == 1) {
                    padding = padding + 12;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        // row-1
        this.text_1 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_1.lineHeight = 15;
        this.text_1.setTransform(63, 61);
        this.text_1.skewX=-13;
        this.text_1.skewY=-13;
        this.text_2 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_2.lineHeight = 15;
        this.text_2.setTransform(147, 59);
        this.text_2.skewX=-23;
        this.text_2.skewY=-23;
        this.text_3 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_3.lineHeight = 15;
        this.text_3.setTransform(210, 58);
        this.text_3.skewX=-18;
        this.text_3.skewY=-18;
        this.text_4 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_4.lineHeight = 15;
        this.text_4.setTransform(302, 55.5);
        this.text_4.skewX=24;
        this.text_4.skewY=24;
        this.text_5 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_5.lineHeight = 14;
        this.text_5.setTransform(379, 74);
        this.text_5.skewX=-24;
        this.text_5.skewY=-24;
        this.text_6 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_6.lineHeight = 15;
        this.text_6.setTransform(439, 51);
        this.text_6.skewX=24;
        this.text_6.skewY=24;
        // row-2
        this.text_7 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 15;
        this.text_7.setTransform(29, 188);
        this.text_7.skewX=13;
        this.text_7.skewY=13;
        this.text_8 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_8.lineHeight = 15;
        this.text_8.setTransform(99.5, 211);
        this.text_8.skewX=-9;
        this.text_8.skewY=-9;
        this.text_9 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 15;
        this.text_9.setTransform(211, 198);
        this.text_9.skewX=-18;
        this.text_9.skewY=-18;
        this.text_10 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_10.lineHeight = 15;
        this.text_10.setTransform(320, 202.5);
        this.text_10.skewX=-12;
        this.text_10.skewY=-12;
        this.text_11 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_11.lineHeight = 15;
        this.text_11.setTransform(392, 201);
        this.text_11.skewX=-13;
        this.text_11.skewY=-13;
        this.text_12 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_12.lineHeight = 15;
        this.text_12.setTransform(464, 202);
        this.text_12.skewX=-13;
        this.text_12.skewY=-13;
        // row-3
        this.text_13 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_13.lineHeight = 15;
        this.text_13.setTransform(31, 334);
        this.text_13.skewX=13;
        this.text_13.skewY=13;
        this.text_14 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_14.lineHeight = 15;
        this.text_14.setTransform(120, 339);
        this.text_14.skewX=-17;
        this.text_14.skewY=-17;
        this.text_15 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_15.lineHeight = 15;
        this.text_15.setTransform(206, 339);
        this.text_16 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_16.lineHeight = 15;
        this.text_16.setTransform(312, 333.5);
        this.text_16.skewX=-12;
        this.text_16.skewY=-12;
        this.text_17 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_17.lineHeight = 15;
        this.text_17.setTransform(371, 334);
        this.text_17.skewX=19;
        this.text_17.skewY=19;
        this.text_18 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_18.lineHeight = 15;
        this.text_18.setTransform(465, 353);
        this.text_18.skewX=-13;
        this.text_18.skewY=-13;
        // row-4
        this.text_19 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_19.lineHeight = 15;
        this.text_19.setTransform(47, 490);
        this.text_19.skewX=-25;
        this.text_19.skewY=-25;
        this.text_20 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_20.lineHeight = 15;
        this.text_20.setTransform(107, 478);
        this.text_20.skewX=17;
        this.text_20.skewY=17;
        this.text_21 = new cjs.Text("1 kr", "13px 'Myriad Pro'");
        this.text_21.lineHeight = 15;
        this.text_21.setTransform(204, 492);
        this.text_21.skewX=-9;
        this.text_21.skewY=-9;
        this.text_22 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_22.lineHeight = 15;
        this.text_22.setTransform(288.5, 480);
        this.text_22.skewX=12;
        this.text_22.skewY=12;
        this.text_23 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_23.lineHeight = 15;
        this.text_23.setTransform(391, 485);
        this.text_23.skewX=8;
        this.text_23.skewY=8;
        this.text_24 = new cjs.Text("1 kr", "13px 'Myriad Pro'");
        this.text_24.lineHeight = 15;
        this.text_24.setTransform(460, 488);
        this.text_24.skewX=-24;
        this.text_24.skewY=-24;

        
        this.addChild(this.text_q1, this.text_q2);
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8);
        this.addChild(this.instance,this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);   
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14);
        this.addChild(this.text_15, this.text_16, this.text_17, this.text_18, this.text_19, this.text_20, this.text_21, this.text_22, this.text_23, this.text_24);     
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 550.2);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(297, 259, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
