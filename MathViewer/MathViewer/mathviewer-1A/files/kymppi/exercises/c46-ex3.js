var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };

    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 12", "bold 24px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("46", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Räkna högt: 5, 6 ... 12.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 445);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.number = new cjs.Text("5", "70px 'Myriad Pro'");
        this.number.lineHeight = 19;
        this.number.setTransform(-95, 140);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        this.blueBall = new lib.c2_ex1_1();

        for (var i = 0; i < 5; i++) {
            var xPos = 10;

            var shape = this.blueBall.clone(true);
            shape.setTransform(xPos + (i * 110), 135, 0.7, 0.7);
            this.addChild(shape);
        }

        this.addChild(this.number, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.number = new cjs.Text("5", "70px 'Myriad Pro'");
        this.number.lineHeight = 19;
        this.number.setTransform(-95, 140);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        this.blueBall = new lib.c2_ex1_1();
        var addImage = [];
        for (var i = 0; i < 12; i++) {
            var xPos = 10;
            if (i > 9) {
                xPos = 85;
            } else if (i > 4) {
                xPos = 50;
            } else {
                xPos = 10;
            }

            var shape = this.blueBall.clone(true);
            shape.setTransform(xPos + (i * 110), 135, 0.7, 0.7);
            addImage.push(shape);
        }

        this.addChild(this.number, this.commentTexts);

        this.tweens = [];

        for (var j = 0; j < addImage.length; j++) {
            this.addChild(addImage[j]);
            if (j > 4) {
                this.tweens.push({
                    ref: addImage[j],
                    alphaFrom: 0,
                    alphaTo: 1,
                    wait: 1300 * (j - 4),
                    alphaTimeout: 0
                });

                this.tweens.push({
                    ref: this.number,
                    textRef: this.number,
                    alphaFrom: 1,
                    alphaTo: 1,
                    wait: 1300 * (j - 4),
                    textFrom: "" + (j) + "",
                    textTo: "" + (j + 1) + "",
                    alphaTimeout: 0,
                    override: false
                });
            }
        }

        this.tweens.push({
            ref: this.number,
            textRef: this.number,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 6000,
            textFrom: "5",
            textTo: "5",
            alphaTimeout: 0,
            override: false
        });

        p.tweens = this.tweens;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
