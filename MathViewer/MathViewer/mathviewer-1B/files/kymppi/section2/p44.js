(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p44_1.png",
            id: "p44_1"
        }, {
            src: "images/p44_2.png",
            id: "p44_2"
        }]
    };


    (lib.p44_1 = function() {
        this.initialize(img.p44_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p44_2 = function() {
        this.initialize(img.p44_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 127, 140);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text_4 = new cjs.Text("44", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(39, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.instance = new lib.p44_1();
        this.instance.setTransform(35, 1, 0.465, 0.46);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(453 + (columnSpace * 33), 27, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape_2, this.shape_1, this.shape, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text_2.setTransform(5, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(5, 0);

        this.instance = new lib.p44_2();
        this.instance.setTransform(20, 12, 0.46, 0.46);

        this.roundRect = new cjs.Shape();
        this.roundRect.graphics.f("#ffffff").s('#7D7D7D').drawRoundRect(0, 0, 255, 113, 10);
        this.roundRect.setTransform(0, 11);

        this.roundRect1 = this.roundRect.clone(true);
        this.roundRect1.setTransform(259, 11);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7D7D7D').drawRoundRect(0, 0, 255, 110, 10);
        this.roundRect2.setTransform(0, 132);
        this.roundRect3 = this.roundRect2.clone(true);
        this.roundRect3.setTransform(259, 132);
        //inner red button box
        this.redRountrect = new cjs.Shape();
        this.redRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 10, 80, 65.5, 6);
        this.redRountrect.setTransform(117, 10);

        this.redRountrect1 = this.redRountrect.clone(true);
        this.redRountrect1.setTransform(257, 10);
        // inner yellow button box
        this.yellowRountrect = new cjs.Shape();
        this.yellowRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 32, 55, 25, 6);
        this.yellowRountrect.setTransform(17, 10);

        this.yellowRountrect1 = this.yellowRountrect.clone(true);
        this.yellowRountrect1.setTransform(385, 10);
        //inner Green button box

        this.greenRountrect = new cjs.Shape();
        this.greenRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(10, 30, 122, 49, 6);
        this.greenRountrect.setTransform(20, 113);

        this.greenRountrect1 = this.greenRountrect.clone(true);
        this.greenRountrect1.setTransform(355, 113)
            //inner blue button box
        this.blueRountrect = new cjs.Shape();
        this.blueRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 32, 63, 55, 6);
        this.blueRountrect.setTransform(143, 110);

        this.blueRountrect1 = this.blueRountrect.clone(true);
        this.blueRountrect1.setTransform(250, 110)

        //draw rectangle boxes

        this.textbox_group1 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var row = 0; row < 2; row++) {
            if (row > 1) {
                Xpos = 255;
            } else {
                Xpos = 0;
            }
            for (var col = 0; col < 6; col++) {
                this.textbox_group1.graphics.s("#000000").ss(0.3).drawRect((73 + Xpos) + (col * 20), (92 + Ypos) + (row * 18), 20, 22);
            }
            Ypos = Ypos + 100;
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var row = 0; row < 2; row++) {
            if (row > 1) {
                Xpos = 255;
            } else {
                Xpos = 0;
            }
            for (var col = 0; col < 6; col++) {
                this.textbox_group2.graphics.s("#000000").ss(0.3).drawRect((327 + Xpos) + (col * 20), (92 + Ypos) + (row * 18), 20, 22);
            }
            Ypos = Ypos + 100;
        }
        this.textbox_group1.setTransform(0, 0);


        this.addChild(this.roundRect, this.roundRect1, this.roundRect2, this.roundRect3, this.redRountrect, this.redRountrect1, this.yellowRountrect, this.yellowRountrect1)
        this.addChild(this.greenRountrect, this.greenRountrect1, this.blueRountrect, this.blueRountrect1, this.textbox_group1, this.textbox_group2)
        this.addChild(this.instance_2, this.text, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 6.3);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(5, 6.3);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 17, 512, 145, 10);
        this.roundRect1.setTransform(0, 0);


        this.label1 = new cjs.Text("9  +  3  =", "16px 'Myriad Pro'");
        this.label1.setTransform(35, 46);
        this.label2 = new cjs.Text("3 +  9  =", "16px 'Myriad Pro'");
        this.label2.setTransform(35, 76);
        this.label3 = new cjs.Text("7  +  4  =", "16px 'Myriad Pro'");
        this.label3.setTransform(35, 106);
        this.label4 = new cjs.Text("4  +  7  =", "16px 'Myriad Pro'");
        this.label4.setTransform(35, 134);

        this.label6 = new cjs.Text("5  +  6  =", "16px 'Myriad Pro'");
        this.label6.setTransform(185, 46);
        this.label7 = new cjs.Text("6  +  5  =", "16px 'Myriad Pro'");
        this.label7.setTransform(185, 76);
        this.label8 = new cjs.Text("5  +  9  =", "16px 'Myriad Pro'");
        this.label8.setTransform(185, 106);
        this.label9 = new cjs.Text("9  +  5  =", "16px 'Myriad Pro'");
        this.label9.setTransform(185, 134);

        this.label11 = new cjs.Text("5  +  5  =", "16px 'Myriad Pro'");
        this.label11.setTransform(336, 46);
        this.label12 = new cjs.Text("6  +  6  =", "16px 'Myriad Pro'");
        this.label12.setTransform(336, 76);
        this.label13 = new cjs.Text("7  +  7  =", "16px 'Myriad Pro'");
        this.label13.setTransform(336, 106);
        this.label14 = new cjs.Text("8  +  8  =", "16px 'Myriad Pro'");
        this.label14.setTransform(336, 134);

        var ToBeAdded = [];
        var tmpline = new cjs.Shape();
        var ypos = 30;
        for (var column = 0; column < 3; column++) {
            var colSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (row == 3) {
                    ypos = 29;
                } else {
                    ypos = 30;
                }
                tmpline.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(95 + (152 * colSpace), 52 + (ypos * rowSpace)).lineTo(133 + (152 * colSpace), 52 + (ypos * rowSpace));
                tmpline.setTransform(0, 0);
                ToBeAdded.push(tmpline);
            }
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 522.3, 50);

    // stage content:
    (lib.p72 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(298, 425, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(298, 475, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
