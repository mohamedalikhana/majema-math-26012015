(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p37_1.png",
            id: "p37_1"
        }, {
            src: "images/p37_2.png",
            id: "p37_2"
        }]
    };

    (lib.p37_1 = function() {
        this.initialize(img.p37_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p37_2 = function() {
        this.initialize(img.p37_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.pageNoRight = new cjs.Text("37", "12px 'Myriad Pro'", "#FFFFFF");
        this.pageNoRight.lineHeight = 18;
        this.pageNoRight.setTransform(555, 648);

        this.pageNoRightBackground = new cjs.Shape();
        this.pageNoRightBackground.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.pageNoRightBackground.setTransform(579, 660.8);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med addition 0 till 6", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(380, 651);

        this.pageTitle = new cjs.Text("Vi lär oss addition", "24px 'MyriadPro-Semibold'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(106, 26);

        this.chapterNumber = new cjs.Text("12", "28px 'MyriadPro-Semibold", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(56, 22.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(47.6, 23.5, 1.1, 1);

        this.addChild(this.pageNoRightBackground, this.pageNoRight, this.pageTitle, this.pageBottomText, this.chapterNumberBackground, this.chapterNumber);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p37_1();
        this.instance.setTransform(0, 20, 0.6, 0.6);
        this.text = new cjs.Text("Hur många? ", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 2);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 2);

        this.textBoxGroup1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            var space = 140;
            if (columnSpace > 1) {
                columnSpace = columnSpace - 0.2;
            }
            for (var row = 0; row < 2; row++) {
                this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(40 + (columnSpace * space), 32 + (row * 140), 20, 21);
                //console.log(150 + (columnSpace * 150), 40 + (row * 30))
            }
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.label1 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label1.lineHeight = 27;
        this.label1.setTransform(76, 134);
        this.label2 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label2.lineHeight = 27;
        this.label2.setTransform(76, 275);

        this.label3 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label3.lineHeight = 27;
        this.label3.setTransform(330, 134);
        this.label4 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label4.lineHeight = 27;
        this.label4.setTransform(332, 275);

        this.label5 = new cjs.Text("2", "33px 'UusiTekstausMajema'", "#6C6D70");
        this.label5.lineHeight = 36;
        this.label5.setTransform(43, 23);
        this.label6 = new cjs.Text("1", "33px 'UusiTekstausMajema'", "#6C6D70");
        this.label6.lineHeight = 36;
        this.label6.setTransform(183, 23);
        this.label7 = new cjs.Text("3", "33px 'UusiTekstausMajema'", "#6C6D70");
        this.label7.lineHeight = 36;
        this.label7.setTransform(167, 122);
        this.textBoxGroup2 = new cjs.Shape();
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(165, 130, 20, 21).drawRect(165, 270, 20, 21).drawRect(420, 130, 20, 21).drawRect(420, 270, 20, 21);
        this.textBoxGroup2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((867 * 0.5) - 3) * 0.59, ((467 * 0.5) - 10) * 0.59, 10);
        this.roundRect1.setTransform(3, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((868 * 0.5) + 5) * 0.6, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 165);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((868 * 0.5) + 5) * 0.6, 165);



        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.text, this.text_1, this.textBoxGroup1, this.textBoxGroup2, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 300);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p37_2();
        this.instance.setTransform(0, 0, 0.6, 0.6);
        this.textBoxGroup1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            var space = 140;
            if (columnSpace > 1) {
                columnSpace = columnSpace - 0.2;
            }
            for (var row = 0; row < 2; row++) {
                this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(40 + (columnSpace * space), 13 + (row * 140), 20, 21);
                //console.log(150 + (columnSpace * 150), 40 + (row * 30))
            }
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.label1 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label1.lineHeight = 27;
        this.label1.setTransform(77, 114);
        this.label2 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label2.lineHeight = 27;
        this.label2.setTransform(77, 255);

        this.label3 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label3.lineHeight = 27;
        this.label3.setTransform(333, 114);
        this.label4 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label4.lineHeight = 27;
        this.label4.setTransform(334, 255);

        this.textBoxGroup2 = new cjs.Shape();
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(165, 110, 20, 21).drawRect(165, 250, 20, 21).drawRect(420, 110, 20, 21).drawRect(422, 250, 20, 21);
        this.textBoxGroup2.setTransform(0, 0);
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((867 * 0.5) - 3) * 0.59, ((467 * 0.5) - 10) * 0.59, 10);
        this.roundRect1.setTransform(3, 4);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((868 * 0.5) + 5) * 0.6, 4);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 144);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((868 * 0.5) + 5) * 0.6, 144);
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.textBoxGroup1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.textBoxGroup2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(308.3, 393.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(308.3, 108, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
