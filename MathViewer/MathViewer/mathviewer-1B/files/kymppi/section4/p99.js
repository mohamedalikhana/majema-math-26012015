(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("kunna beskriva egenskaper hos trianglar, fyrhörningar och cirklar", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(292, 659);

        this.text_1 = new cjs.Text("99", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_2 = new cjs.Text("Trianglar, fyrhörningar och cirklar", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_2.setTransform(110, 42);

        this.text_3 = new cjs.Text("34", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(55, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(49, 23.5, 1.15, 1);

        this.addChild(this.shape_2, this.pageBottomText, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.whiteblock10tags = function() {
        this.initialize();

        // Main white block left
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(0, 0, 165, 150, 5);
        this.roundRect2.setTransform(22, 20);

        // for left rect

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_1.setTransform(36, 21);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_2.setTransform(51, 21);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_3.setTransform(66.5, 21);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_4.setTransform(82, 21);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_5.setTransform(97.5, 21);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_6.setTransform(113, 21);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_7.setTransform(128.5, 21);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_8.setTransform(144, 21);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_9.setTransform(159.5, 21);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_28.setTransform(175, 21);

        //tags

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_10.setTransform(37, 26.5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_11.setTransform(37, 26.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_12.setTransform(52, 26.5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_13.setTransform(52, 26.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_14.setTransform(68, 26.5);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_15.setTransform(68, 26.5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_16.setTransform(83.5, 26.5);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_17.setTransform(83.5, 26.5);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_18.setTransform(99, 26.5);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_19.setTransform(99, 26.5);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_20.setTransform(114, 26.5);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_21.setTransform(114, 26.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_22.setTransform(130, 26.5);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_23.setTransform(130, 26.5);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_24.setTransform(145, 26.5);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_25.setTransform(145, 26.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_26.setTransform(161, 26.5);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_27.setTransform(161, 26.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_30.setTransform(177, 26.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_31.setTransform(177, 26.5);


        this.addChild(this.roundRect2,
            this.shape_10, this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_19, this.shape_20,
            this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.shape_25, this.shape_26, this.shape_27, this.shape_30, this.shape_31,
            this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_28);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 537, 143.6);

    (lib.whiteblock7tags = function() {
        this.initialize();

        // Main white block left
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(0, 0, 118, 150, 5);
        this.roundRect2.setTransform(22, 20);

        // for left rect

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_1.setTransform(34, 21);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_2.setTransform(49, 21);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_3.setTransform(64.5, 21);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_4.setTransform(80, 21);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_5.setTransform(95.5, 21);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_6.setTransform(111, 21);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_7.setTransform(126.5, 21);

        //tags

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_10.setTransform(35, 26.5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_11.setTransform(35, 26.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_12.setTransform(50, 26.5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_13.setTransform(50, 26.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_14.setTransform(66, 26.5);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_15.setTransform(66, 26.5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_16.setTransform(81.5, 26.5);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_17.setTransform(81.5, 26.5);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_18.setTransform(97, 26.5);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_19.setTransform(97, 26.5);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_20.setTransform(112, 26.5);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_21.setTransform(112, 26.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_22.setTransform(128, 26.5);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_23.setTransform(128, 26.5);

        this.addChild(this.roundRect2,
            this.shape_10, this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_19, this.shape_20,
            this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.shape_25, this.shape_26, this.shape_27, this.shape_30, this.shape_31,
            this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_28);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 537, 143.6);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Trianglar", "bold 16px 'Myriad Pro'", "#000000");
        this.text.setTransform(68, 48);

        this.text_1 = new cjs.Text("Fyrhörningar", "bold 16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(235, 48);

        this.text_2 = new cjs.Text("Cirklar", "bold 16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(407, 48);

        this.text_3 = new cjs.Text("3 sidor", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(30, 68);

        this.text_4 = new cjs.Text("3 hörn", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(30, 88);

        this.text_5 = new cjs.Text("hörn", "14px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(42, 112);

        this.text_6 = new cjs.Text("sida", "14px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(150, 101);

        this.text_7 = new cjs.Text("4 sidor", "16px 'Myriad Pro'", "#000000");
        this.text_7.setTransform(210, 68);

        this.text_8 = new cjs.Text("4 hörn", "16px 'Myriad Pro'", "#000000");
        this.text_8.setTransform(210, 88);

        this.arrow_1 = new cjs.Shape();
        this.arrow_1.graphics.f('').s("#000000").ss(1.2).moveTo(10, 31).lineTo(10, 45);
        this.arrow_1.setTransform(49, 84);

        this.arrow_2 = new cjs.Shape();
        this.arrow_2.graphics.f('#000000').s("#000000").ss(1.2).moveTo(5, 43).lineTo(15, 43).lineTo(10, 55).lineTo(5, 43);
        this.arrow_2.setTransform(55.3, 107, 0.4, 0.4);

        this.arrow_3 = new cjs.Shape();
        this.arrow_3.graphics.f('#000000').s("#000000").ss(1.2).moveTo(18, 37).lineTo(10.5, 48);
        this.arrow_3.setTransform(148, 65);

        this.arrow_4 = new cjs.Shape();
        this.arrow_4.graphics.f('#000000').s("#000000").ss(1.2).moveTo(7, 44).lineTo(18, 52).lineTo(4, 62).lineTo(5, 44);
        this.arrow_4.setTransform(155.3, 98, 0.3, 0.3);     

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#13A538').s("#000000").ss(1.2).moveTo(24, 52).lineTo(54, 52).lineTo(39, 25).lineTo(24, 52);
        this.shape_1.setTransform(20, 107);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#13A538').s("#000000").ss(1.2).moveTo(24, 52).lineTo(55, 52).lineTo(12, 17).lineTo(24, 52);
        this.shape_2.setTransform(75, 107);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#13A538').s("#000000").ss(1.2).moveTo(10, 20).lineTo(10, 44).lineTo(59, 44).lineTo(10, 20);
        this.shape_3.setTransform(110, 80);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#13A538').s("#000000").ss(1.2).moveTo(0, 52).lineTo(40, 52).lineTo(40, 19).lineTo(0, 30).lineTo(0, 52);
        this.shape_4.setTransform(214, 85);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#13A538').s("#000000").ss(1.2).drawRoundRect(0, 0, 24, 24, 0);
        this.shape_5.setTransform(272, 110);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#13A538').s("#000000").ss(1.2).drawRoundRect(0, 0, 55, 24, 0);
        this.shape_6.setTransform(265, 140);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#13A538").s("#000000").ss(1.2).moveTo(0, 52).lineTo(27, 67).lineTo(27, 38).lineTo(0, 24).lineTo(0, 52);
        this.shape_7.setTransform(319, 80);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#13A538").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 25, 0, 2 * Math.PI).f("#000000").s("").arc(0, 0, 1, 0, 2 * Math.PI);
        this.shape_8.setTransform(425, 95);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#13A538").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 17, 0, 2 * Math.PI).f("#000000").s("").arc(0, 0, 1, 0, 2 * Math.PI);
        this.shape_9.setTransform(465, 137);

        // Main yellow block
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 0, 520, 178, 15);
        this.roundRect1.setTransform(0, 1);

        this.whiteblock = new lib.whiteblock10tags();
        this.whiteblock.setTransform(0, 0);

        this.whiteblock_1 = this.whiteblock.clone(true);
        this.whiteblock_1.setTransform(178, 0);

        this.whiteblock_2 = new lib.whiteblock7tags();
        this.whiteblock_2.setTransform(356, 0);

        this.addChild(this.roundRect1, this.whiteblock, this.whiteblock_1, this.whiteblock_2, this.text, this.text_1, this.text_2,
            this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.shape_1, this.shape_2, this.shape_3,
            this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.arrow_1, this.arrow_2, this.arrow_3, this.arrow_4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);


    (lib.dotMatrixRect = function() {
        this.initialize();

        var width = 130,
            height = 130;
        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#D6326A").ss(0.8).drawRect(0, 0, width, height);
        this.rect1.setTransform(0, 0);

        var ToBeAdded = [];
        var colSpace = 23;
        var rowSpace = 23;
        var startX = 7;
        var startY = 7;
        var dotCount = 0;
        for (var col = 0; col < 6; col++) {

            for (var row = 0; row < 6; row++) {

                var dot = null;
                dot = new cjs.Shape();
                dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 1.25);
                dot.setTransform(startX + (col * colSpace), startY + (row * rowSpace));
                dot.id = dotCount;
                dotCount = dotCount + 1;

                ToBeAdded.push(dot);
            }
        }

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);

        }
        this.dots = ToBeAdded;
        this.addChild(this.rect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    //Part 1
    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(1, 7);

        this.text_1 = new cjs.Text(" Rita 3 olika", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 7);

        this.text_2 = new cjs.Text("trianglar.", "bold 16px 'Myriad Pro'");
        this.text_2.setTransform(93, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 520, 158, 10);
        this.roundRect1.setTransform(0, 20);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(21, 37);

        this.rectimg_1 = this.rectimg.clone(true);
        this.rectimg_1.setTransform(190, 37);

        this.rectimg_2 = this.rectimg.clone(true);
        this.rectimg_2.setTransform(360, 37);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.rectimg, this.rectimg_1, this.rectimg_2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(1, 7);

        this.text_1 = new cjs.Text(" Rita 3 olika", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 7);

        this.text_2 = new cjs.Text("fyrhörningar.", "bold 16px 'Myriad Pro'");
        this.text_2.setTransform(93, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 520, 158, 10);
        this.roundRect1.setTransform(0, 20);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(21, 37);

        this.rectimg_1 = this.rectimg.clone(true);
        this.rectimg_1.setTransform(190, 37);

        this.rectimg_2 = this.rectimg.clone(true);
        this.rectimg_2.setTransform(360, 37);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[13].x, this.rectimg.dots[13].y).lineTo(this.rectimg.dots[25].x, this.rectimg.dots[25].y).lineTo(this.rectimg.dots[28].x, this.rectimg.dots[28].y);
        this.rectimg.addChild(this.Line_1);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[12].x, this.rectimg.dots[12].y).lineTo(this.rectimg.dots[2].x, this.rectimg.dots[2].y).lineTo(this.rectimg.dots[16].x, this.rectimg.dots[16].y);
        this.rectimg_1.addChild(this.Line_2);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[8].x, this.rectimg.dots[8].y).lineTo(this.rectimg.dots[16].x, this.rectimg.dots[16].y).lineTo(this.rectimg.dots[34].x, this.rectimg.dots[34].y);
        this.rectimg_2.addChild(this.Line_3);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.rectimg, this.rectimg_1, this.rectimg_2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(305, 108, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305, 309, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(305, 512, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
