(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p14_1.png",
            id: "p14_1"
        }, {
            src: "images/p14_2.png",
            id: "p14_2"
        }]
    };

    (lib.p14_1 = function() {
        this.initialize(img.p14_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p14_2 = function() {
        this.initialize(img.p14_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {

        this.text = new cjs.Text("14", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(39, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);


        this.addChild(this.shape, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p14_1();
        this.instance.setTransform(0, 45, 0.47, 0.47);

        this.text = new cjs.Text("Hur många kronor är det?", "16px 'Myriad Pro'");
        this.text.setTransform(21, 40);

        this.text_1 = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(2, 40);

        this.text_2 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#9D9C9C");
        this.text_2.setTransform(144, 132);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(5, 25, 168, 99, 10);
        this.roundRect1.setTransform(0, 27);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#7d7d7d').drawRoundRect(5, 86, 168, 99, 10);
        this.roundRect2.setTransform(0, 70);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("").s('#7d7d7d').drawRoundRect(5, 140, 168, 99, 10);
        this.roundRect3.setTransform(0, 120);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("").s('#7d7d7d').drawRoundRect(178, 26, 168, 100, 10);
        this.roundRect4.setTransform(0, 25);

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("").s('#7d7d7d').drawRoundRect(178, 130, 168, 100, 10);
        this.roundRect5.setTransform(0, 25);

        this.roundRect6 = new cjs.Shape();
        this.roundRect6.graphics.f("").s('#7d7d7d').drawRoundRect(178, 235, 168, 100, 10);
        this.roundRect6.setTransform(0, 25);

        this.roundRect7 = new cjs.Shape();
        this.roundRect7.graphics.f("").s('#7d7d7d').drawRoundRect(353, 26, 168, 99, 10);
        this.roundRect7.setTransform(0, 25);

        this.roundRect8 = new cjs.Shape();
        this.roundRect8.graphics.f("").s('#7d7d7d').drawRoundRect(353, 130, 168, 100, 10);
        this.roundRect8.setTransform(0, 25);

        this.roundRect9 = new cjs.Shape();
        this.roundRect9.graphics.f("").s('#7d7d7d').drawRoundRect(353, 235, 168, 100, 10);
        this.roundRect9.setTransform(0, 25);


        var lineArr = [];
        var lineX = [91, 269, 443, 91, 269, 443, 91, 269, 443 ];
        var lineY = [94, 94, 94, 201, 201, 201, 308, 308, 308];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(65, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }


        this.addChild(this.text, this.text_1, this.text_2, this.instance, this.roundRect1, this.roundRect2, this.roundRect3,
            this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.roundRect9);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }




    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 330);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p14_2();
        this.instance.setTransform(0, 27, 0.47, 0.47);

        this.text = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.setTransform(23, 16);

        this.text_1 = new cjs.Text("7.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(4, 16);

        
        //outer rect

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(7, 5, 250, 210, 10);
        this.roundRect1.setTransform(0, 25);

       
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#7d7d7d').drawRoundRect(262, 5, 260, 210, 10);
        this.roundRect2.setTransform(0, 25);

      
        //inner green rect

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("").s('#90BD22').drawRoundRect(27, 16, 210, 119, 10);
        this.roundRect3.setTransform(0, 25);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("").s('#90BD22').drawRoundRect(282, 16, 219, 119, 10);
        this.roundRect4.setTransform(0, 25);

       
        this.text_2 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_2.setTransform(70, 63);

        this.text_3 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_3.setTransform(120, 63);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.setTransform(172, 63);

        this.text_5 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_5.setTransform(190, 63);

        this.text_6 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_6.setTransform(70, 90);


        this.text_7 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_7.setTransform(120, 90);
       

        this.text_8 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_8.setTransform(172, 90);
        
        this.text_9 = new cjs.Text("15", "16px 'Myriad Pro'");
        this.text_9.setTransform(190, 90);
      

        this.text_10 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_10.setTransform(120, 115);
       
        this.text_11 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_11.setTransform(172, 115);
     
        this.text_12 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.text_12.setTransform(45, 140);
       
        this.text_13 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_13.setTransform(70, 140);
      
        this.text_14 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_14.setTransform(120, 140);
       
        this.text_15 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_15.setTransform(172, 140);
      
        this.text_16 = new cjs.Text("20", "16px 'Myriad Pro'");
        this.text_16.setTransform(355,63);
      
        this.text_17 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_17.setTransform(380, 63);
       
        this.text_18 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_18.setTransform(426, 63);
       


        this.text_19 = new cjs.Text("15", "16px 'Myriad Pro'");
        this.text_19.setTransform(310,89);
      
        this.text_20 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_20.setTransform(335, 89);

        this.text_21 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_21.setTransform(380, 89);
       
        this.text_22 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_22.setTransform(426, 89);


        this.text_23 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_23.setTransform(335,115);
      
        this.text_24 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_24.setTransform(380, 115);

        this.text_25 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_25.setTransform(426, 115);
       
        this.text_26 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_26.setTransform(450, 115);


        this.text_27 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_27.setTransform(335,140);
      
        this.text_28 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_28.setTransform(380, 140);

        this.text_29 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_29.setTransform(426, 140);
       
        this.text_30 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_30.setTransform(450, 140);



        this.text_31 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_31.setTransform(65, 187);

        this.text_32 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_32.setTransform(158,187);

        this.text_33 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_33.setTransform(314, 187);

        this.text_34 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_34.setTransform(423, 187);

        this.text_35 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_35.setTransform(65, 219);

        this.text_36 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_36.setTransform(158, 219);

        this.text_37 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_37.setTransform(314, 219);

        this.text_38 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_38.setTransform(423, 219);




        
        var lineArr = [];
        var lineX = [70, 164, 319, 429, 70, 164, 319, 429 ];
        var lineY = [150, 150, 150, 150, 182, 182, 182, 182];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(43, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }


            this.addChild(this.text, this.text_1, this.text_2, this.instance, this.roundRect1, this.roundRect2, this.roundRect3,
            this.roundRect4, this.text_3, this.text_4,
            this.text_5, this.text_6, this.text_7,this.text_8,this.text_9,this.text_10,
            this.text_11,this.text_12,this.text_13,this.text_14,this.text_15,this.text_16,this.text_17,this.text_18,
            this.text_19,this.text_20,this.text_21,this.text_22,this.text_23,this.text_24,this.text_25,this.text_26,
            this.text_27,this.text_28,this.text_29,this.text_30,this.text_31,this.text_32,this.text_33,this.text_34,
            this.text_35,this.text_36,this.text_37,this.text_38);

       for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

      this.addChild(this.text_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 280);


    (lib.p6 = function() {
        this.initialize();


        this.v2 = new lib.Symbol3();
        this.v2.setTransform(290, 435, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290, 85, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
