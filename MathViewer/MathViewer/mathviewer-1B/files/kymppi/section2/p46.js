(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p46_1.png",
            id: "p46_1"
        }, {
            src: "images/p46_2.png",
            id: "p46_2"
        }]
    };

    (lib.p46_1 = function() {
        this.initialize(img.p46_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p46_2 = function() {
        this.initialize(img.p46_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Symmetri", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(232, 77);

        this.text_1 = new cjs.Text("15", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(42, 42);

        this.text_3 = new cjs.Text("kunna rita ut symmetrilinjer", "9px 'Myriad Pro'", "#FAAA33");
        this.text_3.setTransform(86, 657);

        this.text_4 = new cjs.Text("46", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(38.5, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 140, 12, 20);
        this.text_Rect.setTransform(0, 0);



        this.addChild(this.shape_1, this.text5, this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.instance = new lib.p46_1();
        this.instance.setTransform(0, 0, 0.46, 0.46);
        //Harizontal dashed lines 1
        var ToBeAdded = [];
        for (var col = 0; col < 18; col++) {
            var colSpace = col;
            var Line_1 = new cjs.Shape();
            Line_1.graphics.beginStroke("#706F6F").setStrokeStyle(0.7).moveTo(207, 63).lineTo(211, 63);
            Line_1.setTransform(52 + (colSpace * 7), 0);
            ToBeAdded.push(Line_1);
        }
        //Harizontal dashed lines 2
        for (var col = 0; col < 14; col++) {
            var colSpace = col;
            var Line_2 = new cjs.Shape();
            Line_2.graphics.beginStroke("#706F6F").setStrokeStyle(0.7).moveTo(207, 63).lineTo(211, 63);
            Line_2.setTransform(192 + (colSpace * 7), 0);
            ToBeAdded.push(Line_2);
        }

        // Three vertical dashed lines
        var numberOfLines = ['51', '184', '437.5'];
        var ypos = 8;
        for (var indexLine = 0; indexLine < numberOfLines.length; indexLine++) {
            for (var col = 0; col < 15; col++) {
                var colSpace = col;
                if (indexLine > 0) {
                    ypos = 0;
                } else {
                    ypos = 8;
                }
                if (indexLine == 0 && col >13) {
                    continue;
                } else {
                    var Line_3 = new cjs.Shape();
                    Line_3.graphics.beginStroke("#706F6F").setStrokeStyle(0.7).moveTo(10, ypos).lineTo(10, ypos + (4));
                    Line_3.setTransform(numberOfLines[indexLine], 9 + (colSpace * 7));
                    ToBeAdded.push(Line_3);
                }
            }
        }

        this.addChild(this.instance);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 120);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Rita symmetrilinjer.", "16px 'Myriad Pro'");
        this.text_1.setTransform(1, 7.6);

        this.instance = new lib.p46_2();
        this.instance.setTransform(0, 25, 0.46, 0.46);

        this.addChild(
            this.instance, this.text_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 395);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(302, 270, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(302, 410, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
