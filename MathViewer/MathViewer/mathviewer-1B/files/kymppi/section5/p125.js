(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p125_1.png",
            id: "p125_1"
        }]
    };

    (lib.p125_1 = function() {
        this.initialize(img.p125_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("125", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(15, 10);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.home = function(homeText) {
        this.initialize();

        var textPosition = {
            x1: 46,
            y1: 51,
            underlineX: 13,
            underlineY: 52,
            underlineX1: 47,
            underlineY1: 52,
            underlineX2: 82,
            underlineY2: 52
        };;

        this.homeImage = new lib.p125_1();
        this.homeImage.setTransform(0, 0, 0.47, 0.47);

        this.txt_group = new cjs.Container();

        this.text_2 = new cjs.Text(homeText, "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_2.setTransform(textPosition.x1, textPosition.y1);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(29, 0);
        this.hrLine_1.setTransform(textPosition.underlineX, textPosition.underlineY);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(29, 0);
        this.hrLine_2.setTransform(textPosition.underlineX1, textPosition.underlineY1);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(29, 0);
        this.hrLine_3.setTransform(textPosition.underlineX2, textPosition.underlineY2);

        this.txt_group.addChild(this.text_2, this.hrLine_1, this.hrLine_2, this.hrLine_3);

        this.txt_group.setTransform(0, 0);

        this.addChild(this.homeImage, this.txt_group);

        //home ends
    }).prototype = p = new cjs.Container();

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text("  Skriv talen som kommer före och efter.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 265, 10);
        this.roundRect1.setTransform(0, 12);

        var TArr = ['10', '40', '77', '20', '55', '89', '31', '66', '90', '39',
            '70', '99'
        ];
        var startX = 20,
            startY = 20;
        for (var col = 0; col < 3; col++) {
            for (var row = 0; row < 4; row++) {
                var i = row + col;
                if (row == 0) {
                    i = row + col;
                } else if (row == 1) {
                    i = row + (col + 2);
                } else if (row == 2) {
                    i = row + (col + 4);
                } else {
                    i = row + (col + 6)
                }

                var home = new lib.home(TArr[i]);
                home.setTransform(startX + col * 170, startY + row * 63)
                this.addChild(home);
            };

        };

        this.addChild(this.text, this.text_1, this.roundRect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 525.3, 275);

    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 2   

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text("  Skriv talen som saknas i delen från hundrarutan.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 259, 10);
        this.roundRect1.setTransform(0, 12);        

        var crosswordBox_group = new cjs.Container();
      
        var specialTexts = [];
        specialTexts[0] = [31, 41, ' ', ' ', 33, 43];
        specialTexts[2] = [' ', ' ', 39, 49, ' ', ' '];
        specialTexts[4] = [' ', ' ', 62, 72, ' ', ' '];
        specialTexts[6] = [' ', ' ', ' ', ' ', 80, 90];
        specialTexts[1] = [67, ' ', ' ', ' ', 69, 79];
        specialTexts[3] = [' ', ' ', ' ', 93, 84, ' '];
        specialTexts[5] = [85, ' ', ' ', ' ', ' ', 97];
        specialTexts[7] = [' ', 98, 89, ' ', ' ', ' '];
        var count = 0;
        var specialCount = 0;
        for (var column = 0; column < 4; column++) {
            for (var row = 0; row < 2; row++) {
               
                var fillColor = '#ffff00';
                var fillText = specialTexts[count];              
                var rectContainer = new lib.FillBoxWithText(fillText, fillColor)
                rectContainer.setTransform(125 * column, 70 * row);
                crosswordBox_group.addChild(rectContainer);
                 count = count + 1;
            }
        }


        var crossRectBox_group = new cjs.Container();      
        var crossRectBoxText = [];
        crossRectBoxText[0] = [' ',' ',' ',' ',29,' ',' ',30,' '];
     
        crossRectBoxText[1] = [' ',' ',' ',32,42, ' ', ' ', ' ', ' ', ' '];       
        crossRectBoxText[2] =  [' ',' ',' ',' ',89, 99, ' ', ' ', ' ', ' ']; 
        crossRectBoxText[3] = [' ',' ',' ',55, ' ', ' ', ' ',66, ' ',' '];      
        var Boxcount = 0;
        var specialCount = 0;
        for (var col = 0; col < 4; col++) {
            for (var rows = 0; rows < 1; rows++) {
               
                var BoxfillColor = '#ffff00';
                var BoxfillText = crossRectBoxText[Boxcount];              
                var rectBoxContainer = new lib.FillBoxWithTextcrossRect(BoxfillText, BoxfillColor)
                rectBoxContainer.setTransform(125 * col, 70 * rows);
                crossRectBox_group.addChild(rectBoxContainer);
                Boxcount = Boxcount + 1;
            }
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.rectangleBox_group2, this.rectangleBox_group3,
            this.rectangleBox_group4, this.rectangleBox_group5, this.rectangleBox_group6, this.rectangleBox_group7, this.rectangleBox_group8,
            this.crossrectangleBox_group2, this.crossrectangleBox_group3, this.crossrectangleBox_group4,
            crosswordBox_group, crossRectBox_group);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 525.3, 275);


    (lib.FillBoxWithTextcrossRect = function(fillText, fillColor, Textposition) {
        this.initialize();

        var textPosition = {
            x1: 15,
            y1: 40,
            x2: 56,
            y2: 42,
            x3: 86,
            y3: 72,
        };

        if (Textposition == 'left') {
            textPosition = {
                x1: 40,
                y1: 40,
                x2: 15,
                y2: 42,
                underlineX: 15,
                underlineY: 42
            };
        }


       
        var crossrectangleBox_group = new cjs.Container();

        this.horizontalRect = new cjs.Shape();
        this.horizontalRect.graphics.f("").s('#00A3C4').ss(1).drawRect(0, 0, 102, 76/3);
        this.horizontalRect.setTransform(0, 25.33);

        this.verticalRect = new cjs.Shape();
        this.verticalRect.graphics.f("").s('#00A3C4').ss(1).drawRect(0, 0, 102/3, 76);
        this.verticalRect.setTransform(34, 0);

        crossrectangleBox_group.addChild(this.horizontalRect, this.verticalRect);
        crossrectangleBox_group.setTransform(18, 170);

        var count = 0;
        for (var column = 0; column < 3; column++) {
            for (var row = 0; row < 3; row++) {
                var txt_group = new cjs.Container();
                if (fillText[count] == ' ') {
                    fillColor = '#ffffff';
                } else {
                    fillColor = '#EBF2DA';
                }
                var rect = new cjs.Shape();
                rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, (102/3)-1, (76/3)-1);
                var text = new cjs.Text('' + fillText[count], "16px 'Myriad Pro'", "#000000");
                text.setTransform(33 / 2, 18);
                text.textAlign = 'center';
                txt_group.addChild(rect, text);
                txt_group.setTransform(column * 102 / 3, row * 76/3)
                crossrectangleBox_group.addChild(txt_group);
                count = count + 1;

            }
        }

        this.addChild(crossrectangleBox_group, this.txt_group);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.FillBoxWithText = function(fillText, fillColor, Textposition) {
        this.initialize();

        var textPosition = {
            x1: 15,
            y1: 40,
            x2: 56,
            y2: 42,
            x3: 86,
            y3: 72,
        };

        if (Textposition == 'left') {
            textPosition = {
                x1: 40,
                y1: 40,
                x2: 15,
                y2: 42,
                underlineX: 15,
                underlineY: 42
            };
        }


        var rectangleBox_group = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#00A3C4').ss(1).drawRect(0, 0, 102, 50);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.f("#00A3C4").s("#00A3C4").setStrokeStyle(0.7).moveTo(0, 0).lineTo(103, 0);
        this.hrLine_1.setTransform(0, 25);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.f("#00A3C4").s("#00A3C4").setStrokeStyle(0.7).moveTo(0, 0).lineTo(0, 51);
        this.vrLine_1.setTransform(34, 0);

        this.vrLine_2 = new cjs.Shape();
        this.vrLine_2.graphics.f("#00A3C4").s("#00A3C4").setStrokeStyle(0.7).moveTo(0, 0).lineTo(0, 51);
        this.vrLine_2.setTransform(68, 0);

        rectangleBox_group.addChild(this.Rect1, this.hrLine_1, this.vrLine_1, this.vrLine_2);
        rectangleBox_group.setTransform(18, 32);

        var count = 0;
        for (var column = 0; column < 3; column++) {
            for (var row = 0; row < 2; row++) {
                var txt_group = new cjs.Container();
                if (fillText[count] === ' ') {
                    fillColor = '#ffffff';
                } else {
                    fillColor = '#EBF2DA';
                }
                var rect = new cjs.Shape();
                rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, 102 / 3 - 1, 50 / 2 - 1);
                var text = new cjs.Text('' + fillText[count], "16px 'Myriad Pro'", "#000000");
                text.setTransform(33 / 2, 18)
                text.textAlign = 'center';
                txt_group.addChild(rect, text);
                txt_group.setTransform(column * 102 / 3, row * 50 / 2)
                rectangleBox_group.addChild(txt_group);
                count = count + 1;

            }
        }

        this.addChild(rectangleBox_group, this.txt_group);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 107, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 405, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
