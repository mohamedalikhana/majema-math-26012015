(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p100_1.png",
            id: "p100_1"
        }, {
            src: "images/p100_2.png",
            id: "p100_2"
        }]
    };

    (lib.p100_1 = function() {
        this.initialize(img.p100_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p100_2 = function() {
        this.initialize(img.p100_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("100", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(30, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28, 660.8);

        this.instance = new lib.p100_1();
        this.instance.setTransform(34, 9, 0.802, 0.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(457 + (columnSpace * 33), 26, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Det ska vara 10 pinnar. Hur många saknas?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 14);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 14);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 259, 95, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(264, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 125);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(264, 125);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 225);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(264, 225);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 325);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(264, 325);

        this.instance = new lib.p100_2();
        this.instance.setTransform(37, 47, 0.4, 0.4);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['103', '203', '303', '403'];
        var xPos = 88,
            padding,
            yPos,
            rectWidth = 21.5,
            rectHeight = 23,
            boxWidth = 21.5,
            boxHeight = 23,
            numberOfBoxes = 2,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 25;
                if (i == 1) {
                    padding = padding + 111.5;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var ToBeAdded = [];
        var arryPos = ['109', '209', '309', '409'];
        var arrCol1text = ['8  +', '7  +', '4  +', '3  +'];
        var arrCol2text = ['6  +', '5  +', '2  +', '1  +'];
        for (var i = 0; i < arryPos.length; i++) {
            var yPos = parseInt(arryPos[i]);
            var col1text=arrCol1text[i];
            var col2text=arrCol2text[i];

            var temp_text1 = new cjs.Text("=  10", "16px 'Myriad Pro'");
            temp_text1.lineHeight = -1;
            temp_text1.setTransform(143, yPos);
            var temp_text2 = new cjs.Text("=  10", "16px 'Myriad Pro'");
            temp_text2.lineHeight = -1;
            temp_text2.setTransform(143 + 268, yPos);

            var temp_text3 = new cjs.Text(col1text, "16px 'Myriad Pro'");
            temp_text3.lineHeight = -1;
            temp_text3.setTransform(81, yPos);

            var temp_text4 = new cjs.Text(col2text, "16px 'Myriad Pro'");
            temp_text4.lineHeight = -1;
            temp_text4.setTransform(81 + 268, yPos);
            ToBeAdded.push(temp_text1, temp_text2,temp_text3, temp_text4);
        }

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance, this.textbox_group1, this.text_q1, this.text_q2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296, 419, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
