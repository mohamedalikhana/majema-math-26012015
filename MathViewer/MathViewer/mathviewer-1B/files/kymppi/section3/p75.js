(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p75_1.png",
            id: "p75_1"
        }, {
            src: "images/p74_2.png",
            id: "p74_2"
        }, {
            src: "images/p74_3.png",
            id: "p74_3"
        }]
    };

    (lib.p75_1 = function() {
        this.initialize(img.p75_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p74_2 = function() {
        this.initialize(img.p74_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p74_3 = function() {
        this.initialize(img.p74_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("75", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(555, 658);


        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);


        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 20).lineTo(0, 20).moveTo(590, 30).lineTo(590, 660).moveTo(590, 660).lineTo(0, 660);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(580, 30, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(590, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.instance_2 = new lib.p74_2();
        this.instance_2.setTransform(360, 35, 0.46, 0.46);

        this.addChild(this.instance_2, this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 400);


    (lib.Symbol2 = function() {
        this.initialize();
        this.text_1 = new cjs.Text("Nu ska 3 barn dela lika på pengarna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(60, 27);

        this.text_2 = new cjs.Text("Hur mycket får de var? Rita och skriv.", "16px 'Myriad Pro'");
        this.text_2.setTransform(60, 50);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(0, 0, 510, 450, 10);
        this.round_Rect1.setTransform(15, 87);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(22, 130).lineTo(518, 130)
        this.line1.setTransform(0, 0);

        this.line2 = new cjs.Shape();
        this.line2.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(185, 98).lineTo(185, 528)
        this.line2.setTransform(0, 0);

        this.line3 = new cjs.Shape();
        this.line3.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(360, 98).lineTo(360, 528)
        this.line3.setTransform(0, 0);

        this.text_3 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(147, 510);
        this.text_4 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.setTransform(319, 510);
        this.text_5 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.setTransform(484, 510);

        this.instance = new lib.p75_1();
        this.instance.setTransform(98, 62, 0.46, 0.46);

        this.instance_3 = new lib.p74_3();
        this.instance_3.setTransform(12, 0, 0.46, 0.46);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.7).moveTo(85, 510).lineTo(145, 510)
            .moveTo(258, 510).lineTo(318, 510)
            .moveTo(423,510).lineTo(483,510)
        this.hrLine_1.setTransform(0, 0);

        this.text_6 = new cjs.Text("Visa och berätta för en kamrat.", "16px 'Myriad Pro'");    
        this.text_6.setTransform(15,554);

        this.addChild(this.round_Rect1, this.text_1, this.text_2,this.text_3, this.text_4,this.hrLine_1, this.line1, this.line2,this.line3);
        this.addChild(this.text_5,this.text_6, this.instance_3,this.instance)
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0,  620, 505);





    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290, 250, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
