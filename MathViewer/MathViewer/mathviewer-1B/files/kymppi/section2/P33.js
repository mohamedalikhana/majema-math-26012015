(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p33_1.png",
            id: "p33_1"
        }, {
            src: "images/p33_2.png",
            id: "p33_2"
        }, {
            src: "images/p33_3.png",
            id: "p33_3"
        }, {
            src: "images/p33_4.png",
            id: "p33_4"
        }]
    };

    (lib.p33_1 = function() {
        this.initialize(img.p33_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p33_2 = function() {
        this.initialize(img.p33_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p33_3 = function() {
        this.initialize(img.p33_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

     (lib.p33_4 = function() {
        this.initialize(img.p33_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("33", "12px 'Myriad Pro'", "#FFFFFF");      
        this.text_4.setTransform(552, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("I en skattkista är det 4 färre         än      .", "16px 'Myriad Pro'");
        this.text_1.setTransform(60, 18);

        this.text_2 = new cjs.Text("Rita och skriv olika lösningar.", "16px 'Myriad Pro'");       
        this.text_2.setTransform(60, 41);

        this.instance = new lib.p33_2();
        this.instance.setTransform(8, -11, 0.62, 0.62);

        this.instance_2 = new lib.p33_3();
        this.instance_2.setTransform(243, -5, 0.38, 0.38);

        this.instance_3 = new lib.p33_4();
        this.instance_3.setTransform(285, -5, 0.38, 0.38);

        this.instance_4 = new lib.p33_1();
        this.instance_4.setTransform(18, 65, 0.47, 0.47);

        this.instance_5 = new lib.p33_1();
        this.instance_5.setTransform(280, 65, 0.47, 0.47);        
        var ToBeAdded = [];
         this.hrLine_1 = new cjs.Shape();
        for (var colmn=0; colmn < 4;  colmn++) { 
            var tempText = new cjs.Text("st", "16px 'Myriad Pro'");
            tempText.setTransform(100+(colmn*127) , 277);         
            this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(40 + (127 * colmn), 277 ).lineTo(100 + (126 * colmn), 277 );
            this.hrLine_1.setTransform(0, 0);
            ToBeAdded.push(tempText, this.hrLine_1);
        }

        this.instance_6 = new lib.p33_3();
        this.instance_6.setTransform(14, 243, 0.47, 0.47);
        
        this.instance_7 = new lib.p33_4();
        this.instance_7.setTransform(140, 246, 0.47, 0.47);

        this.instance_8 = new lib.p33_3();
        this.instance_8.setTransform(265, 243, 0.47, 0.47);

        this.instance_9 = new lib.p33_4();
        this.instance_9.setTransform(395, 246, 0.47, 0.47);

        this.addChild(this.text_1, this.text_2, this.instance,this.instance_2,this.instance_3,this.instance_4,this.instance_5);
        this.addChild(this.instance_6,this.instance_7,this.instance_8,this.instance_9,this.hrLine_1);

         for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 305);

    (lib.Symbol2 = function() {
        this.initialize();

     

        this.instance_4 = new lib.p33_1();
        this.instance_4.setTransform(20, 0, 0.46, 0.47);

        this.instance_5 = new lib.p33_1();
        this.instance_5.setTransform(285, 0, 0.46, 0.47);        
        var ToBeAdded = [];
         this.hrLine_1 = new cjs.Shape();
        for (var colmn=0; colmn < 4;  colmn++) { 
            var tempText = new cjs.Text("st", "16px 'Myriad Pro'");
            tempText.setTransform(100+(colmn*127) , 210);         
            this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(45 + (127 * colmn), 210 ).lineTo(100 + (127 * colmn), 210 );
            this.hrLine_1.setTransform(0, 0);
            ToBeAdded.push(tempText, this.hrLine_1);
        }

        this.instance_6 = new lib.p33_3();
        this.instance_6.setTransform(14, 180, 0.47, 0.47);
        
        this.instance_7 = new lib.p33_4();
        this.instance_7.setTransform(143, 181, 0.47, 0.47);

        this.instance_8 = new lib.p33_3();
        this.instance_8.setTransform(268, 180, 0.47, 0.47);

        this.instance_9 = new lib.p33_4();
        this.instance_9.setTransform(398, 180, 0.47, 0.47);

        this.addChild(this.instance_4,this.instance_5);
        this.addChild(this.instance_6,this.instance_7,this.instance_8,this.instance_9,this.hrLine_1);

         for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

        this.text_1 = new cjs.Text("Visa och berätta för en kamrat.", "16px 'Myriad Pro'");    
        this.text_1.setTransform(8, 240);

        this.addChild( this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 260);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310, 265, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(309, 563, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
