(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("97", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(554, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(15, 20);

        this.addChild(this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.drawScale = function(scaleLength, paddingLeft, paddingRight, cropRight) {
        this.initialize();
        var scaleHeight = 126.8;

        var corners = {
            TL: 10,
            TR: 10,
            BR: 10,
            BL: 10
        };

        if (cropRight) {
            corners.TR = 0;
            corners.BR = 0;
            paddingRight = 10;
        }

        this.metricscale = new cjs.Shape();
        this.metricscale.graphics.f("#E2F3FD").s("#959C9D").ss(1).drawRoundRectComplex(0, 0, paddingLeft + (scaleLength * 100) + paddingRight, scaleHeight, corners.TL, corners.TR, corners.BR, corners.BL);
        if (cropRight) {
            this.metricscale.graphics.s('#ffffff').drawRect(paddingLeft + (scaleLength * 100) + paddingRight - 1, 1, 1 * 2, scaleHeight - 1 * 2);
        }
        this.metricscale.setTransform(0, 0);
        this.addChild(this.metricscale);

        for (var i = 0; i <= scaleLength * 10; i++) {
            var height = 31.7;
            if (i % 5 == 0) {
                height = 63.4;
            }

            var round = new cjs.Shape();
            round.graphics.f("#000000").s('#7d7d7d').drawRect(0, 0, 1, height);
            round.setTransform(25 + i * 10, 0);
            this.addChild(round, text);
            if (i % 10 == 0) {
                var ispace = 16;
                var number = i / 10;
                if (number > 9) {
                    ispace = 8;
                }
                var text = new cjs.Text(number + "", "36px 'Myriad Pro'", "#000000");
                text.setTransform(ispace + i * 10, height + (height * 50 / 100));
                this.addChild(text);
            }
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 320.3, 143.6);

    (lib.hrdotline = function(dotcount) {
        this.initialize();


        this.dotline = new cjs.Shape();
        for (var i = 0; i < dotcount; i++) {

            this.dotline.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0 + (i * 8), 5).lineTo(5 + (i * 8), 5);

        }
        this.dotline.setTransform(2.5, 0);

        var hrLine_1 = new cjs.Shape();
        hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(0, 10);
        hrLine_1.setTransform(0, 0);

        this.addChild(this.dotline, hrLine_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Rita sträckan.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 549, 10);
        this.roundRect1.setTransform(10, 22);

        this.dotline = new lib.hrdotline(8);
        this.dotline.setTransform(90, 42);

        this.dotline1 = this.dotline.clone(true);
        this.dotline1.setTransform(90, 134);

        this.dotline2 = this.dotline.clone(true);
        this.dotline2.setTransform(90, 183);

        this.dotline3 = this.dotline.clone(true);
        this.dotline3.setTransform(90, 232);

        this.dotline4 = this.dotline.clone(true);
        this.dotline4.setTransform(90, 279);

        this.dotline5 = this.dotline.clone(true);
        this.dotline5.setTransform(90, 326);

        this.dotline6 = this.dotline.clone(true);
        this.dotline6.setTransform(90, 373);

        this.dotline7 = this.dotline.clone(true);
        this.dotline7.setTransform(90, 422);

        this.dotline8 = this.dotline.clone(true);
        this.dotline8.setTransform(90, 471);

        this.dotline9 = this.dotline.clone(true);
        this.dotline9.setTransform(90, 520);

        var TextArr = [];
        var TArr = ['6 cm', '9 cm', '11 cm', '13 cm', '7 cm', '12 cm', '8 cm', '10 cm', '3 cm', '5 cm'];
        var TextX = [25, 25, 25, 25, 25, 25, 25, 25, 25, 25];
        var TextY = [52, 144, 193, 242, 289, 336, 383, 432, 481, 530];

        for (var i = 0; i < TextX.length; i++) {

            var text_1 = new cjs.Text(TArr[i], "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[i], TextY[i]);
            TextArr.push(text_1);
        }

        var scaleLength = 12.5;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = false;
        var bigScale1 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale1.setTransform(83, 57, 0.284, 0.36);

        this.addChild(this.text, this.text_1, this.roundRect1, this.dotline, this.dotline1, this.dotline2, this.dotline3, this.dotline4, this.dotline5,
            this.dotline6, this.dotline7, this.dotline8, this.dotline9, bigScale1);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 572);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(304, 103, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
