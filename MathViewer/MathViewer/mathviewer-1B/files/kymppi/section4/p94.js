(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p94_1.png",
            id: "p94_1"
        }]
    };

    (lib.p94_1 = function() {
        this.initialize(img.p94_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("94", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(3, 20);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   
        // 
        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 5);

        this.text_1 = new cjs.Text(" Bind ihop talen 1 till 13 med hjälp av en linjal..", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 5);

        this.text_2 = new cjs.Text(" Mät varje sträcka och skriv längden.", "16px 'Myriad Pro'");
        this.text_2.setTransform(24, 27);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 530, 10);
        this.roundRect1.setTransform(10, 40);

        this.instance = new lib.p94_1();
        this.instance.setTransform(17, 139, 0.47, 0.47);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#FFFFFF").s('#D6326A').ss(1).drawRoundRect(0, 0, 153, 367, 0);
        this.Rect1.setTransform(338, 64);

        this.text_3 = new cjs.Text("Sträcka", "16px 'Myriad Pro'");
        this.text_3.setTransform(344, 83);

        this.text_4 = new cjs.Text("Längd", "16px 'Myriad Pro'");
        this.text_4.setTransform(426, 83);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#D6326A").setStrokeStyle(1).moveTo(40, 0).lineTo(40, 367);
        this.vrLine_1.setTransform(379, 64);

        var hrlineArr = [];
        var hrlineX = [338, 338, 338, 338, 338, 338, 338, 338, 338, 338, 338, 338];
        var hrlineY = [73.2, 101.4, 129.6, 157.8, 186, 214.2, 242.4, 270.6, 298.8, 327, 355.2, 383.4];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#D6326A").setStrokeStyle(0.5).moveTo(0, 20).lineTo(153, 20);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        var DotArr = [];
        var DotX = [186, 174, 195, 148.5, 237, 207.5, 167, 281, 66.6, 167, 208, 30.5, 330];
        var DotY = [86, 115, 115, 125.5, 153.5, 216, 228.5, 261, 282.5, 404, 400, 444.5, 473];
        5
        for (var row = 0; row < DotX.length; row++) {

            var text_3 = new cjs.Text(".", "30px 'Myriad Pro'", "#000000");
            text_3.setTransform(DotX[row], DotY[row]);
            DotArr.push(text_3);
        }

        var NumArr = [];
        var NArr = [7, 5, 9, 6, 8, 11, 3, 10, 4, 1, 13, 2, 12];
        var NumX = [190, 174.2, 195.5, 140, 243, 203, 171, 290, 57, 163, 205, 28, 339];
        var NumY = [80, 108, 108, 130, 156, 210, 238, 261, 286, 397, 393, 438, 475];
        5
        for (var row = 0; row < NumX.length; row++) {

            var text_4 = new cjs.Text(NArr[row], "10px 'Myriad Pro'", "#000000");
            text_4.setTransform(NumX[row], NumY[row]);
            NumArr.push(text_4);
        }    

        var RectArr = [];
        var RectX = [345, 345, 345, 345, 345, 345, 345, 345, 345, 345, 345, 345];
        var RectY = [110, 140, 170, 198, 226, 254, 284, 309, 339, 367, 395, 423];
        for (var row = 0; row < RectX.length; row++) {

            var text_5 = new cjs.Text(row + 1 + "       " + (row + 2), "16px 'Myriad Pro'", "#000000");
            text_5.setTransform(RectX[row], RectY[row]);
            RectArr.push(text_5);         
        }

        var RectLineArr = [];
        var RectLineX = [357, 357, 357, 357, 357, 357, 357, 357, 358, 365, 365, 365];
        var RectLineY = [86, 116, 146, 174, 202, 230, 260, 285, 315, 343, 371, 399];
        for (var row = 0; row < RectLineX.length; row++) {

            var Line_arrow = new cjs.Shape();
            Line_arrow.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(0, 20).lineTo(15, 20).lineTo(11, 16).moveTo(15, 20).lineTo(11, 24);
            Line_arrow.setTransform(RectLineX[row], RectLineY[row]);
            RectLineArr.push(Line_arrow);
        }

        var lineArr = [];
        var lineX = [426, 426, 426, 426, 426, 426, 426, 426, 426, 426, 426, 426];
        var lineY = [91, 121, 151, 179, 207, 235, 265, 290, 320, 348, 376, 404];

        for (var row = 0; row < lineX.length; row++) {
            var hvrLine_1 = new cjs.Shape();
            hvrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(26, 20);
            hvrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hvrLine_1);
        }

        var TextArr = [];
        var TextX = [456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456];
        var TextY = [110, 140, 170, 198, 226, 254, 284, 309, 339, 367, 395, 423];
        5
        for (var row = 0; row < TextX.length; row++) {

            var text_4 = new cjs.Text("cm", "16px 'Myriad Pro'", "#000000");
            text_4.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_4);
        }

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.instance, this.Rect1, this.text_3, this.text_4, this.vrLine_1);

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < DotArr.length; i++) {
            this.addChild(DotArr[i]);
        }
        for (var i = 0; i < NumArr.length; i++) {
            this.addChild(NumArr[i]);
        }

        for (var i = 0; i < RectArr.length; i++) {
            this.addChild(RectArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < RectLineArr.length; i++) {
            this.addChild(RectLineArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 570);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290, 107, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
