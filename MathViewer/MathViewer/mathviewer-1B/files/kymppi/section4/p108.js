(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("108", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(443 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 5);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.dotMatrixRect = function() {
        this.initialize();

        var width = 120,
            height = 239;
        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#D6326A").ss(1).drawRect(0, 0, width, height);
        this.rect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#D6326A").setStrokeStyle(1).moveTo(0, height / 2).lineTo(width, height / 2);
        this.hrLine_1.setTransform(0, 0);

        var ToBeAdded = [];
        var colSpace = 17.02;
        var rowSpace = 16.85;
        var startX = 8;
        var startY = 11;
        var dotCount = 0;
        for (var col = 0; col < 7; col++) {

            for (var row = 0; row < 14; row++) {

                var dot = null;
                dot = new cjs.Shape();
                dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 1.25);
                dot.setTransform(startX + (col * colSpace), startY + (row * rowSpace));
                dot.id = dotCount;
                dotCount = dotCount + 1;

                ToBeAdded.push(dot);
            }
        }

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);

        }
        this.dots = ToBeAdded;
        this.addChild(this.rect1, this.hrLine_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("Kluring", "18px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(1, 13);

        this.text_1 = new cjs.Text(" Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 48);

        this.text_2 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_2.setTransform(3, 48);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 295, 10);
        this.roundRect1.setTransform(0, 25);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(60, 65);

        this.rectimg_1 = new lib.dotMatrixRect();
        this.rectimg_1.setTransform(334, 65);

        // rectimg_1 shape --- cockroach ---

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[1].x, this.rectimg_1.dots[1].y).lineTo(this.rectimg_1.dots[15].x, this.rectimg_1.dots[15].y).lineTo(this.rectimg_1.dots[14].x, this.rectimg_1.dots[14].y);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[15].x, this.rectimg_1.dots[15].y).lineTo(this.rectimg_1.dots[16].x, this.rectimg_1.dots[16].y).lineTo(this.rectimg_1.dots[30].x, this.rectimg_1.dots[30].y).lineTo(this.rectimg_1.dots[29].x, this.rectimg_1.dots[29].y).lineTo(this.rectimg_1.dots[15].x, this.rectimg_1.dots[15].y);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[16].x, this.rectimg_1.dots[16].y).lineTo(this.rectimg_1.dots[31].x, this.rectimg_1.dots[31].y).lineTo(this.rectimg_1.dots[44].x, this.rectimg_1.dots[44].y).lineTo(this.rectimg_1.dots[29].x, this.rectimg_1.dots[29].y);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[31].x, this.rectimg_1.dots[31].y).lineTo(this.rectimg_1.dots[46].x, this.rectimg_1.dots[46].y).lineTo(this.rectimg_1.dots[59].x, this.rectimg_1.dots[59].y).lineTo(this.rectimg_1.dots[44].x, this.rectimg_1.dots[44].y).lineTo(this.rectimg_1.dots[31].x, this.rectimg_1.dots[31].y);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[46].x, this.rectimg_1.dots[46].y).lineTo(this.rectimg_1.dots[61].x, this.rectimg_1.dots[61].y).lineTo(this.rectimg_1.dots[75].x, this.rectimg_1.dots[75].y).lineTo(this.rectimg_1.dots[74].x, this.rectimg_1.dots[74].y).lineTo(this.rectimg_1.dots[59].x, this.rectimg_1.dots[59].y).lineTo(this.rectimg_1.dots[46].x, this.rectimg_1.dots[46].y);

        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(this.rectimg_1.dots[3].x, this.rectimg_1.dots[3].y).lineTo(this.rectimg_1.dots[2].x, this.rectimg_1.dots[2].y).lineTo(this.rectimg_1.dots[31].x, this.rectimg_1.dots[31].y)
            .moveTo(this.rectimg_1.dots[18].x, this.rectimg_1.dots[18].y).lineTo(this.rectimg_1.dots[17].x, this.rectimg_1.dots[17].y).lineTo(this.rectimg_1.dots[46].x, this.rectimg_1.dots[46].y)
            .moveTo(this.rectimg_1.dots[46].x, this.rectimg_1.dots[46].y).lineTo(this.rectimg_1.dots[47].x, this.rectimg_1.dots[47].y).lineTo(this.rectimg_1.dots[62].x, this.rectimg_1.dots[62].y)
            .moveTo(this.rectimg_1.dots[42].x, this.rectimg_1.dots[42].y).lineTo(this.rectimg_1.dots[28].x, this.rectimg_1.dots[28].y).lineTo(this.rectimg_1.dots[44].x, this.rectimg_1.dots[44].y)
            .moveTo(this.rectimg_1.dots[57].x, this.rectimg_1.dots[57].y).lineTo(this.rectimg_1.dots[43].x, this.rectimg_1.dots[43].y).lineTo(this.rectimg_1.dots[59].x, this.rectimg_1.dots[59].y)
            .moveTo(this.rectimg_1.dots[59].x, this.rectimg_1.dots[59].y).lineTo(this.rectimg_1.dots[73].x, this.rectimg_1.dots[73].y).lineTo(this.rectimg_1.dots[88].x, this.rectimg_1.dots[88].y);

        this.dot = new cjs.Shape();
        this.dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 1.25);
        this.dot.setTransform(this.rectimg_1.dots[45].x, this.rectimg_1.dots[45].y);

        this.rectimg_1.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.dot);

        // rectimg shape  --square --

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.f('#008BD2').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[15].x, this.rectimg.dots[15].y).lineTo(this.rectimg.dots[19].x, this.rectimg.dots[19].y).lineTo(this.rectimg.dots[75].x, this.rectimg.dots[75].y).lineTo(this.rectimg.dots[71].x, this.rectimg.dots[71].y).lineTo(this.rectimg.dots[15].x, this.rectimg.dots[15].y);

        this.Line_8 = new cjs.Shape();
        this.Line_8.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[31].x, this.rectimg.dots[31].y).lineTo(this.rectimg.dots[46].x, this.rectimg.dots[46].y).lineTo(this.rectimg.dots[59].x, this.rectimg.dots[59].y).lineTo(this.rectimg.dots[44].x, this.rectimg_1.dots[44].y).lineTo(this.rectimg.dots[31].x, this.rectimg_1.dots[31].y);

        this.Line_9 = new cjs.Shape();
        this.Line_9.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[0].x, this.rectimg.dots[0].y).lineTo(this.rectimg.dots[16].x, this.rectimg.dots[16].y).lineTo(this.rectimg.dots[3].x, this.rectimg.dots[3].y).lineTo(this.rectimg.dots[18].x, this.rectimg.dots[18].y).lineTo(this.rectimg.dots[6].x, this.rectimg.dots[6].y)
            .lineTo(this.rectimg.dots[33].x, this.rectimg.dots[33].y).lineTo(this.rectimg.dots[48].x, this.rectimg.dots[48].y).lineTo(this.rectimg.dots[61].x, this.rectimg.dots[61].y).lineTo(this.rectimg.dots[90].x, this.rectimg.dots[90].y).lineTo(this.rectimg.dots[74].x, this.rectimg.dots[74].y).lineTo(this.rectimg.dots[87].x, this.rectimg.dots[87].y)
            .lineTo(this.rectimg.dots[72].x, this.rectimg.dots[72].y).lineTo(this.rectimg.dots[84].x, this.rectimg.dots[84].y).lineTo(this.rectimg.dots[57].x, this.rectimg.dots[57].y).lineTo(this.rectimg.dots[42].x, this.rectimg.dots[42].y).lineTo(this.rectimg.dots[29].x, this.rectimg.dots[29].y).lineTo(this.rectimg.dots[0].x, this.rectimg.dots[0].y);
        this.dot_1 = new cjs.Shape();
        this.dot_1.graphics.f('#878787').ss().s().drawCircle(0, 0, 1.25);
        this.dot_1.setTransform(this.rectimg.dots[45].x, this.rectimg.dots[45].y);

        this.rectimg.addChild(this.Line_9, this.Line_7, this.Line_8, this.dot_1);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.rectimg, this.rectimg_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 350);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   
        // 
        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(3, 48);

        this.text_1 = new cjs.Text(" Hitta figurerna inuti rutan. Måla.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 48);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 263, 10);
        this.roundRect1.setTransform(0, 25);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#000000").ss(1).drawRect(0, 0, 112, 112);
        this.rect1.setTransform(34, 150);

        this.rect2 = new cjs.Shape();
        this.rect2.graphics.f('').s("#000000").ss(1).drawRect(0, 0, 112, 112);
        this.rect2.setTransform(197, 150);

        this.rect3 = new cjs.Shape();
        this.rect3.graphics.f('').s("#000000").ss(1).drawRect(0, 0, 112, 112);
        this.rect3.setTransform(357, 150);

        this.LShape = new cjs.Shape();
        this.LShape.graphics.f("#3AAA35").beginStroke("#3AAA35").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 45).lineTo(67, 45).lineTo(67, 21).lineTo(21, 21).lineTo(21, 0).lineTo(0, 0);
        this.LShape.setTransform(135, 75);

        this.TShape = new cjs.Shape();
        this.TShape.graphics.f("#009FE3").beginStroke("#009FE3").setStrokeStyle(1).moveTo(0, 0).lineTo(67, 0).lineTo(67, 20).lineTo(44, 20).lineTo(44, 50)
            .lineTo(22, 50).lineTo(22, 20).lineTo(0, 20).lineTo(0, 0);
        this.TShape.setTransform(300, 75);

        // for square-1

        this.Line1 = new cjs.Shape();
        this.Line1.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 112);
        this.Line1.setTransform(105, 150);

        this.Line2 = new cjs.Shape();
        this.Line2.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(88, 0).lineTo(88, 22).lineTo(112, 22);
        this.Line2.setTransform(34, 220);

        this.Line3 = new cjs.Shape();
        this.Line3.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 47).lineTo(-24, 47).lineTo(-24, 92).lineTo(0, 92).lineTo(0, 112);
        this.Line3.setTransform(82, 150);

        this.Line4 = new cjs.Shape();
        this.Line4.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(-20, 0).lineTo(-20, 26).lineTo(-40, 26);
        this.Line4.setTransform(146, 170);

        // for square-2

        this.Line5 = new cjs.Shape();
        this.Line5.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(112, 0);
        this.Line5.setTransform(197, 220);

        this.Line6 = new cjs.Shape();
        this.Line6.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 20).lineTo(-25, 20).lineTo(-25, 92).lineTo(0, 92).lineTo(0, 112);
        this.Line6.setTransform(250, 150);

        this.Line7 = new cjs.Shape();
        this.Line7.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 20).lineTo(25, 20).lineTo(25, 45).lineTo(45, 45).lineTo(45, 92).lineTo(25, 92).lineTo(25, 112);
        this.Line7.setTransform(250, 150);

        // for square-3

        this.Line8 = new cjs.Shape();
        this.Line8.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 20).lineTo(40, 20).lineTo(40, 112)
        .moveTo(20,20).lineTo(20,92).lineTo(40,92);
        this.Line8.setTransform(357, 150);

        this.Line9 = new cjs.Shape();
        this.Line9.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 20).lineTo(25, 20).lineTo(25, 92).lineTo(0, 92).lineTo(0, 112)
        .moveTo(48,45).lineTo(25,45);
        this.Line9.setTransform(420, 150);

        this.Line10 = new cjs.Shape();
        this.Line10.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(40, 45).lineTo(65, 45).lineTo(65, 70).lineTo(88, 70);
        this.Line10.setTransform(357, 150);
 
        this.addChild(this.text, this.text_1, this.roundRect1, this.rect1, this.rect2, this.rect3, this.LShape, this.TShape,
            this.Line1, this.Line2, this.Line3, this.Line4, this.Line5, this.Line6, this.Line7, this.Line8, this.Line9, this.Line10);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 350);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290, 85, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(290, 387, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
