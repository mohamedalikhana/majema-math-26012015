(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p109_1.png",
            id: "p109_1"
        }, {
            src: "images/p109_2.png",
            id: "p109_2"
        }]
    };

    (lib.p109_1 = function() {
        this.initialize(img.p109_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p109_2 = function() {
        this.initialize(img.p109_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("109", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(552, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol6 = function() {
        this.initialize();

        this.instance = new lib.p109_1();
        this.instance.setTransform(431, -6, 0.375, 0.375);

        this.instance_2 = new lib.p109_2();
        this.instance_2.setTransform(15, 186, 0.62, 0.61);

        this.text = new cjs.Text("Hjärtspelet", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(-6, -12);

        this.text_2 = new cjs.Text("Spel för 2.", "16px 'Myriad Pro'", "#F1662B");
        this.text_2.lineHeight = 20;
        this.text_2.setTransform(-6, 10);

        this.text_1 = new cjs.Text("• Slå 2 tärningar.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(5, 48);

        this.text_3 = new cjs.Text("• Talen i varje hjärta ska vara 10 tillsammans.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(5, 70);

        this.text_4 = new cjs.Text("• Välj det ena tärningstalet eller summan av båda och skriv talet i hjärtat.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(5, 92);

        this.text_5 = new cjs.Text("• Den som först har skrivit tal i alla hjärtan vinner.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(5, 114);

        this.text_6 = new cjs.Text("Spelare 1", "bold 16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(16, 156);

        this.text_7 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(341, 3);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 105, 510, 208, 10);
        this.roundRect1.setTransform(5, 35);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#F1652B').drawRoundRect(327, -10, 197, 38, 2);
        this.roundRect2.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 4) {
                    columnSpace = 4.07;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(61 + (columnSpace * 100), 199 + (row * 82.5), 18, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("9  +", "16.3px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(29, 203);
        this.label2 = new cjs.Text("8  +", "16.3px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(129, 203);
        this.label3 = new cjs.Text("7  +", "16.3px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(229, 203);
        this.label4 = new cjs.Text("6  +", "16.3px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(329, 203);
        this.label5 = new cjs.Text("5  +", "16.3px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(436, 203);

        this.label6 = new cjs.Text("5  +", "16.3px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(29, 286);
        this.label7 = new cjs.Text("4  +", "16.3px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(129, 286);
        this.label8 = new cjs.Text("3  +", "16.3px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(229, 286);
        this.label9 = new cjs.Text("2  +", "16.3px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(329, 286);
        this.label10 = new cjs.Text("1  +", "16.3px 'Myriad Pro'");
        this.label10.lineHeight = 30;
        this.label10.setTransform(436, 286);

        // Block 2
        this.instance_B = new lib.Symbol3();
        this.instance_B.setTransform(0, 358);  
        // outer thin grey line  
        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(-5, 37, 530, 539, 13);
        this.roundRect3.setTransform(0, 0);       

        this.addChild(this.roundRect3, this.roundRect1, this.roundRect2, this.instance_2);
        this.addChild(this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6);
        this.addChild(this.text, this.text_2, this.instance, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7);
        this.addChild(this.label7, this.label8, this.label9, this.label10, this.instance_B);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 497.3, 530);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(5, 0, 510, 208, 10);
        this.roundRect1.setTransform(0, 0);

        this.text_1 = new cjs.Text("Spelare 2", "bold 16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(16, 12);

        this.instance_2 = new lib.p109_2();
        this.instance_2.setTransform(14, 44, 0.62, 0.61);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 4) {
                    columnSpace = 4.07;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(61 + (columnSpace * 100), 58 + (row * 82.5), 18, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("9  +", "16.3px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(29, 62);
        this.label2 = new cjs.Text("8  +", "16.3px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(129, 62);
        this.label3 = new cjs.Text("7  +", "16.3px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(229, 62);
        this.label4 = new cjs.Text("6  +", "16.3px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(329, 62);
        this.label5 = new cjs.Text("5  +", "16.3px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(436, 62);

        this.label6 = new cjs.Text("5  +", "16.3px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(29, 145);
        this.label7 = new cjs.Text("4  +", "16.3px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(129, 145);
        this.label8 = new cjs.Text("3  +", "16.3px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(229, 145);
        this.label9 = new cjs.Text("2  +", "16.3px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(329, 145);
        this.label10 = new cjs.Text("1  +", "16.3px 'Myriad Pro'");
        this.label10.lineHeight = 30;
        this.label10.setTransform(436, 145);

        this.addChild(this.roundRect1, this.instance_2, this.text_1, this.textbox_group1, this.label1, this.label2,
            this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(300, 275, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
