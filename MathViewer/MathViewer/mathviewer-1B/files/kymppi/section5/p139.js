(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

            src: "images/p139_1.png",
            id: "p139_1"
        }, {
            src: "images/p139_2.png",
            id: "p139_2"
        }]
    };

    (lib.p139_1 = function() {
        this.initialize(img.p139_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p139_2 = function() {
        this.initialize(img.p139_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("139", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(15, 15);

        this.instance = new lib.p139_1();
        this.instance.setTransform(47, 21, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Hur många kronor är kvar?", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text("kr", "20px 'UusiTekstausMajema'", "#000000");
        this.text_2.setTransform(219, 113);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 257, 112, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 132);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 249);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(261, 15);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(261, 132);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(261, 249);

        this.instance = new lib.p139_2();
        this.instance.setTransform(10, 15, 0.47, 0.467);

        var lineArr = [];
        var lineX = [177, 442, 177, 442, 177, 442];
        var lineY = [76, 76, 193, 193, 310, 310];
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(57, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.text_3 = new cjs.Text("30 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_3.setTransform(175, 45);
        this.text_3.skewX = -20;
        this.text_3.skewY = -20;

        this.text_4 = new cjs.Text("60 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_4.setTransform(460, 49);
        this.text_4.skewX = 10;
        this.text_4.skewY = 10;

        this.text_5 = new cjs.Text("30 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_5.setTransform(206, 163);
        this.text_5.skewX = 0;
        this.text_5.skewY = 0;

        this.text_6 = new cjs.Text("45 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_6.setTransform(443, 153);
        this.text_6.skewX = 10;
        this.text_6.skewY = 10;

        this.text_7 = new cjs.Text("55 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_7.setTransform(214, 300);
        this.text_7.skewX = 30;
        this.text_7.skewY = 30;

        this.text_8 = new cjs.Text("35 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_8.setTransform(465, 282);
        this.text_8.skewX = -20;
        this.text_8.skewY = -20;

        this.addChild(this.text, this.text_1, this.instance, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,
            this.roundRect5, this.roundRect6, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 350);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(307, 313, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
