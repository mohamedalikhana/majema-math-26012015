(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p130_1.png",
            id: "p130_1"
        }]
    };

    (lib.p130_1 = function() {
        this.initialize(img.p130_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Tiotal och ental", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(177, 37);

        this.text_1 = new cjs.Text("45", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(40, 22);

        this.pageBottomText = new cjs.Text("förstå begreppen tiotal och ental", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(92, 651);

        this.text_4 = new cjs.Text("130", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(38, 649);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(33.1, 660.8, 1.05, 1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(78, 649.7, 155, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.instance_2 = new lib.p130_1();
        this.instance_2.setTransform(475, 38, 0.55, 0.55);

        this.text_5 = new cjs.Text("Det här visar", "14px 'Myriad Pro'");
        this.text_5.lineHeight = 24;
        this.text_5.setTransform(486, 46.5);
        this.text_6 = new cjs.Text("talet 11.", "14px 'Myriad Pro'");
        this.text_6.lineHeight = 24;
        this.text_6.setTransform(499, 62.5);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f("#20B14A").s("#000000").setStrokeStyle(0.7).moveTo(468, 75).lineTo(468, 135).lineTo(474, 135).lineTo(474, 75).lineTo(468, 75)
            .moveTo(468, 81).lineTo(474, 81).moveTo(468, 87).lineTo(474, 87).moveTo(468, 93).lineTo(474, 93).moveTo(468, 99).lineTo(474, 99).moveTo(468, 105).lineTo(474, 105)
            .moveTo(468, 111).lineTo(474, 111).moveTo(468, 117).lineTo(474, 117).moveTo(468, 123).lineTo(474, 123).moveTo(468, 129).lineTo(474, 129);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(480, 135).lineTo(480, 129).lineTo(486, 129).lineTo(486, 135).lineTo(480, 135);
        this.Line_2.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.pageBottomText, this.text_1, this.text, this.instance_2);
        this.addChild(this.text_5, this.text_6, this.Line_1, this.Line_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Ringa in 10 klossar. Skriv antalet klossar på linjen.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(8, 19);

        this.text_2 = new cjs.Text("Rita tiotal och ental.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(8, 42);

        this.text_3 = new cjs.Text("tiotal", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(339, 77);
        this.text_4 = new cjs.Text("ental", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(435, 77);
        this.text_5 = new cjs.Text("st", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(260, 190.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FBAA34').drawRoundRect(308, 49, 200, 150, 10);
        this.roundRect1.setTransform(0, 25);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#FBAA34").setStrokeStyle(0.7).moveTo(406, 74).lineTo(406, 225)
            .moveTo(308, 96).lineTo(508, 96);
        this.Line_1.setTransform(0, 0);

        var ToBeAdded = [];
        this.Line_box1 = new cjs.Shape();
        this.Line_box1.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(34, 119).lineTo(34, 133).lineTo(48, 133).lineTo(48, 119).lineTo(34, 119)
            .moveTo(112, 179).lineTo(112, 193).lineTo(126, 193).lineTo(126, 179).lineTo(112, 179)
            .moveTo(79, 170).lineTo(79, 184).lineTo(93, 184).lineTo(93, 170).lineTo(79, 170);
        this.Line_box1.setTransform(0, 0);
        this.Line_box1.skewX = -9;
        this.Line_box1.skewY = -9;
        ToBeAdded.push(this.Line_box1);

        this.Line_box2 = new cjs.Shape();
        this.Line_box2.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(102, 113).lineTo(102, 127).lineTo(116, 127).lineTo(116, 113).lineTo(102, 113)
            .moveTo(137, 105).lineTo(137, 119).lineTo(151, 119).lineTo(151, 105).lineTo(137, 105)
            .moveTo(163, 144).lineTo(163, 158).lineTo(177, 158).lineTo(177, 144).lineTo(163, 144)
            .moveTo(24, 152).lineTo(24, 166).lineTo(38, 166).lineTo(38, 152).lineTo(24, 152)
            .moveTo(45, 185).lineTo(45, 199).lineTo(59, 199).lineTo(59, 185).lineTo(45, 185)
            .moveTo(148, 183).lineTo(148, 197).lineTo(162, 197).lineTo(162, 183).lineTo(148, 183);
        this.Line_box2.setTransform(0, 0);
        ToBeAdded.push(this.Line_box2);

        this.Line_box3 = new cjs.Shape();
        this.Line_box3.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(60, 182).lineTo(60, 196).lineTo(74, 196)
            .lineTo(74, 182).lineTo(60, 182);
        this.Line_box3.setTransform(0, 0);
        this.Line_box3.skewX = -5;
        this.Line_box3.skewY = -5;
        ToBeAdded.push(this.Line_box3);

        this.Line_box4 = new cjs.Shape();
        this.Line_box4.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(184, 108).lineTo(184, 122).lineTo(198, 122)
            .lineTo(198, 108).lineTo(184, 108);
        this.Line_box4.setTransform(0, 0);
        this.Line_box4.skewX = 25;
        this.Line_box4.skewY = 25;
        ToBeAdded.push(this.Line_box4);

        this.Line_box5 = new cjs.Shape();
        this.Line_box5.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(163, 90).lineTo(163, 104).lineTo(177, 104)
            .lineTo(177, 90).lineTo(163, 90);
        this.Line_box5.setTransform(0, 0);
        this.Line_box5.skewX = 17;
        this.Line_box5.skewY = 17;
        ToBeAdded.push(this.Line_box5);

        this.Line_box6 = new cjs.Shape();
        this.Line_box6.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(95, 125).lineTo(95, 139).lineTo(109, 139)
            .lineTo(109, 125).lineTo(95, 125);
        this.Line_box6.setTransform(0, 0);
        this.Line_box6.skewX = 9;
        this.Line_box6.skewY = 9;
        ToBeAdded.push(this.Line_box6);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#8D8E92").setStrokeStyle(0.7).moveTo(200, 204).lineTo(258, 204);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.text_1, this.text_2, this.roundRect1, this.Line_1, this.text_3, this.text_4, this.text_5, this.hrLine_1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }
        this.addChild(this.Line_box1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 305);

    (lib.Symbol2 = function() {
        this.initialize();
        this.text_3 = new cjs.Text("tiotal", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(339, 77);
        this.text_4 = new cjs.Text("ental", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(435, 77);
        this.text_5 = new cjs.Text("st", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(260, 190.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FBAA34').drawRoundRect(308, 49, 200, 150, 10);
        this.roundRect1.setTransform(0, 25);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#FBAA34").setStrokeStyle(0.7).moveTo(406, 74).lineTo(406, 225)
            .moveTo(308, 96).lineTo(508, 96);
        this.Line_1.setTransform(0, 0);

        var ToBeAdded = [];
        this.Line_box1 = new cjs.Shape();
        this.Line_box1.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(34, 119).lineTo(34, 133).lineTo(48, 133).lineTo(48, 119).lineTo(34, 119)
            .moveTo(112, 179).lineTo(112, 193).lineTo(126, 193).lineTo(126, 179).lineTo(112, 179)
            .moveTo(79, 170).lineTo(79, 184).lineTo(93, 184).lineTo(93, 170).lineTo(79, 170)
            .moveTo(24, 168).lineTo(24, 182).lineTo(38, 182).lineTo(38, 168).lineTo(24, 168);
        this.Line_box1.setTransform(0, 0);
        this.Line_box1.skewX = -9;
        this.Line_box1.skewY = -9;
        ToBeAdded.push(this.Line_box1);

        this.Line_box2 = new cjs.Shape();
        this.Line_box2.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(102, 113).lineTo(102, 127).lineTo(116, 127).lineTo(116, 113).lineTo(102, 113)
            .moveTo(137, 105).lineTo(137, 119).lineTo(151, 119).lineTo(151, 105).lineTo(137, 105)
            .moveTo(163, 144).lineTo(163, 158).lineTo(177, 158).lineTo(177, 144).lineTo(163, 144)
            .moveTo(24, 152).lineTo(24, 166).lineTo(38, 166).lineTo(38, 152).lineTo(24, 152)
            .moveTo(45, 185).lineTo(45, 199).lineTo(59, 199).lineTo(59, 185).lineTo(45, 185)
            .moveTo(148, 183).lineTo(148, 197).lineTo(162, 197).lineTo(162, 183).lineTo(148, 183);
        this.Line_box2.setTransform(0, 0);
        ToBeAdded.push(this.Line_box2);

        this.Line_box3 = new cjs.Shape();
        this.Line_box3.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(60, 182).lineTo(60, 196).lineTo(74, 196)
            .lineTo(74, 182).lineTo(60, 182);
        this.Line_box3.setTransform(0, 0);
        this.Line_box3.skewX = -5;
        this.Line_box3.skewY = -5;
        ToBeAdded.push(this.Line_box3);

        this.Line_box4 = new cjs.Shape();
        this.Line_box4.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(184, 108).lineTo(184, 122).lineTo(198, 122).lineTo(198, 108).lineTo(184, 108)
            .moveTo(68, 101).lineTo(68, 115).lineTo(82, 115).lineTo(82, 101).lineTo(68, 101);
        this.Line_box4.setTransform(0, 0);
        this.Line_box4.skewX = 25;
        this.Line_box4.skewY = 25;
        ToBeAdded.push(this.Line_box4);

        this.Line_box5 = new cjs.Shape();
        this.Line_box5.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(163, 90).lineTo(163, 104).lineTo(177, 104)
            .lineTo(177, 90).lineTo(163, 90);
        this.Line_box5.setTransform(0, 0);
        this.Line_box5.skewX = 17;
        this.Line_box5.skewY = 17;
        ToBeAdded.push(this.Line_box5);

        this.Line_box6 = new cjs.Shape();
        this.Line_box6.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(95, 125).lineTo(95, 139).lineTo(109, 139)
            .lineTo(109, 125).lineTo(95, 125);
        this.Line_box6.setTransform(0, 0);
        this.Line_box6.skewX = 9;
        this.Line_box6.skewY = 9;
        ToBeAdded.push(this.Line_box6);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#8D8E92").setStrokeStyle(0.7).moveTo(200, 204).lineTo(258, 204);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.Line_1, this.text_3, this.text_4, this.text_5, this.hrLine_1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }
        this.addChild(this.Line_box1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 305);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text_3 = new cjs.Text("tiotal", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(339, 77);
        this.text_4 = new cjs.Text("ental", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(435, 77);
        this.text_5 = new cjs.Text("st", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(260, 190.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FBAA34').drawRoundRect(308, 49, 200, 150, 10);
        this.roundRect1.setTransform(0, 25);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#FBAA34").setStrokeStyle(0.7).moveTo(406, 74).lineTo(406, 225)
            .moveTo(308, 96).lineTo(508, 96);
        this.Line_1.setTransform(0, 0);

        var ToBeAdded = [];
        this.Line_box1 = new cjs.Shape();
        this.Line_box1.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(34, 119).lineTo(34, 133).lineTo(48, 133).lineTo(48, 119).lineTo(34, 119)
            .moveTo(112, 179).lineTo(112, 193).lineTo(126, 193).lineTo(126, 179).lineTo(112, 179)
            .moveTo(79, 170).lineTo(79, 184).lineTo(93, 184).lineTo(93, 170).lineTo(79, 170)
            .moveTo(24, 168).lineTo(24, 182).lineTo(38, 182).lineTo(38, 168).lineTo(24, 168);
        this.Line_box1.setTransform(0, 0);
        this.Line_box1.skewX = -9;
        this.Line_box1.skewY = -9;
        ToBeAdded.push(this.Line_box1);

        this.Line_box2 = new cjs.Shape();
        this.Line_box2.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(102, 113).lineTo(102, 127).lineTo(116, 127).lineTo(116, 113).lineTo(102, 113)
            .moveTo(137, 105).lineTo(137, 119).lineTo(151, 119).lineTo(151, 105).lineTo(137, 105)
            .moveTo(163, 144).lineTo(163, 158).lineTo(177, 158).lineTo(177, 144).lineTo(163, 144)
            .moveTo(24, 152).lineTo(24, 166).lineTo(38, 166).lineTo(38, 152).lineTo(24, 152)
            .moveTo(45, 185).lineTo(45, 199).lineTo(59, 199).lineTo(59, 185).lineTo(45, 185);
        this.Line_box2.setTransform(0, 0);
        ToBeAdded.push(this.Line_box2);

        this.Line_box3 = new cjs.Shape();
        this.Line_box3.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(60, 182).lineTo(60, 196).lineTo(74, 196)
            .lineTo(74, 182).lineTo(60, 182);
        this.Line_box3.setTransform(0, 0);
        this.Line_box3.skewX = -5;
        this.Line_box3.skewY = -5;
        ToBeAdded.push(this.Line_box3);

        this.Line_box4 = new cjs.Shape();
        this.Line_box4.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(184, 108).lineTo(184, 122).lineTo(198, 122).lineTo(198, 108).lineTo(184, 108)
            .moveTo(68, 101).lineTo(68, 115).lineTo(82, 115).lineTo(82, 101).lineTo(68, 101);
        this.Line_box4.setTransform(0, 0);
        this.Line_box4.skewX = 25;
        this.Line_box4.skewY = 25;
        ToBeAdded.push(this.Line_box4);

        this.Line_box5 = new cjs.Shape();
        this.Line_box5.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(163, 90).lineTo(163, 104).lineTo(177, 104)
            .lineTo(177, 90).lineTo(163, 90);
        this.Line_box5.setTransform(0, 0);
        this.Line_box5.skewX = 17;
        this.Line_box5.skewY = 17;
        ToBeAdded.push(this.Line_box5);

        this.Line_box6 = new cjs.Shape();
        this.Line_box6.graphics.beginStroke("#231F20").f("#FFF679").setStrokeStyle(0.7).moveTo(95, 125).lineTo(95, 139).lineTo(109, 139)
            .lineTo(109, 125).lineTo(95, 125);
        this.Line_box6.setTransform(0, 0);
        this.Line_box6.skewX = 9;
        this.Line_box6.skewY = 9;
        ToBeAdded.push(this.Line_box6);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#8D8E92").setStrokeStyle(0.7).moveTo(200, 204).lineTo(258, 204);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.Line_1, this.text_3, this.text_4, this.text_5, this.hrLine_1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }
        this.addChild(this.Line_box1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 255, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(296.8, 419, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(296.8, 583, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
