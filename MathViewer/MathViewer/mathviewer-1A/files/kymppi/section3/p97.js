(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p97_1.png",
            id: "p97_1"
        }]
    };

    (lib.p97_1 = function() {
        this.initialize(img.p97_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("97", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(468 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Några fåglar kommer och några flyger iväg.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(6, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol6 = function() {
        this.initialize();

        var adjustX = 10;
        var adjustY = 7;

        this.instance = new lib.p97_1();
        this.instance.setTransform(3 - adjustX, 45 - adjustY, 0.375, 0.375);

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193 - adjustX, 13.6 - adjustY, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(5 - adjustX, 0 - adjustY);

        this.text_1 = new cjs.Text("Hur många är det till slut?", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(24 - adjustX, 22 - adjustY);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 253, 115, 10);
        this.roundRect1.setTransform(5 - adjustX, 35 - adjustY);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(263 - adjustX, 35 - adjustY);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(5 - adjustX, 155 - adjustY);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(263 - adjustX, 155 - adjustY);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(5 - adjustX, 275 - adjustY);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(263 - adjustX, 275 - adjustY);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            if (column == 1) {
                columnSpace = 1.433;
            }
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(165 + (columnSpace * 180.5), 126 + (row * 122.5), 20, 23);
                //console.log(150 + (columnSpace * 150), 40 + (row * 30))
            }
        }
        this.textbox_group1.setTransform(0 - adjustX, 0 - adjustY);

        this.label1 = new cjs.Text("4  +  3  –  2  =", "16.3px 'Myriad Pro'");
        this.label1.lineHeight = 23;
        this.label1.setTransform(71 - adjustX, 130 - adjustY);
        this.label2 = new cjs.Text("4  +  4  –  5  =", "16.3px 'Myriad Pro'");
        this.label2.lineHeight = 23;
        this.label2.setTransform(71 - adjustX, 252 - adjustY);
        this.label3 = new cjs.Text("5  +  3  –  5  =", "16.3px 'Myriad Pro'");
        this.label3.lineHeight = 23;
        this.label3.setTransform(71 - adjustX, 375 - adjustY);

        this.label4 = new cjs.Text("5  +  4  –  3  =", "16.3px 'Myriad Pro'");
        this.label4.lineHeight = 23;
        this.label4.setTransform(330 - adjustX, 130 - adjustY);
        this.label5 = new cjs.Text("6  +  3  –  4  =", "16.3px 'Myriad Pro'");
        this.label5.lineHeight = 23;
        this.label5.setTransform(330 - adjustX, 252 - adjustY);
        this.label6 = new cjs.Text("7  +  2  –  6  =", "16.3px 'Myriad Pro'");
        this.label6.lineHeight = 23;
        this.label6.setTransform(330 - adjustX, 375 - adjustY);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6);
        this.addChild(this.instance_2, this.text, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 502, 389);

    (lib.Symbol3 = function() {
        this.initialize();

        var adjustX = 10;
        var adjustY = 7;

        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19 - adjustX, 0 - adjustY);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(5 - adjustX, 0 - adjustY);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 25, 510, 140, 10);
        this.roundRect1.setTransform(0 - adjustX, 0 - adjustY);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 1) {
                    columnSpace = 1.06;
                } else if (column == 2) {
                    columnSpace = 2.12;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(133.5 + (columnSpace * 154), 40 + (row * 30), 21, 24);
            }
        }
        this.textbox_group1.setTransform(0 - adjustX, 0 - adjustY);

        this.label1 = new cjs.Text("4  +  2  +  2  =", "16px 'Myriad Pro'");
        this.label1.lineHeight = 23;
        this.label1.setTransform(38 - adjustX, 43 - adjustY);
        this.label2 = new cjs.Text("4  +  3  +  2  =", "16px 'Myriad Pro'");
        this.label2.lineHeight = 23;
        this.label2.setTransform(38 - adjustX, 73 - adjustY);
        this.label3 = new cjs.Text("3  +  3  +  2  =", "16px 'Myriad Pro'");
        this.label3.lineHeight = 23;
        this.label3.setTransform(38 - adjustX, 103 - adjustY);
        this.label4 = new cjs.Text("3  +  3  +  3  =", "16px 'Myriad Pro'");
        this.label4.lineHeight = 23;
        this.label4.setTransform(38 - adjustX, 133 - adjustY);

        this.label6 = new cjs.Text("8  –  3  –  3  =", "16px 'Myriad Pro'");
        this.label6.lineHeight = 23;
        this.label6.setTransform(204 - adjustX, 43 - adjustY);
        this.label7 = new cjs.Text("8  –  4  –  4  =", "16px 'Myriad Pro'");
        this.label7.lineHeight = 23;
        this.label7.setTransform(204 - adjustX, 73 - adjustY);
        this.label8 = new cjs.Text("9  –  3  –  3  =", "16px 'Myriad Pro'");
        this.label8.lineHeight = 23;
        this.label8.setTransform(204 - adjustX, 103 - adjustY);
        this.label9 = new cjs.Text("9  –  4  –  4  =", "16px 'Myriad Pro'");
        this.label9.lineHeight = 23;
        this.label9.setTransform(204 - adjustX, 133 - adjustY);

        this.label11 = new cjs.Text("6  +  2  –  2  =", "16px 'Myriad Pro'");
        this.label11.lineHeight = 23;
        this.label11.setTransform(366 - adjustX, 43 - adjustY);
        this.label12 = new cjs.Text("6  +  3  –  3  =", "16px 'Myriad Pro'");
        this.label12.lineHeight = 23;
        this.label12.setTransform(366 - adjustX, 73 - adjustY);
        this.label13 = new cjs.Text("7  +  2  –  7  =", "16px 'Myriad Pro'");
        this.label13.lineHeight = 23;
        this.label13.setTransform(366 - adjustX, 103 - adjustY);
        this.label14 = new cjs.Text("7  +  2  –  2  =", "16px 'Myriad Pro'");
        this.label14.lineHeight = 23;
        this.label14.setTransform(366 - adjustX, 133 - adjustY);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 502, 154);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(319, 283, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(319, 477, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
