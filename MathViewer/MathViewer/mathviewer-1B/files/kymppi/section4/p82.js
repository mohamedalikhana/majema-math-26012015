(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p82_1.png",
            id: "p82_1"
        }]
    };

    (lib.p82_1 = function() {
        this.initialize(img.p82_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("kunna klockan – hel och halv timme", "9px 'Myriad Pro'");
        this.text_1.setTransform(71, 658);

        this.text_2 = new cjs.Text("82", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(37.5, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.text_3 = new cjs.Text("Vi repeterar klockan –", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_3.setTransform(95.5, 42);

        this.text_5 = new cjs.Text("hel timme", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_5.setTransform(94.5, 75);

        this.text_4 = new cjs.Text("28", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(46, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.Symbolimg = new lib.Symbolimg();
        this.Symbolimg.setTransform(0, 0);

        this.addChild(this.Symbolimg, this.shape_2, this.text_5, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbolimg = function() {
        this.initialize();

        this.instance = new lib.p82_1();
        this.instance.setTransform(0, 0, 0.47, 0.47);

        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);


    //Part 1
    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Vad är klockan?", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 330, 10);
        this.roundRect1.setTransform(10, 20);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_1.setTransform(10, 98);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_2.setTransform(10, 204);

        this.Symbolclock = new lib.Symbolclock();
        this.Symbolclock.setTransform(72, 72, 0.83, 0.83);

        this.Symbolclock_1 = new lib.Symbolclock();
        this.Symbolclock_1.setTransform(200, 72, 0.83, 0.83);

        this.Symbolclock_2 = new lib.Symbolclock();
        this.Symbolclock_2.setTransform(329, 72, 0.83, 0.83);

        this.Symbolclock_3 = new lib.Symbolclock();
        this.Symbolclock_3.setTransform(455, 72, 0.83, 0.83);

        this.Symbolclock_4 = new lib.Symbolclock();
        this.Symbolclock_4.setTransform(72, 181, 0.83, 0.83);

        this.Symbolclock_5 = new lib.Symbolclock();
        this.Symbolclock_5.setTransform(200, 181, 0.83, 0.83);

        this.Symbolclock_6 = new lib.Symbolclock();
        this.Symbolclock_6.setTransform(329, 181, 0.83, 0.83);

        this.Symbolclock_7 = new lib.Symbolclock();
        this.Symbolclock_7.setTransform(455, 181, 0.83, 0.83);

        this.Symbolclock_8 = new lib.Symbolclock();
        this.Symbolclock_8.setTransform(72, 286, 0.83, 0.83);

        this.Symbolclock_9 = new lib.Symbolclock();
        this.Symbolclock_9.setTransform(200, 286, 0.83, 0.83);

        this.Symbolclock_10 = new lib.Symbolclock();
        this.Symbolclock_10.setTransform(329, 286, 0.83, 0.83);

        this.Symbolclock_11 = new lib.Symbolclock();
        this.Symbolclock_11.setTransform(455, 286, 0.83, 0.83);

        var TextArr = [];
        var TextX = [21, 147, 273, 399, 21, 147, 273, 399, 21, 147, 273, 399];
        var TextY = [130, 130, 130, 130, 235, 235, 235, 235, 340, 340, 340, 340];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("Klockan är", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        var lineArr = [];
        var lineX = [92, 217, 344, 469, 92, 217, 344, 469, 92, 217, 344, 469];
        var lineY = [92, 92, 92, 92, 197, 197, 197, 197, 303, 303, 303, 303];

        for (var row = 0; row < lineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(30, 40);
            vrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(vrLine_1);
        }

        var DotArr = [];
        var DotX = [122, 247, 374, 499, 122, 247, 374, 499, 122, 247, 374, 499];
        var DotY = [130, 130, 130, 130, 235, 235, 235, 235, 341, 341, 341, 341];

        for (var row = 0; row < DotX.length; row++) {

            var text_1 = new cjs.Text(".", "12px 'Myriad Pro'", "#9D9D9C");
            text_1.setTransform(DotX[row], DotY[row]);
            DotArr.push(text_1);
        }

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#706F6F");
        this.text_2.setTransform(99, 131);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.hrLine_1, this.hrLine_2,
            this.Symbolclock, this.Symbolclock_1, this.Symbolclock_2, this.Symbolclock_3, this.Symbolclock_4, this.Symbolclock_5,
            this.Symbolclock_6, this.Symbolclock_7, this.Symbolclock_8, this.Symbolclock_9, this.Symbolclock_10, this.Symbolclock_11);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }
        for (var i = 0; i < DotArr.length; i++) {
            this.addChild(DotArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

        //row-1

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(82, 71.5, 1.2, 0.7, 89);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(72, 59.1, 1.2, 0.9);
        this.text_dot1 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot1.setTransform(66.6, 70.5);

        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(193, 77, 1.2, 0.7, 240);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(200, 59.1, 1.2, 0.9);
        this.text_dot2 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot2.setTransform(194, 71.5);

        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(333.5, 80.5, 1.2, 0.7, 329);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(329, 59.1, 1.2, 0.9);
        this.text_dot3 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot3.setTransform(323, 71.5);

        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(452, 79, 1.2, 0.7, 207);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(455.5, 59.1, 1.2, 0.9);
        this.text_dot4 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot4.setTransform(449.5, 71.5);

        //row-2

        this.shape_9 = new cjs.Shape(); // clock handle red
        this.shape_9.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_9.setTransform(69, 173.5, 1.2, 0.7, 515);
        this.shape_10 = new cjs.Shape(); // clock handle blue
        this.shape_10.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_10.setTransform(73, 168.1, 1.2, 0.9);
        this.text_dot5 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot5.setTransform(66.6, 179.5);

        this.shape_11 = new cjs.Shape(); // clock handle red
        this.shape_11.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_11.setTransform(200, 189.5, 1.2, 0.7, 179);
        this.shape_12 = new cjs.Shape(); // clock handle blue
        this.shape_12.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_12.setTransform(200, 168.1, 1.2, 0.9);
        this.text_dot6 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot6.setTransform(193.5, 179.5);

        this.shape_13 = new cjs.Shape(); // clock handle red
        this.shape_13.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_13.setTransform(333, 174, 1.2, 0.7, 25);
        this.shape_14 = new cjs.Shape(); // clock handle blue
        this.shape_14.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_14.setTransform(329, 168.1, 1.2, 0.9);
        this.text_dot7 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot7.setTransform(323.7, 179.5);

        this.shape_15 = new cjs.Shape(); // clock handle red
        this.shape_15.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_15.setTransform(445.5, 180.5, 1.2, 0.7, 90);
        this.shape_16 = new cjs.Shape(); // clock handle blue
        this.shape_16.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_16.setTransform(455.5, 168.1, 1.2, 0.9);
        this.text_dot8 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot8.setTransform(449.6, 179.5);

        //row-3

        this.shape_17 = new cjs.Shape(); // clock handle red
        this.shape_17.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_17.setTransform(66, 282, 1.2, 0.7, 299);
        this.shape_18 = new cjs.Shape(); // clock handle blue
        this.shape_18.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_18.setTransform(73, 273.1, 1.2, 0.9);
        this.text_dot9 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot9.setTransform(66.6, 284.5);

        this.shape_19 = new cjs.Shape(); // clock handle red
        this.shape_19.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_19.setTransform(207, 282, 1.2, 0.7, 60);
        this.shape_20 = new cjs.Shape(); // clock handle blue
        this.shape_20.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_20.setTransform(200, 274.1, 1.2, 0.9);
        this.text_dot10 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot10.setTransform(194, 284.5);

        this.shape_21 = new cjs.Shape(); // clock handle red
        this.shape_21.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_21.setTransform(337.5, 290.5, 1.2, 0.7, 117);
        this.shape_22 = new cjs.Shape(); // clock handle blue
        this.shape_22.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_22.setTransform(329, 273.1, 1.2, 0.9);
        this.text_dot11 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot11.setTransform(323, 284.5);

        this.shape_23 = new cjs.Shape(); // clock handle red
        this.shape_23.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_23.setTransform(455, 278, 1.2, 0.7);
        this.shape_24 = new cjs.Shape(); // clock handle blue
        this.shape_24.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_24.setTransform(455, 274.1, 1.2, 0.9);
        this.text_dot12 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot12.setTransform(449, 284.5);

        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10, this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_20, this.shape_19, this.shape_21, this.shape_22, this.shape_24, this.shape_23, this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4, this.text_dot5, this.text_dot6, this.text_dot7, this.text_dot8, this.text_dot9, this.text_dot10, this.text_dot11, this.text_dot12)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 513.3, 324);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287, 340, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
