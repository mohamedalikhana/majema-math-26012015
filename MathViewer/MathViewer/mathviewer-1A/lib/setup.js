var pageInfo = [
    {title:'Kymppi', folder:'files/kymppi', preload:true, pages:[]}
];
var pages = [
    {a:0, k:'', o:'Front Page', s:'', t:'files/toc/toc.html'},
        
    {a:0, n:'1', o:'Taluppfattning <br>– talen 1 till 4 och 0', color:'#65AFC5',childColor:'#E3F0F3',columnColor:'#E3F0F3'},
    {a:1, n:'1', o:'Räkna till 10', t:'section1/p6.html'},
    {a:1, t:'section1/p7.html'},
    {a:1, t:'section1/p8.html'},
    {a:1, n:'2', o:'Lika många <br>– är lika med', t: 'section1/p9.html'},
    {a:1, t:'section1/p10.html'},
    {a:1, t:'section1/p11.html'},
    {a:1, n:'3', o:'Talen 1 och 2', t: 'section1/p12.html'},
    {a:1, t: 'section1/p13.html'},
    {a:1, t: 'section1/p14.html'},
    {a:1, n:'4', o:'Talet 3', t: 'section1/p15.html'},
    {a:1, t: 'section1/p16.html'},
    {a:1, t: 'section1/p17.html'},
    { a: 1, n: '5', o: 'I klassrummet', t: 'section1/p18.html' },
    { a: 1, t: 'section1/p19.html' },
    {a:1, n:'6', o:'Talet 4', t: 'section1/p20.html'},
    {a:1, t: 'section1/p21.html'},
    {a:1, t: 'section1/p22.html'},
    {a:1, n:'7', o:'Större än och mindre än', t: 'section1/p23.html'},
    {a:1, t: 'section1/p24.html'},
    {a:1, t: 'section1/p25.html'},
    { a: 1, n: '8', o: 'Talet 0', t: 'section1/p26.html' },
    { a: 1, t: 'section1/p27.html' },
    { a: 1, t: 'section1/p28.html' },
    { a: 1, n: '9', o: 'Testa dina kunskaper', t: 'section1/p29.html' },
    { a: 1, t: 'section1/p30.html' },
    { a: 1, t: 'section1/p31.html' },     

    { a: 1, n: '1', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section1/larandemal.html' },
   
    {a:0, n:'2', o:'Addition och subtraktion <br>– talen 5 och 6', color:'#A2BD30',childColor:'#EEF3E0',columnColor:'#EEF3E0'},
    {a:1, n:'10', o:'Hälften', t: 'section2/p32.html'},
     { a: 1, t: 'section2/p33.html' },
    {a:1, n:'11', o:'Talet 5', t: 'section2/p34.html'},
    {a:1, t: 'section2/p35.html'},
    {a:1, t: 'section2/p36.html'},
    {a:1, n:'12', o:'Vi lär oss addition', t: 'section2/p37.html'},
    {a:1, t: 'section2/p38.html'},{a:1, t: 'section2/p39.html'},
    {a:1, n:'13', o:'Vi skriver addition', t: 'section2/p40.html'},
    {a:1, t: 'section2/p41.html'},{a:1, t: 'section2/p42.html'},
    {a:1, n:'14', o:'Öva addition', t: 'section2/p43.html'},
    {a:1, t: 'section2/p44.html'},{a:1, t: 'section2/p45.html'},
    { a: 1, n: '15', o: 'Räknehändelser addition', t: 'section2/p46.html' },
    { a: 1, t: 'section2/p47.html' },
    
    { a: 1, n: '16', o: 'Vi lär oss subtraktion', t: 'section2/p48.html' },
    { a: 1, t: 'section2/p49.html' },
    { a: 1, t: 'section2/p50.html' },
    { a: 1, n: '17', o: 'Vi skriver subtraktion', t: 'section2/p51.html' },
    { a: 1, t: 'section2/p52.html' },
    { a: 1, t: 'section2/p53.html' },
    { a: 1, n: '18', o: 'Öva subtraktion', t: 'section2/p54.html' },
    { a: 1, t: 'section2/p55.html' },
    { a: 1, t: 'section2/p56.html' },
    { a: 1, n: '19', o: 'Talet 6', t: 'section2/p57.html' },
    { a: 1, t: 'section2/p58.html' },
    { a: 1, t: 'section2/p59.html' },
    { a: 1, n: '20', o: 'Räknehändelser subtraktion', t: 'section2/p60.html' },
    { a: 1, t: 'section2/p61.html' },
    { a: 1, n: '21', o: 'Vi övar', t: 'section2/p62.html' },
    { a: 1, t: 'section2/p63.html' },
    { a: 1, t: 'section2/p64.html' },
    { a: 1, n: '22', o: 'Ett tal saknas', t: 'section2/p65.html' },
    { a: 1, t: 'section2/p66.html' },
    { a: 1, t: 'section2/p67.html' },
    { a: 1, n: '23', o: 'Testa dina kunskaper', t: 'section2/p68.html' },
    { a: 1, t: 'section2/p69.html' },
    { a: 1, t: 'section2/p70.html' },

    { a: 1, n: '2', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section2/larandemal.html' },
    
    {a:0, n:'3', o:'Talen 7 till 10', color:'#d67f32',childColor:'#F8E9D8',columnColor:'#F8E9D8'},
    { a: 1, n: '24', o: 'Talet 7', t: 'section3/p71.html' },
    { a: 1, t: 'section3/p72.html' },
    { a: 1, t: 'section3/p73.html' },
    { a: 1, n: '25', o: 'Likhetstecknet', t: 'section3/p74.html' },
    { a: 1, t: 'section3/p75.html' },
    { a: 1, n: '26', o: 'Kommutativa lagen i addition', t: 'section3/p76.html' },
    { a: 1, t: 'section3/p77.html' },
    { a: 1, t: 'section3/p78.html' },
    {a:1, n:'27', o:'Talfamiljer', t: 'section3/p79.html' },
    { a: 1, t: 'section3/p80.html' },
    { a: 1, t: 'section3/p81.html' },
    { a: 1, n: '28', o: 'Talet 8', t: 'section3/p82.html' },
    { a: 1, t: 'section3/p83.html' },
    { a: 1, t: 'section3/p84.html' },
    { a: 1, n: '29', o: 'Addera tre tal', t: 'section3/p85.html' },
    { a: 1, t: 'section3/p86.html' },
    { a: 1, t: 'section3/p87.html' },
    { a: 1, n: '30', o: 'Mönster', t: 'section3/p88.html' },
    { a: 1, t: 'section3/p89.html' },
    { a: 1, n: '31', o: 'Subtrahera med tre tal', t: 'section3/p90.html' },
    { a: 1, t: 'section3/p91.html' },
    { a: 1, t: 'section3/p92.html' },
    { a: 1, n: '32', o: 'Talet 9', t: 'section3/p93.html' },
    { a: 1, t: 'section3/p94.html' },
    { a: 1, t: 'section3/p95.html' },
    { a: 1, n: '33', o: 'Addera och subtrahera', t: 'section3/p96.html' },
    { a: 1, t: 'section3/p97.html' },
	{ a: 1, t: 'section3/p98.html' },
    { a: 1, n: '34', o: 'Talet 10', t: 'section3/p99.html' },
    { a: 1, t: 'section3/p100.html' },
	{ a: 1, t: 'section3/p101.html' },
    { a: 1, n: '35', o: 'Dubbelt', t: 'section3/p102.html' },
    { a: 1, t: 'section3/p103.html' },
    { a: 1, n: '36', o: 'Talkamrater', t: 'section3/p104.html' },
    { a: 1, t: 'section3/p105.html' },
	{ a: 1, t: 'section3/p106.html' },
    { a: 1, n: '37', o: 'Testa dina kunskaper', t: 'section3/p107.html' },
    { a: 1, t: 'section3/p108.html' },
	{ a: 1, t: 'section3/p109.html' },

    { a: 1, n: '3', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section3/larandemal.html' },
  
   
    {a:0, n:'4', o:'Pengar, talen 11 och 12', color:'#8E83B2',childColor:'#EDECF3',columnColor:'#EDECF3'},
    { a: 1, n: '38', o: 'Pengar', t: 'section4/p110.html' },
    { a: 1, t: 'section4/p111.html' },
	{ a: 1, t: 'section4/p112.html' },
    { a: 1, n: '39', o: 'Vad kostar det?', t: 'section4/p113.html' },
    { a: 1, t: 'section4/p114.html' },
	{ a: 1, t: 'section4/p115.html' },
    { a: 1, n: '40', o: 'Problemlösning', t: 'section4/p116.html' },
    { a: 1, t: 'section4/p117.html' },
    { a: 1, n: '41', o: 'Hur mycket pengar är kvar?', t: 'section4/p118.html' },
    { a: 1, t: 'section4/p119.html' },
	{ a: 1, t: 'section4/p120.html' },
    { a: 1, n: '42', o: 'Prisskillnad', t: 'section4/p121.html' },
    { a: 1, t: 'section4/p122.html' },
	{ a: 1, t: 'section4/p123.html' },
    { a: 1, n: '43', o: 'Ett tal saknas', t: 'section4/p124.html' },
    { a: 1, t: 'section4/p125.html' },
	{ a: 1, t: 'section4/p126.html' },
    { a: 1, n: '44', o: 'Talet 11', t: 'section4/p127.html' },
    { a: 1, t: 'section4/p128.html' },
	{ a: 1, t: 'section4/p129.html' },
    { a: 1, n: '45', o: 'Tiotal och ental', t: 'section4/p130.html' },
    { a: 1, t: 'section4/p131.html' },
    { a: 1, n: '46', o: 'Talet 12', t: 'section4/p132.html' },
    { a: 1, t: 'section4/p133.html' },
	{ a: 1, t: 'section4/p134.html' },
    { a: 1, n: '47', o: 'Stapeldiagram', t: 'section4/p135.html' },
    { a: 1, t: 'section4/p136.html' },
	{ a: 1, t: 'section4/p137.html' },
    { a: 1, n: '48', o: 'Eget stapeldiagram', t: 'section4/p138.html' },
	{ a: 1, t: 'section4/p139.html' },
    { a: 1, n: '49', o: 'Klockan <br>– hel timme', t: 'section4/p140.html' },
    { a: 1, t: 'section4/p141.html' },
	{ a: 1, t: 'section4/p142.html' },
    { a: 1, n: '50', o: 'Testa dina kunskaper', t: 'section4/p143.html' },
    { a: 1, t: 'section4/p144.html' },
	{ a: 1, t: 'section4/p145.html' },

    { a: 1, n: '4', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section4/larandemal.html' },
    
    {a:0, n:'5', o:'Programmering', color:'#BE0075',childColor:'#F5E4E7',columnColor:'#EEE6F4'},
    { a: 1, n: '51', o: 'Mot programmering 1', t: 'section5/p146.html' },
    { a: 1, t: 'section5/p147.html' },
	{ a: 1, t: 'section5/p148.html' },   

    { a: 1, n: '52', o: 'Mot programmering 2', t: 'section5/p149.html' },
    { a: 1, t: 'section5/p150.html' },
	{ a: 1, t: 'section5/p151.html' },
    { a: 1, t: 'lagesord/p152.html' },

    { a: 1, n: '5', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section5/larandemal.html' },
    
    
];

var pages2 = [
    {a:0, k:'', o:'', s:'', t: ''},
        
    {a:0, n:'1', o:'Lukukäsite – luvut 1–4 ja 0'},
    {a:1, n:'1', o:'Lukumäärä 1–10', t: ''},
    {a:1, t: ''},
    {a:1, n:'2', o:'Yhtä monta tai yhtä suuri', t: ''},
    {a:1, t: ''},
    {a:1, n:'3', o:'Luvut 1 ja 2', t: ''},
    {a:1, t: ''},
    {a:1, n:'4', o:'Luku 3', t: ''},
    {a:1, t: ''},
    {a:1, n:'5', o:'Luku 4', t: ''},
    {a:1, t: ''},
    {a:1, n:'6', o:'Pienempi ja suurempi kuin', t: ''},
    {a:1, t: ''},
    {a:1, n:'7', o:'Luku 0', t: ''},
    {a:1, t: ''},
    {a:1, n:'8', o:'Pelaa ja testaa taitosi', t: ''},
    {a:1, t: ''},
    
    {a:0, n:'2', o:'Yhteen- ja vähennyslaskua – luvut 5 ja 6'},
    {a:1, n:'9', o:'Luku 5', t: ''},
    {a:1, t: ''},
    {a:1, n:'10', o:'Tutustutaan yhteenlaskuun', t: ''},
    {a:1, t: ''},
    {a:1, n:'11', o:'Tehdään yhteenlasku', t: ''},
    {a:1, t: ''},
    {a:1, n:'12', o:'Harjoitellaan yhteenlaskua', t: ''},
    {a:1, t: ''},
    {a:1, n:'13', o:'Tutustutaan vähennyslaskuun', t: ''},
    {a:1, t: ''},
    {a:1, n:'14', o:'Tehdään vähennyslasku', t: ''},
    {a:1, t: ''},
    {a:1, n:'15', o:'Harjoitellaan vähennyslaskua', t: ''},
    {a:1, t: ''},
    {a:1, n:'16', o:'Harjoitellaan', t: ''},
    {a:1, t: ''},
    {a:1, n:'17', o:'Luku 6', t: ''},
    {a:1, t: ''},
    {a:1, n:'18', o:'Harjoitellaan', t: ''},
    {a:1, t: ''},
    {a:1, n:'19', o:'Puuttuva yhteenlaskettava', t: ''},
    {a:1, t: ''},
    {a:1, n:'20', o:'Pelaa ja testaa taitosi', t: ''},
    {a:1, t: ''},
    
    {a:0, n:'3', o:'Yhteen- ja vähennyslaskua luvut 0–10 000'},
    
    {a:1, n:'21', o:'Luku 7', t: ''},
    {a:1, t: ''},
    {a:1, n:'22', o:'Yhteenlaskun vaihdannaisuus', t: ''},
    {a:1, t: ''},
    {a:1, n:'23', o:'Laskuperhe', t: ''},
    {a:1, t: ''},
    {a:1, n:'24', o:'Luku 8', t: ''},
    {a:1, t: ''},
    {a:1, n:'25', o:'Pitkiä yhteenlaskuja', t: ''},
    {a:1, t: ''},
    {a:1, n:'26', o:'Pitkiä vähennyslaskuja', t: ''},
    {a:1, t: ''},
    {a:1, n:'27', o:'Luku 9', t: ''},
    {a:1, t: ''},
    {a:1, n:'28', o:'Yhteen- ja vähennyslaskua', t: ''},
    {a:1, t: ''},
    {a:1, n:'29', o:'Luku 10', t: ''},
    {a:1, t: ''},
    {a:1, n:'30', o:'Harjoitellaan', t: ''},
    {a:1, t: ''},
    {a:1, n:'31', o:'Lukujen suuruusjärjestys', t: ''},
    {a:1, t: ''},
    {a:1, n:'32', o:'Kerrataan hajotelmia', t: ''},
    {a:1, t: ''},
    {a:1, n:'33', o:'Pelaa ja testaa taitosi', t: ''},
    {a:1, t: ''},
    
    {a:0, n:'4', o:'Eurot, luvut 11 ja 12'},

    {a:1, n:'34', o:'Eurot', t: ''},
    {a:1, t: ''},
    {a:1, n:'35', o:'Ostosten hinta', t: ''},
    {a:1, t: ''},
    {a:1, n:'36', o:'Rahaa jää', t: ''},
    {a:1, t: ''},
    {a:1, n:'37', o:'Hintaero', t: ''},
    {a:1, t: ''},
    {a:1, n:'38', o:'Puuttuva vähentäjä', t: ''},
    {a:1, t: ''},
    {a:1, n:'39', o:'Harjoitellaan', t: ''},
    {a:1, t: ''},
    {a:1, n:'40', o:'Luku 11', t: ''},
    {a:1, t: ''},
    {a:1, n:'41', o:'Luku 12', t: ''},
    {a:1, t: ''},
    {a:1, n:'42', o:'Pylväskuvio', t: ''},
    {a:1, t: ''},
    {a:1, n:'43', o:'Tasatunnit', t: ''},
    {a:1, t: ''},
    {a:1, n:'44', o:'Pelaa ja testaa taitosi', t: ''},
    {a:1, t: ''},
    
    {a:0, n:'5', o:'Kerbackground'},
    {a:1, n:'45', o:'Kerrataan lukujen järjestys', t: ''},
    {a:1, t: ''},
    {a:1, n:'46', o:'Harjoitellaan', t: ''},
    {a:1, t: ''},
    {a:1, n:'47', o:'Kerrataan rahalaskuja', t: ''},
    {a:1, t: ''},
    {a:1, n:'', o:'Kellopeli', t: ''}
];

var _havaintovalineet = {o:'Visual Aids', t:[
    {o:'Lukukortit ja lukumääräkortit', t:[
        {o:'Lukukortit', t: ''},
        {o:'Yhdistä', t: ''},
        {o:'Järjestä kortit', t: ''},
        {o:'Suurempi, pienempi', t: ''}]},
    {o:'Laskuhelmet', t:[
        {o:'Laskuhelmet', t: ''},
        {o:'Yhteenlaskua', t: ''},
        {o:'Vähennyslaskua', t: ''},
        {o:'Yhteen- ja vähennyslaskua', t: ''}]},
    {o:'Lukusuora', t:[
        {o:'', t: ''}]},
    {o:'Toimintapohja', t:[
        {o:'Toimintapohja', t: ''},
        {o:'Yhteenlaskua', t: ''},
        {o:'Vähennyslaskua', t: ''},
        {o:'Vertailua', t: ''}]},
    {o:'Hajotelmakone', t:[
        {o:'', t: ''}]},
    {o:'Kello', t:[
        {o:'Kello', t: ''},
        {o:'Ilmoita aika', t: ''},
        {o:'Näytä aika', t: ''}]},
    {o:'Eurot', t:[
        {o:'Eurot', t: ''},
        {o:'Kokoa rahamäärä', t: ''},
        {o:'Ilmoita rahamäärä', t: ''},
        {o:'Maksa ostos', t: ''}]},
    {o:'Kymppiparit', t:[
        {o:'', t: ''}]},
    {o:'Numeroiden kirjoitusmalli', t:[
        {o:'', t: ''}]}
]};

var _handouts = {o:'Monisteet', t:[
    /*{o:'Luku 1', t:[{o:'', t: ''}]},*/
]};
for(a=1;a<=47;a++){
    _handouts.t.push({o:'Luku '+a, t:[{o:'', t: ''+a}]});
}

var _generalHandouts = {o:'Yleishandouts', t:[
    {o:'A Lähtöleveltesti', t:[{o:'Perusosa', t: ''}]},
    {o:'B Lukumääräkortit 1-12', t:[{o:'', t: ''}]},
    {o:'C Bingopelipohjia', t:[{o:'', t: ''}]},
    {o:'D Neliösenttimetriruudukko', t:[{o:'', t: ''}]},
    {o:'E Yhteenlaskukortit 1', t:[{o:'', t: ''}]},
    {o:'F Yhteenlaskukortit 2', t:[{o:'', t: ''}]},
    {o:'G Vähennyslaskukortit 1', t:[{o:'', t: ''}]},
    {o:'H Vähennyslaskukortit 2', t:[{o:'', t: ''}]},
    {o:'I Kellonaikakortit', t:[{o:'', t: ''}]},
    {o:'J Ostoskortit', t:[{o:'', t: ''}]},
    {o:'K Rahamääräkortit', t:[{o:'', t: ''}]},
    {o:'L Tapahtumakortit', t:[{o:'', t: ''}]},
    {o:'M Muistituki yhteenlaskuun', t:[{o:'', t: ''}]},
    {o:'N Muistituki vähennyslaskuun', t:[{o:'', t: ''}]},
    {o:'Mallinumbert', t:[{o:'', t: ''}]},
    {o:'Kymppiparit', t:[{o:'', t: ''}]}
]};


var toolbarLinksTop = [];
var toolbarLinksBottom = [];
pageInfo[0].pages = Pages(pages);

function Pages(titletable){
    var a;
    
    var pages = [];
    searchPages(0, pages, 0);
    
    for(a=0;a<pages.length;a++){
        if(pages[a].length === 0){
            pages.splice(a,1);
            a--;
        }
    }
    
    
    function searchPages(level, links, index){
        var a,obj;
        for(a=index;a<titletable.length;a++){
            obj = titletable[a];
            
            if(level === 0){
                pages.push([]);
                links = pages[pages.length-1];
            }
            
            if(obj.a === level){
                if(obj.t != '' && obj.t != 'DEMO' && obj.t != undefined){
                    links.push(obj.t);
                }
                if(a+1 < titletable.length){
                    if(titletable[a+1].a > obj.a){
                        a = searchPages(titletable[a+1].a, links, a+1);   
                    }
                }
            }
            else
                return a-1;
        }
        return titletable.length;
    }


    return pages;
}