(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
           
        }]
    };  

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("105", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(19, 15);

        this.addChild(this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 265, 10);
        this.roundRect1.setTransform(0, 22);      

        //first row

        this.squarebig1 = new cjs.Shape();
        this.squarebig1.graphics.f("#008BD2").s('#706F6F').drawRect(0, 0, 54, 54, 0);
        this.squarebig1.setTransform(35, 36);

        this.trianglebig1 = new cjs.Shape();
        this.trianglebig1.graphics.f("#FFF374").s("#706F6F").ss(1.2).moveTo(24, 52).lineTo(90, 52).lineTo(56, 0).lineTo(24, 52);
        this.trianglebig1.setTransform(124, 40);

        this.circlebig1 = new cjs.Shape();
        this.circlebig1.graphics.f("#D51317").s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 27, 0, 2 * Math.PI);
        this.circlebig1.setTransform(310, 64);

        this.squaresmall1 = new cjs.Shape();
        this.squaresmall1.graphics.f("#008BD2").s('#706F6F').drawRect(0, 0, 30, 30, 0);
        this.squaresmall1.setTransform(418, 62);

        //2nd row

        this.circlesmall1 = new cjs.Shape();
        this.circlesmall1.graphics.f("#D51317").s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 15, 0, 2 * Math.PI);
        this.circlesmall1.setTransform(60, 155);

        this.trianglesmall1 = new cjs.Shape();
        this.trianglesmall1.graphics.f("#008BD2").s("#706F6F").ss(1.2).moveTo(24, 52).lineTo(60, 52).lineTo(43, 18).lineTo(24, 52);
        this.trianglesmall1.setTransform(135, 120);

        this.squaresmall2 = new cjs.Shape();
        this.squaresmall2.graphics.f("#D51317").s('#706F6F').drawRect(0, 0, 30, 30, 0);
        this.squaresmall2.setTransform(294, 140);

        this.squarebig2 = new cjs.Shape();
        this.squarebig2.graphics.f("#FFF374").s('#706F6F').drawRect(0, 0, 54, 54, 0);
        this.squarebig2.setTransform(405, 120);

        //3rd row

        this.circlebig2 = new cjs.Shape();
        this.circlebig2.graphics.f("#008BD2").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 27, 0, 2 * Math.PI);
        this.circlebig2.setTransform(60, 230);

        this.circlesmall2 = new cjs.Shape();
        this.circlesmall2.graphics.f("#FFF374").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 15, 0, 2 * Math.PI);
        this.circlesmall2.setTransform(179, 229);

        this.trianglebig2 = new cjs.Shape();
        this.trianglebig2.graphics.f("#008BD2").s("#000000").ss(1.2).moveTo(24, 52).lineTo(90, 52).lineTo(56, 0).lineTo(24, 52);
        this.trianglebig2.setTransform(253, 204);

        this.trianglesmall2 = new cjs.Shape();
        this.trianglesmall2.graphics.f("#D51317").s("#000000").ss(1.2).moveTo(24, 52).lineTo(60, 52).lineTo(43, 18).lineTo(24, 52);
        this.trianglesmall2.setTransform(390, 197);

        var TArr = [];
        var TxtArr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
        var TxtlineX = [54, 173, 302, 427, 55, 173, 302, 427, 57, 175, 303, 427];
        var TxtlineY = [109, 109, 109, 109, 192, 192, 192, 192, 275, 275, 275, 274];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        this.addChild(this.roundRect1, this.instance, this.squarebig1, this.squarebig2, this.squaresmall1, this.squaresmall2,
            this.trianglebig1, this.trianglebig2, this.trianglesmall1, this.trianglesmall2, this.circlebig1, this.circlebig2, this.circlesmall1, this.circlesmall2);
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 280);

    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(0, 7);

        this.text_1 = new cjs.Text(" Hitta formerna och skriv bokstäverna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 101, 10);
        this.roundRect1.setTransform(0, 22);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 70);
        this.vrLine_1.setTransform(243, 39);

        var TArr = [];
        var TxtArr = ['små trianglar', 'stora trianglar', 'små cirklar', 'röda cirklar', 'blå fyrhörningar', 'blå trianglar',
            ',', ',', ',', ',', ',', ','
        ];

        var TxtlineX = [15, 15, 15, 266, 266, 266, 175, 175, 174, 451, 451, 450];
        var TxtlineY = [50, 78, 106, 50, 78, 106, 50, 78, 106, 50, 78, 106];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "15px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var lineArr = [];
        var lineX = [143, 143, 143, 172, 172, 172, 418, 418, 418, 448, 448, 448];
        var lineY = [12, 40, 68, 12, 40, 68, 12, 40, 68, 12, 40, 68];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(33, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.vrLine_1);
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 120);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(0, 7);

        this.text_1 = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 124, 10);
        this.roundRect1.setTransform(0, 17);

        var lineArr = [];
        var lineX = [97, 262, 421, 97, 262, 421, 97, 262, 421, 97, 262, 421];
        var lineY = [10, 10, 10, 37, 37, 37, 64, 64, 64, 93, 93, 93];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(42, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var TxtArr = ['9', '8', '9', '7', '3', '3', '5', '4', '5', '4', '6', '3',

            '11', '12', '14', '11', '4', '3', '6', '5', '5', '4', '3', '6',

            '9', '8', '9', '7', '2', '5', '4', '5', '4', '6', '5', '3',
        ];

        var TArr = [];
        var TxtlineX = [21, 21, 21, 21, 51, 51, 51, 51, 81, 81, 81, 81, 182, 182, 182, 182, 216, 216, 216, 216, 246, 246, 246, 246, 343, 343, 343, 343,
            376, 376, 376, 376, 404, 404, 404, 404
        ];
        var TxtlineY = [45, 73, 101, 128, 45, 73, 101, 128, 45, 73, 101, 128, 45, 73, 101, 128, 45, 73, 101, 128, 45, 73, 101, 128, 45, 73, 101, 128,
            45, 73, 101, 128, 45, 73, 101, 128
        ];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }


        var symblArr = ['+', '+', '+', '+', '–', '–', '–', '–', '=', '=', '=', '=', '–', '–', '–', '–', '+', '+', '+', '+', '=', '=', '=', '=', '+', '+', '+', '+', '–', '–', '–', '–', '=', '=', '=', '='];

        var SymArr = [];
        var symbllineX = [36, 36, 36, 36, 66, 66, 66, 66, 95, 95, 95, 95, 203, 203, 203, 203, 231, 231, 231, 231, 260, 260, 260, 260, 361, 361, 361,
            361, 390, 390, 390, 390, 418, 418, 418, 416
        ];
        var symbllineY = [46, 73, 101, 128, 46, 73, 101, 128, 46, 73, 101, 128, 46, 73, 101, 128, 46, 73, 101, 128, 46, 73, 101, 128, 46, 73, 101, 128,
            46, 73, 101, 128, 46, 73, 101, 128
        ];

        for (var i = 0; i < symbllineX.length; i++) {
            var symbltxt = symblArr[i];
            this.temp_label = new cjs.Text(symbltxt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(symbllineX[i], symbllineY[i]);
            SymArr.push(this.temp_label);
        }
        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

        for (var i = 0; i < SymArr.length; i++) {
            this.addChild(SymArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 140);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(320, 80, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(320, 387, 1, 1, 0, 0, 0, 255.8, 38);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(320, 530, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2, this.v3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
