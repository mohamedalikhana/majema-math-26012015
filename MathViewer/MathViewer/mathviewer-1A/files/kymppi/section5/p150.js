(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p150_1.png",
            id: "p150_1"
        }, {
            src: "images/p150_2.png",
            id: "p150_2"
        }]
    };

    // symbols:
    (lib.p150_1 = function() {
        this.initialize(img.p150_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p150_2 = function() {
        this.initialize(img.p150_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("150", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(35, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.7).drawRect(449 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Färglägg halsbandet efter instruktionen.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(6, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol1 = function() {
        this.initialize();

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D83770");
        this.text.lineHeight = 20;
        this.text.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 510.5, 206, 10);
        this.roundRect1.setTransform(5, 15);

        var ToBeAdded = [];

        var arrText = ['3', '2', '4', '3', '2', '4', '2'];
        var arrcolor = ['#1CB0E7', '#E7544E', '#FFF23C', '#20B14A', '#1CB0E7', '#E7544E', '#FFF23C'];

        for (var col = 0; col < 7; col++) {

            var shape_circ = new cjs.Shape(); // blue circle
            shape_circ.graphics.f(arrcolor[col]).s("#000000").ss(0.8, 0, 0, 4).arc(68, 59, 8, 0, 2 * Math.PI);
            shape_circ.setTransform(0, 0 + (col * 24));
            ToBeAdded.push(shape_circ);
            var temptext = new cjs.Text(arrText[col], "16px 'Myriad Pro'");
            temptext.lineHeight = -1;
            temptext.setTransform(40, 52 + (col * 24));
            ToBeAdded.push(temptext);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            var height = (column == 0) ? 181 : 134;
            for (var row = 0; row < 1; row++) {
                var rowSpace = row;
                this.textbox_group1.graphics.f('#ffffff').s("#D83770").ss(0.8).drawRect(30 + (columnSpace * 258), 44 + (rowSpace * 22), 60, height);
            }
        }
        this.textbox_group1.setTransform(0, -4);

        this.instance = new lib.p150_1();
        this.instance.setTransform(150, 44, 0.47, 0.47);

        this.line_2 = new cjs.Shape();
        this.line_2.graphics.s("#000000").f("#000000").ss(0.8).moveTo(24, 44).lineTo(24, 64).lineTo(26, 63).lineTo(24, 69).lineTo(22, 63).lineTo(24, 64);
        this.line_2.setTransform(75, -3);

        this.line_3 = new cjs.Shape();
        this.line_3.graphics.s("#000000").f("#000000").ss(0.8).moveTo(179, 47).lineTo(195, 47);
        this.line_3.setTransform(0, 0);

        this.shape_arrow2 = new cjs.Shape();
        this.shape_arrow2.graphics.f("#000000").s().p("AgegVIA9AVIg9AXg");
        this.shape_arrow2.setTransform(197, 47, 1, 1, 0);

        this.addChild(this.instance_2, this.text, this.roundRect1, this.instance, this.textbox_group1, this.shape_group1, this.line_1, this.line_2, this.line_3, this.shape_arrow2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 5, 510, 298, 10);
        this.roundRect1.setTransform(0, 20);

        this.text = new cjs.Text("Dra streck mellan bilderna i rätt ordning.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#D83770");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p150_2();
        this.instance.setTransform(-31, 26, 0.47, 0.47);

        this.text_2 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_2.lineHeight = 35;
        this.text_2.setTransform(198.3, 94.7);
        this.text_3 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_3.lineHeight = 35;
        this.text_3.setTransform(279.3, 94.7);
        this.text_4 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_4.lineHeight = 35;
        this.text_4.setTransform(143.3, 152.7);
        this.text_5 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_5.lineHeight = 35;
        this.text_5.setTransform(336.3, 152.7);
        this.text_6 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_6.lineHeight = 35;
        this.text_6.setTransform(143.3, 233.7);
        this.text_7 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_7.lineHeight = 35;
        this.text_7.setTransform(336.3, 233.7);
        this.text_8 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_8.lineHeight = 35;
        this.text_8.setTransform(198.5, 289.7);
        this.text_9 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_9.lineHeight = 35;
        this.text_9.setTransform(279.3, 289.7);

        this.line_1 = new cjs.Shape();
        this.line_1.graphics.s("#DA2129").f("#DA2129").ss(0.8).moveTo(50, 53).lineTo(62, 53).lineTo(62, 50).lineTo(66, 53).lineTo(62, 56).lineTo(62, 53)
            .moveTo(102, 53).lineTo(114, 53).lineTo(114, 50).lineTo(118, 53).lineTo(114, 56).lineTo(114, 53)
            .moveTo(152, 53).lineTo(164, 53).lineTo(164, 50).lineTo(168, 53).lineTo(164, 56).lineTo(164, 53)
            .moveTo(206, 53).lineTo(218, 53).lineTo(218, 50).lineTo(222, 53).lineTo(218, 56).lineTo(218, 53)
            .moveTo(262, 53).lineTo(274, 53).lineTo(274, 50).lineTo(278, 53).lineTo(274, 56).lineTo(274, 53)
            .moveTo(312, 53).lineTo(324, 53).lineTo(324, 50).lineTo(328, 53).lineTo(324, 56).lineTo(324, 53)
            .moveTo(371, 53).lineTo(383, 53).lineTo(383, 50).lineTo(387, 53).lineTo(383, 56).lineTo(383, 53)
            .moveTo(425, 53).lineTo(437, 53).lineTo(437, 50).lineTo(441, 53).lineTo(437, 56).lineTo(437, 53);
        this.line_1.setTransform(0, 0);






        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.line_1);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 330.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(290, 105, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296, 350, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
