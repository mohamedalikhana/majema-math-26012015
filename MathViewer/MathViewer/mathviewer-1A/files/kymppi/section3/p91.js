(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p91_1.png",
            id: "p91_1"
        }, {
            src: "images/p91_2.png",
            id: "p91_2"
        }]
    };

    (lib.p91_1 = function() {
        this.initialize(img.p91_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p91_2 = function() {
        this.initialize(img.p91_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("91", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(537, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(560, 660.8);

        this.instance = new lib.p91_1();
        this.instance.setTransform(33, 8, 0.78, 0.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(450 + (columnSpace * 32.5), 26, 27.5, 27.5);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många finns kvar?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20, 2);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 1);

        this.instance_2 = new lib.p91_2();
        this.instance_2.setTransform(25, 58, 0.378, 0.38);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 121, 10);
        this.roundRect1.setTransform(0, 17);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 17);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 143);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 143);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 269);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 269);

        var arrTxtbox = ['116', '243', '369'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 140,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 7,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 52.5;
                if (i == 1) {
                    padding = 87.5;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("7", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(53, 106.2);

        this.text_2 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(73.5, 106.2);

        this.text_3 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(93, 106.2);

        this.text_4 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(113.5, 106.2);

        this.text_5 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(133, 106.2);

        this.text_6 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 40;
        this.text_6.setTransform(154, 106.2);

        var ToBeAdded = [];
        var arryPos = ['35', '161', '287'];
        var arrxPos = ['30', '288'];
        for (var i = 0; i < arryPos.length; i++) {
            var yPos = parseInt(arryPos[i]);
            var text = '';

            for (var j = 0; j < arrxPos.length; j++) {
                var xPos = parseInt(arrxPos[j]);
                if (i == 0) {
                    if (j == 0) {
                        text = 'Först tar jag 2 och sedan 3.';
                    } else {
                        text = 'Först tar jag 3 och sedan 3.';
                    }
                } else if (i == 1) {
                    if (j == 0) {
                        text = 'Först tar jag 2 och sedan 2.';
                    } else {
                        text = 'Först tar jag 2 och sedan 3.';
                    }
                } else if (i == 2) {
                    if (j == 0) {
                        text = 'Först tar jag 2 och sedan 4.';
                    } else {
                        text = 'Först tar jag 5 och sedan 3.';
                    }
                }

                var tempLabel = new cjs.Text(text, "16px 'Myriad Pro'");
                tempLabel.lineHeight = -1;
                tempLabel.setTransform(xPos, yPos);
                ToBeAdded.push(tempLabel);
            }
        }

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(628.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(311, 455, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
