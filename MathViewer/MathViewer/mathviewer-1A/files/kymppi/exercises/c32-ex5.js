var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };
    var transformX = transformY = 0.185;
    var moveX = 485 + 64,
        moveY = 4;
    var startX = ((902.0052) * transformX) + moveX,
        startY = ((0.0000) * transformY) + moveY;
    var endX = ((667.0000) * transformX) + moveX,
        endY = ((1900.0000) * transformY) + moveY;
    var pencil = null;

    var startX2 = ((667.5555) * transformX) + moveX,
        startY2 = ((1900.0000) * transformY) + moveY,
        endX2 = ((667.0000) * transformX) + moveX,
        endY2 = ((1900.0000) * transformY) + moveY;
    var altStartX2 = startX,
        altStartY2 = startY,
        altEndX2 = endX,
        altEndY2 = endY;
    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [
        new createjs.Point(((902.0052) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((849.9948) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY),

        new createjs.Point(((781.3350) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((764.6650) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY),

        new createjs.Point(((739.3342) * transformX) + moveX, ((0.3333) * transformY) + moveY),
        new createjs.Point(((730.6658) * transformX) + moveX, ((0.6667) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((1.0000) * transformY) + moveY),

        new createjs.Point(((722.0000) * transformX) + moveX, ((1.3333) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((1.6667) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((714.6674) * transformX) + moveX, ((2.3333) * transformY) + moveY),
        new createjs.Point(((707.3326) * transformX) + moveX, ((2.6667) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((681.0019) * transformX) + moveX, ((5.9997) * transformY) + moveY),
        new createjs.Point(((661.9981) * transformX) + moveX, ((9.0003) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((643.0000) * transformX) + moveX, ((12.3333) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((12.6667) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((13.0000) * transformY) + moveY),

        new createjs.Point(((641.6668) * transformX) + moveX, ((13.0000) * transformY) + moveY),
        new createjs.Point(((640.3332) * transformX) + moveX, ((13.0000) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((13.0000) * transformY) + moveY),

        new createjs.Point(((639.0000) * transformX) + moveX, ((13.3333) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((13.6667) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((14.0000) * transformY) + moveY),

        new createjs.Point(((637.3335) * transformX) + moveX, ((14.0000) * transformY) + moveY),
        new createjs.Point(((635.6665) * transformX) + moveX, ((14.0000) * transformY) + moveY),
        new createjs.Point(((634.0000) * transformX) + moveX, ((14.0000) * transformY) + moveY),

        new createjs.Point(((634.0000) * transformX) + moveX, ((14.3333) * transformY) + moveY),
        new createjs.Point(((634.0000) * transformX) + moveX, ((14.6667) * transformY) + moveY),
        new createjs.Point(((634.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((632.6668) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((631.3332) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((630.0000) * transformX) + moveX, ((15.3333) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((15.6667) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((628.6668) * transformX) + moveX, ((16.0000) * transformY) + moveY),
        new createjs.Point(((627.3332) * transformX) + moveX, ((16.0000) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((626.0000) * transformX) + moveX, ((16.3333) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((16.6667) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((17.0000) * transformY) + moveY),

        new createjs.Point(((623.0003) * transformX) + moveX, ((17.3333) * transformY) + moveY),
        new createjs.Point(((619.9997) * transformX) + moveX, ((17.6667) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((617.0000) * transformX) + moveX, ((18.3333) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((18.6667) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((19.0000) * transformY) + moveY),

        new createjs.Point(((613.3337) * transformX) + moveX, ((19.6666) * transformY) + moveY),
        new createjs.Point(((609.6663) * transformX) + moveX, ((20.3334) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((606.0000) * transformX) + moveX, ((21.3333) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((21.6667) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((603.6669) * transformX) + moveX, ((22.3333) * transformY) + moveY),
        new createjs.Point(((601.3331) * transformX) + moveX, ((22.6667) * transformY) + moveY),
        new createjs.Point(((599.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((599.0000) * transformX) + moveX, ((23.3333) * transformY) + moveY),
        new createjs.Point(((599.0000) * transformX) + moveX, ((23.6667) * transformY) + moveY),
        new createjs.Point(((599.0000) * transformX) + moveX, ((24.0000) * transformY) + moveY),

        new createjs.Point(((598.0001) * transformX) + moveX, ((24.0000) * transformY) + moveY),
        new createjs.Point(((596.9999) * transformX) + moveX, ((24.0000) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((24.0000) * transformY) + moveY),

        new createjs.Point(((596.0000) * transformX) + moveX, ((24.3333) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((24.6667) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((25.0000) * transformY) + moveY),

        new createjs.Point(((592.6670) * transformX) + moveX, ((25.6666) * transformY) + moveY),
        new createjs.Point(((589.3330) * transformX) + moveX, ((26.3334) * transformY) + moveY),
        new createjs.Point(((586.0000) * transformX) + moveX, ((27.0000) * transformY) + moveY),

        new createjs.Point(((586.0000) * transformX) + moveX, ((27.3333) * transformY) + moveY),
        new createjs.Point(((586.0000) * transformX) + moveX, ((27.6667) * transformY) + moveY),
        new createjs.Point(((586.0000) * transformX) + moveX, ((28.0000) * transformY) + moveY),

        new createjs.Point(((584.6668) * transformX) + moveX, ((28.0000) * transformY) + moveY),
        new createjs.Point(((583.3332) * transformX) + moveX, ((28.0000) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((28.0000) * transformY) + moveY),

        new createjs.Point(((582.0000) * transformX) + moveX, ((28.3333) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((28.6667) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((581.0001) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((579.9999) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((579.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((579.0000) * transformX) + moveX, ((29.3333) * transformY) + moveY),
        new createjs.Point(((579.0000) * transformX) + moveX, ((29.6667) * transformY) + moveY),
        new createjs.Point(((579.0000) * transformX) + moveX, ((30.0000) * transformY) + moveY),

        new createjs.Point(((578.0001) * transformX) + moveX, ((30.0000) * transformY) + moveY),
        new createjs.Point(((576.9999) * transformX) + moveX, ((30.0000) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((30.0000) * transformY) + moveY),

        new createjs.Point(((576.0000) * transformX) + moveX, ((30.3333) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((30.6667) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((575.0001) * transformX) + moveX, ((31.0000) * transformY) + moveY),
        new createjs.Point(((573.9999) * transformX) + moveX, ((31.0000) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((573.0000) * transformX) + moveX, ((31.3333) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((31.6667) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((32.0000) * transformY) + moveY),

        new createjs.Point(((571.0002) * transformX) + moveX, ((32.3333) * transformY) + moveY),
        new createjs.Point(((568.9998) * transformX) + moveX, ((32.6667) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((33.0000) * transformY) + moveY),

        new createjs.Point(((567.0000) * transformX) + moveX, ((33.3333) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((33.6667) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((566.3334) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((565.6666) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((565.0000) * transformX) + moveX, ((34.3333) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((34.6667) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((35.0000) * transformY) + moveY),

        new createjs.Point(((562.0003) * transformX) + moveX, ((35.6666) * transformY) + moveY),
        new createjs.Point(((558.9997) * transformX) + moveX, ((36.3334) * transformY) + moveY),
        new createjs.Point(((556.0000) * transformX) + moveX, ((37.0000) * transformY) + moveY),

        new createjs.Point(((556.0000) * transformX) + moveX, ((37.3333) * transformY) + moveY),
        new createjs.Point(((556.0000) * transformX) + moveX, ((37.6667) * transformY) + moveY),
        new createjs.Point(((556.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((555.3334) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((554.6666) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((554.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((554.0000) * transformX) + moveX, ((38.3333) * transformY) + moveY),
        new createjs.Point(((554.0000) * transformX) + moveX, ((38.6667) * transformY) + moveY),
        new createjs.Point(((554.0000) * transformX) + moveX, ((39.0000) * transformY) + moveY),

        new createjs.Point(((552.0002) * transformX) + moveX, ((39.3333) * transformY) + moveY),
        new createjs.Point(((549.9998) * transformX) + moveX, ((39.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((40.0000) * transformY) + moveY),

        new createjs.Point(((548.0000) * transformX) + moveX, ((40.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((40.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((41.0000) * transformY) + moveY),

        new createjs.Point(((547.3334) * transformX) + moveX, ((41.0000) * transformY) + moveY),
        new createjs.Point(((546.6666) * transformX) + moveX, ((41.0000) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((41.0000) * transformY) + moveY),

        new createjs.Point(((546.0000) * transformX) + moveX, ((41.3333) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((41.6667) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((545.0001) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((543.9999) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((543.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((543.0000) * transformX) + moveX, ((42.3333) * transformY) + moveY),
        new createjs.Point(((543.0000) * transformX) + moveX, ((42.6667) * transformY) + moveY),
        new createjs.Point(((543.0000) * transformX) + moveX, ((43.0000) * transformY) + moveY),

        new createjs.Point(((542.3334) * transformX) + moveX, ((43.0000) * transformY) + moveY),
        new createjs.Point(((541.6666) * transformX) + moveX, ((43.0000) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((43.0000) * transformY) + moveY),

        new createjs.Point(((541.0000) * transformX) + moveX, ((43.3333) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((43.6667) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((44.0000) * transformY) + moveY),

        new createjs.Point(((540.0001) * transformX) + moveX, ((44.0000) * transformY) + moveY),
        new createjs.Point(((538.9999) * transformX) + moveX, ((44.0000) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((44.0000) * transformY) + moveY),

        new createjs.Point(((538.0000) * transformX) + moveX, ((44.3333) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((44.6667) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((45.0000) * transformY) + moveY),

        new createjs.Point(((537.3334) * transformX) + moveX, ((45.0000) * transformY) + moveY),
        new createjs.Point(((536.6666) * transformX) + moveX, ((45.0000) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((45.0000) * transformY) + moveY),

        new createjs.Point(((536.0000) * transformX) + moveX, ((45.3333) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((45.6667) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((535.0001) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((533.9999) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((533.0000) * transformX) + moveX, ((46.3333) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((46.6667) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((531.6668) * transformX) + moveX, ((47.3333) * transformY) + moveY),
        new createjs.Point(((530.3332) * transformX) + moveX, ((47.6667) * transformY) + moveY),
        new createjs.Point(((529.0000) * transformX) + moveX, ((48.0000) * transformY) + moveY),

        new createjs.Point(((529.0000) * transformX) + moveX, ((48.3333) * transformY) + moveY),
        new createjs.Point(((529.0000) * transformX) + moveX, ((48.6667) * transformY) + moveY),
        new createjs.Point(((529.0000) * transformX) + moveX, ((49.0000) * transformY) + moveY),

        new createjs.Point(((528.0001) * transformX) + moveX, ((49.0000) * transformY) + moveY),
        new createjs.Point(((526.9999) * transformX) + moveX, ((49.0000) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((49.0000) * transformY) + moveY),

        new createjs.Point(((526.0000) * transformX) + moveX, ((49.3333) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((49.6667) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((50.0000) * transformY) + moveY),

        new createjs.Point(((524.6668) * transformX) + moveX, ((50.3333) * transformY) + moveY),
        new createjs.Point(((523.3332) * transformX) + moveX, ((50.6667) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((51.0000) * transformY) + moveY),

        new createjs.Point(((522.0000) * transformX) + moveX, ((51.3333) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((51.6667) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((52.0000) * transformY) + moveY),

        new createjs.Point(((521.0001) * transformX) + moveX, ((52.0000) * transformY) + moveY),
        new createjs.Point(((519.9999) * transformX) + moveX, ((52.0000) * transformY) + moveY),
        new createjs.Point(((519.0000) * transformX) + moveX, ((52.0000) * transformY) + moveY),

        new createjs.Point(((519.0000) * transformX) + moveX, ((52.3333) * transformY) + moveY),
        new createjs.Point(((519.0000) * transformX) + moveX, ((52.6667) * transformY) + moveY),
        new createjs.Point(((519.0000) * transformX) + moveX, ((53.0000) * transformY) + moveY),

        new createjs.Point(((518.3334) * transformX) + moveX, ((53.0000) * transformY) + moveY),
        new createjs.Point(((517.6666) * transformX) + moveX, ((53.0000) * transformY) + moveY),
        new createjs.Point(((517.0000) * transformX) + moveX, ((53.0000) * transformY) + moveY),

        new createjs.Point(((517.0000) * transformX) + moveX, ((53.3333) * transformY) + moveY),
        new createjs.Point(((517.0000) * transformX) + moveX, ((53.6667) * transformY) + moveY),
        new createjs.Point(((517.0000) * transformX) + moveX, ((54.0000) * transformY) + moveY),

        new createjs.Point(((516.3334) * transformX) + moveX, ((54.0000) * transformY) + moveY),
        new createjs.Point(((515.6666) * transformX) + moveX, ((54.0000) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((54.0000) * transformY) + moveY),

        new createjs.Point(((515.0000) * transformX) + moveX, ((54.3333) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((54.6667) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((55.0000) * transformY) + moveY),

        new createjs.Point(((513.0002) * transformX) + moveX, ((55.6666) * transformY) + moveY),
        new createjs.Point(((510.9998) * transformX) + moveX, ((56.3334) * transformY) + moveY),
        new createjs.Point(((509.0000) * transformX) + moveX, ((57.0000) * transformY) + moveY),

        new createjs.Point(((509.0000) * transformX) + moveX, ((57.3333) * transformY) + moveY),
        new createjs.Point(((509.0000) * transformX) + moveX, ((57.6667) * transformY) + moveY),
        new createjs.Point(((509.0000) * transformX) + moveX, ((58.0000) * transformY) + moveY),

        new createjs.Point(((508.0001) * transformX) + moveX, ((58.0000) * transformY) + moveY),
        new createjs.Point(((506.9999) * transformX) + moveX, ((58.0000) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((58.0000) * transformY) + moveY),

        new createjs.Point(((506.0000) * transformX) + moveX, ((58.3333) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((58.6667) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((505.3334) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((504.6666) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((504.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((504.0000) * transformX) + moveX, ((59.3333) * transformY) + moveY),
        new createjs.Point(((504.0000) * transformX) + moveX, ((59.6667) * transformY) + moveY),
        new createjs.Point(((504.0000) * transformX) + moveX, ((60.0000) * transformY) + moveY),

        new createjs.Point(((502.6668) * transformX) + moveX, ((60.3333) * transformY) + moveY),
        new createjs.Point(((501.3332) * transformX) + moveX, ((60.6667) * transformY) + moveY),
        new createjs.Point(((500.0000) * transformX) + moveX, ((61.0000) * transformY) + moveY),

        new createjs.Point(((500.0000) * transformX) + moveX, ((61.3333) * transformY) + moveY),
        new createjs.Point(((500.0000) * transformX) + moveX, ((61.6667) * transformY) + moveY),
        new createjs.Point(((500.0000) * transformX) + moveX, ((62.0000) * transformY) + moveY),

        new createjs.Point(((499.3334) * transformX) + moveX, ((62.0000) * transformY) + moveY),
        new createjs.Point(((498.6666) * transformX) + moveX, ((62.0000) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((62.0000) * transformY) + moveY),

        new createjs.Point(((498.0000) * transformX) + moveX, ((62.3333) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((62.6667) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((63.0000) * transformY) + moveY),

        new createjs.Point(((497.3334) * transformX) + moveX, ((63.0000) * transformY) + moveY),
        new createjs.Point(((496.6666) * transformX) + moveX, ((63.0000) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((63.0000) * transformY) + moveY),

        new createjs.Point(((496.0000) * transformX) + moveX, ((63.3333) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((63.6667) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((64.0000) * transformY) + moveY),

        new createjs.Point(((495.3334) * transformX) + moveX, ((64.0000) * transformY) + moveY),
        new createjs.Point(((494.6666) * transformX) + moveX, ((64.0000) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((64.0000) * transformY) + moveY),

        new createjs.Point(((494.0000) * transformX) + moveX, ((64.3333) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((64.6667) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((65.0000) * transformY) + moveY),

        new createjs.Point(((493.3334) * transformX) + moveX, ((65.0000) * transformY) + moveY),
        new createjs.Point(((492.6666) * transformX) + moveX, ((65.0000) * transformY) + moveY),
        new createjs.Point(((492.0000) * transformX) + moveX, ((65.0000) * transformY) + moveY),

        new createjs.Point(((492.0000) * transformX) + moveX, ((65.3333) * transformY) + moveY),
        new createjs.Point(((492.0000) * transformX) + moveX, ((65.6667) * transformY) + moveY),
        new createjs.Point(((492.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((491.3334) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((490.6666) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((490.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((490.0000) * transformX) + moveX, ((66.3333) * transformY) + moveY),
        new createjs.Point(((490.0000) * transformX) + moveX, ((66.6667) * transformY) + moveY),
        new createjs.Point(((490.0000) * transformX) + moveX, ((67.0000) * transformY) + moveY),

        new createjs.Point(((489.3334) * transformX) + moveX, ((67.0000) * transformY) + moveY),
        new createjs.Point(((488.6666) * transformX) + moveX, ((67.0000) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((67.0000) * transformY) + moveY),

        new createjs.Point(((488.0000) * transformX) + moveX, ((67.3333) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((67.6667) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((68.0000) * transformY) + moveY),

        new createjs.Point(((487.3334) * transformX) + moveX, ((68.0000) * transformY) + moveY),
        new createjs.Point(((486.6666) * transformX) + moveX, ((68.0000) * transformY) + moveY),
        new createjs.Point(((486.0000) * transformX) + moveX, ((68.0000) * transformY) + moveY),

        new createjs.Point(((486.0000) * transformX) + moveX, ((68.3333) * transformY) + moveY),
        new createjs.Point(((486.0000) * transformX) + moveX, ((68.6667) * transformY) + moveY),
        new createjs.Point(((486.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((485.3334) * transformX) + moveX, ((69.0000) * transformY) + moveY),
        new createjs.Point(((484.6666) * transformX) + moveX, ((69.0000) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((484.0000) * transformX) + moveX, ((69.3333) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((69.6667) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((70.0000) * transformY) + moveY),

        new createjs.Point(((483.3334) * transformX) + moveX, ((70.0000) * transformY) + moveY),
        new createjs.Point(((482.6666) * transformX) + moveX, ((70.0000) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((70.0000) * transformY) + moveY),

        new createjs.Point(((482.0000) * transformX) + moveX, ((70.3333) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((70.6667) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((71.0000) * transformY) + moveY),

        new createjs.Point(((481.3334) * transformX) + moveX, ((71.0000) * transformY) + moveY),
        new createjs.Point(((480.6666) * transformX) + moveX, ((71.0000) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((71.0000) * transformY) + moveY),

        new createjs.Point(((480.0000) * transformX) + moveX, ((71.3333) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((71.6667) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((72.0000) * transformY) + moveY),

        new createjs.Point(((479.3334) * transformX) + moveX, ((72.0000) * transformY) + moveY),
        new createjs.Point(((478.6666) * transformX) + moveX, ((72.0000) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((72.0000) * transformY) + moveY),

        new createjs.Point(((477.6667) * transformX) + moveX, ((72.6666) * transformY) + moveY),
        new createjs.Point(((477.3333) * transformX) + moveX, ((73.3334) * transformY) + moveY),
        new createjs.Point(((477.0000) * transformX) + moveX, ((74.0000) * transformY) + moveY),

        new createjs.Point(((476.3334) * transformX) + moveX, ((74.0000) * transformY) + moveY),
        new createjs.Point(((475.6666) * transformX) + moveX, ((74.0000) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((74.0000) * transformY) + moveY),

        new createjs.Point(((475.0000) * transformX) + moveX, ((74.3333) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((74.6667) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((75.0000) * transformY) + moveY),

        new createjs.Point(((474.3334) * transformX) + moveX, ((75.0000) * transformY) + moveY),
        new createjs.Point(((473.6666) * transformX) + moveX, ((75.0000) * transformY) + moveY),
        new createjs.Point(((473.0000) * transformX) + moveX, ((75.0000) * transformY) + moveY),

        new createjs.Point(((473.0000) * transformX) + moveX, ((75.3333) * transformY) + moveY),
        new createjs.Point(((473.0000) * transformX) + moveX, ((75.6667) * transformY) + moveY),
        new createjs.Point(((473.0000) * transformX) + moveX, ((76.0000) * transformY) + moveY),

        new createjs.Point(((472.3334) * transformX) + moveX, ((76.0000) * transformY) + moveY),
        new createjs.Point(((471.6666) * transformX) + moveX, ((76.0000) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((76.0000) * transformY) + moveY),

        new createjs.Point(((471.0000) * transformX) + moveX, ((76.3333) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((76.6667) * transformY) + moveY),
        new createjs.Point(((471.0000) * transformX) + moveX, ((77.0000) * transformY) + moveY),

        new createjs.Point(((470.3334) * transformX) + moveX, ((77.0000) * transformY) + moveY),
        new createjs.Point(((469.6666) * transformX) + moveX, ((77.0000) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((77.0000) * transformY) + moveY),

        new createjs.Point(((469.0000) * transformX) + moveX, ((77.3333) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((77.6667) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((468.3334) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((467.6666) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((466.6667) * transformX) + moveX, ((78.6666) * transformY) + moveY),
        new createjs.Point(((466.3333) * transformX) + moveX, ((79.3334) * transformY) + moveY),
        new createjs.Point(((466.0000) * transformX) + moveX, ((80.0000) * transformY) + moveY),

        new createjs.Point(((464.0002) * transformX) + moveX, ((80.6666) * transformY) + moveY),
        new createjs.Point(((461.9998) * transformX) + moveX, ((81.3334) * transformY) + moveY),
        new createjs.Point(((460.0000) * transformX) + moveX, ((82.0000) * transformY) + moveY),

        new createjs.Point(((459.6667) * transformX) + moveX, ((82.6666) * transformY) + moveY),
        new createjs.Point(((459.3333) * transformX) + moveX, ((83.3334) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),

        new createjs.Point(((457.0002) * transformX) + moveX, ((84.6666) * transformY) + moveY),
        new createjs.Point(((454.9998) * transformX) + moveX, ((85.3334) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((86.0000) * transformY) + moveY),

        new createjs.Point(((452.6667) * transformX) + moveX, ((86.6666) * transformY) + moveY),
        new createjs.Point(((452.3333) * transformX) + moveX, ((87.3334) * transformY) + moveY),
        new createjs.Point(((452.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),

        new createjs.Point(((450.6668) * transformX) + moveX, ((88.3333) * transformY) + moveY),
        new createjs.Point(((449.3332) * transformX) + moveX, ((88.6667) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((89.0000) * transformY) + moveY),

        new createjs.Point(((447.6667) * transformX) + moveX, ((89.6666) * transformY) + moveY),
        new createjs.Point(((447.3333) * transformX) + moveX, ((90.3334) * transformY) + moveY),
        new createjs.Point(((447.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),

        new createjs.Point(((445.6668) * transformX) + moveX, ((91.3333) * transformY) + moveY),
        new createjs.Point(((444.3332) * transformX) + moveX, ((91.6667) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((92.0000) * transformY) + moveY),

        new createjs.Point(((443.0000) * transformX) + moveX, ((92.3333) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((92.6667) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((93.0000) * transformY) + moveY),

        new createjs.Point(((442.0001) * transformX) + moveX, ((93.3333) * transformY) + moveY),
        new createjs.Point(((440.9999) * transformX) + moveX, ((93.6667) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((439.6667) * transformX) + moveX, ((94.6666) * transformY) + moveY),
        new createjs.Point(((439.3333) * transformX) + moveX, ((95.3334) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((96.0000) * transformY) + moveY),

        new createjs.Point(((437.6668) * transformX) + moveX, ((96.3333) * transformY) + moveY),
        new createjs.Point(((436.3332) * transformX) + moveX, ((96.6667) * transformY) + moveY),
        new createjs.Point(((435.0000) * transformX) + moveX, ((97.0000) * transformY) + moveY),

        new createjs.Point(((434.6667) * transformX) + moveX, ((97.6666) * transformY) + moveY),
        new createjs.Point(((434.3333) * transformX) + moveX, ((98.3334) * transformY) + moveY),
        new createjs.Point(((434.0000) * transformX) + moveX, ((99.0000) * transformY) + moveY),

        new createjs.Point(((433.3334) * transformX) + moveX, ((99.0000) * transformY) + moveY),
        new createjs.Point(((432.6666) * transformX) + moveX, ((99.0000) * transformY) + moveY),
        new createjs.Point(((432.0000) * transformX) + moveX, ((99.0000) * transformY) + moveY),

        new createjs.Point(((431.6667) * transformX) + moveX, ((99.6666) * transformY) + moveY),
        new createjs.Point(((431.3333) * transformX) + moveX, ((100.3334) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),

        new createjs.Point(((430.3334) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((429.6666) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),

        new createjs.Point(((428.6667) * transformX) + moveX, ((101.6666) * transformY) + moveY),
        new createjs.Point(((428.3333) * transformX) + moveX, ((102.3334) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),

        new createjs.Point(((427.3334) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((426.6666) * transformX) + moveX, ((103.0000) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((103.0000) * transformY) + moveY),

        new createjs.Point(((425.6667) * transformX) + moveX, ((103.6666) * transformY) + moveY),
        new createjs.Point(((425.3333) * transformX) + moveX, ((104.3334) * transformY) + moveY),
        new createjs.Point(((425.0000) * transformX) + moveX, ((105.0000) * transformY) + moveY),

        new createjs.Point(((424.3334) * transformX) + moveX, ((105.0000) * transformY) + moveY),
        new createjs.Point(((423.6666) * transformX) + moveX, ((105.0000) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((105.0000) * transformY) + moveY),

        new createjs.Point(((422.6667) * transformX) + moveX, ((105.6666) * transformY) + moveY),
        new createjs.Point(((422.3333) * transformX) + moveX, ((106.3334) * transformY) + moveY),
        new createjs.Point(((422.0000) * transformX) + moveX, ((107.0000) * transformY) + moveY),

        new createjs.Point(((421.3334) * transformX) + moveX, ((107.0000) * transformY) + moveY),
        new createjs.Point(((420.6666) * transformX) + moveX, ((107.0000) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((107.0000) * transformY) + moveY),

        new createjs.Point(((420.0000) * transformX) + moveX, ((107.3333) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((107.6667) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((108.0000) * transformY) + moveY),

        new createjs.Point(((419.0001) * transformX) + moveX, ((108.3333) * transformY) + moveY),
        new createjs.Point(((417.9999) * transformX) + moveX, ((108.6667) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((109.0000) * transformY) + moveY),

        new createjs.Point(((416.6667) * transformX) + moveX, ((109.6666) * transformY) + moveY),
        new createjs.Point(((416.3333) * transformX) + moveX, ((110.3334) * transformY) + moveY),
        new createjs.Point(((416.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),

        new createjs.Point(((415.3334) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((414.6666) * transformX) + moveX, ((111.0000) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),

        new createjs.Point(((414.0000) * transformX) + moveX, ((111.3333) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((111.6667) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((112.0000) * transformY) + moveY),

        new createjs.Point(((413.0001) * transformX) + moveX, ((112.3333) * transformY) + moveY),
        new createjs.Point(((411.9999) * transformX) + moveX, ((112.6667) * transformY) + moveY),
        new createjs.Point(((411.0000) * transformX) + moveX, ((113.0000) * transformY) + moveY),

        new createjs.Point(((410.6667) * transformX) + moveX, ((113.6666) * transformY) + moveY),
        new createjs.Point(((410.3333) * transformX) + moveX, ((114.3334) * transformY) + moveY),
        new createjs.Point(((410.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),

        new createjs.Point(((409.3334) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((408.6666) * transformX) + moveX, ((115.0000) * transformY) + moveY),
        new createjs.Point(((408.0000) * transformX) + moveX, ((115.0000) * transformY) + moveY),

        new createjs.Point(((407.3334) * transformX) + moveX, ((115.9999) * transformY) + moveY),
        new createjs.Point(((406.6666) * transformX) + moveX, ((117.0001) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((118.0000) * transformY) + moveY),

        new createjs.Point(((405.3334) * transformX) + moveX, ((118.0000) * transformY) + moveY),
        new createjs.Point(((404.6666) * transformX) + moveX, ((118.0000) * transformY) + moveY),
        new createjs.Point(((404.0000) * transformX) + moveX, ((118.0000) * transformY) + moveY),

        new createjs.Point(((403.6667) * transformX) + moveX, ((118.6666) * transformY) + moveY),
        new createjs.Point(((403.3333) * transformX) + moveX, ((119.3334) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((120.0000) * transformY) + moveY),

        new createjs.Point(((402.3334) * transformX) + moveX, ((120.0000) * transformY) + moveY),
        new createjs.Point(((401.6666) * transformX) + moveX, ((120.0000) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((120.0000) * transformY) + moveY),

        new createjs.Point(((400.6667) * transformX) + moveX, ((120.6666) * transformY) + moveY),
        new createjs.Point(((400.3333) * transformX) + moveX, ((121.3334) * transformY) + moveY),
        new createjs.Point(((400.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),

        new createjs.Point(((399.0001) * transformX) + moveX, ((122.3333) * transformY) + moveY),
        new createjs.Point(((397.9999) * transformX) + moveX, ((122.6667) * transformY) + moveY),
        new createjs.Point(((397.0000) * transformX) + moveX, ((123.0000) * transformY) + moveY),

        new createjs.Point(((396.3334) * transformX) + moveX, ((123.9999) * transformY) + moveY),
        new createjs.Point(((395.6666) * transformX) + moveX, ((125.0001) * transformY) + moveY),
        new createjs.Point(((395.0000) * transformX) + moveX, ((126.0000) * transformY) + moveY),

        new createjs.Point(((394.3334) * transformX) + moveX, ((126.0000) * transformY) + moveY),
        new createjs.Point(((393.6666) * transformX) + moveX, ((126.0000) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((126.0000) * transformY) + moveY),

        new createjs.Point(((392.3334) * transformX) + moveX, ((126.9999) * transformY) + moveY),
        new createjs.Point(((391.6666) * transformX) + moveX, ((128.0001) * transformY) + moveY),
        new createjs.Point(((391.0000) * transformX) + moveX, ((129.0000) * transformY) + moveY),

        new createjs.Point(((390.3334) * transformX) + moveX, ((129.0000) * transformY) + moveY),
        new createjs.Point(((389.6666) * transformX) + moveX, ((129.0000) * transformY) + moveY),
        new createjs.Point(((389.0000) * transformX) + moveX, ((129.0000) * transformY) + moveY),

        new createjs.Point(((388.3334) * transformX) + moveX, ((129.9999) * transformY) + moveY),
        new createjs.Point(((387.6666) * transformX) + moveX, ((131.0001) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((132.0000) * transformY) + moveY),

        new createjs.Point(((386.3334) * transformX) + moveX, ((132.0000) * transformY) + moveY),
        new createjs.Point(((385.6666) * transformX) + moveX, ((132.0000) * transformY) + moveY),
        new createjs.Point(((385.0000) * transformX) + moveX, ((132.0000) * transformY) + moveY),

        new createjs.Point(((384.3334) * transformX) + moveX, ((132.9999) * transformY) + moveY),
        new createjs.Point(((383.6666) * transformX) + moveX, ((134.0001) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((135.0000) * transformY) + moveY),

        new createjs.Point(((382.3334) * transformX) + moveX, ((135.0000) * transformY) + moveY),
        new createjs.Point(((381.6666) * transformX) + moveX, ((135.0000) * transformY) + moveY),
        new createjs.Point(((381.0000) * transformX) + moveX, ((135.0000) * transformY) + moveY),

        new createjs.Point(((380.0001) * transformX) + moveX, ((136.3332) * transformY) + moveY),
        new createjs.Point(((378.9999) * transformX) + moveX, ((137.6668) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((139.0000) * transformY) + moveY),

        new createjs.Point(((377.3334) * transformX) + moveX, ((139.0000) * transformY) + moveY),
        new createjs.Point(((376.6666) * transformX) + moveX, ((139.0000) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((139.0000) * transformY) + moveY),

        new createjs.Point(((375.0001) * transformX) + moveX, ((140.3332) * transformY) + moveY),
        new createjs.Point(((373.9999) * transformX) + moveX, ((141.6668) * transformY) + moveY),
        new createjs.Point(((373.0000) * transformX) + moveX, ((143.0000) * transformY) + moveY),

        new createjs.Point(((372.3334) * transformX) + moveX, ((143.0000) * transformY) + moveY),
        new createjs.Point(((371.6666) * transformX) + moveX, ((143.0000) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((143.0000) * transformY) + moveY),

        new createjs.Point(((370.0001) * transformX) + moveX, ((144.3332) * transformY) + moveY),
        new createjs.Point(((368.9999) * transformX) + moveX, ((145.6668) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((147.0000) * transformY) + moveY),

        new createjs.Point(((367.3334) * transformX) + moveX, ((147.0000) * transformY) + moveY),
        new createjs.Point(((366.6666) * transformX) + moveX, ((147.0000) * transformY) + moveY),
        new createjs.Point(((366.0000) * transformX) + moveX, ((147.0000) * transformY) + moveY),

        new createjs.Point(((364.6668) * transformX) + moveX, ((148.6665) * transformY) + moveY),
        new createjs.Point(((363.3332) * transformX) + moveX, ((150.3335) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((152.0000) * transformY) + moveY),

        new createjs.Point(((361.3334) * transformX) + moveX, ((152.0000) * transformY) + moveY),
        new createjs.Point(((360.6666) * transformX) + moveX, ((152.0000) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((152.0000) * transformY) + moveY),

        new createjs.Point(((358.6668) * transformX) + moveX, ((153.6665) * transformY) + moveY),
        new createjs.Point(((357.3332) * transformX) + moveX, ((155.3335) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((157.0000) * transformY) + moveY),

        new createjs.Point(((355.3334) * transformX) + moveX, ((157.0000) * transformY) + moveY),
        new createjs.Point(((354.6666) * transformX) + moveX, ((157.0000) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((157.0000) * transformY) + moveY),

        new createjs.Point(((352.3335) * transformX) + moveX, ((158.9998) * transformY) + moveY),
        new createjs.Point(((350.6665) * transformX) + moveX, ((161.0002) * transformY) + moveY),
        new createjs.Point(((349.0000) * transformX) + moveX, ((163.0000) * transformY) + moveY),

        new createjs.Point(((348.3334) * transformX) + moveX, ((163.0000) * transformY) + moveY),
        new createjs.Point(((347.6666) * transformX) + moveX, ((163.0000) * transformY) + moveY),
        new createjs.Point(((347.0000) * transformX) + moveX, ((163.0000) * transformY) + moveY),

        new createjs.Point(((345.6668) * transformX) + moveX, ((164.6665) * transformY) + moveY),
        new createjs.Point(((344.3332) * transformX) + moveX, ((166.3335) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((168.0000) * transformY) + moveY),

        new createjs.Point(((339.3337) * transformX) + moveX, ((170.9997) * transformY) + moveY),
        new createjs.Point(((335.6663) * transformX) + moveX, ((174.0003) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((177.0000) * transformY) + moveY),

        new createjs.Point(((329.0003) * transformX) + moveX, ((180.3330) * transformY) + moveY),
        new createjs.Point(((325.9997) * transformX) + moveX, ((183.6670) * transformY) + moveY),
        new createjs.Point(((323.0000) * transformX) + moveX, ((187.0000) * transformY) + moveY),

        new createjs.Point(((322.3334) * transformX) + moveX, ((187.0000) * transformY) + moveY),
        new createjs.Point(((321.6666) * transformX) + moveX, ((187.0000) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((187.0000) * transformY) + moveY),

        new createjs.Point(((318.3336) * transformX) + moveX, ((189.9997) * transformY) + moveY),
        new createjs.Point(((315.6664) * transformX) + moveX, ((193.0003) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((196.0000) * transformY) + moveY),

        new createjs.Point(((309.6670) * transformX) + moveX, ((198.9997) * transformY) + moveY),
        new createjs.Point(((306.3330) * transformX) + moveX, ((202.0003) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((205.0000) * transformY) + moveY),

        new createjs.Point(((303.0000) * transformX) + moveX, ((205.6666) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((206.3334) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((207.0000) * transformY) + moveY),

        new createjs.Point(((298.6671) * transformX) + moveX, ((210.9996) * transformY) + moveY),
        new createjs.Point(((294.3329) * transformX) + moveX, ((215.0004) * transformY) + moveY),
        new createjs.Point(((290.0000) * transformX) + moveX, ((219.0000) * transformY) + moveY),

        new createjs.Point(((290.0000) * transformX) + moveX, ((219.3333) * transformY) + moveY),
        new createjs.Point(((290.0000) * transformX) + moveX, ((219.6667) * transformY) + moveY),
        new createjs.Point(((290.0000) * transformX) + moveX, ((220.0000) * transformY) + moveY),

        new createjs.Point(((289.3334) * transformX) + moveX, ((220.3333) * transformY) + moveY),
        new createjs.Point(((288.6666) * transformX) + moveX, ((220.6667) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),

        new createjs.Point(((288.0000) * transformX) + moveX, ((221.6666) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((222.3334) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((223.0000) * transformY) + moveY),

        new createjs.Point(((285.0003) * transformX) + moveX, ((225.6664) * transformY) + moveY),
        new createjs.Point(((281.9997) * transformX) + moveX, ((228.3336) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((231.0000) * transformY) + moveY),

        new createjs.Point(((279.0000) * transformX) + moveX, ((231.6666) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((232.3334) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((233.0000) * transformY) + moveY),

        new createjs.Point(((277.0002) * transformX) + moveX, ((234.6665) * transformY) + moveY),
        new createjs.Point(((274.9998) * transformX) + moveX, ((236.3335) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),

        new createjs.Point(((273.0000) * transformX) + moveX, ((238.6666) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((239.3334) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((240.0000) * transformY) + moveY),

        new createjs.Point(((271.0002) * transformX) + moveX, ((241.6665) * transformY) + moveY),
        new createjs.Point(((268.9998) * transformX) + moveX, ((243.3335) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((245.0000) * transformY) + moveY),

        new createjs.Point(((267.0000) * transformX) + moveX, ((245.6666) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((246.3334) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((247.0000) * transformY) + moveY),

        new createjs.Point(((265.3335) * transformX) + moveX, ((248.3332) * transformY) + moveY),
        new createjs.Point(((263.6665) * transformX) + moveX, ((249.6668) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((251.0000) * transformY) + moveY),

        new createjs.Point(((262.0000) * transformX) + moveX, ((251.6666) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((252.3334) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((253.0000) * transformY) + moveY),

        new createjs.Point(((260.6668) * transformX) + moveX, ((253.9999) * transformY) + moveY),
        new createjs.Point(((259.3332) * transformX) + moveX, ((255.0001) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((256.0000) * transformY) + moveY),

        new createjs.Point(((258.0000) * transformX) + moveX, ((256.6666) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((257.3334) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((258.0000) * transformY) + moveY),

        new createjs.Point(((256.6668) * transformX) + moveX, ((258.9999) * transformY) + moveY),
        new createjs.Point(((255.3332) * transformX) + moveX, ((260.0001) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((261.0000) * transformY) + moveY),

        new createjs.Point(((254.0000) * transformX) + moveX, ((261.6666) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((262.3334) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((263.0000) * transformY) + moveY),

        new createjs.Point(((252.6668) * transformX) + moveX, ((263.9999) * transformY) + moveY),
        new createjs.Point(((251.3332) * transformX) + moveX, ((265.0001) * transformY) + moveY),
        new createjs.Point(((250.0000) * transformX) + moveX, ((266.0000) * transformY) + moveY),

        new createjs.Point(((250.0000) * transformX) + moveX, ((266.6666) * transformY) + moveY),
        new createjs.Point(((250.0000) * transformX) + moveX, ((267.3334) * transformY) + moveY),
        new createjs.Point(((250.0000) * transformX) + moveX, ((268.0000) * transformY) + moveY),

        new createjs.Point(((249.3334) * transformX) + moveX, ((268.3333) * transformY) + moveY),
        new createjs.Point(((248.6666) * transformX) + moveX, ((268.6667) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((269.0000) * transformY) + moveY),

        new createjs.Point(((248.0000) * transformX) + moveX, ((269.3333) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((269.6667) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((270.0000) * transformY) + moveY),

        new createjs.Point(((247.3334) * transformX) + moveX, ((270.3333) * transformY) + moveY),
        new createjs.Point(((246.6666) * transformX) + moveX, ((270.6667) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((271.0000) * transformY) + moveY),

        new createjs.Point(((246.0000) * transformX) + moveX, ((271.6666) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((272.3334) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((273.0000) * transformY) + moveY),

        new createjs.Point(((245.0001) * transformX) + moveX, ((273.6666) * transformY) + moveY),
        new createjs.Point(((243.9999) * transformX) + moveX, ((274.3334) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((275.0000) * transformY) + moveY),

        new createjs.Point(((243.0000) * transformX) + moveX, ((275.6666) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((276.3334) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((277.0000) * transformY) + moveY),

        new createjs.Point(((242.0001) * transformX) + moveX, ((277.6666) * transformY) + moveY),
        new createjs.Point(((240.9999) * transformX) + moveX, ((278.3334) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((279.0000) * transformY) + moveY),

        new createjs.Point(((240.0000) * transformX) + moveX, ((279.6666) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((280.3334) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((281.0000) * transformY) + moveY),

        new createjs.Point(((239.0001) * transformX) + moveX, ((281.6666) * transformY) + moveY),
        new createjs.Point(((237.9999) * transformX) + moveX, ((282.3334) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((283.0000) * transformY) + moveY),

        new createjs.Point(((237.0000) * transformX) + moveX, ((283.6666) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((284.3334) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((285.0000) * transformY) + moveY),

        new createjs.Point(((236.0001) * transformX) + moveX, ((285.6666) * transformY) + moveY),
        new createjs.Point(((234.9999) * transformX) + moveX, ((286.3334) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((287.0000) * transformY) + moveY),

        new createjs.Point(((234.0000) * transformX) + moveX, ((287.6666) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((288.3334) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((289.0000) * transformY) + moveY),

        new createjs.Point(((233.0001) * transformX) + moveX, ((289.6666) * transformY) + moveY),
        new createjs.Point(((231.9999) * transformX) + moveX, ((290.3334) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((291.0000) * transformY) + moveY),

        new createjs.Point(((231.0000) * transformX) + moveX, ((291.6666) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((292.3334) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((293.0000) * transformY) + moveY),

        new createjs.Point(((230.3334) * transformX) + moveX, ((293.3333) * transformY) + moveY),
        new createjs.Point(((229.6666) * transformX) + moveX, ((293.6667) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((294.0000) * transformY) + moveY),

        new createjs.Point(((229.0000) * transformX) + moveX, ((294.6666) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((295.3334) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((296.0000) * transformY) + moveY),

        new createjs.Point(((228.0001) * transformX) + moveX, ((296.6666) * transformY) + moveY),
        new createjs.Point(((226.9999) * transformX) + moveX, ((297.3334) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((298.0000) * transformY) + moveY),

        new createjs.Point(((226.0000) * transformX) + moveX, ((298.6666) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((299.3334) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((300.0000) * transformY) + moveY),

        new createjs.Point(((225.6667) * transformX) + moveX, ((300.0000) * transformY) + moveY),
        new createjs.Point(((225.3333) * transformX) + moveX, ((300.0000) * transformY) + moveY),
        new createjs.Point(((225.0000) * transformX) + moveX, ((300.0000) * transformY) + moveY),

        new createjs.Point(((224.6667) * transformX) + moveX, ((300.9999) * transformY) + moveY),
        new createjs.Point(((224.3333) * transformX) + moveX, ((302.0001) * transformY) + moveY),
        new createjs.Point(((224.0000) * transformX) + moveX, ((303.0000) * transformY) + moveY),

        new createjs.Point(((223.3334) * transformX) + moveX, ((303.3333) * transformY) + moveY),
        new createjs.Point(((222.6666) * transformX) + moveX, ((303.6667) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((304.0000) * transformY) + moveY),

        new createjs.Point(((222.0000) * transformX) + moveX, ((304.6666) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((305.3334) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((306.0000) * transformY) + moveY),

        new createjs.Point(((221.0001) * transformX) + moveX, ((306.6666) * transformY) + moveY),
        new createjs.Point(((219.9999) * transformX) + moveX, ((307.3334) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((308.0000) * transformY) + moveY),

        new createjs.Point(((219.0000) * transformX) + moveX, ((308.6666) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((309.3334) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((310.0000) * transformY) + moveY),

        new createjs.Point(((218.3334) * transformX) + moveX, ((310.3333) * transformY) + moveY),
        new createjs.Point(((217.6666) * transformX) + moveX, ((310.6667) * transformY) + moveY),
        new createjs.Point(((217.0000) * transformX) + moveX, ((311.0000) * transformY) + moveY),

        new createjs.Point(((216.6667) * transformX) + moveX, ((311.9999) * transformY) + moveY),
        new createjs.Point(((216.3333) * transformX) + moveX, ((313.0001) * transformY) + moveY),
        new createjs.Point(((216.0000) * transformX) + moveX, ((314.0000) * transformY) + moveY),

        new createjs.Point(((215.6667) * transformX) + moveX, ((314.0000) * transformY) + moveY),
        new createjs.Point(((215.3333) * transformX) + moveX, ((314.0000) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((314.0000) * transformY) + moveY),

        new createjs.Point(((215.0000) * transformX) + moveX, ((314.6666) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((315.3334) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((316.0000) * transformY) + moveY),

        new createjs.Point(((214.3334) * transformX) + moveX, ((316.3333) * transformY) + moveY),
        new createjs.Point(((213.6666) * transformX) + moveX, ((316.6667) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((317.0000) * transformY) + moveY),

        new createjs.Point(((213.0000) * transformX) + moveX, ((317.6666) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((318.3334) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((319.0000) * transformY) + moveY),

        new createjs.Point(((212.3334) * transformX) + moveX, ((319.3333) * transformY) + moveY),
        new createjs.Point(((211.6666) * transformX) + moveX, ((319.6667) * transformY) + moveY),
        new createjs.Point(((211.0000) * transformX) + moveX, ((320.0000) * transformY) + moveY),

        new createjs.Point(((211.0000) * transformX) + moveX, ((320.6666) * transformY) + moveY),
        new createjs.Point(((211.0000) * transformX) + moveX, ((321.3334) * transformY) + moveY),
        new createjs.Point(((211.0000) * transformX) + moveX, ((322.0000) * transformY) + moveY),

        new createjs.Point(((210.3334) * transformX) + moveX, ((322.3333) * transformY) + moveY),
        new createjs.Point(((209.6666) * transformX) + moveX, ((322.6667) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((323.0000) * transformY) + moveY),

        new createjs.Point(((209.0000) * transformX) + moveX, ((323.6666) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((324.3334) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((325.0000) * transformY) + moveY),

        new createjs.Point(((208.3334) * transformX) + moveX, ((325.3333) * transformY) + moveY),
        new createjs.Point(((207.6666) * transformX) + moveX, ((325.6667) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((326.0000) * transformY) + moveY),

        new createjs.Point(((207.0000) * transformX) + moveX, ((326.6666) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((327.3334) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((328.0000) * transformY) + moveY),

        new createjs.Point(((206.3334) * transformX) + moveX, ((328.3333) * transformY) + moveY),
        new createjs.Point(((205.6666) * transformX) + moveX, ((328.6667) * transformY) + moveY),
        new createjs.Point(((205.0000) * transformX) + moveX, ((329.0000) * transformY) + moveY),

        new createjs.Point(((204.6667) * transformX) + moveX, ((330.3332) * transformY) + moveY),
        new createjs.Point(((204.3333) * transformX) + moveX, ((331.6668) * transformY) + moveY),
        new createjs.Point(((204.0000) * transformX) + moveX, ((333.0000) * transformY) + moveY),

        new createjs.Point(((203.3334) * transformX) + moveX, ((333.3333) * transformY) + moveY),
        new createjs.Point(((202.6666) * transformX) + moveX, ((333.6667) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((334.0000) * transformY) + moveY),

        new createjs.Point(((202.0000) * transformX) + moveX, ((334.6666) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((335.3334) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((336.0000) * transformY) + moveY),

        new createjs.Point(((201.3334) * transformX) + moveX, ((336.3333) * transformY) + moveY),
        new createjs.Point(((200.6666) * transformX) + moveX, ((336.6667) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((337.0000) * transformY) + moveY),

        new createjs.Point(((200.0000) * transformX) + moveX, ((337.6666) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((338.3334) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),

        new createjs.Point(((199.3334) * transformX) + moveX, ((339.3333) * transformY) + moveY),
        new createjs.Point(((198.6666) * transformX) + moveX, ((339.6667) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((340.0000) * transformY) + moveY),

        new createjs.Point(((198.0000) * transformX) + moveX, ((340.6666) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((341.3334) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((342.0000) * transformY) + moveY),

        new createjs.Point(((197.6667) * transformX) + moveX, ((342.0000) * transformY) + moveY),
        new createjs.Point(((197.3333) * transformX) + moveX, ((342.0000) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((342.0000) * transformY) + moveY),

        new createjs.Point(((197.0000) * transformX) + moveX, ((342.6666) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((343.3334) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((344.0000) * transformY) + moveY),

        new createjs.Point(((196.3334) * transformX) + moveX, ((344.3333) * transformY) + moveY),
        new createjs.Point(((195.6666) * transformX) + moveX, ((344.6667) * transformY) + moveY),
        new createjs.Point(((195.0000) * transformX) + moveX, ((345.0000) * transformY) + moveY),

        new createjs.Point(((194.6667) * transformX) + moveX, ((346.3332) * transformY) + moveY),
        new createjs.Point(((194.3333) * transformX) + moveX, ((347.6668) * transformY) + moveY),
        new createjs.Point(((194.0000) * transformX) + moveX, ((349.0000) * transformY) + moveY),

        new createjs.Point(((193.3334) * transformX) + moveX, ((349.3333) * transformY) + moveY),
        new createjs.Point(((192.6666) * transformX) + moveX, ((349.6667) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((350.0000) * transformY) + moveY),

        new createjs.Point(((191.6667) * transformX) + moveX, ((351.3332) * transformY) + moveY),
        new createjs.Point(((191.3333) * transformX) + moveX, ((352.6668) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((354.0000) * transformY) + moveY),

        new createjs.Point(((190.3334) * transformX) + moveX, ((354.3333) * transformY) + moveY),
        new createjs.Point(((189.6666) * transformX) + moveX, ((354.6667) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((355.0000) * transformY) + moveY),

        new createjs.Point(((188.6667) * transformX) + moveX, ((356.3332) * transformY) + moveY),
        new createjs.Point(((188.3333) * transformX) + moveX, ((357.6668) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((359.0000) * transformY) + moveY),

        new createjs.Point(((187.3334) * transformX) + moveX, ((359.3333) * transformY) + moveY),
        new createjs.Point(((186.6666) * transformX) + moveX, ((359.6667) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((360.0000) * transformY) + moveY),

        new createjs.Point(((185.6667) * transformX) + moveX, ((361.3332) * transformY) + moveY),
        new createjs.Point(((185.3333) * transformX) + moveX, ((362.6668) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((364.0000) * transformY) + moveY),

        new createjs.Point(((184.3334) * transformX) + moveX, ((364.3333) * transformY) + moveY),
        new createjs.Point(((183.6666) * transformX) + moveX, ((364.6667) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((365.0000) * transformY) + moveY),

        new createjs.Point(((182.3334) * transformX) + moveX, ((366.9998) * transformY) + moveY),
        new createjs.Point(((181.6666) * transformX) + moveX, ((369.0002) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((371.0000) * transformY) + moveY),

        new createjs.Point(((180.3334) * transformX) + moveX, ((371.3333) * transformY) + moveY),
        new createjs.Point(((179.6666) * transformX) + moveX, ((371.6667) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((372.0000) * transformY) + moveY),

        new createjs.Point(((179.0000) * transformX) + moveX, ((372.6666) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((373.3334) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((374.0000) * transformY) + moveY),

        new createjs.Point(((178.6667) * transformX) + moveX, ((374.0000) * transformY) + moveY),
        new createjs.Point(((178.3333) * transformX) + moveX, ((374.0000) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((374.0000) * transformY) + moveY),

        new createjs.Point(((178.0000) * transformX) + moveX, ((374.6666) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((375.3334) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((376.0000) * transformY) + moveY),

        new createjs.Point(((177.6667) * transformX) + moveX, ((376.0000) * transformY) + moveY),
        new createjs.Point(((177.3333) * transformX) + moveX, ((376.0000) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((376.0000) * transformY) + moveY),

        new createjs.Point(((177.0000) * transformX) + moveX, ((376.6666) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((377.3334) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((378.0000) * transformY) + moveY),

        new createjs.Point(((176.6667) * transformX) + moveX, ((378.0000) * transformY) + moveY),
        new createjs.Point(((176.3333) * transformX) + moveX, ((378.0000) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((378.0000) * transformY) + moveY),

        new createjs.Point(((176.0000) * transformX) + moveX, ((378.6666) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((379.3334) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((380.0000) * transformY) + moveY),

        new createjs.Point(((175.3334) * transformX) + moveX, ((380.3333) * transformY) + moveY),
        new createjs.Point(((174.6666) * transformX) + moveX, ((380.6667) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((381.0000) * transformY) + moveY),

        new createjs.Point(((174.0000) * transformX) + moveX, ((381.6666) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((382.3334) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((383.0000) * transformY) + moveY),

        new createjs.Point(((173.6667) * transformX) + moveX, ((383.0000) * transformY) + moveY),
        new createjs.Point(((173.3333) * transformX) + moveX, ((383.0000) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((383.0000) * transformY) + moveY),

        new createjs.Point(((173.0000) * transformX) + moveX, ((383.6666) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((384.3334) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((385.0000) * transformY) + moveY),

        new createjs.Point(((172.6667) * transformX) + moveX, ((385.0000) * transformY) + moveY),
        new createjs.Point(((172.3333) * transformX) + moveX, ((385.0000) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((385.0000) * transformY) + moveY),

        new createjs.Point(((172.0000) * transformX) + moveX, ((385.6666) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((386.3334) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((387.0000) * transformY) + moveY),

        new createjs.Point(((171.6667) * transformX) + moveX, ((387.0000) * transformY) + moveY),
        new createjs.Point(((171.3333) * transformX) + moveX, ((387.0000) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((387.0000) * transformY) + moveY),

        new createjs.Point(((171.0000) * transformX) + moveX, ((387.6666) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((388.3334) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((170.6667) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((170.3333) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((170.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((170.0000) * transformX) + moveX, ((389.6666) * transformY) + moveY),
        new createjs.Point(((170.0000) * transformX) + moveX, ((390.3334) * transformY) + moveY),
        new createjs.Point(((170.0000) * transformX) + moveX, ((391.0000) * transformY) + moveY),

        new createjs.Point(((169.3334) * transformX) + moveX, ((391.3333) * transformY) + moveY),
        new createjs.Point(((168.6666) * transformX) + moveX, ((391.6667) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((392.0000) * transformY) + moveY),

        new createjs.Point(((167.6667) * transformX) + moveX, ((393.3332) * transformY) + moveY),
        new createjs.Point(((167.3333) * transformX) + moveX, ((394.6668) * transformY) + moveY),
        new createjs.Point(((167.0000) * transformX) + moveX, ((396.0000) * transformY) + moveY),

        new createjs.Point(((166.6667) * transformX) + moveX, ((396.0000) * transformY) + moveY),
        new createjs.Point(((166.3333) * transformX) + moveX, ((396.0000) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((396.0000) * transformY) + moveY),

        new createjs.Point(((166.0000) * transformX) + moveX, ((396.6666) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((397.3334) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((398.0000) * transformY) + moveY),

        new createjs.Point(((165.6667) * transformX) + moveX, ((398.0000) * transformY) + moveY),
        new createjs.Point(((165.3333) * transformX) + moveX, ((398.0000) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((398.0000) * transformY) + moveY),

        new createjs.Point(((165.0000) * transformX) + moveX, ((398.6666) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((399.3334) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((400.0000) * transformY) + moveY),

        new createjs.Point(((164.6667) * transformX) + moveX, ((400.0000) * transformY) + moveY),
        new createjs.Point(((164.3333) * transformX) + moveX, ((400.0000) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((400.0000) * transformY) + moveY),

        new createjs.Point(((164.0000) * transformX) + moveX, ((400.6666) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((401.3334) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((402.0000) * transformY) + moveY),

        new createjs.Point(((163.6667) * transformX) + moveX, ((402.0000) * transformY) + moveY),
        new createjs.Point(((163.3333) * transformX) + moveX, ((402.0000) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((402.0000) * transformY) + moveY),

        new createjs.Point(((163.0000) * transformX) + moveX, ((402.6666) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((403.3334) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((404.0000) * transformY) + moveY),

        new createjs.Point(((162.6667) * transformX) + moveX, ((404.0000) * transformY) + moveY),
        new createjs.Point(((162.3333) * transformX) + moveX, ((404.0000) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((404.0000) * transformY) + moveY),

        new createjs.Point(((162.0000) * transformX) + moveX, ((404.6666) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((405.3334) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((406.0000) * transformY) + moveY),

        new createjs.Point(((161.6667) * transformX) + moveX, ((406.0000) * transformY) + moveY),
        new createjs.Point(((161.3333) * transformX) + moveX, ((406.0000) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((406.0000) * transformY) + moveY),

        new createjs.Point(((160.6667) * transformX) + moveX, ((407.3332) * transformY) + moveY),
        new createjs.Point(((160.3333) * transformX) + moveX, ((408.6668) * transformY) + moveY),
        new createjs.Point(((160.0000) * transformX) + moveX, ((410.0000) * transformY) + moveY),

        new createjs.Point(((159.6667) * transformX) + moveX, ((410.0000) * transformY) + moveY),
        new createjs.Point(((159.3333) * transformX) + moveX, ((410.0000) * transformY) + moveY),
        new createjs.Point(((159.0000) * transformX) + moveX, ((410.0000) * transformY) + moveY),

        new createjs.Point(((158.6667) * transformX) + moveX, ((411.3332) * transformY) + moveY),
        new createjs.Point(((158.3333) * transformX) + moveX, ((412.6668) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((414.0000) * transformY) + moveY),

        new createjs.Point(((157.6667) * transformX) + moveX, ((414.0000) * transformY) + moveY),
        new createjs.Point(((157.3333) * transformX) + moveX, ((414.0000) * transformY) + moveY),
        new createjs.Point(((157.0000) * transformX) + moveX, ((414.0000) * transformY) + moveY),

        new createjs.Point(((157.0000) * transformX) + moveX, ((414.6666) * transformY) + moveY),
        new createjs.Point(((157.0000) * transformX) + moveX, ((415.3334) * transformY) + moveY),
        new createjs.Point(((157.0000) * transformX) + moveX, ((416.0000) * transformY) + moveY),

        new createjs.Point(((156.6667) * transformX) + moveX, ((416.0000) * transformY) + moveY),
        new createjs.Point(((156.3333) * transformX) + moveX, ((416.0000) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((416.0000) * transformY) + moveY),

        new createjs.Point(((156.0000) * transformX) + moveX, ((416.6666) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((417.3334) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((418.0000) * transformY) + moveY),

        new createjs.Point(((155.6667) * transformX) + moveX, ((418.0000) * transformY) + moveY),
        new createjs.Point(((155.3333) * transformX) + moveX, ((418.0000) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((418.0000) * transformY) + moveY),

        new createjs.Point(((155.0000) * transformX) + moveX, ((418.6666) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((419.3334) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((420.0000) * transformY) + moveY),

        new createjs.Point(((154.6667) * transformX) + moveX, ((420.0000) * transformY) + moveY),
        new createjs.Point(((154.3333) * transformX) + moveX, ((420.0000) * transformY) + moveY),
        new createjs.Point(((154.0000) * transformX) + moveX, ((420.0000) * transformY) + moveY),

        new createjs.Point(((154.0000) * transformX) + moveX, ((420.6666) * transformY) + moveY),
        new createjs.Point(((154.0000) * transformX) + moveX, ((421.3334) * transformY) + moveY),
        new createjs.Point(((154.0000) * transformX) + moveX, ((422.0000) * transformY) + moveY),

        new createjs.Point(((153.6667) * transformX) + moveX, ((422.0000) * transformY) + moveY),
        new createjs.Point(((153.3333) * transformX) + moveX, ((422.0000) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((422.0000) * transformY) + moveY),

        new createjs.Point(((153.0000) * transformX) + moveX, ((422.6666) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((423.3334) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((424.0000) * transformY) + moveY),

        new createjs.Point(((152.6667) * transformX) + moveX, ((424.0000) * transformY) + moveY),
        new createjs.Point(((152.3333) * transformX) + moveX, ((424.0000) * transformY) + moveY),
        new createjs.Point(((152.0000) * transformX) + moveX, ((424.0000) * transformY) + moveY),

        new createjs.Point(((151.6667) * transformX) + moveX, ((425.3332) * transformY) + moveY),
        new createjs.Point(((151.3333) * transformX) + moveX, ((426.6668) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((428.0000) * transformY) + moveY),

        new createjs.Point(((150.6667) * transformX) + moveX, ((428.0000) * transformY) + moveY),
        new createjs.Point(((150.3333) * transformX) + moveX, ((428.0000) * transformY) + moveY),
        new createjs.Point(((150.0000) * transformX) + moveX, ((428.0000) * transformY) + moveY),

        new createjs.Point(((150.0000) * transformX) + moveX, ((428.6666) * transformY) + moveY),
        new createjs.Point(((150.0000) * transformX) + moveX, ((429.3334) * transformY) + moveY),
        new createjs.Point(((150.0000) * transformX) + moveX, ((430.0000) * transformY) + moveY),

        new createjs.Point(((149.6667) * transformX) + moveX, ((430.0000) * transformY) + moveY),
        new createjs.Point(((149.3333) * transformX) + moveX, ((430.0000) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((430.0000) * transformY) + moveY),

        new createjs.Point(((149.0000) * transformX) + moveX, ((430.9999) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((432.0001) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((433.0000) * transformY) + moveY),

        new createjs.Point(((148.6667) * transformX) + moveX, ((433.0000) * transformY) + moveY),
        new createjs.Point(((148.3333) * transformX) + moveX, ((433.0000) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((433.0000) * transformY) + moveY),

        new createjs.Point(((148.0000) * transformX) + moveX, ((433.6666) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((434.3334) * transformY) + moveY),
        new createjs.Point(((148.0000) * transformX) + moveX, ((435.0000) * transformY) + moveY),

        new createjs.Point(((147.6667) * transformX) + moveX, ((435.0000) * transformY) + moveY),
        new createjs.Point(((147.3333) * transformX) + moveX, ((435.0000) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((435.0000) * transformY) + moveY),

        new createjs.Point(((147.0000) * transformX) + moveX, ((435.6666) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((436.3334) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((437.0000) * transformY) + moveY),

        new createjs.Point(((146.6667) * transformX) + moveX, ((437.0000) * transformY) + moveY),
        new createjs.Point(((146.3333) * transformX) + moveX, ((437.0000) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((437.0000) * transformY) + moveY),

        new createjs.Point(((145.3334) * transformX) + moveX, ((438.9998) * transformY) + moveY),
        new createjs.Point(((144.6666) * transformX) + moveX, ((441.0002) * transformY) + moveY),
        new createjs.Point(((144.0000) * transformX) + moveX, ((443.0000) * transformY) + moveY),

        new createjs.Point(((143.6667) * transformX) + moveX, ((443.0000) * transformY) + moveY),
        new createjs.Point(((143.3333) * transformX) + moveX, ((443.0000) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((443.0000) * transformY) + moveY),

        new createjs.Point(((143.0000) * transformX) + moveX, ((443.9999) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((445.0001) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((446.0000) * transformY) + moveY),

        new createjs.Point(((142.6667) * transformX) + moveX, ((446.0000) * transformY) + moveY),
        new createjs.Point(((142.3333) * transformX) + moveX, ((446.0000) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((446.0000) * transformY) + moveY),

        new createjs.Point(((142.0000) * transformX) + moveX, ((446.6666) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((447.3334) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((448.0000) * transformY) + moveY),

        new createjs.Point(((141.6667) * transformX) + moveX, ((448.0000) * transformY) + moveY),
        new createjs.Point(((141.3333) * transformX) + moveX, ((448.0000) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((448.0000) * transformY) + moveY),

        new createjs.Point(((140.6667) * transformX) + moveX, ((449.3332) * transformY) + moveY),
        new createjs.Point(((140.3333) * transformX) + moveX, ((450.6668) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((452.0000) * transformY) + moveY),

        new createjs.Point(((139.6667) * transformX) + moveX, ((452.0000) * transformY) + moveY),
        new createjs.Point(((139.3333) * transformX) + moveX, ((452.0000) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((452.0000) * transformY) + moveY),

        new createjs.Point(((139.0000) * transformX) + moveX, ((452.9999) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((454.0001) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((138.6667) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((138.3333) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((137.6667) * transformX) + moveX, ((456.3332) * transformY) + moveY),
        new createjs.Point(((137.3333) * transformX) + moveX, ((457.6668) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((459.0000) * transformY) + moveY),

        new createjs.Point(((136.6667) * transformX) + moveX, ((459.0000) * transformY) + moveY),
        new createjs.Point(((136.3333) * transformX) + moveX, ((459.0000) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((459.0000) * transformY) + moveY),

        new createjs.Point(((136.0000) * transformX) + moveX, ((459.9999) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((461.0001) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((462.0000) * transformY) + moveY),

        new createjs.Point(((135.6667) * transformX) + moveX, ((462.0000) * transformY) + moveY),
        new createjs.Point(((135.3333) * transformX) + moveX, ((462.0000) * transformY) + moveY),
        new createjs.Point(((135.0000) * transformX) + moveX, ((462.0000) * transformY) + moveY),

        new createjs.Point(((134.6667) * transformX) + moveX, ((463.3332) * transformY) + moveY),
        new createjs.Point(((134.3333) * transformX) + moveX, ((464.6668) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((466.0000) * transformY) + moveY),

        new createjs.Point(((133.6667) * transformX) + moveX, ((466.0000) * transformY) + moveY),
        new createjs.Point(((133.3333) * transformX) + moveX, ((466.0000) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((466.0000) * transformY) + moveY),

        new createjs.Point(((133.0000) * transformX) + moveX, ((466.9999) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((468.0001) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((469.0000) * transformY) + moveY),

        new createjs.Point(((132.6667) * transformX) + moveX, ((469.0000) * transformY) + moveY),
        new createjs.Point(((132.3333) * transformX) + moveX, ((469.0000) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((469.0000) * transformY) + moveY),

        new createjs.Point(((132.0000) * transformX) + moveX, ((469.6666) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((470.3334) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((471.0000) * transformY) + moveY),

        new createjs.Point(((131.6667) * transformX) + moveX, ((471.0000) * transformY) + moveY),
        new createjs.Point(((131.3333) * transformX) + moveX, ((471.0000) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((471.0000) * transformY) + moveY),

        new createjs.Point(((130.6667) * transformX) + moveX, ((472.6665) * transformY) + moveY),
        new createjs.Point(((130.3333) * transformX) + moveX, ((474.3335) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((476.0000) * transformY) + moveY),

        new createjs.Point(((129.6667) * transformX) + moveX, ((476.0000) * transformY) + moveY),
        new createjs.Point(((129.3333) * transformX) + moveX, ((476.0000) * transformY) + moveY),
        new createjs.Point(((129.0000) * transformX) + moveX, ((476.0000) * transformY) + moveY),

        new createjs.Point(((129.0000) * transformX) + moveX, ((476.9999) * transformY) + moveY),
        new createjs.Point(((129.0000) * transformX) + moveX, ((478.0001) * transformY) + moveY),
        new createjs.Point(((129.0000) * transformX) + moveX, ((479.0000) * transformY) + moveY),

        new createjs.Point(((128.6667) * transformX) + moveX, ((479.0000) * transformY) + moveY),
        new createjs.Point(((128.3333) * transformX) + moveX, ((479.0000) * transformY) + moveY),
        new createjs.Point(((128.0000) * transformX) + moveX, ((479.0000) * transformY) + moveY),

        new createjs.Point(((128.0000) * transformX) + moveX, ((479.6666) * transformY) + moveY),
        new createjs.Point(((128.0000) * transformX) + moveX, ((480.3334) * transformY) + moveY),
        new createjs.Point(((128.0000) * transformX) + moveX, ((481.0000) * transformY) + moveY),

        new createjs.Point(((127.6667) * transformX) + moveX, ((481.0000) * transformY) + moveY),
        new createjs.Point(((127.3333) * transformX) + moveX, ((481.0000) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((481.0000) * transformY) + moveY),

        new createjs.Point(((127.0000) * transformX) + moveX, ((481.9999) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((483.0001) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((484.0000) * transformY) + moveY),

        new createjs.Point(((126.6667) * transformX) + moveX, ((484.0000) * transformY) + moveY),
        new createjs.Point(((126.3333) * transformX) + moveX, ((484.0000) * transformY) + moveY),
        new createjs.Point(((126.0000) * transformX) + moveX, ((484.0000) * transformY) + moveY),

        new createjs.Point(((126.0000) * transformX) + moveX, ((484.6666) * transformY) + moveY),
        new createjs.Point(((126.0000) * transformX) + moveX, ((485.3334) * transformY) + moveY),
        new createjs.Point(((126.0000) * transformX) + moveX, ((486.0000) * transformY) + moveY),

        new createjs.Point(((125.6667) * transformX) + moveX, ((486.0000) * transformY) + moveY),
        new createjs.Point(((125.3333) * transformX) + moveX, ((486.0000) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((486.0000) * transformY) + moveY),

        new createjs.Point(((125.0000) * transformX) + moveX, ((486.9999) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((488.0001) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((124.6667) * transformX) + moveX, ((489.0000) * transformY) + moveY),
        new createjs.Point(((124.3333) * transformX) + moveX, ((489.0000) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((124.0000) * transformX) + moveX, ((489.6666) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((490.3334) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((123.6667) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((123.3333) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((123.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((122.6667) * transformX) + moveX, ((492.9998) * transformY) + moveY),
        new createjs.Point(((122.3333) * transformX) + moveX, ((495.0002) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((497.0000) * transformY) + moveY),

        new createjs.Point(((121.6667) * transformX) + moveX, ((497.0000) * transformY) + moveY),
        new createjs.Point(((121.3333) * transformX) + moveX, ((497.0000) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((497.0000) * transformY) + moveY),

        new createjs.Point(((121.0000) * transformX) + moveX, ((497.6666) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((498.3334) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),

        new createjs.Point(((120.6667) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((120.3333) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),

        new createjs.Point(((120.0000) * transformX) + moveX, ((499.9999) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((501.0001) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),

        new createjs.Point(((119.6667) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((119.3333) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),

        new createjs.Point(((119.0000) * transformX) + moveX, ((502.9999) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((504.0001) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((505.0000) * transformY) + moveY),

        new createjs.Point(((118.6667) * transformX) + moveX, ((505.0000) * transformY) + moveY),
        new createjs.Point(((118.3333) * transformX) + moveX, ((505.0000) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((505.0000) * transformY) + moveY),

        new createjs.Point(((118.0000) * transformX) + moveX, ((505.6666) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((506.3334) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((507.0000) * transformY) + moveY),

        new createjs.Point(((117.6667) * transformX) + moveX, ((507.0000) * transformY) + moveY),
        new createjs.Point(((117.3333) * transformX) + moveX, ((507.0000) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((507.0000) * transformY) + moveY),

        new createjs.Point(((117.0000) * transformX) + moveX, ((507.9999) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((509.0001) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((116.6667) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((116.3333) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((116.0000) * transformX) + moveX, ((510.9999) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((512.0001) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((513.0000) * transformY) + moveY),

        new createjs.Point(((115.6667) * transformX) + moveX, ((513.0000) * transformY) + moveY),
        new createjs.Point(((115.3333) * transformX) + moveX, ((513.0000) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((513.0000) * transformY) + moveY),

        new createjs.Point(((115.0000) * transformX) + moveX, ((513.9999) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((515.0001) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((114.6667) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((114.3333) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((114.0000) * transformX) + moveX, ((516.9999) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((518.0001) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((519.0000) * transformY) + moveY),

        new createjs.Point(((113.6667) * transformX) + moveX, ((519.0000) * transformY) + moveY),
        new createjs.Point(((113.3333) * transformX) + moveX, ((519.0000) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((519.0000) * transformY) + moveY),

        new createjs.Point(((113.0000) * transformX) + moveX, ((519.9999) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((521.0001) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((522.0000) * transformY) + moveY),

        new createjs.Point(((107.8854) * transformX) + moveX, ((534.8356) * transformY) + moveY),
        new createjs.Point(((103.7727) * transformX) + moveX, ((549.7097) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((563.0000) * transformY) + moveY),

        new createjs.Point(((97.6668) * transformX) + moveX, ((568.9994) * transformY) + moveY),
        new createjs.Point(((96.3332) * transformX) + moveX, ((575.0006) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((581.0000) * transformY) + moveY),

        new createjs.Point(((94.6667) * transformX) + moveX, ((581.0000) * transformY) + moveY),
        new createjs.Point(((94.3333) * transformX) + moveX, ((581.0000) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((581.0000) * transformY) + moveY),

        new createjs.Point(((93.6667) * transformX) + moveX, ((583.6664) * transformY) + moveY),
        new createjs.Point(((93.3333) * transformX) + moveX, ((586.3336) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((589.0000) * transformY) + moveY),

        new createjs.Point(((92.6667) * transformX) + moveX, ((589.0000) * transformY) + moveY),
        new createjs.Point(((92.3333) * transformX) + moveX, ((589.0000) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((589.0000) * transformY) + moveY),

        new createjs.Point(((92.0000) * transformX) + moveX, ((590.3332) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((591.6668) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((593.0000) * transformY) + moveY),

        new createjs.Point(((91.6667) * transformX) + moveX, ((593.0000) * transformY) + moveY),
        new createjs.Point(((91.3333) * transformX) + moveX, ((593.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((593.0000) * transformY) + moveY),

        new createjs.Point(((91.0000) * transformX) + moveX, ((594.3332) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((595.6668) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((597.0000) * transformY) + moveY),

        new createjs.Point(((90.6667) * transformX) + moveX, ((597.0000) * transformY) + moveY),
        new createjs.Point(((90.3333) * transformX) + moveX, ((597.0000) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((597.0000) * transformY) + moveY),

        new createjs.Point(((90.0000) * transformX) + moveX, ((598.3332) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((599.6668) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((601.0000) * transformY) + moveY),

        new createjs.Point(((89.6667) * transformX) + moveX, ((601.0000) * transformY) + moveY),
        new createjs.Point(((89.3333) * transformX) + moveX, ((601.0000) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((601.0000) * transformY) + moveY),

        new createjs.Point(((89.0000) * transformX) + moveX, ((602.3332) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((603.6668) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((605.0000) * transformY) + moveY),

        new createjs.Point(((88.6667) * transformX) + moveX, ((605.0000) * transformY) + moveY),
        new createjs.Point(((88.3333) * transformX) + moveX, ((605.0000) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((605.0000) * transformY) + moveY),

        new createjs.Point(((87.6667) * transformX) + moveX, ((607.9997) * transformY) + moveY),
        new createjs.Point(((87.3333) * transformX) + moveX, ((611.0003) * transformY) + moveY),
        new createjs.Point(((87.0000) * transformX) + moveX, ((614.0000) * transformY) + moveY),

        new createjs.Point(((86.6667) * transformX) + moveX, ((614.0000) * transformY) + moveY),
        new createjs.Point(((86.3333) * transformX) + moveX, ((614.0000) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((614.0000) * transformY) + moveY),

        new createjs.Point(((85.6667) * transformX) + moveX, ((617.3330) * transformY) + moveY),
        new createjs.Point(((85.3333) * transformX) + moveX, ((620.6670) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((624.0000) * transformY) + moveY),

        new createjs.Point(((80.8402) * transformX) + moveX, ((636.9467) * transformY) + moveY),
        new createjs.Point(((79.9597) * transformX) + moveX, ((653.5127) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((667.0000) * transformY) + moveY),

        new createjs.Point(((76.0000) * transformX) + moveX, ((669.3331) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((671.6669) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((674.0000) * transformY) + moveY),

        new createjs.Point(((73.4246) * transformX) + moveX, ((682.8814) * transformY) + moveY),
        new createjs.Point(((73.5738) * transformX) + moveX, ((694.9806) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((704.0000) * transformY) + moveY),

        new createjs.Point(((71.0000) * transformX) + moveX, ((707.3330) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((710.6670) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((714.0000) * transformY) + moveY),

        new createjs.Point(((70.6667) * transformX) + moveX, ((714.0000) * transformY) + moveY),
        new createjs.Point(((70.3333) * transformX) + moveX, ((714.0000) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((714.0000) * transformY) + moveY),

        new createjs.Point(((70.0000) * transformX) + moveX, ((717.6663) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((721.3337) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((725.0000) * transformY) + moveY),

        new createjs.Point(((69.6667) * transformX) + moveX, ((725.0000) * transformY) + moveY),
        new createjs.Point(((69.3333) * transformX) + moveX, ((725.0000) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((725.0000) * transformY) + moveY),

        new createjs.Point(((69.0000) * transformX) + moveX, ((729.3329) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((733.6671) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((738.0000) * transformY) + moveY),

        new createjs.Point(((68.6667) * transformX) + moveX, ((738.0000) * transformY) + moveY),
        new createjs.Point(((68.3333) * transformX) + moveX, ((738.0000) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((738.0000) * transformY) + moveY),

        new createjs.Point(((68.0000) * transformX) + moveX, ((743.3328) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((748.6672) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((754.0000) * transformY) + moveY),

        new createjs.Point(((65.6832) * transformX) + moveX, ((762.3667) * transformY) + moveY),
        new createjs.Point(((67.0028) * transformX) + moveX, ((775.8915) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((786.0000) * transformY) + moveY),

        new createjs.Point(((66.9932) * transformX) + moveX, ((810.8695) * transformY) + moveY),
        new createjs.Point(((66.2199) * transformX) + moveX, ((838.6098) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((858.0000) * transformY) + moveY),

        new createjs.Point(((72.9999) * transformX) + moveX, ((865.3326) * transformY) + moveY),
        new createjs.Point(((74.0001) * transformX) + moveX, ((872.6674) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((880.0000) * transformY) + moveY),

        new createjs.Point(((75.3333) * transformX) + moveX, ((880.0000) * transformY) + moveY),
        new createjs.Point(((75.6667) * transformX) + moveX, ((880.0000) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((880.0000) * transformY) + moveY),

        new createjs.Point(((76.0000) * transformX) + moveX, ((881.3332) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((882.6668) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((884.0000) * transformY) + moveY),

        new createjs.Point(((76.3333) * transformX) + moveX, ((884.0000) * transformY) + moveY),
        new createjs.Point(((76.6667) * transformX) + moveX, ((884.0000) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((884.0000) * transformY) + moveY),

        new createjs.Point(((77.0000) * transformX) + moveX, ((885.3332) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((886.6668) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((888.0000) * transformY) + moveY),

        new createjs.Point(((77.3333) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((77.6667) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((888.0000) * transformY) + moveY),

        new createjs.Point(((78.0000) * transformX) + moveX, ((889.3332) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((890.6668) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((892.0000) * transformY) + moveY),

        new createjs.Point(((78.3333) * transformX) + moveX, ((892.0000) * transformY) + moveY),
        new createjs.Point(((78.6667) * transformX) + moveX, ((892.0000) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((892.0000) * transformY) + moveY),

        new createjs.Point(((79.0000) * transformX) + moveX, ((893.3332) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((894.6668) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((896.0000) * transformY) + moveY),

        new createjs.Point(((79.3333) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((79.6667) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((896.0000) * transformY) + moveY),

        new createjs.Point(((80.0000) * transformX) + moveX, ((897.3332) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((898.6668) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((900.0000) * transformY) + moveY),

        new createjs.Point(((80.3333) * transformX) + moveX, ((900.0000) * transformY) + moveY),
        new createjs.Point(((80.6667) * transformX) + moveX, ((900.0000) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((900.0000) * transformY) + moveY),

        new createjs.Point(((81.0000) * transformX) + moveX, ((900.9999) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((902.0001) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((903.0000) * transformY) + moveY),

        new createjs.Point(((81.3333) * transformX) + moveX, ((903.0000) * transformY) + moveY),
        new createjs.Point(((81.6667) * transformX) + moveX, ((903.0000) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((903.0000) * transformY) + moveY),

        new createjs.Point(((82.0000) * transformX) + moveX, ((903.9999) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((905.0001) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((906.0000) * transformY) + moveY),

        new createjs.Point(((82.3333) * transformX) + moveX, ((906.0000) * transformY) + moveY),
        new createjs.Point(((82.6667) * transformX) + moveX, ((906.0000) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((906.0000) * transformY) + moveY),

        new createjs.Point(((83.0000) * transformX) + moveX, ((907.3332) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((908.6668) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((910.0000) * transformY) + moveY),

        new createjs.Point(((83.3333) * transformX) + moveX, ((910.0000) * transformY) + moveY),
        new createjs.Point(((83.6667) * transformX) + moveX, ((910.0000) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((910.0000) * transformY) + moveY),

        new createjs.Point(((84.0000) * transformX) + moveX, ((910.9999) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((912.0001) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((913.0000) * transformY) + moveY),

        new createjs.Point(((84.3333) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((84.6667) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((913.0000) * transformY) + moveY),

        new createjs.Point(((85.0000) * transformX) + moveX, ((913.9999) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((915.0001) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((916.0000) * transformY) + moveY),

        new createjs.Point(((85.3333) * transformX) + moveX, ((916.0000) * transformY) + moveY),
        new createjs.Point(((85.6667) * transformX) + moveX, ((916.0000) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((916.0000) * transformY) + moveY),

        new createjs.Point(((86.0000) * transformX) + moveX, ((916.9999) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((918.0001) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((919.0000) * transformY) + moveY),

        new createjs.Point(((86.3333) * transformX) + moveX, ((919.0000) * transformY) + moveY),
        new createjs.Point(((86.6667) * transformX) + moveX, ((919.0000) * transformY) + moveY),
        new createjs.Point(((87.0000) * transformX) + moveX, ((919.0000) * transformY) + moveY),

        new createjs.Point(((87.0000) * transformX) + moveX, ((919.9999) * transformY) + moveY),
        new createjs.Point(((87.0000) * transformX) + moveX, ((921.0001) * transformY) + moveY),
        new createjs.Point(((87.0000) * transformX) + moveX, ((922.0000) * transformY) + moveY),

        new createjs.Point(((87.3333) * transformX) + moveX, ((922.0000) * transformY) + moveY),
        new createjs.Point(((87.6667) * transformX) + moveX, ((922.0000) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((922.0000) * transformY) + moveY),

        new createjs.Point(((88.0000) * transformX) + moveX, ((922.6666) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((923.3334) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((924.0000) * transformY) + moveY),

        new createjs.Point(((88.3333) * transformX) + moveX, ((924.0000) * transformY) + moveY),
        new createjs.Point(((88.6667) * transformX) + moveX, ((924.0000) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((924.0000) * transformY) + moveY),

        new createjs.Point(((89.3333) * transformX) + moveX, ((925.9998) * transformY) + moveY),
        new createjs.Point(((89.6667) * transformX) + moveX, ((928.0002) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((930.0000) * transformY) + moveY),

        new createjs.Point(((90.3333) * transformX) + moveX, ((930.0000) * transformY) + moveY),
        new createjs.Point(((90.6667) * transformX) + moveX, ((930.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((930.0000) * transformY) + moveY),

        new createjs.Point(((91.0000) * transformX) + moveX, ((930.6666) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((931.3334) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((932.0000) * transformY) + moveY),

        new createjs.Point(((91.3333) * transformX) + moveX, ((932.0000) * transformY) + moveY),
        new createjs.Point(((91.6667) * transformX) + moveX, ((932.0000) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((932.0000) * transformY) + moveY),

        new createjs.Point(((92.0000) * transformX) + moveX, ((932.9999) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((934.0001) * transformY) + moveY),
        new createjs.Point(((92.0000) * transformX) + moveX, ((935.0000) * transformY) + moveY),

        new createjs.Point(((92.3333) * transformX) + moveX, ((935.0000) * transformY) + moveY),
        new createjs.Point(((92.6667) * transformX) + moveX, ((935.0000) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((935.0000) * transformY) + moveY),

        new createjs.Point(((93.3333) * transformX) + moveX, ((936.3332) * transformY) + moveY),
        new createjs.Point(((93.6667) * transformX) + moveX, ((937.6668) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((939.0000) * transformY) + moveY),

        new createjs.Point(((94.3333) * transformX) + moveX, ((939.0000) * transformY) + moveY),
        new createjs.Point(((94.6667) * transformX) + moveX, ((939.0000) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((939.0000) * transformY) + moveY),

        new createjs.Point(((95.0000) * transformX) + moveX, ((939.9999) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((941.0001) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((942.0000) * transformY) + moveY),

        new createjs.Point(((95.3333) * transformX) + moveX, ((942.0000) * transformY) + moveY),
        new createjs.Point(((95.6667) * transformX) + moveX, ((942.0000) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((942.0000) * transformY) + moveY),

        new createjs.Point(((96.6666) * transformX) + moveX, ((943.9998) * transformY) + moveY),
        new createjs.Point(((97.3334) * transformX) + moveX, ((946.0002) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((948.0000) * transformY) + moveY),

        new createjs.Point(((98.3333) * transformX) + moveX, ((948.0000) * transformY) + moveY),
        new createjs.Point(((98.6667) * transformX) + moveX, ((948.0000) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((948.0000) * transformY) + moveY),

        new createjs.Point(((99.0000) * transformX) + moveX, ((948.6666) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((949.3334) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((950.0000) * transformY) + moveY),

        new createjs.Point(((99.3333) * transformX) + moveX, ((950.0000) * transformY) + moveY),
        new createjs.Point(((99.6667) * transformX) + moveX, ((950.0000) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((950.0000) * transformY) + moveY),

        new createjs.Point(((100.0000) * transformX) + moveX, ((950.6666) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((951.3334) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),

        new createjs.Point(((100.3333) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((100.6667) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),

        new createjs.Point(((101.3333) * transformX) + moveX, ((953.6665) * transformY) + moveY),
        new createjs.Point(((101.6667) * transformX) + moveX, ((955.3335) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((957.0000) * transformY) + moveY),

        new createjs.Point(((102.6666) * transformX) + moveX, ((957.3333) * transformY) + moveY),
        new createjs.Point(((103.3334) * transformX) + moveX, ((957.6667) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((958.0000) * transformY) + moveY),

        new createjs.Point(((104.3333) * transformX) + moveX, ((959.3332) * transformY) + moveY),
        new createjs.Point(((104.6667) * transformX) + moveX, ((960.6668) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((962.0000) * transformY) + moveY),

        new createjs.Point(((105.3333) * transformX) + moveX, ((962.0000) * transformY) + moveY),
        new createjs.Point(((105.6667) * transformX) + moveX, ((962.0000) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((962.0000) * transformY) + moveY),

        new createjs.Point(((106.0000) * transformX) + moveX, ((962.6666) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((963.3334) * transformY) + moveY),
        new createjs.Point(((106.0000) * transformX) + moveX, ((964.0000) * transformY) + moveY),

        new createjs.Point(((106.3333) * transformX) + moveX, ((964.0000) * transformY) + moveY),
        new createjs.Point(((106.6667) * transformX) + moveX, ((964.0000) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((964.0000) * transformY) + moveY),

        new createjs.Point(((107.0000) * transformX) + moveX, ((964.6666) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((965.3334) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((966.0000) * transformY) + moveY),

        new createjs.Point(((107.3333) * transformX) + moveX, ((966.0000) * transformY) + moveY),
        new createjs.Point(((107.6667) * transformX) + moveX, ((966.0000) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((966.0000) * transformY) + moveY),

        new createjs.Point(((108.0000) * transformX) + moveX, ((966.6666) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((967.3334) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((968.0000) * transformY) + moveY),

        new createjs.Point(((108.3333) * transformX) + moveX, ((968.0000) * transformY) + moveY),
        new createjs.Point(((108.6667) * transformX) + moveX, ((968.0000) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((968.0000) * transformY) + moveY),

        new createjs.Point(((109.0000) * transformX) + moveX, ((968.6666) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((969.3334) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((970.0000) * transformY) + moveY),

        new createjs.Point(((109.6666) * transformX) + moveX, ((970.3333) * transformY) + moveY),
        new createjs.Point(((110.3334) * transformX) + moveX, ((970.6667) * transformY) + moveY),
        new createjs.Point(((111.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),

        new createjs.Point(((111.6666) * transformX) + moveX, ((972.9998) * transformY) + moveY),
        new createjs.Point(((112.3334) * transformX) + moveX, ((975.0002) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((977.0000) * transformY) + moveY),

        new createjs.Point(((113.3333) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((113.6667) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((977.0000) * transformY) + moveY),

        new createjs.Point(((114.3333) * transformX) + moveX, ((977.9999) * transformY) + moveY),
        new createjs.Point(((114.6667) * transformX) + moveX, ((979.0001) * transformY) + moveY),
        new createjs.Point(((115.0000) * transformX) + moveX, ((980.0000) * transformY) + moveY),

        new createjs.Point(((115.6666) * transformX) + moveX, ((980.3333) * transformY) + moveY),
        new createjs.Point(((116.3334) * transformX) + moveX, ((980.6667) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((981.0000) * transformY) + moveY),

        new createjs.Point(((117.3333) * transformX) + moveX, ((982.3332) * transformY) + moveY),
        new createjs.Point(((117.6667) * transformX) + moveX, ((983.6668) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((985.0000) * transformY) + moveY),

        new createjs.Point(((118.3333) * transformX) + moveX, ((985.0000) * transformY) + moveY),
        new createjs.Point(((118.6667) * transformX) + moveX, ((985.0000) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((985.0000) * transformY) + moveY),

        new createjs.Point(((119.3333) * transformX) + moveX, ((985.9999) * transformY) + moveY),
        new createjs.Point(((119.6667) * transformX) + moveX, ((987.0001) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((988.0000) * transformY) + moveY),

        new createjs.Point(((120.6666) * transformX) + moveX, ((988.3333) * transformY) + moveY),
        new createjs.Point(((121.3334) * transformX) + moveX, ((988.6667) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((989.0000) * transformY) + moveY),

        new createjs.Point(((122.0000) * transformX) + moveX, ((989.6666) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((990.3334) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((991.0000) * transformY) + moveY),

        new createjs.Point(((122.9999) * transformX) + moveX, ((991.6666) * transformY) + moveY),
        new createjs.Point(((124.0001) * transformX) + moveX, ((992.3334) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((993.0000) * transformY) + moveY),

        new createjs.Point(((125.0000) * transformX) + moveX, ((993.6666) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((994.3334) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((995.0000) * transformY) + moveY),

        new createjs.Point(((125.6666) * transformX) + moveX, ((995.3333) * transformY) + moveY),
        new createjs.Point(((126.3334) * transformX) + moveX, ((995.6667) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((996.0000) * transformY) + moveY),

        new createjs.Point(((127.0000) * transformX) + moveX, ((996.6666) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((997.3334) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((998.0000) * transformY) + moveY),

        new createjs.Point(((127.9999) * transformX) + moveX, ((998.6666) * transformY) + moveY),
        new createjs.Point(((129.0001) * transformX) + moveX, ((999.3334) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1000.0000) * transformY) + moveY),

        new createjs.Point(((130.0000) * transformX) + moveX, ((1000.6666) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1001.3334) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1002.0000) * transformY) + moveY),

        new createjs.Point(((130.9999) * transformX) + moveX, ((1002.6666) * transformY) + moveY),
        new createjs.Point(((132.0001) * transformX) + moveX, ((1003.3334) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((1004.0000) * transformY) + moveY),

        new createjs.Point(((133.0000) * transformX) + moveX, ((1004.6666) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((1005.3334) * transformY) + moveY),
        new createjs.Point(((133.0000) * transformX) + moveX, ((1006.0000) * transformY) + moveY),

        new createjs.Point(((134.3332) * transformX) + moveX, ((1006.9999) * transformY) + moveY),
        new createjs.Point(((135.6668) * transformX) + moveX, ((1008.0001) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1009.0000) * transformY) + moveY),

        new createjs.Point(((137.0000) * transformX) + moveX, ((1009.6666) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1010.3334) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1011.0000) * transformY) + moveY),

        new createjs.Point(((138.9998) * transformX) + moveX, ((1012.6665) * transformY) + moveY),
        new createjs.Point(((141.0002) * transformX) + moveX, ((1014.3335) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1016.0000) * transformY) + moveY),

        new createjs.Point(((143.0000) * transformX) + moveX, ((1016.6666) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1017.3334) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1018.0000) * transformY) + moveY),

        new createjs.Point(((145.6664) * transformX) + moveX, ((1020.3331) * transformY) + moveY),
        new createjs.Point(((148.3336) * transformX) + moveX, ((1022.6669) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1025.0000) * transformY) + moveY),

        new createjs.Point(((156.6661) * transformX) + moveX, ((1030.9994) * transformY) + moveY),
        new createjs.Point(((162.3339) * transformX) + moveX, ((1037.0006) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1043.0000) * transformY) + moveY),

        new createjs.Point(((168.6666) * transformX) + moveX, ((1043.0000) * transformY) + moveY),
        new createjs.Point(((169.3334) * transformX) + moveX, ((1043.0000) * transformY) + moveY),
        new createjs.Point(((170.0000) * transformX) + moveX, ((1043.0000) * transformY) + moveY),

        new createjs.Point(((171.3332) * transformX) + moveX, ((1044.6665) * transformY) + moveY),
        new createjs.Point(((172.6668) * transformX) + moveX, ((1046.3335) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((1048.0000) * transformY) + moveY),

        new createjs.Point(((174.6666) * transformX) + moveX, ((1048.0000) * transformY) + moveY),
        new createjs.Point(((175.3334) * transformX) + moveX, ((1048.0000) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((1048.0000) * transformY) + moveY),

        new createjs.Point(((176.9999) * transformX) + moveX, ((1049.3332) * transformY) + moveY),
        new createjs.Point(((178.0001) * transformX) + moveX, ((1050.6668) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((1052.0000) * transformY) + moveY),

        new createjs.Point(((179.6666) * transformX) + moveX, ((1052.0000) * transformY) + moveY),
        new createjs.Point(((180.3334) * transformX) + moveX, ((1052.0000) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((1052.0000) * transformY) + moveY),

        new createjs.Point(((181.6666) * transformX) + moveX, ((1052.9999) * transformY) + moveY),
        new createjs.Point(((182.3334) * transformX) + moveX, ((1054.0001) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((1055.0000) * transformY) + moveY),

        new createjs.Point(((183.6666) * transformX) + moveX, ((1055.0000) * transformY) + moveY),
        new createjs.Point(((184.3334) * transformX) + moveX, ((1055.0000) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((1055.0000) * transformY) + moveY),

        new createjs.Point(((185.6666) * transformX) + moveX, ((1055.9999) * transformY) + moveY),
        new createjs.Point(((186.3334) * transformX) + moveX, ((1057.0001) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((1058.0000) * transformY) + moveY),

        new createjs.Point(((187.9999) * transformX) + moveX, ((1058.3333) * transformY) + moveY),
        new createjs.Point(((189.0001) * transformX) + moveX, ((1058.6667) * transformY) + moveY),
        new createjs.Point(((190.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),

        new createjs.Point(((190.3333) * transformX) + moveX, ((1059.6666) * transformY) + moveY),
        new createjs.Point(((190.6667) * transformX) + moveX, ((1060.3334) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((1061.0000) * transformY) + moveY),

        new createjs.Point(((191.6666) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((192.3334) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((193.0000) * transformX) + moveX, ((1061.0000) * transformY) + moveY),

        new createjs.Point(((193.3333) * transformX) + moveX, ((1061.6666) * transformY) + moveY),
        new createjs.Point(((193.6667) * transformX) + moveX, ((1062.3334) * transformY) + moveY),
        new createjs.Point(((194.0000) * transformX) + moveX, ((1063.0000) * transformY) + moveY),

        new createjs.Point(((194.9999) * transformX) + moveX, ((1063.3333) * transformY) + moveY),
        new createjs.Point(((196.0001) * transformX) + moveX, ((1063.6667) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((1064.0000) * transformY) + moveY),

        new createjs.Point(((197.0000) * transformX) + moveX, ((1064.3333) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((1064.6667) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((1065.0000) * transformY) + moveY),

        new createjs.Point(((197.6666) * transformX) + moveX, ((1065.0000) * transformY) + moveY),
        new createjs.Point(((198.3334) * transformX) + moveX, ((1065.0000) * transformY) + moveY),
        new createjs.Point(((199.0000) * transformX) + moveX, ((1065.0000) * transformY) + moveY),

        new createjs.Point(((199.3333) * transformX) + moveX, ((1065.6666) * transformY) + moveY),
        new createjs.Point(((199.6667) * transformX) + moveX, ((1066.3334) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1067.0000) * transformY) + moveY),

        new createjs.Point(((200.9999) * transformX) + moveX, ((1067.3333) * transformY) + moveY),
        new createjs.Point(((202.0001) * transformX) + moveX, ((1067.6667) * transformY) + moveY),
        new createjs.Point(((203.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),

        new createjs.Point(((203.0000) * transformX) + moveX, ((1068.3333) * transformY) + moveY),
        new createjs.Point(((203.0000) * transformX) + moveX, ((1068.6667) * transformY) + moveY),
        new createjs.Point(((203.0000) * transformX) + moveX, ((1069.0000) * transformY) + moveY),

        new createjs.Point(((204.3332) * transformX) + moveX, ((1069.3333) * transformY) + moveY),
        new createjs.Point(((205.6668) * transformX) + moveX, ((1069.6667) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((1070.0000) * transformY) + moveY),

        new createjs.Point(((207.3333) * transformX) + moveX, ((1070.6666) * transformY) + moveY),
        new createjs.Point(((207.6667) * transformX) + moveX, ((1071.3334) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((1072.0000) * transformY) + moveY),

        new createjs.Point(((209.3332) * transformX) + moveX, ((1072.3333) * transformY) + moveY),
        new createjs.Point(((210.6668) * transformX) + moveX, ((1072.6667) * transformY) + moveY),
        new createjs.Point(((212.0000) * transformX) + moveX, ((1073.0000) * transformY) + moveY),

        new createjs.Point(((212.3333) * transformX) + moveX, ((1073.6666) * transformY) + moveY),
        new createjs.Point(((212.6667) * transformX) + moveX, ((1074.3334) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((1075.0000) * transformY) + moveY),

        new createjs.Point(((213.6666) * transformX) + moveX, ((1075.0000) * transformY) + moveY),
        new createjs.Point(((214.3334) * transformX) + moveX, ((1075.0000) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((1075.0000) * transformY) + moveY),

        new createjs.Point(((215.0000) * transformX) + moveX, ((1075.3333) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((1075.6667) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((1076.0000) * transformY) + moveY),

        new createjs.Point(((215.6666) * transformX) + moveX, ((1076.0000) * transformY) + moveY),
        new createjs.Point(((216.3334) * transformX) + moveX, ((1076.0000) * transformY) + moveY),
        new createjs.Point(((217.0000) * transformX) + moveX, ((1076.0000) * transformY) + moveY),

        new createjs.Point(((217.0000) * transformX) + moveX, ((1076.3333) * transformY) + moveY),
        new createjs.Point(((217.0000) * transformX) + moveX, ((1076.6667) * transformY) + moveY),
        new createjs.Point(((217.0000) * transformX) + moveX, ((1077.0000) * transformY) + moveY),

        new createjs.Point(((217.6666) * transformX) + moveX, ((1077.0000) * transformY) + moveY),
        new createjs.Point(((218.3334) * transformX) + moveX, ((1077.0000) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((1077.0000) * transformY) + moveY),

        new createjs.Point(((219.0000) * transformX) + moveX, ((1077.3333) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((1077.6667) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((1078.0000) * transformY) + moveY),

        new createjs.Point(((219.6666) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((220.3334) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((221.0000) * transformX) + moveX, ((1078.0000) * transformY) + moveY),

        new createjs.Point(((221.0000) * transformX) + moveX, ((1078.3333) * transformY) + moveY),
        new createjs.Point(((221.0000) * transformX) + moveX, ((1078.6667) * transformY) + moveY),
        new createjs.Point(((221.0000) * transformX) + moveX, ((1079.0000) * transformY) + moveY),

        new createjs.Point(((221.6666) * transformX) + moveX, ((1079.0000) * transformY) + moveY),
        new createjs.Point(((222.3334) * transformX) + moveX, ((1079.0000) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1079.0000) * transformY) + moveY),

        new createjs.Point(((223.0000) * transformX) + moveX, ((1079.3333) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1079.6667) * transformY) + moveY),
        new createjs.Point(((223.0000) * transformX) + moveX, ((1080.0000) * transformY) + moveY),

        new createjs.Point(((224.3332) * transformX) + moveX, ((1080.3333) * transformY) + moveY),
        new createjs.Point(((225.6668) * transformX) + moveX, ((1080.6667) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1081.0000) * transformY) + moveY),

        new createjs.Point(((227.0000) * transformX) + moveX, ((1081.3333) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1081.6667) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1082.0000) * transformY) + moveY),

        new createjs.Point(((227.6666) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((228.3334) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1082.0000) * transformY) + moveY),

        new createjs.Point(((229.0000) * transformX) + moveX, ((1082.3333) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1082.6667) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1083.0000) * transformY) + moveY),

        new createjs.Point(((229.6666) * transformX) + moveX, ((1083.0000) * transformY) + moveY),
        new createjs.Point(((230.3334) * transformX) + moveX, ((1083.0000) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1083.0000) * transformY) + moveY),

        new createjs.Point(((231.0000) * transformX) + moveX, ((1083.3333) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1083.6667) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1084.0000) * transformY) + moveY),

        new createjs.Point(((231.6666) * transformX) + moveX, ((1084.0000) * transformY) + moveY),
        new createjs.Point(((232.3334) * transformX) + moveX, ((1084.0000) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1084.0000) * transformY) + moveY),

        new createjs.Point(((233.0000) * transformX) + moveX, ((1084.3333) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1084.6667) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1085.0000) * transformY) + moveY),

        new createjs.Point(((233.6666) * transformX) + moveX, ((1085.0000) * transformY) + moveY),
        new createjs.Point(((234.3334) * transformX) + moveX, ((1085.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1085.0000) * transformY) + moveY),

        new createjs.Point(((235.0000) * transformX) + moveX, ((1085.3333) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1085.6667) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1086.0000) * transformY) + moveY),

        new createjs.Point(((236.3332) * transformX) + moveX, ((1086.3333) * transformY) + moveY),
        new createjs.Point(((237.6668) * transformX) + moveX, ((1086.6667) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1087.0000) * transformY) + moveY),

        new createjs.Point(((239.0000) * transformX) + moveX, ((1087.3333) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1087.6667) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1088.0000) * transformY) + moveY),

        new createjs.Point(((239.6666) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((240.3334) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1088.0000) * transformY) + moveY),

        new createjs.Point(((241.0000) * transformX) + moveX, ((1088.3333) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1088.6667) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1089.0000) * transformY) + moveY),

        new createjs.Point(((241.9999) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((243.0001) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((1089.0000) * transformY) + moveY),

        new createjs.Point(((244.0000) * transformX) + moveX, ((1089.3333) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((1089.6667) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((1090.0000) * transformY) + moveY),

        new createjs.Point(((244.6666) * transformX) + moveX, ((1090.0000) * transformY) + moveY),
        new createjs.Point(((245.3334) * transformX) + moveX, ((1090.0000) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1090.0000) * transformY) + moveY),

        new createjs.Point(((246.0000) * transformX) + moveX, ((1090.3333) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1090.6667) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),

        new createjs.Point(((246.9999) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((248.0001) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),

        new createjs.Point(((249.0000) * transformX) + moveX, ((1091.3333) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1091.6667) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1092.0000) * transformY) + moveY),

        new createjs.Point(((249.6666) * transformX) + moveX, ((1092.0000) * transformY) + moveY),
        new createjs.Point(((250.3334) * transformX) + moveX, ((1092.0000) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1092.0000) * transformY) + moveY),

        new createjs.Point(((251.0000) * transformX) + moveX, ((1092.3333) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1092.6667) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1093.0000) * transformY) + moveY),

        new createjs.Point(((251.9999) * transformX) + moveX, ((1093.0000) * transformY) + moveY),
        new createjs.Point(((253.0001) * transformX) + moveX, ((1093.0000) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1093.0000) * transformY) + moveY),

        new createjs.Point(((254.0000) * transformX) + moveX, ((1093.3333) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1093.6667) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1094.0000) * transformY) + moveY),

        new createjs.Point(((254.6666) * transformX) + moveX, ((1094.0000) * transformY) + moveY),
        new createjs.Point(((255.3334) * transformX) + moveX, ((1094.0000) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((1094.0000) * transformY) + moveY),

        new createjs.Point(((256.0000) * transformX) + moveX, ((1094.3333) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((1094.6667) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),

        new createjs.Point(((257.9998) * transformX) + moveX, ((1095.3333) * transformY) + moveY),
        new createjs.Point(((260.0002) * transformX) + moveX, ((1095.6667) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((1096.0000) * transformY) + moveY),

        new createjs.Point(((262.0000) * transformX) + moveX, ((1096.3333) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((1096.6667) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),

        new createjs.Point(((262.9999) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((264.0001) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),

        new createjs.Point(((265.0000) * transformX) + moveX, ((1097.3333) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((1097.6667) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((1098.0000) * transformY) + moveY),

        new createjs.Point(((265.9999) * transformX) + moveX, ((1098.0000) * transformY) + moveY),
        new createjs.Point(((267.0001) * transformX) + moveX, ((1098.0000) * transformY) + moveY),
        new createjs.Point(((268.0000) * transformX) + moveX, ((1098.0000) * transformY) + moveY),

        new createjs.Point(((268.0000) * transformX) + moveX, ((1098.3333) * transformY) + moveY),
        new createjs.Point(((268.0000) * transformX) + moveX, ((1098.6667) * transformY) + moveY),
        new createjs.Point(((268.0000) * transformX) + moveX, ((1099.0000) * transformY) + moveY),

        new createjs.Point(((268.9999) * transformX) + moveX, ((1099.0000) * transformY) + moveY),
        new createjs.Point(((270.0001) * transformX) + moveX, ((1099.0000) * transformY) + moveY),
        new createjs.Point(((271.0000) * transformX) + moveX, ((1099.0000) * transformY) + moveY),

        new createjs.Point(((271.0000) * transformX) + moveX, ((1099.3333) * transformY) + moveY),
        new createjs.Point(((271.0000) * transformX) + moveX, ((1099.6667) * transformY) + moveY),
        new createjs.Point(((271.0000) * transformX) + moveX, ((1100.0000) * transformY) + moveY),

        new createjs.Point(((271.9999) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((273.0001) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1100.0000) * transformY) + moveY),

        new createjs.Point(((274.0000) * transformX) + moveX, ((1100.3333) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1100.6667) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1101.0000) * transformY) + moveY),

        new createjs.Point(((274.9999) * transformX) + moveX, ((1101.0000) * transformY) + moveY),
        new createjs.Point(((276.0001) * transformX) + moveX, ((1101.0000) * transformY) + moveY),
        new createjs.Point(((277.0000) * transformX) + moveX, ((1101.0000) * transformY) + moveY),

        new createjs.Point(((277.0000) * transformX) + moveX, ((1101.3333) * transformY) + moveY),
        new createjs.Point(((277.0000) * transformX) + moveX, ((1101.6667) * transformY) + moveY),
        new createjs.Point(((277.0000) * transformX) + moveX, ((1102.0000) * transformY) + moveY),

        new createjs.Point(((278.3332) * transformX) + moveX, ((1102.0000) * transformY) + moveY),
        new createjs.Point(((279.6668) * transformX) + moveX, ((1102.0000) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((1102.0000) * transformY) + moveY),

        new createjs.Point(((281.0000) * transformX) + moveX, ((1102.3333) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((1102.6667) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((1103.0000) * transformY) + moveY),

        new createjs.Point(((282.3332) * transformX) + moveX, ((1103.0000) * transformY) + moveY),
        new createjs.Point(((283.6668) * transformX) + moveX, ((1103.0000) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((1103.0000) * transformY) + moveY),

        new createjs.Point(((285.0000) * transformX) + moveX, ((1103.3333) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((1103.6667) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),

        new createjs.Point(((286.3332) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((287.6668) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),

        new createjs.Point(((289.0000) * transformX) + moveX, ((1104.3333) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((1104.6667) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((1105.0000) * transformY) + moveY),

        new createjs.Point(((290.3332) * transformX) + moveX, ((1105.0000) * transformY) + moveY),
        new createjs.Point(((291.6668) * transformX) + moveX, ((1105.0000) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((1105.0000) * transformY) + moveY),

        new createjs.Point(((293.0000) * transformX) + moveX, ((1105.3333) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((1105.6667) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((1106.0000) * transformY) + moveY),

        new createjs.Point(((294.3332) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((295.6668) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1106.0000) * transformY) + moveY),

        new createjs.Point(((297.0000) * transformX) + moveX, ((1106.3333) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1106.6667) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1107.0000) * transformY) + moveY),

        new createjs.Point(((298.6665) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((300.3335) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((1107.0000) * transformY) + moveY),

        new createjs.Point(((306.5568) * transformX) + moveX, ((1108.4049) * transformY) + moveY),
        new createjs.Point(((313.2918) * transformX) + moveX, ((1109.6226) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((1111.0000) * transformY) + moveY),

        new createjs.Point(((320.3331) * transformX) + moveX, ((1111.0000) * transformY) + moveY),
        new createjs.Point(((322.6669) * transformX) + moveX, ((1111.0000) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((1111.0000) * transformY) + moveY),

        new createjs.Point(((325.0000) * transformX) + moveX, ((1111.3333) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((1111.6667) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),

        new createjs.Point(((327.3331) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((329.6669) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),

        new createjs.Point(((332.0000) * transformX) + moveX, ((1112.3333) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1112.6667) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1113.0000) * transformY) + moveY),

        new createjs.Point(((335.3330) * transformX) + moveX, ((1113.0000) * transformY) + moveY),
        new createjs.Point(((338.6670) * transformX) + moveX, ((1113.0000) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((1113.0000) * transformY) + moveY),

        new createjs.Point(((342.0000) * transformX) + moveX, ((1113.3333) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((1113.6667) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((1114.0000) * transformY) + moveY),

        new createjs.Point(((346.3329) * transformX) + moveX, ((1114.0000) * transformY) + moveY),
        new createjs.Point(((350.6671) * transformX) + moveX, ((1114.0000) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((1114.0000) * transformY) + moveY),

        new createjs.Point(((355.0000) * transformX) + moveX, ((1114.3333) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((1114.6667) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((1115.0000) * transformY) + moveY),

        new createjs.Point(((360.6661) * transformX) + moveX, ((1115.0000) * transformY) + moveY),
        new createjs.Point(((366.3339) * transformX) + moveX, ((1115.0000) * transformY) + moveY),
        new createjs.Point(((372.0000) * transformX) + moveX, ((1115.0000) * transformY) + moveY),

        new createjs.Point(((386.3735) * transformX) + moveX, ((1115.0049) * transformY) + moveY),
        new createjs.Point(((400.8309) * transformX) + moveX, ((1115.2877) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),

        new createjs.Point(((413.9998) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((416.0002) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),

        new createjs.Point(((418.0000) * transformX) + moveX, ((1111.6667) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((1111.3333) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((1111.0000) * transformY) + moveY),

        new createjs.Point(((419.9998) * transformX) + moveX, ((1111.0000) * transformY) + moveY),
        new createjs.Point(((422.0002) * transformX) + moveX, ((1111.0000) * transformY) + moveY),
        new createjs.Point(((424.0000) * transformX) + moveX, ((1111.0000) * transformY) + moveY),

        new createjs.Point(((424.0000) * transformX) + moveX, ((1110.6667) * transformY) + moveY),
        new createjs.Point(((424.0000) * transformX) + moveX, ((1110.3333) * transformY) + moveY),
        new createjs.Point(((424.0000) * transformX) + moveX, ((1110.0000) * transformY) + moveY),

        new createjs.Point(((425.3332) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((426.6668) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((1110.0000) * transformY) + moveY),

        new createjs.Point(((428.0000) * transformX) + moveX, ((1109.6667) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((1109.3333) * transformY) + moveY),
        new createjs.Point(((428.0000) * transformX) + moveX, ((1109.0000) * transformY) + moveY),

        new createjs.Point(((429.6665) * transformX) + moveX, ((1109.0000) * transformY) + moveY),
        new createjs.Point(((431.3335) * transformX) + moveX, ((1109.0000) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((1109.0000) * transformY) + moveY),

        new createjs.Point(((433.0000) * transformX) + moveX, ((1108.6667) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((1108.3333) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((1108.0000) * transformY) + moveY),

        new createjs.Point(((434.3332) * transformX) + moveX, ((1108.0000) * transformY) + moveY),
        new createjs.Point(((435.6668) * transformX) + moveX, ((1108.0000) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((1108.0000) * transformY) + moveY),

        new createjs.Point(((437.0000) * transformX) + moveX, ((1107.6667) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((1107.3333) * transformY) + moveY),
        new createjs.Point(((437.0000) * transformX) + moveX, ((1107.0000) * transformY) + moveY),

        new createjs.Point(((438.3332) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((439.6668) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((441.0000) * transformX) + moveX, ((1107.0000) * transformY) + moveY),

        new createjs.Point(((441.0000) * transformX) + moveX, ((1106.6667) * transformY) + moveY),
        new createjs.Point(((441.0000) * transformX) + moveX, ((1106.3333) * transformY) + moveY),
        new createjs.Point(((441.0000) * transformX) + moveX, ((1106.0000) * transformY) + moveY),

        new createjs.Point(((442.3332) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((443.6668) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((1106.0000) * transformY) + moveY),

        new createjs.Point(((445.0000) * transformX) + moveX, ((1105.6667) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((1105.3333) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((1105.0000) * transformY) + moveY),

        new createjs.Point(((446.3332) * transformX) + moveX, ((1105.0000) * transformY) + moveY),
        new createjs.Point(((447.6668) * transformX) + moveX, ((1105.0000) * transformY) + moveY),
        new createjs.Point(((449.0000) * transformX) + moveX, ((1105.0000) * transformY) + moveY),

        new createjs.Point(((449.0000) * transformX) + moveX, ((1104.6667) * transformY) + moveY),
        new createjs.Point(((449.0000) * transformX) + moveX, ((1104.3333) * transformY) + moveY),
        new createjs.Point(((449.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),

        new createjs.Point(((450.3332) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((451.6668) * transformX) + moveX, ((1104.0000) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1104.0000) * transformY) + moveY),

        new createjs.Point(((453.0000) * transformX) + moveX, ((1103.6667) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1103.3333) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1103.0000) * transformY) + moveY),

        new createjs.Point(((453.9999) * transformX) + moveX, ((1103.0000) * transformY) + moveY),
        new createjs.Point(((455.0001) * transformX) + moveX, ((1103.0000) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1103.0000) * transformY) + moveY),

        new createjs.Point(((456.0000) * transformX) + moveX, ((1102.6667) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1102.3333) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1102.0000) * transformY) + moveY),

        new createjs.Point(((456.9999) * transformX) + moveX, ((1102.0000) * transformY) + moveY),
        new createjs.Point(((458.0001) * transformX) + moveX, ((1102.0000) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1102.0000) * transformY) + moveY),

        new createjs.Point(((459.0000) * transformX) + moveX, ((1101.6667) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1101.3333) * transformY) + moveY),
        new createjs.Point(((459.0000) * transformX) + moveX, ((1101.0000) * transformY) + moveY),

        new createjs.Point(((460.3332) * transformX) + moveX, ((1101.0000) * transformY) + moveY),
        new createjs.Point(((461.6668) * transformX) + moveX, ((1101.0000) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((1101.0000) * transformY) + moveY),

        new createjs.Point(((463.0000) * transformX) + moveX, ((1100.6667) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((1100.3333) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((1100.0000) * transformY) + moveY),

        new createjs.Point(((463.9999) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((465.0001) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((466.0000) * transformX) + moveX, ((1100.0000) * transformY) + moveY),

        new createjs.Point(((466.0000) * transformX) + moveX, ((1099.6667) * transformY) + moveY),
        new createjs.Point(((466.0000) * transformX) + moveX, ((1099.3333) * transformY) + moveY),
        new createjs.Point(((466.0000) * transformX) + moveX, ((1099.0000) * transformY) + moveY),

        new createjs.Point(((466.9999) * transformX) + moveX, ((1099.0000) * transformY) + moveY),
        new createjs.Point(((468.0001) * transformX) + moveX, ((1099.0000) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((1099.0000) * transformY) + moveY),

        new createjs.Point(((469.0000) * transformX) + moveX, ((1098.6667) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((1098.3333) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((1098.0000) * transformY) + moveY),

        new createjs.Point(((469.9999) * transformX) + moveX, ((1098.0000) * transformY) + moveY),
        new createjs.Point(((471.0001) * transformX) + moveX, ((1098.0000) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((1098.0000) * transformY) + moveY),

        new createjs.Point(((472.0000) * transformX) + moveX, ((1097.6667) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((1097.3333) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),

        new createjs.Point(((473.9998) * transformX) + moveX, ((1096.6667) * transformY) + moveY),
        new createjs.Point(((476.0002) * transformX) + moveX, ((1096.3333) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1096.0000) * transformY) + moveY),

        new createjs.Point(((478.0000) * transformX) + moveX, ((1095.6667) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1095.3333) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),

        new createjs.Point(((478.6666) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((479.3334) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),

        new createjs.Point(((480.0000) * transformX) + moveX, ((1094.6667) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((1094.3333) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((1094.0000) * transformY) + moveY),

        new createjs.Point(((482.9997) * transformX) + moveX, ((1093.3334) * transformY) + moveY),
        new createjs.Point(((486.0003) * transformX) + moveX, ((1092.6666) * transformY) + moveY),
        new createjs.Point(((489.0000) * transformX) + moveX, ((1092.0000) * transformY) + moveY),

        new createjs.Point(((489.0000) * transformX) + moveX, ((1091.6667) * transformY) + moveY),
        new createjs.Point(((489.0000) * transformX) + moveX, ((1091.3333) * transformY) + moveY),
        new createjs.Point(((489.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),

        new createjs.Point(((489.6666) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((490.3334) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((491.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),

        new createjs.Point(((491.0000) * transformX) + moveX, ((1090.6667) * transformY) + moveY),
        new createjs.Point(((491.0000) * transformX) + moveX, ((1090.3333) * transformY) + moveY),
        new createjs.Point(((491.0000) * transformX) + moveX, ((1090.0000) * transformY) + moveY),

        new createjs.Point(((491.9999) * transformX) + moveX, ((1090.0000) * transformY) + moveY),
        new createjs.Point(((493.0001) * transformX) + moveX, ((1090.0000) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((1090.0000) * transformY) + moveY),

        new createjs.Point(((494.0000) * transformX) + moveX, ((1089.6667) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((1089.3333) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((1089.0000) * transformY) + moveY),

        new createjs.Point(((494.6666) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((495.3334) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((1089.0000) * transformY) + moveY),

        new createjs.Point(((496.0000) * transformX) + moveX, ((1088.6667) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((1088.3333) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((1088.0000) * transformY) + moveY),

        new createjs.Point(((496.9999) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((498.0001) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((499.0000) * transformX) + moveX, ((1088.0000) * transformY) + moveY),

        new createjs.Point(((499.0000) * transformX) + moveX, ((1087.6667) * transformY) + moveY),
        new createjs.Point(((499.0000) * transformX) + moveX, ((1087.3333) * transformY) + moveY),
        new createjs.Point(((499.0000) * transformX) + moveX, ((1087.0000) * transformY) + moveY),

        new createjs.Point(((500.3332) * transformX) + moveX, ((1086.6667) * transformY) + moveY),
        new createjs.Point(((501.6668) * transformX) + moveX, ((1086.3333) * transformY) + moveY),
        new createjs.Point(((503.0000) * transformX) + moveX, ((1086.0000) * transformY) + moveY),

        new createjs.Point(((503.0000) * transformX) + moveX, ((1085.6667) * transformY) + moveY),
        new createjs.Point(((503.0000) * transformX) + moveX, ((1085.3333) * transformY) + moveY),
        new createjs.Point(((503.0000) * transformX) + moveX, ((1085.0000) * transformY) + moveY),

        new createjs.Point(((503.9999) * transformX) + moveX, ((1085.0000) * transformY) + moveY),
        new createjs.Point(((505.0001) * transformX) + moveX, ((1085.0000) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((1085.0000) * transformY) + moveY),

        new createjs.Point(((506.0000) * transformX) + moveX, ((1084.6667) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((1084.3333) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((1084.0000) * transformY) + moveY),

        new createjs.Point(((507.3332) * transformX) + moveX, ((1083.6667) * transformY) + moveY),
        new createjs.Point(((508.6668) * transformX) + moveX, ((1083.3333) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((1083.0000) * transformY) + moveY),

        new createjs.Point(((510.0000) * transformX) + moveX, ((1082.6667) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((1082.3333) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((1082.0000) * transformY) + moveY),

        new createjs.Point(((510.9999) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((512.0001) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1082.0000) * transformY) + moveY),

        new createjs.Point(((513.0000) * transformX) + moveX, ((1081.6667) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1081.3333) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1081.0000) * transformY) + moveY),

        new createjs.Point(((513.6666) * transformX) + moveX, ((1081.0000) * transformY) + moveY),
        new createjs.Point(((514.3334) * transformX) + moveX, ((1081.0000) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1081.0000) * transformY) + moveY),

        new createjs.Point(((515.0000) * transformX) + moveX, ((1080.6667) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1080.3333) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1080.0000) * transformY) + moveY),

        new createjs.Point(((516.3332) * transformX) + moveX, ((1079.6667) * transformY) + moveY),
        new createjs.Point(((517.6668) * transformX) + moveX, ((1079.3333) * transformY) + moveY),
        new createjs.Point(((519.0000) * transformX) + moveX, ((1079.0000) * transformY) + moveY),

        new createjs.Point(((519.0000) * transformX) + moveX, ((1078.6667) * transformY) + moveY),
        new createjs.Point(((519.0000) * transformX) + moveX, ((1078.3333) * transformY) + moveY),
        new createjs.Point(((519.0000) * transformX) + moveX, ((1078.0000) * transformY) + moveY),

        new createjs.Point(((519.9999) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((521.0001) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((1078.0000) * transformY) + moveY),

        new createjs.Point(((522.0000) * transformX) + moveX, ((1077.6667) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((1077.3333) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((1077.0000) * transformY) + moveY),

        new createjs.Point(((522.6666) * transformX) + moveX, ((1077.0000) * transformY) + moveY),
        new createjs.Point(((523.3334) * transformX) + moveX, ((1077.0000) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((1077.0000) * transformY) + moveY),

        new createjs.Point(((524.0000) * transformX) + moveX, ((1076.6667) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((1076.3333) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((1076.0000) * transformY) + moveY),

        new createjs.Point(((525.3332) * transformX) + moveX, ((1075.6667) * transformY) + moveY),
        new createjs.Point(((526.6668) * transformX) + moveX, ((1075.3333) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((1075.0000) * transformY) + moveY),

        new createjs.Point(((528.0000) * transformX) + moveX, ((1074.6667) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((1074.3333) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((1074.0000) * transformY) + moveY),

        new createjs.Point(((529.3332) * transformX) + moveX, ((1073.6667) * transformY) + moveY),
        new createjs.Point(((530.6668) * transformX) + moveX, ((1073.3333) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1073.0000) * transformY) + moveY),

        new createjs.Point(((532.0000) * transformX) + moveX, ((1072.6667) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1072.3333) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1072.0000) * transformY) + moveY),

        new createjs.Point(((532.6666) * transformX) + moveX, ((1072.0000) * transformY) + moveY),
        new createjs.Point(((533.3334) * transformX) + moveX, ((1072.0000) * transformY) + moveY),
        new createjs.Point(((534.0000) * transformX) + moveX, ((1072.0000) * transformY) + moveY),

        new createjs.Point(((534.0000) * transformX) + moveX, ((1071.6667) * transformY) + moveY),
        new createjs.Point(((534.0000) * transformX) + moveX, ((1071.3333) * transformY) + moveY),
        new createjs.Point(((534.0000) * transformX) + moveX, ((1071.0000) * transformY) + moveY),

        new createjs.Point(((534.6666) * transformX) + moveX, ((1071.0000) * transformY) + moveY),
        new createjs.Point(((535.3334) * transformX) + moveX, ((1071.0000) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((1071.0000) * transformY) + moveY),

        new createjs.Point(((536.0000) * transformX) + moveX, ((1070.6667) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((1070.3333) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((1070.0000) * transformY) + moveY),

        new createjs.Point(((536.6666) * transformX) + moveX, ((1070.0000) * transformY) + moveY),
        new createjs.Point(((537.3334) * transformX) + moveX, ((1070.0000) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((1070.0000) * transformY) + moveY),

        new createjs.Point(((538.0000) * transformX) + moveX, ((1069.6667) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((1069.3333) * transformY) + moveY),
        new createjs.Point(((538.0000) * transformX) + moveX, ((1069.0000) * transformY) + moveY),

        new createjs.Point(((538.6666) * transformX) + moveX, ((1069.0000) * transformY) + moveY),
        new createjs.Point(((539.3334) * transformX) + moveX, ((1069.0000) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((1069.0000) * transformY) + moveY),

        new createjs.Point(((540.0000) * transformX) + moveX, ((1068.6667) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((1068.3333) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),

        new createjs.Point(((540.6666) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((541.3334) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),

        new createjs.Point(((542.0000) * transformX) + moveX, ((1067.6667) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1067.3333) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1067.0000) * transformY) + moveY),

        new createjs.Point(((542.6666) * transformX) + moveX, ((1067.0000) * transformY) + moveY),
        new createjs.Point(((543.3334) * transformX) + moveX, ((1067.0000) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1067.0000) * transformY) + moveY),

        new createjs.Point(((544.0000) * transformX) + moveX, ((1066.6667) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1066.3333) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1066.0000) * transformY) + moveY),

        new createjs.Point(((544.6666) * transformX) + moveX, ((1066.0000) * transformY) + moveY),
        new createjs.Point(((545.3334) * transformX) + moveX, ((1066.0000) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((1066.0000) * transformY) + moveY),

        new createjs.Point(((546.0000) * transformX) + moveX, ((1065.6667) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((1065.3333) * transformY) + moveY),
        new createjs.Point(((546.0000) * transformX) + moveX, ((1065.0000) * transformY) + moveY),

        new createjs.Point(((546.6666) * transformX) + moveX, ((1065.0000) * transformY) + moveY),
        new createjs.Point(((547.3334) * transformX) + moveX, ((1065.0000) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1065.0000) * transformY) + moveY),

        new createjs.Point(((548.0000) * transformX) + moveX, ((1064.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1064.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1064.0000) * transformY) + moveY),

        new createjs.Point(((548.6666) * transformX) + moveX, ((1064.0000) * transformY) + moveY),
        new createjs.Point(((549.3334) * transformX) + moveX, ((1064.0000) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1064.0000) * transformY) + moveY),

        new createjs.Point(((550.0000) * transformX) + moveX, ((1063.6667) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1063.3333) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1063.0000) * transformY) + moveY),

        new createjs.Point(((550.6666) * transformX) + moveX, ((1063.0000) * transformY) + moveY),
        new createjs.Point(((551.3334) * transformX) + moveX, ((1063.0000) * transformY) + moveY),
        new createjs.Point(((552.0000) * transformX) + moveX, ((1063.0000) * transformY) + moveY),

        new createjs.Point(((552.3333) * transformX) + moveX, ((1062.3334) * transformY) + moveY),
        new createjs.Point(((552.6667) * transformX) + moveX, ((1061.6666) * transformY) + moveY),
        new createjs.Point(((553.0000) * transformX) + moveX, ((1061.0000) * transformY) + moveY),

        new createjs.Point(((553.6666) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((554.3334) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((1061.0000) * transformY) + moveY),

        new createjs.Point(((555.0000) * transformX) + moveX, ((1060.6667) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((1060.3333) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((1060.0000) * transformY) + moveY),

        new createjs.Point(((555.6666) * transformX) + moveX, ((1060.0000) * transformY) + moveY),
        new createjs.Point(((556.3334) * transformX) + moveX, ((1060.0000) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1060.0000) * transformY) + moveY),

        new createjs.Point(((557.0000) * transformX) + moveX, ((1059.6667) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1059.3333) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),

        new createjs.Point(((557.6666) * transformX) + moveX, ((1059.0000) * transformY) + moveY),
        new createjs.Point(((558.3334) * transformX) + moveX, ((1059.0000) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),

        new createjs.Point(((559.0000) * transformX) + moveX, ((1058.6667) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1058.3333) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1058.0000) * transformY) + moveY),

        new createjs.Point(((559.6666) * transformX) + moveX, ((1058.0000) * transformY) + moveY),
        new createjs.Point(((560.3334) * transformX) + moveX, ((1058.0000) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1058.0000) * transformY) + moveY),

        new createjs.Point(((561.3333) * transformX) + moveX, ((1057.3334) * transformY) + moveY),
        new createjs.Point(((561.6667) * transformX) + moveX, ((1056.6666) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((1056.0000) * transformY) + moveY),

        new createjs.Point(((563.9998) * transformX) + moveX, ((1055.3334) * transformY) + moveY),
        new createjs.Point(((566.0002) * transformX) + moveX, ((1054.6666) * transformY) + moveY),
        new createjs.Point(((568.0000) * transformX) + moveX, ((1054.0000) * transformY) + moveY),

        new createjs.Point(((568.3333) * transformX) + moveX, ((1053.3334) * transformY) + moveY),
        new createjs.Point(((568.6667) * transformX) + moveX, ((1052.6666) * transformY) + moveY),
        new createjs.Point(((569.0000) * transformX) + moveX, ((1052.0000) * transformY) + moveY),

        new createjs.Point(((570.9998) * transformX) + moveX, ((1051.3334) * transformY) + moveY),
        new createjs.Point(((573.0002) * transformX) + moveX, ((1050.6666) * transformY) + moveY),
        new createjs.Point(((575.0000) * transformX) + moveX, ((1050.0000) * transformY) + moveY),

        new createjs.Point(((575.3333) * transformX) + moveX, ((1049.3334) * transformY) + moveY),
        new createjs.Point(((575.6667) * transformX) + moveX, ((1048.6666) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((1048.0000) * transformY) + moveY),

        new createjs.Point(((577.3332) * transformX) + moveX, ((1047.6667) * transformY) + moveY),
        new createjs.Point(((578.6668) * transformX) + moveX, ((1047.3333) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1047.0000) * transformY) + moveY),

        new createjs.Point(((580.3333) * transformX) + moveX, ((1046.3334) * transformY) + moveY),
        new createjs.Point(((580.6667) * transformX) + moveX, ((1045.6666) * transformY) + moveY),
        new createjs.Point(((581.0000) * transformX) + moveX, ((1045.0000) * transformY) + moveY),

        new createjs.Point(((582.3332) * transformX) + moveX, ((1044.6667) * transformY) + moveY),
        new createjs.Point(((583.6668) * transformX) + moveX, ((1044.3333) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1044.0000) * transformY) + moveY),

        new createjs.Point(((585.0000) * transformX) + moveX, ((1043.6667) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1043.3333) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1043.0000) * transformY) + moveY),

        new createjs.Point(((585.9999) * transformX) + moveX, ((1042.6667) * transformY) + moveY),
        new createjs.Point(((587.0001) * transformX) + moveX, ((1042.3333) * transformY) + moveY),
        new createjs.Point(((588.0000) * transformX) + moveX, ((1042.0000) * transformY) + moveY),

        new createjs.Point(((588.3333) * transformX) + moveX, ((1041.3334) * transformY) + moveY),
        new createjs.Point(((588.6667) * transformX) + moveX, ((1040.6666) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1040.0000) * transformY) + moveY),

        new createjs.Point(((590.3332) * transformX) + moveX, ((1039.6667) * transformY) + moveY),
        new createjs.Point(((591.6668) * transformX) + moveX, ((1039.3333) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((1039.0000) * transformY) + moveY),

        new createjs.Point(((593.3333) * transformX) + moveX, ((1038.3334) * transformY) + moveY),
        new createjs.Point(((593.6667) * transformX) + moveX, ((1037.6666) * transformY) + moveY),
        new createjs.Point(((594.0000) * transformX) + moveX, ((1037.0000) * transformY) + moveY),

        new createjs.Point(((594.6666) * transformX) + moveX, ((1037.0000) * transformY) + moveY),
        new createjs.Point(((595.3334) * transformX) + moveX, ((1037.0000) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((1037.0000) * transformY) + moveY),

        new createjs.Point(((596.0000) * transformX) + moveX, ((1036.6667) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((1036.3333) * transformY) + moveY),
        new createjs.Point(((596.0000) * transformX) + moveX, ((1036.0000) * transformY) + moveY),

        new createjs.Point(((596.9999) * transformX) + moveX, ((1035.6667) * transformY) + moveY),
        new createjs.Point(((598.0001) * transformX) + moveX, ((1035.3333) * transformY) + moveY),
        new createjs.Point(((599.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),

        new createjs.Point(((599.3333) * transformX) + moveX, ((1034.3334) * transformY) + moveY),
        new createjs.Point(((599.6667) * transformX) + moveX, ((1033.6666) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1033.0000) * transformY) + moveY),

        new createjs.Point(((600.6666) * transformX) + moveX, ((1033.0000) * transformY) + moveY),
        new createjs.Point(((601.3334) * transformX) + moveX, ((1033.0000) * transformY) + moveY),
        new createjs.Point(((602.0000) * transformX) + moveX, ((1033.0000) * transformY) + moveY),

        new createjs.Point(((602.3333) * transformX) + moveX, ((1032.3334) * transformY) + moveY),
        new createjs.Point(((602.6667) * transformX) + moveX, ((1031.6666) * transformY) + moveY),
        new createjs.Point(((603.0000) * transformX) + moveX, ((1031.0000) * transformY) + moveY),

        new createjs.Point(((603.6666) * transformX) + moveX, ((1031.0000) * transformY) + moveY),
        new createjs.Point(((604.3334) * transformX) + moveX, ((1031.0000) * transformY) + moveY),
        new createjs.Point(((605.0000) * transformX) + moveX, ((1031.0000) * transformY) + moveY),

        new createjs.Point(((605.3333) * transformX) + moveX, ((1030.3334) * transformY) + moveY),
        new createjs.Point(((605.6667) * transformX) + moveX, ((1029.6666) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((1029.0000) * transformY) + moveY),

        new createjs.Point(((606.6666) * transformX) + moveX, ((1029.0000) * transformY) + moveY),
        new createjs.Point(((607.3334) * transformX) + moveX, ((1029.0000) * transformY) + moveY),
        new createjs.Point(((608.0000) * transformX) + moveX, ((1029.0000) * transformY) + moveY),

        new createjs.Point(((608.0000) * transformX) + moveX, ((1028.6667) * transformY) + moveY),
        new createjs.Point(((608.0000) * transformX) + moveX, ((1028.3333) * transformY) + moveY),
        new createjs.Point(((608.0000) * transformX) + moveX, ((1028.0000) * transformY) + moveY),

        new createjs.Point(((608.9999) * transformX) + moveX, ((1027.6667) * transformY) + moveY),
        new createjs.Point(((610.0001) * transformX) + moveX, ((1027.3333) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1027.0000) * transformY) + moveY),

        new createjs.Point(((611.3333) * transformX) + moveX, ((1026.3334) * transformY) + moveY),
        new createjs.Point(((611.6667) * transformX) + moveX, ((1025.6666) * transformY) + moveY),
        new createjs.Point(((612.0000) * transformX) + moveX, ((1025.0000) * transformY) + moveY),

        new createjs.Point(((612.6666) * transformX) + moveX, ((1025.0000) * transformY) + moveY),
        new createjs.Point(((613.3334) * transformX) + moveX, ((1025.0000) * transformY) + moveY),
        new createjs.Point(((614.0000) * transformX) + moveX, ((1025.0000) * transformY) + moveY),

        new createjs.Point(((614.6666) * transformX) + moveX, ((1024.0001) * transformY) + moveY),
        new createjs.Point(((615.3334) * transformX) + moveX, ((1022.9999) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((1022.0000) * transformY) + moveY),

        new createjs.Point(((616.6666) * transformX) + moveX, ((1022.0000) * transformY) + moveY),
        new createjs.Point(((617.3334) * transformX) + moveX, ((1022.0000) * transformY) + moveY),
        new createjs.Point(((618.0000) * transformX) + moveX, ((1022.0000) * transformY) + moveY),

        new createjs.Point(((618.3333) * transformX) + moveX, ((1021.3334) * transformY) + moveY),
        new createjs.Point(((618.6667) * transformX) + moveX, ((1020.6666) * transformY) + moveY),
        new createjs.Point(((619.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),

        new createjs.Point(((619.6666) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((620.3334) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),

        new createjs.Point(((621.6666) * transformX) + moveX, ((1019.0001) * transformY) + moveY),
        new createjs.Point(((622.3334) * transformX) + moveX, ((1017.9999) * transformY) + moveY),
        new createjs.Point(((623.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),



        new createjs.Point(((827.68) * transformX) + moveX, ((828.94) * transformY) + moveY),
        new createjs.Point(((830.68) * transformX) + moveX, ((831.94) * transformY) + moveY),
        new createjs.Point(((832.68) * transformX) + moveX, ((833.94) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY),


        new createjs.Point(((902.0052) * transformX) + moveX, ((0.0000) * transformY) + moveY),


        new createjs.Point(((954.0000) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((858.6762) * transformX) + moveX, ((632.9367) * transformY) + moveY),
        new createjs.Point(((763.3238) * transformX) + moveX, ((1266.0633) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((667.6667) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((667.3333) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((667.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((667.0000) * transformX) + moveX, ((1899.3333) * transformY) + moveY),
        new createjs.Point(((667.0000) * transformX) + moveX, ((1899.6667) * transformY) + moveY),
        new createjs.Point(((667.0000) * transformX) + moveX, ((1900.0000) * transformY) + moveY),

    ];

    var points2 = [
        new createjs.Point(startX2, startY2),
        new createjs.Point(endX2, endY2)
    ];
    var points1Final = points;

    var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath, motionPath2);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final), getMotionPathFromPoints(points2Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 9", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("32", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 60).call(function() {
            setTimeout(function() {
                gotoLastAndStop(Stage1);
            }, 500);

        })).on('change', (function(event) {

            if (currentMotionStep == 1) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        this.timeline.addTween(cjs.Tween.get(bar2).wait(40).to({
            guide: {
                path: motionPath2
            }
        }, 20).wait(50)).on('change', (function(event) {
            if (currentMotionStep == 2) {
                bar2 = drawCharacter(thisStage, bar2, true, event);
            }
        }));


    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        console.log(thisbar.x + " - " + thisbar.y);
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            debugger
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];

                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }
            
            if (thisbar.x == 634.1657536059089 && thisbar.y == 207.685) { // finished plotting 1/3 part 
                Stage1.shape.visible = false;
            } else if (thisbar.x === startX && thisbar.y === startY) { // plot 1st point
                Stage1.shape.visible = true;
            }

            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.32)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.movie.pencil.visible = true;
            thisStage.paused = false;
            thisStage.movie.play();
        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            thisStage.movie.pencil.visible = true;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            thisStage.movie.pencil.visible = false;
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);

        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }

    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function gotoLastAndStop(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        thisStage.pencil = thisStage.movie.pencil;
        thisStage.addChild(thisStage.pencil);
        currentMotionStep = 1;
        Stage1.shape.visible = false;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
