(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("kunna addera och subtrahera med hela tiotal", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(365, 658);

        this.text_1 = new cjs.Text("127", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(552, 657);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_2 = new cjs.Text("Addera och subtrahera med hela tiotal", "24px 'MyriadPro-Semibold'", "#00A3C4");
        this.text_2.setTransform(109, 42);

        this.text_3 = new cjs.Text("44", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(63, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(50, 23.5, 1.15, 1);

        this.addChild(this.shape_2, this.text_3, this.text_2, this.shape_1, this.text_1, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();
        // Block-1

        this.text = new cjs.Text("Addition", "bold 16px 'Myriad Pro'", "#000000");
        this.text.setTransform(97, 40);

        this.text_1 = new cjs.Text("Subtraktion", "bold 16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(341, 40);

        var ToBeAdded = [];

        var xpos = 27;
        var spiral = new lib.spiral(14);
        spiral.setTransform(xpos, 16.4)

        var xpos1 = 282;
        var spiral1 = new lib.spiral(14);
        spiral1.setTransform(xpos1, 16.4);

        this.roundRect2 = new cjs.Shape(); // white block
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(20, 15, 233, 147, 5);
        this.roundRect2.setTransform(-7, 0);

        this.roundRect3 = new cjs.Shape(); // white block
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(275, 15, 235, 147, 5);
        this.roundRect3.setTransform(-8, 0);

        // Main yellow block
        this.roundRect1 = new cjs.Shape(); // yellow block
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 0, 514, 171, 10);
        this.roundRect1.setTransform(0, 0);

        var xpos = 0;
        for (var col = 0; col < 16; col++) {

            var Line_2 = new cjs.Shape();
            if (col > 13) {
                xpos = 168;
            } else if (col > 8) {
                xpos = 158;
            } else if (col > 3) {
                xpos = 31;
            } else {
                xpos = 13;
            }
            Line_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(45, 31).lineTo(45, 119).lineTo(54, 119).lineTo(54, 31).lineTo(45, 31)
                .moveTo(45, 39.6).lineTo(54, 39.6).moveTo(45, 48.1).lineTo(54, 48.1).moveTo(45, 56.6).lineTo(54, 56.6).moveTo(45, 65.1).lineTo(54, 65.1)
                .moveTo(45, 73.6).lineTo(54, 73.6).moveTo(45, 82.1).lineTo(54, 82.1).moveTo(45, 91.6).lineTo(54, 91.6).moveTo(45, 101.1).lineTo(54, 101.1)
                .moveTo(45, 110.7).lineTo(54, 110.7);
            Line_2.setTransform(xpos + (col * 14.4), 19);
            ToBeAdded.push(Line_2)
        }

        var tmptxt = ""
        var xpos = 63
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 2; col++) {
                if (col == 0 && row == 1) {
                    tmptxt = "40 + 50 = 90";
                    xpos = 88;
                    ypos = 145
                } else if (col == 1 && row == 1) {
                    tmptxt = "70 – 30 = 40";
                    xpos = 86;
                    ypos = 145
                } else {
                    xpos = 76;
                    ypos = 170
                }

                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText);
            }
        }

        this.crossLine = new cjs.Shape();
        this.crossLine.graphics.f("").s("#DB3A58").setStrokeStyle(1).moveTo(0, 15).lineTo(17, 0)
        this.crossLine.setTransform(387, 88);

        this.crossLine2 = this.crossLine.clone(true);
        this.crossLine2.setTransform(410, 86);

        this.crossLine3 = this.crossLine.clone(true);
        this.crossLine3.setTransform(426, 86);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.hrLine, this.hrLine1, this.vrLine1, this.Line_3);
        this.addChild(this.hrLine2, this.hrLine3, this.vrLine2)
        this.addChild(spiral, spiral1, this.text, this.text_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.crossLine, this.crossLine2, this.crossLine3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 530.3, 123.3);

    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Räkna.", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 514, 372, 10);
        this.roundRect1.setTransform(0, 11);

        // Draw horizantial  lines

        var ToBeAdded = [];
        var moveToxposition = 137;
        for (var row = 0; row < 2; row++) {
            if (row == 1) {
                moveToxposition = 141;

            } else {
                moveToxposition = 137;
            }

            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(5, moveToxposition + (row * 119)).lineTo(507, moveToxposition + (row * 119));
            ToBeAdded.push(hrLine)
        };

        // Draw vertical lines

        var moveToypos = 13;
        var lineToypos = 123;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 1; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 142;
                    lineToypos = 255;
                } else if (row == 2) {
                    moveToypos = 263;
                    lineToypos = 376;
                } else {
                    moveToypos = 17;
                    lineToypos = 132;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(171 + (col * 170), moveToypos).lineTo(171 + (col * 170), lineToypos)
                    .moveTo(343 + (col * 170), moveToypos).lineTo(343 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }

        //Row-1

        // Draw green boxes

        var xpos = 0;
        for (var col = 0; col < 17; col++) {
            var Line_2 = new cjs.Shape();
            if (col > 14) {
                xpos = 197;
            } else if (col > 9) {
                xpos = 184;
            } else if (col > 6) {
                xpos = 142;
            } else if (col > 3) {
                xpos = 105;
            } else if (col > 1) {
                xpos = 47;
            } else {
                xpos = -9;
            }
            Line_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(33, 20).lineTo(33, 108).lineTo(42, 108).lineTo(42, 20).lineTo(33, 20)
                .moveTo(33, 28.6).lineTo(42, 28.6).moveTo(33, 37.1).lineTo(42, 37.1).moveTo(33, 45.6).lineTo(42, 45.6).moveTo(33, 54.1).lineTo(42, 54.1)
                .moveTo(33, 62.6).lineTo(42, 62.6).moveTo(33, 71.1).lineTo(42, 71.1).moveTo(33, 80.6).lineTo(42, 80.6).moveTo(33, 90.1).lineTo(42, 90.1)
                .moveTo(33, 99.7).lineTo(42, 99.7);
            Line_2.setTransform(xpos + (col * 14.4), 0);
            ToBeAdded.push(Line_2)
        }

        var tmptxt = ""
        var xpos = 0
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 3; col++) {
                if (col == 0 && row == 1) {
                    tmptxt = "20 + 20 =";
                    xpos = 33.5;
                    ypos = 117
                } else if (col == 1 && row == 1) {
                    tmptxt = "30 + 30 =";
                    xpos = -57;
                    ypos = 117
                } else if (col == 2 && row == 1) {
                    tmptxt = "50 + 20 =";
                    xpos = -143;
                    ypos = 117;

                }

                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText);
            }
        }

        //Row-2

        // Draw green boxes

        var xpos = 0;
        for (var col = 0; col < 22; col++) {
            var Line_4 = new cjs.Shape();
            if (col > 17) {
                xpos = 172;
            } else if (col > 13) {
                xpos = 144;
            } else if (col > 10) {
                xpos = 99;
            } else if (col > 5) {
                xpos = 92;
            } else if (col > 3) {
                xpos = 35;
            } else {
                xpos = 8;
            }
            Line_4.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(16, 145).lineTo(16, 233).lineTo(25, 233).lineTo(25, 145).lineTo(16, 145)
                .moveTo(16, 153.6).lineTo(25, 153.6).moveTo(16, 162.1).lineTo(25, 162.1).moveTo(16, 170.6).lineTo(25, 170.6).moveTo(16, 179.1).lineTo(25, 179.1)
                .moveTo(16, 187.6).lineTo(25, 187.6).moveTo(16, 196.1).lineTo(25, 196.1).moveTo(16, 205.6).lineTo(25, 205.6).moveTo(16, 215.1).lineTo(25, 215.1)
                .moveTo(16, 224.7).lineTo(25, 224.7);
            Line_4.setTransform(xpos + (col * 14.4), -2);
            ToBeAdded.push(Line_4)
        }

        var tmptxt1 = ""
        var xpos = 0
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 3; col++) {
                if (col == 0 && row == 1) {
                    tmptxt1 = "40 + 20 =";
                    xpos = 25.5;
                    ypos = 240
                } else if (col == 1 && row == 1) {
                    tmptxt1 = "50 + 30 =";
                    xpos = -54;
                    ypos = 240
                } else if (col == 2 && row == 1) {
                    tmptxt1 = "40 + 40 =";
                    xpos = -143;
                    ypos = 240;

                }

                var tempText1 = new cjs.Text(tmptxt1, "16px 'Myriad Pro'");
                tempText1.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText1);
            }
        }

        //Row-3

        // Draw green boxes

        var xpos = 0;
        for (var col = 0; col < 24; col++) {
            var Line_6 = new cjs.Shape();

            if (col > 19) {
                xpos = 142;
            } else if (col > 14) {
                xpos = 130;
            } else if (col > 12) {
                xpos = 74;
            } else if (col > 7) {
                xpos = 66;
            } else if (col > 4) {
                xpos = 20;
            } else {
                xpos = 8;
            }
            Line_6.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(16, 271).lineTo(16, 359).lineTo(25, 359).lineTo(25, 271).lineTo(16, 271)
                .moveTo(16, 279.6).lineTo(25, 279.6).moveTo(16, 288.1).lineTo(25, 288.1).moveTo(16, 296.6).lineTo(25, 296.6).moveTo(16, 305.1).lineTo(25, 305.1)
                .moveTo(16, 313.6).lineTo(25, 313.6).moveTo(16, 322.1).lineTo(25, 322.1).moveTo(16, 331.6).lineTo(25, 331.6).moveTo(16, 341.1).lineTo(25, 341.1)
                .moveTo(16, 350.7).lineTo(25, 350.7);
            Line_6.setTransform(xpos + (col * 14.4), -5);
            ToBeAdded.push(Line_6)
        }

        var tmptxt2 = ""
        var xpos = 0
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 3; col++) {
                if (col == 0 && row == 1) {
                    tmptxt2 = "80 – 10 =";
                    xpos = 25.5;
                    ypos = 363
                } else if (col == 1 && row == 1) {
                    tmptxt2 = "70 – 20 =";
                    xpos = -57;
                    ypos = 365
                } else if (col == 2 && row == 1) {
                    tmptxt2 = "90 – 40 =";
                    xpos = -143;
                    ypos = 363;

                }

                var tempText2 = new cjs.Text(tmptxt2, "16px 'Myriad Pro'");
                tempText2.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText2);
            }
        }

        // draw small horizantal lines

        var moveToxpos = 0;
        var lineToxpos = 0;
        var ypos = 130;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 3; col++) {
                var hrLine1 = new cjs.Shape();

                if ((row == 0 || row == 2) && col == 3) {
                    moveToxpos = 65;
                    lineToxpos = 105;
                } else if (row == 1 && col == 3) {
                    moveToxpos = 61;
                    lineToxpos = 101;
                } else if ((row == 1 || row == 2) && col == 0) {
                    moveToxpos = 90;
                    lineToxpos = 130;
                } else if (row == 0 && col == 0) {
                    moveToxpos = 99;
                    lineToxpos = 139;
                } else if ((row == 0 || row == 2) && col == 1) {
                    moveToxpos = 91;
                    lineToxpos = 131;
                } else if (row == 1 && col == 1) {
                    moveToxpos = 94;
                    lineToxpos = 134;
                }

                if (row == 0) {
                    ypos = 129;
                } else if (row == 1) {
                    ypos = 126;
                } else if (row == 2) {
                    ypos = 123;
                }
                hrLine1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(moveToxpos + (col * 172), ypos + (row * 126))
                    .lineTo(lineToxpos + (col * 172), ypos + (row * 126));
                ToBeAdded.push(hrLine1)
            }
        }

        this.addChild(this.text, this.text_1, this.roundRect1);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 530.3, 380);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(307, 108, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(307, 305, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
