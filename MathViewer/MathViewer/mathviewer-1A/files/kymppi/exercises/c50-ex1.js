var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section4/images/p145_1.png",
            id: "p145_1"
        }, {
            src: "../section4/images/p145_2.png",
            id: "p145_2"
        }]
    };

    (lib.p145_1 = function() {
        this.initialize(img.p145_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p145_2 = function() {
        this.initialize(img.p145_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Testa dina kunskaper", "bold 24px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("50", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("----", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550 + 100, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();

        this.text = new cjs.Text("Karusellspelet", "18px 'MyriadPro-Semibold'", "#8390C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, -7);

        this.text_2 = new cjs.Text("Spel för 2.", "16px 'Myriad Pro'", "#8390C8");
        this.text_2.lineHeight = 20;
        this.text_2.setTransform(0, 15);

        this.text_1 = new cjs.Text("•  Slå 2 tärningar.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 60);

        this.text_3 = new cjs.Text("•  Addera eller subtrahera tärningstalen.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(0, 80);

        this.text_4 = new cjs.Text("•  Måla svaret i din karusell.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(0, 102);

        this.text_5 = new cjs.Text("•  Turen går över till näste spelare.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(0, 124);

        this.text_6 = new cjs.Text("•  Den som först har målat alla tal i sin karusell\n    vinner omgången.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(0, 146);

        this.addChild(this.text_1);
        this.addChild(this.text, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.GameBoard = function() {
        this.initialize();

        this.instance = new lib.p145_1();
        this.instance.setTransform(400, 110, 0.375, 0.375);
        this.text_9 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(310, 119);
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#848FC7').drawRoundRect(296, 106, 197, 38, 7);
        this.roundRect2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(-6, 150, 500, 432, 13);
        this.roundRect1.setTransform(0, 0);

        this.instance_2 = new lib.p145_2();
        this.instance_2.setTransform(70, 305, 0.45, 0.45);

        this.text_7 = new cjs.Text("Omgång 1", "16px 'MyriadPro-Semibold'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(5.5, 161);

        this.text_8 = new cjs.Text("Omgång 2", "16px 'MyriadPro-Semibold'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(5.5, 413);

        var circleArr = [];

        var ArrCircle1x = ['100', '131.5', '153', '157', '145', '116', '83', '55', '41', '45', '68'];
        var ArrCircle1y = ['179.5', '188', '213.5', '246.5', '277.5', '294', '294', '277', '246.5', '213.5', '188.5'];
        var ArrCircle2x = ['402', '433.5', '455', '459', '447', '418', '385', '357', '343', '347', '370'];
        var ArrCircle2y = ['179.5', '188', '213.5', '246.5', '277.5', '294', '294', '277', '246.5', '213.5', '188.5'];
        var ArrCircle3x = ['100', '131.5', '153', '157', '145', '116', '83', '55', '41', '45', '68'];
        var ArrCircle3y = ['435.5', '444', '469.5', '502.5', '533.5', '550', '550', '533', '502.5', '469.5', '444.5'];
        var ArrCircle4x = ['402', '433.5', '455', '459', '447', '418', '385', '357', '343', '347', '370'];
        var ArrCircle4y = ['435.5', '444', '469.5', '502.5', '533.5', '550', '550', '533', '502.5', '469.5', '444.5'];

        for (var i = 0; i < 11; i++) {
            this.shape_circle1 = new cjs.Shape();
            this.shape_circle1.graphics.f("#ffffff").s("#DB2128").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle1.setTransform(ArrCircle1x[i], ArrCircle1y[i]);
            circleArr.push(this.shape_circle1);

            this.shape_circle2 = new cjs.Shape();
            this.shape_circle2.graphics.f("#ffffff").s("#DB2128").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle2.setTransform(ArrCircle2x[i], ArrCircle2y[i]);
            circleArr.push(this.shape_circle2);

            this.shape_circle3 = new cjs.Shape();
            this.shape_circle3.graphics.f("#ffffff").s("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle3.setTransform(ArrCircle3x[i], ArrCircle3y[i]);
            circleArr.push(this.shape_circle3);

            this.shape_circle4 = new cjs.Shape();
            this.shape_circle4.graphics.f("#ffffff").s("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle4.setTransform(ArrCircle4x[i], ArrCircle4y[i]);
            circleArr.push(this.shape_circle4);


            if (i != 10) {
                var text_circle1 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle1.lineHeight = -1;
                text_circle1.setTransform(parseInt(ArrCircle1x[i]) + 5, parseInt(ArrCircle1y[i]) + 3.5);
                circleArr.push(text_circle1);
            } else {
                var text_circle1 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle1.lineHeight = -1;
                text_circle1.setTransform(parseInt(ArrCircle1x[i]) + 0, parseInt(ArrCircle1y[i]) + 3);
                circleArr.push(text_circle1);
            }

            if (i != 10) {
                var text_circle2 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle2.lineHeight = -1;
                text_circle2.setTransform(parseInt(ArrCircle2x[i]) + 5, parseInt(ArrCircle2y[i]) + 3.5);
                circleArr.push(text_circle2);
            } else {
                var text_circle2 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle2.lineHeight = -1;
                text_circle2.setTransform(parseInt(ArrCircle2x[i]) + 0, parseInt(ArrCircle2y[i]) + 3);
                circleArr.push(text_circle2);
            }

            if (i != 10) {
                var text_circle3 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle3.lineHeight = -1;
                text_circle3.setTransform(parseInt(ArrCircle3x[i]) + 5, parseInt(ArrCircle3y[i]) + 3.5);
                circleArr.push(text_circle3);
            } else {
                var text_circle3 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle3.lineHeight = -1;
                text_circle3.setTransform(parseInt(ArrCircle3x[i]) + 0, parseInt(ArrCircle3y[i]) + 3);
                circleArr.push(text_circle3);
            }

            if (i != 10) {
                var text_circle4 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle4.lineHeight = -1;
                text_circle4.setTransform(parseInt(ArrCircle4x[i]) + 5, parseInt(ArrCircle4y[i]) + 3.5);
                circleArr.push(text_circle4);
            } else {
                var text_circle4 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle4.lineHeight = -1;
                text_circle4.setTransform(parseInt(ArrCircle4x[i]) + 0, parseInt(ArrCircle4y[i]) + 3);
                circleArr.push(text_circle4);
            }

        }

        // innercircle
        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 41, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(99.3, 238);
        circleArr.push(this.shape_groupl);

        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 41, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(401.3, 238);
        circleArr.push(this.shape_groupl);

        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#F26529").ss(0.8, 0, 0, 4).arc(10, 10, 40.5, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(99.3, 494);
        circleArr.push(this.shape_groupl);

        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#F26529").ss(0.8, 0, 0, 4).arc(10, 10, 40.5, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(401.3, 494);
        circleArr.push(this.shape_groupl);

        this.textCir1 = new cjs.Text("Spelare 1", "16px 'Myriad Pro'", "#ffffff");
        this.textCir1.lineHeight = 24;
        this.textCir1.setTransform(80, 239);
        circleArr.push(this.textCir1);

        this.textCir2 = new cjs.Text("Spelare 2", "16px 'Myriad Pro'", "#ffffff");
        this.textCir2.lineHeight = 24;
        this.textCir2.setTransform(380, 239);
        circleArr.push(this.textCir2);

        this.textCir3 = new cjs.Text("Spelare 1", "16px 'Myriad Pro'", "#ffffff");
        this.textCir3.lineHeight = 24;
        this.textCir3.setTransform(80, 495);
        circleArr.push(this.textCir3);

        this.textCir4 = new cjs.Text("Spelare 2", "16px 'Myriad Pro'", "#ffffff");
        this.textCir4.lineHeight = 24;
        this.textCir4.setTransform(380, 495);
        circleArr.push(this.textCir4);

        this.addChild(this.roundRect1, this.instance_2);
        this.addChild(this.text_7, this.text_8);

        for (var rect = 0; rect < circleArr.length; rect++) {
            this.addChild(circleArr[rect]);
        }

        this.addChild(this.roundRect2, this.instance, this.text_9);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 590, 530);


    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage0();
        stage1_1.setTransform(0, 10, 2.1, 2.1);

        var stage1_2 = new lib.GameBoard();
        stage1_2.setTransform(680, -280, 1.4, 1.4);

        this.addChild(stage1_2, stage1_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
