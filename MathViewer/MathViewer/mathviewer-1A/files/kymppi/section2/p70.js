(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p70_1.png",
            id: "p70_1"
        }, {
            src: "images/p70_2.png",
            id: "p70_2"
        }]
    };

    (lib.p70_1 = function() {
        this.initialize(img.p70_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p70_2 = function() {
        this.initialize(img.p70_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("70", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(39, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#87C140").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.5, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol6 = function() {
        this.initialize();   
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(2, 45, 526, 533, 13);
        this.roundRect1.setTransform(0, 0);    

        this.instance_2 = new lib.p70_2();
        this.instance_2.setTransform(0, 47, 0.55, 0.55);

        this.text = new cjs.Text("Kycklingspelet", "18px 'MyriadPro-Semibold'", "#87C140");
        this.text.lineHeight = 20;
        this.text.setTransform(2, 0);
        this.text_1 = new cjs.Text("Spel för 2 eller fler.", "16px 'Myriad Pro'", "#87C140");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(1, 22);

        this.instance = new lib.p70_1();
        this.instance.setTransform(436, 2, 0.38, 0.38);
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#87C140').drawRoundRect(344, 1, 182, 38, 2);
        this.roundRect2.setTransform(0, 0);
        this.text_7 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(353, 12);

        this.label1 = new cjs.Text("Mål", "18px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(131, 70);
        this.label2 = new cjs.Text("Start", "18px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(337, 70);

        this.text_2 = new cjs.Text("• Slå tärningen.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(145, 200);

        this.text_3 = new cjs.Text("• Subtrahera tärningstalet från 6", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(145, 227);

        this.text_4 = new cjs.Text("och gå lika många steg som svaret.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(153, 246);

        this.text_5 = new cjs.Text("• Om du landar på", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(145, 274);

        this.text_6 = new cjs.Text("får du slå", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(296, 274);

        this.text_8 = new cjs.Text("en extra gång.", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(153, 293);

        this.text_9 = new cjs.Text("• Först i mål vinner.", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(145, 321.5);        

        this.addChild(this.roundRect2, this.roundRect1, this.instance_2);
        this.addChild(this.text_1, this.label1, this.label2);
        this.addChild(this.text, this.text_2, this.instance, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8,this.text_9);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 497, 530);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(296, 276, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
