(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p95_1.png",
            id: "p95_1"
        }, {
            src: "images/p95_2.png",
            id: "p95_2"
        }]
    };

    // symbols:
    (lib.p95_1 = function() {
        this.initialize(img.p95_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p95_2 = function() {
        this.initialize(img.p95_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("95", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(556, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Räkna och hitta bokstäverna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p95_2();
        this.instance.setTransform(355, 206, 0.45, 0.45);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 527, 328, 10);
        this.roundRect1.setTransform(0, 25);
        //column-1
        this.label1 = new cjs.Text("5   +   0   =", "16px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(27, 45);
        this.label2 = new cjs.Text("3   +   3   =", "16px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(27, 77);
        this.label3 = new cjs.Text("9   –   3   =", "16px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(27, 107);
        //column-2
        this.label8 = new cjs.Text("8   –   3   =", "16px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(194, 45);
        this.label9 = new cjs.Text("6   –   2   =", "16px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(194, 77);
        this.label10 = new cjs.Text("3   +   4   =", "16px 'Myriad Pro'");
        this.label10.lineHeight = 30;
        this.label10.setTransform(194, 107);
        this.label11 = new cjs.Text("9   –   6   =", "16px 'Myriad Pro'");
        this.label11.lineHeight = 30;
        this.label11.setTransform(194, 137);
        this.label12 = new cjs.Text("5   +   4   =", "16px 'Myriad Pro'");
        this.label12.lineHeight = 30;
        this.label12.setTransform(194, 168);
        this.label13 = new cjs.Text("4   +   2   =", "16px 'Myriad Pro'");
        this.label13.lineHeight = 30;
        this.label13.setTransform(194, 198);
        //column-3
        this.label15 = new cjs.Text("2   +   5   =", "16px 'Myriad Pro'");
        this.label15.lineHeight = 30;
        this.label15.setTransform(368, 45);
        this.label16 = new cjs.Text("7   –   5   =", "16px 'Myriad Pro'");
        this.label16.lineHeight = 30;
        this.label16.setTransform(368, 77);
        this.label17 = new cjs.Text("9   –   8   =", "16px 'Myriad Pro'");
        this.label17.lineHeight = 30;
        this.label17.setTransform(368, 107);
        this.label18 = new cjs.Text("1   +   7   =", "16px 'Myriad Pro'");
        this.label18.lineHeight = 30;
        this.label18.setTransform(368, 137);
        this.label19 = new cjs.Text("9   –   4   =", "16px 'Myriad Pro'");
        this.label19.lineHeight = 30;
        this.label19.setTransform(368, 168);

        this.label4 = new cjs.Text("5", "39px 'UusiTekstausMajema'", "#6E6E70");
        this.label4.lineHeight = 50;
        this.label4.setTransform(99, 29);
        this.label5 = new cjs.Text("E", "25px 'UusiTekstausMajema'", "#6E6E70");
        this.label5.lineHeight = 27;
        this.label5.setTransform(131, 39.5);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (column == 0 && row > 2) {
                    continue;
                } else if (column == 2 && row > 4) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.642;
                } else if (column == 2) {
                    columnSpace = 1.327;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#F1662B").ss(0.9).drawRect(129 + (columnSpace * 258), 41 + (row * 30.5), 20, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 9; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#F1662B").ss(0.9).drawRect(34 + (columnSpace * 28.5), 277, 20, 24);
        }
        this.textbox_group2.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(98, 64.5).lineTo(118, 64.5).moveTo(98, 95.5).lineTo(118, 95.5).moveTo(98, 126).lineTo(118, 126);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(264, 64.5).lineTo(284, 64.5).moveTo(264, 95.5).lineTo(284, 95.5).moveTo(264, 126).lineTo(284, 126)
            .moveTo(264, 156).lineTo(284, 156).moveTo(264, 187).lineTo(284, 187).moveTo(264, 217.5).lineTo(284, 217.5);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(440, 64.5).lineTo(460, 64.5).moveTo(440, 95.5).lineTo(460, 95.5).moveTo(440, 126).lineTo(460, 126)
            .moveTo(440, 156).lineTo(460, 156).moveTo(440, 187).lineTo(460, 187);
        this.hrLine_3.setTransform(0, 0);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(32, 309).lineTo(283, 309);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(285, 309, 1.2, 1.2, 265);

        var arrText = ['Ö', 'P', 'A', 'N', 'E', 'T', 'S', 'K', 'M'];
        var ToBeAdded = [];

        for (var i = 0; i < arrText.length; i++) {
            var columnSpace = i;
            var text = arrText[i];

            if (i == 2) {
                columnSpace = 1.87;
            } else if (i == 3) {
                columnSpace = 2.78;
            } else if (i == 4) {
                columnSpace = 3.75;
            } else if (i == 5) {
                columnSpace = 4.66;
            } else if (i == 6) {
                columnSpace = 5.59;
            } else if (i == 7) {
                columnSpace = 6.49;
            } else if (i == 8) {
                columnSpace = 7.35;
            }

            var temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(38 + (columnSpace * 31), 283);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 9; i++) {
            var columnSpace = i;
            var text = (i + 1).toString();

            if (i == 2) {
                columnSpace = 1.87;
            } else if (i == 3) {
                columnSpace = 2.78;
            } else if (i == 4) {
                columnSpace = 3.75;
            } else if (i == 5) {
                columnSpace = 4.66;
            } else if (i == 6) {
                columnSpace = 5.59;
            } else if (i == 7) {
                columnSpace = 6.49;
            } else if (i == 8) {
                columnSpace = 7.35;
            }

            var temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(38 + (columnSpace * 31), 315);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 9; i++) {
            var columnSpace = i;

            if (i == 2) {
                columnSpace = 1.87;
            } else if (i == 3) {
                columnSpace = 2.78;
            } else if (i == 4) {
                columnSpace = 3.75;
            } else if (i == 5) {
                columnSpace = 4.66;
            } else if (i == 6) {
                columnSpace = 5.59;
            } else if (i == 7) {
                columnSpace = 6.49;
            } else if (i == 8) {
                columnSpace = 7.35;
            }
            var temp_Line = new cjs.Shape();
            temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(43 + (columnSpace *31), 302).lineTo(43+ (columnSpace *31), 309);
            temp_Line.setTransform(0, 0);
            ToBeAdded.push(temp_Line);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4);
        this.addChild(this.instance, this.textbox_group1, this.textbox_group2, this.shape_1)
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label8, this.label9, this.label10, this.label11, this.label12, this.label13);
        this.addChild(this.label15, this.label16, this.label17, this.label18, this.label19);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p95_1();
        this.instance.setTransform(24, 83, 0.27, 0.27);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 527, 194, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(20, 39, 482, 168, 10);
        this.roundRect2.setTransform(0, 0);

        this.text = new cjs.Text("Hur många äpplen är kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.hrRule_1 = new cjs.Shape();
        this.hrRule_1.graphics.beginStroke("#707070").setStrokeStyle(0.8).moveTo(20, 78.5).lineTo(503, 78.5).moveTo(20, 122).lineTo(503, 122).moveTo(20, 164).lineTo(503, 164);
        this.hrRule_1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#707070").setStrokeStyle(0.8).moveTo(161.8, 39).lineTo(161.8, 207).moveTo(274.8, 39).lineTo(274.8, 207).moveTo(386.8, 39).lineTo(386.8, 207);
        this.Line_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("Äpplen från början", "15px 'MyriadPro-Semibold'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(27, 51);

        this.text_3 = new cjs.Text("Läggs till", "15px 'MyriadPro-Semibold'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(184, 51);

        this.text_4 = new cjs.Text("Äts upp", "15px 'MyriadPro-Semibold'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(302, 51);

        this.text_5 = new cjs.Text("Är kvar till slut", "15px 'MyriadPro-Semibold'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(395, 51);

        var arrVal = ['3', '5', '5', '6', '6', '7'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            var yPos = 94;
            if (index == 1) {
                colSpace = 0.65;
                yPos = 94;
            } else if (index == 2) {
                colSpace = 0;
                yPos = 137;
            } else if (index == 3) {
                colSpace = 0.65;
                yPos = 137;
            } else if (index == 4) {
                colSpace = 0;
                yPos = 178;
            } else if (index == 5) {
                colSpace = 0.65;
                yPos = 178;
            }

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(210 + (colSpace * 173.5), yPos);
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(433, 91 + (row * 42), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.text, this.text_1, this.hrRule_1, this.instance);
        this.addChild(this.Line_1, this.text_2, this.text_3, this.text_4, this.text_5, this.textbox_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(299, 455.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(299, 93.7, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
