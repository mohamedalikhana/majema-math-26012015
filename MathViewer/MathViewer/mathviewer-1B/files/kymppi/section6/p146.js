(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p146_1.png",
            id: "p146_1"
        }, {
            src: "images/p146_2.png",
            id: "p146_2"

        }]
    };

    (lib.p146_1 = function() {
        this.initialize(img.p146_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p146_2 = function() {
        this.initialize(img.p146_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("kunna tolka enkla koder", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 658);

        this.text_1 = new cjs.Text("146", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(35, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#1A863A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.text_2 = new cjs.Text("Mot programmering 1", "24px 'MyriadPro-Semibold'", "#1A863A");
        this.text_2.setTransform(95.5, 42);

        this.text_3 = new cjs.Text("51", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(46, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#1A863A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.instance = new lib.p146_1();
        this.instance.setTransform(33, 24, 0.47, 0.467);

        this.addChild(this.instance, this.shape_2, this.text_3, this.text_2, this.shape_1, this.text_1, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.dotMatrixRect = function() {
        this.initialize();

        var width = 354,
            height = 289;

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#000000").ss(0.7).drawRect(0, 0, width, height);
        this.rect1.setTransform(0, 0);

        var ToBeAdded = [];
        var colSpace = 21.1;
        var rowSpace = 21.1;
        var startX = 8;
        var startY = 8;
        var dotCount = 0;
        for (var col = 0; col < 17; col++) {

            for (var row = 0; row < 14; row++) {

                var dot = null;
                dot = new cjs.Shape();
                dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 2.25);
                dot.setTransform(startX + (col * colSpace), startY + (row * rowSpace));
                dot.id = dotCount;
                dotCount = dotCount + 1;

                ToBeAdded.push(dot);
            }
        }

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);

        }
        this.dots = ToBeAdded;
        this.addChild(this.rect1, this.hrLine_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();
        // Block-1         

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#1A863A");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Rita robotens väg. Pilen visar riktningen och talet antalet steg.", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(15, 0);

        this.text_2 = new cjs.Text("Färglägg cirkeln du kommer till.", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(18, 20);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s("#959C9D").ss(1).drawRoundRect(0, 0, 512, 370, 10);
        this.roundRect1.setTransform(0, 30);

        this.instance = new lib.p146_2();
        this.instance.setTransform(11, 38.5, 0.47, 0.467);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(74, 93);

        var dots1 = this.rectimg.dots;
        this.dot = new cjs.Shape();
        this.dot.graphics.f('#D51317').s('#D51317').ss(1).drawCircle(0, 0, 2.25);
        this.dot.setTransform(dots1[0].x, dots1[0].y);

        this.dot2 = new cjs.Shape();
        this.dot2.graphics.f('#D51317').s('#D51317').ss(1).drawCircle(0, 0, 2.25);
        this.dot2.setTransform(dots1[0].x, dots1[7].y);

        this.dot3 = new cjs.Shape();
        this.dot3.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot3.setTransform(dots1[17].x, dots1[17].y);

        this.dot4 = new cjs.Shape();
        this.dot4.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot4.setTransform(dots1[26].x, dots1[26].y);

        this.dot5 = new cjs.Shape();
        this.dot5.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot5.setTransform(dots1[101].x, dots1[101].y);

        this.dot6 = new cjs.Shape();
        this.dot6.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot6.setTransform(dots1[105].x, dots1[105].y);

        this.dot7 = new cjs.Shape();
        this.dot7.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot7.setTransform(dots1[110].x, dots1[110].y);

        this.dot8 = new cjs.Shape();
        this.dot8.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot8.setTransform(dots1[128].x, dots1[128].y);

        this.dot9 = new cjs.Shape();
        this.dot9.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot9.setTransform(dots1[134].x, dots1[134].y);

        this.dot10 = new cjs.Shape();
        this.dot10.graphics.f('').s('#878787').ss(1).drawCircle(0, 0, 9);
        this.dot10.setTransform(dots1[192].x, dots1[192].y);

        this.dot11 = new cjs.Shape();
        this.dot11.graphics.f('#D51317').s('#D51317').ss(1).drawCircle(0, 0, 2.25);
        this.dot11.setTransform(dots1[225].x, dots1[225].y);

        this.dot12 = new cjs.Shape();
        this.dot12.graphics.f('#D51317').s('#D51317').ss(1).drawCircle(0, 0, 2.25);
        this.dot12.setTransform(dots1[232].x, dots1[232].y);


        this.rectimg.addChild(this.dot, this.dot2, this.dot3, this.dot4, this.dot5, this.dot6, this.dot7, this.dot8, this.dot9,
            this.dot10, this.dot11, this.dot12)


        var RectBox_group = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s("#959C9D").ss(1).drawRect(0, 0, 37, 91);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(37, 0);
        this.hrLine_1.setTransform(0, 22.7);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(37, 0);
        this.hrLine_2.setTransform(0, 45.4);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(37, 0);
        this.hrLine_3.setTransform(0, 68.1);

        RectBox_group.addChild(this.Rect1, this.hrLine_1, this.hrLine_2, this.hrLine_3);
        RectBox_group.setTransform(15, 88);

        this.RectBox_group2 = RectBox_group.clone(true);
        RectBox_group.setTransform(15, 248);

        this.RectBox_group3 = RectBox_group.clone(true);
        RectBox_group.setTransform(450, 109);

        this.RectBox_group4 = RectBox_group.clone(true);
        RectBox_group.setTransform(450, 268);

        var Arrow_group = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#1A863A').s("#1A863A").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(15, 0);
        this.arrow.setTransform(17, 0.5, 0.25, 0.25);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#1A863A").s("#1A863A").ss(1).moveTo(0, 0).lineTo(27.5, 0);
        this.line.setTransform(0, 0);

        Arrow_group.addChild(this.line, this.arrow);
        Arrow_group.setTransform(52, 101)

        this.Arrow_group2 = Arrow_group.clone(true);
        this.Arrow_group2.setTransform(451, 122);
        this.Arrow_group2.rotation = 180;

        this.Arrow_group3 = Arrow_group.clone(true);
        this.Arrow_group3.setTransform(52, 249);

        this.Arrow_group4 = Arrow_group.clone(true);
        this.Arrow_group4.setTransform(451, 269.5);
        this.Arrow_group4.rotation = 180;

        var TxtArr = [];
        var TxtlineX = [38, 38, 38, 38, 471, 471, 471, 471, 38, 38, 38, 38, 471, 471, 471, 470];
        var TxtlineY = [105, 127, 149, 171, 126, 148, 170, 194, 265, 288, 310, 333, 284, 308, 330, 353];
        var TxtArrNos = [4, 5, 3, 2, 3, 4, 4, 3, 3, 4, 2, 3, 2, 4, 1, 2];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text('' + TxtArrNos[i], "16px 'Myriad Pro'", "9D9C9C");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(this.temp_label);
        }

        var blackArrow_group = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(7, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(13, 0);
        this.line.setTransform(0, 0);

        blackArrow_group.addChild(this.line, this.arrow);
        blackArrow_group.setTransform(19, 100);

        this.blackArrow_group2 = blackArrow_group.clone(true);
        this.blackArrow_group2.setTransform(29, 115);
        this.blackArrow_group2.rotation = 90;

        this.blackArrow_group3 = blackArrow_group.clone(true);
        this.blackArrow_group3.setTransform(19, 145);

        this.blackArrow_group4 = blackArrow_group.clone(true);
        this.blackArrow_group4.setTransform(29, 175);
        this.blackArrow_group4.rotation = 270;

        this.blackArrow_group5 = blackArrow_group.clone(true);
        this.blackArrow_group5.setTransform(469, 122);
        this.blackArrow_group5.rotation = 180;

        this.blackArrow_group6 = blackArrow_group.clone(true);
        this.blackArrow_group6.setTransform(463, 136);
        this.blackArrow_group6.rotation = 90;

        this.blackArrow_group7 = blackArrow_group.clone(true);
        this.blackArrow_group7.setTransform(469, 165);
        this.blackArrow_group7.rotation = 180;

        this.blackArrow_group8 = blackArrow_group.clone(true);
        this.blackArrow_group8.setTransform(463, 182);
        this.blackArrow_group8.rotation = 90;

        this.blackArrow_group9 = blackArrow_group.clone(true);
        this.blackArrow_group9.setTransform(29, 252);
        this.blackArrow_group9.rotation = 90;

        this.blackArrow_group10 = blackArrow_group.clone(true);
        this.blackArrow_group10.setTransform(19, 282);

        this.blackArrow_group11 = blackArrow_group.clone(true);
        this.blackArrow_group11.setTransform(29, 297);
        this.blackArrow_group11.rotation = 90;

        this.blackArrow_group12 = blackArrow_group.clone(true);
        this.blackArrow_group12.setTransform(19, 327);

        this.blackArrow_group13 = blackArrow_group.clone(true);
        this.blackArrow_group13.setTransform(469, 279);
        this.blackArrow_group13.rotation = 180;

        this.blackArrow_group14 = blackArrow_group.clone(true);
        this.blackArrow_group14.setTransform(463, 295);
        this.blackArrow_group14.rotation = 90;

        this.blackArrow_group15 = blackArrow_group.clone(true);
        this.blackArrow_group15.setTransform(469, 325);
        this.blackArrow_group15.rotation = 180;

        this.blackArrow_group16 = blackArrow_group.clone(true);
        this.blackArrow_group16.setTransform(463, 354);
        this.blackArrow_group16.rotation = 270;

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.instance, this.rectimg, RectBox_group,
            this.RectBox_group2, this.RectBox_group3, this.RectBox_group4, Arrow_group, this.Arrow_group2, this.Arrow_group3, this.Arrow_group4,
            blackArrow_group, this.blackArrow_group2, this.blackArrow_group3, this.blackArrow_group4, this.blackArrow_group5, this.blackArrow_group6,
            this.blackArrow_group7, this.blackArrow_group8, this.blackArrow_group9, this.blackArrow_group10, this.blackArrow_group11,
            this.blackArrow_group12, this.blackArrow_group13, this.blackArrow_group14, this.blackArrow_group15, this.blackArrow_group16);
        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 550.3, 395);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 287, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
