(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p29_1.png",
            id: "p29_1"
        }, {
            src: "images/p29_2.png",
            id: "p29_2"
        }]
    };

    (lib.p29_1 = function() {
        this.initialize(img.p29_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p29_2 = function() {
        this.initialize(img.p29_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol11 = function() {
        this.initialize();

       
        this.text_1 = new cjs.Text("kunna lösa uppgifter i addition 0 till 20, med tiotalsövergång", "9.5px 'Myriad Pro'");
        this.text_1.setTransform(295, 659);

        this.text_2 = new cjs.Text("29", "13px 'Myriad Pro'", "#FFFFFF");       
        this.text_2.setTransform(555, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Addition – först upp till 10", "24px 'MyriadPro-Semibold'", "#F1662B");        
        this.text_3.setTransform(116, 42);

        this.instance = new lib.p29_1();
        this.instance.setTransform(35, 58, 0.531, 0.5);

        this.text_4 = new cjs.Text("9", "28px 'MyriadPro-Semibold'", "#FFFFFF");     
        this.text_4.setTransform(74, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52, 23.5, 1.2, 1);

        this.addChild(this.instance, this.shape_2, this.text_1, this.text_4, this.text_3, this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 20, 520, 144, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p29_1();
        this.instance.setTransform(503, 37, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Hur mycket saknas till 10?", "16px 'Myriad Pro'");
        this.text_q1.setTransform(20, 6);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(0, 6);
        var tmptextArr = ['9', '7', '8', '4', '6', '5', '3', '2', '1', '3', '6', '5'];
        var ToBeAdded = [];
        var i = 0;
        this.hrLine_1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (i < tmptextArr.length) {
                    var tempText = new cjs.Text(tmptextArr[i] + "  +           =  10", "16px 'Myriad Pro'");
                    tempText.setTransform(38 + (160 * colSpace), 56 + (28 * rowSpace));
                    this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(68 + (160 * colSpace), 60 + (28 * rowSpace)).lineTo(97 + (160 * colSpace), 60 + (28 * rowSpace));
                    this.hrLine_1.setTransform(0, 0);
                    ToBeAdded.push(tempText, this.hrLine_1);
                    i++;

                }

            }
        }
        this.hrLine_2 = new cjs.Shape();
        var j = 0;
        var tmptextArr1 = ['1', '3', '6', '5'];
        var tmptextArr2 = ['2', '4', '1', '2'];
        for (var row = 0; row < 4; row++) {
            var rowSpace = row;
            if (j < tmptextArr1.length) {
                var tempText1 = new cjs.Text(tmptextArr1[j] + "  +  " + tmptextArr2[j] + "  +          =  10", "16px 'Myriad Pro'");
                tempText1.setTransform(198 + (160 * colSpace), 56 + (28 * rowSpace));

                this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(110 + (160 * 2.1), 60 + (28 * rowSpace)).lineTo(100 + (160    * 2), 60 + (28 * rowSpace));
                    this.hrLine_2.setTransform(0, 0);
            }
            ToBeAdded.push(tempText1,this.hrLine_2);
            j++;
        };
        //   var ToBeAdded = [];



        this.addChild(this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.label1);
        this.addChild(this.Line_1, this.Line_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol4 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 20, 520, 152, 10);
        this.roundRect1.setTransform(0, 0);

        this.text_q1 = new cjs.Text("Skriv tiokamraten.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(20, 6);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(0, 6);

        this.instance = new lib.p29_2();
        this.instance.setTransform(13, 25, 0.475, 0.5);

        var tmpHartArr=['9','5','10','3','8','4','6','2','7','1'];
        var ToBeAdded = [];
        var i=0;
        this.hrLine_3 = new cjs.Shape();
         for (var column = 0; column < 5; column++) {
            var colSpace = column;
            for (var row = 0; row < 2; row++) {
                var rowSpace = row;

                if (i < tmpHartArr.length) {
                    var tempTextHart = new cjs.Text(tmpHartArr[i] + "  + ", "16px 'Myriad Pro'");
                    tempTextHart.setTransform(33 + (100 * colSpace), 62 + (73 * rowSpace));
                     if(colSpace==1){
                    this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(62 + (100 * colSpace), 70 + (70 * rowSpace)).lineTo(82 + (100 * colSpace), 70 + (70 * rowSpace));
                }
                else
                {
                    this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(56 + (100 * colSpace), 70 + (71 * rowSpace)).lineTo(77 + (100 * colSpace), 70 + (71 * rowSpace));
                }
                    this.hrLine_3.setTransform(0, 0);

                    ToBeAdded.push(tempTextHart, this.hrLine_3);
                    i++;

                }

            }
        }
        this.addChild(this.roundRect1, this.text_q1, this.text_q2, this.instance);
        this.addChild(this.text_2, this.Line_1);
         for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol3();
        this.v1.setTransform(314, 334, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol4();
        this.v2.setTransform(314, 514, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
