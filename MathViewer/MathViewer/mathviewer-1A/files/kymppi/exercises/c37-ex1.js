var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section3/images/p109_1.png",
            id: "p109_1"
        }, {
            src: "../section3/images/p109_2.png",
            id: "p109_2"
        }]
    };

    (lib.p109_1 = function() {
        this.initialize(img.p109_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p109_2 = function() {
        this.initialize(img.p109_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.gameBorard = new lib.gameBorard();
        this.gameBorard.setTransform(710, 40 - 90, 1.3, 1.3);

        this.gameBorard2 = new lib.gameBorard();
        this.gameBorard2.setTransform(710, 260 - 20, 1.3, 1.3)
        this.gameBorard2.text_1.visible = false;
        this.gameBorard2.text_1a.visible = true;

        this.text_h1 = new cjs.Text("Hjärtspelet", "bold 40px 'MyriadPro-Semibold'", "#F1662B");
        this.text_h1.lineHeight = 20;
        this.text_h1.setTransform(0, 0);

        this.text_h2 = new cjs.Text("Spel för 2.", "35px 'Myriad Pro'", "#F1662B");
        this.text_h2.lineHeight = 20;
        this.text_h2.setTransform(0, 45);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#F1652B').drawRoundRect(1024 + 155, -10-90, 197, 38, 2);
        this.roundRect1.setTransform(0, 0);

        this.label1 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(1038 + 155, 3-90);

        this.img_dice = new lib.p109_1();
        this.img_dice.setTransform(1128 + 155, -6-90, 0.375, 0.375);

        this.instructions = new lib.instructions();
        this.instructions.text_1.visible = true;
        this.instructions.text_2.visible = true;
        this.instructions.text_3.visible = true;
        this.instructions.text_4.visible = true;

        this.addChild(this.gameBorard, this.gameBorard2, this.text_h1, this.text_h2, this.roundRect1, this.label1, this.img_dice, this.instructions);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Testa dina kunskaper", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("37", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.gameBorard = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(5, 0, 510, 208, 10);
        this.roundRect1.setTransform(0, 0);

        this.text_1 = new cjs.Text("Spelare 1", "bold 16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(16, 12);

        this.text_1a = new cjs.Text("Spelare 2", "bold 16px 'Myriad Pro'");
        this.text_1a.lineHeight = 19;
        this.text_1a.visible = false;
        this.text_1a.setTransform(16, 12);

        this.instance_2 = new lib.p109_2();
        this.instance_2.setTransform(14, 44, 0.62, 0.61);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 4) {
                    columnSpace = 4.07;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(61 + (columnSpace * 100), 58 + (row * 82.5), 18, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("9  +", "16.3px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(29, 62);
        this.label2 = new cjs.Text("8  +", "16.3px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(129, 62);
        this.label3 = new cjs.Text("7  +", "16.3px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(229, 62);
        this.label4 = new cjs.Text("6  +", "16.3px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(329, 62);
        this.label5 = new cjs.Text("5  +", "16.3px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(436, 62);

        this.label6 = new cjs.Text("5  +", "16.3px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(29, 145);
        this.label7 = new cjs.Text("4  +", "16.3px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(129, 145);
        this.label8 = new cjs.Text("3  +", "16.3px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(229, 145);
        this.label9 = new cjs.Text("2  +", "16.3px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(329, 145);
        this.label10 = new cjs.Text("1  +", "16.3px 'Myriad Pro'");
        this.label10.lineHeight = 30;
        this.label10.setTransform(436, 145);

        this.addChild(this.roundRect1, this.instance_2, this.text_1, this.text_1a, this.textbox_group1, this.label1, this.label2,
            this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    (lib.instructions = function() {
        this.initialize();

        this.text_1 = new cjs.Text("1.  Slå 2 tärningar.", "32px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 130);

        this.text_2 = new cjs.Text("2.  Talen i varje hjärta ska vara 10 tillsammans.", "32px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(0, 180);

        this.text_3 = new cjs.Text("3.  Välj det ena tärningstalet eller summan av båda", "32px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(0, 230);

        this.text_4 = new cjs.Text("och skriv talet i hjärtat.", "32px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(35, 275);

        this.text_5 = new cjs.Text("4.  Den som först har skrivit tal i alla hjärtan vinner.", "32px 'Myriad Pro'");
        this.text_5.visible = true;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(0, 320);

        this.addChild(this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
