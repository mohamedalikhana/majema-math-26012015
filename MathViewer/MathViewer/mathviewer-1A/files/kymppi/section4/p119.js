(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p119_1.png",
            id: "p119_1"
        },{
            src: "images/p119_2.png",
            id: "p119_2"
        }]
    };

    (lib.p119_1 = function() {
        this.initialize(img.p119_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p119_2 = function() {
        this.initialize(img.p119_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("119", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(549, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(576, 660.8);

        this.instance = new lib.p119_1();
        this.instance.setTransform(-3, -2, 0.537, 0.545);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(465 + (columnSpace * 33), 31, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Hur många kronor är kvar?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 254, 123, 10);
        this.roundRect1.setTransform(0, 10);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(258, 10);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 141);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(258, 141);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 272);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(258, 272);

        this.instance = new lib.p119_2();
        this.instance.setTransform(5, 28, 0.47, 0.47);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['108','239','370'];
        var xPos = 6,
            padding,
            yPos,
            rectWidth = 240,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 12,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 0;
                if (i == 1) {
                    padding = padding + 9;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("9", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(6, 98);
        this.text_2 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(47.2, 98);
        this.text_3 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(66, 98);
        this.text_8 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 45;
        this.text_8.setTransform(107.2, 98);
        this.text_9 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 45;
        this.text_9.setTransform(126, 98);
        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(167, 98);
        this.text_5 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 45;
        this.text_5.setTransform(85, 103);
        this.text_6 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 45;
        this.text_6.setTransform(25, 103);
        this.text_7 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 45;
        this.text_7.setTransform(145, 103);


        // // row-1
        this.text_10 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_10.lineHeight = 15;
        this.text_10.setTransform(109, 42);
        this.text_10.skewX=19;
        this.text_10.skewY=19;
        this.text_11 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_11.lineHeight = 15;
        this.text_11.setTransform(220, 61);
        this.text_11.skewX=23;
        this.text_11.skewY=23;
        this.text_12 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_12.lineHeight = 15;
        this.text_12.setTransform(351, 77);
        this.text_12.skewX=-31;
        this.text_12.skewY=-31;
        this.text_13 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_13.lineHeight = 15;
        this.text_13.setTransform(476, 49.5);
        this.text_13.skewX=-24;
        this.text_13.skewY=-24;
        // // row-2
        this.text_14 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_14.lineHeight = 15;
        this.text_14.setTransform(114, 167);
        this.text_14.skewX=9;
        this.text_14.skewY=9;
        this.text_15 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_15.lineHeight = 15;
        this.text_15.setTransform(213, 184.5);
        this.text_15.skewX=-30;
        this.text_15.skewY=-30;
        this.text_16 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_16.lineHeight = 15;
        this.text_16.setTransform(364, 164);
        this.text_16.skewX=23;
        this.text_16.skewY=23;
        this.text_17 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_17.lineHeight = 15;
        this.text_17.setTransform(480, 191.5);
        this.text_17.skewX=24;
        this.text_17.skewY=24;
        // // row-3
        this.text_18 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_18.lineHeight = 15;
        this.text_18.setTransform(143, 312);
        this.text_19 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_19.lineHeight = 15;
        this.text_19.setTransform(161, 344.5);
        this.text_19.skewX=-41;
        this.text_19.skewY=-41;
        this.text_20 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_20.lineHeight = 15;
        this.text_20.setTransform(360, 323);
        this.text_21 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_21.lineHeight = 15;
        this.text_21.setTransform(415, 348.5);
        this.text_21.skewX=-41;
        this.text_21.skewY=-41;
        
        this.addChild(this.text_q1, this.text_q2);
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);   
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14);
        this.addChild(this.text_15, this.text_16, this.text_17, this.text_18, this.text_19, this.text_20, this.text_21);     
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(311, 445, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
