(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

            src: "images/p128_1.png",
            id: "p128_1"
        }]
    };

    (lib.p128_1 = function() {
        this.initialize(img.p128_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1

        this.text = new cjs.Text("128", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(33, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 10);

        this.instance = new lib.p128_1();
        this.instance.setTransform(107, 28, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
        this.numberLine1 = new lib.NumberLineType1();
        this.addChild(this.numberLine1)


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.TextBox = function(number, color) {
        this.initialize();
        i = number/5;
        if (i % 10 == 0) {
            var text = new cjs.Text(number + "", " bold 16px 'Myriad Pro'", color);
        } else {
            var text = new cjs.Text(number + "", "16px 'Myriad Pro'", color);
        }
        text.textAlign = 'center';

        this.addChild(text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.NumberLineType1 = function(width, count) {
        this.initialize();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(15, 0);
        this.arrow.setTransform(512, 191.5, 0.3, 0.3);
        this.arrow.rotation = -50;
        var positionArray = [76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 78, 0, 78, 0, 78, 0, 78, 0, 80, 0]
        for (var i = 0; i < 21; i++) {
            var height = 7;
            lineY = 184
            var colspace = 21.2;
            var icount = i;

            if (icount % 2 == 0) {
                height = 10;
                lineY = 181;
            }

            var vline = new cjs.Shape();

            if (icount % 10 == 0) {
                vline.graphics.f("#0CA3CB").s('#0CA3CB').ss(0.5).drawRect(0, 0, 0.2, height);
            } else {
                vline.graphics.f("#000000").s('#000000').ss(0.5).drawRect(0, 0, 0.1, height);
            }
            vline.setTransform(76 + i * colspace, lineY);

            this.addChild(vline);

            var text;
            var ispace = 21;
            var number = i * 5;

            if (i % 10 == 0) {
                text = new lib.TextBox(number, "#0CA3CB");
                text.setTransform(positionArray[i] + i * ispace, 208);
                this.addChild(text);
            } else if (number % 10 == 0) {
                text = new lib.TextBox(number, "#000000");
                text.setTransform(positionArray[i] + i * ispace, 208);
                this.addChild(text);
            }
        };

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(0.7).moveTo(0, 0).lineTo(455, 0);
        this.linescale.setTransform(61, 191);

        this.addChild(this.linescale, this.arrow);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 525.3, 170);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 378, 10);
        this.roundRect1.setTransform(0, 15);

        // Draw horizantial  lines

        var ToBeAdded = [];
        var moveToxposition = 141;
        for (var row = 0; row < 2; row++) {
            if (row == 1) {
                moveToxposition = 146;
            } else {
                moveToxposition = 141;
            }

            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(5, moveToxposition + (row * 122)).lineTo(507, moveToxposition + (row * 122));
            ToBeAdded.push(hrLine)
        };

        var lineArr = [];

        var lineX = [73, 254, 431, 73, 254, 431, 73, 254, 431, 73, 254, 431,
            73, 254, 431, 73, 254, 431, 73, 254, 431, 73, 254, 431,
            73, 254, 431, 73, 254, 431, 73, 254, 431, 73, 254, 431
        ];

        var lineY = [5, 5, 5, 35, 35, 35, 63, 63, 63, 90, 90, 90,
            128, 128, 128, 158, 158, 158, 186, 186, 186, 213, 213, 213,
            256, 256, 256, 286, 286, 286, 314, 314, 314, 341, 341, 341
        ];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(42, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }
        // block-1

        this.label1 = new cjs.Text("2  +  3  =", "16px 'Myriad Pro'");
        this.label1.setTransform(22, 40);
        this.label2 = new cjs.Text("4  +  4  =", "16px 'Myriad Pro'");
        this.label2.setTransform(22, 68);
        this.label3 = new cjs.Text("6  +  3  =", "16px 'Myriad Pro'");
        this.label3.setTransform(22, 96);
        this.label4 = new cjs.Text("6  +  4  =", "16px 'Myriad Pro'");
        this.label4.setTransform(22, 124);

        this.label6 = new cjs.Text("20  +  30  =", "16px 'Myriad Pro'");
        this.label6.setTransform(187, 40);
        this.label7 = new cjs.Text("40  +  40  =", "16px 'Myriad Pro'");
        this.label7.setTransform(187, 68);
        this.label8 = new cjs.Text("60  +  30  =", "16px 'Myriad Pro'");
        this.label8.setTransform(187, 96);
        this.label9 = new cjs.Text("60  +  40  =", "16px 'Myriad Pro'");
        this.label9.setTransform(187, 124);

        this.label11 = new cjs.Text("50  –  30  =", "16px 'Myriad Pro'");
        this.label11.setTransform(365, 40);
        this.label12 = new cjs.Text("80  –  40  =", "16px 'Myriad Pro'");
        this.label12.setTransform(365, 68);
        this.label13 = new cjs.Text("90  –  30  =", "16px 'Myriad Pro'");
        this.label13.setTransform(365, 96);
        this.label14 = new cjs.Text("100  –  40  =", "16px 'Myriad Pro'");
        this.label14.setTransform(357, 124);

        // block-2

        this.label16 = new cjs.Text("6  +  2  =", "16px 'Myriad Pro'");
        this.label16.setTransform(22, 163);
        this.label17 = new cjs.Text("4  +  3  =", "16px 'Myriad Pro'");
        this.label17.setTransform(22, 191);
        this.label18 = new cjs.Text("5  +  2  =", "16px 'Myriad Pro'");
        this.label18.setTransform(22, 219);
        this.label19 = new cjs.Text("5  +  5  =", "16px 'Myriad Pro'");
        this.label19.setTransform(22, 247);

        this.label20 = new cjs.Text("60  +  20  =", "16px 'Myriad Pro'");
        this.label20.setTransform(187, 163);
        this.label21 = new cjs.Text("40  +  30  =", "16px 'Myriad Pro'");
        this.label21.setTransform(187, 191);
        this.label22 = new cjs.Text("50  +  20  =", "16px 'Myriad Pro'");
        this.label22.setTransform(187, 219);
        this.label23 = new cjs.Text("50  +  50  =", "16px 'Myriad Pro'");
        this.label23.setTransform(187, 247);

        this.label24 = new cjs.Text("80  –  20  =", "16px 'Myriad Pro'");
        this.label24.setTransform(365, 163);
        this.label25 = new cjs.Text("70  –  30  =", "16px 'Myriad Pro'");
        this.label25.setTransform(365, 191);
        this.label26 = new cjs.Text("70  –  20  =", "16px 'Myriad Pro'");
        this.label26.setTransform(365, 219);
        this.label27 = new cjs.Text("100  –  50  =", "16px 'Myriad Pro'");
        this.label27.setTransform(357, 247);

        // block-3

        this.label28 = new cjs.Text("2  +  8  =", "16px 'Myriad Pro'");
        this.label28.setTransform(22, 291);
        this.label29 = new cjs.Text("1  +  9  =", "16px 'Myriad Pro'");
        this.label29.setTransform(22, 319);
        this.label30 = new cjs.Text("3  +  7  =", "16px 'Myriad Pro'");
        this.label30.setTransform(22, 347);
        this.label31 = new cjs.Text("4  +  6  =", "16px 'Myriad Pro'");
        this.label31.setTransform(22, 375);

        this.label32 = new cjs.Text("20  +  80  =", "16px 'Myriad Pro'");
        this.label32.setTransform(187, 291);
        this.label33 = new cjs.Text("10  +  90  =", "16px 'Myriad Pro'");
        this.label33.setTransform(187, 319);
        this.label34 = new cjs.Text("30  +  70  =", "16px 'Myriad Pro'");
        this.label34.setTransform(187, 347);
        this.label35 = new cjs.Text("40  +  60  =", "16px 'Myriad Pro'");
        this.label35.setTransform(187, 375);

        this.label36 = new cjs.Text("100  –  80  =", "16px 'Myriad Pro'");
        this.label36.setTransform(357, 291);
        this.label37 = new cjs.Text("100  –  90  =", "16px 'Myriad Pro'");
        this.label37.setTransform(357, 319);
        this.label38 = new cjs.Text("100  –  70  =", "16px 'Myriad Pro'");
        this.label38.setTransform(357, 347);
        this.label39 = new cjs.Text("100  –  60  =", "16px 'Myriad Pro'");
        this.label39.setTransform(357, 376);

        this.addChild(this.text, this.text_1, this.roundRect1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.label16, this.label17, this.label18, this.label19, this.label20, this.label21, this.label22, this.label23,
            this.label24, this.label25, this.label26, this.label27, this.label28, this.label29, this.label30, this.label31,
            this.label32, this.label33, this.label34, this.label35, this.label36, this.label37, this.label38, this.label39);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 513.3, 390);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(297, 279, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
