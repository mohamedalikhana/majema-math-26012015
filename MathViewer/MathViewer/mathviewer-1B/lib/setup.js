var pageInfo = [
    { title: 'Kymppi', folder: 'files/kymppi', preload: true, pages: [] }
];
var pages = [
    { a: 0, k: '', o: 'Front Page', s: '', t: 'files/toc/toc.html' },

    { a: 0, n: '1', o: 'Talen 0 till 20', color: '#81B53C', childColor: '#EDF4DD', columnColor: '#EDF4DD' },
    { a: 1, n: '1', o: 'Vi repeterar talen 0 <br />till 12', t: 'section1/p6.html' },
    { a: 1, t: 'section1/p7.html' },
    { a: 1, t: 'section1/p8.html' },
    { a: 1, n: '2', o: 'Talen 13 till 15', t: 'section1/p9.html' },
    { a: 1, t: 'section1/p10.html' },
    { a: 1, t: 'section1/p11.html' },
    { a: 1, n: '3', o: 'Talen 16 till 18', t: 'section1/p12.html' },
    { a: 1, t: 'section1/p13.html' },
    { a: 1, t: 'section1/p14.html' },
    { a: 1, n: '4', o: 'Talen 19 och 20', t: 'section1/p15.html' },
    { a: 1, t: 'section1/p16.html' },
    { a: 1, t: 'section1/p17.html' },
    { a: 1, n: '5', o: 'Talsymboler från förr.', t: 'section1/p18.html' },
    { a: 1, t: 'section1/p19.html' },
    { a: 1, n: '6', o: 'Udda och jämna tal', t: 'section1/p20.html' },
    { a: 1, t: 'section1/p21.html' },
    { a: 1, t: 'section1/p22.html' },
    { a: 1, n: '7', o: 'Textuppgifter', t: 'section1/p23.html' },
    { a: 1, t: 'section1/p24.html' },
    { a: 1, t: 'section1/p25.html' },
    { a: 1, n: '8', o: 'Testa dina kunskaper', t: 'section1/p26.html' },
    { a: 1, t: 'section1/p27.html' },
    { a: 1, t: 'section1/p28.html' },

    { a: 1, n: '1', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section1/larandemal.html' },

    { a: 0, n: '2', o: 'Addition med <br>tiotalsövergång', color: '#E75E33', childColor: '#FEE5D4', columnColor: '#FEE5D4' },
    { a: 1, n: '9', o: 'Addition <br />– först upp till 10', t: 'section2/p29.html' },
    { a: 1, t: 'section2/p30.html' },
    { a: 1, t: 'section2/p31.html' },
    { a: 1, n: '10', o: 'Fler och färre', t: 'section2/p32.html' },
    { a: 1, t: 'section2/p33.html' },
    { a: 1, n: '11', o: 'Addera till talen 9 <br />och 8', t: 'section2/p34.html' },
    { a: 1, t: 'section2/p35.html' },
    { a: 1, t: 'section2/p36.html' },
    { a: 1, n: '12', o: 'Vi övar', t: 'section2/p37.html' },
    { a: 1, t: 'section2/p38.html' },
    { a: 1, t: 'section2/p39.html' },
    { a: 1, n: '13', o: 'Addera till talen 7 <br />och 6', t: 'section2/p40.html' },
    { a: 1, t: 'section2/p41.html' },
    { a: 1, t: 'section2/p42.html' },
    { a: 1, n: '14', o: 'Kommutativa lagen<br>i addition', t: 'section2/p43.html' },
    { a: 1, t: 'section2/p44.html' },
    { a: 1, t: 'section2/p45.html' },
    { a: 1, n: '15', o: 'Symmetri', t: 'section2/p46.html' },
    { a: 1, t: 'section2/p47.html' },
    { a: 1, n: '16', o: 'Stapeldiagram', t: 'section2/p48.html' },
    { a: 1, t: 'section2/p49.html' },
    { a: 1, t: 'section2/p50.html' },
    { a: 1, n: '17', o: 'Testa dina kunskaper', t: 'section2/p51.html' },
    { a: 1, t: 'section2/p52.html' },
    { a: 1, t: 'section2/p53.html' },

    { a: 1, n: '2', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section2/larandemal.html' },

    { a: 0, n: '3', o: 'Subtraktion med<br>tiotalsövergång', color: '#918EC1', childColor: '#EBEBF6', columnColor: '#EBEBF6' },
    { a: 1, n: '18', o: 'Subtraktion <br />– först ner till 10', t: 'section3/p54.html' },
    { a: 1, t: 'section3/p55.html' },
    { a: 1, t: 'section3/p56.html' },
    { a: 1, n: '19', o: 'Subtrahera från<br>talen 11 och 12', t: 'section3/p57.html' },
    { a: 1, t: 'section3/p58.html' },
    { a: 1, t: 'section3/p59.html' },
    { a: 1, n: '20', o: 'Talföljder', t: 'section3/p60.html' },
    { a: 1, t: 'section3/p61.html' },
    { a: 1, n: '21', o: 'Subtrahera från<br>talen 13 och 14', t: 'section3/p62.html' },
    { a: 1, t: 'section3/p63.html' },
    { a: 1, t: 'section3/p64.html' },
    { a: 1, n: '22', o: 'Summa och differens', t: 'section3/p65.html' },
    { a: 1, t: 'section3/p66.html' },
    { a: 1, t: 'section3/p67.html' },
    { a: 1, n: '23', o: 'Subtrahera från<br>talen 15 till 18', t: 'section3/p68.html' },
    { a: 1, t: 'section3/p69.html' },
    { a: 1, t: 'section3/p70.html' },
    { a: 1, n: '24', o: 'Vi övar', t: 'section3/p71.html' },
    { a: 1, t: 'section3/p72.html' },
    { a: 1, t: 'section3/p73.html' },
    { a: 1, n: '25', o: 'Problemlösning', t: 'section3/p74.html' },
    { a: 1, t: 'section3/p75.html' },
    { a: 1, n: '26', o: 'Vi övar', t: 'section3/p76.html' },
    { a: 1, t: 'section3/p77.html' },
    { a: 1, t: 'section3/p78.html' },
    { a: 1, n: '27', o: 'Testa dina kunskaper', t: 'section3/p79.html' },
    { a: 1, t: 'section3/p80.html' },
    { a: 1, t: 'section3/p81.html' },

    { a: 1, n: '3', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section3/larandemal.html' },

    { a: 0, n: '4', o: 'Geometri och mätning', color: '#D5476F', childColor: '#F9E0E3', columnColor: '#F9E0E3' },
    { a: 1, n: '28', o: 'Vi repeterar klockan<br>– hel timme', t: 'section4/p82.html' },
    { a: 1, t: 'section4/p83.html' },
    { a: 1, t: 'section4/p84.html' },
    { a: 1, n: '29', o: 'Klockan – halvtimme', t: 'section4/p85.html' },
    { a: 1, t: 'section4/p86.html' },
    { a: 1, t: 'section4/p87.html' },
    { a: 1, n: '30', o: 'Tid', t: 'section4/p88.html' },
    { a: 1, t: 'section4/p89.html' },
    { a: 1, n: '31', o: 'Mäta längd', t: 'section4/p90.html' },
    { a: 1, t: 'section4/p91.html' },
    { a: 1, t: 'section4/p92.html' },
    { a: 1, n: '32', o: 'Mäta längd i centimeter', t: 'section4/p93.html' },
    { a: 1, t: 'section4/p94.html' },
    { a: 1, t: 'section4/p95.html' },
    { a: 1, n: '33', o: 'Vi övar', t: 'section4/p96.html' },
    { a: 1, t: 'section4/p97.html' },
    { a: 1, t: 'section4/p98.html' },
    { a: 1, n: '34', o: 'Trianglar, fyrhörningar<br>och cirklar', t: 'section4/p99.html' },
    { a: 1, t: 'section4/p100.html' },
    { a: 1, t: 'section4/p101.html' },
    { a: 1, n: '35', o: 'Äldre måttenheter', t: 'section4/p102.html' },
    { a: 1, t: 'section4/p103.html' },
    { a: 1, n: '36', o: 'Vi övar', t: 'section4/p104.html' },
    { a: 1, t: 'section4/p105.html' },
    { a: 1, t: 'section4/p106.html' },
    { a: 1, n: '37', o: 'Testa dina kunskaper', t: 'section4/p107.html' },
    { a: 1, t: 'section4/p108.html' },
    { a: 1, t: 'section4/p109.html' },

    { a: 1, n: '4', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section4/larandemal.html' },

    { a: 0, n: '5', o: 'Talen 0 till 100', color: '#00A3C0', childColor: '#DDF0F5', columnColor: '#DDF0F5' },
    { a: 1, n: '38', o: 'Talen 0 till 50', t: 'section5/p110.html' },
    { a: 1, t: 'section5/p111.html' },
    { a: 1, t: 'section5/p112.html' },
    { a: 1, n: '39', o: 'Jämföra talen 0 till 50', t: 'section5/p113.html' },
    { a: 1, t: 'section5/p114.html' },
    { a: 1, t: 'section5/p115.html' },
    { a: 1, n: '40', o: 'Problemlösning', t: 'section5/p116.html' },
    { a: 1, t: 'section5/p117.html' },
    { a: 1, n: '41', o: 'Talen 50 till 100', t: 'section5/p118.html' },
    { a: 1, t: 'section5/p119.html' },
    { a: 1, t: 'section5/p120.html' },
    { a: 1, n: '42', o: 'Jämföra talen 0 till 100', t: 'section5/p121.html' },
    { a: 1, t: 'section5/p122.html' },
    { a: 1, t: 'section5/p123.html' },
    { a: 1, n: '43', o: 'Hundrarutan', t: 'section5/p124.html' },
    { a: 1, t: 'section5/p125.html' },
    { a: 1, t: 'section5/p126.html' },
    { a: 1, n: '44', o: 'Addera och subtrahera<br>med hela tiotal', t: 'section5/p127.html' },
    { a: 1, t: 'section5/p128.html' },
    { a: 1, t: 'section5/p129.html' },
    { a: 1, n: '45', o: 'Talgåtor', t: 'section5/p130.html' },
    { a: 1, t: 'section5/p131.html' },
    { a: 1, n: '46', o: 'Addera ental', t: 'section5/p132.html' },
    { a: 1, t: 'section5/p133.html' },
    { a: 1, t: 'section5/p134.html' },
    { a: 1, n: '47', o: 'Subtrahera ental', t: 'section5/p135.html' },
    { a: 1, t: 'section5/p136.html' },
    { a: 1, t: 'section5/p137.html' },
    { a: 1, n: '48', o: 'Pengar', t: 'section5/p138.html' },
    { a: 1, t: 'section5/p139.html' },
    { a: 1, t: 'section5/p140.html' },
    { a: 1, n: '49', o: 'Testa dina kunskaper', t: 'section5/p141.html' },
    { a: 1, t: 'section5/p142.html' },
    { a: 1, t: 'section5/p143.html' },
    { a: 1, n: '50', o: 'Miniräknaren', t: 'section5/p144.html' },
    { a: 1, t: 'section5/p145.html' },

    { a: 1, n: '5', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section5/larandemal.html' },

    { a: 0, n: '6', o: 'Programmering', color: '#21814B', childColor: '#E0E7D8', columnColor: '#EEE6F4' },
    { a: 1, n: '51', o: 'Mot programmering 1', t: 'section6/p146.html' },
    { a: 1, t: 'section6/p147.html' },
    { a: 1, t: 'section6/p148.html' },
    { a: 1, n: '52', o: 'Mot programmering 2', t: 'section6/p149.html' },
    { a: 1, t: 'section6/p150.html' },
    { a: 1, t: 'section6/p151.html' },
    { a: 1, t: 'samtalsbild/p152.html' },

    { a: 1, n: '6', o: 'Lärandemål', t: 'files/toc/toc.html', u: 'section6/larandemal.html' },

];

var pages2 = [
    { a: 0, k: '', o: '', s: '', t: '' },

    { a: 0, n: '1', o: 'Lukukäsite – luvut 1–4 ja 0' },
    { a: 1, n: '1', o: 'Lukumäärä 1–10', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '2', o: 'Yhtä monta tai yhtä suuri', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '3', o: 'Luvut 1 ja 2', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '4', o: 'Luku 3', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '5', o: 'Luku 4', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '6', o: 'Pienempi ja suurempi kuin', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '7', o: 'Luku 0', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '8', o: 'Pelaa ja testaa taitosi', t: '' },
    { a: 1, t: '' },

    { a: 0, n: '2', o: 'Yhteen- ja vähennyslaskua – luvut 5 ja 6' },
    { a: 1, n: '9', o: 'Luku 5', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '10', o: 'Tutustutaan yhteenlaskuun', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '11', o: 'Tehdään yhteenlasku', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '12', o: 'Harjoitellaan yhteenlaskua', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '13', o: 'Tutustutaan vähennyslaskuun', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '14', o: 'Tehdään vähennyslasku', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '15', o: 'Harjoitellaan vähennyslaskua', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '16', o: 'Harjoitellaan', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '17', o: 'Luku 6', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '18', o: 'Harjoitellaan', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '19', o: 'Puuttuva yhteenlaskettava', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '20', o: 'Pelaa ja testaa taitosi', t: '' },
    { a: 1, t: '' },

    { a: 0, n: '3', o: 'Yhteen- ja vähennyslaskua luvut 0–10 000' },

    { a: 1, n: '21', o: 'Luku 7', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '22', o: 'Yhteenlaskun vaihdannaisuus', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '23', o: 'Laskuperhe', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '24', o: 'Luku 8', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '25', o: 'Pitkiä yhteenlaskuja', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '26', o: 'Pitkiä vähennyslaskuja', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '27', o: 'Luku 9', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '28', o: 'Yhteen- ja vähennyslaskua', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '29', o: 'Luku 10', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '30', o: 'Harjoitellaan', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '31', o: 'Lukujen suuruusjärjestys', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '32', o: 'Kerrataan hajotelmia', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '33', o: 'Pelaa ja testaa taitosi', t: '' },
    { a: 1, t: '' },

    { a: 0, n: '4', o: 'Eurot, luvut 11 ja 12' },

    { a: 1, n: '34', o: 'Eurot', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '35', o: 'Ostosten hinta', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '36', o: 'Rahaa jää', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '37', o: 'Hintaero', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '38', o: 'Puuttuva vähentäjä', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '39', o: 'Harjoitellaan', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '40', o: 'Luku 11', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '41', o: 'Luku 12', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '42', o: 'Pylväskuvio', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '43', o: 'Tasatunnit', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '44', o: 'Pelaa ja testaa taitosi', t: '' },
    { a: 1, t: '' },

    { a: 0, n: '5', o: 'Kerbackground' },
    { a: 1, n: '45', o: 'Kerrataan lukujen järjestys', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '46', o: 'Harjoitellaan', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '47', o: 'Kerrataan rahalaskuja', t: '' },
    { a: 1, t: '' },
    { a: 1, n: '', o: 'Kellopeli', t: '' }
];

var _havaintovalineet = {
    o: 'Visual Aids', t: [
        {
            o: 'Lukukortit ja lukumääräkortit', t: [
               { o: 'Lukukortit', t: '' },
               { o: 'Yhdistä', t: '' },
               { o: 'Järjestä kortit', t: '' },
               { o: 'Suurempi, pienempi', t: '' }]
        },
        {
            o: 'Laskuhelmet', t: [
               { o: 'Laskuhelmet', t: '' },
               { o: 'Yhteenlaskua', t: '' },
               { o: 'Vähennyslaskua', t: '' },
               { o: 'Yhteen- ja vähennyslaskua', t: '' }]
        },
        {
            o: 'Lukusuora', t: [
               { o: '', t: '' }]
        },
        {
            o: 'Toimintapohja', t: [
               { o: 'Toimintapohja', t: '' },
               { o: 'Yhteenlaskua', t: '' },
               { o: 'Vähennyslaskua', t: '' },
               { o: 'Vertailua', t: '' }]
        },
        {
            o: 'Hajotelmakone', t: [
               { o: '', t: '' }]
        },
        {
            o: 'Kello', t: [
               { o: 'Kello', t: '' },
               { o: 'Ilmoita aika', t: '' },
               { o: 'Näytä aika', t: '' }]
        },
        {
            o: 'Eurot', t: [
               { o: 'Eurot', t: '' },
               { o: 'Kokoa rahamäärä', t: '' },
               { o: 'Ilmoita rahamäärä', t: '' },
               { o: 'Maksa ostos', t: '' }]
        },
        {
            o: 'Kymppiparit', t: [
               { o: '', t: '' }]
        },
        {
            o: 'Numeroiden kirjoitusmalli', t: [
               { o: '', t: '' }]
        }
    ]
};

var _handouts = {
    o: 'Monisteet', t: [
        /*{o:'Luku 1', t:[{o:'', t: ''}]},*/
    ]
};
for (a = 1; a <= 47; a++) {
    _handouts.t.push({ o: 'Luku ' + a, t: [{ o: '', t: '' + a }] });
}

var _generalHandouts = {
    o: 'Yleishandouts', t: [
        { o: 'A Lähtöleveltesti', t: [{ o: 'Perusosa', t: '' }] },
        { o: 'B Lukumääräkortit 1-12', t: [{ o: '', t: '' }] },
        { o: 'C Bingopelipohjia', t: [{ o: '', t: '' }] },
        { o: 'D Neliösenttimetriruudukko', t: [{ o: '', t: '' }] },
        { o: 'E Yhteenlaskukortit 1', t: [{ o: '', t: '' }] },
        { o: 'F Yhteenlaskukortit 2', t: [{ o: '', t: '' }] },
        { o: 'G Vähennyslaskukortit 1', t: [{ o: '', t: '' }] },
        { o: 'H Vähennyslaskukortit 2', t: [{ o: '', t: '' }] },
        { o: 'I Kellonaikakortit', t: [{ o: '', t: '' }] },
        { o: 'J Ostoskortit', t: [{ o: '', t: '' }] },
        { o: 'K Rahamääräkortit', t: [{ o: '', t: '' }] },
        { o: 'L Tapahtumakortit', t: [{ o: '', t: '' }] },
        { o: 'M Muistituki yhteenlaskuun', t: [{ o: '', t: '' }] },
        { o: 'N Muistituki vähennyslaskuun', t: [{ o: '', t: '' }] },
        { o: 'Mallinumbert', t: [{ o: '', t: '' }] },
        { o: 'Kymppiparit', t: [{ o: '', t: '' }] }
    ]
};


var toolbarLinksTop = [];
var toolbarLinksBottom = [];
pageInfo[0].pages = Pages(pages);

function Pages(titletable) {
    var a;

    var pages = [];
    searchPages(0, pages, 0);

    for (a = 0; a < pages.length; a++) {
        if (pages[a].length === 0) {
            pages.splice(a, 1);
            a--;
        }
    }


    function searchPages(level, links, index) {
        var a, obj;
        for (a = index; a < titletable.length; a++) {
            obj = titletable[a];

            if (level === 0) {
                pages.push([]);
                links = pages[pages.length - 1];
            }

            if (obj.a === level) {
                if (obj.t != '' && obj.t != 'DEMO' && obj.t != undefined) {
                    links.push(obj.t);
                }
                if (a + 1 < titletable.length) {
                    if (titletable[a + 1].a > obj.a) {
                        a = searchPages(titletable[a + 1].a, links, a + 1);
                    }
                }
            }
            else
                return a - 1;
        }
        return titletable.length;
    }


    return pages;
}