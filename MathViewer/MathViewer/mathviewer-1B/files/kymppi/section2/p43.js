(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p43_1.png",
            id: "p43_1"
        }, {
            src: "images/p43_2.png",
            id: "p43_2"
        }]
    };
    (lib.p43_1 = function() {
        this.initialize(img.p43_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p43_2 = function() {
        this.initialize(img.p43_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("förstå och kunna använda den kommutativa lagen i addition", "9px 'Myriad Pro'");
        this.text_1.setTransform(307, 660);

        this.text_2 = new cjs.Text("43", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(555, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Kommutativa lagen i addition", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.setTransform(95.5, 42);

        this.text_4 = new cjs.Text("14", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(50, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Block-1

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_119.setTransform(205.9, 21.4);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(190.9, 21.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_37.setTransform(176.1, 21.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(160.9, 21.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_39.setTransform(145.3, 21.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(129.9, 21.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(114.2, 21.4);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(98.2, 21.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(82.7, 21.4);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(67.2, 21.4);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(51.6, 21.4);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_63.setTransform(49.2, 27.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_64.setTransform(49.2, 27.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_59.setTransform(65, 27.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_60.setTransform(65, 27.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_51.setTransform(80, 27.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_52.setTransform(80, 27.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_55.setTransform(96, 27.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_56.setTransform(96, 27.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_65.setTransform(112, 27.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_66.setTransform(112, 27.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_61.setTransform(127.5, 27.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_62.setTransform(127.5, 27.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_57.setTransform(143, 27.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_58.setTransform(143, 27.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_49.setTransform(159, 27.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_50.setTransform(159, 27.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_53.setTransform(174, 27.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_54.setTransform(174, 27.9);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_47.setTransform(189, 27.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_48.setTransform(189, 27.9);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_124.setTransform(203.5, 27.9);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_125.setTransform(203.5, 27.9);

        // Block-2  

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_114.setTransform(448.7, 21.4);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_70.setTransform(433.9, 21.4);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_71.setTransform(419.1, 21.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_72.setTransform(403.9, 21.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_73.setTransform(388.3, 21.4);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_74.setTransform(372.9, 21.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_75.setTransform(357.2, 21.4);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(341.2, 21.4);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(325.7, 21.4);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(310.2, 21.4);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(294.6, 21.4);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_80.setTransform(448.2, 27.9);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_81.setTransform(448.2, 27.9);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_82.setTransform(418.5, 27.9);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_83.setTransform(418.5, 27.9);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_84.setTransform(340.5, 27.9);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_85.setTransform(340.5, 27.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_86.setTransform(403.4, 27.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_87.setTransform(403.4, 27.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(325.3, 27.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(325.3, 27.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_90.setTransform(388.7, 27.9);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_91.setTransform(388.7, 27.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(310.6, 27.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(310.6, 27.9);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_94.setTransform(371.2, 27.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_95.setTransform(371.2, 27.9);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(293.2, 27.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(293.2, 27.9);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_98.setTransform(356.5, 27.9);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_99.setTransform(356.5, 27.9);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_111.setTransform(433.5, 27.9);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_112.setTransform(433.5, 27.9);


        // Inner White block
        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(40, 20, 178, 125, 5);
        this.shape_100.setTransform(0, 0);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(283, 20, 178, 125, 5);
        this.shape_118.setTransform(0, 0);

        // Main yellow block
        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFF679").s("#959C9D").ss(1).drawRoundRect(0, 0, 515, 182, 10);
        this.shape_115.setTransform(0, 0);

        this.instance = new lib.p43_1();
        this.instance.setTransform(42, 35, 0.46, 0.46);

        this.redRountrect = new cjs.Shape();
        this.redRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 30, 80, 66, 6);
        this.redRountrect.setTransform(97, 15);

        this.redRountrect1 = this.redRountrect.clone(true);
        this.redRountrect1.setTransform(267.5, 15);

        this.yellowRountrect = new cjs.Shape();
        this.yellowRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 32, 57, 43, 6);
        this.yellowRountrect.setTransform(22, 15);

        this.yellowRountrect1 = this.yellowRountrect.clone(true);
        this.yellowRountrect1.setTransform(358.5, 15);

        this.text_1 = new cjs.Text("Att byta ordning på talen ändrar inte svaret.", "16px 'Myriad Pro'");
        this.text_1.setTransform(35, 168);

        this.text_2 = new cjs.Text("4 + 9 = 13", "16px 'Myriad Pro'");
        this.text_2.setTransform(85, 137);

        this.text_3 = new cjs.Text("9 + 4 = 13", "16px 'Myriad Pro'");
        this.text_3.setTransform(338, 137);


        this.addChild(this.shape_115, this.shape_100, this.shape_118, this.shape_112, this.shape_111, this.shape_117, this.shape_116, this.shape_113, this.shape_114);
        this.addChild(this.shape_137, this.shape_138, this.shape_139, this.shape_140, this.shape_141, this.shape_142);
        this.addChild(this.shape_128, this.shape_129, this.shape_130, this.shape_131, this.shape_132, this.shape_133, this.shape_134, this.shape_135, this.shape_136);
        this.addChild(this.shape_124, this.shape_125, this.shape_126, this.shape_127, this.shape_119, this.shape_120, this.shape_121, this.shape_122, this.shape_123);
        this.addChild(this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36);
        this.addChild(this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5, this.textbox_group1, this.text_6, this.text_7, this.text_8, this.text_9);
        this.addChild(this.redRountrect, this.greenRountrect, this.yellowRountrect, this.blueRountrect, this.redRountrect1, this.yellowRountrect, this.yellowRountrect1);
        this.addChild(this.instance, this.text_1, this.text_2, this.text_3);



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 182.6);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Addera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(19, 14);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(0, 14);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 255, 115, 10);
        this.roundRect1.setTransform(0, 27);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(259, 27);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 147);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(259, 147);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 267);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(259, 267);

        this.instance = new lib.p43_2();
        this.instance.setTransform(27, 37, 0.46, 0.46);

        this.redRountrect = new cjs.Shape();
        this.redRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 30, 80, 65.5, 6);
        this.redRountrect.setTransform(111, 8.5);

        this.redRountrect1 = this.redRountrect.clone(true);
        this.redRountrect1.setTransform(272, 8.5);

        this.redRountrect2 = this.redRountrect.clone(true);
        this.redRountrect2.setTransform(111, 250.5);

        this.redRountrect3 = this.redRountrect.clone(true);
        this.redRountrect3.setTransform(272, 250.5);

        this.yellowRountrect = new cjs.Shape();
        this.yellowRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 32, 57, 43, 6);
        this.yellowRountrect.setTransform(9.5, 17.5);

        this.yellowRountrect1 = this.yellowRountrect.clone(true);
        this.yellowRountrect1.setTransform(395.5, 17.5);

        this.yellowRountrect2 = this.yellowRountrect.clone(true);
        this.yellowRountrect2.setTransform(1.5, 132);

        this.yellowRountrect3 = this.yellowRountrect.clone(true);
        this.yellowRountrect3.setTransform(395.5, 132);

        this.greenRountrect = new cjs.Shape();
        this.greenRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(10, 30, 122, 49, 6);
        this.greenRountrect.setTransform(105, 130);

        this.greenRountrect1 = this.greenRountrect.clone(true);
        this.greenRountrect1.setTransform(270.5, 130)

        this.blueRountrect = new cjs.Shape();
        this.blueRountrect.graphics.f("#ffffff").s('#706F6F').drawRoundRect(30, 32, 61, 55, 6);
        this.blueRountrect.setTransform(9, 250);

        this.blueRountrect1 = this.blueRountrect.clone(true);
        this.blueRountrect1.setTransform(394.5, 250)

        this.text_1 = new cjs.Text("3 + 9 =", "16px 'Myriad Pro'");
        this.text_1.setTransform(85, 126);
        this.text_2 = new cjs.Text("4 + 8 =", "16px 'Myriad Pro'");
        this.text_2.setTransform(85, 244);
        this.text_3 = new cjs.Text("5 + 9 =", "16px 'Myriad Pro'");
        this.text_3.setTransform(85, 367);
        this.text_4 = new cjs.Text("9 + 3 =", "16px 'Myriad Pro'");
        this.text_4.setTransform(344, 126);
        this.text_5 = new cjs.Text("8 + 4 =", "16px 'Myriad Pro'");
        this.text_5.setTransform(344, 244);
        this.text_6 = new cjs.Text("9 + 5 =", "16px 'Myriad Pro'");
        this.text_6.setTransform(344, 367);

        var ToBeAdded = [];
        var tmpline = new cjs.Shape();
        var ypos = 119;
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                if (row == 2) {
                    ypos = 120;
                } else {
                    ypos = 119;
                }
                tmpline.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(140 + (260 * colSpace), 132 + (ypos * rowSpace)).lineTo(175 + (260 * colSpace), 132 + (ypos * rowSpace));
                tmpline.setTransform(0, 0);
                ToBeAdded.push(tmpline);
            }
        }

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.redRountrect, this.redRountrect1, this.redRountrect2, this.redRountrect3);
        this.addChild(this.yellowRountrect, this.yellowRountrect1, this.yellowRountrect2, this.yellowRountrect3);
        this.addChild(this.greenRountrect, this.greenRountrect1, this.blueRountrect, this.blueRountrect1);
        this.addChild(this.instance, this.text_q1, this.text_q2, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 350.2);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310, 109, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(310, 308, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
