(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p60_1.png",
            id: "p60_1"
        }, {
            src: "images/p60_2.png",
            id: "p60_2"
        }]
    };

    (lib.p60_1 = function() {
        this.initialize(img.p60_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p60_2 = function() {
        this.initialize(img.p60_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Räknehändelser", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(159, 38.6);

        this.text_0 = new cjs.Text("-", "bold 36px 'Epic Awesomeness'", "#ffffff");
        this.text_0.lineHeight = 45;
        this.text_0.setTransform(429, 36.8, 1.2, 1);

        this.text_1 = new cjs.Text("20", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(41.5, 22);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med subtraktion 0 till 6", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(80.5, 654.7);

        this.text_4 = new cjs.Text("60", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(39, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 20).lineTo(610, 20).moveTo(20, 20).lineTo(20, 660).moveTo(20, 660).lineTo(610, 660);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(70, 653.7, 195, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FAAA33").ss(0.5, 0, 0, 4).arc(0, 0, 14, 0, 2 * Math.PI);
        this.shape_5.setTransform(438, 61);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.shape_5, this.text_Rect, this.text_4, this.pageBottomText,
            this.text_1, this.text, this.text_0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text_5 = new cjs.Text("Hitta på räknehändelser till bilden.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(10, 22);

        this.instance = new lib.p60_1();
        this.instance.setTransform(0, 0, 0.506, 0.506);

        this.addChild(this.text_5, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 330);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Hitta på räknehändelser till subtraktionerna.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(8, -10);

        this.instance = new lib.p60_2();
        this.instance.setTransform(45, 30, 0.12, 0.12);

        this.instance1 = new lib.p60_2();
        this.instance1.setTransform(45, (980 * 0.12) + 30, 0.12, 0.12);

        this.instance2 = new lib.p60_2();
        this.instance2.setTransform((1780 * 0.12) + 80, 30, 0.12, 0.12);

        this.instance3 = new lib.p60_2();
        this.instance3.setTransform((1780 * 0.12) + 80, (980 * 0.12) + 30, 0.12, 0.12);

        this.labelTemp1 = new cjs.Text("4 – 1 = 3", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp1.lineHeight = 34;
        this.labelTemp1.setTransform(75, 45);

        this.labelTemp2 = new cjs.Text("3 – 2 = 1", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp2.lineHeight = 34;
        this.labelTemp2.setTransform(325, 45);

        this.labelTemp3 = new cjs.Text("5 – 2 = 3", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp3.lineHeight = 34;
        this.labelTemp3.setTransform(75, 163);

        this.labelTemp4 = new cjs.Text("2 – 0 = 2", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp4.lineHeight = 34;
        this.labelTemp4.setTransform(325, 163);


        this.addChild(this.text_1, this.instance, this.instance1, this.instance2, this.instance3, this.labelTemp1, this.labelTemp2, this.labelTemp3, this.labelTemp4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);


    (lib.p46 = function() {
        this.initialize();
        this.v1 = new lib.Symbol3();
        this.v1.setTransform(296.8, -20, 1, 1, 0, 0, 0, 256.3, -100);
        this.v2 = new lib.Symbol2();
        this.v2.setTransform(296.8, 270, 1, 1, 0, 0, 0, 256.3, -100);
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
