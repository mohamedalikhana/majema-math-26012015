(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p18_1.png",
            id: "p18_1"
        }, {
            src: "images/p18_2.png",
            id: "p18_2"
        }]
    };

    (lib.p18_1 = function() {
        this.initialize(img.p18_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p18_2 = function() {
        this.initialize(img.p18_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Talsymboler från förr", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(130, 73);

        this.text_1 = new cjs.Text("5", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(54, 43);

        this.text_3 = new cjs.Text("ha fått arbeta med talsymboler som användes förr", "9px 'Myriad Pro'", "#FAAA33");
        this.text_3.setTransform(85.5, 658);

        this.text_4 = new cjs.Text("18", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(38, 659);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 215, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p18_1();
        this.instance.setTransform(3, 19, 0.47, 0.47);

        this.text_1 = new cjs.Text("Egyptierna", "16px 'Myriad Pro'");
        this.text_1.setTransform(13, 17);

        this.text_2 = new cjs.Text("Mayafolket", "16px 'Myriad Pro");
        this.text_2.setTransform(13, 161);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(7, 24, 89, 107, 5);
        this.round_Rect1.setTransform(0, 0);

        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(7, 169, 89, 107.5, 5);
        this.round_Rect2.setTransform(0, 0);

        this.round_Rect3 = new cjs.Shape();
        this.round_Rect3.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(109, 24, 398, 123, 5);
        this.round_Rect3.setTransform(0, 0);

        this.round_Rect4 = new cjs.Shape();
        this.round_Rect4.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(109, 153, 398, 123.5, 5);
        this.round_Rect4.setTransform(0, 0);

        var NArr = [];
        var NlineX = [126, 166, 205, 245, 285, 325, 365, 405, 445, 478];
        var NlineY = [17, 17, 17, 17, 17, 17, 17, 17, 17, 17];

        for (i = 0; i < NlineX.length; i++) {
            var Num = i + 1;
            this.text_3 = new cjs.Text("" + Num, "16px 'Myriad Pro'", "#FAAA33");
            this.text_3.setTransform(NlineX[i], NlineY[i]);
            NArr.push(this.text_3);

        }


        var lineArr = [];
        var lineX = [108, 148, 189, 229, 269, 309, 349, 389, 429, 108, 148, 189, 229, 269, 309, 349, 389, 429];
        var lineY = [42, 42, 42, 42, 42, 42, 42, 42, 42, 172, 172, 172, 172, 172, 172, 172, 172, 172];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, -14).lineTo(40, 100);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.addChild(this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4, this.instance, this.text_1,
            this.text_2);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < NArr.length; i++) {
            this.addChild(NArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 350);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p18_2();
        this.instance.setTransform(3, 0, 0.47, 0.47);

        this.text_1 = new cjs.Text("Gör egna symboler för talen.", "16px 'Myriad Pro'");
        this.text_1.setTransform(59, 35);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(7, 62, 500, 80, 5);
        this.round_Rect1.setTransform(0, 0);

        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(7, 164, 500, 80, 5);
        this.round_Rect2.setTransform(0, 0);

        var lineArr = [];
        var lineX = [52, 136, 219, 303, 384, 52, 136, 219, 303, 384];
        var lineY = [64, 64, 64, 64, 64, 166, 166, 166, 166, 166];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, -1).lineTo(40, 77);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var NArr = [];
        var NlineX = [43, 126, 209, 292, 375, 458, 43, 126, 209, 292, 375, 458];
        var NlineY = [57, 57, 57, 57, 57, 57, 159, 159, 159, 159, 159, 157];

        for (i = 0; i < NlineX.length; i++) {
            var Num = i + 1;
            this.text_2 = new cjs.Text("" + Num, "16px 'Myriad Pro'", "#FAAA33");
            this.text_2.setTransform(NlineX[i], NlineY[i]);
            NArr.push(this.text_2);

        }

        this.addChild(this.instance, this.text_1, this.round_Rect1, this.round_Rect2);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < NArr.length; i++) {
            this.addChild(NArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 250);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(302, 275, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(302, 565, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
