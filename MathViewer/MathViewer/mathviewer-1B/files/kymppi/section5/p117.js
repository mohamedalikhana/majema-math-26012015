(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p116_1.png",
            id: "p116_1"
        }, {
            src: "images/p116_5.png",
            id: "p116_5"
        }, {
            src: "images/p116_3.png",
            id: "p116_3"
        }]
    };

    (lib.p116_1 = function() {
        this.initialize(img.p116_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p116_5 = function() {
        this.initialize(img.p116_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p116_3 = function() {
        this.initialize(img.p116_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("117", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(550, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);


        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 20).lineTo(0, 20).moveTo(590, 30).lineTo(590, 660).moveTo(590, 660).lineTo(0, 660);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(580, 30, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(590, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.instance_1 = new lib.p116_1();
        this.instance_1.setTransform(110, 70, 0.46, 0.46);

        this.instance_2 = new lib.p116_5();
        this.instance_2.setTransform(45, 75, 0.46, 0.46);

        this.instance_3 = new lib.p116_3();
        this.instance_3.setTransform(325, 105, 0.46, 0.46);

        this.text_6 = new cjs.Text("Mira och Leo fortsätter", "16px 'Myriad Pro'");
        this.text_6.setTransform(320, 190);

        this.text_5 = new cjs.Text("att kasta pil.", "16px 'Myriad Pro'");
        this.text_5.setTransform(320, 210);

        var ToBeAdded = []
        var tmptxt = [9, 8, 1, 3, 5, 6, 4, 2, 7, 10]
        var i = 0
        var xpos = 110
        var ypos = 125

        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 5; col++) {
                if (row == 0 && col == 0) {
                    xpos = 135;
                    ypos = 160
                } else if (row == 0 && col == 2) {
                    ypos = 115
                } else if (row == 0 && col == 4) {
                    xpos = 90;
                    ypos = 160
                } else if (row == 1 && col == 0) {
                    xpos = 140;
                    ypos = 200
                } else if (row == 1 && col == 3) {
                    xpos = 127;
                    ypos = 205
                } else if (row == 1 && col == 4) {
                    xpos = 22;
                    ypos = 171
                } else if (row == 1) {
                    ypos = 225;
                    xpos = 130
                } else {
                    xpos = 110;
                    ypos = 125
                }
                var tempText = new cjs.Text(tmptxt[i], "27.96px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 40), ypos);
                ToBeAdded.push(tempText);
                i++
            }
        };


        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4);
        this.addChild(this.instance_1, this.instance_2, this.instance_3, this.text_6, this.text_5)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 400);


    (lib.Symbol2 = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(0, 0, 510, 330, 10);
        this.round_Rect1.setTransform(15, 10);

        this.text_3 = new cjs.Text("Skriv och rita en räknehändelse om pilkastningen.", "16px 'Myriad Pro'");
        this.text_3.setTransform(15, 0);
        var ToBeAdded = []
        var lineToXpos = 500
        for (var row = 0; row < 7; row++) {
            var hrLine = new cjs.Shape();
            if (row > 2) {
                lineToXpos = 290
            } else {
                lineToXpos = 500
            }
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(40, 55 + (row * 43)).lineTo(lineToXpos, 55 + (row * 43));
            ToBeAdded.push(hrLine)
        };

        this.text_6 = new cjs.Text("Visa och berätta för en kamrat.", "16px 'Myriad Pro'");
        this.text_6.setTransform(15, 358);

        this.addChild(this.text_3, this.round_Rect1, this.text_6);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 310);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290, 455, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
