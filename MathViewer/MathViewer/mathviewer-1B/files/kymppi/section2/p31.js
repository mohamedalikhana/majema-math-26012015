(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p31_1.png",
            id: "p31_1"
        }]
    };

    (lib.p31_1 = function() {
        this.initialize(img.p31_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("31", "13px 'Myriad Pro'", "#FFFFFF");        
        this.text.setTransform(555, 663);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);        

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));        

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p31_1();
        this.instance.setTransform(15, 72, 0.47, 0.47);

        this.label1 = new cjs.Text("14 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label1.setTransform(25, 110);
        this.label2 = new cjs.Text("16 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label2.setTransform(280, 110);
        this.label3 = new cjs.Text("17 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label3.setTransform(25, 230);
        this.label4 = new cjs.Text("19 kr tillsammans", "16.3px 'Myriad Pro'");
        this.label4.setTransform(280, 230);

        this.label5 = new cjs.Text("4 kr", "16.3px 'Myriad Pro'");
        this.label5.setTransform(202, 103);
        this.label6 = new cjs.Text("6 kr", "16.3px 'Myriad Pro'");
        this.label6.setTransform(463, 103);
        this.label7 = new cjs.Text("1 kr", "16.3px 'Myriad Pro'");
        this.label7.setTransform(202, 227);
        this.label8 = new cjs.Text("5 kr", "16.3px 'Myriad Pro'");
        this.label8.setTransform(463, 227);

        this.label5.skewX = 7;
        this.label5.skewY = 7;
        this.label6.skewX = 7;
        this.label6.skewY = 7;
        this.label7.skewX = 7;
        this.label7.skewY = 7;
        this.label8.skewX = 7;
        this.label8.skewY = 7;

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(5, 50);

        this.text_1 = new cjs.Text("Vad kostar pärlan?", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 50);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 253, 115, 10);
        this.roundRect1.setTransform(5, 55);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(263, 55);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(5, 175);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(263, 175);


        this.InnerroundRect1 = new cjs.Shape();
        this.InnerroundRect1.graphics.f("#ffffff").s('#DFEAC5').ss(2).drawRoundRect(0, 10, 140, 75, 10);
        this.InnerroundRect1.setTransform(17, 75);

        this.InnerroundRect2 = this.InnerroundRect1.clone(true);
        this.InnerroundRect2.setTransform(270, 75);

        this.InnerroundRect3 = this.InnerroundRect1.clone(true);
        this.InnerroundRect3.setTransform(17, 195);

        this.InnerroundRect4 = this.InnerroundRect1.clone(true);
        this.InnerroundRect4.setTransform(270, 195);
       

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.InnerroundRect1, this.InnerroundRect2);
        this.addChild(this.text_1, this.textbox_group1, this.InnerroundRect3, this.InnerroundRect4);
        this.addChild(this.instance_2, this.text, this.instance, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 497.3, 380);

    (lib.Symbol6 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna och måla svaret.", "16px 'Myriad Pro'");     
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");      
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 511, 267, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();        
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text(" 8  +  2  +  2  =", "16px 'Myriad Pro'");
        this.label1.setTransform(42, 43);

        this.label2 = new cjs.Text("20  –  2  –  3  =", "16px 'Myriad Pro'");
        this.label2.setTransform(40, 73);

        this.label3 = new cjs.Text("12  –  2  –  1  =", "16px 'Myriad Pro'");
        this.label3.setTransform(40, 103);

        this.label4 = new cjs.Text(" 6  +  4  +  8  =", "16px 'Myriad Pro'");
        this.label4.setTransform(42, 133);

        this.label5 = new cjs.Text(" 7  +  3  +  4  =", "16px 'Myriad Pro'");
        this.label5.setTransform(42, 163);

        this.label6 = new cjs.Text(" 7  +  3  +  7  =", "16px 'Myriad Pro'");
        this.label6.setTransform(42, 193);


        this.label7 = new cjs.Text("10  +  5  +  5  =", "16px 'Myriad Pro'");
        this.label7.setTransform(260, 43);

        this.label8 = new cjs.Text("20  –  4  –  5  =", "16px 'Myriad Pro'");
        this.label8.setTransform(262, 73);

        this.label9 = new cjs.Text(" 8  +  2  +  6  =", "16px 'Myriad Pro'");
        this.label9.setTransform(264, 103);

        this.label10 = new cjs.Text("20  –  5  –  5  =", "16px 'Myriad Pro'");
        this.label10.setTransform(262, 133);

        this.label11 = new cjs.Text(" 5  +  5  +  3  =", "16px 'Myriad Pro'");
        this.label11.setTransform(264, 163);

        this.label12 = new cjs.Text(" 5  +  5  +  9  =", "16px 'Myriad Pro'");
        this.label12.setTransform(264, 193);

        // ROW-1 ROUND SHAPES--columns1
        this.hrLine_1 = new cjs.Shape();
        this.shape_group2 = new cjs.Shape();
        var fillcolor ="";
        var columnXpos=0;  
        var MoveToX=0;
        var LineToX=0;
        var LineToY=0;
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {

                if (row < 4 && column == 0) {
                	fillcolor="#008BD2";
                	columnXpos=195;                    
                } else if (row > 3 && column == 0) {
                	fillcolor="#FFF374";
                	columnXpos=195;                   
                } else if (row < 2 && column == 1) {
                	fillcolor="#FFF374";
                	columnXpos=390;                   
                } else if (row > 1 && column == 1) {
                	fillcolor="#D51317";
                	columnXpos=390;     
                	}               
                 this.shape_group2.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 25), 42 + (row * 29), 9.5, 0, 2 * Math.PI)
                 
                if (column == 0 && row==4) 
                {
                	 MoveToX=138;
        			 LineToX=178;
       				 LineToY=54;                    
                } 
                else if(column == 0 && row==5)
                {
                	 MoveToX=138;
        			 LineToX=178;
       				 LineToY=54;                      
                }
                 else if(column == 1 && row==5)
                 {
                 	 MoveToX=337;
        			 LineToX=375;
       				 LineToY=54; 					
                 }
                 else if(column == 1 && row==4)
                 {
                 	 MoveToX=337;
        			 LineToX=375;
       				 LineToY=54;                  	
                 }
                  else if(column == 0)
                  {
                  	 MoveToX=138;
        			 LineToX=178;
       				 LineToY=51;                   	
                  }
                  else if(column == 1)
                  {
                  	 MoveToX=337;
        			 LineToX=375;
       				 LineToY=51;                   
                  }
                  this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(MoveToX + (25 * columnSpace), LineToY + (29 * row)).lineTo(LineToX + (25 * columnSpace), LineToY + (29 * row));
               
            }
        }
        this.shape_group2.setTransform(0, 0);
        this.hrLine_1.setTransform(0, 0);

        //Row-1 round shapes with numbers

        var arrxPos = ['58'];
        var circleNum = 8;
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            for (var column = 0; column < 12; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) {
                    this.shape_group1.graphics.s("#6E6E70").ss(0.8, 0, 0, 6).arc(xPos + (columnSpace * 35), 248 + (row * 23), 15, 0, 2 * Math.PI);

                }

            }
        }
        this.shape_group1.setTransform(0, 0);

        var ToBeAdded = [];
        var xval = 0;
        var j = 9;
   
        for (var xval = 0; xval < 12; xval++) {
            var colSpace = xval;
            if (xval == 2) {
                colSpace = 2.1;
            } else if (xval == 3) {
                colSpace = 3.15;
            } else if (xval == 4) {
                colSpace = 4.25;
            } else if (xval == 5) {
                colSpace = 5.35;
            } else if (xval == 6) {
                colSpace = 6.45;
            } else if (xval == 7) {
                colSpace = 7.55;
            } else if (xval == 8) {
                colSpace = 8.65;
            } else if (xval == 9) {
                colSpace = 9.75;
            } else if (xval == 10) {
                colSpace = 10.85;
            } else if (xval == 11) {
                colSpace = 11.95;
            }

            var temp_text = new cjs.Text(j, "15.5px 'Myriad Pro'");
            temp_text.setTransform(53 + (colSpace * 32), 253);
            ToBeAdded.push(temp_text);
            j++;
        };


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12);

        this.addChild(this.shape_group1, this.label13, this.shape_group2, this.hrLine_1, this.hrLine_2);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(309, 274, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 337.5, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
