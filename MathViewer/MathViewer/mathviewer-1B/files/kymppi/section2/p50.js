(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p50_1.png",
            id: "p50_1"
        }, {
            src: "images/p50_2.png",
            id: "p50_2"
        }, {
            src: "images/p50_3.png",
            id: "p50_3"
        }]
    };

    // symbols:
    (lib.p50_1 = function() {
        this.initialize(img.p50_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p50_2 = function() {
        this.initialize(img.p50_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p50_3 = function() {
        this.initialize(img.p50_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1

        this.text = new cjs.Text("50", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.2);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Måla cirkeln om svaret stämmer.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(0, 0);

        var arryVal=['4 + 7','6 + 5','6 + 6','9 + 2','3 + 8','9 + 3','4 + 8','5 + 6','6 + 6','7 + 4','3 + 9','7 + 6','6 + 7','9 + 4','5 + 8','6 + 6','7 + 6','4 + 9'] ;
        var ToBeAdded = [];
        var i=0;
        var tmparc= new cjs.Shape(); 
        for (var col = 0; col < 3; col++) {
                var columnSpace=col;
                for (var row = 0; row < 6; row++) {
                    var rowSpace=row;
                     var tempText = new cjs.Text( arryVal[i], "16px 'Myriad Pro'");
                     tempText.setTransform(55 + (172 * columnSpace), 125 + (22 * rowSpace));
                     tmparc.graphics.f("#FFFFFF").s("#878787").ss(0.8, 0, 0, 4).arc(103 + (columnSpace * 172), 120 + (row * 22), 8, 0, 2 * Math.PI)
                     
                     ToBeAdded.push(tempText,tmparc)
                     i++;
                };
            };    

        this.instance = new lib.p50_1();
        this.instance.setTransform(10, 18, 0.46, 0.46);


        this.label = new cjs.Text("11", "16px 'Myriad Pro'");
        this.label.setTransform(85 , 45 )

        this.label1 = new cjs.Text("12", "16px 'Myriad Pro'");
        this.label1.setTransform(257 , 45 );

        this.label2 = new cjs.Text("13", "16px 'Myriad Pro'");
        this.label2.setTransform(430 , 45 );



        this.addChild(this.text, this.text_1, this.instance,this.label,this.label1,this.label2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 250);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p50_2();
        this.instance.setTransform(13, 23, 0.46, 0.46);

        this.instance1 = new lib.p50_3();
        this.instance1.setTransform(445.5, 17.5, 0.46, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(10, 11, 515, 245, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Rita prickarna som saknas på den tomma tärningen.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(10, 0);

        var tmpRect = new cjs.Shape();
        var ToBeAdded = [];
        for (var row = 0; row < 5; row++) {
            var rowSpace = row;
            for (var column = 0; column < 15; column++) {
                var columnSpace = column;
                tmpRect.graphics.f('#ffffff').s("#9D9D9C").drawRoundRect(129 + (columnSpace * 26), 25 + (rowSpace * 47), 25, 25, 6);
                tmpRect.setTransform(0, 0);
                ToBeAdded.push(tmpRect);
            }
        }

        // set numbers

        var xval = 0;
        // var tmpspace = 26.5;
        var tmpspace = 26.5;
        var tmpspace1=47.5;
        for (var row = 0; row < 5; row++) {
            var rowSpace = row;
            for (var xval = 0; xval < 15; xval++) {
                var colSpace = xval;
                if (xval == 8) {
                    tmpspace = 26.2;
                } else if (xval == 9) {
                    tmpspace = 25.8;
                } else if (xval == 10) {
                    tmpspace = 25.8;
                } else if (xval == 11) {
                    tmpspace = 25.8;
                }  else if (xval == 13) {
                    tmpspace = 25.8;
                } else if (xval == 14) {
                    tmpspace = 25.8;
                }  else {
                    tmpspace = 26.5;
                }
            if(xval==12 && row>1){tmpspace=25.8;}
            if(row>2){tmpspace1=47} else{tmpspace1=47.5}
             if (xval == 12 && row === 0) {continue;} 
             if (xval == 12 && row === 1) {continue;}
             if (xval == 13 && row === 2) {continue;}  
             if (xval == 13 && row === 3) {continue;}
             if (xval == 14 && row === 4) {continue;} 

                var temp_text = new cjs.Text(xval + 1, "16px 'Myriad Pro'");
                temp_text.setTransform(135 + (colSpace * tmpspace), 42 + (rowSpace * tmpspace1));
                ToBeAdded.push(temp_text);
            };
        };

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance, this.instance1);



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(285, 401, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(294, 122, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
