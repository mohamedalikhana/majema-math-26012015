(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p42_1.png",
            id: "p42_1"
        }, {
            src: "images/p42_2.png",
            id: "p42_2"
        }]
    };

    // symbols:
    (lib.p42_1 = function() {
        this.initialize(img.p42_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p42_2 = function() {
        this.initialize(img.p42_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1

        this.text = new cjs.Text("42", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(39, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.2);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Addera talet i mitten med talen runt omkring.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 507, 242, 10);
        this.roundRect1.setTransform(0, 12);

        this.instance = new lib.p42_1();
        this.instance.setTransform(30, 18, 0.46, 0.46);
        // set NUmber in wheel 1
        this.label1 = new cjs.Text("9", "16px 'MyriadPro-Semibold'");
        this.label1.setTransform(107.5, 137.5);
        this.label2 = new cjs.Text("3", "16px 'MyriadPro-Semibold'");
        this.label2.setTransform(115, 121);
        this.label3 = new cjs.Text("5", "16px 'MyriadPro-Semibold'");
        this.label3.setTransform(135, 121);
        this.label4 = new cjs.Text("7", "16px 'MyriadPro-Semibold'");
        this.label4.setTransform(145, 137.5);
        this.label5 = new cjs.Text("6", "16px 'MyriadPro-Semibold'");
        this.label5.setTransform(135, 155);
        this.label6 = new cjs.Text("8", "16px 'MyriadPro-Semibold'");
        this.label6.setTransform(115, 154);

        // set NUmber in wheel 2
        this.label7 = new cjs.Text("9", "16px 'MyriadPro-Semibold'");
        this.label7.setTransform(349, 137.5);
        this.label8 = new cjs.Text("3", "16px 'MyriadPro-Semibold'");
        this.label8.setTransform(360, 121);
        this.label9 = new cjs.Text("4", "16px 'MyriadPro-Semibold'");
        this.label9.setTransform(379, 121);
        this.label10 = new cjs.Text("6", "16px 'MyriadPro-Semibold'");
        this.label10.setTransform(389, 137.5);
        this.label11 = new cjs.Text("5", "16px 'MyriadPro-Semibold'");
        this.label11.setTransform(379, 155);
        this.label12 = new cjs.Text("7", "16px 'MyriadPro-Semibold'");
        this.label12.setTransform(359, 154);

        //set number in wheel centre
        this.label13 = new cjs.Text("9", "bold 16px 'Myriad Pro'", "#D5132B");
        this.label13.setTransform(125, 138);
        this.label14 = new cjs.Text("8", "bold 16px 'Myriad Pro'", "#D5132B");
        this.label14.setTransform(370, 138);
        //set line in wheels
        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(87, 72).lineTo(115, 72);
        this.Line_1.setTransform(10, 30);
        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(131, 72).lineTo(156, 72);
        this.Line_2.setTransform(10, 30);
        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(64, 114).lineTo(87, 114);
        this.Line_3.setTransform(10, 30);
        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(155, 114).lineTo(180, 114);
        this.Line_4.setTransform(10, 30);
        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(87, 152).lineTo(113, 152);
        this.Line_5.setTransform(10, 30);
        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(131, 152).lineTo(156, 152);
        this.Line_6.setTransform(10, 30);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(330, 72).lineTo(358, 72);
        this.Line_7.setTransform(10, 30);
        this.Line_8 = new cjs.Shape();
        this.Line_8.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(375, 72).lineTo(402, 72);
        this.Line_8.setTransform(10, 30);
        this.Line_9 = new cjs.Shape();
        this.Line_9.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(308, 114).lineTo(331, 114);
        this.Line_9.setTransform(10, 30);
        this.Line_10 = new cjs.Shape();
        this.Line_10.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(399, 114).lineTo(424, 114);
        this.Line_10.setTransform(10, 30);
        this.Line_11 = new cjs.Shape();
        this.Line_11.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(332, 152).lineTo(358, 152);
        this.Line_11.setTransform(10, 30);
        this.Line_12 = new cjs.Shape();
        this.Line_12.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(374, 152).lineTo(401, 152);
        this.Line_12.setTransform(10, 30);

        this.text_3 = new cjs.Text("12", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(95, 100.8);


        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.text_3);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14)
        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7, this.Line_8, this.Line_9, this.Line_10, this.Line_11, this.Line_12)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 250);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p42_2();
        this.instance.setTransform(43, 27, 0.46, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(10, 11, 250, 261, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(265, 11, 250, 261, 10);
        this.roundRect2.setTransform(0, 0);

        this.innerRoundRect = new cjs.Shape();
        this.innerRoundRect.graphics.f("#ffffff").s('#F7B48C').drawRoundRect(51, 34.5, 172, 115, 6);
        this.innerRoundRect.setTransform(0, 0)

        this.innerRoundRect1 = new cjs.Shape();
        this.innerRoundRect1.graphics.f("#ffffff").s('#F7B48C').drawRoundRect(283, 34.5, 213, 115, 6);
        this.innerRoundRect1.setTransform(0, 0)

        this.text = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(10, 0);

        var arrRowpos = ['190', '223'];
        var arrColumnpos = ['80', '170', '322', '430'];
        var lxpos = ['92', '185', '335', '442'];
        var lypos = ['195', '227'];
        var lxpos1 = ['132', '225', '375', '480'];

        var ToBeAdded = [];
        var tmpline = new cjs.Shape();
        for (var Rindex = 0; Rindex < arrRowpos.length; Rindex++) {
            var row = parseInt(arrRowpos[Rindex]);
            for (var Cindex = 0; Cindex < arrColumnpos.length; Cindex++) {
                var col = parseInt(arrColumnpos[Cindex]);
                var tempLabel = new cjs.Text("=", "16px 'Myriad Pro'");
                tmpline.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(lxpos[Cindex], lypos[Rindex]).lineTo(lxpos1[Cindex], lypos[Rindex]);
                tempLabel.setTransform(col, row);
                ToBeAdded.push(tempLabel, tmpline);
            }
        }

        var arrRowpos1 = ['55', '85', '107', '135'];
        var arrNum = ['16', '12', ' ', ' '];
        var tmpline1 = new cjs.Shape();
        for (var Rindex1 = 0; Rindex1 < arrRowpos1.length; Rindex1++) {
            var row1 = parseInt(arrRowpos1[Rindex1]);
            var tempLabe2 = new cjs.Text("+           =     " + arrNum[Rindex1], "16px 'Myriad Pro'");
            tempLabe2.setTransform(110, row1);
            ToBeAdded.push(tempLabe2, tmpline1);

        }

        var arrRowpos2 = ['56', '85', '107', '135'];

        var arrNum1 = ['18', '18', ' ', ' '];
        var tmpline2 = new cjs.Shape();
        for (var Rindex2 = 0; Rindex2 < arrRowpos2.length; Rindex2++) {
            var row2 = parseInt(arrRowpos2[Rindex2]);
            var tempLabe3 = new cjs.Text("+           =     " + arrNum1[Rindex2], "16px 'Myriad Pro'");
            tempLabe3.setTransform(385, row2);
            ToBeAdded.push(tempLabe3, tmpline2);

        }

        this.label1 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.label1.setTransform(327, 85);
        this.label2 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.label2.setTransform(327, 135);


        this.addChild(this.roundRect1, this.roundRect2, this.innerRoundRect, this.innerRoundRect1, this.text, this.text_1, this.text_2, this.instance, this.label1, this.label2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(290, 401, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(299, 122, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
