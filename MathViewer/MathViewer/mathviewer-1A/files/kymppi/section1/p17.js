(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/s18_19_1.png",
            id: "s18_19_1"
        }, {
            src: "images/s18_19_13.png",
            id: "s18_19_13"
        }, {
            src: "images/s18_19_2.png",
            id: "s18_19_2"
        }, {
            src: "images/s18_19_42.png",
            id: "s18_19_42"
        }, {
            src: "images/s18_19_43.png",
            id: "s18_19_43"
        }, {
            src: "images/s18_19_44.png",
            id: "s18_19_44"
        }, {
            src: "images/s18_19_45.png",
            id: "s18_19_45"
        }, {
            src: "images/s18_19_46.png",
            id: "s18_19_46"
        }, {
            src: "images/s18_19_57.png",
            id: "s18_19_57"
        }]
    };

    // symbols:
    (lib.s18_19_1 = function() {
        this.initialize(img.s18_19_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 951, 610);


    (lib.s18_19_13 = function() {
        this.initialize(img.s18_19_13);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1076, 415);


    (lib.s18_19_2 = function() {
        this.initialize(img.s18_19_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 929, 391);


    (lib.s18_19_42 = function() {
        this.initialize(img.s18_19_42);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1274, 630);


    (lib.s18_19_43 = function() {
        this.initialize(img.s18_19_43);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1274, 630);


    (lib.s18_19_44 = function() {
        this.initialize(img.s18_19_44);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1274, 630);


    (lib.s18_19_45 = function() {
        this.initialize(img.s18_19_45);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1274, 630);


    (lib.s18_19_46 = function() {
        this.initialize(img.s18_19_46);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1059, 408);


    (lib.s18_19_57 = function() {
        this.initialize(img.s18_19_57);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 179, 146);

    (lib.Symbol17 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("17", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 24;
        this.text.setTransform(555, 6);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 18.4);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 35.1);


    (lib.Symbol1 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text = new cjs.Text("Kulorna delas lika mellan Mira och Leo. Visa.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(5, 6.2);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(5, 0, 485.3, 25.3);


    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4pgQgcAAgdAOQg4AdAABHIAAPeIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhGIAAveQAAgdgNgcQgdg5hHAAg");
        this.shape.setTransform(386.6, 227.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4pgQgcAAgdAOQg4AcAABIIAAPdIABASQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gdAAhHIAAvdQAAgdgNgcQgdg5hHAAg");
        this.shape_1.setTransform(386.6, 95.4);

        this.instance = new lib.Symbol1();
        this.instance.setTransform(224, 13.6, 1, 1, 0, 0, 0, 208.6, 12.6);

        this.text = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 6);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnLjKQgdAAgcAPQg5AcAABHIAACyIACARQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEARgJQA5gcAAhGIAAiyQAAgdgOgcQgdg5hGAAg");
        this.shape_2.setTransform(447.9, 259.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnLjKQgdAAgcAPQg5AcAABHIAACyIACARQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEASgJQA4gcAAhGIAAiyQAAgdgOgcQgcg5hHAAg");
        this.shape_3.setTransform(324.8, 259.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnMjKQgcAAgcAPQg5AcAABHIAACyIACARQADAVAJASQAcA5BHAAIOYAAIASgCQAWgEARgJQA5gcAAhGIAAiyQAAgdgOgcQgdg5hHAAg");
        this.shape_4.setTransform(190.1, 259.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnMjKQgcAAgcAPQg5AcAABHIAACyIACARQAEAVAIASQAdA5BGAAIOZAAIARgCQAWgEARgJQA5gcAAhGIAAiyQAAgdgOgcQgdg5hGAAg");
        this.shape_5.setTransform(66.9, 259.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnLjJQgdAAgcAOQg5AcAABHIAACxIACASQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEARgIQA5gdAAhHIAAixQAAgcgOgcQgdg5hGAAg");
        this.shape_6.setTransform(447.9, 127.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnLjJQgdAAgcAOQg5AcAABHIAACxIACASQAEAVAIASQAdA5BHAAIOYAAIARgCQAWgEASgIQA4gdAAhHIAAixQAAgcgOgcQgcg5hHAAg");
        this.shape_7.setTransform(324.8, 127.6);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnMjJQgcAAgcAOQg5AcAABHIAACxIACASQADAVAJASQAcA5BHAAIOYAAIASgCQAWgEARgIQA5gdAAhHIAAixQAAgcgOgcQgdg5hHAAg");
        this.shape_8.setTransform(190.1, 127.6);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnMjJQgcAAgcAOQg5AcAABHIAACxIACASQAEAVAIASQAdA5BGAAIOZAAIARgCQAWgEARgIQA5gdAAhHIAAixQAAgcgOgcQgdg5hGAAg");
        this.shape_9.setTransform(66.9, 127.6);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4pgQgcAAgdAOQg4AdAABHIAAPeIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhGIAAveQAAgdgNgcQgdg5hHAAg");
        this.shape_10.setTransform(127.9, 227.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4pgQgcAAgdAOQg4AcAABIIAAPdIABASQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gdAAhHIAAvdQAAgdgNgcQgdg5hHAAg");
        this.shape_11.setTransform(127.9, 95.4);

        this.instance_1 = new lib.s18_19_2();
        this.instance_1.setTransform(27.3, 42.5, 0.5, 0.5);

        this.addChild(this.instance_1, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.instance, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.5, 289.9);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 2


        // Layer 1
        this.text = new cjs.Text("Måla.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(30.2, 1.2);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(11.3, 0);

        this.instance = new lib.s18_19_46();
        this.instance.setTransform(1.6, 48.7, 0.5, 0.5);

        this.text_2 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 16;
        this.text_2.setTransform(172.6, 29);

        this.text_3 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 16;
        this.text_3.setTransform(121.1, 29);

        this.text_4 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 16;
        this.text_4.setTransform(69.6, 29);

        this.text_5 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 16;
        this.text_5.setTransform(18.2, 29);

        this.instance_1 = new lib.s18_19_42();
        this.instance_1.setTransform(166.9, 26.6, 0.035, 0.035);

        this.instance_2 = new lib.s18_19_43();
        this.instance_2.setTransform(62.7, 26.6, 0.035, 0.035);

        this.instance_3 = new lib.s18_19_44();
        this.instance_3.setTransform(115.6, 26.6, 0.035, 0.035);

        this.instance_4 = new lib.s18_19_45();
        this.instance_4.setTransform(11.3, 26.6, 0.035, 0.035);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#C51930").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
        this.shape.setTransform(199.6, 37.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFF173").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
        this.shape_1.setTransform(147.3, 37.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#25A449").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
        this.shape_2.setTransform(94.8, 37.4);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#0089CA").s().p("AhYBUIAAinIBVAAIBcBYIhcBPg");
        this.shape_3.setTransform(43.3, 37.4);

        this.addChild(this.shape_3, this.shape_2, this.shape_1, this.shape, this.instance_4, this.instance_3, this.instance_2, this.instance_1, this.text_5, this.text_4, this.text_3, this.text_2, this.instance, this.text_1, this.text, this.ra1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-3.3, 0, 538, 233.3);


    // stage content:
    (lib.p17 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol17();
        this.other.setTransform(609.5, 660.2, 1, 1, 0, 0, 0, 609.5, 17.5);

        this.v3 = new lib.Symbol11();
        this.v3.setTransform(311.8, 485, 1, 1, 0, 0, 0, 256.3, 144.6);

        this.v1 = new lib.Symbol3();
        this.v1.setTransform(310.9, 165.7, 1, 1, 0, 0, 0, 266.6, 114.8);

        this.addChild(this.v1, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 389.9, 1218.9, 626.9);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
