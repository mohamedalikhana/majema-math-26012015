(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p64_2.png",
            id: "p64_2"
        }, {
            src: "images/p64_1.png",
            id: "p64_1"
        }, {
            src: "images/p64_3.png",
            id: "p64_3"
        }]
    };

    (lib.p64_2 = function() {
        this.initialize(img.p64_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p64_1 = function() {
        this.initialize(img.p64_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p64_3 = function() {
        this.initialize(img.p64_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 650, 450);

    (lib.customShape1 = function(fCTriangle, fCRectangle, text, x, y, scaleX, scaleY) {
        var shapeWithText = {
            shape: null,
            text: null
        };
        var customShape = new cjs.Shape();
        customShape.graphics.f(fCTriangle).s('#7d7d7d').ss(3.5).moveTo(50, 0).lineTo(70, 0).lineTo(100, 25).lineTo(70, 50).lineTo(50, 50).lineTo(50, 0);
        customShape.graphics.f(fCRectangle).s('#7d7d7d').ss(3.5).moveTo(50, 0).lineTo(0, 0).lineTo(0, 50).lineTo(50, 50);
        var text = new cjs.Text(text, "16px 'Myriad Pro'");
        text.lineHeight = -1;
        text.setTransform(x + 3, y + 3);
        customShape.setTransform(x, y, scaleX, scaleY);
        shapeWithText.shape = customShape.clone(true);
        shapeWithText.text = text;
        return shapeWithText;
    }).prototype = p = new cjs.Container();


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_2 = new cjs.Text("64", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(39, 668);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#88C13F").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 680.8);

        this.addChild(this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol3 = function() {
        this.initialize();
        this.customShape4 = lib.customShape1("#cc3333", "#ffffff", "2", 261, 25.5, 0.38, 0.38);
        this.customShape3 = lib.customShape1("#0099cc", "#ffffff", "3", 313, 25.5, 0.38, 0.38);
        this.customShape2 = lib.customShape1("#ffff66", "#ffffff", "4", 363, 25.5, 0.38, 0.38);
        this.customShape5 = lib.customShape1("#339933", "#ffffff", "5", 418, 25.5, 0.38, 0.38);
        this.customShape6 = lib.customShape1("#BD8146", "#ffffff", "6", 471, 25.5, 0.38, 0.38);

        this.pageTitle = new cjs.Text("Räkna och måla.", "16px 'Myriad Pro'");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(26.5, 27.2);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(6, 27);

        this.instance = new lib.p64_2();
        this.instance.setTransform(-18, 52, 0.39, 0.39);

        this.label1 = new cjs.Text("4 + 2", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(50, 125);

        this.label2 = new cjs.Text("6 – 3", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(53, 153);

        this.label3 = new cjs.Text("5 – 3", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(53, 179);

        this.label4 = new cjs.Text("6 – 2", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(197, 92, 1, 1, 7);

        this.label5 = new cjs.Text("2 + 3", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(193, 115, 1, 1, 7);

        this.label6 = new cjs.Text("5 – 1", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(191, 137, 1, 1, 7);

        this.label7 = new cjs.Text("2 + 2", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(161, 191, 1, 1, -5);

        this.label8 = new cjs.Text("3 + 3", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(165, 217, 1, 1, -5);

        this.label9 = new cjs.Text("4 – 1", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(167, 243, 1, 1, -5);

        this.label10 = new cjs.Text("6 – 1", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(319, 131, 1, 1, -5);

        this.label11 = new cjs.Text("5 – 2", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(320, 157, 1, 1, -5);

        this.label12 = new cjs.Text("4 – 2", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(322, 185, 1, 1, -5);

        this.label13 = new cjs.Text("1 + 5", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(430, 164, 1, 1, 7);

        this.label14 = new cjs.Text("3 + 1", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(427, 197, 1, 1, 7);

        this.label15 = new cjs.Text("6 – 4", "16px 'Myriad Pro'");
        this.label15.lineHeight = 19;
        this.label15.setTransform(425, 228, 1, 1, 7);

        this.addChild(this.pageTitle, this.shape_2, this.shape, this.text_1, this.text_3, this.instance,
            this.pageFooterTextRight, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7,
            this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15, this.shape_3, this.shape_4);
        this.addChild(this.customShape2.shape, this.customShape2.text, this.customShape3.shape, this.customShape3.text, this.customShape4.shape,
            this.customShape4.text, this.customShape5.shape, this.customShape5.text, this.customShape6.shape, this.customShape6.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 360);


    (lib.Symbol2 = function() {
        this.initialize();

        var textArr = [];
        this.text_1 = new cjs.Text("Hitta vägen med rätt svar.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(25, 7, 1, 1, 0, 1, 0);

        this.text_2 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_2.lineHeight = 27;
        this.text_2.setTransform(5, 7);

        this.labelR11 = new cjs.Text("6 – 1", "16px 'Myriad Pro'");
        this.labelR11.lineHeight = 19;
        this.labelR11.setTransform(88, 77);
        this.labelR12 = new cjs.Text("2 + 3", "16px 'Myriad Pro'");
        this.labelR12.lineHeight = 19;
        this.labelR12.setTransform(88, 123);
        this.labelR13 = new cjs.Text("1 + 1", "16px 'Myriad Pro'");
        this.labelR13.lineHeight = 19;
        this.labelR13.setTransform(88, 169);
        this.labelR14 = new cjs.Text("3 + 1", "16px 'Myriad Pro'");
        this.labelR14.lineHeight = 19;
        this.labelR14.setTransform(88, 215);
        this.labelR15 = new cjs.Text("6 – 2", "16px 'Myriad Pro'");
        this.labelR15.lineHeight = 19;
        this.labelR15.setTransform(88, 260);

        this.labelR21 = new cjs.Text("4 + 0", "16px 'Myriad Pro'");
        this.labelR21.lineHeight = 19;
        this.labelR21.setTransform(170, 77);
        this.labelR22 = new cjs.Text("2 + 2", "16px 'Myriad Pro'");
        this.labelR22.lineHeight = 19;
        this.labelR22.setTransform(170, 123);
        this.labelR23 = new cjs.Text("5 – 0", "16px 'Myriad Pro'");
        this.labelR23.lineHeight = 19;
        this.labelR23.setTransform(170, 169);
        this.labelR24 = new cjs.Text("4 – 0", "16px 'Myriad Pro'");
        this.labelR24.lineHeight = 19;
        this.labelR24.setTransform(170, 215);
        this.labelR25 = new cjs.Text("6 – 3", "16px 'Myriad Pro'");
        this.labelR25.lineHeight = 19;
        this.labelR25.setTransform(170, 260);

        this.labelR31 = new cjs.Text("5 – 3", "16px 'Myriad Pro'");
        this.labelR31.lineHeight = 19;
        this.labelR31.setTransform(264, 77);
        this.labelR32 = new cjs.Text("6 – 2", "16px 'Myriad Pro'");
        this.labelR32.lineHeight = 19;
        this.labelR32.setTransform(264, 123);
        this.labelR33 = new cjs.Text("1 + 3", "16px 'Myriad Pro'");
        this.labelR33.lineHeight = 19;
        this.labelR33.setTransform(264, 169);
        this.labelR34 = new cjs.Text("5 – 1", "16px 'Myriad Pro'");
        this.labelR34.lineHeight = 19;
        this.labelR34.setTransform(264, 215);
        this.labelR35 = new cjs.Text("2 + 3", "16px 'Myriad Pro'");
        this.labelR35.lineHeight = 19;
        this.labelR35.setTransform(264, 260);

        this.labelR41 = new cjs.Text("3 – 0", "16px 'Myriad Pro'");
        this.labelR41.lineHeight = 19;
        this.labelR41.setTransform(350.5, 77);
        this.labelR42 = new cjs.Text("4 – 1", "16px 'Myriad Pro'");
        this.labelR42.lineHeight = 19;
        this.labelR42.setTransform(350.5, 123);
        this.labelR43 = new cjs.Text("3 – 1", "16px 'Myriad Pro'");
        this.labelR43.lineHeight = 19;
        this.labelR43.setTransform(350.5, 169);
        this.labelR44 = new cjs.Text("5 – 2", "16px 'Myriad Pro'");
        this.labelR44.lineHeight = 19;
        this.labelR44.setTransform(350.5, 215);
        this.labelR45 = new cjs.Text("1 + 2", "16px 'Myriad Pro'");
        this.labelR45.lineHeight = 19;
        this.labelR45.setTransform(350.5, 260);

        this.labelR51 = new cjs.Text("6 – 4", "16px 'Myriad Pro'");
        this.labelR51.lineHeight = 19;
        this.labelR51.setTransform(440, 77);
        this.labelR52 = new cjs.Text("5 – 2", "16px 'Myriad Pro'");
        this.labelR52.lineHeight = 19;
        this.labelR52.setTransform(440, 123);
        this.labelR53 = new cjs.Text("6 – 3", "16px 'Myriad Pro'");
        this.labelR53.lineHeight = 19;
        this.labelR53.setTransform(440, 169);
        this.labelR54 = new cjs.Text("2 + 1", "16px 'Myriad Pro'");
        this.labelR54.lineHeight = 19;
        this.labelR54.setTransform(440, 215);
        this.labelR55 = new cjs.Text("3 + 1", "16px 'Myriad Pro'");
        this.labelR55.lineHeight = 19;
        this.labelR55.setTransform(440, 260);

        this.instance = new lib.p64_1();
        this.instance.setTransform(25, 25, 0.38, 0.38);

        this.addChild(this.instance, this.img_Rect1, this.img_Rect2, this.img_Rect3,
            this.img_Rect4, this.img_Rect5, this.img_Rect6, this.text_1, this.text_2, this.textBoxGroup1, this.textBoxGroup2,
            this.textBoxGroup3, this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6,
            this.labelR11, this.labelR12, this.labelR13, this.labelR14,
            this.labelR15, this.labelR21, this.labelR22, this.labelR23, this.labelR24, this.labelR25, this.labelR31, this.labelR32,
            this.labelR33, this.labelR34, this.labelR35, this.labelR41, this.labelR42, this.labelR43, this.labelR44, this.labelR45,
            this.labelR51, this.labelR52, this.labelR53, this.labelR54, this.labelR55);
        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 300);

    (lib.p64 = function() {
        this.initialize();
        this.v1 = new lib.Symbol3();
        this.v1.setTransform(284.8, 0, 1, 1, 0, 0, 0, 253.3, -20);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(284.8, 292, 1, 1, 0, 0, 0, 253.3, -20);

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 319, 1, 1, 0, 0, 0, 609.5, 338.7);


        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
