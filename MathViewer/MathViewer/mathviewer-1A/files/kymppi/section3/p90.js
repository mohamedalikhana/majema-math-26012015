(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p90_1.png",
            id: "p90_1"
        }, {
            src: "images/p90_2.png",
            id: "p90_2"
        }, {
            src: "images/p90_3.png",
            id: "p90_3"
        }, {
            src: "images/p90_4.png",
            id: "p90_4"
        }, {
            src: "images/p90_5.png",
            id: "p90_5"
        }, {
            src: "images/p90_6.png",
            id: "p90_6"
        }, {
            src: "images/p90_7.png",
            id: "p90_7"
        }, {
            src: "images/p90_8.png",
            id: "p90_8"
        }]
    };

    (lib.p90_1 = function() {
        this.initialize(img.p90_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_2 = function() {
        this.initialize(img.p90_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_3 = function() {
        this.initialize(img.p90_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_4 = function() {
        this.initialize(img.p90_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_5 = function() {
        this.initialize(img.p90_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_6 = function() {
        this.initialize(img.p90_6);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_7 = function() {
        this.initialize(img.p90_7);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_8 = function() {
        this.initialize(img.p90_8);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med subtraktion 0 till 10", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(70, 651);

        this.text_2 = new cjs.Text("90", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(38, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31, 660.8);

        this.text_3 = new cjs.Text("Subtrahera med tre tal", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(95.5, 25);

        this.text_4 = new cjs.Text("31", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(44, 22.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 40;
        this.text.setTransform(63, 108);

        this.text_1 = new cjs.Text("–", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(85, 108);

        this.text_2 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(105, 108);

        this.text_3 = new cjs.Text("–", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(128, 108);

        this.text_4 = new cjs.Text("3", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(148, 108);

        this.text_5 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(171, 108);

        this.text_6 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 40;
        this.text_6.setTransform(192, 108);


        this.text_7 = new cjs.Text("8", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 40;
        this.text_7.setTransform(322, 108);

        this.text_8 = new cjs.Text("–", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 40;
        this.text_8.setTransform(344, 108);

        this.text_9 = new cjs.Text("4", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 40;
        this.text_9.setTransform(364, 108);

        this.text_10 = new cjs.Text("–", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 40;
        this.text_10.setTransform(387, 108);

        this.text_11 = new cjs.Text("3", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_11.lineHeight = 40;
        this.text_11.setTransform(407, 108);

        this.text_12 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_12.lineHeight = 40;
        this.text_12.setTransform(430, 108);

        this.text_13 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_13.lineHeight = 40;
        this.text_13.setTransform(450, 108);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) { // green ball
                this.shape_group1.graphics.f("#20B14A").s("#6E6E70").ss(0.5, 0, 0, 4).arc(62 + (columnSpace * 24), 97 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) { // green ball
                this.shape_group2.graphics.f("#20B14A").s("#6E6E70").ss(0.5, 0, 0, 4).arc(306 + (columnSpace * 24), 97 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) { // green ball
                this.shape_group3.graphics.f("#20B14A").s("#6E6E70").ss(0.5, 0, 0, 4).arc(197 + (columnSpace * 24), 97 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group3.setTransform(0, 0);

        this.shape_group4 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) { // green ball
                this.shape_group4.graphics.f("#20B14A").s("#6E6E70").ss(0.5, 0, 0, 4).arc(436 + (columnSpace * 24), 97 + (row * 23), 10, 0, 2 * Math.PI);
            }
        }
        this.shape_group4.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['118'];
        var xPos = 37,
            padding,
            yPos,
            rectWidth = 149.8,
            rectHeight = 23,
            boxWidth = 21.4,
            boxHeight = 23,
            numberOfBoxes = 7,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 25;
                if (i == 1) {
                    padding = padding + 42;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);


        // Block-1
        // this.shape_123 = new cjs.Shape();
        // this.shape_123.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        // this.shape_123.setTransform(245.9, 61.4);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_122.setTransform(240.9, 61.4);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_121.setTransform(224.9, 61.4);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_120.setTransform(208.9, 61.4);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_119.setTransform(192.9, 61.4);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(176.9, 61.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_37.setTransform(162.1, 61.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(146.9, 61.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_39.setTransform(131.3, 61.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(115.9, 61.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(100.2, 61.4);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(84.2, 61.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(68.7, 61.4);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(53.2, 61.4);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(35.6, 61.4);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_63.setTransform(36.2, 67.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_64.setTransform(36.2, 67.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_59.setTransform(53, 67.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_60.setTransform(53, 67.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_51.setTransform(68, 67.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_52.setTransform(68, 67.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_55.setTransform(84, 67.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_56.setTransform(84, 67.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_65.setTransform(100, 67.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_66.setTransform(100, 67.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_61.setTransform(115.5, 67.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_62.setTransform(115.5, 67.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_57.setTransform(131, 67.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_58.setTransform(131, 67.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_49.setTransform(147, 67.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_50.setTransform(147, 67.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_53.setTransform(162, 67.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_54.setTransform(162, 67.9);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_47.setTransform(177, 67.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_48.setTransform(177, 67.9);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_124.setTransform(193.5, 67.9);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_125.setTransform(193.5, 67.9);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_126.setTransform(209, 67.9);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_127.setTransform(209, 67.9);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_128.setTransform(225, 67.9);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_129.setTransform(225, 67.9);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_130.setTransform(241, 67.9);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_131.setTransform(241, 67.9);

        // this.shape_132 = new cjs.Shape();
        // this.shape_132.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        // this.shape_132.setTransform(247, 67.9);

        // this.shape_133 = new cjs.Shape();
        // this.shape_133.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        // this.shape_133.setTransform(247, 67.9);

        // Block-2
        // this.shape_136 = new cjs.Shape();
        // this.shape_136.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        // this.shape_136.setTransform(502, 61.4);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_135.setTransform(496, 61.4);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_134.setTransform(480, 61.4);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_113.setTransform(464.5, 61.4);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_114.setTransform(448.7, 61.4);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_70.setTransform(433.9, 61.4);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_71.setTransform(417.1, 61.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_72.setTransform(402.9, 61.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_73.setTransform(387.3, 61.4);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_74.setTransform(371.9, 61.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_75.setTransform(356.2, 61.4);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(340.2, 61.4);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(324.7, 61.4);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(309.2, 61.4);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(290.6, 61.4);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(291.2, 67.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(291.2, 67.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(309.6, 67.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(309.6, 67.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(325.3, 67.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(325.3, 67.9);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_84.setTransform(340.5, 67.9);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_85.setTransform(340.5, 67.9);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_98.setTransform(356.5, 67.9);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_99.setTransform(356.5, 67.9);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_94.setTransform(372.2, 67.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_95.setTransform(372.2, 67.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_90.setTransform(387.7, 67.9);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_91.setTransform(387.7, 67.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_86.setTransform(403.4, 67.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_87.setTransform(403.4, 67.9);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_82.setTransform(417.5, 67.9);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_83.setTransform(417.5, 67.9);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_80.setTransform(433.2, 67.9);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_81.setTransform(433.2, 67.9);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_111.setTransform(448.5, 67.9);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_112.setTransform(448.5, 67.9);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_116.setTransform(464.5, 67.9);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_117.setTransform(464.5, 67.9);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_137.setTransform(480.5, 67.9);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_138.setTransform(480.5, 67.9);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_139.setTransform(496.5, 67.9);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_140.setTransform(496.5, 67.9);

        // this.shape_141 = new cjs.Shape();
        // this.shape_141.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        // this.shape_141.setTransform(502.5, 67.9);

        // this.shape_142 = new cjs.Shape();
        // this.shape_142.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        // this.shape_142.setTransform(502.5, 67.9);

        // Inner White block
        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(27, 60, 225, 91, 5);
        this.shape_100.setTransform(0, 0);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(282, 60, 225, 91, 5);
        this.shape_118.setTransform(0, 0);

        // Main yellow block

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFF679").s("#959C9D").ss(1).drawRoundRect(5, 0, 523, 164, 10);
        this.shape_115.setTransform(0, 0);

        this.label_1 = new cjs.Text("Subtrahera först med 2", "16px 'Myriad Pro'");
        this.label_1.lineHeight = 19;
        this.label_1.setTransform(35, 8);

        this.label_2 = new cjs.Text("Subtrahera först med 4", "16px 'Myriad Pro'");
        this.label_2.lineHeight = 19;
        this.label_2.setTransform(294, 8);

        this.label_3 = new cjs.Text("och sedan med 3.", "16px 'Myriad Pro'");
        this.label_3.lineHeight = 19;
        this.label_3.setTransform(35, 28);

        this.label_4 = new cjs.Text("och sedan med 3.", "16px 'Myriad Pro'");
        this.label_4.lineHeight = 19;
        this.label_4.setTransform(294, 28);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#6E6E70").setStrokeStyle(2).moveTo(105, 85).lineTo(116, 110).moveTo(129, 85).lineTo(140, 110).moveTo(152, 85).lineTo(163, 110)
            .moveTo(325, 85).lineTo(336, 110).moveTo(349, 85).lineTo(360, 110).moveTo(373, 85).lineTo(384, 110);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#6E6E70").setStrokeStyle(2).moveTo(202, 85).lineTo(191, 110).moveTo(226, 85).lineTo(215, 110).moveTo(407, 85).lineTo(396, 110)
            .moveTo(441, 85).lineTo(430, 110).moveTo(465, 85).lineTo(454, 110).moveTo(489, 85).lineTo(478, 110);
        this.Line_2.setTransform(0, 0);

        this.addChild(this.shape_115, this.shape_100, this.shape_118, this.shape_112, this.shape_111, this.shape_117, this.shape_116, this.shape_113, this.shape_114);
        this.addChild(this.shape_137, this.shape_138, this.shape_139, this.shape_140, this.shape_141, this.shape_142);
        this.addChild(this.shape_128, this.shape_129, this.shape_130, this.shape_131, this.shape_132, this.shape_133, this.shape_134, this.shape_135, this.shape_136);
        this.addChild(this.shape_124, this.shape_125, this.shape_126, this.shape_127, this.shape_119, this.shape_120, this.shape_121, this.shape_122, this.shape_123);

        this.addChild(this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text);
        this.addChild(this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.textbox_group1, this.text_6, this.text_7, this.text_8, this.text_9);
        this.addChild(this.text_10, this.text_11, this.text_12, this.text_13);
        this.addChild(this.label_1, this.label_2, this.label_3, this.label_4, this.Line_1, this.Line_2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 16);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 16);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 259, 95, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(264, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 125);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(264, 125);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 225);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(264, 225);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 325);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(264, 325);

        this.text = new cjs.Text("6  –  3  –  1  =", "16.2px 'Myriad Pro'");
        this.text.lineHeight = 20;
        this.text.setTransform(57, 106);

        this.text_1 = new cjs.Text("6  –  2  –  1  =", "16.2px 'Myriad Pro'");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(319, 106);

        this.text_2 = new cjs.Text("7  –  2  –  1  =", "16.2px 'Myriad Pro'");
        this.text_2.lineHeight = 20;
        this.text_2.setTransform(57, 206);

        this.text_3 = new cjs.Text("7  –  3  –  1  =", "16.2px 'Myriad Pro'");
        this.text_3.lineHeight = 20;
        this.text_3.setTransform(319, 206);

        this.text_4 = new cjs.Text("7  –  2  –  2  =", "16.2px 'Myriad Pro'");
        this.text_4.lineHeight = 20;
        this.text_4.setTransform(57, 306);

        this.text_5 = new cjs.Text("7  –  3  –  3  =", "16.2px 'Myriad Pro'");
        this.text_5.lineHeight = 20;
        this.text_5.setTransform(319, 306);

        this.text_6 = new cjs.Text("8  –  4  –  2  =", "16.2px 'Myriad Pro'");
        this.text_6.lineHeight = 20;
        this.text_6.setTransform(57, 406);

        this.text_7 = new cjs.Text("8  –  4  –  3  =", "16.2px 'Myriad Pro'");
        this.text_7.lineHeight = 20;
        this.text_7.setTransform(319, 406);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(152 + (columnSpace * 262), 102 + (row * 100), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.instance_1 = new lib.p90_1();
        this.instance_1.setTransform(35, 51, 0.378, 0.39);

        this.instance_2 = new lib.p90_2();
        this.instance_2.setTransform(301, 49, 0.378, 0.39);

        this.instance_3 = new lib.p90_3();
        this.instance_3.setTransform(35, 144, 0.378, 0.39);

        this.instance_4 = new lib.p90_4();
        this.instance_4.setTransform(295, 144, 0.378, 0.39);

        this.instance_5 = new lib.p90_5();
        this.instance_5.setTransform(19, 248, 0.378, 0.39);

        this.instance_6 = new lib.p90_6();
        this.instance_6.setTransform(295, 250, 0.378, 0.39);

        this.instance_7 = new lib.p90_7();
        this.instance_7.setTransform(29, 346, 0.378, 0.39);

        this.instance_8 = new lib.p90_8();
        this.instance_8.setTransform(285, 343, 0.378, 0.39);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.text, this.text_1);
        this.addChild(this.text_q1, this.text_q2, this.textbox_group1);
        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7);
        this.addChild(this.instance_1, this.instance_2, this.instance_3, this.instance_4, this.instance_5, this.instance_6, this.instance_7, this.instance_8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(288, 106, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(293, 264, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
