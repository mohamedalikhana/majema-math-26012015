var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }, {
            src: "../viovar/images/bat.png",
            id: "bat"
        }]
    };

    lib.init_Magic_Wand();

    var iconProperties = {
        x: 850 + 370,
        y: 360,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 150,
        wrongY: 345 - 15
    };


    (lib.bat = function() {
        this.initialize(img.bat);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.tick = function() {
        this.initialize(img.tick);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);

    // (lib.tick = function() {
    //     this.initialize();
    //     var tickMark = new createjs.Shape();
    //     tickMark.graphics.f('#61df7f').ss(4).moveTo(77, 5).quadraticCurveTo(82, 10, 84, 10).quadraticCurveTo(78, 5, 25.79, 147.32).quadraticCurveTo(12, 152, 10.5, 152.84).quadraticCurveTo(12, 152, 77, 5).moveTo(15.26, 66.03).drawCircle(15.26, 66.03, 10).moveTo(72.89, 85).drawCircle(72.89, 85, 10);
    //     tickMark.setTransform(0, 0, 2, 2);
    //     tickMark.alpha = 1;
    //     this.addChild(tickMark);
    // }).prototype = p = new cjs.Container();

    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // code for drag & drop starts here

    var heart = getHeart();

    var spring = new createjs.Shape();
    spring.graphics.s().ss(0).beginRadialGradientFill(["#ffffff", "#ADADAE"], [0, 1], 146, 72 - 49, 2, 146, 72 - 49, 6).drawCircle(146, 72 - 49, 6).f().s('#7d7d7d').ss(2.7, "round").moveTo(139, 12).bezierCurveTo(145.2, 0.6, 160, 12, 144, 20);
    var paddingBox = {
        left: 280,
        right: 130,
        top: 0,
        bottom: 60
    };

    var dropBoxSpacing = {
        width: 40,
        height: 0
    };

    var sectionSeperator = {
        width: 50,
        height: 0
    };


    var dropBox = {
        width: 215,
        height: 250
    };

    var dropBox1 = {
        x: paddingBox.left,
        y: paddingBox.top,
        width: dropBox.width,
        height: dropBox.height
    };

    var dropBox2 = {
        x: paddingBox.left + dropBoxSpacing.width + dropBox.width,
        y: paddingBox.top,
        width: dropBox.width,
        height: dropBox.height
    };


    var cellSpacing = {
        width: 4,
        height: 6,
        cellSectionSpace: 16
    };

    var cell = {
        x: paddingBox.left + dropBox.width + dropBoxSpacing.width + dropBox.width + sectionSeperator.width,
        y: paddingBox.top,
        width: 55,
        height: 55
    };
    var additionalCell = {
        x: cell.x + cell.width + cellSpacing.width * 2,
        y: cell.y + (cell.height + cellSpacing.height) * 4,
        width: 55,
        height: 55
    };
    var heartBig = {
        height: 110,
        width: 120
    };
    var heartSmall = {
        height: 26,
        width: 24
    };



    var springHole = {
        height: 10,
        width: 10
    };

    var draggableNumber = {
        x: paddingBox.left,
        y: 380,
        width: 75,
        height: 90
    };
    var draggableNumberSpacing = {
        width: 10,
        height: 0
    };

    function getHeart() {
        var heart = new createjs.Container();
        var heartShape = new createjs.Shape();
        heartShape.graphics.s('#aa0002').ss(4).beginRadialGradientFill(["#ffffff", "#ffc3c5"], [0, 1], 75, 75, 0, 75, 75, 50).bezierCurveTo(75, 37, 70, 25, 50, 25).bezierCurveTo(20, 25, 20, 62.5, 20, 62.5).bezierCurveTo(20, 80, 40, 102, 75, 120).bezierCurveTo(110, 102, 130, 80, 130, 62.5).bezierCurveTo(130, 62.5, 130, 25, 100, 25).bezierCurveTo(85, 25, 75, 37, 75, 40);
        var heartText = new createjs.Text("10", "bold 40px 'Myriad Pro'", "#000000");

        heartText.x = 55, heartText.y = 56;
        heart.addChild(heartShape, heartText);
        return heart;
    }

    var isDragging = false;
    var draggingNumber = null;

    function addCells(currentStage) {
        for (var section = 0; section < sections; section++) {
            for (var cellBlock = 0; cellBlock < cellBlocks; cellBlock++) {

                for (var row = 0; row < rows; row++) {
                    var x = (cell.x + (cell.width * cellBlock) + (cellSpacing.width * cellBlock)) + ((cell.width * cellBlocks) * section + cellSpacing.cellSectionSpace * section);
                    var y = cell.y + (cell.height * row) + (cellSpacing.height * row);


                    var tempCell = new createjs.Shape()
                    tempCell.graphics.f('#ffffff').s('#7d7d7d').drawRoundRect(x, y, cell.width, cell.height, 5);

                    var tempCellText = new createjs.Text("", "30px 'Myriad Pro'", "#000000");
                    tempCellText.textAlign = "center";
                    tempCellText.x = x + 27;
                    tempCellText.y = y + 16;
                    currentStage.addChild(tempCell, tempCellText);

                    if (cellBlock == 1) {
                        var heartSmall1 = heart.clone(true);
                        heartSmall1.setTransform((cell.x + (cell.width * cellBlock) + (cellSpacing.width * cellBlock)) + ((cell.width * cellBlocks) * section + cellSpacing.cellSectionSpace * section) - 18, cell.y + (cell.height * row) + (cellSpacing.height * row) + 32, heartSmall.width / 116, heartSmall.height / 110);
                        currentStage.addChild(heartSmall1);
                    }
                    if (cellBlock === 0) {
                        cellsLeft.push(tempCellText);
                    } else {
                        cellsRight.push(tempCellText);
                    }
                }

            }
            if (section == 0) {
                addAdditionalCells(currentStage);
            }
        }
    }

    function addAdditionalCells(currentStage) {
        var sections = 1,
            cellBlocks = 2,
            rows = 1;

        for (var section = 0; section < sections; section++) {
            for (var cellBlock = 0; cellBlock < cellBlocks; cellBlock++) {
                for (var row = 0; row < rows; row++) {
                    var x = (additionalCell.x + (additionalCell.width * cellBlock) + (cellSpacing.width * cellBlock)) + ((additionalCell.width * cellBlocks) * section + cellSpacing.cellSectionSpace * section);
                    var y = additionalCell.y + (additionalCell.height * row) + (cellSpacing.height * row);
                    var tempCell = new createjs.Shape()
                    tempCell.graphics.f('#ffffff').s('#7d7d7d').drawRoundRect(x, y, additionalCell.width, additionalCell.height, 5);

                    var tempCellText = new createjs.Text("", "30px 'Myriad Pro'", "#000000");
                    tempCellText.textAlign = "center";
                    tempCellText.x = x + 27;
                    tempCellText.y = y + 16;
                    currentStage.addChild(tempCell, tempCellText);
                    if (cellBlock == 1) {
                        var heartSmall1 = heart.clone(true);
                        heartSmall1.setTransform((additionalCell.x + (additionalCell.width * cellBlock) + (cellSpacing.width * cellBlock)) + ((additionalCell.width * cellBlocks) * section + cellSpacing.cellSectionSpace * section) - 18, additionalCell.y + (additionalCell.height * row) + (cellSpacing.height * row) + 32, heartSmall.width / 116, heartSmall.height / 110);
                        currentStage.addChild(heartSmall1);
                    }
                    if (cellBlock === 0) {
                        cellsLeft.push(tempCellText);
                    } else {
                        cellsRight.push(tempCellText);
                    }
                }
            }

        }
    }

    function handlePress(evt) {
        if (evt.currentTarget && evt.currentTarget.value) {
            var dragger = new createjs.Container();
            dragger.value = evt.currentTarget.value;
            var tempCell = new createjs.Shape()
            tempCell.graphics.s('#7d7d7d').beginLinearGradientFill(["#fff880", "#FFFFFF"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);

            var tempCellText = new createjs.Text(dragger.value, "bold 64px 'Myriad Pro'", "#000000");
            tempCellText.textAlign = "center";

            tempCellText.x = draggableNumber.width / 2;
            tempCellText.y = draggableNumber.height / 5;

            dragger.addChild(tempCell);
            dragger.addChild(tempCellText);

            dragger.scaleX = dragger.scaleY = currentStage.scaleX;

            stage.addChild(dragger);
            stage.draggingShape = dragger;
            // stage.draggingShape.x = stage.mouseX - (draggableNumber.width / 2), stage.draggingShape.y = stage.mouseY - (draggableNumber.height / 2);
            var pt = stage.globalToLocal(stage.mouseX, stage.mouseY);
            stage.draggingShape.x = pt.x - (draggableNumber.width / 2),
                stage.draggingShape.y = pt.y - (draggableNumber.height / 2);
            isDragging = true;
            stage.addEventListener("stagemousemove", dragging);
            stage.addEventListener("stagemouseup", dropping);

        }
        stage.update();
    }

    function handleOver(evt) {
        if (evt.currentTarget && evt.currentTarget.value) {
            var dragger = evt.currentTarget;
            dragger.value = evt.currentTarget.value;

            dragger.children[0].graphics.clear().s('#7d7d7d').beginLinearGradientFill(["#fff880", "#FFFFFF"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);

        }

    }

    function handleOut(evt) {
        if (evt.currentTarget && evt.currentTarget.value) {
            var dragger = evt.currentTarget;
            dragger.value = evt.currentTarget.value;

            dragger.children[0].graphics.clear().s('#7d7d7d').beginLinearGradientFill(["#FFFFFF", "#fff880"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);

        }

    }

    function dragging(event) {
        if (isDragging) {
            var isOver = false,
                targets = stage.getObjectsUnderPoint(stage.mouseX, stage.mouseY);
            if (stage.draggingShape) {
                stage.draggingShape.children[0].shadow = new createjs.Shadow('#7d7d7d', 0, 0.5, 4);

                // stage.draggingShape.x = currentStage.mouseX - (draggableNumber.width / 2), currentStage.draggingShape.y = currentStage.mouseY - (draggableNumber.height / 2);
                var pt = stage.globalToLocal(stage.mouseX, stage.mouseY);
                stage.draggingShape.x = pt.x - (draggableNumber.width / 2),
                    stage.draggingShape.y = pt.y - (draggableNumber.height / 2);
                stage.update();
            }
        }

    }

    function dropping(event) {
        if (isDragging) {
            isDragging = false;
            var dropArea = null;
            var dropAreaText = currentStage.dropBoxText1;
            var boxNumber = 0;

            if (stage.mouseInBounds) {
                for (var i = 0; i < dropBoxes.length; i++) {
                    if (dropBoxes[i].isHover) {
                        dropArea = dropBoxes[i];
                        dropAreaText = dropBoxes[i].dropBoxText;
                        boxNumber = dropBoxes[i].value;
                        break;
                    }
                };
            }

            if (dropArea) {
                stage.draggingShape.x = dropArea.x;

                stage.draggingShape.y = dropArea.y;
                dropAreaText.text = stage.draggingShape.value;
                var isChanged = false;

                var leftValue = parseInt(currentStage.dropBoxText1.text);
                var rightValue = parseInt(currentStage.dropBoxText2.text);
                if (leftValue && rightValue && leftValue + rightValue == 10) {
                    cells[leftValue - 1].left.text = leftValue;
                    cells[leftValue - 1].right.text = rightValue;
                    cells[leftValue - 1].solved = true;
                    currentStage.correctIcon.visible = true;
                    createjs.Tween.get(currentStage.correctIcon).to({
                        alpha: 1
                    }, 500).wait(500).to({
                        alpha: 0
                    }, 800).call(function() {
                        if (isAllCellsSolved() === true) {
                            resetAll();
                        }
                        currentStage.dropBoxText1.text = "";
                        currentStage.dropBoxText2.text = "";
                    });
                    isChanged = true;

                } else if ((leftValue && boxNumber == 2) || (rightValue && boxNumber == 1)) {
                    currentStage.incorrectIcon.visible = true;
                    createjs.Tween.get(currentStage.incorrectIcon).to({
                        alpha: 1
                    }, 500).wait(500).to({
                        alpha: 0
                    }, 3000);

                    dropAreaText.text = "";
                    isChanged = false;
                }

                var xPos = (boxNumber == 1) ? dropArea.x + 37 : dropArea.x - 24;
                if (isChanged === true) {
                    stage.draggingShape.x = dropArea.x + dropBox.width / 3,
                        stage.draggingShape.y = dropArea.y + dropBox.height / 3;

                    createjs.Tween.get(stage.draggingShape).to({
                        x: xPos,
                        y: dropArea.y + 138,
                        scaleX: 2.1,
                        scaleY: 2.1
                    }, 300).wait(300).call(function() {
                        stage.removeChild(stage.draggingShape);
                        stage.draggingShape = null;
                    });
                } else {
                    stage.draggingShape.x = dropArea.x + dropBox.width / 3,
                        stage.draggingShape.y = dropArea.y + dropBox.height / 3;

                    createjs.Tween.get(stage.draggingShape).to({
                        x: xPos,
                        y: dropArea.y + 138,
                        scaleX: 2.1,
                        scaleY: 2.1
                    }, 300).wait(300).call(function() {
                        stage.removeChild(stage.draggingShape);
                        stage.draggingShape = null;
                    });
                }
                stage.update();


                var isAllBoxesHavingValue = true;
                for (var i = 0; i < dropBoxes.length; i++) {
                    if (!dropBoxes[i].solved) {
                        isAllBoxesHavingValue = false;
                    }
                }
                if (isAllBoxesHavingValue) {
                    $(btn_newExercise).hide();
                    $(btn_newExercise).addClass('disabled');
                    $(btn_answers).show();
                    $(btn_answers).removeClass('disabled');

                }

            } else {
                stage.removeChild(stage.draggingShape);
                stage.draggingShape = null;


            }
        }
        stage.update();
    }

    function isAllCellsSolved() {
        for (var i = 0; i < cells.length; i++) {
            if (!cells[i].solved) {
                return false;
            }
        }
        return true;
    }

    function resetAll() {
        currentStage.removeChild(currentStage.draggingShape);
        currentStage.dropBoxText1.text = "";
        currentStage.dropBoxText2.text = "";
        //btnReset.visible=false;

        for (var i = 0; i < cells.length; i++) {
            cells[i].left.text = "";
            cells[i].right.text = "";
        }
        createjs.Tween.get(btnReset).to({
            alpha: 0
        }, 200);
    }

    function continueNext() {
        currentStage.removeChild(currentStage.draggingShape);
        currentStage.dropBoxText1.text = "";
        currentStage.dropBoxText2.text = "";
        createjs.Tween.get(btnContinue).to({
            alpha: 0
        }, 200);
    }

    function checkHover(object) {
        object.on("mouseover", function(e) {

            for (var i = 0; i < dropBoxes.length; i++) {
                dropBoxes[i].isHover = false;
            };
            e.currentTarget.isHover = true;
        });
        object.on("mouseout", function(e) {
            for (var i = 0; i < dropBoxes.length; i++) {
                dropBoxes[i].isHover = false;
            };
            e.currentTarget.isHover = false;
        });
        object.on("mousedown", function(e) {
            for (var i = 0; i < dropBoxes.length; i++) {
                dropBoxes[i].isHover = false;
            };
            e.currentTarget.isHover = true;
        });
    }

    var sections = 2,
        cellBlocks = 2,
        rows = 4;

    var cells = [];
    var cellsLeft = [];
    var cellsRight = [];


    var currentStage;
    var dropBoxes = [];
    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;
        this.questionText = new cjs.Text("Hitta två tal som är tiokamrater.", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -100);

        this.hintText1 = new cjs.Text("Dra talen som passar till rutorna.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.setTransform(660, 530);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        //this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.addChild(this.questionText, this.hintText1, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        var startX = 375;
        var startY = 125;

        this.circles = [];
        var circleCount = 6;
        var startX = 45;
        thisStage.currentAnswer = 0;
        var dropBoxesProp = {
                x: 320,
                y: 80
            }
            // logic for vivoar drag & drop exercise starts here

        this.dropBox1 = new createjs.Container();
        this.dropBox1.x = dropBox1.x, this.dropBox1.y = dropBox1.y;
        this.dropBox1.value = 1;
        checkHover(this.dropBox1)
        this.dropBox2 = new createjs.Container();
        this.dropBox2.x = dropBox2.x, this.dropBox2.y = dropBox2.y;
        this.dropBox2.value = 2;
        checkHover(this.dropBox2)

        dropBoxes.push(this.dropBox1)
        dropBoxes.push(this.dropBox2)

        this.box1 = new createjs.Shape()
        this.box1.graphics.f('#ffffff').s('#7d7d7d').drawRoundRect(0, 0, dropBox.width, dropBox.height, 5);
        this.dropBox1.addChild(this.box1);

        this.box2 = new createjs.Shape()
        this.box2.graphics.f('#ffffff').s('#7d7d7d').drawRoundRect(0, 0, dropBox.width, dropBox.height, 5);
        this.dropBox2.addChild(this.box2);

        this.dropBoxText1 = new createjs.Text(" ", "bold 200px 'Myriad Pro'", "#000000");
        this.dropBoxText1.x = 40;
        this.dropBoxText1.y = 40;
        this.dropBox1.addChild(this.dropBoxText1);

        this.dropBoxText2 = new createjs.Text(" ", "bold 200px 'Myriad Pro'", "#000000");
        this.dropBoxText2.textAlign = "center";
        this.dropBoxText2.x = 100;
        this.dropBoxText2.y = 40;
        this.dropBox1.dropBoxText = this.dropBoxText1;
        this.dropBox2.dropBoxText = this.dropBoxText2;

        var heartBig1 = heart.clone(true);
        heartBig1.setTransform(dropBox1.x + dropBox.width / 1.37, dropBox1.y + dropBox.height / 1.37, heartBig.width / 116, heartBig.height / 110)

        this.dropBox2.addChild(this.dropBoxText2);
        var dropBoxText1 = this.dropBoxText1;
        var dropBoxText2 = this.dropBoxText2;
        for (var i = 0; i < (dropBox.width / 24); i++) {
            var springCloned = spring.clone(true);
            springCloned.setTransform(-132 + i * 24, -12)
            this.dropBox1.addChild(springCloned);
            this.dropBox2.addChild(springCloned.clone());

        }
        this.addChild(this.dropBox1, this.dropBox2, heartBig1)

        var button = new createjs.Container();
        var buttonShape = new createjs.Shape()
        buttonShape.graphics.s('#7d7d7d').beginLinearGradientFill(["#FFFFFF", "#000000"], [0, 1], 0, 10, 0, 90).drawRoundRect(0, 0, 150, 40, 5);
        buttonShape.shadow = new createjs.Shadow("#000000", 0, 2, 5);
        var buttonResetText = new createjs.Text("Försök igen", "bold 25px 'Myriad Pro'", "#000000");
        buttonResetText.textAlign = "center";
        buttonResetText.x = 75;
        buttonResetText.y = 6 + 5;
        button.addChild(buttonShape, buttonResetText);
        var btnReset = button.clone(true);
        btnReset.setTransform(850, 575);
        var btnContinue = button.clone(true);
        btnContinue.setTransform(850, 575);
        btnContinue.children[1].text = "fortsätta";
        this.addChild(btnContinue, btnReset);
        btnContinue.alpha = 0;
        btnReset.alpha = 0;

        btnReset.on("mousedown", function() {
            btnReset.children[0].shadow = new createjs.Shadow("#000000", 0, 2, 5);
            btnReset.children[0].graphics.s('#7d7d7d').beginLinearGradientFill(["#000000", "#FFFFFF"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, 150, 40, 5);
            btnReset.children[1].color = "#ffffff";
            resetAll();
        });
        btnReset.on("mouseout", function() {
            btnReset.children[0].shadow = new createjs.Shadow("#000000", 0, 2, 5);
            btnReset.children[0].graphics.s('#7d7d7d').beginLinearGradientFill(["#FFFFFF", "#000000"], [0, 1], 0, 10, 0, 90).drawRoundRect(0, 0, 150, 40, 5);
            btnReset.children[1].color = "#000000";
        });
        btnContinue.on("mousedown", function() {
            btnContinue.children[0].shadow = new createjs.Shadow("#000000", 0, 2, 5);
            btnContinue.children[0].graphics.s('#7d7d7d').beginLinearGradientFill(["#000000", "#FFFFFF"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, 150, 40, 5);
            btnContinue.children[1].color = "#ffffff";
            continueNext();
        });
        btnContinue.on("mouseout", function() {
            btnContinue.children[0].shadow = new createjs.Shadow("#000000", 0, 2, 5);
            btnContinue.children[0].graphics.s('#7d7d7d').beginLinearGradientFill(["#FFFFFF", "#000000"], [0, 1], 0, 10, 0, 90).drawRoundRect(0, 0, 150, 40, 5);
            btnContinue.children[1].color = "#000000";
        });

        addCells(this);

        for (var i = 0; i < cellsLeft.length - rows; i++) {
            cells.push({
                "left": cellsLeft[i],
                "right": cellsRight[i]
            });
        };
        for (var i = cellsLeft.length - 1; i > cellsLeft.length - rows - 1; i--) {
            cells.push({
                "left": cellsLeft[i],
                "right": cellsRight[i]
            });
        };

        sections = 1,
            cellBlocks = 9,
            rows = 1;

        for (var section = 0; section < sections; section++) {
            for (var cellBlock = 0; cellBlock < cellBlocks; cellBlock++) {
                for (var row = 0; row < rows; row++) {

                    var dragger = new createjs.Container();

                    dragger.value = cellBlock + 1;

                    dragger.x = (draggableNumber.x + (draggableNumber.width * cellBlock) + (draggableNumberSpacing.width * cellBlock)) + ((draggableNumber.width * cellBlocks) * section + cellSpacing.cellSectionSpace * section);

                    dragger.y = draggableNumber.y + (draggableNumber.height * row) + (draggableNumberSpacing.height * row);

                    var tempCell = new createjs.Shape()
                    tempCell.graphics.s('#7d7d7d').beginLinearGradientFill(["#FFFFFF", "#fff880"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);

                    var tempCellText = new createjs.Text("" + dragger.value, "bold 64px 'Myriad Pro'", "#000000");
                    tempCellText.textAlign = "center";

                    tempCellText.x = draggableNumber.width / 2;
                    tempCellText.y = draggableNumber.height / 5;

                    dragger.addChild(tempCell);
                    dragger.addChild(tempCellText);

                    this.addChild(dragger);
                    dragger.on("mouseover", handleOver);
                    dragger.on("mouseout", handleOut);
                    dragger.on("mousedown", handlePress);

                }
            }

        }

        currentStage = this;
        var myStage = this;

        new lib.Magic_Wand(myStage, false);

        var showIncorrectAnswerTween = null;

        this.onCheckAnswer = function() {
            myStage.correctIcon.visible = true;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            var answer = isAllCellsSolved();
            if (answer) {
                currentAnswers[1] = myStage.answer = answer;
                $(btn_answers).hide();
                $(btn_answers).addClass('disabled');
                $(btn_newExercise).show();
                $(btn_newExercise).removeClass('disabled');
                if (exerciseCount == values.length) {
                    $(btn_newExercise).hide();
                    $(btn_newExercise).addClass('disabled');
                    $(btn_answers).hide();
                    $(btn_answers).removeClass('disabled');
                }
            } else {
                myStage.answer = false;
                currentAnswers[1] = 0;
            }

        };
        this.onIncorrectAnswer = function() {
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = false;
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talkamrater", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);

        this.text_1 = new cjs.Text("36", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(48, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)



        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
