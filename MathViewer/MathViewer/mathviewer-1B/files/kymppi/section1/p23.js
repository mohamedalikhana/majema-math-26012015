(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p23_1.png",
            id: "p23_1"
        }, {
            src: "images/p23_2.png",
            id: "p23_2"
        }]
    };

    (lib.p23_1 = function() {
        this.initialize(img.p23_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p23_2 = function() {
        this.initialize(img.p23_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);


    (lib.Symbol1 = function() {
        this.initialize();

        this.pageTitle = new cjs.Text("Textuppgifter", "24px 'MyriadPro-Semibold'", "#90BD22");
        this.pageTitle.setTransform(107, 42);

        this.text_1 = new cjs.Text("7", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(67, 42);

        this.text_2 = new cjs.Text("23", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(558, 658);

        this.pageFooterTextRight = new cjs.Text("ha fått arbeta med att lösa textuppgifter", "9px 'Myriad Pro'");
        this.pageFooterTextRight.setTransform(390, 659);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(48, 15, 1.10, 1.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.Symbolimg = new lib.Symbolimg();
        this.Symbolimg.setTransform(26, 15);

        this.addChild(this.pageTitle, this.pageFooterTextRight, this.shape_2, this.shape_1,
            this.shape, this.text, this.text_1, this.text_2, this.text_3, this.Symbolimg);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbolimg = function() {
        this.initialize();

        this.instance = new lib.p23_1();
        this.instance.setTransform(29, 35, 0.47, 0.47);

        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Hur många kronor är kvar?", "16px 'Myriad Pro'");
        this.text.setTransform(19, 22);

        this.text_1 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 22);

        this.instance = new lib.p23_2();
        this.instance.setTransform(35, 63, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(6, 11, 257, 158, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(6, 181, 257, 158, 10);
        this.roundRect2.setTransform(0, 20);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(267, 11, 258, 158, 10);
        this.roundRect3.setTransform(0, 25);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(267, 181, 258, 158, 10);
        this.roundRect4.setTransform(0, 20);


        this.text_2 = new cjs.Text("Sara har 10 kr.", "17px 'Myriad Pro'");
        this.text_2.setTransform(25, 58);

        this.text_11 = new cjs.Text("Hon köper en mössa.", "17px 'Myriad Pro'");
        this.text_11.setTransform(25, 80);

        this.text_3 = new cjs.Text("Manuel har 12 kr.", "16px 'Myriad Pro'");
        this.text_3.setTransform(285, 58);

        this.text_4 = new cjs.Text("Han köper en halsduk.", "16px 'Myriad Pro'");
        this.text_4.setTransform(285, 80);

        this.text_5 = new cjs.Text("Leo har 15 kr.", "16px 'Myriad Pro'");
        this.text_5.setTransform(25, 223);

        this.text_6 = new cjs.Text("Han köper vantar.", "16px 'Myriad Pro'");
        this.text_6.setTransform(25, 245);

        this.text_7 = new cjs.Text("Nelly har 17 kr.", "16px 'Myriad Pro'");
        this.text_7.setTransform(285, 223);

        this.text_10 = new cjs.Text("Hon köper sockor.", "16px 'Myriad Pro'");
        this.text_10.setTransform(285, 245);


        this.text_8 = new cjs.Text("7 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_8.setTransform(88, 145);
        this.text_8.skewX = -20;
        this.text_8.skewY = -20;

        this.text_9 = new cjs.Text("5 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_9.setTransform(60, 292);
        this.text_9.skewX = -20;
        this.text_9.skewY = -20;
       
        this.text_12 = new cjs.Text("10 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_12.setTransform(312, 119);
        this.text_12.skewX = 20;
        this.text_12.skewY = 20;

        this.text_13 = new cjs.Text("6 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_13.setTransform(340, 277);
        this.text_13.skewX = 20;
        this.text_13.skewY = 20;
     
        var RectArr = [];
        var RectX = [25, 285, 25, 285];
        var RectY = [51, 51, 215, 215];

        for (var row = 0; row < RectX.length; row++) {

            var Rect = new cjs.Shape();
            Rect.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(6, 110, 200, 23, 0);
            Rect.setTransform(RectX[row], RectY[row]);
            RectArr.push(Rect);

        }

        var lineArr = [];
        var lineX = [11, 31, 51, 71, 91, 111, 131, 151, 171, 11, 31, 51, 71, 91, 111, 131, 151, 171,
            271, 291, 311, 331, 351, 371, 391, 411, 431, 271, 291, 311, 331, 351, 371, 391, 411, 431
        ];
        var lineY = [161, 161, 161, 161, 161, 161, 161, 161, 161, 325, 325, 325, 325, 325, 325, 325, 325, 325,
            161, 161, 161, 161, 161, 161, 161, 161, 161, 325, 325, 325, 325, 325, 325, 325, 325, 325
        ];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 23);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.roundRect1,
            this.roundRect2, this.roundRect3, this.roundRect4, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7,
            this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13);

        for (var i = 0; i < RectArr.length; i++) {
            this.addChild(RectArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 510, 350);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(305, 279, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
