(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p147_1.png",
            id: "p147_1"
        }]
    };

    (lib.p147_1 = function() {
        this.initialize(img.p147_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("147", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#1A863A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(15, 7);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#1A863A");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text("  Hitta starttalet i hundrarutan.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.text_5 = new cjs.Text("  Följ instruktionen. Skriv bokstaven du kommer till.", "16px 'Myriad Pro'");
        this.text_5.setTransform(14, 20);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 533, 10);
        this.roundRect1.setTransform(0, 35);

        this.dog = new lib.p147_1();
        this.dog.setTransform(13, 85, 0.47, 0.47);

        this.text_2 = new cjs.Text("Starttalen finns", "14px 'Myriad Pro'");
        this.text_2.setTransform(74, 107);

        this.text_3 = new cjs.Text("i de gula rutorna.", "14px 'Myriad Pro'");
        this.text_3.setTransform(69, 125);

        var crosswordBox_group = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#FFFFFF").s("#1A863A").ss(1).drawRect(0, 0, 255, 255, 0);
        this.Rect1.setTransform(0, 0);

        crosswordBox_group.addChild(this.Rect1);
        crosswordBox_group.setTransform(240, 50);

        var specialTexts = [1, 2, 3, 'T', 5, 6, 'E', 8, 9, 10, 11, 12, 13, 'I', 15, 16, 17, 18, 19, 'L', 21, 'P', 23, 24, 25, 'O', 27, 28, 29, 30,
            31, 32, "E", 34, 35, "A", 37, 38, 'S', 40, 41, 'U', 43, 44, 45, 46, 47, 48, 49, "E", "R", 52, 53, "N", 55, 56, 57, "I", 59, "L",
            61, 62, 63, 64, "D", 66, "T", 68, 'P', 70, "D", 72, "N", 74, 'U', 76, 77, 78, 79, 80, 'U', 82, 83, 84, 85, "H", 87, 88, 89, 90,
            91, 'Ä', 93, 94, "E", 96, "O", 98, "N", 100
        ];

        var count = 0;
        var specialCount = 0;
        for (var column = 0; column < 10; column++) {
            for (var row = 0; row < 10; row++) {
                count = count + 1;
                var fillColor = '#ffffff';
                var fillText = count;
                fillText = specialTexts[specialCount];
                if (isNaN(fillText)) {
                    fillColor = '#CAD7BE';
                }
                specialCount = specialCount + 1;


                var rectContainer = new lib.FillBoxWithText("" + fillText, fillColor)
                rectContainer.setTransform(25.5 * row, 25.5 * column);
                crosswordBox_group.addChild(rectContainer);
            }
        }

        var hrlineX = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        var hrlineY = [25.5, 51, 76.5, 102, 127.5, 153, 178.5, 204, 229.5];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#1A863A").setStrokeStyle(1).moveTo(0, 0).lineTo(255, 0);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            crosswordBox_group.addChild(hrLine_1);
        }

        var vrlineX = [25.5, 51, 76.5, 102, 127.5, 153, 178.5, 204, 229.5];

        var vrlineY = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#1A863A").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 255);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            crosswordBox_group.addChild(vrLine_1);
        }

        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.text_5, this.roundRect1, this.dog, crosswordBox_group);


        // Container started

        this.text_4 = new cjs.Text("P", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_4.setTransform(24, 430);
        this.addChild(this.text_4);


        var wordBox_group = new cjs.Container();

        var firstColFillText = ['5', '3', '6', '9', '18', '100', '96', '94', '72', '34', '23'];

        var count = 0;
        for (var col = 0; col < 11; col++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#FFF374';
                var fillText = firstColFillText[count];
                var rectContainer = new lib.wordRectangle(fillText, fillColor)
                rectContainer.setTransform(36.8 * col, 140 * row);
                wordBox_group.addChild(rectContainer, this.verticalRectangle, this.horline, this.horline2, this.horline3);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group);

        var wordBox_group2 = new cjs.Container();

        var specialTexts = [];
        specialTexts = ['2', '2', '1', '1', '2', '1', '1', '2', '3', '3', '1'];
        var arrowPos = ['down', 'down', 'down', 'down', 'down', 'up', 'up', 'up', 'right', 'right', 'right']
        var count = 0;
        for (var column = 0; column < 11; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle2(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37 * column, 140 * row);
                wordBox_group2.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group2);


        var wordBox_group3 = new cjs.Container();

        specialTexts = ['3', '3', '4', '5', '1', '4', '5', '1', '1', '3', '3'];
        var arrowPos = ['left', 'right', 'right', 'left', 'right', 'left', 'left', 'left', 'up', 'up', 'down']
        var count = 0;
        for (var column = 0; column < 11; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle3(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37.5 * column, 140 * row);
                wordBox_group3.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group3);


        var wordBox_group4 = new cjs.Container();

        var firstColFillText = ['63', '24', '55', '37', '88', '100', '100', '94', '84', '62', '93', '68'];
        var count = 0;
        for (var col = 0; col < 12; col++) {
            for (var row = 0; row < 1; row++) {

                var startX = 0,
                    startY = 0;

                if (col < 2) {
                    startX = 0, startY = 0;
                }
                if (col > 1 && col < 8) {
                    startX = 19, startY = 0;
                }
                if (col > 7) {
                    startX = 39, startY = 0;
                }

                var fillColor = '#FFF374';
                var fillText = firstColFillText[count];
                var rectContainer = new lib.wordRectangle4(fillText, fillColor)
                rectContainer.setTransform(startX + 36.8 * col, 140 * row);
                wordBox_group4.addChild(rectContainer, this.verticalRectangle, this.horline, this.horline2, this.horline3);
                count = count + 1;
            }
        }
        this.addChild(wordBox_group4);

        var wordBox_group5 = new cjs.Container();

        var specialTexts = [];
        specialTexts = ['3', '3', '1', '2', '2', '3', '4', '2', '3', '3', '3', '3'];
        var arrowPos = ['down', 'down', 'right', 'down', 'right', 'left', 'up', 'up', 'up', 'down', 'up', 'down']
        var count = 0;
        for (var column = 0; column < 12; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle5(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37 * column, 140 * row);
                wordBox_group5.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group5);


        var wordBox_group6 = new cjs.Container();

        specialTexts = ['1', '3', '2', '3', '3', '3', '2', '3', '3', '3', '2', '1'];
        var arrowPos = ['left', 'left', 'up', 'right', 'up', 'up', 'left', 'left', 'left', 'right', 'right', 'left']
        var count = 0;
        for (var column = 0; column < 12; column++) {
            for (var row = 0; row < 1; row++) {

                var fillColor = '#ffffff';
                var fillText = specialTexts[count];
                var arrowPosition = arrowPos[count]
                var rectContainer = new lib.wordRectangle6(fillText, fillColor, arrowPosition, count)
                rectContainer.setTransform(37 * column, 140 * row);
                wordBox_group6.addChild(rectContainer);
                count = count + 1;
            }
        }

        this.addChild(wordBox_group6);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 560);


    (lib.wordRectangle = function(fillText, fillColor) {
        this.initialize();

        var vertRect_group = new cjs.Container();

        this.verticalRectangle = new cjs.Shape();
        this.verticalRectangle.graphics.f("").s('#7d7d7d').drawRect(0, 0, 31, 97.7);
        this.verticalRectangle.setTransform(0, 0);

        this.horline = new cjs.Shape();
        this.horline.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline.setTransform(0, 23);

        this.horline2 = new cjs.Shape();
        this.horline2.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline2.setTransform(0, 46);

        this.horline3 = new cjs.Shape();
        this.horline3.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline3.setTransform(0, 69);

        vertRect_group.addChild(this.verticalRectangle, this.horline, this.horline2, this.horline3);
        vertRect_group.setTransform(19, 334);

        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, (31 - 1), (92 / 4) - 1);
        rect.setTransform(19, 334);

        var Xpos = 0,
            Ypos = 0;
        if (fillText < 10) {
            Xpos = 28, Ypos = 351;
        }
        if (fillText > 9) {
            Xpos = 23, Ypos = 351;
        }
        if (fillText > 99) {
            Xpos = 18, Ypos = 351;
        }

        this.text = new cjs.Text(" " + fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(Xpos, Ypos);

        this.addChild(vertRect_group, rect, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle2 = function(fillText, fillColor, position, count) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 4) {
            startX = 0;
            startY = 0;
        }
        if (count > 3 && count < 11) {
            startX = -2;
            startY = 0;
        }


        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 37, 373);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);

        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 25, 369);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 23, 369);
                break;
            case 'up':
                Arrow.setTransform(startX + 29, 375);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 31, 362);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }
        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle3 = function(fillText, fillColor, position, count) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 5) {
            startX = 0;
            startY = 0;
        }
        if (count > 4 && count < 9) {
            startX = -2;
            startY = 0;
        }
        if (count > 8) {
            startX = -6;
            startY = 0;
        }

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 35, 396);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);
        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 35, 392);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 21, 392);
                break;
            case 'up':
                Arrow.setTransform(startX + 27, 398);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 27, 383);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }

        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.wordRectangle4 = function(fillText, fillColor) {
        this.initialize();

        var vertRect_group = new cjs.Container();

        this.verticalRectangle = new cjs.Shape();
        this.verticalRectangle.graphics.f("").s('#7d7d7d').drawRect(0, 0, 31, 97.7);
        this.verticalRectangle.setTransform(0, 0);

        this.horline = new cjs.Shape();
        this.horline.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline.setTransform(0, 23);

        this.horline2 = new cjs.Shape();
        this.horline2.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline2.setTransform(0, 46);

        this.horline3 = new cjs.Shape();
        this.horline3.graphics.f('').s("#7d7d7d").ss(1).moveTo(0, 0).lineTo(31, 0);
        this.horline3.setTransform(0, 69);

        vertRect_group.addChild(this.verticalRectangle, this.horline, this.horline2, this.horline3);
        vertRect_group.setTransform(19, 458);

        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0.5, 0.5, (31 - 1), (92 / 4) - 1);
        rect.setTransform(19, 458);

        var Xpos = 0,
            Ypos = 0;
        if (fillText < 10) {
            Xpos = 28, Ypos = 475;
        }
        if (fillText > 9) {
            Xpos = 23, Ypos = 475;
        }
        if (fillText > 99) {
            Xpos = 18, Ypos = 475;
        }

        this.text = new cjs.Text(" " + fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(Xpos, Ypos);

        this.addChild(vertRect_group, rect, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.wordRectangle5 = function(fillText, fillColor, position, count) {
        this.initialize();


        var startX = 0,
            startY = 0;

        if (count < 2) {
            startX = -1;
            startY = 0;
        }
        if (count > 1 && count < 8) {
            startX = 17.5;
            startY = 0;
        }
        if (count > 7) {
            startX = 35;
            startY = 0;
        }

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 37, 497);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);

        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 37, 493);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 23, 493);
                break;
            case 'up':
                Arrow.setTransform(startX + 31, 499);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 30, 485);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }
        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    (lib.wordRectangle6 = function(fillText, fillColor, position, count) {
        this.initialize();

        var startX = 0,
            startY = 0;

        if (count < 2) {
            startX = 0;
            startY = 0;
        }
        if (count > 1 && count < 8) {
            startX = 20;
            startY = 0;
        }
        if (count > 7) {
            startX = 39;
            startY = 0;
        }

        this.text = new cjs.Text(fillText, "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(startX + 35, 520);

        var Arrow = new cjs.Container();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(27, 27).lineTo(15, 0);
        this.arrow.setTransform(6, 0.45, 0.22, 0.22);
        this.arrow.rotation = -50;

        this.line = new cjs.Shape();
        this.line.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(12, 0);
        this.line.setTransform(0, 0);

        Arrow.addChild(this.line, this.arrow);
        switch (position) {
            case 'left':
                Arrow.setTransform(startX + 36, 516);
                Arrow.rotation = 180;
                break;
            case 'right':
                Arrow.rotation = 0;
                Arrow.setTransform(startX + 21, 516);
                break;
            case 'up':
                Arrow.setTransform(startX + 27, 522);
                Arrow.rotation = 270;
                break;
            case 'down':
                Arrow.setTransform(startX + 23, 505);
                Arrow.rotation = 90;
                break;
            default:

                break;
        }

        this.addChild(Arrow, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.FillBoxWithText = function(fillText, fillColor) {
        this.initialize();
        var rect = new cjs.Shape();
        rect.graphics.f(fillColor).s('').drawRect(0, 0, 25.2, 25.2);
        var text = new cjs.Text('' + fillText, "14px 'Myriad Pro'", "#000000");
        text.setTransform(12, 20)
        text.textAlign = 'center';
        this.addChild(rect, text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 102, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
