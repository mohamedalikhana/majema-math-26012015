(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p111_1.png",
            id: "p111_1"
        }, {
            src: "images/p111_2.png",
            id: "p111_2"
        }]
    };

    // symbols:

    (lib.p111_1 = function() {
        this.initialize(img.p111_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p111_2 = function() {
        this.initialize(img.p111_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);


    (lib.Symbol1 = function() {
        this.initialize();         
 
        this.pageNoLeft = new cjs.Text("111", "12px 'Myriad Pro'", "#FFFFFF");
        this.pageNoLeft.lineHeight = 18;
        this.pageNoLeft.setTransform(552, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.instance = new lib.p111_1();
        this.instance.setTransform(47, 13.5, 0.476, 0.475);

        this.textBox1 = new cjs.Shape();
        this.textBox1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(469, 26, 27, 27);

        this.textBox2 = new cjs.Shape();
        this.textBox2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(501, 26, 27, 27);

        this.textBox3 = new cjs.Shape();
        this.textBox3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(533, 26, 27, 27);

        this.addChild(this.instance,this.textBox1,
            this.textBox2,this.textBox3,this.shape,this.pageNoLeft);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();
        this.instance = new lib.p111_2();
        this.instance.setTransform(0, 29, 0.382, 0.382);

        this.text = new cjs.Text("Hur många kronor är det tillsammans?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(18, 6);
        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 6);

        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 9; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(42 + (i * 20), 125, 20.2, 23);
            
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 9; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(305 + (i * 20), 125, 20.2, 23);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 9; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(42 + (i * 20), 253, 20.2, 23);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 9; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(305 + (i * 20), 253, 20.2, 23);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 9; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(42 + (i * 20), 382, 20.2, 23);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 9; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(305 + (i * 20), 382, 20.2, 23);
        }
        this.textBoxGroup6.setTransform(0, 0);

          
        
        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(0, 31, 520, 383, 10);
        this.img_Rect1.setTransform(0, 0);

       this.line_1 = new cjs.Shape();
       this.line_1.graphics.s("#AAAAAA").ss(0.7).moveTo(5.5, 5.5).lineTo(514, 5.5);
       this.line_1.setTransform(0, 152);

       this.line_2 = new cjs.Shape();
       this.line_2.graphics.s("#AAAAAA").ss(0.7).moveTo(5.5, 5.5).lineTo(514, 5.5);
       this.line_2.setTransform(0, 280);

       this.line_3 = new cjs.Shape();
       this.line_3.graphics.s("#AAAAAA").ss(0.7).moveTo(20, 20).lineTo(20, 130);
       this.line_3.setTransform(240.5, 18);

       this.line_4 = new cjs.Shape();
       this.line_4.graphics.s("#AAAAAA").ss(0.7).moveTo(20, 20).lineTo(20, 132);
       this.line_4.setTransform(240.5, 150);

       this.line_5 = new cjs.Shape();
       this.line_5.graphics.s("#AAAAAA").ss(0.7).moveTo(20, 20).lineTo(20, 125);
       this.line_5.setTransform(240.5, 285);


       this.labelTemp1 = new cjs.Text('4', "35px 'UusiTekstausMajema'", "#6C6D70");
       this.labelTemp1.lineHeight = 34;
       this.labelTemp1.setTransform(42, 117);

       this.labelTemp2 = new cjs.Text('kr', "30px 'UusiTekstausMajema'", "#6C6D70");
       this.labelTemp2.lineHeight = 34;
       this.labelTemp2.setTransform(60.3, 121);

       this.labelTemp3 = new cjs.Text('+', "35px 'UusiTekstausMajema'", "#6C6D70");
       this.labelTemp3.lineHeight = 34;
       this.labelTemp3.setTransform(82, 117);

       this.labelTemp4 = new cjs.Text('4', "35px 'UusiTekstausMajema'", "#6C6D70");
       this.labelTemp4.lineHeight = 34;
       this.labelTemp4.setTransform(102, 117);

       this.labelTemp5 = new cjs.Text('kr', "30px 'UusiTekstausMajema'", "#6C6D70");
       this.labelTemp5.lineHeight = 34;
       this.labelTemp5.setTransform(120.5, 121);

       this.labelTemp6 = new cjs.Text('=', "35px 'UusiTekstausMajema'", "#6C6D70");
       this.labelTemp6.lineHeight = 34;
       this.labelTemp6.setTransform(143, 117);

        this.addChild(this.instance, this.textBoxGroup1,this.textBoxGroup2 , this.textBoxGroup3,this.textBoxGroup4, 
         this.textBoxGroup5,this.textBoxGroup6, 
             this.text, this.text_1,this.img_Rect1,this.line_1,this.line_2,this.line_3,this.line_4,this.line_5,this.labelTemp1,
             this.labelTemp2,this.labelTemp3,this.labelTemp4,this.labelTemp5,this.labelTemp6);     
       
    
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400);
 


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(305, 260, 1, 1, 0, 0, 0, 255.8, 38); 

        this.addChild(this.v1,this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
