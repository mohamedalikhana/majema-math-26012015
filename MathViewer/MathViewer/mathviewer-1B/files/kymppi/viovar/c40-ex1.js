var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }, {
            src: "images/c40_ex1_1.png",
            id: "c40_ex1_1"
        }, {
            src: "images/c40_ex1_2.png",
            id: "c40_ex1_2"
        }]
    };

    lib.init_Magic_Wand();

    var iconProperties = {
        x: 850 + 200,
        y: 430,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 150,
        wrongY: 345 - 15
    };

    var boxProperties = {
        width: 80,
        height: 90
    };

    var objects = [];

    (lib.c40_ex1_1 = function() {
        this.initialize(img.c40_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c40_ex1_2 = function() {
        this.initialize(img.c40_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.tick = function() {
        this.initialize(img.tick);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);


    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function generateImages(images, imageCount, X, Y, scaleX, scaleY, image) {
        X = 280, Y = 100;
        if (imageCount < 5) {
            //X=X+(100*((5-imageCount-1)));
            switch (imageCount) {
                case 1:
                    X = X + 200;
                    break;
                case 2:
                    X = X + 150;
                    break;
                case 3:
                    X = X + 100;
                    break;
                case 4:
                    X = X + 50;
                    break;
            }

        }
        for (var i = 0; i < imageCount; i++) {
            var iteration = i;
            if (5 <= i) {
                iteration = i - 5;
                Y = 200;
            }
            var tempImage = image.clone(true);
            tempImage.setTransform(X + (100 * iteration), Y, 0.5, 0.5);
            images.addChild(tempImage);
        }
        return images;
    }

    (lib.dice = function(count, size) {
        this.initialize();
        //console.log(size);
        thisStage = this;
        this.square = new cjs.Shape();
        this.square.graphics.f("#ffffff").ss(0.5).s('black').drawRoundRect(0, 0, size, size, 20);
        this.addChild(this.square);
        switch (count) {
            case 1:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 2:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 3, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 2 / 3, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;

            case 3:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 4:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 5:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 6:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 9:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.numberBox = function(count, properties, number) {
        this.initialize();
        //console.log(ballWidth);
        var ballWidth = properties.width;
        thisStage = this;
        this.square = new cjs.Shape();
        this.square.graphics.f("#ffffff").ss(0.5).s('black').drawRoundRect(0, 0, properties.width, properties.height, 6);
        ballWidth = (properties.width + properties.height) / 1.35;
        this.addChild(this.square);
        switch (count) {
            case 1:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 2:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 2 / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;

            case 3:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 4:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 5:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 6:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 9:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case -1:
                this.number = new cjs.Text('' + number, "72px 'Myriad Pro'", '#000000');
                this.number.textAlign = 'center';
                this.number.setTransform(40, 69)
                this.addChild(this.number);
                break;
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();
        this.hintText1 = new cjs.Text("Klicka på talet som passar.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 500);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.dartboard = function() {
        this.initialize();

        this.img1 = new lib.c40_ex1_1();
        this.img1.setTransform(0, 0);

        this.num1 = new cjs.Text("1", "50px 'Myriad Pro'")
        this.num1.setTransform(210, 100);

        this.num2 = new cjs.Text("2", "50px 'Myriad Pro'")
        this.num2.setTransform(260, 370);

        this.num3 = new cjs.Text("3", "50px 'Myriad Pro'")
        this.num3.setTransform(310, 120);

        this.num4 = new cjs.Text("4", "50px 'Myriad Pro'")
        this.num4.setTransform(160, 370);

        this.num5 = new cjs.Text("5", "50px 'Myriad Pro'")
        this.num5.setTransform(350, 210);

        this.num6 = new cjs.Text("6", "50px 'Myriad Pro'")
        this.num6.setTransform(95, 305);

        this.num7 = new cjs.Text("7", "50px 'Myriad Pro'")
        this.num7.setTransform(345, 315);

        this.num8 = new cjs.Text("8", "50px 'Myriad Pro'")
        this.num8.setTransform(110, 120);

        this.num9 = new cjs.Text("9", "50px 'Myriad Pro'")
        this.num9.setTransform(70, 210);

        this.num10 = new cjs.Text("10", "50px 'Myriad Pro'")
        this.num10.setTransform(197, 237);

        this.addChild(this.img1, this.num1, this.num2, this.num3, this.num4, this.num5, this.num6, this.num7, this.num8, this.num9, this.num10);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    function handleOver(evt) {
        var box = evt.currentTarget;
        box.children[0].graphics.clear().ss(0.5).s('#000000').beginLinearGradientFill(["#ADD8E6", "#ADD8E6"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, boxProperties.width, boxProperties.height, 5);
    }

    function handleOut(evt) {
        var box = evt.currentTarget;
        box.children[0].graphics.clear().ss(0.5).s('#000000').beginLinearGradientFill(["#FFFFFF", "#ffffff"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, boxProperties.width, boxProperties.height, 5);
    }


    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);

        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.addChild(this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        thisStage.currentAnswer = 0;

        var instructionText = [];
        instructionText.push("Summan är 15. Var på tavlan ska den tredje pilen sitta?");
        instructionText.push("Summan är 16. Var på tavlan ska den tredje pilen sitta?");
        instructionText.push("Summan är 14. Var på tavlan ska den tredje pilen sitta?");
        instructionText.push("Summan är 18. Var på tavlan ska den tredje pilen sitta?");
        instructionText.push("Summan är 20. Var på tavlan ska den tredje pilen sitta?");

        this.questionText = new cjs.Text(" ", "40px 'Myriad Pro'")
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -80);

        this.dartboard = new lib.dartboard();
        this.dartboard.setTransform(415, -50);

        var arrowPos = [];
        arrowPos[0] = [];
        arrowPos[1] = [];
        arrowPos[2] = [];
        arrowPos[3] = [];
        arrowPos[4] = [];

        arrowPos[0].push({
            x1: 1200,
            y1: 300,
            x2: 1020,
            y2: 80,
            rotation: 180
        });

        arrowPos[1].push({
            x1: 1200,
            y1: 300,
            x2: 1090,
            y2: 107,
            rotation: 160
        });

        arrowPos[2].push({
            x1: 1200,
            y1: 300,
            x2: 1090,
            y2: 107,
            rotation: 160
        });

        arrowPos[3].push({
            x1: 1200,
            y1: 300,
            x2: 970,
            y2: 170,
            rotation: 140
        });

        arrowPos[4].push({
            x1: 1200,
            y1: 300,
            x2: 950,
            y2: 150,
            rotation: 160
        });

        this.arrow3 = new lib.c40_ex1_2();
        // this.arrow3.setTransform(950, 150, 1, 1, 160)

        this.container1 = new cjs.Container();

        this.ct1_arrow1 = new lib.c40_ex1_2();
        this.ct1_arrow1.setTransform(240, 0);
        this.ct1_arrow2 = new lib.c40_ex1_2();
        this.ct1_arrow2.setTransform(280, 240);

        this.container1.addChild(this.ct1_arrow1, this.ct1_arrow2);
        this.container1.visible = false;

        this.container2 = new cjs.Container();

        this.ct2_arrow1 = new lib.c40_ex1_2();
        this.ct2_arrow1.setTransform(1075, 320, 1, 1, 180);
        this.ct2_arrow2 = new lib.c40_ex1_2();
        this.ct2_arrow2.setTransform(280, 240);

        this.container2.addChild(this.ct2_arrow1, this.ct2_arrow2)
        this.container2.visible = false;

        this.container3 = new cjs.Container();

        this.ct3_arrow1 = new lib.c40_ex1_2();
        this.ct3_arrow1.setTransform(1075, 280, 1, 1, 190);
        this.ct3_arrow2 = new lib.c40_ex1_2();
        this.ct3_arrow2.setTransform(280, 240);

        this.container3.addChild(this.ct3_arrow1, this.ct3_arrow2)
        this.container3.visible = false;

        this.container4 = new cjs.Container();

        this.ct4_arrow1 = new lib.c40_ex1_2();
        this.ct4_arrow1.setTransform(230, -20, 1, 1, 0);
        this.ct4_arrow2 = new lib.c40_ex1_2();
        this.ct4_arrow2.setTransform(250, 130, 1, 1, 340);

        this.container4.addChild(this.ct4_arrow1, this.ct4_arrow2)
        this.container4.visible = false;

        this.container5 = new cjs.Container();

        this.ct5_arrow1 = new lib.c40_ex1_2();
        this.ct5_arrow1.setTransform(1075, 320, 1, 1, 180);
        this.ct5_arrow2 = new lib.c40_ex1_2();
        this.ct5_arrow2.setTransform(1015, 80, 1, 1, 180);

        this.container5.addChild(this.ct5_arrow1, this.ct5_arrow2)
        this.container5.visible = false;

        var myStage = this;
        this.addChild(this.questionText, this.dartboard, this.container1, this.container2, this.container3, this.container4, this.container5, this.arrow3);

        var boxes = [],
            boxesX = 450,
            boxesY = 420;
        var boxeCount = 3;

        var nums = [];

        nums[0] = [2, 3, 4];
        nums[1] = [4, 5, 6];
        nums[2] = [4, 5, 6];
        nums[3] = [2, 3, 4];
        nums[4] = [8, 9, 10];

        for (var i = 0; i < boxeCount; i++) {
            var box = new lib.numberBox(-1, {
                width: boxProperties.width,
                height: boxProperties.height
            }, nums[0][i]);

            box.setTransform(boxesX + (150 * i), boxesY, 1.4, 1.4);
            box.value = nums[0][i];
            myStage.addChild(box);
            boxes.push(box);
            box.addEventListener('click', checkAnswer);
            box.on("mouseover", handleOver);
            box.on("mouseout", handleOut);
        };

        myStage.answer = 0;

        function moveArrowToNum(e) {
            cjs.Tween.get(myStage.arrow3).to({
                x: arrowPos[exerciseCount - 1][0].x2,
                y: arrowPos[exerciseCount - 1][0].y2,
                rotation: arrowPos[exerciseCount - 1][0].rotation
            }, 2000).call(function() {
                myStage.onAnswerCorrect(e.currentTarget);
            });
        }

        function checkAnswer(e) {
            resetCheckAns();
            if (e.currentTarget.value == myStage.answer) {
                moveArrowToNum(e);
            } else {
                myStage.arrow3.x = arrowPos[0][0].x1,
                    myStage.arrow3.y = arrowPos[0][0].y1,
                    myStage.arrow3.rotation = 269;
                myStage.onAnswerIncorrect(e.currentTarget);
            }
        }

        function resetCheckAns() {
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
        }

        function resetAllContainers() {
            myStage.container1.visible = false;
            myStage.container2.visible = false;
            myStage.container3.visible = false;
            myStage.container4.visible = false;
            myStage.container5.visible = false;
        }

        exerciseCount = 0;

        this.onNewExercise = function(e) {
            if (exerciseCount < 6) {
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                myStage.questionText.text = instructionText[exerciseCount];
                myStage.arrow3.setTransform(arrowPos[exerciseCount][0].x1, arrowPos[exerciseCount][0].y1, 1, 1, 269);
                resetAllContainers();
                if (exerciseCount == 0) {
                    myStage.container1.visible = true;
                } else if (exerciseCount == 1) {
                    myStage.container2.visible = true;
                } else if (exerciseCount == 2) {
                    myStage.container3.visible = true;
                } else if (exerciseCount == 3) {
                    myStage.container4.visible = true;
                } else if (exerciseCount == 4) {
                    myStage.container5.visible = true;
                }

                for (var i = 0; i < boxes.length; i++) {
                    boxes[i].number.color = "#000000";
                    boxes[i].number.text = nums[exerciseCount][i] + "";
                    if (nums[exerciseCount][i] == 10) {
                        boxes[i].number.x = boxes[i].number.x - 3;
                    }
                    boxes[i].value = nums[exerciseCount][i];
                };

                var answers = [3, 5, 5, 2, 10];

                myStage.answer = answers[exerciseCount];

                new lib.Magic_Wand(myStage, false);

                if (exerciseCount > 0) {
                    $(btn_newExercise).hide();
                    $(btn_newExercise).addClass('disabled');
                }

                exerciseCount = exerciseCount + 1;
            }
            if (typeof btn_newExercise != 'undefined') {
                $(btn_newExercise).hide();
            }
        };
        var showIncorrectAnswerTween = null;
        var currentTimeoutAnimation = null;
        this.onAnswerCorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = true;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            box.number.color = "#008000";
            if (exerciseCount < 5) {
                currentTimeoutAnimation = setTimeout(function() {
                    $(btn_newExercise).removeClass('disabled');
                    $(btn_newExercise).show();
                }, 1000)
            }
            stage.update();
        };
        this.onAnswerIncorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            box.number.color = "#ff0000";
            $(btn_newExercise).hide();
            stage.update();
            showIncorrectAnswerTween = cjs.Tween.get(box.number).wait(4000).call(function(e) {
                box.number.color = "#000000";
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                stage.update();
            });
        };

        this.onIncorrectAnswer = function() {
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = true;
            // for (var i = 0; i < circles.length; i++) {
            //     var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
            //         resetFillStyle[1] = "#ffffff";
            // }
            cjs.Tween.get(myStage.dices).wait(500).call(function(e) {
                for (var i = 0; i < myStage.dices.children.length; i++) {
                    var resetFillStyle = myStage.dices.children[i].square.graphics._fillInstructions[0].params;
                    if (resetFillStyle[1] === "#ffff00") {
                        resetFillStyle[1] = "#ffffff";
                    }
                }
                stage.update();
            });

        };

        this.onNewExercise();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Problemlösning", "bold 24px 'Myriad Pro'", "#FAAA33");
        this.text.setTransform(96.5, 25);

        this.text_1 = new cjs.Text("40", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(48, 24);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.balls = function() {
        this.initialize();
        //function () {



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)



        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
