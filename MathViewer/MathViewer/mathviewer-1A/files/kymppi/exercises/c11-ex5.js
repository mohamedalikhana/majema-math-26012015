var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        // ishideButtons: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };
    var transformX = transformY = 0.191;
    var moveX = 480 + 70,
        moveY = 6;
    var startX = ((362.0960) * transformX) + moveX,
        startY = ((5.9588) * transformY) + moveY;
    var endX = ((1.0000) * transformX) + moveX,
        endY = ((1661.0000) * transformY) + moveY;
    var pencil = null;
    //  new createjs.Point(((1.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),

    var startX2 = ((363.0000) * transformX) + moveX,
        startY2 = ((0) * transformY) + moveY,
        endX2 = ((1000.0000) * transformX) + moveX,
        endY2 = ((0) * transformY) + moveY;
    var altStartX2 = startX,
        altStartY2 = startY,
        altEndX2 = endX,
        altEndY2 = endY;
    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [
        new createjs.Point(((362.0960) * transformX) + moveX, ((5.9588) * transformY) + moveY),
        new createjs.Point(((361.2579) * transformX) + moveX, ((10.6546) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((360.0000) * transformX) + moveX, ((17.6664) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((20.3336) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((359.6667) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((359.3333) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((359.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((359.0000) * transformX) + moveX, ((25.6664) * transformY) + moveY),
        new createjs.Point(((359.0000) * transformX) + moveX, ((28.3336) * transformY) + moveY),
        new createjs.Point(((359.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((358.6667) * transformX) + moveX, ((31.0000) * transformY) + moveY),
        new createjs.Point(((358.3333) * transformX) + moveX, ((31.0000) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((358.0000) * transformX) + moveX, ((33.6664) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((36.3336) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((39.0000) * transformY) + moveY),

        new createjs.Point(((357.6667) * transformX) + moveX, ((39.0000) * transformY) + moveY),
        new createjs.Point(((357.3333) * transformX) + moveX, ((39.0000) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((39.0000) * transformY) + moveY),

        new createjs.Point(((357.0000) * transformX) + moveX, ((41.6664) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((44.3336) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((356.6667) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((356.3333) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((356.0000) * transformX) + moveX, ((49.3331) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((51.6669) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((54.0000) * transformY) + moveY),

        new createjs.Point(((355.6667) * transformX) + moveX, ((54.0000) * transformY) + moveY),
        new createjs.Point(((355.3333) * transformX) + moveX, ((54.0000) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((54.0000) * transformY) + moveY),

        new createjs.Point(((355.0000) * transformX) + moveX, ((56.6664) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((59.3336) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((62.0000) * transformY) + moveY),

        new createjs.Point(((354.6667) * transformX) + moveX, ((62.0000) * transformY) + moveY),
        new createjs.Point(((354.3333) * transformX) + moveX, ((62.0000) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((62.0000) * transformY) + moveY),

        new createjs.Point(((353.6667) * transformX) + moveX, ((67.3328) * transformY) + moveY),
        new createjs.Point(((353.3333) * transformX) + moveX, ((72.6672) * transformY) + moveY),
        new createjs.Point(((353.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((348.9547) * transformX) + moveX, ((91.9207) * transformY) + moveY),
        new createjs.Point(((350.0172) * transformX) + moveX, ((110.0970) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((124.0000) * transformY) + moveY),

        new createjs.Point(((345.3334) * transformX) + moveX, ((131.9992) * transformY) + moveY),
        new createjs.Point(((344.6666) * transformX) + moveX, ((140.0008) * transformY) + moveY),
        new createjs.Point(((344.0000) * transformX) + moveX, ((148.0000) * transformY) + moveY),

        new createjs.Point(((343.6667) * transformX) + moveX, ((148.0000) * transformY) + moveY),
        new createjs.Point(((343.3333) * transformX) + moveX, ((148.0000) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((148.0000) * transformY) + moveY),

        new createjs.Point(((343.0000) * transformX) + moveX, ((150.6664) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((153.3336) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((156.0000) * transformY) + moveY),

        new createjs.Point(((342.6667) * transformX) + moveX, ((156.0000) * transformY) + moveY),
        new createjs.Point(((342.3333) * transformX) + moveX, ((156.0000) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((156.0000) * transformY) + moveY),

        new createjs.Point(((342.0000) * transformX) + moveX, ((158.3331) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((160.6669) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((163.0000) * transformY) + moveY),

        new createjs.Point(((341.6667) * transformX) + moveX, ((163.0000) * transformY) + moveY),
        new createjs.Point(((341.3333) * transformX) + moveX, ((163.0000) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((163.0000) * transformY) + moveY),

        new createjs.Point(((341.0000) * transformX) + moveX, ((165.6664) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((168.3336) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),

        new createjs.Point(((340.6667) * transformX) + moveX, ((171.0000) * transformY) + moveY),
        new createjs.Point(((340.3333) * transformX) + moveX, ((171.0000) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),

        new createjs.Point(((340.0000) * transformX) + moveX, ((173.6664) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((176.3336) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((179.0000) * transformY) + moveY),

        new createjs.Point(((339.6667) * transformX) + moveX, ((179.0000) * transformY) + moveY),
        new createjs.Point(((339.3333) * transformX) + moveX, ((179.0000) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((179.0000) * transformY) + moveY),

        new createjs.Point(((339.0000) * transformX) + moveX, ((181.6664) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((184.3336) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((187.0000) * transformY) + moveY),

        new createjs.Point(((334.2438) * transformX) + moveX, ((203.4025) * transformY) + moveY),
        new createjs.Point(((335.7297) * transformX) + moveX, ((224.6086) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((241.0000) * transformY) + moveY),

        new createjs.Point(((329.6668) * transformX) + moveX, ((253.9987) * transformY) + moveY),
        new createjs.Point(((328.3332) * transformX) + moveX, ((267.0013) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((280.0000) * transformY) + moveY),

        new createjs.Point(((324.3238) * transformX) + moveX, ((289.2356) * transformY) + moveY),
        new createjs.Point(((324.6417) * transformX) + moveX, ((301.8426) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((311.0000) * transformY) + moveY),

        new createjs.Point(((322.0000) * transformX) + moveX, ((313.6664) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((316.3336) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((319.0000) * transformY) + moveY),

        new createjs.Point(((321.6667) * transformX) + moveX, ((319.0000) * transformY) + moveY),
        new createjs.Point(((321.3333) * transformX) + moveX, ((319.0000) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((319.0000) * transformY) + moveY),

        new createjs.Point(((321.0000) * transformX) + moveX, ((321.6664) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((324.3336) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((327.0000) * transformY) + moveY),

        new createjs.Point(((320.6667) * transformX) + moveX, ((327.0000) * transformY) + moveY),
        new createjs.Point(((320.3333) * transformX) + moveX, ((327.0000) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((327.0000) * transformY) + moveY),

        new createjs.Point(((320.0000) * transformX) + moveX, ((329.6664) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((332.3336) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((335.0000) * transformY) + moveY),

        new createjs.Point(((319.6667) * transformX) + moveX, ((335.0000) * transformY) + moveY),
        new createjs.Point(((319.3333) * transformX) + moveX, ((335.0000) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((335.0000) * transformY) + moveY),

        new createjs.Point(((319.0000) * transformX) + moveX, ((337.6664) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((340.3336) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((343.0000) * transformY) + moveY),

        new createjs.Point(((318.6667) * transformX) + moveX, ((343.0000) * transformY) + moveY),
        new createjs.Point(((318.3333) * transformX) + moveX, ((343.0000) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((343.0000) * transformY) + moveY),

        new createjs.Point(((318.0000) * transformX) + moveX, ((345.3331) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((347.6669) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((350.0000) * transformY) + moveY),

        new createjs.Point(((317.6667) * transformX) + moveX, ((350.0000) * transformY) + moveY),
        new createjs.Point(((317.3333) * transformX) + moveX, ((350.0000) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((350.0000) * transformY) + moveY),

        new createjs.Point(((317.0000) * transformX) + moveX, ((352.6664) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((355.3336) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((358.0000) * transformY) + moveY),

        new createjs.Point(((316.6667) * transformX) + moveX, ((358.0000) * transformY) + moveY),
        new createjs.Point(((316.3333) * transformX) + moveX, ((358.0000) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((358.0000) * transformY) + moveY),

        new createjs.Point(((316.0000) * transformX) + moveX, ((360.6664) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((363.3336) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((366.0000) * transformY) + moveY),

        new createjs.Point(((311.8769) * transformX) + moveX, ((380.2317) * transformY) + moveY),
        new createjs.Point(((313.1387) * transformX) + moveX, ((398.7107) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((413.0000) * transformY) + moveY),

        new createjs.Point(((308.6667) * transformX) + moveX, ((417.9995) * transformY) + moveY),
        new createjs.Point(((308.3333) * transformX) + moveX, ((423.0005) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((428.0000) * transformY) + moveY),

        new createjs.Point(((307.6667) * transformX) + moveX, ((428.0000) * transformY) + moveY),
        new createjs.Point(((307.3333) * transformX) + moveX, ((428.0000) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((428.0000) * transformY) + moveY),

        new createjs.Point(((305.0002) * transformX) + moveX, ((446.3315) * transformY) + moveY),
        new createjs.Point(((302.9998) * transformX) + moveX, ((464.6685) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((483.0000) * transformY) + moveY),

        new createjs.Point(((300.6667) * transformX) + moveX, ((483.0000) * transformY) + moveY),
        new createjs.Point(((300.3333) * transformX) + moveX, ((483.0000) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((483.0000) * transformY) + moveY),

        new createjs.Point(((300.0000) * transformX) + moveX, ((485.6664) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((488.3336) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((299.6667) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((299.3333) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((299.0000) * transformX) + moveX, ((493.3331) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((495.6669) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((498.0000) * transformY) + moveY),

        new createjs.Point(((298.6667) * transformX) + moveX, ((498.0000) * transformY) + moveY),
        new createjs.Point(((298.3333) * transformX) + moveX, ((498.0000) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((498.0000) * transformY) + moveY),

        new createjs.Point(((298.0000) * transformX) + moveX, ((500.6664) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((503.3336) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((297.6667) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((297.3333) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((297.0000) * transformX) + moveX, ((508.6664) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((511.3336) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((514.0000) * transformY) + moveY),

        new createjs.Point(((296.6667) * transformX) + moveX, ((514.0000) * transformY) + moveY),
        new createjs.Point(((296.3333) * transformX) + moveX, ((514.0000) * transformY) + moveY),
        new createjs.Point(((296.0000) * transformX) + moveX, ((514.0000) * transformY) + moveY),

        new createjs.Point(((296.0000) * transformX) + moveX, ((516.6664) * transformY) + moveY),
        new createjs.Point(((296.0000) * transformX) + moveX, ((519.3336) * transformY) + moveY),
        new createjs.Point(((296.0000) * transformX) + moveX, ((522.0000) * transformY) + moveY),

        new createjs.Point(((295.6667) * transformX) + moveX, ((522.0000) * transformY) + moveY),
        new createjs.Point(((295.3333) * transformX) + moveX, ((522.0000) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((522.0000) * transformY) + moveY),

        new createjs.Point(((295.0000) * transformX) + moveX, ((524.3331) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((526.6669) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((529.0000) * transformY) + moveY),

        new createjs.Point(((294.6667) * transformX) + moveX, ((529.0000) * transformY) + moveY),
        new createjs.Point(((294.3333) * transformX) + moveX, ((529.0000) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((529.0000) * transformY) + moveY),

        new createjs.Point(((294.0000) * transformX) + moveX, ((531.6664) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((534.3336) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((537.0000) * transformY) + moveY),

        new createjs.Point(((293.6667) * transformX) + moveX, ((537.0000) * transformY) + moveY),
        new createjs.Point(((293.3333) * transformX) + moveX, ((537.0000) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((537.0000) * transformY) + moveY),

        new createjs.Point(((293.0000) * transformX) + moveX, ((539.6664) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((542.3336) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((545.0000) * transformY) + moveY),

        new createjs.Point(((292.6667) * transformX) + moveX, ((545.0000) * transformY) + moveY),
        new createjs.Point(((292.3333) * transformX) + moveX, ((545.0000) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((545.0000) * transformY) + moveY),

        new createjs.Point(((292.0000) * transformX) + moveX, ((547.6664) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((550.3336) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((553.0000) * transformY) + moveY),

        new createjs.Point(((287.2489) * transformX) + moveX, ((569.4055) * transformY) + moveY),
        new createjs.Point(((288.7278) * transformX) + moveX, ((590.6072) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((607.0000) * transformY) + moveY),

        new createjs.Point(((283.0001) * transformX) + moveX, ((617.6656) * transformY) + moveY),
        new createjs.Point(((281.9999) * transformX) + moveX, ((628.3344) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((639.0000) * transformY) + moveY),

        new createjs.Point(((280.6667) * transformX) + moveX, ((639.0000) * transformY) + moveY),
        new createjs.Point(((280.3333) * transformX) + moveX, ((639.0000) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((639.0000) * transformY) + moveY),

        new createjs.Point(((280.0000) * transformX) + moveX, ((641.3331) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((643.6669) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((646.0000) * transformY) + moveY),

        new createjs.Point(((279.6667) * transformX) + moveX, ((646.0000) * transformY) + moveY),
        new createjs.Point(((279.3333) * transformX) + moveX, ((646.0000) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((646.0000) * transformY) + moveY),

        new createjs.Point(((279.0000) * transformX) + moveX, ((648.6664) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((651.3336) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((654.0000) * transformY) + moveY),

        new createjs.Point(((278.6667) * transformX) + moveX, ((654.0000) * transformY) + moveY),
        new createjs.Point(((278.3333) * transformX) + moveX, ((654.0000) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((654.0000) * transformY) + moveY),

        new createjs.Point(((278.0000) * transformX) + moveX, ((656.6664) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((659.3336) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((662.0000) * transformY) + moveY),

        new createjs.Point(((273.2532) * transformX) + moveX, ((678.4080) * transformY) + moveY),
        new createjs.Point(((274.7250) * transformX) + moveX, ((699.6055) * transformY) + moveY),
        new createjs.Point(((270.0000) * transformX) + moveX, ((716.0000) * transformY) + moveY),

        new createjs.Point(((269.6667) * transformX) + moveX, ((721.3328) * transformY) + moveY),
        new createjs.Point(((269.3333) * transformX) + moveX, ((726.6672) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((732.0000) * transformY) + moveY),

        new createjs.Point(((265.5873) * transformX) + moveX, ((743.8154) * transformY) + moveY),
        new createjs.Point(((266.3914) * transformX) + moveX, ((759.1745) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((771.0000) * transformY) + moveY),

        new createjs.Point(((262.6667) * transformX) + moveX, ((776.3328) * transformY) + moveY),
        new createjs.Point(((262.3333) * transformX) + moveX, ((781.6672) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),

        new createjs.Point(((261.6667) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((261.3333) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),

        new createjs.Point(((261.0000) * transformX) + moveX, ((789.3331) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((791.6669) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),

        new createjs.Point(((260.6667) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((260.3333) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),

        new createjs.Point(((260.0000) * transformX) + moveX, ((796.6664) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((799.3336) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((802.0000) * transformY) + moveY),

        new createjs.Point(((259.6667) * transformX) + moveX, ((802.0000) * transformY) + moveY),
        new createjs.Point(((259.3333) * transformX) + moveX, ((802.0000) * transformY) + moveY),
        new createjs.Point(((259.0000) * transformX) + moveX, ((802.0000) * transformY) + moveY),

        new createjs.Point(((259.0000) * transformX) + moveX, ((804.6664) * transformY) + moveY),
        new createjs.Point(((259.0000) * transformX) + moveX, ((807.3336) * transformY) + moveY),
        new createjs.Point(((259.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),

        new createjs.Point(((258.6667) * transformX) + moveX, ((810.0000) * transformY) + moveY),
        new createjs.Point(((258.3333) * transformX) + moveX, ((810.0000) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),

        new createjs.Point(((258.0000) * transformX) + moveX, ((812.6664) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((815.3336) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((257.6667) * transformX) + moveX, ((818.0000) * transformY) + moveY),
        new createjs.Point(((257.3333) * transformX) + moveX, ((818.0000) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((257.0000) * transformX) + moveX, ((820.3331) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((822.6669) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((825.0000) * transformY) + moveY),

        new createjs.Point(((256.6667) * transformX) + moveX, ((825.0000) * transformY) + moveY),
        new createjs.Point(((256.3333) * transformX) + moveX, ((825.0000) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((825.0000) * transformY) + moveY),

        new createjs.Point(((256.0000) * transformX) + moveX, ((827.6664) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((830.3336) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((833.0000) * transformY) + moveY),

        new createjs.Point(((255.6667) * transformX) + moveX, ((833.0000) * transformY) + moveY),
        new createjs.Point(((255.3333) * transformX) + moveX, ((833.0000) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((833.0000) * transformY) + moveY),

        new createjs.Point(((255.0000) * transformX) + moveX, ((835.6664) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((838.3336) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((841.0000) * transformY) + moveY),

        new createjs.Point(((253.4434) * transformX) + moveX, ((846.4053) * transformY) + moveY),
        new createjs.Point(((252.3635) * transformX) + moveX, ((854.7606) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((861.0000) * transformY) + moveY),

        new createjs.Point(((282.9969) * transformX) + moveX, ((850.3344) * transformY) + moveY),
        new createjs.Point(((314.0031) * transformX) + moveX, ((839.6656) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((829.0000) * transformY) + moveY),

        new createjs.Point(((350.6661) * transformX) + moveX, ((827.6668) * transformY) + moveY),
        new createjs.Point(((356.3339) * transformX) + moveX, ((826.3332) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((825.0000) * transformY) + moveY),

        new createjs.Point(((362.0000) * transformX) + moveX, ((824.6667) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((824.3333) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((824.0000) * transformY) + moveY),

        new createjs.Point(((362.9999) * transformX) + moveX, ((824.0000) * transformY) + moveY),
        new createjs.Point(((364.0001) * transformX) + moveX, ((824.0000) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((824.0000) * transformY) + moveY),

        new createjs.Point(((365.0000) * transformX) + moveX, ((823.6667) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((823.3333) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((823.0000) * transformY) + moveY),

        new createjs.Point(((365.9999) * transformX) + moveX, ((823.0000) * transformY) + moveY),
        new createjs.Point(((367.0001) * transformX) + moveX, ((823.0000) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((823.0000) * transformY) + moveY),

        new createjs.Point(((368.0000) * transformX) + moveX, ((822.6667) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((822.3333) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((822.0000) * transformY) + moveY),

        new createjs.Point(((371.3330) * transformX) + moveX, ((821.3334) * transformY) + moveY),
        new createjs.Point(((374.6670) * transformX) + moveX, ((820.6666) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((820.0000) * transformY) + moveY),

        new createjs.Point(((378.0000) * transformX) + moveX, ((819.6667) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((819.3333) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((819.0000) * transformY) + moveY),

        new createjs.Point(((380.3331) * transformX) + moveX, ((818.6667) * transformY) + moveY),
        new createjs.Point(((382.6669) * transformX) + moveX, ((818.3333) * transformY) + moveY),
        new createjs.Point(((385.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((385.0000) * transformX) + moveX, ((817.6667) * transformY) + moveY),
        new createjs.Point(((385.0000) * transformX) + moveX, ((817.3333) * transformY) + moveY),
        new createjs.Point(((385.0000) * transformX) + moveX, ((817.0000) * transformY) + moveY),

        new createjs.Point(((389.6662) * transformX) + moveX, ((816.0001) * transformY) + moveY),
        new createjs.Point(((394.3338) * transformX) + moveX, ((814.9999) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((814.0000) * transformY) + moveY),

        new createjs.Point(((399.0000) * transformX) + moveX, ((813.6667) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((813.3333) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((813.0000) * transformY) + moveY),

        new createjs.Point(((405.3327) * transformX) + moveX, ((811.6668) * transformY) + moveY),
        new createjs.Point(((411.6673) * transformX) + moveX, ((810.3332) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((809.0000) * transformY) + moveY),

        new createjs.Point(((418.0000) * transformX) + moveX, ((808.6667) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((808.3333) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((419.3332) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((420.6668) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((422.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((422.0000) * transformX) + moveX, ((807.6667) * transformY) + moveY),
        new createjs.Point(((422.0000) * transformX) + moveX, ((807.3333) * transformY) + moveY),
        new createjs.Point(((422.0000) * transformX) + moveX, ((807.0000) * transformY) + moveY),

        new createjs.Point(((423.3332) * transformX) + moveX, ((807.0000) * transformY) + moveY),
        new createjs.Point(((424.6668) * transformX) + moveX, ((807.0000) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((807.0000) * transformY) + moveY),

        new createjs.Point(((426.0000) * transformX) + moveX, ((806.6667) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((806.3333) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((806.0000) * transformY) + moveY),

        new createjs.Point(((427.6665) * transformX) + moveX, ((806.0000) * transformY) + moveY),
        new createjs.Point(((429.3335) * transformX) + moveX, ((806.0000) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((806.0000) * transformY) + moveY),

        new createjs.Point(((431.0000) * transformX) + moveX, ((805.6667) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((805.3333) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),

        new createjs.Point(((432.3332) * transformX) + moveX, ((805.0000) * transformY) + moveY),
        new createjs.Point(((433.6668) * transformX) + moveX, ((805.0000) * transformY) + moveY),
        new createjs.Point(((435.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),

        new createjs.Point(((435.0000) * transformX) + moveX, ((804.6667) * transformY) + moveY),
        new createjs.Point(((435.0000) * transformX) + moveX, ((804.3333) * transformY) + moveY),
        new createjs.Point(((435.0000) * transformX) + moveX, ((804.0000) * transformY) + moveY),

        new createjs.Point(((436.3332) * transformX) + moveX, ((804.0000) * transformY) + moveY),
        new createjs.Point(((437.6668) * transformX) + moveX, ((804.0000) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((804.0000) * transformY) + moveY),

        new createjs.Point(((439.0000) * transformX) + moveX, ((803.6667) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((803.3333) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((803.0000) * transformY) + moveY),

        new createjs.Point(((448.6657) * transformX) + moveX, ((801.3335) * transformY) + moveY),
        new createjs.Point(((458.3343) * transformX) + moveX, ((799.6665) * transformY) + moveY),
        new createjs.Point(((468.0000) * transformX) + moveX, ((798.0000) * transformY) + moveY),

        new createjs.Point(((472.7792) * transformX) + moveX, ((796.5174) * transformY) + moveY),
        new createjs.Point(((480.0831) * transformX) + moveX, ((795.4523) * transformY) + moveY),
        new createjs.Point(((485.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),

        new createjs.Point(((487.3331) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((489.6669) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((492.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),

        new createjs.Point(((504.2475) * transformX) + moveX, ((790.4893) * transformY) + moveY),
        new createjs.Point(((521.6222) * transformX) + moveX, ((790.0055) * transformY) + moveY),
        new createjs.Point(((537.0000) * transformX) + moveX, ((790.0000) * transformY) + moveY),

        new createjs.Point(((546.3324) * transformX) + moveX, ((790.0000) * transformY) + moveY),
        new createjs.Point(((555.6676) * transformX) + moveX, ((790.0000) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((790.0000) * transformY) + moveY),

        new createjs.Point(((565.0000) * transformX) + moveX, ((790.3333) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((790.6667) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((791.0000) * transformY) + moveY),

        new createjs.Point(((570.3328) * transformX) + moveX, ((791.0000) * transformY) + moveY),
        new createjs.Point(((575.6672) * transformX) + moveX, ((791.0000) * transformY) + moveY),
        new createjs.Point(((581.0000) * transformX) + moveX, ((791.0000) * transformY) + moveY),

        new createjs.Point(((591.2362) * transformX) + moveX, ((793.9535) * transformY) + moveY),
        new createjs.Point(((605.0488) * transformX) + moveX, ((793.0331) * transformY) + moveY),
        new createjs.Point(((615.0000) * transformX) + moveX, ((796.0000) * transformY) + moveY),

        new createjs.Point(((616.9998) * transformX) + moveX, ((796.0000) * transformY) + moveY),
        new createjs.Point(((619.0002) * transformX) + moveX, ((796.0000) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((796.0000) * transformY) + moveY),

        new createjs.Point(((633.3321) * transformX) + moveX, ((798.6664) * transformY) + moveY),
        new createjs.Point(((645.6679) * transformX) + moveX, ((801.3336) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((804.0000) * transformY) + moveY),

        new createjs.Point(((658.0000) * transformX) + moveX, ((804.3333) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((804.6667) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),

        new createjs.Point(((660.3331) * transformX) + moveX, ((805.3333) * transformY) + moveY),
        new createjs.Point(((662.6669) * transformX) + moveX, ((805.6667) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((806.0000) * transformY) + moveY),

        new createjs.Point(((665.0000) * transformX) + moveX, ((806.3333) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((806.6667) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((807.0000) * transformY) + moveY),

        new createjs.Point(((665.9999) * transformX) + moveX, ((807.0000) * transformY) + moveY),
        new createjs.Point(((667.0001) * transformX) + moveX, ((807.0000) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((807.0000) * transformY) + moveY),

        new createjs.Point(((668.0000) * transformX) + moveX, ((807.3333) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((807.6667) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((668.9999) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((670.0001) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((671.0000) * transformX) + moveX, ((808.3333) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((808.6667) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((809.0000) * transformY) + moveY),

        new createjs.Point(((671.9999) * transformX) + moveX, ((809.0000) * transformY) + moveY),
        new createjs.Point(((673.0001) * transformX) + moveX, ((809.0000) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((809.0000) * transformY) + moveY),

        new createjs.Point(((674.0000) * transformX) + moveX, ((809.3333) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((809.6667) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),

        new createjs.Point(((674.9999) * transformX) + moveX, ((810.0000) * transformY) + moveY),
        new createjs.Point(((676.0001) * transformX) + moveX, ((810.0000) * transformY) + moveY),
        new createjs.Point(((677.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),

        new createjs.Point(((677.0000) * transformX) + moveX, ((810.3333) * transformY) + moveY),
        new createjs.Point(((677.0000) * transformX) + moveX, ((810.6667) * transformY) + moveY),
        new createjs.Point(((677.0000) * transformX) + moveX, ((811.0000) * transformY) + moveY),

        new createjs.Point(((677.9999) * transformX) + moveX, ((811.0000) * transformY) + moveY),
        new createjs.Point(((679.0001) * transformX) + moveX, ((811.0000) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((811.0000) * transformY) + moveY),

        new createjs.Point(((680.0000) * transformX) + moveX, ((811.3333) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((811.6667) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((812.0000) * transformY) + moveY),

        new createjs.Point(((680.9999) * transformX) + moveX, ((812.0000) * transformY) + moveY),
        new createjs.Point(((682.0001) * transformX) + moveX, ((812.0000) * transformY) + moveY),
        new createjs.Point(((683.0000) * transformX) + moveX, ((812.0000) * transformY) + moveY),

        new createjs.Point(((683.0000) * transformX) + moveX, ((812.3333) * transformY) + moveY),
        new createjs.Point(((683.0000) * transformX) + moveX, ((812.6667) * transformY) + moveY),
        new createjs.Point(((683.0000) * transformX) + moveX, ((813.0000) * transformY) + moveY),

        new createjs.Point(((685.9997) * transformX) + moveX, ((813.6666) * transformY) + moveY),
        new createjs.Point(((689.0003) * transformX) + moveX, ((814.3334) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((815.0000) * transformY) + moveY),

        new createjs.Point(((692.0000) * transformX) + moveX, ((815.3333) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((815.6667) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((816.0000) * transformY) + moveY),

        new createjs.Point(((692.6666) * transformX) + moveX, ((816.0000) * transformY) + moveY),
        new createjs.Point(((693.3334) * transformX) + moveX, ((816.0000) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((816.0000) * transformY) + moveY),

        new createjs.Point(((694.0000) * transformX) + moveX, ((816.3333) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((816.6667) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((817.0000) * transformY) + moveY),

        new createjs.Point(((694.9999) * transformX) + moveX, ((817.0000) * transformY) + moveY),
        new createjs.Point(((696.0001) * transformX) + moveX, ((817.0000) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((817.0000) * transformY) + moveY),

        new createjs.Point(((697.0000) * transformX) + moveX, ((817.3333) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((817.6667) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((697.6666) * transformX) + moveX, ((818.0000) * transformY) + moveY),
        new createjs.Point(((698.3334) * transformX) + moveX, ((818.0000) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((699.0000) * transformX) + moveX, ((818.3333) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((818.6667) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((819.0000) * transformY) + moveY),

        new createjs.Point(((699.9999) * transformX) + moveX, ((819.0000) * transformY) + moveY),
        new createjs.Point(((701.0001) * transformX) + moveX, ((819.0000) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((819.0000) * transformY) + moveY),

        new createjs.Point(((702.0000) * transformX) + moveX, ((819.3333) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((819.6667) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((820.0000) * transformY) + moveY),

        new createjs.Point(((703.3332) * transformX) + moveX, ((820.3333) * transformY) + moveY),
        new createjs.Point(((704.6668) * transformX) + moveX, ((820.6667) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((821.0000) * transformY) + moveY),

        new createjs.Point(((706.0000) * transformX) + moveX, ((821.3333) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((821.6667) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((822.0000) * transformY) + moveY),

        new createjs.Point(((706.9999) * transformX) + moveX, ((822.0000) * transformY) + moveY),
        new createjs.Point(((708.0001) * transformX) + moveX, ((822.0000) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((822.0000) * transformY) + moveY),

        new createjs.Point(((709.0000) * transformX) + moveX, ((822.3333) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((822.6667) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((823.0000) * transformY) + moveY),

        new createjs.Point(((710.9998) * transformX) + moveX, ((823.6666) * transformY) + moveY),
        new createjs.Point(((713.0002) * transformX) + moveX, ((824.3334) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((825.0000) * transformY) + moveY),

        new createjs.Point(((715.0000) * transformX) + moveX, ((825.3333) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((825.6667) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((826.0000) * transformY) + moveY),

        new createjs.Point(((715.6666) * transformX) + moveX, ((826.0000) * transformY) + moveY),
        new createjs.Point(((716.3334) * transformX) + moveX, ((826.0000) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((826.0000) * transformY) + moveY),

        new createjs.Point(((717.0000) * transformX) + moveX, ((826.3333) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((826.6667) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((827.0000) * transformY) + moveY),

        new createjs.Point(((717.6666) * transformX) + moveX, ((827.0000) * transformY) + moveY),
        new createjs.Point(((718.3334) * transformX) + moveX, ((827.0000) * transformY) + moveY),
        new createjs.Point(((719.0000) * transformX) + moveX, ((827.0000) * transformY) + moveY),

        new createjs.Point(((719.0000) * transformX) + moveX, ((827.3333) * transformY) + moveY),
        new createjs.Point(((719.0000) * transformX) + moveX, ((827.6667) * transformY) + moveY),
        new createjs.Point(((719.0000) * transformX) + moveX, ((828.0000) * transformY) + moveY),

        new createjs.Point(((719.6666) * transformX) + moveX, ((828.0000) * transformY) + moveY),
        new createjs.Point(((720.3334) * transformX) + moveX, ((828.0000) * transformY) + moveY),
        new createjs.Point(((721.0000) * transformX) + moveX, ((828.0000) * transformY) + moveY),

        new createjs.Point(((721.0000) * transformX) + moveX, ((828.3333) * transformY) + moveY),
        new createjs.Point(((721.0000) * transformX) + moveX, ((828.6667) * transformY) + moveY),
        new createjs.Point(((721.0000) * transformX) + moveX, ((829.0000) * transformY) + moveY),

        new createjs.Point(((721.6666) * transformX) + moveX, ((829.0000) * transformY) + moveY),
        new createjs.Point(((722.3334) * transformX) + moveX, ((829.0000) * transformY) + moveY),
        new createjs.Point(((723.0000) * transformX) + moveX, ((829.0000) * transformY) + moveY),

        new createjs.Point(((723.0000) * transformX) + moveX, ((829.3333) * transformY) + moveY),
        new createjs.Point(((723.0000) * transformX) + moveX, ((829.6667) * transformY) + moveY),
        new createjs.Point(((723.0000) * transformX) + moveX, ((830.0000) * transformY) + moveY),

        new createjs.Point(((723.6666) * transformX) + moveX, ((830.0000) * transformY) + moveY),
        new createjs.Point(((724.3334) * transformX) + moveX, ((830.0000) * transformY) + moveY),
        new createjs.Point(((725.0000) * transformX) + moveX, ((830.0000) * transformY) + moveY),

        new createjs.Point(((725.0000) * transformX) + moveX, ((830.3333) * transformY) + moveY),
        new createjs.Point(((725.0000) * transformX) + moveX, ((830.6667) * transformY) + moveY),
        new createjs.Point(((725.0000) * transformX) + moveX, ((831.0000) * transformY) + moveY),

        new createjs.Point(((725.6666) * transformX) + moveX, ((831.0000) * transformY) + moveY),
        new createjs.Point(((726.3334) * transformX) + moveX, ((831.0000) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((831.0000) * transformY) + moveY),

        new createjs.Point(((727.0000) * transformX) + moveX, ((831.3333) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((831.6667) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((832.0000) * transformY) + moveY),

        new createjs.Point(((727.6666) * transformX) + moveX, ((832.0000) * transformY) + moveY),
        new createjs.Point(((728.3334) * transformX) + moveX, ((832.0000) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((832.0000) * transformY) + moveY),

        new createjs.Point(((729.0000) * transformX) + moveX, ((832.3333) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((832.6667) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((833.0000) * transformY) + moveY),

        new createjs.Point(((729.6666) * transformX) + moveX, ((833.0000) * transformY) + moveY),
        new createjs.Point(((730.3334) * transformX) + moveX, ((833.0000) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((833.0000) * transformY) + moveY),

        new createjs.Point(((731.0000) * transformX) + moveX, ((833.3333) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((833.6667) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((834.0000) * transformY) + moveY),

        new createjs.Point(((731.6666) * transformX) + moveX, ((834.0000) * transformY) + moveY),
        new createjs.Point(((732.3334) * transformX) + moveX, ((834.0000) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((834.0000) * transformY) + moveY),

        new createjs.Point(((733.3333) * transformX) + moveX, ((834.6666) * transformY) + moveY),
        new createjs.Point(((733.6667) * transformX) + moveX, ((835.3334) * transformY) + moveY),
        new createjs.Point(((734.0000) * transformX) + moveX, ((836.0000) * transformY) + moveY),

        new createjs.Point(((735.9998) * transformX) + moveX, ((836.6666) * transformY) + moveY),
        new createjs.Point(((738.0002) * transformX) + moveX, ((837.3334) * transformY) + moveY),
        new createjs.Point(((740.0000) * transformX) + moveX, ((838.0000) * transformY) + moveY),

        new createjs.Point(((740.3333) * transformX) + moveX, ((838.6666) * transformY) + moveY),
        new createjs.Point(((740.6667) * transformX) + moveX, ((839.3334) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((840.0000) * transformY) + moveY),

        new createjs.Point(((742.3332) * transformX) + moveX, ((840.3333) * transformY) + moveY),
        new createjs.Point(((743.6668) * transformX) + moveX, ((840.6667) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((841.0000) * transformY) + moveY),

        new createjs.Point(((745.3333) * transformX) + moveX, ((841.6666) * transformY) + moveY),
        new createjs.Point(((745.6667) * transformX) + moveX, ((842.3334) * transformY) + moveY),
        new createjs.Point(((746.0000) * transformX) + moveX, ((843.0000) * transformY) + moveY),

        new createjs.Point(((746.6666) * transformX) + moveX, ((843.0000) * transformY) + moveY),
        new createjs.Point(((747.3334) * transformX) + moveX, ((843.0000) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((843.0000) * transformY) + moveY),

        new createjs.Point(((748.3333) * transformX) + moveX, ((843.6666) * transformY) + moveY),
        new createjs.Point(((748.6667) * transformX) + moveX, ((844.3334) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((845.0000) * transformY) + moveY),

        new createjs.Point(((749.6666) * transformX) + moveX, ((845.0000) * transformY) + moveY),
        new createjs.Point(((750.3334) * transformX) + moveX, ((845.0000) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((845.0000) * transformY) + moveY),

        new createjs.Point(((751.0000) * transformX) + moveX, ((845.3333) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((845.6667) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((846.0000) * transformY) + moveY),

        new createjs.Point(((751.9999) * transformX) + moveX, ((846.3333) * transformY) + moveY),
        new createjs.Point(((753.0001) * transformX) + moveX, ((846.6667) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((847.0000) * transformY) + moveY),

        new createjs.Point(((754.3333) * transformX) + moveX, ((847.6666) * transformY) + moveY),
        new createjs.Point(((754.6667) * transformX) + moveX, ((848.3334) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((849.0000) * transformY) + moveY),

        new createjs.Point(((755.6666) * transformX) + moveX, ((849.0000) * transformY) + moveY),
        new createjs.Point(((756.3334) * transformX) + moveX, ((849.0000) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((849.0000) * transformY) + moveY),

        new createjs.Point(((757.3333) * transformX) + moveX, ((849.6666) * transformY) + moveY),
        new createjs.Point(((757.6667) * transformX) + moveX, ((850.3334) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((851.0000) * transformY) + moveY),

        new createjs.Point(((758.6666) * transformX) + moveX, ((851.0000) * transformY) + moveY),
        new createjs.Point(((759.3334) * transformX) + moveX, ((851.0000) * transformY) + moveY),
        new createjs.Point(((760.0000) * transformX) + moveX, ((851.0000) * transformY) + moveY),

        new createjs.Point(((760.6666) * transformX) + moveX, ((851.9999) * transformY) + moveY),
        new createjs.Point(((761.3334) * transformX) + moveX, ((853.0001) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((854.0000) * transformY) + moveY),

        new createjs.Point(((762.6666) * transformX) + moveX, ((854.0000) * transformY) + moveY),
        new createjs.Point(((763.3334) * transformX) + moveX, ((854.0000) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((854.0000) * transformY) + moveY),

        new createjs.Point(((764.3333) * transformX) + moveX, ((854.6666) * transformY) + moveY),
        new createjs.Point(((764.6667) * transformX) + moveX, ((855.3334) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((856.0000) * transformY) + moveY),

        new createjs.Point(((765.6666) * transformX) + moveX, ((856.0000) * transformY) + moveY),
        new createjs.Point(((766.3334) * transformX) + moveX, ((856.0000) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((856.0000) * transformY) + moveY),

        new createjs.Point(((767.9999) * transformX) + moveX, ((857.3332) * transformY) + moveY),
        new createjs.Point(((769.0001) * transformX) + moveX, ((858.6668) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((860.0000) * transformY) + moveY),

        new createjs.Point(((770.6666) * transformX) + moveX, ((860.0000) * transformY) + moveY),
        new createjs.Point(((771.3334) * transformX) + moveX, ((860.0000) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((860.0000) * transformY) + moveY),

        new createjs.Point(((772.9999) * transformX) + moveX, ((861.3332) * transformY) + moveY),
        new createjs.Point(((774.0001) * transformX) + moveX, ((862.6668) * transformY) + moveY),
        new createjs.Point(((775.0000) * transformX) + moveX, ((864.0000) * transformY) + moveY),

        new createjs.Point(((775.6666) * transformX) + moveX, ((864.0000) * transformY) + moveY),
        new createjs.Point(((776.3334) * transformX) + moveX, ((864.0000) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((864.0000) * transformY) + moveY),

        new createjs.Point(((778.3332) * transformX) + moveX, ((865.6665) * transformY) + moveY),
        new createjs.Point(((779.6668) * transformX) + moveX, ((867.3335) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((869.0000) * transformY) + moveY),

        new createjs.Point(((781.6666) * transformX) + moveX, ((869.0000) * transformY) + moveY),
        new createjs.Point(((782.3334) * transformX) + moveX, ((869.0000) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((869.0000) * transformY) + moveY),

        new createjs.Point(((785.9997) * transformX) + moveX, ((872.3330) * transformY) + moveY),
        new createjs.Point(((789.0003) * transformX) + moveX, ((875.6670) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((879.0000) * transformY) + moveY),

        new createjs.Point(((792.3333) * transformX) + moveX, ((879.0000) * transformY) + moveY),
        new createjs.Point(((792.6667) * transformX) + moveX, ((879.0000) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((879.0000) * transformY) + moveY),

        new createjs.Point(((793.3333) * transformX) + moveX, ((879.6666) * transformY) + moveY),
        new createjs.Point(((793.6667) * transformX) + moveX, ((880.3334) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((881.0000) * transformY) + moveY),

        new createjs.Point(((794.6666) * transformX) + moveX, ((881.0000) * transformY) + moveY),
        new createjs.Point(((795.3334) * transformX) + moveX, ((881.0000) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((881.0000) * transformY) + moveY),

        new createjs.Point(((796.6666) * transformX) + moveX, ((881.9999) * transformY) + moveY),
        new createjs.Point(((797.3334) * transformX) + moveX, ((883.0001) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((884.0000) * transformY) + moveY),

        new createjs.Point(((798.3333) * transformX) + moveX, ((884.0000) * transformY) + moveY),
        new createjs.Point(((798.6667) * transformX) + moveX, ((884.0000) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((884.0000) * transformY) + moveY),

        new createjs.Point(((799.0000) * transformX) + moveX, ((884.6666) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((885.3334) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((886.0000) * transformY) + moveY),

        new createjs.Point(((802.9996) * transformX) + moveX, ((889.6663) * transformY) + moveY),
        new createjs.Point(((807.0004) * transformX) + moveX, ((893.3337) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((897.0000) * transformY) + moveY),

        new createjs.Point(((811.0000) * transformX) + moveX, ((897.6666) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((898.3334) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((899.0000) * transformY) + moveY),

        new createjs.Point(((812.9998) * transformX) + moveX, ((900.6665) * transformY) + moveY),
        new createjs.Point(((815.0002) * transformX) + moveX, ((902.3335) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((904.0000) * transformY) + moveY),

        new createjs.Point(((817.0000) * transformX) + moveX, ((904.6666) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((905.3334) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((906.0000) * transformY) + moveY),

        new createjs.Point(((818.3332) * transformX) + moveX, ((906.9999) * transformY) + moveY),
        new createjs.Point(((819.6668) * transformX) + moveX, ((908.0001) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((909.0000) * transformY) + moveY),

        new createjs.Point(((821.0000) * transformX) + moveX, ((909.6666) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((910.3334) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((911.0000) * transformY) + moveY),

        new createjs.Point(((821.9999) * transformX) + moveX, ((911.6666) * transformY) + moveY),
        new createjs.Point(((823.0001) * transformX) + moveX, ((912.3334) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((913.0000) * transformY) + moveY),

        new createjs.Point(((824.0000) * transformX) + moveX, ((913.6666) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((914.3334) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((915.0000) * transformY) + moveY),

        new createjs.Point(((824.9999) * transformX) + moveX, ((915.6666) * transformY) + moveY),
        new createjs.Point(((826.0001) * transformX) + moveX, ((916.3334) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),

        new createjs.Point(((827.0000) * transformX) + moveX, ((917.6666) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((918.3334) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((919.0000) * transformY) + moveY),

        new createjs.Point(((827.9999) * transformX) + moveX, ((919.6666) * transformY) + moveY),
        new createjs.Point(((829.0001) * transformX) + moveX, ((920.3334) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((921.0000) * transformY) + moveY),

        new createjs.Point(((830.0000) * transformX) + moveX, ((921.6666) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((922.3334) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((923.0000) * transformY) + moveY),

        new createjs.Point(((830.6666) * transformX) + moveX, ((923.3333) * transformY) + moveY),
        new createjs.Point(((831.3334) * transformX) + moveX, ((923.6667) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((924.0000) * transformY) + moveY),

        new createjs.Point(((832.0000) * transformX) + moveX, ((924.6666) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((925.3334) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((926.0000) * transformY) + moveY),

        new createjs.Point(((832.6666) * transformX) + moveX, ((926.3333) * transformY) + moveY),
        new createjs.Point(((833.3334) * transformX) + moveX, ((926.6667) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((927.0000) * transformY) + moveY),

        new createjs.Point(((834.0000) * transformX) + moveX, ((927.6666) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((928.3334) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((929.0000) * transformY) + moveY),

        new createjs.Point(((834.6666) * transformX) + moveX, ((929.3333) * transformY) + moveY),
        new createjs.Point(((835.3334) * transformX) + moveX, ((929.6667) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((930.0000) * transformY) + moveY),

        new createjs.Point(((836.0000) * transformX) + moveX, ((930.6666) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((931.3334) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((932.0000) * transformY) + moveY),

        new createjs.Point(((836.6666) * transformX) + moveX, ((932.3333) * transformY) + moveY),
        new createjs.Point(((837.3334) * transformX) + moveX, ((932.6667) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((933.0000) * transformY) + moveY),

        new createjs.Point(((838.0000) * transformX) + moveX, ((933.6666) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((934.3334) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((935.0000) * transformY) + moveY),

        new createjs.Point(((838.6666) * transformX) + moveX, ((935.3333) * transformY) + moveY),
        new createjs.Point(((839.3334) * transformX) + moveX, ((935.6667) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((936.0000) * transformY) + moveY),

        new createjs.Point(((840.0000) * transformX) + moveX, ((936.6666) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((937.3334) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((938.0000) * transformY) + moveY),

        new createjs.Point(((840.3333) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((840.6667) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((938.0000) * transformY) + moveY),

        new createjs.Point(((841.0000) * transformX) + moveX, ((938.6666) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((939.3334) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((940.0000) * transformY) + moveY),

        new createjs.Point(((841.6666) * transformX) + moveX, ((940.3333) * transformY) + moveY),
        new createjs.Point(((842.3334) * transformX) + moveX, ((940.6667) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((941.0000) * transformY) + moveY),

        new createjs.Point(((843.3333) * transformX) + moveX, ((942.3332) * transformY) + moveY),
        new createjs.Point(((843.6667) * transformX) + moveX, ((943.6668) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((945.0000) * transformY) + moveY),

        new createjs.Point(((844.6666) * transformX) + moveX, ((945.3333) * transformY) + moveY),
        new createjs.Point(((845.3334) * transformX) + moveX, ((945.6667) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((946.0000) * transformY) + moveY),

        new createjs.Point(((846.6666) * transformX) + moveX, ((947.9998) * transformY) + moveY),
        new createjs.Point(((847.3334) * transformX) + moveX, ((950.0002) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),

        new createjs.Point(((848.6666) * transformX) + moveX, ((952.3333) * transformY) + moveY),
        new createjs.Point(((849.3334) * transformX) + moveX, ((952.6667) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((953.0000) * transformY) + moveY),

        new createjs.Point(((850.0000) * transformX) + moveX, ((953.6666) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((954.3334) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((955.0000) * transformY) + moveY),

        new createjs.Point(((850.3333) * transformX) + moveX, ((955.0000) * transformY) + moveY),
        new createjs.Point(((850.6667) * transformX) + moveX, ((955.0000) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((955.0000) * transformY) + moveY),

        new createjs.Point(((851.0000) * transformX) + moveX, ((955.6666) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((956.3334) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((957.0000) * transformY) + moveY),

        new createjs.Point(((851.3333) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((851.6667) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((957.0000) * transformY) + moveY),

        new createjs.Point(((852.0000) * transformX) + moveX, ((957.6666) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((958.3334) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((959.0000) * transformY) + moveY),

        new createjs.Point(((852.3333) * transformX) + moveX, ((959.0000) * transformY) + moveY),
        new createjs.Point(((852.6667) * transformX) + moveX, ((959.0000) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((959.0000) * transformY) + moveY),

        new createjs.Point(((853.0000) * transformX) + moveX, ((959.6666) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((960.3334) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((961.0000) * transformY) + moveY),

        new createjs.Point(((853.3333) * transformX) + moveX, ((961.0000) * transformY) + moveY),
        new createjs.Point(((853.6667) * transformX) + moveX, ((961.0000) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((961.0000) * transformY) + moveY),

        new createjs.Point(((854.0000) * transformX) + moveX, ((961.6666) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((962.3334) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((963.0000) * transformY) + moveY),

        new createjs.Point(((854.3333) * transformX) + moveX, ((963.0000) * transformY) + moveY),
        new createjs.Point(((854.6667) * transformX) + moveX, ((963.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((963.0000) * transformY) + moveY),

        new createjs.Point(((855.0000) * transformX) + moveX, ((963.6666) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((964.3334) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((965.0000) * transformY) + moveY),

        new createjs.Point(((855.3333) * transformX) + moveX, ((965.0000) * transformY) + moveY),
        new createjs.Point(((855.6667) * transformX) + moveX, ((965.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((965.0000) * transformY) + moveY),

        new createjs.Point(((856.0000) * transformX) + moveX, ((965.6666) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((966.3334) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),

        new createjs.Point(((856.3333) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((856.6667) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),

        new createjs.Point(((857.0000) * transformX) + moveX, ((967.6666) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((968.3334) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((969.0000) * transformY) + moveY),

        new createjs.Point(((857.3333) * transformX) + moveX, ((969.0000) * transformY) + moveY),
        new createjs.Point(((857.6667) * transformX) + moveX, ((969.0000) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((969.0000) * transformY) + moveY),

        new createjs.Point(((858.0000) * transformX) + moveX, ((969.6666) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((970.3334) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),

        new createjs.Point(((858.3333) * transformX) + moveX, ((971.0000) * transformY) + moveY),
        new createjs.Point(((858.6667) * transformX) + moveX, ((971.0000) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),

        new createjs.Point(((859.6666) * transformX) + moveX, ((972.9998) * transformY) + moveY),
        new createjs.Point(((860.3334) * transformX) + moveX, ((975.0002) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((977.0000) * transformY) + moveY),

        new createjs.Point(((861.3333) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((861.6667) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((977.0000) * transformY) + moveY),

        new createjs.Point(((862.0000) * transformX) + moveX, ((977.9999) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((979.0001) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((980.0000) * transformY) + moveY),

        new createjs.Point(((862.3333) * transformX) + moveX, ((980.0000) * transformY) + moveY),
        new createjs.Point(((862.6667) * transformX) + moveX, ((980.0000) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((980.0000) * transformY) + moveY),

        new createjs.Point(((863.3333) * transformX) + moveX, ((981.3332) * transformY) + moveY),
        new createjs.Point(((863.6667) * transformX) + moveX, ((982.6668) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),

        new createjs.Point(((864.3333) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((864.6667) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),

        new createjs.Point(((865.0000) * transformX) + moveX, ((984.9999) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((986.0001) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((987.0000) * transformY) + moveY),

        new createjs.Point(((865.3333) * transformX) + moveX, ((987.0000) * transformY) + moveY),
        new createjs.Point(((865.6667) * transformX) + moveX, ((987.0000) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((987.0000) * transformY) + moveY),

        new createjs.Point(((866.3333) * transformX) + moveX, ((988.3332) * transformY) + moveY),
        new createjs.Point(((866.6667) * transformX) + moveX, ((989.6668) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((991.0000) * transformY) + moveY),

        new createjs.Point(((867.3333) * transformX) + moveX, ((991.0000) * transformY) + moveY),
        new createjs.Point(((867.6667) * transformX) + moveX, ((991.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((991.0000) * transformY) + moveY),

        new createjs.Point(((868.3333) * transformX) + moveX, ((992.9998) * transformY) + moveY),
        new createjs.Point(((868.6667) * transformX) + moveX, ((995.0002) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((997.0000) * transformY) + moveY),

        new createjs.Point(((869.3333) * transformX) + moveX, ((997.0000) * transformY) + moveY),
        new createjs.Point(((869.6667) * transformX) + moveX, ((997.0000) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((997.0000) * transformY) + moveY),

        new createjs.Point(((870.0000) * transformX) + moveX, ((997.6666) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((998.3334) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((999.0000) * transformY) + moveY),

        new createjs.Point(((870.3333) * transformX) + moveX, ((999.0000) * transformY) + moveY),
        new createjs.Point(((870.6667) * transformX) + moveX, ((999.0000) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((999.0000) * transformY) + moveY),

        new createjs.Point(((874.9996) * transformX) + moveX, ((1010.9988) * transformY) + moveY),
        new createjs.Point(((879.0004) * transformX) + moveX, ((1023.0012) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),

        new createjs.Point(((883.3333) * transformX) + moveX, ((1037.6664) * transformY) + moveY),
        new createjs.Point(((883.6667) * transformX) + moveX, ((1040.3336) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1043.0000) * transformY) + moveY),

        new createjs.Point(((885.6028) * transformX) + moveX, ((1047.7123) * transformY) + moveY),
        new createjs.Point(((887.4002) * transformX) + moveX, ((1054.2867) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1059.0000) * transformY) + moveY),

        new createjs.Point(((889.0000) * transformX) + moveX, ((1060.3332) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1061.6668) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1063.0000) * transformY) + moveY),

        new createjs.Point(((889.3333) * transformX) + moveX, ((1063.0000) * transformY) + moveY),
        new createjs.Point(((889.6667) * transformX) + moveX, ((1063.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1063.0000) * transformY) + moveY),

        new createjs.Point(((890.3333) * transformX) + moveX, ((1066.3330) * transformY) + moveY),
        new createjs.Point(((890.6667) * transformX) + moveX, ((1069.6670) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1073.0000) * transformY) + moveY),

        new createjs.Point(((893.6877) * transformX) + moveX, ((1081.6192) * transformY) + moveY),
        new createjs.Point(((894.4028) * transformX) + moveX, ((1093.1342) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1102.0000) * transformY) + moveY),

        new createjs.Point(((897.0000) * transformX) + moveX, ((1104.6664) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1107.3336) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1110.0000) * transformY) + moveY),

        new createjs.Point(((897.3333) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((897.6667) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1110.0000) * transformY) + moveY),

        new createjs.Point(((898.0000) * transformX) + moveX, ((1112.6664) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1115.3336) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1118.0000) * transformY) + moveY),

        new createjs.Point(((898.3333) * transformX) + moveX, ((1118.0000) * transformY) + moveY),
        new createjs.Point(((898.6667) * transformX) + moveX, ((1118.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1118.0000) * transformY) + moveY),

        new createjs.Point(((899.0000) * transformX) + moveX, ((1121.3330) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1124.6670) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1128.0000) * transformY) + moveY),

        new createjs.Point(((899.3333) * transformX) + moveX, ((1128.0000) * transformY) + moveY),
        new createjs.Point(((899.6667) * transformX) + moveX, ((1128.0000) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1128.0000) * transformY) + moveY),

        new createjs.Point(((900.0000) * transformX) + moveX, ((1131.6663) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1135.3337) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1139.0000) * transformY) + moveY),

        new createjs.Point(((900.3333) * transformX) + moveX, ((1139.0000) * transformY) + moveY),
        new createjs.Point(((900.6667) * transformX) + moveX, ((1139.0000) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1139.0000) * transformY) + moveY),

        new createjs.Point(((901.0000) * transformX) + moveX, ((1143.9995) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1149.0005) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1154.0000) * transformY) + moveY),

        new createjs.Point(((901.3333) * transformX) + moveX, ((1154.0000) * transformY) + moveY),
        new createjs.Point(((901.6667) * transformX) + moveX, ((1154.0000) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1154.0000) * transformY) + moveY),

        new createjs.Point(((902.0000) * transformX) + moveX, ((1163.6657) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1173.3343) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1183.0000) * transformY) + moveY),

        new createjs.Point(((902.3333) * transformX) + moveX, ((1183.0000) * transformY) + moveY),
        new createjs.Point(((902.6667) * transformX) + moveX, ((1183.0000) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((1183.0000) * transformY) + moveY),

        new createjs.Point(((904.5633) * transformX) + moveX, ((1188.4822) * transformY) + moveY),
        new createjs.Point(((902.6522) * transformX) + moveX, ((1197.9814) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1202.0000) * transformY) + moveY),

        new createjs.Point(((900.5844) * transformX) + moveX, ((1210.7223) * transformY) + moveY),
        new createjs.Point(((903.0736) * transformX) + moveX, ((1221.7216) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1229.0000) * transformY) + moveY),

        new createjs.Point(((900.6667) * transformX) + moveX, ((1237.9991) * transformY) + moveY),
        new createjs.Point(((900.3333) * transformX) + moveX, ((1247.0009) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1256.0000) * transformY) + moveY),

        new createjs.Point(((899.6667) * transformX) + moveX, ((1256.0000) * transformY) + moveY),
        new createjs.Point(((899.3333) * transformX) + moveX, ((1256.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1256.0000) * transformY) + moveY),

        new createjs.Point(((899.0000) * transformX) + moveX, ((1258.9997) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1262.0003) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1265.0000) * transformY) + moveY),

        new createjs.Point(((898.6667) * transformX) + moveX, ((1265.0000) * transformY) + moveY),
        new createjs.Point(((898.3333) * transformX) + moveX, ((1265.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1265.0000) * transformY) + moveY),

        new createjs.Point(((898.0000) * transformX) + moveX, ((1267.9997) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1271.0003) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1274.0000) * transformY) + moveY),

        new createjs.Point(((895.6049) * transformX) + moveX, ((1282.2131) * transformY) + moveY),
        new createjs.Point(((895.3899) * transformX) + moveX, ((1293.0478) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1301.0000) * transformY) + moveY),

        new createjs.Point(((892.6667) * transformX) + moveX, ((1304.9996) * transformY) + moveY),
        new createjs.Point(((892.3333) * transformX) + moveX, ((1309.0004) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((1313.0000) * transformY) + moveY),

        new createjs.Point(((887.0005) * transformX) + moveX, ((1334.3312) * transformY) + moveY),
        new createjs.Point(((881.9995) * transformX) + moveX, ((1355.6688) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1377.0000) * transformY) + moveY),

        new createjs.Point(((870.3340) * transformX) + moveX, ((1396.6647) * transformY) + moveY),
        new createjs.Point(((863.6660) * transformX) + moveX, ((1416.3353) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1436.0000) * transformY) + moveY),

        new createjs.Point(((856.6667) * transformX) + moveX, ((1436.0000) * transformY) + moveY),
        new createjs.Point(((856.3333) * transformX) + moveX, ((1436.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1436.0000) * transformY) + moveY),

        new createjs.Point(((856.0000) * transformX) + moveX, ((1436.6666) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1437.3334) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1438.0000) * transformY) + moveY),

        new createjs.Point(((855.6667) * transformX) + moveX, ((1438.0000) * transformY) + moveY),
        new createjs.Point(((855.3333) * transformX) + moveX, ((1438.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1438.0000) * transformY) + moveY),

        new createjs.Point(((855.0000) * transformX) + moveX, ((1438.9999) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1440.0001) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1441.0000) * transformY) + moveY),

        new createjs.Point(((854.6667) * transformX) + moveX, ((1441.0000) * transformY) + moveY),
        new createjs.Point(((854.3333) * transformX) + moveX, ((1441.0000) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1441.0000) * transformY) + moveY),

        new createjs.Point(((854.0000) * transformX) + moveX, ((1441.6666) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1442.3334) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1443.0000) * transformY) + moveY),

        new createjs.Point(((853.6667) * transformX) + moveX, ((1443.0000) * transformY) + moveY),
        new createjs.Point(((853.3333) * transformX) + moveX, ((1443.0000) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1443.0000) * transformY) + moveY),

        new createjs.Point(((853.0000) * transformX) + moveX, ((1443.9999) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1445.0001) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1446.0000) * transformY) + moveY),

        new createjs.Point(((852.6667) * transformX) + moveX, ((1446.0000) * transformY) + moveY),
        new createjs.Point(((852.3333) * transformX) + moveX, ((1446.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1446.0000) * transformY) + moveY),

        new createjs.Point(((851.6667) * transformX) + moveX, ((1447.3332) * transformY) + moveY),
        new createjs.Point(((851.3333) * transformX) + moveX, ((1448.6668) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1450.0000) * transformY) + moveY),

        new createjs.Point(((850.6667) * transformX) + moveX, ((1450.0000) * transformY) + moveY),
        new createjs.Point(((850.3333) * transformX) + moveX, ((1450.0000) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1450.0000) * transformY) + moveY),

        new createjs.Point(((850.0000) * transformX) + moveX, ((1450.9999) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1452.0001) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1453.0000) * transformY) + moveY),

        new createjs.Point(((849.6667) * transformX) + moveX, ((1453.0000) * transformY) + moveY),
        new createjs.Point(((849.3333) * transformX) + moveX, ((1453.0000) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1453.0000) * transformY) + moveY),

        new createjs.Point(((848.6667) * transformX) + moveX, ((1454.3332) * transformY) + moveY),
        new createjs.Point(((848.3333) * transformX) + moveX, ((1455.6668) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((1457.0000) * transformY) + moveY),

        new createjs.Point(((847.6667) * transformX) + moveX, ((1457.0000) * transformY) + moveY),
        new createjs.Point(((847.3333) * transformX) + moveX, ((1457.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1457.0000) * transformY) + moveY),

        new createjs.Point(((847.0000) * transformX) + moveX, ((1457.9999) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1459.0001) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1460.0000) * transformY) + moveY),

        new createjs.Point(((846.6667) * transformX) + moveX, ((1460.0000) * transformY) + moveY),
        new createjs.Point(((846.3333) * transformX) + moveX, ((1460.0000) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1460.0000) * transformY) + moveY),

        new createjs.Point(((846.0000) * transformX) + moveX, ((1460.6666) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1461.3334) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1462.0000) * transformY) + moveY),

        new createjs.Point(((845.6667) * transformX) + moveX, ((1462.0000) * transformY) + moveY),
        new createjs.Point(((845.3333) * transformX) + moveX, ((1462.0000) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1462.0000) * transformY) + moveY),

        new createjs.Point(((844.6667) * transformX) + moveX, ((1463.3332) * transformY) + moveY),
        new createjs.Point(((844.3333) * transformX) + moveX, ((1464.6668) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1466.0000) * transformY) + moveY),

        new createjs.Point(((843.6667) * transformX) + moveX, ((1466.0000) * transformY) + moveY),
        new createjs.Point(((843.3333) * transformX) + moveX, ((1466.0000) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1466.0000) * transformY) + moveY),

        new createjs.Point(((843.0000) * transformX) + moveX, ((1466.9999) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1468.0001) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1469.0000) * transformY) + moveY),

        new createjs.Point(((842.6667) * transformX) + moveX, ((1469.0000) * transformY) + moveY),
        new createjs.Point(((842.3333) * transformX) + moveX, ((1469.0000) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1469.0000) * transformY) + moveY),

        new createjs.Point(((842.0000) * transformX) + moveX, ((1469.6666) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1470.3334) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1471.0000) * transformY) + moveY),

        new createjs.Point(((841.6667) * transformX) + moveX, ((1471.0000) * transformY) + moveY),
        new createjs.Point(((841.3333) * transformX) + moveX, ((1471.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1471.0000) * transformY) + moveY),

        new createjs.Point(((840.6667) * transformX) + moveX, ((1472.3332) * transformY) + moveY),
        new createjs.Point(((840.3333) * transformX) + moveX, ((1473.6668) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1475.0000) * transformY) + moveY),

        new createjs.Point(((839.6667) * transformX) + moveX, ((1475.0000) * transformY) + moveY),
        new createjs.Point(((839.3333) * transformX) + moveX, ((1475.0000) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1475.0000) * transformY) + moveY),

        new createjs.Point(((838.6667) * transformX) + moveX, ((1476.3332) * transformY) + moveY),
        new createjs.Point(((838.3333) * transformX) + moveX, ((1477.6668) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1479.0000) * transformY) + moveY),

        new createjs.Point(((837.6667) * transformX) + moveX, ((1479.0000) * transformY) + moveY),
        new createjs.Point(((837.3333) * transformX) + moveX, ((1479.0000) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1479.0000) * transformY) + moveY),

        new createjs.Point(((836.6667) * transformX) + moveX, ((1480.3332) * transformY) + moveY),
        new createjs.Point(((836.3333) * transformX) + moveX, ((1481.6668) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1483.0000) * transformY) + moveY),

        new createjs.Point(((835.6667) * transformX) + moveX, ((1483.0000) * transformY) + moveY),
        new createjs.Point(((835.3333) * transformX) + moveX, ((1483.0000) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((1483.0000) * transformY) + moveY),

        new createjs.Point(((835.0000) * transformX) + moveX, ((1483.6666) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((1484.3334) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((1485.0000) * transformY) + moveY),

        new createjs.Point(((834.6667) * transformX) + moveX, ((1485.0000) * transformY) + moveY),
        new createjs.Point(((834.3333) * transformX) + moveX, ((1485.0000) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1485.0000) * transformY) + moveY),

        new createjs.Point(((834.0000) * transformX) + moveX, ((1485.6666) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1486.3334) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1487.0000) * transformY) + moveY),

        new createjs.Point(((833.6667) * transformX) + moveX, ((1487.0000) * transformY) + moveY),
        new createjs.Point(((833.3333) * transformX) + moveX, ((1487.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1487.0000) * transformY) + moveY),

        new createjs.Point(((833.0000) * transformX) + moveX, ((1487.6666) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1488.3334) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1489.0000) * transformY) + moveY),

        new createjs.Point(((832.6667) * transformX) + moveX, ((1489.0000) * transformY) + moveY),
        new createjs.Point(((832.3333) * transformX) + moveX, ((1489.0000) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1489.0000) * transformY) + moveY),

        new createjs.Point(((832.0000) * transformX) + moveX, ((1489.6666) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1490.3334) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1491.0000) * transformY) + moveY),

        new createjs.Point(((831.6667) * transformX) + moveX, ((1491.0000) * transformY) + moveY),
        new createjs.Point(((831.3333) * transformX) + moveX, ((1491.0000) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1491.0000) * transformY) + moveY),

        new createjs.Point(((831.0000) * transformX) + moveX, ((1491.6666) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1492.3334) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1493.0000) * transformY) + moveY),

        new createjs.Point(((830.6667) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((830.3333) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1493.0000) * transformY) + moveY),

        new createjs.Point(((827.6669) * transformX) + moveX, ((1498.3328) * transformY) + moveY),
        new createjs.Point(((825.3331) * transformX) + moveX, ((1503.6672) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((1509.0000) * transformY) + moveY),

        new createjs.Point(((822.3334) * transformX) + moveX, ((1509.3333) * transformY) + moveY),
        new createjs.Point(((821.6666) * transformX) + moveX, ((1509.6667) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1510.0000) * transformY) + moveY),

        new createjs.Point(((821.0000) * transformX) + moveX, ((1510.6666) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1511.3334) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1512.0000) * transformY) + moveY),

        new createjs.Point(((820.6667) * transformX) + moveX, ((1512.0000) * transformY) + moveY),
        new createjs.Point(((820.3333) * transformX) + moveX, ((1512.0000) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1512.0000) * transformY) + moveY),

        new createjs.Point(((820.0000) * transformX) + moveX, ((1512.6666) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1513.3334) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1514.0000) * transformY) + moveY),

        new createjs.Point(((819.6667) * transformX) + moveX, ((1514.0000) * transformY) + moveY),
        new createjs.Point(((819.3333) * transformX) + moveX, ((1514.0000) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1514.0000) * transformY) + moveY),

        new createjs.Point(((819.0000) * transformX) + moveX, ((1514.6666) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1515.3334) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1516.0000) * transformY) + moveY),

        new createjs.Point(((818.6667) * transformX) + moveX, ((1516.0000) * transformY) + moveY),
        new createjs.Point(((818.3333) * transformX) + moveX, ((1516.0000) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((1516.0000) * transformY) + moveY),

        new createjs.Point(((818.0000) * transformX) + moveX, ((1516.6666) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((1517.3334) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((1518.0000) * transformY) + moveY),

        new createjs.Point(((817.3334) * transformX) + moveX, ((1518.3333) * transformY) + moveY),
        new createjs.Point(((816.6666) * transformX) + moveX, ((1518.6667) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((1519.0000) * transformY) + moveY),

        new createjs.Point(((815.3334) * transformX) + moveX, ((1520.9998) * transformY) + moveY),
        new createjs.Point(((814.6666) * transformX) + moveX, ((1523.0002) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((1525.0000) * transformY) + moveY),

        new createjs.Point(((813.3334) * transformX) + moveX, ((1525.3333) * transformY) + moveY),
        new createjs.Point(((812.6666) * transformX) + moveX, ((1525.6667) * transformY) + moveY),
        new createjs.Point(((812.0000) * transformX) + moveX, ((1526.0000) * transformY) + moveY),

        new createjs.Point(((811.6667) * transformX) + moveX, ((1527.3332) * transformY) + moveY),
        new createjs.Point(((811.3333) * transformX) + moveX, ((1528.6668) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1530.0000) * transformY) + moveY),

        new createjs.Point(((810.3334) * transformX) + moveX, ((1530.3333) * transformY) + moveY),
        new createjs.Point(((809.6666) * transformX) + moveX, ((1530.6667) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((1531.0000) * transformY) + moveY),

        new createjs.Point(((808.6667) * transformX) + moveX, ((1532.3332) * transformY) + moveY),
        new createjs.Point(((808.3333) * transformX) + moveX, ((1533.6668) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1535.0000) * transformY) + moveY),

        new createjs.Point(((807.3334) * transformX) + moveX, ((1535.3333) * transformY) + moveY),
        new createjs.Point(((806.6666) * transformX) + moveX, ((1535.6667) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1536.0000) * transformY) + moveY),

        new createjs.Point(((805.6667) * transformX) + moveX, ((1537.3332) * transformY) + moveY),
        new createjs.Point(((805.3333) * transformX) + moveX, ((1538.6668) * transformY) + moveY),
        new createjs.Point(((805.0000) * transformX) + moveX, ((1540.0000) * transformY) + moveY),

        new createjs.Point(((804.3334) * transformX) + moveX, ((1540.3333) * transformY) + moveY),
        new createjs.Point(((803.6666) * transformX) + moveX, ((1540.6667) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1541.0000) * transformY) + moveY),

        new createjs.Point(((802.6667) * transformX) + moveX, ((1542.3332) * transformY) + moveY),
        new createjs.Point(((802.3333) * transformX) + moveX, ((1543.6668) * transformY) + moveY),
        new createjs.Point(((802.0000) * transformX) + moveX, ((1545.0000) * transformY) + moveY),

        new createjs.Point(((801.3334) * transformX) + moveX, ((1545.3333) * transformY) + moveY),
        new createjs.Point(((800.6666) * transformX) + moveX, ((1545.6667) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((1546.0000) * transformY) + moveY),

        new createjs.Point(((800.0000) * transformX) + moveX, ((1546.6666) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((1547.3334) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((1548.0000) * transformY) + moveY),

        new createjs.Point(((799.3334) * transformX) + moveX, ((1548.3333) * transformY) + moveY),
        new createjs.Point(((798.6666) * transformX) + moveX, ((1548.6667) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((1549.0000) * transformY) + moveY),

        new createjs.Point(((798.0000) * transformX) + moveX, ((1549.6666) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((1550.3334) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((1551.0000) * transformY) + moveY),

        new createjs.Point(((797.3334) * transformX) + moveX, ((1551.3333) * transformY) + moveY),
        new createjs.Point(((796.6666) * transformX) + moveX, ((1551.6667) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((1552.0000) * transformY) + moveY),

        new createjs.Point(((796.0000) * transformX) + moveX, ((1552.6666) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((1553.3334) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((1554.0000) * transformY) + moveY),

        new createjs.Point(((795.3334) * transformX) + moveX, ((1554.3333) * transformY) + moveY),
        new createjs.Point(((794.6666) * transformX) + moveX, ((1554.6667) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1555.0000) * transformY) + moveY),

        new createjs.Point(((794.0000) * transformX) + moveX, ((1555.6666) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1556.3334) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1557.0000) * transformY) + moveY),

        new createjs.Point(((793.3334) * transformX) + moveX, ((1557.3333) * transformY) + moveY),
        new createjs.Point(((792.6666) * transformX) + moveX, ((1557.6667) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((1558.0000) * transformY) + moveY),

        new createjs.Point(((792.0000) * transformX) + moveX, ((1558.6666) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((1559.3334) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),

        new createjs.Point(((791.6667) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((791.3333) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),

        new createjs.Point(((790.6667) * transformX) + moveX, ((1560.9999) * transformY) + moveY),
        new createjs.Point(((790.3333) * transformX) + moveX, ((1562.0001) * transformY) + moveY),
        new createjs.Point(((790.0000) * transformX) + moveX, ((1563.0000) * transformY) + moveY),

        new createjs.Point(((789.3334) * transformX) + moveX, ((1563.3333) * transformY) + moveY),
        new createjs.Point(((788.6666) * transformX) + moveX, ((1563.6667) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1564.0000) * transformY) + moveY),

        new createjs.Point(((788.0000) * transformX) + moveX, ((1564.6666) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1565.3334) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1566.0000) * transformY) + moveY),

        new createjs.Point(((787.6667) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((787.3333) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((787.0000) * transformX) + moveX, ((1566.0000) * transformY) + moveY),

        new createjs.Point(((786.6667) * transformX) + moveX, ((1566.9999) * transformY) + moveY),
        new createjs.Point(((786.3333) * transformX) + moveX, ((1568.0001) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1569.0000) * transformY) + moveY),

        new createjs.Point(((785.3334) * transformX) + moveX, ((1569.3333) * transformY) + moveY),
        new createjs.Point(((784.6666) * transformX) + moveX, ((1569.6667) * transformY) + moveY),
        new createjs.Point(((784.0000) * transformX) + moveX, ((1570.0000) * transformY) + moveY),

        new createjs.Point(((784.0000) * transformX) + moveX, ((1570.6666) * transformY) + moveY),
        new createjs.Point(((784.0000) * transformX) + moveX, ((1571.3334) * transformY) + moveY),
        new createjs.Point(((784.0000) * transformX) + moveX, ((1572.0000) * transformY) + moveY),

        new createjs.Point(((783.0001) * transformX) + moveX, ((1572.6666) * transformY) + moveY),
        new createjs.Point(((781.9999) * transformX) + moveX, ((1573.3334) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1574.0000) * transformY) + moveY),

        new createjs.Point(((781.0000) * transformX) + moveX, ((1574.6666) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1575.3334) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1576.0000) * transformY) + moveY),

        new createjs.Point(((780.3334) * transformX) + moveX, ((1576.3333) * transformY) + moveY),
        new createjs.Point(((779.6666) * transformX) + moveX, ((1576.6667) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((1577.0000) * transformY) + moveY),

        new createjs.Point(((779.0000) * transformX) + moveX, ((1577.6666) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((1578.3334) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((1579.0000) * transformY) + moveY),

        new createjs.Point(((778.3334) * transformX) + moveX, ((1579.3333) * transformY) + moveY),
        new createjs.Point(((777.6666) * transformX) + moveX, ((1579.6667) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((1580.0000) * transformY) + moveY),

        new createjs.Point(((776.6667) * transformX) + moveX, ((1580.9999) * transformY) + moveY),
        new createjs.Point(((776.3333) * transformX) + moveX, ((1582.0001) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((1583.0000) * transformY) + moveY),

        new createjs.Point(((775.0001) * transformX) + moveX, ((1583.6666) * transformY) + moveY),
        new createjs.Point(((773.9999) * transformX) + moveX, ((1584.3334) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((1585.0000) * transformY) + moveY),

        new createjs.Point(((773.0000) * transformX) + moveX, ((1585.6666) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((1586.3334) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((1587.0000) * transformY) + moveY),

        new createjs.Point(((772.0001) * transformX) + moveX, ((1587.6666) * transformY) + moveY),
        new createjs.Point(((770.9999) * transformX) + moveX, ((1588.3334) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1589.0000) * transformY) + moveY),

        new createjs.Point(((770.0000) * transformX) + moveX, ((1589.6666) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1590.3334) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1591.0000) * transformY) + moveY),

        new createjs.Point(((769.0001) * transformX) + moveX, ((1591.6666) * transformY) + moveY),
        new createjs.Point(((767.9999) * transformX) + moveX, ((1592.3334) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((1593.0000) * transformY) + moveY),

        new createjs.Point(((767.0000) * transformX) + moveX, ((1593.6666) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((1594.3334) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((1595.0000) * transformY) + moveY),

        new createjs.Point(((765.6668) * transformX) + moveX, ((1595.9999) * transformY) + moveY),
        new createjs.Point(((764.3332) * transformX) + moveX, ((1597.0001) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1598.0000) * transformY) + moveY),

        new createjs.Point(((763.0000) * transformX) + moveX, ((1598.6666) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1599.3334) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1600.0000) * transformY) + moveY),

        new createjs.Point(((761.6668) * transformX) + moveX, ((1600.9999) * transformY) + moveY),
        new createjs.Point(((760.3332) * transformX) + moveX, ((1602.0001) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),

        new createjs.Point(((759.0000) * transformX) + moveX, ((1603.6666) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((1604.3334) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((1605.0000) * transformY) + moveY),

        new createjs.Point(((757.6668) * transformX) + moveX, ((1605.9999) * transformY) + moveY),
        new createjs.Point(((756.3332) * transformX) + moveX, ((1607.0001) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((1608.0000) * transformY) + moveY),

        new createjs.Point(((755.0000) * transformX) + moveX, ((1608.6666) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((1609.3334) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((1610.0000) * transformY) + moveY),

        new createjs.Point(((753.3335) * transformX) + moveX, ((1611.3332) * transformY) + moveY),
        new createjs.Point(((751.6665) * transformX) + moveX, ((1612.6668) * transformY) + moveY),
        new createjs.Point(((750.0000) * transformX) + moveX, ((1614.0000) * transformY) + moveY),

        new createjs.Point(((750.0000) * transformX) + moveX, ((1614.6666) * transformY) + moveY),
        new createjs.Point(((750.0000) * transformX) + moveX, ((1615.3334) * transformY) + moveY),
        new createjs.Point(((750.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),

        new createjs.Point(((748.0002) * transformX) + moveX, ((1617.6665) * transformY) + moveY),
        new createjs.Point(((745.9998) * transformX) + moveX, ((1619.3335) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((1621.0000) * transformY) + moveY),

        new createjs.Point(((744.0000) * transformX) + moveX, ((1621.6666) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((1622.3334) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((1623.0000) * transformY) + moveY),

        new createjs.Point(((741.6669) * transformX) + moveX, ((1624.9998) * transformY) + moveY),
        new createjs.Point(((739.3331) * transformX) + moveX, ((1627.0002) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((1629.0000) * transformY) + moveY),

        new createjs.Point(((737.0000) * transformX) + moveX, ((1629.6666) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((1630.3334) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),

        new createjs.Point(((736.3334) * transformX) + moveX, ((1631.3333) * transformY) + moveY),
        new createjs.Point(((735.6666) * transformX) + moveX, ((1631.6667) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((1632.0000) * transformY) + moveY),

        new createjs.Point(((734.0001) * transformX) + moveX, ((1633.3332) * transformY) + moveY),
        new createjs.Point(((732.9999) * transformX) + moveX, ((1634.6668) * transformY) + moveY),
        new createjs.Point(((732.0000) * transformX) + moveX, ((1636.0000) * transformY) + moveY),

        new createjs.Point(((729.3336) * transformX) + moveX, ((1638.3331) * transformY) + moveY),
        new createjs.Point(((726.6664) * transformX) + moveX, ((1640.6669) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((1643.0000) * transformY) + moveY),

        new createjs.Point(((724.0000) * transformX) + moveX, ((1643.6666) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((1644.3334) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((1645.0000) * transformY) + moveY),

        new createjs.Point(((721.0003) * transformX) + moveX, ((1647.6664) * transformY) + moveY),
        new createjs.Point(((717.9997) * transformX) + moveX, ((1650.3336) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((1653.0000) * transformY) + moveY),

        new createjs.Point(((712.0003) * transformX) + moveX, ((1656.3330) * transformY) + moveY),
        new createjs.Point(((708.9997) * transformX) + moveX, ((1659.6670) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((1663.0000) * transformY) + moveY),

        new createjs.Point(((705.3334) * transformX) + moveX, ((1663.0000) * transformY) + moveY),
        new createjs.Point(((704.6666) * transformX) + moveX, ((1663.0000) * transformY) + moveY),
        new createjs.Point(((704.0000) * transformX) + moveX, ((1663.0000) * transformY) + moveY),

        new createjs.Point(((701.6669) * transformX) + moveX, ((1665.6664) * transformY) + moveY),
        new createjs.Point(((699.3331) * transformX) + moveX, ((1668.3336) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((1671.0000) * transformY) + moveY),

        new createjs.Point(((695.6668) * transformX) + moveX, ((1671.9999) * transformY) + moveY),
        new createjs.Point(((694.3332) * transformX) + moveX, ((1673.0001) * transformY) + moveY),
        new createjs.Point(((693.0000) * transformX) + moveX, ((1674.0000) * transformY) + moveY),

        new createjs.Point(((692.6667) * transformX) + moveX, ((1674.6666) * transformY) + moveY),
        new createjs.Point(((692.3333) * transformX) + moveX, ((1675.3334) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((1676.0000) * transformY) + moveY),

        new createjs.Point(((691.3334) * transformX) + moveX, ((1676.0000) * transformY) + moveY),
        new createjs.Point(((690.6666) * transformX) + moveX, ((1676.0000) * transformY) + moveY),
        new createjs.Point(((690.0000) * transformX) + moveX, ((1676.0000) * transformY) + moveY),

        new createjs.Point(((688.0002) * transformX) + moveX, ((1678.3331) * transformY) + moveY),
        new createjs.Point(((685.9998) * transformX) + moveX, ((1680.6669) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((1683.0000) * transformY) + moveY),

        new createjs.Point(((683.3334) * transformX) + moveX, ((1683.0000) * transformY) + moveY),
        new createjs.Point(((682.6666) * transformX) + moveX, ((1683.0000) * transformY) + moveY),
        new createjs.Point(((682.0000) * transformX) + moveX, ((1683.0000) * transformY) + moveY),

        new createjs.Point(((680.3335) * transformX) + moveX, ((1684.9998) * transformY) + moveY),
        new createjs.Point(((678.6665) * transformX) + moveX, ((1687.0002) * transformY) + moveY),
        new createjs.Point(((677.0000) * transformX) + moveX, ((1689.0000) * transformY) + moveY),

        new createjs.Point(((676.3334) * transformX) + moveX, ((1689.0000) * transformY) + moveY),
        new createjs.Point(((675.6666) * transformX) + moveX, ((1689.0000) * transformY) + moveY),
        new createjs.Point(((675.0000) * transformX) + moveX, ((1689.0000) * transformY) + moveY),

        new createjs.Point(((673.6668) * transformX) + moveX, ((1690.6665) * transformY) + moveY),
        new createjs.Point(((672.3332) * transformX) + moveX, ((1692.3335) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((1694.0000) * transformY) + moveY),

        new createjs.Point(((670.3334) * transformX) + moveX, ((1694.0000) * transformY) + moveY),
        new createjs.Point(((669.6666) * transformX) + moveX, ((1694.0000) * transformY) + moveY),
        new createjs.Point(((669.0000) * transformX) + moveX, ((1694.0000) * transformY) + moveY),

        new createjs.Point(((668.0001) * transformX) + moveX, ((1695.3332) * transformY) + moveY),
        new createjs.Point(((666.9999) * transformX) + moveX, ((1696.6668) * transformY) + moveY),
        new createjs.Point(((666.0000) * transformX) + moveX, ((1698.0000) * transformY) + moveY),

        new createjs.Point(((665.3334) * transformX) + moveX, ((1698.0000) * transformY) + moveY),
        new createjs.Point(((664.6666) * transformX) + moveX, ((1698.0000) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((1698.0000) * transformY) + moveY),

        new createjs.Point(((663.0001) * transformX) + moveX, ((1699.3332) * transformY) + moveY),
        new createjs.Point(((661.9999) * transformX) + moveX, ((1700.6668) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((1702.0000) * transformY) + moveY),

        new createjs.Point(((660.3334) * transformX) + moveX, ((1702.0000) * transformY) + moveY),
        new createjs.Point(((659.6666) * transformX) + moveX, ((1702.0000) * transformY) + moveY),
        new createjs.Point(((659.0000) * transformX) + moveX, ((1702.0000) * transformY) + moveY),

        new createjs.Point(((658.6667) * transformX) + moveX, ((1702.6666) * transformY) + moveY),
        new createjs.Point(((658.3333) * transformX) + moveX, ((1703.3334) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((1704.0000) * transformY) + moveY),

        new createjs.Point(((657.0001) * transformX) + moveX, ((1704.3333) * transformY) + moveY),
        new createjs.Point(((655.9999) * transformX) + moveX, ((1704.6667) * transformY) + moveY),
        new createjs.Point(((655.0000) * transformX) + moveX, ((1705.0000) * transformY) + moveY),

        new createjs.Point(((654.0001) * transformX) + moveX, ((1706.3332) * transformY) + moveY),
        new createjs.Point(((652.9999) * transformX) + moveX, ((1707.6668) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((1709.0000) * transformY) + moveY),

        new createjs.Point(((651.3334) * transformX) + moveX, ((1709.0000) * transformY) + moveY),
        new createjs.Point(((650.6666) * transformX) + moveX, ((1709.0000) * transformY) + moveY),
        new createjs.Point(((650.0000) * transformX) + moveX, ((1709.0000) * transformY) + moveY),

        new createjs.Point(((649.3334) * transformX) + moveX, ((1709.9999) * transformY) + moveY),
        new createjs.Point(((648.6666) * transformX) + moveX, ((1711.0001) * transformY) + moveY),
        new createjs.Point(((648.0000) * transformX) + moveX, ((1712.0000) * transformY) + moveY),

        new createjs.Point(((647.3334) * transformX) + moveX, ((1712.0000) * transformY) + moveY),
        new createjs.Point(((646.6666) * transformX) + moveX, ((1712.0000) * transformY) + moveY),
        new createjs.Point(((646.0000) * transformX) + moveX, ((1712.0000) * transformY) + moveY),

        new createjs.Point(((645.6667) * transformX) + moveX, ((1712.6666) * transformY) + moveY),
        new createjs.Point(((645.3333) * transformX) + moveX, ((1713.3334) * transformY) + moveY),
        new createjs.Point(((645.0000) * transformX) + moveX, ((1714.0000) * transformY) + moveY),

        new createjs.Point(((644.3334) * transformX) + moveX, ((1714.0000) * transformY) + moveY),
        new createjs.Point(((643.6666) * transformX) + moveX, ((1714.0000) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((1714.0000) * transformY) + moveY),

        new createjs.Point(((642.3334) * transformX) + moveX, ((1714.9999) * transformY) + moveY),
        new createjs.Point(((641.6666) * transformX) + moveX, ((1716.0001) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((1717.0000) * transformY) + moveY),

        new createjs.Point(((640.3334) * transformX) + moveX, ((1717.0000) * transformY) + moveY),
        new createjs.Point(((639.6666) * transformX) + moveX, ((1717.0000) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((1717.0000) * transformY) + moveY),

        new createjs.Point(((638.6667) * transformX) + moveX, ((1717.6666) * transformY) + moveY),
        new createjs.Point(((638.3333) * transformX) + moveX, ((1718.3334) * transformY) + moveY),
        new createjs.Point(((638.0000) * transformX) + moveX, ((1719.0000) * transformY) + moveY),

        new createjs.Point(((637.3334) * transformX) + moveX, ((1719.0000) * transformY) + moveY),
        new createjs.Point(((636.6666) * transformX) + moveX, ((1719.0000) * transformY) + moveY),
        new createjs.Point(((636.0000) * transformX) + moveX, ((1719.0000) * transformY) + moveY),

        new createjs.Point(((635.3334) * transformX) + moveX, ((1719.9999) * transformY) + moveY),
        new createjs.Point(((634.6666) * transformX) + moveX, ((1721.0001) * transformY) + moveY),
        new createjs.Point(((634.0000) * transformX) + moveX, ((1722.0000) * transformY) + moveY),

        new createjs.Point(((633.3334) * transformX) + moveX, ((1722.0000) * transformY) + moveY),
        new createjs.Point(((632.6666) * transformX) + moveX, ((1722.0000) * transformY) + moveY),
        new createjs.Point(((632.0000) * transformX) + moveX, ((1722.0000) * transformY) + moveY),

        new createjs.Point(((631.6667) * transformX) + moveX, ((1722.6666) * transformY) + moveY),
        new createjs.Point(((631.3333) * transformX) + moveX, ((1723.3334) * transformY) + moveY),
        new createjs.Point(((631.0000) * transformX) + moveX, ((1724.0000) * transformY) + moveY),

        new createjs.Point(((630.0001) * transformX) + moveX, ((1724.3333) * transformY) + moveY),
        new createjs.Point(((628.9999) * transformX) + moveX, ((1724.6667) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),

        new createjs.Point(((628.0000) * transformX) + moveX, ((1725.3333) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((1725.6667) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((1726.0000) * transformY) + moveY),

        new createjs.Point(((627.3334) * transformX) + moveX, ((1726.0000) * transformY) + moveY),
        new createjs.Point(((626.6666) * transformX) + moveX, ((1726.0000) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((1726.0000) * transformY) + moveY),

        new createjs.Point(((625.6667) * transformX) + moveX, ((1726.6666) * transformY) + moveY),
        new createjs.Point(((625.3333) * transformX) + moveX, ((1727.3334) * transformY) + moveY),
        new createjs.Point(((625.0000) * transformX) + moveX, ((1728.0000) * transformY) + moveY),

        new createjs.Point(((624.3334) * transformX) + moveX, ((1728.0000) * transformY) + moveY),
        new createjs.Point(((623.6666) * transformX) + moveX, ((1728.0000) * transformY) + moveY),
        new createjs.Point(((623.0000) * transformX) + moveX, ((1728.0000) * transformY) + moveY),

        new createjs.Point(((622.6667) * transformX) + moveX, ((1728.6666) * transformY) + moveY),
        new createjs.Point(((622.3333) * transformX) + moveX, ((1729.3334) * transformY) + moveY),
        new createjs.Point(((622.0000) * transformX) + moveX, ((1730.0000) * transformY) + moveY),

        new createjs.Point(((621.3334) * transformX) + moveX, ((1730.0000) * transformY) + moveY),
        new createjs.Point(((620.6666) * transformX) + moveX, ((1730.0000) * transformY) + moveY),
        new createjs.Point(((620.0000) * transformX) + moveX, ((1730.0000) * transformY) + moveY),

        new createjs.Point(((619.6667) * transformX) + moveX, ((1730.6666) * transformY) + moveY),
        new createjs.Point(((619.3333) * transformX) + moveX, ((1731.3334) * transformY) + moveY),
        new createjs.Point(((619.0000) * transformX) + moveX, ((1732.0000) * transformY) + moveY),

        new createjs.Point(((618.3334) * transformX) + moveX, ((1732.0000) * transformY) + moveY),
        new createjs.Point(((617.6666) * transformX) + moveX, ((1732.0000) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((1732.0000) * transformY) + moveY),

        new createjs.Point(((616.6667) * transformX) + moveX, ((1732.6666) * transformY) + moveY),
        new createjs.Point(((616.3333) * transformX) + moveX, ((1733.3334) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((1734.0000) * transformY) + moveY),

        new createjs.Point(((615.3334) * transformX) + moveX, ((1734.0000) * transformY) + moveY),
        new createjs.Point(((614.6666) * transformX) + moveX, ((1734.0000) * transformY) + moveY),
        new createjs.Point(((614.0000) * transformX) + moveX, ((1734.0000) * transformY) + moveY),

        new createjs.Point(((613.6667) * transformX) + moveX, ((1734.6666) * transformY) + moveY),
        new createjs.Point(((613.3333) * transformX) + moveX, ((1735.3334) * transformY) + moveY),
        new createjs.Point(((613.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),

        new createjs.Point(((612.3334) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((611.6666) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),

        new createjs.Point(((611.0000) * transformX) + moveX, ((1736.3333) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1736.6667) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1737.0000) * transformY) + moveY),

        new createjs.Point(((610.3334) * transformX) + moveX, ((1737.0000) * transformY) + moveY),
        new createjs.Point(((609.6666) * transformX) + moveX, ((1737.0000) * transformY) + moveY),
        new createjs.Point(((609.0000) * transformX) + moveX, ((1737.0000) * transformY) + moveY),

        new createjs.Point(((608.6667) * transformX) + moveX, ((1737.6666) * transformY) + moveY),
        new createjs.Point(((608.3333) * transformX) + moveX, ((1738.3334) * transformY) + moveY),
        new createjs.Point(((608.0000) * transformX) + moveX, ((1739.0000) * transformY) + moveY),

        new createjs.Point(((606.6668) * transformX) + moveX, ((1739.3333) * transformY) + moveY),
        new createjs.Point(((605.3332) * transformX) + moveX, ((1739.6667) * transformY) + moveY),
        new createjs.Point(((604.0000) * transformX) + moveX, ((1740.0000) * transformY) + moveY),

        new createjs.Point(((603.6667) * transformX) + moveX, ((1740.6666) * transformY) + moveY),
        new createjs.Point(((603.3333) * transformX) + moveX, ((1741.3334) * transformY) + moveY),
        new createjs.Point(((603.0000) * transformX) + moveX, ((1742.0000) * transformY) + moveY),

        new createjs.Point(((601.6668) * transformX) + moveX, ((1742.3333) * transformY) + moveY),
        new createjs.Point(((600.3332) * transformX) + moveX, ((1742.6667) * transformY) + moveY),
        new createjs.Point(((599.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),

        new createjs.Point(((598.6667) * transformX) + moveX, ((1743.6666) * transformY) + moveY),
        new createjs.Point(((598.3333) * transformX) + moveX, ((1744.3334) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((1745.0000) * transformY) + moveY),

        new createjs.Point(((596.6668) * transformX) + moveX, ((1745.3333) * transformY) + moveY),
        new createjs.Point(((595.3332) * transformX) + moveX, ((1745.6667) * transformY) + moveY),
        new createjs.Point(((594.0000) * transformX) + moveX, ((1746.0000) * transformY) + moveY),

        new createjs.Point(((593.6667) * transformX) + moveX, ((1746.6666) * transformY) + moveY),
        new createjs.Point(((593.3333) * transformX) + moveX, ((1747.3334) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((1748.0000) * transformY) + moveY),

        new createjs.Point(((591.0002) * transformX) + moveX, ((1748.6666) * transformY) + moveY),
        new createjs.Point(((588.9998) * transformX) + moveX, ((1749.3334) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1750.0000) * transformY) + moveY),

        new createjs.Point(((586.6667) * transformX) + moveX, ((1750.6666) * transformY) + moveY),
        new createjs.Point(((586.3333) * transformX) + moveX, ((1751.3334) * transformY) + moveY),
        new createjs.Point(((586.0000) * transformX) + moveX, ((1752.0000) * transformY) + moveY),

        new createjs.Point(((585.3334) * transformX) + moveX, ((1752.0000) * transformY) + moveY),
        new createjs.Point(((584.6666) * transformX) + moveX, ((1752.0000) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((1752.0000) * transformY) + moveY),

        new createjs.Point(((584.0000) * transformX) + moveX, ((1752.3333) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((1752.6667) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((1753.0000) * transformY) + moveY),

        new createjs.Point(((583.3334) * transformX) + moveX, ((1753.0000) * transformY) + moveY),
        new createjs.Point(((582.6666) * transformX) + moveX, ((1753.0000) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((1753.0000) * transformY) + moveY),

        new createjs.Point(((582.0000) * transformX) + moveX, ((1753.3333) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((1753.6667) * transformY) + moveY),
        new createjs.Point(((582.0000) * transformX) + moveX, ((1754.0000) * transformY) + moveY),

        new createjs.Point(((581.3334) * transformX) + moveX, ((1754.0000) * transformY) + moveY),
        new createjs.Point(((580.6666) * transformX) + moveX, ((1754.0000) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1754.0000) * transformY) + moveY),

        new createjs.Point(((580.0000) * transformX) + moveX, ((1754.3333) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1754.6667) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),

        new createjs.Point(((579.3334) * transformX) + moveX, ((1755.0000) * transformY) + moveY),
        new createjs.Point(((578.6666) * transformX) + moveX, ((1755.0000) * transformY) + moveY),
        new createjs.Point(((578.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),

        new createjs.Point(((578.0000) * transformX) + moveX, ((1755.3333) * transformY) + moveY),
        new createjs.Point(((578.0000) * transformX) + moveX, ((1755.6667) * transformY) + moveY),
        new createjs.Point(((578.0000) * transformX) + moveX, ((1756.0000) * transformY) + moveY),

        new createjs.Point(((577.3334) * transformX) + moveX, ((1756.0000) * transformY) + moveY),
        new createjs.Point(((576.6666) * transformX) + moveX, ((1756.0000) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((1756.0000) * transformY) + moveY),

        new createjs.Point(((575.6667) * transformX) + moveX, ((1756.6666) * transformY) + moveY),
        new createjs.Point(((575.3333) * transformX) + moveX, ((1757.3334) * transformY) + moveY),
        new createjs.Point(((575.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),

        new createjs.Point(((574.3334) * transformX) + moveX, ((1758.0000) * transformY) + moveY),
        new createjs.Point(((573.6666) * transformX) + moveX, ((1758.0000) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),

        new createjs.Point(((573.0000) * transformX) + moveX, ((1758.3333) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((1758.6667) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((1759.0000) * transformY) + moveY),

        new createjs.Point(((572.3334) * transformX) + moveX, ((1759.0000) * transformY) + moveY),
        new createjs.Point(((571.6666) * transformX) + moveX, ((1759.0000) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((1759.0000) * transformY) + moveY),

        new createjs.Point(((571.0000) * transformX) + moveX, ((1759.3333) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((1759.6667) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),

        new createjs.Point(((570.3334) * transformX) + moveX, ((1760.0000) * transformY) + moveY),
        new createjs.Point(((569.6666) * transformX) + moveX, ((1760.0000) * transformY) + moveY),
        new createjs.Point(((569.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),

        new createjs.Point(((569.0000) * transformX) + moveX, ((1760.3333) * transformY) + moveY),
        new createjs.Point(((569.0000) * transformX) + moveX, ((1760.6667) * transformY) + moveY),
        new createjs.Point(((569.0000) * transformX) + moveX, ((1761.0000) * transformY) + moveY),

        new createjs.Point(((568.3334) * transformX) + moveX, ((1761.0000) * transformY) + moveY),
        new createjs.Point(((567.6666) * transformX) + moveX, ((1761.0000) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((1761.0000) * transformY) + moveY),

        new createjs.Point(((567.0000) * transformX) + moveX, ((1761.3333) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((1761.6667) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((1762.0000) * transformY) + moveY),

        new createjs.Point(((566.3334) * transformX) + moveX, ((1762.0000) * transformY) + moveY),
        new createjs.Point(((565.6666) * transformX) + moveX, ((1762.0000) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((1762.0000) * transformY) + moveY),

        new createjs.Point(((565.0000) * transformX) + moveX, ((1762.3333) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((1762.6667) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((1763.0000) * transformY) + moveY),

        new createjs.Point(((564.3334) * transformX) + moveX, ((1763.0000) * transformY) + moveY),
        new createjs.Point(((563.6666) * transformX) + moveX, ((1763.0000) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((1763.0000) * transformY) + moveY),

        new createjs.Point(((563.0000) * transformX) + moveX, ((1763.3333) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((1763.6667) * transformY) + moveY),
        new createjs.Point(((563.0000) * transformX) + moveX, ((1764.0000) * transformY) + moveY),

        new createjs.Point(((562.3334) * transformX) + moveX, ((1764.0000) * transformY) + moveY),
        new createjs.Point(((561.6666) * transformX) + moveX, ((1764.0000) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1764.0000) * transformY) + moveY),

        new createjs.Point(((561.0000) * transformX) + moveX, ((1764.3333) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1764.6667) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),

        new createjs.Point(((560.3334) * transformX) + moveX, ((1765.0000) * transformY) + moveY),
        new createjs.Point(((559.6666) * transformX) + moveX, ((1765.0000) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),

        new createjs.Point(((559.0000) * transformX) + moveX, ((1765.3333) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1765.6667) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1766.0000) * transformY) + moveY),

        new createjs.Point(((557.0002) * transformX) + moveX, ((1766.6666) * transformY) + moveY),
        new createjs.Point(((554.9998) * transformX) + moveX, ((1767.3334) * transformY) + moveY),
        new createjs.Point(((553.0000) * transformX) + moveX, ((1768.0000) * transformY) + moveY),

        new createjs.Point(((553.0000) * transformX) + moveX, ((1768.3333) * transformY) + moveY),
        new createjs.Point(((553.0000) * transformX) + moveX, ((1768.6667) * transformY) + moveY),
        new createjs.Point(((553.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),

        new createjs.Point(((552.0001) * transformX) + moveX, ((1769.0000) * transformY) + moveY),
        new createjs.Point(((550.9999) * transformX) + moveX, ((1769.0000) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),

        new createjs.Point(((550.0000) * transformX) + moveX, ((1769.3333) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1769.6667) * transformY) + moveY),
        new createjs.Point(((550.0000) * transformX) + moveX, ((1770.0000) * transformY) + moveY),

        new createjs.Point(((549.3334) * transformX) + moveX, ((1770.0000) * transformY) + moveY),
        new createjs.Point(((548.6666) * transformX) + moveX, ((1770.0000) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1770.0000) * transformY) + moveY),

        new createjs.Point(((548.0000) * transformX) + moveX, ((1770.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1770.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1771.0000) * transformY) + moveY),

        new createjs.Point(((546.0002) * transformX) + moveX, ((1771.6666) * transformY) + moveY),
        new createjs.Point(((543.9998) * transformX) + moveX, ((1772.3334) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1773.0000) * transformY) + moveY),

        new createjs.Point(((542.0000) * transformX) + moveX, ((1773.3333) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1773.6667) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1774.0000) * transformY) + moveY),

        new createjs.Point(((541.0001) * transformX) + moveX, ((1774.0000) * transformY) + moveY),
        new createjs.Point(((539.9999) * transformX) + moveX, ((1774.0000) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1774.0000) * transformY) + moveY),

        new createjs.Point(((539.0000) * transformX) + moveX, ((1774.3333) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1774.6667) * transformY) + moveY),
        new createjs.Point(((539.0000) * transformX) + moveX, ((1775.0000) * transformY) + moveY),

        new createjs.Point(((538.3334) * transformX) + moveX, ((1775.0000) * transformY) + moveY),
        new createjs.Point(((537.6666) * transformX) + moveX, ((1775.0000) * transformY) + moveY),
        new createjs.Point(((537.0000) * transformX) + moveX, ((1775.0000) * transformY) + moveY),

        new createjs.Point(((537.0000) * transformX) + moveX, ((1775.3333) * transformY) + moveY),
        new createjs.Point(((537.0000) * transformX) + moveX, ((1775.6667) * transformY) + moveY),
        new createjs.Point(((537.0000) * transformX) + moveX, ((1776.0000) * transformY) + moveY),

        new createjs.Point(((535.6668) * transformX) + moveX, ((1776.3333) * transformY) + moveY),
        new createjs.Point(((534.3332) * transformX) + moveX, ((1776.6667) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),

        new createjs.Point(((533.0000) * transformX) + moveX, ((1777.3333) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((1777.6667) * transformY) + moveY),
        new createjs.Point(((533.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),

        new createjs.Point(((532.0001) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((530.9999) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),

        new createjs.Point(((530.0000) * transformX) + moveX, ((1778.3333) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((1778.6667) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((1779.0000) * transformY) + moveY),

        new createjs.Point(((529.3334) * transformX) + moveX, ((1779.0000) * transformY) + moveY),
        new createjs.Point(((528.6666) * transformX) + moveX, ((1779.0000) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((1779.0000) * transformY) + moveY),

        new createjs.Point(((528.0000) * transformX) + moveX, ((1779.3333) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((1779.6667) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),

        new createjs.Point(((527.0001) * transformX) + moveX, ((1780.0000) * transformY) + moveY),
        new createjs.Point(((525.9999) * transformX) + moveX, ((1780.0000) * transformY) + moveY),
        new createjs.Point(((525.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),

        new createjs.Point(((525.0000) * transformX) + moveX, ((1780.3333) * transformY) + moveY),
        new createjs.Point(((525.0000) * transformX) + moveX, ((1780.6667) * transformY) + moveY),
        new createjs.Point(((525.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),

        new createjs.Point(((524.3334) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((523.6666) * transformX) + moveX, ((1781.0000) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),

        new createjs.Point(((523.0000) * transformX) + moveX, ((1781.3333) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((1781.6667) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),

        new createjs.Point(((521.3335) * transformX) + moveX, ((1782.3333) * transformY) + moveY),
        new createjs.Point(((519.6665) * transformX) + moveX, ((1782.6667) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),

        new createjs.Point(((518.0000) * transformX) + moveX, ((1783.3333) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((1783.6667) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((1784.0000) * transformY) + moveY),

        new createjs.Point(((517.0001) * transformX) + moveX, ((1784.0000) * transformY) + moveY),
        new createjs.Point(((515.9999) * transformX) + moveX, ((1784.0000) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1784.0000) * transformY) + moveY),

        new createjs.Point(((515.0000) * transformX) + moveX, ((1784.3333) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1784.6667) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((514.3334) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((513.6666) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((513.0000) * transformX) + moveX, ((1785.3333) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1785.6667) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((1786.0000) * transformY) + moveY),

        new createjs.Point(((511.0002) * transformX) + moveX, ((1786.3333) * transformY) + moveY),
        new createjs.Point(((508.9998) * transformX) + moveX, ((1786.6667) * transformY) + moveY),
        new createjs.Point(((507.0000) * transformX) + moveX, ((1787.0000) * transformY) + moveY),

        new createjs.Point(((507.0000) * transformX) + moveX, ((1787.3333) * transformY) + moveY),
        new createjs.Point(((507.0000) * transformX) + moveX, ((1787.6667) * transformY) + moveY),
        new createjs.Point(((507.0000) * transformX) + moveX, ((1788.0000) * transformY) + moveY),

        new createjs.Point(((506.3334) * transformX) + moveX, ((1788.0000) * transformY) + moveY),
        new createjs.Point(((505.6666) * transformX) + moveX, ((1788.0000) * transformY) + moveY),
        new createjs.Point(((505.0000) * transformX) + moveX, ((1788.0000) * transformY) + moveY),

        new createjs.Point(((505.0000) * transformX) + moveX, ((1788.3333) * transformY) + moveY),
        new createjs.Point(((505.0000) * transformX) + moveX, ((1788.6667) * transformY) + moveY),
        new createjs.Point(((505.0000) * transformX) + moveX, ((1789.0000) * transformY) + moveY),

        new createjs.Point(((502.0003) * transformX) + moveX, ((1789.6666) * transformY) + moveY),
        new createjs.Point(((498.9997) * transformX) + moveX, ((1790.3334) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((1791.0000) * transformY) + moveY),

        new createjs.Point(((486.1147) * transformX) + moveX, ((1794.9347) * transformY) + moveY),
        new createjs.Point(((474.2836) * transformX) + moveX, ((1798.3179) * transformY) + moveY),
        new createjs.Point(((464.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),

        new createjs.Point(((461.3336) * transformX) + moveX, ((1802.3333) * transformY) + moveY),
        new createjs.Point(((458.6664) * transformX) + moveX, ((1802.6667) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),

        new createjs.Point(((456.0000) * transformX) + moveX, ((1803.3333) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1803.6667) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),

        new createjs.Point(((455.0001) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((453.9999) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),

        new createjs.Point(((453.0000) * transformX) + moveX, ((1804.3333) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1804.6667) * transformY) + moveY),
        new createjs.Point(((453.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),

        new createjs.Point(((450.0003) * transformX) + moveX, ((1805.3333) * transformY) + moveY),
        new createjs.Point(((446.9997) * transformX) + moveX, ((1805.6667) * transformY) + moveY),
        new createjs.Point(((444.0000) * transformX) + moveX, ((1806.0000) * transformY) + moveY),

        new createjs.Point(((444.0000) * transformX) + moveX, ((1806.3333) * transformY) + moveY),
        new createjs.Point(((444.0000) * transformX) + moveX, ((1806.6667) * transformY) + moveY),
        new createjs.Point(((444.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((442.6668) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((441.3332) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((440.0000) * transformX) + moveX, ((1807.3333) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((1807.6667) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),

        new createjs.Point(((438.6668) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((437.3332) * transformX) + moveX, ((1808.0000) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),

        new createjs.Point(((436.0000) * transformX) + moveX, ((1808.3333) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((1808.6667) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),

        new createjs.Point(((434.3335) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((432.6665) * transformX) + moveX, ((1809.0000) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),

        new createjs.Point(((431.0000) * transformX) + moveX, ((1809.3333) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((1809.6667) * transformY) + moveY),
        new createjs.Point(((431.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((429.3335) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((427.6665) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((426.0000) * transformX) + moveX, ((1810.3333) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((1810.6667) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),

        new createjs.Point(((424.3335) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((422.6665) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((421.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),

        new createjs.Point(((421.0000) * transformX) + moveX, ((1811.3333) * transformY) + moveY),
        new createjs.Point(((421.0000) * transformX) + moveX, ((1811.6667) * transformY) + moveY),
        new createjs.Point(((421.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),

        new createjs.Point(((417.3337) * transformX) + moveX, ((1812.3333) * transformY) + moveY),
        new createjs.Point(((413.6663) * transformX) + moveX, ((1812.6667) * transformY) + moveY),
        new createjs.Point(((410.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),

        new createjs.Point(((402.0408) * transformX) + moveX, ((1815.3638) * transformY) + moveY),
        new createjs.Point(((391.2228) * transformX) + moveX, ((1815.6281) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),

        new createjs.Point(((380.0003) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((376.9997) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),

        new createjs.Point(((374.0000) * transformX) + moveX, ((1818.3333) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((1818.6667) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),

        new createjs.Point(((370.3337) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((366.6663) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((363.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),

        new createjs.Point(((363.0000) * transformX) + moveX, ((1819.3333) * transformY) + moveY),
        new createjs.Point(((363.0000) * transformX) + moveX, ((1819.6667) * transformY) + moveY),
        new createjs.Point(((363.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),

        new createjs.Point(((362.0001) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((360.9999) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),

        new createjs.Point(((356.3337) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((352.6663) * transformX) + moveX, ((1820.0000) * transformY) + moveY),
        new createjs.Point(((349.0000) * transformX) + moveX, ((1820.0000) * transformY) + moveY),

        new createjs.Point(((341.2013) * transformX) + moveX, ((1822.1618) * transformY) + moveY),
        new createjs.Point(((328.5159) * transformX) + moveX, ((1820.9967) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((1821.0000) * transformY) + moveY),

        new createjs.Point(((295.3954) * transformX) + moveX, ((1821.0082) * transformY) + moveY),
        new createjs.Point(((272.2345) * transformX) + moveX, ((1820.5626) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1815.0000) * transformY) + moveY),

        new createjs.Point(((252.0002) * transformX) + moveX, ((1815.0000) * transformY) + moveY),
        new createjs.Point(((249.9998) * transformX) + moveX, ((1815.0000) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((1815.0000) * transformY) + moveY),

        new createjs.Point(((248.0000) * transformX) + moveX, ((1814.6667) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((1814.3333) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((1814.0000) * transformY) + moveY),

        new createjs.Point(((245.0003) * transformX) + moveX, ((1813.6667) * transformY) + moveY),
        new createjs.Point(((241.9997) * transformX) + moveX, ((1813.3333) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),

        new createjs.Point(((239.0000) * transformX) + moveX, ((1812.6667) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1812.3333) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),

        new createjs.Point(((237.6668) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((236.3332) * transformX) + moveX, ((1812.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1812.0000) * transformY) + moveY),

        new createjs.Point(((235.0000) * transformX) + moveX, ((1811.6667) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1811.3333) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),

        new createjs.Point(((233.6668) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((232.3332) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),

        new createjs.Point(((231.0000) * transformX) + moveX, ((1810.6667) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1810.3333) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((229.6668) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((228.3332) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((227.0000) * transformX) + moveX, ((1809.6667) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1809.3333) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1809.0000) * transformY) + moveY),

        new createjs.Point(((224.6669) * transformX) + moveX, ((1808.6667) * transformY) + moveY),
        new createjs.Point(((222.3331) * transformX) + moveX, ((1808.3333) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((1808.0000) * transformY) + moveY),

        new createjs.Point(((220.0000) * transformX) + moveX, ((1807.6667) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((1807.3333) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((217.6669) * transformX) + moveX, ((1806.6667) * transformY) + moveY),
        new createjs.Point(((215.3331) * transformX) + moveX, ((1806.3333) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((1806.0000) * transformY) + moveY),

        new createjs.Point(((213.0000) * transformX) + moveX, ((1805.6667) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((1805.3333) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),

        new createjs.Point(((212.0001) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((210.9999) * transformX) + moveX, ((1805.0000) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((1805.0000) * transformY) + moveY),

        new createjs.Point(((210.0000) * transformX) + moveX, ((1804.6667) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((1804.3333) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),

        new createjs.Point(((209.0001) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((207.9999) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),

        new createjs.Point(((207.0000) * transformX) + moveX, ((1803.6667) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((1803.3333) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),

        new createjs.Point(((205.0002) * transformX) + moveX, ((1802.6667) * transformY) + moveY),
        new createjs.Point(((202.9998) * transformX) + moveX, ((1802.3333) * transformY) + moveY),
        new createjs.Point(((201.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),

        new createjs.Point(((201.0000) * transformX) + moveX, ((1801.6667) * transformY) + moveY),
        new createjs.Point(((201.0000) * transformX) + moveX, ((1801.3333) * transformY) + moveY),
        new createjs.Point(((201.0000) * transformX) + moveX, ((1801.0000) * transformY) + moveY),

        new createjs.Point(((200.3334) * transformX) + moveX, ((1801.0000) * transformY) + moveY),
        new createjs.Point(((199.6666) * transformX) + moveX, ((1801.0000) * transformY) + moveY),
        new createjs.Point(((199.0000) * transformX) + moveX, ((1801.0000) * transformY) + moveY),

        new createjs.Point(((199.0000) * transformX) + moveX, ((1800.6667) * transformY) + moveY),
        new createjs.Point(((199.0000) * transformX) + moveX, ((1800.3333) * transformY) + moveY),
        new createjs.Point(((199.0000) * transformX) + moveX, ((1800.0000) * transformY) + moveY),

        new createjs.Point(((196.0003) * transformX) + moveX, ((1799.3334) * transformY) + moveY),
        new createjs.Point(((192.9997) * transformX) + moveX, ((1798.6666) * transformY) + moveY),
        new createjs.Point(((190.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),

        new createjs.Point(((190.0000) * transformX) + moveX, ((1797.6667) * transformY) + moveY),
        new createjs.Point(((190.0000) * transformX) + moveX, ((1797.3333) * transformY) + moveY),
        new createjs.Point(((190.0000) * transformX) + moveX, ((1797.0000) * transformY) + moveY),

        new createjs.Point(((189.3334) * transformX) + moveX, ((1797.0000) * transformY) + moveY),
        new createjs.Point(((188.6666) * transformX) + moveX, ((1797.0000) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((1797.0000) * transformY) + moveY),

        new createjs.Point(((188.0000) * transformX) + moveX, ((1796.6667) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((1796.3333) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),

        new createjs.Point(((187.0001) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((185.9999) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),

        new createjs.Point(((185.0000) * transformX) + moveX, ((1795.6667) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((1795.3333) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((1795.0000) * transformY) + moveY),

        new createjs.Point(((183.6668) * transformX) + moveX, ((1794.6667) * transformY) + moveY),
        new createjs.Point(((182.3332) * transformX) + moveX, ((1794.3333) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((1794.0000) * transformY) + moveY),

        new createjs.Point(((181.0000) * transformX) + moveX, ((1793.6667) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((1793.3333) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((1793.0000) * transformY) + moveY),

        new createjs.Point(((180.0001) * transformX) + moveX, ((1793.0000) * transformY) + moveY),
        new createjs.Point(((178.9999) * transformX) + moveX, ((1793.0000) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((1793.0000) * transformY) + moveY),

        new createjs.Point(((178.0000) * transformX) + moveX, ((1792.6667) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((1792.3333) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((1792.0000) * transformY) + moveY),

        new createjs.Point(((176.6668) * transformX) + moveX, ((1791.6667) * transformY) + moveY),
        new createjs.Point(((175.3332) * transformX) + moveX, ((1791.3333) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((1791.0000) * transformY) + moveY),

        new createjs.Point(((174.0000) * transformX) + moveX, ((1790.6667) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((1790.3333) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((1790.0000) * transformY) + moveY),

        new createjs.Point(((173.0001) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((171.9999) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((1790.0000) * transformY) + moveY),

        new createjs.Point(((171.0000) * transformX) + moveX, ((1789.6667) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((1789.3333) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((1789.0000) * transformY) + moveY),

        new createjs.Point(((169.0002) * transformX) + moveX, ((1788.3334) * transformY) + moveY),
        new createjs.Point(((166.9998) * transformX) + moveX, ((1787.6666) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((1787.0000) * transformY) + moveY),

        new createjs.Point(((165.0000) * transformX) + moveX, ((1786.6667) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((1786.3333) * transformY) + moveY),
        new createjs.Point(((165.0000) * transformX) + moveX, ((1786.0000) * transformY) + moveY),

        new createjs.Point(((164.3334) * transformX) + moveX, ((1786.0000) * transformY) + moveY),
        new createjs.Point(((163.6666) * transformX) + moveX, ((1786.0000) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((1786.0000) * transformY) + moveY),

        new createjs.Point(((163.0000) * transformX) + moveX, ((1785.6667) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((1785.3333) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((162.3334) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((161.6666) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((161.0000) * transformX) + moveX, ((1784.6667) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((1784.3333) * transformY) + moveY),
        new createjs.Point(((161.0000) * transformX) + moveX, ((1784.0000) * transformY) + moveY),

        new createjs.Point(((160.0001) * transformX) + moveX, ((1784.0000) * transformY) + moveY),
        new createjs.Point(((158.9999) * transformX) + moveX, ((1784.0000) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1784.0000) * transformY) + moveY),

        new createjs.Point(((158.0000) * transformX) + moveX, ((1783.6667) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1783.3333) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),

        new createjs.Point(((157.3334) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((156.6666) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),

        new createjs.Point(((156.0000) * transformX) + moveX, ((1782.6667) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((1782.3333) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),

        new createjs.Point(((155.3334) * transformX) + moveX, ((1782.0000) * transformY) + moveY),
        new createjs.Point(((154.6666) * transformX) + moveX, ((1782.0000) * transformY) + moveY),
        new createjs.Point(((154.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),

        new createjs.Point(((153.6667) * transformX) + moveX, ((1781.3334) * transformY) + moveY),
        new createjs.Point(((153.3333) * transformX) + moveX, ((1780.6666) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),

        new createjs.Point(((152.3334) * transformX) + moveX, ((1780.0000) * transformY) + moveY),
        new createjs.Point(((151.6666) * transformX) + moveX, ((1780.0000) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),

        new createjs.Point(((151.0000) * transformX) + moveX, ((1779.6667) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1779.3333) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1779.0000) * transformY) + moveY),

        new createjs.Point(((150.3334) * transformX) + moveX, ((1779.0000) * transformY) + moveY),
        new createjs.Point(((149.6666) * transformX) + moveX, ((1779.0000) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((1779.0000) * transformY) + moveY),

        new createjs.Point(((149.0000) * transformX) + moveX, ((1778.6667) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((1778.3333) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),

        new createjs.Point(((148.3334) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((147.6666) * transformX) + moveX, ((1778.0000) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),

        new createjs.Point(((147.0000) * transformX) + moveX, ((1777.6667) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((1777.3333) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),

        new createjs.Point(((146.3334) * transformX) + moveX, ((1777.0000) * transformY) + moveY),
        new createjs.Point(((145.6666) * transformX) + moveX, ((1777.0000) * transformY) + moveY),
        new createjs.Point(((145.0000) * transformX) + moveX, ((1777.0000) * transformY) + moveY),

        new createjs.Point(((145.0000) * transformX) + moveX, ((1776.6667) * transformY) + moveY),
        new createjs.Point(((145.0000) * transformX) + moveX, ((1776.3333) * transformY) + moveY),
        new createjs.Point(((145.0000) * transformX) + moveX, ((1776.0000) * transformY) + moveY),

        new createjs.Point(((144.3334) * transformX) + moveX, ((1776.0000) * transformY) + moveY),
        new createjs.Point(((143.6666) * transformX) + moveX, ((1776.0000) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1776.0000) * transformY) + moveY),

        new createjs.Point(((143.0000) * transformX) + moveX, ((1775.6667) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1775.3333) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1775.0000) * transformY) + moveY),

        new createjs.Point(((141.6668) * transformX) + moveX, ((1774.6667) * transformY) + moveY),
        new createjs.Point(((140.3332) * transformX) + moveX, ((1774.3333) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((1774.0000) * transformY) + moveY),

        new createjs.Point(((138.6667) * transformX) + moveX, ((1773.3334) * transformY) + moveY),
        new createjs.Point(((138.3333) * transformX) + moveX, ((1772.6666) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((1772.0000) * transformY) + moveY),

        new createjs.Point(((136.0002) * transformX) + moveX, ((1771.3334) * transformY) + moveY),
        new createjs.Point(((133.9998) * transformX) + moveX, ((1770.6666) * transformY) + moveY),
        new createjs.Point(((132.0000) * transformX) + moveX, ((1770.0000) * transformY) + moveY),

        new createjs.Point(((131.6667) * transformX) + moveX, ((1769.3334) * transformY) + moveY),
        new createjs.Point(((131.3333) * transformX) + moveX, ((1768.6666) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1768.0000) * transformY) + moveY),

        new createjs.Point(((129.6668) * transformX) + moveX, ((1767.6667) * transformY) + moveY),
        new createjs.Point(((128.3332) * transformX) + moveX, ((1767.3333) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1767.0000) * transformY) + moveY),

        new createjs.Point(((126.6667) * transformX) + moveX, ((1766.3334) * transformY) + moveY),
        new createjs.Point(((126.3333) * transformX) + moveX, ((1765.6666) * transformY) + moveY),
        new createjs.Point(((126.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),

        new createjs.Point(((124.6668) * transformX) + moveX, ((1764.6667) * transformY) + moveY),
        new createjs.Point(((123.3332) * transformX) + moveX, ((1764.3333) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((1764.0000) * transformY) + moveY),

        new createjs.Point(((122.0000) * transformX) + moveX, ((1763.6667) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((1763.3333) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((1763.0000) * transformY) + moveY),

        new createjs.Point(((121.0001) * transformX) + moveX, ((1762.6667) * transformY) + moveY),
        new createjs.Point(((119.9999) * transformX) + moveX, ((1762.3333) * transformY) + moveY),
        new createjs.Point(((119.0000) * transformX) + moveX, ((1762.0000) * transformY) + moveY),

        new createjs.Point(((118.6667) * transformX) + moveX, ((1761.3334) * transformY) + moveY),
        new createjs.Point(((118.3333) * transformX) + moveX, ((1760.6666) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),

        new createjs.Point(((116.6668) * transformX) + moveX, ((1759.6667) * transformY) + moveY),
        new createjs.Point(((115.3332) * transformX) + moveX, ((1759.3333) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1759.0000) * transformY) + moveY),

        new createjs.Point(((114.0000) * transformX) + moveX, ((1758.6667) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1758.3333) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),

        new createjs.Point(((113.0001) * transformX) + moveX, ((1757.6667) * transformY) + moveY),
        new createjs.Point(((111.9999) * transformX) + moveX, ((1757.3333) * transformY) + moveY),
        new createjs.Point(((111.0000) * transformX) + moveX, ((1757.0000) * transformY) + moveY),

        new createjs.Point(((110.6667) * transformX) + moveX, ((1756.3334) * transformY) + moveY),
        new createjs.Point(((110.3333) * transformX) + moveX, ((1755.6666) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),

        new createjs.Point(((109.3334) * transformX) + moveX, ((1755.0000) * transformY) + moveY),
        new createjs.Point(((108.6666) * transformX) + moveX, ((1755.0000) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),

        new createjs.Point(((108.0000) * transformX) + moveX, ((1754.6667) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((1754.3333) * transformY) + moveY),
        new createjs.Point(((108.0000) * transformX) + moveX, ((1754.0000) * transformY) + moveY),

        new createjs.Point(((107.0001) * transformX) + moveX, ((1753.6667) * transformY) + moveY),
        new createjs.Point(((105.9999) * transformX) + moveX, ((1753.3333) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((1753.0000) * transformY) + moveY),

        new createjs.Point(((104.6667) * transformX) + moveX, ((1752.3334) * transformY) + moveY),
        new createjs.Point(((104.3333) * transformX) + moveX, ((1751.6666) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1751.0000) * transformY) + moveY),

        new createjs.Point(((103.3334) * transformX) + moveX, ((1751.0000) * transformY) + moveY),
        new createjs.Point(((102.6666) * transformX) + moveX, ((1751.0000) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1751.0000) * transformY) + moveY),

        new createjs.Point(((101.3334) * transformX) + moveX, ((1750.0001) * transformY) + moveY),
        new createjs.Point(((100.6666) * transformX) + moveX, ((1748.9999) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((1748.0000) * transformY) + moveY),

        new createjs.Point(((99.3334) * transformX) + moveX, ((1748.0000) * transformY) + moveY),
        new createjs.Point(((98.6666) * transformX) + moveX, ((1748.0000) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1748.0000) * transformY) + moveY),

        new createjs.Point(((97.6667) * transformX) + moveX, ((1747.3334) * transformY) + moveY),
        new createjs.Point(((97.3333) * transformX) + moveX, ((1746.6666) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1746.0000) * transformY) + moveY),

        new createjs.Point(((96.3334) * transformX) + moveX, ((1746.0000) * transformY) + moveY),
        new createjs.Point(((95.6666) * transformX) + moveX, ((1746.0000) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((1746.0000) * transformY) + moveY),

        new createjs.Point(((94.3334) * transformX) + moveX, ((1745.0001) * transformY) + moveY),
        new createjs.Point(((93.6666) * transformX) + moveX, ((1743.9999) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),

        new createjs.Point(((92.3334) * transformX) + moveX, ((1743.0000) * transformY) + moveY),
        new createjs.Point(((91.6666) * transformX) + moveX, ((1743.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),

        new createjs.Point(((90.6667) * transformX) + moveX, ((1742.3334) * transformY) + moveY),
        new createjs.Point(((90.3333) * transformX) + moveX, ((1741.6666) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),

        new createjs.Point(((89.3334) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((88.6666) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),

        new createjs.Point(((87.3334) * transformX) + moveX, ((1740.0001) * transformY) + moveY),
        new createjs.Point(((86.6666) * transformX) + moveX, ((1738.9999) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((1738.0000) * transformY) + moveY),

        new createjs.Point(((85.3334) * transformX) + moveX, ((1738.0000) * transformY) + moveY),
        new createjs.Point(((84.6666) * transformX) + moveX, ((1738.0000) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1738.0000) * transformY) + moveY),

        new createjs.Point(((83.3334) * transformX) + moveX, ((1737.0001) * transformY) + moveY),
        new createjs.Point(((82.6666) * transformX) + moveX, ((1735.9999) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1735.0000) * transformY) + moveY),

        new createjs.Point(((81.3334) * transformX) + moveX, ((1735.0000) * transformY) + moveY),
        new createjs.Point(((80.6666) * transformX) + moveX, ((1735.0000) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1735.0000) * transformY) + moveY),

        new createjs.Point(((79.0001) * transformX) + moveX, ((1733.6668) * transformY) + moveY),
        new createjs.Point(((77.9999) * transformX) + moveX, ((1732.3332) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),

        new createjs.Point(((76.3334) * transformX) + moveX, ((1731.0000) * transformY) + moveY),
        new createjs.Point(((75.6666) * transformX) + moveX, ((1731.0000) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),

        new createjs.Point(((74.0001) * transformX) + moveX, ((1729.6668) * transformY) + moveY),
        new createjs.Point(((72.9999) * transformX) + moveX, ((1728.3332) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1727.0000) * transformY) + moveY),

        new createjs.Point(((71.3334) * transformX) + moveX, ((1727.0000) * transformY) + moveY),
        new createjs.Point(((70.6666) * transformX) + moveX, ((1727.0000) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1727.0000) * transformY) + moveY),

        new createjs.Point(((69.6667) * transformX) + moveX, ((1726.3334) * transformY) + moveY),
        new createjs.Point(((69.3333) * transformX) + moveX, ((1725.6666) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),

        new createjs.Point(((68.6667) * transformX) + moveX, ((1725.0000) * transformY) + moveY),
        new createjs.Point(((68.3333) * transformX) + moveX, ((1725.0000) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),

        new createjs.Point(((67.6667) * transformX) + moveX, ((1724.3334) * transformY) + moveY),
        new createjs.Point(((67.3333) * transformX) + moveX, ((1723.6666) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1723.0000) * transformY) + moveY),

        new createjs.Point(((66.3334) * transformX) + moveX, ((1723.0000) * transformY) + moveY),
        new createjs.Point(((65.6666) * transformX) + moveX, ((1723.0000) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1723.0000) * transformY) + moveY),

        new createjs.Point(((63.6668) * transformX) + moveX, ((1721.3335) * transformY) + moveY),
        new createjs.Point(((62.3332) * transformX) + moveX, ((1719.6665) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),

        new createjs.Point(((60.3334) * transformX) + moveX, ((1718.0000) * transformY) + moveY),
        new createjs.Point(((59.6666) * transformX) + moveX, ((1718.0000) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),

        new createjs.Point(((57.3335) * transformX) + moveX, ((1716.0002) * transformY) + moveY),
        new createjs.Point(((55.6665) * transformX) + moveX, ((1713.9998) * transformY) + moveY),
        new createjs.Point(((54.0000) * transformX) + moveX, ((1712.0000) * transformY) + moveY),

        new createjs.Point(((41.3843) * transformX) + moveX, ((1702.8603) * transformY) + moveY),
        new createjs.Point(((30.8660) * transformX) + moveX, ((1691.8672) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((1681.0000) * transformY) + moveY),

        new createjs.Point(((14.3528) * transformX) + moveX, ((1675.3521) * transformY) + moveY),
        new createjs.Point(((4.4170) * transformX) + moveX, ((1668.2922) * transformY) + moveY),
        new createjs.Point(((1.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),

        // new createjs.Point(((0.6667) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        // new createjs.Point(((0.3333) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        // new createjs.Point(((0.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY)


    ];

    var points2 = [
        new createjs.Point(startX2, startY2),
        new createjs.Point(((574.6454) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(((787.3546) * transformX) + moveX, ((0.0000) * transformY) + moveY),
        new createjs.Point(endX2, endY2)
    ];
    var points1Final = points;

    var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath, motionPath2);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final), getMotionPathFromPoints(points2Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 5", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("11", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        loop = false;
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 60).wait(35).call(function() {
            currentMotionStep = 2;
        })).on('change', (function(event) {

            if (currentMotionStep == 1) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        this.timeline.addTween(cjs.Tween.get(bar2).wait(140).to({
            guide: {
                path: motionPath2
            }
        }, 20).call(function() {
            gotoLastAndStop(Stage1);
        })).on('change', (function(event) {
            if (currentMotionStep == 2) {
                bar2 = drawCharacter(thisStage, bar2, true, event);
            }
        }));



    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];
                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }
            
            if (thisbar.x == 624.8223278830715 && thisbar.y == 161.77114402505612) { // finished 1/3 of line
                Stage1.shape.visible = false;
            } else if (thisbar.x == startX && thisbar.y == startY) { // plot 1st point
                Stage1.shape.visible = true;
            }

            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.32)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.paused = false;
            thisStage.movie.play();
        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);

        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }


    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        // thisStage.pencil=thisStage.movie.pencil;
        // thisStage.addChild(thisStage.pencil);
        if (thisStage.pencil) {
            thisStage.removeChild(thisStage.pencil);
        }
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function gotoLastAndStop(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2] + 190;
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - (145 * 3.19);
        thisStage.movie.removeChild(thisStage.movie.pencil);


        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        thisStage.pencil = thisStage.movie.pencil;
        thisStage.addChild(thisStage.pencil);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145);
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
