(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p59_1.png",
            id: "p59_1"
        }, {
            src: "images/p59_2.png",
            id: "p59_2"
        }]
    };

    // symbols:
    (lib.p59_1 = function() {
        this.initialize(img.p59_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p59_2 = function() {
        this.initialize(img.p59_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1       
        this.text = new cjs.Text("59", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(553, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Subtrahera talen från talet i mitten.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 507, 242, 10);
        this.roundRect1.setTransform(0, 12);

        this.instance = new lib.p59_1();
        this.instance.setTransform(30, 18, 0.46, 0.46);
        // set NUmber in wheel 1
        this.label1 = new cjs.Text("9", "16px 'MyriadPro-Semibold'");
        this.label1.setTransform(107.5, 137.5);
        this.label2 = new cjs.Text("7", "16px 'MyriadPro-Semibold'");
        this.label2.setTransform(115, 122);
        this.label3 = new cjs.Text("4", "16px 'MyriadPro-Semibold'");
        this.label3.setTransform(135, 122);
        this.label4 = new cjs.Text("6", "16px 'MyriadPro-Semibold'");
        this.label4.setTransform(145, 137.5);
        this.label5 = new cjs.Text("3", "16px 'MyriadPro-Semibold'");
        this.label5.setTransform(135, 156);
        this.label6 = new cjs.Text("8", "16px 'MyriadPro-Semibold'");
        this.label6.setTransform(115, 155);

        // set NUmber in wheel 2
        this.label7 = new cjs.Text("5", "16px 'MyriadPro-Semibold'");
        this.label7.setTransform(349, 137.5);
        this.label8 = new cjs.Text("9", "16px 'MyriadPro-Semibold'");
        this.label8.setTransform(360, 122);
        this.label9 = new cjs.Text("7", "16px 'MyriadPro-Semibold'");
        this.label9.setTransform(379, 122);
        this.label10 = new cjs.Text("6", "16px 'MyriadPro-Semibold'");
        this.label10.setTransform(389, 137.5);
        this.label11 = new cjs.Text("4", "16px 'MyriadPro-Semibold'");
        this.label11.setTransform(379, 156);
        this.label12 = new cjs.Text("3", "16px 'MyriadPro-Semibold'");
        this.label12.setTransform(359, 155);

        //set number in wheel centre
        this.label13 = new cjs.Text("11", "bold 16px 'Myriad Pro'", "#D5132B");
        this.label13.setTransform(121, 138);
        this.label14 = new cjs.Text("12", "bold 16px 'Myriad Pro'", "#D5132B");
        this.label14.setTransform(365, 138);
        //set line in wheels
        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(87, 72).lineTo(115, 72);
        this.Line_1.setTransform(10, 30);
        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(131, 72).lineTo(156, 72);
        this.Line_2.setTransform(10, 30);
        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(64, 114).lineTo(87, 114);
        this.Line_3.setTransform(10, 30);
        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(155, 114).lineTo(180, 114);
        this.Line_4.setTransform(10, 30);
        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(87, 152).lineTo(113, 152);
        this.Line_5.setTransform(10, 30);
        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(131, 152).lineTo(156, 152);
        this.Line_6.setTransform(10, 30);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(330, 72).lineTo(358, 72);
        this.Line_7.setTransform(10, 30);
        this.Line_8 = new cjs.Shape();
        this.Line_8.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(375, 72).lineTo(402, 72);
        this.Line_8.setTransform(10, 30);
        this.Line_9 = new cjs.Shape();
        this.Line_9.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(308, 114).lineTo(331, 114);
        this.Line_9.setTransform(10, 30);
        this.Line_10 = new cjs.Shape();
        this.Line_10.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(399, 114).lineTo(424, 114);
        this.Line_10.setTransform(10, 30);
        this.Line_11 = new cjs.Shape();
        this.Line_11.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(332, 152).lineTo(358, 152);
        this.Line_11.setTransform(10, 30);
        this.Line_12 = new cjs.Shape();
        this.Line_12.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(374, 152).lineTo(401, 152);
        this.Line_12.setTransform(10, 30);

        this.text_3 = new cjs.Text("7", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(145, 100.8);


        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.text_3);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14)
        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7, this.Line_8, this.Line_9, this.Line_10, this.Line_11, this.Line_12)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 250);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p59_2();
        this.instance.setTransform(43, 40, 0.46, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(10, 11, 250, 251, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(265, 11, 250, 251, 10);
        this.roundRect2.setTransform(0, 0);

        this.innerRoundRect = new cjs.Shape();
        this.innerRoundRect.graphics.f("#ffffff").s('#F7B48C').drawRoundRect(51, 34.5, 172, 120, 6);
        this.innerRoundRect.setTransform(0, 0)

        this.innerRoundRect1 = new cjs.Shape();
        this.innerRoundRect1.graphics.f("#ffffff").s('#F7B48C').drawRoundRect(290, 34.5, 200, 120, 6);
        this.innerRoundRect1.setTransform(0, 0)

        this.text = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(10, 0);

        var arrRowpos = ['187', '220'];
        var arrColumnpos = ['74', '167', '319', '427'];
        var lxpos = ['87', '180', '330', '440'];
        var lypos = ['190', '223'];
        var lxpos1 = ['130', '223', '373', '478'];

        var ToBeAdded = [];
        var tmpline = new cjs.Shape();
        for (var Rindex = 0; Rindex < arrRowpos.length; Rindex++) {
            var row = parseInt(arrRowpos[Rindex]);
            for (var Cindex = 0; Cindex < arrColumnpos.length; Cindex++) {
                var col = parseInt(arrColumnpos[Cindex]);
                var tmp_lypos = lypos[Rindex];
                if (Cindex == 3 && row == 187) {
                    row = 192
                }
                if (Cindex == 3 && row == 220) {
                    row = 225
                }
                if (Cindex == 3 && tmp_lypos == 190) {
                    tmp_lypos = 195
                }
                if (Cindex == 3 && tmp_lypos == 223) {
                    tmp_lypos = 227
                }
                var tempLabel = new cjs.Text("=", "16px 'Myriad Pro'");
                tmpline.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(lxpos[Cindex], tmp_lypos).lineTo(lxpos1[Cindex], tmp_lypos);
                tempLabel.setTransform(col, row);
                ToBeAdded.push(tempLabel, tmpline);
            }
        }
        var tmp_txt = "–"
        for (var row = 0; row < 4; row++) {
            var rowspaace = row;
            for (var col = 0; col < 2; col++) {
                var colspace = col;
                if (row == 2 && col == 0) {
                    tmp_txt = "+"
                } else {
                    tmp_txt = "–"
                }
                var tempLabe1 = new cjs.Text(tmp_txt, "16px 'Myriad Pro'");
                tempLabe1.setTransform(105, 60 + (rowspaace * 27));
                var tempLabe2 = new cjs.Text("=", "16px 'Myriad Pro'");
                tempLabe2.setTransform(155, 60 + (rowspaace * 27));
                ToBeAdded.push(tempLabe1, tempLabe2);
            }
        }

        var tmp_txt1 = "–"
        for (var row = 0; row < 4; row++) {
            var rowspaace = row;
            for (var col = 0; col < 2; col++) {
                var colspace = col;
                if (row == 2 && col == 0) {
                    tmp_txt1 = "+"
                } else {
                    tmp_txt1 = "–"
                }
                var tempLabe1 = new cjs.Text(tmp_txt1, "16px 'Myriad Pro'");
                tempLabe1.setTransform(378, 60 + (rowspaace * 27));
                var tempLabe2 = new cjs.Text("=", "16px 'Myriad Pro'");
                tempLabe2.setTransform(422, 60 + (rowspaace * 27));
                ToBeAdded.push(tempLabe1, tempLabe2);
            }
        }



        this.label1 = new cjs.Text("10", "16px 'Myriad Pro'");
        this.label1.setTransform(80, 60);
        this.label2 = new cjs.Text("11", "16px 'Myriad Pro'");
        this.label2.setTransform(80, 87);
        this.label3 = new cjs.Text("12", "16px 'Myriad Pro'");
        this.label3.setTransform(350, 60);
        this.label4 = new cjs.Text("12  –", "16px 'Myriad Pro'");
        this.label4.setTransform(310, 87);



        this.addChild(this.roundRect1, this.roundRect2, this.innerRoundRect, this.innerRoundRect1, this.text, this.text_1, this.text_2, this.instance );
        this.addChild(this.label1, this.label2,this.label3, this.label4)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(300, 415, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(309, 122, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
