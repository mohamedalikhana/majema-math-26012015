(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p58_1.png",
            id: "p58_1"
        }]
    };

    (lib.p58_1 = function() {
        this.initialize(img.p58_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();
    
        // Layer 1
        this.text = new cjs.Text("58", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 660.8);

        this.instance = new lib.p58_1();
        this.instance.setTransform(42, 0, 0.464, 0.46);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(452 + (columnSpace * 33), 26, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_q2.setTransform(22, 0);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(2, 0);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 252.7, 96, 10);
        this.roundRect1.setTransform(0, 12);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 114);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 216);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(0, 317);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(257, 12);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(257, 114);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(257, 216);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(257, 317);

        var ToBeAdded = [];
        var xpos = 15;
        var ypos = 25;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 4; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 127
            } else if (numOfrow == 2) {
                ypos = 229
            } else if (numOfrow == 3) {
                ypos = 330
            } else {
                ypos = 25
            }
            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 2; row++) {
                    for (var column = 0; column < 5; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 138
                        } else if (numOfcol == 2) {
                            xpos = 272
                        } else if (numOfcol == 3) {
                            xpos = 393
                        } else {
                            xpos = 15
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 20), ypos + (row * 20), 20, 21);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }


        var tmp_arc = new cjs.Shape();
        var xpos = 25;
        var ypos = 35;
        var fillcolor = "#D51317"
        for (var numOfrow = 0; numOfrow < 4; numOfrow++) {
            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var column = 0; column < 7; column++) {
                    var columnSpace = column;
                    for (var row = 0; row < 2; row++) {
                        if (row == 0 && column == 5 && numOfcol == 0) {
                            xpos = 48
                        }
                        if (numOfcol == 1) {
                            xpos = 282
                        }
                        if (row == 0 && column >= 5 && numOfcol == 1) {
                            xpos = 303
                        }
                        if (row == 1 && column == 5) {
                            continue
                        }
                        if (row == 1 && column == 6) {
                            continue;
                        }

                        if (numOfrow == 1) {
                            ypos = 137;
                            xpos = 25
                        }
                        if (numOfrow == 1 && numOfcol == 1) {
                            ypos = 137;
                            xpos = 282
                        }
                        if (numOfrow == 2) {
                            ypos = 239;
                            xpos = 25
                        }
                        if (numOfrow == 2 && numOfcol == 1) {
                            ypos = 239;
                            xpos = 282
                        }
                        if (numOfrow == 3) {
                            ypos = 340;
                            xpos = 25
                        }
                        if (numOfrow == 3 && numOfcol == 1) {
                            ypos = 340;
                            xpos = 282
                        }

                        if (row == 0 && column >= 5 && numOfcol == 0 && numOfrow == 1) {
                            xpos = 48
                        }
                        if (row == 0 && column >= 5 && numOfcol == 0 && numOfrow == 2) {
                            xpos = 48
                        }
                        if (row == 0 && column >= 5 && numOfcol == 0 && numOfrow == 3) {
                            xpos = 48
                        }

                        if (row == 0 && column >= 5 && numOfcol == 1 && numOfrow == 1) {
                            xpos = 303
                        }
                        if (row == 0 && column >= 5 && numOfcol == 1 && numOfrow == 2) {
                            xpos = 303
                        }
                        if (row == 0 && column >= 5 && numOfcol == 1 && numOfrow == 3) {
                            xpos = 303
                        }


                        tmp_arc.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 20), 7, 0, 2 * Math.PI);
                        ToBeAdded.push(tmp_arc);
                    }
                }
            }
        }


        var arryVal = ["12  –  2 =", "12  –  5 =", "12  –  4 =", "12  –  9 =", "12  –  3 =", "12  –  6 =", "12  –  7 =", "12  – 8 ="];
        var tmprow = 20;
        var tmpcol = 312;
        var i = 0;
        var temp_Line1 = new cjs.Shape();
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(arryVal[i], "16px 'Myriad Pro'");
                tempText.setTransform(81 + (258 * columnSpace), 93 + (100 * rowSpace));
                if (col == 1) {
                    tmpcol = 258
                } else if (col == 2) {
                    tmpcol = 165
                } else {
                    tmpcol = 162
                }
                if (row > 0) {
                    tmprow = 100
                } else {
                    tmprow = 20
                }
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(144 + (columnSpace * tmpcol), 97 + (rowSpace * tmprow)).lineTo(177 + (columnSpace * tmpcol), 97 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.text_q1, this.text_q2, this.text)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 410);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(610.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 442, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
