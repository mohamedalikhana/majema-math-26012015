(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p40_1.png",
            id: "p40_1"
        }]
    };

    (lib.p40_1 = function() {
        this.initialize(img.p40_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);




    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("kunna lösa uppgifter i addition 0 till 20, med tiotalsövergång, med tiotalsövergång", "9px 'Myriad Pro'");
        this.text_1.setTransform(71, 658);

        this.text_2 = new cjs.Text("40", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(38.3, 657);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(30, 660.2);

        this.text_3 = new cjs.Text("Addera till talen 7 och 6", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.setTransform(96, 45);

        this.text_4 = new cjs.Text("13", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(50.5, 45);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43, 23.5, 1, 1.1);

        this.instance = new lib.p40_1();
        this.instance.setTransform(36, 55, 0.465, 0.48);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Rita cirklar och addera.", "16px 'Myriad Pro'");
        this.text_q2.setTransform(20, 0);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 15, 252.7, 86, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 15, 252.7, 86, 10);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 106, 252.7, 86, 10);
        this.roundRect3.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 106, 252.7, 86, 10);
        this.roundRect4.setTransform(0, 0);

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 197, 252.7, 86, 10);
        this.roundRect5.setTransform(0, 0);

        this.roundRect6 = new cjs.Shape();
        this.roundRect6.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 197, 252.7, 86, 10);
        this.roundRect6.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        var numberofBoxs = 2;
        var Ypos = 0;
        var Xpos = 0;
        for (var i = 0; i < 12; i++) {

            for (var col = 0; col < 5; col++) {
                for (var row = 0; row < 2; row++) {
                    if (i == 1) {
                        Xpos = 120
                    } else if (i == 2) {
                        Xpos = 258
                    } else if (i == 3) {
                        Xpos = 379
                    } else if (i == 4) {
                        Xpos = 0, Ypos = 92
                    } else if (i == 5) {
                        Xpos = 120, Ypos = 92
                    } else if (i == 6) {
                        Xpos = 258, Ypos = 92
                    } else if (i == 7) {
                        Xpos = 379, Ypos = 92
                    } else if (i == 8) {
                        Xpos = 0, Ypos = 182
                    } else if (i == 9) {
                        Xpos = 120, Ypos = 182
                    } else if (i == 10) {
                        Xpos = 258, Ypos = 182
                    } else if (i == 11) {
                        Xpos = 379, Ypos = 182
                    } else {
                        Ypos = 0;
                        Xpos = 0
                    }
                    this.textbox_group1.graphics.s("#949599").ss(0.4).drawRect((13 + Xpos) + (col * 20.5), (27 + Ypos) + (row * 20.5), 20.5, 20.5);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        //------------------draw balls left side
        this.shape_group1 = new cjs.Shape();
        var xpos = 25;
        var ypos = 38;
        var fillcolor = "";
        var ssVal = 0.5;

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (row == 1) {
                    xpos = 25;
                    ypos = 44;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 25;
                    ypos = 38;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#FFFFFF";
                    ssVal = 0.8;
                }
                if (row == 1 && column == 3) {
                    fillcolor = "#FFFFFF";
                    ssVal = 0.8;
                }
                if (row == 1 && column == 2) {
                    fillcolor = "#FFFFFF";
                    ssVal = 0.8;
                }
                if (row == 2) {
                    ypos = 101;
                    fillcolor = "#D51317";
                }
                if (row == 3) {
                    ypos = 107;
                    fillcolor = "#D51317";
                }
                if (row == 4) {
                    ypos = 163;
                    fillcolor = "#D51317";
                }
                if (row == 5) {
                    ypos = 170;
                    fillcolor = "#D51317";
                }

                if (row == 3 && column >= 2) {
                    continue;
                }
                if (row == 5 && column >= 2) {
                    continue;
                }


                this.shape_group1.graphics.f(fillcolor).s("#6E6E70").ss(ssVal, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        //----------------------Draw balls right
        this.shape_group2 = new cjs.Shape();
        var xpos = 282;
        var ypos = 38;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (row == 1) {
                    xpos = 282;
                    ypos = 44;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 282;
                    ypos = 38;
                    fillcolor = "#D51317";
                }


                if (row == 2) {
                    ypos = 101;
                    fillcolor = "#D51317";
                }
                if (row == 3) {
                    ypos = 108;
                    fillcolor = "#D51317";
                }
                if (row == 4) {
                    ypos = 163;
                    fillcolor = "#D51317";
                }
                if (row == 5) {
                    ypos = 170;
                    fillcolor = "#D51317";
                }
                if (row == 6) {
                    ypos = 224;
                    fillcolor = "#D51317";
                }

                if (row == 1 && column >= 2) {
                    continue;
                }
                if (row == 3 && column >= 2) {
                    continue;
                }
                if (row == 5 && column >= 2) {
                    continue;
                }

                this.shape_group2.graphics.f(fillcolor).s("#6E6E70").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20.5), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f("#FFFFFF").s("#6E6E70").ss(0.8, 0, 0, 4).arc(142.5, 38, 7, 0, 2 * Math.PI);
        this.shape_group3.setTransform(0, 0);

        this.text_3 = new cjs.Text("7 + 4 =", "16px 'Myriad Pro'");
        this.text_3.setTransform(80, 87);
        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.s("##707070").ss(0.4).moveTo(130, 91).lineTo(170, 91);


        this.text_4 = new cjs.Text("7 + 6 =", "16px 'Myriad Pro'");
        this.text_4.setTransform(338, 87);
        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.s("##707070").ss(0.4).moveTo(388, 91).lineTo(430, 91);

        this.text_5 = new cjs.Text("7 + 5 =", "16px 'Myriad Pro'");
        this.text_5.setTransform(80, 178);
        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.s("##707070").ss(0.4).moveTo(130, 183).lineTo(170, 183);

        this.text_6 = new cjs.Text("7 + 7 =", "16px 'Myriad Pro'");
        this.text_6.setTransform(338, 178);
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.s("##707070").ss(0.4).moveTo(388, 183).lineTo(430, 183);

        this.text_7 = new cjs.Text("7 + 9 =", "16px 'Myriad Pro'");
        this.text_7.setTransform(80, 268);
        this.hrLine_5 = new cjs.Shape();
        this.hrLine_5.graphics.s("##707070").ss(0.4).moveTo(130, 275).lineTo(170, 275);

        this.text_8 = new cjs.Text("7 + 8 =", "16px 'Myriad Pro'");
        this.text_8.setTransform(338, 268);
        this.hrLine_6 = new cjs.Shape();
        this.hrLine_6.graphics.s("##707070").ss(0.4).moveTo(388, 275).lineTo(430, 275);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.text_q1, this.text_q2, this.text);
        this.addChild(this.textbox_group1, this.shape_group1, this.shape_group2, this.shape_group3);
        this.addChild(this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9);
        this.addChild(this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.hrLine_5, this.hrLine_6, this.hrLine_7);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 285.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(610.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 570, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
