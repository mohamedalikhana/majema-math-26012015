var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/balloon.png",
            id: "balloon"
        }, {
            src: "../exercises/images/present.png",
            id: "present"
        }, {
            src: "../exercises/images/cup.png",
            id: "cup"
        }, {
            src: "../exercises/images/icecream.png",
            id: "icecream"
        }, {
            src: "../exercises/images/spoon.png",
            id: "spoon"
        }, {
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }]
    };
    var iconProperties = {
        x: 850 + 50,
        y: 350 - 35,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 50,
        wrongY: 345 - 15
    };

    (lib.balloon = function() {
        this.initialize(img.balloon);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.present = function() {
        this.initialize(img.present);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.cup = function() {
        this.initialize(img.cup);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.icecream = function() {
        this.initialize(img.icecream);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.spoon = function() {
        this.initialize(img.spoon);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.tick = function() {
        this.initialize(img.tick);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function generateImages(images, imageCount, X, Y, scaleX, scaleY, image) {
        X = 280, Y = 100 - 50;
        if (imageCount < 5) {
            //X=X+(100*((5-imageCount-1)));
            switch (imageCount) {
                case 1:
                    X = X + 200;
                    break;
                case 2:
                    X = X + 150;
                    break;
                case 3:
                    X = X + 100;
                    break;
                case 4:
                    X = X + 50;
                    break;
            }

        }
        for (var i = 0; i < imageCount; i++) {
            var iteration = i;
            if (5 <= i) {
                iteration = i - 5;
                Y = 200;
            }
            var tempImage = image.clone(true);
            tempImage.setTransform(X + (170 * iteration), Y, 0.8, 0.8);
            images.addChild(tempImage);
        }
        return images;
    }
    (lib.CommentText = function() {
        this.initialize();
        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);
        this.hintText2 = new cjs.Text("Vi räknar de blå bollarna.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText2.textAlign = 'center';
        this.hintText2.lineHeight = 19;
        this.hintText2.visible = false;
        this.hintText2.setTransform(550, 400);
        this.hintText3 = new cjs.Text("Vi säger tillsammans: 3 är lika med 3.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText3.textAlign = 'center';
        this.hintText3.lineHeight = 19;
        this.hintText3.visible = false;
        this.hintText3.setTransform(550, 400);
        this.hintText4 = new cjs.Text("Hur kan vi se att det är lika många?", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText4.textAlign = 'center';
        this.hintText4.lineHeight = 19;
        this.hintText4.visible = false;
        this.hintText4.setTransform(550, 400);
        this.addChild(this.hintText1, this.hintText2, this.hintText3, this.hintText4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.balloon();
        this.imageCount = 3;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);
        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        thisStage.currentAnswer = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[1] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[1] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.present();
        this.imageCount = 5;
        this.images = new cjs.Container();

        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        var startY = 50;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
                startY = 50;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), startY, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[2] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[2] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.icecream();
        this.imageCount = 4;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[3] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[3] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.spoon();
        this.imageCount = 2;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[4] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[4] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.cup();
        this.imageCount = 6;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[5] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[5] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage6 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.balloon();
        this.imageCount = 5;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[6] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[6] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.Stage7 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.present();
        this.imageCount = 8;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[7] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[7] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.Stage8 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.icecream();
        this.imageCount = 6;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[8] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[8] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage9 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.spoon();
        this.imageCount = 9;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image)

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[9] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[9] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage10 = function() {
        this.initialize();
        thisStage = this;

        this.questionText = new cjs.Text("Klicka på den mängd bollar som du ser föremål.", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX + 150, iconProperties.wrongY + 80);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x + 150, iconProperties.y + 80, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = 0;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        this.image = new lib.cup();
        this.imageCount = 4;
        this.images = new cjs.Container();
        var startX = 375;
        var startY = 125;
        generateImages(this.images, this.imageCount, startX, startY, 0.5, 0.5, this.image)
        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.images);
        var circles = [];
        var circleCount = 10;
        var startX = 0;
        for (var j = 1; j <= circleCount; j++) {

            if ((circleCount / 2) + 1 == j) {
                startX = 30;
            }
            var circle = new createjs.Shape();
            circle.graphics.f('#ffffff').s('#000000').ss(0.7).arc(startX + (30 * j), 50, 10, 0, Math.PI * 2, 1);
            circle.number = j;
            circle.addEventListener('click', function(e) {
                var currentStage = steps[stepsCount].object;
                currentStage.incorrectAnswer.visible = false;
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                var fillStyle = e.target.graphics._fillInstructions[0].params;
                var currentFillStyle = fillStyle[1] + "";
                for (var i = 0; i < circles.length; i++) {
                    var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
                    resetFillStyle[1] = "#ffffff";
                }
                if (currentFillStyle == "#00B4EA") {
                    fillStyle[1] = "#FFFFFF";
                    currentAnswers[10] = 0;
                } else {
                    fillStyle[1] = "#00B4EA";
                    currentAnswers[10] = e.target.number;
                }
                stage.update();
            })
            circles.push(circle);
        };
        for (var i = 0; i < circles.length; i++) {
            circles[i].setTransform(350 + 150, 300 + 80)
            this.addChild(circles[i]);
        }
        this.answer = this.imageCount;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Lika många eller lika stor - Övning", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);

        this.text_1 = new cjs.Text("2", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.balls = function() {
        this.initialize();
        //function () {



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(100, 100, 1, 1, 0, 0, 0);

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(100, 100, 1, 1, 0, 0, 0);
        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(100, 100, 1, 1, 0, 0, 0);
        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(100, 100, 1, 1, 0, 0, 0);
        this.stage6 = new lib.Stage6();
        this.stage6.visible = false;
        this.stage6.setTransform(100, 100, 1, 1, 0, 0, 0);
        this.stage7 = new lib.Stage7();
        this.stage7.visible = false;
        this.stage7.setTransform(100, 100, 1, 1, 0, 0, 0);
        this.stage8 = new lib.Stage8();
        this.stage8.visible = false;
        this.stage8.setTransform(100, 100, 1, 1, 0, 0, 0);
        this.stage9 = new lib.Stage9();
        this.stage9.visible = false;
        this.stage9.setTransform(100, 100, 1, 1, 0, 0, 0);
        this.stage10 = new lib.Stage10();
        this.stage10.visible = false;
        this.stage10.setTransform(100, 100, 1, 1, 0, 0, 0);

        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7, this.stage8, this.stage9, this.stage10);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
