(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("151", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#1A863A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#1A863A");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Använd de här formerna:", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#1A863A').drawRect(0, 0, 509, 80);
        this.Rect1.setTransform(0, 16);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group1.graphics.f('#E24A44').s("#1F1F1F").ss(1).moveTo(29, 53).lineTo(63, 53).lineTo(46, 24).lineTo(29, 53);
                } else if (column == 1 && row == 0) {
                    this.shape_group1.graphics.f('#2DAFE6').s("#1F1F1F").ss(1).moveTo(75, 53).lineTo(109, 53).lineTo(92, 24).lineTo(75, 53);
                } else if (column == 2 && row == 0) {
                    this.shape_group1.graphics.f('#FFF050').s("#1F1F1F").ss(1).moveTo(119, 53).lineTo(153, 53).lineTo(136, 24).lineTo(119, 53);
                } else if (column == 0 && row == 1) {
                    this.shape_group1.graphics.f('#E24A44').s("#1F1F1F").ss(1).moveTo(34, 83).lineTo(56, 83).lineTo(45, 64).lineTo(34, 83);
                } else if (column == 1 && row == 1) {
                    this.shape_group1.graphics.f('#2DAFE6').s("#1F1F1F").ss(1).moveTo(80, 83).lineTo(102, 83).lineTo(91, 64).lineTo(80, 83);
                } else {
                    this.shape_group1.graphics.f('#FFF050').s("#1F1F1F").ss(1).moveTo(125, 83).lineTo(147, 83).lineTo(136, 64).lineTo(125, 83);
                }

            }

        }
        this.shape_group1.setTransform(0, 0);


        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group2.graphics.f("#E24A44").s("#1F1F1F").ss(0.8, 0, 0, 4).arc(88 + (columnSpace * 20.5), 42 + (row * 19), 15, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group2.graphics.f("#2DAFE6").s("#1F1F1F").ss(0.8, 0, 0, 4).arc(103 + (columnSpace * 20.5), 42 + (row * 19), 15, 0, 2 * Math.PI);
                } else if (column == 2 && row == 0) {
                    this.shape_group2.graphics.f("#FFF050").s("#1F1F1F").ss(0.8, 0, 0, 4).arc(119 + (columnSpace * 20.5), 42 + (row * 19), 15, 0, 2 * Math.PI);
                } else if (column == 0 && row == 1) {
                    this.shape_group2.graphics.f("#E24A44").s("#1F1F1F").ss(0.8, 0, 0, 4).arc(87 + (columnSpace * 20.5), 57 + (row * 19), 10, 0, 2 * Math.PI);
                } else if (column == 1 && row == 1) {
                    this.shape_group2.graphics.f("#2DAFE6").s("#1F1F1F").ss(0.8, 0, 0, 4).arc(103 + (columnSpace * 20.5), 57 + (row * 19), 10, 0, 2 * Math.PI);
                } else if (column == 2 && row == 1) {
                    this.shape_group2.graphics.f("#FFF050").s("#1F1F1F").ss(0.8, 0, 0, 4).arc(119 + (columnSpace * 20.5), 57 + (row * 19), 10, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group2.setTransform(130, 0);


        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group3.graphics.f('#E24A44').s("#1F1F1F").ss(0.7).drawRect(14, 26, 28, 28, 0);
                } else if (column == 1 && row == 0) {
                    this.shape_group3.graphics.f('#2DAFE6').s("#1F1F1F").ss(0.7).drawRect(53, 26, 28, 28, 0);
                } else if (column == 2 && row == 0) {
                    this.shape_group3.graphics.f('#FFF050').s("#1F1F1F").ss(0.7).drawRect(92, 26, 28, 28, 0);
                } else if (column == 0 && row == 1) {
                    this.shape_group3.graphics.f('#E24A44').s("#1F1F1F").ss(0.7).drawRect(18, 67, 20, 20, 0);
                } else if (column == 1 && row == 1) {
                    this.shape_group3.graphics.f('#2DAFE6').s("#1F1F1F").ss(0.7).drawRect(57, 67, 20, 20, 0);
                } else {
                    this.shape_group3.graphics.f('#FFF050').s("#1F1F1F").ss(0.7).drawRect(96, 67, 20, 20, 0);
                }

            }

        }
        this.shape_group3.setTransform(350, 0);

        this.text_2 = new cjs.Text(" Rita och måla 2 former.", "16px 'Myriad Pro'");
        this.text_2.setTransform(14, 123);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 512, 142, 10);
        this.roundRect1.setTransform(0, 137);

        this.hrline1 = new cjs.Shape();
        this.hrline1.graphics.f('#706F6F').s("#706F6F").ss(1).moveTo(0, 0).lineTo(512, 0)
        this.hrline1.setTransform(0, 187);

        this.hrline2 = new cjs.Shape();
        this.hrline2.graphics.f('#706F6F').s("#706F6F").ss(1).moveTo(0, 0).lineTo(512, 0)
        this.hrline2.setTransform(0, 233);

        this.vrline1 = new cjs.Shape();
        this.vrline1.graphics.f('#706F6F').s("#706F6F").ss(1).moveTo(0, 0).lineTo(0, 142)
        this.vrline1.setTransform(259, 137);

        var TxtArr = [];
        var TxtlineX = [10, 104, 269, 361, 10, 104, 269, 361, 10, 104, 269, 361];
        var TxtlineY = [168, 168, 168, 168, 214, 214, 214, 214, 263, 263, 263, 263];
        var TxtArrwords = ['liten', 'och', 'liten', 'och', 'stor', 'och', 'stor', 'och', 'liten', 'och', 'stor', 'och'];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text('' + TxtArrwords[i], "16px 'Myriad Pro'", "9D9C9C");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(this.temp_label);
        }


        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("").s('#9D9D9C').drawRect(0, 0, 76, 34);
        this.Rect2.setTransform(165, 145);

        this.Rect3 = this.Rect2.clone(true);
        this.Rect3.setTransform(165, 193);

        this.Rect4 = this.Rect2.clone(true);
        this.Rect4.setTransform(165, 239);

        this.Rect5 = this.Rect2.clone(true);
        this.Rect5.setTransform(421, 145);

        this.Rect6 = this.Rect2.clone(true);
        this.Rect6.setTransform(421, 193);

        this.Rect7 = this.Rect2.clone(true);
        this.Rect7.setTransform(421, 239);

        this.rectangle = new cjs.Shape();
        this.rectangle.graphics.f('').s("#1F1F1F").ss(0.7).drawRect(0, 0, 20, 20, 0);
        this.rectangle.setTransform(78, 247);

        this.rectangle2 = this.rectangle.clone(true);
        this.rectangle2.setTransform(336, 152);

        this.rectangle3 = this.rectangle.clone(true);
        this.rectangle3.setTransform(336, 247);

        this.rectangle4 = this.rectangle.clone(true);
        this.rectangle4.setTransform(389, 200);

        this.circle = new cjs.Shape();
        this.circle.graphics.f("").s("#1F1F1F").ss(0.8, 0, 0, 4).arc(10 + (columnSpace * 20.5), 57 + (row * 19), 10, 0, 2 * Math.PI);
        this.circle.setTransform(40, 112);

        this.circle2 = this.circle.clone(true);
        this.circle2.setTransform(91, 67);

        this.circle3 = this.circle.clone(true);
        this.circle3.setTransform(348, 64);

        this.circle4 = this.circle.clone(true);
        this.circle4.setTransform(350, 162);

        this.triangle = new cjs.Shape();
        this.triangle.graphics.f('').s("#1F1F1F").ss(1).moveTo(0, 83).lineTo(22, 83).lineTo(11, 64).lineTo(0, 83);
        this.triangle.setTransform(77, 87);

        this.triangle2 = this.triangle.clone(true);
        this.triangle2.setTransform(134, 135);

        this.triangle3 = this.triangle.clone(true);
        this.triangle3.setTransform(134, 184);

        this.triangle4 = this.triangle.clone(true);
        this.triangle4.setTransform(335, 135);

        var bluecloud = new lib.ShapeCloud('#2DAFE6', '', 2);
        bluecloud.setTransform(48, 164, 0.052, 0.052);

        var bluecloud2 = bluecloud.clone(true);
        bluecloud2.setTransform(307, 260, 0.052, 0.052);

        var yellowcloud = new lib.ShapeCloud('#FFF050', '#000000', 2);
        yellowcloud.setTransform(48, 210, 0.052, 0.052);

        var yellowcloud2 = yellowcloud.clone(true);
        yellowcloud2.setTransform(307, 215, 0.052, 0.052);

        var redcloud = new lib.ShapeCloud('#E24A44', '', 2);
        redcloud.setTransform(48, 260, 0.052, 0.052);

        var redcloud2 = redcloud.clone(true);
        redcloud2.setTransform(307, 165, 0.052, 0.052);

        this.addChild(bluecloud, bluecloud2, yellowcloud, yellowcloud2, redcloud, redcloud2);

        this.addChild(this.text, this.text_1, this.text_2, this.Rect1, this.roundRect1,
            this.shape_group1, this.shape_group2, this.shape_group3, this.hrline1, this.hrline2, this.vrline1,
            this.Rect2, this.Rect3, this.Rect4, this.Rect5, this.Rect6, this.Rect7,
            this.rectangle, this.rectangle2, this.rectangle3, this.rectangle4,
            this.circle, this.circle2, this.circle3, this.circle4,
            this.triangle, this.triangle2, this.triangle3, this.triangle4);
        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 529, 265);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Rita och måla i rutorna.", "16px 'Myriad Pro'");
        this.text.setTransform(14, 0);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#1A863A");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 512, 242, 10);
        this.roundRect1.setTransform(0, 15);

        var rectContainer_group = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#1A863A').drawRect(0, 0, 153, 153);
        this.Rect1.setTransform(37, 44);

        this.hrline1 = new cjs.Shape();
        this.hrline1.graphics.f('#1A863A').s("#1A863A").ss(1).moveTo(0, 0).lineTo(153, 0)
        this.hrline1.setTransform(37, 95);

        this.hrline2 = new cjs.Shape();
        this.hrline2.graphics.f('#1A863A').s("#1A863A").ss(1).moveTo(0, 0).lineTo(153, 0)
        this.hrline2.setTransform(37, 146);

        this.vrline1 = new cjs.Shape();
        this.vrline1.graphics.f('#1A863A').s("#1A863A").ss(1).moveTo(0, 0).lineTo(0, 153)
        this.vrline1.setTransform(88, 44);

        this.vrline2 = new cjs.Shape();
        this.vrline2.graphics.f('#1A863A').s("#1A863A").ss(1).moveTo(0, 0).lineTo(0, 153)
        this.vrline2.setTransform(139, 44);

        rectContainer_group.addChild(this.Rect1, this.hrline1, this.hrline2, this.vrline1, this.vrline2)

        var dotArr = [];
        var dotlineX = [62.5, 113.5, 164.5, 37, 88, 139, 190, 62.5, 113.5, 164.5, 37, 88, 139, 190,
            62.5, 113.5, 164.5, 37, 88, 139, 190, 62.5, 113.5, 163.5
        ];

        var dotlineY = [44, 44, 44, 69.5, 69.5, 69.5, 69.5, 95, 95, 95, 120.5, 120.5, 120.5, 120.5,
            146, 146, 146, 171.5, 171.5, 171.5, 171.5, 197, 197, 197, 197
        ];


        for (var i = 0; i < dotlineX.length; i++) {
            this.dot = new cjs.Shape();
            this.dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 2);
            this.dot.setTransform(dotlineX[i], dotlineY[i]);
            rectContainer_group.addChild(this.dot);
        }

        var TxtArr = [];
        var TxtlineX = [19, 19, 19, 58, 109, 160, 250, 314, 383, 450, 250, 314, 383, 450, 250];
        var TxtlineY = [74, 126, 176, 214, 214, 214, 97, 97, 97, 97, 170, 170, 170, 170, 243];
        var TxtArrNos = ['C', 'B', 'A', '1', '2', '3', '2A', '2C', '3A', '3C', '1A', '1C', '2B', '3B', '1B'];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text('' + TxtArrNos[i], "15px 'Myriad Pro'", "9D9C9C");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            rectContainer_group.addChild(this.temp_label);
        }

        this.addChild(rectContainer_group);

        var rect_group1 = new cjs.Container();

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("").s('#000000').drawRect(0, 0, 50, 50);
        this.Rect2.setTransform(0, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f('#E24A44').s("#000000").ss(1).moveTo(0, 25).lineTo(50, 25).lineTo(25, 50).lineTo(0, 25);
        this.line1.setTransform(0, 0);

        rect_group1.addChild(this.Rect2, this.line1);
        rect_group1.setTransform(231, 31);

        this.rect_group2 = rect_group1.clone(true);
        this.rect_group2.setTransform(349, 81);
        this.rect_group2.rotation = 180;

        var rect_group3 = new cjs.Container();

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("").s('#000000').drawRect(0, 0, 50, 50);
        this.Rect3.setTransform(0, 0);

        this.line2 = new cjs.Shape();
        this.line2.graphics.f('#2DAFE6').s("#000000").ss(1).moveTo(0, 0).lineTo(0, 25).lineTo(25, 0).lineTo(0, 0);
        this.line2.setTransform(0, 0);

        rect_group3.addChild(this.Rect3, this.line2);
        rect_group3.setTransform(367, 31);

        this.rect_group4 = rect_group3.clone(true);
        this.rect_group4.setTransform(435, 81);
        this.rect_group4.rotation = 270;

        this.rect_group5 = rect_group3.clone(true);
        this.rect_group5.setTransform(281, 105);
        this.rect_group5.rotation = 90;

        this.rect_group6 = rect_group3.clone(true);
        this.rect_group6.setTransform(349, 155);
        this.rect_group6.rotation = 180;

        var rect_group7 = new cjs.Container();

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("").s('#000000').drawRect(0, 0, 50, 50);
        this.Rect3.setTransform(0, 0);

        this.line2 = new cjs.Shape();
        this.line2.graphics.f('#2DAFE6').s("#000000").ss(1).moveTo(0, 25).lineTo(25, 0).lineTo(50, 25).lineTo(25, 50).lineTo(0, 25);
        this.line2.setTransform(0, 0);

        rect_group7.addChild(this.Rect3, this.line2);
        rect_group7.setTransform(367, 105);

        this.rect_group8 = rect_group1.clone(true);
        this.rect_group8.setTransform(435, 155);
        this.rect_group8.rotation = 270;

        this.rect_group9 = rect_group1.clone(true);
        this.rect_group9.setTransform(281, 178);
        this.rect_group9.rotation = 90;

        this.addChild(this.text, this.text_1, this.roundRect1,
            rect_group1, this.rect_group2, rect_group3, this.rect_group4, this.rect_group5, this.rect_group6, rect_group7,
            this.rect_group8, this.rect_group9);
        // for (var i = 0; i < dotArr.length; i++) {
        //     this.addChild(dotArr[i]);
        // }
        // for (var i = 0; i < TxtArr.length; i++) {
        //     this.addChild(TxtArr[i]);
        // }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 250);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 107, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 434, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
