(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p127_1.png",
            id: "p127_1"
        }, {
            src: "images/p127_2.png",
            id: "p127_2"
        }]
    };

    (lib.p127_1 = function() {
        this.initialize(img.p127_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p127_2 = function() {
        this.initialize(img.p127_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 11 och 12", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(377, 651);

        this.text_2 = new cjs.Text("127", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(557, 650);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8390C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Talet 11", "24px 'MyriadPro-Semibold'", "#8390C8");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(115.5, 25);

        this.text_4 = new cjs.Text("44", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(66.7, 22);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52.6, 23.5, 1.2, 1);


        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Part 1
    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("elva", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(40, 122);

        this.text_col1 = new cjs.Text("tiotal", "10.5px 'Myriad Pro'");
        this.text_col1.lineHeight = 19;
        this.text_col1.setTransform(20, 30);

        this.text_col2 = new cjs.Text("ental", "10.5px 'Myriad Pro'");
        this.text_col2.lineHeight = 19;
        this.text_col2.setTransform(66, 30);

        this.text_1 = new cjs.Text("1", "bold 80px 'UusiTekstausMajema'");
        this.text_1.lineHeight = 96;
        this.text_1.setTransform(61, 33, 1, 1.07);

        this.text_no1 = new cjs.Text("1", "bold 80px 'UusiTekstausMajema'");
        this.text_no1.lineHeight = 96;
        this.text_no1.setTransform(14, 33, 1, 1.07);

        this.shape_rect1 = new cjs.Shape();
        this.shape_rect1.graphics.f('#ffffff').s("#000000").ss(0.5).drawRect(9, 23, 97, 95);
        this.shape_rect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 48).lineTo(106, 48);
        this.hrLine_1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(55, 23).lineTo(55, 118);
        this.Line_1.setTransform(0, 0);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#000000").s().p("");
        this.shape_39.setTransform(71, 58.5, 1, 1, 400);

        //start clock

        this.shape = new cjs.Shape(); // clock center dot
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(444, 79.4);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_1.setTransform(439, 71.4, 1, 1, 212);

        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(444, 65.1);

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(436.8, 46);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(423.1, 50.2);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(413.1, 59.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C6CBCC").s().p("AkPBPQhxhvAAifIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACghxBuQhxBxifAAQieAAhxhxg");
        this.shape_3.setTransform(444, 98.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohmQhohniSAAQiRAAhoBnQhoBmAACTIgfAAQAAifBxhvQBxhxCeAAQCfAABxBxQBxBvAACfg");
        this.shape_4.setTransform(444, 60.1);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(409.5, 73.5);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(414.2, 88);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(424.1, 98.2);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(439.6, 102.8);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(453.9, 98.6);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(465.1, 88.3);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(469.6, 74.2);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(464.9, 59.6);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(454, 50);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(444, 45.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(426.8, 49.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(414.3, 62.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(409.7, 79.4);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(414.3, 96.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAJIAJgR");
        this.shape_10.setTransform(426.8, 109.1);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(444, 113.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAJIgJgR");
        this.shape_12.setTransform(461.1, 109.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKAFIgTgJ");
        this.shape_13.setTransform(473.7, 96.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(478.3, 79.4);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAKgEIgTAJ");
        this.shape_15.setTransform(473.7, 62.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(461.1, 49.7);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShnBoQhpBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABpBoQBnBoAACRg");
        this.shape_17.setTransform(444, 79.4);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(444, 79.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(444, 79.4);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(444, 79.4);

        //End clock       

        //  Rec

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQggAAgZAYQgYAYAAAhQAAAiAYAYQAZAYAgAAg");
        this.shape_21.setTransform(214, 51.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_22.setTransform(214, 51.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#0089CA").s("#000000").ss(0.5, 0, 0, 4).arc(0, 0, 8.2, 0, 2 * Math.PI);
        this.shape_52.setTransform(237, 50.7);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_23.setTransform(191.8, 51.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgZAYghAAQghAAgYgYg");
        this.shape_24.setTransform(191.8, 51.1);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_25.setTransform(169.9, 51.1);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQghAAgYgYg");
        this.shape_26.setTransform(169.9, 51.1);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_27.setTransform(146, 51.1);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_28.setTransform(146, 51.1);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao2AAIRsAA");
        this.shape_29.setTransform(191.6, 61.3);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_30.setTransform(226.4, 61.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_31.setTransform(203.2, 61.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_32.setTransform(180.5, 61.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_33.setTransform(158.1, 61.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfQgLAAgLAFQgXAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAIAXgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_34.setTransform(191.8, 61.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAXgMQALgFALAAIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMIgXAFg");
        this.shape_35.setTransform(191.8, 61.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_40.setTransform(229.7, 96.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_41.setTransform(229.7, 96.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_42.setTransform(201.3, 96.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgFAEgGAAQgFAAgFgEg");
        this.shape_43.setTransform(201.3, 96.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_44.setTransform(172.5, 96.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_45.setTransform(172.5, 96.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_46.setTransform(144.2, 96.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_47.setTransform(144.2, 96.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#000000").s().p("AgegWIA9AWIg9AXg");
        this.shape_48.setTransform(250.7, 96.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_49.setTransform(192.5, 96.6);


        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_62.setTransform(146, 72.6);


        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_64.setTransform(170, 72.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_66.setTransform(192, 72.6);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_68.setTransform(214, 72.6);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_70.setTransform(237, 72.6);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_61.setTransform(146, 72.6);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_63.setTransform(170, 72.6);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_65.setTransform(192, 72.6);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_67.setTransform(214, 72.6);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_69.setTransform(237, 72.6);


        // line two
        this.shapetwo_29 = new cjs.Shape();
        this.shapetwo_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao2AAIRsAA");
        this.shapetwo_29.setTransform(321.6, 61.3);

        this.shapetwo_30 = new cjs.Shape();
        this.shapetwo_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_30.setTransform(356.4, 61.5);

        this.shapetwo_31 = new cjs.Shape();
        this.shapetwo_31.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_31.setTransform(333.2, 61.5);

        this.shapetwo_32 = new cjs.Shape();
        this.shapetwo_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_32.setTransform(310.5, 61.5);

        this.shapetwo_33 = new cjs.Shape();
        this.shapetwo_33.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shapetwo_33.setTransform(288.1, 61.5);

        this.shapetwo_34 = new cjs.Shape();
        this.shapetwo_34.graphics.f("#ffffff").s("#000000").ss(0.5).drawRoundRect(0, 0, 113, 44, 5);
        this.shapetwo_34.setTransform(265, 40);

        this.shapetwo_35 = new cjs.Shape();
        this.shapetwo_35.graphics.f("#0089CA").s("#000000").ss(0.5).p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shapetwo_35.setTransform(276, 51.1);

        // Main Yellow block
        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFF173").s("").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 509, 134, 10);
        this.shape_51.setTransform(0, 12);



        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(129, 90).lineTo(329.5 + 60, 90).moveTo(329.5 + 60, 90).lineTo(329.5 + 60, 88).lineTo(336 + 60, 90).lineTo(329.5 + 60, 92).lineTo(329.5 + 60, 90);
        var numberLineLimit = 11
        for (var dot = 0; dot < 13; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(126 + (21.8 * dot), 85).lineTo(126 + (21.8 * dot), 95);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(125, 90).lineTo(126 + (21.8 * numberLineLimit), 90);

        for (var dot = 0; dot < 13; dot++) {
            var temptext = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 13px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot > 9) ? (dot - 0.2) : dot;
            temptext.setTransform(122.5 + (21.8 * colSpace), 112);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);

        this.addChild(this.shape_51, this.shape_rect1, this.hrLine_1, this.Line_1, this.text_no1, this.text_col1, this.text_col2);
        this.addChild(this.shape_39, this.shape_38, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.shape_4, this.shape_3, this.text_4, this.text_3, this.text_2, this.shape_2, this.shape_1, this.shape, this.text_1, this.shape_52, this.text);

        this.addChild(this.numberLine, this.shape_61, this.shape_62, this.shape_63, this.shape_64, this.shape_65, this.shape_66, this.shape_67, this.shape_68);
        this.addChild(this.shape_69, this.shape_70);
        this.addChild(this.shapetwo_34, this.shapetwo_29, this.shapetwo_30, this.shapetwo_31, this.shapetwo_32,
            this.shapetwo_33, this.shapetwo_35);
        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 550, 107.1);

    //Part 4
    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Räkna.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 1);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 1);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 510, 182, 10);
        this.roundRect1.setTransform(0, 10);

        // row-1
        var arrxPos = ['131', '257'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                this.shape_group1.graphics.f("#24B24B").s("#6E6E70").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * 22.5), 43, 9.3, 0, 2 * Math.PI);

                if (i == 1 && column == 4) {
                    this.shape_group1.graphics.f("#24B24B").s("#6E6E70").ss(0.8, 0, 0, 4).arc(xPos + ((columnSpace + 1.7) * 22.5), 43, 9.3, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        // row-2
        this.text_4 = new cjs.Text("5   +   4   +   1  =  ", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(56, 65);
        this.text_5 = new cjs.Text("11   –   1   –   1  =  ", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(299, 65);
        // row-3
        this.text_6 = new cjs.Text("5   +   5   +   1  =  ", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 25;
        this.text_6.setTransform(56, 93);
        this.text_7 = new cjs.Text("11   –   1   –   2  =  ", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 25;
        this.text_7.setTransform(299, 93);
        // row-4
        this.text_8 = new cjs.Text("6   +   3   +   1  =  ", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 25;
        this.text_8.setTransform(56, 121);
        this.text_9 = new cjs.Text("11   –   1   –   4  =  ", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 25;
        this.text_9.setTransform(299, 121);
        // row-5
        this.text_10 = new cjs.Text("6   +   4   +   1  =  ", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 25;
        this.text_10.setTransform(56, 149);
        this.text_11 = new cjs.Text("11   –   1   –   6  =  ", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 25;
        this.text_11.setTransform(299, 149);

        this.text_12 = new cjs.Text("7   +   3   +   1  =  ", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 25;
        this.text_12.setTransform(56, 177);
        this.text_13 = new cjs.Text("11   –   1   –   8  =  ", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 25;
        this.text_13.setTransform(299, 177);

        var textArr = [];

        for (var row = 0; row < 5; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(77, 84).lineTo(105, 84);
            if (row == 4) {
                hrLine_1.setTransform(88, -2 + (row * 27.5));
            } else {
                hrLine_1.setTransform(88, -2 + (row * 27));
            }
            textArr.push(hrLine_1);


            var hrLine_2 = new cjs.Shape();
            hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(77, 84).lineTo(105, 84);
            if (row == 4) {
                hrLine_2.setTransform(335, -2 + (row * 27.5));
            } else {
                hrLine_2.setTransform(335, -2 + (row * 27));
            }
            textArr.push(hrLine_2);
        }


        this.addChild(this.text_q1, this.text_q2, this.roundRect1);
        this.addChild(this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12,
            this.text_13, this.shape_group1);
        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);



    (lib.Symbol4 = function() {
        this.initialize();

        this.instance = new lib.p127_1();
        this.instance.setTransform(0, 22, 0.37, 0.37);

        // Layer 1
        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#7d7d7d").drawRoundRect(0, 0, 512, 200, 10);
        this.round_Rect1.setTransform(0, 22);

        this.text = new cjs.Text("Skriv talen som saknas.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_11 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_11.lineHeight = 27;
        this.text_11.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(33, 40);
        this.hrLine_2.setTransform(70.5, 107);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(33, 40);
        this.hrLine_3.setTransform(101.5, 107);

        var textArr = [];

        for (var row = 0; row < 12; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(30, 40);
            hrLine_1.setTransform(75 + (28 * row), 43);
            textArr.push(hrLine_1);
        }

        for (var row = 0; row < 3; row++) {
            this.hrLine_2 = new cjs.Shape();
            this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
            this.hrLine_2.setTransform(71 + (30 * row), 107);
            textArr.push(this.hrLine_2);

            this.hrLine_2 = new cjs.Shape();
            this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
            this.hrLine_2.setTransform(160 + (28 * row), 107);
            textArr.push(this.hrLine_2);

            this.hrLine_2 = new cjs.Shape();
            this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
            this.hrLine_2.setTransform(243 + (28 * row), 107);
            textArr.push(this.hrLine_2);

            this.hrLine_2 = new cjs.Shape();
            this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
            this.hrLine_2.setTransform(327 + (28 * row), 107);
            textArr.push(this.hrLine_2);
        }

        for (var row = 0; row < 5; row++) {
            this.hrLine_2 = new cjs.Shape();
            this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
            this.hrLine_2.setTransform(42 + (28 * row), 171.5);
            textArr.push(this.hrLine_2);
            if (row != 4) {
                this.hrLine_2 = new cjs.Shape();
                this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
                this.hrLine_2.setTransform(270 + (29 * row), 171.5);
                textArr.push(this.hrLine_2);
            }
        }

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
        this.hrLine_2.setTransform(185, 171.5);
        textArr.push(this.hrLine_2);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
        this.hrLine_2.setTransform(385, 171.5);
        textArr.push(this.hrLine_2);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(32, 40);
        this.hrLine_2.setTransform(412.5, 171.5);
        textArr.push(this.hrLine_2);

        this.text_1 = new cjs.Text("0", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(86, 55);
        textArr.push(this.text_1);

        this.text_1 = new cjs.Text("1", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(114, 55);
        textArr.push(this.text_1);


        this.text_1 = new cjs.Text("9", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(339, 119);
        textArr.push(this.text_1);

        this.text_1 = new cjs.Text("10", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(360.5, 119);
        textArr.push(this.text_1);

        this.text_1 = new cjs.Text("5", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(169, 183.5);
        textArr.push(this.text_1);

        this.text_1 = new cjs.Text("6", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(197, 183.5);
        textArr.push(this.text_1);

        this.text_1 = new cjs.Text("8", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(343, 183.5);
        textArr.push(this.text_1);

        this.text_1 = new cjs.Text("9", "bold 32px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(371, 183.5);
        textArr.push(this.text_1);


        this.addChild(this.round_Rect1, this.instance, this.text, this.text_11, this.hrLine_2, this.hrLine_3);

        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 550, 149.8);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v4 = new lib.Symbol3();
        this.v4.setTransform(315, 472, 1, 1, 0, 0, 0, 256.3, 38.8);

        this.v2 = new lib.Symbol4();
        this.v2.setTransform(315, 277, 1, 1, 0, 0, 0, 255.1, 74.8);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(315, 95.9, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
