(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p10_1.png",
            id: "p8_1"
        }, {
            src: "images/p10_2.png",
            id: "p8_2"
        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1

        this.text = new cjs.Text("16", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(39, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 29, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 10);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, -2);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, -2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 10, 510, 140, 10);
        this.roundRect1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [94, 268, 434, 94, 268, 434, 94, 268, 434, 94, 268, 434];
        var lineY = [25, 25, 25, 52, 52, 52, 77, 77, 77, 101, 101, 101];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("19  –  2  =", "16px 'Myriad Pro'");
        this.label1.setTransform(37, 60);
        this.label2 = new cjs.Text("19  –  4  =", "16px 'Myriad Pro'");
        this.label2.setTransform(37, 85);
        this.label3 = new cjs.Text("19  –  5  =", "16px 'Myriad Pro'");
        this.label3.setTransform(37, 110);
        this.label4 = new cjs.Text("19  –  7  =", "16px 'Myriad Pro'");
        this.label4.setTransform(37, 135);

        this.label6 = new cjs.Text("10  +  9  =", "16px 'Myriad Pro'");
        this.label6.setTransform(208, 60);
        this.label7 = new cjs.Text("17  +  1  =", "16px 'Myriad Pro'");
        this.label7.setTransform(208, 85);
        this.label8 = new cjs.Text("17  +  2  =", "16px 'Myriad Pro'");
        this.label8.setTransform(208, 112);
        this.label9 = new cjs.Text("16  +  3  =", "16px 'Myriad Pro'");
        this.label9.setTransform(208, 135);

        this.label11 = new cjs.Text("15  +  3  =", "16px 'Myriad Pro'");
        this.label11.setTransform(375, 60);
        this.label12 = new cjs.Text("15  +  4  =", "16px 'Myriad Pro'");
        this.label12.setTransform(375, 85);
        this.label13 = new cjs.Text("16  +  3  =", "16px 'Myriad Pro'");
        this.label13.setTransform(375, 112);
        this.label14 = new cjs.Text("17  +  2  =", "16px 'Myriad Pro'");
        this.label14.setTransform(375, 135);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(5, 20).lineTo(465, 20);
        this.Line_1.setTransform(23, 10);


        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(123.5, 33);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_3.setTransform(231, 34);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(343, 34);


        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 19; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.3), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 10) {
                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(55 + (columnSpace * 20.3), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 15) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(65 + (columnSpace * 20.3), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else {

                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(75 + (columnSpace * 20.3), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
                }
            }

        }
        this.shape_group1.setTransform(3, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    (lib.Symbol3 = function() {

        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, -2);
        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, -2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 12, 510, 143, 10);
        this.roundRect1.setTransform(0, 0);


        var lineArr = [];
        var lineX = [94, 268, 434, 94, 268, 434, 94, 268, 434, 94, 268, 434];
        var lineY = [30, 30, 30, 57, 57, 57, 82, 82, 82, 106, 106, 106];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("20  –  2  =", "16px 'Myriad Pro'");
        this.label1.setTransform(37, 65);
        this.label2 = new cjs.Text("20  –  4  =", "16px 'Myriad Pro'");
        this.label2.setTransform(37, 90);
        this.label3 = new cjs.Text("20  –  5  =", "16px 'Myriad Pro'");
        this.label3.setTransform(37, 115);
        this.label4 = new cjs.Text("20  –  3  =", "16px 'Myriad Pro'");
        this.label4.setTransform(37, 140);

        this.label6 = new cjs.Text("20  –  7  =", "16px 'Myriad Pro'");
        this.label6.setTransform(208, 65);
        this.label7 = new cjs.Text("20  –  6  =", "16px 'Myriad Pro'");
        this.label7.setTransform(208, 90);
        this.label8 = new cjs.Text("20  –  8  =", "16px 'Myriad Pro'");
        this.label8.setTransform(208, 115);
        this.label9 = new cjs.Text("20  –  9  =", "16px 'Myriad Pro'");
        this.label9.setTransform(208, 140);

        this.label11 = new cjs.Text("18  +  2  =", "16px 'Myriad Pro'");
        this.label11.setTransform(375, 65);
        this.label12 = new cjs.Text("16  +  4  =", "16px 'Myriad Pro'");
        this.label12.setTransform(375, 90);
        this.label13 = new cjs.Text("17  +  3  =", "16px 'Myriad Pro'");
        this.label13.setTransform(375, 115);
        this.label14 = new cjs.Text("15  +  5  =", "16px 'Myriad Pro'");
        this.label14.setTransform(375, 140);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(5, 20).lineTo(465, 20);
        this.Line_1.setTransform(23, 13);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(123.5, 36);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_3.setTransform(231, 37);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(343, 37);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_5.setTransform(455, 37);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 10) {
                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(55 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 15) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(65 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else {

                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(75 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    (lib.Symbol4 = function() {
        this.initialize();
        this.text = new cjs.Text(" De 3 talen bildar en talfamilj. Skriv svaren.", "16px 'Myriad Pro'");
        this.text.setTransform(19, -10);

        this.text_1 = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, -10);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 5, 510, 165, 10);
        this.roundRect1.setTransform(0, 0);
        
        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 6; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 5 && row == 5) {
                    continue;
                }
                if (column < 3) {
                    this.textbox_group1.graphics.f('#DEDEF1').s("#707070").ss(0.8).drawRect(100 + (columnSpace * 40), 24, 26, 24);
                } else if (column < 6) {
                    this.textbox_group1.graphics.f('#C4E5F0').s("#707070").ss(0.8).drawRect(228 + (columnSpace * 40), 24, 26, 24);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [165, 413, 165, 413, 165, 413, 165, 413];
        var lineY = [42, 42, 65, 65, 90, 90, 114, 115];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }


        this.label1 = new cjs.Text("10  +  5", "16px 'Myriad Pro'");
        this.label1.setTransform(102, 76);
        this.label2 = new cjs.Text("  5 +  10", "16px 'Myriad Pro'");
        this.label2.setTransform(102, 100);
        this.label3 = new cjs.Text("15  –  5", "16px 'Myriad Pro'");
        this.label3.setTransform(104, 125);
        this.label4 = new cjs.Text("15  – 10", "16px 'Myriad Pro'");
        this.label4.setTransform(102, 150);

        this.label6 = new cjs.Text("10  +  9", "16px 'Myriad Pro'");
        this.label6.setTransform(350, 76);
        this.label7 = new cjs.Text("  9  + 10", "16px 'Myriad Pro'");
        this.label7.setTransform(350, 100);
        this.label8 = new cjs.Text("19  –  9", "16px 'Myriad Pro'");
        this.label8.setTransform(352, 125);
        this.label9 = new cjs.Text("19  – 10", "16px 'Myriad Pro'");
        this.label9.setTransform(350, 150);

        var EqArr = [];
        var EqlineX = [161,161,161,161,409,409,409,407];
        var EqlineY = [77,101,126,150,77,101,126,150];

        for (i = 0; i < EqlineX.length; i++) {

            this.text2 = new cjs.Text("=", "16px 'Myriad Pro'");
            this.text2.setTransform(EqlineX[i], EqlineY[i]);
            EqArr.push(this.text2);

        }


        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 0).lineTo(20, 148);
        this.shape_group3.setTransform(260, 15);


        this.text_2 = new cjs.Text("10", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(101, 40);

        this.text_3 = new cjs.Text("5", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(148, 40);

        this.text_4 = new cjs.Text("15", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(182, 40);

        this.text_5 = new cjs.Text("10", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(350, 40);

        this.text_6 = new cjs.Text("9", "16px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(395, 40);

        this.text_7 = new cjs.Text("19", "16px 'Myriad Pro'", "#000000");
        this.text_7.setTransform(430, 40);


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.textbox_group1, this.shape_group3, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < EqArr.length; i++) {
            this.addChild(EqArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 120, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 320, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(294, 520, 1, 1, 0, 0, 0, 255.8, 53.5);


        this.addChild(this.other, this.v1, this.v2, this.v3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
