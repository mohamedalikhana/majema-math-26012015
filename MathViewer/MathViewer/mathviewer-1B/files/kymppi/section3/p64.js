(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p64_1.png",
            id: "p64_1"
        }, {
            src: "images/p64_2.png",
            id: "p64_2"
        }]
    };

    // symbols:
    (lib.p64_1 = function() {
        this.initialize(img.p64_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p64_2 = function() {
        this.initialize(img.p64_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.Symbol3 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("64", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Subtrahera talen från talet i mitten.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 507, 255, 10);
        this.roundRect1.setTransform(0, 12);

        this.instance = new lib.p64_1();
        this.instance.setTransform(24, 25, 0.46, 0.46);
        // set NUmber in wheel 1
        this.label1 = new cjs.Text("9", "16px 'MyriadPro-Semibold'");
        this.label1.setTransform(98.5, 139.5);
        this.label2 = new cjs.Text("7", "16px 'MyriadPro-Semibold'");
        this.label2.setTransform(106, 124);
        this.label3 = new cjs.Text("4", "16px 'MyriadPro-Semibold'");
        this.label3.setTransform(126, 124);
        this.label4 = new cjs.Text("5", "16px 'MyriadPro-Semibold'");
        this.label4.setTransform(136, 139.5);
        this.label5 = new cjs.Text("6", "16px 'MyriadPro-Semibold'");
        this.label5.setTransform(126, 158);
        this.label6 = new cjs.Text("8", "16px 'MyriadPro-Semibold'");
        this.label6.setTransform(106, 157);

        // set NUmber in wheel 2
        this.label7 = new cjs.Text("6", "16px 'MyriadPro-Semibold'");
        this.label7.setTransform(320, 140.5);
        this.label8 = new cjs.Text("10", "16px 'MyriadPro-Semibold'");
        this.label8.setTransform(326, 125);
        this.label9 = new cjs.Text("9", "16px 'MyriadPro-Semibold'");
        this.label9.setTransform(350, 125);
        this.label10 = new cjs.Text("5", "16px 'MyriadPro-Semibold'");
        this.label10.setTransform(360, 140.5);
        this.label11 = new cjs.Text("8", "16px 'MyriadPro-Semibold'");
        this.label11.setTransform(350, 159);
        this.label12 = new cjs.Text("7", "16px 'MyriadPro-Semibold'");
        this.label12.setTransform(330, 158);

        //set number in wheel centre
        this.label13 = new cjs.Text("13", "bold 16px 'Myriad Pro'", "#D5132B");
        this.label13.setTransform(112, 139);
        this.label14 = new cjs.Text("14", "bold 16px 'Myriad Pro'", "#D5132B");
        this.label14.setTransform(335, 139);
        //set line in wheels
        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(86, 72).lineTo(114, 72);
        this.Line_1.setTransform(0, 32);
        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(130, 72).lineTo(155, 72);
        this.Line_2.setTransform(0, 32);
        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(63, 114).lineTo(86, 114);
        this.Line_3.setTransform(0, 32);
        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(154, 114).lineTo(179, 114);
        this.Line_4.setTransform(0, 32);
        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(86, 152).lineTo(112, 152);
        this.Line_5.setTransform(0, 32);
        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(130, 152).lineTo(155, 152);
        this.Line_6.setTransform(0, 32);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(310, 73).lineTo(339, 73);
        this.Line_7.setTransform(0, 32);
        this.Line_8 = new cjs.Shape();
        this.Line_8.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(355, 73).lineTo(383, 73);
        this.Line_8.setTransform(0, 32);
        this.Line_9 = new cjs.Shape();
        this.Line_9.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(288, 115).lineTo(312, 115);
        this.Line_9.setTransform(0, 32);
        this.Line_10 = new cjs.Shape();
        this.Line_10.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(379, 115).lineTo(405, 115);
        this.Line_10.setTransform(0, 32);
        this.Line_11 = new cjs.Shape();
        this.Line_11.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(312, 153).lineTo(339, 153);
        this.Line_11.setTransform(0, 32);
        this.Line_12 = new cjs.Shape();
        this.Line_12.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(354, 153).lineTo(382, 153);
        this.Line_12.setTransform(0, 32);

        this.text_3 = new cjs.Text("9", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(134, 102.3);


        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.text_3);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14)
        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7, this.Line_8, this.Line_9, this.Line_10, this.Line_11, this.Line_12)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 250);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p64_2();
        this.instance.setTransform(43, 50, 0.46, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 33, 250, 101, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 112);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(258, 0);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(258, 112);

        this.text = new cjs.Text("Manuels och Miras kort är lika mycket värda.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_2 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_2.setTransform(19, 20);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(0, 0);

        var ToBeAdded = [];
        var xpos = 20;
        var ypos = 95;
        fColor = '#DFDDF0';
        sColor = '#958FC5';
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 207
            } else {
                ypos = 95
            }

            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 3; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 138
                        } else if (numOfcol == 2) {
                            xpos = 279
                        } else if (numOfcol == 3) {
                            xpos = 397
                        } else {
                            xpos = 20
                        }
                        if (numOfrow == 0 && numOfcol < 1) {
                            fColor = '#DFDDF0';
                            sColor = '#958FC5';
                        }
                        if (numOfrow == 1 && numOfcol < 1) {
                            fColor = '#C4E5F0';
                            sColor = '#00A3C4';
                        }
                        if (numOfrow == 0 && numOfcol > 1) {
                            fColor = '#E1EBC5';
                            sColor = '#90BD22';
                        }
                        if (numOfrow == 1 && numOfcol > 1) {
                            fColor = '#FBD3B9';
                            sColor = '#EB5B1B';
                        }
                        if (column == 2 && numOfcol == 1) {
                            fColor = '#FFFFFF';
                            sColor = '#706F6F';
                        }
                        if (column == 2 && numOfcol == 3) {
                            fColor = '#FFFFFF';
                            sColor = '#706F6F';
                        }
                        if (column == 1 && numOfrow == 1) {
                            ypos = ypos - 4
                        } else if (column == 0 && numOfrow == 1) {
                            ypos = 207
                        } else if (column == 2 && numOfrow == 1) {
                            ypos = 207
                        }
                        if (column == 1 && numOfrow == 0) {
                            ypos = ypos - 4
                        } else if (column == 0 && numOfrow == 0) {
                            ypos = 95
                        } else if (column == 2 && numOfrow == 0) {
                            ypos = 95
                        }
                        tmp_Rect.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xpos + (columnSpace * 33), ypos + (row * 20), 23, 24);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }

        var xpos = 27;
        var ypos = 112;  
        var tmpNum=[5,3,2,4,2,2,6,2,4,1,3,3,6,2,2,6,3,5,7,4];
        var i=0;      
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 223
            } else {
                ypos = 112
            }

            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 3; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 145
                        } else if (numOfcol == 2) {
                            xpos = 285
                        } else if (numOfcol == 3) {
                            xpos = 405
                        } else {
                            xpos = 27
                        }
                         if (column == 2 && numOfcol == 1) {
                            continue;
                        }
                        if (column == 2 && numOfcol == 3) {
                          continue;
                        } 

                        if (column == 1 && numOfrow == 1) {
                            ypos = ypos - 3
                        } else if (column == 0 && numOfrow == 1) {
                            ypos = 223
                        } else if (column == 2 && numOfrow == 1) {
                            ypos = 223
                        }
                        if (column == 1 && numOfrow == 0) {
                            ypos = ypos - 3
                        } else if (column == 0 && numOfrow == 0) {
                            ypos = 112
                        } else if (column == 2 && numOfrow == 0) {
                            ypos = 112
                        }
                        var tempText = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                        tempText.setTransform(xpos + (columnSpace * 33), ypos + (row * 20));
                        ToBeAdded.push(tempText);
                        i++;
                    }
                }
            }
        }



        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.text, this.text_1, this.text_2, this.instance);       
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(300, 430, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(300, 122, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
