(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p125_1.png",
            id: "p125_1"
        }, {
            src: "images/p122_2.png",
            id: "p122_2"
        }]
    };

    (lib.p125_1 = function() {
        this.initialize(img.p125_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p122_2 = function() {
        this.initialize(img.p122_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("125", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(549, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(576.3, 660.8);

        this.instance = new lib.p125_1();
        this.instance.setTransform(50, 26, 0.535, 0.52);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(465 + (columnSpace * 32), 38, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 510, 155, 10);
        this.roundRect1.setTransform(0, 10);

        // row-1
        this.text_1 = new cjs.Text("7  –        =  6", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(48, 37);
        this.text_2 = new cjs.Text("9  –        =  8", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 27;
        this.text_2.setTransform(207, 37);
        this.text_3 = new cjs.Text("9  –        =  9", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 27;
        this.text_3.setTransform(363, 37);
        // row-2
        this.text_4 = new cjs.Text("7  –        =  4", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 27;
        this.text_4.setTransform(48, 65);
        this.text_5 = new cjs.Text("9  –        =  6", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 27;
        this.text_5.setTransform(207, 65);
        this.text_6 = new cjs.Text("9  –        =  0", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 27;
        this.text_6.setTransform(363, 65);
        // row-3
        this.text_7 = new cjs.Text("7  –        =  2", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 27;
        this.text_7.setTransform(48, 93);
        this.text_8 = new cjs.Text("9  –        =  7", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 27;
        this.text_8.setTransform(207, 93);
        this.text_9 = new cjs.Text("9  –        =  3", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 27;
        this.text_9.setTransform(363, 93);
        // row-4
        this.text_10 = new cjs.Text("7  –        =  1", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 27;
        this.text_10.setTransform(48, 121);
        this.text_11 = new cjs.Text("9  –        =  5", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 27;
        this.text_11.setTransform(207, 121);
        this.text_12 = new cjs.Text("9  –        =  1", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 27;
        this.text_12.setTransform(363, 121);
        // row-5
        this.text_13 = new cjs.Text("7  –        =  3", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 27;
        this.text_13.setTransform(48, 149);
        this.text_14 = new cjs.Text("9  –        =  4", "16px 'Myriad Pro'");
        this.text_14.lineHeight = 27;
        this.text_14.setTransform(207, 149);
        this.text_15 = new cjs.Text("9  –        =  2", "16px 'Myriad Pro'");
        this.text_15.lineHeight = 27;
        this.text_15.setTransform(363, 149);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(77, 55).lineTo(97, 55).moveTo(236, 55).lineTo(256, 55).moveTo(393, 55).lineTo(413, 55)
            .moveTo(77, 84).lineTo(97, 84).moveTo(236, 84).lineTo(256, 84).moveTo(393, 84).lineTo(413, 84)
            .moveTo(77, 111).lineTo(97, 111).moveTo(236, 111).lineTo(256, 111).moveTo(393, 111).lineTo(413, 111)
            .moveTo(77, 140).lineTo(97, 140).moveTo(236, 140).lineTo(256, 140).moveTo(393, 140).lineTo(413, 140)
            .moveTo(77, 168).lineTo(97, 168).moveTo(236, 168).lineTo(256, 168).moveTo(393, 168).lineTo(413, 168);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.text_q1, this.text_q2, this.roundRect1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12,
            this.text_13, this.text_14, this.text_15, this.hrLine_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 570, 200);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 510, 155, 10);
        this.roundRect1.setTransform(0, 10);

        // row-1
        var arrxPos = ['131', '257'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                this.shape_group1.graphics.f("#DA2129").s("#6E6E70").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * 22.5), 43, 9.3, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        // row-2
        this.text_4 = new cjs.Text("10  –        =    9", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 27;
        this.text_4.setTransform(40, 65);
        this.text_5 = new cjs.Text("10  –        =  5", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 27;
        this.text_5.setTransform(199, 65);
        this.text_6 = new cjs.Text("10  –        =  3", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 27;
        this.text_6.setTransform(355, 65);
        // row-3
        this.text_7 = new cjs.Text("10  –        =    7", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 27;
        this.text_7.setTransform(40, 93);
        this.text_8 = new cjs.Text("10  –        =  6", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 27;
        this.text_8.setTransform(199, 93);
        this.text_9 = new cjs.Text("10  –        =  1", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 27;
        this.text_9.setTransform(355, 93);
        // row-4
        this.text_10 = new cjs.Text("10  –        =    8", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 27;
        this.text_10.setTransform(40, 121);
        this.text_11 = new cjs.Text("10  –        =  4", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 27;
        this.text_11.setTransform(199, 121);
        this.text_12 = new cjs.Text("10  –        =  7", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 27;
        this.text_12.setTransform(355, 121);
        // row-5
        this.text_13 = new cjs.Text("10  –        =  10", "16px 'Myriad Pro'");
        this.text_13.lineHeight = 27;
        this.text_13.setTransform(40, 149);
        this.text_14 = new cjs.Text("10  –        =  8", "16px 'Myriad Pro'");
        this.text_14.lineHeight = 27;
        this.text_14.setTransform(199, 149);
        this.text_15 = new cjs.Text("10  –        =  2", "16px 'Myriad Pro'");
        this.text_15.lineHeight = 27;
        this.text_15.setTransform(355, 149);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(77, 84).lineTo(97, 84).moveTo(236, 84).lineTo(256, 84).moveTo(393, 84).lineTo(413, 84)
            .moveTo(77, 111).lineTo(97, 111).moveTo(236, 111).lineTo(256, 111).moveTo(393, 111).lineTo(413, 111)
            .moveTo(77, 140).lineTo(97, 140).moveTo(236, 140).lineTo(256, 140).moveTo(393, 140).lineTo(413, 140)
            .moveTo(77, 168).lineTo(97, 168).moveTo(236, 168).lineTo(256, 168).moveTo(393, 168).lineTo(413, 168);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.text_q1, this.text_q2, this.roundRect1);
        this.addChild(this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12,
            this.text_13, this.text_14, this.text_15, this.hrLine_1, this.shape_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 170);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(313, 487, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(313, 676, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
