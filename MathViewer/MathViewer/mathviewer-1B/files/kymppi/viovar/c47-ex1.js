var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }]
    };

    lib.init_Magic_Wand();

    var iconProperties = {
        x: 850 + 300,
        y: 310,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 150,
        wrongY: 345 - 15
    };

    var boxProperties = {
        width: 90,
        height: 100
    };

    var objects = [];

    (lib.tick = function() {
        this.initialize(img.tick);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);


    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function generateImages(images, imageCount, X, Y, scaleX, scaleY, image) {
        X = 280, Y = 100;
        if (imageCount < 5) {
            //X=X+(100*((5-imageCount-1)));
            switch (imageCount) {
                case 1:
                    X = X + 200;
                    break;
                case 2:
                    X = X + 150;
                    break;
                case 3:
                    X = X + 100;
                    break;
                case 4:
                    X = X + 50;
                    break;
            }

        }
        for (var i = 0; i < imageCount; i++) {
            var iteration = i;
            if (5 <= i) {
                iteration = i - 5;
                Y = 200;
            }
            var tempImage = image.clone(true);
            tempImage.setTransform(X + (100 * iteration), Y, 0.5, 0.5);
            images.addChild(tempImage);
        }
        return images;
    }

    (lib.dice = function(count, size) {
        this.initialize();
        //console.log(size);
        thisStage = this;
        this.square = new cjs.Shape();
        this.square.graphics.f("#ffffff").ss(0.5).s('black').drawRoundRect(0, 0, size, size, 20);
        this.addChild(this.square);
        switch (count) {
            case 1:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 2:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 3, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 2 / 3, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;

            case 3:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 4:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 5:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 6:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 9:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size / 2, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size / 2, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(size * 3 / 4, size * 3 / 4, size / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.numberBox = function(count, properties, number) {
        this.initialize();
        //console.log(ballWidth);
        var ballWidth = properties.width;
        thisStage = this;
        this.square = new cjs.Shape();
        this.square.graphics.f("#ffffff").ss(0.5).s('black').drawRoundRect(0, 0, properties.width, properties.height, 6);
        ballWidth = (properties.width + properties.height) / 1.35;
        this.addChild(this.square);
        switch (count) {
            case 1:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 2:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 2 / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;

            case 3:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 4:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 5:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 6:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 9:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case -1:
                this.number = new cjs.Text('' + number, "72px 'Myriad Pro'", '#000000');
                this.number.textAlign = 'center';
                this.number.setTransform(45, 74)
                this.addChild(this.number);
                break;
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();
        this.hintText1 = new cjs.Text("Klicka på talet som passar.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 500);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text(" ", "16px 'Myriad Pro'");
        this.text_1.textAlign = 'center';
        this.text_1.setTransform(0, 10);

        this.text_2 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_2.textAlign = 'center';
        this.text_2.setTransform(18, 10);

        this.text_3 = new cjs.Text(" ", "16px 'Myriad Pro'");
        this.text_3.textAlign = 'center';
        this.text_3.setTransform((16.5 * 2), 10);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.textAlign = 'center';
        this.text_4.setTransform((16.5 * 3), 10);

        this.text_5 = new cjs.Text(" ", "16px 'Myriad Pro'");
        this.text_5.textAlign = 'center';
        this.text_5.setTransform((17.5 * 4), 10);

        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    function handleOver(evt) {
        var box = evt.currentTarget;
        box.children[0].graphics.clear().ss(0.5).s('#000000').beginLinearGradientFill(["#ADD8E6", "#ADD8E6"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, boxProperties.width, boxProperties.height, 5);
    }

    function handleOut(evt) {
        var box = evt.currentTarget;
        box.children[0].graphics.clear().ss(0.5).s('#000000').beginLinearGradientFill(["#FFFFFF", "#ffffff"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, boxProperties.width, boxProperties.height, 5);
    }


    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        //this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.addChild(this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        thisStage.currentAnswer = 0;

        this.answers = new lib.answers();
        this.answers.setTransform(300, 40, 10, 10);

        var myStage = this;
        this.addChild(this.answers);

        var boxes = [],
            boxesX = 450,
            boxesY = 300;
        var boxeCount = 3;

        var nums = [];

        nums[0] = [32, 31, 34];
        nums[1] = [40, 41, 42];
        nums[2] = [22, 23, 21];
        nums[3] = [76, 74, 72];
        nums[4] = [63, 61, 62];

        for (var i = 0; i < boxeCount; i++) {
            var box = new lib.numberBox(-1, {
                width: boxProperties.width,
                height: boxProperties.height
            }, nums[0][i]);

            box.setTransform(boxesX + (150 * i), boxesY, 1.4, 1.4);
            box.value = nums[0][i];
            myStage.addChild(box);
            boxes.push(box);
            box.addEventListener('click', checkAnswer);
            box.on("mouseover", handleOver);
            box.on("mouseout", handleOut);
        };

        myStage.answer = 0;

        function checkAnswer(e) {
            if (e.currentTarget.value == myStage.answer) {
                myStage.onAnswerCorrect(e.currentTarget);
                myStage.answers.children[4].text = myStage.answer;
            } else {
                myStage.onAnswerIncorrect(e.currentTarget);
                myStage.answers.children[4].text = " ";
            }
        }

        exerciseCount = 0;

        this.onNewExercise = function(e) {
            if (exerciseCount < 6) {
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                for (var i = 0; i < boxes.length; i++) {
                    boxes[i].number.color = "#000000";
                    boxes[i].number.text = nums[exerciseCount][i] + "";
                    boxes[i].value = nums[exerciseCount][i];
                };

                var numCol1 = [36, 46, 29, 78, 64];
                var numCol2 = [4, 5, 7, 6, 3];

                myStage.answers.children[0].text = numCol1[exerciseCount] + "";
                myStage.answers.children[2].text = numCol2[exerciseCount] + "";
                myStage.answers.children[4].text = " ";

                myStage.answer = numCol1[exerciseCount] - numCol2[exerciseCount];

                new lib.Magic_Wand(myStage, false);

                if (exerciseCount > 0) {
                    $(btn_newExercise).hide();
                    $(btn_newExercise).addClass('disabled');
                }

                exerciseCount = exerciseCount + 1;
            }
            if (typeof btn_newExercise != 'undefined') {
                $(btn_newExercise).hide();
            }
        };
        var showIncorrectAnswerTween = null;
        var currentTimeoutAnimation = null;
        this.onAnswerCorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = true;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            box.number.color = "#008000";
            if (exerciseCount < 5) {
                currentTimeoutAnimation = setTimeout(function() {
                    $(btn_newExercise).removeClass('disabled');
                    $(btn_newExercise).show();
                }, 1000)
            }
            stage.update();
        };
        this.onAnswerIncorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            box.number.color = "#ff0000";
            $(btn_newExercise).hide();
            stage.update();
            showIncorrectAnswerTween = cjs.Tween.get(box.number).wait(4000).call(function(e) {
                box.number.color = "#000000";
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                stage.update();
            });
        };

        this.onIncorrectAnswer = function() {
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = true;
            // for (var i = 0; i < circles.length; i++) {
            //     var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
            //         resetFillStyle[1] = "#ffffff";
            // }
            cjs.Tween.get(myStage.dices).wait(500).call(function(e) {
                for (var i = 0; i < myStage.dices.children.length; i++) {
                    var resetFillStyle = myStage.dices.children[i].square.graphics._fillInstructions[0].params;
                    if (resetFillStyle[1] === "#ffff00") {
                        resetFillStyle[1] = "#ffffff";
                    }
                }
                stage.update();
            });

        };

        this.onNewExercise();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Subtrahera ental", "bold 24px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(96.5, 25);

        this.text_1 = new cjs.Text("47", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(48, 24);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.balls = function() {
        this.initialize();
        //function () {



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)



        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
