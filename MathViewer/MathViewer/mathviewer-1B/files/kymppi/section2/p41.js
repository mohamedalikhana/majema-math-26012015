(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p41_1.png",
            id: "p41_1"
        }]
    };

    (lib.p41_1 = function() {
        this.initialize(img.p41_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("41", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(551, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(460 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(4, 0);

        this.text_1 = new cjs.Text("Rita cirklar och addera.", "16px 'Myriad Pro'");
        this.text_1.setTransform(23, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 15, 254, 86, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(258, 15, 254, 86, 10);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 106, 254, 86, 10);
        this.roundRect3.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(258, 106, 254, 86, 10);
        this.roundRect4.setTransform(0, 0);

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 197, 254, 86, 10);
        this.roundRect5.setTransform(0, 0);

        this.instance = new lib.p41_1();
        this.instance.setTransform(285, 205, 0.466, 0.466);

        this.textbox_group1 = new cjs.Shape();
        var numberofBoxs = 2;
        var Ypos = 0;
        var Xpos = 0;
        for (var i = 0; i < 10; i++) {

            for (var col = 0; col < 5; col++) {
                for (var row = 0; row < 2; row++) {
                    if (i == 1) {
                        Xpos = 120
                    } else if (i == 2) {
                        Xpos = 258
                    } else if (i == 3) {
                        Xpos = 379
                    } else if (i == 4) {
                        Xpos = 0, Ypos = 92
                    } else if (i == 5) {
                        Xpos = 120, Ypos = 92
                    } else if (i == 6) {
                        Xpos = 258, Ypos = 92
                    } else if (i == 7) {
                        Xpos = 379, Ypos = 92
                    } else if (i == 8) {
                        Xpos = 0, Ypos = 182
                    } else if (i == 9) {
                        Xpos = 120, Ypos = 182
                    } else if (i == 10) {
                        Xpos = 258, Ypos = 182
                    } else if (i == 11) {
                        Xpos = 379, Ypos = 182
                    } else {
                        Ypos = 0;
                        Xpos = 0
                    }
                    this.textbox_group1.graphics.s("#949599").ss(0.4).drawRect((13 + Xpos) + (col * 20.5), (27 + Ypos) + (row * 20.5), 20.5, 20.5);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        //------------------draw balls left side
        this.shape_group1 = new cjs.Shape();
        var xpos = 25;
        var ypos = 38;
        var fillcolor = "";
        var ssVal = 0.5;

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (row == 1) {
                    xpos = 25;
                    ypos = 44;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 25;
                    ypos = 38;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    continue;
                    ssVal = 0.8;
                }
                if (row == 1 && column == 3) {
                    continue;
                    ssVal = 0.8;
                }
                if (row == 1 && column == 2) {
                    continue;
                    ssVal = 0.8;
                }
                if (row == 2) {
                    ypos = 101;
                    fillcolor = "#D51317";
                }
                if (row == 3) {
                    ypos = 107;
                    fillcolor = "#D51317";
                }
                if (row == 4) {
                    ypos = 163;
                    fillcolor = "#D51317";
                }
                if (row == 5) {
                    ypos = 170;
                    fillcolor = "#D51317";
                }

                if (row == 3 && column >= 1) {
                    continue;
                }
                if (row == 5 && column >= 1) {
                    continue;
                }
                if (row == 1 && column == 1) {
                    continue;
                }

                this.shape_group1.graphics.f(fillcolor).s("#6E6E70").ss(ssVal, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        //----------------------Draw balls right
        this.shape_group2 = new cjs.Shape();
        var xpos = 282;
        var ypos = 38;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (row == 1) {
                    xpos = 282;
                    ypos = 44;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 282;
                    ypos = 38;
                    fillcolor = "#D51317";
                }

                if (row == 2) {
                    ypos = 101;
                    fillcolor = "#D51317";
                }
                if (row == 3) {
                    ypos = 108;
                    fillcolor = "#D51317";
                }

                if (row == 1 && column >= 1) {
                    continue;
                }
                if (row == 3 && column >= 1) {
                    continue;
                }


                this.shape_group2.graphics.f(fillcolor).s("#6E6E70").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20.5), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group2.setTransform(0, 0);

        //write number
        this.label1 = new cjs.Text("6  +  5  =", "16px 'Myriad Pro'");
        this.label1.setTransform(73, 87);
        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.s("##707070").ss(0.4).moveTo(134, 93).lineTo(175, 93);

        this.label2 = new cjs.Text("6  +  6  =", "16px 'Myriad Pro'");
        this.label2.setTransform(73, 179);
        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.s("##707070").ss(0.4).moveTo(134, 185).lineTo(175, 185);

        this.label3 = new cjs.Text("6  +  9  =", "16px 'Myriad Pro'");
        this.label3.setTransform(73, 269);
        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.s("##707070").ss(0.4).moveTo(134, 275).lineTo(175, 275);

        this.label4 = new cjs.Text("6  +  7  =", "16px 'Myriad Pro'");
        this.label4.setTransform(332, 87);
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.s("##707070").ss(0.4).moveTo(433, 93).lineTo(390, 93);

        this.label5 = new cjs.Text("6  +  8  =", "16px 'Myriad Pro'");
        this.label5.setTransform(332, 179);
        this.hrLine_5 = new cjs.Shape();
        this.hrLine_5.graphics.s("##707070").ss(0.4).moveTo(433, 185).lineTo(390, 185);

        this.label6 = new cjs.Text("Fyll på upp", "14px 'Myriad Pro'");
        this.label6.setTransform(310, 235);
        this.label7 = new cjs.Text("till 10 först.", "14px 'Myriad Pro'");
        this.label7.setTransform(310, 255);

        this.addChild(this.instance, this.text_1, this.text);
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.textbox_group1, this.shape_group1, this.shape_group2)
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7)
        this.addChild(this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.hrLine_5)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 497.3, 280);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(18, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(4, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 12, 511, 234, 10);
        this.roundRect1.setTransform(0, 0);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(25, 0).lineTo(473, 0);
        this.Line_1.setTransform(5, 45);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 46;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 56;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 66;
                } else {
                    fillcolor = "#008BD2";
                    xpos = 76;
                }

                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 45 + (row * 19), 7.9, 0, 2 * Math.PI);

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(123, 48);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_3.setTransform(229, 48);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#ffffff");
        this.text_4.setTransform(340, 48);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#ffffff");
        this.text_5.setTransform(450, 48);
        //number in column one
        this.label1 = new cjs.Text("6  +  5  =", "16px 'Myriad Pro'");
        this.label1.setTransform(50, 90);
        this.label2 = new cjs.Text("6  +  6  =", "16px 'Myriad Pro'");
        this.label2.setTransform(50, 120);
        this.label3 = new cjs.Text("7  +  6  =", "16px 'Myriad Pro'");
        this.label3.setTransform(50, 150);
        this.label4 = new cjs.Text("7  +  8  =", "16px 'Myriad Pro'");
        this.label4.setTransform(50, 180);
        this.label5 = new cjs.Text("7  +  5  =", "16px 'Myriad Pro'");
        this.label5.setTransform(50, 210);
        //number in column TWO
        this.label6 = new cjs.Text("7  +  7  =", "16px 'Myriad Pro'");
        this.label6.setTransform(205, 90);
        this.label7 = new cjs.Text("8  +  7  =", "16px 'Myriad Pro'");
        this.label7.setTransform(205, 120);
        this.label8 = new cjs.Text("8  +  8  =", "16px 'Myriad Pro'");
        this.label8.setTransform(205, 150);
        this.label9 = new cjs.Text("9  +  8  =", "16px 'Myriad Pro'");
        this.label9.setTransform(205, 180);
        this.label10 = new cjs.Text("6  +  8  =", "16px 'Myriad Pro'");
        this.label10.setTransform(205, 210);

        //number in column Three
        this.label11 = new cjs.Text("9  +  9  =", "16px 'Myriad Pro'");
        this.label11.setTransform(368, 90);
        this.label12 = new cjs.Text("7  +  9  =", "16px 'Myriad Pro'");
        this.label12.setTransform(368, 120);
        this.label13 = new cjs.Text("6  +  9  =", "16px 'Myriad Pro'");
        this.label13.setTransform(368, 150);
        this.label14 = new cjs.Text("8  +  9  =", "16px 'Myriad Pro'");
        this.label14.setTransform(368, 180);
        this.label15 = new cjs.Text("6  +  7  =", "16px 'Myriad Pro'");
        this.label15.setTransform(368, 210);

        var ToBeAdded = [];
        var tmpline = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var colSpace = column;
            for (var row = 0; row < 5; row++) {
                var rowSpace = row;
                tmpline.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(108 + (160 * colSpace), 98 + (30 * rowSpace)).lineTo(153 + (160 * colSpace), 98 + (30 * rowSpace));
                tmpline.setTransform(0, 0);
                ToBeAdded.push(tmpline);
            }
        }


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label5);
        this.addChild(this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5)
        this.addChild(this.label11, this.label12, this.label13, this.label14, this.label15, this.label10)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(308, 284, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(308, 389, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
