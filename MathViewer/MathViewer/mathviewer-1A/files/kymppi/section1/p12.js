(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/s12_13_1.png",
            id: "s12_13_1"
        }, {
            src: "images/s12_13_2.png",
            id: "s12_13_2"
        }, {
            src: "images/s12_13_3.png",
            id: "s12_13_3"
        }, {
            src: "images/s12_13_4.png",
            id: "s12_13_4"
        }]
    };

    // symbols:
    (lib.s12_13_1 = function() {
        this.initialize(img.s12_13_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 589, 67);


    (lib.s12_13_2 = function() {
        this.initialize(img.s12_13_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 589, 66);


    (lib.s12_13_3 = function() {
        this.initialize(img.s12_13_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1020, 370);


    (lib.s12_13_4 = function() {
        this.initialize(img.s12_13_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1020, 353);


    (lib.Symbol20 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("två", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(45.3, 77.9);

        this.text_1 = new cjs.Text("2", "bold 80px 'UusiTekstausMajema'");
        this.text_1.lineHeight = 96;
        this.text_1.setTransform(41.6, 0);
        // arc above num
        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f("#000000").s().p("AgegVIA9AVIg9AXg");
        this.shape_36.setTransform(61, 17.1);
        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AgxAzQALgnAjgdQAfgdAbAA");
        this.shape_37.setTransform(53.9, 21.9);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(427.2, 56.3);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D7172F").s().p("AheApICyhlIALATIiyBmg");
        this.shape_1.setTransform(436.2, 51.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(427.3, 42.1);

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(420.1, 23);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(406.4, 27.1);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(396.4, 36.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C6CBCC").s().p("AkPBPQhxhuAAigIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACfhxBvQhxBxifAAQieAAhxhxg");
        this.shape_3.setTransform(427.2, 75.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohmQhohniSAAQiRAAhoBnQhoBmAACTIgfAAQAAifBxhvQBxhxCeAAQCfAABxBxQBxBvAACfg");
        this.shape_4.setTransform(427.2, 37.1);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(392.8, 50.5);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(397.5, 65);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(407.3, 75.1);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(422.8, 79.8);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(437.2, 75.5);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(448.3, 65.3);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(452.9, 51.2);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(448.1, 36.6);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(437.3, 26.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(427.3, 22.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(410.1, 26.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(397.6, 39.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(393, 56.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(397.6, 73.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAKIAJgT");
        this.shape_10.setTransform(410.1, 86);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(427.3, 90.6);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAKIgJgT");
        this.shape_12.setTransform(444.4, 86);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJAFIgRgJ");
        this.shape_13.setTransform(456.9, 73.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(461.5, 56.3);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJgEIgRAJ");
        this.shape_15.setTransform(456.9, 39.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(444.4, 26.6);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShoBoQhoBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRg");
        this.shape_17.setTransform(427.2, 56.3);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(427.2, 56.3);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(427.2, 56.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(427.2, 56.3);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAgggYgZQgYgYgiAAQghAAgYAYQgYAZAAAgQAAAiAYAYQAYAYAhAAg");
        this.shape_21.setTransform(215.4, 26.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#0089CA").s().p("Ag5A6QgYgYAAgiQAAggAYgZQAYgYAhAAQAiAAAYAYQAYAZAAAgQAAAigYAYQgYAYgiAAQghAAgYgYg");
        this.shape_22.setTransform(215.4, 26.5);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAgggYgZQgYgYgiAAQghAAgYAYQgYAZAAAgQAAAiAYAYQAYAYAhAAg");
        this.shape_23.setTransform(192.5, 26.5);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#0089CA").s().p("Ag5A6QgYgYAAgiQAAggAYgZQAZgYAgAAQAiAAAYAYQAYAZAAAgQAAAigYAYQgYAYgiAAQggAAgZgYg");
        this.shape_24.setTransform(192.5, 26.5);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_25.setTransform(237.9, 39);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_26.setTransform(272.7, 39.2);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_27.setTransform(249.5, 39.2);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_28.setTransform(226.9, 39.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_29.setTransform(204.4, 39.2);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfIgXAFQgWAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAQALAAAMgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_30.setTransform(238.2, 39);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAWgMIAXgFIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMQgMAFgLAAg");
        this.shape_31.setTransform(238.2, 39);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_32.setTransform(218.5, 76.1);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_33.setTransform(218.5, 76.1);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_34.setTransform(190.2, 76.1);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_35.setTransform(190.2, 76.1);

        this.text_14 = new cjs.Text("2", "bold 16px 'Myriad Pro'", "#0089CA");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(211.6, 76.8);

        this.text_15 = new cjs.Text("1", "bold 16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(184, 76.8);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f("#000000").s().p("AgegWIA9AWIg9AWg");
        this.shape_38.setTransform(240.2, 76.1);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AkbAAII3AA");
        this.shape_39.setTransform(210.2, 76.1);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gHZQgHAAgLACQgVAEgSAIQg5AdAABHIAALQIACARQAEAWAIARQAdA5BHAAMBL/AAAIASgCQAVgEASgIQA5gdAAhGIAArQQAAgdgOgcQgdg5hHAAg");
        this.shape_40.setTransform(254.6, 56.1);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFF173").s().p("Egl/AHaQhHAAgdg5QgIgRgEgWIgCgRIAArQQAAhHA5gdQASgIAVgEQALgCAHAAMBL/AAAQBHAAAdA5QAOAcAAAdIAALQQAABGg5AdQgSAIgVAEIgSACg");
        this.shape_41.setTransform(254.6, 56.1);

        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(130, 66).lineTo(330.5, 66).moveTo(330.5, 66).lineTo(330.5, 64).lineTo(337, 66).lineTo(330.5, 68).lineTo(330.5, 66);
        var numberLineLimit = 2
        for (var dot = 0; dot < 11; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(130 + (20 * dot), 61).lineTo(130 + (20 * dot), 71);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(129, 66).lineTo(130 + (20 * numberLineLimit), 66);

        for (var dot = 0; dot < 11; dot++) {
            var temptext = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 13px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot == 10) ? 9.8 : dot;
            temptext.setTransform(127 + (20 * colSpace), 88);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);

        this.addChild(this.shape_41, this.shape_40, this.shape_37, this.shape_36, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.shape_4, this.shape_3, this.text_4, this.text_3, this.text_2, this.shape_2, this.shape_1, this.shape, this.text_1, this.text, this.numberLine);
        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 511.3, 104.5);


    (lib.Symbol18 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("ett", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(45, 77.9);

        this.text_1 = new cjs.Text("1", "bold 80px 'UusiTekstausMajema'");
        this.text_1.lineHeight = 96;
        this.text_1.setTransform(41.1, 0);
        // arc above num
        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f("#000000").s().p("AgVggIArAHIgdA5g");
        this.shape_32.setTransform(54.5, 37.3);
        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAKhKIgTCV");
        this.shape_33.setTransform(55.5, 28.3);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(427.2, 56);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D7172F").s().p("Ag8BUIBmiyIATALIhlCyg");
        this.shape_1.setTransform(432.4, 47);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(427.3, 41.7);

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(420.1, 22.6);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(406.4, 26.8);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(396.4, 35.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C6CBCC").s().p("AkPBPQhxhuAAigIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACfhxBvQhxBxifAAQieAAhxhxg");
        this.shape_3.setTransform(427.2, 75.2);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohlQhohoiSAAQiRAAhoBoQhoBlAACTIgfAAQAAigBxhuQBxhxCeAAQCfAABxBxQBxBuAACgg");
        this.shape_4.setTransform(427.2, 36.7);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(392.8, 50.1);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(397.5, 64.6);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(407.3, 74.8);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(422.8, 79.4);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(437.2, 75.2);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(448.3, 64.9);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(452.9, 50.8);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(448.1, 36.2);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(437.3, 26.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(427.3, 21.7);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(410.1, 26.3);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(397.6, 38.8);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(393, 56);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(397.6, 73.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAKIAJgT");
        this.shape_10.setTransform(410.1, 85.7);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(427.3, 90.3);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAKIgJgT");
        this.shape_12.setTransform(444.4, 85.7);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJAFIgRgJ");
        this.shape_13.setTransform(456.9, 73.1);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(461.5, 56);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJgEIgRAJ");
        this.shape_15.setTransform(456.9, 38.8);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(444.4, 26.3);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShoBoQhoBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRg");
        this.shape_17.setTransform(427.2, 56);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(427.2, 56);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(427.2, 56);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(427.2, 56);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_21.setTransform(192.5, 28.3);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_22.setTransform(192.5, 28.3);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_23.setTransform(237.9, 39.6);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_24.setTransform(272.7, 39.8);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_25.setTransform(249.5, 39.8);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_26.setTransform(226.9, 39.8);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_27.setTransform(204.4, 39.8);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfQgLAAgMAFQgWAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAIAXgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_28.setTransform(238.2, 39.6);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAWgMQAMgFALAAIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMIgXAFg");
        this.shape_29.setTransform(238.2, 39.6);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_30.setTransform(190.2, 75);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_31.setTransform(190.2, 75);

        this.text_14 = new cjs.Text("1", "bold 16px 'Myriad Pro'", "#0089CA");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(184, 75.6);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f("#000000").s().p("AgegWIA9AWIg9AXg");
        this.shape_34.setTransform(211.9, 74.9);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AiNAAIEbAA");
        this.shape_35.setTransform(196.1, 74.9);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gHZQgdAAgcAOQg5AdAABHIAALQIACARQAEAWAIARQAdA5BHAAMBL/AAAIASgCQAVgEASgIQA5gdAAhGIAArQQAAgdgOgcQgdg5hHAAg");
        this.shape_36.setTransform(254.6, 56.9);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFF173").s().p("Egl/AHaQhHAAgdg5QgIgRgEgWIgCgRIAArQQAAhHA5gdQAcgOAdAAMBL/AAAQBHAAAdA5QAOAcAAAdIAALQQAABGg5AdQgSAIgVAEIgSACg");
        this.shape_37.setTransform(254.6, 56.9);

        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(130, 66).lineTo(330.5, 66).moveTo(330.5, 66).lineTo(330.5, 64).lineTo(337, 66).lineTo(330.5, 68).lineTo(330.5, 66);
        var numberLineLimit = 1
        for (var dot = 0; dot < 11; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(130 + (20 * dot), 61).lineTo(130 + (20 * dot), 71);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(129, 66).lineTo(130 + (20 * numberLineLimit), 66);

        for (var dot = 0; dot < 11; dot++) {
            var temptext = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 13px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot == 10) ? 9.8 : dot;
            temptext.setTransform(127 + (20 * colSpace), 88);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);

        this.addChild(this.shape_37, this.shape_36, this.shape_33, this.shape_32, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.shape_4, this.shape_3, this.text_4, this.text_3, this.text_2, this.shape_2, this.shape_1, this.shape, this.text_1, this.text, this.numberLine);
        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 511.3, 105.3);


    (lib.Symbol17 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Talen 1 och 2", "24px 'MyriadPro-Semibold'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(94.5, 25);

        this.text_1 = new cjs.Text("3", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(54.7, 22);

        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 0 till 4", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(67.5, 651);

        this.text_4 = new cjs.Text("12", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(38, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.addChild(this.shape_2, this.shape_1, this.shape, this.text_4, this.pageBottomText, this.text_2, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("2", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol15 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("1", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol14 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("2", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol13 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("1", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol12 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("2", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("1", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol10 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("2", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol9 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("1", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol8 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("2", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);


    (lib.Symbol7 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("1", "bold 37px 'UusiTekstausMajema'", "#D74479");
        this.text.lineHeight = 44;

        this.addChild(this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 19.9, 47.7);

    (lib.Symbol21 = function() {
        this.initialize();

        // Layer 1

        this.text = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text.lineHeight = 11;
        this.text.setTransform(358.5, 139.2);

        this.text_1 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_1.lineHeight = 11;
        this.text_1.setTransform(358.5, 109.7);

        this.text_2 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_2.lineHeight = 11;
        this.text_2.setTransform(358.5, 78.9);

        this.text_3 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_3.lineHeight = 11;
        this.text_3.setTransform(358.5, 48.1);

        this.text_4 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_4.lineHeight = 11;
        this.text_4.setTransform(109.6, 139.2);

        this.text_5 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_5.lineHeight = 11;
        this.text_5.setTransform(109.6, 109.7);

        this.text_6 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_6.lineHeight = 11;
        this.text_6.setTransform(109.6, 78.9);

        this.text_7 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_7.lineHeight = 11;
        this.text_7.setTransform(109.6, 48.1);

        this.text_8 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_8.lineHeight = 11;
        this.text_8.setTransform(302.3, 139.2);

        this.text_9 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_9.lineHeight = 11;
        this.text_9.setTransform(302.3, 109.7);

        this.text_10 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_10.lineHeight = 11;
        this.text_10.setTransform(302.3, 78.9);

        this.text_11 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_11.lineHeight = 11;
        this.text_11.setTransform(302.3, 48.1);

        this.text_12 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_12.lineHeight = 11;
        this.text_12.setTransform(53.4, 139.2);

        this.text_13 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_13.lineHeight = 11;
        this.text_13.setTransform(53.4, 109.7);

        this.text_14 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_14.lineHeight = 11;
        this.text_14.setTransform(53.4, 78.9);

        this.text_15 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_15.lineHeight = 11;
        this.text_15.setTransform(53.4, 48.1);

        this.text_16 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_16.lineHeight = 11;
        this.text_16.setTransform(471.9, 139.2);

        this.text_17 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_17.lineHeight = 11;
        this.text_17.setTransform(471.9, 109.7);

        this.text_18 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_18.lineHeight = 11;
        this.text_18.setTransform(471.9, 78.9);

        this.text_19 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_19.lineHeight = 11;
        this.text_19.setTransform(471.9, 48.1);

        this.text_20 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_20.lineHeight = 11;
        this.text_20.setTransform(223, 139.2);

        this.text_21 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_21.lineHeight = 11;
        this.text_21.setTransform(223, 109.7);

        this.text_22 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_22.lineHeight = 11;
        this.text_22.setTransform(223, 78.9);

        this.text_23 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_23.lineHeight = 11;
        this.text_23.setTransform(223, 48.1);

        this.text_24 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_24.lineHeight = 11;
        this.text_24.setTransform(415.6, 139.2);

        this.text_25 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_25.lineHeight = 11;
        this.text_25.setTransform(415.6, 109.7);

        this.text_26 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_26.lineHeight = 11;
        this.text_26.setTransform(415.6, 78.9);

        this.text_27 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_27.lineHeight = 11;
        this.text_27.setTransform(415.6, 48.1);

        this.text_28 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_28.lineHeight = 11;
        this.text_28.setTransform(166.8, 139.2);

        this.text_29 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_29.lineHeight = 11;
        this.text_29.setTransform(166.8, 109.7);

        this.text_30 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_30.lineHeight = 11;
        this.text_30.setTransform(166.8, 78.9);

        this.text_31 = new cjs.Text("•", "bold 9px 'Myriad Pro'", "#A9AFB0");
        this.text_31.lineHeight = 11;
        this.text_31.setTransform(166.8, 48.1);

        this.text_32 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_32.lineHeight = 12;
        this.text_32.setTransform(467.7, 12.6);

        this.text_33 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_33.lineHeight = 12;
        this.text_33.setTransform(391.1, 12.6);

        this.text_34 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_34.lineHeight = 12;
        this.text_34.setTransform(315.6, 12.6);

        this.text_35 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_35.lineHeight = 46;
        this.text_35.setTransform(440.7, 132.8);

        this.text_36 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_36.lineHeight = 46;
        this.text_36.setTransform(191.9, 132.8);

        this.text_37 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_37.lineHeight = 46;
        this.text_37.setTransform(440.7, 103.5);

        this.text_38 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_38.lineHeight = 46;
        this.text_38.setTransform(191.9, 103.5);

        this.text_39 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_39.lineHeight = 46;
        this.text_39.setTransform(440.7, 72.4);

        this.text_40 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_40.lineHeight = 46;
        this.text_40.setTransform(191.9, 72.4);

        this.text_41 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_41.lineHeight = 46;
        this.text_41.setTransform(440.7, 42);

        this.text_42 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_42.lineHeight = 46;
        this.text_42.setTransform(191.9, 42);

        this.text_43 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_43.lineHeight = 46;
        this.text_43.setTransform(384, 132.8);

        this.text_44 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_44.lineHeight = 46;
        this.text_44.setTransform(135.2, 132.8);

        this.text_45 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_45.lineHeight = 46;
        this.text_45.setTransform(384, 103.5);

        this.text_46 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_46.lineHeight = 46;
        this.text_46.setTransform(135.2, 103.5);

        this.text_47 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_47.lineHeight = 46;
        this.text_47.setTransform(384, 72.4);

        this.text_48 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_48.lineHeight = 46;
        this.text_48.setTransform(135.2, 72.4);

        this.text_49 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_49.lineHeight = 46;
        this.text_49.setTransform(384, 42);

        this.text_50 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_50.lineHeight = 46;
        this.text_50.setTransform(135.2, 42);

        this.text_51 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_51.lineHeight = 46;
        this.text_51.setTransform(326.9, 132.8);

        this.text_52 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_52.lineHeight = 46;
        this.text_52.setTransform(78.2, 132.8);

        this.text_53 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_53.lineHeight = 46;
        this.text_53.setTransform(326.9, 103.5);

        this.text_54 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_54.lineHeight = 46;
        this.text_54.setTransform(78.2, 103.5);

        this.text_55 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_55.lineHeight = 46;
        this.text_55.setTransform(326.9, 72.4);

        this.text_56 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_56.lineHeight = 46;
        this.text_56.setTransform(78.2, 72.4);

        this.text_57 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_57.lineHeight = 46;
        this.text_57.setTransform(326.9, 42);

        this.text_58 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_58.lineHeight = 46;
        this.text_58.setTransform(78.2, 42);

        this.text_59 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_59.lineHeight = 46;
        this.text_59.setTransform(270.6, 132.8);

        this.text_60 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_60.lineHeight = 46;
        this.text_60.setTransform(21.8, 132.8);

        this.text_61 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_61.lineHeight = 46;
        this.text_61.setTransform(270.6, 103.5);

        this.text_62 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_62.lineHeight = 46;
        this.text_62.setTransform(21.8, 103.5);

        this.text_63 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_63.lineHeight = 46;
        this.text_63.setTransform(270.6, 72.4);

        this.text_64 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_64.lineHeight = 46;
        this.text_64.setTransform(21.8, 72.4);

        this.text_65 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_65.lineHeight = 46;
        this.text_65.setTransform(270.6, 42);

        this.text_66 = new cjs.Text("2", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_66.lineHeight = 46;
        this.text_66.setTransform(21.8, 42);

        this.text_67 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_67.lineHeight = 56;
        this.text_67.setTransform(214.7, 3);

        this.text_68 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_68.lineHeight = 56;
        this.text_68.setTransform(425.4, 3);

        this.text_69 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_69.lineHeight = 56;
        this.text_69.setTransform(175.9, 3);

        this.text_70 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_70.lineHeight = 56;
        this.text_70.setTransform(137.2, 3);

        this.text_71 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_71.lineHeight = 56;
        this.text_71.setTransform(349.1, 3);

        this.text_72 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_72.lineHeight = 56;
        this.text_72.setTransform(100.7, 3);

        this.text_73 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_73.lineHeight = 56;
        this.text_73.setTransform(62.5, 3);

        this.text_74 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_74.lineHeight = 56;
        this.text_74.setTransform(273.6, 3);

        this.text_75 = new cjs.Text("2", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_75.lineHeight = 56;
        this.text_75.setTransform(24.2, 3);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape.setTransform(474.9, 29.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_1.setTransform(474.9, 29.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_2.setTransform(226.2, 29.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_3.setTransform(226.2, 29.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_4.setTransform(436.8, 29.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_5.setTransform(436.8, 29.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_6.setTransform(188.2, 29.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_7.setTransform(188.2, 29.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_8.setTransform(397.8, 29.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_9.setTransform(397.8, 29.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_10.setTransform(149.2, 29.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_11.setTransform(149.2, 29.9);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_12.setTransform(360.8, 29.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_13.setTransform(360.8, 29.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiJCLIEUAAIAAkVIkUAAg");
        this.shape_14.setTransform(112.2, 29.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEUAAIAAEVg");
        this.shape_15.setTransform(112.2, 29.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_16.setTransform(322.8, 29.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_17.setTransform(322.8, 29.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_18.setTransform(74.2, 29.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_19.setTransform(74.2, 29.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_20.setTransform(479.1, 154.2);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_21.setTransform(479.1, 154.2);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_22.setTransform(230.3, 154.2);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_23.setTransform(230.3, 154.2);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_24.setTransform(450.8, 154.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_25.setTransform(450.8, 154.2);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_26.setTransform(202, 154.2);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_27.setTransform(202, 154.2);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_28.setTransform(422.5, 154.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_29.setTransform(422.5, 154.2);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_30.setTransform(173.7, 154.2);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_31.setTransform(173.7, 154.2);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_32.setTransform(394.1, 154.2);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_33.setTransform(394.1, 154.2);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_34.setTransform(145.3, 154.2);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_35.setTransform(145.3, 154.2);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_36.setTransform(365.8, 154.2);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_37.setTransform(365.8, 154.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_38.setTransform(117, 154.2);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_39.setTransform(117, 154.2);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_40.setTransform(337.4, 154.2);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_41.setTransform(337.4, 154.2);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_42.setTransform(88.6, 154.2);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_43.setTransform(88.6, 154.2);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_44.setTransform(309.1, 154.2);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_45.setTransform(309.1, 154.2);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_46.setTransform(60.3, 154.2);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_47.setTransform(60.3, 154.2);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_48.setTransform(280.7, 154.2);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_49.setTransform(280.7, 154.2);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_50.setTransform(31.9, 154.2);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_51.setTransform(31.9, 154.2);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_52.setTransform(479.1, 124.5);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_53.setTransform(479.1, 124.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_54.setTransform(230.3, 124.5);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_55.setTransform(230.3, 124.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_56.setTransform(450.8, 124.5);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_57.setTransform(450.8, 124.5);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_58.setTransform(202, 124.5);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_59.setTransform(202, 124.5);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_60.setTransform(422.5, 124.5);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_61.setTransform(422.5, 124.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_62.setTransform(173.7, 124.5);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_63.setTransform(173.7, 124.5);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_64.setTransform(394.1, 124.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_65.setTransform(394.1, 124.5);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_66.setTransform(145.3, 124.5);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_67.setTransform(145.3, 124.5);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_68.setTransform(365.8, 124.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_69.setTransform(365.8, 124.5);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_70.setTransform(117, 124.5);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_71.setTransform(117, 124.5);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_72.setTransform(337.4, 124.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_73.setTransform(337.4, 124.5);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_74.setTransform(88.6, 124.5);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_75.setTransform(88.6, 124.5);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_76.setTransform(309.1, 124.5);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_77.setTransform(309.1, 124.5);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_78.setTransform(60.3, 124.5);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_79.setTransform(60.3, 124.5);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_80.setTransform(280.7, 124.5);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_81.setTransform(280.7, 124.5);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_82.setTransform(31.9, 124.5);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_83.setTransform(31.9, 124.5);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_84.setTransform(479.1, 93.8);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_85.setTransform(479.1, 93.8);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_86.setTransform(230.3, 93.8);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_87.setTransform(230.3, 93.8);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_88.setTransform(450.8, 93.8);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_89.setTransform(450.8, 93.8);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_90.setTransform(202, 93.8);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_91.setTransform(202, 93.8);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_92.setTransform(422.5, 93.8);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_93.setTransform(422.5, 93.8);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_94.setTransform(173.7, 93.8);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_95.setTransform(173.7, 93.8);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_96.setTransform(394.1, 93.8);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_97.setTransform(394.1, 93.8);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_98.setTransform(145.3, 93.8);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_99.setTransform(145.3, 93.8);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_100.setTransform(365.8, 93.8);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_101.setTransform(365.8, 93.8);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_102.setTransform(117, 93.8);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_103.setTransform(117, 93.8);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_104.setTransform(337.4, 93.8);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_105.setTransform(337.4, 93.8);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_106.setTransform(88.6, 93.8);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_107.setTransform(88.6, 93.8);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_108.setTransform(309.1, 93.8);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_109.setTransform(309.1, 93.8);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_110.setTransform(60.3, 93.8);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_111.setTransform(60.3, 93.8);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_112.setTransform(280.7, 93.8);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_113.setTransform(280.7, 93.8);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_114.setTransform(31.9, 93.8);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_115.setTransform(31.9, 93.8);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_116.setTransform(479.1, 63.2);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_117.setTransform(479.1, 63.2);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_118.setTransform(230.3, 63.2);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_119.setTransform(230.3, 63.2);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_120.setTransform(450.8, 63.2);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_121.setTransform(450.8, 63.2);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_122.setTransform(202, 63.2);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_123.setTransform(202, 63.2);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_124.setTransform(422.5, 63.2);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_125.setTransform(422.5, 63.2);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_126.setTransform(173.7, 63.2);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_127.setTransform(173.7, 63.2);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_128.setTransform(394.1, 63.2);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_129.setTransform(394.1, 63.2);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_130.setTransform(145.3, 63.2);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_131.setTransform(145.3, 63.2);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_132.setTransform(365.8, 63.2);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_133.setTransform(365.8, 63.2);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_134.setTransform(117, 63.2);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_135.setTransform(117, 63.2);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_136.setTransform(337.4, 63.2);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_137.setTransform(337.4, 63.2);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_138.setTransform(88.6, 63.2);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_139.setTransform(88.6, 63.2);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_140.setTransform(309.1, 63.2);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_141.setTransform(309.1, 63.2);

        this.shape_142 = new cjs.Shape();
        this.shape_142.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_142.setTransform(60.3, 63.2);

        this.shape_143 = new cjs.Shape();
        this.shape_143.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_143.setTransform(60.3, 63.2);

        this.shape_144 = new cjs.Shape();
        this.shape_144.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_144.setTransform(280.7, 63.2);

        this.shape_145 = new cjs.Shape();
        this.shape_145.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_145.setTransform(280.7, 63.2);

        this.shape_146 = new cjs.Shape();
        this.shape_146.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_146.setTransform(31.9, 63.2);

        this.shape_147 = new cjs.Shape();
        this.shape_147.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_147.setTransform(31.9, 63.2);

        this.shape_148 = new cjs.Shape();
        this.shape_148.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_148.setTransform(284.8, 29.9);

        this.shape_149 = new cjs.Shape();
        this.shape_149.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_149.setTransform(284.8, 29.9);

        this.shape_150 = new cjs.Shape();
        this.shape_150.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_150.setTransform(36.2, 29.9);

        this.shape_151 = new cjs.Shape();
        this.shape_151.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_151.setTransform(36.2, 29.9);

        this.shape_152 = new cjs.Shape();
        this.shape_152.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgNNQgHAAgLACQgVADgSAJQg5AcAABHIAAW5IACARQAEAWAIASQAdA4BHAAMBMJAAAQAdAAAcgOQA5gcAAhHIAA25QAAgcgOgdQgdg4hHAAg");
        this.shape_152.setTransform(255.1, 89.5);

        this.shape_153 = new cjs.Shape();
        this.shape_153.graphics.f("#FFFFFF").s().p("EgmEANOQhHAAgdg4QgIgSgEgWIgCgRIAA25QAAhHA5gcQASgJAVgDQALgCAHAAMBMJAAAQBHAAAdA4QAOAdAAAcIAAW5QAABHg5AcQgcAOgdAAg");
        this.shape_153.setTransform(255.1, 89.5);

        this.addChild(this.shape_153, this.shape_152, this.shape_151, this.shape_150, this.shape_149, this.shape_148, this.shape_147, this.shape_146, this.shape_145, this.shape_144, this.shape_143, this.shape_142, this.shape_141, this.shape_140, this.shape_139, this.shape_138, this.shape_137, this.shape_136, this.shape_135, this.shape_134, this.shape_133, this.shape_132, this.shape_131, this.shape_130, this.shape_129, this.shape_128, this.shape_127, this.shape_126, this.shape_125, this.shape_124, this.shape_123, this.shape_122, this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_75, this.text_74, this.text_73, this.text_72, this.text_71, this.text_70, this.text_69, this.text_68, this.text_67, this.text_66, this.text_65, this.text_64, this.text_63, this.text_62, this.text_61, this.text_60, this.text_59, this.text_58, this.text_57, this.text_56, this.text_55, this.text_54, this.text_53, this.text_52, this.text_51, this.text_50, this.text_49, this.text_48, this.text_47, this.text_46, this.text_45, this.text_44, this.text_43, this.text_42, this.text_41, this.text_40, this.text_39, this.text_38, this.text_37, this.text_36, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.ra1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 512.3, 178.5);


    (lib.Symbol19 = function() {
        this.initialize();

        // Layer 1

        this.text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text.lineHeight = 12;
        this.text.setTransform(476.7, 138.6);

        this.text_1 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(476.7, 109.3);

        this.text_2 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(476.7, 78.7);

        this.text_3 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(476.7, 47.5);

        this.text_4 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(227.8, 138.6);

        this.text_5 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(227.8, 109.3);

        this.text_6 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(227.8, 78.7);

        this.text_7 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(227.8, 47.5);

        this.text_8 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(419.9, 138.6);

        this.text_9 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(419.9, 109.3);

        this.text_10 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(419.9, 78.7);

        this.text_11 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(419.9, 47.5);

        this.text_12 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(171, 138.6);

        this.text_13 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(171, 109.3);

        this.text_14 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_14.lineHeight = 12;
        this.text_14.setTransform(171, 78.7);

        this.text_15 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_15.lineHeight = 12;
        this.text_15.setTransform(171, 47.5);

        this.text_16 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_16.lineHeight = 12;
        this.text_16.setTransform(363.3, 138.6);

        this.text_17 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_17.lineHeight = 12;
        this.text_17.setTransform(363.3, 109.3);

        this.text_18 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_18.lineHeight = 12;
        this.text_18.setTransform(363.3, 78.7);

        this.text_19 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_19.lineHeight = 12;
        this.text_19.setTransform(363.3, 47.5);

        this.text_20 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_20.lineHeight = 12;
        this.text_20.setTransform(114.4, 138.6);

        this.text_21 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_21.lineHeight = 12;
        this.text_21.setTransform(114.4, 109.3);

        this.text_22 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_22.lineHeight = 12;
        this.text_22.setTransform(114.4, 78.7);

        this.text_23 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_23.lineHeight = 12;
        this.text_23.setTransform(114.4, 47.5);

        this.text_24 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_24.lineHeight = 12;
        this.text_24.setTransform(307.1, 138.6);

        this.text_25 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_25.lineHeight = 12;
        this.text_25.setTransform(307.1, 109.3);

        this.text_26 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_26.lineHeight = 12;
        this.text_26.setTransform(307.1, 78.7);

        this.text_27 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_27.lineHeight = 12;
        this.text_27.setTransform(307.1, 47.5);

        this.text_28 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_28.lineHeight = 12;
        this.text_28.setTransform(58.2, 138.6);

        this.text_29 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_29.lineHeight = 12;
        this.text_29.setTransform(58.2, 109.3);

        this.text_30 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_30.lineHeight = 12;
        this.text_30.setTransform(58.2, 78.7);

        this.text_31 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_31.lineHeight = 12;
        this.text_31.setTransform(58.2, 47.5);

        this.text_32 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_32.lineHeight = 12;
        this.text_32.setTransform(473.9, 12.2);

        this.text_33 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_33.lineHeight = 12;
        this.text_33.setTransform(396.3, 12.2);

        this.text_34 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_34.lineHeight = 12;
        this.text_34.setTransform(321.2, 12.2);

        this.text_35 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_35.lineHeight = 55;
        this.text_35.setTransform(425.3, 3);

        this.text_36 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_36.lineHeight = 55;
        this.text_36.setTransform(349.3, 3);

        this.text_37 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_37.lineHeight = 55;
        this.text_37.setTransform(272.7, 3);

        this.text_38 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_38.lineHeight = 55;
        this.text_38.setTransform(214.8, 3);

        this.text_39 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_39.lineHeight = 55;
        this.text_39.setTransform(177.3, 3);

        this.text_40 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_40.lineHeight = 55;
        this.text_40.setTransform(138.4, 3);

        this.text_41 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_41.lineHeight = 55;
        this.text_41.setTransform(100.8, 3);

        this.text_42 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_42.lineHeight = 55;
        this.text_42.setTransform(62.5, 3);

        this.text_43 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_43.lineHeight = 47;
        this.text_43.setTransform(440.4, 133.1);

        this.text_44 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_44.lineHeight = 47;
        this.text_44.setTransform(440.4, 103.6);

        this.text_45 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_45.lineHeight = 47;
        this.text_45.setTransform(440.4, 72.6);

        this.text_46 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_46.lineHeight = 47;
        this.text_46.setTransform(440.4, 42.2);

        this.text_47 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_47.lineHeight = 47;
        this.text_47.setTransform(191.8, 133);

        this.text_48 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_48.lineHeight = 47;
        this.text_48.setTransform(191.8, 103.6);

        this.text_49 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_49.lineHeight = 47;
        this.text_49.setTransform(191.8, 72.6);

        this.text_50 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_50.lineHeight = 47;
        this.text_50.setTransform(191.8, 42.2);

        this.text_51 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_51.lineHeight = 47;
        this.text_51.setTransform(383.7, 133.1);

        this.text_52 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_52.lineHeight = 47;
        this.text_52.setTransform(383.7, 103.6);

        this.text_53 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_53.lineHeight = 47;
        this.text_53.setTransform(383.7, 72.6);

        this.text_54 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_54.lineHeight = 47;
        this.text_54.setTransform(383.7, 42.2);

        this.text_55 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_55.lineHeight = 47;
        this.text_55.setTransform(135.1, 133);

        this.text_56 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_56.lineHeight = 47;
        this.text_56.setTransform(135.1, 103.6);

        this.text_57 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_57.lineHeight = 47;
        this.text_57.setTransform(135.1, 72.6);

        this.text_58 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_58.lineHeight = 47;
        this.text_58.setTransform(135.1, 42.2);

        this.text_59 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_59.lineHeight = 47;
        this.text_59.setTransform(326.6, 133.1);

        this.text_60 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_60.lineHeight = 47;
        this.text_60.setTransform(326.6, 103.6);

        this.text_61 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_61.lineHeight = 47;
        this.text_61.setTransform(326.6, 72.6);

        this.text_62 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_62.lineHeight = 47;
        this.text_62.setTransform(326.6, 42.2);

        this.text_63 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_63.lineHeight = 47;
        this.text_63.setTransform(78.1, 133);

        this.text_64 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_64.lineHeight = 47;
        this.text_64.setTransform(78.1, 103.6);

        this.text_65 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_65.lineHeight = 47;
        this.text_65.setTransform(78.1, 72.6);

        this.text_66 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_66.lineHeight = 47;
        this.text_66.setTransform(78.1, 42.2);

        this.text_67 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_67.lineHeight = 47;
        this.text_67.setTransform(270.3, 133.1);

        this.text_68 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_68.lineHeight = 47;
        this.text_68.setTransform(270.3, 103.6);

        this.text_69 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_69.lineHeight = 47;
        this.text_69.setTransform(270.3, 72.6);

        this.text_70 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_70.lineHeight = 47;
        this.text_70.setTransform(270.3, 42.2);

        this.text_71 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_71.lineHeight = 47;
        this.text_71.setTransform(21.7, 133);

        this.text_72 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_72.lineHeight = 47;
        this.text_72.setTransform(21.7, 103.6);

        this.text_73 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_73.lineHeight = 47;
        this.text_73.setTransform(21.7, 72.6);

        this.text_74 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_74.lineHeight = 47;
        this.text_74.setTransform(21.7, 42.2);

        this.text_75 = new cjs.Text("1", "bold 46px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_75.lineHeight = 55;
        this.text_75.setTransform(24.3, 3);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape.setTransform(474.9, 29.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_1.setTransform(474.9, 29.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_2.setTransform(226.2, 29.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_3.setTransform(226.2, 29.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_4.setTransform(436.8, 29.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_5.setTransform(436.8, 29.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_6.setTransform(188.2, 29.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_7.setTransform(188.2, 29.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_8.setTransform(397.8, 29.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_9.setTransform(397.8, 29.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_10.setTransform(149.2, 29.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_11.setTransform(149.2, 29.9);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_12.setTransform(360.8, 29.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_13.setTransform(360.8, 29.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiJCLIEUAAIAAkVIkUAAg");
        this.shape_14.setTransform(112.2, 29.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEUAAIAAEVg");
        this.shape_15.setTransform(112.2, 29.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_16.setTransform(322.8, 29.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_17.setTransform(322.8, 29.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_18.setTransform(74.2, 29.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_19.setTransform(74.2, 29.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_20.setTransform(479, 154.2);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_21.setTransform(479, 154.2);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_22.setTransform(230.3, 154.2);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_23.setTransform(230.3, 154.2);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_24.setTransform(450.6, 154.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_25.setTransform(450.6, 154.2);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_26.setTransform(202, 154.2);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_27.setTransform(202, 154.2);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_28.setTransform(422.3, 154.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_29.setTransform(422.3, 154.2);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_30.setTransform(173.7, 154.2);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_31.setTransform(173.7, 154.2);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_32.setTransform(393.9, 154.2);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_33.setTransform(393.9, 154.2);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_34.setTransform(145.3, 154.2);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_35.setTransform(145.3, 154.2);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_36.setTransform(365.6, 154.2);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_37.setTransform(365.6, 154.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_38.setTransform(117, 154.2);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_39.setTransform(117, 154.2);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_40.setTransform(337.2, 154.2);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_41.setTransform(337.2, 154.2);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_42.setTransform(88.6, 154.2);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_43.setTransform(88.6, 154.2);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_44.setTransform(308.9, 154.2);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_45.setTransform(308.9, 154.2);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_46.setTransform(60.3, 154.2);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_47.setTransform(60.3, 154.2);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_48.setTransform(280.5, 154.2);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_49.setTransform(280.5, 154.2);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_50.setTransform(31.9, 154.2);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_51.setTransform(31.9, 154.2);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_52.setTransform(479, 124.5);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_53.setTransform(479, 124.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_54.setTransform(230.3, 124.5);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_55.setTransform(230.3, 124.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_56.setTransform(450.6, 124.5);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_57.setTransform(450.6, 124.5);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_58.setTransform(202, 124.5);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_59.setTransform(202, 124.5);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_60.setTransform(422.3, 124.5);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_61.setTransform(422.3, 124.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_62.setTransform(173.7, 124.5);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_63.setTransform(173.7, 124.5);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_64.setTransform(393.9, 124.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_65.setTransform(393.9, 124.5);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_66.setTransform(145.3, 124.5);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_67.setTransform(145.3, 124.5);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_68.setTransform(365.6, 124.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_69.setTransform(365.6, 124.5);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_70.setTransform(117, 124.5);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_71.setTransform(117, 124.5);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_72.setTransform(337.2, 124.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_73.setTransform(337.2, 124.5);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_74.setTransform(88.6, 124.5);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_75.setTransform(88.6, 124.5);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_76.setTransform(308.9, 124.5);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_77.setTransform(308.9, 124.5);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_78.setTransform(60.3, 124.5);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_79.setTransform(60.3, 124.5);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_80.setTransform(280.5, 124.5);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_81.setTransform(280.5, 124.5);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_82.setTransform(31.9, 124.5);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_83.setTransform(31.9, 124.5);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_84.setTransform(479, 93.9);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_85.setTransform(479, 93.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_86.setTransform(230.3, 93.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_87.setTransform(230.3, 93.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_88.setTransform(450.6, 93.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_89.setTransform(450.6, 93.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_90.setTransform(202, 93.9);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_91.setTransform(202, 93.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_92.setTransform(422.3, 93.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_93.setTransform(422.3, 93.9);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_94.setTransform(173.7, 93.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_95.setTransform(173.7, 93.9);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_96.setTransform(393.9, 93.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_97.setTransform(393.9, 93.9);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_98.setTransform(145.3, 93.9);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_99.setTransform(145.3, 93.9);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_100.setTransform(365.6, 93.9);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_101.setTransform(365.6, 93.9);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_102.setTransform(117, 93.9);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_103.setTransform(117, 93.9);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_104.setTransform(337.2, 93.9);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_105.setTransform(337.2, 93.9);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_106.setTransform(88.6, 93.9);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_107.setTransform(88.6, 93.9);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_108.setTransform(308.9, 93.9);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_109.setTransform(308.9, 93.9);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_110.setTransform(60.3, 93.9);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_111.setTransform(60.3, 93.9);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_112.setTransform(280.5, 93.9);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_113.setTransform(280.5, 93.9);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_114.setTransform(31.9, 93.9);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_115.setTransform(31.9, 93.9);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_116.setTransform(479, 63.2);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_117.setTransform(479, 63.2);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_118.setTransform(230.3, 63.2);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_119.setTransform(230.3, 63.2);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_120.setTransform(450.6, 63.2);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_121.setTransform(450.6, 63.2);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_122.setTransform(202, 63.2);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_123.setTransform(202, 63.2);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_124.setTransform(422.3, 63.2);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_125.setTransform(422.3, 63.2);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_126.setTransform(173.7, 63.2);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_127.setTransform(173.7, 63.2);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_128.setTransform(393.9, 63.2);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_129.setTransform(393.9, 63.2);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_130.setTransform(145.3, 63.2);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_131.setTransform(145.3, 63.2);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_132.setTransform(365.6, 63.2);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_133.setTransform(365.6, 63.2);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_134.setTransform(117, 63.2);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_135.setTransform(117, 63.2);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_136.setTransform(337.2, 63.2);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_137.setTransform(337.2, 63.2);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_138.setTransform(88.6, 63.2);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_139.setTransform(88.6, 63.2);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_140.setTransform(308.9, 63.2);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_141.setTransform(308.9, 63.2);

        this.shape_142 = new cjs.Shape();
        this.shape_142.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_142.setTransform(60.3, 63.2);

        this.shape_143 = new cjs.Shape();
        this.shape_143.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_143.setTransform(60.3, 63.2);

        this.shape_144 = new cjs.Shape();
        this.shape_144.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_144.setTransform(280.5, 63.2);

        this.shape_145 = new cjs.Shape();
        this.shape_145.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_145.setTransform(280.5, 63.2);

        this.shape_146 = new cjs.Shape();
        this.shape_146.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_146.setTransform(31.9, 63.2);

        this.shape_147 = new cjs.Shape();
        this.shape_147.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_147.setTransform(31.9, 63.2);

        this.shape_148 = new cjs.Shape();
        this.shape_148.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_148.setTransform(284.8, 29.9);

        this.shape_149 = new cjs.Shape();
        this.shape_149.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_149.setTransform(284.8, 29.9);

        this.shape_150 = new cjs.Shape();
        this.shape_150.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_150.setTransform(36.2, 29.9);

        this.shape_151 = new cjs.Shape();
        this.shape_151.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_151.setTransform(36.2, 29.9);

        this.shape_152 = new cjs.Shape();
        this.shape_152.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgNNQgHAAgLACQgVADgSAJQg5AcAABHIAAW5IACARQAEAWAIASQAdA4BHAAMBMJAAAIASgCQAVgDASgJQA5gcAAhHIAA25QAAgcgOgdQgdg4hHAAg");
        this.shape_152.setTransform(255.1, 89.6);

        this.shape_153 = new cjs.Shape();
        this.shape_153.graphics.f("#FFFFFF").s().p("EgmEANOQhHAAgdg4QgIgSgEgWIgCgRIAA25QAAhHA5gcQASgJAVgDQALgCAHAAMBMJAAAQBHAAAdA4QAOAdAAAcIAAW5QAABHg5AcQgSAJgVADIgSACg");
        this.shape_153.setTransform(255.1, 89.6);

        this.addChild(this.shape_153, this.shape_152, this.shape_151, this.shape_150, this.shape_149, this.shape_148, this.shape_147, this.shape_146, this.shape_145, this.shape_144, this.shape_143, this.shape_142, this.shape_141, this.shape_140, this.shape_139, this.shape_138, this.shape_137, this.shape_136, this.shape_135, this.shape_134, this.shape_133, this.shape_132, this.shape_131, this.shape_130, this.shape_129, this.shape_128, this.shape_127, this.shape_126, this.shape_125, this.shape_124, this.shape_123, this.shape_122, this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_75, this.text_74, this.text_73, this.text_72, this.text_71, this.text_70, this.text_69, this.text_68, this.text_67, this.text_66, this.text_65, this.text_64, this.text_63, this.text_62, this.text_61, this.text_60, this.text_59, this.text_58, this.text_57, this.text_56, this.text_55, this.text_54, this.text_53, this.text_52, this.text_51, this.text_50, this.text_49, this.text_48, this.text_47, this.text_46, this.text_45, this.text_44, this.text_43, this.text_42, this.text_41, this.text_40, this.text_39, this.text_38, this.text_37, this.text_36, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.ra1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 512.3, 178.9);


    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgThQgcAAgcAOQg5AcAABHMAAAAjhIACARQADAWAJASQAcA4BHAAMBMFAAAIARgCQAWgDASgJQA4gcAAhHMAAAgjhQAAgcgOgcQgcg5hHAAg");
        this.shape.setTransform(257.4, 156.7);

        this.instance = new lib.s12_13_3();
        this.instance.setTransform(2.7, 31.7, 0.5, 0.5);

        this.instance_1 = new lib.s12_13_1();
        this.instance_1.setTransform(47.6, 207.2, 0.667, 0.667);

        this.ra5 = new lib.Symbol16();
        this.ra5.setTransform(418.3, 263.1, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra4 = new lib.Symbol15();
        this.ra4.setTransform(333.3, 263.1, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra3 = new lib.Symbol14();
        this.ra3.setTransform(249, 263.1, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra2 = new lib.Symbol13();
        this.ra2.setTransform(164, 263.1, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra1 = new lib.Symbol12();
        this.ra1.setTransform(78.2, 263.1, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.text = new cjs.Text("Kuin-ka mon-ta ku-vas-sa on?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1.6);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_1.setTransform(418.9, 263.7);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_2.setTransform(418.9, 263.7);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_3.setTransform(333.8, 263.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_4.setTransform(333.8, 263.7);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_5.setTransform(248.8, 263.7);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_6.setTransform(248.8, 263.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_7.setTransform(163.7, 263.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_8.setTransform(163.7, 263.7);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_9.setTransform(78.7, 263.7);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AhfBuIAAjcIC/AAIAADcg");
        this.shape_10.setTransform(78.7, 263.7);

        this.addChild(this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.instance_1, this.instance, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 286.9);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgTwQgcAAgcAOQg5AdAABHMAAAAj+IACASQADAVAJASQAcA4BHAAMBMFAAAIARgCQAWgDASgJQA4gcAAhHMAAAgj+QAAgdgOgcQgcg5hHAAg");
        this.shape.setTransform(257.2, 159.3);

        this.instance = new lib.s12_13_4();
        this.instance.setTransform(2.4, 33.1, 0.5, 0.5);

        this.instance_1 = new lib.s12_13_2();
        this.instance_1.setTransform(47.6, 209.1, 0.667, 0.667);

        this.ra5 = new lib.Symbol11();
        this.ra5.setTransform(418.3, 265.5, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra4 = new lib.Symbol10();
        this.ra4.setTransform(333.3, 265.5, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra3 = new lib.Symbol9();
        this.ra3.setTransform(249, 265.5, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra2 = new lib.Symbol8();
        this.ra2.setTransform(164, 265.5, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.ra1 = new lib.Symbol7();
        this.ra1.setTransform(78.2, 265.5, 1, 1, 0, 0, 0, 9.9, 23.9);

        this.text = new cjs.Text("Kuin-ka mon-ta ku-vas-sa on?", "16px 'Tavutus'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1.6);

        this.text_1 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_1.setTransform(418.9, 265.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_2.setTransform(418.9, 265.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_3.setTransform(333.8, 265.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_4.setTransform(333.8, 265.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_5.setTransform(248.8, 265.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_6.setTransform(248.8, 265.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_7.setTransform(163.7, 265.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_8.setTransform(163.7, 265.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_9.setTransform(78.7, 265.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_10.setTransform(78.7, 265.9);

        this.addChild(this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.instance_1, this.instance, this.shape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.1, 289.3);


    // stage content:
    (lib.p12 = function() {
        this.initialize();

        // Layer 1
        this.v4 = new lib.Symbol21();
        this.v4.setTransform(297.6, 550.2, 1, 1, 0, 0, 0, 255.1, 89.2);

        this.v3 = new lib.Symbol20();
        this.v3.setTransform(297.9, 400, 1, 1, 0, 0, 0, 254.6, 52.1);

        this.v2 = new lib.Symbol19();
        this.v2.setTransform(297.6, 250.2, 1, 1, 0, 0, 0, 255.1, 89.5);

        this.v1 = new lib.Symbol18();
        this.v1.setTransform(297.9, 100.8, 1, 1, 0, 0, 0, 254.6, 52.5);

        this.other = new lib.Symbol17();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
