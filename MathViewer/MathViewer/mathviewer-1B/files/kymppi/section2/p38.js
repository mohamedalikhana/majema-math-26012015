(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p38_1.png",
            id: "p38_1"
        }]
    };

    (lib.p38_1 = function() {
        this.initialize(img.p38_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("38", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(42, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(457 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p38_1();
        this.instance.setTransform(25, 28, 0.46, 0.46);

        this.innerRountrect = new cjs.Shape();
        this.innerRountrect.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(25, 20, 102, 46, 6);
        this.innerRountrect.setTransform(0, 10.5);

        this.innerRountrect1 = new cjs.Shape();
        this.innerRountrect1.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(434, 21, 54, 45, 6);
        this.innerRountrect1.setTransform(0, 10.5);

        this.innerRountrect2 = new cjs.Shape();
        this.innerRountrect2.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(397, 252, 102, 68, 6);
        this.innerRountrect2.setTransform(0, 10.5);

        this.innerRountrect5 = new cjs.Shape();
        this.innerRountrect5.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(157, 21, 75, 45, 6);
        this.innerRountrect5.setTransform(0, 10.5);

        this.innerRountrect10 = this.innerRountrect5.clone(true);
        this.innerRountrect10.setTransform(0, 127);

        this.innerRountrect3 = this.innerRountrect.clone(true);
        this.innerRountrect3.setTransform(0, 127);

        this.innerRountrect4 = this.innerRountrect.clone(true);
        this.innerRountrect4.setTransform(0, 252);

        this.innerRountrect6 = this.innerRountrect.clone(true);
        this.innerRountrect6.setTransform(117, 253);

        this.innerRountrect7 = this.innerRountrect.clone(true);
        this.innerRountrect7.setTransform(266, 10.5);

        this.innerRountrect8 = this.innerRountrect.clone(true);
        this.innerRountrect8.setTransform(254, 127);

        this.innerRountrect9 = this.innerRountrect.clone(true);
        this.innerRountrect9.setTransform(253, 252);

        this.innerRountrect11 = this.innerRountrect.clone(true);
        this.innerRountrect11.setTransform(373, 127);


        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(5, 0);

        this.text_1 = new cjs.Text("Hur många hjärtan är det tillsammans?", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 253, 115, 10);
        this.roundRect1.setTransform(5, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(263, 15);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(5, 135.5);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(263, 135.5);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(5, 257);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(263, 257);



        var arrTxtbox = ['96', '217', '338'];
        this.textbox_group1 = new cjs.Shape();
        var xPos = 10,
            padding,
            yPos,
            rectWidth = 120,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 6,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 52.5;
                if (i == 1) {
                    padding = 97.5;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.6).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_2 = new cjs.Text("8", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.setTransform(63, 118.2);

        this.text_3 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(83.5, 116.2);

        this.text_4 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.setTransform(103, 118);



        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.text_1, this.text, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4);
        this.addChild(this.innerRountrect, this.innerRountrect1, this.innerRountrect2, this.innerRountrect3, this.innerRountrect4, this.innerRountrect5, this.innerRountrect6, this.innerRountrect7, this.innerRountrect8, this.innerRountrect9, this.innerRountrect10, this.innerRountrect11, this.instance)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 497.3, 380);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 12, 510, 154, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 1) {
                    columnSpace = 1.35;
                } else if (column == 2) {
                    columnSpace = 2.12;
                }

                this.textbox_group1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(151 + (160 * columnSpace), 70 + (28 * row)).lineTo(196 + (160 * columnSpace), 70 + (28 * row));
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("2  +  7  +  4  =", "16px 'Myriad Pro'");
        this.label1.setTransform(60, 60);
        this.label2 = new cjs.Text("3  +  6  +  7  =", "16px 'Myriad Pro'");
        this.label2.setTransform(60, 90);
        this.label3 = new cjs.Text("4  +  5  +  9  =", "16px 'Myriad Pro'");
        this.label3.setTransform(60, 120);
        this.label4 = new cjs.Text("7  +  2  +  6  =", "16px 'Myriad Pro'");
        this.label4.setTransform(60, 150);
        this.label6 = new cjs.Text("5  +  3  +  3  =", "16px 'Myriad Pro'");
        this.label6.setTransform(275, 60);
        this.label7 = new cjs.Text("7  +  1  +  5  =", "16px 'Myriad Pro'");
        this.label7.setTransform(275, 90);
        this.label8 = new cjs.Text("6  +  2  +  7  =", "16px 'Myriad Pro'");
        this.label8.setTransform(275, 120);
        this.label9 = new cjs.Text("4  +  4  +  6  =", "16px 'Myriad Pro'");
        this.label9.setTransform(275, 150);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(30, 0).lineTo(475, 0);
        this.Line_1.setTransform(10, 30);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 55;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 65;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 75;
                } else {
                    fillcolor = "#008BD2";
                    xpos = 85;
                }

                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(132, 33);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_3.setTransform(238, 33);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#ffffff");
        this.text_4.setTransform(349, 33);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#ffffff");
        this.text_5.setTransform(458.5, 33);




        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9);
        this.addChild(this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(298, 284, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(298, 469, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
