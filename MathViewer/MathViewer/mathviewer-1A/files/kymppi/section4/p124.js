(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p124_1.png",
            id: "p124_1"
        }, {
            src: "images/p124_2.png",
            id: "p124_2"
        }]
    };

    (lib.p124_1 = function() {
        this.initialize(img.p124_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p124_2 = function() {
        this.initialize(img.p124_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("43", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(57, 22);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8490C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(52, 23.5);

        this.text_2 = new cjs.Text("Ett tal saknas", "24px 'MyriadPro-Semibold'", "#8490C8");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(103, 26);

        this.pageBottomText = new cjs.Text("kunna räkna med utelämnade tal i subtraktion", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(79, 649.5);

        this.text = new cjs.Text("124", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(44, 647);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(39.5, 660.8, 1, 1.1);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Hur många kakor har ätits upp?", "16px 'Myriad Pro'");
        this.text.lineHeight = 40;
        this.text.setTransform(15, 8);

        this.text_1 = new cjs.Text("6", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 35;
        this.text_1.setTransform(78, 79);
        this.text_2 = new cjs.Text("–", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 35;
        this.text_2.setTransform(97.2, 79);
        this.text_4 = new cjs.Text("=", "bold 33px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 35;
        this.text_4.setTransform(138.5, 81);
        this.text_3 = new cjs.Text("2", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 35;
        this.text_3.setTransform(157.5, 79);

        this.text_5 = new cjs.Text("6", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 35;
        this.text_5.setTransform(78, 108);
        this.text_6 = new cjs.Text("–", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 35;
        this.text_6.setTransform(97.2, 108);
        this.text_7 = new cjs.Text("4", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 35;
        this.text_7.setTransform(118, 108);
        this.text_8 = new cjs.Text("=", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 35;
        this.text_8.setTransform(138.5, 108);
        this.text_9 = new cjs.Text("2", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 35;
        this.text_9.setTransform(157.5, 108);

        this.text_10 = new cjs.Text("7", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 35;
        this.text_10.setTransform(320, 79);
        this.text_11 = new cjs.Text("–", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_11.lineHeight = 35;
        this.text_11.setTransform(339.2, 79);
        this.text_12 = new cjs.Text("=", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_12.lineHeight = 35;
        this.text_12.setTransform(380, 80);
        this.text_13 = new cjs.Text("4", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_13.lineHeight = 35;
        this.text_13.setTransform(399, 79);

        this.text_14 = new cjs.Text("7", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_14.lineHeight = 35;
        this.text_14.setTransform(320, 107);
        this.text_15 = new cjs.Text("–", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_15.lineHeight = 35;
        this.text_15.setTransform(339.2, 107);
        this.text_16 = new cjs.Text("3", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_16.lineHeight = 35;
        this.text_16.setTransform(359, 107);
        this.text_17 = new cjs.Text("=", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_17.lineHeight = 35;
        this.text_17.setTransform(380, 106);
        this.text_18 = new cjs.Text("4", "bold 34px 'UusiTekstausMajema'", "#6C7373");
        this.text_18.lineHeight = 35;
        this.text_18.setTransform(399, 107);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            this.textbox_group1.graphics.f('#BDBEC1').s("#BDBEC1").ss(0.9).drawRect(118.5 + (columnSpace * 240), 88.5, 19.5, 22);
        }
        this.textbox_group1.setTransform(0, 0);

        // Block-1
        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(168.9, 36.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_37.setTransform(154.1, 36.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(138.9, 36.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_39.setTransform(123.3, 36.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(107.9, 36.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(92.2, 36.4);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(76.2, 36.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(60.7, 36.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(45.2, 36.3);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(29.6, 36.3);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_47.setTransform(170.2, 42.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_48.setTransform(170.2, 42.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_49.setTransform(155.5, 42.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_50.setTransform(155.5, 42.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_51.setTransform(77.5, 42.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_52.setTransform(77.5, 42.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_53.setTransform(140.4, 42.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_54.setTransform(140.4, 42.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_55.setTransform(62.3, 42.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_56.setTransform(62.3, 42.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_57.setTransform(124.7, 42.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_58.setTransform(124.7, 42.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_59.setTransform(46.6, 42.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_60.setTransform(46.6, 42.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_61.setTransform(109.2, 42.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_62.setTransform(109.2, 42.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_63.setTransform(31.2, 42.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_64.setTransform(31.2, 42.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_65.setTransform(93.5, 42.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_66.setTransform(93.5, 42.9);

        // Block-2    
        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_113.setTransform(481, 36.4);
        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_110.setTransform(466, 36.4);
        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_109.setTransform(452, 36.4);
        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_108.setTransform(436, 36.4);
        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_107.setTransform(420, 36.4);
        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_106.setTransform(406, 36.4);
        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_105.setTransform(392, 36.4);
        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_104.setTransform(377, 36.4);
        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_103.setTransform(362, 36.4);
        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_102.setTransform(347, 36.4);
        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_101.setTransform(331, 36.4);
        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_35.setTransform(315, 36.4);
        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_34.setTransform(300, 36.4);
        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_33.setTransform(285, 36.4);
        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_32.setTransform(270, 36.4);
        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_31.setTransform(229, 36.4);
        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(214.7, 36.3);
        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(199.2, 36.3);
        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(183.6, 36.3);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(185.2, 42.9);
        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(185.2, 42.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(200.6, 42.9);
        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(200.6, 42.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(216.3, 42.9);
        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(216.3, 42.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_1.setTransform(230.3, 42.9);
        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_2.setTransform(230.3, 42.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_3.setTransform(271.3, 42.9);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_4.setTransform(271.3, 42.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_5.setTransform(286.3, 42.9);
        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_6.setTransform(286.3, 42.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_7.setTransform(301.3, 42.9);
        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_8.setTransform(301.3, 42.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_9.setTransform(316.3, 42.9);
        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_10.setTransform(316.3, 42.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_11.setTransform(332.3, 42.9);
        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_12.setTransform(332.3, 42.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_13.setTransform(348.3, 42.9);
        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_14.setTransform(348.3, 42.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_15.setTransform(363.3, 42.9);
        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_16.setTransform(363.3, 42.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_17.setTransform(378.3, 42.9);
        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_18.setTransform(378.3, 42.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_19.setTransform(393.3, 42.9);
        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_20.setTransform(393.3, 42.9);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_21.setTransform(407.3, 42.9);
        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_22.setTransform(407.3, 42.9);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_23.setTransform(421.3, 42.9);
        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_24.setTransform(421.3, 42.9);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_25.setTransform(437.3, 42.9);
        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_26.setTransform(437.3, 42.9);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_27.setTransform(453.3, 42.9);
        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_28.setTransform(453.3, 42.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_29.setTransform(467.3, 42.9);
        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_30.setTransform(467.3, 42.9);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_111.setTransform(482.3, 42.9);
        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_112.setTransform(482.3, 42.9);

        this.roundRect2 = new cjs.Shape(); // white block
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(20, 35, 219, 107, 5);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape(); // white block
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(20, 35, 233, 107, 5);
        this.roundRect3.setTransform(238, 0);

        // Main yellow block
        this.roundRect1 = new cjs.Shape(); // white block
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 1, 509, 149, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance_1 = new lib.p124_1();
        this.instance_1.setTransform(29, 53, 0.465, 0.465);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.instance_1, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95,
            this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86,
            this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77,
            this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_66, this.shape_65,
            this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56,
            this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47,
            this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37,
            this.shape_36, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.text_10);
        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10);
        this.addChild(this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_19, this.shape_20);
        this.addChild(this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.shape_25, this.shape_26, this.shape_27, this.shape_28, this.shape_29, this.shape_30,
            this.shape_31, this.shape_32, this.shape_33, this.shape_34, this.shape_35);
        this.addChild(this.shape_101, this.shape_102, this.shape_103, this.shape_104, this.shape_105, this.shape_106, this.shape_107, this.shape_108,
            this.shape_109, this.shape_110, this.shape_111, this.shape_112, this.shape_113);
        this.addChild(this.text_6, this.text_7, this.text_8, this.text_9, this.text_11, this.text_12, this.text_13, this.text_14,
            this.text_15, this.text_16, this.text_17, this.text_18, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.3, 143.6);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Hur många kakor har ätits upp?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.instance_2 = new lib.p124_2();
        this.instance_2.setTransform(0, 36, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 252.5, 93, 10);
        this.roundRect1.setTransform(0, 16);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(256.5, 16);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 114);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(256.5, 114);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 212.5);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(256.5, 212.5);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 311);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(256.5, 311);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (row == 2) {
                    rowSpace = 2.028;
                } else if (row == 3) {
                    rowSpace = 3.04;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.9).drawRect(110 + (columnSpace * 259.5), 87 + (rowSpace * 98), 19.5, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);
        // row-1
        this.text_1 = new cjs.Text("6  –          =  4", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 25;
        this.text_1.setTransform(78, 90);
        this.text_2 = new cjs.Text("6  –          =  3", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(337, 90);
        // row-2
        this.text_3 = new cjs.Text("7  –          =  5", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(78, 188);
        this.text_4 = new cjs.Text("7  –          =  3", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(337, 188);
        // row-3
        this.text_5 = new cjs.Text("8  –          =  6", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(78, 289);
        this.text_6 = new cjs.Text("8  –          =  5", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 25;
        this.text_6.setTransform(337, 289);
        // row-4
        this.text_7 = new cjs.Text("8  –          =  6", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 25;
        this.text_7.setTransform(78, 388);
        this.text_8 = new cjs.Text("8  –          =  5", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 25;
        this.text_8.setTransform(337, 388);

        this.addChild(this.text_q1, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,
            this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 380);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(299, 276, 1, 1, 0, 0, 0, 256.3, 217.9);
        this.v2 = new lib.Symbol6();
        this.v2.setTransform(299, 434, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
