(function(lib, img, cjs) {
    (lib.DottedCircle = function(dotCount, gap) {
        this.initialize();
        for (var i = 0; i < dotCount; i++) {
            var text = new createjs.TextArc("–", 'bold 24pt Arial Black', '#000', 100);
            text.textAlign = "center";
            text.rotation = 0 + (gap * i);
            text.x = 0;
            text.y = 0;
            this.addChild(text);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.init_Magic_Wand = function() {
        for (var i = 0; i < 18; i++) {
            lib.properties.manifest.push({
                src: "../viovar/images/magic_wand/magic_wand" + i + ".png",
                id: "magic_wand" + i
            })
        };
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.magic_wand0 = function() {
        this.initialize(img.magic_wand0);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand1 = function() {
        this.initialize(img.magic_wand1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand2 = function() {
        this.initialize(img.magic_wand2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand3 = function() {
        this.initialize(img.magic_wand3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand4 = function() {
        this.initialize(img.magic_wand4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand5 = function() {
        this.initialize(img.magic_wand5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand6 = function() {
        this.initialize(img.magic_wand6);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand7 = function() {
        this.initialize(img.magic_wand7);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand8 = function() {
        this.initialize(img.magic_wand8);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand9 = function() {
        this.initialize(img.magic_wand9);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand10 = function() {
        this.initialize(img.magic_wand10);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand11 = function() {
        this.initialize(img.magic_wand11);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand12 = function() {
        this.initialize(img.magic_wand12);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand13 = function() {
        this.initialize(img.magic_wand13);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand14 = function() {
        this.initialize(img.magic_wand14);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand15 = function() {
        this.initialize(img.magic_wand15);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand16 = function() {
        this.initialize(img.magic_wand16);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.magic_wand17 = function() {
        this.initialize(img.magic_wand17);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    // (lib.magic_wand18 = function() {
    //     this.initialize(img.magic_wand18);
    // }).prototype = p = new cjs.Bitmap();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    // (lib.magic_wand19 = function() {
    //     this.initialize(img.magic_wand19);
    // }).prototype = p = new cjs.Bitmap();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    // (lib.magic_wand20 = function() {
    //     this.initialize(img.magic_wand20);
    // }).prototype = p = new cjs.Bitmap();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    // (lib.magic_wand21 = function() {
    //     this.initialize(img.magic_wand21);
    // }).prototype = p = new cjs.Bitmap();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    // (lib.magic_wand22 = function() {
    //     this.initialize(img.magic_wand22);
    // }).prototype = p = new cjs.Bitmap();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.Magic_Wand = function(myStage, isShowAnsBtn) {
        this.initialize();

        if (isShowAnsBtn == null) {
            isShowAnsBtn = true;
        }
        var whiteBackground = new cjs.Shape();
        whiteBackground.graphics.f('#ffffff').drawRect(-135, -100, 1920, 1080);
        var xPos = 463,
            yPos = -120,
            scaleX = 1,
            scaleY = 1;

        var magic_wand0 = new lib.magic_wand0();
        magic_wand0.setTransform(xPos, yPos, scaleX, scaleY);
        var magic_wand1 = new lib.magic_wand1();
        magic_wand1.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand1.visible = false;
        var magic_wand2 = new lib.magic_wand2();
        magic_wand2.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand2.visible = false;
        var magic_wand3 = new lib.magic_wand3();
        magic_wand3.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand3.visible = false;
        var magic_wand4 = new lib.magic_wand4();
        magic_wand4.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand4.visible = false;
        var magic_wand5 = new lib.magic_wand5();
        magic_wand5.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand5.visible = false;
        var magic_wand6 = new lib.magic_wand6();
        magic_wand6.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand6.visible = false;
        var magic_wand7 = new lib.magic_wand7();
        magic_wand7.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand7.visible = false;
        var magic_wand8 = new lib.magic_wand8();
        magic_wand8.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand8.visible = false;
        var magic_wand9 = new lib.magic_wand9();
        magic_wand9.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand9.visible = false;
        var magic_wand10 = new lib.magic_wand10();
        magic_wand10.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand2.visible = false;
        var magic_wand11 = new lib.magic_wand11();
        magic_wand11.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand11.visible = false;
        var magic_wand12 = new lib.magic_wand12();
        magic_wand12.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand12.visible = false;
        var magic_wand13 = new lib.magic_wand13();
        magic_wand13.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand13.visible = false;
        var magic_wand14 = new lib.magic_wand14();
        magic_wand14.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand14.visible = false;
        var magic_wand15 = new lib.magic_wand15();
        magic_wand15.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand15.visible = false;
        var magic_wand16 = new lib.magic_wand16();
        magic_wand16.setTransform(xPos, yPos, scaleX, scaleY);
        // magic_wand16.visible = false;
        var magic_wand17 = new lib.magic_wand17();
        magic_wand17.setTransform(xPos + 180, yPos + 300, scaleX, scaleY);
        magic_wand17.regX = 180, magic_wand17.regY = 300;
        // magic_wand17.visible = false;
        // var magic_wand18 = new lib.magic_wand18();
        // magic_wand18.setTransform(xPos, yPos, scaleX, scaleY);
        // // magic_wand18.visible = false;
        // var magic_wand19 = new lib.magic_wand19();
        // magic_wand19.setTransform(xPos, yPos, scaleX, scaleY);
        // // magic_wand19.visible = false;
        // var magic_wand20 = new lib.magic_wand20();
        // magic_wand20.setTransform(xPos, yPos, scaleX, scaleY);
        // // magic_wand20.visible = false;
        // var magic_wand21 = new lib.magic_wand21();
        // magic_wand21.setTransform(xPos, yPos, scaleX, scaleY);
        // // magic_wand21.visible = false;
        // var magic_wand22 = new lib.magic_wand22();
        // magic_wand22.setTransform(xPos + 180, yPos + 300);
        // magic_wand22.regX = 180, magic_wand22.regY = 300;
        // magic_wand22.visible = false;

        // myStage.addChild(whiteBackground, magic_wand22, magic_wand21, magic_wand20, magic_wand19, magic_wand18, magic_wand17, magic_wand16, magic_wand15, magic_wand14, magic_wand13, magic_wand12, magic_wand11, magic_wand10, magic_wand9, magic_wand8, magic_wand7, magic_wand6, magic_wand5, magic_wand4, magic_wand3, magic_wand2, magic_wand1, magic_wand0);
        myStage.addChild(whiteBackground, magic_wand17, magic_wand16, magic_wand15, magic_wand14, magic_wand13, magic_wand12, magic_wand11, magic_wand10, magic_wand9, magic_wand8, magic_wand7, magic_wand6, magic_wand5, magic_wand4, magic_wand3, magic_wand2, magic_wand1, magic_wand0);

        magic_wand0.addEventListener('click', function() {

            cjs.Tween.get(magic_wand0).to({
                alphaFrom: 1,
                alphaTo: 1
            }, 1000).call(function() {

                magic_wand0.visible = false;
                cjs.Tween.get(magic_wand1).to({
                    alphaFrom: 1,
                    alphaTo: 1
                }, 0).call(function() {
                    magic_wand1.visible = false;
                    cjs.Tween.get(magic_wand2).to({
                        alphaFrom: 1,
                        alphaTo: 1
                    }, 0).call(function() {
                        magic_wand2.visible = false;
                        cjs.Tween.get(magic_wand3).to({
                            alphaFrom: 1,
                            alphaTo: 1
                        }, 0).call(function() {
                            magic_wand3.visible = false;
                            cjs.Tween.get(magic_wand4).to({
                                alphaFrom: 1,
                                alphaTo: 1
                            }, 0).call(function() {
                                magic_wand4.visible = false;
                                cjs.Tween.get(magic_wand5).to({
                                    alphaFrom: 1,
                                    alphaTo: 1
                                }, 0).call(function() {
                                    magic_wand5.visible = false;
                                    cjs.Tween.get(magic_wand6).to({
                                        alphaFrom: 1,
                                        alphaTo: 1
                                    }, 0).call(function() {
                                        magic_wand6.visible = false;
                                        cjs.Tween.get(magic_wand7).to({
                                            alphaFrom: 1,
                                            alphaTo: 1
                                        }, 0).call(function() {
                                            magic_wand7.visible = false;
                                            cjs.Tween.get(magic_wand8).to({
                                                alphaFrom: 1,
                                                alphaTo: 1
                                            }, 0).call(function() {
                                                magic_wand8.visible = false;
                                                cjs.Tween.get(magic_wand9).to({
                                                    alphaFrom: 1,
                                                    alphaTo: 1
                                                }, 0).call(function() {
                                                    magic_wand9.visible = false;
                                                    cjs.Tween.get(magic_wand10).to({
                                                        alphaFrom: 1,
                                                        alphaTo: 1
                                                    }, 0).call(function() {
                                                        magic_wand10.visible = false;
                                                        cjs.Tween.get(magic_wand11).to({
                                                            alphaFrom: 1,
                                                            alphaTo: 1
                                                        }, 0).call(function() {
                                                            magic_wand11.visible = false;
                                                            cjs.Tween.get(magic_wand12).to({
                                                                alphaFrom: 1,
                                                                alphaTo: 1
                                                            }, 200).call(function() {
                                                                magic_wand12.visible = false;
                                                                cjs.Tween.get(magic_wand13).to({
                                                                    alphaFrom: 1,
                                                                    alphaTo: 1
                                                                }, 100).call(function() {
                                                                    magic_wand13.visible = false;
                                                                    cjs.Tween.get(magic_wand14).to({
                                                                        alphaFrom: 1,
                                                                        alphaTo: 1
                                                                    }, 200).call(function() {
                                                                        magic_wand14.visible = false;
                                                                        cjs.Tween.get(magic_wand15).to({
                                                                            alphaFrom: 1,
                                                                            alphaTo: 1
                                                                        }, 200).call(function() {
                                                                            magic_wand15.visible = false;
                                                                            cjs.Tween.get(magic_wand16).to({
                                                                                alphaFrom: 1,
                                                                                alphaTo: 1
                                                                            }, 200).call(function() {
                                                                                magic_wand16.visible = false;
                                                                                cjs.Tween.get(magic_wand17).to({
                                                                                    alphaFrom: 1,
                                                                                    alphaTo: 1
                                                                                }, 2400).call(function() {
                                                                                    // magic_wand17.visible = false;

                                                                                    cjs.Tween.get(magic_wand17).to({
                                                                                        scaleX: 0,
                                                                                        scaleY: 0
                                                                                    }, 700).call(function() {
                                                                                        magic_wand17.visible = false;
                                                                                        whiteBackground.visible = false;
                                                                                        if (isShowAnsBtn == true) {
                                                                                            $(btn_answers).show();
                                                                                            $(btn_answers).removeClass('disabled');
                                                                                        }
                                                                                    });



                                                                                    // cjs.Tween.get(magic_wand18).to({
                                                                                    //     alphaFrom: 1,
                                                                                    //     alphaTo: 1
                                                                                    // }, 100).call(function() {
                                                                                    //     magic_wand18.visible = false;
                                                                                    //     cjs.Tween.get(magic_wand19).to({
                                                                                    //         alphaFrom: 1,
                                                                                    //         alphaTo: 1
                                                                                    //     }, 100).call(function() {
                                                                                    //         magic_wand19.visible = false;
                                                                                    //         cjs.Tween.get(magic_wand20).to({
                                                                                    //             alphaFrom: 1,
                                                                                    //             alphaTo: 1
                                                                                    //         }, 100).call(function() {
                                                                                    //             magic_wand20.visible = false;
                                                                                    //             cjs.Tween.get(magic_wand21).to({
                                                                                    //                 alphaFrom: 1,
                                                                                    //                 alphaTo: 1
                                                                                    //             }, 100).call(function() {
                                                                                    //                 magic_wand21.visible = false;
                                                                                    //                 cjs.Tween.get(magic_wand22).to({
                                                                                    //                     alphaFrom: 1,
                                                                                    //                     alphaTo: 1
                                                                                    //                 }, 2000).call(function() {
                                                                                    //                     cjs.Tween.get(magic_wand22).to({
                                                                                    //                         scaleX: 0,
                                                                                    //                         scaleY: 0
                                                                                    //                     }, 700).call(function() {
                                                                                    //                         magic_wand22.visible = false;
                                                                                    //                         whiteBackground.visible = false;
                                                                                    //                         if (isShowAnsBtn == true) {
                                                                                    //                             $(btn_answers).show();
                                                                                    //                             $(btn_answers).removeClass('disabled');
                                                                                    //                         }
                                                                                    //                     });
                                                                                    //                 });
                                                                                    //             });
                                                                                    //         });
                                                                                    //     });
                                                                                    // });



                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
