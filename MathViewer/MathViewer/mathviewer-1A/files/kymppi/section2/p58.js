(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p58_1.png",
            id: "p58_1"
        }, {
            src: "images/p58_2.png",
            id: "p58_2"
        }]
    };


    (lib.p58_1 = function() {
        this.initialize(img.p58_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p58_2 = function() {
        this.initialize(img.p58_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 127, 140);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text_4 = new cjs.Text("58", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(39, 650);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#85BE3F").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.instance = new lib.p58_1();
        this.instance.setTransform(92, 35, 0.68, 0.68);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(450 + (columnSpace * 32), 41, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape_2, this.shape_1, this.shape, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text_q.lineHeight = 19;
        this.text_q.setTransform(15, 1.2);

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#85BE3F");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 256, 86, 10);
        this.roundRect1.setTransform(0, 14);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 14);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 108);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 108);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 202);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 202);

        var arrxPos = ['65', '328'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);

            for (var column = 0; column < 6; column++) {
                var columnSpace = column;
                var fColor = '';
                if (column == 5) {
                    columnSpace = 5.4;
                }
                for (var row = 0; row < 3; row++) {
                    var rowSpace = row;
                    if (i == 0 && row == 0) {
                        fColor = '#DA2129'; //red    
                    } else if (i == 0 && row == 1) {
                        fColor = '#20B14A'; //green    
                    } else if (i == 0 && row == 2) {
                        rowSpace = 1.96;
                        fColor = '#F1652B'; //orange    
                    } else if (i == 1 && row == 0) {
                        fColor = '#0095DA'; //blue
                    } else if (i == 1 && row == 1) {
                        fColor = '#FFF679'; //yellow
                    } else if (i == 1 && row == 2) {
                        rowSpace = 1.96;
                        fColor = '#8490C8'; // light blue
                    }

                    this.shape_group1.graphics.f(fColor).s("#6E6E70").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * 22.5), 56 + (rowSpace * 96), 9.2, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['79', '173', '267'];
        var xPos = 27,
            padding,
            yPos,
            rectWidth = 100,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 44;
                if (i == 1) {
                    padding = padding + 60.5;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(71, 69);
        this.text_2 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(92, 69);
        this.text_3 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(111.5, 69);
        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(132, 69);

        this.text_5 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(71, 163);
        this.text_6 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 40;
        this.text_6.setTransform(92, 163);
        this.text_7 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 40;
        this.text_7.setTransform(111.5, 163);
        this.text_8 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 40;
        this.text_8.setTransform(132, 163);

        this.text_9 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 40;
        this.text_9.setTransform(71, 257);
        this.text_10 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 40;
        this.text_10.setTransform(92, 257);
        this.text_11 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_11.lineHeight = 40;
        this.text_11.setTransform(111.5, 257);
        this.text_12 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_12.lineHeight = 40;
        this.text_12.setTransform(132, 257);

        this.text_13 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_13.lineHeight = 40;
        this.text_13.setTransform(336, 69);
        this.text_14 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_14.lineHeight = 40;
        this.text_14.setTransform(357, 69);
        this.text_15 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_15.lineHeight = 40;
        this.text_15.setTransform(376.5, 69);
        this.text_16 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_16.lineHeight = 40;
        this.text_16.setTransform(397, 69);

        this.text_17 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_17.lineHeight = 40;
        this.text_17.setTransform(336, 163);
        this.text_18 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_18.lineHeight = 40;
        this.text_18.setTransform(357, 163);
        this.text_19 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_19.lineHeight = 40;
        this.text_19.setTransform(376.5, 163);
        this.text_20 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_20.lineHeight = 40;
        this.text_20.setTransform(397, 163);

        this.text_21 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_21.lineHeight = 40;
        this.text_21.setTransform(336, 257);
        this.text_22 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_22.lineHeight = 40;
        this.text_22.setTransform(357, 257);
        this.text_23 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_23.lineHeight = 40;
        this.text_23.setTransform(376.5, 257);
        this.text_24 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_24.lineHeight = 40;
        this.text_24.setTransform(397, 257);

        this.addChild(this.text_q, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.shape_group1, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12)
        this.addChild(this.text_13, this.text_14, this.text_15, this.text_16, this.text_17, this.text_18, this.text_19, this.text_20)
        this.addChild(this.text_21, this.text_22, this.text_23, this.text_24);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 336.6);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#85BE3F");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 25, 516, 129, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(100 + (columnSpace * 120.5), 34 + (row * 29), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("6  –  1  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(35, 37);
        this.label2 = new cjs.Text("6  –  2  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(35, 67);
        this.label3 = new cjs.Text("6  –  3  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(35, 97);
        this.label4 = new cjs.Text("6  –  4  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(35, 127);

        this.label6 = new cjs.Text("6  –  0  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(155, 37);
        this.label7 = new cjs.Text("6  –  5  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(155, 67);
        this.label8 = new cjs.Text("6  –  6  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(155, 97);
        this.label9 = new cjs.Text("6  –  3  =  ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(155, 127);

        this.label11 = new cjs.Text("5  +  1  =  ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(275, 37);
        this.label12 = new cjs.Text("1  +  5  =  ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(275, 67);
        this.label13 = new cjs.Text("4  +  2  =  ", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(275, 97);
        this.label14 = new cjs.Text("2  +  4  =  ", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(275, 127);

        this.instance = new lib.p58_2();
        this.instance.setTransform(410, 37, 0.549, 0.549);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);
        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.p72 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(298.5, 385, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(293, 482, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
