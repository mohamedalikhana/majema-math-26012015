var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c17_ex1_s2_1.png",
            id: "c17_ex1_s2_1"
        }, {
            src: "images/c17_ex1_s2_2.png",
            id: "c17_ex1_s2_2"
        }, {
            src: "images/c17_ex1_s1_3.png",
            id: "c17_ex1_s2_3"
        }, {
            src: "images/c17_ex1_s1_2.png",
            id: "c17_ex1_s2_4"
        }]
    };



    (lib.c17_ex1_s2_1 = function() {
        this.initialize(img.c17_ex1_s2_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c17_ex1_s2_2 = function() {
        this.initialize(img.c17_ex1_s2_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c17_ex1_s2_3 = function() {
        this.initialize(img.c17_ex1_s2_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c17_ex1_s2_4 = function() {
        this.initialize(img.c17_ex1_s2_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Vi skriver subtraktion", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("17", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);
  
        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.answers = function(fontSize) {
        this.initialize();
        this.text_1 = new cjs.Text("4", fontSize + "px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 0);

        this.text_2 = this.text_1.clone(true);
        this.text_2.text = "–";
        this.text_2.setTransform(fontSize - (40 * fontSize / 100), 0);

        this.text_3 = this.text_1.clone(true);
        this.text_3.text = "2";
        this.text_3.setTransform(2 * (fontSize - (40 * fontSize / 100)), 0);

        this.text_4 = this.text_1.clone(true);
        this.text_4.text = "=";
        this.text_4.setTransform(3 * (fontSize - (40 * fontSize / 100)), 0);

        this.text_5 = this.text_1.clone(true);
        this.text_5.text = "2";
        this.text_5.setTransform(4 * (fontSize - (35 * fontSize / 100)), 0);

        this.addChild(this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.Stage0 = function() {
        this.initialize();

        this.allbirds = new lib.c17_ex1_s2_1();
        this.allbirds.setTransform(740, -60, 0.52, 0.52);

        this.thirdBird = new lib.c17_ex1_s2_4();
        this.thirdBird.setTransform(980, 8, 0.52, 0.52)
        this.thirdBird.visible = true;

        this.flyBird1 = new lib.c17_ex1_s2_3();
        this.flyBird1.setTransform(1005, -50, 0.52, 0.52)
        this.flyBird1.visible = false;

        this.flyBird2 = new lib.c17_ex1_s2_3();
        this.flyBird2.setTransform(900, -40, 0.52, 0.52)
        this.flyBird2.visible = false;


        this.secondBird = new lib.c17_ex1_s2_2();
        this.secondBird.setTransform(890, 23, 0.52, 0.52)

        this.instrTexts = [];
        //this.instrTexts.push('');
        this.instrTexts.push('Hur många?');
        this.instrTexts.push('Det är 4 fåglar.');
        this.instrTexts.push('2 fåglar flyger iväg.');
        this.instrTexts.push('Hur många stannar kvar?');
        this.instrTexts.push('2 fåglar stannar kvar.');
        this.instrText1 = new cjs.Text(this.instrTexts[0], "48px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'left';
        this.instrText1.setTransform(0, -30);
        this.instrText1.visible = false;

        this.instrText2 = this.instrText1.clone(true);
        this.instrText2.text = this.instrTexts[1];
        this.instrText3 = this.instrText1.clone(true);
        this.instrText3.text = this.instrTexts[2];
        this.instrText3.textAlign = 'left';
        this.instrText3.setTransform(0, 40);


        this.instrText4 = this.instrText1.clone(true);
        this.instrText4.text = this.instrTexts[3];
        this.instrText4.textAlign = 'left';
        this.instrText4.setTransform(0, 110);



        this.instrText5 = this.instrText1.clone(true);
        this.instrText5.text = this.instrTexts[4];
        this.instrText5.textAlign = 'left';
        this.instrText5.setTransform(0, 180);
         this.answers = new lib.answers(80);
        // this.answers.text_4.visible = true;
        // this.answers.text_3.visible = true;
        // this.answers.text_2.visible = true;
        // this.answers.text_1.visible = true;
        this.answers.setTransform(0, 260, 1, 1);
        this.answerText = new cjs.Text("4 minus 2 är lika med 2.", "48px 'Myriad Pro'");
        this.answerText.lineHeight = 19;
        this.answerText.textAlign = 'left';
        this.answerText.setTransform(0, 370);
        this.answerText.visible = false;
        this.teacherInstructions = [];

        this.teacherInstructions.push("Hur många fåglar stannar kvar?");
        this.teacherInstructions.push("Säg högt: 4 minus 2 är lika med 2.");

        this.teacherInstruction1 = new cjs.Text(this.teacherInstructions[0], "26px 'Myriad Pro'", "#00B4EA")
        this.teacherInstruction1.textAlign = 'center';
        this.teacherInstruction1.lineHeight = 19;
        this.teacherInstruction1.visible = false;
        this.teacherInstruction1.setTransform(663, 440);

        this.teacherInstruction2 = this.teacherInstruction1.clone(true);
        this.teacherInstruction2.text = this.teacherInstructions[1];

        //Hur många fåglar stannar kvar?
        this.addChild(this.answerText, this.instrText1, this.instrText2, this.instrText3, this.instrText4, this.instrText5, this.allbirds, this.thirdBird, this.flyBird1, this.flyBird2, this.secondBird, this.teacherInstruction1, this.teacherInstruction2, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    //birdFly
    /*
this.stage0.flyBird1.visible = true;
        this.tweens = [];
        var modX = 500,
            modY = -150;
        this.tweens.push({
            ref: this.stage0.flyBird1,
            alphaFrom: 1,
            alphaTo: 1,
            positionTo: {
                x: this.stage0.flyBird1.x + modX,
                y: this.stage0.flyBird1.y + modY
            },
            positionFrom: {
                x: this.stage0.flyBird1.x,
                y: this.stage0.flyBird1.y
            },
            wait: 1000,
            alphaTimeout: 1500
        });
    */

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
        this.stage0.instrText2.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText1,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 1500
        });

        this.tweens.push({
            ref: this.stage0.instrText2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2800,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.instrText4.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.instrText4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.instrText4.visible = true;
        this.stage0.thirdBird.visible = true;
        this.stage0.flyBird1.visible = true;
        this.stage0.flyBird2.visible = true;

        // this.stage0.teacherInstruction1.visible = true;
        this.tweens = [];
        var modX = 1200,
            modY = -150;

        this.tweens.push({
            ref: this.stage0.flyBird1,
            alphaFrom: 0,
            alphaTo: 10,
            positionTo: {
                x: this.stage0.flyBird1.x + modX,
                y: this.stage0.flyBird1.y + modY
            },
            positionFrom: {
                x: this.stage0.flyBird1.x,
                y: this.stage0.flyBird1.y
            },
            wait: 1300,
            alphaTimeout: 5000,
            override: true
        });

        this.tweens.push({
            ref: this.stage0.flyBird2,
            alphaFrom: 0,
            alphaTo: 10,
            positionTo: {
                x: this.stage0.flyBird2.x + modX,
                y: this.stage0.flyBird2.y + modY
            },
            positionFrom: {
                x: this.stage0.flyBird2.x,
                y: this.stage0.flyBird2.y
            },
            wait: 1300,
            alphaTimeout: 5000,
            override: true
        });
        //  this.tweens.push({
        //     ref: this.stage0.flyBird1,
        //     alphaFrom: 0,
        //     alphaTo: 1,
        //     wait: 500,
        //     alphaTimeout: 1000,override:true
        // });
        this.tweens.push({
            ref: this.stage0.thirdBird,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 1000
        });
        this.tweens.push({
            ref: this.stage0.secondBird,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 1000
        });
        // this.tweens.push({
        //     ref: this.stage0.teacherInstruction1,
        //     alphaFrom: 0,
        //     alphaTo: 1,
        //     wait: 3500,
        //     alphaTimeout: 2000
        // });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage5 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.instrText4.visible = true;
        this.stage0.instrText5.visible = true;
        this.stage0.thirdBird.visible = false;
        this.stage0.secondBird.visible = false;

        // this.stage0.teacherInstruction1.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage6 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.instrText4.visible = true;
        this.stage0.instrText5.visible = true;

        this.stage0.thirdBird.visible = false;
        this.stage0.secondBird.visible = false;

        // this.stage0.teacherInstruction1.visible = true;
        this.answers = this.stage0.answers;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;


        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage7 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.instrText4.visible = true;
        this.stage0.instrText5.visible = true;

        this.stage0.thirdBird.visible = false;
        this.stage0.secondBird.visible = false;

        // this.stage0.teacherInstruction1.visible = true;
        this.answers = this.stage0.answers;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;


        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage8 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.instrText4.visible = true;
        this.stage0.instrText5.visible = true;

        this.stage0.thirdBird.visible = false;
        this.stage0.secondBird.visible = false;

        // this.stage0.teacherInstruction1.visible = true;
        this.stage0.teacherInstruction2.visible = true;
        this.answers = this.stage0.answers;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.stage0.answerText.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.answerText,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        // this.tweens.push({
        //     ref: this.stage0.teacherInstruction1,
        //     alphaFrom: 1,
        //     alphaTo: 0,
        //     wait: 3000,
        //     alphaTimeout: 2000
        // });
        this.tweens.push({
            ref: this.stage0.teacherInstruction2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 4500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;

        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;

        this.stage6 = new lib.Stage6();
        this.stage6.visible = false;

        this.stage7 = new lib.Stage7();
        this.stage7.visible = false;

        this.stage8 = new lib.Stage8();
        this.stage8.visible = false;


        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7, this.stage8);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
