(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p115_1.png",
            id: "p115_1"
        }, {
            src: "images/p115_2.png",
            id: "p115_2"
        }]
    };

    // symbols:
    (lib.p115_1 = function() {
        this.initialize(img.p115_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p115_2 = function() {
        this.initialize(img.p115_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("115", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(553, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Räkna och hitta bokstäverna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p115_1();
        this.instance.setTransform(410, 254, 0.45, 0.45);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 517, 337, 10);
        this.roundRect1.setTransform(0, 25);
        //column-1
        this.label1 = new cjs.Text("10  –  4  =", "16px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(20, 44);
        this.label2 = new cjs.Text("6  +  2  =", "16px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(27, 72);
        this.label3 = new cjs.Text("10  –  9  =", "16px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(20, 101);
        this.label4 = new cjs.Text("8  –  7  =", "16px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(27.5, 131);
        this.label5 = new cjs.Text("6  +  1  =", "16px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(27, 159);
        //column-2
        this.label6 = new cjs.Text("1  +  1  =", "16px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(194, 44);
        this.label7 = new cjs.Text("8  +  2  =", "16px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(194.1, 72);
        this.label8 = new cjs.Text("8  –  4  =", "16px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(196, 101);
        this.label9 = new cjs.Text("2  +  3  =", "16px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(194, 131);
        this.label10 = new cjs.Text("10  –  1  =", "16px 'Myriad Pro'");
        this.label10.lineHeight = 30;
        this.label10.setTransform(188, 159);
        this.label11 = new cjs.Text("5  +  4  =", "16px 'Myriad Pro'");
        this.label11.lineHeight = 30;
        this.label11.setTransform(194, 189);
        this.label12 = new cjs.Text("10  –  5  =", "16px 'Myriad Pro'");
        this.label12.lineHeight = 30;
        this.label12.setTransform(188, 217);
        this.label13 = new cjs.Text("9  –  5  =", "16px 'Myriad Pro'");
        this.label13.lineHeight = 30;
        this.label13.setTransform(196, 246);
        //column-3
        this.label15 = new cjs.Text("9  –  4  =", "16px 'Myriad Pro'");
        this.label15.lineHeight = 30;
        this.label15.setTransform(363, 44);
        this.label16 = new cjs.Text("6  –  5  =", "16px 'Myriad Pro'");
        this.label16.lineHeight = 30;
        this.label16.setTransform(363, 72);
        this.label17 = new cjs.Text("9  –  8  =", "16px 'Myriad Pro'");
        this.label17.lineHeight = 30;
        this.label17.setTransform(363, 101);
        this.label18 = new cjs.Text("2  +  7  =", "16px 'Myriad Pro'");
        this.label18.lineHeight = 30;
        this.label18.setTransform(362, 131);
        this.label19 = new cjs.Text("10  –  7  =", "16px 'Myriad Pro'");
        this.label19.lineHeight = 30;
        this.label19.setTransform(355.5, 159);
        this.label14 = new cjs.Text("9  –  9  =", "16px 'Myriad Pro'");
        this.label14.lineHeight = 30;
        this.label14.setTransform(363, 189);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 8; row++) {
                if (column == 0 && row > 4) {
                    continue;
                } else if (column == 2 && row > 5) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.662;
                } else if (column == 2) {
                    columnSpace = 1.31;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#8490C8").ss(0.9).drawRect(129 + (columnSpace * 258), 39 + (row * 29.1), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 11; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#8490C8").ss(0.9).drawRect(34 + (columnSpace * 29), 284, 20, 23);
        }
        this.textbox_group2.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(87, 62).lineTo(123, 62).moveTo(87, 91).lineTo(123, 91).moveTo(87, 120.3).lineTo(123, 120.3)
            .moveTo(87, 149.3).lineTo(123, 149.3).moveTo(87, 178.3).lineTo(123, 178.3);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(255, 62).lineTo(291, 62).moveTo(255, 91).lineTo(291, 91).moveTo(255, 120.3).lineTo(291, 120.3)
            .moveTo(255, 149.3).lineTo(291, 149.3).moveTo(255, 178.3).lineTo(291, 178.3).moveTo(255, 207.3).lineTo(291, 207.3).moveTo(255, 236.3).lineTo(291, 236.3).moveTo(255, 265.3).lineTo(291, 265.3);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(423, 62).lineTo(459, 62).moveTo(423, 91).lineTo(459, 91).moveTo(423, 120.3).lineTo(459, 120.3)
            .moveTo(423, 149.3).lineTo(459, 149.3).moveTo(423, 178.3).lineTo(459, 178.3).moveTo(423, 207.3).lineTo(459, 207.3);
        this.hrLine_3.setTransform(0, 0);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(32, 341).lineTo(352, 341);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(355, 340.5, 1.2, 1.2, 265);

        var arrText = ['D', 'L', 'S', 'I', 'R', 'A', 'N', 'Y', 'E', 'T', 'K'];
        var ToBeAdded = [];

        for (var i = 0; i < arrText.length; i++) {
            var columnSpace = i;
            var text = arrText[i];

            if (i == 2) {
                columnSpace = 1.94;
            } else if (i == 3) {
                columnSpace = 2.93;
            } else if (i == 4) {
                columnSpace = 3.8;
            } else if (i == 5) {
                columnSpace = 4.7;
            } else if (i == 6) {
                columnSpace = 5.63;
            } else if (i == 7) {
                columnSpace = 6.6;
            } else if (i == 8) {
                columnSpace = 7.55;
            } else if (i == 9) {
                columnSpace = 8.49;
            } else if (i == 10) {
                columnSpace = 9.4;
            }

            var temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(38 + (columnSpace * 31), 290);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 11; i++) {
            var columnSpace = i;
            var text = (i).toString();

            if (i == 2) {
                columnSpace = 1.94;
            } else if (i == 3) {
                columnSpace = 2.93;
            } else if (i == 4) {
                columnSpace = 3.8;
            } else if (i == 5) {
                columnSpace = 4.7;
            } else if (i == 6) {
                columnSpace = 5.63;
            } else if (i == 7) {
                columnSpace = 6.6;
            } else if (i == 8) {
                columnSpace = 7.55;
            } else if (i == 9) {
                columnSpace = 8.49;
            } else if (i == 10) {
                columnSpace = 9.2;
            }

            var temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            temp_label.lineHeight = -1;
            temp_label.setTransform(39 + (columnSpace * 31), 318);
            ToBeAdded.push(temp_label);
        }

        for (var i = 0; i < 11; i++) {
            var columnSpace = i;

            if (i == 2) {
                columnSpace = 1.94;
            } else if (i == 3) {
                columnSpace = 2.93;
            } else if (i == 4) {
                columnSpace = 3.8;
            } else if (i == 5) {
                columnSpace = 4.7;
            } else if (i == 6) {
                columnSpace = 5.63;
            } else if (i == 7) {
                columnSpace = 6.6;
            } else if (i == 8) {
                columnSpace = 7.55;
            } else if (i == 9) {
                columnSpace = 8.49;
            } else if (i == 10) {
                columnSpace = 9.4;
            }
            var temp_Line = new cjs.Shape();
            temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(43 + (columnSpace * 31), 334).lineTo(43 + (columnSpace * 31), 341);
            temp_Line.setTransform(0, 0);
            ToBeAdded.push(temp_Line);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4);
        this.addChild(this.instance, this.textbox_group1, this.textbox_group2, this.shape_1)
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7);
        this.addChild(this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14);
        this.addChild(this.label15, this.label16, this.label17, this.label18, this.label19);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 350);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många kan du köpa av varje?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(19, 0);

        this.text_q1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 0);

        this.instance = new lib.p115_2();
        this.instance.setTransform(10, 31, 0.48, 0.48);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 24, 95, 180, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(102, 24, 415, 180, 10);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(109, 30, 127, 82, 5);
        this.roundRect3.setTransform(0, 0);

        this.roundRect4 = this.roundRect3.clone(true);
        this.roundRect4.setTransform(136, 0);

        this.roundRect5 = this.roundRect3.clone(true);
        this.roundRect5.setTransform(273, 0);

        this.roundRect6 = this.roundRect3.clone(true);
        this.roundRect6.setTransform(0, 86);

        this.roundRect7 = this.roundRect3.clone(true);
        this.roundRect7.setTransform(136, 86);

        this.roundRect8 = this.roundRect3.clone(true);
        this.roundRect8.setTransform(273, 86);

        // row-1
        this.text_1 = new cjs.Text("1 kr", "13px 'Myriad Pro'");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(143, 37);
        this.text_1.skewX=13;
        this.text_1.skewY=13;
        this.text_2 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(332, 49.5);
        this.text_2.skewX=-13;
        this.text_2.skewY=-13;
        this.text_3 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(404, 39);
        this.text_3.skewX=21;
        this.text_3.skewY=21;
        // row-2
        this.text_4 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(175, 148);
        this.text_4.skewX=-16;
        this.text_4.skewY=-16;
        this.text_5 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(322, 138.5);
        this.text_5.skewX=-15;
        this.text_5.skewY=-15;
        this.text_6 = new cjs.Text("8 kr", "13px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(453, 139);
        this.text_6.skewX=-24;
        this.text_6.skewY=-24;

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(121, 107).lineTo(166, 107).moveTo(318, 107).lineTo(363, 107).moveTo(455, 107).lineTo(500, 107)
            .moveTo(176, 193).lineTo(221, 193).moveTo(318, 193).lineTo(363, 193).moveTo(455, 193).lineTo(500, 193);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text_q1, this.roundRect1, this.roundRect2);
        this.addChild(this.roundRect3, this.roundRect4, this.roundRect5,this.roundRect6, this.roundRect7, this.roundRect8, this.instance);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.hrLine_1);  
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 469, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(309, 104.7, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
