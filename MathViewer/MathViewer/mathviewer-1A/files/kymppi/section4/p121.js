(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p121_1.png",
            id: "p121_1"
        }, {
            src: "images/p121_2.png",
            id: "p121_2"
        }]
    };

    (lib.p121_1 = function() {
        this.initialize(img.p121_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p121_2 = function() {
        this.initialize(img.p121_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("42", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(73, 26);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8490C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(59, 23.5, 1.17, 1.2);

        this.text_2 = new cjs.Text("Prisskillnad", "24px 'MyriadPro-Semibold'", "#8490C8");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(120, 30);

        this.pageBottomText = new cjs.Text("kunna räkna addition och subtraktion med mynt upp till 10 kr", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(313, 650.5);

        this.text = new cjs.Text("121", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(560, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(587.3, 660.8);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_12 = new cjs.Text("Hur mycket mer kostar boken än nallen?", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 40;
        this.text_12.setTransform(15, 8);
        this.text_0 = new cjs.Text("Subtrahera det lägsta priset", "16px 'Myriad Pro'");
        this.text_0.lineHeight = 19;
        this.text_0.setTransform(286, 39);
        this.text = new cjs.Text("från det högsta. Då får du", "16px 'Myriad Pro'");
        this.text.lineHeight = 40;
        this.text.setTransform(292, 58);
        this.text_11 = new cjs.Text("prisskillnaden.", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 40;
        this.text_11.setTransform(328, 79);

        this.text_1 = new cjs.Text("8", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(38, 132);
        this.text_2 = new cjs.Text("–", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(79.2, 132);
        this.text_3 = new cjs.Text("3", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(98, 132);
        this.text_4 = new cjs.Text("=", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(139, 132);
        this.text_7 = new cjs.Text("5", "bold 35px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 40;
        this.text_7.setTransform(158, 132);
        this.text_5 = new cjs.Text("kr", "bold 29px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(117, 137);
        this.text_6 = new cjs.Text("kr", "bold 29px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 40;
        this.text_6.setTransform(57, 137);
        this.text_8 = new cjs.Text("kr", "bold 29px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 40;
        this.text_8.setTransform(177, 137);
        this.text_9 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 15;
        this.text_9.setTransform(182, 78);
        this.text_9.skewX = 21;
        this.text_9.skewY = 21;
        this.text_10 = new cjs.Text("8 kr", "13px 'Myriad Pro'");
        this.text_10.lineHeight = 15;
        this.text_10.setTransform(71, 74);
        this.text_10.skewX = -21;
        this.text_10.skewY = -21;


        var arrTxtbox = ['141'];
        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding = 38,
            yPos,
            rectWidth = 160,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 8,
            numberOfRects = 1;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);


        // Block-1
        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(165.9, 50.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_37.setTransform(151.1, 50.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(135.9, 50.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_39.setTransform(120.3, 50.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(104.9, 50.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(89.2, 50.4);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(73.2, 50.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(57.7, 50.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(42.2, 50.3);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(26.6, 50.3);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_47.setTransform(167.2, 56.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_48.setTransform(167.2, 56.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_49.setTransform(152.5, 56.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_50.setTransform(152.5, 56.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_51.setTransform(74.5, 56.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_52.setTransform(74.5, 56.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_53.setTransform(137.4, 56.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_54.setTransform(137.4, 56.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_55.setTransform(59.3, 56.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_56.setTransform(59.3, 56.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_57.setTransform(121.7, 56.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_58.setTransform(121.7, 56.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_59.setTransform(43.6, 56.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_60.setTransform(43.6, 56.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_61.setTransform(106.2, 56.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_62.setTransform(106.2, 56.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_63.setTransform(28.2, 56.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_64.setTransform(28.2, 56.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_65.setTransform(90.5, 56.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_66.setTransform(90.5, 56.9);

        // Block-2
        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(211.7, 50.3);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(196.2, 50.3);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(180.6, 50.3);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(213.3, 56.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(213.3, 56.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(197.6, 56.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(197.6, 56.9);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(182.2, 56.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(182.2, 56.9);

        this.shape_69 = new cjs.Shape(); // white block
        this.shape_69.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(17, 49, 208, 120, 5);
        this.shape_69.setTransform(0, 0);

        // Main yellow block
        this.shape_115 = new cjs.Shape(); // white block
        this.shape_115.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 1, 509, 175, 10);
        this.shape_115.setTransform(0, 0);

        this.instance_1 = new lib.p121_1();
        this.instance_1.setTransform(30, 15, 0.465, 0.465);

        this.addChild(this.shape_115, this.shape_69, this.instance_1, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95,
            this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86,
            this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77,
            this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_66, this.shape_65,
            this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56,
            this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47,
            this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37,
            this.shape_36, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.text_10, this.text_11, this.text_12);
        this.addChild(this.textbox_group1, this.text_0, this.shape_group1, this.shape_group2, this.text_6, this.text_7, this.text_8, this.text_9);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.3, 143.6);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Hur mycket mer kostar boken än leksaken?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.instance_2 = new lib.p121_2();
        this.instance_2.setTransform(12, 28, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 252.5, 117, 10);
        this.roundRect1.setTransform(0, 16);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(256.5, 16);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 139);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(256.5, 139);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 262);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(256.5, 262);

        var arrTxtbox = ['115', '237', '360'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 160,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 8,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 44;
                if (i == 1) {
                    padding = 71;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(44, 105);
        this.text_2 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(85.2, 105);
        this.text_3 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(104, 105);
        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(145, 105);
        this.text_5 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 45;
        this.text_5.setTransform(123, 110);
        this.text_6 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 45;
        this.text_6.setTransform(63, 110);

        // // row-1
        this.text_7 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 15;
        this.text_7.setTransform(46, 39.5);
        this.text_7.skewX = -10;
        this.text_7.skewY = -10;
        this.text_8 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_8.lineHeight = 15;
        this.text_8.setTransform(204, 48);
        this.text_8.skewX = 5;
        this.text_8.skewY = 5;
        this.text_9 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 15;
        this.text_9.setTransform(329, 39.5);
        this.text_9.skewX = -19;
        this.text_9.skewY = -19;
        this.text_10 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_10.lineHeight = 15;
        this.text_10.setTransform(454, 48);
        this.text_10.skewX = 5;
        this.text_10.skewY = 5;
        // // row-2
        this.text_11 = new cjs.Text("7 kr", "13px 'Myriad Pro'");
        this.text_11.lineHeight = 15;
        this.text_11.setTransform(64, 159.5);
        this.text_11.skewX = -10;
        this.text_11.skewY = -10;
        this.text_12 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_12.lineHeight = 15;
        this.text_12.setTransform(205.5, 181);
        this.text_12.skewX = -21;
        this.text_12.skewY = -21;
        this.text_13 = new cjs.Text("7 kr", "13px 'Myriad Pro'");
        this.text_13.lineHeight = 15;
        this.text_13.setTransform(334, 172);
        this.text_13.skewX = -29;
        this.text_13.skewY = -29;
        this.text_14 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_14.lineHeight = 15;
        this.text_14.setTransform(463, 193);
        this.text_14.skewX = -19;
        this.text_14.skewY = -19;
        // // row-3
        this.text_15 = new cjs.Text("9 kr", "13px 'Myriad Pro'");
        this.text_15.lineHeight = 15;
        this.text_15.setTransform(84, 291);
        this.text_15.skewX = -20;
        this.text_15.skewY = -20;
        this.text_16 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_16.lineHeight = 15;
        this.text_16.setTransform(211.5, 330);
        this.text_16.skewX = 21;
        this.text_16.skewY = 21;
        this.text_17 = new cjs.Text("9 kr", "13px 'Myriad Pro'");
        this.text_17.lineHeight = 15;
        this.text_17.setTransform(326, 279);
        this.text_17.skewX = 1;
        this.text_17.skewY = 1;
        this.text_18 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_18.lineHeight = 15;
        this.text_18.setTransform(469, 310);
        this.text_18.skewX = 13;
        this.text_18.skewY = 13;

        this.addChild(this.text_q1, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14);
        this.addChild(this.text_15, this.text_16, this.text_17, this.text_18);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(313, 280, 1, 1, 0, 0, 0, 256.3, 217.9);
        this.v2 = new lib.Symbol6();
        this.v2.setTransform(313, 462, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
