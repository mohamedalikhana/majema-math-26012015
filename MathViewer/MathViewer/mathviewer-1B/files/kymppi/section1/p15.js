(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p15_1.png",
            id: "p15_1"
        }, {
            src: "images/p15_2.png",
            id: "p15_2"
        }]
    };

    (lib.p15_1 = function() {
        this.initialize(img.p15_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);


    (lib.p15_2 = function() {
        this.initialize(img.p15_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();


        this.pageTitle = new cjs.Text("Talen 19 och 20", "24px 'MyriadPro-Semibold'", "#90BD22");
        this.pageTitle.setTransform(105, 42);

        this.text_1 = new cjs.Text("4", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(62, 42);

        this.text_2 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(553, 658);

        this.pageFooterTextRight = new cjs.Text("förstå och kunna använda talen 0 till 20", "9px 'Myriad Pro'");
        this.pageFooterTextRight.setTransform(390, 659);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(46, 15, 1.05, 1.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.pageTitle, this.pageFooterTextRight, this.shape_2, this.shape_1, this.shape, this.text, this.text_1, this.text_2, this.text_3);


        this.Symbol_yellowbox = new lib.Symbol_yellowbox();
        this.Symbol_yellowbox.setTransform(49, 57);

        this.Symbol_dog = new lib.Symbol_dog();
        this.Symbol_dog.setTransform(37, 38);

        this.addChild(this.Symbol_yellowbox, this.Symbol_dog);



        //green and yellow boxes


        // col-1

        var box_size = 14;
        var box_count = 10;


        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f('#20B14A').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_4.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_4.setTransform(462, 150);


        var box_size = 14;
        var box_count = 9;


        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.f('#FFF679').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_5.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_5.setTransform(483, 164);

        // col-2

        var box_size = 14;
        var box_count = 10;


        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.f('#20B14A').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_6.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_6.setTransform(528, 150);


        var box_size = 14;
        var box_count = 10;


        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.f('#20B14A').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_7.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_7.setTransform(549, 150);


        //Rectangle Boxes 4


        this.shape_group4 = new cjs.Shape();
        this.shape_group4.graphics.f('#EBF2DA').s("#000000").ss(0.7).drawRect(0, 0, 40, 16.5);
        this.shape_group4.setTransform(460, 295);

        this.shape_group5 = new cjs.Shape();
        this.shape_group5.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(0, 16.5).lineTo(0, 40).lineTo(40, 40).lineTo(40, 16.5);
        this.shape_group5.setTransform(460, 295);

        this.shape_group6 = new cjs.Shape();
        this.shape_group6.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 0).lineTo(20, 40);
        this.shape_group6.setTransform(460, 295);


        this.shape_group7 = new cjs.Shape();
        this.shape_group7.graphics.f('#EBF2DA').s("#000000").ss(0.7).drawRect(0, 0, 40, 16.5);
        this.shape_group7.setTransform(525, 295);

        this.shape_group8 = new cjs.Shape();
        this.shape_group8.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(0, 16.5).lineTo(0, 40).lineTo(40, 40).lineTo(40, 16.5);
        this.shape_group8.setTransform(525, 295);

        this.shape_group9 = new cjs.Shape();
        this.shape_group9.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 0).lineTo(20, 40);
        this.shape_group9.setTransform(525, 295);

        this.text_2 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.setTransform(460, 334);

        this.text_3 = new cjs.Text("9", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(480, 334);

        this.text_4 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.setTransform(525, 334);

        this.text_5 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.setTransform(545, 334);


        this.text_13 = new cjs.Text("t", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_13.setTransform(465, 307);

        this.text_14 = new cjs.Text("e", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_14.setTransform(485, 306);

        this.text_15 = new cjs.Text("t", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_15.setTransform(529, 307);

        this.text_16 = new cjs.Text("e", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_16.setTransform(548, 306);


        this.addChild(this.Line_4, this.Line_5, this.Line_6, this.Line_7,
            this.shape_group4, this.shape_group5, this.shape_group6, this.shape_group7, this.shape_group8, this.shape_group9, this.shape_group11,
            this.text_13, this.text_14, this.text_15, this.text_16, this.text_2, this.text_3, this.text_4, this.text_5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol_dog = function() {
        this.initialize();
        this.text = new cjs.Text("Nu har vi", "14px 'Myriad Pro'");
        this.text.setTransform(324, 34);

        this.text_1 = new cjs.Text("2 hela tiotal.", "14px 'Myriad Pro'");
        this.text_1.setTransform(313, 51);

        this.instance = new lib.p15_1();
        this.instance.setTransform(257, 12, 0.49, 0.49);
        this.addChild(this.text, this.text_1, this.instance);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol_yellowbox = function() {
        this.initialize();

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_71.setTransform(34, 11);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_72.setTransform(50, 11);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_73.setTransform(65.8, 11);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_74.setTransform(81.6, 11);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_75.setTransform(97.4, 11);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(113.2, 11);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(129, 11);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(144.8, 11);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(160.6, 11);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_80.setTransform(35, 16.5);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_81.setTransform(35, 16.5);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_82.setTransform(49.9, 16.5);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_83.setTransform(49.9, 16.5);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_84.setTransform(66, 16.5);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_85.setTransform(66, 16.5);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_86.setTransform(80.5, 16.5);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_87.setTransform(80.5, 16.5);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(97, 16.5);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(97, 16.5);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_90.setTransform(112, 16.5);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_91.setTransform(112, 16.5);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(128, 16.5);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(128, 16.5);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_94.setTransform(143, 16.5);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_95.setTransform(143, 16.5);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(159, 16.5);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(159, 16.5);

        // Main yellow block

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, -2, 205, 107, 10);
        this.shape_115.setTransform(0, 0);

        // Main white block
        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(22, 11, 160, 83, 5);
        this.shape_100.setTransform(0, 0);


        //Four innerrectangle boxes 

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f('#FFFFFF').s("#000000").ss(0.7).drawRect(0, 0, 145, 19);
        this.shape_group1.setTransform(30, 26);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(0, 19).lineTo(0, 62).lineTo(145, 62).lineTo(145, 19);
        this.shape_group2.setTransform(30, 26);

        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(40, 0).lineTo(40, 62);
        this.shape_group3.setTransform(60, 26);


        this.text_10 = new cjs.Text("tiotal", "16px 'Myriad Pro'");
        this.text_10.setTransform(50, 39);

        this.text_11 = new cjs.Text("ental", "16px 'Myriad Pro'");
        this.text_11.setTransform(117, 39);


        this.text_6 = new cjs.Text("2", "bold 60px 'UusiTekstausMajema'", "#706F7A");
        this.text_6.setTransform(50, 84);

        this.text_7 = new cjs.Text("0", "bold 60px 'UusiTekstausMajema'", "#706F7A");
        this.text_7.setTransform(117, 84);


        this.addChild(this.shape_115, this.shape_116, this.shape_100, this.shape_80,
            this.shape_81, this.shape_82, this.shape_83, this.shape_84, this.shape_85,
            this.shape_86, this.shape_87, this.shape_88, this.shape_89, this.shape_90, this.shape_91,
            this.shape_92, this.shape_93, this.shape_94, this.shape_95, this.shape_96, this.shape_97,
            this.shape_71, this.shape_72, this.shape_73, this.shape_74, this.shape_75, this.shape_76, this.shape_77,
            this.shape_78, this.shape_79, this.shape_group1, this.shape_group2, this.shape_group3, this.text_10, this.text_11, this.text_6, this.text_7);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 236.6);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text.setTransform(5, 0);

        this.text_1 = new cjs.Text(" Skriv talet.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 0);

        this.text_8 = new cjs.Text("nitton", "16px 'Myriad Pro'");
        this.text_8.setTransform(23, 33);

        this.text_9 = new cjs.Text("tjugo", "16px 'Myriad Pro'");
        this.text_9.setTransform(23, 98);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 0, 380, 135, 10);
        this.roundRect1.setTransform(0, 13);

        var arrxPos = ['26', '75', '125', '175', '225', '275', '325'];
        var arryPos = ['45', '109'];
        this.textbox_group1 = new cjs.Shape();
        for (var j = 0; j < arryPos.length; j++) {
            var yPos = parseInt(arryPos[j]);

            for (var i = 0; i < arrxPos.length; i++) {
                var xPos = parseInt(arrxPos[i]);

                for (var column = 0; column < 2; column++) {
                    var columnSpace = column;
                    this.textbox_group1.graphics.f('').s("#707070").ss(0.8).drawRect(xPos + (columnSpace * 21), yPos, 21, 25);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_2 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.setTransform(28, 68);

        this.text_3 = new cjs.Text("9", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(47, 68);

        this.text_4 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.setTransform(28, 132);

        this.text_5 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.setTransform(47, 132);


        this.addChild(this.roundRect1, this.shape_group10, this.text, this.text_1, this.textbox_group1, this.text_2, this.text_3, this.text_4, this.text_5,
            this.text_8, this.text_9, this.text_12,
            this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7,
            this.shape_group4, this.shape_group5, this.shape_group6, this.shape_group7, this.shape_group8, this.shape_group9, this.shape_group11,
            this.text_13, this.text_14, this.text_15, this.text_16, this.text_17, this.text_18, this.text_19, this.text_20, this.text_21, this.text_22);


        this.addChild(this.textbox_group1, this.shape_115, this.shape_116, this.shape_100, this.shape_80,
            this.shape_81, this.shape_82, this.shape_83, this.shape_84, this.shape_85,
            this.shape_86, this.shape_87, this.shape_88, this.shape_89, this.shape_90, this.shape_91,
            this.shape_92, this.shape_93, this.shape_94, this.shape_95, this.shape_96, this.shape_97,
            this.shape_71, this.shape_72, this.shape_73, this.shape_74, this.shape_75, this.shape_76, this.shape_77,
            this.shape_78, this.shape_79, this.shape_group1, this.shape_group2, this.shape_group3, this.text_10, this.text_11, this.text_6, this.text_7);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 430, 147);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Skriv talen som saknas på tallinjen.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 7);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 18.5, 517, 65, 10);
        this.roundRect1.setTransform(0, 0);


        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(17, 41).lineTo(460, 41);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(460, 41, 1.2, 1.2, 265);


        var ToBeAdded = [];
        var square = [0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

        for (var i = 6; i < 21; i++) {
            var columnSpace = i - 6;


            if (square[i] == 0) {

                var temp_label = new cjs.Text("" + i, "16px 'Myriad Pro'");
                temp_label.setTransform(26 + (columnSpace * 28), 63);
                ToBeAdded.push(temp_label);

            } else if (square[i] == 11 || square[i] == 12 || square[i] == 13 || square[i] == 14 || square[i] == 15 || square[i] == 16 || square[i] == 17 || square[i] == 18) {

                var temp_label = new cjs.Text("" + i, "16px 'Myriad Pro'");
                temp_label.setTransform(26 + (columnSpace * 28), 63);
                ToBeAdded.push(temp_label);

            } else if (square[i] == 9 || square[i] == 10) {

                var temp_label = new cjs.Shape();
                temp_label.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(0, 0, 24, 22);
                temp_label.setTransform(20 + (columnSpace * 28), 46);
                ToBeAdded.push(temp_label);

            } else if (square[i] == 19 || square[i] == 20) {

                var temp_label = new cjs.Shape();
                temp_label.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(0, 0, 24, 22);
                temp_label.setTransform(20 + (columnSpace * 28.2), 46);
                ToBeAdded.push(temp_label);

            } else {

                var temp_label = new cjs.Shape();
                temp_label.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(0, 0, 24, 22);
                temp_label.setTransform(20 + (columnSpace * 27.9), 46);
                ToBeAdded.push(temp_label);
            }


        }

        for (var i = 6; i < 21; i++) {
            var columnSpace = i - 6;
            if (i == 11 || i == 12 || i == 13 || i == 14 || i == 15 || i == 16 || i == 17 || i == 18) {
                columnSpace = columnSpace + 0.2;
            } else if (i == 9 || i == 10) {
                columnSpace = columnSpace + 0.1;
            } else if (i == 19 || i == 20) {
                columnSpace = columnSpace + 0.2;
            }

            this.temp_Line = new cjs.Shape();
            this.temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(30 + (columnSpace * 28), 32).lineTo(30 + (columnSpace * 28), 40);
            this.temp_Line.setTransform(0, 0);
            ToBeAdded.push(this.temp_Line);


        }

        this.addChild(this.text, this.text_1, this.roundRect1,
            this.textbox_group2, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.shape_1, this.instance)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 80);

    (lib.Symbol4 = function() {
        this.initialize();


        this.text = new cjs.Text(" Bind ihop talen 0 till 20.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(5, 12, 517, 144, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p15_2();
        this.instance.setTransform(5, 10, 0.47, 0.42);

        this.addChild(this.text, this.text_1, this.instance, this.roundRect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 514.3, 150);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 190, 1, 1, 0, 0, 0, 255.8, 0);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300, 366, 1, 1, 0, 0, 0, 255.8, 0);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(300, 480, 1, 1, 0, 0, 0, 255.8, 0);


        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
