(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p60_1.png",
            id: "p60_1"
        }, {
            src: "images/p60_2.png",
            id: "p60_2"
        }, {
            src: "images/p60_3.png",
            id: "p60_3"
        }, {
            src: "images/p60_4.png",
            id: "p60_4"
        }, {
            src: "images/p60_5.png",
            id: "p60_5"
        },{
            src: "images/p60_6.png",
            id: "p60_6"
        }]
    };

    (lib.p60_1 = function() {
        this.initialize(img.p60_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p60_2 = function() {
        this.initialize(img.p60_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p60_3 = function() {
        this.initialize(img.p60_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p60_4 = function() {
        this.initialize(img.p60_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p60_5 = function() {
        this.initialize(img.p60_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

       (lib.p60_6 = function() {
        this.initialize(img.p60_6);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Talföljder", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(220, 72);

        this.text_1 = new cjs.Text("20", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(42, 42);

        this.text_3 = new cjs.Text("kunna skriva regeln för enkla talföljder", "9px 'Myriad Pro'", "#FAAA33");
        this.text_3.setTransform(88, 658);

        this.text_4 = new cjs.Text("60", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(42, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(33.1, 660.8, 1.05, 1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(78, 649.7, 168, 12, 20);
        this.text_Rect.setTransform(0, 0);
     
        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text_1, this.text, this.instance_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance1 = new lib.p60_2();
        this.instance1.setTransform(110, 65, 0.46, 0.46);

        this.instance = new lib.p60_1();
        this.instance.setTransform(10, 54, 0.46, 0.46);

        this.text_1 = new cjs.Text("Skriv talen i talföljden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(18, 15);
        var ToBeAdded = [];
        var j = 1;

        for (var col = 0; col < 2; col++) {
            columnSpace = col
            var tempText = new cjs.Text(j, "36px 'UusiTekstausMajema'", "#6C7373");
            tempText.setTransform(112 + (columnSpace * 77), 137);
            ToBeAdded.push(tempText);
            j++
        }


        for (var col = 0; col < 5; col++) {
            var columnSpace = col;
            var tempText1 = new cjs.Text("+1", "16px 'MyriadPro'");
            tempText1.setTransform(148 + (columnSpace * 75), 52);
            ToBeAdded.push(tempText1)
        }

        //Draw harizantal doted lines loop       
        for (var col = 0; col < 72; col++) {
            var colSpace = col;
            var Line_1 = new cjs.Shape();
            Line_1.graphics.beginStroke("#F8AC23").setStrokeStyle(0.5).moveTo(0, 150).lineTo(4, 150);
            Line_1.setTransform(18 + (colSpace * 7), 0);
            ToBeAdded.push(Line_1);
        }


        // Harizantol Line Loop   
        this.hrLine_2 = new cjs.Shape();
        var ypos = 138

        for (var col = 0; col < 6; col++) {
            var columnSpace = col
            this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(95 + (75 * columnSpace), ypos).lineTo(144 + (75 * columnSpace), ypos);

        }
        this.hrLine_2.setTransform(0, 0);

        //Arrow Shape Loop
        this.shape_group2 = new cjs.Shape();
        this.hrLine_1 = new cjs.Shape();
        var arcYpos = 72.6
        var arrowYpos = 60

        for (var col = 0; col < 5; col++) {
            var colSpace = col;
            this.shape_group2.graphics.f("#FFFFFF").s("#000000").ss(0.8, 0, 0, 4).arc(155 + (colSpace * 75), arcYpos, 32, 4.1, 5.3)

            this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(173 + (colSpace * 75), arrowYpos).lineTo(173 + (colSpace * 75), (arrowYpos + 5)).lineTo(168 + (colSpace * 75), (arrowYpos + 5))

        }
        this.shape_group2.setTransform(0, 0, 1, 1.4);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.text_1, this.instance, this.instance1,  this.hrLine_1, this.hrLine_2, this.shape_group2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 150);


    (lib.Symbol3 = function() {
        this.initialize();
        this.text_1 = new cjs.Text("Skriv talen i talföljden. Skriv regeln.", "16px 'Myriad Pro'");
        this.text_1.setTransform(18, 15);

        this.instance = new lib.p60_6();
        this.instance.setTransform(10, 50, 0.46, 0.46);

        this.instance2=this.instance.clone(true)
        this.instance2.setTransform(10, 170, 0.46, 0.46);
        this.instance5=this.instance.clone(true)
        this.instance5.setTransform(10, 285, 0.46, 0.46);

        this.instance1 = new lib.p60_3();
        this.instance1.setTransform(175,33, 0.46, 0.46);

        this.instance3 = new lib.p60_4();
        this.instance3.setTransform(104, 150, 0.46, 0.46);

        this.instance4 = new lib.p60_5();
        this.instance4.setTransform(105, 278, 0.46, 0.46);

          var ToBeAdded = [];
        //Draw harizantal doted lines loop
        var tmpypos = 150;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 72; col++) {
                var colSpace = col;
                var Line_1 = new cjs.Shape();
                Line_1.graphics.beginStroke("#F8AC23").setStrokeStyle(0.5).moveTo(0, 145).lineTo(4, 145);
                if (row == 1) {
                    tmpypos = 120
                } else {
                    tmpypos = 150
                }
                Line_1.setTransform(18 + (colSpace * 7), 0 + (row * tmpypos));
                ToBeAdded.push(Line_1);
            }
        }

        // Harizantol Line Loop   
        this.hrLine_2 = new cjs.Shape();
        var ypos = 135
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 6; col++) {
                var columnSpace = col
                if (row == 1) {
                    ypos = 180
                } else if (row == 2) {
                    ypos = 225
                } else {
                    ypos = 135
                }
                this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(95 + (75 * columnSpace), ypos + (75 * row)).lineTo(144 + (75 * columnSpace), ypos + (75 * row));
            }
        }
        this.hrLine_2.setTransform(0, 0);

        //Arrow Shape Loop
        this.shape_group2 = new cjs.Shape();
        this.hrLine_1 = new cjs.Shape();
        var arcYpos = 60.6
        var arrowYpos = 43
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 5; col++) {
                var colSpace = col;
                if (row == 1) {
                    arcYpos = 75;
                    arrowYpos = 93
                } 
                else if (row == 2) {
                    arcYpos = 84;
                    arrowYpos = 136
                } 
                else {
                    arcYpos = 60.6;
                    arrowYpos = 43
                }
                this.shape_group2.graphics.f("#FFFFFF").s("#000000").ss(0.8, 0, 0, 4).arc(155 + (colSpace * 75), arcYpos + (row * 75), 32, 4.1, 5.3)

                this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(173 + (colSpace * 75), arrowYpos + (row * 75)).lineTo(173 + (colSpace * 75), (arrowYpos + 5) + (row * 75)).lineTo(168 + (colSpace * 75), (arrowYpos + 5) + (row * 75))

            }
        }
        this.shape_group2.setTransform(0, 0, 1, 1.4);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.text_1, this.instance, this.instance1, this.instance2, this.instance3,this.instance4,this.instance5, this.hrLine_1, this.hrLine_2, this.shape_group2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 350);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290.8, 278, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(290.8, 437, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
