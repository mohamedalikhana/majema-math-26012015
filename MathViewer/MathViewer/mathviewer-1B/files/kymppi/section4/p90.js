(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p90_1.png",
            id: "p90_1"
        }, {
            src: "images/p90_2.png",
            id: "p90_2"
        }]
    };

    (lib.p90_1 = function() {
        this.initialize(img.p90_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p90_2 = function() {
        this.initialize(img.p90_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("kunna mäta längd med klossar och rutor", "9px 'Myriad Pro'");
        this.text_1.setTransform(71, 658);

        this.text_2 = new cjs.Text("90", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(37.5, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.text_3 = new cjs.Text("Mäta längd", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_3.setTransform(95.5, 42);

        this.text_4 = new cjs.Text("31", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(46, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.Symbolimg = new lib.Symbolimg();
        this.Symbolimg.setTransform(65, 32);

        this.addChild(this.Symbolimg, this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbolimg = function() {
        this.initialize();

        this.instance = new lib.p90_1();
        this.instance.setTransform(0, 0, 0.47, 0.47);

        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);


    //Part 1
    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 0);

        this.text_1 = new cjs.Text(" Hur lång är leksaken?", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 363, 10);
        this.roundRect1.setTransform(10, 13);

        this.instance = new lib.p90_2();
        this.instance.setTransform(20, 19, 0.47, 0.47);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#74C9DD").s('#000000').drawRoundRect(0, 0, 160, 15, 0);
        this.Rect1.setTransform(45, 89);

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("#E78FA4").s('#000000').drawRoundRect(0, 0, 160, 15, 0);
        this.Rect2.setTransform(45, 111);

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("#74C9DD").s('#000000').drawRoundRect(0, 0, 203, 15, 0);
        this.Rect3.setTransform(298, 89);

        this.Rect4 = new cjs.Shape();
        this.Rect4.graphics.f("#E78FA4").s('#000000').drawRoundRect(0, 0, 203, 15, 0);
        this.Rect4.setTransform(298, 111);

        this.Rect5 = new cjs.Shape();
        this.Rect5.graphics.f("#74C9DD").s('#000000').drawRoundRect(0, 0, 410, 15, 0);
        this.Rect5.setTransform(61, 278);

        this.Rect6 = new cjs.Shape();
        this.Rect6.graphics.f("#E78FA4").s('#000000').drawRoundRect(0, 0, 410, 15, 0);
        this.Rect6.setTransform(61, 302);

        var vrlineArr = [];
        var vrlineX = [103, 345, 412, 64, 103, 142.2, 311.5, 344.8, 378.1, 412.2, 445.5, 143.5, 247, 349,
            93, 144, 195, 246, 297, 348, 399
        ];
        var vrlineY = [89, 89, 89, 111, 111, 111, 111, 111, 111, 111, 111, 278, 278, 278,
            302, 302, 302, 302, 302, 302, 302
        ];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(20, 0).lineTo(20, 15);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            vrlineArr.push(vrLine_1);
        }

        var TextArr = [];
        var TArr = ['blå klossar', 'rosa klossar', 'blå klossar', 'rosa klossar', 'blå klossar', 'rosa klossar'];
        var TextX = [80, 80, 350, 350,210,210];
        var TextY = [155, 176, 155, 176, 339, 360];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text(TArr[row], "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        var hrlineArr = [];
        var hrlineX = [46, 317, 46, 317, 177, 177];
        var hrlineY = [135, 135, 157, 157, 320, 341];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 20).lineTo(30, 20);
            hrLine_1.setTransform(hrlineX[row],hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.Rect1, this.Rect2, this.Rect3, this.Rect4, this.Rect5, this.Rect6);

        for (var i = 0; i < vrlineArr.length; i++) {
            this.addChild(vrlineArr[i]);
        }

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 513.3, 372);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287, 315, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
