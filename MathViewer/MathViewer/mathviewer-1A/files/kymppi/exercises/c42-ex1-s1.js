var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c42_ex1_s1_1.png",
            id: "c42_ex1_s1_1"
        }, {
            src: "images/c42_ex1_s1_2.png",
            id: "c42_ex1_s1_2"
        }]
    };



    (lib.c42_ex1_s1_1 = function() {
        this.initialize(img.c42_ex1_s1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c42_ex1_s1_2 = function() {
        this.initialize(img.c42_ex1_s1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Prisskillnad", "bold 24px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("42", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();
        this.image = new lib.c42_ex1_s1_1();
        this.image.setTransform(10, 40, 0.9, 0.9)

        this.image_cat = new lib.c42_ex1_s1_2();
        this.image_cat.setTransform(850, 50, 0.5, 0.5)
        this.image_cat.visible = false;

        this.answers = new lib.answers();
        // this.answers.text_4.visible = true;
        // this.answers.text_3.visible = true;
        // this.answers.text_2.visible = true;
        // this.answers.text_1.visible = true;
        this.answers.setTransform(10, 400, 5, 5, 0, 0, 0, 0, 0);


        this.instrTexts = [];
        this.instrTexts.push('Hur mycket mer kostar boken än nallen?');
        this.instrTexts.push('8 kr');
        this.instrTexts.push('3 kr');
        this.instrTexts.push('Subtrahera det lägsta\n\n priset från det högsta.');


        this.instrText1 = new cjs.Text(this.instrTexts[0], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(550 + 120, -40);

        this.instrText4 = new cjs.Text(this.instrTexts[3], "40px 'Myriad Pro'");
        this.instrText4.lineHeight = 19;
        this.instrText4.textAlign = 'center';
        this.instrText4.setTransform(1075, 115);
        this.instrText4.visible = false;

        this.instrText2 = new cjs.Text(this.instrTexts[1], "50px 'Myriad Pro'");
        this.instrText2.lineHeight = 19;
        this.instrText2.textAlign = 'center';
        this.instrText2.setTransform(230, 82);
        this.instrText2.skewX = -18;
        this.instrText2.skewY = -18;

        this.instrText3 = new cjs.Text(this.instrTexts[2], "50px 'Myriad Pro'");
        this.instrText3.lineHeight = 19;
        this.instrText3.textAlign = 'center';
        this.instrText3.setTransform(678, 112);
        this.instrText3.skewX = 20;
        this.instrText3.skewY = 20;

        this.addChild(this.image, this.image_cat, this.answers, this.instrText1, this.instrText2, this.instrText3, this.instrText4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText4.visible = true;
        this.stage0.image_cat.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.image_cat,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.instrText4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText4.visible = true;
        this.stage0.image_cat.visible = true;

        this.answers = this.stage0.answers;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText4.visible = true;
        this.stage0.image_cat.visible = true;

        this.answers = this.stage0.answers;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("8 kr", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(65, 0);

        this.text_2 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(100, 0);

        this.text_3 = new cjs.Text("3 kr", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(120, 0);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(150, 0);

        this.text_5 = new cjs.Text("5 kr", "16px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(170, 0);

        this.addChild(this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
