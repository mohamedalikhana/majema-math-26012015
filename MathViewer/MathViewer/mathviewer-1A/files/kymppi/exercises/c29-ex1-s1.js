var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }, {
            src: "../section1/images/c2_ex1_3.png",
            id: "c2_ex1_3"
        }]
    };


    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c2_ex1_3 = function() {
        this.initialize(img.c2_ex1_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.Stage1 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Addera bollarna.", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(570 + 80, -40);

        this.addBall = new lib.insertBall_1();
        this.addBall.setTransform(320, 0, 1.8, 1.8);

        this.addChild(this.addBall, this.questionText);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Addera bollarna.", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(570 + 80, -40);

        this.addBall = new lib.insertBall_1();
        this.addBall.setTransform(320, 0, 1.8, 1.8);

        this.answers = new lib.answers();
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(150, 30, 4, 4, 0, 0, 0, 0, 0);

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_6,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 3000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.addBall, this.questionText, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Addera bollarna.", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(570 + 80, -40);

        this.addBall = new lib.insertBall_1();
        this.addBall.setTransform(320, 0, 1.8, 1.8);

        this.answers = new lib.answers();
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(150, 30, 4, 4, 0, 0, 0, 0, 0);

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_7,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.addBall, this.questionText, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Addera tre tal", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("29", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.insertBall_1 = function(ball, count) {
        this.initialize();
        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();
        this.greenBall = new lib.c2_ex1_3();
        this.redBalls = new cjs.Container();
        this.blueBalls = new cjs.Container();
        this.greenBalls = new cjs.Container();
        var addImage = [];

        var redBallX = ['20'];
        var redBallY = ['109'];
        for (var j = 0; j < redBallX.length; j++) {
            var redBall = this.redBall.clone(true);
            redBall.setTransform(redBallX[j], redBallY[j], 0.4, 0.4);
            this.redBalls.addChild(redBall);
        }

        var blueBallX = ['142', '110', '174'];
        var blueBallY = ['40', '109', '109'];
        for (var i = 0; i < 3; i++) {
            var blueBall = this.blueBall.clone(true);
            blueBall.setTransform(blueBallX[i], blueBallY[i], 0.4, 0.4);
            this.blueBalls.addChild(blueBall);
        }


        var greenBallX = ['295', '263', '327'];
        var greenBallY = ['40', '109', '109'];
        for (var i = 0; i < 3; i++) {
            var greenBall = this.greenBall.clone(true);
            greenBall.setTransform(greenBallX[i], greenBallY[i], 0.4, 0.4);
            this.greenBalls.addChild(greenBall);
        }
        this.addChild(this.blueBalls, this.greenBalls, this.redBalls);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);




    (lib.rectangles = function() {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();

        for (var j = 0; j < 6; j++) {
            boxes.graphics.f('#ffffff').s('#000000').ss(0.7).moveTo(0 + (30 * j), 0).lineTo(0 + (30 * j), 50);
        };
        for (var i = 0; i < 3; i++) {
            boxes.graphics.f('#ffffff').s('#000000').ss(0.7).moveTo(0, 0 + (25 * i)).lineTo(150, 0 + (25 * i));
        };

        boxes.setTransform(0, 0);
        this.addChild(boxes);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(85, 80);

        this.text_2 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(97, 80);

        this.text_3 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(109, 80);

        this.text_4 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(121, 80);

        this.text_5 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(133, 80);

        this.text_6 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_6.visible = false;
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(145, 80);

        this.text_7 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_7.visible = false;
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(157, 80);

        this.addChild(this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
