var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };



    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Kommutativa lagen i addition", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("26", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Addera", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(0, 0);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();
        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();

        this.roundRect1 = new lib.roundRect(this.redBall, 4);
        this.roundRect1.setTransform(320, 80, 1.5, 1.5);

        this.roundRect2 = new lib.roundRect(this.blueBall, 1);
        this.roundRect2.setTransform(730, 80, 1.5, 1.5);

        this.questionText = new cjs.Text("Addera.", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(570 + 80, -40);


        this.addChild(this.roundRect1, this.roundRect2, this.questionText, this.hintText1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();

        this.answers = new lib.answers();
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(280, 130, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();

        this.answers = new lib.answers();
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(280, 130, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();

        this.answers = new lib.answers();
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(280, 130, 3, 3, 0, 0, 0, 0, 0);
        this.tweens = [];
        this.tweens.push({
            ref: this.stage1.roundRect1,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 320,
                y: 80
            },
            positionTo: {
                x: 730,
                y: 80
            },
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage1.roundRect2,
            alphaFrom: 1,
            alphaTo: 1,

            positionFrom: {
                x: 730,
                y: 80
            },
            positionTo: {
                x: 320,
                y: 80
            },
            wait: 500,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage5 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();
        this.stage1.roundRect1.setTransform(730, 80, 1.5, 1.5);
        this.stage1.roundRect2.setTransform(320, 80, 1.5, 1.5);
        this.answers = new lib.answers();

        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(280, 130, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_6,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_7,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_8,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_9,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage6 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();
        this.stage1.roundRect1.setTransform(730, 80, 1.5, 1.5);
        this.stage1.roundRect2.setTransform(320, 80, 1.5, 1.5);

        this.answers = new lib.answers();
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(280, 130, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_10,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.Stage7 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();
        this.stage1.roundRect1.setTransform(730, 80, 1.5, 1.5);
        this.stage1.roundRect2.setTransform(320, 80, 1.5, 1.5);
        this.stage1.questionText.text = 'Additionens resultat ändras\n\ninte även om talens ordning ändras.';

        this.answers = new lib.answers();
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(280, 130, 3, 3, 0, 0, 0, 0, 0);

        // this.shadowFrom = new cjs.Shadow("#ff00ff", 0, 0, 0);
        // this.shadowTo = new cjs.Shadow("#ff00ff", 0, 0, 2);
        this.tweens = [];

        // this.answers.text_10.shadow = new cjs.Shadow('red', 0, 0, 0)
        this.answers.text_10.color = "red";
        this.tweens.push({
            ref: this.answers.text_10,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 500,
            // shadow: {
            //     blurFrom: 0,
            //     blurTo: 40
            // },
            alphaTimeout: 2000
        });

        // this.answers.text_5.shadow = new cjs.Shadow('red', 0, 0, 0)
        this.answers.text_5.color = "red";
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 500,
            // shadow: {
            //     blurFrom: 0,
            //     blurTo: 40
            // },
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(95, 80);

        this.text_2 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(107, 80);

        this.text_3 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(119, 80);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(131, 80);

        this.text_5 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(143, 80);

        this.text_6 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_6.visible = false;
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(95, 100);

        this.text_7 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_7.visible = false;
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(107, 100);

        this.text_8 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_8.visible = false;
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(119, 100);

        this.text_9 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_9.visible = false;
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(131, 100);

        this.text_10 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_10.visible = false;
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(143, 100);

        this.addChild(this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    (lib.roundRect = function(ball, count) {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#000000').ss(0.8).drawRoundRect(0, 0, 175, 175, 8);
        this.roundRect1.setTransform(0, 0);
        this.addChild(this.roundRect1);
        switch (count) {
            case 4:
                var redBall1 = ball.clone(true);
                redBall1.setTransform(16, 20, 0.5, 0.5);
                var redBall2 = ball.clone(true);
                redBall2.setTransform(16, 100, 0.5, 0.5);
                var redBall3 = ball.clone(true);
                redBall3.setTransform(96, 20, 0.5, 0.5);
                var redBall4 = ball.clone(true);
                redBall4.setTransform(96, 100, 0.5, 0.5);
                this.addChild(redBall1, redBall2, redBall3, redBall4);
                break;
            case 1:
                var blueBall1 = ball.clone(true);
                blueBall1.setTransform(16, 54, 0.5, 0.5);
                var blueBall2 = ball.clone(true);
                blueBall2.setTransform(96, 54, 0.5, 0.5);
                this.addChild(blueBall1, blueBall2);

                break;
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage6 = new lib.Stage6();
        this.stage6.visible = false;
        this.stage6.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage7 = new lib.Stage7();
        this.stage7.visible = false;
        this.stage7.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
