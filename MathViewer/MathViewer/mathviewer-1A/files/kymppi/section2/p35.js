(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p35_1.png",
            id: "p35_1"
        }, {
            src: "images/p35_2.png",
            id: "p35_2"
        }]
    };

    (lib.p35_1 = function() {
        this.initialize(img.p35_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p35_2 = function() {
        this.initialize(img.p35_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("35", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);


        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#949599").ss(0.7).drawRect(480 + (i * 32), 23, 27, 27);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.top_textbox_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p35_1();
        this.instance.setTransform(-1, 20, 0.503, 0.505);
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s('#7d7d7d').drawRoundRect(0, 20, 1035 * 0.5, 553 * 0.5, 10);

        this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, -3);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, -3);
        this.textbox_group1 = new cjs.Shape();
        this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.7).drawRect(45, 260, 20, 21).drawRect(145, 260, 20, 21).drawRect(245, 260, 20, 21).drawRect(345, 260, 20, 21).drawRect(438, 260, 20, 21);
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1,this.instance, this.text, this.text_1, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 330);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p35_2();
        this.instance.setTransform(0, 20, 0.5, 0.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1037 * 0.5) - 10) * 0.5, 492 * 0.5, 10);
        this.roundRect1.setTransform(0, 20);

        this.roundRect2 =this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1037 * 0.5) + 10) * 0.5, 20);


        this.crossLinesSet1 = new cjs.Shape();
        this.crossLinesSet1.graphics.f("#ffffff").s('#949599').moveTo(10, 45).lineTo(((1037 * 0.5) - 20) * 0.5, 45).moveTo(10, 115).lineTo(((1037 * 0.5) - 20) * 0.5, 115).moveTo(10, 185).lineTo(((1037 * 0.5) - 20) * 0.5, 185).moveTo(195,25).lineTo(195, 260).moveTo(140,25).lineTo(140, 260);
        this.crossLinesSet1.setTransform(0, 0);

        this.crossLinesSet2 = new cjs.Shape();
        this.crossLinesSet2.graphics.f("#ffffff").s('#949599').moveTo(10, 45).lineTo(((1037 * 0.5) - 20) * 0.5, 45).moveTo(10, 115).lineTo(((1037 * 0.5) - 20) * 0.5, 115).moveTo(10, 185).lineTo(((1037 * 0.5) - 20) * 0.5, 185).moveTo(185,25).lineTo(185, 260).moveTo(130,25).lineTo(130, 260);
        
        this.crossLinesSet2.setTransform(((1037 * 0.5) + 10) * 0.5, 0);


        this.text = new cjs.Text("Hur många ser du i bilden?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, -4);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, -4);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            if (column > 1) {
                columnSpace = column + 3.1;
            }
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(160 + (columnSpace * 50), 70 + (row * 68), 20, 21);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.crossLinesSet1, this.crossLinesSet2, this.instance, this.text, this.text_1, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 320);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305.3, 408.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(305.3, 104.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
