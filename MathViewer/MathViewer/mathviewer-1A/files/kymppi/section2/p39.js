(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p39_1.png",
            id: "p39_1"
        }, {
            src: "images/p39_2.png",
            id: "p39_2"
        }]
    };

    // symbols:

    (lib.p39_1 = function() {
        this.initialize(img.p39_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p39_2 = function() {
        this.initialize(img.p39_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageNoRight = new cjs.Text("39", "12px 'Myriad Pro'", "#FFFFFF");
        this.pageNoRight.lineHeight = 18;
        this.pageNoRight.setTransform(555, 648);
        this.pageNoRightBackground = new cjs.Shape();
        this.pageNoRightBackground.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.pageNoRightBackground.setTransform(579, 660.8);

        this.pageTitle = new cjs.Text("Vi lär oss addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 19);

        this.chapterNumber = new cjs.Text("12", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(50.7, 15.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoRightBackground, this.pageNoRight);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p39_1();
        this.instance.setTransform(0, 26, 0.49, 0.49);


        this.text = new cjs.Text("Hur många?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 1);


        this.textBoxGroup1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            var space = 128;
            if (columnSpace > 1) {
                columnSpace = columnSpace - 0.03;
            }
            for (var row = 0; row < 2; row++) {
                this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(40 + (columnSpace * space), 37 + (row * 140), 20, 21);
                //console.log(150 + (columnSpace * 150), 40 + (row * 30))
            }
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.label1 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label1.lineHeight = 27;
        this.label1.setTransform(70, 137);
        this.label2 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label2.lineHeight = 27;
        this.label2.setTransform(70, 277);


        this.label3 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label3.lineHeight = 27;
        this.label3.setTransform(320, 137);
        this.label4 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label4.lineHeight = 27;
        this.label4.setTransform(320, 277);

        this.textBoxGroup2 = new cjs.Shape();
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(160, 132, 20, 21).drawRect(160, 272, 20, 21);
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(410, 132, 20, 21).drawRect(410, 272, 20, 21);
        this.textBoxGroup2.setTransform(0, 0);
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1038 * 0.5) - 10) * 0.49, ((568 * 0.5) - 2) * 0.49, 10);
        this.roundRect1.setTransform(3, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1038 * 0.5) + 10) * 0.49, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 170);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1038 * 0.5) + 10) * 0.49, 170);



        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.text, this.text_1, this.textBoxGroup1, this.label1, this.label2, this.label3, this.label4, this.textBoxGroup2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 370);


    (lib.Symbol3 = function() {
        this.initialize();
        this.instance = new lib.p39_2();
        this.instance.setTransform(0, 26, 0.49, 0.49);

        this.textBoxGroup1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            var space = 128;
            if (columnSpace > 1) {
                columnSpace = columnSpace - 0.03;
            }
            for (var row = 0; row < 2; row++) {
                this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(40 + (columnSpace * space), 37 + (row * 140), 20, 21);
                //console.log(150 + (columnSpace * 150), 40 + (row * 30))
            }
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.label1 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label1.lineHeight = 27;
        this.label1.setTransform(70, 137);
        this.label2 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label2.lineHeight = 27;
        this.label2.setTransform(70, 277);


        this.label3 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label3.lineHeight = 27;
        this.label3.setTransform(320, 137);
        this.label4 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label4.lineHeight = 27;
        this.label4.setTransform(320, 277);

        this.textBoxGroup2 = new cjs.Shape();
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(160, 132, 20, 21).drawRect(160, 272, 20, 21);
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(410, 132, 20, 21).drawRect(410, 272, 20, 21);
        this.textBoxGroup2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1038 * 0.5) - 10) * 0.49, ((568 * 0.5) - 2) * 0.49, 10);
        this.roundRect1.setTransform(3, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1038 * 0.5) + 10) * 0.49, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 170);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1038 * 0.5) + 10) * 0.49, 170);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.textBoxGroup1, this.label1, this.label2, this.label3, this.label4, this.textBoxGroup2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 300);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(315.3, 365.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(315.3, 91, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
