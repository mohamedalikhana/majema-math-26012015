(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p55_1.png",
            id: "p55_1"
        }]
    };

    // symbols:

    (lib.p55_1 = function() {
        this.initialize(img.p55_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();

        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(470 + (i * 32), 37, 27, 27);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageNoRight = new cjs.Text("55", "12px 'Myriad Pro'", "#FFFFFF");
        this.pageNoRight.lineHeight = 18;
        this.pageNoRight.setTransform(555, 648);
        this.pageNoRightBackground = new cjs.Shape();
        this.pageNoRightBackground.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.pageNoRightBackground.setTransform(579, 660.8);

        this.pageTitle = new cjs.Text("Vi skriver addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 19);

        this.chapterNumber = new cjs.Text("13", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(50.7, 15.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoRightBackground, this.pageNoRight, this.top_textbox_group);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p55_1();
        this.instance.setTransform(0, 26, 0.37, 0.37);
        this.text = new cjs.Text("Hur många finns kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(23, 6);
        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(4, 6);

        this.textBoxGroup1 = new cjs.Shape();

        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 116, 20, 22.5);
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 116, 20, 22.5);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 239, 20, 22.5);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 239, 20, 22.5);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 362, 20, 22.5);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 362, 20, 22.5);
        }
        this.textBoxGroup6.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((1394 * 0.5) - 10) * 0.365, ((1004 * (1 / 3)) - 16) * 0.365, 10);
        this.roundRect1.setTransform(3, 31);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1394 * 0.5) + 8) * 0.37, 31);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 154);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1394 * 0.5) + 8) * 0.37, 154);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(3, 277);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((1394 * 0.5) + 8) * 0.37, 277);

        var textArr = [];

        this.text_3 = new cjs.Text("Manuel tar 0.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(81, 36);
        this.text_4 = new cjs.Text("Manuel tar 2.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(341, 36);
        this.text_5 = new cjs.Text("Manuel tar 3.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(81, 160);
        this.text_6 = new cjs.Text("Manuel tar 4.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(341, 160);
        this.text_7 = new cjs.Text("Manuel tar 3.", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(81, 282);
        this.text_8 = new cjs.Text("Manuel tar 4.", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(341, 282);

        textArr.push(this.text_3);
        textArr.push(this.text_4);
        textArr.push(this.text_5);
        textArr.push(this.text_6);
        textArr.push(this.text_7);
        textArr.push(this.text_8);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5,
            this.roundRect6, this.instance, this.textBoxGroup1, this.textBoxGroup2, this.textBoxGroup3,
            this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6, this.text, this.text_1);

        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);


    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(26, 2);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(7, 2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(6.5, 25, 508.5, 152, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 5; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(111 + (columnSpace * 150), 33 + (row * 28), 20, 22.5);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("1  –  1  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(46, 36);
        this.label2 = new cjs.Text("2  –  2  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(46, 65);
        this.label3 = new cjs.Text("3  –  3  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(46, 91);
        this.label4 = new cjs.Text("1  –  0  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(46, 121);
        this.label5 = new cjs.Text("2  –  0  =  ", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(46, 148.5);

        this.label6 = new cjs.Text("3  –  1  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(197, 36);
        this.label7 = new cjs.Text("3  –  2  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(197, 65);
        this.label8 = new cjs.Text("4  –  2  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(197, 91);
        this.label9 = new cjs.Text("4  –  3  =  ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(197, 121);
        this.label10 = new cjs.Text("4  –  4  =  ", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(197, 148.5);

        this.label11 = new cjs.Text("5  –  1  =  ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(346, 36);
        this.label12 = new cjs.Text("5  –  3  =  ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(346, 65);
        this.label13 = new cjs.Text("5  –  2  =  ", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(346, 91);
        this.label14 = new cjs.Text("5  –  4  =  ", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(346, 121);
        this.label15 = new cjs.Text("5  –  5  =  ", "16px 'Myriad Pro'");
        this.label15.lineHeight = 19;
        this.label15.setTransform(346, 148.5);
        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(310, 110, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(308, 497, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
