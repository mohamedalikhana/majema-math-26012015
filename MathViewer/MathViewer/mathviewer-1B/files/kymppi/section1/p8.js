(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p8_1.png",
            id: "p8_1"
        }, {
            src: "images/p8_2.png",
            id: "p8_2"
        }]
    };

    // symbols:
    (lib.p8_1 = function() {
        this.initialize(img.p8_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p8_2 = function() {
        this.initialize(img.p8_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1

        this.text_2 = new cjs.Text("8", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(40, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);


        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Ringa in 2 tal i varje rad som tillsammans är 10.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p8_1();
        this.instance.setTransform(12, 8, 0.51, 0.51);

      
        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 0 && row > 3) {
                    continue;
                } else if (column == 1 && row > 4) {
                    continue;
                } else if (column == 2 && row == 3) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.65;
                } else if (column == 2) {
                    columnSpace = 1.36;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#90BD22").ss(0.7).drawRect(70 + (columnSpace * 258), 68 + (row * 28), 155, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);


        var numbArr = [];
        var numbr = [3, 6, 5, 2, 5, 1, 2, 7, 5, 4, 3, 1, 1, 2, 4, 7, 5, 9, 2, 3, 5, 6, 1, 7];
        var numbX = [78, 103, 128, 153, 178, 203, 78, 103, 128, 153, 178, 203, 78, 103, 128, 153, 178, 203, 78, 103, 128, 153, 178, 203];
        var numbY = [85, 85, 85, 85, 85, 85, 112, 112, 112, 112, 112, 112, 140, 140, 140, 140, 140, 140, 167, 167, 167, 167, 167, 167];

        for (var j = 0; j < numbX.length; j++) {
            var numb_1 = null;
            numb_1 = new cjs.Text(numbr[j], "16px 'Myriad Pro'");
            numb_1.setTransform(numbX[j], numbY[j]);
            numbArr.push(numb_1);
        }

        this.box2 = new lib.Symbol3();
        this.box2.setTransform(245, 0);

        this.addChild(this.text, this.text_1, this.instance, this.instance2, this.textbox_group1,this.box2);
        for (var i = 0; i < numbArr.length; i++) {
            this.addChild(numbArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 180);

    (lib.Symbol3 = function() {
        this.initialize();

        // this.instance2 = new lib.p8_1();
        // this.instance2.setTransform(260, 8, 0.47, 0.47);

        this.instance = new lib.p8_1();
        this.instance.setTransform(12, 8, 0.51, 0.51);


        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 0 && row > 3) {
                    continue;
                } else if (column == 1 && row > 4) {
                    continue;
                } else if (column == 2 && row == 3) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.65;
                } else if (column == 2) {
                    columnSpace = 1.36;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#90BD22").ss(0.7).drawRect(70 + (columnSpace * 258), 68 + (row * 28), 155, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var numbArr = [];
        var numbr = [5, 6, 8, 7, 4, 1, 9, 4, 2, 3, 8, 5, 5, 6, 9, 7, 1, 2, 3, 9, 8, 6, 5, 2];
        var numbX = [78, 103, 128, 153, 178, 203, 78, 103, 128, 153, 178, 203, 78, 103, 128, 153, 178, 203, 78, 103, 128, 153, 178, 203];
        var numbY = [85, 85, 85, 85, 85, 85, 112, 112, 112, 112, 112, 112, 140, 140, 140, 140, 140, 140, 167, 167, 167, 167, 167, 167];

        for (var j = 0; j < numbX.length; j++) {
            var numb_1 = null;
            numb_1 = new cjs.Text(numbr[j], "16px 'Myriad Pro'");
            numb_1.setTransform(numbX[j], numbY[j]);
            numbArr.push(numb_1);
        }

        this.addChild(this.instance, this.textbox_group1);
        for (var i = 0; i < numbArr.length; i++) {
            this.addChild(numbArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text(" Räkna och hitta bokstäverna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 10);

        this.text_1 = new cjs.Text("7.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(0, 10);

        this.instance = new lib.p8_2();
        this.instance.setTransform(345, 212.5, 0.60, 0.605);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 517, 285, 10);
        this.roundRect1.setTransform(0, 25);

        //column-1
        this.label1 = new cjs.Text("2  +  8  =", "16px 'Myriad Pro'");
        this.label1.setTransform(33, 57);
        this.label2 = new cjs.Text("10  +  1  =", "16px 'Myriad Pro'");
        this.label2.setTransform(25, 83);
        this.label3 = new cjs.Text("10  +  2  =", "16px 'Myriad Pro'");
        this.label3.setTransform(25, 109);
        this.label4 = new cjs.Text("11  +  1  =", "16px 'Myriad Pro'");
        this.label4.setTransform(25, 134);

        //column-2
        this.label5 = new cjs.Text("10  –  5  =", "16px 'Myriad Pro'");
        this.label5.setTransform(194, 57);
        this.label6 = new cjs.Text("9  –  3  =", "16px 'Myriad Pro'");
        this.label6.setTransform(202, 83);
        this.label7 = new cjs.Text("10  –  7  =", "16px 'Myriad Pro'");
        this.label7.setTransform(194, 109);
        this.label8 = new cjs.Text("10  –  2  =", "16px 'Myriad Pro'");
        this.label8.setTransform(194, 161);
        this.label9 = new cjs.Text("9  –  5  =", "16px 'Myriad Pro'");
        this.label9.setTransform(202, 187);

        //column-3
        this.label10 = new cjs.Text("8  –  6  =", "16px 'Myriad Pro'");
        this.label10.setTransform(366.3, 57);
        this.label11 = new cjs.Text("6  +  2  =", "16px 'Myriad Pro'");
        this.label11.setTransform(365, 83);
        this.label12 = new cjs.Text("10  –  3  =", "16px 'Myriad Pro'");
        this.label12.setTransform(358, 109);
        this.label13 = new cjs.Text("12  –  0  =", "16px 'Myriad Pro'");
        this.label13.setTransform(358, 134);
        this.label14 = new cjs.Text("9  –  8  =", "16px 'Myriad Pro'");
        this.label14.setTransform(366.3, 161);
        this.label15 = new cjs.Text("4  +  5  =", "16px 'Myriad Pro'");
        this.label15.setTransform(365, 187);


        var textbox_group1 = new cjs.Shape();

        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (column == 0 && row > 3) {
                    continue;
                } else if (column == 1 && row == 3) {
                    continue;
                }
                

                textbox_group1.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(135 + (columnSpace * 163), 40 + (row * 27), 20, 22);
            }
        }
         textbox_group1.setTransform(0, 0);


        this.text_2 = new cjs.Text("10","36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.setTransform(95, 61);

        this.text_3 = new cjs.Text("B","30px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(137, 62.5);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 1; column < 13; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(2 + (columnSpace * 28.5), 245, 20, 22);
        }
        this.textbox_group2.setTransform(0, 0);


        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(95, 61).lineTo(131, 61).moveTo(95, 88.5).lineTo(131, 88.5).moveTo(95, 
            114.5).lineTo(131, 114.5).moveTo(95, 141).lineTo(131, 141);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(262, 61).lineTo(294, 61).moveTo(262, 88.5).lineTo(294, 88.5).moveTo(262, 114.5).lineTo(294, 114.5).moveTo(262, 167).lineTo(294, 167).moveTo(262, 194).lineTo(294, 194);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(425, 61).lineTo(457, 61).moveTo(425, 88.5).lineTo(457, 88.5).moveTo(425, 114.5).lineTo(457, 114.5).moveTo(425, 141.5).lineTo(457, 141.5).moveTo(425, 169).lineTo(457, 169).moveTo(425, 195).lineTo(457, 195);
        this.hrLine_3.setTransform(0, 0);


        var arrText = ['A', 'L', 'R', 'N', 'H', 'A', 'K', 'E', 'K', 'B', 'O', 'S'];

        var ToBeAdded = [];

        for (var i = 0; i < arrText.length; i++) {
            var columnSpace = i;
            var text = arrText[i];

            if (i == 2) {
                columnSpace = 2.109;
            } else if (i == 3) {
                columnSpace = 3.05;
            } else if (i == 4) {
                columnSpace = 4.08;
            } else if (i == 5) {
                columnSpace = 5.1;
            } else if (i == 6) {
                columnSpace = 6.1;
            } else if (i == 7) {
                columnSpace = 7.15;
            } else if (i == 8) {
                columnSpace = 8.17;
            } else if (i == 9) {
                columnSpace = 9.18;
            } else if (i == 10) {
                columnSpace = 10.19;
            } else if (i == 11) {
                columnSpace = 11.25;
            }

            this.temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            this.temp_label.setTransform(35 + (columnSpace * 28), 261.5);
            ToBeAdded.push(this.temp_label);
        }

        for (var i = 1; i < 13; i++) {
            var columnSpace = i;
            var text = i.toString();

            if (i == 2) {
                columnSpace = 1.87;
            } else if (i == 3) {
                columnSpace = 2.78;
            } else if (i == 4) {
                columnSpace = 3.75;
            } else if (i == 5) {
                columnSpace = 4.66;
            } else if (i == 6) {
                columnSpace = 5.59;
            } else if (i == 7) {
                columnSpace = 6.49;
            } else if (i == 8) {
                columnSpace = 7.35;
            } else if (i == 9) {
                columnSpace = 8.34;
            } else if (i == 10) {
                columnSpace = 9.14;
            } else if (i == 11) {
                columnSpace = 10.07;
            } else if (i == 12) {
                columnSpace = 10.87;
            }
            this.temp_label = new cjs.Text(text, "14px 'Myriad Pro'");
            this.temp_label.setTransform(5 + (columnSpace * 31), 285);
            ToBeAdded.push(this.temp_label);
        }


        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2,this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4);
        this.addChild(this.instance, textbox_group1, this.textbox_group2, this.shape_1)
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7);
        this.addChild(this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14);
        this.addChild(this.label15, this.label16, this.label17, this.label18, this.label19, this.label20, this.label21,this.text_3);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296, 120, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol4();
        this.v2.setTransform(296, 380, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
