var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }, {
            src: "../viovar/images/bat.png",
            id: "bat"
        }]
    };

    lib.init_Magic_Wand();

    var iconProperties = {
        x: 850 + 350,
        y: 350 - 240,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 150,
        wrongY: 345 - 15
    };

    var objects = [];

    (lib.bat = function() {
        this.initialize(img.bat);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.tick = function() {
        this.initialize(img.tick);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);

    // (lib.tick = function() {
    //     this.initialize();
    //     var tickMark = new createjs.Shape();
    //     tickMark.graphics.f('#61df7f').ss(4).moveTo(77, 5).quadraticCurveTo(82, 10, 84, 10).quadraticCurveTo(78, 5, 25.79, 147.32).quadraticCurveTo(12, 152, 10.5, 152.84).quadraticCurveTo(12, 152, 77, 5).moveTo(15.26, 66.03).drawCircle(15.26, 66.03, 10).moveTo(72.89, 85).drawCircle(72.89, 85, 10);
    //     tickMark.setTransform(0, 0, 2, 2);
    //     tickMark.alpha = 1;
    //     this.addChild(tickMark);
    // }).prototype = p = new cjs.Container();

    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    // code for drag & drop starts here

    var dropBoxSpacing = {
        width: 0,
        height: 0
    };


    var cellSpacing = {
        width: 4,
        height: 6,
        cellSectionSpace: 16
    };

    var dropBox = {
        width: 140,
        height: 150
    };

    var draggableNumber = {
        x: 300 - 150,
        y: 330,
        width: 140,
        height: 150
    };
    var draggableNumberSpacing = {
        width: 10,
        height: 0
    };

    var isDragging = false;
    var draggingNumber = null;

    function dragging(event) {
        if (isDragging) {
            var isOver = false,
                targets = stage.getObjectsUnderPoint(stage.mouseX, stage.mouseY);
            if (stage.draggingShape) {
                stage.draggingShape.children[0].shadow = new createjs.Shadow('#7d7d7d', 0, 0.5, 4);

                // stage.draggingShape.x = stage.mouseX - currentStage.x - (draggableNumber.width / 2), stage.draggingShape.y = stage.mouseY - (draggableNumber.height / 2) - currentStage.y;
                // stage.draggingShape.x = stage.mouseX - currentStage.x,
                //     stage.draggingShape.y = stage.mouseY;
                var pt = stage.globalToLocal(stage.mouseX, stage.mouseY);
                stage.draggingShape.x = pt.x - (draggableNumber.width / 2),
                    stage.draggingShape.y = pt.y - (draggableNumber.height / 2);
                stage.update();
            }
        }

    }

    function handlePress(evt) {
        if (evt.currentTarget && evt.currentTarget.value >= 0) {
            var dragger = new createjs.Container();
            dragger.value = evt.currentTarget.value;
            var tempCell = new createjs.Shape()
                //tempCell.graphics.s('#7d7d7d').beginLinearGradientFill(["#fff880", "#FFFFFF"], [0, 1], 0, 10, 0, 80);
            tempCell.graphics.drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);
            var tempCellText = new createjs.Text(dragger.value, "bold 150px 'Myriad Pro'", "#000000");
            tempCellText.textAlign = "center";

            tempCellText.x = draggableNumber.width / 2;
            tempCellText.y = draggableNumber.height / 4;

            dragger.addChild(tempCell);
            dragger.addChild(tempCellText);

            dragger.scaleX = dragger.scaleY = currentStage.scaleX;

            stage.addChild(dragger);
            stage.draggingShape = dragger;
            // stage.draggingShape.x = stage.mouseX - (draggableNumber.width / 2), stage.draggingShape.y = stage.mouseY - (draggableNumber.height / 2);
            var pt = stage.globalToLocal(stage.mouseX, stage.mouseY);
            stage.draggingShape.x = pt.x - (draggableNumber.width / 2),
                stage.draggingShape.y = pt.y - (draggableNumber.height / 2);
            isDragging = true;
            stage.addEventListener("stagemousemove", dragging);
            stage.addEventListener("stagemouseup", dropping);

        }
        stage.update();

    }

    function handleOver(evt) {
        if (evt.currentTarget && evt.currentTarget.value >= 0) {
            var dragger = evt.currentTarget;
            dragger.value = evt.currentTarget.value;

            dragger.children[0].graphics.clear().s('#7d7d7d').beginLinearGradientFill(["#fff880", "#FFFFFF"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);

        }

    }

    function handleOut(evt) {
        if (evt.currentTarget && evt.currentTarget.value >= 0) {
            var dragger = evt.currentTarget;
            dragger.value = evt.currentTarget.value;

            dragger.children[0].graphics.clear().s('#7d7d7d').beginLinearGradientFill(["#FFFFFF", "#fff880"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);

        }

    }

    function dropping(event) {

        if (isDragging) {
            isDragging = false;
            var dropArea = null;
            var dropAreaText = currentStage.dropBoxText1;
            var boxNumber = 0;
            // var pt1 = currentStage.dropBox1.globalToLocal(stage.mouseX, stage.mouseY);
            // var pt2 = currentStage.dropBox2.globalToLocal(stage.mouseX, stage.mouseY);
            if (stage.mouseInBounds) {
                for (var i = 0; i < dropBoxes.length; i++) {
                    if (dropBoxes[i].isHover) {
                        dropArea = dropBoxes[i];
                        dropAreaText = dropBoxes[i].dropBoxText;
                        boxNumber = dropBoxes[i].value;
                        break;
                    }
                };
            }

            if (dropArea) {
                currentStage.correctIcon.visible = false;
                currentStage.incorrectIcon.visible = false;
                currentStage.incorrectAnswer.visible = false;

                var isChanged = false;

                //if (dropArea.value === stage.draggingShape.value) {
                dropAreaText.text = stage.draggingShape.value;
                dropBoxes[i].value = stage.draggingShape.value;
                dropArea.solved = true;
                isChanged = false;
                //}
                // var leftValue = parseInt(currentStage.dropBoxText1.text);
                // var rightValue = parseInt(currentStage.dropBoxText2.text);
                // if (leftValue && rightValue && leftValue + rightValue == 10) {

                //     // createjs.Tween.get(tickMark).to({
                //     //     alpha: 1
                //     // }, 500).wait(500).to({
                //     //     alpha: 0
                //     // }, 500).call(function() {
                //     //     if (isAllCellsSolved() === true) {
                //     //         //btnContinue.visible = false;
                //     //         createjs.Tween.get(btnContinue).to({
                //     //             alpha: 0
                //     //         }, 200);
                //     //         createjs.Tween.get(btnReset).to({
                //     //             alpha: 1
                //     //         }, 200);
                //     //         //btnReset.visible = true;
                //     //     } else {
                //     //         //btnContinue.visible = true;
                //     //         createjs.Tween.get(btnContinue).to({
                //     //             alpha: 1
                //     //         }, 200);
                //     //         createjs.Tween.get(btnReset).to({
                //     //             alpha: 0
                //     //         }, 200);
                //     //     }

                //     // });

                //     isChanged = true;

                // } else if ((leftValue && boxNumber == 2) || (rightValue && boxNumber == 1)) {
                //     dropAreaText.text = "";
                //     isChanged = false;
                // }
                if (isChanged === true) {
                    // createjs.Tween.get(stage.draggingShape).to({
                    // alpha: 0
                    // }).wait(1000).call(function() {
                    stage.removeChild(stage.draggingShape);
                    stage.draggingShape = null;
                    // });
                } else {
                    // createjs.Tween.get(stage.draggingShape).to({
                    //     scaleX: 0,
                    //     scaleY: 0
                    // }, 100).call(function() {
                    stage.removeChild(stage.draggingShape);
                    stage.draggingShape = null;
                    // });
                }
                var isAllBoxesHavingValue = true;
                for (var i = 0; i < dropBoxes.length; i++) {
                    if (!dropBoxes[i].solved) {
                        isAllBoxesHavingValue = false;
                    }
                }
                if (isAllBoxesHavingValue) {
                    if (dropAreaText.text == currentStage.answer) {
                        currentAnswers[1] = currentStage.answer;
                        currentStage.correctIcon.visible = true;
                        currentStage.incorrectIcon.visible = false;
                        currentStage.incorrectAnswer.visible = false;
                        if (exerciseCount < 5) {
                            $(btn_answers).hide();
                            $(btn_answers).addClass('disabled');
                            $(btn_newExercise).show();
                            $(btn_newExercise).removeClass('disabled');
                        }
                    } else {
                        currentAnswers[1] = 0;
                        currentStage.correctIcon.visible = false;
                        currentStage.incorrectIcon.visible = false;
                        currentStage.incorrectAnswer.visible = false;
                        $(btn_newExercise).hide();
                        $(btn_newExercise).addClass('disabled');
                        setTimeout(function() {
                            resetAll();
                        }, 400);

                    }

                }

            } else {
                stage.removeChild(stage.draggingShape);
                stage.draggingShape = null;


            }
        }
        stage.update();
    }

    function isAllCellsSolved() {
        for (var i = 0; i < dropBoxes.length; i++) {
            if (dropBoxes[i].solved) {
                if (dropBoxes[i].value !== i) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    function resetAll() {
        if (typeof stage != 'undefined') {
            stage.removeChild(stage.draggingShape);
        }
        for (var i = 0; i < dropBoxes.length; i++) {
            dropBoxes[i].solved = false;
            dropBoxes[i].value = null;
            dropBoxes[i].dropBoxText.text = "";

        }
        if (typeof btnReset != 'undefined') {
            createjs.Tween.get(btnReset).to({
                alpha: 0
            }, 200);
        }
    }

    function checkHover(object) {
        object.on("mouseover", function(e) {

            for (var i = 0; i < dropBoxes.length; i++) {
                dropBoxes[i].isHover = false;
            };
            e.currentTarget.isHover = true;
        });
        object.on("mouseout", function(e) {
            for (var i = 0; i < dropBoxes.length; i++) {
                dropBoxes[i].isHover = false;
            };
            e.currentTarget.isHover = false;
        });
        object.on("mousedown", function(e) {
            for (var i = 0; i < dropBoxes.length; i++) {
                dropBoxes[i].isHover = false;
            };
            e.currentTarget.isHover = true;
        });
    }

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text(" ", "bold 16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.textAlign = 'center';
        this.text_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("+", "bold 16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.textAlign = 'center';
        this.text_2.setTransform(15, 0);

        this.text_3 = new cjs.Text(" ", "bold 16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.textAlign = 'center';
        this.text_3.setTransform(60, 0);

        this.text_4 = new cjs.Text("=", "bold 16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.textAlign = 'center';
        this.text_4.setTransform(45, 0);

        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    var currentStage;
    var dropBoxes = [];
    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;
        this.questionText = new cjs.Text("Vilket tal saknas?", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -50);

        this.TeacherText = new cjs.Text("Dra talet till den tomma rutan.", "24px 'Myriad Pro'", "#00B4EA")
        this.TeacherText.textAlign = 'center';
        this.TeacherText.lineHeight = 19;
        this.TeacherText.setTransform(660, 520);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        //this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.answers = new lib.answers();
        this.answers.setTransform(375, 70, 9.5, 9.5);

        this.addChild(this.questionText, this.TeacherText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon, this.answers);


        thisStage.currentAnswer = 0;
        var dropBoxesProp = {
                x: 610,
                y: 75
            }
            // logic for vivoar drag & drop exercise starts here
        for (var i = 0; i < 1; i++) {
            var dropBoxtemp = new createjs.Container();
            dropBoxtemp.x = dropBoxesProp.x + i * (dropBox.width + dropBoxSpacing.width),
                dropBoxtemp.y = dropBoxesProp.y;
            var box = new createjs.Shape()
            box.graphics.f('#ffffff').s('#7d7d7d').drawRect(0, 0, dropBox.width, dropBox.height, 5);
            dropBoxtemp.addChild(box);
            var dropBoxText = new createjs.Text(" ", "bold 150px 'Myriad Pro'", "#000000");
            dropBoxText.textAlign = 'center';
            dropBoxText.x = 70;
            dropBoxText.y = 10;
            dropBoxtemp.addChild(box, dropBoxText);
            dropBoxtemp.dropBoxText = dropBoxText;
            dropBoxtemp.value = i;
            checkHover(dropBoxtemp);
            dropBoxes.push(dropBoxtemp);
            this.addChild(dropBoxtemp);
        }


        var sections = 1,
            cellBlocks = 7,
            rows = 1;

        var allDraggables = [];
        for (var section = 0; section < sections; section++) {
            for (var cellBlock = 0; cellBlock < cellBlocks; cellBlock++) {
                for (var row = 0; row < rows; row++) {

                    var dragger = new createjs.Container();

                    dragger.value = cellBlock;

                    dragger.x = (draggableNumber.x + (draggableNumber.width * cellBlock) + (draggableNumberSpacing.width * cellBlock)) + ((draggableNumber.width * cellBlocks) * section + cellSpacing.cellSectionSpace * section);

                    dragger.y = draggableNumber.y + (draggableNumber.height * row) + (draggableNumberSpacing.height * row);

                    var tempCell = new createjs.Shape()
                    tempCell.graphics.s('#7d7d7d').beginLinearGradientFill(["#FFFFFF", "#fff880"], [0, 1], 0, 10, 0, 80).drawRoundRect(0, 0, draggableNumber.width, draggableNumber.height, 5);

                    var tempCellText = new createjs.Text("" + dragger.value, "bold 150px 'Myriad Pro'", "#000000");
                    tempCellText.textAlign = "center";

                    tempCellText.x = draggableNumber.width / 2;
                    tempCellText.y = draggableNumber.height / 12;
                    dragger.textBox = tempCellText;
                    dragger.addChild(tempCell);
                    dragger.addChild(tempCellText);

                    this.addChild(dragger);
                    allDraggables.push(dragger);
                    dragger.on("mouseover", handleOver);
                    dragger.on("mouseout", handleOut);
                    dragger.on("mousedown", handlePress);

                }
            }
        }
        currentStage = this;
        var myStage = this;

        exerciseCount = 0;
        var ranNums = [];
        objects = shuffle(objects);
        var values = [0, 1, 2, 3, 4, 5, 6];

        this.onNewExercise = function(e) {

            if (exerciseCount < 5) {
                resetAll();
                for (var i = 0; i < allDraggables.length; i++) {
                    allDraggables[i].value = values[i];
                    allDraggables[i].textBox.text = values[i];
                };
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                var numCol1 = [0, 4, 1, 6, 3];
                var numCol2 = [6, 5, 5, 6, 5];
                myStage.answers.children[0].text = numCol1[exerciseCount] + "";
                myStage.answers.children[2].text = numCol2[exerciseCount] + "";
                myStage.answer = numCol2[exerciseCount] - numCol1[exerciseCount];

                new lib.Magic_Wand(myStage, false);

                exerciseCount = exerciseCount + 1;
            }

            if (typeof btn_newExercise != 'undefined') {
                $(btn_newExercise).hide();
            }
        };
        var showIncorrectAnswerTween = null;

        // this.onCheckAnswer = function() {
        //     myStage.correctIcon.visible = false;
        //     myStage.incorrectIcon.visible = false;
        //     myStage.incorrectAnswer.visible = false;
        //     debugger
        //     var answer = isAllCellsSolved();
        //     if (answer) {
        //         currentAnswers[1] = myStage.answer = answer;
        //         $(btn_answers).hide();
        //         $(btn_answers).addClass('disabled');
        //         $(btn_newExercise).show();
        //         $(btn_newExercise).removeClass('disabled');
        //         if (exerciseCount == values.length) {
        //             $(btn_newExercise).hide();
        //             $(btn_newExercise).addClass('disabled');
        //             $(btn_answers).hide();
        //             $(btn_answers).removeClass('disabled');
        //         }
        //     } else {
        //         myStage.answer = false;
        //         currentAnswers[1] = 0;
        //     }

        // };

        this.onNewExercise();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Ett tal saknas", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);

        this.text_1 = new cjs.Text("22", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(48, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.balls = function() {
        this.initialize();
        //function () {



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)



        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
