var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };
    var transformX = transformY = 0.187;
    var moveX = 485 + 64,
        moveY = 4;
    var startX = ((701.4114) * transformX) + moveX,
        startY = ((-1.0931) * transformY) + moveY;
    var endX = ((709.0000) * transformX) + moveX,
        endY = ((1.0000) * transformY) + moveY;
    var pencil = null;

    var startX2 = ((708.5555) * transformX) + moveX,
        startY2 = ((0.5555) * transformY) + moveY,
        endX2 = ((708.0000) * transformX) + moveX,
        endY2 = ((1.0000) * transformY) + moveY;
    var altStartX2 = startX,
        altStartY2 = startY,
        altEndX2 = endX,
        altEndY2 = endY;
    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [

        new createjs.Point(((701.4114) * transformX) + moveX, ((-1.0931) * transformY) + moveY),
        new createjs.Point(((671.5634) * transformX) + moveX, ((-1.1391) * transformY) + moveY),

        new createjs.Point(((664.0000) * transformX) + moveX, ((1.0000) * transformY) + moveY),

        new createjs.Point(((660.6670) * transformX) + moveX, ((1.0000) * transformY) + moveY),
        new createjs.Point(((657.3330) * transformX) + moveX, ((1.0000) * transformY) + moveY),
        new createjs.Point(((654.0000) * transformX) + moveX, ((1.0000) * transformY) + moveY),

        new createjs.Point(((652.0666) * transformX) + moveX, ((1.1686) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((649.0003) * transformX) + moveX, ((2.0000) * transformY) + moveY),
        new createjs.Point(((645.9997) * transformX) + moveX, ((2.0000) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((643.0000) * transformX) + moveX, ((2.3333) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((2.6667) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((640.6669) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((638.3331) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((636.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((629.3554) * transformX) + moveX, ((4.9694) * transformY) + moveY),
        new createjs.Point(((620.4362) * transformX) + moveX, ((5.9886) * transformY) + moveY),
        new createjs.Point(((614.0000) * transformX) + moveX, ((8.0000) * transformY) + moveY),

        new createjs.Point(((612.3335) * transformX) + moveX, ((8.0000) * transformY) + moveY),
        new createjs.Point(((610.6665) * transformX) + moveX, ((8.0000) * transformY) + moveY),
        new createjs.Point(((609.0000) * transformX) + moveX, ((8.0000) * transformY) + moveY),

        new createjs.Point(((609.0000) * transformX) + moveX, ((8.3333) * transformY) + moveY),
        new createjs.Point(((609.0000) * transformX) + moveX, ((8.6667) * transformY) + moveY),
        new createjs.Point(((609.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((605.0004) * transformX) + moveX, ((9.6666) * transformY) + moveY),
        new createjs.Point(((600.9996) * transformX) + moveX, ((10.3334) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((11.0000) * transformY) + moveY),

        new createjs.Point(((597.0000) * transformX) + moveX, ((11.3333) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((11.6667) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((595.6668) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((594.3332) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((593.0000) * transformX) + moveX, ((12.3333) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((12.6667) * transformY) + moveY),
        new createjs.Point(((593.0000) * transformX) + moveX, ((13.0000) * transformY) + moveY),

        new createjs.Point(((590.6669) * transformX) + moveX, ((13.3333) * transformY) + moveY),
        new createjs.Point(((588.3331) * transformX) + moveX, ((13.6667) * transformY) + moveY),
        new createjs.Point(((586.0000) * transformX) + moveX, ((14.0000) * transformY) + moveY),

        new createjs.Point(((586.0000) * transformX) + moveX, ((14.3333) * transformY) + moveY),
        new createjs.Point(((586.0000) * transformX) + moveX, ((14.6667) * transformY) + moveY),
        new createjs.Point(((586.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((585.0001) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((583.9999) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((583.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((583.0000) * transformX) + moveX, ((15.3333) * transformY) + moveY),
        new createjs.Point(((583.0000) * transformX) + moveX, ((15.6667) * transformY) + moveY),
        new createjs.Point(((583.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((582.0001) * transformX) + moveX, ((16.0000) * transformY) + moveY),
        new createjs.Point(((580.9999) * transformX) + moveX, ((16.0000) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((580.0000) * transformX) + moveX, ((16.3333) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((16.6667) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((17.0000) * transformY) + moveY),

        new createjs.Point(((579.0001) * transformX) + moveX, ((17.0000) * transformY) + moveY),
        new createjs.Point(((577.9999) * transformX) + moveX, ((17.0000) * transformY) + moveY),
        new createjs.Point(((577.0000) * transformX) + moveX, ((17.0000) * transformY) + moveY),

        new createjs.Point(((577.0000) * transformX) + moveX, ((17.3333) * transformY) + moveY),
        new createjs.Point(((577.0000) * transformX) + moveX, ((17.6667) * transformY) + moveY),
        new createjs.Point(((577.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((576.0001) * transformX) + moveX, ((18.0000) * transformY) + moveY),
        new createjs.Point(((574.9999) * transformX) + moveX, ((18.0000) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((574.0000) * transformX) + moveX, ((18.3333) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((18.6667) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((19.0000) * transformY) + moveY),

        new createjs.Point(((573.0001) * transformX) + moveX, ((19.0000) * transformY) + moveY),
        new createjs.Point(((571.9999) * transformX) + moveX, ((19.0000) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((19.0000) * transformY) + moveY),

        new createjs.Point(((571.0000) * transformX) + moveX, ((19.3333) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((19.6667) * transformY) + moveY),
        new createjs.Point(((571.0000) * transformX) + moveX, ((20.0000) * transformY) + moveY),

        new createjs.Point(((570.0001) * transformX) + moveX, ((20.0000) * transformY) + moveY),
        new createjs.Point(((568.9999) * transformX) + moveX, ((20.0000) * transformY) + moveY),
        new createjs.Point(((568.0000) * transformX) + moveX, ((20.0000) * transformY) + moveY),

        new createjs.Point(((568.0000) * transformX) + moveX, ((20.3333) * transformY) + moveY),
        new createjs.Point(((568.0000) * transformX) + moveX, ((20.6667) * transformY) + moveY),
        new createjs.Point(((568.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((567.0001) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((565.9999) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((565.0000) * transformX) + moveX, ((21.3333) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((21.6667) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((564.0001) * transformX) + moveX, ((22.0000) * transformY) + moveY),
        new createjs.Point(((562.9999) * transformX) + moveX, ((22.0000) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((562.0000) * transformX) + moveX, ((22.3333) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((22.6667) * transformY) + moveY),
        new createjs.Point(((562.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((561.3334) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((560.6666) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((560.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((560.0000) * transformX) + moveX, ((23.3333) * transformY) + moveY),
        new createjs.Point(((560.0000) * transformX) + moveX, ((23.6667) * transformY) + moveY),
        new createjs.Point(((560.0000) * transformX) + moveX, ((24.0000) * transformY) + moveY),

        new createjs.Point(((559.0001) * transformX) + moveX, ((24.0000) * transformY) + moveY),
        new createjs.Point(((557.9999) * transformX) + moveX, ((24.0000) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((24.0000) * transformY) + moveY),

        new createjs.Point(((557.0000) * transformX) + moveX, ((24.3333) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((24.6667) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((25.0000) * transformY) + moveY),

        new createjs.Point(((556.3334) * transformX) + moveX, ((25.0000) * transformY) + moveY),
        new createjs.Point(((555.6666) * transformX) + moveX, ((25.0000) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((25.0000) * transformY) + moveY),

        new createjs.Point(((555.0000) * transformX) + moveX, ((25.3333) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((25.6667) * transformY) + moveY),
        new createjs.Point(((555.0000) * transformX) + moveX, ((26.0000) * transformY) + moveY),

        new createjs.Point(((554.0001) * transformX) + moveX, ((26.0000) * transformY) + moveY),
        new createjs.Point(((552.9999) * transformX) + moveX, ((26.0000) * transformY) + moveY),
        new createjs.Point(((552.0000) * transformX) + moveX, ((26.0000) * transformY) + moveY),

        new createjs.Point(((552.0000) * transformX) + moveX, ((26.3333) * transformY) + moveY),
        new createjs.Point(((552.0000) * transformX) + moveX, ((26.6667) * transformY) + moveY),
        new createjs.Point(((552.0000) * transformX) + moveX, ((27.0000) * transformY) + moveY),

        new createjs.Point(((550.6668) * transformX) + moveX, ((27.3333) * transformY) + moveY),
        new createjs.Point(((549.3332) * transformX) + moveX, ((27.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((28.0000) * transformY) + moveY),

        new createjs.Point(((548.0000) * transformX) + moveX, ((28.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((28.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((547.0001) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((545.9999) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((545.0000) * transformX) + moveX, ((29.3333) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((29.6667) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((30.0000) * transformY) + moveY),

        new createjs.Point(((543.6668) * transformX) + moveX, ((30.3333) * transformY) + moveY),
        new createjs.Point(((542.3332) * transformX) + moveX, ((30.6667) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((541.0000) * transformX) + moveX, ((31.3333) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((31.6667) * transformY) + moveY),
        new createjs.Point(((541.0000) * transformX) + moveX, ((32.0000) * transformY) + moveY),

        new createjs.Point(((539.3335) * transformX) + moveX, ((32.3333) * transformY) + moveY),
        new createjs.Point(((537.6665) * transformX) + moveX, ((32.6667) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((33.0000) * transformY) + moveY),

        new createjs.Point(((536.0000) * transformX) + moveX, ((33.3333) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((33.6667) * transformY) + moveY),
        new createjs.Point(((536.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((534.0002) * transformX) + moveX, ((34.6666) * transformY) + moveY),
        new createjs.Point(((531.9998) * transformX) + moveX, ((35.3334) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((36.0000) * transformY) + moveY),

        new createjs.Point(((530.0000) * transformX) + moveX, ((36.3333) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((36.6667) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((37.0000) * transformY) + moveY),

        new createjs.Point(((529.3334) * transformX) + moveX, ((37.0000) * transformY) + moveY),
        new createjs.Point(((528.6666) * transformX) + moveX, ((37.0000) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((37.0000) * transformY) + moveY),

        new createjs.Point(((528.0000) * transformX) + moveX, ((37.3333) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((37.6667) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((527.3334) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((526.6666) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((526.0000) * transformX) + moveX, ((38.3333) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((38.6667) * transformY) + moveY),
        new createjs.Point(((526.0000) * transformX) + moveX, ((39.0000) * transformY) + moveY),

        new createjs.Point(((525.3334) * transformX) + moveX, ((39.0000) * transformY) + moveY),
        new createjs.Point(((524.6666) * transformX) + moveX, ((39.0000) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((39.0000) * transformY) + moveY),

        new createjs.Point(((524.0000) * transformX) + moveX, ((39.3333) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((39.6667) * transformY) + moveY),
        new createjs.Point(((524.0000) * transformX) + moveX, ((40.0000) * transformY) + moveY),

        new createjs.Point(((523.3334) * transformX) + moveX, ((40.0000) * transformY) + moveY),
        new createjs.Point(((522.6666) * transformX) + moveX, ((40.0000) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((40.0000) * transformY) + moveY),

        new createjs.Point(((522.0000) * transformX) + moveX, ((40.3333) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((40.6667) * transformY) + moveY),
        new createjs.Point(((522.0000) * transformX) + moveX, ((41.0000) * transformY) + moveY),

        new createjs.Point(((521.3334) * transformX) + moveX, ((41.0000) * transformY) + moveY),
        new createjs.Point(((520.6666) * transformX) + moveX, ((41.0000) * transformY) + moveY),
        new createjs.Point(((520.0000) * transformX) + moveX, ((41.0000) * transformY) + moveY),

        new createjs.Point(((520.0000) * transformX) + moveX, ((41.3333) * transformY) + moveY),
        new createjs.Point(((520.0000) * transformX) + moveX, ((41.6667) * transformY) + moveY),
        new createjs.Point(((520.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((519.3334) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((518.6666) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((518.0000) * transformX) + moveX, ((42.3333) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((42.6667) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((43.0000) * transformY) + moveY),

        new createjs.Point(((517.3334) * transformX) + moveX, ((43.0000) * transformY) + moveY),
        new createjs.Point(((516.6666) * transformX) + moveX, ((43.0000) * transformY) + moveY),
        new createjs.Point(((516.0000) * transformX) + moveX, ((43.0000) * transformY) + moveY),

        new createjs.Point(((515.6667) * transformX) + moveX, ((43.6666) * transformY) + moveY),
        new createjs.Point(((515.3333) * transformX) + moveX, ((44.3334) * transformY) + moveY),
        new createjs.Point(((515.0000) * transformX) + moveX, ((45.0000) * transformY) + moveY),

        new createjs.Point(((514.3334) * transformX) + moveX, ((45.0000) * transformY) + moveY),
        new createjs.Point(((513.6666) * transformX) + moveX, ((45.0000) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((45.0000) * transformY) + moveY),

        new createjs.Point(((513.0000) * transformX) + moveX, ((45.3333) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((45.6667) * transformY) + moveY),
        new createjs.Point(((513.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((512.3334) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((511.6666) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((511.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((511.0000) * transformX) + moveX, ((46.3333) * transformY) + moveY),
        new createjs.Point(((511.0000) * transformX) + moveX, ((46.6667) * transformY) + moveY),
        new createjs.Point(((511.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((510.3334) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((509.6666) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((509.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((509.0000) * transformX) + moveX, ((47.3333) * transformY) + moveY),
        new createjs.Point(((509.0000) * transformX) + moveX, ((47.6667) * transformY) + moveY),
        new createjs.Point(((509.0000) * transformX) + moveX, ((48.0000) * transformY) + moveY),

        new createjs.Point(((508.3334) * transformX) + moveX, ((48.0000) * transformY) + moveY),
        new createjs.Point(((507.6666) * transformX) + moveX, ((48.0000) * transformY) + moveY),
        new createjs.Point(((507.0000) * transformX) + moveX, ((48.0000) * transformY) + moveY),

        new createjs.Point(((506.6667) * transformX) + moveX, ((48.6666) * transformY) + moveY),
        new createjs.Point(((506.3333) * transformX) + moveX, ((49.3334) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((50.0000) * transformY) + moveY),

        new createjs.Point(((504.6668) * transformX) + moveX, ((50.3333) * transformY) + moveY),
        new createjs.Point(((503.3332) * transformX) + moveX, ((50.6667) * transformY) + moveY),
        new createjs.Point(((502.0000) * transformX) + moveX, ((51.0000) * transformY) + moveY),

        new createjs.Point(((501.6667) * transformX) + moveX, ((51.6666) * transformY) + moveY),
        new createjs.Point(((501.3333) * transformX) + moveX, ((52.3334) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((53.0000) * transformY) + moveY),

        new createjs.Point(((499.6668) * transformX) + moveX, ((53.3333) * transformY) + moveY),
        new createjs.Point(((498.3332) * transformX) + moveX, ((53.6667) * transformY) + moveY),
        new createjs.Point(((497.0000) * transformX) + moveX, ((54.0000) * transformY) + moveY),

        new createjs.Point(((496.6667) * transformX) + moveX, ((54.6666) * transformY) + moveY),
        new createjs.Point(((496.3333) * transformX) + moveX, ((55.3334) * transformY) + moveY),
        new createjs.Point(((496.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((495.3334) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((494.6666) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((494.0000) * transformX) + moveX, ((56.3333) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((56.6667) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((57.0000) * transformY) + moveY),

        new createjs.Point(((493.3334) * transformX) + moveX, ((57.0000) * transformY) + moveY),
        new createjs.Point(((492.6666) * transformX) + moveX, ((57.0000) * transformY) + moveY),
        new createjs.Point(((492.0000) * transformX) + moveX, ((57.0000) * transformY) + moveY),

        new createjs.Point(((491.6667) * transformX) + moveX, ((57.6666) * transformY) + moveY),
        new createjs.Point(((491.3333) * transformX) + moveX, ((58.3334) * transformY) + moveY),
        new createjs.Point(((491.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((490.3334) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((489.6666) * transformX) + moveX, ((59.0000) * transformY) + moveY),
        new createjs.Point(((489.0000) * transformX) + moveX, ((59.0000) * transformY) + moveY),

        new createjs.Point(((488.6667) * transformX) + moveX, ((59.6666) * transformY) + moveY),
        new createjs.Point(((488.3333) * transformX) + moveX, ((60.3334) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((61.0000) * transformY) + moveY),

        new createjs.Point(((487.3334) * transformX) + moveX, ((61.0000) * transformY) + moveY),
        new createjs.Point(((486.6666) * transformX) + moveX, ((61.0000) * transformY) + moveY),
        new createjs.Point(((486.0000) * transformX) + moveX, ((61.0000) * transformY) + moveY),

        new createjs.Point(((485.6667) * transformX) + moveX, ((61.6666) * transformY) + moveY),
        new createjs.Point(((485.3333) * transformX) + moveX, ((62.3334) * transformY) + moveY),
        new createjs.Point(((485.0000) * transformX) + moveX, ((63.0000) * transformY) + moveY),

        new createjs.Point(((484.3334) * transformX) + moveX, ((63.0000) * transformY) + moveY),
        new createjs.Point(((483.6666) * transformX) + moveX, ((63.0000) * transformY) + moveY),
        new createjs.Point(((483.0000) * transformX) + moveX, ((63.0000) * transformY) + moveY),

        new createjs.Point(((483.0000) * transformX) + moveX, ((63.3333) * transformY) + moveY),
        new createjs.Point(((483.0000) * transformX) + moveX, ((63.6667) * transformY) + moveY),
        new createjs.Point(((483.0000) * transformX) + moveX, ((64.0000) * transformY) + moveY),

        new createjs.Point(((482.0001) * transformX) + moveX, ((64.3333) * transformY) + moveY),
        new createjs.Point(((480.9999) * transformX) + moveX, ((64.6667) * transformY) + moveY),
        new createjs.Point(((480.0000) * transformX) + moveX, ((65.0000) * transformY) + moveY),

        new createjs.Point(((479.6667) * transformX) + moveX, ((65.6666) * transformY) + moveY),
        new createjs.Point(((479.3333) * transformX) + moveX, ((66.3334) * transformY) + moveY),
        new createjs.Point(((479.0000) * transformX) + moveX, ((67.0000) * transformY) + moveY),

        new createjs.Point(((478.3334) * transformX) + moveX, ((67.0000) * transformY) + moveY),
        new createjs.Point(((477.6666) * transformX) + moveX, ((67.0000) * transformY) + moveY),
        new createjs.Point(((477.0000) * transformX) + moveX, ((67.0000) * transformY) + moveY),

        new createjs.Point(((476.6667) * transformX) + moveX, ((67.6666) * transformY) + moveY),
        new createjs.Point(((476.3333) * transformX) + moveX, ((68.3334) * transformY) + moveY),
        new createjs.Point(((476.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((475.3334) * transformX) + moveX, ((69.0000) * transformY) + moveY),
        new createjs.Point(((474.6666) * transformX) + moveX, ((69.0000) * transformY) + moveY),
        new createjs.Point(((474.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((473.3334) * transformX) + moveX, ((69.9999) * transformY) + moveY),
        new createjs.Point(((472.6666) * transformX) + moveX, ((71.0001) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((72.0000) * transformY) + moveY),

        new createjs.Point(((471.3334) * transformX) + moveX, ((72.0000) * transformY) + moveY),
        new createjs.Point(((470.6666) * transformX) + moveX, ((72.0000) * transformY) + moveY),
        new createjs.Point(((470.0000) * transformX) + moveX, ((72.0000) * transformY) + moveY),

        new createjs.Point(((469.6667) * transformX) + moveX, ((72.6666) * transformY) + moveY),
        new createjs.Point(((469.3333) * transformX) + moveX, ((73.3334) * transformY) + moveY),
        new createjs.Point(((469.0000) * transformX) + moveX, ((74.0000) * transformY) + moveY),

        new createjs.Point(((468.3334) * transformX) + moveX, ((74.0000) * transformY) + moveY),
        new createjs.Point(((467.6666) * transformX) + moveX, ((74.0000) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((74.0000) * transformY) + moveY),

        new createjs.Point(((466.6667) * transformX) + moveX, ((74.6666) * transformY) + moveY),
        new createjs.Point(((466.3333) * transformX) + moveX, ((75.3334) * transformY) + moveY),
        new createjs.Point(((466.0000) * transformX) + moveX, ((76.0000) * transformY) + moveY),

        new createjs.Point(((465.0001) * transformX) + moveX, ((76.3333) * transformY) + moveY),
        new createjs.Point(((463.9999) * transformX) + moveX, ((76.6667) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((77.0000) * transformY) + moveY),

        new createjs.Point(((462.0001) * transformX) + moveX, ((78.3332) * transformY) + moveY),
        new createjs.Point(((460.9999) * transformX) + moveX, ((79.6668) * transformY) + moveY),
        new createjs.Point(((460.0000) * transformX) + moveX, ((81.0000) * transformY) + moveY),

        new createjs.Point(((459.3334) * transformX) + moveX, ((81.0000) * transformY) + moveY),
        new createjs.Point(((458.6666) * transformX) + moveX, ((81.0000) * transformY) + moveY),
        new createjs.Point(((458.0000) * transformX) + moveX, ((81.0000) * transformY) + moveY),

        new createjs.Point(((457.3334) * transformX) + moveX, ((81.9999) * transformY) + moveY),
        new createjs.Point(((456.6666) * transformX) + moveX, ((83.0001) * transformY) + moveY),
        new createjs.Point(((456.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),

        new createjs.Point(((455.3334) * transformX) + moveX, ((84.0000) * transformY) + moveY),
        new createjs.Point(((454.6666) * transformX) + moveX, ((84.0000) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((84.0000) * transformY) + moveY),

        new createjs.Point(((453.0001) * transformX) + moveX, ((85.3332) * transformY) + moveY),
        new createjs.Point(((451.9999) * transformX) + moveX, ((86.6668) * transformY) + moveY),
        new createjs.Point(((451.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),

        new createjs.Point(((450.3334) * transformX) + moveX, ((88.0000) * transformY) + moveY),
        new createjs.Point(((449.6666) * transformX) + moveX, ((88.0000) * transformY) + moveY),
        new createjs.Point(((449.0000) * transformX) + moveX, ((88.0000) * transformY) + moveY),

        new createjs.Point(((447.3335) * transformX) + moveX, ((89.9998) * transformY) + moveY),
        new createjs.Point(((445.6665) * transformX) + moveX, ((92.0002) * transformY) + moveY),
        new createjs.Point(((444.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((443.3334) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((442.6666) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((442.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((440.0002) * transformX) + moveX, ((96.3331) * transformY) + moveY),
        new createjs.Point(((437.9998) * transformX) + moveX, ((98.6669) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),

        new createjs.Point(((435.3334) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((434.6666) * transformX) + moveX, ((101.0000) * transformY) + moveY),
        new createjs.Point(((434.0000) * transformX) + moveX, ((101.0000) * transformY) + moveY),

        new createjs.Point(((429.0005) * transformX) + moveX, ((106.3328) * transformY) + moveY),
        new createjs.Point(((423.9995) * transformX) + moveX, ((111.6672) * transformY) + moveY),
        new createjs.Point(((419.0000) * transformX) + moveX, ((117.0000) * transformY) + moveY),

        new createjs.Point(((412.6673) * transformX) + moveX, ((122.9994) * transformY) + moveY),
        new createjs.Point(((406.3327) * transformX) + moveX, ((129.0006) * transformY) + moveY),
        new createjs.Point(((400.0000) * transformX) + moveX, ((135.0000) * transformY) + moveY),

        new createjs.Point(((400.0000) * transformX) + moveX, ((135.6666) * transformY) + moveY),
        new createjs.Point(((400.0000) * transformX) + moveX, ((136.3334) * transformY) + moveY),
        new createjs.Point(((400.0000) * transformX) + moveX, ((137.0000) * transformY) + moveY),

        new createjs.Point(((397.6669) * transformX) + moveX, ((138.9998) * transformY) + moveY),
        new createjs.Point(((395.3331) * transformX) + moveX, ((141.0002) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((143.0000) * transformY) + moveY),

        new createjs.Point(((393.0000) * transformX) + moveX, ((143.6666) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((144.3334) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((145.0000) * transformY) + moveY),

        new createjs.Point(((391.0002) * transformX) + moveX, ((146.6665) * transformY) + moveY),
        new createjs.Point(((388.9998) * transformX) + moveX, ((148.3335) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((150.0000) * transformY) + moveY),

        new createjs.Point(((387.0000) * transformX) + moveX, ((150.6666) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((151.3334) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((152.0000) * transformY) + moveY),

        new createjs.Point(((385.6668) * transformX) + moveX, ((152.9999) * transformY) + moveY),
        new createjs.Point(((384.3332) * transformX) + moveX, ((154.0001) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((155.0000) * transformY) + moveY),

        new createjs.Point(((383.0000) * transformX) + moveX, ((155.6666) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((156.3334) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((157.0000) * transformY) + moveY),

        new createjs.Point(((381.6668) * transformX) + moveX, ((157.9999) * transformY) + moveY),
        new createjs.Point(((380.3332) * transformX) + moveX, ((159.0001) * transformY) + moveY),
        new createjs.Point(((379.0000) * transformX) + moveX, ((160.0000) * transformY) + moveY),

        new createjs.Point(((379.0000) * transformX) + moveX, ((160.6666) * transformY) + moveY),
        new createjs.Point(((379.0000) * transformX) + moveX, ((161.3334) * transformY) + moveY),
        new createjs.Point(((379.0000) * transformX) + moveX, ((162.0000) * transformY) + moveY),

        new createjs.Point(((378.0001) * transformX) + moveX, ((162.6666) * transformY) + moveY),
        new createjs.Point(((376.9999) * transformX) + moveX, ((163.3334) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((164.0000) * transformY) + moveY),

        new createjs.Point(((376.0000) * transformX) + moveX, ((164.6666) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((165.3334) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((166.0000) * transformY) + moveY),

        new createjs.Point(((375.0001) * transformX) + moveX, ((166.6666) * transformY) + moveY),
        new createjs.Point(((373.9999) * transformX) + moveX, ((167.3334) * transformY) + moveY),
        new createjs.Point(((373.0000) * transformX) + moveX, ((168.0000) * transformY) + moveY),

        new createjs.Point(((373.0000) * transformX) + moveX, ((168.6666) * transformY) + moveY),
        new createjs.Point(((373.0000) * transformX) + moveX, ((169.3334) * transformY) + moveY),
        new createjs.Point(((373.0000) * transformX) + moveX, ((170.0000) * transformY) + moveY),

        new createjs.Point(((372.0001) * transformX) + moveX, ((170.6666) * transformY) + moveY),
        new createjs.Point(((370.9999) * transformX) + moveX, ((171.3334) * transformY) + moveY),
        new createjs.Point(((370.0000) * transformX) + moveX, ((172.0000) * transformY) + moveY),

        new createjs.Point(((370.0000) * transformX) + moveX, ((172.6666) * transformY) + moveY),
        new createjs.Point(((370.0000) * transformX) + moveX, ((173.3334) * transformY) + moveY),
        new createjs.Point(((370.0000) * transformX) + moveX, ((174.0000) * transformY) + moveY),

        new createjs.Point(((369.0001) * transformX) + moveX, ((174.6666) * transformY) + moveY),
        new createjs.Point(((367.9999) * transformX) + moveX, ((175.3334) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((176.0000) * transformY) + moveY),

        new createjs.Point(((367.0000) * transformX) + moveX, ((176.6666) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((177.3334) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((178.0000) * transformY) + moveY),

        new createjs.Point(((366.3334) * transformX) + moveX, ((178.3333) * transformY) + moveY),
        new createjs.Point(((365.6666) * transformX) + moveX, ((178.6667) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((179.0000) * transformY) + moveY),

        new createjs.Point(((365.0000) * transformX) + moveX, ((179.6666) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((180.3334) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((181.0000) * transformY) + moveY),

        new createjs.Point(((364.0001) * transformX) + moveX, ((181.6666) * transformY) + moveY),
        new createjs.Point(((362.9999) * transformX) + moveX, ((182.3334) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((183.0000) * transformY) + moveY),

        new createjs.Point(((362.0000) * transformX) + moveX, ((183.6666) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((184.3334) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((185.0000) * transformY) + moveY),

        new createjs.Point(((361.3334) * transformX) + moveX, ((185.3333) * transformY) + moveY),
        new createjs.Point(((360.6666) * transformX) + moveX, ((185.6667) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((186.0000) * transformY) + moveY),

        new createjs.Point(((360.0000) * transformX) + moveX, ((186.6666) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((187.3334) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),

        new createjs.Point(((359.3334) * transformX) + moveX, ((188.3333) * transformY) + moveY),
        new createjs.Point(((358.6666) * transformX) + moveX, ((188.6667) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((189.0000) * transformY) + moveY),

        new createjs.Point(((358.0000) * transformX) + moveX, ((189.6666) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((190.3334) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((191.0000) * transformY) + moveY),

        new createjs.Point(((357.3334) * transformX) + moveX, ((191.3333) * transformY) + moveY),
        new createjs.Point(((356.6666) * transformX) + moveX, ((191.6667) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((192.0000) * transformY) + moveY),

        new createjs.Point(((356.0000) * transformX) + moveX, ((192.6666) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((193.3334) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((194.0000) * transformY) + moveY),

        new createjs.Point(((355.3334) * transformX) + moveX, ((194.3333) * transformY) + moveY),
        new createjs.Point(((354.6666) * transformX) + moveX, ((194.6667) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),

        new createjs.Point(((354.0000) * transformX) + moveX, ((195.6666) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((196.3334) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),

        new createjs.Point(((353.3334) * transformX) + moveX, ((197.3333) * transformY) + moveY),
        new createjs.Point(((352.6666) * transformX) + moveX, ((197.6667) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((198.0000) * transformY) + moveY),

        new createjs.Point(((351.6667) * transformX) + moveX, ((199.3332) * transformY) + moveY),
        new createjs.Point(((351.3333) * transformX) + moveX, ((200.6668) * transformY) + moveY),
        new createjs.Point(((351.0000) * transformX) + moveX, ((202.0000) * transformY) + moveY),

        new createjs.Point(((350.3334) * transformX) + moveX, ((202.3333) * transformY) + moveY),
        new createjs.Point(((349.6666) * transformX) + moveX, ((202.6667) * transformY) + moveY),
        new createjs.Point(((349.0000) * transformX) + moveX, ((203.0000) * transformY) + moveY),

        new createjs.Point(((349.0000) * transformX) + moveX, ((203.6666) * transformY) + moveY),
        new createjs.Point(((349.0000) * transformX) + moveX, ((204.3334) * transformY) + moveY),
        new createjs.Point(((349.0000) * transformX) + moveX, ((205.0000) * transformY) + moveY),

        new createjs.Point(((348.3334) * transformX) + moveX, ((205.3333) * transformY) + moveY),
        new createjs.Point(((347.6666) * transformX) + moveX, ((205.6667) * transformY) + moveY),
        new createjs.Point(((347.0000) * transformX) + moveX, ((206.0000) * transformY) + moveY),

        new createjs.Point(((347.0000) * transformX) + moveX, ((206.6666) * transformY) + moveY),
        new createjs.Point(((347.0000) * transformX) + moveX, ((207.3334) * transformY) + moveY),
        new createjs.Point(((347.0000) * transformX) + moveX, ((208.0000) * transformY) + moveY),

        new createjs.Point(((346.6667) * transformX) + moveX, ((208.0000) * transformY) + moveY),
        new createjs.Point(((346.3333) * transformX) + moveX, ((208.0000) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((208.0000) * transformY) + moveY),

        new createjs.Point(((346.0000) * transformX) + moveX, ((208.6666) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((209.3334) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((210.0000) * transformY) + moveY),

        new createjs.Point(((345.3334) * transformX) + moveX, ((210.3333) * transformY) + moveY),
        new createjs.Point(((344.6666) * transformX) + moveX, ((210.6667) * transformY) + moveY),
        new createjs.Point(((344.0000) * transformX) + moveX, ((211.0000) * transformY) + moveY),

        new createjs.Point(((343.6667) * transformX) + moveX, ((212.3332) * transformY) + moveY),
        new createjs.Point(((343.3333) * transformX) + moveX, ((213.6668) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((215.0000) * transformY) + moveY),

        new createjs.Point(((342.3334) * transformX) + moveX, ((215.3333) * transformY) + moveY),
        new createjs.Point(((341.6666) * transformX) + moveX, ((215.6667) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((216.0000) * transformY) + moveY),

        new createjs.Point(((340.6667) * transformX) + moveX, ((217.3332) * transformY) + moveY),
        new createjs.Point(((340.3333) * transformX) + moveX, ((218.6668) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((220.0000) * transformY) + moveY),

        new createjs.Point(((339.6667) * transformX) + moveX, ((220.0000) * transformY) + moveY),
        new createjs.Point(((339.3333) * transformX) + moveX, ((220.0000) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((220.0000) * transformY) + moveY),

        new createjs.Point(((339.0000) * transformX) + moveX, ((220.6666) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((221.3334) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((222.0000) * transformY) + moveY),

        new createjs.Point(((338.3334) * transformX) + moveX, ((222.3333) * transformY) + moveY),
        new createjs.Point(((337.6666) * transformX) + moveX, ((222.6667) * transformY) + moveY),
        new createjs.Point(((337.0000) * transformX) + moveX, ((223.0000) * transformY) + moveY),

        new createjs.Point(((337.0000) * transformX) + moveX, ((223.6666) * transformY) + moveY),
        new createjs.Point(((337.0000) * transformX) + moveX, ((224.3334) * transformY) + moveY),
        new createjs.Point(((337.0000) * transformX) + moveX, ((225.0000) * transformY) + moveY),

        new createjs.Point(((336.6667) * transformX) + moveX, ((225.0000) * transformY) + moveY),
        new createjs.Point(((336.3333) * transformX) + moveX, ((225.0000) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((225.0000) * transformY) + moveY),

        new createjs.Point(((336.0000) * transformX) + moveX, ((225.6666) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((226.3334) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((227.0000) * transformY) + moveY),

        new createjs.Point(((335.6667) * transformX) + moveX, ((227.0000) * transformY) + moveY),
        new createjs.Point(((335.3333) * transformX) + moveX, ((227.0000) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((227.0000) * transformY) + moveY),

        new createjs.Point(((335.0000) * transformX) + moveX, ((227.6666) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((228.3334) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((229.0000) * transformY) + moveY),

        new createjs.Point(((334.6667) * transformX) + moveX, ((229.0000) * transformY) + moveY),
        new createjs.Point(((334.3333) * transformX) + moveX, ((229.0000) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((229.0000) * transformY) + moveY),

        new createjs.Point(((334.0000) * transformX) + moveX, ((229.6666) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((230.3334) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((231.0000) * transformY) + moveY),

        new createjs.Point(((333.6667) * transformX) + moveX, ((231.0000) * transformY) + moveY),
        new createjs.Point(((333.3333) * transformX) + moveX, ((231.0000) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((231.0000) * transformY) + moveY),

        new createjs.Point(((333.0000) * transformX) + moveX, ((231.6666) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((232.3334) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((233.0000) * transformY) + moveY),

        new createjs.Point(((332.3334) * transformX) + moveX, ((233.3333) * transformY) + moveY),
        new createjs.Point(((331.6666) * transformX) + moveX, ((233.6667) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((234.0000) * transformY) + moveY),

        new createjs.Point(((331.0000) * transformX) + moveX, ((234.6666) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((235.3334) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),

        new createjs.Point(((330.6667) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((330.3333) * transformX) + moveX, ((236.0000) * transformY) + moveY),
        new createjs.Point(((330.0000) * transformX) + moveX, ((236.0000) * transformY) + moveY),

        new createjs.Point(((330.0000) * transformX) + moveX, ((236.6666) * transformY) + moveY),
        new createjs.Point(((330.0000) * transformX) + moveX, ((237.3334) * transformY) + moveY),
        new createjs.Point(((330.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),

        new createjs.Point(((329.6667) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((329.3333) * transformX) + moveX, ((238.0000) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((238.0000) * transformY) + moveY),

        new createjs.Point(((329.0000) * transformX) + moveX, ((238.6666) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((239.3334) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((240.0000) * transformY) + moveY),

        new createjs.Point(((328.6667) * transformX) + moveX, ((240.0000) * transformY) + moveY),
        new createjs.Point(((328.3333) * transformX) + moveX, ((240.0000) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((240.0000) * transformY) + moveY),

        new createjs.Point(((328.0000) * transformX) + moveX, ((240.6666) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((241.3334) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((242.0000) * transformY) + moveY),

        new createjs.Point(((327.6667) * transformX) + moveX, ((242.0000) * transformY) + moveY),
        new createjs.Point(((327.3333) * transformX) + moveX, ((242.0000) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((242.0000) * transformY) + moveY),

        new createjs.Point(((327.0000) * transformX) + moveX, ((242.6666) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((243.3334) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((244.0000) * transformY) + moveY),

        new createjs.Point(((326.6667) * transformX) + moveX, ((244.0000) * transformY) + moveY),
        new createjs.Point(((326.3333) * transformX) + moveX, ((244.0000) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((244.0000) * transformY) + moveY),

        new createjs.Point(((326.0000) * transformX) + moveX, ((244.6666) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((245.3334) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((246.0000) * transformY) + moveY),

        new createjs.Point(((325.6667) * transformX) + moveX, ((246.0000) * transformY) + moveY),
        new createjs.Point(((325.3333) * transformX) + moveX, ((246.0000) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((246.0000) * transformY) + moveY),

        new createjs.Point(((325.0000) * transformX) + moveX, ((246.6666) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((247.3334) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),

        new createjs.Point(((324.6667) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((324.3333) * transformX) + moveX, ((248.0000) * transformY) + moveY),
        new createjs.Point(((324.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),

        new createjs.Point(((323.3334) * transformX) + moveX, ((249.9998) * transformY) + moveY),
        new createjs.Point(((322.6666) * transformX) + moveX, ((252.0002) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((254.0000) * transformY) + moveY),

        new createjs.Point(((321.6667) * transformX) + moveX, ((254.0000) * transformY) + moveY),
        new createjs.Point(((321.3333) * transformX) + moveX, ((254.0000) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((254.0000) * transformY) + moveY),

        new createjs.Point(((321.0000) * transformX) + moveX, ((254.9999) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((256.0001) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((257.0000) * transformY) + moveY),

        new createjs.Point(((320.6667) * transformX) + moveX, ((257.0000) * transformY) + moveY),
        new createjs.Point(((320.3333) * transformX) + moveX, ((257.0000) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((257.0000) * transformY) + moveY),

        new createjs.Point(((320.0000) * transformX) + moveX, ((257.6666) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((258.3334) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((259.0000) * transformY) + moveY),

        new createjs.Point(((319.6667) * transformX) + moveX, ((259.0000) * transformY) + moveY),
        new createjs.Point(((319.3333) * transformX) + moveX, ((259.0000) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((259.0000) * transformY) + moveY),

        new createjs.Point(((318.3334) * transformX) + moveX, ((260.9998) * transformY) + moveY),
        new createjs.Point(((317.6666) * transformX) + moveX, ((263.0002) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((265.0000) * transformY) + moveY),

        new createjs.Point(((316.6667) * transformX) + moveX, ((265.0000) * transformY) + moveY),
        new createjs.Point(((316.3333) * transformX) + moveX, ((265.0000) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((265.0000) * transformY) + moveY),

        new createjs.Point(((316.0000) * transformX) + moveX, ((265.9999) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((267.0001) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((268.0000) * transformY) + moveY),

        new createjs.Point(((315.6667) * transformX) + moveX, ((268.0000) * transformY) + moveY),
        new createjs.Point(((315.3333) * transformX) + moveX, ((268.0000) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((268.0000) * transformY) + moveY),

        new createjs.Point(((314.6667) * transformX) + moveX, ((269.3332) * transformY) + moveY),
        new createjs.Point(((314.3333) * transformX) + moveX, ((270.6668) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((272.0000) * transformY) + moveY),

        new createjs.Point(((313.6667) * transformX) + moveX, ((272.0000) * transformY) + moveY),
        new createjs.Point(((313.3333) * transformX) + moveX, ((272.0000) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((272.0000) * transformY) + moveY),

        new createjs.Point(((313.0000) * transformX) + moveX, ((272.9999) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((274.0001) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((275.0000) * transformY) + moveY),

        new createjs.Point(((312.6667) * transformX) + moveX, ((275.0000) * transformY) + moveY),
        new createjs.Point(((312.3333) * transformX) + moveX, ((275.0000) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((275.0000) * transformY) + moveY),

        new createjs.Point(((311.6667) * transformX) + moveX, ((276.3332) * transformY) + moveY),
        new createjs.Point(((311.3333) * transformX) + moveX, ((277.6668) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((279.0000) * transformY) + moveY),

        new createjs.Point(((310.6667) * transformX) + moveX, ((279.0000) * transformY) + moveY),
        new createjs.Point(((310.3333) * transformX) + moveX, ((279.0000) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((279.0000) * transformY) + moveY),

        new createjs.Point(((310.0000) * transformX) + moveX, ((279.9999) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((281.0001) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((282.0000) * transformY) + moveY),

        new createjs.Point(((309.6667) * transformX) + moveX, ((282.0000) * transformY) + moveY),
        new createjs.Point(((309.3333) * transformX) + moveX, ((282.0000) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((282.0000) * transformY) + moveY),

        new createjs.Point(((309.0000) * transformX) + moveX, ((282.6666) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((283.3334) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((284.0000) * transformY) + moveY),

        new createjs.Point(((308.6667) * transformX) + moveX, ((284.0000) * transformY) + moveY),
        new createjs.Point(((308.3333) * transformX) + moveX, ((284.0000) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((284.0000) * transformY) + moveY),

        new createjs.Point(((307.6667) * transformX) + moveX, ((285.9998) * transformY) + moveY),
        new createjs.Point(((307.3333) * transformX) + moveX, ((288.0002) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((290.0000) * transformY) + moveY),

        new createjs.Point(((306.6667) * transformX) + moveX, ((290.0000) * transformY) + moveY),
        new createjs.Point(((306.3333) * transformX) + moveX, ((290.0000) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((290.0000) * transformY) + moveY),

        new createjs.Point(((306.0000) * transformX) + moveX, ((290.6666) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((291.3334) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((292.0000) * transformY) + moveY),

        new createjs.Point(((305.6667) * transformX) + moveX, ((292.0000) * transformY) + moveY),
        new createjs.Point(((305.3333) * transformX) + moveX, ((292.0000) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((292.0000) * transformY) + moveY),

        new createjs.Point(((304.6667) * transformX) + moveX, ((293.9998) * transformY) + moveY),
        new createjs.Point(((304.3333) * transformX) + moveX, ((296.0002) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((298.0000) * transformY) + moveY),

        new createjs.Point(((303.6667) * transformX) + moveX, ((298.0000) * transformY) + moveY),
        new createjs.Point(((303.3333) * transformX) + moveX, ((298.0000) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((298.0000) * transformY) + moveY),

        new createjs.Point(((303.0000) * transformX) + moveX, ((298.6666) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((299.3334) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((300.0000) * transformY) + moveY),

        new createjs.Point(((302.6667) * transformX) + moveX, ((300.0000) * transformY) + moveY),
        new createjs.Point(((302.3333) * transformX) + moveX, ((300.0000) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((300.0000) * transformY) + moveY),

        new createjs.Point(((297.6671) * transformX) + moveX, ((314.6652) * transformY) + moveY),
        new createjs.Point(((293.3329) * transformX) + moveX, ((329.3348) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((344.0000) * transformY) + moveY),

        new createjs.Point(((289.0000) * transformX) + moveX, ((345.6665) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((347.3335) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((349.0000) * transformY) + moveY),

        new createjs.Point(((288.6667) * transformX) + moveX, ((349.0000) * transformY) + moveY),
        new createjs.Point(((288.3333) * transformX) + moveX, ((349.0000) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((349.0000) * transformY) + moveY),

        new createjs.Point(((288.0000) * transformX) + moveX, ((350.3332) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((351.6668) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((353.0000) * transformY) + moveY),

        new createjs.Point(((287.6667) * transformX) + moveX, ((353.0000) * transformY) + moveY),
        new createjs.Point(((287.3333) * transformX) + moveX, ((353.0000) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((353.0000) * transformY) + moveY),

        new createjs.Point(((287.0000) * transformX) + moveX, ((354.6665) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((356.3335) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((358.0000) * transformY) + moveY),

        new createjs.Point(((286.6667) * transformX) + moveX, ((358.0000) * transformY) + moveY),
        new createjs.Point(((286.3333) * transformX) + moveX, ((358.0000) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((358.0000) * transformY) + moveY),

        new createjs.Point(((286.0000) * transformX) + moveX, ((359.6665) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((361.3335) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((363.0000) * transformY) + moveY),

        new createjs.Point(((285.6667) * transformX) + moveX, ((363.0000) * transformY) + moveY),
        new createjs.Point(((285.3333) * transformX) + moveX, ((363.0000) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((363.0000) * transformY) + moveY),

        new createjs.Point(((285.0000) * transformX) + moveX, ((364.6665) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((366.3335) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((368.0000) * transformY) + moveY),

        new createjs.Point(((284.6667) * transformX) + moveX, ((368.0000) * transformY) + moveY),
        new createjs.Point(((284.3333) * transformX) + moveX, ((368.0000) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((368.0000) * transformY) + moveY),

        new createjs.Point(((283.6667) * transformX) + moveX, ((371.9996) * transformY) + moveY),
        new createjs.Point(((283.3333) * transformX) + moveX, ((376.0004) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((380.0000) * transformY) + moveY),

        new createjs.Point(((282.6667) * transformX) + moveX, ((380.0000) * transformY) + moveY),
        new createjs.Point(((282.3333) * transformX) + moveX, ((380.0000) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((380.0000) * transformY) + moveY),

        new createjs.Point(((282.0000) * transformX) + moveX, ((382.3331) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((384.6669) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((387.0000) * transformY) + moveY),

        new createjs.Point(((281.6667) * transformX) + moveX, ((387.0000) * transformY) + moveY),
        new createjs.Point(((281.3333) * transformX) + moveX, ((387.0000) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((387.0000) * transformY) + moveY),

        new createjs.Point(((281.0000) * transformX) + moveX, ((389.6664) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((392.3336) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((395.0000) * transformY) + moveY),

        new createjs.Point(((280.6667) * transformX) + moveX, ((395.0000) * transformY) + moveY),
        new createjs.Point(((280.3333) * transformX) + moveX, ((395.0000) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((395.0000) * transformY) + moveY),

        new createjs.Point(((280.0000) * transformX) + moveX, ((397.9997) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((401.0003) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((404.0000) * transformY) + moveY),

        new createjs.Point(((279.6667) * transformX) + moveX, ((404.0000) * transformY) + moveY),
        new createjs.Point(((279.3333) * transformX) + moveX, ((404.0000) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((404.0000) * transformY) + moveY),

        new createjs.Point(((279.0000) * transformX) + moveX, ((408.6662) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((413.3338) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((418.0000) * transformY) + moveY),

        new createjs.Point(((276.2452) * transformX) + moveX, ((428.0495) * transformY) + moveY),
        new createjs.Point(((277.4215) * transformX) + moveX, ((451.0759) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((460.0000) * transformY) + moveY),

        new createjs.Point(((280.0000) * transformX) + moveX, ((462.3331) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((464.6669) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((467.0000) * transformY) + moveY),

        new createjs.Point(((280.3333) * transformX) + moveX, ((467.0000) * transformY) + moveY),
        new createjs.Point(((280.6667) * transformX) + moveX, ((467.0000) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((467.0000) * transformY) + moveY),

        new createjs.Point(((281.0000) * transformX) + moveX, ((468.6665) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((470.3335) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((472.0000) * transformY) + moveY),

        new createjs.Point(((281.3333) * transformX) + moveX, ((472.0000) * transformY) + moveY),
        new createjs.Point(((281.6667) * transformX) + moveX, ((472.0000) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((472.0000) * transformY) + moveY),

        new createjs.Point(((282.0000) * transformX) + moveX, ((473.6665) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((475.3335) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((477.0000) * transformY) + moveY),

        new createjs.Point(((282.3333) * transformX) + moveX, ((477.0000) * transformY) + moveY),
        new createjs.Point(((282.6667) * transformX) + moveX, ((477.0000) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((477.0000) * transformY) + moveY),

        new createjs.Point(((283.3333) * transformX) + moveX, ((479.6664) * transformY) + moveY),
        new createjs.Point(((283.6667) * transformX) + moveX, ((482.3336) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((485.0000) * transformY) + moveY),

        new createjs.Point(((284.3333) * transformX) + moveX, ((485.0000) * transformY) + moveY),
        new createjs.Point(((284.6667) * transformX) + moveX, ((485.0000) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((485.0000) * transformY) + moveY),

        new createjs.Point(((285.0000) * transformX) + moveX, ((486.3332) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((487.6668) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((285.3333) * transformX) + moveX, ((489.0000) * transformY) + moveY),
        new createjs.Point(((285.6667) * transformX) + moveX, ((489.0000) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((286.3333) * transformX) + moveX, ((491.3331) * transformY) + moveY),
        new createjs.Point(((286.6667) * transformX) + moveX, ((493.6669) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((496.0000) * transformY) + moveY),

        new createjs.Point(((287.3333) * transformX) + moveX, ((496.0000) * transformY) + moveY),
        new createjs.Point(((287.6667) * transformX) + moveX, ((496.0000) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((496.0000) * transformY) + moveY),


        new createjs.Point(((288.0000) * transformX) + moveX, ((496.9999) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((498.0001) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),

        new createjs.Point(((288.3333) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((288.6667) * transformX) + moveX, ((499.0000) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((499.0000) * transformY) + moveY),

        new createjs.Point(((289.0000) * transformX) + moveX, ((499.9999) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((501.0001) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),

        new createjs.Point(((289.3333) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((289.6667) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((290.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),

        new createjs.Point(((290.3333) * transformX) + moveX, ((503.9998) * transformY) + moveY),
        new createjs.Point(((290.6667) * transformX) + moveX, ((506.0002) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((291.3333) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((291.6667) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((292.0000) * transformX) + moveX, ((508.6666) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((509.3334) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((292.3333) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((292.6667) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((293.3333) * transformX) + moveX, ((511.9998) * transformY) + moveY),
        new createjs.Point(((293.6667) * transformX) + moveX, ((514.0002) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((294.3333) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((294.6667) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((295.3333) * transformX) + moveX, ((517.3332) * transformY) + moveY),
        new createjs.Point(((295.6667) * transformX) + moveX, ((518.6668) * transformY) + moveY),
        new createjs.Point(((296.0000) * transformX) + moveX, ((520.0000) * transformY) + moveY),

        new createjs.Point(((296.3333) * transformX) + moveX, ((520.0000) * transformY) + moveY),
        new createjs.Point(((296.6667) * transformX) + moveX, ((520.0000) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((520.0000) * transformY) + moveY),

        new createjs.Point(((297.0000) * transformX) + moveX, ((520.9999) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((522.0001) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((523.0000) * transformY) + moveY),

        new createjs.Point(((297.3333) * transformX) + moveX, ((523.0000) * transformY) + moveY),
        new createjs.Point(((297.6667) * transformX) + moveX, ((523.0000) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((523.0000) * transformY) + moveY),

        new createjs.Point(((298.3333) * transformX) + moveX, ((524.3332) * transformY) + moveY),
        new createjs.Point(((298.6667) * transformX) + moveX, ((525.6668) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((527.0000) * transformY) + moveY),

        new createjs.Point(((299.3333) * transformX) + moveX, ((527.0000) * transformY) + moveY),
        new createjs.Point(((299.6667) * transformX) + moveX, ((527.0000) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((527.0000) * transformY) + moveY),

        new createjs.Point(((300.0000) * transformX) + moveX, ((527.9999) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((529.0001) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((530.0000) * transformY) + moveY),

        new createjs.Point(((300.3333) * transformX) + moveX, ((530.0000) * transformY) + moveY),
        new createjs.Point(((300.6667) * transformX) + moveX, ((530.0000) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((530.0000) * transformY) + moveY),

        new createjs.Point(((301.0000) * transformX) + moveX, ((530.6666) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((531.3334) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((532.0000) * transformY) + moveY),

        new createjs.Point(((301.3333) * transformX) + moveX, ((532.0000) * transformY) + moveY),
        new createjs.Point(((301.6667) * transformX) + moveX, ((532.0000) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((532.0000) * transformY) + moveY),

        new createjs.Point(((302.3333) * transformX) + moveX, ((533.3332) * transformY) + moveY),
        new createjs.Point(((302.6667) * transformX) + moveX, ((534.6668) * transformY) + moveY),
        new createjs.Point(((303.0000) * transformX) + moveX, ((536.0000) * transformY) + moveY),

        new createjs.Point(((303.3333) * transformX) + moveX, ((536.0000) * transformY) + moveY),
        new createjs.Point(((303.6667) * transformX) + moveX, ((536.0000) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((536.0000) * transformY) + moveY),

        new createjs.Point(((304.3333) * transformX) + moveX, ((537.3332) * transformY) + moveY),
        new createjs.Point(((304.6667) * transformX) + moveX, ((538.6668) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((540.0000) * transformY) + moveY),

        new createjs.Point(((305.3333) * transformX) + moveX, ((540.0000) * transformY) + moveY),
        new createjs.Point(((305.6667) * transformX) + moveX, ((540.0000) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((540.0000) * transformY) + moveY),

        new createjs.Point(((306.0000) * transformX) + moveX, ((540.6666) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((541.3334) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((542.0000) * transformY) + moveY),

        new createjs.Point(((306.3333) * transformX) + moveX, ((542.0000) * transformY) + moveY),
        new createjs.Point(((306.6667) * transformX) + moveX, ((542.0000) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((542.0000) * transformY) + moveY),

        new createjs.Point(((307.0000) * transformX) + moveX, ((542.6666) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((543.3334) * transformY) + moveY),
        new createjs.Point(((307.0000) * transformX) + moveX, ((544.0000) * transformY) + moveY),

        new createjs.Point(((307.3333) * transformX) + moveX, ((544.0000) * transformY) + moveY),
        new createjs.Point(((307.6667) * transformX) + moveX, ((544.0000) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((544.0000) * transformY) + moveY),

        new createjs.Point(((308.0000) * transformX) + moveX, ((544.6666) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((545.3334) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((546.0000) * transformY) + moveY),

        new createjs.Point(((308.3333) * transformX) + moveX, ((546.0000) * transformY) + moveY),
        new createjs.Point(((308.6667) * transformX) + moveX, ((546.0000) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((546.0000) * transformY) + moveY),

        new createjs.Point(((309.0000) * transformX) + moveX, ((546.6666) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((547.3334) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((548.0000) * transformY) + moveY),

        new createjs.Point(((309.3333) * transformX) + moveX, ((548.0000) * transformY) + moveY),
        new createjs.Point(((309.6667) * transformX) + moveX, ((548.0000) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((548.0000) * transformY) + moveY),

        new createjs.Point(((310.0000) * transformX) + moveX, ((548.6666) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((549.3334) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((550.0000) * transformY) + moveY),

        new createjs.Point(((310.3333) * transformX) + moveX, ((550.0000) * transformY) + moveY),
        new createjs.Point(((310.6667) * transformX) + moveX, ((550.0000) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((550.0000) * transformY) + moveY),

        new createjs.Point(((311.0000) * transformX) + moveX, ((550.6666) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((551.3334) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((552.0000) * transformY) + moveY),

        new createjs.Point(((311.3333) * transformX) + moveX, ((552.0000) * transformY) + moveY),
        new createjs.Point(((311.6667) * transformX) + moveX, ((552.0000) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((552.0000) * transformY) + moveY),

        new createjs.Point(((312.0000) * transformX) + moveX, ((552.6666) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((553.3334) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((554.0000) * transformY) + moveY),

        new createjs.Point(((312.3333) * transformX) + moveX, ((554.0000) * transformY) + moveY),
        new createjs.Point(((312.6667) * transformX) + moveX, ((554.0000) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((554.0000) * transformY) + moveY),

        new createjs.Point(((313.0000) * transformX) + moveX, ((554.6666) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((555.3334) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((556.0000) * transformY) + moveY),

        new createjs.Point(((313.3333) * transformX) + moveX, ((556.0000) * transformY) + moveY),
        new createjs.Point(((313.6667) * transformX) + moveX, ((556.0000) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((556.0000) * transformY) + moveY),

        new createjs.Point(((314.0000) * transformX) + moveX, ((556.6666) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((557.3334) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((558.0000) * transformY) + moveY),

        new createjs.Point(((314.3333) * transformX) + moveX, ((558.0000) * transformY) + moveY),
        new createjs.Point(((314.6667) * transformX) + moveX, ((558.0000) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((558.0000) * transformY) + moveY),

        new createjs.Point(((315.0000) * transformX) + moveX, ((558.6666) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((559.3334) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((560.0000) * transformY) + moveY),

        new createjs.Point(((315.3333) * transformX) + moveX, ((560.0000) * transformY) + moveY),
        new createjs.Point(((315.6667) * transformX) + moveX, ((560.0000) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((560.0000) * transformY) + moveY),

        new createjs.Point(((316.0000) * transformX) + moveX, ((560.6666) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((561.3334) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((562.0000) * transformY) + moveY),

        new createjs.Point(((316.6666) * transformX) + moveX, ((562.3333) * transformY) + moveY),
        new createjs.Point(((317.3334) * transformX) + moveX, ((562.6667) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((563.0000) * transformY) + moveY),

        new createjs.Point(((318.6666) * transformX) + moveX, ((564.9998) * transformY) + moveY),
        new createjs.Point(((319.3334) * transformX) + moveX, ((567.0002) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((569.0000) * transformY) + moveY),

        new createjs.Point(((320.6666) * transformX) + moveX, ((569.3333) * transformY) + moveY),
        new createjs.Point(((321.3334) * transformX) + moveX, ((569.6667) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((570.0000) * transformY) + moveY),

        new createjs.Point(((322.6666) * transformX) + moveX, ((571.9998) * transformY) + moveY),
        new createjs.Point(((323.3334) * transformX) + moveX, ((574.0002) * transformY) + moveY),
        new createjs.Point(((324.0000) * transformX) + moveX, ((576.0000) * transformY) + moveY),

        new createjs.Point(((324.6666) * transformX) + moveX, ((576.3333) * transformY) + moveY),
        new createjs.Point(((325.3334) * transformX) + moveX, ((576.6667) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((577.0000) * transformY) + moveY),

        new createjs.Point(((326.0000) * transformX) + moveX, ((577.6666) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((578.3334) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((579.0000) * transformY) + moveY),

        new createjs.Point(((326.3333) * transformX) + moveX, ((579.0000) * transformY) + moveY),
        new createjs.Point(((326.6667) * transformX) + moveX, ((579.0000) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((579.0000) * transformY) + moveY),

        new createjs.Point(((327.0000) * transformX) + moveX, ((579.6666) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((580.3334) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((581.0000) * transformY) + moveY),

        new createjs.Point(((327.6666) * transformX) + moveX, ((581.3333) * transformY) + moveY),
        new createjs.Point(((328.3334) * transformX) + moveX, ((581.6667) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((582.0000) * transformY) + moveY),

        new createjs.Point(((329.0000) * transformX) + moveX, ((582.6666) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((583.3334) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((584.0000) * transformY) + moveY),

        new createjs.Point(((329.6666) * transformX) + moveX, ((584.3333) * transformY) + moveY),
        new createjs.Point(((330.3334) * transformX) + moveX, ((584.6667) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((585.0000) * transformY) + moveY),

        new createjs.Point(((331.3333) * transformX) + moveX, ((586.3332) * transformY) + moveY),
        new createjs.Point(((331.6667) * transformX) + moveX, ((587.6668) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((589.0000) * transformY) + moveY),

        new createjs.Point(((332.6666) * transformX) + moveX, ((589.3333) * transformY) + moveY),
        new createjs.Point(((333.3334) * transformX) + moveX, ((589.6667) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((590.0000) * transformY) + moveY),

        new createjs.Point(((334.0000) * transformX) + moveX, ((590.6666) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((591.3334) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((592.0000) * transformY) + moveY),

        new createjs.Point(((334.6666) * transformX) + moveX, ((592.3333) * transformY) + moveY),
        new createjs.Point(((335.3334) * transformX) + moveX, ((592.6667) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((593.0000) * transformY) + moveY),

        new createjs.Point(((336.0000) * transformX) + moveX, ((593.6666) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((594.3334) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((595.0000) * transformY) + moveY),

        new createjs.Point(((336.6666) * transformX) + moveX, ((595.3333) * transformY) + moveY),
        new createjs.Point(((337.3334) * transformX) + moveX, ((595.6667) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((596.0000) * transformY) + moveY),

        new createjs.Point(((338.3333) * transformX) + moveX, ((597.3332) * transformY) + moveY),
        new createjs.Point(((338.6667) * transformX) + moveX, ((598.6668) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((600.0000) * transformY) + moveY),

        new createjs.Point(((339.3333) * transformX) + moveX, ((600.0000) * transformY) + moveY),
        new createjs.Point(((339.6667) * transformX) + moveX, ((600.0000) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((600.0000) * transformY) + moveY),

        new createjs.Point(((340.3333) * transformX) + moveX, ((600.9999) * transformY) + moveY),
        new createjs.Point(((340.6667) * transformX) + moveX, ((602.0001) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((603.0000) * transformY) + moveY),

        new createjs.Point(((341.6666) * transformX) + moveX, ((603.3333) * transformY) + moveY),
        new createjs.Point(((342.3334) * transformX) + moveX, ((603.6667) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((604.0000) * transformY) + moveY),

        new createjs.Point(((343.0000) * transformX) + moveX, ((604.6666) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((605.3334) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((606.0000) * transformY) + moveY),

        new createjs.Point(((343.6666) * transformX) + moveX, ((606.3333) * transformY) + moveY),
        new createjs.Point(((344.3334) * transformX) + moveX, ((606.6667) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((607.0000) * transformY) + moveY),

        new createjs.Point(((345.0000) * transformX) + moveX, ((607.6666) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((608.3334) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((609.0000) * transformY) + moveY),

        new createjs.Point(((345.9999) * transformX) + moveX, ((609.6666) * transformY) + moveY),
        new createjs.Point(((347.0001) * transformX) + moveX, ((610.3334) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((611.0000) * transformY) + moveY),

        new createjs.Point(((348.0000) * transformX) + moveX, ((611.6666) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((612.3334) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((613.0000) * transformY) + moveY),

        new createjs.Point(((348.3333) * transformX) + moveX, ((613.0000) * transformY) + moveY),
        new createjs.Point(((348.6667) * transformX) + moveX, ((613.0000) * transformY) + moveY),
        new createjs.Point(((349.0000) * transformX) + moveX, ((613.0000) * transformY) + moveY),

        new createjs.Point(((349.3333) * transformX) + moveX, ((613.9999) * transformY) + moveY),
        new createjs.Point(((349.6667) * transformX) + moveX, ((615.0001) * transformY) + moveY),
        new createjs.Point(((350.0000) * transformX) + moveX, ((616.0000) * transformY) + moveY),

        new createjs.Point(((350.6666) * transformX) + moveX, ((616.3333) * transformY) + moveY),
        new createjs.Point(((351.3334) * transformX) + moveX, ((616.6667) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((617.0000) * transformY) + moveY),

        new createjs.Point(((352.0000) * transformX) + moveX, ((617.6666) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((618.3334) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((619.0000) * transformY) + moveY),

        new createjs.Point(((352.9999) * transformX) + moveX, ((619.6666) * transformY) + moveY),
        new createjs.Point(((354.0001) * transformX) + moveX, ((620.3334) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),

        new createjs.Point(((355.0000) * transformX) + moveX, ((621.6666) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((622.3334) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((623.0000) * transformY) + moveY),

        new createjs.Point(((355.6666) * transformX) + moveX, ((623.3333) * transformY) + moveY),
        new createjs.Point(((356.3334) * transformX) + moveX, ((623.6667) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((624.0000) * transformY) + moveY),

        new createjs.Point(((357.0000) * transformX) + moveX, ((624.6666) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((625.3334) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((626.0000) * transformY) + moveY),

        new createjs.Point(((357.9999) * transformX) + moveX, ((626.6666) * transformY) + moveY),
        new createjs.Point(((359.0001) * transformX) + moveX, ((627.3334) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((628.0000) * transformY) + moveY),

        new createjs.Point(((360.0000) * transformX) + moveX, ((628.6666) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((629.3334) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((630.0000) * transformY) + moveY),

        new createjs.Point(((360.6666) * transformX) + moveX, ((630.3333) * transformY) + moveY),
        new createjs.Point(((361.3334) * transformX) + moveX, ((630.6667) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((631.0000) * transformY) + moveY),

        new createjs.Point(((362.0000) * transformX) + moveX, ((631.6666) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((632.3334) * transformY) + moveY),
        new createjs.Point(((362.0000) * transformX) + moveX, ((633.0000) * transformY) + moveY),

        new createjs.Point(((362.6666) * transformX) + moveX, ((633.3333) * transformY) + moveY),
        new createjs.Point(((363.3334) * transformX) + moveX, ((633.6667) * transformY) + moveY),
        new createjs.Point(((364.0000) * transformX) + moveX, ((634.0000) * transformY) + moveY),

        new createjs.Point(((364.3333) * transformX) + moveX, ((634.9999) * transformY) + moveY),
        new createjs.Point(((364.6667) * transformX) + moveX, ((636.0001) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((637.0000) * transformY) + moveY),

        new createjs.Point(((365.9999) * transformX) + moveX, ((637.6666) * transformY) + moveY),
        new createjs.Point(((367.0001) * transformX) + moveX, ((638.3334) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((639.0000) * transformY) + moveY),

        new createjs.Point(((368.0000) * transformX) + moveX, ((639.6666) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((640.3334) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((641.0000) * transformY) + moveY),

        new createjs.Point(((368.9999) * transformX) + moveX, ((641.6666) * transformY) + moveY),
        new createjs.Point(((370.0001) * transformX) + moveX, ((642.3334) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((643.0000) * transformY) + moveY),

        new createjs.Point(((371.0000) * transformX) + moveX, ((643.6666) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((644.3334) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((645.0000) * transformY) + moveY),

        new createjs.Point(((371.9999) * transformX) + moveX, ((645.6666) * transformY) + moveY),
        new createjs.Point(((373.0001) * transformX) + moveX, ((646.3334) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((647.0000) * transformY) + moveY),

        new createjs.Point(((374.0000) * transformX) + moveX, ((647.6666) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((648.3334) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((649.0000) * transformY) + moveY),

        new createjs.Point(((374.9999) * transformX) + moveX, ((649.6666) * transformY) + moveY),
        new createjs.Point(((376.0001) * transformX) + moveX, ((650.3334) * transformY) + moveY),
        new createjs.Point(((377.0000) * transformX) + moveX, ((651.0000) * transformY) + moveY),

        new createjs.Point(((377.0000) * transformX) + moveX, ((651.6666) * transformY) + moveY),
        new createjs.Point(((377.0000) * transformX) + moveX, ((652.3334) * transformY) + moveY),
        new createjs.Point(((377.0000) * transformX) + moveX, ((653.0000) * transformY) + moveY),

        new createjs.Point(((378.3332) * transformX) + moveX, ((653.9999) * transformY) + moveY),
        new createjs.Point(((379.6668) * transformX) + moveX, ((655.0001) * transformY) + moveY),
        new createjs.Point(((381.0000) * transformX) + moveX, ((656.0000) * transformY) + moveY),

        new createjs.Point(((381.0000) * transformX) + moveX, ((656.6666) * transformY) + moveY),
        new createjs.Point(((381.0000) * transformX) + moveX, ((657.3334) * transformY) + moveY),
        new createjs.Point(((381.0000) * transformX) + moveX, ((658.0000) * transformY) + moveY),

        new createjs.Point(((382.3332) * transformX) + moveX, ((658.9999) * transformY) + moveY),
        new createjs.Point(((383.6668) * transformX) + moveX, ((660.0001) * transformY) + moveY),
        new createjs.Point(((385.0000) * transformX) + moveX, ((661.0000) * transformY) + moveY),

        new createjs.Point(((385.3333) * transformX) + moveX, ((661.9999) * transformY) + moveY),
        new createjs.Point(((385.6667) * transformX) + moveX, ((663.0001) * transformY) + moveY),
        new createjs.Point(((386.0000) * transformX) + moveX, ((664.0000) * transformY) + moveY),

        new createjs.Point(((386.6666) * transformX) + moveX, ((664.3333) * transformY) + moveY),
        new createjs.Point(((387.3334) * transformX) + moveX, ((664.6667) * transformY) + moveY),
        new createjs.Point(((388.0000) * transformX) + moveX, ((665.0000) * transformY) + moveY),

        new createjs.Point(((388.0000) * transformX) + moveX, ((665.6666) * transformY) + moveY),
        new createjs.Point(((388.0000) * transformX) + moveX, ((666.3334) * transformY) + moveY),
        new createjs.Point(((388.0000) * transformX) + moveX, ((667.0000) * transformY) + moveY),

        new createjs.Point(((389.3332) * transformX) + moveX, ((667.9999) * transformY) + moveY),
        new createjs.Point(((390.6668) * transformX) + moveX, ((669.0001) * transformY) + moveY),
        new createjs.Point(((392.0000) * transformX) + moveX, ((670.0000) * transformY) + moveY),

        new createjs.Point(((392.0000) * transformX) + moveX, ((670.6666) * transformY) + moveY),
        new createjs.Point(((392.0000) * transformX) + moveX, ((671.3334) * transformY) + moveY),
        new createjs.Point(((392.0000) * transformX) + moveX, ((672.0000) * transformY) + moveY),

        new createjs.Point(((393.6665) * transformX) + moveX, ((673.3332) * transformY) + moveY),
        new createjs.Point(((395.3335) * transformX) + moveX, ((674.6668) * transformY) + moveY),
        new createjs.Point(((397.0000) * transformX) + moveX, ((676.0000) * transformY) + moveY),

        new createjs.Point(((397.0000) * transformX) + moveX, ((676.6666) * transformY) + moveY),
        new createjs.Point(((397.0000) * transformX) + moveX, ((677.3334) * transformY) + moveY),
        new createjs.Point(((397.0000) * transformX) + moveX, ((678.0000) * transformY) + moveY),

        new createjs.Point(((397.6666) * transformX) + moveX, ((678.3333) * transformY) + moveY),
        new createjs.Point(((398.3334) * transformX) + moveX, ((678.6667) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((679.0000) * transformY) + moveY),

        new createjs.Point(((399.0000) * transformX) + moveX, ((679.3333) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((679.6667) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((680.0000) * transformY) + moveY),

        new createjs.Point(((399.6666) * transformX) + moveX, ((680.3333) * transformY) + moveY),
        new createjs.Point(((400.3334) * transformX) + moveX, ((680.6667) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((681.0000) * transformY) + moveY),

        new createjs.Point(((401.0000) * transformX) + moveX, ((681.6666) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((682.3334) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((683.0000) * transformY) + moveY),

        new createjs.Point(((402.6665) * transformX) + moveX, ((684.3332) * transformY) + moveY),
        new createjs.Point(((404.3335) * transformX) + moveX, ((685.6668) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((687.0000) * transformY) + moveY),

        new createjs.Point(((406.0000) * transformX) + moveX, ((687.6666) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((688.3334) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((689.0000) * transformY) + moveY),

        new createjs.Point(((407.6665) * transformX) + moveX, ((690.3332) * transformY) + moveY),
        new createjs.Point(((409.3335) * transformX) + moveX, ((691.6668) * transformY) + moveY),
        new createjs.Point(((411.0000) * transformX) + moveX, ((693.0000) * transformY) + moveY),

        new createjs.Point(((411.0000) * transformX) + moveX, ((693.6666) * transformY) + moveY),
        new createjs.Point(((411.0000) * transformX) + moveX, ((694.3334) * transformY) + moveY),
        new createjs.Point(((411.0000) * transformX) + moveX, ((695.0000) * transformY) + moveY),

        new createjs.Point(((412.9998) * transformX) + moveX, ((696.6665) * transformY) + moveY),
        new createjs.Point(((415.0002) * transformX) + moveX, ((698.3335) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((700.0000) * transformY) + moveY),

        new createjs.Point(((417.0000) * transformX) + moveX, ((700.6666) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((701.3334) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((702.0000) * transformY) + moveY),

        new createjs.Point(((418.9998) * transformX) + moveX, ((703.6665) * transformY) + moveY),
        new createjs.Point(((421.0002) * transformX) + moveX, ((705.3335) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((707.0000) * transformY) + moveY),

        new createjs.Point(((423.0000) * transformX) + moveX, ((707.6666) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((708.3334) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((709.0000) * transformY) + moveY),

        new createjs.Point(((423.9999) * transformX) + moveX, ((709.6666) * transformY) + moveY),
        new createjs.Point(((425.0001) * transformX) + moveX, ((710.3334) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((711.0000) * transformY) + moveY),

        new createjs.Point(((426.9999) * transformX) + moveX, ((712.3332) * transformY) + moveY),
        new createjs.Point(((428.0001) * transformX) + moveX, ((713.6668) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((715.0000) * transformY) + moveY),

        new createjs.Point(((429.3333) * transformX) + moveX, ((715.0000) * transformY) + moveY),
        new createjs.Point(((429.6667) * transformX) + moveX, ((715.0000) * transformY) + moveY),
        new createjs.Point(((430.0000) * transformX) + moveX, ((715.0000) * transformY) + moveY),

        new createjs.Point(((735.9999) * transformX) + moveX, ((937.6666) * transformY) + moveY),
        new createjs.Point(((737.0001) * transformX) + moveX, ((938.3334) * transformY) + moveY),
        new createjs.Point(((738.0000) * transformX) + moveX, ((939.0000) * transformY) + moveY),
        new createjs.Point(((738.0000) * transformX) + moveX, ((939.6666) * transformY) + moveY),
        new createjs.Point(((738.0000) * transformX) + moveX, ((940.3334) * transformY) + moveY),
        new createjs.Point(((738.0000) * transformX) + moveX, ((941.0000) * transformY) + moveY),
        new createjs.Point(((738.9999) * transformX) + moveX, ((941.6666) * transformY) + moveY),
        new createjs.Point(((740.0001) * transformX) + moveX, ((942.3334) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((943.0000) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((943.6666) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((944.3334) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((945.0000) * transformY) + moveY),
        new createjs.Point(((741.9999) * transformX) + moveX, ((945.6666) * transformY) + moveY),
        new createjs.Point(((743.0001) * transformX) + moveX, ((946.3334) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((947.0000) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((947.6666) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((948.3334) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((949.0000) * transformY) + moveY),
        new createjs.Point(((745.3332) * transformX) + moveX, ((949.9999) * transformY) + moveY),
        new createjs.Point(((746.6668) * transformX) + moveX, ((951.0001) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((952.6666) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((953.3334) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((954.0000) * transformY) + moveY),
        new createjs.Point(((748.9999) * transformX) + moveX, ((954.6666) * transformY) + moveY),
        new createjs.Point(((750.0001) * transformX) + moveX, ((955.3334) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((956.0000) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((956.6666) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((957.3334) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((958.0000) * transformY) + moveY),
        new createjs.Point(((751.9999) * transformX) + moveX, ((958.6666) * transformY) + moveY),
        new createjs.Point(((753.0001) * transformX) + moveX, ((959.3334) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((960.0000) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((960.6666) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((961.3334) * transformY) + moveY),
        new createjs.Point(((754.0000) * transformX) + moveX, ((962.0000) * transformY) + moveY),
        new createjs.Point(((754.6666) * transformX) + moveX, ((962.3333) * transformY) + moveY),
        new createjs.Point(((755.3334) * transformX) + moveX, ((962.6667) * transformY) + moveY),
        new createjs.Point(((756.0000) * transformX) + moveX, ((963.0000) * transformY) + moveY),
        new createjs.Point(((756.0000) * transformX) + moveX, ((963.6666) * transformY) + moveY),
        new createjs.Point(((756.0000) * transformX) + moveX, ((964.3334) * transformY) + moveY),
        new createjs.Point(((756.0000) * transformX) + moveX, ((965.0000) * transformY) + moveY),
        new createjs.Point(((756.9999) * transformX) + moveX, ((965.6666) * transformY) + moveY),
        new createjs.Point(((758.0001) * transformX) + moveX, ((966.3334) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((967.6666) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((968.3334) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((969.0000) * transformY) + moveY),
        new createjs.Point(((759.9999) * transformX) + moveX, ((969.6666) * transformY) + moveY),
        new createjs.Point(((761.0001) * transformX) + moveX, ((970.3334) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((971.6666) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((972.3334) * transformY) + moveY),
        new createjs.Point(((762.0000) * transformX) + moveX, ((973.0000) * transformY) + moveY),
        new createjs.Point(((762.9999) * transformX) + moveX, ((973.6666) * transformY) + moveY),
        new createjs.Point(((764.0001) * transformX) + moveX, ((974.3334) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((975.6666) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((976.3334) * transformY) + moveY),
        new createjs.Point(((765.0000) * transformX) + moveX, ((977.0000) * transformY) + moveY),
        new createjs.Point(((765.6666) * transformX) + moveX, ((977.3333) * transformY) + moveY),
        new createjs.Point(((766.3334) * transformX) + moveX, ((977.6667) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((978.0000) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((978.6666) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((979.3334) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((980.0000) * transformY) + moveY),
        new createjs.Point(((767.9999) * transformX) + moveX, ((980.6666) * transformY) + moveY),
        new createjs.Point(((769.0001) * transformX) + moveX, ((981.3334) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((982.0000) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((982.6666) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((983.3334) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((770.6666) * transformX) + moveX, ((984.3333) * transformY) + moveY),
        new createjs.Point(((771.3334) * transformX) + moveX, ((984.6667) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((985.0000) * transformY) + moveY),
        new createjs.Point(((772.3333) * transformX) + moveX, ((985.9999) * transformY) + moveY),
        new createjs.Point(((772.6667) * transformX) + moveX, ((987.0001) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((988.0000) * transformY) + moveY),
        new createjs.Point(((773.3333) * transformX) + moveX, ((988.0000) * transformY) + moveY),
        new createjs.Point(((773.6667) * transformX) + moveX, ((988.0000) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((988.0000) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((988.6666) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((989.3334) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((990.0000) * transformY) + moveY),
        new createjs.Point(((774.9999) * transformX) + moveX, ((990.6666) * transformY) + moveY),
        new createjs.Point(((776.0001) * transformX) + moveX, ((991.3334) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((992.0000) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((992.6666) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((993.3334) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((777.6666) * transformX) + moveX, ((994.3333) * transformY) + moveY),
        new createjs.Point(((778.3334) * transformX) + moveX, ((994.6667) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((995.0000) * transformY) + moveY),
        new createjs.Point(((779.3333) * transformX) + moveX, ((995.9999) * transformY) + moveY),
        new createjs.Point(((779.6667) * transformX) + moveX, ((997.0001) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((998.0000) * transformY) + moveY),
        new createjs.Point(((780.3333) * transformX) + moveX, ((998.0000) * transformY) + moveY),
        new createjs.Point(((780.6667) * transformX) + moveX, ((998.0000) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((998.0000) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((998.6666) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((999.3334) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1000.0000) * transformY) + moveY),
        new createjs.Point(((781.6666) * transformX) + moveX, ((1000.3333) * transformY) + moveY),
        new createjs.Point(((782.3334) * transformX) + moveX, ((1000.6667) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1001.0000) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1001.6666) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1002.3334) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1003.0000) * transformY) + moveY),
        new createjs.Point(((783.9999) * transformX) + moveX, ((1003.6666) * transformY) + moveY),
        new createjs.Point(((785.0001) * transformX) + moveX, ((1004.3334) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1005.0000) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1005.6666) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1006.3334) * transformY) + moveY),
        new createjs.Point(((786.0000) * transformX) + moveX, ((1007.0000) * transformY) + moveY),
        new createjs.Point(((786.6666) * transformX) + moveX, ((1007.3333) * transformY) + moveY),
        new createjs.Point(((787.3334) * transformX) + moveX, ((1007.6667) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1008.0000) * transformY) + moveY),
        new createjs.Point(((788.3333) * transformX) + moveX, ((1008.9999) * transformY) + moveY),
        new createjs.Point(((788.6667) * transformX) + moveX, ((1010.0001) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((1011.0000) * transformY) + moveY),
        new createjs.Point(((789.3333) * transformX) + moveX, ((1011.0000) * transformY) + moveY),
        new createjs.Point(((789.6667) * transformX) + moveX, ((1011.0000) * transformY) + moveY),
        new createjs.Point(((790.0000) * transformX) + moveX, ((1011.0000) * transformY) + moveY),
        new createjs.Point(((790.0000) * transformX) + moveX, ((1011.6666) * transformY) + moveY),
        new createjs.Point(((790.0000) * transformX) + moveX, ((1012.3334) * transformY) + moveY),
        new createjs.Point(((790.0000) * transformX) + moveX, ((1013.0000) * transformY) + moveY),
        new createjs.Point(((790.6666) * transformX) + moveX, ((1013.3333) * transformY) + moveY),
        new createjs.Point(((791.3334) * transformX) + moveX, ((1013.6667) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((1014.0000) * transformY) + moveY),
        new createjs.Point(((792.3333) * transformX) + moveX, ((1014.9999) * transformY) + moveY),
        new createjs.Point(((792.6667) * transformX) + moveX, ((1016.0001) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((793.3333) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((793.6667) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1017.6666) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1018.3334) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1019.0000) * transformY) + moveY),
        new createjs.Point(((794.6666) * transformX) + moveX, ((1019.3333) * transformY) + moveY),
        new createjs.Point(((795.3334) * transformX) + moveX, ((1019.6667) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((796.3333) * transformX) + moveX, ((1021.3332) * transformY) + moveY),
        new createjs.Point(((796.6667) * transformX) + moveX, ((1022.6668) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1024.0000) * transformY) + moveY),
        new createjs.Point(((797.6666) * transformX) + moveX, ((1024.3333) * transformY) + moveY),
        new createjs.Point(((798.3334) * transformX) + moveX, ((1024.6667) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1025.0000) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1025.6666) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1026.3334) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1027.0000) * transformY) + moveY),
        new createjs.Point(((799.6666) * transformX) + moveX, ((1027.3333) * transformY) + moveY),
        new createjs.Point(((800.3334) * transformX) + moveX, ((1027.6667) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1028.0000) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1028.6666) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1029.3334) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1030.0000) * transformY) + moveY),
        new createjs.Point(((801.6666) * transformX) + moveX, ((1030.3333) * transformY) + moveY),
        new createjs.Point(((802.3334) * transformX) + moveX, ((1030.6667) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1031.0000) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1031.6666) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1032.3334) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1033.0000) * transformY) + moveY),
        new createjs.Point(((803.6666) * transformX) + moveX, ((1033.3333) * transformY) + moveY),
        new createjs.Point(((804.3334) * transformX) + moveX, ((1033.6667) * transformY) + moveY),
        new createjs.Point(((805.0000) * transformX) + moveX, ((1034.0000) * transformY) + moveY),
        new createjs.Point(((805.3333) * transformX) + moveX, ((1035.3332) * transformY) + moveY),
        new createjs.Point(((805.6667) * transformX) + moveX, ((1036.6668) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1038.0000) * transformY) + moveY),
        new createjs.Point(((806.6666) * transformX) + moveX, ((1038.3333) * transformY) + moveY),
        new createjs.Point(((807.3334) * transformX) + moveX, ((1038.6667) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1039.0000) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1039.6666) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1040.3334) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1041.0000) * transformY) + moveY),
        new createjs.Point(((808.6666) * transformX) + moveX, ((1041.3333) * transformY) + moveY),
        new createjs.Point(((809.3334) * transformX) + moveX, ((1041.6667) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((1042.0000) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((1042.6666) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((1043.3334) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((1044.0000) * transformY) + moveY),
        new createjs.Point(((810.3333) * transformX) + moveX, ((1044.0000) * transformY) + moveY),
        new createjs.Point(((810.6667) * transformX) + moveX, ((1044.0000) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1044.0000) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1044.6666) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1045.3334) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1046.0000) * transformY) + moveY),
        new createjs.Point(((811.6666) * transformX) + moveX, ((1046.3333) * transformY) + moveY),
        new createjs.Point(((812.3334) * transformX) + moveX, ((1046.6667) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((1047.0000) * transformY) + moveY),
        new createjs.Point(((813.3333) * transformX) + moveX, ((1048.3332) * transformY) + moveY),
        new createjs.Point(((813.6667) * transformX) + moveX, ((1049.6668) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((814.6666) * transformX) + moveX, ((1051.3333) * transformY) + moveY),
        new createjs.Point(((815.3334) * transformX) + moveX, ((1051.6667) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((1052.0000) * transformY) + moveY),
        new createjs.Point(((816.3333) * transformX) + moveX, ((1053.3332) * transformY) + moveY),
        new createjs.Point(((816.6667) * transformX) + moveX, ((1054.6668) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((1056.0000) * transformY) + moveY),
        new createjs.Point(((817.6666) * transformX) + moveX, ((1056.3333) * transformY) + moveY),
        new createjs.Point(((818.3334) * transformX) + moveX, ((1056.6667) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1057.0000) * transformY) + moveY),
        new createjs.Point(((819.3333) * transformX) + moveX, ((1058.3332) * transformY) + moveY),
        new createjs.Point(((819.6667) * transformX) + moveX, ((1059.6668) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1061.0000) * transformY) + moveY),
        new createjs.Point(((820.6666) * transformX) + moveX, ((1061.3333) * transformY) + moveY),
        new createjs.Point(((821.3334) * transformX) + moveX, ((1061.6667) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((1062.0000) * transformY) + moveY),
        new createjs.Point(((822.6666) * transformX) + moveX, ((1063.9998) * transformY) + moveY),
        new createjs.Point(((823.3334) * transformX) + moveX, ((1066.0002) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((1068.0000) * transformY) + moveY),
        new createjs.Point(((824.6666) * transformX) + moveX, ((1068.3333) * transformY) + moveY),
        new createjs.Point(((825.3334) * transformX) + moveX, ((1068.6667) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1069.0000) * transformY) + moveY),
        new createjs.Point(((826.6666) * transformX) + moveX, ((1070.9998) * transformY) + moveY),
        new createjs.Point(((827.3334) * transformX) + moveX, ((1073.0002) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1075.0000) * transformY) + moveY),
        new createjs.Point(((828.6666) * transformX) + moveX, ((1075.3333) * transformY) + moveY),
        new createjs.Point(((829.3334) * transformX) + moveX, ((1075.6667) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1076.0000) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1076.6666) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1077.3334) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((830.3333) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((830.6667) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1078.0000) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1078.6666) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1079.3334) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1080.0000) * transformY) + moveY),
        new createjs.Point(((831.3333) * transformX) + moveX, ((1080.0000) * transformY) + moveY),
        new createjs.Point(((831.6667) * transformX) + moveX, ((1080.0000) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1080.0000) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1080.6666) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1081.3334) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((832.3333) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((832.6667) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1082.6666) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1083.3334) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1084.0000) * transformY) + moveY),
        new createjs.Point(((833.6666) * transformX) + moveX, ((1084.3333) * transformY) + moveY),
        new createjs.Point(((834.3334) * transformX) + moveX, ((1084.6667) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((1085.0000) * transformY) + moveY),
        new createjs.Point(((835.3333) * transformX) + moveX, ((1086.3332) * transformY) + moveY),
        new createjs.Point(((835.6667) * transformX) + moveX, ((1087.6668) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((836.3333) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((836.6667) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1089.0000) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1089.6666) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1090.3334) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((837.3333) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((837.6667) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1091.6666) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1092.3334) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1093.0000) * transformY) + moveY),
        new createjs.Point(((838.3333) * transformX) + moveX, ((1093.0000) * transformY) + moveY),
        new createjs.Point(((838.6667) * transformX) + moveX, ((1093.0000) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1093.0000) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1093.6666) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1094.3334) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((839.3333) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((839.6667) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1095.0000) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1095.6666) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1096.3334) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((840.3333) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((840.6667) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1097.6666) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1098.3334) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1099.0000) * transformY) + moveY),
        new createjs.Point(((841.6666) * transformX) + moveX, ((1099.3333) * transformY) + moveY),
        new createjs.Point(((842.3334) * transformX) + moveX, ((1099.6667) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1100.0000) * transformY) + moveY),
        new createjs.Point(((843.6666) * transformX) + moveX, ((1101.9998) * transformY) + moveY),
        new createjs.Point(((844.3334) * transformX) + moveX, ((1104.0002) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((845.3333) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((845.6667) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1106.0000) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1106.6666) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1107.3334) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1108.0000) * transformY) + moveY),
        new createjs.Point(((846.3333) * transformX) + moveX, ((1108.0000) * transformY) + moveY),
        new createjs.Point(((846.6667) * transformX) + moveX, ((1108.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1108.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1108.6666) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1109.3334) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((847.3333) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((847.6667) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((1110.0000) * transformY) + moveY),
        new createjs.Point(((848.6666) * transformX) + moveX, ((1111.9998) * transformY) + moveY),
        new createjs.Point(((849.3334) * transformX) + moveX, ((1114.0002) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1116.0000) * transformY) + moveY),
        new createjs.Point(((850.3333) * transformX) + moveX, ((1116.0000) * transformY) + moveY),
        new createjs.Point(((850.6667) * transformX) + moveX, ((1116.0000) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1116.0000) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1116.9999) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1118.0001) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((851.3333) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((851.6667) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((852.6666) * transformX) + moveX, ((1120.9998) * transformY) + moveY),
        new createjs.Point(((853.3334) * transformX) + moveX, ((1123.0002) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1125.0000) * transformY) + moveY),
        new createjs.Point(((854.3333) * transformX) + moveX, ((1125.0000) * transformY) + moveY),
        new createjs.Point(((854.6667) * transformX) + moveX, ((1125.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1125.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1125.6666) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1126.3334) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1127.0000) * transformY) + moveY),
        new createjs.Point(((855.3333) * transformX) + moveX, ((1127.0000) * transformY) + moveY),
        new createjs.Point(((855.6667) * transformX) + moveX, ((1127.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1127.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1127.6666) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1128.3334) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1129.0000) * transformY) + moveY),
        new createjs.Point(((856.3333) * transformX) + moveX, ((1129.0000) * transformY) + moveY),
        new createjs.Point(((856.6667) * transformX) + moveX, ((1129.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1129.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1129.6666) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1130.3334) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1131.0000) * transformY) + moveY),
        new createjs.Point(((857.3333) * transformX) + moveX, ((1131.0000) * transformY) + moveY),
        new createjs.Point(((857.6667) * transformX) + moveX, ((1131.0000) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1131.0000) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1131.9999) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1133.0001) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1134.0000) * transformY) + moveY),
        new createjs.Point(((858.3333) * transformX) + moveX, ((1134.0000) * transformY) + moveY),
        new createjs.Point(((858.6667) * transformX) + moveX, ((1134.0000) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1134.0000) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1134.6666) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1135.3334) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((859.3333) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((859.6667) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1136.6666) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1137.3334) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((860.3333) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((860.6667) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1138.6666) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1139.3334) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1140.0000) * transformY) + moveY),
        new createjs.Point(((861.3333) * transformX) + moveX, ((1140.0000) * transformY) + moveY),
        new createjs.Point(((861.6667) * transformX) + moveX, ((1140.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1140.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1140.9999) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1142.0001) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1143.0000) * transformY) + moveY),
        new createjs.Point(((862.3333) * transformX) + moveX, ((1143.0000) * transformY) + moveY),
        new createjs.Point(((862.6667) * transformX) + moveX, ((1143.0000) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((1143.0000) * transformY) + moveY),
        new createjs.Point(((863.3333) * transformX) + moveX, ((1144.3332) * transformY) + moveY),
        new createjs.Point(((863.6667) * transformX) + moveX, ((1145.6668) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1147.0000) * transformY) + moveY),
        new createjs.Point(((864.3333) * transformX) + moveX, ((1147.0000) * transformY) + moveY),
        new createjs.Point(((864.6667) * transformX) + moveX, ((1147.0000) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1147.0000) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1147.9999) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1149.0001) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((865.3333) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((865.6667) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((1150.0000) * transformY) + moveY),
        new createjs.Point(((866.3333) * transformX) + moveX, ((1151.3332) * transformY) + moveY),
        new createjs.Point(((866.6667) * transformX) + moveX, ((1152.6668) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1154.0000) * transformY) + moveY),
        new createjs.Point(((867.3333) * transformX) + moveX, ((1154.0000) * transformY) + moveY),
        new createjs.Point(((867.6667) * transformX) + moveX, ((1154.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1154.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1154.9999) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1156.0001) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((868.3333) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((868.6667) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1157.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1157.6666) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1158.3334) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1159.0000) * transformY) + moveY),
        new createjs.Point(((869.3333) * transformX) + moveX, ((1159.0000) * transformY) + moveY),
        new createjs.Point(((869.6667) * transformX) + moveX, ((1159.0000) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1159.0000) * transformY) + moveY),
        new createjs.Point(((870.3333) * transformX) + moveX, ((1160.6665) * transformY) + moveY),
        new createjs.Point(((870.6667) * transformX) + moveX, ((1162.3335) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1164.0000) * transformY) + moveY),
        new createjs.Point(((871.3333) * transformX) + moveX, ((1164.0000) * transformY) + moveY),
        new createjs.Point(((871.6667) * transformX) + moveX, ((1164.0000) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1164.0000) * transformY) + moveY),
        new createjs.Point(((872.3333) * transformX) + moveX, ((1165.9998) * transformY) + moveY),
        new createjs.Point(((872.6667) * transformX) + moveX, ((1168.0002) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((1170.0000) * transformY) + moveY),
        new createjs.Point(((873.3333) * transformX) + moveX, ((1170.0000) * transformY) + moveY),
        new createjs.Point(((873.6667) * transformX) + moveX, ((1170.0000) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1170.0000) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1170.6666) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1171.3334) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1172.0000) * transformY) + moveY),
        new createjs.Point(((874.3333) * transformX) + moveX, ((1172.0000) * transformY) + moveY),
        new createjs.Point(((874.6667) * transformX) + moveX, ((1172.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1172.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1172.9999) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1174.0001) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((875.3333) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((875.6667) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1175.9999) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1177.0001) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1178.0000) * transformY) + moveY),
        new createjs.Point(((876.3333) * transformX) + moveX, ((1178.0000) * transformY) + moveY),
        new createjs.Point(((876.6667) * transformX) + moveX, ((1178.0000) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1178.0000) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1178.9999) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1180.0001) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1181.0000) * transformY) + moveY),
        new createjs.Point(((877.3333) * transformX) + moveX, ((1181.0000) * transformY) + moveY),
        new createjs.Point(((877.6667) * transformX) + moveX, ((1181.0000) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1181.0000) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1181.9999) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1183.0001) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((878.3333) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((878.6667) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1184.6666) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1185.3334) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((879.3333) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((879.6667) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((880.6666) * transformX) + moveX, ((1189.3330) * transformY) + moveY),
        new createjs.Point(((881.3334) * transformX) + moveX, ((1192.6670) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1196.0000) * transformY) + moveY),
        new createjs.Point(((882.3333) * transformX) + moveX, ((1196.0000) * transformY) + moveY),
        new createjs.Point(((882.6667) * transformX) + moveX, ((1196.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1196.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1196.9999) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1198.0001) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((883.3333) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((883.6667) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1199.9999) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1201.0001) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1202.0000) * transformY) + moveY),
        new createjs.Point(((884.3333) * transformX) + moveX, ((1202.0000) * transformY) + moveY),
        new createjs.Point(((884.6667) * transformX) + moveX, ((1202.0000) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1202.0000) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1202.9999) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1204.0001) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((885.3333) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((885.6667) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((886.3333) * transformX) + moveX, ((1207.3331) * transformY) + moveY),
        new createjs.Point(((886.6667) * transformX) + moveX, ((1209.6669) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1212.0000) * transformY) + moveY),
        new createjs.Point(((887.3333) * transformX) + moveX, ((1212.0000) * transformY) + moveY),
        new createjs.Point(((887.6667) * transformX) + moveX, ((1212.0000) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((1212.0000) * transformY) + moveY),
        new createjs.Point(((888.3333) * transformX) + moveX, ((1214.6664) * transformY) + moveY),
        new createjs.Point(((888.6667) * transformX) + moveX, ((1217.3336) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((889.3333) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((889.6667) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1221.3332) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1222.6668) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1224.0000) * transformY) + moveY),
        new createjs.Point(((890.3333) * transformX) + moveX, ((1224.0000) * transformY) + moveY),
        new createjs.Point(((890.6667) * transformX) + moveX, ((1224.0000) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1224.0000) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1225.3332) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1226.6668) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((891.3333) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((891.6667) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((1229.3332) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((1230.6668) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((1232.0000) * transformY) + moveY),
        new createjs.Point(((892.3333) * transformX) + moveX, ((1232.0000) * transformY) + moveY),
        new createjs.Point(((892.6667) * transformX) + moveX, ((1232.0000) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1232.0000) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1233.6665) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1235.3335) * transformY) + moveY),
        new createjs.Point(((893.0000) * transformX) + moveX, ((1237.0000) * transformY) + moveY),
        new createjs.Point(((893.3333) * transformX) + moveX, ((1237.0000) * transformY) + moveY),
        new createjs.Point(((893.6667) * transformX) + moveX, ((1237.0000) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((1237.0000) * transformY) + moveY),
        new createjs.Point(((894.3333) * transformX) + moveX, ((1240.3330) * transformY) + moveY),
        new createjs.Point(((894.6667) * transformX) + moveX, ((1243.6670) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((895.3333) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((895.6667) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((896.6666) * transformX) + moveX, ((1252.6661) * transformY) + moveY),
        new createjs.Point(((897.3334) * transformX) + moveX, ((1258.3339) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1264.0000) * transformY) + moveY),
        new createjs.Point(((898.3333) * transformX) + moveX, ((1264.0000) * transformY) + moveY),
        new createjs.Point(((898.6667) * transformX) + moveX, ((1264.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1264.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1266.6664) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1269.3336) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1272.0000) * transformY) + moveY),
        new createjs.Point(((899.3333) * transformX) + moveX, ((1272.0000) * transformY) + moveY),
        new createjs.Point(((899.6667) * transformX) + moveX, ((1272.0000) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1272.0000) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1274.9997) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1278.0003) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1281.0000) * transformY) + moveY),
        new createjs.Point(((900.3333) * transformX) + moveX, ((1281.0000) * transformY) + moveY),
        new createjs.Point(((900.6667) * transformX) + moveX, ((1281.0000) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1281.0000) * transformY) + moveY),
        new createjs.Point(((901.3333) * transformX) + moveX, ((1292.9988) * transformY) + moveY),
        new createjs.Point(((901.6667) * transformX) + moveX, ((1305.0012) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((1317.0000) * transformY) + moveY),
        new createjs.Point(((901.9971) * transformX) + moveX, ((1325.9289) * transformY) + moveY),
        new createjs.Point(((902.9956) * transformX) + moveX, ((1337.7617) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1345.0000) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1350.6661) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1356.3339) * transformY) + moveY),
        new createjs.Point(((901.0000) * transformX) + moveX, ((1362.0000) * transformY) + moveY),
        new createjs.Point(((900.6667) * transformX) + moveX, ((1362.0000) * transformY) + moveY),
        new createjs.Point(((900.3333) * transformX) + moveX, ((1362.0000) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1362.0000) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1366.3329) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1370.6671) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((1375.0000) * transformY) + moveY),
        new createjs.Point(((899.6667) * transformX) + moveX, ((1375.0000) * transformY) + moveY),
        new createjs.Point(((899.3333) * transformX) + moveX, ((1375.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1375.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1378.3330) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1381.6670) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((1385.0000) * transformY) + moveY),
        new createjs.Point(((898.6667) * transformX) + moveX, ((1385.0000) * transformY) + moveY),
        new createjs.Point(((898.3333) * transformX) + moveX, ((1385.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1385.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1387.9997) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1391.0003) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((1394.0000) * transformY) + moveY),
        new createjs.Point(((897.6667) * transformX) + moveX, ((1394.0000) * transformY) + moveY),
        new createjs.Point(((897.3333) * transformX) + moveX, ((1394.0000) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1394.0000) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1396.6664) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1399.3336) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1402.0000) * transformY) + moveY),
        new createjs.Point(((896.6667) * transformX) + moveX, ((1402.0000) * transformY) + moveY),
        new createjs.Point(((896.3333) * transformX) + moveX, ((1402.0000) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((1402.0000) * transformY) + moveY),
        new createjs.Point(((895.6667) * transformX) + moveX, ((1406.9995) * transformY) + moveY),
        new createjs.Point(((895.3333) * transformX) + moveX, ((1412.0005) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((1417.0000) * transformY) + moveY),
        new createjs.Point(((891.5243) * transformX) + moveX, ((1428.8293) * transformY) + moveY),
        new createjs.Point(((890.6439) * transformX) + moveX, ((1443.6338) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1455.0000) * transformY) + moveY),
        new createjs.Point(((886.6667) * transformX) + moveX, ((1458.3330) * transformY) + moveY),
        new createjs.Point(((886.3333) * transformX) + moveX, ((1461.6670) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1465.0000) * transformY) + moveY),
        new createjs.Point(((885.6667) * transformX) + moveX, ((1465.0000) * transformY) + moveY),
        new createjs.Point(((885.3333) * transformX) + moveX, ((1465.0000) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1465.0000) * transformY) + moveY),
        new createjs.Point(((884.0001) * transformX) + moveX, ((1470.6661) * transformY) + moveY),
        new createjs.Point(((882.9999) * transformX) + moveX, ((1476.3339) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1482.0000) * transformY) + moveY),
        new createjs.Point(((881.6667) * transformX) + moveX, ((1482.0000) * transformY) + moveY),
        new createjs.Point(((881.3333) * transformX) + moveX, ((1482.0000) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1482.0000) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1482.9999) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1484.0001) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1485.0000) * transformY) + moveY),
        new createjs.Point(((880.6667) * transformX) + moveX, ((1485.0000) * transformY) + moveY),
        new createjs.Point(((880.3333) * transformX) + moveX, ((1485.0000) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((1485.0000) * transformY) + moveY),
        new createjs.Point(((879.6667) * transformX) + moveX, ((1487.6664) * transformY) + moveY),
        new createjs.Point(((879.3333) * transformX) + moveX, ((1490.3336) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((878.6667) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((878.3333) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1493.0000) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1494.3332) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1495.6668) * transformY) + moveY),
        new createjs.Point(((878.0000) * transformX) + moveX, ((1497.0000) * transformY) + moveY),
        new createjs.Point(((877.6667) * transformX) + moveX, ((1497.0000) * transformY) + moveY),
        new createjs.Point(((877.3333) * transformX) + moveX, ((1497.0000) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((1497.0000) * transformY) + moveY),
        new createjs.Point(((876.6667) * transformX) + moveX, ((1499.3331) * transformY) + moveY),
        new createjs.Point(((876.3333) * transformX) + moveX, ((1501.6669) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((1504.0000) * transformY) + moveY),
        new createjs.Point(((875.6667) * transformX) + moveX, ((1504.0000) * transformY) + moveY),
        new createjs.Point(((875.3333) * transformX) + moveX, ((1504.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1504.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1504.9999) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1506.0001) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1507.0000) * transformY) + moveY),
        new createjs.Point(((874.6667) * transformX) + moveX, ((1507.0000) * transformY) + moveY),
        new createjs.Point(((874.3333) * transformX) + moveX, ((1507.0000) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1507.0000) * transformY) + moveY),
        new createjs.Point(((872.6668) * transformX) + moveX, ((1512.6661) * transformY) + moveY),
        new createjs.Point(((871.3332) * transformX) + moveX, ((1518.3339) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1524.0000) * transformY) + moveY),
        new createjs.Point(((869.6667) * transformX) + moveX, ((1524.0000) * transformY) + moveY),
        new createjs.Point(((869.3333) * transformX) + moveX, ((1524.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1524.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1524.9999) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1526.0001) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1527.0000) * transformY) + moveY),
        new createjs.Point(((868.6667) * transformX) + moveX, ((1527.0000) * transformY) + moveY),
        new createjs.Point(((868.3333) * transformX) + moveX, ((1527.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1527.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1527.9999) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1529.0001) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((867.6667) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((867.3333) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1530.9999) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1532.0001) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1533.0000) * transformY) + moveY),
        new createjs.Point(((866.6667) * transformX) + moveX, ((1533.0000) * transformY) + moveY),
        new createjs.Point(((866.3333) * transformX) + moveX, ((1533.0000) * transformY) + moveY),
        new createjs.Point(((866.0000) * transformX) + moveX, ((1533.0000) * transformY) + moveY),
        new createjs.Point(((865.6667) * transformX) + moveX, ((1534.9998) * transformY) + moveY),
        new createjs.Point(((865.3333) * transformX) + moveX, ((1537.0002) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1539.0000) * transformY) + moveY),
        new createjs.Point(((864.6667) * transformX) + moveX, ((1539.0000) * transformY) + moveY),
        new createjs.Point(((864.3333) * transformX) + moveX, ((1539.0000) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1539.0000) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1539.6666) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1540.3334) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1541.0000) * transformY) + moveY),
        new createjs.Point(((863.6667) * transformX) + moveX, ((1541.0000) * transformY) + moveY),
        new createjs.Point(((863.3333) * transformX) + moveX, ((1541.0000) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((1541.0000) * transformY) + moveY),
        new createjs.Point(((862.3334) * transformX) + moveX, ((1543.9997) * transformY) + moveY),
        new createjs.Point(((861.6666) * transformX) + moveX, ((1547.0003) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1550.0000) * transformY) + moveY),
        new createjs.Point(((860.6667) * transformX) + moveX, ((1550.0000) * transformY) + moveY),
        new createjs.Point(((860.3333) * transformX) + moveX, ((1550.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1550.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1550.6666) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1551.3334) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1552.0000) * transformY) + moveY),
        new createjs.Point(((859.6667) * transformX) + moveX, ((1552.0000) * transformY) + moveY),
        new createjs.Point(((859.3333) * transformX) + moveX, ((1552.0000) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1552.0000) * transformY) + moveY),
        new createjs.Point(((858.6667) * transformX) + moveX, ((1553.9998) * transformY) + moveY),
        new createjs.Point(((858.3333) * transformX) + moveX, ((1556.0002) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1558.0000) * transformY) + moveY),
        new createjs.Point(((857.6667) * transformX) + moveX, ((1558.0000) * transformY) + moveY),
        new createjs.Point(((857.3333) * transformX) + moveX, ((1558.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1558.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1558.6666) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1559.3334) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((856.6667) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((856.3333) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1560.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1560.9999) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1562.0001) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1563.0000) * transformY) + moveY),
        new createjs.Point(((855.6667) * transformX) + moveX, ((1563.0000) * transformY) + moveY),
        new createjs.Point(((855.3333) * transformX) + moveX, ((1563.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1563.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1563.9999) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1565.0001) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((854.6667) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((854.3333) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((853.6667) * transformX) + moveX, ((1567.3332) * transformY) + moveY),
        new createjs.Point(((853.3333) * transformX) + moveX, ((1568.6668) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1570.0000) * transformY) + moveY),
        new createjs.Point(((852.6667) * transformX) + moveX, ((1570.0000) * transformY) + moveY),
        new createjs.Point(((852.3333) * transformX) + moveX, ((1570.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1570.0000) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1570.9999) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1572.0001) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1573.0000) * transformY) + moveY),
        new createjs.Point(((851.6667) * transformX) + moveX, ((1573.0000) * transformY) + moveY),
        new createjs.Point(((851.3333) * transformX) + moveX, ((1573.0000) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1573.0000) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1573.6666) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1574.3334) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1575.0000) * transformY) + moveY),
        new createjs.Point(((850.6667) * transformX) + moveX, ((1575.0000) * transformY) + moveY),
        new createjs.Point(((850.3333) * transformX) + moveX, ((1575.0000) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1575.0000) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1575.9999) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1577.0001) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((849.6667) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((849.3333) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((848.6667) * transformX) + moveX, ((1579.3332) * transformY) + moveY),
        new createjs.Point(((848.3333) * transformX) + moveX, ((1580.6668) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((1582.0000) * transformY) + moveY),
        new createjs.Point(((847.6667) * transformX) + moveX, ((1582.0000) * transformY) + moveY),
        new createjs.Point(((847.3333) * transformX) + moveX, ((1582.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1582.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1582.9999) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1584.0001) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1585.0000) * transformY) + moveY),
        new createjs.Point(((846.6667) * transformX) + moveX, ((1585.0000) * transformY) + moveY),
        new createjs.Point(((846.3333) * transformX) + moveX, ((1585.0000) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1585.0000) * transformY) + moveY),
        new createjs.Point(((845.6667) * transformX) + moveX, ((1586.3332) * transformY) + moveY),
        new createjs.Point(((845.3333) * transformX) + moveX, ((1587.6668) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1589.0000) * transformY) + moveY),
        new createjs.Point(((844.6667) * transformX) + moveX, ((1589.0000) * transformY) + moveY),
        new createjs.Point(((844.3333) * transformX) + moveX, ((1589.0000) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1589.0000) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1589.9999) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1591.0001) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((843.6667) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((843.3333) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((842.3334) * transformX) + moveX, ((1593.9998) * transformY) + moveY),
        new createjs.Point(((841.6666) * transformX) + moveX, ((1596.0002) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1598.0000) * transformY) + moveY),
        new createjs.Point(((840.6667) * transformX) + moveX, ((1598.0000) * transformY) + moveY),
        new createjs.Point(((840.3333) * transformX) + moveX, ((1598.0000) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1598.0000) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1598.6666) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1599.3334) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1600.0000) * transformY) + moveY),
        new createjs.Point(((839.6667) * transformX) + moveX, ((1600.0000) * transformY) + moveY),
        new createjs.Point(((839.3333) * transformX) + moveX, ((1600.0000) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1600.0000) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1600.6666) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1601.3334) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1602.0000) * transformY) + moveY),
        new createjs.Point(((838.6667) * transformX) + moveX, ((1602.0000) * transformY) + moveY),
        new createjs.Point(((838.3333) * transformX) + moveX, ((1602.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1602.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1602.9999) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1604.0001) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((1605.0000) * transformY) + moveY),
        new createjs.Point(((837.6667) * transformX) + moveX, ((1605.0000) * transformY) + moveY),
        new createjs.Point(((837.3333) * transformX) + moveX, ((1605.0000) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1605.0000) * transformY) + moveY),
        new createjs.Point(((836.3334) * transformX) + moveX, ((1606.9998) * transformY) + moveY),
        new createjs.Point(((835.6666) * transformX) + moveX, ((1609.0002) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((1611.0000) * transformY) + moveY),
        new createjs.Point(((834.6667) * transformX) + moveX, ((1611.0000) * transformY) + moveY),
        new createjs.Point(((834.3333) * transformX) + moveX, ((1611.0000) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1611.0000) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1611.6666) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1612.3334) * transformY) + moveY),
        new createjs.Point(((834.0000) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((833.6667) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((833.3333) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1613.6666) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1614.3334) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1615.0000) * transformY) + moveY),
        new createjs.Point(((832.6667) * transformX) + moveX, ((1615.0000) * transformY) + moveY),
        new createjs.Point(((832.3333) * transformX) + moveX, ((1615.0000) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1615.0000) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1615.6666) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1616.3334) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((1617.0000) * transformY) + moveY),
        new createjs.Point(((831.6667) * transformX) + moveX, ((1617.0000) * transformY) + moveY),
        new createjs.Point(((831.3333) * transformX) + moveX, ((1617.0000) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1617.0000) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1617.6666) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1618.3334) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((830.6667) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((830.3333) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1619.6666) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1620.3334) * transformY) + moveY),
        new createjs.Point(((830.0000) * transformX) + moveX, ((1621.0000) * transformY) + moveY),
        new createjs.Point(((829.6667) * transformX) + moveX, ((1621.0000) * transformY) + moveY),
        new createjs.Point(((829.3333) * transformX) + moveX, ((1621.0000) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((1621.0000) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((1621.6666) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((1622.3334) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((828.6667) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((828.3333) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1623.6666) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1624.3334) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((827.6667) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((827.3333) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((1625.6666) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((1626.3334) * transformY) + moveY),
        new createjs.Point(((827.0000) * transformX) + moveX, ((1627.0000) * transformY) + moveY),
        new createjs.Point(((826.6667) * transformX) + moveX, ((1627.0000) * transformY) + moveY),
        new createjs.Point(((826.3333) * transformX) + moveX, ((1627.0000) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1627.0000) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1627.6666) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1628.3334) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1629.0000) * transformY) + moveY),
        new createjs.Point(((825.3334) * transformX) + moveX, ((1629.3333) * transformY) + moveY),
        new createjs.Point(((824.6666) * transformX) + moveX, ((1629.6667) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((1630.0000) * transformY) + moveY),
        new createjs.Point(((823.6667) * transformX) + moveX, ((1631.3332) * transformY) + moveY),
        new createjs.Point(((823.3333) * transformX) + moveX, ((1632.6668) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((1634.0000) * transformY) + moveY),
        new createjs.Point(((822.6667) * transformX) + moveX, ((1634.0000) * transformY) + moveY),
        new createjs.Point(((822.3333) * transformX) + moveX, ((1634.0000) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((1634.0000) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((1634.6666) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((1635.3334) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((1636.0000) * transformY) + moveY),
        new createjs.Point(((821.6667) * transformX) + moveX, ((1636.0000) * transformY) + moveY),
        new createjs.Point(((821.3333) * transformX) + moveX, ((1636.0000) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1636.0000) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1636.6666) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1637.3334) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1638.0000) * transformY) + moveY),
        new createjs.Point(((820.6667) * transformX) + moveX, ((1638.0000) * transformY) + moveY),
        new createjs.Point(((820.3333) * transformX) + moveX, ((1638.0000) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1638.0000) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1638.6666) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1639.3334) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1640.0000) * transformY) + moveY),
        new createjs.Point(((819.6667) * transformX) + moveX, ((1640.0000) * transformY) + moveY),
        new createjs.Point(((819.3333) * transformX) + moveX, ((1640.0000) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1640.0000) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1640.6666) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1641.3334) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1642.0000) * transformY) + moveY),
        new createjs.Point(((818.3334) * transformX) + moveX, ((1642.3333) * transformY) + moveY),
        new createjs.Point(((817.6666) * transformX) + moveX, ((1642.6667) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((1643.0000) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((1643.6666) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((1644.3334) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((1645.0000) * transformY) + moveY),
        new createjs.Point(((816.6667) * transformX) + moveX, ((1645.0000) * transformY) + moveY),
        new createjs.Point(((816.3333) * transformX) + moveX, ((1645.0000) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((1645.0000) * transformY) + moveY),
        new createjs.Point(((815.6667) * transformX) + moveX, ((1646.3332) * transformY) + moveY),
        new createjs.Point(((815.3333) * transformX) + moveX, ((1647.6668) * transformY) + moveY),
        new createjs.Point(((815.0000) * transformX) + moveX, ((1649.0000) * transformY) + moveY),
        new createjs.Point(((814.3334) * transformX) + moveX, ((1649.3333) * transformY) + moveY),
        new createjs.Point(((813.6666) * transformX) + moveX, ((1649.6667) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((1650.0000) * transformY) + moveY),
        new createjs.Point(((812.6667) * transformX) + moveX, ((1651.3332) * transformY) + moveY),
        new createjs.Point(((812.3333) * transformX) + moveX, ((1652.6668) * transformY) + moveY),
        new createjs.Point(((812.0000) * transformX) + moveX, ((1654.0000) * transformY) + moveY),
        new createjs.Point(((811.3334) * transformX) + moveX, ((1654.3333) * transformY) + moveY),
        new createjs.Point(((810.6666) * transformX) + moveX, ((1654.6667) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((1655.0000) * transformY) + moveY),
        new createjs.Point(((809.6667) * transformX) + moveX, ((1656.3332) * transformY) + moveY),
        new createjs.Point(((809.3333) * transformX) + moveX, ((1657.6668) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((1659.0000) * transformY) + moveY),
        new createjs.Point(((808.3334) * transformX) + moveX, ((1659.3333) * transformY) + moveY),
        new createjs.Point(((807.6666) * transformX) + moveX, ((1659.6667) * transformY) + moveY),
        new createjs.Point(((807.0000) * transformX) + moveX, ((1660.0000) * transformY) + moveY),
        new createjs.Point(((806.6667) * transformX) + moveX, ((1661.3332) * transformY) + moveY),
        new createjs.Point(((806.3333) * transformX) + moveX, ((1662.6668) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1664.0000) * transformY) + moveY),
        new createjs.Point(((805.3334) * transformX) + moveX, ((1664.3333) * transformY) + moveY),
        new createjs.Point(((804.6666) * transformX) + moveX, ((1664.6667) * transformY) + moveY),
        new createjs.Point(((804.0000) * transformX) + moveX, ((1665.0000) * transformY) + moveY),
        new createjs.Point(((803.6667) * transformX) + moveX, ((1666.3332) * transformY) + moveY),
        new createjs.Point(((803.3333) * transformX) + moveX, ((1667.6668) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((1669.0000) * transformY) + moveY),
        new createjs.Point(((802.3334) * transformX) + moveX, ((1669.3333) * transformY) + moveY),
        new createjs.Point(((801.6666) * transformX) + moveX, ((1669.6667) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1670.0000) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1670.6666) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1671.3334) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((1672.0000) * transformY) + moveY),
        new createjs.Point(((800.3334) * transformX) + moveX, ((1672.3333) * transformY) + moveY),
        new createjs.Point(((799.6666) * transformX) + moveX, ((1672.6667) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1673.0000) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1673.6666) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1674.3334) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1675.0000) * transformY) + moveY),
        new createjs.Point(((798.3334) * transformX) + moveX, ((1675.3333) * transformY) + moveY),
        new createjs.Point(((797.6666) * transformX) + moveX, ((1675.6667) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1676.0000) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1676.6666) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1677.3334) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1678.0000) * transformY) + moveY),
        new createjs.Point(((796.3334) * transformX) + moveX, ((1678.3333) * transformY) + moveY),
        new createjs.Point(((795.6666) * transformX) + moveX, ((1678.6667) * transformY) + moveY),
        new createjs.Point(((795.0000) * transformX) + moveX, ((1679.0000) * transformY) + moveY),
        new createjs.Point(((795.0000) * transformX) + moveX, ((1679.6666) * transformY) + moveY),
        new createjs.Point(((795.0000) * transformX) + moveX, ((1680.3334) * transformY) + moveY),
        new createjs.Point(((795.0000) * transformX) + moveX, ((1681.0000) * transformY) + moveY),
        new createjs.Point(((794.3334) * transformX) + moveX, ((1681.3333) * transformY) + moveY),
        new createjs.Point(((793.6666) * transformX) + moveX, ((1681.6667) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((1682.0000) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((1682.6666) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((1683.3334) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((1684.0000) * transformY) + moveY),
        new createjs.Point(((792.3334) * transformX) + moveX, ((1684.3333) * transformY) + moveY),
        new createjs.Point(((791.6666) * transformX) + moveX, ((1684.6667) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1685.0000) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1685.6666) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1686.3334) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1687.0000) * transformY) + moveY),
        new createjs.Point(((790.3334) * transformX) + moveX, ((1687.3333) * transformY) + moveY),
        new createjs.Point(((789.6666) * transformX) + moveX, ((1687.6667) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((1688.0000) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((1688.6666) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((1689.3334) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((1690.0000) * transformY) + moveY),
        new createjs.Point(((788.6667) * transformX) + moveX, ((1690.0000) * transformY) + moveY),
        new createjs.Point(((788.3333) * transformX) + moveX, ((1690.0000) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1690.0000) * transformY) + moveY),
        new createjs.Point(((787.6667) * transformX) + moveX, ((1690.9999) * transformY) + moveY),
        new createjs.Point(((787.3333) * transformX) + moveX, ((1692.0001) * transformY) + moveY),
        new createjs.Point(((787.0000) * transformX) + moveX, ((1693.0000) * transformY) + moveY),
        new createjs.Point(((786.3334) * transformX) + moveX, ((1693.3333) * transformY) + moveY),
        new createjs.Point(((785.6666) * transformX) + moveX, ((1693.6667) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((1694.0000) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((1694.6666) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((1695.3334) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((1696.0000) * transformY) + moveY),
        new createjs.Point(((784.0001) * transformX) + moveX, ((1696.6666) * transformY) + moveY),
        new createjs.Point(((782.9999) * transformX) + moveX, ((1697.3334) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((1698.0000) * transformY) + moveY),
        new createjs.Point(((781.6667) * transformX) + moveX, ((1698.9999) * transformY) + moveY),
        new createjs.Point(((781.3333) * transformX) + moveX, ((1700.0001) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1701.0000) * transformY) + moveY),
        new createjs.Point(((780.3334) * transformX) + moveX, ((1701.3333) * transformY) + moveY),
        new createjs.Point(((779.6666) * transformX) + moveX, ((1701.6667) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((1702.0000) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((1702.6666) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((1703.3334) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((1704.0000) * transformY) + moveY),
        new createjs.Point(((778.3334) * transformX) + moveX, ((1704.3333) * transformY) + moveY),
        new createjs.Point(((777.6666) * transformX) + moveX, ((1704.6667) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((1705.0000) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((1705.6666) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((1706.3334) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((1707.0000) * transformY) + moveY),
        new createjs.Point(((776.0001) * transformX) + moveX, ((1707.6666) * transformY) + moveY),
        new createjs.Point(((774.9999) * transformX) + moveX, ((1708.3334) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((1709.0000) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((1709.6666) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((1710.3334) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((1711.0000) * transformY) + moveY),
        new createjs.Point(((772.6668) * transformX) + moveX, ((1711.9999) * transformY) + moveY),
        new createjs.Point(((771.3332) * transformX) + moveX, ((1713.0001) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1714.0000) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1714.6666) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1715.3334) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((1716.0000) * transformY) + moveY),
        new createjs.Point(((769.0001) * transformX) + moveX, ((1716.6666) * transformY) + moveY),
        new createjs.Point(((767.9999) * transformX) + moveX, ((1717.3334) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((1718.6666) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((1719.3334) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((1720.0000) * transformY) + moveY),
        new createjs.Point(((765.6668) * transformX) + moveX, ((1720.9999) * transformY) + moveY),
        new createjs.Point(((764.3332) * transformX) + moveX, ((1722.0001) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1723.0000) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1723.6666) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1724.3334) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),
        new createjs.Point(((761.3335) * transformX) + moveX, ((1726.3332) * transformY) + moveY),
        new createjs.Point(((759.6665) * transformX) + moveX, ((1727.6668) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((1729.0000) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((1729.6666) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((1730.3334) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),
        new createjs.Point(((756.0002) * transformX) + moveX, ((1732.6665) * transformY) + moveY),
        new createjs.Point(((753.9998) * transformX) + moveX, ((1734.3335) * transformY) + moveY),
        new createjs.Point(((752.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((752.0000) * transformX) + moveX, ((1736.6666) * transformY) + moveY),
        new createjs.Point(((752.0000) * transformX) + moveX, ((1737.3334) * transformY) + moveY),
        new createjs.Point(((752.0000) * transformX) + moveX, ((1738.0000) * transformY) + moveY),
        new createjs.Point(((749.6669) * transformX) + moveX, ((1739.9998) * transformY) + moveY),
        new createjs.Point(((747.3331) * transformX) + moveX, ((1742.0002) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((1744.0000) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((1744.6666) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((1745.3334) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((1746.0000) * transformY) + moveY),
        new createjs.Point(((740.0005) * transformX) + moveX, ((1750.6662) * transformY) + moveY),
        new createjs.Point(((734.9995) * transformX) + moveX, ((1755.3338) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((1760.6666) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((1761.3334) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((1762.0000) * transformY) + moveY),
        new createjs.Point(((728.3335) * transformX) + moveX, ((1763.3332) * transformY) + moveY),
        new createjs.Point(((726.6665) * transformX) + moveX, ((1764.6668) * transformY) + moveY),
        new createjs.Point(((725.0000) * transformX) + moveX, ((1766.0000) * transformY) + moveY),
        new createjs.Point(((724.6667) * transformX) + moveX, ((1766.6666) * transformY) + moveY),
        new createjs.Point(((724.3333) * transformX) + moveX, ((1767.3334) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((1768.0000) * transformY) + moveY),
        new createjs.Point(((723.3334) * transformX) + moveX, ((1768.0000) * transformY) + moveY),
        new createjs.Point(((722.6666) * transformX) + moveX, ((1768.0000) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((1768.0000) * transformY) + moveY),
        new createjs.Point(((717.3338) * transformX) + moveX, ((1772.9995) * transformY) + moveY),
        new createjs.Point(((712.6662) * transformX) + moveX, ((1778.0005) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((707.3334) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((706.6666) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),
        new createjs.Point(((704.0002) * transformX) + moveX, ((1785.3331) * transformY) + moveY),
        new createjs.Point(((701.9998) * transformX) + moveX, ((1787.6669) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((699.3334) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((698.6666) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((698.0000) * transformX) + moveX, ((1790.0000) * transformY) + moveY),
        new createjs.Point(((696.3335) * transformX) + moveX, ((1791.9998) * transformY) + moveY),
        new createjs.Point(((694.6665) * transformX) + moveX, ((1794.0002) * transformY) + moveY),
        new createjs.Point(((693.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((692.3334) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((691.6666) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((691.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),
        new createjs.Point(((690.0001) * transformX) + moveX, ((1797.3332) * transformY) + moveY),
        new createjs.Point(((688.9999) * transformX) + moveX, ((1798.6668) * transformY) + moveY),
        new createjs.Point(((688.0000) * transformX) + moveX, ((1800.0000) * transformY) + moveY),
        new createjs.Point(((687.3334) * transformX) + moveX, ((1800.0000) * transformY) + moveY),
        new createjs.Point(((686.6666) * transformX) + moveX, ((1800.0000) * transformY) + moveY),
        new createjs.Point(((686.0000) * transformX) + moveX, ((1800.0000) * transformY) + moveY),
        new createjs.Point(((685.0001) * transformX) + moveX, ((1801.3332) * transformY) + moveY),
        new createjs.Point(((683.9999) * transformX) + moveX, ((1802.6668) * transformY) + moveY),
        new createjs.Point(((683.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((682.3334) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((681.6666) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((681.0000) * transformX) + moveX, ((1804.0000) * transformY) + moveY),
        new createjs.Point(((680.3334) * transformX) + moveX, ((1804.9999) * transformY) + moveY),
        new createjs.Point(((679.6666) * transformX) + moveX, ((1806.0001) * transformY) + moveY),
        new createjs.Point(((679.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((678.3334) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((677.6666) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((677.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((676.0001) * transformX) + moveX, ((1808.3332) * transformY) + moveY),
        new createjs.Point(((674.9999) * transformX) + moveX, ((1809.6668) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((673.3334) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((672.6666) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((672.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((671.6667) * transformX) + moveX, ((1811.6666) * transformY) + moveY),
        new createjs.Point(((671.3333) * transformX) + moveX, ((1812.3334) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((670.3334) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((669.6666) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((669.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((668.3334) * transformX) + moveX, ((1813.9999) * transformY) + moveY),
        new createjs.Point(((667.6666) * transformX) + moveX, ((1815.0001) * transformY) + moveY),
        new createjs.Point(((667.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((666.3334) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((665.6666) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((665.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((664.6667) * transformX) + moveX, ((1816.6666) * transformY) + moveY),
        new createjs.Point(((664.3333) * transformX) + moveX, ((1817.3334) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((663.3334) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((662.6666) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((662.0000) * transformX) + moveX, ((1818.0000) * transformY) + moveY),
        new createjs.Point(((661.3334) * transformX) + moveX, ((1818.9999) * transformY) + moveY),
        new createjs.Point(((660.6666) * transformX) + moveX, ((1820.0001) * transformY) + moveY),
        new createjs.Point(((660.0000) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((659.3334) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((658.6666) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((658.0000) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((657.6667) * transformX) + moveX, ((1821.6666) * transformY) + moveY),
        new createjs.Point(((657.3333) * transformX) + moveX, ((1822.3334) * transformY) + moveY),
        new createjs.Point(((657.0000) * transformX) + moveX, ((1823.0000) * transformY) + moveY),
        new createjs.Point(((656.0001) * transformX) + moveX, ((1823.3333) * transformY) + moveY),
        new createjs.Point(((654.9999) * transformX) + moveX, ((1823.6667) * transformY) + moveY),
        new createjs.Point(((654.0000) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((654.0000) * transformX) + moveX, ((1824.3333) * transformY) + moveY),
        new createjs.Point(((654.0000) * transformX) + moveX, ((1824.6667) * transformY) + moveY),
        new createjs.Point(((654.0000) * transformX) + moveX, ((1825.0000) * transformY) + moveY),
        new createjs.Point(((653.3334) * transformX) + moveX, ((1825.0000) * transformY) + moveY),
        new createjs.Point(((652.6666) * transformX) + moveX, ((1825.0000) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((1825.0000) * transformY) + moveY),
        new createjs.Point(((651.6667) * transformX) + moveX, ((1825.6666) * transformY) + moveY),
        new createjs.Point(((651.3333) * transformX) + moveX, ((1826.3334) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((1827.0000) * transformY) + moveY),
        new createjs.Point(((650.0001) * transformX) + moveX, ((1827.3333) * transformY) + moveY),
        new createjs.Point(((648.9999) * transformX) + moveX, ((1827.6667) * transformY) + moveY),
        new createjs.Point(((648.0000) * transformX) + moveX, ((1828.0000) * transformY) + moveY),
        new createjs.Point(((648.0000) * transformX) + moveX, ((1828.3333) * transformY) + moveY),
        new createjs.Point(((648.0000) * transformX) + moveX, ((1828.6667) * transformY) + moveY),
        new createjs.Point(((648.0000) * transformX) + moveX, ((1829.0000) * transformY) + moveY),
        new createjs.Point(((646.6668) * transformX) + moveX, ((1829.3333) * transformY) + moveY),
        new createjs.Point(((645.3332) * transformX) + moveX, ((1829.6667) * transformY) + moveY),
        new createjs.Point(((644.0000) * transformX) + moveX, ((1830.0000) * transformY) + moveY),
        new createjs.Point(((643.6667) * transformX) + moveX, ((1830.6666) * transformY) + moveY),
        new createjs.Point(((643.3333) * transformX) + moveX, ((1831.3334) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((1832.0000) * transformY) + moveY),
        new createjs.Point(((642.0001) * transformX) + moveX, ((1832.3333) * transformY) + moveY),
        new createjs.Point(((640.9999) * transformX) + moveX, ((1832.6667) * transformY) + moveY),
        new createjs.Point(((640.0000) * transformX) + moveX, ((1833.0000) * transformY) + moveY),
        new createjs.Point(((640.0000) * transformX) + moveX, ((1833.3333) * transformY) + moveY),
        new createjs.Point(((640.0000) * transformX) + moveX, ((1833.6667) * transformY) + moveY),
        new createjs.Point(((640.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((638.6668) * transformX) + moveX, ((1834.3333) * transformY) + moveY),
        new createjs.Point(((637.3332) * transformX) + moveX, ((1834.6667) * transformY) + moveY),
        new createjs.Point(((636.0000) * transformX) + moveX, ((1835.0000) * transformY) + moveY),
        new createjs.Point(((635.6667) * transformX) + moveX, ((1835.6666) * transformY) + moveY),
        new createjs.Point(((635.3333) * transformX) + moveX, ((1836.3334) * transformY) + moveY),
        new createjs.Point(((635.0000) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((633.6668) * transformX) + moveX, ((1837.3333) * transformY) + moveY),
        new createjs.Point(((632.3332) * transformX) + moveX, ((1837.6667) * transformY) + moveY),
        new createjs.Point(((631.0000) * transformX) + moveX, ((1838.0000) * transformY) + moveY),
        new createjs.Point(((630.6667) * transformX) + moveX, ((1838.6666) * transformY) + moveY),
        new createjs.Point(((630.3333) * transformX) + moveX, ((1839.3334) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((1840.0000) * transformY) + moveY),
        new createjs.Point(((628.0002) * transformX) + moveX, ((1840.6666) * transformY) + moveY),
        new createjs.Point(((625.9998) * transformX) + moveX, ((1841.3334) * transformY) + moveY),
        new createjs.Point(((624.0000) * transformX) + moveX, ((1842.0000) * transformY) + moveY),
        new createjs.Point(((623.6667) * transformX) + moveX, ((1842.6666) * transformY) + moveY),
        new createjs.Point(((623.3333) * transformX) + moveX, ((1843.3334) * transformY) + moveY),
        new createjs.Point(((623.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((622.3334) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((621.6666) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((1844.3333) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((1844.6667) * transformY) + moveY),
        new createjs.Point(((621.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((620.3334) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((619.6666) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((619.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((619.0000) * transformX) + moveX, ((1845.3333) * transformY) + moveY),
        new createjs.Point(((619.0000) * transformX) + moveX, ((1845.6667) * transformY) + moveY),
        new createjs.Point(((619.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((618.3334) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((617.6666) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((1846.3333) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((1846.6667) * transformY) + moveY),
        new createjs.Point(((617.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((616.3334) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((615.6666) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((615.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((615.0000) * transformX) + moveX, ((1847.3333) * transformY) + moveY),
        new createjs.Point(((615.0000) * transformX) + moveX, ((1847.6667) * transformY) + moveY),
        new createjs.Point(((615.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((613.6668) * transformX) + moveX, ((1848.3333) * transformY) + moveY),
        new createjs.Point(((612.3332) * transformX) + moveX, ((1848.6667) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1849.0000) * transformY) + moveY),
        new createjs.Point(((610.6667) * transformX) + moveX, ((1849.6666) * transformY) + moveY),
        new createjs.Point(((610.3333) * transformX) + moveX, ((1850.3334) * transformY) + moveY),
        new createjs.Point(((610.0000) * transformX) + moveX, ((1851.0000) * transformY) + moveY),
        new createjs.Point(((608.6668) * transformX) + moveX, ((1851.3333) * transformY) + moveY),
        new createjs.Point(((607.3332) * transformX) + moveX, ((1851.6667) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((1852.3333) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((1852.6667) * transformY) + moveY),
        new createjs.Point(((606.0000) * transformX) + moveX, ((1853.0000) * transformY) + moveY),
        new createjs.Point(((604.0002) * transformX) + moveX, ((1853.6666) * transformY) + moveY),
        new createjs.Point(((601.9998) * transformX) + moveX, ((1854.3334) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1855.3333) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1855.6667) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((599.0001) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((597.9999) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1856.3333) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1856.6667) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1857.0000) * transformY) + moveY),
        new createjs.Point(((595.0002) * transformX) + moveX, ((1857.6666) * transformY) + moveY),
        new createjs.Point(((592.9998) * transformX) + moveX, ((1858.3334) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((1859.3333) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((1859.6667) * transformY) + moveY),
        new createjs.Point(((591.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((590.3334) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((589.6666) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1860.3333) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1860.6667) * transformY) + moveY),
        new createjs.Point(((589.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((588.3334) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((587.6666) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1861.3333) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1861.6667) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((586.0001) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((584.9999) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((1862.3333) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((1862.6667) * transformY) + moveY),
        new createjs.Point(((584.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((582.6668) * transformX) + moveX, ((1863.3333) * transformY) + moveY),
        new createjs.Point(((581.3332) * transformX) + moveX, ((1863.6667) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1864.3333) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1864.6667) * transformY) + moveY),
        new createjs.Point(((580.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((579.0001) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((577.9999) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((577.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((577.0000) * transformX) + moveX, ((1865.3333) * transformY) + moveY),
        new createjs.Point(((577.0000) * transformX) + moveX, ((1865.6667) * transformY) + moveY),
        new createjs.Point(((577.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((576.3334) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((575.6666) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((575.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((575.0000) * transformX) + moveX, ((1866.3333) * transformY) + moveY),
        new createjs.Point(((575.0000) * transformX) + moveX, ((1866.6667) * transformY) + moveY),
        new createjs.Point(((575.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((573.3335) * transformX) + moveX, ((1867.3333) * transformY) + moveY),
        new createjs.Point(((571.6665) * transformX) + moveX, ((1867.6667) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((1868.3333) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((1868.6667) * transformY) + moveY),
        new createjs.Point(((570.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((569.0001) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((567.9999) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((1869.3333) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((1869.6667) * transformY) + moveY),
        new createjs.Point(((567.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((566.3334) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((565.6666) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((1870.3333) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((1870.6667) * transformY) + moveY),
        new createjs.Point(((565.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),
        new createjs.Point(((563.0002) * transformX) + moveX, ((1871.3333) * transformY) + moveY),
        new createjs.Point(((560.9998) * transformX) + moveX, ((1871.6667) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1872.3333) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1872.6667) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((558.3334) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((557.6666) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1873.3333) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1873.6667) * transformY) + moveY),
        new createjs.Point(((557.0000) * transformX) + moveX, ((1874.0000) * transformY) + moveY),
        new createjs.Point(((555.0002) * transformX) + moveX, ((1874.3333) * transformY) + moveY),
        new createjs.Point(((552.9998) * transformX) + moveX, ((1874.6667) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1875.3333) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1875.6667) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((550.0001) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((548.9999) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1876.3333) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1876.6667) * transformY) + moveY),
        new createjs.Point(((548.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((547.0001) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((545.9999) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((1877.3333) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((1877.6667) * transformY) + moveY),
        new createjs.Point(((545.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((544.0001) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((542.9999) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1878.3333) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1878.6667) * transformY) + moveY),
        new createjs.Point(((542.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((538.6670) * transformX) + moveX, ((1879.6666) * transformY) + moveY),
        new createjs.Point(((535.3330) * transformX) + moveX, ((1880.3334) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1881.0000) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1881.3333) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1881.6667) * transformY) + moveY),
        new createjs.Point(((532.0000) * transformX) + moveX, ((1882.0000) * transformY) + moveY),
        new createjs.Point(((529.6669) * transformX) + moveX, ((1882.3333) * transformY) + moveY),
        // new createjs.Point(((527.3331)*transformX)+moveX,((1882.6667)*transformY)+moveY),
        //  new createjs.Point(((525.0000)*transformX)+moveX,((1883.0000)*transformY)+moveY),
        //  new createjs.Point(((525.0000)*transformX)+moveX,((1883.3333)*transformY)+moveY),
        // new createjs.Point(((525.0000)*transformX)+moveX,((1883.6667)*transformY)+moveY),
        //  new createjs.Point(((525.0000)*transformX)+moveX,((1884.0000)*transformY)+moveY),
        //  new createjs.Point(((509.3349)*transformX)+moveX,((1887.3330)*transformY)+moveY),
        // new createjs.Point(((493.6651)*transformX)+moveX,((1890.6670)*transformY)+moveY),
        //  new createjs.Point(((478.0000)*transformX)+moveX,((1894.0000)*transformY)+moveY),
        //  new createjs.Point(((470.3341)*transformX)+moveX,((1894.6666)*transformY)+moveY),
        // new createjs.Point(((462.6659)*transformX)+moveX,((1895.3334)*transformY)+moveY),
        //  new createjs.Point(((455.0000)*transformX)+moveX,((1896.0000)*transformY)+moveY),
        //  new createjs.Point(((455.0000)*transformX)+moveX,((1896.3333)*transformY)+moveY),
        // new createjs.Point(((455.0000)*transformX)+moveX,((1896.6667)*transformY)+moveY),
        //  new createjs.Point(((455.0000)*transformX)+moveX,((1897.0000)*transformY)+moveY),
        //  new createjs.Point(((449.6672)*transformX)+moveX,((1897.3333)*transformY)+moveY),
        // new createjs.Point(((444.3328)*transformX)+moveX,((1897.6667)*transformY)+moveY),
        //  new createjs.Point(((439.0000)*transformX)+moveX,((1898.0000)*transformY)+moveY),
        //  new createjs.Point(((435.6670)*transformX)+moveX,((1898.0000)*transformY)+moveY),
        // new createjs.Point(((432.3330)*transformX)+moveX,((1898.0000)*transformY)+moveY),
        //  new createjs.Point(((429.0000)*transformX)+moveX,((1898.0000)*transformY)+moveY),
        // new createjs.Point(((418.8262)*transformX)+moveX,((1900.8999)*transformY)+moveY),

        //Bottom Right Ends

        //Bottom Left Starts
        new createjs.Point(((395.5953) * transformX) + moveX, ((1899.2352) * transformY) + moveY),

        new createjs.Point(((383.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((380.1119) * transformX) + moveX, ((1896.4357) * transformY) + moveY),
        new createjs.Point(((373.3826) * transformX) + moveX, ((1899.2494) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((1898.0000) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((1897.6667) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((1897.3333) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((366.0003) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((362.9997) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((1896.6667) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((1896.3333) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((357.3336) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((354.6664) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((1896.0000) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((1895.6667) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((1895.3333) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((349.6669) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((347.3331) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((1895.0000) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((1894.6667) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((1894.3333) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((1894.0000) * transformY) + moveY),
        new createjs.Point(((343.0002) * transformX) + moveX, ((1894.0000) * transformY) + moveY),
        new createjs.Point(((340.9998) * transformX) + moveX, ((1894.0000) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((1894.0000) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((1893.6667) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((1893.3333) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((1893.0000) * transformY) + moveY),
        new createjs.Point(((335.6670) * transformX) + moveX, ((1892.6667) * transformY) + moveY),
        new createjs.Point(((332.3330) * transformX) + moveX, ((1892.3333) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1892.0000) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1891.6667) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1891.3333) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1891.0000) * transformY) + moveY),
        new createjs.Point(((327.3335) * transformX) + moveX, ((1891.0000) * transformY) + moveY),
        new createjs.Point(((325.6665) * transformX) + moveX, ((1891.0000) * transformY) + moveY),
        new createjs.Point(((324.0000) * transformX) + moveX, ((1891.0000) * transformY) + moveY),
        new createjs.Point(((324.0000) * transformX) + moveX, ((1890.6667) * transformY) + moveY),
        new createjs.Point(((324.0000) * transformX) + moveX, ((1890.3333) * transformY) + moveY),
        new createjs.Point(((324.0000) * transformX) + moveX, ((1890.0000) * transformY) + moveY),
        new createjs.Point(((322.6668) * transformX) + moveX, ((1890.0000) * transformY) + moveY),
        new createjs.Point(((321.3332) * transformX) + moveX, ((1890.0000) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1890.0000) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1889.6667) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1889.3333) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1889.0000) * transformY) + moveY),
        new createjs.Point(((318.6668) * transformX) + moveX, ((1889.0000) * transformY) + moveY),
        new createjs.Point(((317.3332) * transformX) + moveX, ((1889.0000) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((1889.0000) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((1888.6667) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((1888.3333) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((1888.0000) * transformY) + moveY),
        new createjs.Point(((314.6668) * transformX) + moveX, ((1888.0000) * transformY) + moveY),
        new createjs.Point(((313.3332) * transformX) + moveX, ((1888.0000) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((1888.0000) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((1887.6667) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((1887.3333) * transformY) + moveY),
        new createjs.Point(((312.0000) * transformX) + moveX, ((1887.0000) * transformY) + moveY),
        new createjs.Point(((309.3336) * transformX) + moveX, ((1886.6667) * transformY) + moveY),
        new createjs.Point(((306.6664) * transformX) + moveX, ((1886.3333) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((1886.0000) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((1885.6667) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((1885.3333) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((1885.0000) * transformY) + moveY),
        new createjs.Point(((301.6669) * transformX) + moveX, ((1884.6667) * transformY) + moveY),
        new createjs.Point(((299.3331) * transformX) + moveX, ((1884.3333) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1884.0000) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1883.6667) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1883.3333) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((296.0001) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((294.9999) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((1882.6667) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((1882.3333) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((1882.0000) * transformY) + moveY),
        new createjs.Point(((293.0001) * transformX) + moveX, ((1882.0000) * transformY) + moveY),
        new createjs.Point(((291.9999) * transformX) + moveX, ((1882.0000) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1882.0000) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1881.6667) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1881.3333) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1881.0000) * transformY) + moveY),
        new createjs.Point(((290.0001) * transformX) + moveX, ((1881.0000) * transformY) + moveY),
        new createjs.Point(((288.9999) * transformX) + moveX, ((1881.0000) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((1881.0000) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((1880.6667) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((1880.3333) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((286.0002) * transformX) + moveX, ((1879.6667) * transformY) + moveY),
        new createjs.Point(((283.9998) * transformX) + moveX, ((1879.3333) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((1878.6667) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((1878.3333) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((281.3334) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((280.6666) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((1877.6667) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((1877.3333) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((278.0002) * transformX) + moveX, ((1876.6667) * transformY) + moveY),
        new createjs.Point(((275.9998) * transformX) + moveX, ((1876.3333) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1875.6667) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1875.3333) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((273.3334) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((272.6666) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((272.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((272.0000) * transformX) + moveX, ((1874.6667) * transformY) + moveY),
        new createjs.Point(((272.0000) * transformX) + moveX, ((1874.3333) * transformY) + moveY),
        new createjs.Point(((272.0000) * transformX) + moveX, ((1874.0000) * transformY) + moveY),
        new createjs.Point(((271.0001) * transformX) + moveX, ((1874.0000) * transformY) + moveY),
        new createjs.Point(((269.9999) * transformX) + moveX, ((1874.0000) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((1874.0000) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((1873.6667) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((1873.3333) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((268.3334) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((267.6666) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((1873.0000) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((1872.6667) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((1872.3333) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),
        new createjs.Point(((266.0001) * transformX) + moveX, ((1872.0000) * transformY) + moveY),
        new createjs.Point(((264.9999) * transformX) + moveX, ((1872.0000) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((1871.6667) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((1871.3333) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),
        new createjs.Point(((262.6668) * transformX) + moveX, ((1870.6667) * transformY) + moveY),
        new createjs.Point(((261.3332) * transformX) + moveX, ((1870.3333) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((1869.6667) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((1869.3333) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((259.0001) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((257.9999) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((1868.6667) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((1868.3333) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((256.3334) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((255.6666) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((1867.6667) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((1867.3333) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((253.6668) * transformX) + moveX, ((1866.6667) * transformY) + moveY),
        new createjs.Point(((252.3332) * transformX) + moveX, ((1866.3333) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1865.6667) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1865.3333) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((250.3334) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((249.6666) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1864.6667) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1864.3333) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),
        new createjs.Point(((248.3334) * transformX) + moveX, ((1864.0000) * transformY) + moveY),
        new createjs.Point(((247.6666) * transformX) + moveX, ((1864.0000) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((1863.6667) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((1863.3333) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((246.3334) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((245.6666) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((1862.6667) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((1862.3333) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((244.3334) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((243.6666) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((1861.6667) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((1861.3333) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((242.3334) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((241.6666) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1860.6667) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1860.3333) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((240.3334) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((239.6666) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1859.6667) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1859.3333) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),
        new createjs.Point(((238.3334) * transformX) + moveX, ((1859.0000) * transformY) + moveY),
        new createjs.Point(((237.6666) * transformX) + moveX, ((1859.0000) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((1858.6667) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((1858.3333) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((236.3334) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((235.6666) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1857.6667) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1857.3333) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1857.0000) * transformY) + moveY),
        new createjs.Point(((234.3334) * transformX) + moveX, ((1857.0000) * transformY) + moveY),
        new createjs.Point(((233.6666) * transformX) + moveX, ((1857.0000) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1857.0000) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1856.6667) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1856.3333) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((232.3334) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((231.6666) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1855.6667) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1855.3333) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((230.3334) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((229.6666) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1854.6667) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1854.3333) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((228.3334) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((227.6666) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((226.6667) * transformX) + moveX, ((1853.3334) * transformY) + moveY),
        new createjs.Point(((226.3333) * transformX) + moveX, ((1852.6666) * transformY) + moveY),
        new createjs.Point(((226.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),
        new createjs.Point(((224.6668) * transformX) + moveX, ((1851.6667) * transformY) + moveY),
        new createjs.Point(((223.3332) * transformX) + moveX, ((1851.3333) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((1851.0000) * transformY) + moveY),
        new createjs.Point(((221.6667) * transformX) + moveX, ((1850.3334) * transformY) + moveY),
        new createjs.Point(((221.3333) * transformX) + moveX, ((1849.6666) * transformY) + moveY),
        new createjs.Point(((221.0000) * transformX) + moveX, ((1849.0000) * transformY) + moveY),
        new createjs.Point(((219.6668) * transformX) + moveX, ((1848.6667) * transformY) + moveY),
        new createjs.Point(((218.3332) * transformX) + moveX, ((1848.3333) * transformY) + moveY),
        new createjs.Point(((217.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((216.6667) * transformX) + moveX, ((1847.3334) * transformY) + moveY),
        new createjs.Point(((216.3333) * transformX) + moveX, ((1846.6666) * transformY) + moveY),
        new createjs.Point(((216.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((215.3334) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((214.6666) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1845.6667) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1845.3333) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((213.3334) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((212.6666) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((212.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((211.6667) * transformX) + moveX, ((1844.3334) * transformY) + moveY),
        new createjs.Point(((211.3333) * transformX) + moveX, ((1843.6666) * transformY) + moveY),
        new createjs.Point(((211.0000) * transformX) + moveX, ((1843.0000) * transformY) + moveY),
        new createjs.Point(((210.3334) * transformX) + moveX, ((1843.0000) * transformY) + moveY),
        new createjs.Point(((209.6666) * transformX) + moveX, ((1843.0000) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((1843.0000) * transformY) + moveY),
        new createjs.Point(((208.6667) * transformX) + moveX, ((1842.3334) * transformY) + moveY),
        new createjs.Point(((208.3333) * transformX) + moveX, ((1841.6666) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((207.3334) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((206.6666) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((206.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((205.6667) * transformX) + moveX, ((1840.3334) * transformY) + moveY),
        new createjs.Point(((205.3333) * transformX) + moveX, ((1839.6666) * transformY) + moveY),
        new createjs.Point(((205.0000) * transformX) + moveX, ((1839.0000) * transformY) + moveY),
        new createjs.Point(((204.3334) * transformX) + moveX, ((1839.0000) * transformY) + moveY),
        new createjs.Point(((203.6666) * transformX) + moveX, ((1839.0000) * transformY) + moveY),
        new createjs.Point(((203.0000) * transformX) + moveX, ((1839.0000) * transformY) + moveY),
        new createjs.Point(((202.6667) * transformX) + moveX, ((1838.3334) * transformY) + moveY),
        new createjs.Point(((202.3333) * transformX) + moveX, ((1837.6666) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((201.3334) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((200.6666) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1837.0000) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1836.6667) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1836.3333) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((1836.0000) * transformY) + moveY),
        new createjs.Point(((199.0001) * transformX) + moveX, ((1835.6667) * transformY) + moveY),
        new createjs.Point(((197.9999) * transformX) + moveX, ((1835.3333) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((1835.0000) * transformY) + moveY),
        new createjs.Point(((196.6667) * transformX) + moveX, ((1834.3334) * transformY) + moveY),
        new createjs.Point(((196.3333) * transformX) + moveX, ((1833.6666) * transformY) + moveY),
        new createjs.Point(((196.0000) * transformX) + moveX, ((1833.0000) * transformY) + moveY),
        new createjs.Point(((195.3334) * transformX) + moveX, ((1833.0000) * transformY) + moveY),
        new createjs.Point(((194.6666) * transformX) + moveX, ((1833.0000) * transformY) + moveY),
        new createjs.Point(((194.0000) * transformX) + moveX, ((1833.0000) * transformY) + moveY),
        new createjs.Point(((193.3334) * transformX) + moveX, ((1832.0001) * transformY) + moveY),
        new createjs.Point(((192.6666) * transformX) + moveX, ((1830.9999) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((1830.0000) * transformY) + moveY),
        new createjs.Point(((191.3334) * transformX) + moveX, ((1830.0000) * transformY) + moveY),
        new createjs.Point(((190.6666) * transformX) + moveX, ((1830.0000) * transformY) + moveY),
        new createjs.Point(((190.0000) * transformX) + moveX, ((1830.0000) * transformY) + moveY),
        new createjs.Point(((189.3334) * transformX) + moveX, ((1829.0001) * transformY) + moveY),
        new createjs.Point(((188.6666) * transformX) + moveX, ((1827.9999) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((1827.0000) * transformY) + moveY),
        new createjs.Point(((187.3334) * transformX) + moveX, ((1827.0000) * transformY) + moveY),
        new createjs.Point(((186.6666) * transformX) + moveX, ((1827.0000) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((1827.0000) * transformY) + moveY),
        new createjs.Point(((185.3334) * transformX) + moveX, ((1826.0001) * transformY) + moveY),
        new createjs.Point(((184.6666) * transformX) + moveX, ((1824.9999) * transformY) + moveY),
        new createjs.Point(((184.0000) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((183.3334) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((182.6666) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((1824.0000) * transformY) + moveY),
        new createjs.Point(((181.3334) * transformX) + moveX, ((1823.0001) * transformY) + moveY),
        new createjs.Point(((180.6666) * transformX) + moveX, ((1821.9999) * transformY) + moveY),
        new createjs.Point(((180.0000) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((179.3334) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((178.6666) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((176.6668) * transformX) + moveX, ((1819.3335) * transformY) + moveY),
        new createjs.Point(((175.3332) * transformX) + moveX, ((1817.6665) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((173.3334) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((172.6666) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((170.6668) * transformX) + moveX, ((1814.3335) * transformY) + moveY),
        new createjs.Point(((169.3332) * transformX) + moveX, ((1812.6665) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((167.3334) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((166.6666) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),
        new createjs.Point(((163.3336) * transformX) + moveX, ((1808.0003) * transformY) + moveY),
        new createjs.Point(((160.6664) * transformX) + moveX, ((1804.9997) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((157.3334) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((156.6666) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((156.0000) * transformX) + moveX, ((1802.0000) * transformY) + moveY),
        new createjs.Point(((152.6670) * transformX) + moveX, ((1798.3337) * transformY) + moveY),
        new createjs.Point(((149.3330) * transformX) + moveX, ((1794.6663) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1791.0000) * transformY) + moveY),
        new createjs.Point(((142.6670) * transformX) + moveX, ((1788.0003) * transformY) + moveY),
        new createjs.Point(((139.3330) * transformX) + moveX, ((1784.9997) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((1782.0000) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((1781.3334) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((1780.6666) * transformY) + moveY),
        new createjs.Point(((136.0000) * transformX) + moveX, ((1780.0000) * transformY) + moveY),
        new createjs.Point(((133.0003) * transformX) + moveX, ((1777.3336) * transformY) + moveY),
        new createjs.Point(((129.9997) * transformX) + moveX, ((1774.6664) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1772.0000) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1771.3334) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1770.6666) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1770.0000) * transformY) + moveY),
        new createjs.Point(((126.3334) * transformX) + moveX, ((1769.6667) * transformY) + moveY),
        new createjs.Point(((125.6666) * transformX) + moveX, ((1769.3333) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((1768.6667) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((1768.3333) * transformY) + moveY),
        new createjs.Point(((125.0000) * transformX) + moveX, ((1768.0000) * transformY) + moveY),
        new createjs.Point(((123.6668) * transformX) + moveX, ((1767.0001) * transformY) + moveY),
        new createjs.Point(((122.3332) * transformX) + moveX, ((1765.9999) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1764.3334) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1763.6666) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1763.0000) * transformY) + moveY),
        new createjs.Point(((119.6668) * transformX) + moveX, ((1762.0001) * transformY) + moveY),
        new createjs.Point(((118.3332) * transformX) + moveX, ((1760.9999) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((1759.3334) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((1758.6666) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),
        new createjs.Point(((115.6668) * transformX) + moveX, ((1757.0001) * transformY) + moveY),
        new createjs.Point(((114.3332) * transformX) + moveX, ((1755.9999) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((1754.3334) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((1753.6666) * transformY) + moveY),
        new createjs.Point(((113.0000) * transformX) + moveX, ((1753.0000) * transformY) + moveY),
        new createjs.Point(((112.0001) * transformX) + moveX, ((1752.3334) * transformY) + moveY),
        new createjs.Point(((110.9999) * transformX) + moveX, ((1751.6666) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1751.0000) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1750.3334) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1749.6666) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1749.0000) * transformY) + moveY),
        new createjs.Point(((109.0001) * transformX) + moveX, ((1748.3334) * transformY) + moveY),
        new createjs.Point(((107.9999) * transformX) + moveX, ((1747.6666) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1747.0000) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1746.3334) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1745.6666) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1745.0000) * transformY) + moveY),
        new createjs.Point(((106.0001) * transformX) + moveX, ((1744.3334) * transformY) + moveY),
        new createjs.Point(((104.9999) * transformX) + moveX, ((1743.6666) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1742.3334) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1741.6666) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((103.3334) * transformX) + moveX, ((1740.6667) * transformY) + moveY),
        new createjs.Point(((102.6666) * transformX) + moveX, ((1740.3333) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1740.0000) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1739.3334) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1738.6666) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1738.0000) * transformY) + moveY),
        new createjs.Point(((101.0001) * transformX) + moveX, ((1737.3334) * transformY) + moveY),
        new createjs.Point(((99.9999) * transformX) + moveX, ((1736.6666) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1735.3334) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1734.6666) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1734.0000) * transformY) + moveY),
        new createjs.Point(((98.3334) * transformX) + moveX, ((1733.6667) * transformY) + moveY),
        new createjs.Point(((97.6666) * transformX) + moveX, ((1733.3333) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1733.0000) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1732.3334) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1731.6666) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),
        new createjs.Point(((96.3334) * transformX) + moveX, ((1730.6667) * transformY) + moveY),
        new createjs.Point(((95.6666) * transformX) + moveX, ((1730.3333) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((1730.0000) * transformY) + moveY),
        new createjs.Point(((94.6667) * transformX) + moveX, ((1729.0001) * transformY) + moveY),
        new createjs.Point(((94.3333) * transformX) + moveX, ((1727.9999) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1727.0000) * transformY) + moveY),
        new createjs.Point(((93.6667) * transformX) + moveX, ((1727.0000) * transformY) + moveY),
        new createjs.Point(((93.3333) * transformX) + moveX, ((1727.0000) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1727.0000) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1726.3334) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1725.6666) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),
        new createjs.Point(((92.3334) * transformX) + moveX, ((1724.6667) * transformY) + moveY),
        new createjs.Point(((91.6666) * transformX) + moveX, ((1724.3333) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1724.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1723.3334) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1722.6666) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1722.0000) * transformY) + moveY),
        new createjs.Point(((90.3334) * transformX) + moveX, ((1721.6667) * transformY) + moveY),
        new createjs.Point(((89.6666) * transformX) + moveX, ((1721.3333) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1721.0000) * transformY) + moveY),
        new createjs.Point(((88.6667) * transformX) + moveX, ((1719.6668) * transformY) + moveY),
        new createjs.Point(((88.3333) * transformX) + moveX, ((1718.3332) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1717.0000) * transformY) + moveY),
        new createjs.Point(((87.3334) * transformX) + moveX, ((1716.6667) * transformY) + moveY),
        new createjs.Point(((86.6666) * transformX) + moveX, ((1716.3333) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((1716.0000) * transformY) + moveY),
        new createjs.Point(((85.6667) * transformX) + moveX, ((1714.6668) * transformY) + moveY),
        new createjs.Point(((85.3333) * transformX) + moveX, ((1713.3332) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1712.0000) * transformY) + moveY),
        new createjs.Point(((84.3334) * transformX) + moveX, ((1711.6667) * transformY) + moveY),
        new createjs.Point(((83.6666) * transformX) + moveX, ((1711.3333) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1711.0000) * transformY) + moveY),
        new createjs.Point(((82.6667) * transformX) + moveX, ((1709.6668) * transformY) + moveY),
        new createjs.Point(((82.3333) * transformX) + moveX, ((1708.3332) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1707.0000) * transformY) + moveY),
        new createjs.Point(((81.3334) * transformX) + moveX, ((1706.6667) * transformY) + moveY),
        new createjs.Point(((80.6666) * transformX) + moveX, ((1706.3333) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1706.0000) * transformY) + moveY),
        new createjs.Point(((79.6667) * transformX) + moveX, ((1704.6668) * transformY) + moveY),
        new createjs.Point(((79.3333) * transformX) + moveX, ((1703.3332) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1702.0000) * transformY) + moveY),
        new createjs.Point(((78.3334) * transformX) + moveX, ((1701.6667) * transformY) + moveY),
        new createjs.Point(((77.6666) * transformX) + moveX, ((1701.3333) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1701.0000) * transformY) + moveY),
        new createjs.Point(((76.6667) * transformX) + moveX, ((1699.6668) * transformY) + moveY),
        new createjs.Point(((76.3333) * transformX) + moveX, ((1698.3332) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1697.0000) * transformY) + moveY),
        new createjs.Point(((75.6667) * transformX) + moveX, ((1697.0000) * transformY) + moveY),
        new createjs.Point(((75.3333) * transformX) + moveX, ((1697.0000) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1697.0000) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1696.3334) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1695.6666) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1695.0000) * transformY) + moveY),
        new createjs.Point(((74.6667) * transformX) + moveX, ((1695.0000) * transformY) + moveY),
        new createjs.Point(((74.3333) * transformX) + moveX, ((1695.0000) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1695.0000) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1694.3334) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1693.6666) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1693.0000) * transformY) + moveY),
        new createjs.Point(((73.6667) * transformX) + moveX, ((1693.0000) * transformY) + moveY),
        new createjs.Point(((73.3333) * transformX) + moveX, ((1693.0000) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1693.0000) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1692.3334) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1691.6666) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1691.0000) * transformY) + moveY),
        new createjs.Point(((72.6667) * transformX) + moveX, ((1691.0000) * transformY) + moveY),
        new createjs.Point(((72.3333) * transformX) + moveX, ((1691.0000) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1691.0000) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1690.3334) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1689.6666) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1689.0000) * transformY) + moveY),
        new createjs.Point(((71.3334) * transformX) + moveX, ((1688.6667) * transformY) + moveY),
        new createjs.Point(((70.6666) * transformX) + moveX, ((1688.3333) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1688.0000) * transformY) + moveY),
        new createjs.Point(((69.3334) * transformX) + moveX, ((1686.0002) * transformY) + moveY),
        new createjs.Point(((68.6666) * transformX) + moveX, ((1683.9998) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1682.0000) * transformY) + moveY),
        new createjs.Point(((67.6667) * transformX) + moveX, ((1682.0000) * transformY) + moveY),
        new createjs.Point(((67.3333) * transformX) + moveX, ((1682.0000) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1682.0000) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1681.3334) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1680.6666) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1680.0000) * transformY) + moveY),
        new createjs.Point(((66.6667) * transformX) + moveX, ((1680.0000) * transformY) + moveY),
        new createjs.Point(((66.3333) * transformX) + moveX, ((1680.0000) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1680.0000) * transformY) + moveY),
        new createjs.Point(((65.3334) * transformX) + moveX, ((1678.0002) * transformY) + moveY),
        new createjs.Point(((64.6666) * transformX) + moveX, ((1675.9998) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1674.0000) * transformY) + moveY),
        new createjs.Point(((63.6667) * transformX) + moveX, ((1674.0000) * transformY) + moveY),
        new createjs.Point(((63.3333) * transformX) + moveX, ((1674.0000) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1674.0000) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1673.0001) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1671.9999) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1671.0000) * transformY) + moveY),
        new createjs.Point(((62.6667) * transformX) + moveX, ((1671.0000) * transformY) + moveY),
        new createjs.Point(((62.3333) * transformX) + moveX, ((1671.0000) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1671.0000) * transformY) + moveY),
        new createjs.Point(((61.3334) * transformX) + moveX, ((1669.0002) * transformY) + moveY),
        new createjs.Point(((60.6666) * transformX) + moveX, ((1666.9998) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1665.0000) * transformY) + moveY),
        new createjs.Point(((59.6667) * transformX) + moveX, ((1665.0000) * transformY) + moveY),
        new createjs.Point(((59.3333) * transformX) + moveX, ((1665.0000) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1665.0000) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1664.3334) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1663.6666) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1663.0000) * transformY) + moveY),
        new createjs.Point(((58.6667) * transformX) + moveX, ((1663.0000) * transformY) + moveY),
        new createjs.Point(((58.3333) * transformX) + moveX, ((1663.0000) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1663.0000) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1662.3334) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1661.6666) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((57.6667) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((57.3333) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1660.0001) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1658.9999) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1658.0000) * transformY) + moveY),
        new createjs.Point(((56.6667) * transformX) + moveX, ((1658.0000) * transformY) + moveY),
        new createjs.Point(((56.3333) * transformX) + moveX, ((1658.0000) * transformY) + moveY),
        new createjs.Point(((56.0000) * transformX) + moveX, ((1658.0000) * transformY) + moveY),
        new createjs.Point(((55.6667) * transformX) + moveX, ((1656.6668) * transformY) + moveY),
        new createjs.Point(((55.3333) * transformX) + moveX, ((1655.3332) * transformY) + moveY),
        new createjs.Point(((55.0000) * transformX) + moveX, ((1654.0000) * transformY) + moveY),
        new createjs.Point(((54.6667) * transformX) + moveX, ((1654.0000) * transformY) + moveY),
        new createjs.Point(((54.3333) * transformX) + moveX, ((1654.0000) * transformY) + moveY),
        new createjs.Point(((54.0000) * transformX) + moveX, ((1654.0000) * transformY) + moveY),
        new createjs.Point(((54.0000) * transformX) + moveX, ((1653.0001) * transformY) + moveY),
        new createjs.Point(((54.0000) * transformX) + moveX, ((1651.9999) * transformY) + moveY),
        new createjs.Point(((54.0000) * transformX) + moveX, ((1651.0000) * transformY) + moveY),
        new createjs.Point(((53.6667) * transformX) + moveX, ((1651.0000) * transformY) + moveY),
        new createjs.Point(((53.3333) * transformX) + moveX, ((1651.0000) * transformY) + moveY),
        new createjs.Point(((53.0000) * transformX) + moveX, ((1651.0000) * transformY) + moveY),
        new createjs.Point(((53.0000) * transformX) + moveX, ((1650.3334) * transformY) + moveY),
        new createjs.Point(((53.0000) * transformX) + moveX, ((1649.6666) * transformY) + moveY),
        new createjs.Point(((53.0000) * transformX) + moveX, ((1649.0000) * transformY) + moveY),
        new createjs.Point(((52.6667) * transformX) + moveX, ((1649.0000) * transformY) + moveY),
        new createjs.Point(((52.3333) * transformX) + moveX, ((1649.0000) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1649.0000) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1648.0001) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1646.9999) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1646.0000) * transformY) + moveY),
        new createjs.Point(((51.6667) * transformX) + moveX, ((1646.0000) * transformY) + moveY),
        new createjs.Point(((51.3333) * transformX) + moveX, ((1646.0000) * transformY) + moveY),
        new createjs.Point(((51.0000) * transformX) + moveX, ((1646.0000) * transformY) + moveY),
        new createjs.Point(((51.0000) * transformX) + moveX, ((1645.3334) * transformY) + moveY),
        new createjs.Point(((51.0000) * transformX) + moveX, ((1644.6666) * transformY) + moveY),
        new createjs.Point(((51.0000) * transformX) + moveX, ((1644.0000) * transformY) + moveY),
        new createjs.Point(((50.6667) * transformX) + moveX, ((1644.0000) * transformY) + moveY),
        new createjs.Point(((50.3333) * transformX) + moveX, ((1644.0000) * transformY) + moveY),
        new createjs.Point(((50.0000) * transformX) + moveX, ((1644.0000) * transformY) + moveY),
        new createjs.Point(((50.0000) * transformX) + moveX, ((1643.0001) * transformY) + moveY),
        new createjs.Point(((50.0000) * transformX) + moveX, ((1641.9999) * transformY) + moveY),
        new createjs.Point(((50.0000) * transformX) + moveX, ((1641.0000) * transformY) + moveY),
        new createjs.Point(((49.6667) * transformX) + moveX, ((1641.0000) * transformY) + moveY),
        new createjs.Point(((49.3333) * transformX) + moveX, ((1641.0000) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1641.0000) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1640.3334) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1639.6666) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1639.0000) * transformY) + moveY),
        new createjs.Point(((48.6667) * transformX) + moveX, ((1639.0000) * transformY) + moveY),
        new createjs.Point(((48.3333) * transformX) + moveX, ((1639.0000) * transformY) + moveY),
        new createjs.Point(((48.0000) * transformX) + moveX, ((1639.0000) * transformY) + moveY),
        new createjs.Point(((47.6667) * transformX) + moveX, ((1637.0002) * transformY) + moveY),
        new createjs.Point(((47.3333) * transformX) + moveX, ((1634.9998) * transformY) + moveY),
        new createjs.Point(((47.0000) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((46.6667) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((46.3333) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1632.3334) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1631.6666) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((45.6667) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((45.3333) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((45.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((44.6667) * transformX) + moveX, ((1629.0002) * transformY) + moveY),
        new createjs.Point(((44.3333) * transformX) + moveX, ((1626.9998) * transformY) + moveY),
        new createjs.Point(((44.0000) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((43.6667) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((43.3333) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((43.0000) * transformX) + moveX, ((1625.0000) * transformY) + moveY),
        new createjs.Point(((43.0000) * transformX) + moveX, ((1624.0001) * transformY) + moveY),
        new createjs.Point(((43.0000) * transformX) + moveX, ((1622.9999) * transformY) + moveY),
        new createjs.Point(((43.0000) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((42.6667) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((42.3333) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((42.0000) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((42.0000) * transformX) + moveX, ((1621.0001) * transformY) + moveY),
        new createjs.Point(((42.0000) * transformX) + moveX, ((1619.9999) * transformY) + moveY),
        new createjs.Point(((42.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((41.6667) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((41.3333) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((41.0000) * transformX) + moveX, ((1619.0000) * transformY) + moveY),
        new createjs.Point(((41.0000) * transformX) + moveX, ((1618.0001) * transformY) + moveY),
        new createjs.Point(((41.0000) * transformX) + moveX, ((1616.9999) * transformY) + moveY),
        new createjs.Point(((41.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((40.6667) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((40.3333) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1615.0001) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1613.9999) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((39.6667) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((39.3333) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((39.0000) * transformX) + moveX, ((1613.0000) * transformY) + moveY),
        new createjs.Point(((39.0000) * transformX) + moveX, ((1611.6668) * transformY) + moveY),
        new createjs.Point(((39.0000) * transformX) + moveX, ((1610.3332) * transformY) + moveY),
        new createjs.Point(((39.0000) * transformX) + moveX, ((1609.0000) * transformY) + moveY),
        new createjs.Point(((38.6667) * transformX) + moveX, ((1609.0000) * transformY) + moveY),
        new createjs.Point(((38.3333) * transformX) + moveX, ((1609.0000) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1609.0000) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1608.0001) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1606.9999) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((37.6667) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((37.3333) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1605.0001) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1603.9999) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((36.6667) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((36.3333) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((36.0000) * transformX) + moveX, ((1603.0000) * transformY) + moveY),
        new createjs.Point(((35.6667) * transformX) + moveX, ((1600.6669) * transformY) + moveY),
        new createjs.Point(((35.3333) * transformX) + moveX, ((1598.3331) * transformY) + moveY),
        new createjs.Point(((35.0000) * transformX) + moveX, ((1596.0000) * transformY) + moveY),
        new createjs.Point(((34.6667) * transformX) + moveX, ((1596.0000) * transformY) + moveY),
        new createjs.Point(((34.3333) * transformX) + moveX, ((1596.0000) * transformY) + moveY),
        new createjs.Point(((34.0000) * transformX) + moveX, ((1596.0000) * transformY) + moveY),
        new createjs.Point(((33.6667) * transformX) + moveX, ((1593.3336) * transformY) + moveY),
        new createjs.Point(((33.3333) * transformX) + moveX, ((1590.6664) * transformY) + moveY),
        new createjs.Point(((33.0000) * transformX) + moveX, ((1588.0000) * transformY) + moveY),
        new createjs.Point(((32.6667) * transformX) + moveX, ((1588.0000) * transformY) + moveY),
        new createjs.Point(((32.3333) * transformX) + moveX, ((1588.0000) * transformY) + moveY),
        new createjs.Point(((32.0000) * transformX) + moveX, ((1588.0000) * transformY) + moveY),
        new createjs.Point(((31.3334) * transformX) + moveX, ((1584.0004) * transformY) + moveY),
        new createjs.Point(((30.6666) * transformX) + moveX, ((1579.9996) * transformY) + moveY),
        new createjs.Point(((30.0000) * transformX) + moveX, ((1576.0000) * transformY) + moveY),
        new createjs.Point(((29.6667) * transformX) + moveX, ((1576.0000) * transformY) + moveY),
        new createjs.Point(((29.3333) * transformX) + moveX, ((1576.0000) * transformY) + moveY),
        new createjs.Point(((29.0000) * transformX) + moveX, ((1576.0000) * transformY) + moveY),
        new createjs.Point(((28.6667) * transformX) + moveX, ((1572.6670) * transformY) + moveY),
        new createjs.Point(((28.3333) * transformX) + moveX, ((1569.3330) * transformY) + moveY),
        new createjs.Point(((28.0000) * transformX) + moveX, ((1566.0000) * transformY) + moveY),
        new createjs.Point(((25.4027) * transformX) + moveX, ((1557.6965) * transformY) + moveY),
        new createjs.Point(((24.5231) * transformX) + moveX, ((1546.5504) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1538.0000) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1535.3336) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1532.6664) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((21.6667) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((21.3333) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((1530.0000) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((1527.3336) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((1524.6664) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((1522.0000) * transformY) + moveY),
        new createjs.Point(((20.6667) * transformX) + moveX, ((1522.0000) * transformY) + moveY),
        new createjs.Point(((20.3333) * transformX) + moveX, ((1522.0000) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((1522.0000) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((1518.6670) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((1515.3330) * transformY) + moveY),
        new createjs.Point(((20.0000) * transformX) + moveX, ((1512.0000) * transformY) + moveY),
        new createjs.Point(((19.6667) * transformX) + moveX, ((1512.0000) * transformY) + moveY),
        new createjs.Point(((19.3333) * transformX) + moveX, ((1512.0000) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((1512.0000) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((1507.0005) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((1501.9995) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((1497.0000) * transformY) + moveY),
        new createjs.Point(((17.5976) * transformX) + moveX, ((1492.0448) * transformY) + moveY),
        new createjs.Point(((17.9980) * transformX) + moveX, ((1483.5497) * transformY) + moveY),
        new createjs.Point(((18.0000) * transformX) + moveX, ((1477.0000) * transformY) + moveY),
        new createjs.Point(((18.0036) * transformX) + moveX, ((1465.1143) * transformY) + moveY),
        new createjs.Point(((16.2138) * transformX) + moveX, ((1449.0694) * transformY) + moveY),
        new createjs.Point(((19.0000) * transformX) + moveX, ((1439.0000) * transformY) + moveY),
        new createjs.Point(((19.6666) * transformX) + moveX, ((1426.0013) * transformY) + moveY),
        new createjs.Point(((20.3334) * transformX) + moveX, ((1412.9987) * transformY) + moveY),
        new createjs.Point(((21.0000) * transformX) + moveX, ((1400.0000) * transformY) + moveY),
        new createjs.Point(((21.3333) * transformX) + moveX, ((1400.0000) * transformY) + moveY),
        new createjs.Point(((21.6667) * transformX) + moveX, ((1400.0000) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1400.0000) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1397.3336) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1394.6664) * transformY) + moveY),
        new createjs.Point(((22.0000) * transformX) + moveX, ((1392.0000) * transformY) + moveY),
        new createjs.Point(((22.3333) * transformX) + moveX, ((1392.0000) * transformY) + moveY),
        new createjs.Point(((22.6667) * transformX) + moveX, ((1392.0000) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((1392.0000) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((1389.3336) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((1386.6664) * transformY) + moveY),
        new createjs.Point(((23.0000) * transformX) + moveX, ((1384.0000) * transformY) + moveY),
        new createjs.Point(((23.3333) * transformX) + moveX, ((1384.0000) * transformY) + moveY),
        new createjs.Point(((23.6667) * transformX) + moveX, ((1384.0000) * transformY) + moveY),
        new createjs.Point(((24.0000) * transformX) + moveX, ((1384.0000) * transformY) + moveY),
        new createjs.Point(((24.0000) * transformX) + moveX, ((1381.6669) * transformY) + moveY),
        new createjs.Point(((24.0000) * transformX) + moveX, ((1379.3331) * transformY) + moveY),
        new createjs.Point(((24.0000) * transformX) + moveX, ((1377.0000) * transformY) + moveY),
        new createjs.Point(((24.3333) * transformX) + moveX, ((1377.0000) * transformY) + moveY),
        new createjs.Point(((24.6667) * transformX) + moveX, ((1377.0000) * transformY) + moveY),
        new createjs.Point(((25.0000) * transformX) + moveX, ((1377.0000) * transformY) + moveY),
        new createjs.Point(((25.0000) * transformX) + moveX, ((1374.6669) * transformY) + moveY),
        new createjs.Point(((25.0000) * transformX) + moveX, ((1372.3331) * transformY) + moveY),
        new createjs.Point(((25.0000) * transformX) + moveX, ((1370.0000) * transformY) + moveY),
        new createjs.Point(((26.9402) * transformX) + moveX, ((1363.3990) * transformY) + moveY),
        new createjs.Point(((27.9822) * transformX) + moveX, ((1354.4856) * transformY) + moveY),
        new createjs.Point(((30.0000) * transformX) + moveX, ((1348.0000) * transformY) + moveY),
        new createjs.Point(((30.0000) * transformX) + moveX, ((1346.3335) * transformY) + moveY),
        new createjs.Point(((30.0000) * transformX) + moveX, ((1344.6665) * transformY) + moveY),
        new createjs.Point(((30.0000) * transformX) + moveX, ((1343.0000) * transformY) + moveY),
        new createjs.Point(((30.3333) * transformX) + moveX, ((1343.0000) * transformY) + moveY),
        new createjs.Point(((30.6667) * transformX) + moveX, ((1343.0000) * transformY) + moveY),
        new createjs.Point(((31.0000) * transformX) + moveX, ((1343.0000) * transformY) + moveY),
        new createjs.Point(((31.3333) * transformX) + moveX, ((1340.0003) * transformY) + moveY),
        new createjs.Point(((31.6667) * transformX) + moveX, ((1336.9997) * transformY) + moveY),
        new createjs.Point(((32.0000) * transformX) + moveX, ((1334.0000) * transformY) + moveY),
        new createjs.Point(((32.3333) * transformX) + moveX, ((1334.0000) * transformY) + moveY),
        new createjs.Point(((32.6667) * transformX) + moveX, ((1334.0000) * transformY) + moveY),
        new createjs.Point(((33.0000) * transformX) + moveX, ((1334.0000) * transformY) + moveY),
        new createjs.Point(((33.3333) * transformX) + moveX, ((1331.0003) * transformY) + moveY),
        new createjs.Point(((33.6667) * transformX) + moveX, ((1327.9997) * transformY) + moveY),
        new createjs.Point(((34.0000) * transformX) + moveX, ((1325.0000) * transformY) + moveY),
        new createjs.Point(((34.3333) * transformX) + moveX, ((1325.0000) * transformY) + moveY),
        new createjs.Point(((34.6667) * transformX) + moveX, ((1325.0000) * transformY) + moveY),
        new createjs.Point(((35.0000) * transformX) + moveX, ((1325.0000) * transformY) + moveY),
        new createjs.Point(((35.3333) * transformX) + moveX, ((1322.3336) * transformY) + moveY),
        new createjs.Point(((35.6667) * transformX) + moveX, ((1319.6664) * transformY) + moveY),
        new createjs.Point(((36.0000) * transformX) + moveX, ((1317.0000) * transformY) + moveY),
        new createjs.Point(((36.3333) * transformX) + moveX, ((1317.0000) * transformY) + moveY),
        new createjs.Point(((36.6667) * transformX) + moveX, ((1317.0000) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1317.0000) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1315.6668) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1314.3332) * transformY) + moveY),
        new createjs.Point(((37.0000) * transformX) + moveX, ((1313.0000) * transformY) + moveY),
        new createjs.Point(((37.3333) * transformX) + moveX, ((1313.0000) * transformY) + moveY),
        new createjs.Point(((37.6667) * transformX) + moveX, ((1313.0000) * transformY) + moveY),
        new createjs.Point(((38.0000) * transformX) + moveX, ((1313.0000) * transformY) + moveY),
        new createjs.Point(((38.3333) * transformX) + moveX, ((1310.6669) * transformY) + moveY),
        new createjs.Point(((38.6667) * transformX) + moveX, ((1308.3331) * transformY) + moveY),
        new createjs.Point(((39.0000) * transformX) + moveX, ((1306.0000) * transformY) + moveY),
        new createjs.Point(((39.3333) * transformX) + moveX, ((1306.0000) * transformY) + moveY),
        new createjs.Point(((39.6667) * transformX) + moveX, ((1306.0000) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1306.0000) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1305.0001) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1303.9999) * transformY) + moveY),
        new createjs.Point(((40.0000) * transformX) + moveX, ((1303.0000) * transformY) + moveY),
        new createjs.Point(((40.3333) * transformX) + moveX, ((1303.0000) * transformY) + moveY),
        new createjs.Point(((40.6667) * transformX) + moveX, ((1303.0000) * transformY) + moveY),
        new createjs.Point(((41.0000) * transformX) + moveX, ((1303.0000) * transformY) + moveY),
        new createjs.Point(((41.9999) * transformX) + moveX, ((1298.3338) * transformY) + moveY),
        new createjs.Point(((43.0001) * transformX) + moveX, ((1293.6662) * transformY) + moveY),
        new createjs.Point(((44.0000) * transformX) + moveX, ((1289.0000) * transformY) + moveY),
        new createjs.Point(((44.3333) * transformX) + moveX, ((1289.0000) * transformY) + moveY),
        new createjs.Point(((44.6667) * transformX) + moveX, ((1289.0000) * transformY) + moveY),
        new createjs.Point(((45.0000) * transformX) + moveX, ((1289.0000) * transformY) + moveY),
        new createjs.Point(((45.0000) * transformX) + moveX, ((1288.0001) * transformY) + moveY),
        new createjs.Point(((45.0000) * transformX) + moveX, ((1286.9999) * transformY) + moveY),
        new createjs.Point(((45.0000) * transformX) + moveX, ((1286.0000) * transformY) + moveY),
        new createjs.Point(((45.3333) * transformX) + moveX, ((1286.0000) * transformY) + moveY),
        new createjs.Point(((45.6667) * transformX) + moveX, ((1286.0000) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1286.0000) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1285.0001) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1283.9999) * transformY) + moveY),
        new createjs.Point(((46.0000) * transformX) + moveX, ((1283.0000) * transformY) + moveY),
        new createjs.Point(((46.3333) * transformX) + moveX, ((1283.0000) * transformY) + moveY),
        new createjs.Point(((46.6667) * transformX) + moveX, ((1283.0000) * transformY) + moveY),
        new createjs.Point(((47.0000) * transformX) + moveX, ((1283.0000) * transformY) + moveY),
        new createjs.Point(((47.0000) * transformX) + moveX, ((1282.0001) * transformY) + moveY),
        new createjs.Point(((47.0000) * transformX) + moveX, ((1280.9999) * transformY) + moveY),
        new createjs.Point(((47.0000) * transformX) + moveX, ((1280.0000) * transformY) + moveY),
        new createjs.Point(((47.3333) * transformX) + moveX, ((1280.0000) * transformY) + moveY),
        new createjs.Point(((47.6667) * transformX) + moveX, ((1280.0000) * transformY) + moveY),
        new createjs.Point(((48.0000) * transformX) + moveX, ((1280.0000) * transformY) + moveY),
        new createjs.Point(((48.0000) * transformX) + moveX, ((1279.0001) * transformY) + moveY),
        new createjs.Point(((48.0000) * transformX) + moveX, ((1277.9999) * transformY) + moveY),
        new createjs.Point(((48.0000) * transformX) + moveX, ((1277.0000) * transformY) + moveY),
        new createjs.Point(((48.3333) * transformX) + moveX, ((1277.0000) * transformY) + moveY),
        new createjs.Point(((48.6667) * transformX) + moveX, ((1277.0000) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1277.0000) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1276.0001) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1274.9999) * transformY) + moveY),
        new createjs.Point(((49.0000) * transformX) + moveX, ((1274.0000) * transformY) + moveY),
        new createjs.Point(((49.3333) * transformX) + moveX, ((1274.0000) * transformY) + moveY),
        new createjs.Point(((49.6667) * transformX) + moveX, ((1274.0000) * transformY) + moveY),
        new createjs.Point(((50.0000) * transformX) + moveX, ((1274.0000) * transformY) + moveY),
        new createjs.Point(((50.3333) * transformX) + moveX, ((1272.0002) * transformY) + moveY),
        new createjs.Point(((50.6667) * transformX) + moveX, ((1269.9998) * transformY) + moveY),
        new createjs.Point(((51.0000) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((51.3333) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((51.6667) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1267.3334) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1266.6666) * transformY) + moveY),
        new createjs.Point(((52.0000) * transformX) + moveX, ((1266.0000) * transformY) + moveY),
        new createjs.Point(((52.3333) * transformX) + moveX, ((1266.0000) * transformY) + moveY),
        new createjs.Point(((52.6667) * transformX) + moveX, ((1266.0000) * transformY) + moveY),
        new createjs.Point(((53.0000) * transformX) + moveX, ((1266.0000) * transformY) + moveY),
        new createjs.Point(((53.6666) * transformX) + moveX, ((1263.0003) * transformY) + moveY),
        new createjs.Point(((54.3334) * transformX) + moveX, ((1259.9997) * transformY) + moveY),
        new createjs.Point(((55.0000) * transformX) + moveX, ((1257.0000) * transformY) + moveY),
        new createjs.Point(((55.3333) * transformX) + moveX, ((1257.0000) * transformY) + moveY),
        new createjs.Point(((55.6667) * transformX) + moveX, ((1257.0000) * transformY) + moveY),
        new createjs.Point(((56.0000) * transformX) + moveX, ((1257.0000) * transformY) + moveY),
        new createjs.Point(((56.0000) * transformX) + moveX, ((1256.3334) * transformY) + moveY),
        new createjs.Point(((56.0000) * transformX) + moveX, ((1255.6666) * transformY) + moveY),
        new createjs.Point(((56.0000) * transformX) + moveX, ((1255.0000) * transformY) + moveY),
        new createjs.Point(((56.3333) * transformX) + moveX, ((1255.0000) * transformY) + moveY),
        new createjs.Point(((56.6667) * transformX) + moveX, ((1255.0000) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1255.0000) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1254.0001) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1252.9999) * transformY) + moveY),
        new createjs.Point(((57.0000) * transformX) + moveX, ((1252.0000) * transformY) + moveY),
        new createjs.Point(((57.3333) * transformX) + moveX, ((1252.0000) * transformY) + moveY),
        new createjs.Point(((57.6667) * transformX) + moveX, ((1252.0000) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1252.0000) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1251.3334) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1250.6666) * transformY) + moveY),
        new createjs.Point(((58.0000) * transformX) + moveX, ((1250.0000) * transformY) + moveY),
        new createjs.Point(((58.3333) * transformX) + moveX, ((1250.0000) * transformY) + moveY),
        new createjs.Point(((58.6667) * transformX) + moveX, ((1250.0000) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1250.0000) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1249.0001) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1247.9999) * transformY) + moveY),
        new createjs.Point(((59.0000) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((59.3333) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((59.6667) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1247.0000) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1246.3334) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1245.6666) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1245.0000) * transformY) + moveY),
        new createjs.Point(((60.3333) * transformX) + moveX, ((1245.0000) * transformY) + moveY),
        new createjs.Point(((60.6667) * transformX) + moveX, ((1245.0000) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1245.0000) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1244.0001) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1242.9999) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1242.0000) * transformY) + moveY),
        new createjs.Point(((61.3333) * transformX) + moveX, ((1242.0000) * transformY) + moveY),
        new createjs.Point(((61.6667) * transformX) + moveX, ((1242.0000) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1242.0000) * transformY) + moveY),
        new createjs.Point(((62.3333) * transformX) + moveX, ((1240.6668) * transformY) + moveY),
        new createjs.Point(((62.6667) * transformX) + moveX, ((1239.3332) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1238.0000) * transformY) + moveY),
        new createjs.Point(((63.3333) * transformX) + moveX, ((1238.0000) * transformY) + moveY),
        new createjs.Point(((63.6667) * transformX) + moveX, ((1238.0000) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1238.0000) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1237.0001) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1235.9999) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1235.0000) * transformY) + moveY),
        new createjs.Point(((64.3333) * transformX) + moveX, ((1235.0000) * transformY) + moveY),
        new createjs.Point(((64.6667) * transformX) + moveX, ((1235.0000) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1235.0000) * transformY) + moveY),
        new createjs.Point(((65.3333) * transformX) + moveX, ((1233.6668) * transformY) + moveY),
        new createjs.Point(((65.6667) * transformX) + moveX, ((1232.3332) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1231.0000) * transformY) + moveY),
        new createjs.Point(((66.3333) * transformX) + moveX, ((1231.0000) * transformY) + moveY),
        new createjs.Point(((66.6667) * transformX) + moveX, ((1231.0000) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1231.0000) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1230.0001) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1228.9999) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((67.3333) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((67.6667) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1228.0000) * transformY) + moveY),
        new createjs.Point(((68.6666) * transformX) + moveX, ((1226.0002) * transformY) + moveY),
        new createjs.Point(((69.3334) * transformX) + moveX, ((1223.9998) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1222.0000) * transformY) + moveY),
        new createjs.Point(((70.3333) * transformX) + moveX, ((1222.0000) * transformY) + moveY),
        new createjs.Point(((70.6667) * transformX) + moveX, ((1222.0000) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((1222.0000) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((1221.3334) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((1220.6666) * transformY) + moveY),
        new createjs.Point(((71.0000) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((71.3333) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((71.6667) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1220.0000) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1219.0001) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1217.9999) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1217.0000) * transformY) + moveY),
        new createjs.Point(((72.3333) * transformX) + moveX, ((1217.0000) * transformY) + moveY),
        new createjs.Point(((72.6667) * transformX) + moveX, ((1217.0000) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1217.0000) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1216.3334) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1215.6666) * transformY) + moveY),
        new createjs.Point(((73.0000) * transformX) + moveX, ((1215.0000) * transformY) + moveY),
        new createjs.Point(((73.3333) * transformX) + moveX, ((1215.0000) * transformY) + moveY),
        new createjs.Point(((73.6667) * transformX) + moveX, ((1215.0000) * transformY) + moveY),
        new createjs.Point(((74.0000) * transformX) + moveX, ((1215.0000) * transformY) + moveY),
        new createjs.Point(((74.3333) * transformX) + moveX, ((1213.6668) * transformY) + moveY),
        new createjs.Point(((74.6667) * transformX) + moveX, ((1212.3332) * transformY) + moveY),
        new createjs.Point(((75.0000) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((75.3333) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((75.6667) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1210.3334) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1209.6666) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1209.0000) * transformY) + moveY),
        new createjs.Point(((76.3333) * transformX) + moveX, ((1209.0000) * transformY) + moveY),
        new createjs.Point(((76.6667) * transformX) + moveX, ((1209.0000) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1209.0000) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1208.3334) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1207.6666) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1207.0000) * transformY) + moveY),
        new createjs.Point(((77.3333) * transformX) + moveX, ((1207.0000) * transformY) + moveY),
        new createjs.Point(((77.6667) * transformX) + moveX, ((1207.0000) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((1207.0000) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((1206.3334) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((1205.6666) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((78.3333) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((78.6667) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1205.0000) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1204.3334) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1203.6666) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1203.0000) * transformY) + moveY),
        new createjs.Point(((79.3333) * transformX) + moveX, ((1203.0000) * transformY) + moveY),
        new createjs.Point(((79.6667) * transformX) + moveX, ((1203.0000) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1203.0000) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1202.3334) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1201.6666) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1201.0000) * transformY) + moveY),
        new createjs.Point(((80.3333) * transformX) + moveX, ((1201.0000) * transformY) + moveY),
        new createjs.Point(((80.6667) * transformX) + moveX, ((1201.0000) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1201.0000) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1200.3334) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1199.6666) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((81.3333) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((81.6667) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1199.0000) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1198.3334) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1197.6666) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1197.0000) * transformY) + moveY),
        new createjs.Point(((82.3333) * transformX) + moveX, ((1197.0000) * transformY) + moveY),
        new createjs.Point(((82.6667) * transformX) + moveX, ((1197.0000) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1197.0000) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1196.3334) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1195.6666) * transformY) + moveY),
        new createjs.Point(((83.0000) * transformX) + moveX, ((1195.0000) * transformY) + moveY),
        new createjs.Point(((83.3333) * transformX) + moveX, ((1195.0000) * transformY) + moveY),
        new createjs.Point(((83.6667) * transformX) + moveX, ((1195.0000) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1195.0000) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1194.3334) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1193.6666) * transformY) + moveY),
        new createjs.Point(((84.0000) * transformX) + moveX, ((1193.0000) * transformY) + moveY),
        new createjs.Point(((84.3333) * transformX) + moveX, ((1193.0000) * transformY) + moveY),
        new createjs.Point(((84.6667) * transformX) + moveX, ((1193.0000) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1193.0000) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1192.3334) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1191.6666) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1191.0000) * transformY) + moveY),
        new createjs.Point(((85.3333) * transformX) + moveX, ((1191.0000) * transformY) + moveY),
        new createjs.Point(((85.6667) * transformX) + moveX, ((1191.0000) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((1191.0000) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((1190.3334) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((1189.6666) * transformY) + moveY),
        new createjs.Point(((86.0000) * transformX) + moveX, ((1189.0000) * transformY) + moveY),
        new createjs.Point(((86.6666) * transformX) + moveX, ((1188.6667) * transformY) + moveY),
        new createjs.Point(((87.3334) * transformX) + moveX, ((1188.3333) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1188.0000) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1187.3334) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1186.6666) * transformY) + moveY),
        new createjs.Point(((88.0000) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((88.3333) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((88.6667) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1186.0000) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1185.3334) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1184.6666) * transformY) + moveY),
        new createjs.Point(((89.0000) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((89.3333) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((89.6667) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1184.0000) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1183.3334) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1182.6666) * transformY) + moveY),
        new createjs.Point(((90.0000) * transformX) + moveX, ((1182.0000) * transformY) + moveY),
        new createjs.Point(((90.3333) * transformX) + moveX, ((1182.0000) * transformY) + moveY),
        new createjs.Point(((90.6667) * transformX) + moveX, ((1182.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1182.0000) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1181.3334) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1180.6666) * transformY) + moveY),
        new createjs.Point(((91.0000) * transformX) + moveX, ((1180.0000) * transformY) + moveY),
        new createjs.Point(((91.6666) * transformX) + moveX, ((1179.6667) * transformY) + moveY),
        new createjs.Point(((92.3334) * transformX) + moveX, ((1179.3333) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1179.0000) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1178.3334) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1177.6666) * transformY) + moveY),
        new createjs.Point(((93.0000) * transformX) + moveX, ((1177.0000) * transformY) + moveY),
        new createjs.Point(((93.3333) * transformX) + moveX, ((1177.0000) * transformY) + moveY),
        new createjs.Point(((93.6667) * transformX) + moveX, ((1177.0000) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1177.0000) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1176.3334) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1175.6666) * transformY) + moveY),
        new createjs.Point(((94.0000) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((94.3333) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((94.6667) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((95.0000) * transformX) + moveX, ((1175.0000) * transformY) + moveY),
        new createjs.Point(((95.3333) * transformX) + moveX, ((1173.6668) * transformY) + moveY),
        new createjs.Point(((95.6667) * transformX) + moveX, ((1172.3332) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((1171.0000) * transformY) + moveY),
        new createjs.Point(((96.6666) * transformX) + moveX, ((1170.6667) * transformY) + moveY),
        new createjs.Point(((97.3334) * transformX) + moveX, ((1170.3333) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1170.0000) * transformY) + moveY),
        new createjs.Point(((98.3333) * transformX) + moveX, ((1168.6668) * transformY) + moveY),
        new createjs.Point(((98.6667) * transformX) + moveX, ((1167.3332) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1166.0000) * transformY) + moveY),
        new createjs.Point(((99.6666) * transformX) + moveX, ((1165.6667) * transformY) + moveY),
        new createjs.Point(((100.3334) * transformX) + moveX, ((1165.3333) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1165.0000) * transformY) + moveY),
        new createjs.Point(((101.3333) * transformX) + moveX, ((1163.6668) * transformY) + moveY),
        new createjs.Point(((101.6667) * transformX) + moveX, ((1162.3332) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1161.0000) * transformY) + moveY),
        new createjs.Point(((102.6666) * transformX) + moveX, ((1160.6667) * transformY) + moveY),
        new createjs.Point(((103.3334) * transformX) + moveX, ((1160.3333) * transformY) + moveY),
        new createjs.Point(((104.0000) * transformX) + moveX, ((1160.0000) * transformY) + moveY),
        new createjs.Point(((104.3333) * transformX) + moveX, ((1158.6668) * transformY) + moveY),
        new createjs.Point(((104.6667) * transformX) + moveX, ((1157.3332) * transformY) + moveY),
        new createjs.Point(((105.0000) * transformX) + moveX, ((1156.0000) * transformY) + moveY),
        new createjs.Point(((105.6666) * transformX) + moveX, ((1155.6667) * transformY) + moveY),
        new createjs.Point(((106.3334) * transformX) + moveX, ((1155.3333) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1155.0000) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1154.3334) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1153.6666) * transformY) + moveY),
        new createjs.Point(((107.0000) * transformX) + moveX, ((1153.0000) * transformY) + moveY),
        new createjs.Point(((107.6666) * transformX) + moveX, ((1152.6667) * transformY) + moveY),
        new createjs.Point(((108.3334) * transformX) + moveX, ((1152.3333) * transformY) + moveY),
        new createjs.Point(((109.0000) * transformX) + moveX, ((1152.0000) * transformY) + moveY),
        new createjs.Point(((109.3333) * transformX) + moveX, ((1150.6668) * transformY) + moveY),
        new createjs.Point(((109.6667) * transformX) + moveX, ((1149.3332) * transformY) + moveY),
        new createjs.Point(((110.0000) * transformX) + moveX, ((1148.0000) * transformY) + moveY),
        new createjs.Point(((110.6666) * transformX) + moveX, ((1147.6667) * transformY) + moveY),
        new createjs.Point(((111.3334) * transformX) + moveX, ((1147.3333) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((1147.0000) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((1146.3334) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((1145.6666) * transformY) + moveY),
        new createjs.Point(((112.0000) * transformX) + moveX, ((1145.0000) * transformY) + moveY),
        new createjs.Point(((112.6666) * transformX) + moveX, ((1144.6667) * transformY) + moveY),
        new createjs.Point(((113.3334) * transformX) + moveX, ((1144.3333) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1144.0000) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1143.3334) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1142.6666) * transformY) + moveY),
        new createjs.Point(((114.0000) * transformX) + moveX, ((1142.0000) * transformY) + moveY),
        new createjs.Point(((114.6666) * transformX) + moveX, ((1141.6667) * transformY) + moveY),
        new createjs.Point(((115.3334) * transformX) + moveX, ((1141.3333) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((1141.0000) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((1140.3334) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((1139.6666) * transformY) + moveY),
        new createjs.Point(((116.0000) * transformX) + moveX, ((1139.0000) * transformY) + moveY),
        new createjs.Point(((116.6666) * transformX) + moveX, ((1138.6667) * transformY) + moveY),
        new createjs.Point(((117.3334) * transformX) + moveX, ((1138.3333) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1138.0000) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1137.3334) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1136.6666) * transformY) + moveY),
        new createjs.Point(((118.0000) * transformX) + moveX, ((1136.0000) * transformY) + moveY),
        new createjs.Point(((118.6666) * transformX) + moveX, ((1135.6667) * transformY) + moveY),
        new createjs.Point(((119.3334) * transformX) + moveX, ((1135.3333) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((1135.0000) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((1134.3334) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((1133.6666) * transformY) + moveY),
        new createjs.Point(((120.0000) * transformX) + moveX, ((1133.0000) * transformY) + moveY),
        new createjs.Point(((120.3333) * transformX) + moveX, ((1133.0000) * transformY) + moveY),
        new createjs.Point(((120.6667) * transformX) + moveX, ((1133.0000) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((1133.0000) * transformY) + moveY),
        new createjs.Point(((121.3333) * transformX) + moveX, ((1132.0001) * transformY) + moveY),
        new createjs.Point(((121.6667) * transformX) + moveX, ((1130.9999) * transformY) + moveY),
        new createjs.Point(((122.0000) * transformX) + moveX, ((1130.0000) * transformY) + moveY),
        new createjs.Point(((122.6666) * transformX) + moveX, ((1129.6667) * transformY) + moveY),
        new createjs.Point(((123.3334) * transformX) + moveX, ((1129.3333) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((1129.0000) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((1128.3334) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((1127.6666) * transformY) + moveY),
        new createjs.Point(((124.0000) * transformX) + moveX, ((1127.0000) * transformY) + moveY),
        new createjs.Point(((124.9999) * transformX) + moveX, ((1126.3334) * transformY) + moveY),
        new createjs.Point(((126.0001) * transformX) + moveX, ((1125.6666) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1125.0000) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1124.3334) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1123.6666) * transformY) + moveY),
        new createjs.Point(((127.0000) * transformX) + moveX, ((1123.0000) * transformY) + moveY),
        new createjs.Point(((127.6666) * transformX) + moveX, ((1122.6667) * transformY) + moveY),
        new createjs.Point(((128.3334) * transformX) + moveX, ((1122.3333) * transformY) + moveY),
        new createjs.Point(((129.0000) * transformX) + moveX, ((1122.0000) * transformY) + moveY),
        new createjs.Point(((129.3333) * transformX) + moveX, ((1121.0001) * transformY) + moveY),
        new createjs.Point(((129.6667) * transformX) + moveX, ((1119.9999) * transformY) + moveY),
        new createjs.Point(((130.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((130.3333) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((130.6667) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1118.3334) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1117.6666) * transformY) + moveY),
        new createjs.Point(((131.0000) * transformX) + moveX, ((1117.0000) * transformY) + moveY),
        new createjs.Point(((131.9999) * transformX) + moveX, ((1116.3334) * transformY) + moveY),
        new createjs.Point(((133.0001) * transformX) + moveX, ((1115.6666) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((1115.0000) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((1114.3334) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((1113.6666) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((1113.0000) * transformY) + moveY),
        new createjs.Point(((134.9999) * transformX) + moveX, ((1112.3334) * transformY) + moveY),
        new createjs.Point(((136.0001) * transformX) + moveX, ((1111.6666) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1111.0000) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1110.3334) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1109.6666) * transformY) + moveY),
        new createjs.Point(((137.0000) * transformX) + moveX, ((1109.0000) * transformY) + moveY),
        new createjs.Point(((137.9999) * transformX) + moveX, ((1108.3334) * transformY) + moveY),
        new createjs.Point(((139.0001) * transformX) + moveX, ((1107.6666) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1107.0000) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1106.3334) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1105.6666) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1105.0000) * transformY) + moveY),
        new createjs.Point(((140.9999) * transformX) + moveX, ((1104.3334) * transformY) + moveY),
        new createjs.Point(((142.0001) * transformX) + moveX, ((1103.6666) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1103.0000) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1102.3334) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1101.6666) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1101.0000) * transformY) + moveY),
        new createjs.Point(((143.9999) * transformX) + moveX, ((1100.3334) * transformY) + moveY),
        new createjs.Point(((145.0001) * transformX) + moveX, ((1099.6666) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1099.0000) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1098.3334) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1097.6666) * transformY) + moveY),
        new createjs.Point(((146.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),
        new createjs.Point(((147.3332) * transformX) + moveX, ((1096.0001) * transformY) + moveY),
        new createjs.Point(((148.6668) * transformX) + moveX, ((1094.9999) * transformY) + moveY),
        new createjs.Point(((150.0000) * transformX) + moveX, ((1094.0000) * transformY) + moveY),
        new createjs.Point(((150.3333) * transformX) + moveX, ((1093.0001) * transformY) + moveY),
        new createjs.Point(((150.6667) * transformX) + moveX, ((1091.9999) * transformY) + moveY),
        new createjs.Point(((151.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),
        new createjs.Point(((151.6666) * transformX) + moveX, ((1090.6667) * transformY) + moveY),
        new createjs.Point(((152.3334) * transformX) + moveX, ((1090.3333) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1090.0000) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1089.3334) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1088.6666) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1088.0000) * transformY) + moveY),
        new createjs.Point(((154.6665) * transformX) + moveX, ((1086.6668) * transformY) + moveY),
        new createjs.Point(((156.3335) * transformX) + moveX, ((1085.3332) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1084.0000) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1083.3334) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1082.6666) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1082.0000) * transformY) + moveY),
        new createjs.Point(((159.3332) * transformX) + moveX, ((1081.0001) * transformY) + moveY),
        new createjs.Point(((160.6668) * transformX) + moveX, ((1079.9999) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((1079.0000) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((1078.3334) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((1077.6666) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((1077.0000) * transformY) + moveY),
        new createjs.Point(((163.6665) * transformX) + moveX, ((1075.6668) * transformY) + moveY),
        new createjs.Point(((165.3335) * transformX) + moveX, ((1074.3332) * transformY) + moveY),
        new createjs.Point(((167.0000) * transformX) + moveX, ((1073.0000) * transformY) + moveY),
        new createjs.Point(((180.9692) * transformX) + moveX, ((1053.6471) * transformY) + moveY),
        new createjs.Point(((198.3399) * transformX) + moveX, ((1037.6647) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((1021.0000) * transformY) + moveY),
        new createjs.Point(((223.6658) * transformX) + moveX, ((1012.0009) * transformY) + moveY),
        new createjs.Point(((232.3342) * transformX) + moveX, ((1002.9991) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((241.6666) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((242.3334) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((994.0000) * transformY) + moveY),
        new createjs.Point(((245.9997) * transformX) + moveX, ((990.6670) * transformY) + moveY),
        new createjs.Point(((249.0003) * transformX) + moveX, ((987.3330) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((252.6666) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((253.3334) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((256.6664) * transformX) + moveX, ((981.0003) * transformY) + moveY),
        new createjs.Point(((259.3336) * transformX) + moveX, ((977.9997) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((262.6666) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((263.3334) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((265.9998) * transformX) + moveX, ((972.6669) * transformY) + moveY),
        new createjs.Point(((268.0002) * transformX) + moveX, ((970.3331) * transformY) + moveY),
        new createjs.Point(((270.0000) * transformX) + moveX, ((968.0000) * transformY) + moveY),
        new createjs.Point(((270.6666) * transformX) + moveX, ((968.0000) * transformY) + moveY),
        new createjs.Point(((271.3334) * transformX) + moveX, ((968.0000) * transformY) + moveY),
        new createjs.Point(((272.0000) * transformX) + moveX, ((968.0000) * transformY) + moveY),
        new createjs.Point(((273.6665) * transformX) + moveX, ((966.0002) * transformY) + moveY),
        new createjs.Point(((275.3335) * transformX) + moveX, ((963.9998) * transformY) + moveY),
        new createjs.Point(((277.0000) * transformX) + moveX, ((962.0000) * transformY) + moveY),
        new createjs.Point(((277.6666) * transformX) + moveX, ((962.0000) * transformY) + moveY),
        new createjs.Point(((278.3334) * transformX) + moveX, ((962.0000) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((962.0000) * transformY) + moveY),
        new createjs.Point(((280.3332) * transformX) + moveX, ((960.3335) * transformY) + moveY),
        new createjs.Point(((281.6668) * transformX) + moveX, ((958.6665) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((283.6666) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((284.3334) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((957.0000) * transformY) + moveY),
        new createjs.Point(((286.3332) * transformX) + moveX, ((955.3335) * transformY) + moveY),
        new createjs.Point(((287.6668) * transformX) + moveX, ((953.6665) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((289.6666) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((290.3334) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),
        new createjs.Point(((292.3332) * transformX) + moveX, ((950.3335) * transformY) + moveY),
        new createjs.Point(((293.6668) * transformX) + moveX, ((948.6665) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((947.0000) * transformY) + moveY),
        new createjs.Point(((295.6666) * transformX) + moveX, ((947.0000) * transformY) + moveY),
        new createjs.Point(((296.3334) * transformX) + moveX, ((947.0000) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((947.0000) * transformY) + moveY),
        new createjs.Point(((297.9999) * transformX) + moveX, ((945.6668) * transformY) + moveY),
        new createjs.Point(((299.0001) * transformX) + moveX, ((944.3332) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((943.0000) * transformY) + moveY),
        new createjs.Point(((300.6666) * transformX) + moveX, ((943.0000) * transformY) + moveY),
        new createjs.Point(((301.3334) * transformX) + moveX, ((943.0000) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((943.0000) * transformY) + moveY),
        new createjs.Point(((303.3332) * transformX) + moveX, ((941.3335) * transformY) + moveY),
        new createjs.Point(((304.6668) * transformX) + moveX, ((939.6665) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((306.6666) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((307.3334) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((938.0000) * transformY) + moveY),
        new createjs.Point(((308.3333) * transformX) + moveX, ((937.3334) * transformY) + moveY),
        new createjs.Point(((308.6667) * transformX) + moveX, ((936.6666) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((936.0000) * transformY) + moveY),
        new createjs.Point(((309.3333) * transformX) + moveX, ((936.0000) * transformY) + moveY),
        new createjs.Point(((309.6667) * transformX) + moveX, ((936.0000) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((936.0000) * transformY) + moveY),
        new createjs.Point(((310.3333) * transformX) + moveX, ((935.3334) * transformY) + moveY),
        new createjs.Point(((310.6667) * transformX) + moveX, ((934.6666) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((311.6666) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((312.3334) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((313.3333) * transformX) + moveX, ((933.3334) * transformY) + moveY),
        new createjs.Point(((313.6667) * transformX) + moveX, ((932.6666) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((932.0000) * transformY) + moveY),
        new createjs.Point(((314.9999) * transformX) + moveX, ((931.6667) * transformY) + moveY),
        new createjs.Point(((316.0001) * transformX) + moveX, ((931.3333) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((931.0000) * transformY) + moveY),
        new createjs.Point(((317.9999) * transformX) + moveX, ((929.6668) * transformY) + moveY),
        new createjs.Point(((319.0001) * transformX) + moveX, ((928.3332) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((927.0000) * transformY) + moveY),
        new createjs.Point(((320.6666) * transformX) + moveX, ((927.0000) * transformY) + moveY),
        new createjs.Point(((321.3334) * transformX) + moveX, ((927.0000) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((927.0000) * transformY) + moveY),
        new createjs.Point(((322.9999) * transformX) + moveX, ((925.6668) * transformY) + moveY),
        new createjs.Point(((324.0001) * transformX) + moveX, ((924.3332) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((923.0000) * transformY) + moveY),
        new createjs.Point(((325.6666) * transformX) + moveX, ((923.0000) * transformY) + moveY),
        new createjs.Point(((326.3334) * transformX) + moveX, ((923.0000) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((923.0000) * transformY) + moveY),
        new createjs.Point(((327.6666) * transformX) + moveX, ((922.0001) * transformY) + moveY),
        new createjs.Point(((328.3334) * transformX) + moveX, ((920.9999) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((920.0000) * transformY) + moveY),
        new createjs.Point(((329.6666) * transformX) + moveX, ((920.0000) * transformY) + moveY),
        new createjs.Point(((330.3334) * transformX) + moveX, ((920.0000) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((920.0000) * transformY) + moveY),
        new createjs.Point(((331.6666) * transformX) + moveX, ((919.0001) * transformY) + moveY),
        new createjs.Point(((332.3334) * transformX) + moveX, ((917.9999) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((333.6666) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((334.3334) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),
        new createjs.Point(((335.9999) * transformX) + moveX, ((915.6668) * transformY) + moveY),
        new createjs.Point(((337.0001) * transformX) + moveX, ((914.3332) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((338.6666) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((339.3334) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((340.6666) * transformX) + moveX, ((912.0001) * transformY) + moveY),
        new createjs.Point(((341.3334) * transformX) + moveX, ((910.9999) * transformY) + moveY),
        new createjs.Point(((342.0000) * transformX) + moveX, ((910.0000) * transformY) + moveY),
        new createjs.Point(((342.6666) * transformX) + moveX, ((910.0000) * transformY) + moveY),
        new createjs.Point(((343.3334) * transformX) + moveX, ((910.0000) * transformY) + moveY),
        new createjs.Point(((344.0000) * transformX) + moveX, ((910.0000) * transformY) + moveY),
        new createjs.Point(((344.6666) * transformX) + moveX, ((909.0001) * transformY) + moveY),
        new createjs.Point(((345.3334) * transformX) + moveX, ((907.9999) * transformY) + moveY),
        new createjs.Point(((346.0000) * transformX) + moveX, ((907.0000) * transformY) + moveY),
        new createjs.Point(((346.6666) * transformX) + moveX, ((907.0000) * transformY) + moveY),
        new createjs.Point(((347.3334) * transformX) + moveX, ((907.0000) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((907.0000) * transformY) + moveY),
        new createjs.Point(((348.6666) * transformX) + moveX, ((906.0001) * transformY) + moveY),
        new createjs.Point(((349.3334) * transformX) + moveX, ((904.9999) * transformY) + moveY),
        new createjs.Point(((350.0000) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((350.6666) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((351.3334) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((352.6666) * transformX) + moveX, ((903.0001) * transformY) + moveY),
        new createjs.Point(((353.3334) * transformX) + moveX, ((901.9999) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((901.0000) * transformY) + moveY),
        new createjs.Point(((354.9999) * transformX) + moveX, ((900.6667) * transformY) + moveY),
        new createjs.Point(((356.0001) * transformX) + moveX, ((900.3333) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((900.0000) * transformY) + moveY),
        new createjs.Point(((357.3333) * transformX) + moveX, ((899.3334) * transformY) + moveY),
        new createjs.Point(((357.6667) * transformX) + moveX, ((898.6666) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((898.0000) * transformY) + moveY),
        new createjs.Point(((358.6666) * transformX) + moveX, ((898.0000) * transformY) + moveY),
        new createjs.Point(((359.3334) * transformX) + moveX, ((898.0000) * transformY) + moveY),
        new createjs.Point(((360.0000) * transformX) + moveX, ((898.0000) * transformY) + moveY),
        new createjs.Point(((360.3333) * transformX) + moveX, ((897.3334) * transformY) + moveY),
        new createjs.Point(((360.6667) * transformX) + moveX, ((896.6666) * transformY) + moveY),
        new createjs.Point(((361.0000) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((361.6666) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((362.3334) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((363.0000) * transformX) + moveX, ((896.0000) * transformY) + moveY),
        new createjs.Point(((363.6666) * transformX) + moveX, ((895.0001) * transformY) + moveY),
        new createjs.Point(((364.3334) * transformX) + moveX, ((893.9999) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((893.0000) * transformY) + moveY),
        new createjs.Point(((365.9999) * transformX) + moveX, ((892.6667) * transformY) + moveY),
        new createjs.Point(((367.0001) * transformX) + moveX, ((892.3333) * transformY) + moveY),
        new createjs.Point(((368.0000) * transformX) + moveX, ((892.0000) * transformY) + moveY),
        new createjs.Point(((368.3333) * transformX) + moveX, ((891.3334) * transformY) + moveY),
        new createjs.Point(((368.6667) * transformX) + moveX, ((890.6666) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((890.0000) * transformY) + moveY),
        new createjs.Point(((369.6666) * transformX) + moveX, ((890.0000) * transformY) + moveY),
        new createjs.Point(((370.3334) * transformX) + moveX, ((890.0000) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((890.0000) * transformY) + moveY),
        new createjs.Point(((371.3333) * transformX) + moveX, ((889.3334) * transformY) + moveY),
        new createjs.Point(((371.6667) * transformX) + moveX, ((888.6666) * transformY) + moveY),
        new createjs.Point(((372.0000) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((372.6666) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((373.3334) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((374.6666) * transformX) + moveX, ((887.0001) * transformY) + moveY),
        new createjs.Point(((375.3334) * transformX) + moveX, ((885.9999) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((885.0000) * transformY) + moveY),
        new createjs.Point(((376.6666) * transformX) + moveX, ((885.0000) * transformY) + moveY),
        new createjs.Point(((377.3334) * transformX) + moveX, ((885.0000) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((885.0000) * transformY) + moveY),
        new createjs.Point(((378.6666) * transformX) + moveX, ((884.0001) * transformY) + moveY),
        new createjs.Point(((379.3334) * transformX) + moveX, ((882.9999) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((882.0000) * transformY) + moveY),
        new createjs.Point(((380.6666) * transformX) + moveX, ((882.0000) * transformY) + moveY),
        new createjs.Point(((381.3334) * transformX) + moveX, ((882.0000) * transformY) + moveY),
        new createjs.Point(((382.0000) * transformX) + moveX, ((882.0000) * transformY) + moveY),
        new createjs.Point(((382.3333) * transformX) + moveX, ((881.3334) * transformY) + moveY),
        new createjs.Point(((382.6667) * transformX) + moveX, ((880.6666) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((880.0000) * transformY) + moveY),
        new createjs.Point(((383.6666) * transformX) + moveX, ((880.0000) * transformY) + moveY),
        new createjs.Point(((384.3334) * transformX) + moveX, ((880.0000) * transformY) + moveY),
        new createjs.Point(((385.0000) * transformX) + moveX, ((880.0000) * transformY) + moveY),
        new createjs.Point(((385.6666) * transformX) + moveX, ((879.0001) * transformY) + moveY),
        new createjs.Point(((386.3334) * transformX) + moveX, ((877.9999) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((877.0000) * transformY) + moveY),
        new createjs.Point(((387.6666) * transformX) + moveX, ((877.0000) * transformY) + moveY),
        new createjs.Point(((388.3334) * transformX) + moveX, ((877.0000) * transformY) + moveY),
        new createjs.Point(((389.0000) * transformX) + moveX, ((877.0000) * transformY) + moveY),
        new createjs.Point(((389.3333) * transformX) + moveX, ((876.3334) * transformY) + moveY),
        new createjs.Point(((389.6667) * transformX) + moveX, ((875.6666) * transformY) + moveY),
        new createjs.Point(((390.0000) * transformX) + moveX, ((875.0000) * transformY) + moveY),
        new createjs.Point(((390.9999) * transformX) + moveX, ((874.6667) * transformY) + moveY),
        new createjs.Point(((392.0001) * transformX) + moveX, ((874.3333) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((874.0000) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((873.6667) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((873.3333) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((873.0000) * transformY) + moveY),
        new createjs.Point(((393.6666) * transformX) + moveX, ((873.0000) * transformY) + moveY),
        new createjs.Point(((394.3334) * transformX) + moveX, ((873.0000) * transformY) + moveY),
        new createjs.Point(((395.0000) * transformX) + moveX, ((873.0000) * transformY) + moveY),
        new createjs.Point(((395.6666) * transformX) + moveX, ((872.0001) * transformY) + moveY),
        new createjs.Point(((396.3334) * transformX) + moveX, ((870.9999) * transformY) + moveY),
        new createjs.Point(((397.0000) * transformX) + moveX, ((870.0000) * transformY) + moveY),
        new createjs.Point(((397.6666) * transformX) + moveX, ((870.0000) * transformY) + moveY),
        new createjs.Point(((398.3334) * transformX) + moveX, ((870.0000) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((870.0000) * transformY) + moveY),
        new createjs.Point(((399.3333) * transformX) + moveX, ((869.3334) * transformY) + moveY),
        new createjs.Point(((399.6667) * transformX) + moveX, ((868.6666) * transformY) + moveY),
        new createjs.Point(((400.0000) * transformX) + moveX, ((868.0000) * transformY) + moveY),
        new createjs.Point(((400.9999) * transformX) + moveX, ((867.6667) * transformY) + moveY),
        new createjs.Point(((402.0001) * transformX) + moveX, ((867.3333) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((867.0000) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((866.6667) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((866.3333) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((866.0000) * transformY) + moveY),
        new createjs.Point(((403.6666) * transformX) + moveX, ((866.0000) * transformY) + moveY),
        new createjs.Point(((404.3334) * transformX) + moveX, ((866.0000) * transformY) + moveY),
        new createjs.Point(((405.0000) * transformX) + moveX, ((866.0000) * transformY) + moveY),
        new createjs.Point(((405.6666) * transformX) + moveX, ((865.0001) * transformY) + moveY),
        new createjs.Point(((406.3334) * transformX) + moveX, ((863.9999) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((863.0000) * transformY) + moveY),
        new createjs.Point(((407.6666) * transformX) + moveX, ((863.0000) * transformY) + moveY),
        new createjs.Point(((408.3334) * transformX) + moveX, ((863.0000) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((863.0000) * transformY) + moveY),
        new createjs.Point(((409.3333) * transformX) + moveX, ((862.3334) * transformY) + moveY),
        new createjs.Point(((409.6667) * transformX) + moveX, ((861.6666) * transformY) + moveY),
        new createjs.Point(((410.0000) * transformX) + moveX, ((861.0000) * transformY) + moveY),
        new createjs.Point(((410.9999) * transformX) + moveX, ((860.6667) * transformY) + moveY),
        new createjs.Point(((412.0001) * transformX) + moveX, ((860.3333) * transformY) + moveY),
        new createjs.Point(((413.0000) * transformX) + moveX, ((860.0000) * transformY) + moveY),
        new createjs.Point(((413.0000) * transformX) + moveX, ((859.6667) * transformY) + moveY),
        new createjs.Point(((413.0000) * transformX) + moveX, ((859.3333) * transformY) + moveY),
        new createjs.Point(((413.0000) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((413.6666) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((414.3334) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((415.0000) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((415.3333) * transformX) + moveX, ((858.3334) * transformY) + moveY),
        new createjs.Point(((415.6667) * transformX) + moveX, ((857.6666) * transformY) + moveY),
        new createjs.Point(((416.0000) * transformX) + moveX, ((857.0000) * transformY) + moveY),
        new createjs.Point(((416.6666) * transformX) + moveX, ((857.0000) * transformY) + moveY),
        new createjs.Point(((417.3334) * transformX) + moveX, ((857.0000) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((857.0000) * transformY) + moveY),
        new createjs.Point(((418.6666) * transformX) + moveX, ((856.0001) * transformY) + moveY),
        new createjs.Point(((419.3334) * transformX) + moveX, ((854.9999) * transformY) + moveY),

        new createjs.Point(((691.6666) * transformX) + moveX, ((770.0000) * transformY) + moveY),
        new createjs.Point(((692.3334) * transformX) + moveX, ((770.0000) * transformY) + moveY),
        new createjs.Point(((693.0000) * transformX) + moveX, ((770.0000) * transformY) + moveY),

        new createjs.Point(((693.3333) * transformX) + moveX, ((769.3334) * transformY) + moveY),
        new createjs.Point(((693.6667) * transformX) + moveX, ((768.6666) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((768.0000) * transformY) + moveY),

        new createjs.Point(((694.6666) * transformX) + moveX, ((768.0000) * transformY) + moveY),
        new createjs.Point(((695.3334) * transformX) + moveX, ((768.0000) * transformY) + moveY),
        new createjs.Point(((696.0000) * transformX) + moveX, ((768.0000) * transformY) + moveY),

        new createjs.Point(((696.3333) * transformX) + moveX, ((767.3334) * transformY) + moveY),
        new createjs.Point(((696.6667) * transformX) + moveX, ((766.6666) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((766.0000) * transformY) + moveY),

        new createjs.Point(((697.6666) * transformX) + moveX, ((766.0000) * transformY) + moveY),
        new createjs.Point(((698.3334) * transformX) + moveX, ((766.0000) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((766.0000) * transformY) + moveY),

        new createjs.Point(((699.3333) * transformX) + moveX, ((765.3334) * transformY) + moveY),
        new createjs.Point(((699.6667) * transformX) + moveX, ((764.6666) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((764.0000) * transformY) + moveY),

        new createjs.Point(((700.6666) * transformX) + moveX, ((764.0000) * transformY) + moveY),
        new createjs.Point(((701.3334) * transformX) + moveX, ((764.0000) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((764.0000) * transformY) + moveY),

        new createjs.Point(((702.3333) * transformX) + moveX, ((763.3334) * transformY) + moveY),
        new createjs.Point(((702.6667) * transformX) + moveX, ((762.6666) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((762.0000) * transformY) + moveY),

        new createjs.Point(((703.6666) * transformX) + moveX, ((762.0000) * transformY) + moveY),
        new createjs.Point(((704.3334) * transformX) + moveX, ((762.0000) * transformY) + moveY),
        new createjs.Point(((705.0000) * transformX) + moveX, ((762.0000) * transformY) + moveY),

        new createjs.Point(((705.6666) * transformX) + moveX, ((761.0001) * transformY) + moveY),
        new createjs.Point(((706.3334) * transformX) + moveX, ((759.9999) * transformY) + moveY),
        new createjs.Point(((707.0000) * transformX) + moveX, ((759.0000) * transformY) + moveY),

        new createjs.Point(((707.6666) * transformX) + moveX, ((759.0000) * transformY) + moveY),
        new createjs.Point(((708.3334) * transformX) + moveX, ((759.0000) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((759.0000) * transformY) + moveY),

        new createjs.Point(((709.0000) * transformX) + moveX, ((758.6667) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((758.3333) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((758.0000) * transformY) + moveY),

        new createjs.Point(((709.9999) * transformX) + moveX, ((757.6667) * transformY) + moveY),
        new createjs.Point(((711.0001) * transformX) + moveX, ((757.3333) * transformY) + moveY),
        new createjs.Point(((712.0000) * transformX) + moveX, ((757.0000) * transformY) + moveY),

        new createjs.Point(((712.3333) * transformX) + moveX, ((756.3334) * transformY) + moveY),
        new createjs.Point(((712.6667) * transformX) + moveX, ((755.6666) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((755.0000) * transformY) + moveY),

        new createjs.Point(((713.6666) * transformX) + moveX, ((755.0000) * transformY) + moveY),
        new createjs.Point(((714.3334) * transformX) + moveX, ((755.0000) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((755.0000) * transformY) + moveY),

        new createjs.Point(((715.6666) * transformX) + moveX, ((754.0001) * transformY) + moveY),
        new createjs.Point(((716.3334) * transformX) + moveX, ((752.9999) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((752.0000) * transformY) + moveY),

        new createjs.Point(((717.6666) * transformX) + moveX, ((752.0000) * transformY) + moveY),
        new createjs.Point(((718.3334) * transformX) + moveX, ((752.0000) * transformY) + moveY),
        new createjs.Point(((719.0000) * transformX) + moveX, ((752.0000) * transformY) + moveY),

        new createjs.Point(((719.3333) * transformX) + moveX, ((751.3334) * transformY) + moveY),
        new createjs.Point(((719.6667) * transformX) + moveX, ((750.6666) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((750.0000) * transformY) + moveY),

        new createjs.Point(((720.6666) * transformX) + moveX, ((750.0000) * transformY) + moveY),
        new createjs.Point(((721.3334) * transformX) + moveX, ((750.0000) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((750.0000) * transformY) + moveY),

        new createjs.Point(((722.6666) * transformX) + moveX, ((749.0001) * transformY) + moveY),
        new createjs.Point(((723.3334) * transformX) + moveX, ((747.9999) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((747.0000) * transformY) + moveY),

        new createjs.Point(((724.6666) * transformX) + moveX, ((747.0000) * transformY) + moveY),
        new createjs.Point(((725.3334) * transformX) + moveX, ((747.0000) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((747.0000) * transformY) + moveY),

        new createjs.Point(((726.3333) * transformX) + moveX, ((746.3334) * transformY) + moveY),
        new createjs.Point(((726.6667) * transformX) + moveX, ((745.6666) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((745.0000) * transformY) + moveY),

        new createjs.Point(((727.6666) * transformX) + moveX, ((745.0000) * transformY) + moveY),
        new createjs.Point(((728.3334) * transformX) + moveX, ((745.0000) * transformY) + moveY),
        new createjs.Point(((729.0000) * transformX) + moveX, ((745.0000) * transformY) + moveY),

        new createjs.Point(((729.6666) * transformX) + moveX, ((744.0001) * transformY) + moveY),
        new createjs.Point(((730.3334) * transformX) + moveX, ((742.9999) * transformY) + moveY),
        new createjs.Point(((731.0000) * transformX) + moveX, ((742.0000) * transformY) + moveY),

        new createjs.Point(((731.6666) * transformX) + moveX, ((742.0000) * transformY) + moveY),
        new createjs.Point(((732.3334) * transformX) + moveX, ((742.0000) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((742.0000) * transformY) + moveY),

        new createjs.Point(((733.3333) * transformX) + moveX, ((741.3334) * transformY) + moveY),
        new createjs.Point(((733.6667) * transformX) + moveX, ((740.6666) * transformY) + moveY),
        new createjs.Point(((734.0000) * transformX) + moveX, ((740.0000) * transformY) + moveY),

        new createjs.Point(((734.6666) * transformX) + moveX, ((740.0000) * transformY) + moveY),
        new createjs.Point(((735.3334) * transformX) + moveX, ((740.0000) * transformY) + moveY),
        new createjs.Point(((736.0000) * transformX) + moveX, ((740.0000) * transformY) + moveY),

        new createjs.Point(((736.3333) * transformX) + moveX, ((739.3334) * transformY) + moveY),
        new createjs.Point(((736.6667) * transformX) + moveX, ((738.6666) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((738.0000) * transformY) + moveY),

        new createjs.Point(((737.9999) * transformX) + moveX, ((737.6667) * transformY) + moveY),
        new createjs.Point(((739.0001) * transformX) + moveX, ((737.3333) * transformY) + moveY),
        new createjs.Point(((740.0000) * transformX) + moveX, ((737.0000) * transformY) + moveY),

        new createjs.Point(((740.6666) * transformX) + moveX, ((736.0001) * transformY) + moveY),
        new createjs.Point(((741.3334) * transformX) + moveX, ((734.9999) * transformY) + moveY),
        new createjs.Point(((742.0000) * transformX) + moveX, ((734.0000) * transformY) + moveY),

        new createjs.Point(((742.6666) * transformX) + moveX, ((734.0000) * transformY) + moveY),
        new createjs.Point(((743.3334) * transformX) + moveX, ((734.0000) * transformY) + moveY),
        new createjs.Point(((744.0000) * transformX) + moveX, ((734.0000) * transformY) + moveY),

        new createjs.Point(((744.3333) * transformX) + moveX, ((733.3334) * transformY) + moveY),
        new createjs.Point(((744.6667) * transformX) + moveX, ((732.6666) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((732.0000) * transformY) + moveY),

        new createjs.Point(((745.6666) * transformX) + moveX, ((732.0000) * transformY) + moveY),
        new createjs.Point(((746.3334) * transformX) + moveX, ((732.0000) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((732.0000) * transformY) + moveY),

        new createjs.Point(((747.3333) * transformX) + moveX, ((731.3334) * transformY) + moveY),
        new createjs.Point(((747.6667) * transformX) + moveX, ((730.6666) * transformY) + moveY),
        new createjs.Point(((748.0000) * transformX) + moveX, ((730.0000) * transformY) + moveY),

        new createjs.Point(((748.9999) * transformX) + moveX, ((729.6667) * transformY) + moveY),
        new createjs.Point(((750.0001) * transformX) + moveX, ((729.3333) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((729.0000) * transformY) + moveY),

        new createjs.Point(((751.6666) * transformX) + moveX, ((728.0001) * transformY) + moveY),
        new createjs.Point(((752.3334) * transformX) + moveX, ((726.9999) * transformY) + moveY),
        new createjs.Point(((753.0000) * transformX) + moveX, ((726.0000) * transformY) + moveY),

        new createjs.Point(((753.6666) * transformX) + moveX, ((726.0000) * transformY) + moveY),
        new createjs.Point(((754.3334) * transformX) + moveX, ((726.0000) * transformY) + moveY),
        new createjs.Point(((755.0000) * transformX) + moveX, ((726.0000) * transformY) + moveY),

        new createjs.Point(((755.6666) * transformX) + moveX, ((725.0001) * transformY) + moveY),
        new createjs.Point(((756.3334) * transformX) + moveX, ((723.9999) * transformY) + moveY),
        new createjs.Point(((757.0000) * transformX) + moveX, ((723.0000) * transformY) + moveY),

        new createjs.Point(((757.6666) * transformX) + moveX, ((723.0000) * transformY) + moveY),
        new createjs.Point(((758.3334) * transformX) + moveX, ((723.0000) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((723.0000) * transformY) + moveY),

        new createjs.Point(((759.6666) * transformX) + moveX, ((722.0001) * transformY) + moveY),
        new createjs.Point(((760.3334) * transformX) + moveX, ((720.9999) * transformY) + moveY),
        new createjs.Point(((761.0000) * transformX) + moveX, ((720.0000) * transformY) + moveY),

        new createjs.Point(((761.6666) * transformX) + moveX, ((720.0000) * transformY) + moveY),
        new createjs.Point(((762.3334) * transformX) + moveX, ((720.0000) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((720.0000) * transformY) + moveY),

        new createjs.Point(((763.9999) * transformX) + moveX, ((718.6668) * transformY) + moveY),
        new createjs.Point(((765.0001) * transformX) + moveX, ((717.3332) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((716.0000) * transformY) + moveY),

        new createjs.Point(((766.9999) * transformX) + moveX, ((715.6667) * transformY) + moveY),
        new createjs.Point(((768.0001) * transformX) + moveX, ((715.3333) * transformY) + moveY),
        new createjs.Point(((769.0000) * transformX) + moveX, ((715.0000) * transformY) + moveY),

        new createjs.Point(((769.3333) * transformX) + moveX, ((714.3334) * transformY) + moveY),
        new createjs.Point(((769.6667) * transformX) + moveX, ((713.6666) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((713.0000) * transformY) + moveY),

        new createjs.Point(((770.6666) * transformX) + moveX, ((713.0000) * transformY) + moveY),
        new createjs.Point(((771.3334) * transformX) + moveX, ((713.0000) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((713.0000) * transformY) + moveY),

        new createjs.Point(((772.9999) * transformX) + moveX, ((711.6668) * transformY) + moveY),
        new createjs.Point(((774.0001) * transformX) + moveX, ((710.3332) * transformY) + moveY),
        new createjs.Point(((775.0000) * transformX) + moveX, ((709.0000) * transformY) + moveY),

        new createjs.Point(((775.6666) * transformX) + moveX, ((709.0000) * transformY) + moveY),
        new createjs.Point(((776.3334) * transformX) + moveX, ((709.0000) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((709.0000) * transformY) + moveY),

        new createjs.Point(((777.3333) * transformX) + moveX, ((708.3334) * transformY) + moveY),
        new createjs.Point(((777.6667) * transformX) + moveX, ((707.6666) * transformY) + moveY),
        new createjs.Point(((778.0000) * transformX) + moveX, ((707.0000) * transformY) + moveY),

        new createjs.Point(((778.3333) * transformX) + moveX, ((707.0000) * transformY) + moveY),
        new createjs.Point(((778.6667) * transformX) + moveX, ((707.0000) * transformY) + moveY),
        new createjs.Point(((779.0000) * transformX) + moveX, ((707.0000) * transformY) + moveY),

        new createjs.Point(((779.3333) * transformX) + moveX, ((706.3334) * transformY) + moveY),
        new createjs.Point(((779.6667) * transformX) + moveX, ((705.6666) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((705.0000) * transformY) + moveY),

        new createjs.Point(((780.6666) * transformX) + moveX, ((705.0000) * transformY) + moveY),
        new createjs.Point(((781.3334) * transformX) + moveX, ((705.0000) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((705.0000) * transformY) + moveY),

        new createjs.Point(((782.9999) * transformX) + moveX, ((703.6668) * transformY) + moveY),
        new createjs.Point(((784.0001) * transformX) + moveX, ((702.3332) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((701.0000) * transformY) + moveY),

        new createjs.Point(((785.6666) * transformX) + moveX, ((701.0000) * transformY) + moveY),
        new createjs.Point(((786.3334) * transformX) + moveX, ((701.0000) * transformY) + moveY),
        new createjs.Point(((787.0000) * transformX) + moveX, ((701.0000) * transformY) + moveY),

        new createjs.Point(((787.3333) * transformX) + moveX, ((700.3334) * transformY) + moveY),
        new createjs.Point(((787.6667) * transformX) + moveX, ((699.6666) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((699.0000) * transformY) + moveY),

        new createjs.Point(((788.3333) * transformX) + moveX, ((699.0000) * transformY) + moveY),
        new createjs.Point(((788.6667) * transformX) + moveX, ((699.0000) * transformY) + moveY),
        new createjs.Point(((789.0000) * transformX) + moveX, ((699.0000) * transformY) + moveY),

        new createjs.Point(((789.3333) * transformX) + moveX, ((698.3334) * transformY) + moveY),
        new createjs.Point(((789.6667) * transformX) + moveX, ((697.6666) * transformY) + moveY),
        new createjs.Point(((790.0000) * transformX) + moveX, ((697.0000) * transformY) + moveY),

        new createjs.Point(((790.6666) * transformX) + moveX, ((697.0000) * transformY) + moveY),
        new createjs.Point(((791.3334) * transformX) + moveX, ((697.0000) * transformY) + moveY),
        new createjs.Point(((792.0000) * transformX) + moveX, ((697.0000) * transformY) + moveY),

        new createjs.Point(((792.9999) * transformX) + moveX, ((695.6668) * transformY) + moveY),
        new createjs.Point(((794.0001) * transformX) + moveX, ((694.3332) * transformY) + moveY),
        new createjs.Point(((795.0000) * transformX) + moveX, ((693.0000) * transformY) + moveY),

        new createjs.Point(((795.6666) * transformX) + moveX, ((693.0000) * transformY) + moveY),
        new createjs.Point(((796.3334) * transformX) + moveX, ((693.0000) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((693.0000) * transformY) + moveY),

        new createjs.Point(((797.6666) * transformX) + moveX, ((692.0001) * transformY) + moveY),
        new createjs.Point(((798.3334) * transformX) + moveX, ((690.9999) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((690.0000) * transformY) + moveY),

        new createjs.Point(((799.3333) * transformX) + moveX, ((690.0000) * transformY) + moveY),
        new createjs.Point(((799.6667) * transformX) + moveX, ((690.0000) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((690.0000) * transformY) + moveY),

        new createjs.Point(((800.3333) * transformX) + moveX, ((689.3334) * transformY) + moveY),
        new createjs.Point(((800.6667) * transformX) + moveX, ((688.6666) * transformY) + moveY),
        new createjs.Point(((801.0000) * transformX) + moveX, ((688.0000) * transformY) + moveY),

        new createjs.Point(((801.6666) * transformX) + moveX, ((688.0000) * transformY) + moveY),
        new createjs.Point(((802.3334) * transformX) + moveX, ((688.0000) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((688.0000) * transformY) + moveY),

        new createjs.Point(((804.6665) * transformX) + moveX, ((686.0002) * transformY) + moveY),
        new createjs.Point(((806.3335) * transformX) + moveX, ((683.9998) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((682.0000) * transformY) + moveY),

        new createjs.Point(((808.6666) * transformX) + moveX, ((682.0000) * transformY) + moveY),
        new createjs.Point(((809.3334) * transformX) + moveX, ((682.0000) * transformY) + moveY),
        new createjs.Point(((810.0000) * transformX) + moveX, ((682.0000) * transformY) + moveY),

        new createjs.Point(((811.6665) * transformX) + moveX, ((680.0002) * transformY) + moveY),
        new createjs.Point(((813.3335) * transformX) + moveX, ((677.9998) * transformY) + moveY),
        new createjs.Point(((815.0000) * transformX) + moveX, ((676.0000) * transformY) + moveY),

        new createjs.Point(((815.6666) * transformX) + moveX, ((676.0000) * transformY) + moveY),
        new createjs.Point(((816.3334) * transformX) + moveX, ((676.0000) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((676.0000) * transformY) + moveY),

        new createjs.Point(((819.3331) * transformX) + moveX, ((673.3336) * transformY) + moveY),
        new createjs.Point(((821.6669) * transformX) + moveX, ((670.6664) * transformY) + moveY),
        new createjs.Point(((824.0000) * transformX) + moveX, ((668.0000) * transformY) + moveY),

        new createjs.Point(((824.6666) * transformX) + moveX, ((668.0000) * transformY) + moveY),
        new createjs.Point(((825.3334) * transformX) + moveX, ((668.0000) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((668.0000) * transformY) + moveY),

        new createjs.Point(((829.3330) * transformX) + moveX, ((664.3337) * transformY) + moveY),
        new createjs.Point(((832.6670) * transformX) + moveX, ((660.6663) * transformY) + moveY),
        new createjs.Point(((836.0000) * transformX) + moveX, ((657.0000) * transformY) + moveY),

        new createjs.Point(((836.6666) * transformX) + moveX, ((657.0000) * transformY) + moveY),
        new createjs.Point(((837.3334) * transformX) + moveX, ((657.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((657.0000) * transformY) + moveY),

        new createjs.Point(((841.6663) * transformX) + moveX, ((653.0004) * transformY) + moveY),
        new createjs.Point(((845.3337) * transformX) + moveX, ((648.9996) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((645.0000) * transformY) + moveY),

        new createjs.Point(((856.9323) * transformX) + moveX, ((637.0650) * transformY) + moveY),
        new createjs.Point(((865.3129) * transformX) + moveX, ((630.2473) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((621.0000) * transformY) + moveY),

        new createjs.Point(((875.3330) * transformX) + moveX, ((618.0003) * transformY) + moveY),
        new createjs.Point(((878.6670) * transformX) + moveX, ((614.9997) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((612.0000) * transformY) + moveY),

        new createjs.Point(((882.0000) * transformX) + moveX, ((611.3334) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((610.6666) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((610.0000) * transformY) + moveY),

        new createjs.Point(((884.3331) * transformX) + moveX, ((608.0002) * transformY) + moveY),
        new createjs.Point(((886.6669) * transformX) + moveX, ((605.9998) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((604.0000) * transformY) + moveY),

        new createjs.Point(((889.0000) * transformX) + moveX, ((603.3334) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((602.6666) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((602.0000) * transformY) + moveY),

        new createjs.Point(((890.9998) * transformX) + moveX, ((600.3335) * transformY) + moveY),
        new createjs.Point(((893.0002) * transformX) + moveX, ((598.6665) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((597.0000) * transformY) + moveY),

        new createjs.Point(((895.0000) * transformX) + moveX, ((596.3334) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((595.6666) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((595.0000) * transformY) + moveY),

        new createjs.Point(((896.3332) * transformX) + moveX, ((594.0001) * transformY) + moveY),
        new createjs.Point(((897.6668) * transformX) + moveX, ((592.9999) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((592.0000) * transformY) + moveY),

        new createjs.Point(((899.0000) * transformX) + moveX, ((591.3334) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((590.6666) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((590.0000) * transformY) + moveY),

        new createjs.Point(((900.3332) * transformX) + moveX, ((589.0001) * transformY) + moveY),
        new createjs.Point(((901.6668) * transformX) + moveX, ((587.9999) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((587.0000) * transformY) + moveY),

        new createjs.Point(((903.0000) * transformX) + moveX, ((586.3334) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((585.6666) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((585.0000) * transformY) + moveY),

        new createjs.Point(((904.3332) * transformX) + moveX, ((584.0001) * transformY) + moveY),
        new createjs.Point(((905.6668) * transformX) + moveX, ((582.9999) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((582.0000) * transformY) + moveY),

        new createjs.Point(((907.0000) * transformX) + moveX, ((581.3334) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((580.6666) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((580.0000) * transformY) + moveY),

        new createjs.Point(((907.9999) * transformX) + moveX, ((579.3334) * transformY) + moveY),
        new createjs.Point(((909.0001) * transformX) + moveX, ((578.6666) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((578.0000) * transformY) + moveY),

        new createjs.Point(((910.3333) * transformX) + moveX, ((577.0001) * transformY) + moveY),
        new createjs.Point(((910.6667) * transformX) + moveX, ((575.9999) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((575.0000) * transformY) + moveY),

        new createjs.Point(((911.6666) * transformX) + moveX, ((574.6667) * transformY) + moveY),
        new createjs.Point(((912.3334) * transformX) + moveX, ((574.3333) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((574.0000) * transformY) + moveY),

        new createjs.Point(((913.0000) * transformX) + moveX, ((573.3334) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((572.6666) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((572.0000) * transformY) + moveY),

        new createjs.Point(((913.9999) * transformX) + moveX, ((571.3334) * transformY) + moveY),
        new createjs.Point(((915.0001) * transformX) + moveX, ((570.6666) * transformY) + moveY),
        new createjs.Point(((916.0000) * transformX) + moveX, ((570.0000) * transformY) + moveY),

        new createjs.Point(((916.0000) * transformX) + moveX, ((569.3334) * transformY) + moveY),
        new createjs.Point(((916.0000) * transformX) + moveX, ((568.6666) * transformY) + moveY),
        new createjs.Point(((916.0000) * transformX) + moveX, ((568.0000) * transformY) + moveY),

        new createjs.Point(((916.6666) * transformX) + moveX, ((567.6667) * transformY) + moveY),
        new createjs.Point(((917.3334) * transformX) + moveX, ((567.3333) * transformY) + moveY),
        new createjs.Point(((918.0000) * transformX) + moveX, ((567.0000) * transformY) + moveY),

        new createjs.Point(((918.0000) * transformX) + moveX, ((566.3334) * transformY) + moveY),
        new createjs.Point(((918.0000) * transformX) + moveX, ((565.6666) * transformY) + moveY),
        new createjs.Point(((918.0000) * transformX) + moveX, ((565.0000) * transformY) + moveY),

        new createjs.Point(((918.9999) * transformX) + moveX, ((564.3334) * transformY) + moveY),
        new createjs.Point(((920.0001) * transformX) + moveX, ((563.6666) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((563.0000) * transformY) + moveY),

        new createjs.Point(((921.0000) * transformX) + moveX, ((562.3334) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((561.6666) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((561.0000) * transformY) + moveY),

        new createjs.Point(((921.6666) * transformX) + moveX, ((560.6667) * transformY) + moveY),
        new createjs.Point(((922.3334) * transformX) + moveX, ((560.3333) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((560.0000) * transformY) + moveY),

        new createjs.Point(((923.3333) * transformX) + moveX, ((559.0001) * transformY) + moveY),
        new createjs.Point(((923.6667) * transformX) + moveX, ((557.9999) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((557.0000) * transformY) + moveY),

        new createjs.Point(((924.3333) * transformX) + moveX, ((557.0000) * transformY) + moveY),
        new createjs.Point(((924.6667) * transformX) + moveX, ((557.0000) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((557.0000) * transformY) + moveY),

        new createjs.Point(((925.0000) * transformX) + moveX, ((556.3334) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((555.6666) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((555.0000) * transformY) + moveY),

        new createjs.Point(((925.6666) * transformX) + moveX, ((554.6667) * transformY) + moveY),
        new createjs.Point(((926.3334) * transformX) + moveX, ((554.3333) * transformY) + moveY),
        new createjs.Point(((927.0000) * transformX) + moveX, ((554.0000) * transformY) + moveY),

        new createjs.Point(((927.0000) * transformX) + moveX, ((553.3334) * transformY) + moveY),
        new createjs.Point(((927.0000) * transformX) + moveX, ((552.6666) * transformY) + moveY),
        new createjs.Point(((927.0000) * transformX) + moveX, ((552.0000) * transformY) + moveY),

        new createjs.Point(((927.6666) * transformX) + moveX, ((551.6667) * transformY) + moveY),
        new createjs.Point(((928.3334) * transformX) + moveX, ((551.3333) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((551.0000) * transformY) + moveY),

        new createjs.Point(((929.0000) * transformX) + moveX, ((550.3334) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((549.6666) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((549.0000) * transformY) + moveY),

        new createjs.Point(((929.6666) * transformX) + moveX, ((548.6667) * transformY) + moveY),
        new createjs.Point(((930.3334) * transformX) + moveX, ((548.3333) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((548.0000) * transformY) + moveY),

        new createjs.Point(((931.0000) * transformX) + moveX, ((547.3334) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((546.6666) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((546.0000) * transformY) + moveY),

        new createjs.Point(((931.6666) * transformX) + moveX, ((545.6667) * transformY) + moveY),
        new createjs.Point(((932.3334) * transformX) + moveX, ((545.3333) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((545.0000) * transformY) + moveY),

        new createjs.Point(((933.0000) * transformX) + moveX, ((544.3334) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((543.6666) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((543.0000) * transformY) + moveY),

        new createjs.Point(((933.6666) * transformX) + moveX, ((542.6667) * transformY) + moveY),
        new createjs.Point(((934.3334) * transformX) + moveX, ((542.3333) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((542.0000) * transformY) + moveY),

        new createjs.Point(((935.0000) * transformX) + moveX, ((541.3334) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((540.6666) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((540.0000) * transformY) + moveY),

        new createjs.Point(((935.3333) * transformX) + moveX, ((540.0000) * transformY) + moveY),
        new createjs.Point(((935.6667) * transformX) + moveX, ((540.0000) * transformY) + moveY),
        new createjs.Point(((936.0000) * transformX) + moveX, ((540.0000) * transformY) + moveY),

        new createjs.Point(((936.0000) * transformX) + moveX, ((539.3334) * transformY) + moveY),
        new createjs.Point(((936.0000) * transformX) + moveX, ((538.6666) * transformY) + moveY),
        new createjs.Point(((936.0000) * transformX) + moveX, ((538.0000) * transformY) + moveY),

        new createjs.Point(((936.6666) * transformX) + moveX, ((537.6667) * transformY) + moveY),
        new createjs.Point(((937.3334) * transformX) + moveX, ((537.3333) * transformY) + moveY),
        new createjs.Point(((938.0000) * transformX) + moveX, ((537.0000) * transformY) + moveY),

        new createjs.Point(((938.3333) * transformX) + moveX, ((535.6668) * transformY) + moveY),
        new createjs.Point(((938.6667) * transformX) + moveX, ((534.3332) * transformY) + moveY),
        new createjs.Point(((939.0000) * transformX) + moveX, ((533.0000) * transformY) + moveY),

        new createjs.Point(((939.6666) * transformX) + moveX, ((532.6667) * transformY) + moveY),
        new createjs.Point(((940.3334) * transformX) + moveX, ((532.3333) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((532.0000) * transformY) + moveY),

        new createjs.Point(((941.0000) * transformX) + moveX, ((531.3334) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((530.6666) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((530.0000) * transformY) + moveY),

        new createjs.Point(((941.3333) * transformX) + moveX, ((530.0000) * transformY) + moveY),
        new createjs.Point(((941.6667) * transformX) + moveX, ((530.0000) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((530.0000) * transformY) + moveY),

        new createjs.Point(((942.0000) * transformX) + moveX, ((529.3334) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((528.6666) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((528.0000) * transformY) + moveY),

        new createjs.Point(((942.6666) * transformX) + moveX, ((527.6667) * transformY) + moveY),
        new createjs.Point(((943.3334) * transformX) + moveX, ((527.3333) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((527.0000) * transformY) + moveY),

        new createjs.Point(((944.0000) * transformX) + moveX, ((526.3334) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((525.6666) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((525.0000) * transformY) + moveY),

        new createjs.Point(((944.3333) * transformX) + moveX, ((525.0000) * transformY) + moveY),
        new createjs.Point(((944.6667) * transformX) + moveX, ((525.0000) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((525.0000) * transformY) + moveY),

        new createjs.Point(((945.0000) * transformX) + moveX, ((524.3334) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((523.6666) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((523.0000) * transformY) + moveY),

        new createjs.Point(((945.3333) * transformX) + moveX, ((523.0000) * transformY) + moveY),
        new createjs.Point(((945.6667) * transformX) + moveX, ((523.0000) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((523.0000) * transformY) + moveY),

        new createjs.Point(((946.0000) * transformX) + moveX, ((522.3334) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((521.6666) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((521.0000) * transformY) + moveY),

        new createjs.Point(((946.3333) * transformX) + moveX, ((521.0000) * transformY) + moveY),
        new createjs.Point(((946.6667) * transformX) + moveX, ((521.0000) * transformY) + moveY),
        new createjs.Point(((947.0000) * transformX) + moveX, ((521.0000) * transformY) + moveY),

        new createjs.Point(((947.0000) * transformX) + moveX, ((520.3334) * transformY) + moveY),
        new createjs.Point(((947.0000) * transformX) + moveX, ((519.6666) * transformY) + moveY),
        new createjs.Point(((947.0000) * transformX) + moveX, ((519.0000) * transformY) + moveY),

        new createjs.Point(((947.6666) * transformX) + moveX, ((518.6667) * transformY) + moveY),
        new createjs.Point(((948.3334) * transformX) + moveX, ((518.3333) * transformY) + moveY),
        new createjs.Point(((949.0000) * transformX) + moveX, ((518.0000) * transformY) + moveY),

        new createjs.Point(((949.0000) * transformX) + moveX, ((517.3334) * transformY) + moveY),
        new createjs.Point(((949.0000) * transformX) + moveX, ((516.6666) * transformY) + moveY),
        new createjs.Point(((949.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((949.3333) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((949.6667) * transformX) + moveX, ((516.0000) * transformY) + moveY),
        new createjs.Point(((950.0000) * transformX) + moveX, ((516.0000) * transformY) + moveY),

        new createjs.Point(((950.0000) * transformX) + moveX, ((515.3334) * transformY) + moveY),
        new createjs.Point(((950.0000) * transformX) + moveX, ((514.6666) * transformY) + moveY),
        new createjs.Point(((950.0000) * transformX) + moveX, ((514.0000) * transformY) + moveY),

        new createjs.Point(((950.3333) * transformX) + moveX, ((514.0000) * transformY) + moveY),
        new createjs.Point(((950.6667) * transformX) + moveX, ((514.0000) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((514.0000) * transformY) + moveY),

        new createjs.Point(((951.0000) * transformX) + moveX, ((513.3334) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((512.6666) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((512.0000) * transformY) + moveY),

        new createjs.Point(((951.3333) * transformX) + moveX, ((512.0000) * transformY) + moveY),
        new createjs.Point(((951.6667) * transformX) + moveX, ((512.0000) * transformY) + moveY),
        new createjs.Point(((952.0000) * transformX) + moveX, ((512.0000) * transformY) + moveY),

        new createjs.Point(((952.0000) * transformX) + moveX, ((511.3334) * transformY) + moveY),
        new createjs.Point(((952.0000) * transformX) + moveX, ((510.6666) * transformY) + moveY),
        new createjs.Point(((952.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((952.3333) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((952.6667) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((953.0000) * transformX) + moveX, ((509.3334) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((508.6666) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((953.3333) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((953.6667) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((954.0000) * transformX) + moveX, ((507.3334) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((506.6666) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((954.3333) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((954.6667) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((955.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((955.0000) * transformX) + moveX, ((505.3334) * transformY) + moveY),
        new createjs.Point(((955.0000) * transformX) + moveX, ((504.6666) * transformY) + moveY),
        new createjs.Point(((955.0000) * transformX) + moveX, ((504.0000) * transformY) + moveY),

        new createjs.Point(((955.3333) * transformX) + moveX, ((504.0000) * transformY) + moveY),
        new createjs.Point(((955.6667) * transformX) + moveX, ((504.0000) * transformY) + moveY),
        new createjs.Point(((956.0000) * transformX) + moveX, ((504.0000) * transformY) + moveY),

        new createjs.Point(((956.0000) * transformX) + moveX, ((503.3334) * transformY) + moveY),
        new createjs.Point(((956.0000) * transformX) + moveX, ((502.6666) * transformY) + moveY),
        new createjs.Point(((956.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),

        new createjs.Point(((956.3333) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((956.6667) * transformX) + moveX, ((502.0000) * transformY) + moveY),
        new createjs.Point(((957.0000) * transformX) + moveX, ((502.0000) * transformY) + moveY),

        new createjs.Point(((957.6666) * transformX) + moveX, ((500.0002) * transformY) + moveY),
        new createjs.Point(((958.3334) * transformX) + moveX, ((497.9998) * transformY) + moveY),
        new createjs.Point(((959.0000) * transformX) + moveX, ((496.0000) * transformY) + moveY),

        new createjs.Point(((959.3333) * transformX) + moveX, ((496.0000) * transformY) + moveY),
        new createjs.Point(((959.6667) * transformX) + moveX, ((496.0000) * transformY) + moveY),
        new createjs.Point(((960.0000) * transformX) + moveX, ((496.0000) * transformY) + moveY),

        new createjs.Point(((960.3333) * transformX) + moveX, ((494.3335) * transformY) + moveY),
        new createjs.Point(((960.6667) * transformX) + moveX, ((492.6665) * transformY) + moveY),
        new createjs.Point(((961.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((961.3333) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((961.6667) * transformX) + moveX, ((491.0000) * transformY) + moveY),
        new createjs.Point(((962.0000) * transformX) + moveX, ((491.0000) * transformY) + moveY),

        new createjs.Point(((962.3333) * transformX) + moveX, ((489.6668) * transformY) + moveY),
        new createjs.Point(((962.6667) * transformX) + moveX, ((488.3332) * transformY) + moveY),
        new createjs.Point(((963.0000) * transformX) + moveX, ((487.0000) * transformY) + moveY),

        new createjs.Point(((963.3333) * transformX) + moveX, ((487.0000) * transformY) + moveY),
        new createjs.Point(((963.6667) * transformX) + moveX, ((487.0000) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((487.0000) * transformY) + moveY),

        new createjs.Point(((964.0000) * transformX) + moveX, ((486.0001) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((484.9999) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((484.0000) * transformY) + moveY),

        new createjs.Point(((964.3333) * transformX) + moveX, ((484.0000) * transformY) + moveY),
        new createjs.Point(((964.6667) * transformX) + moveX, ((484.0000) * transformY) + moveY),
        new createjs.Point(((965.0000) * transformX) + moveX, ((484.0000) * transformY) + moveY),

        new createjs.Point(((965.3333) * transformX) + moveX, ((482.6668) * transformY) + moveY),
        new createjs.Point(((965.6667) * transformX) + moveX, ((481.3332) * transformY) + moveY),
        new createjs.Point(((966.0000) * transformX) + moveX, ((480.0000) * transformY) + moveY),

        new createjs.Point(((966.3333) * transformX) + moveX, ((480.0000) * transformY) + moveY),
        new createjs.Point(((966.6667) * transformX) + moveX, ((480.0000) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((480.0000) * transformY) + moveY),

        new createjs.Point(((967.0000) * transformX) + moveX, ((479.0001) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((477.9999) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((477.0000) * transformY) + moveY),

        new createjs.Point(((967.3333) * transformX) + moveX, ((477.0000) * transformY) + moveY),
        new createjs.Point(((967.6667) * transformX) + moveX, ((477.0000) * transformY) + moveY),
        new createjs.Point(((968.0000) * transformX) + moveX, ((477.0000) * transformY) + moveY),

        new createjs.Point(((968.0000) * transformX) + moveX, ((476.3334) * transformY) + moveY),
        new createjs.Point(((968.0000) * transformX) + moveX, ((475.6666) * transformY) + moveY),
        new createjs.Point(((968.0000) * transformX) + moveX, ((475.0000) * transformY) + moveY),

        new createjs.Point(((968.3333) * transformX) + moveX, ((475.0000) * transformY) + moveY),
        new createjs.Point(((968.6667) * transformX) + moveX, ((475.0000) * transformY) + moveY),
        new createjs.Point(((969.0000) * transformX) + moveX, ((475.0000) * transformY) + moveY),

        new createjs.Point(((969.0000) * transformX) + moveX, ((474.0001) * transformY) + moveY),
        new createjs.Point(((969.0000) * transformX) + moveX, ((472.9999) * transformY) + moveY),
        new createjs.Point(((969.0000) * transformX) + moveX, ((472.0000) * transformY) + moveY),

        new createjs.Point(((969.3333) * transformX) + moveX, ((472.0000) * transformY) + moveY),
        new createjs.Point(((969.6667) * transformX) + moveX, ((472.0000) * transformY) + moveY),
        new createjs.Point(((970.0000) * transformX) + moveX, ((472.0000) * transformY) + moveY),

        new createjs.Point(((970.0000) * transformX) + moveX, ((471.3334) * transformY) + moveY),
        new createjs.Point(((970.0000) * transformX) + moveX, ((470.6666) * transformY) + moveY),
        new createjs.Point(((970.0000) * transformX) + moveX, ((470.0000) * transformY) + moveY),

        new createjs.Point(((970.3333) * transformX) + moveX, ((470.0000) * transformY) + moveY),
        new createjs.Point(((970.6667) * transformX) + moveX, ((470.0000) * transformY) + moveY),
        new createjs.Point(((971.0000) * transformX) + moveX, ((470.0000) * transformY) + moveY),

        new createjs.Point(((971.6666) * transformX) + moveX, ((467.0003) * transformY) + moveY),
        new createjs.Point(((972.3334) * transformX) + moveX, ((463.9997) * transformY) + moveY),
        new createjs.Point(((973.0000) * transformX) + moveX, ((461.0000) * transformY) + moveY),

        new createjs.Point(((973.3333) * transformX) + moveX, ((461.0000) * transformY) + moveY),
        new createjs.Point(((973.6667) * transformX) + moveX, ((461.0000) * transformY) + moveY),
        new createjs.Point(((974.0000) * transformX) + moveX, ((461.0000) * transformY) + moveY),

        new createjs.Point(((974.0000) * transformX) + moveX, ((460.0001) * transformY) + moveY),
        new createjs.Point(((974.0000) * transformX) + moveX, ((458.9999) * transformY) + moveY),
        new createjs.Point(((974.0000) * transformX) + moveX, ((458.0000) * transformY) + moveY),

        new createjs.Point(((974.3333) * transformX) + moveX, ((458.0000) * transformY) + moveY),
        new createjs.Point(((974.6667) * transformX) + moveX, ((458.0000) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((458.0000) * transformY) + moveY),

        new createjs.Point(((975.0000) * transformX) + moveX, ((457.0001) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((455.9999) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((975.3333) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((975.6667) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((976.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((976.0000) * transformX) + moveX, ((454.0001) * transformY) + moveY),
        new createjs.Point(((976.0000) * transformX) + moveX, ((452.9999) * transformY) + moveY),
        new createjs.Point(((976.0000) * transformX) + moveX, ((452.0000) * transformY) + moveY),

        new createjs.Point(((976.3333) * transformX) + moveX, ((452.0000) * transformY) + moveY),
        new createjs.Point(((976.6667) * transformX) + moveX, ((452.0000) * transformY) + moveY),
        new createjs.Point(((977.0000) * transformX) + moveX, ((452.0000) * transformY) + moveY),

        new createjs.Point(((977.0000) * transformX) + moveX, ((451.0001) * transformY) + moveY),
        new createjs.Point(((977.0000) * transformX) + moveX, ((449.9999) * transformY) + moveY),
        new createjs.Point(((977.0000) * transformX) + moveX, ((449.0000) * transformY) + moveY),

        new createjs.Point(((977.3333) * transformX) + moveX, ((449.0000) * transformY) + moveY),
        new createjs.Point(((977.6667) * transformX) + moveX, ((449.0000) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((449.0000) * transformY) + moveY),

        new createjs.Point(((978.0000) * transformX) + moveX, ((448.0001) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((446.9999) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((446.0000) * transformY) + moveY),

        new createjs.Point(((978.3333) * transformX) + moveX, ((446.0000) * transformY) + moveY),
        new createjs.Point(((978.6667) * transformX) + moveX, ((446.0000) * transformY) + moveY),
        new createjs.Point(((979.0000) * transformX) + moveX, ((446.0000) * transformY) + moveY),

        new createjs.Point(((979.3333) * transformX) + moveX, ((443.6669) * transformY) + moveY),
        new createjs.Point(((979.6667) * transformX) + moveX, ((441.3331) * transformY) + moveY),
        new createjs.Point(((980.0000) * transformX) + moveX, ((439.0000) * transformY) + moveY),

        new createjs.Point(((980.3333) * transformX) + moveX, ((439.0000) * transformY) + moveY),
        new createjs.Point(((980.6667) * transformX) + moveX, ((439.0000) * transformY) + moveY),
        new createjs.Point(((981.0000) * transformX) + moveX, ((439.0000) * transformY) + moveY),

        new createjs.Point(((981.0000) * transformX) + moveX, ((437.6668) * transformY) + moveY),
        new createjs.Point(((981.0000) * transformX) + moveX, ((436.3332) * transformY) + moveY),
        new createjs.Point(((981.0000) * transformX) + moveX, ((435.0000) * transformY) + moveY),

        new createjs.Point(((981.3333) * transformX) + moveX, ((435.0000) * transformY) + moveY),
        new createjs.Point(((981.6667) * transformX) + moveX, ((435.0000) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((435.0000) * transformY) + moveY),

        new createjs.Point(((982.0000) * transformX) + moveX, ((433.6668) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((432.3332) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((431.0000) * transformY) + moveY),

        new createjs.Point(((982.3333) * transformX) + moveX, ((431.0000) * transformY) + moveY),
        new createjs.Point(((982.6667) * transformX) + moveX, ((431.0000) * transformY) + moveY),
        new createjs.Point(((983.0000) * transformX) + moveX, ((431.0000) * transformY) + moveY),

        new createjs.Point(((983.0000) * transformX) + moveX, ((429.6668) * transformY) + moveY),
        new createjs.Point(((983.0000) * transformX) + moveX, ((428.3332) * transformY) + moveY),
        new createjs.Point(((983.0000) * transformX) + moveX, ((427.0000) * transformY) + moveY),

        new createjs.Point(((983.3333) * transformX) + moveX, ((427.0000) * transformY) + moveY),
        new createjs.Point(((983.6667) * transformX) + moveX, ((427.0000) * transformY) + moveY),
        new createjs.Point(((984.0000) * transformX) + moveX, ((427.0000) * transformY) + moveY),

        new createjs.Point(((984.0000) * transformX) + moveX, ((425.6668) * transformY) + moveY),
        new createjs.Point(((984.0000) * transformX) + moveX, ((424.3332) * transformY) + moveY),
        new createjs.Point(((984.0000) * transformX) + moveX, ((423.0000) * transformY) + moveY),

        new createjs.Point(((984.3333) * transformX) + moveX, ((423.0000) * transformY) + moveY),
        new createjs.Point(((984.6667) * transformX) + moveX, ((423.0000) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((423.0000) * transformY) + moveY),

        new createjs.Point(((985.0000) * transformX) + moveX, ((421.3335) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((419.6665) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((418.0000) * transformY) + moveY),

        new createjs.Point(((985.3333) * transformX) + moveX, ((418.0000) * transformY) + moveY),
        new createjs.Point(((985.6667) * transformX) + moveX, ((418.0000) * transformY) + moveY),
        new createjs.Point(((986.0000) * transformX) + moveX, ((418.0000) * transformY) + moveY),

        new createjs.Point(((986.0000) * transformX) + moveX, ((416.6668) * transformY) + moveY),
        new createjs.Point(((986.0000) * transformX) + moveX, ((415.3332) * transformY) + moveY),
        new createjs.Point(((986.0000) * transformX) + moveX, ((414.0000) * transformY) + moveY),

        new createjs.Point(((986.3333) * transformX) + moveX, ((414.0000) * transformY) + moveY),
        new createjs.Point(((986.6667) * transformX) + moveX, ((414.0000) * transformY) + moveY),
        new createjs.Point(((987.0000) * transformX) + moveX, ((414.0000) * transformY) + moveY),

        new createjs.Point(((987.0000) * transformX) + moveX, ((412.0002) * transformY) + moveY),
        new createjs.Point(((987.0000) * transformX) + moveX, ((409.9998) * transformY) + moveY),
        new createjs.Point(((987.0000) * transformX) + moveX, ((408.0000) * transformY) + moveY),

        new createjs.Point(((987.3333) * transformX) + moveX, ((408.0000) * transformY) + moveY),
        new createjs.Point(((987.6667) * transformX) + moveX, ((408.0000) * transformY) + moveY),
        new createjs.Point(((988.0000) * transformX) + moveX, ((408.0000) * transformY) + moveY),

        new createjs.Point(((988.0000) * transformX) + moveX, ((406.3335) * transformY) + moveY),
        new createjs.Point(((988.0000) * transformX) + moveX, ((404.6665) * transformY) + moveY),
        new createjs.Point(((988.0000) * transformX) + moveX, ((403.0000) * transformY) + moveY),

        new createjs.Point(((988.3333) * transformX) + moveX, ((403.0000) * transformY) + moveY),
        new createjs.Point(((988.6667) * transformX) + moveX, ((403.0000) * transformY) + moveY),
        new createjs.Point(((989.0000) * transformX) + moveX, ((403.0000) * transformY) + moveY),

        new createjs.Point(((989.0000) * transformX) + moveX, ((400.6669) * transformY) + moveY),
        new createjs.Point(((989.0000) * transformX) + moveX, ((398.3331) * transformY) + moveY),
        new createjs.Point(((989.0000) * transformX) + moveX, ((396.0000) * transformY) + moveY),

        new createjs.Point(((989.3333) * transformX) + moveX, ((396.0000) * transformY) + moveY),
        new createjs.Point(((989.6667) * transformX) + moveX, ((396.0000) * transformY) + moveY),
        new createjs.Point(((990.0000) * transformX) + moveX, ((396.0000) * transformY) + moveY),

        new createjs.Point(((990.0000) * transformX) + moveX, ((393.6669) * transformY) + moveY),
        new createjs.Point(((990.0000) * transformX) + moveX, ((391.3331) * transformY) + moveY),
        new createjs.Point(((990.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((990.3333) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((990.6667) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((991.0000) * transformX) + moveX, ((386.0003) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((382.9997) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((380.0000) * transformY) + moveY),

        new createjs.Point(((991.3333) * transformX) + moveX, ((380.0000) * transformY) + moveY),
        new createjs.Point(((991.6667) * transformX) + moveX, ((380.0000) * transformY) + moveY),
        new createjs.Point(((992.0000) * transformX) + moveX, ((380.0000) * transformY) + moveY),

        new createjs.Point(((992.0000) * transformX) + moveX, ((376.0004) * transformY) + moveY),
        new createjs.Point(((992.0000) * transformX) + moveX, ((371.9996) * transformY) + moveY),
        new createjs.Point(((992.0000) * transformX) + moveX, ((368.0000) * transformY) + moveY),

        new createjs.Point(((994.6254) * transformX) + moveX, ((358.4949) * transformY) + moveY),
        new createjs.Point(((992.9972) * transformX) + moveX, ((343.2957) * transformY) + moveY),
        new createjs.Point(((993.0000) * transformX) + moveX, ((332.0000) * transformY) + moveY),

        new createjs.Point(((992.6667) * transformX) + moveX, ((323.6675) * transformY) + moveY),
        new createjs.Point(((992.3333) * transformX) + moveX, ((315.3325) * transformY) + moveY),
        new createjs.Point(((992.0000) * transformX) + moveX, ((307.0000) * transformY) + moveY),

        new createjs.Point(((991.6667) * transformX) + moveX, ((307.0000) * transformY) + moveY),
        new createjs.Point(((991.3333) * transformX) + moveX, ((307.0000) * transformY) + moveY),
        new createjs.Point(((991.0000) * transformX) + moveX, ((307.0000) * transformY) + moveY),

        new createjs.Point(((990.6667) * transformX) + moveX, ((301.3339) * transformY) + moveY),
        new createjs.Point(((990.3333) * transformX) + moveX, ((295.6661) * transformY) + moveY),
        new createjs.Point(((990.0000) * transformX) + moveX, ((290.0000) * transformY) + moveY),

        new createjs.Point(((989.6667) * transformX) + moveX, ((290.0000) * transformY) + moveY),
        new createjs.Point(((989.3333) * transformX) + moveX, ((290.0000) * transformY) + moveY),
        new createjs.Point(((989.0000) * transformX) + moveX, ((290.0000) * transformY) + moveY),

        new createjs.Point(((988.6667) * transformX) + moveX, ((285.6671) * transformY) + moveY),
        new createjs.Point(((988.3333) * transformX) + moveX, ((281.3329) * transformY) + moveY),
        new createjs.Point(((988.0000) * transformX) + moveX, ((277.0000) * transformY) + moveY),

        new createjs.Point(((987.6667) * transformX) + moveX, ((277.0000) * transformY) + moveY),
        new createjs.Point(((987.3333) * transformX) + moveX, ((277.0000) * transformY) + moveY),
        new createjs.Point(((987.0000) * transformX) + moveX, ((277.0000) * transformY) + moveY),

        new createjs.Point(((987.0000) * transformX) + moveX, ((275.0002) * transformY) + moveY),
        new createjs.Point(((987.0000) * transformX) + moveX, ((272.9998) * transformY) + moveY),
        new createjs.Point(((987.0000) * transformX) + moveX, ((271.0000) * transformY) + moveY),

        new createjs.Point(((986.6667) * transformX) + moveX, ((271.0000) * transformY) + moveY),
        new createjs.Point(((986.3333) * transformX) + moveX, ((271.0000) * transformY) + moveY),
        new createjs.Point(((986.0000) * transformX) + moveX, ((271.0000) * transformY) + moveY),

        new createjs.Point(((986.0000) * transformX) + moveX, ((269.3335) * transformY) + moveY),
        new createjs.Point(((986.0000) * transformX) + moveX, ((267.6665) * transformY) + moveY),
        new createjs.Point(((986.0000) * transformX) + moveX, ((266.0000) * transformY) + moveY),

        new createjs.Point(((985.6667) * transformX) + moveX, ((266.0000) * transformY) + moveY),
        new createjs.Point(((985.3333) * transformX) + moveX, ((266.0000) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((266.0000) * transformY) + moveY),

        new createjs.Point(((985.0000) * transformX) + moveX, ((264.3335) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((262.6665) * transformY) + moveY),
        new createjs.Point(((985.0000) * transformX) + moveX, ((261.0000) * transformY) + moveY),

        new createjs.Point(((984.6667) * transformX) + moveX, ((261.0000) * transformY) + moveY),
        new createjs.Point(((984.3333) * transformX) + moveX, ((261.0000) * transformY) + moveY),
        new createjs.Point(((984.0000) * transformX) + moveX, ((261.0000) * transformY) + moveY),

        new createjs.Point(((984.0000) * transformX) + moveX, ((259.6668) * transformY) + moveY),
        new createjs.Point(((984.0000) * transformX) + moveX, ((258.3332) * transformY) + moveY),
        new createjs.Point(((984.0000) * transformX) + moveX, ((257.0000) * transformY) + moveY),

        new createjs.Point(((983.6667) * transformX) + moveX, ((257.0000) * transformY) + moveY),
        new createjs.Point(((983.3333) * transformX) + moveX, ((257.0000) * transformY) + moveY),
        new createjs.Point(((983.0000) * transformX) + moveX, ((257.0000) * transformY) + moveY),

        new createjs.Point(((983.0000) * transformX) + moveX, ((255.6668) * transformY) + moveY),
        new createjs.Point(((983.0000) * transformX) + moveX, ((254.3332) * transformY) + moveY),
        new createjs.Point(((983.0000) * transformX) + moveX, ((253.0000) * transformY) + moveY),

        new createjs.Point(((982.6667) * transformX) + moveX, ((253.0000) * transformY) + moveY),
        new createjs.Point(((982.3333) * transformX) + moveX, ((253.0000) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((253.0000) * transformY) + moveY),

        new createjs.Point(((982.0000) * transformX) + moveX, ((251.6668) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((250.3332) * transformY) + moveY),
        new createjs.Point(((982.0000) * transformX) + moveX, ((249.0000) * transformY) + moveY),

        new createjs.Point(((981.6667) * transformX) + moveX, ((249.0000) * transformY) + moveY),
        new createjs.Point(((981.3333) * transformX) + moveX, ((249.0000) * transformY) + moveY),
        new createjs.Point(((981.0000) * transformX) + moveX, ((249.0000) * transformY) + moveY),

        new createjs.Point(((980.3334) * transformX) + moveX, ((245.0004) * transformY) + moveY),
        new createjs.Point(((979.6666) * transformX) + moveX, ((240.9996) * transformY) + moveY),
        new createjs.Point(((979.0000) * transformX) + moveX, ((237.0000) * transformY) + moveY),

        new createjs.Point(((978.6667) * transformX) + moveX, ((237.0000) * transformY) + moveY),
        new createjs.Point(((978.3333) * transformX) + moveX, ((237.0000) * transformY) + moveY),
        new createjs.Point(((978.0000) * transformX) + moveX, ((237.0000) * transformY) + moveY),

        new createjs.Point(((977.3334) * transformX) + moveX, ((233.6670) * transformY) + moveY),
        new createjs.Point(((976.6666) * transformX) + moveX, ((230.3330) * transformY) + moveY),
        new createjs.Point(((976.0000) * transformX) + moveX, ((227.0000) * transformY) + moveY),

        new createjs.Point(((975.6667) * transformX) + moveX, ((227.0000) * transformY) + moveY),
        new createjs.Point(((975.3333) * transformX) + moveX, ((227.0000) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((227.0000) * transformY) + moveY),

        new createjs.Point(((975.0000) * transformX) + moveX, ((226.0001) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((224.9999) * transformY) + moveY),
        new createjs.Point(((975.0000) * transformX) + moveX, ((224.0000) * transformY) + moveY),

        new createjs.Point(((974.6667) * transformX) + moveX, ((224.0000) * transformY) + moveY),
        new createjs.Point(((974.3333) * transformX) + moveX, ((224.0000) * transformY) + moveY),
        new createjs.Point(((974.0000) * transformX) + moveX, ((224.0000) * transformY) + moveY),

        new createjs.Point(((974.0000) * transformX) + moveX, ((223.0001) * transformY) + moveY),
        new createjs.Point(((974.0000) * transformX) + moveX, ((221.9999) * transformY) + moveY),
        new createjs.Point(((974.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),

        new createjs.Point(((973.6667) * transformX) + moveX, ((221.0000) * transformY) + moveY),
        new createjs.Point(((973.3333) * transformX) + moveX, ((221.0000) * transformY) + moveY),
        new createjs.Point(((973.0000) * transformX) + moveX, ((221.0000) * transformY) + moveY),

        new createjs.Point(((972.6667) * transformX) + moveX, ((219.0002) * transformY) + moveY),
        new createjs.Point(((972.3333) * transformX) + moveX, ((216.9998) * transformY) + moveY),
        new createjs.Point(((972.0000) * transformX) + moveX, ((215.0000) * transformY) + moveY),

        new createjs.Point(((971.6667) * transformX) + moveX, ((215.0000) * transformY) + moveY),
        new createjs.Point(((971.3333) * transformX) + moveX, ((215.0000) * transformY) + moveY),
        new createjs.Point(((971.0000) * transformX) + moveX, ((215.0000) * transformY) + moveY),

        new createjs.Point(((971.0000) * transformX) + moveX, ((214.3334) * transformY) + moveY),
        new createjs.Point(((971.0000) * transformX) + moveX, ((213.6666) * transformY) + moveY),
        new createjs.Point(((971.0000) * transformX) + moveX, ((213.0000) * transformY) + moveY),

        new createjs.Point(((970.6667) * transformX) + moveX, ((213.0000) * transformY) + moveY),
        new createjs.Point(((970.3333) * transformX) + moveX, ((213.0000) * transformY) + moveY),
        new createjs.Point(((970.0000) * transformX) + moveX, ((213.0000) * transformY) + moveY),

        new createjs.Point(((969.6667) * transformX) + moveX, ((211.0002) * transformY) + moveY),
        new createjs.Point(((969.3333) * transformX) + moveX, ((208.9998) * transformY) + moveY),
        new createjs.Point(((969.0000) * transformX) + moveX, ((207.0000) * transformY) + moveY),

        new createjs.Point(((968.6667) * transformX) + moveX, ((207.0000) * transformY) + moveY),
        new createjs.Point(((968.3333) * transformX) + moveX, ((207.0000) * transformY) + moveY),
        new createjs.Point(((968.0000) * transformX) + moveX, ((207.0000) * transformY) + moveY),

        new createjs.Point(((968.0000) * transformX) + moveX, ((206.3334) * transformY) + moveY),
        new createjs.Point(((968.0000) * transformX) + moveX, ((205.6666) * transformY) + moveY),
        new createjs.Point(((968.0000) * transformX) + moveX, ((205.0000) * transformY) + moveY),

        new createjs.Point(((967.6667) * transformX) + moveX, ((205.0000) * transformY) + moveY),
        new createjs.Point(((967.3333) * transformX) + moveX, ((205.0000) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((205.0000) * transformY) + moveY),

        new createjs.Point(((967.0000) * transformX) + moveX, ((204.0001) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((202.9999) * transformY) + moveY),
        new createjs.Point(((967.0000) * transformX) + moveX, ((202.0000) * transformY) + moveY),

        new createjs.Point(((966.6667) * transformX) + moveX, ((202.0000) * transformY) + moveY),
        new createjs.Point(((966.3333) * transformX) + moveX, ((202.0000) * transformY) + moveY),
        new createjs.Point(((966.0000) * transformX) + moveX, ((202.0000) * transformY) + moveY),

        new createjs.Point(((966.0000) * transformX) + moveX, ((201.3334) * transformY) + moveY),
        new createjs.Point(((966.0000) * transformX) + moveX, ((200.6666) * transformY) + moveY),
        new createjs.Point(((966.0000) * transformX) + moveX, ((200.0000) * transformY) + moveY),

        new createjs.Point(((965.6667) * transformX) + moveX, ((200.0000) * transformY) + moveY),
        new createjs.Point(((965.3333) * transformX) + moveX, ((200.0000) * transformY) + moveY),
        new createjs.Point(((965.0000) * transformX) + moveX, ((200.0000) * transformY) + moveY),

        new createjs.Point(((965.0000) * transformX) + moveX, ((199.0001) * transformY) + moveY),
        new createjs.Point(((965.0000) * transformX) + moveX, ((197.9999) * transformY) + moveY),
        new createjs.Point(((965.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),

        new createjs.Point(((964.6667) * transformX) + moveX, ((197.0000) * transformY) + moveY),
        new createjs.Point(((964.3333) * transformX) + moveX, ((197.0000) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((197.0000) * transformY) + moveY),

        new createjs.Point(((964.0000) * transformX) + moveX, ((196.3334) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((195.6666) * transformY) + moveY),
        new createjs.Point(((964.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),

        new createjs.Point(((963.6667) * transformX) + moveX, ((195.0000) * transformY) + moveY),
        new createjs.Point(((963.3333) * transformX) + moveX, ((195.0000) * transformY) + moveY),
        new createjs.Point(((963.0000) * transformX) + moveX, ((195.0000) * transformY) + moveY),

        new createjs.Point(((963.0000) * transformX) + moveX, ((194.0001) * transformY) + moveY),
        new createjs.Point(((963.0000) * transformX) + moveX, ((192.9999) * transformY) + moveY),
        new createjs.Point(((963.0000) * transformX) + moveX, ((192.0000) * transformY) + moveY),

        new createjs.Point(((962.6667) * transformX) + moveX, ((192.0000) * transformY) + moveY),
        new createjs.Point(((962.3333) * transformX) + moveX, ((192.0000) * transformY) + moveY),
        new createjs.Point(((962.0000) * transformX) + moveX, ((192.0000) * transformY) + moveY),

        new createjs.Point(((961.6667) * transformX) + moveX, ((190.6668) * transformY) + moveY),
        new createjs.Point(((961.3333) * transformX) + moveX, ((189.3332) * transformY) + moveY),
        new createjs.Point(((961.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),

        new createjs.Point(((960.6667) * transformX) + moveX, ((188.0000) * transformY) + moveY),
        new createjs.Point(((960.3333) * transformX) + moveX, ((188.0000) * transformY) + moveY),
        new createjs.Point(((960.0000) * transformX) + moveX, ((188.0000) * transformY) + moveY),

        new createjs.Point(((960.0000) * transformX) + moveX, ((187.0001) * transformY) + moveY),
        new createjs.Point(((960.0000) * transformX) + moveX, ((185.9999) * transformY) + moveY),
        new createjs.Point(((960.0000) * transformX) + moveX, ((185.0000) * transformY) + moveY),

        new createjs.Point(((959.6667) * transformX) + moveX, ((185.0000) * transformY) + moveY),
        new createjs.Point(((959.3333) * transformX) + moveX, ((185.0000) * transformY) + moveY),
        new createjs.Point(((959.0000) * transformX) + moveX, ((185.0000) * transformY) + moveY),

        new createjs.Point(((958.3334) * transformX) + moveX, ((183.0002) * transformY) + moveY),
        new createjs.Point(((957.6666) * transformX) + moveX, ((180.9998) * transformY) + moveY),
        new createjs.Point(((957.0000) * transformX) + moveX, ((179.0000) * transformY) + moveY),

        new createjs.Point(((956.6667) * transformX) + moveX, ((179.0000) * transformY) + moveY),
        new createjs.Point(((956.3333) * transformX) + moveX, ((179.0000) * transformY) + moveY),
        new createjs.Point(((956.0000) * transformX) + moveX, ((179.0000) * transformY) + moveY),

        new createjs.Point(((956.0000) * transformX) + moveX, ((178.3334) * transformY) + moveY),
        new createjs.Point(((956.0000) * transformX) + moveX, ((177.6666) * transformY) + moveY),
        new createjs.Point(((956.0000) * transformX) + moveX, ((177.0000) * transformY) + moveY),

        new createjs.Point(((955.6667) * transformX) + moveX, ((177.0000) * transformY) + moveY),
        new createjs.Point(((955.3333) * transformX) + moveX, ((177.0000) * transformY) + moveY),
        new createjs.Point(((955.0000) * transformX) + moveX, ((177.0000) * transformY) + moveY),

        new createjs.Point(((955.0000) * transformX) + moveX, ((176.3334) * transformY) + moveY),
        new createjs.Point(((955.0000) * transformX) + moveX, ((175.6666) * transformY) + moveY),
        new createjs.Point(((955.0000) * transformX) + moveX, ((175.0000) * transformY) + moveY),

        new createjs.Point(((954.6667) * transformX) + moveX, ((175.0000) * transformY) + moveY),
        new createjs.Point(((954.3333) * transformX) + moveX, ((175.0000) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((175.0000) * transformY) + moveY),

        new createjs.Point(((954.0000) * transformX) + moveX, ((174.3334) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((173.6666) * transformY) + moveY),
        new createjs.Point(((954.0000) * transformX) + moveX, ((173.0000) * transformY) + moveY),

        new createjs.Point(((953.6667) * transformX) + moveX, ((173.0000) * transformY) + moveY),
        new createjs.Point(((953.3333) * transformX) + moveX, ((173.0000) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((173.0000) * transformY) + moveY),

        new createjs.Point(((953.0000) * transformX) + moveX, ((172.3334) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((171.6666) * transformY) + moveY),
        new createjs.Point(((953.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),

        new createjs.Point(((952.6667) * transformX) + moveX, ((171.0000) * transformY) + moveY),
        new createjs.Point(((952.3333) * transformX) + moveX, ((171.0000) * transformY) + moveY),
        new createjs.Point(((952.0000) * transformX) + moveX, ((171.0000) * transformY) + moveY),

        new createjs.Point(((952.0000) * transformX) + moveX, ((170.3334) * transformY) + moveY),
        new createjs.Point(((952.0000) * transformX) + moveX, ((169.6666) * transformY) + moveY),
        new createjs.Point(((952.0000) * transformX) + moveX, ((169.0000) * transformY) + moveY),

        new createjs.Point(((951.6667) * transformX) + moveX, ((169.0000) * transformY) + moveY),
        new createjs.Point(((951.3333) * transformX) + moveX, ((169.0000) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((169.0000) * transformY) + moveY),

        new createjs.Point(((951.0000) * transformX) + moveX, ((168.3334) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((167.6666) * transformY) + moveY),
        new createjs.Point(((951.0000) * transformX) + moveX, ((167.0000) * transformY) + moveY),

        new createjs.Point(((950.6667) * transformX) + moveX, ((167.0000) * transformY) + moveY),
        new createjs.Point(((950.3333) * transformX) + moveX, ((167.0000) * transformY) + moveY),
        new createjs.Point(((950.0000) * transformX) + moveX, ((167.0000) * transformY) + moveY),

        new createjs.Point(((950.0000) * transformX) + moveX, ((166.3334) * transformY) + moveY),
        new createjs.Point(((950.0000) * transformX) + moveX, ((165.6666) * transformY) + moveY),
        new createjs.Point(((950.0000) * transformX) + moveX, ((165.0000) * transformY) + moveY),

        new createjs.Point(((949.6667) * transformX) + moveX, ((165.0000) * transformY) + moveY),
        new createjs.Point(((949.3333) * transformX) + moveX, ((165.0000) * transformY) + moveY),
        new createjs.Point(((949.0000) * transformX) + moveX, ((165.0000) * transformY) + moveY),

        new createjs.Point(((949.0000) * transformX) + moveX, ((164.3334) * transformY) + moveY),
        new createjs.Point(((949.0000) * transformX) + moveX, ((163.6666) * transformY) + moveY),
        new createjs.Point(((949.0000) * transformX) + moveX, ((163.0000) * transformY) + moveY),

        new createjs.Point(((948.6667) * transformX) + moveX, ((163.0000) * transformY) + moveY),
        new createjs.Point(((948.3333) * transformX) + moveX, ((163.0000) * transformY) + moveY),
        new createjs.Point(((948.0000) * transformX) + moveX, ((163.0000) * transformY) + moveY),

        new createjs.Point(((948.0000) * transformX) + moveX, ((162.3334) * transformY) + moveY),
        new createjs.Point(((948.0000) * transformX) + moveX, ((161.6666) * transformY) + moveY),
        new createjs.Point(((948.0000) * transformX) + moveX, ((161.0000) * transformY) + moveY),

        new createjs.Point(((947.3334) * transformX) + moveX, ((160.6667) * transformY) + moveY),
        new createjs.Point(((946.6666) * transformX) + moveX, ((160.3333) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((160.0000) * transformY) + moveY),

        new createjs.Point(((946.0000) * transformX) + moveX, ((159.3334) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((158.6666) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((158.0000) * transformY) + moveY),

        new createjs.Point(((945.6667) * transformX) + moveX, ((158.0000) * transformY) + moveY),
        new createjs.Point(((945.3333) * transformX) + moveX, ((158.0000) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((158.0000) * transformY) + moveY),

        new createjs.Point(((945.0000) * transformX) + moveX, ((157.3334) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((156.6666) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((156.0000) * transformY) + moveY),

        new createjs.Point(((944.6667) * transformX) + moveX, ((156.0000) * transformY) + moveY),
        new createjs.Point(((944.3333) * transformX) + moveX, ((156.0000) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((156.0000) * transformY) + moveY),

        new createjs.Point(((944.0000) * transformX) + moveX, ((155.3334) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((154.6666) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((154.0000) * transformY) + moveY),

        new createjs.Point(((943.6667) * transformX) + moveX, ((154.0000) * transformY) + moveY),
        new createjs.Point(((943.3333) * transformX) + moveX, ((154.0000) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((154.0000) * transformY) + moveY),

        new createjs.Point(((943.0000) * transformX) + moveX, ((153.3334) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((152.6666) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((152.0000) * transformY) + moveY),

        new createjs.Point(((942.3334) * transformX) + moveX, ((151.6667) * transformY) + moveY),
        new createjs.Point(((941.6666) * transformX) + moveX, ((151.3333) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((151.0000) * transformY) + moveY),

        new createjs.Point(((940.6667) * transformX) + moveX, ((149.6668) * transformY) + moveY),
        new createjs.Point(((940.3333) * transformX) + moveX, ((148.3332) * transformY) + moveY),
        new createjs.Point(((940.0000) * transformX) + moveX, ((147.0000) * transformY) + moveY),

        new createjs.Point(((939.3334) * transformX) + moveX, ((146.6667) * transformY) + moveY),
        new createjs.Point(((938.6666) * transformX) + moveX, ((146.3333) * transformY) + moveY),
        new createjs.Point(((938.0000) * transformX) + moveX, ((146.0000) * transformY) + moveY),

        new createjs.Point(((937.6667) * transformX) + moveX, ((144.6668) * transformY) + moveY),
        new createjs.Point(((937.3333) * transformX) + moveX, ((143.3332) * transformY) + moveY),
        new createjs.Point(((937.0000) * transformX) + moveX, ((142.0000) * transformY) + moveY),

        new createjs.Point(((936.3334) * transformX) + moveX, ((141.6667) * transformY) + moveY),
        new createjs.Point(((935.6666) * transformX) + moveX, ((141.3333) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((141.0000) * transformY) + moveY),

        new createjs.Point(((935.0000) * transformX) + moveX, ((140.3334) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((139.6666) * transformY) + moveY),
        new createjs.Point(((935.0000) * transformX) + moveX, ((139.0000) * transformY) + moveY),

        new createjs.Point(((934.3334) * transformX) + moveX, ((138.6667) * transformY) + moveY),
        new createjs.Point(((933.6666) * transformX) + moveX, ((138.3333) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((138.0000) * transformY) + moveY),

        new createjs.Point(((933.0000) * transformX) + moveX, ((137.3334) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((136.6666) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((136.0000) * transformY) + moveY),

        new createjs.Point(((932.3334) * transformX) + moveX, ((135.6667) * transformY) + moveY),
        new createjs.Point(((931.6666) * transformX) + moveX, ((135.3333) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((135.0000) * transformY) + moveY),

        new createjs.Point(((931.0000) * transformX) + moveX, ((134.3334) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((133.6666) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((133.0000) * transformY) + moveY),

        new createjs.Point(((930.3334) * transformX) + moveX, ((132.6667) * transformY) + moveY),
        new createjs.Point(((929.6666) * transformX) + moveX, ((132.3333) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((132.0000) * transformY) + moveY),

        new createjs.Point(((929.0000) * transformX) + moveX, ((131.3334) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((130.6666) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((130.0000) * transformY) + moveY),

        new createjs.Point(((928.3334) * transformX) + moveX, ((129.6667) * transformY) + moveY),
        new createjs.Point(((927.6666) * transformX) + moveX, ((129.3333) * transformY) + moveY),
        new createjs.Point(((927.0000) * transformX) + moveX, ((129.0000) * transformY) + moveY),

        new createjs.Point(((927.0000) * transformX) + moveX, ((128.3334) * transformY) + moveY),
        new createjs.Point(((927.0000) * transformX) + moveX, ((127.6666) * transformY) + moveY),
        new createjs.Point(((927.0000) * transformX) + moveX, ((127.0000) * transformY) + moveY),

        new createjs.Point(((926.0001) * transformX) + moveX, ((126.3334) * transformY) + moveY),
        new createjs.Point(((924.9999) * transformX) + moveX, ((125.6666) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((125.0000) * transformY) + moveY),

        new createjs.Point(((924.0000) * transformX) + moveX, ((124.3334) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((123.6666) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((123.0000) * transformY) + moveY),

        new createjs.Point(((923.3334) * transformX) + moveX, ((122.6667) * transformY) + moveY),
        new createjs.Point(((922.6666) * transformX) + moveX, ((122.3333) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((122.0000) * transformY) + moveY),

        new createjs.Point(((922.0000) * transformX) + moveX, ((121.3334) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((120.6666) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((120.0000) * transformY) + moveY),

        new createjs.Point(((921.0001) * transformX) + moveX, ((119.3334) * transformY) + moveY),
        new createjs.Point(((919.9999) * transformX) + moveX, ((118.6666) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((118.0000) * transformY) + moveY),

        new createjs.Point(((919.0000) * transformX) + moveX, ((117.3334) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((116.6666) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((116.0000) * transformY) + moveY),

        new createjs.Point(((917.6668) * transformX) + moveX, ((115.0001) * transformY) + moveY),
        new createjs.Point(((916.3332) * transformX) + moveX, ((113.9999) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((113.0000) * transformY) + moveY),

        new createjs.Point(((915.0000) * transformX) + moveX, ((112.3334) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((111.6666) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((111.0000) * transformY) + moveY),

        new createjs.Point(((913.6668) * transformX) + moveX, ((110.0001) * transformY) + moveY),
        new createjs.Point(((912.3332) * transformX) + moveX, ((108.9999) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((108.0000) * transformY) + moveY),

        new createjs.Point(((911.0000) * transformX) + moveX, ((107.3334) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((106.6666) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((106.0000) * transformY) + moveY),

        new createjs.Point(((909.3335) * transformX) + moveX, ((104.6668) * transformY) + moveY),
        new createjs.Point(((907.6665) * transformX) + moveX, ((103.3332) * transformY) + moveY),
        new createjs.Point(((906.0000) * transformX) + moveX, ((102.0000) * transformY) + moveY),

        new createjs.Point(((906.0000) * transformX) + moveX, ((101.3334) * transformY) + moveY),
        new createjs.Point(((906.0000) * transformX) + moveX, ((100.6666) * transformY) + moveY),
        new createjs.Point(((906.0000) * transformX) + moveX, ((100.0000) * transformY) + moveY),

        new createjs.Point(((902.6670) * transformX) + moveX, ((97.0003) * transformY) + moveY),
        new createjs.Point(((899.3330) * transformX) + moveX, ((93.9997) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((91.0000) * transformY) + moveY),

        new createjs.Point(((896.0000) * transformX) + moveX, ((90.3334) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((89.6666) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((89.0000) * transformY) + moveY),

        new createjs.Point(((894.6668) * transformX) + moveX, ((88.0001) * transformY) + moveY),
        new createjs.Point(((893.3332) * transformX) + moveX, ((86.9999) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((86.0000) * transformY) + moveY),

        new createjs.Point(((889.6669) * transformX) + moveX, ((83.3336) * transformY) + moveY),
        new createjs.Point(((887.3331) * transformX) + moveX, ((80.6664) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((884.3334) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((883.6666) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((880.3336) * transformX) + moveX, ((75.0003) * transformY) + moveY),
        new createjs.Point(((877.6664) * transformX) + moveX, ((71.9997) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((874.3334) * transformX) + moveX, ((69.0000) * transformY) + moveY),
        new createjs.Point(((873.6666) * transformX) + moveX, ((69.0000) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((69.0000) * transformY) + moveY),

        new createjs.Point(((871.6668) * transformX) + moveX, ((67.3335) * transformY) + moveY),
        new createjs.Point(((870.3332) * transformX) + moveX, ((65.6665) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((64.0000) * transformY) + moveY),

        new createjs.Point(((868.3334) * transformX) + moveX, ((64.0000) * transformY) + moveY),
        new createjs.Point(((867.6666) * transformX) + moveX, ((64.0000) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((64.0000) * transformY) + moveY),

        new createjs.Point(((866.0001) * transformX) + moveX, ((62.6668) * transformY) + moveY),
        new createjs.Point(((864.9999) * transformX) + moveX, ((61.3332) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((60.0000) * transformY) + moveY),

        new createjs.Point(((863.3334) * transformX) + moveX, ((60.0000) * transformY) + moveY),
        new createjs.Point(((862.6666) * transformX) + moveX, ((60.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((60.0000) * transformY) + moveY),

        new createjs.Point(((861.0001) * transformX) + moveX, ((58.6668) * transformY) + moveY),
        new createjs.Point(((859.9999) * transformX) + moveX, ((57.3332) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((858.3334) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((857.6666) * transformX) + moveX, ((56.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((56.0000) * transformY) + moveY),

        new createjs.Point(((856.3334) * transformX) + moveX, ((55.0001) * transformY) + moveY),
        new createjs.Point(((855.6666) * transformX) + moveX, ((53.9999) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((53.0000) * transformY) + moveY),

        new createjs.Point(((854.3334) * transformX) + moveX, ((53.0000) * transformY) + moveY),
        new createjs.Point(((853.6666) * transformX) + moveX, ((53.0000) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((53.0000) * transformY) + moveY),

        new createjs.Point(((852.6667) * transformX) + moveX, ((52.3334) * transformY) + moveY),
        new createjs.Point(((852.3333) * transformX) + moveX, ((51.6666) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((51.0000) * transformY) + moveY),

        new createjs.Point(((851.0001) * transformX) + moveX, ((50.6667) * transformY) + moveY),
        new createjs.Point(((849.9999) * transformX) + moveX, ((50.3333) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((50.0000) * transformY) + moveY),

        new createjs.Point(((849.0000) * transformX) + moveX, ((49.6667) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((49.3333) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((49.0000) * transformY) + moveY),

        new createjs.Point(((848.3334) * transformX) + moveX, ((49.0000) * transformY) + moveY),
        new createjs.Point(((847.6666) * transformX) + moveX, ((49.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((49.0000) * transformY) + moveY),

        new createjs.Point(((846.6667) * transformX) + moveX, ((48.3334) * transformY) + moveY),
        new createjs.Point(((846.3333) * transformX) + moveX, ((47.6666) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((845.3334) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((844.6666) * transformX) + moveX, ((47.0000) * transformY) + moveY),
        new createjs.Point(((844.0000) * transformX) + moveX, ((47.0000) * transformY) + moveY),

        new createjs.Point(((843.6667) * transformX) + moveX, ((46.3334) * transformY) + moveY),
        new createjs.Point(((843.3333) * transformX) + moveX, ((45.6666) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((45.0000) * transformY) + moveY),

        new createjs.Point(((842.3334) * transformX) + moveX, ((45.0000) * transformY) + moveY),
        new createjs.Point(((841.6666) * transformX) + moveX, ((45.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((45.0000) * transformY) + moveY),

        new createjs.Point(((840.6667) * transformX) + moveX, ((44.3334) * transformY) + moveY),
        new createjs.Point(((840.3333) * transformX) + moveX, ((43.6666) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((43.0000) * transformY) + moveY),

        new createjs.Point(((839.3334) * transformX) + moveX, ((43.0000) * transformY) + moveY),
        new createjs.Point(((838.6666) * transformX) + moveX, ((43.0000) * transformY) + moveY),
        new createjs.Point(((838.0000) * transformX) + moveX, ((43.0000) * transformY) + moveY),

        new createjs.Point(((837.6667) * transformX) + moveX, ((42.3334) * transformY) + moveY),
        new createjs.Point(((837.3333) * transformX) + moveX, ((41.6666) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((41.0000) * transformY) + moveY),

        new createjs.Point(((836.3334) * transformX) + moveX, ((41.0000) * transformY) + moveY),
        new createjs.Point(((835.6666) * transformX) + moveX, ((41.0000) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((41.0000) * transformY) + moveY),

        new createjs.Point(((835.0000) * transformX) + moveX, ((40.6667) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((40.3333) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((40.0000) * transformY) + moveY),

        new createjs.Point(((834.3334) * transformX) + moveX, ((40.0000) * transformY) + moveY),
        new createjs.Point(((833.6666) * transformX) + moveX, ((40.0000) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((40.0000) * transformY) + moveY),

        new createjs.Point(((832.6667) * transformX) + moveX, ((39.3334) * transformY) + moveY),
        new createjs.Point(((832.3333) * transformX) + moveX, ((38.6666) * transformY) + moveY),
        new createjs.Point(((832.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((830.0002) * transformX) + moveX, ((37.3334) * transformY) + moveY),
        new createjs.Point(((827.9998) * transformX) + moveX, ((36.6666) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((36.0000) * transformY) + moveY),

        new createjs.Point(((825.6667) * transformX) + moveX, ((35.3334) * transformY) + moveY),
        new createjs.Point(((825.3333) * transformX) + moveX, ((34.6666) * transformY) + moveY),
        new createjs.Point(((825.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((824.3334) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((823.6666) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((823.0000) * transformX) + moveX, ((33.6667) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((33.3333) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((33.0000) * transformY) + moveY),

        new createjs.Point(((822.3334) * transformX) + moveX, ((33.0000) * transformY) + moveY),
        new createjs.Point(((821.6666) * transformX) + moveX, ((33.0000) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((33.0000) * transformY) + moveY),

        new createjs.Point(((821.0000) * transformX) + moveX, ((32.6667) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((32.3333) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((32.0000) * transformY) + moveY),

        new createjs.Point(((820.3334) * transformX) + moveX, ((32.0000) * transformY) + moveY),
        new createjs.Point(((819.6666) * transformX) + moveX, ((32.0000) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((32.0000) * transformY) + moveY),

        new createjs.Point(((819.0000) * transformX) + moveX, ((31.6667) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((31.3333) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((818.3334) * transformX) + moveX, ((31.0000) * transformY) + moveY),
        new createjs.Point(((817.6666) * transformX) + moveX, ((31.0000) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((817.0000) * transformX) + moveX, ((30.6667) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((30.3333) * transformY) + moveY),
        new createjs.Point(((817.0000) * transformX) + moveX, ((30.0000) * transformY) + moveY),

        new createjs.Point(((816.3334) * transformX) + moveX, ((30.0000) * transformY) + moveY),
        new createjs.Point(((815.6666) * transformX) + moveX, ((30.0000) * transformY) + moveY),
        new createjs.Point(((815.0000) * transformX) + moveX, ((30.0000) * transformY) + moveY),

        new createjs.Point(((815.0000) * transformX) + moveX, ((29.6667) * transformY) + moveY),
        new createjs.Point(((815.0000) * transformX) + moveX, ((29.3333) * transformY) + moveY),
        new createjs.Point(((815.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((814.3334) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((813.6666) * transformX) + moveX, ((29.0000) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((29.0000) * transformY) + moveY),

        new createjs.Point(((813.0000) * transformX) + moveX, ((28.6667) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((28.3333) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((28.0000) * transformY) + moveY),

        new createjs.Point(((812.3334) * transformX) + moveX, ((28.0000) * transformY) + moveY),
        new createjs.Point(((811.6666) * transformX) + moveX, ((28.0000) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((28.0000) * transformY) + moveY),

        new createjs.Point(((811.0000) * transformX) + moveX, ((27.6667) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((27.3333) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((27.0000) * transformY) + moveY),

        new createjs.Point(((810.3334) * transformX) + moveX, ((27.0000) * transformY) + moveY),
        new createjs.Point(((809.6666) * transformX) + moveX, ((27.0000) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((27.0000) * transformY) + moveY),

        new createjs.Point(((809.0000) * transformX) + moveX, ((26.6667) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((26.3333) * transformY) + moveY),
        new createjs.Point(((809.0000) * transformX) + moveX, ((26.0000) * transformY) + moveY),

        new createjs.Point(((807.0002) * transformX) + moveX, ((25.3334) * transformY) + moveY),
        new createjs.Point(((804.9998) * transformX) + moveX, ((24.6666) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((24.0000) * transformY) + moveY),

        new createjs.Point(((803.0000) * transformX) + moveX, ((23.6667) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((23.3333) * transformY) + moveY),
        new createjs.Point(((803.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((802.0001) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((800.9999) * transformX) + moveX, ((23.0000) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((23.0000) * transformY) + moveY),

        new createjs.Point(((800.0000) * transformX) + moveX, ((22.6667) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((22.3333) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((22.0000) * transformY) + moveY),

        new createjs.Point(((798.6668) * transformX) + moveX, ((21.6667) * transformY) + moveY),
        new createjs.Point(((797.3332) * transformX) + moveX, ((21.3333) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((796.0000) * transformX) + moveX, ((20.6667) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((20.3333) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((20.0000) * transformY) + moveY),

        new createjs.Point(((795.0001) * transformX) + moveX, ((20.0000) * transformY) + moveY),
        new createjs.Point(((793.9999) * transformX) + moveX, ((20.0000) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((20.0000) * transformY) + moveY),

        new createjs.Point(((793.0000) * transformX) + moveX, ((19.6667) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((19.3333) * transformY) + moveY),
        new createjs.Point(((793.0000) * transformX) + moveX, ((19.0000) * transformY) + moveY),

        new createjs.Point(((792.3334) * transformX) + moveX, ((19.0000) * transformY) + moveY),
        new createjs.Point(((791.6666) * transformX) + moveX, ((19.0000) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((19.0000) * transformY) + moveY),

        new createjs.Point(((791.0000) * transformX) + moveX, ((18.6667) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((18.3333) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((788.0003) * transformX) + moveX, ((17.3334) * transformY) + moveY),
        new createjs.Point(((784.9997) * transformX) + moveX, ((16.6666) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((16.0000) * transformY) + moveY),

        new createjs.Point(((782.0000) * transformX) + moveX, ((15.6667) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((15.3333) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((781.3334) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((780.6666) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((780.0000) * transformX) + moveX, ((14.6667) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((14.3333) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((14.0000) * transformY) + moveY),

        new createjs.Point(((777.6669) * transformX) + moveX, ((13.6667) * transformY) + moveY),
        new createjs.Point(((775.3331) * transformX) + moveX, ((13.3333) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((13.0000) * transformY) + moveY),

        new createjs.Point(((773.0000) * transformX) + moveX, ((12.6667) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((12.3333) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((772.0001) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((770.9999) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((770.0000) * transformX) + moveX, ((11.6667) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((11.3333) * transformY) + moveY),
        new createjs.Point(((770.0000) * transformX) + moveX, ((11.0000) * transformY) + moveY),

        new createjs.Point(((769.0001) * transformX) + moveX, ((11.0000) * transformY) + moveY),
        new createjs.Point(((767.9999) * transformX) + moveX, ((11.0000) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((11.0000) * transformY) + moveY),

        new createjs.Point(((767.0000) * transformX) + moveX, ((10.6667) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((10.3333) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((10.0000) * transformY) + moveY),

        new createjs.Point(((764.3336) * transformX) + moveX, ((9.6667) * transformY) + moveY),
        new createjs.Point(((761.6664) * transformX) + moveX, ((9.3333) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((759.0000) * transformX) + moveX, ((8.6667) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((8.3333) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((8.0000) * transformY) + moveY),

        new createjs.Point(((756.3336) * transformX) + moveX, ((7.6667) * transformY) + moveY),
        new createjs.Point(((753.6664) * transformX) + moveX, ((7.3333) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((7.0000) * transformY) + moveY),

        new createjs.Point(((751.0000) * transformX) + moveX, ((6.6667) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((6.3333) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((6.0000) * transformY) + moveY),

        new createjs.Point(((749.3335) * transformX) + moveX, ((6.0000) * transformY) + moveY),
        new createjs.Point(((747.6665) * transformX) + moveX, ((6.0000) * transformY) + moveY),
        new createjs.Point(((746.0000) * transformX) + moveX, ((6.0000) * transformY) + moveY),

        new createjs.Point(((746.0000) * transformX) + moveX, ((5.6667) * transformY) + moveY),
        new createjs.Point(((746.0000) * transformX) + moveX, ((5.3333) * transformY) + moveY),
        new createjs.Point(((746.0000) * transformX) + moveX, ((5.0000) * transformY) + moveY),

        new createjs.Point(((744.3335) * transformX) + moveX, ((5.0000) * transformY) + moveY),
        new createjs.Point(((742.6665) * transformX) + moveX, ((5.0000) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((5.0000) * transformY) + moveY),

        new createjs.Point(((741.0000) * transformX) + moveX, ((4.6667) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((4.3333) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((4.0000) * transformY) + moveY),

        new createjs.Point(((739.0002) * transformX) + moveX, ((4.0000) * transformY) + moveY),
        new createjs.Point(((736.9998) * transformX) + moveX, ((4.0000) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((4.0000) * transformY) + moveY),

        new createjs.Point(((735.0000) * transformX) + moveX, ((3.6667) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((3.3333) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((732.6669) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((730.3331) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((728.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((728.0000) * transformX) + moveX, ((2.6667) * transformY) + moveY),
        new createjs.Point(((728.0000) * transformX) + moveX, ((2.3333) * transformY) + moveY),
        new createjs.Point(((728.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((725.3336) * transformX) + moveX, ((2.0000) * transformY) + moveY),
        new createjs.Point(((722.6664) * transformX) + moveX, ((2.0000) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((2.0000) * transformY) + moveY),

        new createjs.Point(((720.0000) * transformX) + moveX, ((1.6667) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((1.3333) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((1.0000) * transformY) + moveY),

        new createjs.Point(((716.3337) * transformX) + moveX, ((1.0000) * transformY) + moveY),
        new createjs.Point(((712.6663) * transformX) + moveX, ((1.0000) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((1.0000) * transformY) + moveY),
        //Top Right Ends


    ];

    var points2 = [
        new createjs.Point(startX2, startY2),
        new createjs.Point(endX2, endY2)
    ];
    var points1Final = points;

    var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath, motionPath2);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final), getMotionPathFromPoints(points2Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 8", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("28", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 100).call(function() {
            setTimeout(function() {
                gotoLastAndStop(Stage1);
            }, 500);

        })).on('change', (function(event) {
            if (currentMotionStep == 1) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        this.timeline.addTween(cjs.Tween.get(bar2).wait(40).to({
            guide: {
                path: motionPath2
            }
        }, 80).wait(50)).on('change', (function(event) {
            if (currentMotionStep == 2) {
                bar2 = drawCharacter(thisStage, bar2, true, event);
            }
        }));


    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];

                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }
            
            if (thisbar.x == 706.8166059697317 && thisbar.y == 210.226817909195) { // finished plotting 1/3 part 
                Stage1.shape.visible = false;
            } else if (thisbar.x === startX && thisbar.y === startY) { // plot 1st point
                Stage1.shape.visible = true;
            }

            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.32)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.movie.pencil.visible = true;
            thisStage.paused = false;
            thisStage.movie.play();
        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            thisStage.movie.pencil.visible = true;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            thisStage.movie.pencil.visible = false;
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);

        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }

    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function gotoLastAndStop(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        thisStage.pencil = thisStage.movie.pencil;
        thisStage.addChild(thisStage.pencil);
        currentMotionStep = 1;
        Stage1.shape.visible = false;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
