(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p84_1.png",
            id: "p84_1"
        }]
    };

    (lib.p84_1 = function() {
        this.initialize(img.p84_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("84", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(37, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Rita visare och skriv tiden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.text_2 = new cjs.Text("För 1 timme sedan", "bold 16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(30, 43);

        this.text_3 = new cjs.Text("Nu", "bold 16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(260, 43);

        this.text_4 = new cjs.Text("Om 1 timme", "bold 16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(390, 43);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 250, 10);
        this.roundRect1.setTransform(10, 25);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_1.setTransform(10, 15);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_2.setTransform(10, 125);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 30);
        this.vrLine_1.setTransform(145, 25);

        this.vrLine_2 = new cjs.Shape();
        this.vrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 30);
        this.vrLine_2.setTransform(310, 25);

        this.vrLine_3 = new cjs.Shape();
        this.vrLine_3.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 100);
        this.vrLine_3.setTransform(145, 60);

        this.vrLine_4 = new cjs.Shape();
        this.vrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 100);
        this.vrLine_4.setTransform(310, 60);

        this.vrLine_5 = new cjs.Shape();
        this.vrLine_5.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 100);
        this.vrLine_5.setTransform(145, 170);

        this.vrLine_6 = new cjs.Shape();
        this.vrLine_6.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 100);
        this.vrLine_6.setTransform(310, 170);


        this.Symbolclock_1 = new lib.Symboldottedclock();
        this.Symbolclock_1.setTransform(100, 101, 0.83, 0.83);

        this.Symbolclock_2 = new lib.Symboldotclock();
        this.Symbolclock_2.setTransform(270, 101, 0.83, 0.83);

        this.Symbolclock_3 = new lib.Symboldottedclock();
        this.Symbolclock_3.setTransform(435, 101, 0.83, 0.83);

        this.Symbolclock_4 = new lib.Symboldottedclock();
        this.Symbolclock_4.setTransform(100, 206, 0.83, 0.83);

        this.Symbolclock_5 = new lib.Symboldotclock();
        this.Symbolclock_5.setTransform(270, 206, 0.83, 0.83);

        this.Symbolclock_6 = new lib.Symboldottedclock();
        this.Symbolclock_6.setTransform(435, 206, 0.83, 0.83);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(280, 101, 1.2, 0.7, 89);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(270.5, 87.1, 1.2, 0.9);
        this.text_dot1 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot1.setTransform(264.2, 99.8);

        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(261, 212, 1.2, 0.7, 419);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(270.5, 192.1, 1.2, 0.9);
        this.text_dot2 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot2.setTransform(264, 205);

        var TextArr = [];
        var TextX = [37, 215, 387, 37, 215, 387];
        var TextY = [155, 155, 155, 260, 260, 260];

        for (var row = 0; row < TextX.length; row++) {

            if (row == 0 || row == 3) {
                var text_1 = new cjs.Text("Klockan var", "16px 'Myriad Pro'", "#000000");
                text_1.setTransform(TextX[row], TextY[row]);
                TextArr.push(text_1);

            } else {
                var text_1 = new cjs.Text("Klockan är", "16px 'Myriad Pro'", "#000000");
                text_1.setTransform(TextX[row], TextY[row]);
                TextArr.push(text_1);
            }

        }

        var lineArr = [];
        var lineX = [120, 290, 462, 120, 290, 462];
        var lineY = [117, 117, 117, 223, 223, 223];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_3 = new cjs.Shape();
            hrLine_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(30, 40);
            hrLine_3.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_3);
        }

        var DotArr = [];
        var DotX = [150, 320, 492, 150, 320, 492];
        var DotY = [155, 155, 155, 260, 260, 260];

        for (var row = 0; row < DotX.length; row++) {

            var text_1 = new cjs.Text(".", "16px 'Myriad Pro'", "#9D9D9C");
            text_1.setTransform(DotX[row], DotY[row]);
            DotArr.push(text_1);
        }
        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.roundRect1, this.hrLine_1, this.hrLine_2,
            this.vrLine_1, this.vrLine_2, this.vrLine_3, this.vrLine_4, this.vrLine_5, this.vrLine_6,
            this.Symbolclock, this.Symbolclock_1, this.Symbolclock_2, this.Symbolclock_3, this.Symbolclock_4, this.Symbolclock_5,
            this.Symbolclock_6,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.text_dot1,this.text_dot2);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }
        for (var i = 0; i < DotArr.length; i++) {
            this.addChild(DotArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 275);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Hur många likadana smycken kan du göra av pärlorna?", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(10, 0);

        this.text_2 = new cjs.Text("st", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(140, 233);

        this.text_3 = new cjs.Text("st", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(390, 233);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 510, 242, 10);
        this.roundRect1.setTransform(10, 12);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 205);
        this.vrLine_1.setTransform(225, 30);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f('').s("#C7DA8F").ss(0.7).drawRoundRect(0, 0, 70, 86, 10);
        this.roundRect2.setTransform(105, 26);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f('').s("#C7DA8F").ss(0.7).drawRoundRect(0, 0, 120, 86, 10);
        this.roundRect3.setTransform(325, 26);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f('').s("#9D9D9C").ss(0.7).drawRoundRect(0, 0, 20, 23, 0);
        this.roundRect4.setTransform(119, 212);

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f('').s("#9D9D9C").ss(0.7).drawRoundRect(0, 0, 20, 23, 0);
        this.roundRect5.setTransform(370, 212);

        this.instance = new lib.p84_1();
        this.instance.setTransform(50, 8, 0.47, 0.47);

        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.roundRect1, this.vrLine_1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5,
            this.instance, this.text_2, this.text_3, this.text_4, this.text_5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 250);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287, 103, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(287, 438, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
