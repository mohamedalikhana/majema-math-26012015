(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p59_2.png",
            id: "p59_2"
        }, {
            src: "images/p59_1.png",
            id: "p59_1"
        }]
    };


    (lib.p59_2 = function() {
        this.initialize(img.p59_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p59_1 = function() {
        this.initialize(img.p59_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("59", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Måla cirkeln om svaret stämmer.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 6);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#88C13F");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 6);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 7; row++) {
                if (row == 0 && column == 0) {
                    this.shape_group1.graphics.f("#fff").s("#000000").ss(0.5, 0, 0, 4).arc(120 + (columnSpace * 180), 105 + (row * 29), 11, 0, 2 * Math.PI);
                } else {
                    this.shape_group1.graphics.f("#fff").s("#000000").ss(0.5, 0, 0, 4).arc(120 + (columnSpace * 180), 105 + (row * 29), 11, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.Line_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 8; row++) {
                this.Line_group1.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(25 + (columnSpace * 180), 90 + (row * 29)).lineTo(150 + (columnSpace * 180), 90 + (row * 29));
            }
        }
        this.Line_group1.setTransform(0, 0);

        var arrVal = ['0', '3', '5'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 2) {
                colSpace = 2.01;
            }

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(80 + (colSpace * 180), 61);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['1 – 1', '1 + 2', '1 + 4'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 98);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['0 – 0', '5 – 3', '3 + 2'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 128);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['3 + 0', '3 – 0', '3 + 1'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 155);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['2 – 2', '4 – 1', '5 – 0'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 185);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['4 – 0', '5 – 2', '5 – 2'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 213);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['5 – 5', '3 + 1', '3 + 2'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 245);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['3 – 3', '4 – 1', '0 + 5'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 273);
            ToBeAdded.push(tempLabel);
        }

        this.instance = new lib.p59_1();
        this.instance.setTransform(1, 25, 0.24, 0.24);

        this.instance1 = new lib.p59_1();
        this.instance1.setTransform(181, 25, 0.24, 0.24);

        this.instance2 = new lib.p59_1();
        this.instance2.setTransform(363, 25, 0.24, 0.24);

        this.Line1 = new cjs.Shape();
        this.Line1.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(25, 90).lineTo(25, 293);
        this.Line1.setTransform(0, 0);

        this.Line2 = new cjs.Shape();
        this.Line2.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(150, 90).lineTo(150, 293);
        this.Line2.setTransform(0, 0);

        this.Line3 = new cjs.Shape();
        this.Line3.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(205, 90).lineTo(205, 293);
        this.Line3.setTransform(0, 0);

        this.Line4 = new cjs.Shape();
        this.Line4.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(330, 90).lineTo(330, 293);
        this.Line4.setTransform(0, 0);

        this.Line5 = new cjs.Shape();
        this.Line5.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(385, 90).lineTo(385, 293);
        this.Line5.setTransform(0, 0);

        this.Line6 = new cjs.Shape();
        this.Line6.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(510, 90).lineTo(510, 293);
        this.Line6.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FFFFFF').drawRoundRect(0, 0, ((2150 * 0.5) - 10) * 0.5, 630 * 0.5, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text, this.text_1, this.shape_group1, this.Line_group1);
        this.addChild(this.instance, this.instance1, this.instance2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

        this.addChild(this.Line1, this.Line2, this.Line3, this.Line4, this.Line5, this.Line6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p59_2();
        this.instance.setTransform(21, 28, 0.55, 0.52);

        this.text = new cjs.Text("Hitta vägen med rätt svar.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 1);

        this.labelR11 = new cjs.Text("5 + 1", "16px 'Myriad Pro'");
        this.labelR11.lineHeight = 19;
        this.labelR11.setTransform(78, 45);
        this.labelR12 = new cjs.Text("3 + 3", "16px 'Myriad Pro'");
        this.labelR12.lineHeight = 19;
        this.labelR12.setTransform(78, 85);
        this.labelR13 = new cjs.Text("1 + 1", "16px 'Myriad Pro'");
        this.labelR13.lineHeight = 19;
        this.labelR13.setTransform(78, 127);
        this.labelR14 = new cjs.Text("2 – 0", "16px 'Myriad Pro'");
        this.labelR14.lineHeight = 19;
        this.labelR14.setTransform(78, 170);
        this.labelR15 = new cjs.Text("6 – 2", "16px 'Myriad Pro'");
        this.labelR15.lineHeight = 19;
        this.labelR15.setTransform(78, 211);

        this.labelR21 = new cjs.Text("6 – 1", "16px 'Myriad Pro'");
        this.labelR21.lineHeight = 19;
        this.labelR21.setTransform(162, 45);
        this.labelR22 = new cjs.Text("6 + 0", "16px 'Myriad Pro'");
        this.labelR22.lineHeight = 19;
        this.labelR22.setTransform(162, 85);
        this.labelR23 = new cjs.Text("4 – 2", "16px 'Myriad Pro'");
        this.labelR23.lineHeight = 19;
        this.labelR23.setTransform(162, 127);
        this.labelR24 = new cjs.Text("6 – 1", "16px 'Myriad Pro'");
        this.labelR24.lineHeight = 19;
        this.labelR24.setTransform(162, 170);
        this.labelR25 = new cjs.Text("4 + 2", "16px 'Myriad Pro'");
        this.labelR25.lineHeight = 19;
        this.labelR25.setTransform(162, 211);

        this.labelR31 = new cjs.Text("2 + 4", "16px 'Myriad Pro'");
        this.labelR31.lineHeight = 19;
        this.labelR31.setTransform(248, 45);
        this.labelR32 = new cjs.Text("4 + 2", "16px 'Myriad Pro'");
        this.labelR32.lineHeight = 19;
        this.labelR32.setTransform(248, 85);
        this.labelR33 = new cjs.Text("5 – 3", "16px 'Myriad Pro'");
        this.labelR33.lineHeight = 19;
        this.labelR33.setTransform(248, 127);
        this.labelR34 = new cjs.Text("6 – 2", "16px 'Myriad Pro'");
        this.labelR34.lineHeight = 19;
        this.labelR34.setTransform(248, 170);
        this.labelR35 = new cjs.Text("5 – 3", "16px 'Myriad Pro'");
        this.labelR35.lineHeight = 19;
        this.labelR35.setTransform(248, 211);

        this.labelR41 = new cjs.Text("1 + 5", "16px 'Myriad Pro'");
        this.labelR41.lineHeight = 19;
        this.labelR41.setTransform(331, 45);
        this.labelR42 = new cjs.Text("6 – 2", "16px 'Myriad Pro'");
        this.labelR42.lineHeight = 19;
        this.labelR42.setTransform(331, 85);
        this.labelR43 = new cjs.Text("6 – 4", "16px 'Myriad Pro'");
        this.labelR43.lineHeight = 19;
        this.labelR43.setTransform(331, 127);
        this.labelR44 = new cjs.Text("3 – 1", "16px 'Myriad Pro'");
        this.labelR44.lineHeight = 19;
        this.labelR44.setTransform(331, 170);
        this.labelR45 = new cjs.Text("2 + 0", "16px 'Myriad Pro'");
        this.labelR45.lineHeight = 19;
        this.labelR45.setTransform(331, 211);

        this.labelR51 = new cjs.Text("0 + 6", "16px 'Myriad Pro'");
        this.labelR51.lineHeight = 19;
        this.labelR51.setTransform(422, 45);
        this.labelR52 = new cjs.Text("3 + 3", "16px 'Myriad Pro'");
        this.labelR52.lineHeight = 19;
        this.labelR52.setTransform(422, 85);
        this.labelR53 = new cjs.Text("2 + 4", "16px 'Myriad Pro'");
        this.labelR53.lineHeight = 19;
        this.labelR53.setTransform(422, 127);
        this.labelR54 = new cjs.Text("1 + 5", "16px 'Myriad Pro'");
        this.labelR54.lineHeight = 19;
        this.labelR54.setTransform(422, 170);
        this.labelR55 = new cjs.Text("5 + 1", "16px 'Myriad Pro'");
        this.labelR55.lineHeight = 19;
        this.labelR55.setTransform(422, 211);

        this.addChild(this.instance, this.text, this.text_1, this.labelR11, this.labelR12, this.labelR13, this.labelR14,
            this.labelR15, this.labelR21, this.labelR22, this.labelR23, this.labelR24, this.labelR25, this.labelR31, this.labelR32,
            this.labelR33, this.labelR34, this.labelR35, this.labelR41, this.labelR42, this.labelR43, this.labelR44, this.labelR45,
            this.labelR51, this.labelR52, this.labelR53, this.labelR54, this.labelR55, this.img_Rect1, this.line_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500.3, 320);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(289.3, 401, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287.3, 99, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
