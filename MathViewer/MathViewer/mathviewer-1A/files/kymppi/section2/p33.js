(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p33_1.png",
            id: "img1"
        }, {
            src: "images/p33_2.png",
            id: "img2"
        }, {
            src: "images/p33_3.png",
            id: "img3"
        }, {
            src: "images/p33_4.png",
            id: "img4"
        }, {
            src: "images/p33_5.png",
            id: "img5"
        }, {
            src: "images/p33_6.png",
            id: "img6"
        }]
    };

    (lib.img1 = function() {
        this.initialize(img.img1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.img2 = function() {
        this.initialize(img.img2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.img3 = function() {
        this.initialize(img.img3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.img4 = function() {
        this.initialize(img.img4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.img5 = function() {
        this.initialize(img.img5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.img6 = function() {
        this.initialize(img.img6);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("33", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 24;
        this.text_4.setTransform(555, 652);


        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);


        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 20).lineTo(0, 20).moveTo(590, 30).lineTo(590, 660).moveTo(590, 660).lineTo(0, 660);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(580, 30, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(590, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);


        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 400);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Här är hälften så många         som           .", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(70, 5);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(10, 35, 510, 130, 10);
        this.round_Rect1.setTransform(0, 0);

        this.instance_img1 = new lib.img1();
        this.instance_img1.setTransform(10, -27, 0.3, 0.3);
        this.instance_img2 = new lib.img2();
        this.instance_img2.setTransform(231, -17, 0.2, 0.2);
        this.instance_img3 = new lib.img3();
        this.instance_img3.setTransform(289, -6, 0.2, 0.2);
        this.instance_img4 = new lib.img6();
        this.instance_img4.setTransform(62, 55, 0.2, 0.2);

        this.addChild(this.text_1, this.round_Rect1, this.instance_img1, this.instance_img2, this.instance_img3,
            this.instance_img4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 523.3, 185);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Rita hälften så många                 som                .", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(10, 21);

        this.text_2 = new cjs.Text("Rita på 2 olika sätt.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(10, 42);

        this.text_3 = new cjs.Text("Visa och berätta för en kamrat.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(10, 346);

        this.instance_img4 = new lib.img4();
        this.instance_img4.setTransform(160, 0, 0.2, 0.2);
        this.instance_img5 = new lib.img5();
        this.instance_img5.setTransform(246, 0, 0.2, 0.2);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(10, 30, 510, 130, 10);
        this.round_Rect1.setTransform(0, 36);

        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(10, 30, 510, 130, 10);
        this.round_Rect2.setTransform(0, 176);

        this.addChild(this.text_1, this.text_2, this.instance_img4, this.instance_img5, this.round_Rect1, this.round_Rect2,
            this.text_3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 365);



    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296.8, 260, 1, 1, 0, 0, 0, 256.3, 173.6);
        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296.8, 165, 1, 1, 0, 0, 0, 256.3, -110);


        this.addChild(this.v2, this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
