(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p102_2.png",
            id: "p102_2"
        }]
    };

    (lib.p102_2 = function() {
        this.initialize(img.p102_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("103", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(555, 655);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);

        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.SymbolRect = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 199, 125, 5);
        this.round_Rect1.setTransform(0, 5);

        this.addChild(this.round_Rect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p102_2();
        this.instance.setTransform(23, 5, 0.47, 0.47);

        this.round_Rect1 = new lib.SymbolRect();
        this.round_Rect1.setTransform(27, 50);

        this.round_Rect2 = new lib.SymbolRect();
        this.round_Rect2.setTransform(27, 188);

        this.round_Rect3 = new lib.SymbolRect();
        this.round_Rect3.setTransform(27, 326);

        this.round_Rect4 = new lib.SymbolRect();
        this.round_Rect4.setTransform(27, 464);

        var TextArr = [];
        var TArr = ['Måttenhet:', 'Uppskattning:', 'Mätning:', 'Måttenhet:', 'Uppskattning:', 'Mätning:',
            'Måttenhet:', 'Uppskattning:', 'Mätning:', 'Måttenhet:', 'Uppskattning:', 'Mätning:'
        ];
        var TextX = [240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240];
        var TextY = [91, 126, 161, 229, 264, 299, 367, 402, 437, 505, 540, 575];
        for (var i = 0; i < TextX.length; i++) {

            var text_4 = new cjs.Text(TArr[i], "16px 'Myriad Pro'", "#000000");
            text_4.setTransform(TextX[i], TextY[i]);
            TextArr.push(text_4);
        }

        var hrlineArr = [];
        var hrlineX = [317, 338, 303, 317, 338, 303, 317, 338, 303, 317, 338, 303];
        var hrlineY = [71, 106, 140, 209, 244, 279, 347, 382, 417, 485, 519, 554];

        for (var i = 0; i < hrlineX.length; i++) {
            var hrLine_1 = new cjs.Shape();
            if (i == 0 || i == 3 || i == 6 | i == 9) {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(155, 20);
            } else if (i == 1 || i == 4 || i == 7 || i == 10) {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(133, 20);
            } else {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(169, 20);
            }

            hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
            hrlineArr.push(hrLine_1);
        }

        this.addChild(this.instance, this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 580);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(283, 212, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
