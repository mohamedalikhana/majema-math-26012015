(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p44_1.png",
            id: "p44_1"
        }, {
            src: "images/p44_2.png",
            id: "p44_2"
        }]
    };

    // symbols:
    (lib.p44_1 = function() {
        this.initialize(img.p44_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p44_2 = function() {
        this.initialize(img.p44_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol1 = function() {
        this.initialize();

        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(457 + (i * 32), 35, 27, 27);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageNoLeft = new cjs.Text("44", "13px 'Myriad Pro'", "#FFFFFF");
        this.pageNoLeft.lineHeight = 18;
        this.pageNoLeft.setTransform(38, 648);

        this.pageNoLeftBackground = new cjs.Shape();
        this.pageNoLeftBackground.graphics.f("#7AC729").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.pageNoLeftBackground.setTransform(31.1, 660.8);

        this.pageTitle = new cjs.Text("Vi skriver addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 19);

        this.chapterNumber = new cjs.Text("13", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(50.7, 15.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);
        this.instance = new lib.p44_1();
        this.instance.setTransform(40, 25, 0.425, 0.425);

        this.addChild(this.instance, this.pageNoLeftBackground, this.pageNoLeft, this.top_textbox_group);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();
        this.instance = new lib.p44_2();
        this.instance.setTransform(-27, 20, 0.75, 0.75);
        this.text = new cjs.Text("Hur många är det tillsammans?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(1, 6);
        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(-19, 6);

        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(53 + (i * 20), 105, 20, 21);
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(320 + (i * 20), 105, 20, 21);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(320 + (i * 20), 215, 20, 21);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(53 + (i * 20), 215, 20, 21);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(-18, 31, 253, 102, 10);
        this.img_Rect1.setTransform(0, 0);

        this.img_Rect2 = new cjs.Shape();
        this.img_Rect2.graphics.s("#D0CECE").ss(1).drawRoundRect(241, 31, 253, 102, 10);
        this.img_Rect2.setTransform(0, 0);

        this.img_Rect3 = new cjs.Shape();
        this.img_Rect3.graphics.s("#D0CECE").ss(1).drawRoundRect(-18, 140, 253, 102, 10);
        this.img_Rect3.setTransform(0, 0);

        this.img_Rect4 = new cjs.Shape();
        this.img_Rect4.graphics.s("#D0CECE").ss(1).drawRoundRect(241, 140, 253, 102, 10);
        this.img_Rect4.setTransform(0, 0);

        this.addChild(this.instance, this.textBoxGroup1, this.textBoxGroup2, this.textBoxGroup3, this.textBoxGroup4,
            this.textBoxGroup5, this.textBoxGroup6, this.text, this.text_1, this.img_Rect1, this.img_Rect2, this.img_Rect3,
            this.img_Rect4);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 470, 300);


    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text("Addera.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(14, 11);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(-6, 11);
        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(-5, 35, 510, 135, 10);
        this.img_Rect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(100 + (columnSpace * 162), 45 + (row * 30), 20, 21);
            }
        }
        this.textbox_group1.setTransform(0, 0);


        this.label1 = new cjs.Text("1  +  0  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(34, 48);
        this.label2 = new cjs.Text("2  +  0  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(34, 78);
        this.label3 = new cjs.Text("3  +  0  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(34, 108);
        this.label4 = new cjs.Text("4  +  0  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(34, 138);


        this.label6 = new cjs.Text("5  +  0  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(197, 48);
        this.label7 = new cjs.Text("0  +  5  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(197, 78);
        this.label8 = new cjs.Text("1  +  2  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(197, 108);
        this.label9 = new cjs.Text("1  +  3  =  ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(197, 138);


        this.label11 = new cjs.Text("1  +  4  =  ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(357, 48);
        this.label12 = new cjs.Text("2  +  2  =  ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(357, 78);
        this.label13 = new cjs.Text("3  +  2  =  ", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(357, 108);
        this.label14 = new cjs.Text("2  +  3  =  ", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(357, 138);


        this.addChild(this.text_1, this.text, this.text_2, this.img_Rect1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(20, 10, 470, 300);



    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(315, 262, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(316, 505, 1, 1, 0, 0, 0, 270, 38);

        this.addChild(this.v2, this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
