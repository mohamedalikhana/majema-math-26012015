(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };


    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_2 = new cjs.Text("123", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(555, 656);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);


        this.addChild(this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();
        this.text = new cjs.Text("Måla.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 15);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 15);

        this.shape1 = new cjs.Shape();
        this.shape1.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 170, 10);
        this.shape1.setTransform(0, 75);

        var ToBeAdded = [];
        var filcolor = ""
        tmptxt = ["50-69", "70-79", "80-89", "90-100"]
        for (var col = 0; col < 4; col++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(60, 40).lineTo(0, 40).lineTo(0, 58).lineTo(60, 58).lineTo(60, 40);
            hrLine.setTransform(0 + (col * 115), 0)
            if (col == 0) {
                filcolor = "#008BD2"
            } else if (col == 1) {
                filcolor = "#D51317"
            } else if (col == 2) {
                filcolor = "#FFF374"
            } else if (col == 3) {
                filcolor = "#13A538"
            }
            var hrLineArrow = new cjs.Shape();
            hrLineArrow.graphics.f(filcolor).beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(60, 40).lineTo(70, 40).lineTo(80, 49).lineTo(70, 58).lineTo(60, 58);
            hrLineArrow.setTransform(0 + (col * 115), 0)
            var tempText = new cjs.Text(tmptxt[col], "16px 'Myriad Pro'");
            tempText.setTransform(10 + (col * 114), 54);
            ToBeAdded.push(hrLine, hrLineArrow, tempText)
        }

        //draw circle
        var arryNum = [82, 85, 99, 55, 81, 88, 99, 67, 80, 87, 100, 68, 71, 75, 65, 91, 95, 60, 72, 78, 59, 93, 100, 61, 84, 62, 67, 97, 87, 55, 65, 99, 81, 52, 62, 92]
        this.shape_group1 = new cjs.Shape();
        var i = 0;
        var xpos = 36
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 12; column++) {
                var columnSpace = column;
                this.shape_group1.graphics.s("#6E6E70").ss(0.8, 0, 0, 6).arc(40 + (columnSpace * 37.5), 110 + (row * 48), 18, 0, 2 * Math.PI);
                if (column == 10 && (row == 0 || row == 1)) {
                    xpos = 31
                } else {
                    xpos = 36
                }
                var tempText = new cjs.Text(arryNum[i], "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (column * 37.5), 116.5 + (row * 48));
                ToBeAdded.push(tempText)
                i++
            }
        }


        this.shape_group1.setTransform(5, 0);

        this.addChild(this.shape1, this.text, this.text_1, this.shape_group1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 537, 248.5);



    //Part 4
    (lib.Symbol3 = function() {
        this.initialize();

        // this.instance = new lib.p71_1();
        // this.instance.setTransform(15, 27, 0.38, 0.35);

        this.text = new cjs.Text("Ringa in talen som passar.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 15);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#00A3C4");;
        this.text_1.setTransform(0, 15);

        this.shape1 = new cjs.Shape();
        this.shape1.graphics.s("#959C9D").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 512, 285, 10);
        this.shape1.setTransform(0, 25);

        var ToBeAdded = [];
        var lineYpos = 60;
        for (var row = 0; row < 7; row++) {
            var hrLine = new cjs.Shape();
            if (row > 4) {
                lineYpos = 85
            } else {
                lineYpos = 60
            }
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(0, lineYpos + (row * 32)).lineTo(512, lineYpos + (row * 32));
            ToBeAdded.push(hrLine)
        };

        var tmptxt = ["Det är 7 ental.", "27", " 7", "73", "37", "70", "87",
            "Det är 7 tiotal.", "17", "72", "27", "67", "76", "71",
            "Det är 9 ental.", "19", "91", "39", "93", "90", " 9",
            "Det är inga tiotal.", " 9", "10", " 4", "55", " 7", "70",
            "Det är inga ental.", " 8", "80", "30", "13", "100", "10",
            "Det är dubbelt så många", "16", "36", "42", "48", "66", "93",
            "Det är lika många ental som tiotal.", "11", "32", "44", "58", "85", "100",
            "Det är 2 fler tiotal än ental.", "23", "31", "49", "64", "77", "86"
        ]
        var i = 0;
        var xpos = 10
        var ypos = 47
        for (var row = 0; row < 8; row++) {
            for (var col = 0; col < 7; col++) {
                if (col > 0) {
                    xpos = 220
                } else {
                    xpos = 10
                }
                if (row > 5) {
                    ypos = 62
                } else if (col > 0 && row == 5) {
                    ypos = 50
                } else if (col == 0 && row == 5) {
                    ypos = 42
                } else {
                    ypos = 47
                }
                var tempText = new cjs.Text(tmptxt[i], "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 43), ypos + (row * 34));
                ToBeAdded.push(tempText);
                i++
            };
        }

        this.text_2 = new cjs.Text("ental som tiotal.", "16px 'Myriad Pro'");;
        this.text_2.setTransform(10, 230);

        this.vrLine = new cjs.Shape();
        this.vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(247,25).lineTo(247,310);
        this.vrLine.setTransform(0, 0);


        this.addChild(this.shape1, this.text, this.instance, this.text_1, this.text_2,this.vrLine);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 515.3, 310.6);





    // stage content:
    (lib.p71 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(311.8, 365, 1, 1, 0, 0, 0, 256.3, 38.8);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(312.4, 110, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
