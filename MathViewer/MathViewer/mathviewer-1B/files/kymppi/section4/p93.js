(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p93_1.png",
            id: "p93_1"
        }]
    };
    (lib.p93_1 = function() {
        this.initialize(img.p93_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("kunna mäta längd i centimeter", "9px 'Myriad Pro'");
        this.text_1.setTransform(415, 659);

        this.text_2 = new cjs.Text("93", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(555, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Mäta längd i centimeter", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_3.setTransform(110, 42);

        this.text_4 = new cjs.Text("32", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(63, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(49, 23.5, 1.15, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("1 centimeter", "bold 16px 'Myriad Pro'", "#000000");
        this.text.setTransform(47, 54);

        this.text_1 = new cjs.Text("förkortas", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(47, 77);

        this.text_2 = new cjs.Text("1 cm.", "bold 16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(110, 77);

        this.text_3 = new cjs.Text("Börja alltid från 0.", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(265, 54);

        this.text_4 = new cjs.Text("Pennan är 5 cm lång.", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(300, 175);

        // Main yellow block
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 0, 520, 196, 15);
        this.roundRect1.setTransform(10, 1);

        // Main white block left
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(0, 0, 184, 167, 7);
        this.roundRect2.setTransform(32, 20);

        // Main white block right
        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(0, 0, 259, 167, 7);
        this.roundRect3.setTransform(250, 20);

        // for left rect

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_1.setTransform(44, 21);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_2.setTransform(59, 21);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_3.setTransform(74.8, 21);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_4.setTransform(90.6, 21);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_5.setTransform(106.4, 21);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_6.setTransform(122.2, 21);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_7.setTransform(138, 21);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_8.setTransform(153.8, 21);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_9.setTransform(169.6, 21);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_28.setTransform(185.4, 21);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_29.setTransform(201.2, 21);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_10.setTransform(45, 26.5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_11.setTransform(45, 26.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_12.setTransform(60, 26.5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_13.setTransform(60, 26.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_14.setTransform(76, 26.5);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_15.setTransform(76, 26.5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_16.setTransform(91.5, 26.5);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_17.setTransform(91.5, 26.5);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_18.setTransform(107, 26.5);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_19.setTransform(107, 26.5);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_20.setTransform(123, 26.5);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_21.setTransform(123, 26.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_22.setTransform(139, 26.5);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_23.setTransform(139, 26.5);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_24.setTransform(155, 26.5);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_25.setTransform(155, 26.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_26.setTransform(171, 26.5);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_27.setTransform(171, 26.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_30.setTransform(187, 26.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_31.setTransform(187, 26.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_32.setTransform(203, 26.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_33.setTransform(203, 26.5);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.text, this.text_1, this.text_2, this.text_3, this.text_4,
            this.shape_10, this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_19, this.shape_20,
            this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.shape_25, this.shape_26, this.shape_27, this.shape_30, this.shape_31, this.shape_32, this.shape_33,
            this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_28, this.shape_29);



        // for right rect

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(261, 21);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(276.5, 21);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_43.setTransform(292, 21);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(307.5, 21);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(323, 21);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_46.setTransform(338.5, 21);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_47.setTransform(354, 21);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_48.setTransform(369.5, 21);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_49.setTransform(385, 21);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_50.setTransform(401.5, 21);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_51.setTransform(416, 21);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(1.5, 0, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_52.setTransform(431.5, 21);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_53.setTransform(447, 21);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_54.setTransform(462.5, 21);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(1.5, 0, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_55.setTransform(478, 21);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_56.setTransform(494, 21);

        //dots

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_62.setTransform(262, 26.5);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_63.setTransform(262, 26.5);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_64.setTransform(277.5, 26.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_65.setTransform(277.5, 26.5);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_66.setTransform(293, 26.5);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_67.setTransform(293, 26.5);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_68.setTransform(308.5, 26.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_69.setTransform(308.5, 26.5);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_70.setTransform(324, 26.5);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_71.setTransform(324, 26.5);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_72.setTransform(339.5, 26.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_73.setTransform(339.5, 26.5);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_74.setTransform(355, 26.5);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_75.setTransform(355, 26.5);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_76.setTransform(370.5, 26.5);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_77.setTransform(370.5, 26.5);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_78.setTransform(386, 26.5);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_79.setTransform(386, 26.5);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_80.setTransform(402.5, 26.5);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_81.setTransform(402.5, 26.5);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_82.setTransform(417, 26.5);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_83.setTransform(417, 26.5);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_84.setTransform(432.5, 26.5);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_85.setTransform(432.5, 26.5);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_86.setTransform(448, 26.5);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_87.setTransform(448, 26.5);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(463.5, 26.5);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(463.5, 26.5);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_90.setTransform(479, 26.5);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_91.setTransform(479, 26.5);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_92.setTransform(495, 26.5);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_93.setTransform(495, 26.5);

        this.addChild(this.shape_62, this.shape_63, this.shape_64, this.shape_65, this.shape_66, this.shape_67, this.shape_68, this.shape_69, this.shape_70,
            this.shape_71, this.shape_72, this.shape_73, this.shape_74, this.shape_75, this.shape_76, this.shape_77, this.shape_78, this.shape_79, this.shape_80,
            this.shape_81, this.shape_82, this.shape_83, this.shape_84, this.shape_85, this.shape_86, this.shape_87, this.shape_88, this.shape_89, this.shape_90,
            this.shape_91, this.shape_92, this.shape_93,
            this.shape_41, this.shape_42, this.shape_43, this.shape_44, this.shape_45, this.shape_46, this.shape_47, this.shape_48, this.shape_49, this.shape_50,
            this.shape_51, this.shape_52, this.shape_53, this.shape_54, this.shape_55, this.shape_56);

        var scaleLength = 4.1;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = true;
        var bigScale1 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale1.setTransform(50, 110, 0.27, 0.36);

        var scaleLength = 6;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = true;
        var bigScale2 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale2.setTransform(285, 110, 0.27, 0.36);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 20).lineTo(26, 20);
        this.hrLine_2.setTransform(57.5, 79);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(57.5, 95).lineTo(57.5, 103).moveTo(84, 95).lineTo(84, 103);

        this.instance = new lib.p93_1();
        this.instance.setTransform(289, 83, 0.45, 0.47);

        var dotlineX = [272, 407];
        this.dotline = new cjs.Shape();
        for (var col = 0; col < dotlineX.length; col++) {
            var xPos = dotlineX[col];
            for (var i = 0; i < 4; i++) {
                this.dotline.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(xPos, 0 + (i * 8)).lineTo(xPos, 5 + (i * 8));

            }
        }
        this.dotline.setTransform(20, 80);

        this.addChild(bigScale1, bigScale2, this.hrLine_2, this.vrLine_1, this.instance, this.dotline);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 537, 143.6);

    (lib.drawScale = function(scaleLength, paddingLeft, paddingRight, cropRight) {
        this.initialize();
        var scaleHeight = 126.8;

        var corners = {
            TL: 10,
            TR: 10,
            BR: 10,
            BL: 10
        };

        if (cropRight) {
            corners.TR = 0;
            corners.BR = 0;
            paddingRight = 10;
        }
        // this.bigscale = new cjs.Container();
        this.metricscale = new cjs.Shape();
        this.metricscale.graphics.f("#E2F3FD").s("#959C9D").ss(1).drawRoundRectComplex(0, 0, paddingLeft + (scaleLength * 100) + paddingRight, scaleHeight, corners.TL, corners.TR, corners.BR, corners.BL);
        if (cropRight) {
            this.metricscale.graphics.s('#ffffff').drawRect(paddingLeft + (scaleLength * 100) + paddingRight - 1, 1, 1 * 2, scaleHeight - 1 * 2);
        }
        this.metricscale.setTransform(0, 0);
        this.addChild(this.metricscale);

        for (var i = 0; i <= scaleLength * 10; i++) {
            var height = 31.7;
            if (i % 5 == 0) {
                height = 63.4;
            }

            var round = new cjs.Shape();
            round.graphics.f("#000000").s('#7d7d7d').drawRect(0, 0, 1, height);
            round.setTransform(25 + i * 10, 0);
            this.addChild(round, text);
            if (i % 10 == 0) {
                var ispace = 16;
                var number = i / 10;
                if (number > 9) {
                    ispace = 8;
                }
                var text = new cjs.Text(number + "", "36px 'Myriad Pro'", "#000000");
                text.setTransform(ispace + i * 10, height + (height * 50 / 100));
                this.addChild(text);
            }
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 320.3, 143.6);

    //Part 1
    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Mät med linjal och skriv längden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 520, 345, 10);
        this.roundRect1.setTransform(10, 20);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#D51317").s('#000000').drawRoundRect(0, 0, 85, 15, 0);
        this.Rect1.setTransform(39, 41);

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("#008BD2").s('#000000').drawRoundRect(0, 0, 139, 15, 0);
        this.Rect2.setTransform(272, 41);

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("#8F96CB").s('#000000').drawRoundRect(0, 0, 197, 15, 0);
        this.Rect3.setTransform(26, 145);

        this.Rect4 = new cjs.Shape();
        this.Rect4.graphics.f("#EB5B1B").s('#000000').drawRoundRect(0, 0, 112, 15, 0);
        this.Rect4.setTransform(322, 145);

        this.Rect5 = new cjs.Shape();
        this.Rect5.graphics.f("#B3B2B2").s('#000000').drawRoundRect(0, 0, 57, 15, 0);
        this.Rect5.setTransform(38, 209);

        this.Rect6 = new cjs.Shape();
        this.Rect6.graphics.f("#00A3C4").s('#000000').drawRoundRect(0, 0, 168, 15, 0);
        this.Rect6.setTransform(240, 209);

        this.Rect7 = new cjs.Shape();
        this.Rect7.graphics.f("#90BD22").s('#000000').drawRoundRect(0, 0, 422, 15, 0);
        this.Rect7.setTransform(24, 268);

        this.Rect8 = new cjs.Shape();
        this.Rect8.graphics.f("#D6326A").s('#000000').drawRoundRect(0, 0, 281, 15, 0);
        this.Rect8.setTransform(62, 325);

        var hrlineArr = [];
        var hrlineX = [129, 416, 228, 440, 100, 413, 451, 348];
        var hrlineY = [36, 36, 140, 140, 204, 204, 263, 320];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 20).lineTo(32, 20);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        var TextArr = [];
        var TextX = [165, 452, 264, 476, 136, 449, 487, 384];
        var TextY = [55, 55, 159, 159, 223, 223, 282, 339];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("cm", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.Rect1, this.Rect2, this.Rect3, this.Rect4, this.Rect5, this.Rect6, this.Rect7, this.Rect8)
        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

        var scaleLength = 4.1;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = true;
        var bigScale1 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale1.setTransform(33, 64, 0.27, 0.36);

        var scaleLength = 6;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = true;
        var bigScale2 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale2.setTransform(266, 64, 0.27, 0.36);

        this.addChild(bigScale1, bigScale2)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 527, 370);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(295, 108, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295, 327, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
