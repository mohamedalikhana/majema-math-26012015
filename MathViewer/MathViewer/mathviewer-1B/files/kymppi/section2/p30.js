(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p30_1.png",
            id: "p30_1"
        }]
    };

    (lib.p30_1 = function() {
        this.initialize(img.p30_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);  

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("30", "13px 'Myriad Pro'", "#FFFFFF");        
        this.text.setTransform(32, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30.1, 660.8);      

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(456 + (columnSpace * 32), 13, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många pennor är det tillsammans?", "16px 'Myriad Pro'");       
        this.text_q2.setTransform(20, 4);

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#F1662B");        
        this.text.setTransform(0, 4);        

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 110, 10);
        this.roundRect1.setTransform(0, 8);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 8);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 122);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 122);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 236);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 236);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 350);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(260, 350);

        this.roundRect9 = this.roundRect1.clone(true);
        this.roundRect9.setTransform(0, 464);

        this.roundRect10 = this.roundRect1.clone(true);
        this.roundRect10.setTransform(260, 464);

        this.instance = new lib.p30_1();
        this.instance.setTransform(10, 27, 0.47, 0.47);
         
         var ToBeAdded = [];
         var Num1=7;  
         var Num2=1;     
         var i=0;
         var j=0;
         var k=0;
         var l=0;
         this.hrLine_1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 5; row++) {
                var rowSpace = row;
                if (row<2)
                {
                    i=2;
                    if(j==0)
                        {j=3;}             
                   
                    else
                        {j++}
                    var tempText = new cjs.Text((Num1+i)+ " + "+ Num2 +" + " +j+ " = ", "16px 'Myriad Pro'");
                    tempText.setTransform(73 + (259 * colSpace), 109 + (112 * rowSpace));

                     var tempText1 = new cjs.Text("9", "14px 'Myriad Pro'");
                     tempText1.setTransform(40+(259 * colSpace), 47+ (112 * rowSpace));

                     var tempText2 = new cjs.Text("pennor", "14px 'Myriad Pro'");
                     tempText2.setTransform(22+(259 * colSpace), 58+ (112 * rowSpace),.9,.9);

                } 
                
                if (row>1 && row!=4)
                {
                    //num3=1
                    i=1;
                    if(k==0)
                        {k=2;}
                    else
                        {k++}
                    
                     var tempText = new cjs.Text((Num1+i)+ " + "+ (Num2+1) +" + " +k+ " = ", "16px 'Myriad Pro'");
                    tempText.setTransform(73 + (259 * colSpace), 114 + (112 * rowSpace));

                    tempText1 = new cjs.Text("8", "14px 'Myriad Pro'");
                    tempText1.setTransform(40+(259 * colSpace), 47+ (114 * rowSpace));

                    tempText2 = new cjs.Text("pennor", "14px 'Myriad Pro'");
                    tempText2.setTransform(22+(259 * colSpace), 58+ (114 * rowSpace),.9,.9);
                    
                } 
                if (row==4)
                {
                    if(l==0)
                        {l=2;}
                    else
                        {l=l+2}
                    
                     var tempText = new cjs.Text(Num1+ " + "+ (Num2+2) +" + " +l+ " = ", "16px 'Myriad Pro'");
                    tempText.setTransform(73 + (259 * colSpace), 118 + (112 * rowSpace));

                    tempText1 = new cjs.Text("7", "14px 'Myriad Pro'");
                    tempText1.setTransform(40+(259 * colSpace), 47+ (114 * rowSpace));

                    tempText2 = new cjs.Text("pennor", "14px 'Myriad Pro'");
                    tempText2.setTransform(22+(259 * colSpace), 58+ (114 * rowSpace),.9,.9);
                }

                
                tempText2.skewX=5;
                tempText2.skewY=5;
                    
                    if (row==4)
                    {
                         this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(145 + (259 * colSpace), 120+ (113 * rowSpace)).lineTo(186 + (259 * colSpace), 120 + (113 * rowSpace));
                    }
                    else
                    {
                        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(145 + (259 * colSpace), 116+ (113 * rowSpace)).lineTo(186 + (259 * colSpace), 116 + (113 * rowSpace));
                    }
                   this.hrLine_1.setTransform(0, 0);
                   ToBeAdded.push(tempText,tempText2,tempText1, this.hrLine_1,this.label1,this.label2);  

            }
        }
         

        var arrTxtbox = ['109', '227', '345'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding = 40,
            yPos,
            rectWidth = 110,
            rectHeight = 23,
            boxWidth = 22,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.roundRect7,this.roundRect8,this.roundRect9,this.roundRect10,this.instance);
        
         for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 550);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(610.5, 344, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(295, 275, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
