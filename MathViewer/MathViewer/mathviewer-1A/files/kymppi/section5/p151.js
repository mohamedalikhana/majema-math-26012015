(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p151_1.png",
            id: "p151_1"
        }, {
            src: "images/p151_2.png",
            id: "p151_2"
        }]
    };

    // symbols:
    (lib.p151_1 = function() {
        this.initialize(img.p151_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p151_2 = function() {
        this.initialize(img.p151_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1       

        this.text = new cjs.Text("151", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(551, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Lista ut meddelandet.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 4);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#D83770");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 510, 300, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var fcolor = (row == 3) ? "#F5D5DA" : "#ffffff";

                this.textbox_group1.graphics.f(fcolor).s("#D83770").ss(0.8).drawRect(55 + (columnSpace * 28), 56.5 + (rowSpace * 28), 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;

                this.textbox_group2.graphics.f('#F5D5DA').s("#D83770").ss(0.8).drawRect(21 + (columnSpace * 28), 56.5 + (rowSpace * 28), 34, 28);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        //row-1
        this.label1 = new cjs.Text("H", "16px 'Myriad Pro'");
        this.label1.lineHeight = 25;
        this.label1.setTransform(62, 64);
        this.label2 = new cjs.Text("Y", "16px 'Myriad Pro'");
        this.label2.lineHeight = 25;
        this.label2.setTransform(90, 64);
        this.label3 = new cjs.Text("O", "16px 'Myriad Pro'");
        this.label3.lineHeight = 25;
        this.label3.setTransform(118, 64);
        this.label4 = new cjs.Text("T", "16px 'Myriad Pro'");
        this.label4.lineHeight = 25;
        this.label4.setTransform(147, 64);
        this.label5 = new cjs.Text("S", "16px 'Myriad Pro'");
        this.label5.lineHeight = 25;
        this.label5.setTransform(176, 64);
        //row-2
        this.label6 = new cjs.Text("L", "16px 'Myriad Pro'");
        this.label6.lineHeight = 25;
        this.label6.setTransform(62, 91);
        this.label7 = new cjs.Text("A", "16px 'Myriad Pro'");
        this.label7.lineHeight = 25;
        this.label7.setTransform(90, 91);
        this.label8 = new cjs.Text("M", "16px 'Myriad Pro'");
        this.label8.lineHeight = 25;
        this.label8.setTransform(118, 91);
        this.label9 = new cjs.Text("I", "16px 'Myriad Pro'");
        this.label9.lineHeight = 25;
        this.label9.setTransform(147, 91);
        this.label10 = new cjs.Text("E", "16px 'Myriad Pro'");
        this.label10.lineHeight = 25;
        this.label10.setTransform(176, 91);
        //row-3
        this.label11 = new cjs.Text("D", "16px 'Myriad Pro'");
        this.label11.lineHeight = 25;
        this.label11.setTransform(62, 119);
        this.label12 = new cjs.Text("P", "16px 'Myriad Pro'");
        this.label12.lineHeight = 25;
        this.label12.setTransform(90, 119);
        this.label13 = new cjs.Text("N", "16px 'Myriad Pro'");
        this.label13.lineHeight = 25;
        this.label13.setTransform(118, 119);
        this.label14 = new cjs.Text("R", "16px 'Myriad Pro'");
        this.label14.lineHeight = 25;
        this.label14.setTransform(147, 119);
        this.label15 = new cjs.Text("Ö", "16px 'Myriad Pro'");
        this.label15.lineHeight = 25;
        this.label15.setTransform(176, 119);
        //row-4
        this.label16 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.label16.lineHeight = 25;
        this.label16.setTransform(62, 147.1);
        this.label17 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.label17.lineHeight = 25;
        this.label17.setTransform(90, 147.1);
        this.label18 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label18.lineHeight = 25;
        this.label18.setTransform(118, 147.1);
        this.label19 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label19.lineHeight = 25;
        this.label19.setTransform(147, 147.1);
        this.label20 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.label20.lineHeight = 25;
        this.label20.setTransform(176, 147.1);

        this.shape_tri1 = new cjs.Shape(); // yellow triangle
        this.shape_tri1.graphics.s("#000000").f("#FFF459").ss(0.8).moveTo(29, 64).lineTo(48, 64).lineTo(38.5, 47).lineTo(29, 64);
        this.shape_tri1.setTransform(0, 15);
        this.shape_sqr1 = new cjs.Shape(); // red square
        this.shape_sqr1.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(29, 74.8).lineTo(46.5, 74.8).lineTo(46.5, 92.3).lineTo(29, 92.3).lineTo(29, 74.8);
        this.shape_sqr1.setTransform(0, 15);
        this.shape_circ1 = new cjs.Shape(); // blue circle
        this.shape_circ1.graphics.f("#1CB0E7").s("#000000").ss(0.8, 0, 0, 4).arc(38, 112, 9, 0, 2 * Math.PI);
        this.shape_circ1.setTransform(0, 15);

        var ToBeAdded = [];

        var arrtext1 = ['1', '2', '3', '4', '5'];
        var arrtext2 = ['1', '3', '2', '2', '2', '5'];
        var arrtext3 = ['1', '5', '5', '2'];
        var arrtext4 = ['3', '2', '5', '4', '5', '4', '4', '5', '4'];

        for (var i = 0; i < 6; i++) {
            if (i != 5) {
                var textbox1 = new cjs.Shape();
                textbox1.graphics.f("#ffffff").s("#949599").ss(0.8).drawRect(26, 220, 19.5, 22);
                textbox1.setTransform(0 + (i * 35.3), 0);
                ToBeAdded.push(textbox1);

                var labeltext = new cjs.Text(arrtext1[i], "16px 'Myriad Pro'");
                labeltext.lineHeight = -1;
                if (i == 1) {
                    labeltext.setTransform(21.2 + (i * 38), 200);
                } else {
                    labeltext.setTransform(21.2 + (i * 36), 200);
                }

                ToBeAdded.push(labeltext);
            }

            var textbox2 = new cjs.Shape();
            textbox2.graphics.f("#ffffff").s("#949599").ss(0.8).drawRect(26, 220, 19.5, 22);
            textbox2.setTransform(215 + (i * 35.3), 0);
            ToBeAdded.push(textbox2);

            var labeltext2 = new cjs.Text(arrtext2[i], "16px 'Myriad Pro'");
            labeltext2.lineHeight = -1;
            if (i == 1) {
                labeltext2.setTransform(235.3 + (i * 38), 200);
            } else if (i == 2) {
                labeltext2.setTransform(235.3 + (i * 37), 200);
            } else {
                labeltext2.setTransform(235.3 + (i * 36), 200);
            }
            ToBeAdded.push(labeltext2);

        }

        for (var i = 0; i < 9; i++) {
            if (i != 4 && i != 5 && i != 6 && i != 7 && i != 8) {
                var textbox1 = new cjs.Shape();
                textbox1.graphics.f("#ffffff").s("#949599").ss(0.8).drawRect(26, 276, 19.5, 22);
                textbox1.setTransform(0 + (i * 35.3), 0);
                ToBeAdded.push(textbox1);

                var labeltext = new cjs.Text(arrtext3[i], "16px 'Myriad Pro'");
                labeltext.lineHeight = -1;
                if (i == 1) {
                    labeltext.setTransform(21.2 + (i * 37), 256);
                } else {
                    labeltext.setTransform(21.2 + (i * 36), 256);
                }

                ToBeAdded.push(labeltext);
            }

            var textbox2 = new cjs.Shape();
            textbox2.graphics.f("#ffffff").s("#949599").ss(0.8).drawRect(26, 276, 19.5, 22);
            textbox2.setTransform(166.3 + (i * 34.6), 0);
            ToBeAdded.push(textbox2);

            var labeltext2 = new cjs.Text(arrtext4[i], "16px 'Myriad Pro'");
            labeltext2.lineHeight = -1;
            if (i == 0 || i == 1) {
                labeltext2.setTransform(189.5 + (i * 34.6), 256);
            } else if (i == 4 || i == 5 || i == 6) {
                labeltext2.setTransform(189.5 + (i * 34), 256);
            } else if (i == 7) {
                labeltext2.setTransform(189.5 + (i * 34.2), 256);
            } else {
                labeltext2.setTransform(189.5 + (i * 34.6), 256);
            }
            ToBeAdded.push(labeltext2);
        }

        // var ToBeAdded = [];
        var tri_xPos = ['118', '222.5', '261', '403', '82', '213', '246.5', '281', '454'];
        var tri_yPos = ['165', '165', '165', '165', '221', '221', '221', '221', '221'];

        for (var i = 0; i < tri_xPos.length; i++) {
            var xPos = tri_xPos[i];
            var yPos = tri_yPos[i];

            var tempshape_tri = this.shape_tri1.clone(true);
            tempshape_tri.setTransform(xPos, yPos, 0.75, 0.75);
            ToBeAdded.push(tempshape_tri);
        }

        var sqr_xPos = ['48.5', '154.5', '369.5', '10', '119.5', '179', '314.5', '384.3', '418.5'];
        var sqr_yPos = ['144', '144', '144', '200', '200', '200', '200', '200', '200'];

        for (var i = 0; i < sqr_xPos.length; i++) {
            var xPos = sqr_xPos[i];
            var yPos = sqr_yPos[i];

            var tempshape_sqr = this.shape_sqr1.clone(true);
            tempshape_sqr.setTransform(xPos, yPos, 0.75, 0.75);
            ToBeAdded.push(tempshape_sqr);
        }

        var circ_xPos = ['8.8', '82.7', '299', '333', '47.5', '349'];
        var circ_yPos = ['122.7', '122.7', '122.7', '122.7', '178.7', '178.7'];

        for (var i = 0; i < circ_xPos.length; i++) {
            var xPos = circ_xPos[i];
            var yPos = circ_yPos[i];

            var tempshape_circ = this.shape_circ1.clone(true);
            tempshape_circ.setTransform(xPos, yPos, 0.75, 0.75);
            ToBeAdded.push(tempshape_circ);
        }

        this.label21 = new cjs.Text(".", "16px 'Myriad Pro'");
        this.label21.lineHeight = 25;
        this.label21.setTransform(490, 284);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.textbox_group2, this.textbox_group3, this.textbox_group4, this.shape_tri1, this.shape_sqr1, this.shape_circ1);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label6, this.label7, this.label8, this.label9, this.label10);
        this.addChild(this.label11, this.label12, this.label13, this.label14, this.label15);
        this.addChild(this.label16, this.label17, this.label18, this.label19, this.label20);
        this.addChild(this.label21);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 370);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 5, 510, 210, 10);
        this.roundRect1.setTransform(0, 20);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(30, 150, 77, 72, 7);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = this.roundRect2.clone(true);
        this.roundRect3.setTransform(111, 0);

        this.roundRect4 = this.roundRect2.clone(true);
        this.roundRect4.setTransform(258, 0);
        this.roundRect5 = this.roundRect2.clone(true);
        this.roundRect5.setTransform(370, 0);

        this.text = new cjs.Text("Rita figurerna på rätt plats. Måla.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 2);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#D83770");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 2);

        this.instance = new lib.p151_1();
        this.instance.setTransform(40, 127, 0.47, 0.47);

        this.instance1 = new lib.p151_2();
        this.instance1.setTransform(64, 56, 0.47, 0.47);

        this.shape_ellipse1 = new cjs.Shape(); // ellipse
        this.shape_ellipse1.graphics.f("#ffffff").s("#D83770").ss(0.8, 0, 0, 4).arc(62, 70, 37, 0, 2 * Math.PI, false);
        this.shape_ellipse1.setTransform(-4, 9, 2.1, 0.96);
        this.shape_ellipse2 = this.shape_ellipse1.clone(true);
        this.shape_ellipse2.setTransform(256, 9, 2.1, 0.96);

        this.line_arrow1 = new cjs.Shape();
        this.line_arrow1.graphics.s("#D83770").f("#D83770").ss(0.9).moveTo(87, 112).lineTo(69, 145).lineTo(72, 144).lineTo(67, 149).lineTo(68, 142).lineTo(69, 145);
        this.line_arrow1.setTransform(0, -4);

        this.line_arrow2 = this.line_arrow1.clone(true);
        this.line_arrow2.setTransform(262, -4);

        this.line_arrow3 = this.line_arrow1.clone(true);
        this.line_arrow3.setTransform(277, 124);
        this.line_arrow3.skewX = -58;
        this.line_arrow3.skewY = -58;
        this.line_arrow4 = this.line_arrow1.clone(true);
        this.line_arrow4.setTransform(15, 124);
        this.line_arrow4.skewX = -58;
        this.line_arrow4.skewY = -58;


        this.shape_tri2 = new cjs.Shape(); // red triangle
        this.shape_tri2.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(360, 75).lineTo(379, 75).lineTo(369.5, 58).lineTo(360, 75);
        this.shape_tri2.setTransform(8, -3);
        this.shape_tri4 = new cjs.Shape(); // blue triangle
        this.shape_tri4.graphics.s("#000000").f("#42B7E9").ss(0.8).moveTo(167, 78).lineTo(186, 78).lineTo(176.5, 61).lineTo(167, 78);
        this.shape_tri4.setTransform(0, -6);


        this.shape_sqr1 = new cjs.Shape(); // red square
        this.shape_sqr1.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(76, 79.8).lineTo(92.5, 79.8).lineTo(92.5, 96.3).lineTo(76, 96.3).lineTo(76, 79.8);
        this.shape_sqr1.setTransform(74, 3);
        this.shape_sqr2 = new cjs.Shape(); // red square
        this.shape_sqr2.graphics.s("#000000").f("#FFF23C").ss(0.8).moveTo(76, 79.8).lineTo(92.5, 79.8).lineTo(92.5, 96.3).lineTo(76, 96.3).lineTo(76, 79.8);
        this.shape_sqr2.setTransform(40, 3);
        this.shape_sqr3 = new cjs.Shape(); // blue square
        this.shape_sqr3.graphics.s("#000000").f("#FFF23C").ss(0.8).moveTo(342, 55.8).lineTo(358.5, 55.8).lineTo(358.5, 72.3).lineTo(342, 72.3).lineTo(342, 55.8);
        this.shape_sqr3.setTransform(-8, 6);
        this.shape_sqr4 = new cjs.Shape(); // yellow square
        this.shape_sqr4.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(394, 85.8).lineTo(410.5, 85.8).lineTo(410.5, 102.3).lineTo(394, 102.3).lineTo(394, 85.8);
        this.shape_sqr4.setTransform(-5, 0);
        this.shape_sqr5 = new cjs.Shape(); // yellow square
        this.shape_sqr5.graphics.s("#000000").f("#1CB0E7").ss(0.8).moveTo(394, 85.8).lineTo(410.5, 85.8).lineTo(410.5, 102.3).lineTo(394, 102.3).lineTo(394, 85.8);
        this.shape_sqr5.setTransform(31, -15);
        this.shape_sqr6 = new cjs.Shape(); // white square
        this.shape_sqr6.graphics.s("#000000").f("#ffffff").ss(0.8).moveTo(444, 129.8).lineTo(460.5, 129.8).lineTo(460.5, 146.3).lineTo(444, 146.3).lineTo(444, 129.8);
        this.shape_sqr6.setTransform(0, -4);

        this.shape_circ1 = new cjs.Shape(); // blue circle
        this.shape_circ1.graphics.f("#1CB0E7").s("#000000").ss(0.8, 0, 0, 4).arc(86, 61, 8.6, 0, 2 * Math.PI);
        this.shape_circ1.setTransform(25, -1);
        this.shape_circ2 = new cjs.Shape(); // red circle
        this.shape_circ2.graphics.f("#E7544E").s("#000000").ss(0.8, 0, 0, 4).arc(115, 67, 8.6, 0, 2 * Math.PI);
        this.shape_circ2.setTransform(25, -5);
        this.shape_circ3 = new cjs.Shape(); // red circle
        this.shape_circ3.graphics.f("#1CB0E7").s("#000000").ss(0.8, 0, 0, 4).arc(387, 64, 8.6, 0, 2 * Math.PI);
        this.shape_circ3.setTransform(-25, 30);
        this.shape_circ4 = new cjs.Shape(); // red circle
        this.shape_circ4.graphics.f("#FFF23C").s("#000000").ss(0.8, 0, 0, 4).arc(387, 64, 8.6, 0, 2 * Math.PI);
        this.shape_circ4.setTransform(25, -5);
        this.shape_circ5 = new cjs.Shape(); // red circle
        this.shape_circ5.graphics.f("#ffffff").s("#000000").ss(0.8, 0, 0, 4).arc(387, 64, 8.6, 0, 2 * Math.PI);
        this.shape_circ5.setTransform(-74, 71);

        this.line1 = new cjs.Shape();
        this.line1.graphics.s("#949599").ss(0.9).moveTo(256, 35).lineTo(256, 225);
        this.line1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.shape_ellipse1, this.shape_ellipse2, this.shape_circ1, this.shape_circ2, this.shape_circ3, this.shape_circ4, this.shape_circ5, this.instance, this.instance1, this.line1);
        this.addChild(this.shape_tri2, this.shape_tri3, this.shape_tri4, this.shape_tri5, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.shape_sqr1, this.shape_sqr2, this.shape_sqr3, this.shape_sqr4, this.shape_sqr5, this.shape_sqr6);
        this.addChild(this.line_arrow1, this.line_arrow2, this.line_arrow3, this.line_arrow4)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(309, 100, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(310, 436, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
