(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p88_1.png",
            id: "p88_1"
        }, {
            src: "images/p88_2.png",
            id: "p88_2"
        }, {
            src: "images/p88_3.png",
            id: "p88_3"
        }, {
            src: "images/p88_4.png",
            id: "p88_4"
        }, {
            src: "images/p88_5.png",
            id: "p88_5"
        }]
    };

    (lib.p88_1 = function() {
        this.initialize(img.p88_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p88_2 = function() {
        this.initialize(img.p88_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p88_3 = function() {
        this.initialize(img.p88_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p88_4 = function() {
        this.initialize(img.p88_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p88_5 = function() {
        this.initialize(img.p88_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Mönster", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(230, 37);

        this.text_1 = new cjs.Text("30", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(40.7, 21.5);

        this.pageBottomText = new cjs.Text("förstå och kunna fortsätta ett påbörjat mönster", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(79.5, 651.2);

        this.text_4 = new cjs.Text("88", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(38, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(70, 649.7, 199.1, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.pageBottomText, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Fortsätt mönstret.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(9, 4);

        this.instance_1 = new lib.p88_1();
        this.instance_1.setTransform(1.5, 26, 0.4, 0.4);

        this.text_2 = new cjs.Text("Gör ett likadant mönster med 2 andra färger.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(9, 71);

        this.instance_2 = new lib.p88_2();
        this.instance_2.setTransform(1.5, 93, 0.4, 0.4);

        this.addChild(this.text_1, this.instance_1, this.text_2, this.instance_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 567.3, 170);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Fortsätt mönstret.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(9, 4);

        this.instance_1 = new lib.p88_3();
        this.instance_1.setTransform(1.5, 26, 0.4, 0.4);

        this.text_2 = new cjs.Text("Gör ett likadant mönster med 3 andra färger.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(9, 72);

        this.instance_2 = new lib.p88_4();
        this.instance_2.setTransform(1.5, 94, 0.4, 0.4);

        this.addChild(this.text_1, this.instance_1, this.text_2, this.instance_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 567.3, 170);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Gör egna mönster.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(9, 0);

        this.instance_1 = new lib.p88_5();
        this.instance_1.setTransform(1.5, 23, 0.4, 0.4);

        this.addChild(this.text_1, this.instance_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 589, 200);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 265, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(296.8, 418, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(296.8, 581, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
