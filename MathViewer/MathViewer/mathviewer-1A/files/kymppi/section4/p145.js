(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p145_1.png",
            id: "p145_1"
        }, {
            src: "images/p145_2.png",
            id: "p145_2"
        }]
    };

    (lib.p145_1 = function() {
        this.initialize(img.p145_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p145_2 = function() {
        this.initialize(img.p145_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("145", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(552, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#848FC7").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol6 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(-6, 38, 500, 542, 13);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p145_1();
        this.instance.setTransform(400, -2, 0.375, 0.375);

        this.instance_2 = new lib.p145_2();
        this.instance_2.setTransform(70, 305, 0.45, 0.45);

        this.text = new cjs.Text("Karusellspelet", "18px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 20;
        this.text.setTransform(-6, -7);

        this.text_2 = new cjs.Text("Spel för 2.", "16px 'Myriad Pro'", "#8390C8");
        this.text_2.lineHeight = 20;
        this.text_2.setTransform(-6, 15);

        this.text_1 = new cjs.Text("•  Slå 2 tärningar.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(5, 44);

        this.text_3 = new cjs.Text("•  Addera eller subtrahera tärningstalen.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(5, 64);

        this.text_4 = new cjs.Text("•  Måla svaret i din karusell.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(5, 86);

        this.text_5 = new cjs.Text("•  Turen går över till näste spelare.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(5, 108);

        this.text_6 = new cjs.Text("•  Den som först har målat alla tal i sin karusell vinner omgången.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(5, 130);

        this.text_7 = new cjs.Text("Omgång 1", "16px 'MyriadPro-Semibold'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(5.5, 156);

        this.text_8 = new cjs.Text("Omgång 2", "16px 'MyriadPro-Semibold'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(5.5, 413);

        this.text_9 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(310, 7);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#848FC7').drawRoundRect(296, -6, 197, 38, 7);
        this.roundRect2.setTransform(0, 0);

        var circleArr = [];

        var ArrCircle1x = ['100', '131.5', '153', '157', '145', '116', '83', '55', '41', '45', '68'];
        var ArrCircle1y = ['179.5', '188', '213.5', '246.5', '277.5', '294', '294', '277', '246.5', '213.5', '188.5'];
        var ArrCircle2x = ['402', '433.5', '455', '459', '447', '418', '385', '357', '343', '347', '370'];
        var ArrCircle2y = ['179.5', '188', '213.5', '246.5', '277.5', '294', '294', '277', '246.5', '213.5', '188.5'];
        var ArrCircle3x = ['100', '131.5', '153', '157', '145', '116', '83', '55', '41', '45', '68'];
        var ArrCircle3y = ['435.5', '444', '469.5', '502.5', '533.5', '550', '550', '533', '502.5', '469.5', '444.5'];
        var ArrCircle4x = ['402', '433.5', '455', '459', '447', '418', '385', '357', '343', '347', '370'];
        var ArrCircle4y = ['435.5', '444', '469.5', '502.5', '533.5', '550', '550', '533', '502.5', '469.5', '444.5'];

        for (var i = 0; i < 11; i++) {
            this.shape_circle1 = new cjs.Shape();
            this.shape_circle1.graphics.f("#ffffff").s("#DB2128").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle1.setTransform(ArrCircle1x[i], ArrCircle1y[i]);
            circleArr.push(this.shape_circle1);

            this.shape_circle2 = new cjs.Shape();
            this.shape_circle2.graphics.f("#ffffff").s("#DB2128").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle2.setTransform(ArrCircle2x[i], ArrCircle2y[i]);
            circleArr.push(this.shape_circle2);

            this.shape_circle3 = new cjs.Shape();
            this.shape_circle3.graphics.f("#ffffff").s("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle3.setTransform(ArrCircle3x[i], ArrCircle3y[i]);
            circleArr.push(this.shape_circle3);

            this.shape_circle4 = new cjs.Shape();
            this.shape_circle4.graphics.f("#ffffff").s("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 15, 0, 2 * Math.PI);
            this.shape_circle4.setTransform(ArrCircle4x[i], ArrCircle4y[i]);
            circleArr.push(this.shape_circle4);


            if (i != 10) {
                var text_circle1 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle1.lineHeight = -1;
                text_circle1.setTransform(parseInt(ArrCircle1x[i]) + 5, parseInt(ArrCircle1y[i]) + 3.5);
                circleArr.push(text_circle1);
            } else {
                var text_circle1 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle1.lineHeight = -1;
                text_circle1.setTransform(parseInt(ArrCircle1x[i]) + 0, parseInt(ArrCircle1y[i]) + 3);
                circleArr.push(text_circle1);
            }

            if (i != 10) {
                var text_circle2 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle2.lineHeight = -1;
                text_circle2.setTransform(parseInt(ArrCircle2x[i]) + 5, parseInt(ArrCircle2y[i]) + 3.5);
                circleArr.push(text_circle2);
            } else {
                var text_circle2 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle2.lineHeight = -1;
                text_circle2.setTransform(parseInt(ArrCircle2x[i]) + 0, parseInt(ArrCircle2y[i]) + 3);
                circleArr.push(text_circle2);
            }

            if (i != 10) {
                var text_circle3 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle3.lineHeight = -1;
                text_circle3.setTransform(parseInt(ArrCircle3x[i]) + 5, parseInt(ArrCircle3y[i]) + 3.5);
                circleArr.push(text_circle3);
            } else {
                var text_circle3 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle3.lineHeight = -1;
                text_circle3.setTransform(parseInt(ArrCircle3x[i]) + 0, parseInt(ArrCircle3y[i]) + 3);
                circleArr.push(text_circle3);
            }

            if (i != 10) {
                var text_circle4 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle4.lineHeight = -1;
                text_circle4.setTransform(parseInt(ArrCircle4x[i]) + 5, parseInt(ArrCircle4y[i]) + 3.5);
                circleArr.push(text_circle4);
            } else {
                var text_circle4 = new cjs.Text("" + i + "", "16px 'Myriad Pro'");
                text_circle4.lineHeight = -1;
                text_circle4.setTransform(parseInt(ArrCircle4x[i]) + 0, parseInt(ArrCircle4y[i]) + 3);
                circleArr.push(text_circle4);
            }

        }

        // innercircle
        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 41, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(99.3, 238);
        circleArr.push(this.shape_groupl);

        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#0094D9").ss(0.8, 0, 0, 4).arc(10, 10, 41, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(401.3, 238);
        circleArr.push(this.shape_groupl);

        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#F26529").ss(0.8, 0, 0, 4).arc(10, 10, 40.5, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(99.3, 494);
        circleArr.push(this.shape_groupl);

        this.shape_groupl = new cjs.Shape();
        this.shape_groupl.graphics.f("#F26529").ss(0.8, 0, 0, 4).arc(10, 10, 40.5, 0, 2 * Math.PI);
        this.shape_groupl.setTransform(401.3, 494);
        circleArr.push(this.shape_groupl);

        this.textCir1 = new cjs.Text("Spelare 1", "16px 'Myriad Pro'", "#ffffff");
        this.textCir1.lineHeight = 24;
        this.textCir1.setTransform(76, 239);
        circleArr.push(this.textCir1);

        this.textCir2 = new cjs.Text("Spelare 2", "16px 'Myriad Pro'", "#ffffff");
        this.textCir2.lineHeight = 24;
        this.textCir2.setTransform(378, 239);
        circleArr.push(this.textCir2);

        this.textCir3 = new cjs.Text("Spelare 1", "16px 'Myriad Pro'", "#ffffff");
        this.textCir3.lineHeight = 24;
        this.textCir3.setTransform(76, 495);
        circleArr.push(this.textCir3);

        this.textCir4 = new cjs.Text("Spelare 2", "16px 'Myriad Pro'", "#ffffff");
        this.textCir4.lineHeight = 24;
        this.textCir4.setTransform(378, 495);
        circleArr.push(this.textCir4);

        this.addChild(this.roundRect2, this.roundRect1, this.instance_2);
        this.addChild(this.text_1);
        this.addChild(this.text, this.text_2, this.instance, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9);

        for (var rect = 0; rect < circleArr.length; rect++) {
            this.addChild(circleArr[rect]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 590, 530);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(332, 273, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
