var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };

    var transformX = transformY = 0.180;
    var moveX = 500 + 70,
        moveY = 4;
    var startX = ((593.0000) * transformX) + moveX,
        startY = ((2.0000) * transformY) + moveY;
    var endX = ((333.3333) * transformX) + moveX,
        endY = ((1899.3333) * transformY) + moveY;
    var pencil = null;


    var startX2 = ((333.6667) * transformX) + moveX,
        startY2 = ((1899.6667) * transformY) + moveY,
        endX2 = ((334.0000) * transformX) + moveX,
        endY2 = ((1900.0000) * transformY) + moveY;

    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [

        new createjs.Point(startX, startY),
        new createjs.Point(((506.3420) * transformX) + moveX, ((634.2701) * transformY) + moveY),
        new createjs.Point(((419.6580) * transformX) + moveX, ((1266.7299) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(endX, endY)
    ];

    var points2 = [
        new createjs.Point(startX2, startY2),
        new createjs.Point(endX2, endY2)
    ];
    var points1Final = points;

    var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath, motionPath2);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final), getMotionPathFromPoints(points2Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talen1 och 2", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("3", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 30).call(function() {
            // setTimeout(function() {
            pause();
            // }, 800);
        })).on('change', (function(event) {
            if (currentMotionStep == 1) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        this.timeline.addTween(cjs.Tween.get(bar2).wait(40).to({
            guide: {
                path: motionPath2
            }
        }, 20).wait(20)).on('change', (function(event) {
            if (currentMotionStep == 2) {
                bar2 = drawCharacter(thisStage, bar2, true, event);
            }
        }));


    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];

                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }

            if (thisbar.x == 664.2653389180444 && thisbar.y == 95.37704643257591) { // finished plotting 1/3 of line
                Stage1.shape.visible = false;
            } else if (thisbar.x == 676.74 && thisbar.y == 4.36) { // plot 1st point 
                Stage1.shape.visible = true;
            }


            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.2)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.paused = false;
            thisStage.movie.play();
        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);
        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }

    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
