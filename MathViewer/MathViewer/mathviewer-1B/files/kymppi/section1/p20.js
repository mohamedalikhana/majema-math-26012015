(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p20_1.png",
            id: "p20_1"
        }]
    };

    (lib.p20_1 = function() {
        this.initialize(img.p20_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);


    (lib.Symbol1 = function() {
        this.initialize();

        this.pageTitle = new cjs.Text("Udda och jämna tal", "24px 'MyriadPro-Semibold'", "#90BD22");
        this.pageTitle.setTransform(98, 42);

        this.text_1 = new cjs.Text("6", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(58, 42);
            
        this.text_2 = new cjs.Text("20", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(40, 658);

        this.pageFooterTextRight = new cjs.Text("kunna skilja på udda och jämna tal", "9px 'Myriad Pro'");
        this.pageFooterTextRight.setTransform(70, 659);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);
      
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(44, 15, 1, 1.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.pageTitle, this.pageFooterTextRight, this.shape_2, this.shape_1, this.shape, this.text, this.text_1, this.text_2, this.text_3);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Main yellow block

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(5, 0, 517, 172, 10);
        this.shape_115.setTransform(0, 0);

        // Main white block

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(27, 18, 225, 140, 5);
        this.shape_113.setTransform(0, 0);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(275, 18, 225, 140, 5);
        this.shape_114.setTransform(0, 0);

        //tag FOR LEFT WHITE BOX

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_71.setTransform(39, 19);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_72.setTransform(55, 19);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_73.setTransform(70.5, 19);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_74.setTransform(86.2, 19);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_75.setTransform(101.2, 19);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(116, 19);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(131.3, 19);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(146.1, 19);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(161.2, 19);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_98.setTransform(176.2, 19);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_99.setTransform(191.4, 19);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_100.setTransform(206.5, 19);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_101.setTransform(222, 19);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_102.setTransform(238.6, 19);




        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_80.setTransform(39, 16.5);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_81.setTransform(39, 23.5);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_82.setTransform(53.9, 23.5);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_83.setTransform(53.9, 23.5);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_84.setTransform(70, 23.5);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_85.setTransform(70, 23.5);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_86.setTransform(84.5, 23.5);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_87.setTransform(84.5, 23.5);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(101, 23.5);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(101, 23.5);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_90.setTransform(116, 23.5);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_91.setTransform(116, 23.5);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(132, 23.5);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(132, 23.5);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_94.setTransform(147, 23.5);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_95.setTransform(147, 23.5);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(163, 23.5);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(163, 23.5);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_103.setTransform(179, 23.5);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_112.setTransform(179, 23.5);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_104.setTransform(193, 23.5);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_105.setTransform(193, 23.5);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_106.setTransform(208, 23.5);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_107.setTransform(208, 23.5);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_108.setTransform(223, 23.5);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_109.setTransform(223, 23.5);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_110.setTransform(240, 23.5);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_111.setTransform(240, 23.5);


        //tag FOR right WHITE BOX

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_121.setTransform(285, 19);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_122.setTransform(301, 19);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_123.setTransform(317, 19);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_124.setTransform(333, 19);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_125.setTransform(349, 19);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_126.setTransform(364, 19);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_127.setTransform(379, 19);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_128.setTransform(394, 19);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_129.setTransform(409, 19);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_130.setTransform(424.2, 19);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_131.setTransform(439, 19);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_132.setTransform(454, 19);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_133.setTransform(469, 19);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_134.setTransform(484.6, 19);




        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_135.setTransform(286, 23.5);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_136.setTransform(286, 23.5);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_137.setTransform(300.9, 23.5);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_138.setTransform(300.9, 23.5);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_139.setTransform(317, 23.5);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_140.setTransform(317, 23.5);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_141.setTransform(332.5, 23.5);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_141.setTransform(332.5, 23.5);

        this.shape_142 = new cjs.Shape();
        this.shape_142.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_142.setTransform(348, 23.5);

        this.shape_143 = new cjs.Shape();
        this.shape_143.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_143.setTransform(348, 23.5);

        this.shape_144 = new cjs.Shape();
        this.shape_144.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_144.setTransform(363, 23.5);

        this.shape_145 = new cjs.Shape();
        this.shape_145.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_145.setTransform(363, 23.5);

        this.shape_146 = new cjs.Shape();
        this.shape_146.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_146.setTransform(379, 23.5);

        this.shape_147 = new cjs.Shape();
        this.shape_147.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_147.setTransform(379, 23.5);

        this.shape_149 = new cjs.Shape();
        this.shape_149.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_149.setTransform(393, 23.5);

        this.shape_150 = new cjs.Shape();
        this.shape_150.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_150.setTransform(393, 23.5);

        this.shape_151 = new cjs.Shape();
        this.shape_151.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_151.setTransform(408, 23.5);

        this.shape_152 = new cjs.Shape();
        this.shape_152.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_152.setTransform(408, 23.5);

        this.shape_153 = new cjs.Shape();
        this.shape_153.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_153.setTransform(424, 23.5);

        this.shape_154 = new cjs.Shape();
        this.shape_154.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_154.setTransform(424, 23.5);

        this.shape_155 = new cjs.Shape();
        this.shape_155.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_155.setTransform(439, 23.5);

        this.shape_156 = new cjs.Shape();
        this.shape_156.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_156.setTransform(439, 23.5);

        this.shape_157 = new cjs.Shape();
        this.shape_157.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_157.setTransform(454, 23.5);

        this.shape_158 = new cjs.Shape();
        this.shape_158.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_158.setTransform(454, 23.5);

        this.shape_159 = new cjs.Shape();
        this.shape_159.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_159.setTransform(469, 23.5);

        this.shape_160 = new cjs.Shape();
        this.shape_160.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_160.setTransform(469, 23.5);

        this.shape_161 = new cjs.Shape();
        this.shape_161.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_161.setTransform(485, 23.5);

        this.shape_162 = new cjs.Shape();
        this.shape_162.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_162.setTransform(485, 23.5);


        this.instance = new lib.p20_1();
        this.instance.setTransform(50, 40, 0.47, 0.47);

        var lineArr = [];
        var lineX = [45, 45, 45, 45, 45, 290, 290, 290, 290, 290];
        var lineY = [8, 36, 60, 86, 112, 8, 34, 60, 86, 112];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(-13, 40).lineTo(203, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var TxtArr = ['Udda tal är 1, 3, 5, 7, 9, …', '1', '3', '5', '7', 'Jämna tal är 0, 2, 4, 6, 8, …', '0', '2', '4', '6'];
        var TArr = [];
        var TxtlineX = [41, 44, 44, 44, 44, 287, 290, 290, 290, 288];
        var TxtlineY = [42, 67, 94, 118, 142, 42, 67, 94, 118, 142];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        this.addChild(this.shape_115, this.shape_113, this.shape_114, this.shape_80,
            this.shape_81, this.shape_82, this.shape_83, this.shape_84, this.shape_85,
            this.shape_86, this.shape_87, this.shape_88, this.shape_89, this.shape_90, this.shape_91,
            this.shape_92, this.shape_93, this.shape_94, this.shape_95, this.shape_96, this.shape_97,
            this.shape_103, this.shape_104, this.shape_105, this.shape_106, this.shape_107,
            this.shape_108, this.shape_109, this.shape_110, this.shape_111, this.shape_112,
            this.shape_71, this.shape_72, this.shape_73, this.shape_74, this.shape_75, this.shape_76, this.shape_77,
            this.shape_78, this.shape_79, this.shape_98, this.shape_99, this.shape_100, this.shape_101, this.shape_102);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        this.addChild(this.shape_135, this.shape_136, this.shape_137, this.shape_138, this.shape_139, this.shape_140, this.shape_141,
            this.shape_142, this.shape_143, this.shape_144, this.shape_145, this.shape_146, this.shape_147, this.shape_148, this.shape_149,
            this.shape_150, this.shape_151, this.shape_152, this.shape_153, this.shape_154, this.shape_155, this.shape_156, this.shape_157,
            this.shape_158, this.shape_159, this.shape_160, this.shape_161, this.shape_162, this.shape_121, this.shape_122, this.shape_123, this.shape_124, this.shape_125,
            this.shape_126, this.shape_127, this.shape_128, this.shape_129, this.shape_130, this.shape_131, this.shape_132, this.shape_133, this.shape_134, this.instance);
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Skriv antalet. Ringa in jämna tal.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 11.5, 517, 155, 10);
        this.roundRect1.setTransform(0, 0);

        this.SymbolRect = new lib.SymbolRect();
        this.SymbolRect.setTransform(26, 15);

        this.SymbolRect1 = new lib.SymbolRect();
        this.SymbolRect1.setTransform(88.5, 15);

        this.SymbolRect2 = new lib.SymbolRect();
        this.SymbolRect2.setTransform(148, 15);

        this.SymbolRect3 = new lib.SymbolRect();
        this.SymbolRect3.setTransform(206.5, 15);

        this.SymbolRect4 = new lib.SymbolRect();
        this.SymbolRect4.setTransform(266, 15);

        this.SymbolRect5 = new lib.SymbolRect();
        this.SymbolRect5.setTransform(324.5, 15);

        this.SymbolRect6 = new lib.SymbolRect();
        this.SymbolRect6.setTransform(384, 15);

        this.SymbolRect7 = new lib.SymbolRect();
        this.SymbolRect7.setTransform(443.5, 15);


        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (column == 1 && row == 0) {
                    continue;
                }
                this.shape_group1.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(35 + (columnSpace * 18.5), 103.5 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                if (column == 1 && row == 0) {
                    continue;
                }
                this.shape_group2.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(97 + (columnSpace * 19), 86.5 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                this.shape_group3.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(157 + (columnSpace * 19), 103.5 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group3.setTransform(0, 0);

        this.shape_group4 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {

                this.shape_group4.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(215 + (columnSpace * 19), 86.5 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group4.setTransform(0, 0);

        this.shape_group5 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 1 && row == 0) {
                    continue;
                }
                this.shape_group5.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(275 + (columnSpace * 19), 69 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group5.setTransform(0, 0);

        this.shape_group6 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (column == 1 && row == 0) {
                    continue;
                }
                this.shape_group6.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(333 + (columnSpace * 19), 35 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group6.setTransform(0, 0);

        this.shape_group7 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 5; row++) {

                this.shape_group7.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(392 + (columnSpace * 19), 52 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group7.setTransform(0, 0);

        this.shape_group8 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {

                this.shape_group8.graphics.f("#00A3C4").s("#878787").ss(1, 0, 0, 4).arc(452 + (columnSpace * 19), 35 + (row * 17), 6.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group8.setTransform(0, 0);

        var lineArr = [];
        var lineX = [17, 79, 139, 197, 259, 317, 375, 435];
        var lineY = [116, 116, 116, 116, 116, 116, 116, 116];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_2.setTransform(33, 156);

        this.addChild(this.text, this.text_1, this.roundRect1, this.SymbolRect, this.SymbolRect1, this.SymbolRect2, this.SymbolRect3,
            this.SymbolRect4, this.SymbolRect5, this.SymbolRect6, this.SymbolRect7,
            this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5, this.shape_group6, this.shape_group7, this.shape_group8, this.text_2)

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.SymbolRect = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 11.5, 36, 102, 0);
        this.roundRect1.setTransform(0, 0);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(18, 12).lineTo(18, 113);
        this.vrLine_1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [-9, -9, -9, -9, -9];
        var lineY = [-12, 5, 22, 40, 57];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.addChild(this.roundRect1, this.vrLine_1)

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 80);

    (lib.Symbol4 = function() {
        this.initialize();


        this.text = new cjs.Text(" Fortsätt talföljden.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(5, 12, 517, 169, 10);
        this.roundRect1.setTransform(0, 0);

        var RectArr = [];
        var lineX = [55, 95, 135, 175, 215, 255, 295, 335, 375, 415, 55, 95, 135, 175, 215, 255, 295, 335, 375, 415,
            55, 95, 135, 175, 215, 255, 295, 335, 375, 415, 55, 95, 135, 175, 215, 255, 295, 335, 375, 415
        ];
        var lineY = [14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 53, 53, 53, 53, 53, 53, 53, 53, 53, 53,
            92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 131, 131, 131, 131, 131, 131, 131, 131, 131, 131
        ];

        for (var row = 0; row < lineX.length; row++) {
            this.roundRect = new cjs.Shape();
            this.roundRect.graphics.f('').s("#90BD22").ss(0.7).drawRoundRect(5, 11.5, 40, 23.5, 7);
            this.roundRect.setTransform(lineX[row], lineY[row]);
            RectArr.push(this.roundRect);
        }

        this.text_2 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_2.setTransform(70, 48);

        this.text_3 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_3.setTransform(110, 48);

        this.text_4 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_4.setTransform(150, 48);

        this.text5 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text5.setTransform(63, 87);

        this.text_5 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_5.setTransform(77, 87);

        this.text6 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text6.setTransform(103, 87);

        this.text_6 = new cjs.Text("8", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_6.setTransform(113, 87);

        this.text7 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text7.setTransform(143, 87);

        this.text_7 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_7.setTransform(153, 87);

        this.text_8 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_8.setTransform(70, 126);

        this.text_9 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_9.setTransform(110, 126);

        this.text_10 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_10.setTransform(150, 126);

        this.text11 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text11.setTransform(63, 165);

         this.text_11 = new cjs.Text("9", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_11.setTransform(73, 165);

        this.text12 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text12.setTransform(103, 165);

        this.text_12 = new cjs.Text("7", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_12.setTransform(113, 165);

        this.text13 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text13.setTransform(143, 165);

        this.text_13 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_13.setTransform(153, 165);



        this.addChild(this.text, this.text_1, this.roundRect1, this.text_2, this.text_3, this.text_4, this.text5, this.text6, this.text7,
            this.text_5, this.text_6, this.text_7,this.text_8, this.text_9, this.text_10, this.text11, this.text12, this.text13, 
            this.text_11, this.text_12, this.text_13);

        for (var i = 0; i < RectArr.length; i++) {
            this.addChild(RectArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 180);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 57, 1, 1, 0, 0, 0, 255.8, 0);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 260, 1, 1, 0, 0, 0, 255.8, 0);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(294, 455, 1, 1, 0, 0, 0, 255.8, 0);


        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
