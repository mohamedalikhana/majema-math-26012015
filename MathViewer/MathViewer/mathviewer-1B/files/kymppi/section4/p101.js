(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("101", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(1, 5);

        this.text_1 = new cjs.Text(" Rita formerna där de passar. Måla.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#9D9D9C').drawRoundRect(0, 0, 509, 117, 10);
        this.roundRect1.setTransform(0, 18);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#878787').drawRoundRect(0, 0, 160, 95, 0);
        this.Rect1.setTransform(8, 28);

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("").s('#878787').drawRoundRect(0, 0, 160, 95, 0);
        this.Rect2.setTransform(175, 28);

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("").s('#878787').drawRoundRect(0, 0, 160, 95, 0);
        this.Rect3.setTransform(342, 28);

        this.text_2 = new cjs.Text("cirklar", "16px 'Myriad Pro'");
        this.text_2.setTransform(230, 50);

        this.text_3 = new cjs.Text("trianglar", "16px 'Myriad Pro'");
        this.text_3.setTransform(390, 50);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#9D9D9C').drawRoundRect(0, 0, 509, 117, 10);
        this.roundRect2.setTransform(0, 145);

        this.Rect4 = new cjs.Shape();
        this.Rect4.graphics.f("").s('#878787').drawRoundRect(0, 0, 160, 95, 0);
        this.Rect4.setTransform(8, 156);

        this.Rect5 = new cjs.Shape();
        this.Rect5.graphics.f("").s('#878787').drawRoundRect(0, 0, 160, 95, 0);
        this.Rect5.setTransform(175, 156);

        this.Rect6 = new cjs.Shape();
        this.Rect6.graphics.f("").s('#878787').drawRoundRect(0, 0, 160, 95, 0);
        this.Rect6.setTransform(342, 156);

        this.text_4 = new cjs.Text("fyrhörningar", "16px 'Myriad Pro'");
        this.text_4.setTransform(215, 177);

        this.text_5 = new cjs.Text("cirklar", "16px 'Myriad Pro'");
        this.text_5.setTransform(400, 177);


        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group1.graphics.f("#008BD2").s("#706F6F").ss(0.8, 0, 0, 4).arc(41 + (columnSpace * 20.5), 38 + (row * 19), 11.5, 0, 2 * Math.PI);
                } else if (column == 1 && row == 0) {
                    this.shape_group1.graphics.f('#FFF374').s("#706F6F").ss(0.7).drawRoundRect(80, 30, 22, 22, 0);
                } else if (column == 2 && row == 0) {
                    this.shape_group1.graphics.f('#008BD2').s("#706F6F").ss(1.2).moveTo(127, 53).lineTo(157, 53).lineTo(142, 28).lineTo(127, 53);
                } else if (column == 0 && row == 1) {
                    this.shape_group1.graphics.f('#FFF374').s("#706F6F").ss(1.2).moveTo(24, 95).lineTo(54, 95).lineTo(40, 67).lineTo(24, 95);
                } else if (column == 1 && row == 1) {
                    this.shape_group1.graphics.f('#D51317').s("#706F6F").ss(1.2).moveTo(76, 95).lineTo(106, 95).lineTo(91, 67).lineTo(76, 95);
                } else {
                    this.shape_group1.graphics.f("#D51317").s("#706F6F").ss(0.8, 0, 0, 4).arc(100 + (columnSpace * 20.5), 65 + (row * 19), 11.5, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group1.setTransform(0, 15);


        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group2.graphics.f('#D51317').s("#706F6F").ss(1.2).moveTo(24, 52).lineTo(54, 52).lineTo(39, 25).lineTo(24, 52);
                } else if (column == 1 && row == 0) {
                    this.shape_group2.graphics.f("#FFF374").s("#706F6F").ss(0.8, 0, 0, 4).arc(71 + (columnSpace * 20.5), 38 + (row * 19), 11.5, 0, 2 * Math.PI);
                } else if (column == 2 && row == 0) {
                    this.shape_group2.graphics.f('#FFF374').s("#706F6F").ss(0.7).drawRoundRect(129, 30, 22, 22, 0);
                } else if (column == 0 && row == 1) {
                    this.shape_group2.graphics.f('#D51317').s("#706F6F").ss(0.7).drawRoundRect(25, 73, 22, 22, 0);
                } else if (column == 1 && row == 1) {
                    this.shape_group2.graphics.f('#008BD2').s("#706F6F").ss(1.2).moveTo(76, 95).lineTo(106, 95).lineTo(91, 67).lineTo(76, 95);
                } else {
                    this.shape_group2.graphics.f("#D51317").s("#706F6F").ss(0.8, 0, 0, 4).arc(100 + (columnSpace * 20.5), 65 + (row * 19), 11.5, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group2.setTransform(0, 145);

        this.addChild(this.text, this.text_1, this.roundRect1, this.Rect1, this.Rect2, this.Rect3, this.Rect4, this.Rect5, this.Rect6, this.roundRect2,
            this.text_2, this.text_3, this.text_4, this.text_5, this.shape_group1, this.shape_group2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 250);

    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(1, 2);

        this.text_1 = new cjs.Text(" Vilka figurer är trianglar? Kryssa.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#9D9D9C').drawRoundRect(0, 0, 509, 117, 10);
        this.roundRect1.setTransform(0, 12);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#008BD2').s("#706F6F").ss(1.2).moveTo(45, 72).lineTo(72, 63).lineTo(25, 5).lineTo(45, 72);
        this.shape_1.setTransform(0, 17);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#008BD2').s("#000000").ss(1.2).moveTo(63, 52).lineTo(39, 22).lineTo(15, 52).lineTo(39, 82).lineTo(63, 52);
        this.shape_2.setTransform(90, 5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#008BD2').s("#000000").ss(1.2).moveTo(5, 55).lineTo(75, 55).lineTo(5, 15).lineTo(5, 55);
        this.shape_3.setTransform(180, 25);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#008BD2').s("#706F6F").ss(1.2).arc(0, 0, 15, 0, Math.PI * 135 / 180, true).lineTo(15, 40).lineTo(15, 0);
        this.shape_4.setTransform(300, 67, 0.8, 0.8);
        this.shape_4.rotation = 200;

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#008BD2').s("#000000").ss(1.2).moveTo(24, 52).lineTo(75, 77).lineTo(24, 12).lineTo(24, 52);
        this.shape_5.setTransform(345, 25);

        var differenceDegree = 10,
            differenceDistance = 18;
        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#008BD2').s("#706F6F").ss(1.2)
            .arc(10, -4, 15, Math.PI * (138 + differenceDegree) / 180, Math.PI * (40 - differenceDegree) / 180, false)
            .arc(20, 19, 15, Math.PI * (270 + differenceDegree) / 180, Math.PI * (135 - differenceDegree) / 180, false)
            .arc(1, 19, 15, Math.PI * (45 + differenceDegree) / 180, Math.PI * (270 - differenceDegree) / 180, false);
        this.shape_6.setTransform(480, 65, 0.7, 0.7);
        this.shape_6.rotation = 180;


        var TxtBoxArr = [];
        var TxtBox_X = [35, 120, 205, 290, 375, 460];
        var TxtBox_Y = [97, 97, 97, 97, 97, 97];
        for (var row = 0; row < TxtBox_X.length; row++) {
            var TxtBox_1 = new cjs.Shape();
            TxtBox_1.graphics.f('#FFFFFF').s("#706F6F").ss(0.7).drawRoundRect(0, 0, 20, 20, 0);
            TxtBox_1.setTransform(TxtBox_X[row], TxtBox_Y[row]);
            TxtBoxArr.push(TxtBox_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.shape_1, this.shape_2, this.shape_3,
            this.shape_4, this.shape_5, this.shape_6);
        for (var i = 0; i < TxtBoxArr.length; i++) {
            this.addChild(TxtBoxArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 125);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(1, 2);

        this.text_1 = new cjs.Text(" Vilka figurer är cirklar? Kryssa.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#9D9D9C').drawRoundRect(0, 0, 509, 117, 10);
        this.roundRect1.setTransform(0, 12);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#D6326A').s("#706F6F").ss(1.2).drawRoundRect(0, 0, 74, 27, 5);
        this.shape_1.setTransform(10, 50);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#D6326A').s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 21, 0, 2 * Math.PI);
        this.shape_2.setTransform(130, 63);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#D6326A').s("#706F6F").ss(1.2).arc(-13, -13, 15, Math.PI * 120 / 180, Math.PI * 330 / 180, false).arc(13, -13, 15, Math.PI * 210 / 180, Math.PI * 60 / 180, false).arc(13, 13, 15, Math.PI * 300 / 180, Math.PI * 150 / 180, false).arc(-13, 13, 15, Math.PI * 30 / 180, Math.PI * 240 / 180, false);
        this.shape_3.setTransform(215, 50);
        this.shape_3.rotation = 225;

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#D6326A').s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 15, 0, 2 * Math.PI);
        this.shape_4.setTransform(300, 63);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#D6326A').s("#706F6F").ss(1.2).arc(0, 0, 30, 0, Math.PI * 2 / 3, true).lineTo(30, 0);
        this.shape_5.setTransform(385, 60, 0.47, 0.47);
        this.shape_5.rotation = 285 + 0;

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#D6326A').s("#706F6F").ss(1.2).arc(0, 0, 30, 0, Math.PI * 1.5 / 3, true).lineTo(30, 30).lineTo(30, 0);
        this.shape_6.setTransform(470, 60, 0.6, 0.6);
        this.shape_6.rotation = 225 + 35;
  
        var TxtBoxArr = [];
        var TxtBox_X = [35, 120, 205, 290, 375, 460];
        var TxtBox_Y = [97, 97, 97, 97, 97, 97];
        for (var row = 0; row < TxtBox_X.length; row++) {
            var TxtBox_1 = new cjs.Shape();
            TxtBox_1.graphics.f('#FFFFFF').s("#706F6F").ss(0.7).drawRoundRect(0, 0, 20, 20, 0);
            TxtBox_1.setTransform(TxtBox_X[row], TxtBox_Y[row]);
            TxtBoxArr.push(TxtBox_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.shape_1, this.shape_2, this.shape_3,
            this.shape_4, this.shape_5, this.shape_6);
        for (var i = 0; i < TxtBoxArr.length; i++) {
            this.addChild(TxtBoxArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 125);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(315, 100, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(315, 390, 1, 1, 0, 0, 0, 255.8, 38);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(315, 547, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2, this.v3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
