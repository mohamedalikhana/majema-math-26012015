(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p49_1.png",
            id: "p49_1"
        }, {
            src: "images/p49_2.png",
            id: "p49_2"
        }]
    };

    (lib.p49_1 = function() {
        this.initialize(img.p49_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 300, 300);

    (lib.p49_2 = function() {
        this.initialize(img.p49_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("49", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 18;
        this.text_1.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#88C13F").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.drawRect(590, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_1.setTransform(0, 0);

        this.textBox1 = new cjs.Shape();
        this.textBox1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(471, 35, 27, 27);

        this.textBox2 = new cjs.Shape();
        this.textBox2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(503.5, 35, 27, 27);

        this.textBox3 = new cjs.Shape();
        this.textBox3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(536, 35, 27, 27);

        this.addChild(this.shape_1, this.shape, this.text_1, this.textBox1, this.textBox2, this.textBox3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p49_1();
        this.instance.setTransform(0, 15, 0.38, 0.37);

        this.text_1 = new cjs.Text("Hur många finns kvar?", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(20, -8);

        this.text_2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_2.lineHeight = 27;
        this.text_2.setTransform(0, -8);

        this.text_3 = new cjs.Text("Jag tar 2.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(85, 39);

        this.text_4 = new cjs.Text("Jag tar 2.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(85 + 265, 44);

        this.text_5 = new cjs.Text("Jag tar 2.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(90, 40 + 138);

        this.text_6 = new cjs.Text("Jag tar 2.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(87 + 265, 40 + 137);

        var textArr = [];
        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var tempRect=null;
                tempRect = new cjs.Shape();
                tempRect.graphics.s("#7D7D7D").ss(1).drawRoundRect(0, 0, 258, 132, 10);
                tempRect.setTransform(2 + (column * 263), 18 + (row * 138));
                textArr.push(tempRect);
            }
        }


        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var temptext=null;
                temptext = new cjs.Text("finns kvar:", "16px 'Myriad Pro'");
                temptext.lineHeight = -1;
                temptext.setTransform(153 + (column * 258), 128 + (row * 138));
                textArr.push(temptext);
            }
        }


        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 1; row++) {
                var tempBox=null;
                tempBox = new cjs.Shape();
                tempBox.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
                tempBox.setTransform(226 + (column * 256), 122 + (row * 137));
                textArr.push(tempBox);
            }
        }

        this.tempBox1 = new cjs.Shape();
        this.tempBox1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
        this.tempBox1.setTransform(225, 260);

        this.tempBox2 = new cjs.Shape();
        this.tempBox2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
        this.tempBox2.setTransform(225 + 259, 262);

        textArr.push(this.tempBox1);
        textArr.push(this.tempBox2);

        this.addChild(this.text_1, this.text_2, this.instance, this.img_Rect1, this.text_3, this.text_4,
            this.text_5, this.text_6);

        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p49_2();
        this.instance.setTransform(0, 13, 0.38, 0.37);

        this.text_1 = new cjs.Text("Jag tar 3.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(92, 35);

        this.text_2 = new cjs.Text("Jag tar 3.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(91 + 265, 35);

        this.text_3 = new cjs.Text("Jag tar 4.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(90, 37 + 138);

        this.text_4 = new cjs.Text("Jag tar 4.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(87 + 265, 40 + 135);

        var textArr = [];
        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var tempRect=null;
                tempRect = new cjs.Shape();
                tempRect.graphics.s("#7D7D7D").ss(1).drawRoundRect(0, 0, 258, 132, 10);
                tempRect.setTransform(2 + (column * 263), 15 + (row * 138));
                textArr.push(tempRect);
            }
        }

        for (var column = 0; column < 2; column++) {

            for (var row = 0; row < 2; row++) {
                var temptext=null;
                temptext = new cjs.Text("finns kvar:", "16px 'Myriad Pro'");
                temptext.lineHeight = 2;
                temptext.setTransform(153 + (column * 258), 123 + (row * 138));
                textArr.push(temptext);
            }
        }

        this.tempBox1 = new cjs.Shape();
        this.tempBox1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
        this.tempBox1.setTransform(226, 115.5);

        this.tempBox2 = new cjs.Shape();
        this.tempBox2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
        this.tempBox2.setTransform(225 + 259, 115.5);

        this.tempBox3 = new cjs.Shape();
        this.tempBox3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
        this.tempBox3.setTransform(225, 254);

        this.tempBox4 = new cjs.Shape();
        this.tempBox4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(0, 0, 20, 20);
        this.tempBox4.setTransform(225 + 259, 255);

        textArr.push(this.tempBox1);
        textArr.push(this.tempBox2);
        textArr.push(this.tempBox3);
        textArr.push(this.tempBox4);

        this.addChild(this.instance, this.text_1, this.text_2, this.text_3, this.text_4);

        for (var i = 0; i < textArr.length; i++) {
            this.addChild(textArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 310);



    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(301.8, 244, 1, 1, 0, 0, 0, 256.3, 173.6);
        this.v2 = new lib.Symbol3();
        this.v2.setTransform(301.8, 240, 1, 1, 0, 0, 0, 256.3, -110);


        this.addChild(this.v2, this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
