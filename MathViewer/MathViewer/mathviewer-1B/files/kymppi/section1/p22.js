(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p22_1.png",
            id: "p22_1"
        }, {
            src: "images/p22_2.png",
            id: "p22_2"
        }]
    };


    (lib.p22_1 = function() {
        this.initialize(img.p22_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p22_2 = function() {
        this.initialize(img.p22_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("22", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(39, 659);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.addChild(this.shape_1, this.text_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text   


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Räkna och hitta bokstäverna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 6.3);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 6.3);

        this.instance = new lib.p22_1();
        this.instance.setTransform(325, 32, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 510, 260, 10);
        this.roundRect1.setTransform(0, 25);

        //column-1
        this.label1 = new cjs.Text("11  +  3  =", "16px 'Myriad Pro'");
        this.label1.setTransform(25, 57);
        this.label2 = new cjs.Text("11  +  6  =", "16px 'Myriad Pro'");
        this.label2.setTransform(25, 84);
        this.label3 = new cjs.Text("12  +  3  =", "16px 'Myriad Pro'");
        this.label3.setTransform(25, 111);
        this.label4 = new cjs.Text("12  +  4  =", "16px 'Myriad Pro'");
        this.label4.setTransform(25, 137);
        this.label5 = new cjs.Text("13  +  5  =", "16px 'Myriad Pro'");
        this.label5.setTransform(25, 163);
        this.label6 = new cjs.Text("13  +  7  =", "16px 'Myriad Pro'");
        this.label6.setTransform(25, 189);

        //column-2
        this.label7 = new cjs.Text("15  –  5  =", "16px 'Myriad Pro'");
        this.label7.setTransform(194, 57);
        this.label8 = new cjs.Text("18  –  5  =", "16px 'Myriad Pro'");
        this.label8.setTransform(194, 84);
        this.label9 = new cjs.Text("17  –  5  =", "16px 'Myriad Pro'");
        this.label9.setTransform(194, 111);
        this.label10 = new cjs.Text("19  –  8  =", "16px 'Myriad Pro'");
        this.label10.setTransform(194, 137);
        this.label11 = new cjs.Text("20  –  1  =", "16px 'Myriad Pro'");
        this.label11.setTransform(194, 163);

        var lineArr = [];
        var lineX = [84, 84, 84, 84, 84, 84, 249, 249, 249, 249, 249];
        var lineY = [22, 48, 75, 102, 129, 156, 22, 48, 75, 102, 129];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var TxtArr = ['A', 'A', 'M', 'R', 'T', 'R', 'T', 'K', 'O', 'I', 'E'];
        var TArr = [];
        var TxtlineX = [140, 140, 139, 140, 140, 140, 304, 304, 303, 305, 304];
        var TxtlineY = [58, 84, 111, 138, 165, 192, 57, 84, 111, 138, 165];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var textbox_group1 = new cjs.Shape();

        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (column == 0 && row > 5) {
                    continue;
                } else if (column == 1 && row == 5) {
                    continue;
                }

                textbox_group1.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(135 + (columnSpace * 163), 40 + (row * 27), 20, 22);
            }
        }
        textbox_group1.setTransform(0, 0);


        this.textbox_group2 = new cjs.Shape();
        for (var column = 1; column < 12; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(2 + (columnSpace * 28.5), 250, 20, 22);
        }
        this.textbox_group2.setTransform(0, 0);


        var arrText = [];

        var ToBeAdded = [];

        for (var i = 0; i < arrText.length; i++) {
            var columnSpace = i;
            var text = arrText[i];

            if (i == 2) {
                columnSpace = 2.109;
            } else if (i == 3) {
                columnSpace = 3.05;
            } else if (i == 4) {
                columnSpace = 4.08;
            } else if (i == 5) {
                columnSpace = 5.1;
            } else if (i == 6) {
                columnSpace = 6.1;
            } else if (i == 7) {
                columnSpace = 7.15;
            } else if (i == 8) {
                columnSpace = 8.17;
            } else if (i == 9) {
                columnSpace = 9.18;
            } else if (i == 10) {
                columnSpace = 10.19;
            } else if (i == 11) {
                columnSpace = 11.25;
            }

            this.temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            this.temp_label.setTransform(35 + (columnSpace * 28), 261.5);
            ToBeAdded.push(this.temp_label);
        }

        for (var i = 1; i < 12; i++) {
            var columnSpace = i;
            var text = (i + 9).toString();

            if (i == 2) {
                columnSpace = 1.87;
            } else if (i == 3) {
                columnSpace = 2.78;
            } else if (i == 4) {
                columnSpace = 3.75;
            } else if (i == 5) {
                columnSpace = 4.66;
            } else if (i == 6) {
                columnSpace = 5.59;
            } else if (i == 7) {
                columnSpace = 6.49;
            } else if (i == 8) {
                columnSpace = 7.35;
            } else if (i == 9) {
                columnSpace = 8.34;
            } else if (i == 10) {
                columnSpace = 9.25;
            } else if (i == 11) {
                columnSpace = 10.07;
            } else if (i == 12) {
                columnSpace = 10.87;
            }
            this.temp_label = new cjs.Text(text, "14px 'Myriad Pro'");
            this.temp_label.setTransform(2 + (columnSpace * 31), 245);
            ToBeAdded.push(this.temp_label);
        }


        this.addChild(this.roundRect1, this.text, this.text_1, textbox_group1, this.textbox_group2, this.label1, this.label2, this.label3,
            this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        this.addChild(this.instance)
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Askarna och locken finns i 3 färger.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 7);

        this.text_1 = new cjs.Text("7.", "bold 16px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 7);

        this.text_2 = new cjs.Text(" Måla alla olika kombinationer med ask och lock.", "16px 'Myriad Pro'");
        this.text_2.setTransform(19, 30);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 20, 510, 197, 10);
        this.roundRect1.setTransform(0, 25);

        this.instance = new lib.p22_2();
        this.instance.setTransform(15, 45, 0.47, 0.47);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 245);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 612, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 65, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
