(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1219,
	height: 678,
	fps: 20,
	color: "#FFFFFF",
	manifest: [
		{src:"images/s26_27_1.png", id:"s26_27_1"},
		{src:"images/s26_27_2.png", id:"s26_27_2"},
		{src:"images/s26_27_28.png", id:"s26_27_28"},
		{src:"images/s26_27_4.png", id:"s26_27_4"}
	]
};

(lib.s26_27_1 = function() {
	this.initialize(img.s26_27_1);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,982,391);
(lib.s26_27_2 = function() {
	this.initialize(img.s26_27_2);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,993,408);
(lib.s26_27_28 = function() {
	this.initialize(img.s26_27_28);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,179,146);
(lib.s26_27_4 = function() {
	this.initialize(img.s26_27_4);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,890,237);


(lib.Symbol29 = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("25", "12px 'Myriad Pro'", "#FFFFFF");
	this.text.lineHeight = 18;
	this.text.setTransform(555,5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
	this.shape.setTransform(579,18.4);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,1218.9,35.1);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.fontfix = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.text = new cjs.Text(" Rita 1 cirkel mindre än antalet djur.", "16px 'Myriad Pro'");
	this.text.lineHeight = 19;
	this.text.setTransform(0,6.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.virtualBounds = new cjs.Rectangle(0,0,344.1,25.3);


(lib.Symbol5 = function() {
	this.initialize();

	// Layer 2
	

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape.setTransform(444.1,263.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_1.setTransform(415.7,263.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_2.setTransform(387.3,263.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_3.setTransform(359,263.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_4.setTransform(330.5,263.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_5.setTransform(458.2,249.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_6.setTransform(429.9,249.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_7.setTransform(444.1,235.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_8.setTransform(401.5,249.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_9.setTransform(415.7,235.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_10.setTransform(373.2,249.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_11.setTransform(387.3,235.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_12.setTransform(344.8,249.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_13.setTransform(359,235.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_14.setTransform(316.5,249.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_15.setTransform(330.5,235.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_16.setTransform(458.2,221.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_17.setTransform(429.9,221.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_18.setTransform(444.1,207.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_19.setTransform(401.5,221.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_20.setTransform(415.7,207.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_21.setTransform(373.2,221.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_22.setTransform(387.3,207.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_23.setTransform(344.8,221.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_24.setTransform(359,207.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_25.setTransform(316.5,221.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_26.setTransform(330.5,207.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_27.setTransform(181.5,263.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_28.setTransform(153.1,263.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_29.setTransform(124.7,263.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_30.setTransform(96.4,263.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_31.setTransform(67.9,263.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_32.setTransform(195.6,249.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_33.setTransform(167.3,249.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_34.setTransform(181.5,235.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_35.setTransform(138.9,249.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_36.setTransform(153.1,235.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_37.setTransform(110.6,249.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_38.setTransform(124.7,235.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_39.setTransform(82.2,249.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_40.setTransform(96.4,235.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_41.setTransform(53.9,249.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_42.setTransform(67.9,235.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_43.setTransform(195.6,221.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_44.setTransform(167.3,221.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_45.setTransform(181.5,207.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_46.setTransform(138.9,221.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_47.setTransform(153.1,207.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_48.setTransform(110.6,221.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_49.setTransform(124.7,207.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_50.setTransform(82.2,221.2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_51.setTransform(96.4,207.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_52.setTransform(53.9,221.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_53.setTransform(67.9,207.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_54.setTransform(444.1,123.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_55.setTransform(415.7,123.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_56.setTransform(387.3,123.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_57.setTransform(359,123.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_58.setTransform(330.5,123.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_59.setTransform(458.2,109.2);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_60.setTransform(429.9,109.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_61.setTransform(444.1,95.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_62.setTransform(401.5,109.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_63.setTransform(415.7,95.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_64.setTransform(373.2,109.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_65.setTransform(387.3,95.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_66.setTransform(344.8,109.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_67.setTransform(359,95.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_68.setTransform(316.5,109.2);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_69.setTransform(330.5,95.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_70.setTransform(458.2,80.9);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_71.setTransform(429.9,80.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_72.setTransform(444.1,66.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_73.setTransform(401.5,80.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_74.setTransform(415.7,66.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_75.setTransform(373.2,80.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_76.setTransform(387.3,66.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_77.setTransform(344.8,80.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_78.setTransform(359,66.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_79.setTransform(316.5,80.9);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_80.setTransform(330.5,66.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_81.setTransform(181.5,123.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_82.setTransform(153.1,123.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_83.setTransform(124.7,123.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_84.setTransform(96.4,123.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_85.setTransform(67.9,123.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_86.setTransform(195.6,109.2);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_87.setTransform(167.3,109.2);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_88.setTransform(181.5,95.1);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_89.setTransform(138.9,109.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_90.setTransform(153.1,95.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_91.setTransform(110.6,109.2);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_92.setTransform(124.7,95.1);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_93.setTransform(82.2,109.2);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_94.setTransform(96.4,95.1);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_95.setTransform(53.9,109.2);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_96.setTransform(67.9,95.1);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_97.setTransform(195.6,80.9);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_98.setTransform(167.3,80.9);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_99.setTransform(181.5,66.7);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_100.setTransform(138.9,80.9);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_101.setTransform(153.1,66.7);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_102.setTransform(110.6,80.9);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_103.setTransform(124.7,66.7);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_104.setTransform(82.2,80.9);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_105.setTransform(96.4,66.7);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_106.setTransform(53.9,80.9);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_107.setTransform(67.9,66.7);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABIIAAQ6IACASQAEAVAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAAw6QAAgdgNgcQgdg5hHAAg");
	this.shape_108.setTransform(384.4,65.6);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABIIAAQ6IACASQAEAVAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAAw6QAAgdgPgcQgcg5hHAAg");
	this.shape_109.setTransform(125.9,65.6);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABHIAAQ6IACASQAEAVAIASQAdA5BHAAMAjxAAAQAcAAAdgOQA4gdAAhHIAAw6QAAgcgNgcQgdg5hHAAg");
	this.shape_110.setTransform(384.4,205.8);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABHIAAQ6IACASQAEAVAIASQAdA5BHAAMAjxAAAQAdAAAcgOQA5gdAAhHIAAw6QAAgcgPgcQgcg5hHAAg");
	this.shape_111.setTransform(125.9,205.8);

	this.instance = new lib.s26_27_2();
	this.instance.setTransform(8.6,1,0.5,0.5);

	this.addChild(this.instance,this.shape_111,this.shape_110,this.shape_109,this.shape_108,this.shape_107,this.shape_106,this.shape_105,this.shape_104,this.shape_103,this.shape_102,this.shape_101,this.shape_100,this.shape_99,this.shape_98,this.shape_97,this.shape_96,this.shape_95,this.shape_94,this.shape_93,this.shape_92,this.shape_91,this.shape_90,this.shape_89,this.shape_88,this.shape_87,this.shape_86,this.shape_85,this.shape_84,this.shape_83,this.shape_82,this.shape_81,this.shape_80,this.shape_79,this.shape_78,this.shape_77,this.shape_76,this.shape_75,this.shape_74,this.shape_73,this.shape_72,this.shape_71,this.shape_70,this.shape_69,this.shape_68,this.shape_67,this.shape_66,this.shape_65,this.shape_64,this.shape_63,this.shape_62,this.shape_61,this.shape_60,this.shape_59,this.shape_58,this.shape_57,this.shape_56,this.shape_55,this.shape_54,this.shape_53,this.shape_52,this.shape_51,this.shape_50,this.shape_49,this.shape_48,this.shape_47,this.shape_46,this.shape_45,this.shape_44,this.shape_43,this.shape_42,this.shape_41,this.shape_40,this.shape_39,this.shape_38,this.shape_37,this.shape_36,this.shape_35,this.shape_34,this.shape_33,this.shape_32,this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.ra1,this.ra2,this.ra3,this.ra4);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(-1,-1,512.3,304);


(lib.Symbol4 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.setTransform(186.3,13.6,1,1,0,0,0,171.9,12.6);

	

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape.setTransform(446.1,297);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_1.setTransform(417.7,297);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_2.setTransform(389.3,297);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_3.setTransform(361,297);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_4.setTransform(332.5,297);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_5.setTransform(460.2,282.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_6.setTransform(431.9,282.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_7.setTransform(446.1,268.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_8.setTransform(403.5,282.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_9.setTransform(417.7,268.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_10.setTransform(375.2,282.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_11.setTransform(389.3,268.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_12.setTransform(346.8,282.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_13.setTransform(361,268.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_14.setTransform(318.5,282.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_15.setTransform(332.5,268.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_16.setTransform(460.2,254.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_17.setTransform(431.9,254.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_18.setTransform(446.1,240.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_19.setTransform(403.5,254.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_20.setTransform(417.7,240.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_21.setTransform(375.2,254.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_22.setTransform(389.3,240.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_23.setTransform(346.8,254.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_24.setTransform(361,240.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_25.setTransform(318.5,254.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_26.setTransform(332.5,240.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_27.setTransform(183.5,297);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_28.setTransform(155.1,297);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_29.setTransform(126.7,297);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_30.setTransform(98.4,297);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_31.setTransform(69.9,297);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_32.setTransform(197.6,282.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_33.setTransform(169.3,282.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_34.setTransform(183.5,268.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_35.setTransform(140.9,282.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_36.setTransform(155.1,268.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_37.setTransform(112.6,282.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_38.setTransform(126.7,268.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_39.setTransform(84.2,282.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_40.setTransform(98.4,268.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_41.setTransform(55.9,282.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_42.setTransform(69.9,268.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_43.setTransform(197.6,254.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_44.setTransform(169.3,254.5);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_45.setTransform(183.5,240.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_46.setTransform(140.9,254.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_47.setTransform(155.1,240.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_48.setTransform(112.6,254.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_49.setTransform(126.7,240.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_50.setTransform(84.2,254.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_51.setTransform(98.4,240.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_52.setTransform(55.9,254.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_53.setTransform(69.9,240.3);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_54.setTransform(446.1,155.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_55.setTransform(417.7,155.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_56.setTransform(389.3,155.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_57.setTransform(361,155.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_58.setTransform(332.5,155.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_59.setTransform(460.2,141.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_60.setTransform(431.9,141.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_61.setTransform(446.1,127.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_62.setTransform(403.5,141.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_63.setTransform(417.7,127.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_64.setTransform(375.2,141.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_65.setTransform(389.3,127.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_66.setTransform(346.8,141.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_67.setTransform(361,127.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_68.setTransform(318.5,141.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_69.setTransform(332.5,127.5);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_70.setTransform(460.2,113.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_71.setTransform(431.9,113.3);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_72.setTransform(446.1,99.2);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_73.setTransform(403.5,113.3);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_74.setTransform(417.7,99.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_75.setTransform(375.2,113.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_76.setTransform(389.3,99.2);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_77.setTransform(346.8,113.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_78.setTransform(361,99.2);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_79.setTransform(318.5,113.3);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_80.setTransform(332.5,99.2);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_81.setTransform(183.5,155.9);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_82.setTransform(155.1,155.9);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_83.setTransform(126.7,155.9);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_84.setTransform(98.4,155.9);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_85.setTransform(69.9,155.9);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_86.setTransform(197.6,141.7);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_87.setTransform(169.3,141.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_88.setTransform(183.5,127.5);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_89.setTransform(140.9,141.7);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_90.setTransform(155.1,127.5);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_91.setTransform(112.6,141.7);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_92.setTransform(126.7,127.5);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_93.setTransform(84.2,141.7);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_94.setTransform(98.4,127.5);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_95.setTransform(55.9,141.7);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_96.setTransform(69.9,127.5);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_97.setTransform(197.6,113.3);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_98.setTransform(169.3,113.3);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_99.setTransform(183.5,99.2);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_100.setTransform(140.9,113.3);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_101.setTransform(155.1,99.2);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_102.setTransform(112.6,113.3);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_103.setTransform(126.7,99.2);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_104.setTransform(84.2,113.3);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_105.setTransform(98.4,99.2);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_106.setTransform(55.9,113.3);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_107.setTransform(69.9,99.2);

	this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#00A5C0");
	this.text.lineHeight = 20;
	this.text.setTransform(0,6);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgdAAgcAOQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAAw6QAAgdgNgcQgdg5hHAAg");
	this.shape_108.setTransform(386.4,97.9);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgdAAgcAOQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA5gcAAhHIAAw6QAAgdgPgcQgcg5hHAAg");
	this.shape_109.setTransform(127.9,97.9);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgdAAgcAOQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAAw6QAAgdgNgcQgdg5hHAAg");
	this.shape_110.setTransform(386.4,238.1);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVAEgSAIQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAAw6QAAgdgPgcQgcg5hHAAg");
	this.shape_111.setTransform(127.9,238.1);

	this.instance_1 = new lib.s26_27_1();
	this.instance_1.setTransform(16.1,43.6,0.5,0.5);

	this.addChild(this.instance_1,this.shape_111,this.shape_110,this.shape_109,this.shape_108,this.text,this.shape_107,this.shape_106,this.shape_105,this.shape_104,this.shape_103,this.shape_102,this.shape_101,this.shape_100,this.shape_99,this.shape_98,this.shape_97,this.shape_96,this.shape_95,this.shape_94,this.shape_93,this.shape_92,this.shape_91,this.shape_90,this.shape_89,this.shape_88,this.shape_87,this.shape_86,this.shape_85,this.shape_84,this.shape_83,this.shape_82,this.shape_81,this.shape_80,this.shape_79,this.shape_78,this.shape_77,this.shape_76,this.shape_75,this.shape_74,this.shape_73,this.shape_72,this.shape_71,this.shape_70,this.shape_69,this.shape_68,this.shape_67,this.shape_66,this.shape_65,this.shape_64,this.shape_63,this.shape_62,this.shape_61,this.shape_60,this.shape_59,this.shape_58,this.shape_57,this.shape_56,this.shape_55,this.shape_54,this.shape_53,this.shape_52,this.shape_51,this.shape_50,this.shape_49,this.shape_48,this.shape_47,this.shape_46,this.shape_45,this.shape_44,this.shape_43,this.shape_42,this.shape_41,this.shape_40,this.shape_39,this.shape_38,this.shape_37,this.shape_36,this.shape_35,this.shape_34,this.shape_33,this.shape_32,this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.ra1,this.ra2,this.ra3,this.ra4,this.instance);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,304);


// stage content:
(lib.p25 = function() {
	this.initialize();

	// Layer 1
	this.other = new lib.Symbol29();
	this.other.setTransform(609.5,659.9,1,1,0,0,0,609.5,17.5);

	this.v2 = new lib.Symbol5();
	this.v2.setTransform(313.6,499.4,1,1,0,0,0,255.1,135.7);

	this.v1 = new lib.Symbol4();
	this.v1.setTransform(312.8,202.5,1,1,0,0,0,256.3,151.9);

	this.addChild(this.v1,this.v2,this.other);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(609.5,389.6,1218.9,626.9);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;