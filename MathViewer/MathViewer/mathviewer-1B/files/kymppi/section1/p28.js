(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p28_1.png",
            id: "p28_1"
        }, {
            src: "images/p28_2.png",
            id: "p28_2"
        }, {
            src: "images/p28_3.png",
            id: "p28_3"
        }]
    };

    (lib.p28_1 = function() {
        this.initialize(img.p28_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p28_2 = function() {
        this.initialize(img.p28_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p28_3 = function() {
        this.initialize(img.p28_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);


    (lib.Symbol1 = function() {
        this.initialize();

        this.pageTitle = new cjs.Text("De två tornen", "18px 'MyriadPro-Semibold'", "#A1C745");
        this.pageTitle.setTransform(40, 62);

        this.text_1 = new cjs.Text("Spel för 2 eller fler.", "16px 'Myriad Pro'", "#BBBF22");
        this.text_1.setTransform(40, 85);

        this.text_2 = new cjs.Text("28", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(39, 658);

        this.text_8 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'", "#000000");
        this.text_8.setTransform(379, 77);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#ABCD59").ss(0.7).drawRoundRect(373, 27, 183, 40, 10);
        this.roundRect1.setTransform(0, 25);

        this.instance = new lib.p28_1();
        this.instance.setTransform(450, 50, 0.47, 0.47);

        this.addChild(this.pageTitle, this.pageFooterTextRight, this.shape_2, this.shape_1,
            this.text_1, this.text_2, this.roundRect1, this.instance, this.text_8);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("•  Slå 2 tärningar.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 30);

        this.text_3 = new cjs.Text("•  Tärningstalen är lika med antal kronor.", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(19, 52);

        this.text_4 = new cjs.Text("•  Lägg ihop tärningstalen,", "16px 'Myriad Pro'");
        this.text_4.setTransform(19, 74);

        this.text_5 = new cjs.Text("   eller använd dem var för sig,", "16px 'Myriad Pro'");
        this.text_5.setTransform(19, 96);

        this.text_6 = new cjs.Text("•  Kryssa rutan på din spelplan.", "16px 'Myriad Pro'");
        this.text_6.setTransform(19, 118);

        this.text_7 = new cjs.Text("•  Den som först har kryssat alla rutor på spelplanen vinner.", "16px 'Myriad Pro'");
        this.text_7.setTransform(19, 140);

        this.text_8 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_8.setTransform(223, 74);

        this.text_9 = new cjs.Text("= 6 kr", "16px 'Myriad Pro'");
        this.text_9.setTransform(265, 74);

        this.text_10 = new cjs.Text("= 4 kr och", "16px 'Myriad Pro'");
        this.text_10.setTransform(247, 96);

        this.text_11 = new cjs.Text("= 2 kr.", "16px 'Myriad Pro'");
        this.text_11.setTransform(344, 96);

        this.instance = new lib.p28_2();
        this.instance.setTransform(180, 40, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(5, 3, 513, 540, 10);
        this.roundRect1.setTransform(0, 5);

        this.instance2 = new lib.p28_3();
        this.instance2.setTransform(0, 145, 0.47, 0.47);

        this.text_1 = new cjs.Text("Spelare 1", "16px 'Myriad Pro'");
        this.text_1.setTransform(110, 185);

        this.text_2 = new cjs.Text("Spelare 2", "16px 'Myriad Pro'");
        this.text_2.setTransform(363, 183);



        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f('').s("#90BD22").ss(0.7).drawRoundRect(27, 300, 216, 217, 0);
        this.roundRect2.setTransform(0, 20);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f('').s("#90BD22").ss(0.7).drawRoundRect(280, 300, 216, 217, 0);
        this.roundRect3.setTransform(0, 20);

        var lineHArr = [];
        var lineHX = [28, 28, 281, 281];
        var lineHY = [353, 423, 353, 423];

        for (var row = 0; row < lineHX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#90BD22").setStrokeStyle(0.5).moveTo(0, 40).lineTo(215, 40);
            hrLine_1.setTransform(lineHX[row], lineHY[row]);
            lineHArr.push(hrLine_1);
        }

        var lineVArr = [];
        var lineVX = [60, 131, 313, 383];
        var lineVY = [320, 320, 320, 320];

        for (var row = 0; row < lineVX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#90BD22").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 217);
            vrLine_1.setTransform(lineVX[row], lineVY[row]);
            lineVArr.push(vrLine_1);
        }

        this.addChild(this.roundRect1, this.text, this.instance, this.roundRect1,
            this.roundRect2, this.roundRect3, this.instance2, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7,
            this.text_8,this.text_9,this.text_10,this.text_11);

        for (var i = 0; i < lineHArr.length; i++) {
            this.addChild(lineHArr[i]);
        }

        for (var i = 0; i < lineVArr.length; i++) {
            this.addChild(lineVArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 515, 540);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 90, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
