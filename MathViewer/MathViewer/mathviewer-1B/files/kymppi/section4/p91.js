(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p91_1.png",
            id: "p91_1"
        }]
    };

    (lib.p91_1 = function() {
        this.initialize(img.p91_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("91", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(15, 20);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Mät med klossar och skriv längden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 330, 10);
        this.roundRect1.setTransform(10, 20);

        this.instance = new lib.p91_1();
        this.instance.setTransform(22, 31, 0.47, 0.47);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("#B6D06C").s('#000000').drawRoundRect(0, 0, 137, 22, 0);
        this.Rect1.setTransform(92, 84);

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("#B6D06C").s('#000000').drawRoundRect(0, 0, 90.5, 22, 0);
        this.Rect2.setTransform(190, 185);

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("#B6D06C").s('#000000').drawRoundRect(0, 0, 114, 22, 0);
        this.Rect3.setTransform(378, 185);

        this.Rect4 = new cjs.Shape();
        this.Rect4.graphics.f("#B6D06C").s('#000000').drawRoundRect(0, 0, 181, 22, 0);
        this.Rect4.setTransform(310, 284);

        this.Rect5 = new cjs.Shape();
        this.Rect5.graphics.f("#B6D06C").s('#000000').drawRoundRect(0, 0, 22, 69, 0);
        this.Rect5.setTransform(370, 45);

        this.Rect6 = new cjs.Shape();
        this.Rect6.graphics.f("#B6D06C").s('#000000').drawRoundRect(0, 0, 22, 112, 0);
        this.Rect6.setTransform(100, 178);

        var vrlineArr = [];
        var vrlineX = [94, 117, 140, 163, 186, 192, 215, 238, 381, 404, 427, 450, 313, 336, 359, 382, 405, 428, 451];
        var vrlineY = [84, 84, 84, 84, 84, 185, 185, 185, 185, 185, 185, 185, 284, 284, 284, 284, 284, 284, 284];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(20, 0).lineTo(20, 22);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            vrlineArr.push(vrLine_1);
        }

        var hrlineArr = [];
        var hrlineX = [100, 100, 100, 100, 370, 370];
        var hrlineY = [180, 203, 226, 249, 48, 71];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(0, 20).lineTo(22, 20);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        var lineArr = [];
        var lineX = [97, 302, 190, 383, 32, 353];
        var lineY = [116, 122, 214, 214, 299, 313];

        for (var row = 0; row < lineX.length; row++) {
            var Line_1 = new cjs.Shape();
            Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 20).lineTo(30, 20);
            Line_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(Line_1);
        }


        var TextArr = [];
        var TextX = [130, 335, 222, 416, 64, 385];
        var TextY = [134, 140, 232, 232, 318, 332];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("klossar", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.Rect1, this.Rect2, this.Rect3, this.Rect4, this.Rect5, this.Rect6);

        for (var i = 0; i < vrlineArr.length; i++) {
            this.addChild(vrlineArr[i]);
        }
        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 350);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(10, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 510, 180, 10);
        this.roundRect1.setTransform(10, 12);

        var lineArr = [];
        var lineX = [86, 252, 445, 86, 252, 445, 86, 252, 445, 86, 252, 445, 86, 252, 445];
        var lineY = [34, 34, 34, 59, 59, 59, 84, 84, 84, 112, 112, 112, 139, 139, 139];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(38, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var TxtArr = ['16', '15', '17', '15', '16', '7', '9', '8', '7', '8', '17', '18', '15', '16', '15', '9', '9', '8', '9', '6',

            '6', '8', '7', '6', '6', '7', '9', '8', '9', '8', '2', '3', '4', '3', '6'
        ];

        var TArr = [];
        var TxtlineX = [36, 36, 36, 36, 36, 69, 69, 69, 69, 69, 200, 200, 200, 200, 200, 235, 235, 235, 235, 235,
            369, 369, 369, 369, 369, 399, 399, 399, 399, 399, 428, 428, 428, 428, 428
        ];

        var TxtlineY = [69, 95, 121, 147, 173, 69, 95, 121, 147, 173, 69, 95, 121, 147, 173, 69, 95, 121, 147, 173, 69, 95, 121, 147, 173, 69, 95, 121, 147, 173,
            69, 95, 121, 147, 173,
        ];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }


        var symblArr = ['–', '–', '–', '–', '–', '=', '=', '=', '=', '=', '–', '–', '–', '–', '–', '=', '=', '=', '=', '=',
            '+', '+', '+', '+', '+', '+', '+', '+', '+', '+', '=', '=', '=', '=', '='
        ];

        var SymArr = [];
        var symbllineX = [57, 57, 57, 57, 57, 84, 84, 84, 84, 84, 222, 222, 222, 222, 222, 250, 250, 250, 250, 250,

            383, 383, 383, 383, 383, 413, 413, 413, 413, 413, 442, 442, 442, 442, 440
        ];
        var symbllineY = [69, 94, 120, 146, 172, 70, 95, 121, 147, 173, 69, 94, 120, 146, 172, 70, 95, 121, 147, 173,

            69, 94, 120, 146, 172, 69, 94, 120, 146, 172, 69, 94, 120, 146, 172
        ];

        for (var i = 0; i < symbllineX.length; i++) {
            var symbltxt = symblArr[i];
            this.temp_label = new cjs.Text(symbltxt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(symbllineX[i], symbllineY[i]);
            SymArr.push(this.temp_label);
        }


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(5, 20).lineTo(465, 20);
        this.Line_1.setTransform(23, 15);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(123.5, 38);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_3.setTransform(231, 38);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(343, 38);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_5.setTransform(455, 38);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 10) {
                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(55 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 15) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(65 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else {

                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(75 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group1.setTransform(3, 2);


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

        for (var i = 0; i < SymArr.length; i++) {
            this.addChild(SymArr[i]);
        }

        this.addChild(this.text, this.text_1, this.roundRect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 190);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(304, 103, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(304, 498, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
