(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p133_1.png",
            id: "p133_1"
        }, {
            src: "images/p133_2.png",
            id: "p133_2"
        }]
    };

    (lib.p133_1 = function() {
        this.initialize(img.p133_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p133_2 = function() {
        this.initialize(img.p133_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("133", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(537, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(560, 660.8);

        this.instance = new lib.p133_1();
        this.instance.setTransform(32, 21, 0.3805, 0.375);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(454 + (columnSpace * 32), 16, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Hur många kulor är det tillsammans?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 14);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 14);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 259, 90, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(264, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 125);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(264, 125);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 225);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(264, 225);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 325);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(264, 325);

        var rectArr = [];

        // Group 1

        var y1 = 49;
        var y2 = 69;
        for (var j = 0; j < 2; j++) {
            if (j == 1) {
                y1 = 149;
                y2 = 169;
            }
            for (var i = 0; i < 3; i++) {
                //DB2128
                if (j == 0) {
                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(22 + (i * 21), y2);
                    rectArr.push(this.shape_group1);
                } else {
                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(19.1 + (i * 21), y2);
                    rectArr.push(this.shape_group1);
                }

                if (i == 0 || i == 1) {
                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(33 + (i * 21), y1);
                    rectArr.push(this.shape_group1);

                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(292 + (i * 21), y1 - 3);
                    rectArr.push(this.shape_group1);

                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(292 + (i * 21), y2);
                    rectArr.push(this.shape_group1);

                    if (j == 1) {
                        this.shape_group1 = new cjs.Shape();
                        this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                        this.shape_group1.setTransform(118 + (i * 21), y1);
                        rectArr.push(this.shape_group1);
                    } else {
                        this.shape_group1 = new cjs.Shape();
                        this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                        this.shape_group1.setTransform(128 + (i * 21), y1);
                        rectArr.push(this.shape_group1);
                    }
                }
                // FFF579
                if (j == 1) {
                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(107 + (i * 21), y2);
                    rectArr.push(this.shape_group1);
                } else {
                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(117 + (i * 21), y2);
                    rectArr.push(this.shape_group1);
                }


                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(361 + (i * 21), y2);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(361 + (i * 21), y1 - 3);
                rectArr.push(this.shape_group1);


                if (i == 0) {
                    //0094D9 
                    if (j == 0) {
                        this.shape_group1 = new cjs.Shape();
                        this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                        this.shape_group1.setTransform(210, y2);
                        rectArr.push(this.shape_group1);
                    } else {
                        this.shape_group1 = new cjs.Shape();
                        this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                        this.shape_group1.setTransform(197, y2);
                        rectArr.push(this.shape_group1);

                        this.shape_group1 = new cjs.Shape();
                        this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                        this.shape_group1.setTransform(218, y2);
                        rectArr.push(this.shape_group1);

                        this.shape_group1 = new cjs.Shape();
                        this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                        this.shape_group1.setTransform(478, y2);
                        rectArr.push(this.shape_group1);

                    }


                    this.shape_group1 = new cjs.Shape();
                    this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                    this.shape_group1.setTransform(457, y2);
                    rectArr.push(this.shape_group1);
                }

            }

        }


        // Group 3

        y1 = 249;
        y2 = 269;


        for (var i = 0; i < 4; i++) {

            //F1
            //DB2128
            if (i == 0 || i == 1 || i == 2) {
                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(27 + (i * 21), y1 - 3);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(283 + (i * 21), y1 - 3);
                rectArr.push(this.shape_group1);
            }

            if (i == 0) {
                // FFF579
                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(142 + (i * 21), y1);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(407 + (i * 21), y1);
                rectArr.push(this.shape_group1);
            }

            //F1 End 



            this.shape_group1 = new cjs.Shape();
            this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
            this.shape_group1.setTransform(27 + (i * 21), y2);
            rectArr.push(this.shape_group1);

            this.shape_group1 = new cjs.Shape();
            this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
            this.shape_group1.setTransform(283 + (i * 21), y2);
            rectArr.push(this.shape_group1);

            // FFF579
            if (i == 0 || i == 1) {
                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(132 + (i * 21), y2);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(397 + (i * 21), y2);
                rectArr.push(this.shape_group1);
            }

            if (i == 0) {
                //0094D9 

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(199, y2);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(220, y2);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(465, y2);
                rectArr.push(this.shape_group1);
            }

        }



        // Group 4

        y1 = 349;
        y2 = 370;


        for (var i = 0; i < 4; i++) {
            //F1
            this.shape_group1 = new cjs.Shape();
            this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
            this.shape_group1.setTransform(27 + (i * 21), y1);
            rectArr.push(this.shape_group1);

            this.shape_group1 = new cjs.Shape();
            this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
            this.shape_group1.setTransform(341 + (i * 21), y1);
            rectArr.push(this.shape_group1);

            // FFF579
            if (i == 0) {

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(148 + (i * 21), y1);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(293 + (i * 21), y1);
                rectArr.push(this.shape_group1);

            }


            //F1 End

            this.shape_group1 = new cjs.Shape();
            this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
            this.shape_group1.setTransform(27 + (i * 21), y2);
            rectArr.push(this.shape_group1);

            this.shape_group1 = new cjs.Shape();
            this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
            this.shape_group1.setTransform(341 + (i * 21), y2);
            rectArr.push(this.shape_group1);

            // FFF579
            if (i == 0) {

                //0094D9                
                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(199, y2);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#FFF579").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(148 + (i * 21), y2 - 1.5);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#DB2128").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(293 + (i * 21), y2 - 1.5);
                rectArr.push(this.shape_group1);

            }
            if (i == 0 || i == 1) {
                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(450, y2);
                rectArr.push(this.shape_group1);

                this.shape_group1 = new cjs.Shape();
                this.shape_group1.graphics.f("#0094D9").s("#616161").ss(0.8, 0, 0, 4).arc(10, 10, 8.2, 0, 2 * Math.PI);
                this.shape_group1.setTransform(471, y2);
                rectArr.push(this.shape_group1);
            }

        }

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['98', '198', '298', '398'];
        var xPos = 19.5,
            padding,
            yPos,
            rectWidth = 171.4,
            rectHeight = 23,
            boxWidth = 21.5,
            boxHeight = 23,
            numberOfBoxes = 8,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 23;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);


        this.text_1 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(43, 88.2);
        rectArr.push(this.text_1);
        this.text_2 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(65, 88.2);
        rectArr.push(this.text_2);
        this.text_3 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(85, 88.2);
        rectArr.push(this.text_3);
        this.text_4 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(107, 88.2);
        rectArr.push(this.text_4);
        this.text_5 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 45;
        this.text_5.setTransform(128, 88.2);
        rectArr.push(this.text_5);
        this.text_6 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 45;
        this.text_6.setTransform(150, 88.2);
        rectArr.push(this.text_6);

        //text 2
        this.text_7 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 45;
        this.text_7.setTransform(308, 88.2);
        rectArr.push(this.text_7);
        this.text_8 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 45;
        this.text_8.setTransform(330, 88.2);
        rectArr.push(this.text_8);
        this.text_9 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 45;
        this.text_9.setTransform(350, 88.2);
        rectArr.push(this.text_9);
        this.text_10 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_10.lineHeight = 45;
        this.text_10.setTransform(372, 88.2);
        rectArr.push(this.text_10);
        this.text_11 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_11.lineHeight = 45;
        this.text_11.setTransform(393, 88.2);
        rectArr.push(this.text_11);
        this.text_12 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_12.lineHeight = 45;
        this.text_12.setTransform(415, 88.2);
        rectArr.push(this.text_12);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8);
        this.addChild(this.textbox_group1);
        this.addChild(this.text_q1, this.text_q2, this.shape_group1, this.shape_group2);
        this.addChild(this.shape_group3, this.shape_group4, this.shape_group5, this.shape_group6);
        this.addChild(this.shape_group7, this.shape_group8, this.shape_group9, this.shape_group10, this.shape_group11, this.shape_group12)

        for (var rect = 0; rect < rectArr.length; rect++) {
            this.addChild(rectArr[rect]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(628.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(306, 424, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
