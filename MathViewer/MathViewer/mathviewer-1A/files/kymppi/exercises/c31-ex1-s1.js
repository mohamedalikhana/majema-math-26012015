var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_3.png",
            id: "c2_ex1_3"
        }]
    };

    (lib.c2_ex1_3 = function() {
        this.initialize(img.c2_ex1_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.instrText = new lib.instructions();
        this.instrText.setTransform(0, 0);

        this.addBall = new lib.insertBall_1(7);
        this.addBall.setTransform(100 + 160, 25, 0.89, 0.89);

        this.answers = new lib.answers();
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(185 + 160, 30, 3, 3, 0, 0, 0, 0, 0);

        this.addChild(this.addBall, this.instrText, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        // this.stage1 = new lib.Stage1();
        this.instrText = new lib.instructions();
        this.instrText.setTransform(0, 0);

        this.addBall = new lib.insertBall_1(7);
        this.addBall.setTransform(100 + 160, 25, 0.89, 0.89);

        this.answers = new lib.answers();
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(185 + 160, 30, 3, 3, 0, 0, 0, 0, 0);

        this.instrText2 = new cjs.Text("Vilket tal ska vi subtrahera med sedan?", "20px 'Myriad Pro'", "#155EAE")
        this.instrText2.lineHeight = 19;
        this.instrText2.textAlign = 'center';
        this.instrText2.setTransform(550 + 150, 450);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#2F66B2").setStrokeStyle(3.5).moveTo(848 + 160, 0).lineTo(768 + 160, 140);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#2F66B2").setStrokeStyle(3.5).moveTo(968 + 160, 0).lineTo(888 + 160, 140);
        this.Line_2.setTransform(0, 0);

        this.tweens = [];

        // this.answers.text_2.shadow = new cjs.Shadow('red', 0, 0, 0)
        this.answers.text_2.color = "red";
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 0,
            // shadow: {
            //     blurFrom: 0,
            //     blurTo: 100
            // },
            alphaTimeout: 200
        });

        // this.answers.text_3.shadow = new cjs.Shadow('red', 0, 0, 0)
        this.answers.text_3.color = "red";
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 0,
            // shadow: {
            //     blurFrom: 0,
            //     blurTo: 100
            // },
            alphaTimeout: 200
        });

        this.tweens.push({
            ref: this.Line_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.Line_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.instrText,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 4450,
            alphaTimeout: 1000
        });

        this.tweens.push({
            ref: this.instrText2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 4650,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.addBall, this.instrText, this.answers, this.Line_1, this.Line_2, this.instrText2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();

        this.answers = new lib.answers();
        this.answers.text_2.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_5.visible = true;
        this.answers.setTransform(185 + 160, 30, 3, 3, 0, 0, 0, 0, 0);

        this.stage2 = new lib.Stage2();
        this.stage2.instrText.visible = false;

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#2F66B2").setStrokeStyle(3.5).moveTo(588 + 160, 0).lineTo(668 + 160, 140);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#2F66B2").setStrokeStyle(3.5).moveTo(469 + 160, 0).lineTo(549 + 160, 140);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#2F66B2").setStrokeStyle(3.5).moveTo(349 + 160, 0).lineTo(429 + 160, 140);
        this.Line_4.setTransform(0, 0);

        this.tweens = [];

        this.answers.text_2.color = "#000000";
        this.answers.text_3.color = "#000000";

        // this.answers.text_4.shadow = new cjs.Shadow('red', 0, 0, 0)
        this.answers.text_4.color = "red";
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 0,
            // shadow: {
            //     blurFrom: 0,
            //     blurTo: 100
            // },
            alphaTimeout: 200
        });

        // this.answers.text_5.shadow = new cjs.Shadow('red', 0, 0, 0)
        this.answers.text_5.color = "red";
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 0,
            // shadow: {
            //     blurFrom: 0,
            //     blurTo: 100
            // },
            alphaTimeout: 200
        });

        this.tweens.push({
            ref: this.Line_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.Line_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.Line_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;

        this.addChild(this.stage2, this.Line_2, this.Line_3, this.Line_4, this.answers, this.instrText);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();

        this.stage3 = new lib.Stage3();

        this.answers = new lib.answers();
        this.answers.text_7.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_5.visible = true;
        this.answers.setTransform(185 + 160, 30, 3, 3, 0, 0, 0, 0, 0);

        this.answers.text_4.color = "#000000";
        this.answers.text_5.color = "#000000";

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_7,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage3, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Subtrahera med tre tal", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("31", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.insertBall_1 = function(count) {
        this.initialize();
        this.greenBall = new lib.c2_ex1_3();
        this.greenBalls = new cjs.Container();

        for (var i = 0; i < count; i++) {
            var colSpace = (i > 4) ? colSpace = i + 0.48 : i;

            var greenBall = this.greenBall.clone(true);
            greenBall.setTransform(0 + (135 * colSpace), 0, 0.9, 0.9);
            this.greenBalls.addChild(greenBall);
        }

        this.addChild(this.greenBalls);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);




    (lib.rectangles = function() {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();

        for (var j = 0; j < 6; j++) {
            boxes.graphics.f('#ffffff').s('#000000').ss(0.7).moveTo(0 + (30 * j), 0).lineTo(0 + (30 * j), 50);
        };
        for (var i = 0; i < 3; i++) {
            boxes.graphics.f('#ffffff').s('#000000').ss(0.7).moveTo(0, 0 + (25 * i)).lineTo(150, 0 + (25 * i));
        };

        boxes.setTransform(0, 0);
        this.addChild(boxes);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("7", "26px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(35, 60);

        this.text_2 = new cjs.Text("–", "26px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(60, 60);

        this.text_3 = new cjs.Text("2", "26px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(85, 60);

        this.text_4 = new cjs.Text("–", "26px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(110, 60);

        this.text_5 = new cjs.Text("3", "26px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(135, 60);

        this.text_6 = new cjs.Text("=", "26px 'Myriad Pro'");
        this.text_6.visible = false;
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(160, 60);

        this.text_7 = new cjs.Text("2", "26px 'Myriad Pro'");
        this.text_7.visible = false;
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(185, 60);

        this.addChild(this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.instructions = function() {
        this.initialize();

        this.instrText = new cjs.Text("Vilket tal ska subtraheras först?", "20px 'Myriad Pro'", "#155EAE")
        this.instrText.lineHeight = 19;
        this.instrText.textAlign = 'center';
        this.instrText.visible = false;
        this.instrText.setTransform(550 + 150, 450);

        this.addChild(this.instrText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
