(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("142", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(443 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 10);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.dotMatrixRect = function() {
        this.initialize();

        var width = 120,
            height = 239;
        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#00A3C4").ss(1).drawRect(0, 0, width, height);
        this.rect1.setTransform(0, 0);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#00A3C4").setStrokeStyle(1).moveTo(0, height / 2).lineTo(width, height / 2);
        this.hrLine_1.setTransform(0, 0);

        var ToBeAdded = [];
        var colSpace = 17.02;
        var rowSpace = 16.85;
        var startX = 8;
        var startY = 11;
        var dotCount = 0;
        for (var col = 0; col < 7; col++) {

            for (var row = 0; row < 14; row++) {

                var dot = null;
                dot = new cjs.Shape();
                dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 1.25);
                dot.setTransform(startX + (col * colSpace), startY + (row * rowSpace));
                dot.id = dotCount;
                dotCount = dotCount + 1;

                ToBeAdded.push(dot);
            }
        }

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);

        }
        this.dots = ToBeAdded;
        this.addChild(this.rect1, this.hrLine_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("Kluring", "18px 'Myriad Pro'", "#00A3CD");
        this.text.setTransform(1, 12);

        this.text_1 = new cjs.Text(" Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 40);

        this.text_2 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A3CD");
        this.text_2.setTransform(3, 40);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 277, 10);
        this.roundRect1.setTransform(0, 20);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(122, 50);

        this.rectimg_1 = new lib.dotMatrixRect();
        this.rectimg_1.setTransform(295, 50);

        // rectimg_1 shape --- rocket ---

        var dots1 = this.rectimg_1.dots;
        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f('#D51317').s("#9D9D9D").ss(1.2).moveTo(dots1[0].x, dots1[0].y).lineTo(dots1[1].x, dots1[1].y).lineTo(dots1[15].x, dots1[15].y).lineTo(dots1[14].x, dots1[14].y).lineTo(dots1[0].x, dots1[0].y);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(dots1[1].x, dots1[1].y).lineTo(dots1[16].x, dots1[16].y).lineTo(dots1[30].x, dots1[30].y).lineTo(dots1[29].x, dots1[29].y).lineTo(dots1[14].x, dots1[14].y).lineTo(dots1[15].x, dots1[15].y).lineTo(dots1[1].x, dots1[1].y);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.f('#D51317').s("#9D9D9D").ss(1.2).moveTo(dots1[16].x, dots1[16].y).lineTo(dots1[31].x, dots1[31].y).lineTo(dots1[45].x, dots1[45].y).lineTo(dots1[44].x, dots1[44].y).lineTo(dots1[29].x, dots1[29].y).lineTo(dots1[30].x, dots1[30].y).lineTo(dots1[16].x, dots1[16].y);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(dots1[31].x, dots1[31].y).lineTo(dots1[46].x, dots1[46].y).lineTo(dots1[60].x, dots1[60].y).lineTo(dots1[59].x, dots1[59].y).lineTo(dots1[44].x, dots1[44].y).lineTo(dots1[45].x, dots1[45].y).lineTo(dots1[31].x, dots1[31].y);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.f('#D51317').s("#9D9D9D").ss(1.2).moveTo(dots1[46].x, dots1[46].y).lineTo(dots1[61].x, dots1[61].y).lineTo(dots1[75].x, dots1[75].y).lineTo(dots1[74].x, dots1[74].y).lineTo(dots1[59].x, dots1[59].y).lineTo(dots1[60].x, dots1[60].y).lineTo(dots1[46].x, dots1[46].y);

        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(dots1[61].x, dots1[61].y).lineTo(dots1[76].x, dots1[76].y).lineTo(dots1[89].x, dots1[89].y).lineTo(dots1[74].x, dots1[74].y).lineTo(dots1[75].x, dots1[75].y).lineTo(dots1[61].x, dots1[61].y);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(dots1[76].x, dots1[76].y).lineTo(dots1[90].x, dots1[90].y).lineTo(dots1[89].x, dots1[89].y).lineTo(dots1[76].x, dots1[76].y);

        this.Line_8 = new cjs.Shape();
        this.Line_8.graphics.f('#008BD2').s("#9D9D9D").ss(1.2).moveTo(dots1[46].x, dots1[46].y).lineTo(dots1[20].x, dots1[20].y).lineTo(dots1[6].x, dots1[6].y).lineTo(dots1[5].x, dots1[5].y).lineTo(dots1[46].x, dots1[46].y)
            .moveTo(dots1[59].x, dots1[59].y).lineTo(dots1[85].x, dots1[85].y).lineTo(dots1[84].x, dots1[84].y).lineTo(dots1[70].x, dots1[70].y).lineTo(dots1[59].x, dots1[59].y);

        this.Line_9 = new cjs.Shape();
        this.Line_9.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(dots1[21].x, dots1[21].y).lineTo(dots1[96].x, dots1[96].y);

        this.rectimg_1.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7,
            this.Line_8, this.Line_9);

        // rectimg shape  --bird --

        this.Line_10 = new cjs.Shape();
        this.Line_10.graphics.f('#C1833E').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[0].x, this.rectimg.dots[0].y).lineTo(this.rectimg.dots[3].x, this.rectimg.dots[3].y).lineTo(this.rectimg.dots[16].x, this.rectimg.dots[16].y).lineTo(this.rectimg.dots[18].x, this.rectimg.dots[18].y).lineTo(this.rectimg.dots[31].x, this.rectimg.dots[31].y)
            .lineTo(this.rectimg.dots[32].x, this.rectimg.dots[32].y).lineTo(this.rectimg.dots[47].x, this.rectimg.dots[47].y).lineTo(this.rectimg.dots[60].x, this.rectimg.dots[60].y).lineTo(this.rectimg.dots[59].x, this.rectimg.dots[59].y)
            .lineTo(this.rectimg.dots[74].x, this.rectimg.dots[74].y).lineTo(this.rectimg.dots[72].x, this.rectimg.dots[72].y).lineTo(this.rectimg.dots[87].x, this.rectimg.dots[87].y).lineTo(this.rectimg.dots[84].x, this.rectimg.dots[84].y)
            .lineTo(this.rectimg.dots[58].x, this.rectimg.dots[58].y).lineTo(this.rectimg.dots[46].x, this.rectimg.dots[46].y).lineTo(this.rectimg.dots[30].x, this.rectimg.dots[30].y).lineTo(this.rectimg.dots[0].x, this.rectimg.dots[0].y);

        this.Line_11 = new cjs.Shape();
        this.Line_11.graphics.f('#008BD2').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[30].x, this.rectimg.dots[30].y).lineTo(this.rectimg.dots[31].x + 17.02 / 2, this.rectimg.dots[31].y).lineTo(this.rectimg.dots[45].x + 17.02 / 2, this.rectimg.dots[45].y).lineTo(this.rectimg.dots[58].x, this.rectimg.dots[58].y).lineTo(this.rectimg.dots[43].x, dots1[43].y).lineTo(this.rectimg.dots[30].x, dots1[30].y);

        this.Line_12 = new cjs.Shape();
        this.Line_12.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[31].x + 17.02 / 2, this.rectimg.dots[31].y).lineTo(this.rectimg.dots[45].x + 17.02 / 2, this.rectimg.dots[45].y).lineTo(this.rectimg.dots[46].x, this.rectimg.dots[46].y).lineTo(this.rectimg.dots[31].x + 17.02 / 2, this.rectimg.dots[31].y);

        this.Line_13 = new cjs.Shape();
        this.Line_13.graphics.f('#FFF374').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[32].x, this.rectimg.dots[32].y).lineTo(this.rectimg.dots[34].x, this.rectimg.dots[34].y).lineTo(this.rectimg.dots[20].x, this.rectimg.dots[20].y).lineTo(this.rectimg.dots[33].x, dots1[33].y)
            .moveTo(this.rectimg.dots[60].x, dots1[60].y).lineTo(this.rectimg.dots[62].x, dots1[62].y).lineTo(this.rectimg.dots[76].x, dots1[76].y).lineTo(this.rectimg.dots[61].x, dots1[61].y);

        this.Line_14 = new cjs.Shape();
        this.Line_14.graphics.f('').s("#9D9D9D").ss(1.2).moveTo(this.rectimg.dots[37].x, this.rectimg.dots[37].y).lineTo(this.rectimg.dots[50].x, this.rectimg.dots[50].y).lineTo(this.rectimg.dots[65].x, this.rectimg.dots[65].y);

        this.dot_2 = new cjs.Shape();
        this.dot_2.graphics.f('#000000').ss().s().drawCircle(0, 0, 2.25).drawCircle(17.02, 0, 2.25);
        this.dot_2.setTransform(this.rectimg.dots[44].x - 17.02 / 2, this.rectimg.dots[44].y + 3.5);

        this.rectimg.addChild(this.Line_10, this.Line_11, this.Line_12, this.Line_13, this.Line_14, this.dot_2);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.rectimg, this.rectimg_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 290);


    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   
        // 
        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A3CD");
        this.text.setTransform(3, 23);

        this.text_1 = new cjs.Text(" Måla en likadan figur.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 23);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 505, 270, 10);
        this.roundRect1.setTransform(0, 0);

        this.shapegroup = new cjs.Container();
        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#9D9D9C").ss(1).drawRect(0, 0, 55, 112);
        this.rect1.setTransform(134, 31);

        this.Line1 = new cjs.Shape();
        this.Line1.graphics.f("").beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(134, 31).lineTo(189, 143).moveTo(189, 31).lineTo(134, 143).
        moveTo(161.5, 31).lineTo(134, 87).lineTo(161.5, 143).lineTo(189, 87).lineTo(161.5, 31)
            .moveTo(134, 59).lineTo(189, 59).moveTo(134, 87).lineTo(189, 87)
            .moveTo(134, 115).lineTo(189, 115);
        this.Line1.setTransform(0, 0);

        this.shapegroup.addChild(this.rect1, this.Line1);


        this.shapegroup_1 = this.shapegroup.clone(true);
        this.shapegroup_1.setTransform(0, 119);

        this.shapegroup_2 = this.shapegroup.clone(true);
        this.shapegroup_2.setTransform(260, 0);

        this.shapegroup_3 = this.shapegroup.clone(true);
        this.shapegroup_3.setTransform(260, 119);

        this.triangle = new cjs.Shape();
        this.triangle.graphics.f('#D51317').s("#000000").ss(1).moveTo(0, 6).lineTo(52, 6).lineTo(26, 63).lineTo(0, 6);
        this.triangle.setTransform(320, 25);

        this.triangle_1 = new cjs.Shape();
        this.triangle_1.graphics.f('#FFF374').s("#000000").ss(1).moveTo(0, 6).lineTo(52, 6).lineTo(26, 60).lineTo(0, 6);
        this.triangle_1.setTransform(372, 148);
        this.triangle_1.rotation = 180;

        this.poly = new cjs.Shape();
        this.poly.graphics.f('#13A538').s("#000000").ss(1).drawPolyStar(0, 0, 50, 6, 0, 0);
        this.poly.setTransform(345, 205, 0.55, 0.63);
        this.poly.rotation = 0;

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#008BD2').s("#000000").ss(1).moveTo(26, -49).lineTo(0, 7).lineTo(26, 63).lineTo(52, 7).lineTo(26, -49);
        this.shape_1.setTransform(42, 80);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#FFF374').s("#000000").ss(1).moveTo(26, -49).lineTo(0, 7).lineTo(26, 63).lineTo(52, 7).lineTo(26, -49);
        this.shape_2.setTransform(55, 175,0.5,0.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#13A538').s("#000000").ss(1).moveTo(26, -49).lineTo(0, 7).lineTo(26, 63).lineTo(52, 7).lineTo(26, -49);
        this.shape_3.setTransform(55, 230,0.5,0.5);

        this.addChild(this.text, this.text_1, this.roundRect1, this.rect1, this.shapegroup, this.shapegroup_1,
            this.shapegroup_2, this.shapegroup_3, this.triangle, this.triangle_1, this.poly, this.shape_1, this.shape_2, this.shape_3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 270);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(290, 97, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(290, 403, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
