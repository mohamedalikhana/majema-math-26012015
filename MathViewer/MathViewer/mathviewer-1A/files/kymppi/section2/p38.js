(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p38_1.png",
            id: "p38_1"
        }, {
            src: "images/p38_2.png",
            id: "p38_2"
        }, {
            src: "images/p38_3.png",
            id: "p38_3"
        }]
    };

    // symbols:

    (lib.p38_1 = function() {
        this.initialize(img.p38_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p38_2 = function() {
        this.initialize(img.p38_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p38_3 = function() {
        this.initialize(img.p38_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();


        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(442 + (i * 32), 37, 27, 27);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageFooterTextRight = new cjs.Text("kunna lösa uppgifter med addition 0 till 6", "9px 'Myriad Pro'");
        this.pageFooterTextRight.lineHeight = 11;
        this.pageFooterTextRight.setTransform(380, 653.7);

        this.pageNoLeft = new cjs.Text("38", "13px 'Myriad Pro'", "#FFFFFF");
        this.pageNoLeft.lineHeight = 18;
        this.pageNoLeft.setTransform(39, 648);

        this.pageNoLeftBackground = new cjs.Shape();
        this.pageNoLeftBackground.graphics.f("#7AC729").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.pageNoLeftBackground.setTransform(31, 660.8);

        this.pageTitle = new cjs.Text("Vi lär oss addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 19);

        this.chapterNumber = new cjs.Text("12", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(50.7, 15.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoLeftBackground, this.pageNoLeft, this.top_textbox_group);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p38_1();
        this.instance.setTransform(0, 26, 0.7, 0.7);
        this.instance1 = new lib.p38_2();
        this.instance1.setTransform(0, 156, 0.7, 0.7);
        this.instance2 = new lib.p38_3();
        this.instance2.setTransform(0, 286, 0.7, 0.7);

        this.text = new cjs.Text("Hur många? ", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 1);


        this.textBoxGroup1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            var space = 140;
            if (columnSpace > 1) {
                columnSpace = columnSpace - 0.2;
            }
            for (var row = 0; row < 3; row++) {
                this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(40 + (columnSpace * space), 37 + (row * 129), 20, 21);
                //console.log(150 + (columnSpace * 150), 40 + (row * 30))
            }
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.label1 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label1.lineHeight = 27;
        this.label1.setTransform(70, 130);
        this.label2 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label2.lineHeight = 27;
        this.label2.setTransform(70, 260);
        this.label3 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label3.lineHeight = 27;
        this.label3.setTransform(70, 390);

        this.label4 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label4.lineHeight = 27;
        this.label4.setTransform(320, 130);
        this.label5 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label5.lineHeight = 27;
        this.label5.setTransform(320, 260);
        this.label6 = new cjs.Text("tillsammans:", "16px 'Myriad Pro'");
        this.label6.lineHeight = 27;
        this.label6.setTransform(320, 390);
        
        this.textBoxGroup2 = new cjs.Shape();
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(160,125, 20, 21).drawRect(160,255, 20, 21).drawRect(160,385, 20, 21).drawRect(410,125, 20, 21).drawRect(410,255, 20, 21).drawRect(410,385, 20, 21);
        this.textBoxGroup2.setTransform(0,0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((712 * 0.5) - 10) * 0.7, ((181) - 2) * 0.7, 10);
        this.roundRect1.setTransform(0, 25);
        
        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((712 * 0.5) + 7) * 0.7, 25);
        
        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 157);
        
        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((712 * 0.5) + 7) * 0.7, 157);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 289);
        
        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((712 * 0.5) + 7) * 0.7, 289);
        
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,this.roundRect5, this.roundRect6,this.instance, this.instance1, this.instance2, this.text, this.text_1, this.textBoxGroup1,this.label1, this.label2, this.label3, this.label4, this.label5, this.label6,this.textBoxGroup2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);


    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect = new cjs.Shape();
        this.roundRect.graphics.s("#7D7D7D").ss(0.7).drawRoundRect(0, 10, 496.5, 124, 10);
        this.roundRect.setTransform(0, 13);

        this.text = new cjs.Text("Hur många är det tillsammans?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(127 + (columnSpace * 150), 31 + (row * 29), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("1 och 1   är ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(40, 34);
        this.label2 = new cjs.Text("0 och 1   är ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(40, 64);
        this.label3 = new cjs.Text("2 och 1   är ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(40, 94);
        this.label4 = new cjs.Text("3 och 1   är ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(40, 123);


        this.label5 = new cjs.Text("4 och 1   är ", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(190, 34);
        this.label6 = new cjs.Text("1 och 2   är ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(190, 64);
        this.label7 = new cjs.Text("2 och 2   är ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(190, 94);
        this.label8 = new cjs.Text("2 och 3   är ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(190, 123);

        this.label9 = new cjs.Text("1 och 3   är ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(340, 34);
        this.label10 = new cjs.Text("1 och 4   är ", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(340, 64);
        this.label11 = new cjs.Text("4 och 0   är ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(340, 94);
        this.label12 = new cjs.Text("5 och 0   är ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(340, 123);
        this.addChild(this.roundRect, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 150);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(298.3, 527, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297.3, 117.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
