(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p53_1.png",
            id: "p53_1"
        }, {
            src: "images/p53_2.png",
            id: "p53_2"
        }]
    };

    (lib.p53_1 = function() {
        this.initialize(img.p53_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p53_2 = function() {
        this.initialize(img.p53_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("53", "13px 'Myriad Pro'", "#FFFFFF");     
        this.text.setTransform(552, 656);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();   
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(9, 42, 523, 547, 13);
        this.roundRect1.setTransform(0, 0);    

        this.instance_2 = new lib.p53_1();
        this.instance_2.setTransform(0, 220, 0.482, 0.48);

        this.text = new cjs.Text("Surrande bin", "18px 'MyriadPro-Semibold'", "#F39142");     
        this.text.setTransform(7, 13);
        this.text_1 = new cjs.Text("Spel för 2 eller fler.", "16px 'Myriad Pro'", "#F39142");     
        this.text_1.setTransform(7, 33);

        this.instance = new lib.p53_2();
        this.instance.setTransform(423, 2, 0.4, 0.4);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#F39142').drawRoundRect(338, 5, 190, 33, 6);
        this.roundRect2.setTransform(0, 0);

        this.text_7 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");      
        this.text_7.setTransform(346, 26);

        this.label1 = new cjs.Text("Mål", "18px 'Myriad Pro'");     
        this.label1.setTransform(455, 453);
        this.label2 = new cjs.Text("Start", "18px 'Myriad Pro'");    
        this.label2.setTransform(30, 262);

        this.text_2 = new cjs.Text("• Slå 2 tärningar.", "16px 'Myriad Pro'");      
        this.text_2.setTransform(15, 70);

        this.text_3 = new cjs.Text("• Addera tärningstalen med talet 6.", "16px 'Myriad Pro'");     
        this.text_3.setTransform(15, 92);

        this.text_4 = new cjs.Text("• Gå till nästa blomma om svaret finns i något av bladen.", "16px 'Myriad Pro'");   
        this.text_4.setTransform(15, 114);

        this.text_5 = new cjs.Text("• Om inte står du kvar.", "16px 'Myriad Pro'");
        this.text_5.setTransform(15, 136);

        this.text_6 = new cjs.Text("• Bikupan kan du bara nå när du får svaret 10.", "16px 'Myriad Pro'");   
        this.text_6.setTransform(15, 158);        

        this.text_9 = new cjs.Text("• Den som först når bikupan vinner.", "16px 'Myriad Pro'");   
        this.text_9.setTransform(15,180); 
        // Assign numbers in bule color
        this.numtext_1 = new cjs.Text("12", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_1.setTransform(99,306); 
        this.numtext_2 = new cjs.Text("8", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_2.setTransform(75,336);
        this.numtext_3 = new cjs.Text("18", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_3.setTransform(90,368); 
        this.numtext_4 = new cjs.Text("13", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_4.setTransform(127,365);
        this.numtext_5 = new cjs.Text("17", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_5.setTransform(138,328);

        this.numtext_6 = new cjs.Text("9", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_6.setTransform(259,275); 
        this.numtext_7 = new cjs.Text("15", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_7.setTransform(224,293);
        this.numtext_8 = new cjs.Text("10", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_8.setTransform(235,327); 
        this.numtext_9 = new cjs.Text("18", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_9.setTransform(273,330);
        this.numtext_10 = new cjs.Text("14", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_10.setTransform(292,299);

        this.numtext_11 = new cjs.Text("11", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_11.setTransform(375,357); 
        this.numtext_12 = new cjs.Text("12", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_12.setTransform(340,368);
        this.numtext_13 = new cjs.Text("16", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_13.setTransform(338,405); 
        this.numtext_14 = new cjs.Text("8", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_14.setTransform(378,417);
        this.numtext_15 = new cjs.Text("17", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_15.setTransform(400,385);

        this.numtext_16 = new cjs.Text("13", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_16.setTransform(239,388); 
        this.numtext_17 = new cjs.Text("8", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_17.setTransform(206,390);
        this.numtext_18 = new cjs.Text("14", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_18.setTransform(195,427); 
        this.numtext_19 = new cjs.Text("9", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_19.setTransform(235,447);
        this.numtext_20 = new cjs.Text("18", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_20.setTransform(260,417);

        this.numtext_21 = new cjs.Text("8", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_21.setTransform(235,502); 
        this.numtext_22 = new cjs.Text("13", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_22.setTransform(188,516);
        this.numtext_23 = new cjs.Text("9", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_23.setTransform(196,552); 
        this.numtext_24 = new cjs.Text("18", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_24.setTransform(225,565);
        this.numtext_25 = new cjs.Text("12", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_25.setTransform(256,539);

        this.numtext_26 = new cjs.Text("8", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_26.setTransform(92,440); 
        this.numtext_27 = new cjs.Text("17", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_27.setTransform(57,468);
        this.numtext_28 = new cjs.Text("15", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_28.setTransform(77,502); 
        this.numtext_29 = new cjs.Text("11", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_29.setTransform(115,492);
        this.numtext_30 = new cjs.Text("10", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_30.setTransform(122,457);

        this.numtext_31 = new cjs.Text("10", "18px 'Myriad Pro'","#238CD2");   
        this.numtext_31.setTransform(422,504);

        this.addChild(this.roundRect2, this.roundRect1, this.instance_2);
        this.addChild(this.text_1, this.label1, this.label2);
        this.addChild(this.text, this.text_2, this.instance, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8,this.text_9);
        this.addChild(this.numtext_1,this.numtext_2,this.numtext_3,this.numtext_4, this.numtext_5 )
        this.addChild(this.numtext_6,this.numtext_7,this.numtext_8,this.numtext_9,this.numtext_10,this.numtext_11,this.numtext_12,this.numtext_13,this.numtext_14,this.numtext_15)
        this.addChild(this.numtext_16,this.numtext_17,this.numtext_18,this.numtext_19,this.numtext_20,this.numtext_21,this.numtext_22,this.numtext_23,this.numtext_24,this.numtext_25)
        this.addChild(this.numtext_26,this.numtext_27,this.numtext_28,this.numtext_29,this.numtext_30,this.numtext_31)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 530);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296, 268, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
