(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p143_1.png",
            id: "p143_1"
        }, {
            src: "images/p143_2.png",
            id: "p143_2"
        }]
    };

    (lib.p143_1 = function() {
        this.initialize(img.p143_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p143_2 = function() {
        this.initialize(img.p143_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("143", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.text_1 = new cjs.Text("De färggranna girafferna", "18px 'Myriad Pro'", "#09B8DA");
        this.text_1.setTransform(57, 57);

        this.text_2 = new cjs.Text("Spel för 2.", "16px 'Myriad Pro'", "#09B8DA");
        this.text_2.setTransform(57, 75);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#09B8DA').drawRoundRect(0, 0, 250, 39, 10);
        this.roundRect1.setTransform(317, 46);

        this.text_3 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(325, 70);

        this.instance = new lib.p143_1();
        this.instance.setTransform(403, 47, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.roundRect1, this.text_3, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#9D9D9C').drawRoundRect(0, 0, 509, 539, 10);
        this.roundRect1.setTransform(0, 0);

        this.text_1 = new cjs.Text("• Slå 3 tärningar.", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(10, 25);

        this.text_2 = new cjs.Text("• Välj att måla", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(10, 47);

        this.text_3 = new cjs.Text("– 1 av tärningstalen eller", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(17, 65);

        this.text_4 = new cjs.Text("– summan av 2 tärningstal eller", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(17, 84);

        this.text_5 = new cjs.Text("– summan av alla tärningstalen.", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(17, 104);

        this.text_6 = new cjs.Text("• Den som först har målat alla tal vinner.", "16px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(10, 124);

        this.instance = new lib.p143_2();
        this.instance.setTransform(-3, 152, 0.47, 0.47);

        this.text_7 = new cjs.Text("Spelare 1", "bold 16px 'Myriad Pro'", "#000000");
        this.text_7.setTransform(120, 160);

        this.text_8 = new cjs.Text("Spelare 2", "bold 16px 'Myriad Pro'", "#000000");
        this.text_8.setTransform(300, 160);

        var TextArrA = [];

        var TextXA = [83, 181, 67, 200, 88, 183, 56, 210, 70, 30, 190];
        var TextYA = [234, 279, 316, 328, 360, 387, 404, 443, 435, 468, 502];
        for (var i = 0; i < TextXA.length; i++) {

            var num = i + 5;

            var text_9 = new cjs.Text(num, "18px 'Myriad Pro'", "#000000");
            text_9.setTransform(TextXA[i], TextYA[i]);
            TextArrA.push(text_9);
        }

        var TextArrB = [];

        var TextXB = [321, 412, 317, 415, 330, 434, 315, 447, 327, 465, 337];
        var TextYB = [245, 258, 295, 298, 325, 326, 378, 372, 438, 424, 497];
        for (var i = 0; i < TextXB.length; i++) {

            var num = i + 5;

            var text_10 = new cjs.Text(num, "18px 'Myriad Pro'", "#000000");
            text_10.setTransform(TextXB[i], TextYB[i]);
            TextArrB.push(text_10);
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5,
            this.text_6, this.text_7, this.text_8, this.instance);

        for (var i = 0; i < TextArrA.length; i++) {
            this.addChild(TextArrA[i]);
        }
        for (var i = 0; i < TextArrB.length; i++) {
            this.addChild(TextArrB[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 530);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(315, 130, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
