123
var extras = function () { };

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: []
    };

    (lib.Stage1_1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#E75E33').ss(3).drawRoundRect(135, 0, 1100, 350, 13);
        this.roundRect1.setTransform(-25, -25);

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#E75E33").s("#E75E33").ss(0.5, 0, 0, 4).arc(210, 0, 30, 0, 30 * Math.PI);
        this.shape_group1.setTransform(-100, -25);

        this.text_No = new cjs.Text("2", "35px 'MyriadPro-Semibold'", "#ffffff");
        this.text_No.setTransform(100, -15);

        this.textRect = new cjs.Shape();
        this.textRect.graphics.f("#ffffff").s('#E75E33').ss(3).drawRoundRect(330, -25, 550, 50, 19);
        this.textRect.setTransform(-100, -25);

        this.text_1 = new cjs.Text("ADDITION MED TIOTALSÖVERGÅNG", "bold 28px 'Myriad Pro'", "#E75E33");
        this.text_1.setTransform(280, -13);

        this.text_2 = new cjs.Text("•  kunna lösa uppgifter i addition 0 till 20, med tiotalsövergång", "35px 'Myriad Pro'", "#E75E33");
        this.text_2.setTransform(230, 55);

        this.text_3 = new cjs.Text("•  förstå och kunna använda begreppen fler och färre", "35px 'Myriad Pro'", "#E75E33");
        this.text_3.setTransform(230, 110);

        this.text_4 = new cjs.Text("•  förstå och kunna använda den kommutativa lagen i addition", "35px 'Myriad Pro'", "#E75E33");
        this.text_4.setTransform(230, 165);

        this.text_5 = new cjs.Text("•  kunna rita ut symmetrilinjer", "35px 'Myriad Pro'", "#E75E33");
        this.text_5.setTransform(230, 220);

        this.text_6 = new cjs.Text("•  kunna läsa av ett stapeldiagram", "35px 'Myriad Pro'", "#E75E33");
        this.text_6.setTransform(230, 275);

        this.addChild(this.roundRect1, this.shape_group1, this.text_No, this.textRect, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage1_1();
        stage1_1.setTransform(0, 15);

        this.addChild(stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
