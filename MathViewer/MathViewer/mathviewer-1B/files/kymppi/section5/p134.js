(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

            src: "images/p134_1.png",
            id: "p134_1"
        }, {
            src: "images/p134_2.png",
            id: "p134_2"
        }]
    };

    (lib.p134_1 = function() {
        this.initialize(img.p134_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p134_2 = function() {
        this.initialize(img.p134_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("134", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(33, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Hur många guldmynt är det tillsammans?", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_2.setTransform(56, 139);

        this.text_3 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_3.setTransform(76, 139);

        this.text_4 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_4.setTransform(97, 139);

        this.text_5 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_5.setTransform(115, 139);

        this.text_6 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_6.setTransform(137, 139);

        this.instance = new lib.p134_1();
        this.instance.setTransform(24, 24, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 252, 132, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 154);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(261, 15);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(261, 154);

        this.rectgroup = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#878787').drawRoundRect(0, 0, 140, 24, 0);
        this.Rect1.setTransform(0, 0);
        this.rectgroup.addChild(this.Rect1);

        var startX = 0,
            startY = 0;
        var colspace = 140 / 7,
            rowspace = 0;
        var cols = 7,
            rows = 1;

        for (var col = 0; col < cols; col++) {
            for (var row = 0; row < rows; row++) {
                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 24);
                vrLine_1.setTransform(startX + (colspace * col), startY + (rowspace * row));

                this.rectgroup.addChild(vrLine_1);
            }
        }
        this.rectgroup.setTransform(55, 116);

        this.rectgroup2 = this.rectgroup.clone(true);
        this.rectgroup2.setTransform(320, 116);

        this.rectgroup3 = this.rectgroup.clone(true);
        this.rectgroup3.setTransform(55, 255);

        this.rectgroup4 = this.rectgroup.clone(true);
        this.rectgroup4.setTransform(320, 255);

        var TArr = ['30', 'guld-', 'mynt', '45', 'guld-', 'mynt', '50', 'guld-', 'mynt', '63', 'guld-', 'mynt'];
        var TxtArr = [];
        var TxtlineX = [55, 80, 80, 55, 80, 80, 310, 335, 335, 310, 335, 335];
        var TxtlineY = [91, 81, 95, 225, 216, 229, 91, 81, 95, 225, 216, 229];

        for (var i = 0; i < TxtlineX.length; i++) {
            if (i == 0 | i == 3 || i == 6 || i == 9) {
                this.temp_label = new cjs.Text(TArr[i], "18px 'Myriad Pro'", "#000000");
            } else {
                this.temp_label = new cjs.Text(TArr[i], "14px 'Myriad Pro'", "#000000");
            }

            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(this.temp_label);
        }

        this.addChild(this.text, this.text_1, this.instance, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,
            this.rectgroup, this.rectgroup2, this.rectgroup3, this.rectgroup4, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 520, 285);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.setTransform(14, 0);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p134_2();
        this.instance.setTransform(34, 29, 0.46, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 254, 207, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(259, 15);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("").s('#62C4DA').drawRoundRect(0, 0, 195, 117, 10);
        this.roundRect3.setTransform(30, 27);

        this.roundRect4 = this.roundRect3.clone(true);
        this.roundRect4.setTransform(285, 27);

        this.label1 = new cjs.Text("+            =  60", "16px 'Myriad Pro'");
        this.label1.setTransform(121, 48);

        this.label2 = new cjs.Text("+           +            =  60", "16px 'Myriad Pro'");
        this.label2.setTransform(74, 78);

        this.label3 = new cjs.Text("+            =  80", "16px 'Myriad Pro'");
        this.label3.setTransform(121, 105);

        this.label4 = new cjs.Text("+            =", "16px 'Myriad Pro'");
        this.label4.setTransform(121, 135);

        this.label5 = new cjs.Text("30  –            –           =", "16px 'Myriad Pro'");
        this.label5.setTransform(295, 48);

        this.label6 = new cjs.Text("60  –            –           =", "16px 'Myriad Pro'");
        this.label6.setTransform(295, 76);

        this.label7 = new cjs.Text("90  –            –           =", "16px 'Myriad Pro'");
        this.label7.setTransform(295, 103);

        this.label8 = new cjs.Text("+           =", "16px 'Myriad Pro'");
        this.label8.setTransform(365, 132);

        var TxtArr = [];
        var TxtlineX = [67, 163, 325, 431, 67, 163, 325, 429];
        var TxtlineY = [169, 169, 169, 169, 199, 199, 199, 199];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text("=", "bold 16px 'Myriad Pro'", "#000000");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(this.temp_label);
        }

        var lineArr = [];
        var lineX = [80, 176, 338, 444, 80, 176, 338, 442];
        var lineY = [132, 132, 132, 132, 162, 162, 162, 162];
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(31, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.addChild(this.text, this.text_1, this.instance, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,
            this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);

        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 527, 220);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(295, 107, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295, 467, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
