(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p41_1.png",
            id: "p41_1"
        }]
    };

    (lib.p41_1 = function() {
        this.initialize(img.p41_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("63", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(551, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(460 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(4, 0);

        this.text_1 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_1.setTransform(23, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 252.7, 96, 10);
        this.roundRect1.setTransform(0, 12);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 114);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 216);     

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(257, 12);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(257, 114);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(257, 216);

   

        var ToBeAdded = [];
        var xpos = 15;
        var ypos = 25;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 3; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 127
            } else if (numOfrow == 2) {
                ypos = 229
            } else if (numOfrow == 3) {
                ypos = 330
            } else {
                ypos = 25
            }
            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 2; row++) {
                    for (var column = 0; column < 5; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 138
                        } else if (numOfcol == 2) {
                            xpos = 272
                        } else if (numOfcol == 3) {
                            xpos = 393
                        } else {
                            xpos = 15
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 20), ypos + (row * 20), 20, 21);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }


        var tmp_arc = new cjs.Shape();
        var xpos = 25;
        var ypos = 35;
        var fillcolor = "#D51317"
        for (var numOfrow = 0; numOfrow < 3; numOfrow++) {
            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var column = 0; column < 9; column++) {
                    var columnSpace = column;
                    for (var row = 0; row < 2; row++) {
                        if (row == 0 && column == 5 && numOfcol == 0) {
                            xpos = 48
                        }
                        if (numOfcol == 1) {
                            xpos = 282
                        }
                        if (row == 0 && column >= 5 && numOfcol == 1) {
                            xpos = 303
                        }
                        if (row == 1 && column >= 5) {
                            continue
                        }                        

                        if (numOfrow == 1) {
                            ypos = 137;
                            xpos = 25
                        }
                        if (numOfrow == 1 && numOfcol == 1) {
                            ypos = 137;
                            xpos = 282
                        }
                        if (numOfrow == 2) {
                            ypos = 239;
                            xpos = 25
                        }
                        if (numOfrow == 2 && numOfcol == 1) {
                            ypos = 239;
                            xpos = 282
                        }
                        if (numOfrow == 3) {
                            ypos = 340;
                            xpos = 25
                        }
                        if (numOfrow == 3 && numOfcol == 1) {
                            ypos = 340;
                            xpos = 282
                        }

                        if (row == 0 && column >= 5 && numOfcol == 0 && numOfrow == 1) {
                            xpos = 48
                        }
                        if (row == 0 && column >= 5 && numOfcol == 0 && numOfrow == 2) {
                            xpos = 48
                        }
                        if (row == 0 && column >= 5 && numOfcol == 0 && numOfrow == 3) {
                            xpos = 48
                        }

                        if (row == 0 && column >= 5 && numOfcol == 1 && numOfrow == 1) {
                            xpos = 303
                        }
                        if (row == 0 && column >= 5 && numOfcol == 1 && numOfrow == 2) {
                            xpos = 303
                        }
                        if (row == 0 && column >= 5 && numOfcol == 1 && numOfrow == 3) {
                            xpos = 303
                        }


                        tmp_arc.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 20), 7, 0, 2 * Math.PI);
                        ToBeAdded.push(tmp_arc);
                    }
                }
            }
        }


        var arryVal = ["14  –  5 =", "14  –  6 =", "14  –  10 =", "14  –  7 =", "14  –  8 =", "14  –  9 ="];
        var tmprow = 20;
        var tmpcol = 312;
        var i = 0;
        var temp_Line1 = new cjs.Shape();
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(arryVal[i], "16px 'Myriad Pro'");
                tempText.setTransform(81 + (258 * columnSpace), 93 + (100 * rowSpace));
                if (col == 1) {
                    tmpcol = 258
                } else if (col == 2) {
                    tmpcol = 165
                } else {
                    tmpcol = 162
                }
                if (row > 0) {
                    tmprow = 100
                } else {
                    tmprow = 20
                }
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(144 + (columnSpace * tmpcol), 97 + (rowSpace * tmprow)).lineTo(177 + (columnSpace * tmpcol), 97 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.text_q1, this.text_q2, this.text,this.text_1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 290);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text.setTransform(18, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(4, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 12, 511, 198, 10);
        this.roundRect1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(25, 0).lineTo(342, 0);
        this.Line_1.setTransform(60, 35);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 14; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 100;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 110;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 120;
                }  
                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 30 + (row * 19), 7.9, 0, 2 * Math.PI);
            }

        }this.shape_group1.setTransform(3, 5);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(178, 38);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_3.setTransform(284, 38);

        var ToBeAdded = [];
        var arryVal = [5,7,6,8,9,5,7,6,8,9,6,7,9,5,7];
        var tmpArry=["13  –  ","14  –  ","11  –  "];
        var i = 0;
        var j=0;
        var tmpTxt="";
        var temp_Line1 = new cjs.Shape();
        var tmprow = 28;
        var tmpcol = 158;
        for (var col = 0; col < 3; col++) {
            var columnSpace = col;
            for (var row = 0; row < 5; row++) {
                var rowSpace = row;
                 if (col == 1) {
                    tmpcol = 165;
                    j=1;
                } 
                 if (col == 2) {
                    tmpcol = 165;
                    j=2;
                } 
                if (col==2 && row>2){tmpTxt="12  –  "}
                    else {tmpTxt=tmpArry[j]}
                var tempText = new cjs.Text(tmpTxt + arryVal[i] + "  =", "16px 'Myriad Pro'");
                tempText.setTransform(40 + (164 * columnSpace), 75 + (28 * rowSpace));
               
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(105 + (columnSpace * tmpcol), 80 + (rowSpace * tmprow)).lineTo(140 + (columnSpace * tmpcol), 80 + (rowSpace * tmprow));
                ToBeAdded.push(tempText,temp_Line1)
                i++;
            };
        };
       


        this.addChild(this.roundRect1, this.text, this.text_1);
        this.addChild(this.Line_1, this.shape_group1,this.text_2,this.text_3)            
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(308, 284, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(308, 428, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
