var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };



    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

var answersX = 170,
        answersY = 100;
    function ballZoomTween(ref, animationSpeed, waitTime, position) {
        var modX = 0,
            modY = 0;
        switch (position) {
            case 'topleft':
                modX = -(20 + (0.31 * 20));
                modY = -(20 + 8 + (0.31 * 20));
                break;
            case 'bottomleft':
                modX = -(20 + (0.31 * 20));
                modY = 0 + 8;
                break;
            case 'topcenter':
                modX = -(15 + (0.31 * 15));
                modY = -(20 + 8 + (0.31 * 20));
                break;
            case 'bottomcenter':
                modX = -(15 + (0.31 * 15));
                modY = 0 + 8;
                break;
            case 'topright':
                modX = +(15 + (0.31 * 15));
                modY = -(20 + 8 + (0.31 * 20));
                break;
            case 'bottomright':
                modX = +(15 + (0.31 * 15));
                modY = 0 + 8;
                break;
        }

        return {
            ref: ref,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: ref.x,
                y: ref.y
            },
            positionTo: {
                x: ref.x + modX,
                y: ref.y + modY
            },
            scaleFrom: {
                x: 0.445 + 0.23,
                y: 0.445 + 0.23
            },
            scaleTo: {
                x: 0.55 + 0.23,
                y: 0.55 + 0.23
            },
            wait: waitTime,
            alphaTimeout: animationSpeed,
            loop: true,
            loopCount: 1,
            wiggle: true
        }
    }

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talfamiljer", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("27", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Addera", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(0, 0);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function newPoint(x, y, size) {

        var point = new cjs.Shape();
        point.graphics.s('#000000').ss(1).drawRect(x, y, size, size).drawRect(size, y, size, size).drawRect(size, size, size, size).drawRect(x, size, size, size);
        return point;
    }
    (lib.Stage1 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Gör 2 additioner.", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -40);


        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();

        // this.blueBall.visible=false;
        // this.redBall.visible=false;
        this.roundRect1 = new lib.cupBoard();
        this.roundRect1.setTransform(433, 22, 1.45, 1.45);

        this.redBalls = [];
        this.blueBalls = [];

        this.addChild(this.roundRect1, this.questionText, this.hintText1);
        var ballXSpace = 129;
        var blueBallX = [50 + ballXSpace, 50 + ballXSpace, 50 + ballXSpace * 2, 50 + ballXSpace * 2];
        var blueBallY = [0, 115, 0, 115];

        for (var i = 0; i < 4; i++) {
            var blueBall = this.blueBall.clone(true);
            blueBall.setTransform(430 + blueBallX[i], 70 + blueBallY[i], 0.445 + 0.23, 0.445 + 0.23);
            this.addChild(blueBall);
            this.blueBalls.push(blueBall);
        }

        var redBallX = [50, 50];
        var redBallY = [0, 115];
        for (var j = 0; j < 2; j++) {
            var redBall = this.redBall.clone(true);
            redBall.setTransform(430 + redBallX[j], 70 + redBallY[j], 0.445 + 0.23, 0.445 + 0.23);
            this.addChild(redBall);
            this.redBalls.push(redBall);
        }
        // this.addChild(newPoint(0, 0, 433));
        // this.addChild(this.roundRect1, this.roundRect2, this.questionText, this.hintText1, this.addBall);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();


        this.tweens = [];
        var animationSpeed = 1000;
        var redBallsWaitTime = 0;
        var blueBallsWaitTime = 2500;
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[2], animationSpeed, blueBallsWaitTime, 'topright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[3], animationSpeed, blueBallsWaitTime, 'bottomright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[0], animationSpeed, blueBallsWaitTime, 'topcenter'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[1], animationSpeed, blueBallsWaitTime, 'bottomcenter'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[0], animationSpeed, redBallsWaitTime, 'topleft'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[1], animationSpeed, redBallsWaitTime, 'bottomleft'));
        p.tweens = this.tweens;

        this.addChild(this.stage1, this.questionText);
        // this.addChild(this.blueBall1,this.blueBall2,this.redBall1,this.redBall2,this.redBall3,this.redBall4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();

        this.answers = new lib.answers();
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();

        this.answers = new lib.answers();
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.Stage5 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();
        // this.questionText = new cjs.Text("Gör 2 additioner", "36px 'Myriad Pro'")
        // this.questionText.lineHeight = 19;
        // this.questionText.textAlign = 'center';
        // this.questionText.setTransform(570, 15);

        this.answers = new lib.answers();
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];
        var animationSpeed = 1000;
        var redBallsWaitTime = 2500;
        var blueBallsWaitTime = 0;

       this.tweens.push(ballZoomTween(this.stage1.blueBalls[2], animationSpeed, blueBallsWaitTime, 'topright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[3], animationSpeed, blueBallsWaitTime, 'bottomright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[0], animationSpeed, blueBallsWaitTime, 'topcenter'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[1], animationSpeed, blueBallsWaitTime, 'bottomcenter'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[0], animationSpeed, redBallsWaitTime, 'topleft'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[1], animationSpeed, redBallsWaitTime, 'bottomleft'));
       


        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage6 = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();

        this.answers = new lib.answers();
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_6,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_7,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_8,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_9,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage7 = function() {
        this.initialize();

        this.stage5 = new lib.Stage5();
        this.answers = new lib.answers();

        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];

        this.tweens.push({
            ref: this.answers.text_10,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage5, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.Stage8 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();

        this.questionText = this.stage1.questionText.clone(true);
        this.questionText.text = "Gör 2 subtraktioner.";
        this.answers = new lib.answers();
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);
        this.tweens = [];
        this.tweens.push({
            ref: this.stage1.questionText,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 1500,
        });

        this.tweens.push({
            ref: this.questionText,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2800,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers, this.questionText);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage9 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();
        this.stage1.questionText.text = "Gör 2 subtraktioner.";

        this.answers = new lib.answers();


        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];

        // this.tweens.push({
        //     ref: this.addBall.redBalls,
        //     alphaFrom: 1,
        //     alphaTo: 1,
        //     wait: 3000,
        //     alphaTimeout: 200
        // });

        // this.tweens.push({
        //     ref: this.addBall.blueBalls,
        //     alphaFrom: 1,
        //     alphaTo: 0.3,
        //     wait: 3000,
        //     alphaTimeout: 200
        // });

        var animationSpeed = 1000;
        var redBallsWaitTime = 0;
        var blueBallsWaitTime = 0;
        this.tweens.push({
            ref: this.stage1.blueBalls[0],
            alphaFrom: 1,
            alphaTo: 0.3,
            wait: 3000,
            alphaTimeout: 200,
            override: true
        });
        this.tweens.push({
            ref: this.stage1.blueBalls[1],
            alphaFrom: 1,
            alphaTo: 0.3,
            wait: 3000,
            alphaTimeout: 200,
            override: true
        });
        this.tweens.push({
            ref: this.stage1.blueBalls[2],
            alphaFrom: 1,
            alphaTo: 0.3,
            wait: 3000,
            alphaTimeout: 200,
            override: true
        });
        this.tweens.push({
            ref: this.stage1.blueBalls[3],
            alphaFrom: 1,
            alphaTo: 0.3,
            wait: 3000,
            alphaTimeout: 200,
            override: true
        });

 this.tweens.push(ballZoomTween(this.stage1.blueBalls[2], animationSpeed, blueBallsWaitTime, 'topright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[3], animationSpeed, blueBallsWaitTime, 'bottomright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[0], animationSpeed, blueBallsWaitTime, 'topcenter'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[1], animationSpeed, blueBallsWaitTime, 'bottomcenter'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[0], animationSpeed, redBallsWaitTime, 'topleft'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[1], animationSpeed, redBallsWaitTime, 'bottomleft'));




        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage10 = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.questionText.text = "Gör 2 subtraktioner.";
        for (var i = 0; i < this.stage1.blueBalls.length; i++) {
            this.stage1.blueBalls[i].alpha = 0.3;
        };
        for (var i = 0; i < this.stage1.redBalls.length; i++) {
            this.stage1.redBalls[i].alpha = 1;
        };
        this.answers = new lib.answers();
        this.answers.text_14.visible = true;
        this.answers.text_13.visible = true;
        this.answers.text_12.visible = true;
        this.answers.text_11.visible = true;
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];


        this.tweens.push({
            ref: this.answers.text_11,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.answers.text_12,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1400,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.answers.text_13,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2100,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.answers.text_14,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2800,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage11 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();
        this.stage1.questionText.text = "Gör 2 subtraktioner.";

        this.answers = new lib.answers();
        this.answers.text_15.visible = true;
        this.answers.text_14.visible = true;
        this.answers.text_13.visible = true;
        this.answers.text_12.visible = true;
        this.answers.text_11.visible = true;
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];
        for (var i = 0; i < this.stage1.blueBalls.length; i++) {
            this.stage1.blueBalls[i].alpha = 0.3;
        };


        this.tweens.push({
            ref: this.answers.text_15,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.Stage12 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();
        this.stage1.questionText.text = "Gör 2 subtraktioner.";

        this.answers = new lib.answers();
        this.answers.text_15.visible = true;
        this.answers.text_14.visible = true;
        this.answers.text_13.visible = true;
        this.answers.text_12.visible = true;
        this.answers.text_11.visible = true;
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);


        this.tweens = [];
        var animationSpeed = 1000;
        var redBallsWaitTime = 0;
        var blueBallsWaitTime = 0;
        for (var i = 0; i < this.stage1.redBalls.length; i++) {
            this.tweens.push({
                ref: this.stage1.redBalls[i],
                alphaFrom: 1,
                alphaTo: 0.3,
                wait: 3000,
                alphaTimeout: 200
            });
        };

        this.tweens.push(ballZoomTween(this.stage1.blueBalls[2], animationSpeed, blueBallsWaitTime, 'topright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[3], animationSpeed, blueBallsWaitTime, 'bottomright'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[0], animationSpeed, blueBallsWaitTime, 'topcenter'));
        this.tweens.push(ballZoomTween(this.stage1.blueBalls[1], animationSpeed, blueBallsWaitTime, 'bottomcenter'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[0], animationSpeed, redBallsWaitTime, 'topleft'));
        this.tweens.push(ballZoomTween(this.stage1.redBalls[1], animationSpeed, redBallsWaitTime, 'bottomleft'));

        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage13 = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.questionText.text = "Gör 2 subtraktioner.";

        this.answers = new lib.answers();

        this.answers.text_19.visible = true;
        this.answers.text_18.visible = true;
        this.answers.text_17.visible = true;
        this.answers.text_16.visible = true;
        this.answers.text_15.visible = true;
        this.answers.text_14.visible = true;
        this.answers.text_13.visible = true;
        this.answers.text_12.visible = true;
        this.answers.text_11.visible = true;
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);

        this.tweens = [];

        for (var i = 0; i < this.stage1.redBalls.length; i++) {
            this.stage1.redBalls[i].alpha = 0.3;
        };

        this.tweens.push({
            ref: this.answers.text_16,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.answers.text_17,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1200,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.answers.text_18,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1700,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.answers.text_19,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2200,
            alphaTimeout: 2000
        });



        p.tweens = this.tweens;


        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage14 = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.questionText.text = "Gör 2 subtraktioner.";

        this.answers = new lib.answers();
        this.answers.text_20.visible = true;
        this.answers.text_19.visible = true;
        this.answers.text_18.visible = true;
        this.answers.text_17.visible = true;
        this.answers.text_16.visible = true;
        this.answers.text_15.visible = true;
        this.answers.text_14.visible = true;
        this.answers.text_13.visible = true;
        this.answers.text_12.visible = true;
        this.answers.text_11.visible = true;
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_8.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(answersX, answersY, 3, 3, 0, 0, 0, 0, 0);



        this.tweens = [];
        for (var i = 0; i < this.stage1.blueBalls.length; i++) {
            this.stage1.blueBalls[i].alpha = 1;
        };
        for (var i = 0; i < this.stage1.redBalls.length; i++) {
            this.stage1.redBalls[i].alpha = 0.3;
        };

        this.tweens.push({
            ref: this.answers.text_20,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 700,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(85, 80);

        this.text_2 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(97, 80);

        this.text_3 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(109, 80);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(121, 80);

        this.text_5 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(133, 80);

        this.text_6 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_6.visible = false;
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(85, 100);

        this.text_7 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_7.visible = false;
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(97, 100);

        this.text_8 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_8.visible = false;
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(109, 100);

        this.text_9 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_9.visible = false;
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(121, 100);

        this.text_10 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_10.visible = false;
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(133, 100);

        this.text_11 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_11.visible = false;
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(175, 80);

        this.text_12 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_12.visible = false;
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(187, 80);

        this.text_13 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_13.visible = false;
        this.text_13.lineHeight = 19;
        this.text_13.setTransform(199, 80);

        this.text_14 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_14.visible = false;
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(211, 80);

        this.text_15 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_15.visible = false;
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(223, 80);

        this.text_16 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_16.visible = false;
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(175, 100);

        this.text_17 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_17.visible = false;
        this.text_17.lineHeight = 19;
        this.text_17.setTransform(187, 100);

        this.text_18 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_18.visible = false;
        this.text_18.lineHeight = 19;
        this.text_18.setTransform(199, 100);

        this.text_19 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_19.visible = false;
        this.text_19.lineHeight = 19;
        this.text_19.setTransform(211, 100);

        this.text_20 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_20.visible = false;
        this.text_20.lineHeight = 19;
        this.text_20.setTransform(223, 100);



        this.addChild(this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    (lib.cupBoard = function(ball, count) {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();
        var rectProps1 = {
            height: 200,
            width: 300,
            x: 0,
            y: 0
        };

        var rectProps2 = {
            height: 200,
            width: 300,
            x: 0,
            y: 0
        };

        var verticalSpacing = 23,
            horizontalSpacing = 33;
        var skewRectWidth = 10;
        rectProps2.x = rectProps2.x + horizontalSpacing;
        rectProps2.y = rectProps2.y + verticalSpacing;
        rectProps2.height = rectProps2.height - (2 * verticalSpacing);
        rectProps2.width = rectProps2.width - (2 * horizontalSpacing);

        var middleBlock1Points = {
            x1: rectProps1.width / 3,
            y1: rectProps1.y,
            x2: rectProps1.width / 3,
            y2: rectProps1.height,
            x3: (rectProps1.width / 3) + skewRectWidth,
            y3: rectProps1.height - verticalSpacing,
            x4: (rectProps1.width / 3) + skewRectWidth,
            y4: rectProps1.y + verticalSpacing
        }
        var middleBlock2Points = {
            x1: rectProps1.width * 2 / 3,
            y1: rectProps1.y,
            x2: rectProps1.width * 2 / 3,
            y2: rectProps1.height,
            x3: (rectProps1.width * 2 / 3) - skewRectWidth,
            y3: rectProps1.height - verticalSpacing,
            x4: (rectProps1.width * 2 / 3) - skewRectWidth,
            y4: rectProps1.y + verticalSpacing
        }

        var rect1 = new cjs.Shape();
        rect1.graphics.f("#BAD9D7").s('#000000').ss(1).drawRect(rectProps1.x, rectProps1.y, rectProps1.width, rectProps1.height);
        var rect2 = new cjs.Shape();
        rect2.graphics.f("#87B0BB").s('#000000').ss(1).drawRect(rectProps2.x, rectProps2.y, rectProps2.width, rectProps2.height);
        var lineAD = new cjs.Shape();
        lineAD.graphics.s("#000000").ss(0.5).moveTo(rectProps1.x, rectProps1.y).lineTo(rectProps1.width, rectProps1.height);
        var lineBC = new cjs.Shape();
        lineBC.graphics.s("#000000").ss(0.5).moveTo(rectProps1.width, rectProps1.y).lineTo(rectProps1.x, rectProps1.height);

        var middleLine1 = new cjs.Shape();
        middleLine1.graphics.s("#000000").f("#BAD9D7").ss(1).moveTo(middleBlock1Points.x1, middleBlock1Points.y1).lineTo(middleBlock1Points.x2, middleBlock1Points.y2).lineTo(middleBlock1Points.x3, middleBlock1Points.y3).lineTo(middleBlock1Points.x4, middleBlock1Points.y4).lineTo(middleBlock1Points.x1, middleBlock1Points.y1);
        var middleLine2 = new cjs.Shape();
        middleLine2.graphics.s("#000000").f("#BAD9D7").ss(1).moveTo(middleBlock2Points.x1, middleBlock2Points.y1).lineTo(middleBlock2Points.x2, middleBlock2Points.y2).lineTo(middleBlock2Points.x3, middleBlock2Points.y3).lineTo(middleBlock2Points.x4, middleBlock2Points.y4).lineTo(middleBlock2Points.x1, middleBlock2Points.y1);

        var middleLine = new cjs.Shape();
        middleLine.graphics.s("#000000").ss(1).moveTo(rectProps1.x, rectProps1.height / 2).lineTo(rectProps1.width, rectProps1.height / 2);

        this.addChild(rect1, lineAD, lineBC, rect2, middleLine1, middleLine2, middleLine);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);



    (lib.insertBall_1 = function(ball, count) {
        this.initialize();
        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();
        this.redBalls = new cjs.Container();
        this.blueBalls = new cjs.Container();
        var addImage = [];
        var blueBallX = ['119', '119', '207', '207'];
        var blueBallY = ['30', '109', '30', '109'];

        for (var i = 0; i < 4; i++) {
            var blueBall4 = this.blueBall.clone(true);
            blueBall4.setTransform(blueBallX[i], blueBallY[i], 0.5, 0.5);
            this.blueBalls.addChild(blueBall4);
        }

        var redBallX = ['33', '33'];
        var redBallY = ['30', '109'];
        for (var j = 0; j < 2; j++) {

            var redBall2 = this.redBall.clone(true);
            redBall2.setTransform(redBallX[j], redBallY[j], 0.5, 0.5);
            this.redBalls.addChild(redBall2);
        }
        this.addChild(this.blueBalls, this.redBalls);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 500);



    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage2 = new lib.Stage2();
        this.stage2.visible = true;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage3 = new lib.Stage3();
        this.stage3.visible = true;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage4 = new lib.Stage4();
        this.stage4.visible = true;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0);


        this.stage5 = new lib.Stage5();
        this.stage5.visible = true;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage6 = new lib.Stage6();
        this.stage6.visible = true;
        this.stage6.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage7 = new lib.Stage7();
        this.stage7.visible = true;
        this.stage7.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage8 = new lib.Stage8();
        this.stage8.visible = true;
        this.stage8.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage9 = new lib.Stage9();
        this.stage9.visible = true;
        this.stage9.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage10 = new lib.Stage10();
        this.stage10.visible = true;
        this.stage10.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage11 = new lib.Stage11();
        this.stage11.visible = true;
        this.stage11.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage12 = new lib.Stage12();
        this.stage12.visible = true;
        this.stage12.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage13 = new lib.Stage13();
        this.stage13.visible = true;
        this.stage13.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.stage14 = new lib.Stage14();
        this.stage14.visible = true;
        this.stage14.setTransform(0, 0, 1, 1, 0, 0, 0);

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6,
            this.stage7, this.stage8, this.stage9, this.stage10, this.stage11, this.stage12, this.stage13, this.stage14);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
