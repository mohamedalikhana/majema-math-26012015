(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p152_1.png",
            id: "p152_1"
        }]
    };

    // symbols:
    (lib.p152_1 = function() {
        this.initialize(img.p152_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("152", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 659);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#1A863A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.text_1 = new cjs.Text("Ordningstal", "24px 'MyriadPro-Semibold'", "#1A863A");
        this.text_1.setTransform(40, 76);

        this.text_2 = new cjs.Text("Till läraren:", "bold 11px 'Myriad Pro'");
        this.text_2.setTransform(40.5, 555);

        this.text_3 = new cjs.Text("Ta tillsammans reda på vilken kö som är kortast och vilken som är längst", "11px 'Myriad Pro'");
        this.text_3.setTransform(100, 555);

        this.text_4 = new cjs.Text("och hur många färre barn det är i den kortare kön.", "11px 'Myriad Pro'");
        this.text_4.setTransform(40.5, 570);

        this.text_5 = new cjs.Text("Ställ frågor av typen: ”Vem står på första/sista plats?”, ”Vem står på femte plats?”,", "11px 'Myriad Pro'");
        this.text_5.setTransform(40.5, 585);

        this.text_6 = new cjs.Text("”På vilken plats står flickan med randig tröja?”, ”Hur många barn är det mellan barnet", "11px 'Myriad Pro'");
        this.text_6.setTransform(40.5, 600);

        this.text_7 = new cjs.Text("som står på andra plats och barnet som står på sjunde plats?”.", "11px 'Myriad Pro'");
        this.text_7.setTransform(40.5, 615);

        this.pageBottomText = new cjs.Text("kunna ordningstalen första till tjugonde", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(67, 660);

        this.instance = new lib.p152_1();
        this.instance.setTransform(33, 103, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6,
            this.text_7, this.pageBottomText, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Färglägg halsbandet efter instruktionen.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(6, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol1 = function() {
        this.initialize();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol2();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(0, 0, 1, 1, 0, 0, 0, 254.6, 50);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
