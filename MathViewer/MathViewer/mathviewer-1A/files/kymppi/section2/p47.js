(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };    


    (lib.Symbol1 = function() {
        this.initialize();
  
        this.text_4 = new cjs.Text("47", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(555,652);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579,660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 20).lineTo(0, 20).moveTo(590, 30).lineTo(590, 660).moveTo(590, 660).lineTo(0, 660);
        this.shape_3.setTransform(0, 0);    
        this.shapeArc1=new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(580, 30, 10,3*Math.PI/2 , 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(590, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);    

    this.addChild(this.shape_4, this.shape_3,this.shapeArc1,  this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Rita eller skriv en egen räknehändelse till additionen.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(10, 0); 

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(10, 30,510,230, 10);
        this.round_Rect1.setTransform(0, 0);
           
        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(370 + (i * 20), 252, 21, 23);
        }
        this.textBoxGroup1.setTransform(0, 0);      
        
        this.labelTemp1 = new cjs.Text("3", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp1.lineHeight = 40;
        this.labelTemp1.setTransform(370, 242.4);

         this.labelTemp2 = new cjs.Text("+", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp2.lineHeight = 40;
        this.labelTemp2.setTransform(390, 242.4);

         this.labelTemp3 = new cjs.Text("2", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp3.lineHeight = 40;
        this.labelTemp3.setTransform(410, 242.4);

        this.labelTemp4 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp4.lineHeight = 40;
        this.labelTemp4.setTransform(430, 242.4);

        this.labelTemp5 = new cjs.Text("5", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp5.lineHeight = 40;
        this.labelTemp5.setTransform(450, 242.4);



        this.addChild(this.text_1,this.round_Rect1,this.textBoxGroup1,this.labelTemp1,this.labelTemp2
            ,this.labelTemp3,this.labelTemp4,this.labelTemp5);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 270);

    
    (lib.Symbol3 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Rita eller skriv en egen räknehändelse. Skriv additionen.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(10, 0); 

         this.text_2 = new cjs.Text("Visa och berätta för en kamrat.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(10, 270); 

         this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(10, 30,510,230, 10);
        this.round_Rect1.setTransform(0, 0);

        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(370 + (i * 20), 248, 21, 23);
        }
        this.textBoxGroup1.setTransform(0, 0); 

        this.labelTemp1 = new cjs.Text("+", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp1.lineHeight = 40;
        this.labelTemp1.setTransform(391, 237.5);       

         this.labelTemp2 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C6D70");
        this.labelTemp2.lineHeight = 40;
        this.labelTemp2.setTransform(431, 237.5);

 
        this.addChild(this.text_1,this.text_2,this.round_Rect1,this.textBoxGroup1,this.labelTemp1,this.labelTemp2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 260);



    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296.8, 240, 1, 1, 0, 0, 0, 256.3, 173.6);
        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296.8, 240, 1, 1, 0, 0, 0, 256.3, -110);


        this.addChild(this.v2,this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
