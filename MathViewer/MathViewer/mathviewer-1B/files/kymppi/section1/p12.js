(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p12_1.png",
            id: "p12_1"
        }]
    };

    (lib.p12_1 = function() {
        this.initialize(img.p12_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();


        this.pageTitle = new cjs.Text("Talen 16 till 18", "24px 'MyriadPro-Semibold'", "#90BD22");
        this.pageTitle.setTransform(105, 40);

        this.text_1 = new cjs.Text("3", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(60, 40);

        this.text_2 = new cjs.Text("12", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(40, 658);

        this.pageFooterTextRight = new cjs.Text("förstå och kunna använda talen 0 till 20", "9px 'Myriad Pro'");
        this.pageFooterTextRight.setTransform(70, 659);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);


        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(45, 15, 1.025, 1.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.pageTitle, this.pageFooterTextRight, this.shape_2, this.shape_1, this.shape, this.text, this.text_1, this.text_2, this.text_3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol2 = function() {
        this.initialize();


        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text.setTransform(5, 6);

        this.text_1 = new cjs.Text(" Skriv talet.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 6);

        this.text_8 = new cjs.Text("sexton", "16px 'Myriad Pro'");
        this.text_8.setTransform(23, 44);

        this.text_9 = new cjs.Text("sjutton", "16px 'Myriad Pro'");
        this.text_9.setTransform(23, 104);

        this.text_10 = new cjs.Text("arton", "16px 'Myriad Pro'");
        this.text_10.setTransform(23, 164);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 20, 512, 204, 10);
        this.roundRect1.setTransform(0, 0);

        var arrxPos = ['26', '75', '125', '175', '225'];
        var arryPos = ['54', '115', '176'];
        this.textbox_group1 = new cjs.Shape();
        for (var j = 0; j < arryPos.length; j++) {
            var yPos = parseInt(arryPos[j]);

            for (var i = 0; i < arrxPos.length; i++) {
                var xPos = parseInt(arrxPos[i]);

                for (var column = 0; column < 2; column++) {
                    var columnSpace = column;
                    this.textbox_group1.graphics.f('').s("#707070").ss(0.8).drawRect(xPos + (columnSpace * 21), yPos, 21, 25);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_2 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.setTransform(28, 77);

        this.text_3 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.setTransform(47, 77);

        this.text_4 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.setTransform(28, 138);

        this.text_5 = new cjs.Text("7", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.setTransform(47, 138);

        this.text_6 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.setTransform(28, 199);

        this.text_7 = new cjs.Text("8", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.setTransform(47, 199);

        //inside of Rectangle group Box

        this.text_11 = new cjs.Text("t", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_11.setTransform(336, 188);

        this.text_12 = new cjs.Text("e", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_12.setTransform(354, 188);

        this.text_13 = new cjs.Text("t", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_13.setTransform(397, 188);

        this.text_14 = new cjs.Text("e", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_14.setTransform(415, 188);

        this.text_15 = new cjs.Text("t", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_15.setTransform(459, 188);

        this.text_16 = new cjs.Text("e", "16px 'Myriad Pro'", '#6F6F6E');
        this.text_16.setTransform(479, 188);

        this.text_17 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_17.setTransform(330, 214);

        this.text_18 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_18.setTransform(350, 214);

        this.text_19 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_19.setTransform(392, 214);

        this.text_20 = new cjs.Text("7", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_20.setTransform(413, 214);

        this.text_21 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_21.setTransform(456, 214);

        this.text_22 = new cjs.Text("8", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_22.setTransform(475, 214);

        //green and yellow boxes

        // col-1

        var box_size = 14;
        var box_count = 10;

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f('#20B14A').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_2.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_2.setTransform(335, 31);


        var box_size = 14;
        var box_count = 6;


        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.f('#FFF679').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }
        this.Line_3.setTransform(355, 87);

        // col-2

        var box_size = 14;
        var box_count = 10;


        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f('#20B14A').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_4.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_4.setTransform(395, 31);


        var box_size = 14;
        var box_count = 7;


        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.f('#FFF679').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_5.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_5.setTransform(415, 73);

        // col-3

        var box_size = 14;
        var box_count = 10;


        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.f('#20B14A').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_6.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_6.setTransform(457.5, 31);


        var box_size = 14;
        var box_count = 8;


        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.f('#FFF679').s("#000000").ss(0.7).drawRect(0, 0, box_size, box_size * box_count);

        for (i = 0; i < box_count; i++) {
            this.Line_7.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(0, box_size * i).lineTo(box_size, box_size * i);
        }

        this.Line_7.setTransform(477.5, 59);


        //Rectangle Boxes 4


        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f('#EBF2DA').s("#000000").ss(0.7).drawRect(0, 0, 40, 16.5);
        this.shape_group1.setTransform(331, 175);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(0, 16.5).lineTo(0, 40).lineTo(40, 40).lineTo(40, 16.5);
        this.shape_group2.setTransform(331, 175);

        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 0).lineTo(20, 40);
        this.shape_group3.setTransform(331, 175);


        this.shape_group4 = new cjs.Shape();
        this.shape_group4.graphics.f('#EBF2DA').s("#000000").ss(0.7).drawRect(0, 0, 40, 16.5);
        this.shape_group4.setTransform(393, 175);

        this.shape_group5 = new cjs.Shape();
        this.shape_group5.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(0, 16.5).lineTo(0, 40).lineTo(40, 40).lineTo(40, 16.5);
        this.shape_group5.setTransform(393, 175);

        this.shape_group6 = new cjs.Shape();
        this.shape_group6.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 0).lineTo(20, 40);
        this.shape_group6.setTransform(393, 175);


        this.shape_group7 = new cjs.Shape();
        this.shape_group7.graphics.f('#EBF2DA').s("#000000").ss(0.7).drawRect(0, 0, 40, 16.5);
        this.shape_group7.setTransform(455, 175);

        this.shape_group8 = new cjs.Shape();
        this.shape_group8.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(0, 16.5).lineTo(0, 40).lineTo(40, 40).lineTo(40, 16.5);
        this.shape_group8.setTransform(455, 175);

        this.shape_group9 = new cjs.Shape();
        this.shape_group9.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 0).lineTo(20, 40);
        this.shape_group9.setTransform(455, 175);


        this.addChild(this.roundRect1, this.shape_group10, this.text, this.text_1, this.textbox_group1, this.text_2, this.text_3, this.text_4, this.text_5,
            this.text_6, this.text_7, this.text_8, this.text_9, this.text_10,
            this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7,
            this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5,
            this.shape_group6, this.shape_group7, this.shape_group8, this.shape_group9, this.shape_group11, this.text_11, this.text_12,
            this.text_13, this.text_14, this.text_15, this.text_16, this.text_17, this.text_18, this.text_19, this.text_20, this.text_21, this.text_22);


        this.addChild(this.textbox_group1);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 236.6);



    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Skriv talen som saknas på tallinjen.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 4);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 15, 510, 79, 10);
        this.roundRect1.setTransform(0, 0);


        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(17, 47).lineTo(502, 47);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(502, 47, 1.2, 1.2, 265);


        var ToBeAdded = [];
        var square = [0, 0, 0, 0, 0, 0, 6, 7, 8, 0, 10, 11, 12, 13, 14, 15, 16, 17, 18];

        for (var i = 0; i < 19; i++) {
            var columnSpace = i;


            if (square[i] == 0) {

                var temp_label = new cjs.Text("" + i, "16px 'Myriad Pro'");
                temp_label.lineHeight = 19;
                temp_label.setTransform(26 + (columnSpace * 25), 67);
                ToBeAdded.push(temp_label);

            } else if (square[i] == 10 || square[i] == 11 || square[i] == 12 || square[i] == 13 || square[i] == 14 || square[i] == 15) {

                var temp_label = new cjs.Text("" + i, "16px 'Myriad Pro'");
                temp_label.lineHeight = 19;
                temp_label.setTransform(22 + (columnSpace * 25), 67);
                ToBeAdded.push(temp_label);

            } else if (square[i] == 16 || square[i] == 17 || square[i] == 18) {
                if (square[i] == 16) {
                    var tetboxSpace = 0;
                } else if (square[i] == 17) {
                    var tetboxSpace = 1;
                } else if (square[i] == 18) {
                    var tetboxSpace = 2;
                }

                var temp_label = new cjs.Shape();
                temp_label.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(0, 0, 24, 22);
                temp_label.setTransform(418 + (tetboxSpace * 28), 51);
                ToBeAdded.push(temp_label);

            } else {

                var temp_label = new cjs.Shape();
                temp_label.graphics.f('#ffffff').s("#90BD22").ss(0.9).drawRect(0, 0, 20, 22);
                temp_label.setTransform(20 + (columnSpace * 25), 51);
                ToBeAdded.push(temp_label);
            }


        }


        for (var i = 0; i < 19; i++) {
            var columnSpace = i;
            if(i==17)
            {
                columnSpace=columnSpace+0.2;                               
            }

            else if(i==18)
            {
                columnSpace=columnSpace+0.3;               
            }
            this.temp_Line = new cjs.Shape();
            this.temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(30 + (columnSpace * 25), 39).lineTo(30 + (columnSpace * 25), 47);
            this.temp_Line.setTransform(0, 0);
            ToBeAdded.push(this.temp_Line);
         
             
        }

        this.instance = new lib.p12_1();
        this.instance.setTransform(5, 101.5, 0.47, 0.47);


        this.addChild(this.text, this.text_1, this.roundRect1,
            this.textbox_group2, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.shape_1, this.instance)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 350);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(295, 280, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295, 320, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
