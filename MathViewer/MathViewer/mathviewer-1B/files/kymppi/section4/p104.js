(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p104_1.png",
            id: "p104_1"
        }, {
            src: "images/p104_2.png",
            id: "p104_2"
        }]
    };

    (lib.p104_1 = function() {
        this.initialize(img.p104_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p104_2 = function() {
        this.initialize(img.p104_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.pageBottomText = new cjs.Text("kunna beskriva egenskaper hos trianglar, fyrhörningar och cirklar", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 658);

        this.text_1 = new cjs.Text("104", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(35, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.text_2 = new cjs.Text("Vi övar", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_2.setTransform(95.5, 42);

        this.text_3 = new cjs.Text("36", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(46, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.Symbolimg = new lib.Symbolimg()
        this.Symbolimg.setTransform(31, 39);

        this.addChild(this.Symbolimg, this.shape_2, this.text_3, this.text_2, this.shape_1, this.text_1, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbolimg = function() {
        this.initialize();

        this.instance = new lib.p104_1();
        this.instance.setTransform(0, 0, 0.47, 0.47);
        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    //Part 1
    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Måla", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text(" fyrhörningarna blå", "16px 'Myriad Pro'", "#008BD2");
        this.text_2.setTransform(49, 0);

        this.text_3 = new cjs.Text(",", "16px 'Myriad Pro'", "#000000", "#000000");
        this.text_3.setTransform(177, 0);

        this.text_4 = new cjs.Text(" trianglarna röda", "16px 'Myriad Pro'", "#D51317");
        this.text_4.setTransform(180, 0);

        this.text_5 = new cjs.Text(" och", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(14, 24);

        this.text_6 = new cjs.Text(" cirklarna gröna", "16px 'Myriad Pro'", "#13A538");
        this.text_6.setTransform(42, 24);

        this.text_7 = new cjs.Text(".", "16px 'Myriad Pro'", "#000000");
        this.text_7.setTransform(147, 24);

        //1st Shape

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 252, 227, 12);
        this.roundRect1.setTransform(1, 45);
      
        this.TopRect = new cjs.Shape();
        this.TopRect.graphics.f("").s('#000000').drawRect(0, 0, 120, 15, 0);
        this.TopRect.setTransform(68, 74);

        this.RightRect = this.TopRect.clone(true);
        this.RightRect.setTransform(216, 101);
        this.RightRect.rotation = 90;

        this.BottomRect = this.TopRect.clone(true);
        this.BottomRect.setTransform(68, 234);

        this.LeftRect = this.TopRect.clone(true);
        this.LeftRect.setTransform(55, 101);
        this.LeftRect.rotation = 90;

        this.innercircle = new cjs.Shape();
        this.innercircle.graphics.f("").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 13, 0, 2 * Math.PI);
        this.innercircle.setTransform(128, 160);     

        this.RightOuterTriangleGroup = new cjs.Container();
        this.Triangle_1 = new cjs.Shape();
        this.Triangle_1.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(12, -28).lineTo(22, 0).lineTo(0, 0);
        this.Triangle_1.setTransform(119, 127);

        this.Triangle_2 = new cjs.Shape();
        this.Triangle_2.graphics.f("").beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(12, -28).lineTo(22, 2).lineTo(0, 0);
        this.Triangle_2.setTransform(140, 127);
        this.Triangle_2.rotation = 35;

        this.Triangle_3 = this.Triangle_2.clone(true);
        this.Triangle_3.setTransform(157, 141);
        this.Triangle_3.rotation = 70;

        this.Triangle_4 = this.Triangle_2.clone(true);
        this.Triangle_4.setTransform(163, 162);
        this.Triangle_4.rotation = 105;

        this.Triangle_5 = this.Triangle_2.clone(true);
        this.Triangle_5.setTransform(156, 181);
        this.Triangle_5.rotation = 140;

        this.RightOuterTriangleGroup.addChild(this.Triangle_1, this.Triangle_2, this.Triangle_3, this.Triangle_4, this.Triangle_5);

        this.LeftOuterTriangleGroup = this.RightOuterTriangleGroup.clone(true);
        this.LeftOuterTriangleGroup.setTransform(258, 321);
        this.LeftOuterTriangleGroup.rotation = 180;

        this.outercircle_1 = new cjs.Shape();
        this.outercircle_1.graphics.f("").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 8, 0, 2 * Math.PI);
        this.outercircle_1.setTransform(147, 105);

        this.outercircle_2 = this.outercircle_1.clone(true);
        this.outercircle_2.setTransform(176, 127);

        this.outercircle_3 = this.outercircle_1.clone(true);
        this.outercircle_3.setTransform(187, 161);

        this.outercircle_4 = this.outercircle_1.clone(true);
        this.outercircle_4.setTransform(176, 195);

        this.outercircle_5 = this.outercircle_1.clone(true);
        this.outercircle_5.setTransform(147, 216);

        this.outercircle_6 = this.outercircle_1.clone(true);
        this.outercircle_6.setTransform(110, 216);

        this.outercircle_7 = this.outercircle_1.clone(true);
        this.outercircle_7.setTransform(81, 195);

        this.outercircle_8 = this.outercircle_1.clone(true);
        this.outercircle_8.setTransform(70, 161);

        this.outercircle_9 = this.outercircle_1.clone(true);
        this.outercircle_9.setTransform(81, 127);

        this.outercircle_10 = this.outercircle_1.clone(true);
        this.outercircle_10.setTransform(110, 105);

        this.addChild(this.TopRect, this.RightRect, this.BottomRect, this.LeftRect,
            this.centercircle, this.innercircle, this.RightOuterTriangleGroup, this.LeftOuterTriangleGroup,
            this.outercircle_1, this.outercircle_2, this.outercircle_3, this.outercircle_4,
            this.outercircle_5, this.outercircle_6, this.outercircle_7, this.outercircle_8, this.outercircle_9, this.outercircle_10);

        //2nd Shape

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 252, 227, 12);
        this.roundRect2.setTransform(258, 45);

        this.LeftTriangleGroup = new cjs.Container();
        this.LeftTopTriangle = new cjs.Shape();
        this.LeftTopTriangle.graphics.f("").beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 3).lineTo(0, 70).lineTo(-24, 70).lineTo(0, 3);
        this.LeftTopTriangle.setTransform(305, 90);

        this.LeftBottomTriangle = this.LeftTopTriangle.clone(true);
        this.LeftBottomTriangle.setTransform(305, 235);
        this.LeftBottomTriangle.rotation = 180;
        this.LeftBottomTriangle.skewY = 180;

        this.LeftTriangleGroup.addChild(this.LeftTopTriangle, this.LeftBottomTriangle);

        this.RightTriangleGroup = this.LeftTriangleGroup.clone(true);
        this.RightTriangleGroup.setTransform(765, 0);
        this.RightTriangleGroup.rotation = 180;
        this.RightTriangleGroup.skewX = 180;

        this.TopTriangleGroup = new cjs.Container();
        this.TopLeftTriangle = new cjs.Shape();
        this.TopLeftTriangle.graphics.f("").beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(70, 0).lineTo(70, -24).lineTo(0, 0);
        this.TopLeftTriangle.setTransform(309, 89);

        this.TopRightTriangle = new cjs.Shape();
        this.TopRightTriangle.graphics.f("").beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(70, 0).lineTo(70, -24).lineTo(0, 0);
        this.TopRightTriangle.setTransform(455, 89);
        this.TopRightTriangle.rotation = 180;
        this.TopRightTriangle.skewX = 180;

        this.TopTriangleGroup.addChild(this.TopLeftTriangle, this.TopRightTriangle);

        this.BottomTriangleGroup = this.TopTriangleGroup.clone(true);
        this.BottomTriangleGroup.setTransform(0, 324);
        this.BottomTriangleGroup.rotation = 180;
        this.BottomTriangleGroup.skewY = 180;

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#7d7d7d').drawRect(0, 0, 26, 26, 12);
        this.Rect1.setTransform(368, 150);

        this.TopTriangle = new cjs.Shape();
        this.TopTriangle.graphics.f("").beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(13, -30).lineTo(26, 0);
        this.TopTriangle.setTransform(368, 150);

        this.RightTriangle = this.TopTriangle.clone(true);
        this.RightTriangle.setTransform(394, 150);
        this.RightTriangle.rotation = 90;

        this.BottomTriangle = this.TopTriangle.clone(true);
        this.BottomTriangle.setTransform(395, 175);
        this.BottomTriangle.rotation = 180;

        this.LeftTriangle = this.TopTriangle.clone(true);
        this.LeftTriangle.setTransform(368, 176);
        this.LeftTriangle.rotation = 270;

        this.circle_1 = new cjs.Shape();
        this.circle_1.graphics.f("").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 8, 0, 2 * Math.PI);
        this.circle_1.setTransform(381, 112);

        this.circle_2 = this.circle_1.clone(true);
        this.circle_2.setTransform(330, 162);

        this.circle_3 = this.circle_1.clone(true);
        this.circle_3.setTransform(432, 162);

        this.circle_4 = this.circle_1.clone(true);
        this.circle_4.setTransform(381, 213);

        this.TopleftcornerShape = new cjs.Shape();
        this.TopleftcornerShape.graphics.f("").beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(-13, -25).lineTo(-47, -25).lineTo(-31, 0).lineTo(0, 0);
        this.TopleftcornerShape.setTransform(368, 150);

        this.ToprightcornerShape = this.TopleftcornerShape.clone(true);
        this.ToprightcornerShape.setTransform(395, 150);
        this.ToprightcornerShape.rotation = 120;

        this.BottomrightcornerShape = this.TopleftcornerShape.clone(true);
        this.BottomrightcornerShape.setTransform(395, 175);
        this.BottomrightcornerShape.rotation = 180;

        this.BottomleftcornerShape = this.TopleftcornerShape.clone(true);
        this.BottomleftcornerShape.setTransform(368, 175);
        this.BottomleftcornerShape.rotation = 300;

        this.addChild(this.text, this.text_1, this.roundRect1, this.roundRect2, this.Rect1, this.LeftTriangleGroup,
            this.RightTriangleGroup, this.TopTriangleGroup, this.BottomTriangleGroup, this.TopTriangle, this.RightTriangle, this.BottomTriangle, this.LeftTriangle,
            this.circle_1, this.circle_2, this.circle_3, this.circle_4, this.TopleftcornerShape, this.ToprightcornerShape,
            this.BottomrightcornerShape, this.BottomleftcornerShape, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 513.3, 270);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(297, 419, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
