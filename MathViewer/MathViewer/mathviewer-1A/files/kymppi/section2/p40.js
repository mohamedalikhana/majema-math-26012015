(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p40_1.png",
            id: "p40_1"
        }, {
            src: "images/p40_2.png",
            id: "p40_2"
        }]
    };
    // symbols:

    (lib.p40_1 = function() {
        this.initialize(img.p40_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p40_2 = function() {
        this.initialize(img.p40_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageFooterTextRight = new cjs.Text("kunna lösa uppgifter med addition 0 till 6", "9px 'Myriad Pro'");
        this.pageFooterTextRight.lineHeight = 11;
        this.pageFooterTextRight.setTransform(380, 653.7);
        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med addition 0 till 6", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 651);

        this.pageNoLeft = new cjs.Text("40", "13px 'Myriad Pro'", "#FFFFFF");
        this.pageNoLeft.lineHeight = 18;
        this.pageNoLeft.setTransform(38, 648);

        this.pageNoLeftBackground = new cjs.Shape();
        this.pageNoLeftBackground.graphics.f("#7AC729").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.pageNoLeftBackground.setTransform(31.1, 660.8);

        this.pageTitle = new cjs.Text("Vi skriver addition", "24px 'MyriadPro-Semibold'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95, 26.5);

        this.chapterNumber = new cjs.Text("13", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(44, 22.5);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoLeftBackground, this.pageNoLeft, this.pageTitle, this.chapterNumberBackground, this.chapterNumber, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p40_1();
        this.instance.setTransform(0, 26, 0.49, 0.49);

        this.label1 = new cjs.Text("2 och 1 är 3.", "16px 'Myriad Pro'");
        this.label1.lineHeight = 34;
        this.label1.setTransform(78, 40);
        this.label2 = new cjs.Text("3 och 1 är 4.", "16px 'Myriad Pro'");
        this.label2.lineHeight = 34;
        this.label2.setTransform(345, 40);
        this.label3 = new cjs.Text("2 plus 1 är lika med 3.", "16px 'Myriad Pro'");
        this.label3.lineHeight = 34;
        this.label3.setTransform(60, 160);
        this.label4 = new cjs.Text("3 plus 1 är lika med 4.", "16px 'Myriad Pro'");
        this.label4.lineHeight = 34;
        this.label4.setTransform(320, 160);
        var textToBeDisplayed1 = ['2', '+', '1', '=', '3'];

        this.textBoxGroup1 = new cjs.Shape();

        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 132, 20, 22);
            this.labelTemp1 = new cjs.Text(textToBeDisplayed1[i], "bold 35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp1.lineHeight = 40;
            this.labelTemp1.setTransform(78 + (i * 20), 122);
            textToBeDisplayed1[i] = this.labelTemp1;
        }
        this.textBoxGroup1.setTransform(0, 0);

        var textToBeDisplayed2 = ['3', '+', '1', '=', '4'];
        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 132, 20, 22);
            this.labelTemp2 = new cjs.Text(textToBeDisplayed2[i], "bold 35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp2.lineHeight = 40;
            this.labelTemp2.setTransform(338 + (i * 20), 122);
            textToBeDisplayed2[i] = this.labelTemp2;
        }
        this.textBoxGroup2.setTransform(0, 0);
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#fff779").s('#7d7d7d').drawRoundRect(0, 0, ((1038) - 10) * 0.49, ((321) - 2) * 0.49, 10);
        this.roundRect1.setTransform(3, 27);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#0290D4").s("#000").ss(0.5, 0, 0, 4).arc(0, 0, 9, 0, 2 * Math.PI);
        this.shape_1.setTransform(355.3, 77.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#0290D4").s("#000").ss(0.5, 0, 0, 4).arc(0, 0, 9, 0, 2 * Math.PI);
        this.shape_2.setTransform(344, 100);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#0290D4").s("#000").ss(0.5, 0, 0, 4).arc(0, 0, 9, 0, 2 * Math.PI);
        this.shape_3.setTransform(367, 100);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#24B249").s("#000").ss(0.5, 0, 0, 4).arc(0, 0, 9, 0, 2 * Math.PI);
        this.shape_4.setTransform(426.5, 92.5);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.textBoxGroup1,
            this.textBoxGroup2, this.label1, this.label2, this.label3, this.label4, this.shape_1, this.shape_2, this.shape_3,
            this.shape_4);
        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed1[i]);
        }
        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed2[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75);


    (lib.Symbol3 = function() {
        this.initialize();
        this.instance = new lib.p40_2();
        this.instance.setTransform(0, 26, 0.49, 0.49);
        this.text = new cjs.Text("Hur många fåglar är det tillsammans?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 8.5);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 8.5);

        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 123.5, 20, 22);
        }
        this.textBoxGroup1.setTransform(0, 0);

        var textToBeDisplayed1 = ['1', '+', '1', '=', '2'];
        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 123.5, 20, 22);
            this.labelTemp1 = new cjs.Text(textToBeDisplayed1[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp1.lineHeight = 34;
            this.labelTemp1.setTransform(78 + (i * 20), 113.6);
            textToBeDisplayed1[i] = this.labelTemp1;
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 253, 20, 22);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 253, 20, 22);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 382, 20, 22);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 382, 20, 22);
        }
        this.textBoxGroup6.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((1044 * 0.5) - 10) * 0.49, ((789 * (1 / 3)) - 15) * 0.49, 10);
        this.roundRect1.setTransform(3, 31);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1044 * 0.5) + 8) * 0.49, 31);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 160);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1044 * 0.5) + 8) * 0.49, 160);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(3, 289);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((1044 * 0.5) + 8) * 0.49, 289);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.instance, this.text, this.text_1, this.textBoxGroup1, this.textBoxGroup2, this.textBoxGroup3, this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6);
        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed1[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294.3, 260.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(293.3, 85.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
