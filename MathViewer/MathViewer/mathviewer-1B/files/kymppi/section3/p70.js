(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p70_1.png",
            id: "p70_1"
        }, {
            src: "images/p70_2.png",
            id: "p70_2"
        }]
    };

    (lib.p70_1 = function() {
        this.initialize(img.p70_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p70_2 = function() {
        this.initialize(img.p70_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("70", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 661);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna och måla svaret.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 14, 511, 267, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p70_1();
        this.instance.setTransform(357, 45, 0.46, 0.46);

        this.label1 = new cjs.Text("7 + 4 =", "16px 'Myriad Pro'");
        this.label1.setTransform(40, 43);

        this.label2 = new cjs.Text("8 + 6 =", "16px 'Myriad Pro'");
        this.label2.setTransform(40, 73);

        this.label3 = new cjs.Text("9 + 8 =", "16px 'Myriad Pro'");
        this.label3.setTransform(40, 103);

        this.label4 = new cjs.Text("8 + 8 =", "16px 'Myriad Pro'");
        this.label4.setTransform(40, 133);

        this.label5 = new cjs.Text("7 + 6 =", "16px 'Myriad Pro'");
        this.label5.setTransform(40, 163);

        this.label6 = new cjs.Text("3 + 7 =", "16px 'Myriad Pro'");
        this.label6.setTransform(40, 193);


        this.label7 = new cjs.Text("6 + 6 =", "16px 'Myriad Pro'");
        this.label7.setTransform(198, 43);

        this.label8 = new cjs.Text("7 + 8 =", "16px 'Myriad Pro'");
        this.label8.setTransform(198, 73);

        this.label9 = new cjs.Text("9 + 9 =", "16px 'Myriad Pro'");
        this.label9.setTransform(198, 103);

        this.label10 = new cjs.Text("18 – 9 =", "16px 'Myriad Pro'");
        this.label10.setTransform(190, 133);

        this.label11 = new cjs.Text("17 – 9 =", "16px 'Myriad Pro'");
        this.label11.setTransform(190, 163);

        this.label12 = new cjs.Text("16 – 9 =", "16px 'Myriad Pro'");
        this.label12.setTransform(190, 193);

        // ROW-1 ROUND SHAPES--columns1
        this.hrLine_1 = new cjs.Shape();
        this.shape_group2 = new cjs.Shape();
        var fillcolor = "";
        var columnXpos = 0;
        var MoveToX = 0;
        var LineToX = 0;
        var LineToY = 0;
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {

                if (row < 2 && column == 0) {
                    fillcolor = "#008BD2";
                    columnXpos = 144;
                } else if (row > 2 && column == 0) {
                    fillcolor = "#FFF374";
                    columnXpos = 144;
                } else if (row < 4 && column == 1) {
                    fillcolor = "#D51317";
                    columnXpos = 275;
                } else if (row == 4 && column == 1) {
                    fillcolor = "#008BD2";
                    columnXpos = 275;
                } else if (row == 5 && column == 1) {
                    fillcolor = "#FFF374";
                    columnXpos = 275;
                }
                this.shape_group2.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(columnXpos + (columnSpace * 25), 42 + (row * 29), 9.5, 0, 2 * Math.PI)

                if (column == 0) {
                    MoveToX = 95;
                    LineToX = 130;
                    LineToY = 51;
                } else if (column == 1) {
                    MoveToX = 225;
                    LineToX = 260;
                    LineToY = 51;
                }
                this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(MoveToX + (25 * columnSpace), LineToY + (29 * row)).lineTo(LineToX + (25 * columnSpace), LineToY + (29 * row));

            }
        }
        this.shape_group2.setTransform(0, 0);
        this.hrLine_1.setTransform(0, 0);

        var ToBeAdded = [];
        this.textbox_group2 = new cjs.Shape();
        var i = 7;
        var colXpos=65;
        for (var column = 0; column < 12; column++) {
            var columnSpace = column;
            this.textbox_group2.graphics.f('#ffffff').s("#878787").ss(0.7).drawRect(57 + (columnSpace * 25), 230, 25, 27);
            var temp_label = new cjs.Text(i, "16px 'Myriad Pro'");
            if (i>9) {colXpos=60}
            temp_label.setTransform(colXpos + (columnSpace * 25), 249);
            ToBeAdded.push(temp_label);
            i++
        }
        this.textbox_group2.setTransform(0, 0);



        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12)
        this.addChild(this.shape_group2, this.hrLine_1, this.instance, this.textbox_group2);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 572.3, 250);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("7.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(5, 0);

        this.text_1 = new cjs.Text("Nellys och Leos kort är lika mycket värda.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.text_2 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_2.setTransform(24, 20);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7D7D7D').drawRoundRect(0, 0, 252, 108, 10);
        this.roundRect1.setTransform(5, 32);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(263, 32);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(5, 147);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(263, 147);

        this.instance = new lib.p70_2();
        this.instance.setTransform(50, 47, 0.46, 0.46);

        var ToBeAdded = [];
        var xpos = 28;
        var ypos = 105;
        fColor = '#DFDDF0';
        sColor = '#958FC5';
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 217
            } else {
                ypos = 105
            }

            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 3; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 146
                        } else if (numOfcol == 2) {
                            xpos = 287
                        } else if (numOfcol == 3) {
                            xpos = 405
                        } else {
                            xpos = 28
                        }
                        if (numOfrow == 0 && numOfcol < 1) {
                            fColor = '#DFDDF0';
                            sColor = '#958FC5';
                        }
                        if (numOfrow == 1 && numOfcol < 1) {
                            fColor = '#C4E5F0';
                            sColor = '#00A3C4';
                        }
                        if (numOfrow == 0 && numOfcol > 1) {
                            fColor = '#E1EBC5';
                            sColor = '#90BD22';
                        }
                        if (numOfrow == 1 && numOfcol > 1) {
                            fColor = '#FBD3B9';
                            sColor = '#EB5B1B';
                        }
                        if (column == 2 && numOfcol == 1) {
                            fColor = '#FFFFFF';
                            sColor = '#706F6F';
                        }
                        if (column == 2 && numOfcol == 3) {
                            fColor = '#FFFFFF';
                            sColor = '#706F6F';
                        }
                        if (column == 1 && numOfrow == 1) {
                            ypos = ypos - 4
                        } else if (column == 0 && numOfrow == 1) {
                            ypos = 217
                        } else if (column == 2 && numOfrow == 1) {
                            ypos = 217
                        }
                        if (column == 1 && numOfrow == 0) {
                            ypos = ypos - 4
                        } else if (column == 0 && numOfrow == 0) {
                            ypos = 105
                        } else if (column == 2 && numOfrow == 0) {
                            ypos = 105
                        }
                        tmp_Rect.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xpos + (columnSpace * 33), ypos + (row * 20), 23, 24);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }

        var xpos = 35;
        var ypos = 122;
        var tmpNum = [6, 5, 4, 3, 9, 3, 8, 3, 7, 5, 8, 5, 4, 9, 2, 7, 6, 3, 3, 9];
        var i = 0;
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 233
            } else {
                ypos = 122
            }

            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 3; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 153
                        } else if (numOfcol == 2) {
                            xpos = 293
                        } else if (numOfcol == 3) {
                            xpos = 413
                        } else {
                            xpos = 35
                        }
                        if (column == 2 && numOfcol == 1) {
                            continue;
                        }
                        if (column == 2 && numOfcol == 3) {
                            continue;
                        }

                        if (column == 1 && numOfrow == 1) {
                            ypos = ypos - 3
                        } else if (column == 0 && numOfrow == 1) {
                            ypos = 233
                        } else if (column == 2 && numOfrow == 1) {
                            ypos = 233
                        }
                        if (column == 1 && numOfrow == 0) {
                            ypos = ypos - 3
                        } else if (column == 0 && numOfrow == 0) {
                            ypos = 122
                        } else if (column == 2 && numOfrow == 0) {
                            ypos = 122
                        }
                        var tempText = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                        tempText.setTransform(xpos + (columnSpace * 33), ypos + (row * 20));
                        ToBeAdded.push(tempText);
                        i++;
                    }
                }
            }
        }


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        this.addChild(this.text_1, this.textbox_group1, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.text_2);
        this.addChild(this.text, this.instance, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 150);



    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296, 274, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296, 383, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
