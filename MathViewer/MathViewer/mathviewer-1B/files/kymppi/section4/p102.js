(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p102_1.png",
            id: "p102_1"
        }, {
            src: "images/p102_2.png",
            id: "p102_2"
        }]
    };

    (lib.p102_1 = function() {
        this.initialize(img.p102_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p102_2 = function() {
        this.initialize(img.p102_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Äldre måttenheter", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(155, 73);

        this.text_1 = new cjs.Text("35", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(40, 43);

        this.text_3 = new cjs.Text("ha fått arbeta med måttenheter som användes förr", "9px 'Myriad Pro'", "#FAAA33");
        this.text_3.setTransform(85, 658);

        this.text_4 = new cjs.Text("102", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(32, 659);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 215, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 499, 148, 5);
        this.round_Rect1.setTransform(4, 20);

        this.instance = new lib.p102_1();
        this.instance.setTransform(30, 15, 0.47, 0.47);

        this.text_1 = new cjs.Text("tum", "16px 'Myriad Pro'");
        this.text_1.setTransform(35, 147);

        this.text_2 = new cjs.Text("tvärhand", "16px 'Myriad Pro");
        this.text_2.setTransform(115, 147);

        this.text_3 = new cjs.Text("fot", "16px 'Myriad Pro");
        this.text_3.setTransform(245, 147);

        this.text_4 = new cjs.Text("aln", "16px 'Myriad Pro");
        this.text_4.setTransform(397, 147);

        this.addChild(this.round_Rect1, this.instance, this.text_1, this.text_2, this.text_3, this.text_4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 270);

    (lib.SymbolRect = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 199, 125, 5);
        this.round_Rect1.setTransform(0, 5);

        this.addChild(this.round_Rect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 150);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p102_2();
        this.instance.setTransform(0, 12, 0.47, 0.47);

        this.text_1 = new cjs.Text("Mät saker med måttenheterna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(58, 44);

        this.text_2 = new cjs.Text("Gör först en uppskattning. Rita och skriv.", "16px 'Myriad Pro");
        this.text_2.setTransform(58, 67);

        this.text_3 = new cjs.Text("Det här vill jag mäta:", "16px 'Myriad Pro");
        this.text_3.setTransform(58, 97);

        this.round_Rect1 = new lib.SymbolRect();
        this.round_Rect1.setTransform(58, 107);

        this.round_Rect2 = new lib.SymbolRect();
        this.round_Rect2.setTransform(58, 249);

        var TextArr = [];
        var TArr = ['Måttenhet:', 'Uppskattning:', 'Mätning:', 'Måttenhet:', 'Uppskattning:', 'Mätning:'];
        var TextX = [270, 270, 270, 270, 270, 270];
        var TextY = [148, 183, 218, 290, 325, 360];
        for (var i = 0; i < TextX.length; i++) {

            var text_4 = new cjs.Text(TArr[i], "16px 'Myriad Pro'", "#000000");
            text_4.setTransform(TextX[i], TextY[i]);
            TextArr.push(text_4);
        }

        var hrlineArr = [];
        var hrlineX = [347, 368, 333, 347, 368, 333];
        var hrlineY = [128, 163, 197, 270, 304, 340];

        for (var i = 0; i < hrlineX.length; i++) {
            var hrLine_1 = new cjs.Shape();
            if (i == 0 || i == 3) {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(155, 20);
            } else if (i == 1 || i == 4) {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(133, 20);
            } else {
                hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 20).lineTo(169, 20);
            }

            hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
            hrlineArr.push(hrLine_1);
        }       

        this.addChild(this.instance, this.text_1, this.text_2, this.text_3, this.round_Rect1, this.round_Rect2);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 573.3, 400);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(302, 245, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(302, 420, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
