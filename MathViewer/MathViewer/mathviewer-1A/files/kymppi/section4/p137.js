(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p137_1.png",
            id: "p137_1"
        }]
    };

    // symbols:
    (lib.p137_1 = function() {
        this.initialize(img.p137_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("137", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(553, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#E0E4F2").s('#8490C8').drawRoundRect(0, 10, 515, 240, 10);
        this.roundRect1.setTransform(0, 17);

        this.instance = new lib.p137_1();
        this.instance.setTransform(70, 201, 0.47, 0.47);

        this.text_h1 = new cjs.Text("Djur på bondgården", "15px 'MyriadPro-Semibold'");
        this.text_h1.lineHeight = 19;
        this.text_h1.setTransform(175, 39);

        var ToBeAdded = [];
        for (var num = 0; num < 11; num++) {
            var xPos = 69;
            var temptext = new cjs.Text("" + num, "12px 'Myriad Pro'");
            temptext.lineHeight = -1;
            if (num == 10) {
                xPos = 62;
                num = 10.15;
            }
            temptext.setTransform(xPos, 194 + (-13.3 * num));
            ToBeAdded.push(temptext);
        }

        this.shape_arrow1 = new cjs.Shape();
        this.shape_arrow1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow1.setTransform(85.8, 52, 1, 1, 175);

        this.shape_arrow2 = new cjs.Shape();
        this.shape_arrow2.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow2.setTransform(438, 199.8, 1, 1, 265);

        this.Line_bg1 = new cjs.Shape();
        this.Line_bg1.graphics.f("#ffffff").beginStroke("#ffffff").setStrokeStyle(0.7).moveTo(86, 65).lineTo(86, 200).lineTo(414, 200).lineTo(414, 65).lineTo(86, 65);
        this.Line_bg1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(86, 55).lineTo(86, 200).moveTo(80.5, 200).lineTo(436, 200)
            .moveTo(96, 65).lineTo(96, 200).moveTo(120, 65).lineTo(120, 200)
            .moveTo(168, 65).lineTo(168, 200).moveTo(192, 65).lineTo(192, 200)
            .moveTo(243.6, 65).lineTo(243.6, 200).moveTo(267.6, 65).lineTo(267.6, 200).moveTo(316, 65).lineTo(316, 200).moveTo(340, 65).lineTo(340, 200).moveTo(390, 65).lineTo(390, 200).moveTo(414, 65).lineTo(414, 200);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#949599").setStrokeStyle(0.7).moveTo(80.5, 186.5).lineTo(414, 186.5).moveTo(80.5, 173).lineTo(414, 173).moveTo(80.5, 159.5).lineTo(414, 159.5).moveTo(80.5, 146).lineTo(414, 146).moveTo(80.5, 119).lineTo(414, 119).moveTo(80.5, 105.5).lineTo(414, 105.5).moveTo(80.5, 92).lineTo(414, 92).moveTo(80.5, 78.5).lineTo(414, 78.5);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(80.5, 65).lineTo(422, 65).moveTo(80.5, 132.5).lineTo(422, 132.5);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f("#20B14A").s("#20B14A").setStrokeStyle(0.7)
            .moveTo(96, 146).lineTo(96, 200).lineTo(120, 200).lineTo(120, 146).lineTo(96, 146)
            .moveTo(168, 65).lineTo(168, 200).lineTo(192, 200).lineTo(192, 65).lineTo(168, 65)
            .moveTo(243.6, 119.5).lineTo(243.6, 200).lineTo(267.6, 200).lineTo(267.6, 119.5).lineTo(243.6, 119.5)
            .moveTo(316, 133).lineTo(316, 200).lineTo(340, 200).lineTo(340, 133).lineTo(316, 133)
            .moveTo(390, 173).lineTo(390, 200).lineTo(414, 200).lineTo(414, 173).lineTo(390, 173);
        this.Line_4.setTransform(0, 0);

        this.addChild(this.roundRect1, this.instance, this.text_h1, this.Line_bg1, this.Line_4, this.Line_1, this.Line_2, this.Line_3, this.Line_5, this.shape_arrow1, this.shape_arrow2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i])
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 280);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Vad stämmer? Kryssa.", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(19, 0);

        this.text_q1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 27, 515, 270, 10);
        this.roundRect1.setTransform(0, 0);

        this.text_1 = new cjs.Text("Stämmer", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(316, 40);
        this.text_2 = new cjs.Text("Stämmer inte", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(407.5, 40);
        this.text_3 = new cjs.Text("Det är flest får.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(13, 73.5);
        this.text_4 = new cjs.Text("Det är 3 fler får än hästar.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(13, 111);
        this.text_5 = new cjs.Text("Det är 5 hönor.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(13,148);
        this.text_6 = new cjs.Text("Det är 3 färre katter än kor.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 40;
        this.text_6.setTransform(13, 186);
        this.text_7 = new cjs.Text("Det är 9 hönor och hästar tillsammans.", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 40;
        this.text_7.setTransform(13, 224.5);
        this.text_8 = new cjs.Text("Det är minst antal katter.", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 40;
        this.text_8.setTransform(13, 262);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(15, 63).lineTo(500, 63).moveTo(15, 101).lineTo(500, 101).moveTo(15, 139).lineTo(500, 139).moveTo(15, 177).lineTo(500, 177).moveTo(15, 215).lineTo(500, 215).moveTo(15, 253).lineTo(500, 253);
        this.hrLine_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                var rowSpace = row;
                if(row==2){
                    rowSpace = 2.04;
                } else if(row==3){
                    rowSpace = 3.05;
                } else if(row==4){
                    rowSpace = 4.08;
                } else if(row==5){
                    rowSpace = 5.12;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#6D6E70").ss(0.8).drawRect(343 + (columnSpace * 102.5), 72 + (rowSpace * 37), 20, 22.5);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text_q1, this.roundRect1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.hrLine_1, this.textbox_group1);  
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 305.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(309, 100, 1, 1, 0, 0, 0, 255.8, 53.5);
        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 376, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
