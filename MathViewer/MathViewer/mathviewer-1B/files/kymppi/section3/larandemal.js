var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: []
    };

    (lib.Stage1_1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#958FC5').ss(3).drawRoundRect(210, 0, 1100, 320, 13);
        this.roundRect1.setTransform(-100, -30);

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#958FC5").s("#958FC5").ss(0.5, 0, 0, 4).arc(210, 0, 30, 0, 30 * Math.PI);
        this.shape_group1.setTransform(-100, -30);

        this.text_No = new cjs.Text("3", "35px 'MyriadPro-Semibold'", "#ffffff");
        this.text_No.setTransform(100, -20);

        this.textRect = new cjs.Shape();
        this.textRect.graphics.f("#ffffff").s('#958FC5').ss(3).drawRoundRect(330, -25, 610, 50, 19);
        this.textRect.setTransform(-100, -30);

        this.text_1 = new cjs.Text("SUBTRAKTION MED TIOTALSÖVERGÅNG", "bold 28px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(280, -18);

        this.text_2 = new cjs.Text("•  kunna lösa uppgifter i subtraktion 0 till 20, med tiotalsövergång", "35px 'Myriad Pro'", "#958FC5");
        this.text_2.setTransform(230, 55);

        this.text_3 = new cjs.Text("•  kunna skriva regeln för enkla talföljder", "35px 'Myriad Pro'", "#958FC5");
        this.text_3.setTransform(230, 115);

        this.text_4 = new cjs.Text("•  förstå begreppen summa och differens", "35px 'Myriad Pro'", "#958FC5");
        this.text_4.setTransform(230, 175);

        this.text_5 = new cjs.Text("•  ha fått arbeta med problemlösning", "35px 'Myriad Pro'", "#958FC5");
        this.text_5.setTransform(230, 235);

        this.addChild(this.roundRect1, this.shape_group1, this.text_No, this.textRect, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage1_1();
        stage1_1.setTransform(0, 25);

        this.addChild(stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
