(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p147_1.png",
            id: "p147_1"
        }, {
            src: "images/p147_2.png",
            id: "p147_2"
        }]
    };

    (lib.p147_1 = function() {
        this.initialize(img.p147_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);
    (lib.p147_2 = function() {
        this.initialize(img.p147_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("147", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(551, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.7).drawRect(468 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Färglägg halsbandet efter instruktionen.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(6, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D83770");
        this.text.lineHeight = 20;
        this.text.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 510.5, 171, 10);
        this.roundRect1.setTransform(5, 15);

        var ToBeAdded = [];
        var arrText = ['2', '3', '2', '3', '3', '1', '3', '1', '2'];
        var loopCnt = 0;
        for (var col = 0; col < 2; col++) {
            for (var row = 0; row < 5; row++) {
                if (col == 0 && row == 4) {
                    continue;
                }
                var text = arrText[loopCnt];
                var rowSpace = row;
                var colSpace = col;
                if (row == 2) {
                    rowSpace = 2.13;
                } else if (row == 3) {
                    rowSpace = 3.15;
                } else if (row == 4) {
                    rowSpace = 4.27;
                }
                if (col == 1 && row == 4) {
                    colSpace = 0.996;
                }
                var temptext = new cjs.Text(text, "16px 'Myriad Pro'");
                temptext.lineHeight = -1;
                temptext.setTransform(40 + (260 * colSpace), 56 + (24 * rowSpace));
                if (row == 2 && col == 1) {
                    rowSpace = 1.99;
                    colSpace = 1.009;
                }
                ToBeAdded.push(temptext);
                loopCnt++;
            }
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            var height = (column == 0) ? 109 : 134;
            for (var row = 0; row < 1; row++) {
                var rowSpace = row;
                this.textbox_group1.graphics.f('#ffffff').s("#D83770").ss(0.8).drawRect(30 + (columnSpace * 258), 44 + (rowSpace * 22), 60, height);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.instance = new lib.p147_1();
        this.instance.setTransform(100, 35, 0.47, 0.47);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 5; row++) {
                var fColor = "";
                var rowSpace = row;
                if ((row == 0 && column == 0) || (row == 3 && column == 1)) {
                    fColor = "#E7544E"; //red
                } else if ((row == 1 && column == 0) || (row == 2 && column == 1) || (row == 4 && column == 1)) {
                    fColor = "#1CB0E7"; //blue
                } else if ((row == 2 && column == 0) || (row == 0 && column == 1)) {
                    fColor = "#FFF23C"; //yellow
                } else if ((row == 3 && column == 0) || (row == 1 && column == 1)) {
                    fColor = "#20B14A"; //green
                } else if (row == 4 && column == 0) {
                    continue;
                }

                if (row == 2) {
                    rowSpace = 2.13;
                } else if (row == 3) {
                    rowSpace = 3.15;
                } else if (row == 4) {
                    rowSpace = 4.3;
                }

                this.shape_group1.graphics.f(fColor).s("#000000").ss(0.8, 0, 0, 4).arc(68 + (columnSpace * 258), 62 + (rowSpace * 24), 8.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.line_1 = new cjs.Shape();
        this.line_1.graphics.s("#949599").ss(0.8).moveTo(268, 42).lineTo(268, 185);
        this.line_1.setTransform(0, 0);

        this.line_2 = new cjs.Shape();
        this.line_2.graphics.s("#000000").f("#000000").ss(0.8).moveTo(24, 44).lineTo(24, 64).lineTo(26, 63).lineTo(24, 69).lineTo(22, 63).lineTo(24, 64)
            .moveTo(283, 44).lineTo(283, 64).lineTo(285, 63).lineTo(283, 69).lineTo(281, 63).lineTo(283, 64);
        this.line_2.setTransform(0, 0);

        this.shape_arrow1 = new cjs.Shape();
        this.shape_arrow1.graphics.f("#000000").s().p("AgegVIA9AVIg9AXg");
        this.shape_arrow1.setTransform(118.5, 50, 1, 1, 137);
        this.shape_arc1 = new cjs.Shape();
        this.shape_arc1.graphics.f().s("#000000").ss(0.6, 0, 0, 4).p("AgsAhQAVgfAZgPQAZgQAXAA");
        this.shape_arc1.setTransform(124, 46);

        this.shape_arrow2 = new cjs.Shape();
        this.shape_arrow2.graphics.f("#000000").s().p("AgegVIA9AVIg9AXg");
        this.shape_arrow2.setTransform(373.5, 50, 1, 1, 137);
        this.shape_arc2 = new cjs.Shape();
        this.shape_arc2.graphics.f().s("#000000").ss(0.6, 0, 0, 4).p("AgsAhQAVgfAZgPQAZgQAXAA");
        this.shape_arc2.setTransform(379, 46);

        this.addChild(this.instance_2, this.text, this.roundRect1, this.instance, this.textbox_group1, this.shape_group1, this.line_1, this.line_2, this.shape_arc1, this.shape_arc2, this.shape_arrow1, this.shape_arrow2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 290);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text("Dra streck mellan bilderna i rätt ordning.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(22, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#D83770");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 25, 510, 330, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p147_2();
        this.instance.setTransform(22, 35, 0.47, 0.47);

        this.line_1 = new cjs.Shape();
        this.line_1.graphics.s("#DA2129").f("#DA2129").ss(0.8).moveTo(42, 79).lineTo(42, 93).lineTo(45, 93).lineTo(42, 97).lineTo(39, 93).lineTo(42, 93)
            .moveTo(42, 130).lineTo(42, 144).lineTo(45, 144).lineTo(42, 148).lineTo(39, 144).lineTo(42, 144)
            .moveTo(42, 180).lineTo(42, 194).lineTo(45, 194).lineTo(42, 198).lineTo(39, 194).lineTo(42, 194)
            .moveTo(42, 233).lineTo(42, 247).lineTo(45, 247).lineTo(42, 251).lineTo(39, 247).lineTo(42, 247)
            .moveTo(42, 290).lineTo(42, 304).lineTo(45, 304).lineTo(42, 308).lineTo(39, 304).lineTo(42, 304);
        this.line_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_2.lineHeight = 50;
        this.text_2.setTransform(251, 61);
        this.text_3 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_3.lineHeight = 50;
        this.text_3.setTransform(129, 156);
        this.text_4 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_4.lineHeight = 50;
        this.text_4.setTransform(373.5, 156);
        this.text_5 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_5.lineHeight = 50;
        this.text_5.setTransform(328, 307);
        this.text_6 = new cjs.Text("•", "bold 19px 'Myriad Pro'");
        this.text_6.lineHeight = 50;
        this.text_6.setTransform(176, 309);

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.line_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 340);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(307, 274, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(307, 280, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
