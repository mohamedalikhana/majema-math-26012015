(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p110_1.png",
            id: "p110_1"
        }, {
            src: "images/p110_2.png",
            id: "p110_2"
        }]
    };

    (lib.p110_1 = function() {
        this.initialize(img.p110_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p110_2 = function() {
        this.initialize(img.p110_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {


        this.pageTitle = new cjs.Text("Pengar", "24px 'MyriadPro-Semibold'", "#8390C8");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(94, 25);

        this.text_1 = new cjs.Text("38", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(45, 22.6);

        this.text_2 = new cjs.Text("110", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(36, 648);


        this.pageBottomText = new cjs.Text("kunna räkna addition och subtraktion med mynt upp till 10 kr", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(70, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(44, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8390C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.pageTitle, this.shape_2, this.shape_1, this.shape, this.text_1, this.text_2,
            this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p110_1();
        this.instance.setTransform(43, 70, 0.5, 0.5);

        this.label1 = new cjs.Text("Det här är våra mynt. Kronor förkortas kr.", "16px 'Myriad Pro'");
        this.label1.lineHeight = 34;
        this.label1.setTransform(9.5, 36);

        this.label2 = new cjs.Text("1 kr", "16px 'Myriad Pro'");
        this.label2.lineHeight = 34;
        this.label2.setTransform(55, 119);

        this.label3 = new cjs.Text("2 kr", "16px 'Myriad Pro'");
        this.label3.lineHeight = 34;
        this.label3.setTransform(182, 119);

        this.label4 = new cjs.Text("5 kr", "16px 'Myriad Pro'");
        this.label4.lineHeight = 34;
        this.label4.setTransform(309, 119);

        this.label5 = new cjs.Text("10 kr", "16px 'Myriad Pro'");
        this.label5.lineHeight = 34;
        this.label5.setTransform(436, 119);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFF579").s('#7d7d7d').drawRoundRect(5, 0, ((1040) - 15) * 0.50, ((270) - 20) * 0.50, 10);
        this.roundRect1.setTransform(0, 27);


        this.addChild(this.roundRect1, this.instance, this.label1, this.label2, this.label3, this.label4, this.label5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 75);


    (lib.Symbol3 = function() {
        this.initialize();
        this.instance = new lib.p110_2();
        this.instance.setTransform(2, 20, 0.38, 0.38);

        this.text = new cjs.Text("Hur många kronor är det?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(21, -1.5);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#8390C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(2, -1.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((702 * 0.5) - 10) * 0.49, ((789 * (1 / 3)) - 55) * 0.49, 10);
        this.roundRect1.setTransform(4, 21);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(176, 21);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(349, 21);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(4, 127);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(176, 127);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(349, 127);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(4, 233);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(176, 233);

        this.roundRect9 = this.roundRect1.clone(true);
        this.roundRect9.setTransform(349, 233);

        this.roundRect10 = this.roundRect1.clone(true);
        this.roundRect10.setTransform(4, 339);

        this.roundRect11 = this.roundRect1.clone(true);
        this.roundRect11.setTransform(176, 339);

        this.roundRect12 = this.roundRect1.clone(true);
        this.roundRect12.setTransform(349, 339);

        this.line_1 = new cjs.Shape();
        this.line_1.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_1.setTransform(25, 75);

        this.line_2 = new cjs.Shape();
        this.line_2.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_2.setTransform(197, 75);

        this.line_3 = new cjs.Shape();
        this.line_3.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_3.setTransform(368, 75);

        this.line_4 = new cjs.Shape();
        this.line_4.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_4.setTransform(25, 180);

        this.line_5 = new cjs.Shape();
        this.line_5.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_5.setTransform(197, 180);

        this.line_6 = new cjs.Shape();
        this.line_6.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_6.setTransform(368, 180);

        this.line_7 = new cjs.Shape();
        this.line_7.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_7.setTransform(25, 288);

        this.line_8 = new cjs.Shape();
        this.line_8.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_8.setTransform(197, 288);

        this.line_9 = new cjs.Shape();
        this.line_9.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_9.setTransform(368, 288);

        this.line_7 = new cjs.Shape();
        this.line_7.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_7.setTransform(25, 288);

        this.line_8 = new cjs.Shape();
        this.line_8.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_8.setTransform(197, 288);

        this.line_9 = new cjs.Shape();
        this.line_9.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_9.setTransform(368, 288);

        this.line_10 = new cjs.Shape();
        this.line_10.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_10.setTransform(25, 392);

        this.line_11 = new cjs.Shape();
        this.line_11.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_11.setTransform(197, 392);

        this.line_12 = new cjs.Shape();
        this.line_12.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_12.setTransform(368, 392);

        this.text_2 = new cjs.Text("kr", "bold 22px 'UusiTekstausMajema'", "#000");
        this.text_2.lineHeight = 22;
        this.text_2.setTransform(103, 95);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5,
            this.roundRect6, this.roundRect7, this.roundRect8, this.roundRect9, this.roundRect10,
            this.roundRect11, this.roundRect12,
            this.text, this.text_1, this.line_1, this.line_2, this.line_3, this.line_4, this.line_5, this.line_6, this.line_7, this.line_8, this.line_9,
            this.line_10, this.line_11, this.line_12, this.text_2, this.instance);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 450);



    (lib.p110 = function() {
        this.initialize();



        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295.3, 232, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(293, 82, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.addChild(this.other, this.v2, this.v1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
