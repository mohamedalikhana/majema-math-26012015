(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p143_1.png",
            id: "p143_1"
        }, {
            src: "images/p143_2.png",
            id: "p143_2"
        }, {
            src: "images/p143_3.png",
            id: "p143_3"
        }]
    };

    (lib.p143_1 = function() {
        this.initialize(img.p143_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p143_2 = function() {
        this.initialize(img.p143_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p143_3 = function() {
        this.initialize(img.p143_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("143", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(553, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#8490C8");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(118, 26);

        this.text_4 = new cjs.Text("50", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(67, 22);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#8490C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52, 23.5, 1.2, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 142, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 31, 48, 81, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p143_1();
        this.instance.setTransform(503, 36, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);


        var ToBeAdded = [];
        var arrxPos = [];

        arrxPos = ['30', '213', '330'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "";

            for (var j = 0; j < 4; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 0) {
                        text = "6  +  4  +  1  =";
                    } else if (j == 1) {
                        text = "7  +  2  +  1  =";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "8  +  2  +  1  =";
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "3  +  7  +  2  =";
                    }
                } else if (i == 1) {
                    if (j == 0) {
                        text = "8  –            =  3";
                    } else if (j == 1) {
                        text = "9  –            =  1";
                    } else if (j == 2) {
                        text = "10  –            =  4";
                        rowSpace = 2.05;
                        xPos = 205;
                    } else if (j == 3) {
                        text = "10  –            =  2";
                        rowSpace = 3.045;
                    }
                } else if (i == 2) {
                    if (j == 0) {
                        text = "11  –  1  –  6  =";
                        xPos = 348;
                    } else if (j == 1) {
                        text = "12  –  2  –  8  =";
                        xPos = 348;
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "3  +  7  –  1  =";
                        xPos = 354.4;
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "4  +  6  –  7  =";
                        xPos = 352;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 33 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        for (var j = 0; j < 4; j++) {
            var rowSpace = j;

            var temp_Line = new cjs.Shape();
            temp_Line.graphics.s("#949599").ss(1).moveTo(119, 52 + (rowSpace * 27)).lineTo(149, 52 + (rowSpace * 27));
            temp_Line.setTransform(0, 0);

            var temp_Line1 = new cjs.Shape();
            temp_Line1.graphics.s("#949599").ss(1).moveTo(241, 52 + (rowSpace * 27)).lineTo(271, 52 + (rowSpace * 27));
            temp_Line1.setTransform(0, 0);

            var temp_Line_2 = new cjs.Shape();
            temp_Line_2.graphics.s("#949599").ss(1).moveTo(443, 52 + (rowSpace * 27)).lineTo(473, 52 + (rowSpace * 27));
            temp_Line_2.setTransform(0, 0);

            ToBeAdded.push(temp_Line, temp_Line1, temp_Line_2);
        }

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.shape_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 100, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 9, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p143_1();
        this.instance.setTransform(503, 14, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Skriv talen som saknas.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        this.text_1 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(28, 25.2);

        this.text_2 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(53, 25.2);

        this.text_3 = new cjs.Text("9", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(236, 58.2);

        this.text_4 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(257, 58.2);

        this.text_5 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(268, 58.2);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['35'];
        var xPos = 19.5,
            padding,
            yPos,
            rectWidth = 338.4,
            rectHeight = 22,
            boxWidth = 26,
            boxHeight = 22,
            numberOfBoxes = 13,
            numberOfRects = 1;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 7;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        var arryPos = ['68'];
        var xPos = 19.5,
            padding,
            yPos,
            rectWidth = 338.4,
            rectHeight = 22,
            boxWidth = 26,
            boxHeight = 22,
            numberOfBoxes = 13,
            numberOfRects = 1;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 7;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group2.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group2.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group2.setTransform(0, 0);


        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2);
        this.addChild(this.textbox_group1, this.textbox_group2, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 106, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 12, 48, 82, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p143_1();
        this.instance.setTransform(503, 16, 0.4, 0.4);

        this.instance_2 = new lib.p143_2();
        this.instance_2.setTransform(4, 15, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Vad kostar sakerna tillsammans?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        this.text_1 = new cjs.Text("10 kr", "12px 'Myriad Pro'");
        this.text_1.lineHeight = 15;
        this.text_1.setTransform(45.3, 24.8);
        this.text_1.skewX = 25;
        this.text_1.skewY = 25;

        this.text_2 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_2.lineHeight = 15;
        this.text_2.setTransform(157, 38);
        this.text_2.skewX = -30;
        this.text_2.skewY = -30;

        this.text_3 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_3.lineHeight = 15;
        this.text_3.setTransform(280, 43);
        this.text_3.skewX = 10;
        this.text_3.skewY = 10;

        this.text_4 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_4.lineHeight = 15;
        this.text_4.setTransform(426, 30);
        this.text_4.skewX = -20;
        this.text_4.skewY = -20;

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(246, 15).lineTo(246, 83);
        this.Line_1.setTransform(0, 2);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['73'];
        var xPos = 19.5,
            padding,
            yPos,
            rectWidth = 200,
            rectHeight = 23.5,
            boxWidth = 20,
            boxHeight = 23.5,
            numberOfBoxes = 10,
            numberOfRects = 1;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 7;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        var arryPos = ['73'];
        var xPos = 259,
            padding,
            yPos,
            rectWidth = 200,
            rectHeight = 23.5,
            boxWidth = 20,
            boxHeight = 23.5,
            numberOfBoxes = 10,
            numberOfRects = 1;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 7;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group2.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group2.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2);
        this.addChild(this.textbox_group1, this.textbox_group2, this.Line_1, this.instance_2);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);


    (lib.Symbol4 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 106, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 12, 48, 82, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p143_1();
        this.instance.setTransform(503, 16, 0.4, 0.4);

        this.instance_2 = new lib.p143_3();
        this.instance_2.setTransform(4, 6, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Hur många kronor är det kvar?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        this.text_1 = new cjs.Text("8 kr", "13px 'Myriad Pro'");
        this.text_1.lineHeight = 15;
        this.text_1.setTransform(207, 30);
        this.text_1.skewX = -15;
        this.text_1.skewY = -15;

        this.text_2 = new cjs.Text("10 kr", "12px 'Myriad Pro'");
        this.text_2.lineHeight = 15;
        this.text_2.setTransform(396, 11);
        this.text_2.skewX = 15;
        this.text_2.skewY = 15;

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(246, 21).lineTo(246, 89);
        this.Line_1.setTransform(0, 2);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['73'];
        var xPos = 19.5,
            padding,
            yPos,
            rectWidth = 200,
            rectHeight = 23.5,
            boxWidth = 20,
            boxHeight = 23.5,
            numberOfBoxes = 10,
            numberOfRects = 1;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 7;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        var arryPos = ['73'];
        var xPos = 259,
            padding,
            yPos,
            rectWidth = 200,
            rectHeight = 23.5,
            boxWidth = 20,
            boxHeight = 23.5,
            numberOfBoxes = 10,
            numberOfRects = 1;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 7;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group2.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group2.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2);
        this.addChild(this.textbox_group1, this.textbox_group2, this.Line_1, this.instance_2);
        this.addChild(this.text_1, this.text_2);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.SymbolClock = function() {
        this.initialize();

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#C8C9CB").s("#000000").ss(0.6, 0, 0, 4).arc(0, 0, 42, 0, 2 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#ffffff").s("#000000").ss(0.4, 0, 0, 4).arc(0, 0, 37.7, 0, 2 * Math.PI);
        this.shape_group2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(0, -38).lineTo(0, -35).moveTo(0, 35).lineTo(0, 38).moveTo(35, 0).lineTo(38, 0).moveTo(-35, 0).lineTo(-38, 0).moveTo(18, -33).lineTo(16.5, -30).moveTo(33, -19).lineTo(30, -17).moveTo(-30, 17.5).lineTo(-32.5, 19).moveTo(-17, 30).lineTo(-19, 32.5).moveTo(-31, -16).lineTo(-33.5, -18).moveTo(-19, -33).lineTo(-17, -30).moveTo(33, 17.5).lineTo(30.5, 16).moveTo(20, 31.5).lineTo(18.2, 29);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(10, -31);
        this.text_2 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(21.8, -20);
        this.text_3 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(27, -6);
        this.text_4 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(21.8, 10);
        this.text_5 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(10, 20);
        this.text_6 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(-4.5, 25);
        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(-19, 20);
        this.text_8 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(-31, 10);
        this.text_9 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(-36, -6);
        this.text_10 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(-32, -20);
        this.text_11 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(-20, -31);
        this.text_12 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(-7, -35.5);
        //center dot
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.lineHeight = 63;
        this.text_dot.setTransform(-6.5, -32);

        this.addChild(this.shape_group1, this.shape_group2, this.Line_1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_dot)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);



    (lib.Symbol5 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 115, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 25, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p143_1();
        this.instance.setTransform(503, 30, 0.4, 0.4);

        this.instance_clock1 = new lib.SymbolClock();
        this.instance_clock1.setTransform(61, 69, 0.83, 0.83);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_1.setTransform(70.3, 74.3, 0.9, 0.9, 180);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(61, 55.6, 1.2, 0.85);
        this.text_dot1 = new cjs.Text(".", "30px 'Myriad Pro'");
        this.text_dot1.lineHeight = 44;
        this.text_dot1.setTransform(55.5, 45);

        this.instance_clock2 = new lib.SymbolClock();
        this.instance_clock2.setTransform(305, 69, 0.83, 0.83);


        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_3.setTransform(300.3, 78, 0.8, 0.9, 265);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(305, 58.1, 1.2, 0.85);
        this.text_dot2 = new cjs.Text(".", "30px 'Myriad Pro'");
        this.text_dot2.lineHeight = 44;
        this.text_dot2.setTransform(299.5, 45);

        this.text_q1 = new cjs.Text("Vad är klockan?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 10);

        this.text_q2 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(4, 10);

        this.text_1 = new cjs.Text("Klockan är", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 23;
        this.text_1.setTransform(124, 55);
        this.text_2 = new cjs.Text("Klockan är", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 23;
        this.text_2.setTransform(370, 55);

        this.text_3 = new cjs.Text(".", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 30;
        this.text_3.setTransform(214, 76);
        this.text_4 = new cjs.Text(".", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 30;
        this.text_4.setTransform(460, 76);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(246, 32).lineTo(246, 103);
        this.Line_1.setTransform(0, 2);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.s("#949599").ss(1).moveTo(127, 70).lineTo(215, 70);
        this.Line_2.setTransform(0, 20);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.s("#949599").ss(1).moveTo(368, 70).lineTo(456, 70);
        this.Line_3.setTransform(5, 20);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.instance_2);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.Line_1, this.Line_2, this.Line_3, this.instance_clock1, this.instance_clock2);

        this.addChild(this.shape_1, this.shape_2, this.text_dot1, this.shape_3, this.shape_4, this.text_dot2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 551.3, 100);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(314, 109, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(314, 255, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(314, 360, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v4 = new lib.Symbol4();
        this.v4.setTransform(314, 471, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.v5 = new lib.Symbol5();
        this.v5.setTransform(314, 581, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.v5, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
