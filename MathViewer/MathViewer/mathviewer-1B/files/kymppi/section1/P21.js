(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p21_1.png",
            id: "p21_1"

        }]
    };

    (lib.p21_1 = function() {
        this.initialize(img.p21_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1

        this.text = new cjs.Text("21", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 29, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 10);

        this.instance = new lib.p21_1();
        this.instance.setTransform(35, 39, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.textbox_group1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Ringa in jämna tal på tallinjen.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 0);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(10, 28).lineTo(505, 28);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(505, 27.5, 1.2, 1.2, 265);


        var ToBeAdded = [];

        for (var i = 0; i < 21; i++) {
            var columnSpace = i;

            this.temp_Line = new cjs.Shape();
            this.temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(17 + (columnSpace * 23.5), 20).lineTo(17 + (columnSpace * 23.5), 28);
            this.temp_Line.setTransform(0, 0);

            var font;
            if (i == 0 || i == 5 || i == 10 || i == 15 || i == 20) {
                font = "bold 16px 'Myriad Pro'";
            } else {
                font = "16px 'Myriad Pro'";
            }
            var temp_label = new cjs.Text("" + i, font);
            if (i == 5 || i == 6 || i == 7 || i == 8 || i == 9) {
                columnSpace = columnSpace - 0.1;
            } else if (i == 10 || i == 11 || i == 12 || i == 13 || i == 14) {
                columnSpace = columnSpace - 0.4;
            } else if (i == 15 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20) {
                columnSpace = columnSpace - 0.5;
            }
            temp_label.setTransform(12.5 + (columnSpace * 24), 49);

            ToBeAdded.push(this.temp_Line, temp_label);

        }

        this.addChild(this.text, this.text_1,
            this.textbox_group2, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.shape_1, this.instance)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Ringa in udda tal på tallinjen.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 0);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(10, 28).lineTo(505, 28);
        this.hrLine_4.setTransform(0, 0);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_1.setTransform(505, 27.5, 1.2, 1.2, 265);


        var ToBeAdded = [];

        for (var i = 0; i < 21; i++) {
            var columnSpace = i;

            this.temp_Line = new cjs.Shape();
            this.temp_Line.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(17 + (columnSpace * 23.5), 20).lineTo(17 + (columnSpace * 23.5), 28);
            this.temp_Line.setTransform(0, 0);

            var font;
            if (i == 0 || i == 5 || i == 10 || i == 15 || i == 20) {
                font = "bold 16px 'Myriad Pro'";
            } else {
                font = "16px 'Myriad Pro'";
            }
            var temp_label = new cjs.Text("" + i, font);
            if (i == 5 || i == 6 || i == 7 || i == 8 || i == 9) {
                columnSpace = columnSpace - 0.1;
            } else if (i == 10 || i == 11 || i == 12 || i == 13 || i == 14) {
                columnSpace = columnSpace - 0.4;
            } else if (i == 15 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20) {
                columnSpace = columnSpace - 0.5;
            }
            temp_label.setTransform(12.5 + (columnSpace * 24), 49);

            ToBeAdded.push(this.temp_Line, temp_label);

        }

        this.addChild(this.text, this.text_1,
            this.textbox_group2, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.shape_1, this.instance)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.setTransform(19, -2);
        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, -2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 12, 510, 150, 10);
        this.roundRect1.setTransform(0, 0);


        var lineArr = [];
        var lineX = [111, 268, 427, 111, 268, 427, 111, 268, 427, 111, 268, 427];
        var lineY = [34, 34, 34, 59, 59, 59, 84, 84, 84, 112, 112, 112];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var TxtArr = ['2', '3', '4', '5', '8', '7', '5', '5', '2', '4', '1', '2',

            '9', '8', '7', '6', '1', '2', '3', '4', '1', '3', '5', '6',

            '20', '20', '20', '20', '5', '6', '7', '4', '5', '2', '3', '5',
        ];

        var TArr = [];
        var TxtlineX = [35, 35, 35, 35, 65, 65, 65, 65, 95, 95, 95, 95,192,192,192,192,222,222,222,222,252,252,252,252,349,349,349,349,380,380,380,380,410,410,410,410];
        var TxtlineY = [69, 95, 121, 147, 69, 95, 121, 147, 69, 95, 121, 147,69, 95, 121, 147,69, 95, 121, 147,69, 95, 121, 147,69, 95, 121, 147,69, 95, 121, 147,69, 95, 121, 147];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }


        var symblArr = ['+','+','+','+','+','+','+','+','=','=','=','=','+','+','+','+','–','–','–','–','=','=','=','=','–','–','–','–','–','–','–','–','=','=','=','='];

        var SymArr = [];
        var symbllineX = [50, 50, 50, 50, 80, 80, 80, 80,109,109,109,109,207,207,207,207,237,237,237,237,266,266,266,266,367,367,367,367, 397,397,397,397, 424,424,424,422];
        var symbllineY = [70, 95, 121, 147, 70, 95, 121, 147, 70,95, 121, 147, 70,95, 121, 147, 70,95, 121, 147, 70,95, 121, 147, 70,95, 121, 147, 70,95, 121, 147, 70,95, 121, 147];

        for (var i = 0; i < symbllineX.length; i++) {
            var symbltxt = symblArr[i];
            this.temp_label = new cjs.Text(symbltxt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(symbllineX[i], symbllineY[i]);
            SymArr.push(this.temp_label);
        }

       
        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(5, 20).lineTo(465, 20);
        this.Line_1.setTransform(23, 13);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(123.5, 36);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_3.setTransform(231, 36);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(343, 36);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_5.setTransform(455, 36);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(45 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 10) {
                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(55 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else if (column < 15) {
                    this.shape_group1.graphics.f("#D51217").s("#878787").ss(0.8, 0, 0, 4).arc(65 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                } else {

                    this.shape_group1.graphics.f("#008BD3").s("#878787").ss(0.8, 0, 0, 4).arc(75 + (columnSpace * 20.3), 33 + (row * 19), 7.9, 0, 2 * Math.PI);
                }

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.Line_1, this.shape_group1,this.text_2, this.text_3, this.text_4, this.text_5);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

        for (var i = 0; i < SymArr.length; i++) {
            this.addChild(SymArr[i]);
        }
      
       
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 320, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 427, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(309, 520, 1, 1, 0, 0, 0, 255.8, 53.5);


        this.addChild(this.other, this.v1, this.v2, this.v3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
