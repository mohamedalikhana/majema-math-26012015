(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p92_1.png",
            id: "p92_1"
        }, {
            src: "images/p92_2.png",
            id: "p92_2"
        }]
    };

    (lib.p92_1 = function() {
        this.initialize(img.p92_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p92_2 = function() {
        this.initialize(img.p92_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("92", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(37, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 0);

        this.text_1 = new cjs.Text(" Mät med rutor och skriv längden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 287, 10);
        this.roundRect1.setTransform(10, 10);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f('').s("#008BD2").ss(0.7).drawRoundRect(0, 0, 399.5, 28, 0);
        this.Rect1.setTransform(64, 30);

        var vrlineArr = [];
        var vrlineX = [72.7, 101.4, 130.1, 158.8, 187.5, 216.2, 244.9, 273.6, 302.3, 331, 359.3, 388.4, 417.1];
        var vrlineY = [30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#008BD2").setStrokeStyle(1).moveTo(20, 0).lineTo(20, 28);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            vrlineArr.push(vrLine_1);
        }

        var TxtBoxArr = [];
        var TxtBox_X = [169, 369, 255, 369, 155, 327, 155, 385];
        var TxtBox_Y = [82, 82, 120, 162, 211, 211, 258.5, 258.5];
        for (var row = 0; row < TxtBox_X.length; row++) {
            var TxtBox_1 = new cjs.Shape();
            TxtBox_1.graphics.f('#FFFFFF').s("#706F6F").ss(0.7).drawRoundRect(0, 0, 19, 22, 0);
            TxtBox_1.setTransform(TxtBox_X[row], TxtBox_Y[row]);
            TxtBoxArr.push(TxtBox_1);
        }

        var dotlineX = [44, 72.7, 101.4, 130.1, 158.8, 187.5, 216.2, 244.9, 273.6, 302.3, 331, 359.3, 388.4, 417.1, 443.4];
        this.dotline = new cjs.Shape();
        for (var col = 0; col < dotlineX.length; col++) {
            var xPos = dotlineX[col];
            for (var i = 0; i < 29; i++) {
                this.dotline.graphics.beginStroke("#008BD2").setStrokeStyle(0.5).moveTo(xPos, 0 + (i * 8)).lineTo(xPos, 5 + (i * 8));

            }
        }
        this.dotline.setTransform(20, 55);

        this.instance = new lib.p92_1();
        this.instance.setTransform(45, 48, 0.47, 0.47);

        this.addChild(this.text, this.text_1, this.roundRect1, this.instance, this.Rect1, this.dotline);

        for (var i = 0; i < TxtBoxArr.length; i++) {
            this.addChild(TxtBoxArr[i]);
        }

        for (var i = 0; i < vrlineArr.length; i++) {
            this.addChild(vrlineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 290);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Ordna sträckorna efter längd, från kortast till längst.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(10, 0);

        this.text_2 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#706F6F");
        this.text_2.setTransform(463, 119.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 510, 230, 10);
        this.roundRect1.setTransform(10, 10);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_1.setTransform(10, 86);

        var RectangleArr = [];
        var Rectangle_X = [40, 138, 237, 335, 433, 40, 138, 237, 335, 433];
        var Rectangle_Y = [22, 22, 22, 22, 22, 135, 135, 135, 135, 135];

        for (var row = 0; row < Rectangle_X.length; row++) {
            var Rectangle_1 = new cjs.Shape();
            Rectangle_1.graphics.f('').s("#706F6F").ss(0.7).drawRoundRect(0, 0, 70, 69, 10);
            Rectangle_1.setTransform(Rectangle_X[row], Rectangle_Y[row]);
            RectangleArr.push(Rectangle_1);
        }

        var TxtBoxArr = [];
        var TxtBox_X = [66, 165, 264, 363, 462, 66, 165, 264, 363, 462];
        var TxtBox_Y = [97, 97, 97, 97, 97, 211, 211, 211, 211, 211];
        for (var row = 0; row < TxtBox_X.length; row++) {
            var TxtBox_1 = new cjs.Shape();
            TxtBox_1.graphics.f('').s("#706F6F").ss(0.7).drawRoundRect(0, 0, 19, 22.5, 0);
            TxtBox_1.setTransform(TxtBox_X[row], TxtBox_Y[row]);
            TxtBoxArr.push(TxtBox_1);
        }

        this.instance = new lib.p92_2();
        this.instance.setTransform(20, 10, 0.47, 0.47);

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.hrLine_1, this.instance);

        for (var i = 0; i < RectangleArr.length; i++) {
            this.addChild(RectangleArr[i]);
        }
        for (var i = 0; i < TxtBoxArr.length; i++) {
            this.addChild(TxtBoxArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535.3, 240);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287, 109, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(287, 450, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
