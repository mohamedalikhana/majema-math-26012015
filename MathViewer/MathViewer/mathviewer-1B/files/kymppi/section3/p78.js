(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p78_1.png",
            id: "p78_1"
        }, {
            src: "images/p78_2.png",
            id: "p78_2"
        }]
    };

    (lib.p78_1 = function() {
        this.initialize(img.p78_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p78_2 = function() {
        this.initialize(img.p78_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("78", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 661);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();
        this.text = new cjs.Text(" Måla cirkeln om svaret stämmer.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(5, 0);


        this.instance = new lib.p78_1();
        this.instance.setTransform(25, 25, 0.46, 0.46);

        var arryVal = ['13  –  4', '15  –  6', '16  –  8', '14  –  5', '12  –  3', '17  –  9', '14  –  6', '15  –  8', '12  –  4', '17  –  8', '13  –  5', '16  –  9', '14  –  7', '16  –  9', '15  –  7', '13  –  6', '11  –  4', '12  –  6'];
        var ToBeAdded = [];
        var i = 0;
        var tmparc = new cjs.Shape();
        for (var col = 0; col < 3; col++) {
            var columnSpace = col;
            for (var row = 0; row < 6; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(arryVal[i], "16px 'Myriad Pro'");
                tempText.setTransform(46 + (172 * columnSpace), 120 + (22 * rowSpace));
                tmparc.graphics.f("#FFFFFF").s("#878787").ss(0.8, 0, 0, 4).arc(108 + (columnSpace * 172), 115 + (row * 22), 8, 0, 2 * Math.PI)
                ToBeAdded.push(tempText, tmparc)
                i++;
            };
        };

        var topNum=[9, 8, 7]
        for (var col = 0; col < topNum.length; col++) {
            var columnSpace = col;
            var tempText1 = new cjs.Text(topNum[col], "16px 'MyriadPro-Semibold'");
            tempText1.setTransform(75+(columnSpace*173) , 50);
            ToBeAdded.push(tempText1)
        }

        this.addChild(this.instance, this.text, this.text_1)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 572.3, 250);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(5, 0);

        this.text_1 = new cjs.Text("Manuels och Miras kort är lika mycket värda.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 0);

        this.text_2 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_2.setTransform(24, 20);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7D7D7D').drawRoundRect(0, 0, 252, 100, 10);
        this.roundRect1.setTransform(5, 32);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(263, 32);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(5, 138);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(263, 138);

        this.instance = new lib.p78_2();
        this.instance.setTransform(50, 45, 0.46, 0.46);

        var ToBeAdded = [];
        var xpos = 28;
        var ypos = 95;
        fColor = '#DFDDF0';
        sColor = '#958FC5';
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 202
            } else {
                ypos = 95
            }

            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 3; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 146
                        } else if (numOfcol == 2) {
                            xpos = 287
                        } else if (numOfcol == 3) {
                            xpos = 405
                        } else {
                            xpos = 28
                        }
                        if (numOfrow == 0 && numOfcol < 1) {
                            fColor = '#DFDDF0';
                            sColor = '#958FC5';
                        }
                        if (numOfrow == 1 && numOfcol < 1) {
                            fColor = '#C4E5F0';
                            sColor = '#00A3C4';
                        }
                        if (numOfrow == 0 && numOfcol > 1) {
                            fColor = '#E1EBC5';
                            sColor = '#90BD22';
                        }
                        if (numOfrow == 1 && numOfcol > 1) {
                            fColor = '#FBD3B9';
                            sColor = '#EB5B1B';
                        }
                        if (column == 2 && numOfcol == 1) {
                            fColor = '#FFFFFF';
                            sColor = '#706F6F';
                        }
                        if (column == 2 && numOfcol == 3) {
                            fColor = '#FFFFFF';
                            sColor = '#706F6F';
                        }
                        if (column == 1 && numOfrow == 1) {
                            ypos = ypos - 4
                        } else if (column == 0 && numOfrow == 1) {
                            ypos = 202
                        } else if (column == 2 && numOfrow == 1) {
                            ypos = 202
                        }
                        if (column == 1 && numOfrow == 0) {
                            ypos = ypos - 4
                        } else if (column == 0 && numOfrow == 0) {
                            ypos = 95
                        } else if (column == 2 && numOfrow == 0) {
                            ypos = 95
                        }
                        tmp_Rect.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xpos + (columnSpace * 33), ypos + (row * 20), 23, 24);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }

        var xpos = 35;
        var ypos = 112;
        var tmpNum = [6, 6, 5, 5, 8, 7, 6, 2, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 8];
        var i = 0;
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 219
            } else {
                ypos = 112
            }

            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 3; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 153
                        } else if (numOfcol == 2) {
                            xpos = 293
                        } else if (numOfcol == 3) {
                            xpos = 413
                        } else {
                            xpos = 35
                        }
                        if (column == 2 && numOfcol == 1) {
                            continue;
                        }
                        if (column == 2 && numOfcol == 3) {
                            continue;
                        }

                        if (column == 1 && numOfrow == 1) {
                            ypos = ypos - 3
                        } else if (column == 0 && numOfrow == 1) {
                            ypos = 219
                        } else if (column == 2 && numOfrow == 1) {
                            ypos = 219
                        }
                        if (column == 1 && numOfrow == 0) {
                            ypos = ypos - 3
                        } else if (column == 0 && numOfrow == 0) {
                            ypos = 112
                        } else if (column == 2 && numOfrow == 0) {
                            ypos = 112
                        }
                        var tempText = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                        tempText.setTransform(xpos + (columnSpace * 33), ypos + (row * 20));
                        ToBeAdded.push(tempText);
                        i++;
                    }
                }
            }
        }


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        this.addChild(this.text_1, this.textbox_group1, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.text_2);
        this.addChild(this.text, this.instance, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 150);



    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296, 285, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296, 397, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
