(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p141_1.png",
            id: "p141_1"
        }, {
            src: "images/p141_2.png",
            id: "p141_2"
        }, {
            src: "images/p141_3.png",
            id: "p141_3"

        }]
    };

    (lib.p141_1 = function() {
        this.initialize(img.p141_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.p141_2 = function() {
        this.initialize(img.p141_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p141_3 = function() {
        this.initialize(img.p141_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.text_1 = new cjs.Text("141", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(551, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_2 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#00A6D9");
        this.text_2.setTransform(110, 42);

        this.text_3 = new cjs.Text("49", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(63, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(49, 23.5, 1.15, 1);

        this.addChild(this.shape_2, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.drawcat = function() {
        this.initialize();

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s("#7d7d7d").ss(1).drawRoundRect(0, 0, 50, 86, 15);
        this.roundRect2.setTransform(-18, 0);

        this.instance = new lib.p141_3();
        this.instance.setTransform(0, 0, 0.47, 0.47);

        this.addChild(this.roundRect2, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A6D9");
        this.text.setTransform(5, 20);

        this.text_1 = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 20);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s("#7d7d7d").ss(1).drawRoundRect(0, 0, 485, 139, 15);
        this.roundRect1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [79, 245, 408, 79, 245, 408, 79, 245, 408, 79, 245, 408];
        var lineY = [9, 9, 9, 36, 36, 36, 63, 63, 63, 89, 89, 89];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(42, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("50  +  7  =", "16px 'Myriad Pro'");
        this.label1.setTransform(22, 44);
        this.label2 = new cjs.Text("46  +  1  =", "16px 'Myriad Pro'");
        this.label2.setTransform(22, 69);
        this.label3 = new cjs.Text("75  +  2  =", "16px 'Myriad Pro'");
        this.label3.setTransform(22, 96);
        this.label4 = new cjs.Text("84  +  3  =", "16px 'Myriad Pro'");
        this.label4.setTransform(22, 123);

        this.label6 = new cjs.Text("33  –  1  =", "16px 'Myriad Pro'");
        this.label6.setTransform(187, 44);
        this.label7 = new cjs.Text("54  –  2  =", "16px 'Myriad Pro'");
        this.label7.setTransform(187, 69);
        this.label8 = new cjs.Text("66  –  3  =", "16px 'Myriad Pro'");
        this.label8.setTransform(187, 96);
        this.label9 = new cjs.Text("95  –  5  =", "16px 'Myriad Pro'");
        this.label9.setTransform(187, 123);

        this.label11 = new cjs.Text("20  +  60  =", "16px 'Myriad Pro'");
        this.label11.setTransform(341, 44);
        this.label12 = new cjs.Text("30  +  70  =", "16px 'Myriad Pro'");
        this.label12.setTransform(341, 69);
        this.label13 = new cjs.Text("80  –  50  =", "16px 'Myriad Pro'");
        this.label13.setTransform(341, 96);
        this.label14 = new cjs.Text("90  –  80  =", "16px 'Myriad Pro'");
        this.label14.setTransform(341, 123);

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 29);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 120);

    //Part 1
    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A6D9");
        this.text.setTransform(5, 19);

        this.text_1 = new cjs.Text(" Skriv talen i storleksordning.", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 19);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s('#7d7d7d').drawRoundRect(0, 0, 485, 99, 10);
        this.roundRect1.setTransform(0, 0);

        var tBoxArr = [];
        var tBoxX = [32, 69, 106, 190, 227, 264, 355, 392, 432];
        var tBoxY = [33, 33, 33, 33, 33, 33, 33, 33, 33];

        for (var i = 0; i < tBoxX.length; i++) {
            var sqrtbox = new cjs.Shape();
            if (i < 3) {
                sqrtbox.graphics.f("#F3CAD3").s('#D6326A').drawRect(0, 0, 24, 24, 0);
            } else if (i == 3 || i == 5) {
                sqrtbox.graphics.f("#C4E5F0").s('#00A3C4').drawRect(0, 0, 24, 24, 0);
            } else if (i == 4) {
                sqrtbox.graphics.f("#C4E5F0").s('#00A3C4').drawRect(0, 0, 27, 24, 0);
            } else {
                sqrtbox.graphics.f("#E1EBC5").s('#90BD22').drawRect(0, 0, 24, 24, 0);
            }

            sqrtbox.setTransform(tBoxX[i], tBoxY[i]);
            tBoxArr.push(sqrtbox);
        }

        var TArr = [];
        var TxtArr = ['50', '5', '55', '90', '100', '99', '18', '81', '8'];

        var TxtlineX = [36, 77, 110, 194, 227, 268, 356, 397, 438];
        var TxtlineY = [50, 50, 50, 50, 50, 50, 50, 50, 50];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "15px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var lineArr = [];
        var lineX = [13, 63, 117, 174, 225, 277, 332, 385, 435];
        var lineY = [89, 89, 89, 89, 89, 89, 89, 89, 89];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(34, 0);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var arrowArr = [];
        var arrowX = [51, 102, 213, 263, 371, 422];
        var arrowY = [75, 75, 75, 75, 75, 75];

        for (var row = 0; row < arrowX.length; row++) {
            var arrow_1 = new cjs.Shape();
            arrow_1.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(6.5, 0).lineTo(0, 3.5).lineTo(6.5, 7);
            arrow_1.setTransform(arrowX[row], arrowY[row]);
            arrowArr.push(arrow_1);
        }    

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 60);
        this.vrLine_1.setTransform(163, 32);

        this.vrLine_2 = new cjs.Shape();
        this.vrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 60);
        this.vrLine_2.setTransform(322, 32);

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 9);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.instance, this.Line_1, this.vrLine_1,
            this.vrLine_2);

        for (var i = 0; i < tBoxArr.length; i++) {
            this.addChild(tBoxArr[i]);
        }
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < arrowArr.length; i++) {
            this.addChild(arrowArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 90);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A6D9");
        this.text.setTransform(5, 19);

        this.text_1 = new cjs.Text(" Fortsätt talföljden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 19);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s('#7d7d7d').drawRoundRect(0, 0, 485, 105, 10);
        this.roundRect1.setTransform(0, 0);

        this.rectgroup = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#878787').drawRoundRect(0, 0, 440, 25, 0);
        this.Rect1.setTransform(0, 0);
        this.rectgroup.addChild(this.Rect1);

        var startX = 0,
            startY = 0;
        var colspace = 440 / 11,
            rowspace = 0;
        var cols = 11,
            rows = 1;

        for (var col = 0; col < cols; col++) {
            for (var row = 0; row < rows; row++) {
                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 25);
                vrLine_1.setTransform(startX + (colspace * col), startY + (rowspace * row));

                this.rectgroup.addChild(vrLine_1);
            }
        }
        this.rectgroup.setTransform(27, 35);

        this.rectgroup2 = this.rectgroup.clone(true);
        this.rectgroup2.setTransform(27, 70);


        var TArr = [];
        var TxtArr = ['50', '55', '60', '90'];

        var TxtlineX = [30, 70, 110, 70];
        var TxtlineY = [59, 59, 59, 92];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "36px 'UusiTekstausMajema'", "706F6F");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        this.text_2 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "706F6F");
        this.text_2.setTransform(22, 92);

        this.text_3 = new cjs.Text("00", "36px 'UusiTekstausMajema'", "706F6F");
        this.text_3.setTransform(33, 92);

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 9);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.text_2, this.text_3, this.rectgroup, this.rectgroup2);

        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 100);

    (lib.Symbol5 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#00A6D9");
        this.text.setTransform(5, 19);

        this.text_1 = new cjs.Text(" Hur många kronor är det?", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 19);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s('#7d7d7d').drawRoundRect(0, 0, 485, 80, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p141_1();
        this.instance.setTransform(23, 4, 0.47, 0.47);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(60, 0);
        this.hrLine_1.setTransform(170, 65);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(60, 0);
        this.hrLine_2.setTransform(410, 65);

        this.text_2 = new cjs.Text("kr", "20px 'UusiTekstausMajema'");
        this.text_2.setTransform(214, 62);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 40);
        this.vrLine_1.setTransform(240, 40);

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, -3);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.instance, this.hrLine_1, this.hrLine_2,
            this.text_2, this.vrLine_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 75);

    (lib.Symbol6 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#00A6D9");
        this.text.setTransform(5, 19);

        this.text_1 = new cjs.Text(" Hur många kronor är kvar?", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 19);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s('#7d7d7d').drawRoundRect(0, 0, 485, 109, 10);
        this.roundRect1.setTransform(0, 0);

        this.instance = new lib.p141_2();
        this.instance.setTransform(7, 4, 0.47, 0.47);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(60, 0);
        this.hrLine_1.setTransform(170, 94);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(60, 0);
        this.hrLine_2.setTransform(410, 94);

        this.text_2 = new cjs.Text("60 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_2.setTransform(173, 48);
        this.text_2.skewX = 10;
        this.text_2.skewY = 10;

        this.text_3 = new cjs.Text("45 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_3.setTransform(402, 30);
        this.text_3.skewX = 10;
        this.text_3.skewY = 10;

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 70);
        this.vrLine_1.setTransform(240, 30);

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 10.5);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.instance, this.hrLine_1, this.hrLine_2,
            this.text_2, this.text_3, this.vrLine_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 105);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(310, 109, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(310, 260, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(310, 372, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v4 = new lib.Symbol5();
        this.v4.setTransform(310, 490, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v5 = new lib.Symbol6();
        this.v5.setTransform(310, 582, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.v5, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
