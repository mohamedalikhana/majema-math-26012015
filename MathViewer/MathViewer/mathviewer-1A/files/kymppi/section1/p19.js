(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p18_1.png",
            id: "p18_1"
        }]
    };

    (lib.p18_1 = function() {
        this.initialize(img.p18_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("19", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Hitta andra saker i klassrummet. Räkna, rita och visa antalet.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(57, 7.6);

        this.instance = new lib.p18_1();
        this.instance.setTransform(3, -11, 0.64, 0.64);

        this.text_2 = new cjs.Text("Sak", "16px 'UusiTekstausMajema'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(10, 51);

        this.text_3 = new cjs.Text("Antal", "16px 'UusiTekstausMajema'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(107, 101);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 70, 98, 110, 5);
        this.round_Rect1.setTransform(0, 0);
        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(94, 119, 142, 55, 5);
        this.round_Rect2.setTransform(0, 0);

        this.round_Rect3 = this.round_Rect1.clone(true);
        this.round_Rect3.setTransform(270, 0);
        this.round_Rect4 = this.round_Rect2.clone(true);
        this.round_Rect4.setTransform(270, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#949599").ss(1).moveTo(108, 165).lineTo(224, 165).moveTo(378, 165).lineTo(494, 165);
        this.line1.setTransform(0, 0);

        this.addChild(this.text_1, this.instance, this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4,
            this.line1, this.text_2, this.text_3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 220);

    (lib.Symbol2 = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 30, 98, 110, 5);
        this.round_Rect1.setTransform(0, 0);
        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(94, 79, 142, 55, 5);
        this.round_Rect2.setTransform(0, 0);

        this.round_Rect3 = this.round_Rect1.clone(true);
        this.round_Rect3.setTransform(270, 0);
        this.round_Rect4 = this.round_Rect2.clone(true);
        this.round_Rect4.setTransform(270, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#949599").ss(1).moveTo(108, 125).lineTo(224, 125).moveTo(378, 125).lineTo(494, 125);
        this.line1.setTransform(0, 0);

        this.addChild(this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4, this.line1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 220);

    (lib.Symbol3 = function() {
        this.initialize();

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(5, 30, 98, 110, 5);
        this.round_Rect1.setTransform(0, 0);
        this.round_Rect2 = new cjs.Shape();
        this.round_Rect2.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(94, 79, 142, 55, 5);
        this.round_Rect2.setTransform(0, 0);

        this.round_Rect3 = this.round_Rect1.clone(true);
        this.round_Rect3.setTransform(270, 0);
        this.round_Rect4 = this.round_Rect2.clone(true);
        this.round_Rect4.setTransform(270, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#949599").ss(1).moveTo(108, 125).lineTo(224, 125).moveTo(378, 125).lineTo(494, 125);
        this.line1.setTransform(0, 0);

        this.text_1 = new cjs.Text("Visa och berätta för en kamrat.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(2, 165);

        this.addChild(this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4, this.line1, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 220);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(309, 270, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(309, 468, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(309, 627, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
