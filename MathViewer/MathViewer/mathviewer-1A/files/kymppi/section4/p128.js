(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p128_1.png",
            id: "p128_1"
        },{
            src: "images/p128_2.png",
            id: "p128_2"
        }]
    };

    (lib.p128_1 = function() {
        this.initialize(img.p128_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p128_2 = function() {
        this.initialize(img.p128_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("128", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(33, 639);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28, 652.2);

        this.instance = new lib.p128_1();
        this.instance.setTransform(36, 13, 0.378, 0.378);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(453.2 + (columnSpace * 32), 19, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p128_2();
        this.instance.setTransform(0, 18, 0.378, 0.378);

        // Layer 1
        this.text_q1 = new cjs.Text("Hur många kronor är det?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(16, -3);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(-2, -3);

         this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((702 * 0.5) - 10) * 0.49, ((910 * (1 / 3)) - 55) * 0.49, 10);
        this.roundRect1.setTransform(0, 21);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(172, 21);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(345, 21);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(0, 148);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(172, 148);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(345, 148);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 275);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(172, 275);

        this.roundRect9 = this.roundRect1.clone(true);
        this.roundRect9.setTransform(345, 275);

        

        this.line_1 = new cjs.Shape();
        this.line_1.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_1.setTransform(25, 95);

        this.line_2 = new cjs.Shape();
        this.line_2.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_2.setTransform(197, 95);

        this.line_3 = new cjs.Shape();
        this.line_3.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_3.setTransform(368, 95);

        this.line_4 = new cjs.Shape();
        this.line_4.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_4.setTransform(25, 222);

        this.line_5 = new cjs.Shape();
        this.line_5.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_5.setTransform(197, 222);

        this.line_6 = new cjs.Shape();
        this.line_6.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_6.setTransform(368, 222);

        this.line_7 = new cjs.Shape();
        this.line_7.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_7.setTransform(25, 348);

        this.line_8 = new cjs.Shape();
        this.line_8.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_8.setTransform(197, 348);

        this.line_9 = new cjs.Shape();
        this.line_9.graphics.s("#AAAAAA").ss(0.8).moveTo(40, 40).lineTo(94, 40);
        this.line_9.setTransform(368, 348);

               

        this.text_2 = new cjs.Text("kr", "bold 22px 'UusiTekstausMajema'", "#000000");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(103, 115);


        this.addChild(this.text_q1,this.text_q2,this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5,
            this.roundRect6, this.roundRect7, this.roundRect8, this.roundRect9, this.roundRect10,
            this.roundRect11, this.roundRect12,
            this.text, this.text_1, this.line_1, this.line_2, this.line_3, this.line_4, this.line_5, this.line_6, this.line_7, this.line_8, this.line_9,
            this.line_10, this.line_11, this.line_12, this.text_2, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 349, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(297, 459, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
