(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p10_1.png",
            id: "p10_1"
        }, {
            src: "images/p10_2.png",
            id: "p10_2"
        }, {
            src: "images/p10_3.png",
            id: "p10_3"
        }]
    };


    // symbols:
    (lib.p10_1 = function() {
        this.initialize(img.p10_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 827, 288);

    (lib.p10_2 = function() {
        this.initialize(img.p10_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 918, 315);

    (lib.p10_3 = function() {
        this.initialize(img.p10_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 918, 315);


	//Page Number Text
    (lib.Symbol19 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(40, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1
        //Image Layer Bottom
        this.instance = new lib.p10_2();
        this.instance.setTransform(41.7, 272.7, 0.5, 0.5);
        //Image Layer Top
        this.instance_1 = new lib.p10_1();
        this.instance_1.setTransform(75.5, 51, 0.5, 0.5);
        //Bottom Layer Starts
        //Bottom layer text
        // this.instance_2 = new lib.Symbol20();
        // this.instance_2.setTransform(182, 13.6, 1, 1, 0, 0, 0, 163, 12.6);
        this.text1 = new cjs.Text("Måla lika många.", "16px 'Myriad Pro'");
        this.text1.lineHeight = 19;
        this.text1.setTransform(182, 17.2, 1, 1, 0, 0, 0, 163, 12.6);
        
        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 4);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape.setTransform(493.7, 441.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_1.setTransform(493.7, 328.7);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_2.setTransform(493.7, 218.4);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_3.setTransform(493.7, 105.8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_4.setTransform(235.5, 441.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_5.setTransform(235.5, 328.7);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_6.setTransform(235.5, 218.4);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_7.setTransform(235.5, 105.8);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAnAdAdQAcAcAmAAg");
        this.shape_8.setTransform(369.1, 441.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAoAdAcQAcAcAmAAg");
        this.shape_9.setTransform(369.1, 328.7);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAnAdAdQAcAcAmAAg");
        this.shape_10.setTransform(369.1, 218.4);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
        this.shape_11.setTransform(369.1, 105.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_12.setTransform(110.9, 441.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_13.setTransform(110.9, 328.7);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_14.setTransform(110.9, 218.4);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgdQAdgbAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_15.setTransform(110.9, 105.8);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_16.setTransform(470.2, 441.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_17.setTransform(470.2, 328.7);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_18.setTransform(470.2, 218.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgdQAcgbAAgoQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
        this.shape_19.setTransform(470.2, 105.8);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_20.setTransform(212, 441.9);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_21.setTransform(212, 328.7);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_22.setTransform(212, 218.4);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgdQAdgbAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_23.setTransform(212, 105.8);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAdAAAmQAAAnAdAdQAbAcAnAAg");
        this.shape_24.setTransform(345.7, 441.9);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAoAdAcQAbAcAnAAg");
        this.shape_25.setTransform(345.7, 328.7);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAnAdAdQAbAcAnAAg");
        this.shape_26.setTransform(345.7, 218.4);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
        this.shape_27.setTransform(345.7, 105.8);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_28.setTransform(87.5, 441.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_29.setTransform(87.5, 328.7);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_30.setTransform(87.5, 218.4);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_31.setTransform(87.5, 105.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAnAdAdQAcAcAmAAg");
        this.shape_32.setTransform(447.6, 441.9);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAoAdAcQAcAcAmAAg");
        this.shape_33.setTransform(447.6, 328.7);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAnAdAdQAcAcAmAAg");
        this.shape_34.setTransform(447.6, 218.4);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
        this.shape_35.setTransform(447.6, 105.8);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAdAAAmQAAAnAdAdQAbAcAnAAg");
        this.shape_36.setTransform(189.4, 441.9);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAoAdAcQAbAcAnAAg");
        this.shape_37.setTransform(189.4, 328.7);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgcAcQgdAdAAAmQAAAnAdAdQAbAcAnAAg");
        this.shape_38.setTransform(189.4, 218.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgbAcQgdAcAAAnQAAAoAdAbQAcAdAmAAg");
        this.shape_39.setTransform(189.4, 105.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_40.setTransform(323.1, 441.9);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_41.setTransform(323.1, 328.7);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_42.setTransform(323.1, 218.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgdQAcgbAAgoQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
        this.shape_43.setTransform(323.1, 105.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_44.setTransform(64.9, 441.9);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_45.setTransform(64.9, 328.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_46.setTransform(64.9, 218.4);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
        this.shape_47.setTransform(64.9, 105.8);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAcgcQAdgdAAgnQAAgmgdgdQgcgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_48.setTransform(425.5, 441.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAcgcQAdgcAAgoQAAgmgdgdQgcgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_49.setTransform(425.5, 328.7);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAcgcQAdgdAAgnQAAgmgdgdQgcgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_50.setTransform(425.5, 218.4);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAcgdQAdgbAAgoQAAgngdgcQgcgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_51.setTransform(425.5, 105.8);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_52.setTransform(167.2, 441.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_53.setTransform(167.2, 328.7);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_54.setTransform(167.2, 218.4);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAnAAAdgdQAcgbAAgoQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAdAdAmAAg");
        this.shape_55.setTransform(167.2, 105.8);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_56.setTransform(300.9, 441.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_57.setTransform(300.9, 328.7);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_58.setTransform(300.9, 218.4);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgdQAdgbAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_59.setTransform(300.9, 105.8);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_60.setTransform(42.7, 441.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_61.setTransform(42.7, 328.7);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_62.setTransform(42.7, 218.4);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_63.setTransform(42.7, 105.8);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_64.setTransform(402.8, 441.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_65.setTransform(402.8, 328.7);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_66.setTransform(402.8, 218.4);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_67.setTransform(402.8, 105.8);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_68.setTransform(144.6, 441.9);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgcAAgoQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_69.setTransform(144.6, 328.7);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgcQAdgdAAgnQAAgmgdgdQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_70.setTransform(144.6, 218.4);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAbgdQAdgbAAgoQAAgngdgcQgbgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_71.setTransform(144.6, 105.8);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_72.setTransform(278.3, 441.9);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_73.setTransform(278.3, 328.7);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_74.setTransform(278.3, 218.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_75.setTransform(278.3, 105.8);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_76.setTransform(20.1, 441.9);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape_77.setTransform(20.1, 328.7);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_78.setTransform(20.1, 218.4);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#959C9D").ss(0.6, 0, 0, 4).p("AAABgQAoAAAcgdQAcgbAAgoQAAgngcgcQgcgcgoAAQgnAAgcAcQgcAcAAAnQAAAoAcAbQAcAdAnAAg");
        this.shape_79.setTransform(20.1, 105.8);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgcQgdg5hHAAg");
        this.shape_80.setTransform(386.4, 411.9);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAcA5QAOAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
        this.shape_81.setTransform(386.4, 411.9);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgcQgdg5hHAAg");
        this.shape_82.setTransform(386.4, 300.1);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAcA5QAOAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
        this.shape_83.setTransform(386.4, 300.1);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgcAOQg5AcAABHIAALRIACARQADAWAJASQAcA4BHAAMAjxAAAIASgCQAVgDASgJQA5gcAAhHIAArRQAAgcgOgdQgdg4hHAAg");
        this.shape_84.setTransform(386.4, 76.5);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAcA4QAOAdAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
        this.shape_85.setTransform(386.4, 76.5);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4naQgcAAgcAPQg5AcAABHIAALRIACASQADAVAJASQAcA5BHAAMAjxAAAIASgCQAVgEASgIQA5gdAAhHIAArRQAAgdgOgcQgdg5hHAAg");
        this.shape_86.setTransform(386.4, 188.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("Ax4HaQhHABgdg5QgIgSgDgVIgCgSIAArRQAAhHA4gdQAdgNAcAAMAjxAAAQBHAAAcA4QAOAdAAAcIAALRQAABHg4AdQgSAIgVAEIgSABg");
        this.shape_87.setTransform(386.4, 188.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_88.setTransform(128.4, 411.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAdA5QANAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
        this.shape_89.setTransform(128.4, 411.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgcQgdg5hHAAg");
        this.shape_90.setTransform(128.4, 300.1);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAdA5QANAcAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
        this.shape_91.setTransform(128.4, 300.1);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4nZQgcAAgdAOQg4AcAABHIAALRIABARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAArRQAAgcgNgdQgdg4hHAAg");
        this.shape_92.setTransform(128.4, 76.5);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("Ax4HaQhHAAgdg4QgIgSgDgWIgCgRIAArRQAAhHA4gcQAdgOAcAAMAjxAAAQBHAAAdA4QANAdAAAcIAALRQAABHg4AcQgSAJgVADIgSACg");
        this.shape_93.setTransform(128.4, 76.5);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4naQgcAAgdAPQg4AcAABHIAALRIABASQAEAVAIASQAdA5BHAAMAjxAAAIARgCQAWgEASgIQA4gdAAhHIAArRQAAgdgNgcQgdg5hHAAg");
        this.shape_94.setTransform(128.4, 188.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("Ax4HaQhHABgdg5QgIgSgDgVIgCgSIAArRQAAhHA4gdQAdgNAcAAMAjxAAAQBHAAAdA4QANAdAAAcIAALRQAABHg4AdQgSAIgVAEIgSABg");
        this.shape_95.setTransform(128.4, 188.9);

        this.ball_instance1 = new lib.p10_3();
        this.ball_instance1.setTransform(10, 96, 0.19, 0.19);

        this.ball_instance2 = new lib.p10_3();
        this.ball_instance2.setTransform(32, 96, 0.19, 0.19);

        this.ball_instance3 = new lib.p10_3();
        this.ball_instance3.setTransform(54, 96, 0.19, 0.19);

        this.addChild(this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text, this.text1, this.instance_1, this.instance);
        this.addChild(this.ball_instance1,this.ball_instance2,this.ball_instance3);
        //Bottom Layer Ends
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 460.4);
	//p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 214.4);

    (lib.Symbol3 = function() {
        this.initialize();
//Top Layer Starts
        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape.setTransform(433, 61.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_1.setTransform(433, 57.3);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_2.setTransform(434, 97.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_3.setTransform(434, 93.3);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_4.setTransform(393.1, 61.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_5.setTransform(393.1, 57.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_6.setTransform(394.1, 97.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_7.setTransform(394.1, 93.3);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_8.setTransform(353.8, 61.6);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_9.setTransform(353.8, 57.3);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_10.setTransform(354.8, 97.6);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_11.setTransform(354.8, 93.3);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_12.setTransform(314.5, 97.6);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_13.setTransform(314.5, 93.3);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_14.setTransform(314.5, 61.6);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_15.setTransform(314.5, 57.3);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_16.setTransform(176.4, 97.6);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_17.setTransform(176.4, 93.3);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_18.setTransform(136.5, 97.6);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_19.setTransform(136.5, 93.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_20.setTransform(96.7, 97.6);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_21.setTransform(96.7, 93.3);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_22.setTransform(57.3, 97.6);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_23.setTransform(57.3, 93.3);

        this.text = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text.lineHeight = 32;
        this.text.setTransform(452.9, 84.1,0.8,0.78);

        this.text_1 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_1.lineHeight = 32;
        this.text_1.setTransform(413.3, 84.1,0.8,0.78);

        this.text_2 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_2.lineHeight = 32;
        this.text_2.setTransform(373.6, 84.1,0.8,0.78);

        this.text_3 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_3.lineHeight = 32;
        this.text_3.setTransform(333.9, 84.1,0.8,0.78);

        this.text_4 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_4.lineHeight = 32;
        this.text_4.setTransform(294.2, 84.1,0.8,0.78);

        this.text_5 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_5.lineHeight = 32;
        this.text_5.setTransform(195, 84.1,0.8,0.78);

        this.text_6 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_6.lineHeight = 32;
        this.text_6.setTransform(155.3, 84.1,0.8,0.78);

        this.text_7 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_7.lineHeight = 32;
        this.text_7.setTransform(115.6, 84.1,0.8,0.78);

        this.text_8 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_8.lineHeight = 32;
        this.text_8.setTransform(75.9, 84.1,0.8,0.78);

        this.text_9 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_9.lineHeight = 32;
        this.text_9.setTransform(36.2, 84.1,0.8,0.78);

        this.text_10 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_10.lineHeight = 32;
        this.text_10.setTransform(452.2, 47.9,0.8,0.78);

        this.text_11 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_11.lineHeight = 32;
        this.text_11.setTransform(412.5, 47.9,0.8,0.78);

        this.text_12 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_12.lineHeight = 32;
        this.text_12.setTransform(372.9, 47.9,0.8,0.78);

        this.text_13 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_13.lineHeight = 32;
        this.text_13.setTransform(333.2, 47.9,0.8,0.78);

        this.text_14 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_14.lineHeight = 32;
        this.text_14.setTransform(293.5, 47.9,0.8,0.78);

        this.text_15 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_15.lineHeight = 32;
        this.text_15.setTransform(195, 47.9,0.8,0.78);

        this.text_16 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_16.lineHeight = 32;
        this.text_16.setTransform(175.1, 47.9,0.8,0.78);

        this.text_17 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_17.lineHeight = 32;
        this.text_17.setTransform(155.3, 47.9,0.8,0.78);

        this.text_18 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_18.lineHeight = 32;
        this.text_18.setTransform(135.4, 47.9,0.8,0.78);

        this.text_19 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_19.lineHeight = 32;
        this.text_19.setTransform(115.6, 47.9,0.8,0.78);

        this.text_20 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_20.lineHeight = 32;
        this.text_20.setTransform(95.7, 47.9,0.8,0.78);

        this.text_21 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_21.lineHeight = 32;
        this.text_21.setTransform(75.9, 47.9,0.8,0.78);

        this.text_22 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_22.lineHeight = 32;
        this.text_22.setTransform(56, 47.9,0.8,0.78);

        this.text_23 = new cjs.Text("=", "bold 27px 'UusiTekstaus'", "#BCC1C2");
        this.text_23.lineHeight = 32;
        this.text_23.setTransform(36.2, 47.9,0.8,0.78);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_24.setTransform(202.3, 105.8);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_25.setTransform(182.3, 105.8);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_26.setTransform(162.4, 105.8);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_27.setTransform(142.6, 105.8);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_28.setTransform(122.7, 105.8);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDDAA");
        this.shape_29.setTransform(102.9, 105.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_30.setTransform(83.1, 105.8);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_31.setTransform(63.2, 105.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_32.setTransform(43.3, 105.8);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_33.setTransform(212.1, 94.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_34.setTransform(192.2, 94.5);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_35.setTransform(202.3, 83.2);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_36.setTransform(172.4, 94.5);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_37.setTransform(182.3, 83.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_38.setTransform(152.5, 94.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_39.setTransform(162.4, 83.2);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_40.setTransform(132.7, 94.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_41.setTransform(142.6, 83.2);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_42.setTransform(112.8, 94.5);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_43.setTransform(122.7, 83.2);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_44.setTransform(93, 94.5);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDDAA");
        this.shape_45.setTransform(102.9, 83.2);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_46.setTransform(73.2, 94.5);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_47.setTransform(83.1, 83.2);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_48.setTransform(53.3, 94.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_49.setTransform(63.2, 83.2);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_50.setTransform(33.5, 94.5);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_51.setTransform(43.3, 83.2);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_52.setTransform(460.2, 105.8);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_53.setTransform(440.2, 105.8);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_54.setTransform(420.4, 105.8);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_55.setTransform(400.5, 105.8);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_56.setTransform(380.7, 105.8);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_57.setTransform(360.9, 105.8);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_58.setTransform(341, 105.8);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_59.setTransform(321.2, 105.8);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_60.setTransform(301.2, 105.8);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_61.setTransform(470, 94.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_62.setTransform(450.2, 94.5);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_63.setTransform(460.2, 83.2);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_64.setTransform(430.3, 94.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_65.setTransform(440.2, 83.2);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_66.setTransform(410.5, 94.5);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_67.setTransform(420.4, 83.2);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_68.setTransform(390.6, 94.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_69.setTransform(400.5, 83.2);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_70.setTransform(370.8, 94.5);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_71.setTransform(380.7, 83.2);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_72.setTransform(351, 94.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_73.setTransform(360.9, 83.2);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_74.setTransform(331.1, 94.5);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_75.setTransform(341, 83.2);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_76.setTransform(311.3, 94.5);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_77.setTransform(321.2, 83.2);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_78.setTransform(291.4, 94.5);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_79.setTransform(301.2, 83.2);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_80.setTransform(459.5, 69.7);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_81.setTransform(439.5, 69.7);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_82.setTransform(419.7, 69.7);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_83.setTransform(399.9, 69.7);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_84.setTransform(380, 69.7);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_85.setTransform(360.2, 69.7);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_86.setTransform(340.3, 69.7);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_87.setTransform(320.5, 69.7);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_88.setTransform(300.5, 69.7);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_89.setTransform(469.3, 58.3);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_90.setTransform(449.5, 58.3);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_91.setTransform(459.5, 47);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_92.setTransform(429.6, 58.3);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_93.setTransform(439.5, 47);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_94.setTransform(409.8, 58.3);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_95.setTransform(419.7, 47);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_96.setTransform(390, 58.3);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_97.setTransform(399.9, 47);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_98.setTransform(370.1, 58.3);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_99.setTransform(380, 47);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_100.setTransform(350.3, 58.3);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_101.setTransform(360.2, 47);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_102.setTransform(330.4, 58.3);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_103.setTransform(340.3, 47);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_104.setTransform(310.6, 58.3);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_105.setTransform(320.5, 47);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_106.setTransform(290.7, 58.3);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_107.setTransform(300.5, 47);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_108.setTransform(202.3, 69.7);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_109.setTransform(182.3, 69.7);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_110.setTransform(162.4, 69.7);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_111.setTransform(142.6, 69.7);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_112.setTransform(122.7, 69.7);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDDAA");
        this.shape_113.setTransform(102.9, 69.7);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDDAA");
        this.shape_114.setTransform(83.1, 69.7);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_115.setTransform(63.2, 69.7);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_116.setTransform(43.3, 69.7);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_117.setTransform(212.1, 58.3);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_118.setTransform(192.2, 58.3);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_119.setTransform(202.3, 47);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_120.setTransform(172.4, 58.3);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_121.setTransform(182.3, 47);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_122.setTransform(152.5, 58.3);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_123.setTransform(162.4, 47);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_124.setTransform(132.7, 58.3);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_125.setTransform(142.6, 47);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_126.setTransform(112.8, 58.3);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_127.setTransform(122.7, 47);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_128.setTransform(93, 58.3);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDDAA");
        this.shape_129.setTransform(102.9, 47);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_130.setTransform(73.2, 58.3);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDDAA");
        this.shape_131.setTransform(83.1, 47);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_132.setTransform(53.3, 58.3);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_133.setTransform(63.2, 47);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_134.setTransform(33.5, 58.3);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_135.setTransform(43.3, 47);

        this.text_24 = new cjs.Text("Öva.", "16px 'Myriad Pro'");
        this.text_24.lineHeight = 19;
        this.text_24.setTransform(19, 6.1);

        this.text_25 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_25.lineHeight = 20;
        this.text_25.setTransform(0, 5);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgGzQgcAAgcAOQg5AdAABHIAAKDIACASQADAVAJASQAcA5BHAAMBMFAAAIARgCQAWgEASgIQA4gdAAhHIAAqDQAAgdgOgcQgcg5hHAAg");
        this.shape_136.setTransform(257.4, 74.4);

        this.addChild(this.shape_136, this.text_25, this.text_24, this.shape_135, this.shape_134, this.shape_133, this.shape_132, this.shape_131, this.shape_130, this.shape_129, this.shape_128, this.shape_127, this.shape_126, this.shape_125, this.shape_124, this.shape_123, this.shape_122, this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.ra1);
        //Top Layer Ends
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 119.1);


    // (lib.Symbol2 = function() {
    //     this.initialize();

    //     this.addChild();
    // }).prototype = p = new cjs.Container();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 214.4);


    // stage content:
    (lib.p10 = function() {
        this.initialize();

        // Layer 1
        //Bottom Layer PS
        this.v4 = new lib.Symbol4();
        this.v4.setTransform(297, 405.4, 1, 1, 0, 0, 0, 256.3, 229.8);
//Top Layer PS
        this.v3 = new lib.Symbol3();
        this.v3.setTransform(297, 109.7, 1, 1, 0, 0, 0, 256.3, 59.1);

        //this.v2 = new lib.Symbol2();
        //this.v4.setTransform(296.8, 528, 1, 1, 0, 0, 0, 256.3, 106.8);

        this.other = new lib.Symbol19();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.other2 = new lib.Symbol19();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild( this.v3, this.v4,this.other,this.other2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
