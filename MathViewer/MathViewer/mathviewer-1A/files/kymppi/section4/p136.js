(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p136_1.png",
            id: "p136_1"
        }, {
            src: "images/p136_2.png",
            id: "p136_2"
        }, {
            src: "images/p136_3.png",
            id: "p136_3"
        }]
    };

    (lib.p136_1 = function() {
        this.initialize(img.p136_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p136_2 = function() {
        this.initialize(img.p136_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p136_3 = function() {
        this.initialize(img.p136_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("136", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(39.5, 638);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(35, 650.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(461 + (columnSpace * 33), 32, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.instance = new lib.p136_1();
        this.instance.setTransform(101, 91, 0.47, 0.47);

        this.Line_bg1 = new cjs.Shape();
        this.Line_bg1.graphics.f("#E2F0FA").beginStroke("#0095DA").setStrokeStyle(0.7).moveTo(89, 75).lineTo(89, 137.5).lineTo(503, 137.5).lineTo(503, 75).lineTo(89, 75);
        this.Line_bg1.setTransform(0, 0);

        this.Line_bg2 = new cjs.Shape();
        this.Line_bg2.graphics.f("#ffffff").beginStroke("#0095DA").setStrokeStyle(0.7).moveTo(89, 137.5).lineTo(89, 157).lineTo(503, 157).lineTo(503, 137.5).lineTo(89, 137.5);
        this.Line_bg2.setTransform(0, 0);

        this.Line_bg3 = new cjs.Shape();
        this.Line_bg3.graphics.beginStroke("#0095DA").setStrokeStyle(0.5).moveTo(157, 75).lineTo(157, 157).moveTo(225, 75).lineTo(225, 157).moveTo(295, 75).lineTo(295, 157).moveTo(364, 75).lineTo(364, 157).moveTo(434, 75).lineTo(434, 157);
        this.Line_bg3.setTransform(0, 0);
        // col-1
        this.text_1 = new cjs.Text("katt", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 25;
        this.text_1.setTransform(108, 80);
        this.text_num1 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_num1.lineHeight = 25;
        this.text_num1.setTransform(114, 139.5);
        // col-2
        this.text_2 = new cjs.Text("fågel", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(172.5, 80);
        this.text_num2 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_num2.lineHeight = 25;
        this.text_num2.setTransform(184.5, 139.5);
        // col-3
        this.text_3 = new cjs.Text("hund", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(240.5, 80);
        this.text_num3 = new cjs.Text("9", "16px 'Myriad Pro'");
        this.text_num3.lineHeight = 25;
        this.text_num3.setTransform(252.5, 139.5);
        // col-4
        this.text_4 = new cjs.Text("ödla", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(310.5, 80);
        this.text_num4 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_num4.lineHeight = 25;
        this.text_num4.setTransform(322.5, 139.5);
        // col-5
        this.text_5 = new cjs.Text("hamster", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(368.5, 80);
        this.text_num5 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_num5.lineHeight = 25;
        this.text_num5.setTransform(387.5, 139.5);
        // col-6
        this.text_6 = new cjs.Text("kanin", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 25;
        this.text_6.setTransform(445.5, 80);
        this.text_num6 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_num6.lineHeight = 25;
        this.text_num6.setTransform(457.5, 139.5);

        this.text_h1 = new cjs.Text("Elevernas husdjur", "15px 'MyriadPro-Semibold'");
        this.text_h1.lineHeight = 19;
        this.text_h1.setTransform(86, 55);

        this.addChild(this.shape, this.text, this.textbox_group1, this.Line_bg1, this.Line_bg2, this.Line_bg3, this.text_1, this.text_num1, this.text_2, this.text_num2, this.text_3, this.text_num3, this.text_4, this.text_num4, this.text_5, this.text_num5, this.text_6, this.text_num6, this.text_h1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Måla staplarna så att de stämmer med antalet djur.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 16);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 16);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#E0E4F2").s('#8490C8').drawRoundRect(0, 23, 515, 233, 10);
        this.roundRect1.setTransform(0, 17);

        this.instance = new lib.p136_2();
        this.instance.setTransform(45, 220, 0.47, 0.47);

        var ToBeAdded = [];
        for (var num = 0; num < 11; num++) {
            var xPos = 32;
            if (num == 0 || num == 5 || num == 10) {
                var temptext = new cjs.Text("" + num, "12px 'Myriad Pro'");
                temptext.lineHeight = -1;
                if (num == 10) {
                    xPos = 25;
                    num = 10.15;
                }
                temptext.setTransform(xPos, 217 + (-13.3 * num));
                ToBeAdded.push(temptext);
            }
        }

        this.shape_arrow1 = new cjs.Shape();
        this.shape_arrow1.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow1.setTransform(48.8, 50, 1, 1, 175);

        this.shape_arrow2 = new cjs.Shape();
        this.shape_arrow2.graphics.f("#000000").s().p("AgVgfIArAEIgaA7g");
        this.shape_arrow2.setTransform(452, 222.8, 1, 1, 265);

        this.Line_bg1 = new cjs.Shape();
        this.Line_bg1.graphics.f("#ffffff").beginStroke("#ffffff").setStrokeStyle(0.7).moveTo(49, 61).lineTo(49, 223).lineTo(433, 223).lineTo(433, 61).lineTo(49, 61);
        this.Line_bg1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(49, 50).lineTo(49, 223).moveTo(43.5, 223).lineTo(452, 223)
            .moveTo(59, 61).lineTo(59, 223).moveTo(83, 61).lineTo(83, 223)
            .moveTo(132, 61).lineTo(132, 223).moveTo(156, 61).lineTo(156, 223)
            .moveTo(201.6, 61).lineTo(201.6, 223).moveTo(225.6, 61).lineTo(225.6, 223).moveTo(272, 61).lineTo(272, 223).moveTo(296, 61).lineTo(296, 223).moveTo(341, 61).lineTo(341, 223).moveTo(365, 61).lineTo(365, 223).moveTo(409, 61).lineTo(409, 223).moveTo(433, 61).lineTo(433, 223);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#949599").setStrokeStyle(0.7).moveTo(43.5, 209.5).lineTo(433, 209.5).moveTo(43.5, 196).lineTo(433, 196).moveTo(43.5, 182.5).lineTo(433, 182.5).moveTo(43.5, 169).lineTo(433, 169).moveTo(43.5, 142).lineTo(433, 142).moveTo(43.5, 128.5).lineTo(433, 128.5).moveTo(43.5, 115).lineTo(433, 115).moveTo(43.5, 101.5).lineTo(433, 101.5).moveTo(43.5, 74.5).lineTo(433, 74.5).moveTo(43.5, 61).lineTo(433, 61);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(43.5, 88).lineTo(440, 88).moveTo(43.5, 155.5).lineTo(440, 155.5)
            .moveTo(409, 223.7).lineTo(433, 223.7).moveTo(341, 223.7).lineTo(365, 223.7).moveTo(272, 223.7).lineTo(296, 223.7).moveTo(201.6, 223.7).lineTo(225.6, 223.7).moveTo(132, 223.7).lineTo(156, 223.7).moveTo(59, 223.7).lineTo(83, 223.7);
        this.Line_3.setTransform(0, 0);

        this.addChild(this.text_q1, this.text_q2, this.roundRect1, this.Line_bg1, this.Line_1, this.Line_2, this.Line_3, this.Line_5, this.shape_arrow1, this.shape_arrow2, this.instance);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i])
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 280);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.text_q2 = new cjs.Text("Hur många djur är det tillsammans?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20, 1.13);

        this.instance_2 = new lib.p136_3();
        this.instance_2.setTransform(54, 26, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 78, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 15);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 97);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 97);

        var arrTxtbox = ['75.9', '157'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 100,
            rectHeight = 22,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 74.5;
                if (i == 1) {
                    padding = 118;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("1", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(75, 65.1);
        this.text_2 = new cjs.Text("+", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(95, 65.1);
        this.text_3 = new cjs.Text("4", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(115, 65.1);
        this.text_4 = new cjs.Text("=", "35px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(136, 65.1);

        var ToBeAdded = [];
        for (var col = 0; col < 2; col++) {
            for (var row = 0; row < 2; row++) {
                var rowSpace=row;
                var colSpace=col;
                var temptext = new cjs.Text("och", "16px 'Myriad Pro'");
                temptext.lineHeight = -1;
                temptext.setTransform(109 + (262 * colSpace), 45 + (79.5 * rowSpace));
                ToBeAdded.push(temptext);
            }
        }

        this.addChild(this.text, this.text_q2, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 180);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(605.5, 349, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297, 383.5, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(297, 668, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
