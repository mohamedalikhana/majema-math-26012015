var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c14_ex1_s2_1.png",
            id: "c14_ex1_s2_1"
        }]
    };



    (lib.c14_ex1_s2_1 = function() {
        this.initialize(img.c14_ex1_s2_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Öva addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("14", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();
        this.image = new lib.c14_ex1_s2_1();
        this.image.setTransform(500 - 50, 60, 1.55, 1.55)
        this.answers = new lib.answers();
        // this.answers.text_4.visible = true;
        // this.answers.text_3.visible = true;
        // this.answers.text_2.visible = true;
        // this.answers.text_1.visible = true;
        this.answers.setTransform(280 - 100, 300 + 60, 4, 4, 0, 0, 0, 0, 0);


        this.instrTexts = [];
        this.instrTexts.push('Hur många lådor är det tillsammans?');
        this.instrTexts.push('Hur många?');
        this.instrTexts.push('Hur många kommer?');


        this.instrText1 = new cjs.Text(this.instrTexts[0], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(550 + 120, -40);
        this.instrText1.visible = false;

        this.instrText2 = this.instrText1.clone(true);
        this.instrText2.text = this.instrTexts[1];
        this.instrText3 = this.instrText1.clone(true);
        this.instrText3.text = this.instrTexts[2];

        this.addChild(this.image, this.answers, this.instrText1, this.instrText2, this.instrText3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;

        this.answers = this.stage0.answers;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;


        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;

        this.answers = this.stage0.answers;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;


        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });


        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(95, 0);

        this.text_2 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(107, 0);

        this.text_3 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(119, 0);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(131, 0);

        this.text_5 = new cjs.Text("5", "16px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(144, 0);

        this.addChild(this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)



        this.addChild(this.other, this.stage1, this.stage2, this.stage3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
