(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med addition 0 till 10", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(70, 651);

        this.text_2 = new cjs.Text("104", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(33.4, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31, 660.8);

        this.text_3 = new cjs.Text("Talkamrater", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(95.5, 26);

        this.text_4 = new cjs.Text("36", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(48, 22);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();
        this.text_q1 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 16);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 16);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 14, 160, 154, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(178, 14, 160, 181, 10);
        this.roundRect2.setTransform(0, 25);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(356, 14, 160, 210, 10);
        this.roundRect3.setTransform(0, 25);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (column == 0 && row > 3) {
                    continue;
                } else if (column == 1 && row > 4) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.69;
                } else if (column == 2) {
                    columnSpace = 1.392;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.8).drawRect(67 + (columnSpace * 258), 75 + (row * 28), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var arrxPos = ['54', '225', '390'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                var fColor = "";
                if (i == 0) {
                    if (column > 2) {
                        continue;
                    }
                    fColor = "#20B14A"; //green
                } else if (i == 1) {
                    if (column > 3) {
                        continue;
                    }
                    fColor = "#FFF679"; //yellow
                } else if (i == 2) {
                    fColor = "#0095DA"; //blue
                }
                for (var row = 0; row < 1; row++) {
                    this.shape_group1.graphics.f(fColor).s("#6E6E70").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * 23), 58 + (row * 23), 9.3, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        var ToBeAdded = [];
        arrxPos = [];
        arrxPos = ['93', '272', '454'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "=   " + (i + 3).toString();

            for (var j = 0; j < 6; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 2) {
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        rowSpace = 3.045;
                    } else if (j > 3) {
                        continue;
                    }
                } else if (i == 1) {
                    if (j == 2) {
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        rowSpace = 3.045;
                    } else if (j == 4) {
                        rowSpace = 4.1;
                    } else if (j > 4) {
                        continue;
                    }
                } else if (i == 2) {
                    if (j == 2) {
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        rowSpace = 3.045;
                    } else if (j == 4) {
                        rowSpace = 4.1;
                    } else if (j == 5) {
                        rowSpace = 5.1;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 81 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        arrxPos = [];
        arrxPos = ['33', '212', '394'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "";

            for (var j = 0; j < 6; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 0) {
                        text = "0   +";
                    } else if (j == 1) {
                        text = "3   +";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "1   +";
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "2   +";
                    } else if (j > 3) {
                        continue;
                    }
                } else if (i == 1) {
                    if (j == 0) {
                        text = "0   +";
                    } else if (j == 1) {
                        text = "4   +";
                    } else if (j == 2) {
                        text = "1   +";
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        text = "3   +";
                        rowSpace = 3.045;
                    } else if (j == 4) {
                        text = "2   +";
                        rowSpace = 4.1;
                    } else if (j > 4) {
                        continue;
                    }
                } else if (i == 2) {
                    if (j == 0) {
                        text = "0   +";
                    } else if (j == 1) {
                        text = "5   +";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "1   +";
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "3   +";
                    } else if (j == 4) {
                        rowSpace = 4.1;
                        text = "2   +";
                    } else if (j == 5) {
                        rowSpace = 5.1;
                        text = "4   +";
                        xPos = 392;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 81 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        this.addChild(this.text_q1, this.text_q2, this.roundRect1, this.roundRect2, this.roundRect3, this.textbox_group1, this.shape_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 14, 160, 265, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(178, 14, 160, 294, 10);
        this.roundRect2.setTransform(0, 25);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(356, 14, 160, 323, 10);
        this.roundRect3.setTransform(0, 25);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 9; row++) {
                if (column == 0 && row > 6) {
                    continue;
                } else if (column == 1 && row > 7) {
                    continue;
                }

                if (column == 1) {
                    columnSpace = 0.69;
                } else if (column == 2) {
                    columnSpace = 1.392;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.8).drawRect(67 + (columnSpace * 258), 103 + (row * 28), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var arrxPos = ['38', '217', '390'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                var fColor = "";

                for (var row = 0; row < 2; row++) {
                    var colPadding = 23;
                    if (i == 0) {
                        if (row == 1 && column > 0) {
                            continue;
                        }
                        fColor = "#DA2129"; //red
                    } else if (i == 1) {
                        if (row == 1 && column > 1) {
                            continue;
                        }
                        fColor = "#0095DA"; //blue
                        colPadding = 20.5;
                    } else if (i == 2) {
                        if (row == 1 && column > 2) {
                            continue;
                        }
                        fColor = "#20B14A"; //green
                    }
                    this.shape_group1.graphics.f(fColor).s("#6E6E70").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * colPadding), 58 + (row * 23), 9.3, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        var ToBeAdded = [];
        arrxPos = [];
        arrxPos = ['93', '272', '454'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "=   " + (i + 6).toString();

            for (var j = 0; j < 9; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 2) {
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        rowSpace = 3.085;
                    } else if (j == 4) {
                        rowSpace = 4.16;
                    } else if (j == 5) {
                        rowSpace = 5.19;
                    } else if (j == 6) {
                        rowSpace = 6.23;
                    } else if (j > 6) {
                        continue;
                    }
                } else if (i == 1) {
                    if (j == 2) {
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        rowSpace = 3.085;
                    } else if (j == 4) {
                        rowSpace = 4.16;
                    } else if (j == 5) {
                        rowSpace = 5.19;
                    } else if (j == 6) {
                        rowSpace = 6.23;
                    } else if (j == 7) {
                        rowSpace = 7.3;
                    } else if (j > 7) {
                        continue;
                    }
                } else if (i == 2) {
                    if (j == 2) {
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        rowSpace = 3.085;
                    } else if (j == 4) {
                        rowSpace = 4.16;
                    } else if (j == 5) {
                        rowSpace = 5.19;
                    } else if (j == 6) {
                        rowSpace = 6.23;
                    } else if (j == 7) {
                        rowSpace = 7.3;
                    } else if (j == 8) {
                        rowSpace = 8.3;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 108 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        arrxPos = [];
        arrxPos = ['33', '212', '394'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "";

            for (var j = 0; j < 9; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 0) {
                        text = "0   +";
                    } else if (j == 1) {
                        text = "6   +";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "5   +";
                    } else if (j == 3) {
                        rowSpace = 3.085;
                        text = "3   +";
                    } else if (j == 4) {
                        rowSpace = 4.16;
                        text = "4   +";
                    } else if (j == 5) {
                        rowSpace = 5.19;
                        text = "2   +";
                    } else if (j == 6) {
                        rowSpace = 6.23;
                        text = "1   +";
                    } else if (j > 6) {
                        continue;
                    }
                } else if (i == 1) {
                    if (j == 0) {
                        text = "0   +";
                    } else if (j == 1) {
                        text = "7   +";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "6   +";
                    } else if (j == 3) {
                        rowSpace = 3.085;
                        text = "4   +";
                    } else if (j == 4) {
                        rowSpace = 4.16;
                        text = "5   +";
                    } else if (j == 5) {
                        rowSpace = 5.19;
                        text = "3   +";
                    } else if (j == 6) {
                        rowSpace = 6.23;
                        text = "2   +";
                    } else if (j == 7) {
                        rowSpace = 7.3;
                        text = "1   +";
                    } else if (j > 7) {
                        continue;
                    }
                } else if (i == 2) {
                    if (j == 0) {
                        text = "0   +";
                    } else if (j == 1) {
                        text = "8   +";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "7   +";
                    } else if (j == 3) {
                        rowSpace = 3.085;
                        text = "5   +";
                    } else if (j == 4) {
                        rowSpace = 4.16;
                        text = "6   +";
                    } else if (j == 5) {
                        rowSpace = 5.19;
                        text = "4   +";
                    } else if (j == 6) {
                        rowSpace = 6.23;
                        text = "3   +";
                    } else if (j == 7) {
                        rowSpace = 7.3;
                        text = "1   +";
                    } else if (j == 8) {
                        rowSpace = 8.3;
                        text = "2   +";
                        xPos = 392;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 108 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.shape_group1, this.textbox_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 330.2);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(293, 98, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(293, 324, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
