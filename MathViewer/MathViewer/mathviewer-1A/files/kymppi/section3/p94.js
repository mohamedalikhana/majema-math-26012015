(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p94_1.png",
            id: "p94_1"
        }]
    };

    (lib.p94_1 = function() {
        this.initialize(img.p94_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("94", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(33.4, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31, 660.8);

        this.instance = new lib.p94_1();
        this.instance.setTransform(33, 13, 0.822, 0.779);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(450 + (columnSpace * 33), 26 , 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape_2, this.shape_1, this.shape, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(6, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 25, 510, 240, 10);
        this.roundRect1.setTransform(0, 0);

        var colGrp = ['43', '297'];

        this.shape_group1 = new cjs.Shape();
        for (var colIndex = 0; colIndex < colGrp.length; colIndex++) {
            var rowStartVal = parseInt(colGrp[colIndex]);

            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    this.shape_group1.graphics.f("#20B14A").s("#000000").ss(0.8, 0, 0, 4).arc(rowStartVal + (columnSpace * 20), 50 + (row * 75), 8.2, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        var rowGrp = ['154', '408'];

        this.shape_group2 = new cjs.Shape();
        for (var rowIndex = 0; rowIndex < rowGrp.length; rowIndex++) {
            var colStartVal = parseInt(rowGrp[rowIndex]);

            for (var column = 0; column < 4; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    this.shape_group2.graphics.f("#20B14A").s("#000000").ss(0.8, 0, 0, 4).arc(colStartVal + (columnSpace * 20), 50 + (row * 75), 8.2, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            if(column==1){
                columnSpace=1.433;
            }
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(150 + (columnSpace * 174), 72 + (row * 75), 21, 24);
                //console.log(150 + (columnSpace * 150), 40 + (row * 30))
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("9  –  1   =  ", "16.3px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(81, 77);
        this.label2 = new cjs.Text("9  –  4   =  ", "16.3px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(81, 151);
        this.label3 = new cjs.Text("9  –  7   =  ", "16.3px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(81, 227);

        this.label4 = new cjs.Text("9  –  3   =  ", "16.3px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(330, 77);
        this.label5 = new cjs.Text("9  –  6   =  ", "16.3px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(330, 151);
        this.label6 = new cjs.Text("9  –  8   =  ", "16.3px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(330, 227);

        this.hrRule = new cjs.Shape();
        this.hrRule.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 104.5).lineTo(507, 104.5);
        this.hrRule.setTransform(0, 0);

        this.hrRule_2 = new cjs.Shape();
        this.hrRule_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 182).lineTo(507, 182);
        this.hrRule_2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(257.8, 33).lineTo(257.8, 100);
        this.Line_1.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(257.8, 110).lineTo(257.8, 178);
        this.Line_3.setTransform(0, 0);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(257.8, 187).lineTo(257.8, 262);
        this.Line_5.setTransform(0, 0);

        this.addChild(this.roundRect1, this.shape_group1, this.shape_group2, this.textbox_group1, this.hrRule, this.hrRule_2);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6);

        this.addChild(this.instance_2, this.text, this.Line_1, this.Line_3, this.Line_5);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 4);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(5, 4);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 28, 510, 135, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if(column==1){
                    columnSpace=0.96;
                }
                else if(column==2){
                    columnSpace=2.18;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(101.5 + (columnSpace * 154), 40 + (row * 28), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("7  +  2  =", "16px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(35, 43);
        this.label2 = new cjs.Text("6  +  2  =", "16px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(35, 72);
        this.label3 = new cjs.Text("8  +  1  =", "16px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(35, 100);
        this.label4 = new cjs.Text("6  +  3  =", "16px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(35, 128);

        this.label6 = new cjs.Text("4  +  5  =", "16px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(183, 43);
        this.label7 = new cjs.Text("3  +  5  =", "16px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(183, 72);
        this.label8 = new cjs.Text("5  +  4  =", "16px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(183, 100);
        this.label9 = new cjs.Text("2  +  6  =", "16px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(183, 128);

        this.label11 = new cjs.Text("3  +  3  +  3  =", "16px 'Myriad Pro'");
        this.label11.lineHeight = 30;
        this.label11.setTransform(339, 43);
        this.label12 = new cjs.Text("5  +  2  +  2  =", "16px 'Myriad Pro'");
        this.label12.lineHeight = 30;
        this.label12.setTransform(339, 72);
        this.label13 = new cjs.Text("9  –  2  –   1  =", "16px 'Myriad Pro'");
        this.label13.lineHeight = 30;
        this.label13.setTransform(339, 100);
        this.label14 = new cjs.Text("9  –  5  –   2  =", "16px 'Myriad Pro'");
        this.label14.lineHeight = 30;
        this.label14.setTransform(339, 128);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(288, 420, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(288, 472, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
