(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p51_1.png",
            id: "p51_1"
        }, {
            src: "images/p51_2.png",
            id: "p51_2"
        }]
    };

    (lib.p51_1 = function() {
        this.initialize(img.p51_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p51_2 = function() {
        this.initialize(img.p51_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("51", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(553, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.setTransform(118, 40);

        this.text_4 = new cjs.Text("17", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(65, 40);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52, 23.5, 1.2, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 195, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 47, 48, 87, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p51_1();
        this.instance.setTransform(503, 55, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Addera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(23, 27);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(4, 27);

        var ToBeAdded = [];
        var arryVal = ['9  +  3  =', '9  +  7  =', '8  +  8  =', '8  +  6  =', '6  +  7  =', '5  +  8  =', '4  +  9  =', '3  +  8  =', '3  +  4  +  5  =', '4  +  5  +  6  =', '6  +  6  +  7  =', '6  +  5  +  8  ='];
        var i = 0;
        var temp_Line1 = new cjs.Shape();
        var tmprow = 27;
        var tmpcol = 162;
        for (var col = 0; col < 3; col++) {
            var columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(arryVal[i], "16px 'Myriad Pro'");
                tempText.setTransform(33 + (150 * columnSpace), 65 + (30 * rowSpace));
                if (col == 1) {
                    tmpcol = 150
                } else if (col == 2) {
                    tmpcol = 165
                } else {
                    tmpcol = 162
                }
                if (row > 1) {
                    tmprow = 29
                } else {
                    tmprow = 27
                }
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(95 + (columnSpace * tmpcol), 70 + (rowSpace * tmprow)).lineTo(133 + (columnSpace * tmpcol), 70 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };


        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.shape_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 551.3, 173.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 195, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 55, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p51_1();
        this.instance.setTransform(503, 60, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Skriv talen som saknas.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(23, 20);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(4, 20);

        this.innerRoundRect1 = new cjs.Shape();
        this.innerRoundRect1.graphics.f("#ffffff").s('#BCD378').drawRoundRect(27, 30, 175, 148, 10);
        this.innerRoundRect1.setTransform(0, 0);

        this.innerRoundRect2 = new cjs.Shape();
        this.innerRoundRect2.graphics.f("#ffffff").s('#F7B798').drawRoundRect(295, 30, 175, 148, 10);
        this.innerRoundRect2.setTransform(0, 0);

        var ToBeAdded = [];
        var tmpNum = ['', ' 4 ', ' 9 ', 14, 16, 17, '', ' 6 ', 10, 12, 15, 16]
        var xPos = 40;
        //Harizantal Line
        var temp_Line = new cjs.Shape();
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var row = 0; row < 6; row++) {
                var rowSpace = row;
                var numIndex = row;
                temp_Line.graphics.s("#9D9D9C").ss(.7).moveTo(37 + (columnSpace * 268), 57 + (rowSpace * 22)).lineTo(195 + (columnSpace * 268), 57 + (rowSpace * 22));
                if (col == 0) {
                    var temp_text = new cjs.Text(tmpNum[numIndex], "16px 'Myriad Pro'");
                } else {
                    var temp_text = new cjs.Text(tmpNum[numIndex + 6], "16px 'Myriad Pro'");
                }
                temp_text.setTransform(107 + ((columnSpace * 268)), 52 + (rowSpace * 22));
                ToBeAdded.push(temp_Line, temp_text);
            };
        };

        //vertical line
        var temp_Line1 = new cjs.Shape();
        var columnSpace = 0;
        for (var col = 0; col < 5; col++) {
            temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(95 + (columnSpace), 35).lineTo(97 + (columnSpace), 167);
            ToBeAdded.push(temp_Line1);
            if (col == 0) {
                columnSpace = 40;
            }
            if (col == 1) {
                columnSpace = 153
            }
            if (col == 2) {
                columnSpace = 270
            }
            if (col == 3) {
                columnSpace = 310
            }
        };

        this.text = new cjs.Text("2 färre                 2 fler", "16px 'Myriad Pro'");
        this.text.setTransform(45, 50);

        this.text_1 = new cjs.Text("3 färre                    3 fler", "16px 'Myriad Pro'");
        this.text_1.setTransform(310, 50);


        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2);
        this.addChild(this.innerRoundRect1, this.innerRoundRect2, this.text,this.text_1)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 551.3, 173.6);

    (lib.Symbol4 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 195, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 55, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p51_1();
        this.instance.setTransform(503, 60, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Rita symmetrilinjer.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(23, 20);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(4, 20);

        this.instance1 = new lib.p51_2();
        this.instance1.setTransform(40, 30, 0.46, 0.46);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.instance1, this.text_q1, this.text_q2, this.textbox_group1);



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 551.3, 173.6);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 109, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 308, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(309, 507, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);



        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
