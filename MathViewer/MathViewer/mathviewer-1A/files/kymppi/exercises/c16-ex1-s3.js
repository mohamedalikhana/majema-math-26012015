var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_2.png",
            id: "c14_ex1_s1_3"
        }, {
            src: "images/c16_ex1_s1_1.png",
            id: "c14_ex1_s1_1"
        }, {
            src: "images/c16_ex1_s1_2.png",
            id: "c14_ex1_s1_2"
        }]
    };



    (lib.c14_ex1_s1_1 = function() {
        this.initialize(img.c14_ex1_s1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c14_ex1_s1_2 = function() {
        this.initialize(img.c14_ex1_s1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c14_ex1_s1_3 = function() {
        this.initialize(img.c14_ex1_s1_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.BallStack = function(ball, count, scaleX, scaleY, ratio) {

        this.initialize();
        ballCloned = ball.clone();
        ballCloned.setTransform(0, 0, scaleX, scaleY);
        this.balls = [];
        for (var i = 0; i < count; i++) {
            var ballTemp = ballCloned.clone(true);
            ballTemp.setTransform(0, 0, 2, 2);
            this.addChild(ballTemp);
            this.balls.push(ballTemp);
        };
        //1000
        switch (count) {
            case 1:
                this.balls[0].setTransform(50 * ratio, 50 * ratio, scaleX, scaleY);
                break;
            case 2:
                this.balls[0].setTransform(20 * ratio, 50 * ratio, scaleX, scaleY);
                this.balls[1].setTransform(80 * ratio, 50 * ratio, scaleX, scaleY);
                break;
            case 3:
                this.balls[0].setTransform(20 * ratio, 50 * ratio, scaleX, scaleY);
                this.balls[1].setTransform(80 * ratio, 50 * ratio, scaleX, scaleY);
                this.balls[2].setTransform(50 * ratio, 0 * ratio, scaleX, scaleY);
                break;
            case 4:
                this.balls[0].setTransform(0 * ratio, 0 * ratio, scaleX, scaleY);
                this.balls[1].setTransform(100 * ratio, 0 * ratio, scaleX, scaleY);
                this.balls[2].setTransform(0 * ratio, 100 * ratio, scaleX, scaleY);
                this.balls[3].setTransform(100 * ratio, 100 * ratio, scaleX, scaleY);
                break;
            case 5:
                this.balls[0].setTransform(0, 0, scaleX, scaleY);
                this.balls[1].setTransform(100 * ratio, 0, scaleX, scaleY);
                this.balls[2].setTransform(0, 100 * ratio, scaleX, scaleY);
                this.balls[3].setTransform(50 * ratio, 50 * ratio, scaleX, scaleY);
                this.balls[4].setTransform(100 * ratio, 100 * ratio, scaleX, scaleY);
                break;
            case 6:
                this.balls[0].setTransform(10 * ratio, 0 * ratio, scaleX, scaleY);
                this.balls[1].setTransform(10 * ratio, 50 * ratio, scaleX, scaleY);
                this.balls[2].setTransform(10 * ratio, 100 * ratio, scaleX, scaleY);
                this.balls[3].setTransform(90 * ratio, 0 * ratio, scaleX, scaleY);
                this.balls[4].setTransform(90 * ratio, 50 * ratio, scaleX, scaleY);
                this.balls[5].setTransform(90 * ratio, 100 * ratio, scaleX, scaleY);

                break;
        }
        //this.setTransform(0,0,0.25,0.25);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Vi lär oss subtraktion", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("16", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();
        this.ball = new lib.c14_ex1_s1_3();

        this.dogImage1 = new lib.c14_ex1_s1_1();
        this.dogImage1.setTransform(760, -20,1,1);

        this.dogImage2 = new lib.c14_ex1_s1_2();
        this.dogImage2.setTransform(760, -20,1,1);


        var ballScaleX = 2.5,
            ballScaleY = 2.5;
        this.balls = new lib.BallStack(this.ball, 5, ballScaleX, ballScaleY, 4.5);
        this.balls.setTransform(260, 60, 0.30, 0.30, 0, 0, 0, 0, 0);



        this.instrTexts = [];
        this.instrTexts.push('Hunden tar 1 boll. Hur många är kvar?');
        this.instrTexts.push('Hur många?');
        this.instrTexts.push('Hur många kommer?');


        this.instrText1 = new cjs.Text(this.instrTexts[0], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(550 + 120, -40);
        this.instrText1.visible = false;

        this.instrText2 = this.instrText1.clone(true);
        this.instrText2.text = this.instrTexts[1];
        this.instrText3 = this.instrText1.clone(true);
        this.instrText3.text = this.instrTexts[2];

        this.answerText = new cjs.Text("4 bollar är kvar.", "40px 'Myriad Pro'");
        this.answerText.lineHeight = 19;
        this.answerText.textAlign = 'center';
        this.answerText.setTransform(380, 300);
        this.answerText.visible = false;

        this.addChild(this.answerText, this.instrText1, this.instrText2, this.instrText3, this.dogImage2, this.balls, this.dogImage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.tweens = [];
        var modX=1400,modY=480;
        this.tweens.push({
            ref: this.stage0.balls.balls[1],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.balls.balls[1].x,
                y: this.stage0.balls.balls[1].y
            },
            positionTo: {
                x: this.stage0.balls.balls[1].x + modX,
                y: this.stage0.balls.balls[1].y + modY
            },
            wait: 500,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
this.stage0.balls.balls[1].x=this.stage0.balls.balls[1].x+1400;
this.stage0.balls.balls[1].y=this.stage0.balls.balls[1].y+480;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });


        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
        this.stage0.answerText.visible = true;
        this.stage0.balls.balls[1].x=this.stage0.balls.balls[1].x+1400;
this.stage0.balls.balls[1].y=this.stage0.balls.balls[1].y+480;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.answerText,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });


        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
