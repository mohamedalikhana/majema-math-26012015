(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p43_1.png",
            id: "p43_1"
        }, {
            src: "images/p43_2.png",
            id: "p43_2"
        }]
    };

    (lib.p43_1 = function() {
        this.initialize(img.p43_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p43_2 = function() {
        this.initialize(img.p43_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol43_1 = function() {
        this.initialize();

        // Layer 1
        this.pageNoRight = new cjs.Text("43", "12px 'Myriad Pro'", "#FFFFFF");
        this.pageNoRight.lineHeight = 18;
        this.pageNoRight.setTransform(555, 648);
        this.pageNoRightBackground = new cjs.Shape();
        this.pageNoRightBackground.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.pageNoRightBackground.setTransform(579, 660.8);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med addition 0 till 6", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(380, 651);

        this.pageTitle = new cjs.Text("Öva addition", "24px 'MyriadPro-Semibold'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 26);

        this.chapterNumber = new cjs.Text("14", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(47.1, 22);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoRightBackground, this.pageNoRight, this.pageTitle, this.pageBottomText,
            this.chapterNumberBackground, this.chapterNumber);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol43_2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p43_1();
        this.instance.setTransform(0, 20, 0.6, 0.6);
        this.text = new cjs.Text("Hur många lådor är det tillsammans? ", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0.4);

        var textToBeDisplayed1 = ['1', '+', '1', '=', ' '];

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 0.4);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(345 + (i * 20), 127, 20, 22);
        }
        this.textBoxGroup6.setTransform(0, 0);

        this.textBoxGroup7 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup7.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(345 + (i * 20), 268, 20, 22);
        }
        this.textBoxGroup7.setTransform(0, 0);

        this.textBoxGroup8 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup8.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 127, 20, 22);
            this.labelTemp1 = new cjs.Text(textToBeDisplayed1[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp1.lineHeight = 40;
            this.labelTemp1.setTransform(78 + (i * 20), 117);
            textToBeDisplayed1[i] = this.labelTemp1;
        }
        this.textBoxGroup8.setTransform(0, 0);

        this.textBoxGroup9 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup9.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 268, 20, 22);
        }
        this.textBoxGroup9.setTransform(0, 0);

        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(4, 25, 252, 132, 10);
        this.img_Rect1.setTransform(0, 0);

        this.img_Rect2 = new cjs.Shape();
        this.img_Rect2.graphics.s("#D0CECE").ss(1).drawRoundRect(262, 25, 252, 132, 10);
        this.img_Rect2.setTransform(0, 0);

        this.img_Rect3 = new cjs.Shape();
        this.img_Rect3.graphics.s("#D0CECE").ss(1).drawRoundRect(262, 165.5, 252, 132, 10);
        this.img_Rect3.setTransform(0, 0);

        this.img_Rect4 = new cjs.Shape();
        this.img_Rect4.graphics.s("#D0CECE").ss(1).drawRoundRect(4, 165.5, 252, 132, 10);
        this.img_Rect4.setTransform(0, 0);

        this.addChild(this.instance, this.text, this.text_1, this.textBoxGroup1, this.textBoxGroup2,
            this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.textBoxGroup6,
            this.textBoxGroup7, this.textBoxGroup8, this.textBoxGroup9, this.img_Rect1, this.img_Rect2, this.img_Rect3, this.img_Rect4);
        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed1[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 300);


    (lib.Symbol43_3 = function() {
        this.initialize();

        this.instance = new lib.p43_2();
        this.instance.setTransform(0, 0, 0.6, 0.6);
        this.textBoxGroup1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            var space = 140;
            if (columnSpace > 1) {
                columnSpace = columnSpace - 0.2;
            }
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 246, 20, 22);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 107, 20, 22);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(345 + (i * 20), 246, 20, 22);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(345 + (i * 20), 107, 20, 22);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(4, 4.7, 252, 132, 10);
        this.img_Rect1.setTransform(0, 0);

        this.img_Rect2 = new cjs.Shape();
        this.img_Rect2.graphics.s("#D0CECE").ss(1).drawRoundRect(262, 4.7, 252, 132, 10);
        this.img_Rect2.setTransform(0, 0);

        this.img_Rect3 = new cjs.Shape();
        this.img_Rect3.graphics.s("#D0CECE").ss(1).drawRoundRect(4, 145, 252, 132, 10);
        this.img_Rect3.setTransform(0, 0);

        this.img_Rect4 = new cjs.Shape();
        this.img_Rect4.graphics.s("#D0CECE").ss(1).drawRoundRect(262, 145, 252, 132, 10);
        this.img_Rect4.setTransform(0, 0);


        this.addChild(this.instance, this.textBoxGroup1, this.label1, this.label2, this.label3, this.label4,
            this.label5, this.label6, this.textBoxGroup2, this.textBoxGroup3, this.textBoxGroup4, this.textBoxGroup5,
            this.img_Rect1, this.img_Rect2, this.img_Rect3, this.img_Rect4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 280);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol43_1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol43_2();
        this.v1.setTransform(308.7, 111, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol43_3();
        this.v2.setTransform(308.7, 397, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
