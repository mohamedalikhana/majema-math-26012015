(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p109_1.png",
            id: "p109_1"
        }, {
            src: "images/p109_2.png",
            id: "p109_2"
        }]
    };

    (lib.p109_1 = function() {
        this.initialize(img.p109_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p109_2 = function() {
        this.initialize(img.p109_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("109", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.text_1 = new cjs.Text("Formspelet", "18px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(57, 60);

        this.text_2 = new cjs.Text("Spel för 2.", "16px 'Myriad Pro'", "#D6326A");
        this.text_2.setTransform(57, 78);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#D6326A').drawRoundRect(0, 0, 200, 39, 10);
        this.roundRect1.setTransform(367, 49);

        this.text_3 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(375, 73);

        this.instance = new lib.p109_1();
        this.instance.setTransform(453, 47, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.roundRect1, this.text_3, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#9D9D9C').drawRoundRect(0, 0, 509, 539, 10);
        this.roundRect1.setTransform(0, 35);

        this.text_1 = new cjs.Text("• Välj en plats att starta från på spelplanen.", "16px 'Myriad Pro'", "#000000");
        this.text_1.setTransform(10, 60);

        this.text_2 = new cjs.Text("• Slå tärningen och gå lika många steg i pilens riktning.", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(10, 82);

        this.text_3 = new cjs.Text("• Sätt ett kryss vid formen du står på i protokollet.", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(10, 103);

        this.text_4 = new cjs.Text("• Den som först har kryssat en hel rad vid en form vinner.", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(10, 125);

        this.instance = new lib.p109_2();
        this.instance.setTransform(0, 150, 0.47, 0.47);

        this.innercircle = new cjs.Shape();
        this.innercircle.graphics.f("").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 166, 0, 2 * Math.PI);
        this.innercircle.setTransform(260, 360);

        this.outercircle = new cjs.Shape();
        this.outercircle.graphics.f("").s("#000000").ss(0.8, 0, 0, 4).arc(0, 0, 204, 0, 2 * Math.PI);
        this.outercircle.setTransform(260, 360);

        this.text_5 = new cjs.Text("Spelare 1", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(226, 250);

        this.text_6 = new cjs.Text("Spelare 2", "16px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(226, 367);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#000000').drawRect(0, 0, 240, 85, 0);
        this.Rect1.setTransform(139, 260);

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("").s('#000000').drawRect(0, 0, 240, 85, 0);
        this.Rect2.setTransform(139, 375);

        var hrlineArr = [];
        var hrlineX = [139, 139, 139, 139];
        var hrlineY = [288, 316, 402, 432];

        for (var row = 0; row < hrlineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(240, 0);
            hrLine_1.setTransform(hrlineX[row], hrlineY[row]);
            hrlineArr.push(hrLine_1);
        }

        var vrlineArr = [];
        var vrlineX = [180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360];
        var vrlineY = [260, 260, 260, 260, 260, 260, 260, 260, 260, 260, 375, 375, 375, 375, 375, 375, 375, 375, 375, 375];

        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 85);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            vrlineArr.push(vrLine_1);
        }

        this.Rectsquare = new cjs.Shape();
        this.Rectsquare.graphics.f("#00A3C4").s('#706F6F').drawRect(0, 0, 28, 14, 10);
        this.Rectsquare.setTransform(220, 169);

        this.Rectsquare1 = this.Rectsquare.clone(true);
        this.Rectsquare1.setTransform(368, 214);

        this.Rectsquare2 = this.Rectsquare.clone(true);
        this.Rectsquare2.setTransform(430, 377);

        this.Rectsquare3 = this.Rectsquare.clone(true);
        this.Rectsquare3.setTransform(402, 454);

        this.Rectsquare4 = this.Rectsquare.clone(true);
        this.Rectsquare4.setTransform(337, 515);

        this.Rectsquare5 = this.Rectsquare.clone(true);
        this.Rectsquare5.setTransform(245, 538);

        this.Rectsquare6 = this.Rectsquare.clone(true);
        this.Rectsquare6.setTransform(91, 252);

        this.Rectsquare7 = this.Rectsquare.clone(true);
        this.Rectsquare7.setTransform(125, 214);

        this.circle = new cjs.Shape();
        this.circle.graphics.f("#00A3C4").s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 9, 0, 2 * Math.PI);
        this.circle.setTransform(289, 177);

        this.circle1 = this.circle.clone(true);
        this.circle1.setTransform(414, 256);

        this.circle2 = this.circle.clone(true);
        this.circle2.setTransform(435, 296);

        this.circle3 = this.circle.clone(true);
        this.circle3.setTransform(434, 425);

        this.circle4 = this.circle.clone(true);
        this.circle4.setTransform(308, 539);

        this.circle5 = this.circle.clone(true);
        this.circle5.setTransform(141, 501);

        this.circle6 = this.circle.clone(true);
        this.circle6.setTransform(89, 431);

        this.circle7 = this.circle.clone(true);
        this.circle7.setTransform(76, 341);

        this.circle8 = this.circle.clone(true);
        this.circle8.setTransform(185, 192);

        this.triangle = new cjs.Shape();
        this.triangle.graphics.f("#00A3C4").s("#706F6F").ss(1.2).moveTo(20, 44).lineTo(40, 44).lineTo(30, 23).lineTo(20, 44);
        this.triangle.setTransform(308, 155);

        this.triangle1 = this.triangle.clone(true);
        this.triangle1.setTransform(416, 303);

        this.triangle2 = this.triangle.clone(true);
        this.triangle2.setTransform(358, 458);

        this.triangle3 = this.triangle.clone(true);
        this.triangle3.setTransform(181, 504);

        this.triangle4 = this.triangle.clone(true);
        this.triangle4.setTransform(143, 488);

        this.triangle5 = this.triangle.clone(true);
        this.triangle5.setTransform(82, 432);

        this.triangle6 = this.triangle.clone(true);
        this.triangle6.setTransform(47, 349);

        this.triangle7 = this.triangle.clone(true);
        this.triangle7.setTransform(54, 263);

        //Inside circle Shapes

        this.InnerRectsquare = new cjs.Shape();
        this.InnerRectsquare.graphics.f("#D6326A").s('#706F6F').drawRect(0, 0, 28, 14, 10);
        this.InnerRectsquare.setTransform(147, 267);

        this.Innertriangle = new cjs.Shape();
        this.Innertriangle.graphics.f("#D6326A").s("#706F6F").ss(1.2).moveTo(20, 44).lineTo(40, 44).lineTo(30, 23).lineTo(20, 44);
        this.Innertriangle.setTransform(130, 268);

        this.Innercircle = new cjs.Shape();
        this.Innercircle.graphics.f("#D6326A").s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 9, 0, 2 * Math.PI);
        this.Innercircle.setTransform(160, 330);

        this.InnerRectsquare1 = this.InnerRectsquare.clone(true);
        this.InnerRectsquare1.setTransform(147, 381);

        this.Innertriangle1 = this.Innertriangle.clone(true);
        this.Innertriangle1.setTransform(131, 383);

        this.Innercircle1 = this.Innercircle.clone(true);
        this.Innercircle1.setTransform(160, 445);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.instance,
            this.innercircle, this.outercircle, this.text_5, this.text_6, this.Rect1, this.Rect2,
            this.Rectsquare, this.Rectsquare1, this.Rectsquare2, this.Rectsquare3, this.Rectsquare4, this.Rectsquare5, this.Rectsquare6, this.Rectsquare7,
            this.circle, this.circle1, this.circle2, this.circle3, this.circle4, this.circle5, this.circle6, this.circle7, this.circle8,
            this.triangle, this.triangle1, this.triangle2, this.triangle3, this.triangle4, this.triangle5, this.triangle6, this.triangle7,
            this.InnerRectsquare, this.Innercircle, this.Innertriangle, this.InnerRectsquare1, this.Innercircle1, this.Innertriangle1);

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < vrlineArr.length; i++) {
            this.addChild(vrlineArr[i]);
        }

        var arcss = new cjs.Container();
        var distancedegree = [14.4, 13.7, 13, 13, 13.1, 13.3, 13.6, 13.5, 13.4, 13.7, 13.95, 14.1, 14.3, 14.5, 14.65, 14.7, 14.67, 14.63, 14.55, 14.54,
            14.42, 14.33, 14.33, 14.37, 14.35
        ];
        for (var i = 0; i < 25; i++) {
            var line = new cjs.Shape();
            line.graphics.f("").s("#000000").ss(1).moveTo(0, 166).lineTo(0, 204);
            line.setTransform(260, 360);
            line.rotation = 8 + i * distancedegree[i];
            arcss.addChild(line);

        };
        this.addChild(arcss);

        this.SideArrow = new cjs.Container();

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#00A3C4').s("#00A3C4").ss(6.2)
            .arc(1, 19, 360, Math.PI * (182) / 180, Math.PI * (192) / 180, false);
        this.shape_6.setTransform(295, 350, 0.7, 0.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#00A3C4').s("#00A3C4").ss(1.2).arc(0, 0, 20, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(20, 0);
        this.shape_7.setTransform(46, 325, 0.6, 0.6);
        this.shape_7.rotation = 240;

        this.SideArrow.addChild(this.shape_6, this.shape_7);
        this.SideArrow.setTransform(-5, 72, 0.8, 0.8);
        this.SideArrow.rotation = -3;

        this.SideArrow_1 = this.SideArrow.clone(true);
        this.SideArrow_1.setTransform(607, 182, 0.8, 0.8);
        this.SideArrow_1.rotation = 103;

        this.SideArrow_2 = this.SideArrow.clone(true);
        this.SideArrow_2.setTransform(600, 562, 0.8, 0.8);
        this.SideArrow_2.rotation = 160;

        this.addChild(this.SideArrow, this.SideArrow_1, this.SideArrow_2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 540);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(315, 100, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
