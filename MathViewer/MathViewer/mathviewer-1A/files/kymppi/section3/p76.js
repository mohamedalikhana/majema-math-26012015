(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.pageBottomText = new cjs.Text("förstå och kunna använda den kommutativa lagen i addition", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(71, 650);

        this.text_2 = new cjs.Text("76", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(37.5, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.2);

        this.text_3 = new cjs.Text("Kommutativa lagen i addition", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(95.5, 28.5);

        this.text_4 = new cjs.Text("26", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(46.5, 25);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5, 1, 1.1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_0 = new cjs.Text("Talen kan adderas i vilken ordning som helst utan att svaret ändras.", "16px 'Myriad Pro'");
        this.text_0.lineHeight = 19;
        this.text_0.setTransform(6, 8);

        this.text = new cjs.Text("4", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 40;
        this.text.setTransform(68, 96);

        this.text_1 = new cjs.Text("+", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(89, 96);

        this.text_2 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(108, 96);

        this.text_3 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(129, 96);

        this.text_4 = new cjs.Text("5", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(148, 96);

        this.text_5 = new cjs.Text("1", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(337, 96);

        this.text_6 = new cjs.Text("+", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 40;
        this.text_6.setTransform(358, 96);

        this.text_7 = new cjs.Text("4", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 40;
        this.text_7.setTransform(377, 96);

        this.text_8 = new cjs.Text("=", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 40;
        this.text_8.setTransform(398, 96);

        this.text_9 = new cjs.Text("5", "bold 36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 40;
        this.text_9.setTransform(417, 96);

        var colGrp = ['68', '378'];
        this.shape_group1 = new cjs.Shape();
        for (var colIndex = 0; colIndex < colGrp.length; colIndex++) {
            var rowStartVal = parseInt(colGrp[colIndex]);

            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                for (var row = 0; row < 2; row++) { // red ball
                    this.shape_group1.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(rowStartVal + (columnSpace * 25), 65 + (row * 23), 10, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        colGrp = [];
        colGrp = ['122', '346'];
        this.shape_group2 = new cjs.Shape();
        for (var colIndex = 0; colIndex < colGrp.length; colIndex++) {
            var rowStartVal = parseInt(colGrp[colIndex]);

            for (var column = 0; column < 1; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // blue ball
                    this.shape_group2.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(rowStartVal + (columnSpace * 25), 79 + (row * 23), 10, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        var xPos = 43,
            padding = 25,
            yPos = 106,
            rectWidth = 100,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;

        for (var i = 0; i < numberOfRects; i++) {
            if (i == 1) {
                padding = padding + 72;
            }
            var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
            this.textbox_group1.graphics.s("#949599").ss(0.4).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
            for (var j = 1; j < numberOfBoxes; j++) {
                this.textbox_group1.graphics.s("#949599").ss(0.4).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
            }
        }
        this.textbox_group1.setTransform(0, 0);


        // Block-1
        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(185.9, 37.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_37.setTransform(171.1, 37.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(155.9, 37.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_39.setTransform(140.3, 37.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(124.9, 37.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(109.2, 37.4);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(93.2, 37.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(77.7, 37.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(62.2, 37.3);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(46.6, 37.3);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_47.setTransform(187.2, 43.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_48.setTransform(187.2, 43.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_49.setTransform(172.5, 43.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_50.setTransform(172.5, 43.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_51.setTransform(94.5, 43.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_52.setTransform(94.5, 43.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_53.setTransform(157.4, 43.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_54.setTransform(157.4, 43.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_55.setTransform(79.3, 43.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_56.setTransform(79.3, 43.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_57.setTransform(141.7, 43.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_58.setTransform(141.7, 43.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_59.setTransform(63.6, 43.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_60.setTransform(63.6, 43.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_61.setTransform(126.2, 43.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_62.setTransform(126.2, 43.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_63.setTransform(48.2, 43.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_64.setTransform(48.2, 43.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_65.setTransform(110.5, 43.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_66.setTransform(110.5, 43.9);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(38, 36, 160, 98, 5);
        this.shape_69.setTransform(0, 0);

        // Block-2
        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_70.setTransform(454.9, 37.4);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_71.setTransform(440.1, 37.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_72.setTransform(424.9, 37.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_73.setTransform(409.3, 37.4);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_74.setTransform(393.9, 37.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_75.setTransform(378.2, 37.4);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(362.2, 37.4);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(346.7, 37.3);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(331.2, 37.3);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(315.6, 37.3);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_80.setTransform(456.2, 43.9);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_81.setTransform(456.2, 43.9);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_82.setTransform(441.5, 43.9);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_83.setTransform(441.5, 43.9);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_84.setTransform(363.5, 43.9);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_85.setTransform(363.5, 43.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_86.setTransform(426.4, 43.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_87.setTransform(426.4, 43.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_88.setTransform(348.3, 43.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_89.setTransform(348.3, 43.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_90.setTransform(410.7, 43.9);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_91.setTransform(410.7, 43.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_92.setTransform(332.6, 43.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_93.setTransform(332.6, 43.9);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_94.setTransform(395.2, 43.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_95.setTransform(395.2, 43.9);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_96.setTransform(317.2, 43.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_97.setTransform(317.2, 43.9);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_98.setTransform(379.5, 43.9);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_99.setTransform(379.5, 43.9);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(307, 36, 160, 98, 5);
        this.shape_100.setTransform(0, 0);

        // Main yellow block
        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gLDQgdAAgcAOQg5AdAABHIAASjIACARQAEAWAIASQAdA5BHAAMBL/AAAIASgCQAVgEASgIQA5gdAAhHIAAyjQAAgdgOgcQgdg5hHAAg");
        this.shape_115.setTransform(254.6, 70.8);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f("#FFF173").s().p("Egl/ALEQhHAAgdg5QgIgSgEgWIgCgRIAAyjQAAhHA5gcQAcgPAdAAMBL/AAAQBHAAAdA6QAOAbAAAdIAASjQAABHg5AcQgSAJgVADIgSADg");
        this.shape_116.setTransform(254.6, 70.8);

        this.addChild(this.shape_116, this.shape_115, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text);
        this.addChild(this.text_0, this.shape_group1, this.shape_group2, this.textbox_group1, this.text_6, this.text_7, this.text_8, this.text_9);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Skriv 2 olika additioner.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 9);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 9);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, ((743 * 0.5) - 14) * 0.7, ((260) - 80) * 0.7, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((712 * 0.5) + 7) * 0.7, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 157);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((712 * 0.5) + 7) * 0.7, 157);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 289);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((712 * 0.5) + 7) * 0.7, 289);

        var arrTxtbox = ['100', '130', '232', '262', '363', '393'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 43,
            padding = 25,
            yPos,
            rectWidth = 100,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 25;
                if (i == 1) {
                    padding = padding + 72;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("##707070").ss(0.4).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("##707070").ss(0.4).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 40;
        this.text.setTransform(68, 90);

        this.text_1 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(89, 90);

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(108, 90);

        this.text_3 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(129, 90);

        this.text_4 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(68, 120);

        this.text_5 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(89, 120);

        this.text_6 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 40;
        this.text_6.setTransform(108, 120);

        this.text_7 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 40;
        this.text_7.setTransform(129, 120);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(78, 79, 10, 0, 2 * Math.PI);
        this.shape_1.setTransform(0, 0);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(137, 79, 10, 0, 2 * Math.PI);
        this.shape_2.setTransform(0, 0);

        var rowVal = ['79', '212', '342'];
        this.shape_3 = new cjs.Shape();
        for (var i = 0; i < rowVal.length; i++) {
            var yPos = parseInt(rowVal[i]);

            this.shape_3.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(167, yPos, 10, 0, 2 * Math.PI);
        }
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(152, 55, 10, 0, 2 * Math.PI);
        this.shape_4.setTransform(0, 0);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(335, 79, 10, 0, 2 * Math.PI);
        this.shape_5.setTransform(0, 0);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(365, 79, 10, 0, 2 * Math.PI);
        this.shape_6.setTransform(0, 0);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(350, 55, 10, 0, 2 * Math.PI);
        this.shape_7.setTransform(0, 0);

        var rowGrp = ['79', '212', '316', '342'];

        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < rowGrp.length; i++) {
            var Height = parseInt(rowGrp[i]);

            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // blue ball
                    this.shape_group1.graphics.f("#0095DA").s("#6E6E70").ss(0.5, 0, 0, 4).arc(425 + (columnSpace * 29.5), Height + (row * 23), 10, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(335, 212, 10, 0, 2 * Math.PI);
        this.shape_8.setTransform(0, 0);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(365, 212, 10, 0, 2 * Math.PI);
        this.shape_9.setTransform(0, 0);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(350, 188, 10, 0, 2 * Math.PI);
        this.shape_10.setTransform(0, 0);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(304, 212, 10, 0, 2 * Math.PI);
        this.shape_11.setTransform(0, 0);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(321, 188, 10, 0, 2 * Math.PI);
        this.shape_12.setTransform(0, 0);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(77, 212, 10, 0, 2 * Math.PI);
        this.shape_13.setTransform(0, 0);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(107, 212, 10, 0, 2 * Math.PI);
        this.shape_14.setTransform(0, 0);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(92, 188, 10, 0, 2 * Math.PI);
        this.shape_15.setTransform(0, 0);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(46, 212, 10, 0, 2 * Math.PI);
        this.shape_16.setTransform(0, 0);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(63, 188, 10, 0, 2 * Math.PI);
        this.shape_17.setTransform(0, 0);

        var colGrp = ['46', '335'];
        this.shape_group2 = new cjs.Shape();
        for (var colIndex = 0; colIndex < colGrp.length; colIndex++) {
            var rowStartVal = parseInt(colGrp[colIndex]);

            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 2; row++) { // red ball
                    if (rowStartVal == '335' && row == 0) {
                        continue;
                    } else if (rowStartVal == '335' && row == 1 && column == 2) {
                        continue;
                    }
                    this.shape_group2.graphics.f("#DA2129").s("#6E6E70").ss(0.5, 0, 0, 4).arc(rowStartVal + (columnSpace * 30), 315 + (row * 26), 10, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group2.setTransform(0, 0);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.text_q1, this.text_q2);
        this.addChild(this.textbox_group1, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7);
        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_8, this.shape_9, this.shape_10, this.shape_11, this.shape_12);
        this.addChild(this.shape_5, this.shape_6, this.shape_7, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_group1);
        this.addChild(this.shape_group2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(295, 114, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(295, 260, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
