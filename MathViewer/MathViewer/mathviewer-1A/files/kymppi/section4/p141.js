(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p141_1.png",
            id: "p141_1"
        }, {
            src: "images/p122_2.png",
            id: "p122_2"
        }]
    };

    (lib.p141_1 = function() {
        this.initialize(img.p141_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p122_2 = function() {
        this.initialize(img.p122_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.SymbolClock = function() {
        this.initialize();

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#C8C9CB").s("#000000").ss(0.6, 0, 0, 4).arc(0, 0, 42, 0, 2 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#ffffff").s("#000000").ss(0.4, 0, 0, 4).arc(0, 0, 37.7, 0, 2 * Math.PI);
        this.shape_group2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(0, -38).lineTo(0, -35).moveTo(0, 35).lineTo(0, 38).moveTo(35, 0).lineTo(38, 0).moveTo(-35, 0).lineTo(-38, 0).moveTo(18, -33).lineTo(16.5, -30).moveTo(33, -19).lineTo(30, -17).moveTo(-30, 17.5).lineTo(-32.5, 19).moveTo(-17, 30).lineTo(-19, 32.5).moveTo(-31, -16).lineTo(-33.5, -18).moveTo(-19, -33).lineTo(-17, -30).moveTo(33, 17.5).lineTo(30.5, 16).moveTo(20, 31.5).lineTo(18.2, 29);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(10, -31);
        this.text_2 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(21.8, -20);
        this.text_3 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(27, -6);
        this.text_4 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(21.8, 10);
        this.text_5 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(10, 20);
        this.text_6 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(-4.5, 25);
        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(-19, 20);
        this.text_8 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(-31, 10);
        this.text_9 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(-36, -6);
        this.text_10 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(-32, -20);
        this.text_11 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(-20, -31);
        this.text_12 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(-7, -35.5);
        //center dot
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.lineHeight = 63;
        this.text_dot.setTransform(-6.5, -32);

        this.addChild(this.shape_group1, this.shape_group2, this.Line_1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_dot)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.SymbolClock_2 = function() {
        this.initialize();

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#C8C9CB").s("#000000").ss(0.6, 0, 0, 4).arc(0, 0, 42, 0, 2 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#ffffff").s("#000000").ss(0.4, 0, 0, 4).arc(0, 0, 37.7, 0, 2 * Math.PI);
        this.shape_group2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#000000").setStrokeStyle(0.7).moveTo(0, -38).lineTo(0, -35).moveTo(0, 35).lineTo(0, 38).moveTo(35, 0).lineTo(38, 0).moveTo(-35, 0).lineTo(-38, 0).moveTo(18, -33).lineTo(16.5, -30).moveTo(33, -19).lineTo(30, -17).moveTo(-30, 17.5).lineTo(-32.5, 19).moveTo(-17, 30).lineTo(-19, 32.5).moveTo(-31, -16).lineTo(-33.5, -18).moveTo(-19, -33).lineTo(-17, -30).moveTo(33, 17.5).lineTo(30.5, 16).moveTo(20, 31.5).lineTo(18.2, 29);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_1.lineHeight = 12;
        this.text_1.setTransform(10, -31);
        this.text_2 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(21.8, -20);
        this.text_3 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(27, -6);
        this.text_4 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(21.8, 10);
        this.text_5 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(10, 20);
        this.text_6 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(-4.5, 25);
        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(-19, 20);
        this.text_8 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(-31, 10);
        this.text_9 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(-36, -6);
        this.text_10 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(-32, -20);
        this.text_11 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(-20, -31);
        this.text_12 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(-7, -35.5);
        //center dot
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.lineHeight = 63;
        this.text_dot.setTransform(-6.5, -32);

        this.dotted = new lib.DottedCircle(30, 12);
        this.dotted.setTransform(0, 0, 0.18, 0.18);

        this.addChild(this.shape_group1, this.shape_group2, this.Line_1);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_dot, this.dotted)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("141", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(549, 642);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(576.3, 654.8);

        this.instance = new lib.p141_1();
        this.instance.setTransform(35, 20, 0.47, 0.46);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(466 + (columnSpace * 32), 22, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        //row-1
        this.instance_clock1 = new lib.SymbolClock();
        this.instance_clock1.setTransform(68, 47, 0.58, 0.58);
        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(65, 52.5, 0.8, 0.5, 206);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(68, 38.3, 0.8, 0.6);
        this.text_dot1 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot1.lineHeight = 32;
        this.text_dot1.setTransform(63, 27);

        this.instance_clock2 = new lib.SymbolClock();
        this.instance_clock2.setTransform(256, 47, 0.58, 0.58);
        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(249, 46.5, 0.8, 0.5, 90);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(256, 38.3, 0.8, 0.6);
        this.text_dot2 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot2.lineHeight = 32;
        this.text_dot2.setTransform(251, 27);

        this.instance_clock3 = new lib.SymbolClock();
        this.instance_clock3.setTransform(420, 47, 0.58, 0.58);
        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(417, 41.5, 0.8, 0.5, 155);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(420, 38.3, 0.8, 0.6);
        this.text_dot3 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot3.lineHeight = 32;
        this.text_dot3.setTransform(415, 27);
        //row-2
        this.instance_clock4 = new lib.SymbolClock();
        this.instance_clock4.setTransform(68, 172, 0.58, 0.58);
        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(72, 168.5, 0.8, 0.5, 58);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(68, 163.3, 0.8, 0.6);
        this.text_dot4 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot4.lineHeight = 32;
        this.text_dot4.setTransform(63, 152);

        this.instance_clock5 = new lib.SymbolClock();
        this.instance_clock5.setTransform(256, 172, 0.58, 0.58);
        this.shape_9 = new cjs.Shape(); // clock handle red
        this.shape_9.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_9.setTransform(259, 177.5, 0.8, 0.5, 150);
        this.shape_10 = new cjs.Shape(); // clock handle blue
        this.shape_10.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_10.setTransform(256, 163.3, 0.8, 0.6);
        this.text_dot5 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot5.lineHeight = 32;
        this.text_dot5.setTransform(251, 152);

        this.instance_clock6 = new lib.SymbolClock();
        this.instance_clock6.setTransform(420, 172, 0.58, 0.58);
        this.shape_11 = new cjs.Shape(); // clock handle red
        this.shape_11.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_11.setTransform(413, 176, 0.8, 0.5, 56);
        this.shape_12 = new cjs.Shape(); // clock handle blue
        this.shape_12.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_12.setTransform(420, 163.3, 0.8, 0.6);
        this.text_dot6 = new cjs.Text(".", "25px 'Myriad Pro'");
        this.text_dot6.lineHeight = 32;
        this.text_dot6.setTransform(415, 152);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
        this.addChild(this.instance_clock1, this.instance_clock2, this.instance_clock3, this.instance_clock4, this.instance_clock5, this.instance_clock6);
        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10, this.shape_11, this.shape_12, this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4, this.text_dot5, this.text_dot6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Rita visare.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 510, 334, 10);
        this.roundRect1.setTransform(0, 10);        

        // row-1
        this.instance_clock1 = new lib.SymbolClock_2();
        this.instance_clock1.setTransform(63, 89, 0.88, 0.88);
        this.instance_clock2 = new lib.SymbolClock_2();
        this.instance_clock2.setTransform(193, 89, 0.88, 0.88);
        this.instance_clock3 = new lib.SymbolClock_2();
        this.instance_clock3.setTransform(317, 89, 0.88, 0.88);
        this.instance_clock4 = new lib.SymbolClock_2();
        this.instance_clock4.setTransform(447, 89, 0.88, 0.88);
        this.text_1 = new cjs.Text("Klockan är 1.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 25;
        this.text_1.setTransform(19, 29);
        this.text_2 = new cjs.Text("Klockan är 3.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(147, 29);
        this.text_3 = new cjs.Text("Klockan är 5.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(275, 29);
        this.text_4 = new cjs.Text("Klockan är 7.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(404, 29);
        // // row-2   
        this.instance_clock5 = new lib.SymbolClock_2();
        this.instance_clock5.setTransform(63, 200, 0.88, 0.88);
        this.instance_clock6 = new lib.SymbolClock_2();
        this.instance_clock6.setTransform(193, 200, 0.88, 0.88);
        this.instance_clock7 = new lib.SymbolClock_2();
        this.instance_clock7.setTransform(317, 200, 0.88, 0.88);
        this.instance_clock8 = new lib.SymbolClock_2();
        this.instance_clock8.setTransform(447, 200, 0.88, 0.88);
        this.text_5 = new cjs.Text("Klockan är 4.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 25;
        this.text_5.setTransform(19, 140);
        this.text_6 = new cjs.Text("Klockan är 8.", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 25;
        this.text_6.setTransform(147, 140);
        this.text_7 = new cjs.Text("Klockan är 2.", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 25;
        this.text_7.setTransform(275, 140);
        this.text_8 = new cjs.Text("Klockan är 10.", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 25;
        this.text_8.setTransform(404, 140);
        // // row-3
        this.instance_clock9 = new lib.SymbolClock_2();
        this.instance_clock9.setTransform(63, 313, 0.88, 0.88);
        this.instance_clock10 = new lib.SymbolClock_2();
        this.instance_clock10.setTransform(193, 313, 0.88, 0.88);
        this.instance_clock11 = new lib.SymbolClock_2();
        this.instance_clock11.setTransform(317, 313, 0.88, 0.88);
        this.instance_clock12 = new lib.SymbolClock_2();
        this.instance_clock12.setTransform(447, 313, 0.88, 0.88);
        this.text_9 = new cjs.Text("Klockan är 6.", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 25;
        this.text_9.setTransform(19, 253);
        this.text_10 = new cjs.Text("Klockan är 9.", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 25;
        this.text_10.setTransform(147, 253);
        this.text_11 = new cjs.Text("Klockan är 11.", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 25;
        this.text_11.setTransform(275, 253);
        this.text_12 = new cjs.Text("Klockan är 12.", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 25;
        this.text_12.setTransform(404, 253);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(127, 23).lineTo(127, 357).moveTo(255, 23).lineTo(255, 357).moveTo(382, 23).lineTo(382, 357).moveTo(0, 134).lineTo(510, 134).moveTo(0, 246).lineTo(510, 246);
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.text_q1, this.text_q2, this.roundRect1, this.instance_clock1, this.instance_clock2, this.instance_clock3, this.instance_clock4, this.instance_clock5, this.instance_clock6, this.instance_clock7, this.instance_clock8, this.instance_clock9, this.instance_clock10, this.instance_clock11, this.instance_clock12);
        this.addChild(this.hrLine_1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 325);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 345, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(313, 496, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
