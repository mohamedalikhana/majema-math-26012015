var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: []
    };

    (lib.Stage1_1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#19863A').ss(3).drawRoundRect(210, 0, 900, 200, 13);
        this.roundRect1.setTransform(0, 0);

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#19863A").s("#19863A").ss(0.5, 0, 0, 4).arc(210, 0, 30, 0, 30 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.text_No = new cjs.Text("6", "35px 'MyriadPro-Semibold'", "#ffffff");
        this.text_No.lineHeight = 19;
        this.text_No.setTransform(200, 10);

        this.textRect = new cjs.Shape();
        this.textRect.graphics.f("#ffffff").s('#19863A').ss(3).drawRoundRect(330, -25, 340, 50, 19);
        this.textRect.setTransform(0, 0);

        this.text_1 = new cjs.Text("PROGRAMMERING", "bold 28px 'Myriad Pro'", "#19863A");
        this.text_1.setTransform(380, 12);

        this.text_2 = new cjs.Text("•  kunna tolka enkla koder", "35px 'Myriad Pro'", "#19863A");
        this.text_2.setTransform(330, 100);      

        this.addChild(this.roundRect1, this.shape_group1, this.text_No, this.textRect, this.text_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 305.4, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var stage1_1 = new lib.Stage1_1();
        stage1_1.setTransform(0, 30);

        this.addChild(stage1_1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)


        this.addChild(this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
