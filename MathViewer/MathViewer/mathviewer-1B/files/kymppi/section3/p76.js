(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p76_1.png",
            id: "p76_1"
        }, {
            src: "images/p76_2.png",
            id: "p76_2"
        }]
    };

    (lib.p76_1 = function() {
        this.initialize(img.p76_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p76_2 = function() {
        this.initialize(img.p76_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("26", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(63, 42);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#958FC5").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(56, 23.5, 1.09, 1);

        this.text_2 = new cjs.Text("Vi övar", "24px 'MyriadPro-Semibold'", "#958FC5");
        this.text_2.setTransform(113.5, 42);

        this.text_3 = new cjs.Text("kunna lösa uppgifter i subtraktion 0 till 20, med tiotalsövergång", "9px 'Myriad Pro'");
        this.text_3.setTransform(67, 656);

        this.text = new cjs.Text("76", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28, 660.8, 1, 1.1);

        this.instance = new lib.p76_1();
        this.instance.setTransform(60, 0, 0.47, 0.46);

        this.addChild(this.instance, this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.text_3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många poäng fick Manuel?", "16px 'Myriad Pro'");
        this.text_q2.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(0, 0);

        this.instance_2 = new lib.p76_2();
        this.instance_2.setTransform(10, 23, 0.47, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 160, 10);
        this.roundRect1.setTransform(0, 5);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 5);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 172);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 172);



        var ToBeAdded = [];
        var xpos = 60;
        var ypos = 138;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 2; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 305
            } else {
                ypos = 138
            }

            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 6; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 325
                        } else {
                            xpos = 60
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 32), 21, 23);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }



        var tmpValtop = "Jag fick";
        var tmpValbottom = "";
        var ypos = 48;
        var xposTop=35;
        var xposBotm=30;
        for (var row = 0; row < 2; row++) {
            var rowSpace = row;
            for (var column = 0; column < 4; column++) {
                var colSpace = column;
                var tempText = new cjs.Text(tmpValtop, "16px 'Myriad Pro'");
                if(column>1){xposTop=70} else {xposTop=35}
                 tempText.setTransform(xposTop + (115 * colSpace), ypos + (166 * rowSpace));
                 if(row==0 && column==0){tmpValbottom="7 poäng."}
                 else if(row==0 && column==1){tmpValbottom="5 fler."}
                 else if(row==0 && column==2){tmpValbottom="8 poäng."}
                 else if(row==0 && column==3){tmpValbottom="7 fler."}
                 else if(row==1 && column==0){tmpValbottom="11 poäng."}
                 else if(row==1 && column==1){tmpValbottom="6 färre."}
                 else if(row==1 && column==2){tmpValbottom="13 poäng."}
                 else if(row==1 && column==3){tmpValbottom="8 färre."}
                var tempText1 = new cjs.Text(tmpValbottom, "16px 'Myriad Pro'");
                if(column>1){xposBotm=43} else {xposBotm=30}
                tempText1.setTransform(xposBotm + (125 * colSpace), (ypos+17) + (166 * rowSpace));
                ToBeAdded.push(tempText, tempText1);
            }
        }

        var tmpNum = ['7', '+', '5', '=', '', '1', '1', '–', '6', '=']
        var j = 0;
        for (var row = 0; row < 2; row++) {
            rowspace = row;
            for (var col = 0; col < 5; col++) {
                columnSpace = col
                var tempText = new cjs.Text(tmpNum[j], "36px 'UusiTekstausMajema'", "#6C7373");
                tempText.setTransform(65 + (columnSpace * 21), 160.5 + (rowspace * 167));
                ToBeAdded.push(tempText);
                j++
            }
        }

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        this.addChild(this.instance_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(298, 510, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
