(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

            src: "images/p129_1.png",
            id: "p129_1"
        }, {
            src: "images/p129_2.png",
            id: "p129_2"
        }]
    };

    (lib.p129_1 = function() {
        this.initialize(img.p129_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p129_2 = function() {
        this.initialize(img.p129_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("126", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.heart = function(heartText, answerText, isTextLeft) {
        this.initialize();
      
        var textPosition = {
            x1: 15,
            y1: 40,
            x2: 56,
            y2: 42,
            underlineX: 54,
            underlineY: 42
        };;

        if (!isTextLeft) {
            textPosition = {
                x1: 40,
                y1: 40,
                x2: 15,
                y2: 42,
                underlineX: 15,
                underlineY: 42
            };
        }


        this.ellip = new cjs.Shape();
        this.ellip.graphics.f('').s('#000000').drawEllipse(200, 100, 48 / 1.13, 43 / 1.13);

        this.heartImage = new lib.p129_1();
        this.heartImage.setTransform(0, 0, 0.47, 0.47);

        this.txt_group = new cjs.Container();

        this.text_2 = new cjs.Text(heartText, "16px 'Myriad Pro'");
        this.text_2.setTransform(textPosition.x1, textPosition.y1);

        this.text_3 = new cjs.Text(answerText, "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_3.setTransform(textPosition.x2, textPosition.y2);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 0).lineTo(21, 0);
        this.hrLine_1.setTransform(textPosition.underlineX, textPosition.underlineY);

        this.txt_group.addChild(this.text_2, this.hrLine_1);
        if (answerText) {
            this.txt_group.addChild(this.text_3);
        }
        this.txt_group.setTransform(0, 0);

        this.addChild(this.heartImage, this.txt_group);

        //heart ends
    }).prototype = p = new cjs.Container();

    (lib.ellips = function(ellipText, bordercolor) {
        this.initialize();

        var textPosition = {
            x1: 20,
            y1: 25,

        };;

        this.ellip = new cjs.Shape();
        this.ellip.graphics.f('').s(bordercolor).drawEllipse(0, 0, 48 / 1.13, 43 / 1.13);

        this.txt_group = new cjs.Container();

        this.text_2 = new cjs.Text(ellipText, "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(textPosition.x1, textPosition.y1);
        this.text_2.textAlign = 'center';

        this.txt_group.addChild(this.text_2);

        this.txt_group.setTransform(0, 0);

        this.addChild(this.ellip, this.txt_group);

        //home ends
    }).prototype = p = new cjs.Container();

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Fortsätt talföljden.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 513, 259, 10);
        this.roundRect1.setTransform(0, 12);
      

        var bordercolor = ['#D51317', '#0085CD', '#13A538', '#8F96CB'];
        var txtArr = [];
        txtArr[0] = '5';
        txtArr[4] = '10';
        txtArr[8] = '15';
        txtArr[1] = '50';
        txtArr[5] = '55';
        txtArr[9] = '60';
        txtArr[2] = '100';
        txtArr[6] = '98';
        txtArr[10] = '96';
        txtArr[3] = '80';
        txtArr[7] = '75';
        txtArr[11] = '70';


        var startX = 40,
            startY = 40,i=0;
        for (var col = 0; col < 10; col++) {
            for (var row = 0; row < 4; row++) {

                var ellip = new lib.ellips(txtArr[i], bordercolor[row]);
                ellip.setTransform(startX + col * 43, startY + row * 55)
                this.addChild(ellip);
                i=i+1;
            };

        };

        this.addChild(this.text, this.text_1, this.roundRect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 527.3, 265);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Titta på regeln och skriv talet som kommer ut ur maskinen.", "16px 'Myriad Pro'");
        this.text.setTransform(14, 0);

        this.text_1 = new cjs.Text("7.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("Maskin 1", "bold 16px 'Myriad Pro'");
        this.text_2.setTransform(55, 40);

        this.text_3 = new cjs.Text("Maskin 2", "bold 16px 'Myriad Pro'");
        this.text_3.setTransform(227, 40);

        this.text_4 = new cjs.Text("Maskin 3", "bold 16px 'Myriad Pro'");
        this.text_4.setTransform(397, 40);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 513, 240, 10);
        this.roundRect1.setTransform(0, 12);

        var ToBeAdded = [];
        var moveToxposition = 175,
            moveToyposition1 = 45,
            moveToyposition2 = 150;

        for (var col = 0; col < 2; col++) {
            if (col == 1) {
                moveToyposition1 = 45;
                moveToyposition2 = 235;
            } else {
                moveToyposition1 = 45;
                moveToyposition2 = 235;
            }
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(moveToxposition + (col * 170), moveToyposition1).lineTo(moveToxposition + (col * 170), moveToyposition2);
            ToBeAdded.push(hrLine)
        };

        //container

        this.img_group = new cjs.Container();

        this.leftarrow = new cjs.Shape();
        this.leftarrow.graphics.f("#000000").s("#000000").setStrokeStyle(1).moveTo(0, 40).lineTo(9, 40).lineTo(9, 37).lineTo(19, 40)
            .lineTo(9, 43).lineTo(9, 40);
        this.leftarrow.setTransform(39, 50);

        this.instance = new lib.p129_2();
        this.instance.setTransform(56, 52, 0.46, 0.47);
        this.img_group.addChild(this.instance);

        this.rightarrow = this.leftarrow.clone(true);
        this.rightarrow.setTransform(121, 50);

        var TxtBoxArr = [];
        var boxcount = 6;
        var TxtBox_X = [17, 140, 37, 37, 37, 37];
        var TxtBox_Y = [78, 78, 129, 156, 183, 209];

        for (var col = 0; col < boxcount; col++) {

            var boxfillcolor = "#FFFFFF";
            var boxborder = "#000000";
            var boxheight = 22;
            var boxwidth = 22;

            if (col > 1) {
                boxfillcolor = "#FFFFFF";
                boxborder = "#00A3C4";
                boxheight = 23;
                boxwidth = 26;
            } else {
                boxfillcolor = "#FFFFFF";
                boxborder = "#000000";
                // boxheight = 22;
                boxwidth = 22;
            }

            var TxtBox_1 = new cjs.Shape();
            TxtBox_1.graphics.f(boxfillcolor).s(boxborder).ss(0.7).drawRoundRect(0, 0, boxwidth, boxheight, 0);
            TxtBox_1.setTransform(TxtBox_X[col], TxtBox_Y[col]);
            this.img_group.addChild(TxtBox_1);
        }


        var arrowArr = [];
        var arrowX = [79, 79, 79, 79];
        var arrowY = [102, 128, 155, 180];

        for (var row = 0; row < arrowX.length; row++) {
            var arrow_1 = new cjs.Shape();
            arrow_1.graphics.f("#000000").s("#000000").setStrokeStyle(1).moveTo(0, 40).lineTo(10, 40).lineTo(10, 37).lineTo(13, 40)
                .lineTo(10, 43).lineTo(10, 40);
            arrow_1.setTransform(arrowX[row], arrowY[row]);
            this.img_group.addChild(arrow_1);
        }

        var lineArr = [];
        var lineX = [109, 109, 109, 109];
        var lineY = [112, 138, 165, 190];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.f("#000000").s("#000000").setStrokeStyle(0.7).moveTo(0, 40).lineTo(27, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            this.img_group.addChild(hrLine_1);
        }
        this.img_group.addChild(this.leftarrow, this.rightarrow);

        this.img_group.setTransform(0, 0);

        this.img_group2 = this.img_group.clone(true);
        this.img_group2.setTransform(170, 0);

        this.img_group3 = this.img_group.clone(true);
        this.img_group3.setTransform(340, 0);

        var TArr = ['12', '+ 2', '14', '5', '+ 5', '10', '0', '+ 6', '6',
            '15', '25', '35', '45', '2', '7', '8', '9', '4', '8', '7', '9'
        ];

        var TxtlineX = [18, 80, 143, 193, 253, 312, 363, 422, 487,
            41, 41, 41, 41, 215, 215, 215, 215, 385, 385, 385, 385
        ];

        var TxtlineY = [95, 95, 95, 95, 95, 95, 95, 95, 95,
            145, 172, 199, 225, 145, 172, 199, 225, 145, 172, 199, 225
        ];

        var TxtArr = [];
        for (var i = 0; i < TxtlineX.length; i++) {
            var temp_label3;
            if (i == 1 || i == 4 | i == 7) {
                temp_label3 = new cjs.Text(TArr[i] + '', "bold 15px 'Myriad Pro'", "#000000");
            } else {
                temp_label3 = new cjs.Text(TArr[i] + '', "16px 'Myriad Pro'", "#000000");
            }

            temp_label3.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(temp_label3);
        }

        this.addChild(this.text, this.text_1, this.text_2, this.roundRect1, this.text_2, this.text_3, this.text_4);
        this.addChild(this.img_group, this.img_group2, this.img_group3, this.leftarrow, this.rightarrow);

        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 529, 250);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 109, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300, 439, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
