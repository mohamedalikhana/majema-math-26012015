(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p126_1.png",
            id: "p126_1"
        }, {
            src: "images/p126_2.png",
            id: "p126_2"
        }]
    };

    // symbols:
    (lib.p126_1 = function() {
        this.initialize(img.p126_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p126_2 = function() {
        this.initialize(img.p126_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("126", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(36, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.5, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 515, 300, 10);
        this.roundRect1.setTransform(0, 25);

        this.text = new cjs.Text("Vad kostar sakerna?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1);

        this.text_q1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 1);

        this.instance = new lib.p126_1();
        this.instance.setTransform(24, 62, 0.47, 0.47);

        this.text_h2 = new cjs.Text("Kronor från början", "15px 'MyriadPro-Semibold'");
        this.text_h2.lineHeight = 19;
        this.text_h2.setTransform(31, 35);

        this.text_h3 = new cjs.Text("Handlar", "15px 'MyriadPro-Semibold'");
        this.text_h3.lineHeight = 19;
        this.text_h3.setTransform(240, 35);

        this.text_h4 = new cjs.Text("Kronor kvar", "15px 'MyriadPro-Semibold'");
        this.text_h4.lineHeight = 19;
        this.text_h4.setTransform(397, 35);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(0, 54.5).lineTo(515, 54.5).moveTo(0, 142.5).lineTo(515, 142.5)
            .moveTo(0, 233.5).lineTo(515, 233.5);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(179, 25).lineTo(179, 324).moveTo(360, 25).lineTo(360, 324);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(286, 127).lineTo(343, 127).moveTo(286, 218).lineTo(343, 218)
            .moveTo(286, 309).lineTo(343, 309);
        this.hrLine_3.setTransform(0, 0);

        this.text_1 = new cjs.Text("kr", "12px 'Myriad Pro'");
        this.text_1.lineHeight = 25;
        this.text_1.setTransform(331, 115);
        this.text_2 = new cjs.Text("kr", "12px 'Myriad Pro'");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(331, 207);
        this.text_3 = new cjs.Text("kr", "12px 'Myriad Pro'");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(331, 297);

        this.addChild(this.roundRect1, this.text, this.text_q1, this.instance, this.hrLine_1, this.hrLine_2, this.hrLine_3);
        this.addChild(this.text_h2, this.text_h3, this.text_h4, this.text_1, this.text_2, this.text_3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 310);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många likadana armband kan du göra av pärlorna?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(19, 1);

        this.text_q1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 1);

        this.instance = new lib.p126_2();
        this.instance.setTransform(12, 44, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 24, 515, 220, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#BBD992').drawRoundRect(38, 38, 181.5, 82, 7);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#BBD992').drawRoundRect(277, 38, 219, 82, 7);
        this.roundRect3.setTransform(0, 0);

        // row-1
        this.text_1 = new cjs.Text("armband", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 25;
        this.text_1.setTransform(114, 210);
        this.text_2 = new cjs.Text("armband", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(369, 210);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(255, 36).lineTo(255, 230);
        this.hrLine_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                var rowSpace = row;
                this.textbox_group1.graphics.f('#ffffff').s("#6D6E70").ss(0.8).drawRect(89.5 + (columnSpace * 255), 206.5 + (rowSpace * 32.5), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text_q1, this.roundRect1, this.roundRect2, this.roundRect3);
        this.addChild(this.instance, this.textbox_group1, this.text_1, this.text_2, this.hrLine_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(298, 111, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(298, 430, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
