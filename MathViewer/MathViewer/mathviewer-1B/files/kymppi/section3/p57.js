(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p57_1.png",
            id: "p57_1"
        }]
    };

    (lib.p57_1 = function() {
        this.initialize(img.p57_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("kunna lösa uppgifter i subtraktion 0 till 20, med tiotalsövergång", "9px 'Myriad Pro'");
        this.text_1.setTransform(298.5, 657);

        this.text_2 = new cjs.Text("57", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(553, 657);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Subtrahera från talen 11 och 12", "24px 'MyriadPro-Semibold'", "#958FC5");
        this.text_3.setTransform(95.5, 42);

        this.text_4 = new cjs.Text("19", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(49, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#958FC5").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();


        // Block-1
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_1.setTransform(255.9, 18.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_2.setTransform(240.1, 18.4);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_3.setTransform(225.9, 18.4);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_4.setTransform(210.3, 18.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_5.setTransform(195.9, 18.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_6.setTransform(180.2, 18.4);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_7.setTransform(165.2, 18.4);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_8.setTransform(150.7, 18.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_9.setTransform(135.2, 18.3);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_10.setTransform(120.6, 18.3);
        //-----------------------------
        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_11.setTransform(105.6, 18.3);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_12.setTransform(90.6, 18.3);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_13.setTransform(75, 18.3);
        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_14.setTransform(60.6, 18.3);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_15.setTransform(45.6, 18.3);


        //------------------------------------

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_16.setTransform(255.2, 26.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_17.setTransform(255.2, 26.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_18.setTransform(240.5, 26.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_19.setTransform(240.5, 26.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_20.setTransform(225.5, 26.9);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_21.setTransform(225.5, 26.9);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_22.setTransform(210.4, 26.9);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_23.setTransform(210.4, 26.9);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_24.setTransform(195.3, 26.9);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_25.setTransform(195.3, 26.9);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_26.setTransform(180.7, 26.9);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_27.setTransform(180.7, 26.9);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_28.setTransform(165.6, 26.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_29.setTransform(165.6, 26.9);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_30.setTransform(150.2, 26.9);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_31.setTransform(150.2, 26.9);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_32.setTransform(135.2, 26.9);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_33.setTransform(135.2, 26.9);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_34.setTransform(120.5, 26.9);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_35.setTransform(120.5, 26.9);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_36.setTransform(105.5, 26.9);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_37.setTransform(105.5, 26.9);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_38.setTransform(90.5, 26.9);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_39.setTransform(90.5, 26.9);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_40.setTransform(75.5, 26.9);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_41.setTransform(75.5, 26.9);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_42.setTransform(60.5, 26.9);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_43.setTransform(60.5, 26.9);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_44.setTransform(45.5, 26.9);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_45.setTransform(45.5, 26.9);

        // white block
        var shadow = new cjs.Shadow("#7d7d7d", 0, 0, 4);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(28, 19, 245, 96, 5);
        this.shape_69.shadow = shadow.clone(true);
        this.shape_69.setTransform(0, 0);

        //Main yellow block

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f("#FFF173").s("").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 509, 134, 10);
        this.shape_116.shadow = shadow.clone(true);
        this.shape_116.setTransform(0, 0);

        //draw boxs1
        var ToBeAdded = [];
        var xpos = 40;
        var ypos = 42;
        var tmp_Rect = new cjs.Shape();
        var tmp_line1 = new cjs.Shape();
        var linexpos = 62;
        var lineToxos = 78;
        for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
            for (var row = 0; row < 2; row++) {
                for (var column = 0; column < 5; column++) {
                    var columnSpace = column;
                    if (numOfcol == 1) {
                        xpos = 160
                    } else {
                        xpos = 40
                    }

                    tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 20), ypos + (row * 20), 20, 21);
                    ToBeAdded.push(tmp_Rect);
                }
            }
        }

        this.shape_group1 = new cjs.Shape();
        var xpos = 50;
        var ypos = 52;
        var fillcolor = "#D51317";

        for (var column = 0; column < 6; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    ypos = 58;
                    xpos = 50;
                }
                if (row == 0) {
                    ypos = 52;
                    xpos = 50;
                }
                if (column == 5 && row == 1) {
                    continue
                }
                if (column == 5) {
                    xpos = 70
                } else {
                    xpos = 50
                }

                this.shape_group1.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group1.setTransform(0, 0);
        ToBeAdded.push(this.shape_group1);

        tmp_line1 = new cjs.Shape();
        linexpos = 62;
        lineToxos = 78;
        for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
            for (var row = 0; row < 2; row++) {
                for (var column = 0; column < 5; column++) {
                    var columnSpace = column;
                    if (column == 4) {
                        linexpos = 81;
                        lineToxos = 97
                    } else {
                        linexpos = 62;
                        lineToxos = 78
                    }
                    tmp_line1.graphics.s("#9D9D9C").ss(.9).moveTo(linexpos + (columnSpace * 20), 61).lineTo(lineToxos + (columnSpace * 20), 45);
                    ToBeAdded.push(tmp_line1);
                }
            }
        }

        this.text_1 = new cjs.Text("11 – 5 = 6", "16px 'Myriad Pro'");
        this.text_1.setTransform(110, 105);

        this.instance = new lib.p57_1();
        this.instance.setTransform(305, 22, 0.48, 0.48);

        this.text_2 = new cjs.Text("Ta först bort det", "14px 'Myriad Pro'");
        this.text_2.setTransform(323, 45);

        this.text_3 = new cjs.Text("ensamma entalet.", "14px 'Myriad Pro'");
        this.text_3.setTransform(323, 65);


        this.addChild(this.shape_116, this.shape_69, this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10);
        this.addChild(this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_19, this.shape_20);
        this.addChild(this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.shape_25, this.shape_26, this.shape_27, this.shape_28, this.shape_29, this.shape_30);
        this.addChild(this.shape_31, this.shape_32, this.shape_33, this.shape_34, this.shape_35, this.shape_36, this.shape_37, this.shape_38, this.shape_39, this.shape_40);
        this.addChild(this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.text_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

        this.addChild(this.instance, this.text_2, this.text_3, this.textbox_group1, this.textbox_group3, this.textbox_group4);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 252.7, 94, 10);
        this.roundRect1.setTransform(0, 10);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 110);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 210);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(0, 310);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(257, 10);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(257, 110);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(257, 210);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(257, 310);

        var ToBeAdded = [];
        var xpos = 15;
        var ypos = 25;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 4; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 125
            } else if (numOfrow == 2) {
                ypos = 225
            } else if (numOfrow == 3) {
                ypos = 325
            } else {
                ypos = 25
            }
            for (var numOfcol = 0; numOfcol < 4; numOfcol++) {
                for (var row = 0; row < 2; row++) {
                    for (var column = 0; column < 5; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 138
                        } else if (numOfcol == 2) {
                            xpos = 272
                        } else if (numOfcol == 3) {
                            xpos = 393
                        } else {
                            xpos = 15
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 20), ypos + (row * 20), 20, 21);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }


        var tmp_arc = new cjs.Shape();
        var xpos = 25;
        var ypos = 35;
        var fillcolor = "#D51317"
        for (var numOfrow = 0; numOfrow < 4; numOfrow++) {
            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var column = 0; column < 6; column++) {
                    var columnSpace = column;
                    for (var row = 0; row < 2; row++) {
                        if (row == 0 && column == 5 && numOfcol == 0) {
                            xpos = 48
                        }
                        if (numOfcol == 1) {
                            xpos = 282
                        }
                        if (row == 0 && column == 5 && numOfcol == 1) {
                            xpos = 303
                        }
                        if (row == 1 && column == 5) {
                            continue
                        }

                        if (numOfrow == 1) {
                            ypos = 135;
                            xpos = 25
                        }
                        if (numOfrow == 1 && numOfcol == 1) {
                            ypos = 135;
                            xpos = 282
                        }
                        if (numOfrow == 2) {
                            ypos = 235;
                            xpos = 25
                        }
                        if (numOfrow == 2 && numOfcol == 1) {
                            ypos = 235;
                            xpos = 282
                        }
                        if (numOfrow == 3) {
                            ypos = 335;
                            xpos = 25
                        }
                        if (numOfrow == 3 && numOfcol == 1) {
                            ypos = 335;
                            xpos = 282
                        }

                        if (row == 0 && column == 5 && numOfcol == 0 && numOfrow == 1) {
                            xpos = 48
                        }
                        if (row == 0 && column == 5 && numOfcol == 0 && numOfrow == 2) {
                            xpos = 48
                        }
                        if (row == 0 && column == 5 && numOfcol == 0 && numOfrow == 3) {
                            xpos = 48
                        }

                        if (row == 0 && column == 5 && numOfcol == 1 && numOfrow == 1) {
                            xpos = 303
                        }
                        if (row == 0 && column == 5 && numOfcol == 1 && numOfrow == 2) {
                            xpos = 303
                        }
                        if (row == 0 && column == 5 && numOfcol == 1 && numOfrow == 3) {
                            xpos = 303
                        }
                        tmp_arc.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 20), 7, 0, 2 * Math.PI);
                        ToBeAdded.push(tmp_arc);
                    }
                }
            }
        }


        var arryVal = ["11  –  2 =", "11  –  3 =", "11  –  7 =", "11  –  8 =", "11  –  4 =", "11  –  5 =", "11  –  6 =", "11  – 9 ="];
        var tmprow = 20;
        var tmpcol = 312;
        var i = 0;
        var temp_Line1 = new cjs.Shape();
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(arryVal[i], "16px 'Myriad Pro'");
                tempText.setTransform(81+ (258 * columnSpace), 90 + (100 * rowSpace));
                if (col == 1) {
                    tmpcol = 258
                } else if (col == 2) {
                    tmpcol = 165
                } else {
                    tmpcol = 162
                }
                if (row > 0) {
                    tmprow = 100
                } else {
                    tmprow = 20
                }
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(144 + (columnSpace * tmpcol), 95 + (rowSpace * tmprow)).lineTo(177 + (columnSpace * tmpcol), 95 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;
            };
        };

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.text_q1, this.text_q2)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513,410);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(305, 114, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(305, 287, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
