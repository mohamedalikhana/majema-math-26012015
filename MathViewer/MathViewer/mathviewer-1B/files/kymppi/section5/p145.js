(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p145_1.png",
            id: "p145_1"
        }, {
            src: "images/p145_2.png",
            id: "p145_2"
        }]
    };

    (lib.p145_1 = function() {
        this.initialize(img.p145_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p145_2 = function() {
        this.initialize(img.p145_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("145", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 655);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_1.setTransform(0, 0);

        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.shape_2, this.shape_1, this.shapeArc1, this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p145_1();
        this.instance.setTransform(4, 5, 0.47, 0.47);

        this.text = new cjs.Text("Vov!", "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(430, 55);

        this.text_1 = new cjs.Text("21 kr", "bold 164x 'UusiTekstausMajema'", "#000000");
        this.text_1.setTransform(15, 82);
        this.text_1.skewX = 0;
        this.text_1.skewY = 0;

        this.text_2 = new cjs.Text("36 kr", "bold 14px 'UusiTekstausMajema'", "#000000");
        this.text_2.setTransform(170, 68);
        this.text_2.skewX = -10;
        this.text_2.skewY = -10;

        this.text_3 = new cjs.Text("14 kr", "bold 14px 'UusiTekstausMajema'", "#000000");
        this.text_3.setTransform(270, 87);
        this.text_3.skewX = 20;
        this.text_3.skewY = 20;

        this.text_4 = new cjs.Text("45 kr", "bold 14px 'UusiTekstausMajema'", "#000000");
        this.text_4.setTransform(367, 65);
        this.text_4.skewX = -10;
        this.text_4.skewY = -10;

        this.text_5 = new cjs.Text("25 kr", "bold 14px 'UusiTekstausMajema'", "#000000");
        this.text_5.setTransform(85, 155);
        this.text_5.skewX = -10;
        this.text_5.skewY = -10;

        this.text_6 = new cjs.Text("62 kr", "bold 14px 'UusiTekstausMajema'", "#000000");
        this.text_6.setTransform(190, 160);
        this.text_6.skewX = -20;
        this.text_6.skewY = -20;

        this.text_7 = new cjs.Text("70 kr", "bold 14px 'UusiTekstausMajema'", "#000000");
        this.text_7.setTransform(290, 172)
        this.text_7.skewX = 20;
        this.text_7.skewY = 20;

        this.text_8 = new cjs.Text("27 kr", "bold 14px 'UusiTekstausMajema'", "#000000");
        this.text_8.setTransform(373, 155);
        this.text_8.skewX = 0;
        this.text_8.skewY = 0;

        this.addChild(this.instance, this.round_Rect1, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5,
            this.text_6, this.text_7, this.text_8);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 350);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("Räkna med miniräknaren.", "16px 'Myriad Pro'", "#000000");
        this.text.setTransform(0, 5);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 510, 343, 5);
        this.round_Rect1.setTransform(0, 17);

        this.instance = new lib.p145_2();
        this.instance.setTransform(15, 65, 0.47, 0.47);

        var TextArr = [];

        var TArr = ['Leksaker', 'Kostar', 'tillsammans:', 'Kvar av', '100 kronor:'];

        var TextX = [20, 323, 302, 430, 417];
        var TextY = [45, 37, 54, 37, 54];
        for (var i = 0; i < TextX.length; i++) {

            var text_4 = new cjs.Text(TArr[i], "16px 'Myriad Pro'", "#000000");
            text_4.setTransform(TextX[i], TextY[i]);
            TextArr.push(text_4);
        }

        var hrlineArr = [];
        var hrlineX = [0, 0, 0, 0, 0];
        var hrlineY = [64, 120, 180, 240, 300];

        for (var i = 0; i < hrlineX.length; i++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(510, 0);
            hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
            hrlineArr.push(hrLine_1);
        }

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 345);
        this.vrLine_1.setTransform(287, 17);

        this.vrLine_2 = new cjs.Shape();
        this.vrLine_2.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(0, 345);
        this.vrLine_2.setTransform(400, 17);

        this.addChild(this.text, this.round_Rect1, this.instance, this.vrLine_1, this.vrLine_2);

        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 350);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(283, 212, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305, 447, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
