(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p19_1.png",
            id: "p19_1"
        }, {
            src: "images/p19_2.png",
            id: "p19_2"
        }]
    };

    (lib.p19_1 = function() {
        this.initialize(img.p19_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p19_2 = function() {
        this.initialize(img.p19_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("19", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(555, 655);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);

        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Skriv och rita det som saknas.", "16px 'Myriad Pro'");
        this.text.setTransform(59, 34);

        this.instance = new lib.p19_1();
        this.instance.setTransform(5, 0, 0.47, 0.47);

        this.instance_1 = new lib.p19_2();
        this.instance_1.setTransform(80, 54, 0.47, 0.47);

        //large circles

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(145, 163, 64, 0, 2 * Math.PI);
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(295, 163, 64, 0, 2 * Math.PI);
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(445, 163, 64, 0, 2 * Math.PI);
        this.shape_group3.setTransform(0, 0);

        this.shape_group4 = new cjs.Shape();
        this.shape_group4.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(145, 340, 64, 0, 2 * Math.PI);
        this.shape_group4.setTransform(0, 0);

        this.shape_group5 = new cjs.Shape();
        this.shape_group5.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(295, 340, 64, 0, 2 * Math.PI);
        this.shape_group5.setTransform(0, 0);

        this.shape_group6 = new cjs.Shape();
        this.shape_group6.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(445, 340, 64, 0, 2 * Math.PI);
        this.shape_group6.setTransform(0, 0);

        this.shape_group7 = new cjs.Shape();
        this.shape_group7.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(145, 520, 64, 0, 2 * Math.PI);
        this.shape_group7.setTransform(0, 0);

        this.shape_group8 = new cjs.Shape();
        this.shape_group8.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(295, 520, 64, 0, 2 * Math.PI);
        this.shape_group8.setTransform(0, 0);

        this.shape_group9 = new cjs.Shape();
        this.shape_group9.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(445, 520, 64, 0, 2 * Math.PI);
        this.shape_group9.setTransform(0, 0);

        //small circles

        this.shape_group10 = new cjs.Shape();
        this.shape_group10.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(40, 162, 29, 0, 2 * Math.PI);
        this.shape_group10.setTransform(0, 0);

        this.shape_group11 = new cjs.Shape();
        this.shape_group11.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(40, 345, 29, 0, 2 * Math.PI);
        this.shape_group11.setTransform(0, 0);

        this.shape_group12 = new cjs.Shape();
        this.shape_group12.graphics.f("").s("#FAAA33").ss(0.8, 0, 0, 4).arc(40, 525, 29, 0, 2 * Math.PI);
        this.shape_group12.setTransform(0, 0);

        this.text_1 = new cjs.Text("Med siffror", "16px 'Myriad Pro'", "#F8AC58");
        this.text_1.setTransform(3, 123);

        this.text_2 = new cjs.Text("Med siffror", "16px 'Myriad Pro'", "#F8AC58");
        this.text_2.setTransform(3, 306);

        this.text_3 = new cjs.Text("Med siffror", "16px 'Myriad Pro'", "#F8AC58");
        this.text_3.setTransform(3, 485);

        this.text_4 = new cjs.Text("Mina symboler", "16px 'Myriad Pro'", "#F8AC58");
        this.text_4.setTransform(400, 85);

        this.text_5 = new cjs.Text("Mina symboler", "16px 'Myriad Pro'", "#F8AC58");
        this.text_5.setTransform(400, 264);

        this.text_6 = new cjs.Text("Mina symboler", "16px 'Myriad Pro'", "#F8AC58");
        this.text_6.setTransform(400, 443);

        this.text_7 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_7.setTransform(23, 172);

        this.text_8 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_8.setTransform(32, 172);

        this.addChild(this.text,  this.shape_group1,this.shape_group2,this.shape_group3,
         this.shape_group4,this.shape_group5,this.shape_group6,this.shape_group7,this.shape_group8,this.shape_group9,this.shape_group10,
         this.shape_group11,this.shape_group12,this.instance, this.instance_1,this.text_1,this.text_2, this.text_3,this.text_4,this.text_5, this.text_6,this.text_7, this.text_8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 590);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 227, 1, 1, 0, 0, 0, 256.3, 173.6);


        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
