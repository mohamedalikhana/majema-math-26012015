(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

        }]
    };

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("kunna klockan – hel och halv timme", "9px 'Myriad Pro'");
        this.text_1.setTransform(400, 659);

        this.text_2 = new cjs.Text("85", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(552, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Klockan – halvtimme", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_3.setTransform(110, 42);

        this.text_4 = new cjs.Text("29", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(63, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(49, 23.5, 1.15, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Klockan är halv 3.", "16px 'Myriad Pro'");
        this.text.setTransform(28, 45);

        this.text_1 = new cjs.Text("Minutvisaren pekar på 6.", "15.5px 'MyriadPro-Semibold'", "#0095DA");
        this.text_1.setTransform(179, 45);

        this.text_2 = new cjs.Text("Timvisaren pekar mellan 2 och 3.", "15.5px 'MyriadPro-Semibold'", "#DA2129");
        this.text_2.setTransform(179, 67);

        // Main yellow block
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 1, 510, 155, 15);
        this.roundRect1.setTransform(10, 0);

        // Main white block
        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(20, 17, 150, 125, 5);
        this.roundRect2.setTransform(0, 0);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_31.setTransform(32, 18);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_32.setTransform(47, 18);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_3.setTransform(62.8, 18);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_4.setTransform(78.6, 18);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_5.setTransform(94.4, 18);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_6.setTransform(110.2, 18);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_7.setTransform(126, 18);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_8.setTransform(141.8, 18);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_9.setTransform(157.6, 18);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_10.setTransform(33, 23.5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_11.setTransform(33, 23.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_12.setTransform(48, 23.5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_13.setTransform(48, 23.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_14.setTransform(64, 23.5);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_15.setTransform(64, 23.5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_16.setTransform(79.5, 23.5);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_17.setTransform(79.5, 23.5);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_18.setTransform(95, 23.5);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_19.setTransform(95, 23.5);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_20.setTransform(111, 23.5);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_21.setTransform(111, 23.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_22.setTransform(127, 23.5);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_23.setTransform(127, 23.5);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_24.setTransform(143, 23.5);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_25.setTransform(143, 23.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_26.setTransform(159, 23.5);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_27.setTransform(159, 23.5);

        this.instance_clock1 = new lib.Symbolclock();
        this.instance_clock1.setTransform(90, 95, 0.9, 0.9);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AhegoIALgUICyBlIgLAUg");
        this.shape_1.setTransform(100, 92, 1.3, 0.6, 150);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(90, 108, 1.2, 0.9, 180);
        this.text_dot = new cjs.Text(".", "40px 'Myriad Pro'");
        this.text_dot.setTransform(84, 93.5);

        this.addChild(this.roundRect1, this.roundRect2, this.text, this.instance_clock1, this.shape_1, this.shape_2, this.text_dot, this.text_1, this.text_2,
            this.shape_10, this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_19, this.shape_20,
            this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.shape_25, this.shape_26, this.shape_27,
            this.shape_31, this.shape_32, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 580.3, 143.6);

    //Part 1
    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Vad är klockan?", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 385, 10);
        this.roundRect1.setTransform(10, 20);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_1.setTransform(10, 115);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(510, 40);
        this.hrLine_2.setTransform(10, 237);

        var vrlineArr = [];
        var vrlineX = [100, 225, 350, 100, 225, 350, 100, 225, 350];
        var vrlineY = [25, 25, 25, 160, 160, 160, 281, 281, 281];

        for (var row = 0; row < vrlineX.length; row++) {

            if (row > 2 && row < 6) {
                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 113);
                vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
                vrlineArr.push(vrLine_1);
            } else if (row > 5 && row < 9) {
                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 117);
                vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
                vrlineArr.push(vrLine_1);
            } else {

                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 127);
                vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
                vrlineArr.push(vrLine_1);
            }

        }

        this.Symbolclock = new lib.Symbolclock();
        this.Symbolclock.setTransform(72, 75, 0.83, 0.83);

        this.Symbolclock_1 = new lib.Symbolclock();
        this.Symbolclock_1.setTransform(200, 75, 0.83, 0.83);

        this.Symbolclock_2 = new lib.Symbolclock();
        this.Symbolclock_2.setTransform(329, 75, 0.83, 0.83);

        this.Symbolclock_3 = new lib.Symbolclock();
        this.Symbolclock_3.setTransform(455, 75, 0.83, 0.83);

        this.Symbolclock_4 = new lib.Symbolclock();
        this.Symbolclock_4.setTransform(72, 202, 0.83, 0.83);

        this.Symbolclock_5 = new lib.Symbolclock();
        this.Symbolclock_5.setTransform(200, 202, 0.83, 0.83);

        this.Symbolclock_6 = new lib.Symbolclock();
        this.Symbolclock_6.setTransform(329, 202, 0.83, 0.83);

        this.Symbolclock_7 = new lib.Symbolclock();
        this.Symbolclock_7.setTransform(455, 202, 0.83, 0.83);

        this.Symbolclock_8 = new lib.Symbolclock();
        this.Symbolclock_8.setTransform(72, 330, 0.83, 0.83);

        this.Symbolclock_9 = new lib.Symbolclock();
        this.Symbolclock_9.setTransform(200, 330, 0.83, 0.83);

        this.Symbolclock_10 = new lib.Symbolclock();
        this.Symbolclock_10.setTransform(329, 330, 0.83, 0.83);

        this.Symbolclock_11 = new lib.Symbolclock();
        this.Symbolclock_11.setTransform(455, 330, 0.83, 0.83);

        var TextArr = [];
        var TextX = [45, 171, 297, 423, 45, 171, 297, 423, 45, 171, 297, 423];
        var TextY = [142, 142, 142, 142, 265, 265, 265, 265, 390, 390, 390, 390];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("halv", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        var lineArr = [];
        var lineX = [77, 202, 329, 454, 77, 202, 329, 454, 77, 202, 329, 454];
        var lineY = [103, 103, 103, 103, 227, 227, 227, 227, 353, 353, 353, 353];

        for (var row = 0; row < lineX.length; row++) {
            var vrLine_2 = new cjs.Shape();
            vrLine_2.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(30, 40);
            vrLine_2.setTransform(lineX[row], lineY[row]);
            lineArr.push(vrLine_2);
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.hrLine_1, this.hrLine_2,
            this.Symbolclock, this.Symbolclock_1, this.Symbolclock_2, this.Symbolclock_3, this.Symbolclock_4, this.Symbolclock_5,
            this.Symbolclock_6, this.Symbolclock_7, this.Symbolclock_8, this.Symbolclock_9, this.Symbolclock_10, this.Symbolclock_11);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

        for (var i = 0; i < vrlineArr.length; i++) {
            this.addChild(vrlineArr[i]);
        }
        //row-1

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(82, 77.5, 1.15, 0.7, 105);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(72, 86.5, 1.15, 0.9);
        this.text_dot1 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot1.setTransform(66, 74);

        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(202.7, 83, 1.15, 0.7, 166);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(200, 86.5, 1.15, 0.9);
        this.text_dot2 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot2.setTransform(195, 74.5);

        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(319, 72.5, 1.15, 0.7, 287);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(329, 86.5, 1.15, 0.9);
        this.text_dot3 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot3.setTransform(323, 74.5);

        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(452.5, 83.7, 1.15, 0.7, 190);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(455.5, 86.5, 1.15, 0.9);
        this.text_dot4 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot4.setTransform(448.8, 74.5);

        //row-2

        this.shape_9 = new cjs.Shape(); // clock handle red
        this.shape_9.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_9.setTransform(66, 195, 1.15, 0.7, 499);
        this.shape_10 = new cjs.Shape(); // clock handle blue
        this.shape_10.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_10.setTransform(72, 214.1, 1.15, 0.9);
        this.text_dot5 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot5.setTransform(66, 202);

        this.shape_11 = new cjs.Shape(); // clock handle red
        this.shape_11.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_11.setTransform(206, 195.5, 1.15, 0.7, 47);
        this.shape_12 = new cjs.Shape(); // clock handle blue
        this.shape_12.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_12.setTransform(200, 214.1, 1.15, 0.9);
        this.text_dot6 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot6.setTransform(194, 202);

        this.shape_13 = new cjs.Shape(); // clock handle red
        this.shape_13.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_13.setTransform(321, 204, 1.15, 0.7, 255);
        this.shape_14 = new cjs.Shape(); // clock handle blue
        this.shape_14.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_14.setTransform(329.3, 214.1, 1.15, 0.9);
        this.text_dot7 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot7.setTransform(323, 202);

        this.shape_15 = new cjs.Shape(); // clock handle red
        this.shape_15.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_15.setTransform(461.5, 207.5, 1.15, 0.7, 134);
        this.shape_16 = new cjs.Shape(); // clock handle blue
        this.shape_16.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_16.setTransform(455, 214.1, 1.15, 0.9);
        this.text_dot8 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot8.setTransform(450, 202);

        //row-3

        this.shape_17 = new cjs.Shape(); // clock handle red
        this.shape_17.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_17.setTransform(75, 320, 1.15, 0.7, 17);
        this.shape_18 = new cjs.Shape(); // clock handle blue
        this.shape_18.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_18.setTransform(71.9, 341.1, 1.15, 0.9);
        this.text_dot9 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot9.setTransform(66, 329.5);

        this.shape_19 = new cjs.Shape(); // clock handle red
        this.shape_19.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_19.setTransform(197.5, 319, 1.15, 0.7, 167);
        this.shape_20 = new cjs.Shape(); // clock handle blue
        this.shape_20.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_20.setTransform(200, 341.1, 1.15, 0.9);
        this.text_dot10 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot10.setTransform(194, 329.5);

        this.shape_21 = new cjs.Shape(); // clock handle red
        this.shape_21.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_21.setTransform(337.5, 328, 1.15, 0.7, 257);
        this.shape_22 = new cjs.Shape(); // clock handle blue
        this.shape_22.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_22.setTransform(329, 341.1, 1.15, 0.9);
        this.text_dot11 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot11.setTransform(323, 329.5);

        this.shape_23 = new cjs.Shape(); // clock handle red
        this.shape_23.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_23.setTransform(449.3, 336, 1.15, 0.7, 41);
        this.shape_24 = new cjs.Shape(); // clock handle blue
        this.shape_24.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_24.setTransform(455, 341.1, 1.15, 0.9);
        this.text_dot12 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot12.setTransform(449, 329.5);

        this.addChild(this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9,
            this.shape_10, this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18,
            this.shape_20, this.shape_19, this.shape_21, this.shape_22, this.shape_24, this.shape_23, this.text_dot1, this.text_dot2, this.text_dot3,
            this.text_dot4, this.text_dot5, this.text_dot6, this.text_dot7, this.text_dot8, this.text_dot9, this.text_dot10, this.text_dot11, this.text_dot12)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 513.3, 400);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 108, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300, 285, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
