(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p26_1.png",
            id: "p26_1"
        }, {
            src: "images/p26_2.png",
            id: "p26_2"
        }, {
            src: "images/p26_3.png",
            id: "p26_3"
        }, {
            src: "images/p26_4.png",
            id: "p26_4"
        }]
    };

    (lib.p26_1 = function() {
        this.initialize(img.p26_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p26_2 = function() {
        this.initialize(img.p26_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p26_3 = function() {
        this.initialize(img.p26_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p26_4 = function() {
        this.initialize(img.p26_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);


    (lib.Symbol1 = function() {
        this.initialize();

        this.pageTitle = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#90BD22");
        this.pageTitle.setTransform(98, 42);

        this.text_1 = new cjs.Text("8", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(59, 42);

        this.text_2 = new cjs.Text("26", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(40, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.8);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#90BD22").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(45, 15, 1.03, 1.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.pageTitle, this.shape_2, this.shape_1,
            this.shape, this.text, this.text_1, this.text_2, this.text_3, this.Symbolimg);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Sideimg = function() {
        this.initialize();

        this.instance = new lib.p26_1();
        this.instance.setTransform(29, 0, 0.47, 0.47);

        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.setTransform(24, 35);

        this.text_1 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(10, 35);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#FFFFFF').s("#7D7D7D").ss(0.7).drawRoundRect(5, 12, 497, 141, 10);
        this.roundRect1.setTransform(0, 0);

        var lineArr = [];
        var lineX = [90, 243, 420, 90, 243, 420, 90, 243, 420, 90, 243, 420];
        var lineY = [25, 25, 25, 52, 52, 52, 77, 77, 77, 101, 101, 101];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(45, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("10  +  6  =", "16px 'Myriad Pro'");
        this.label1.setTransform(33, 60);
        this.label2 = new cjs.Text("16  +  2  =", "16px 'Myriad Pro'");
        this.label2.setTransform(33, 85);
        this.label3 = new cjs.Text("18  +  2  =", "16px 'Myriad Pro'");
        this.label3.setTransform(33, 110);
        this.label4 = new cjs.Text("15  +  5  =", "16px 'Myriad Pro'");
        this.label4.setTransform(33, 135);

        this.label6 = new cjs.Text("19  –  3   =", "16px 'Myriad Pro'");
        this.label6.setTransform(181, 60);
        this.label7 = new cjs.Text("20  –  5   =", "16px 'Myriad Pro'");
        this.label7.setTransform(181, 85);
        this.label8 = new cjs.Text("18  –  8   =", "16px 'Myriad Pro'");
        this.label8.setTransform(181, 110);
        this.label9 = new cjs.Text("16  – 10  =", "16px 'Myriad Pro'");
        this.label9.setTransform(181, 135);

        this.label11 = new cjs.Text("20  –  2  –  2  =", "16px 'Myriad Pro'");
        this.label11.setTransform(331, 60);
        this.label12 = new cjs.Text("19  –  2  –  2  =", "16px 'Myriad Pro'");
        this.label12.setTransform(331, 85);
        this.label13 = new cjs.Text("18  –  3  –  3  =", "16px 'Myriad Pro'");
        this.label13.setTransform(331, 110);
        this.label14 = new cjs.Text("20  –  5  –  5  =", "16px 'Myriad Pro'");
        this.label14.setTransform(331, 135);

        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(123.5, 36);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_3.setTransform(231, 37);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(343, 37);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_5.setTransform(455, 37);

        this.Rect_1 = new cjs.Shape();
        this.Rect_1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(493, 43, 39, 82, 10);
        this.Rect_1.setTransform(0, 0);

        this.Sideimg = new lib.Sideimg();
        this.Sideimg.setTransform(463, 42);

        this.addChild(this.Rect_1, this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4,
            this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14,
            this.text_2, this.text_3, this.text_4, this.text_5, this.Sideimg);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 590, 150);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Ringa in udda tal.", "16px 'Myriad Pro'");
        this.text.setTransform(24, 31);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(10, 31);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#FFFFFF').s("#7D7D7D").ss(0.7).drawRoundRect(5, 6, 497, 76, 10);
        this.roundRect1.setTransform(0, 0);

        var arrText = [];

        var ToBeAdded = [];

        for (var i = 1; i < 11; i++) {
            var columnSpace = i;
            var text = i.toString();
            if (i == 1) {
                columnSpace = 1.2;
            } else if (i == 2) {
                columnSpace = 2.2;
            } else if (i == 3) {
                columnSpace = 3.1;
            } else if (i == 4) {
                columnSpace = 3.9;
            } else if (i == 5) {
                columnSpace = 4.9;
            } else if (i == 6) {
                columnSpace = 5.8;
            } else if (i == 7) {
                columnSpace = 6.75;
            } else if (i == 8) {
                columnSpace = 7.7;
            } else if (i == 9) {
                columnSpace = 8.7;
            } else if (i == 10) {
                columnSpace = 9.65;
            }

            this.temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            this.temp_label.setTransform(-9 + (columnSpace * 45), 65);
            ToBeAdded.push(this.temp_label);
        }

        this.Rect_1 = new cjs.Shape();
        this.Rect_1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(493, 2.5, 39, 82, 10);
        this.Rect_1.setTransform(0, 0);

        this.Sideimg = new lib.Sideimg();
        this.Sideimg.setTransform(463, 2);

        this.addChild(this.Rect_1, this.roundRect1, this.text, this.text_1, this.Sideimg);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 590, 80);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text(" Ringa in jämna tal.", "16px 'Myriad Pro'");
        this.text.setTransform(24, 31);

        this.text_1 = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(10, 31);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#FFFFFF').s("#7D7D7D").ss(0.7).drawRoundRect(5, 7, 497, 76, 10);
        this.roundRect1.setTransform(0, 0);

        var arrText = [];

        var ToBeAdded = [];

        for (var i = 11; i < 21; i++) {
            var columnSpace = i;
            var text = i.toString();
            if (i == 11) {
                columnSpace = 1.2;
            } else if (i == 12) {
                columnSpace = 2.2;
            } else if (i == 13) {
                columnSpace = 3.1;
            } else if (i == 14) {
                columnSpace = 3.9;
            } else if (i == 15) {
                columnSpace = 4.9;
            } else if (i == 16) {
                columnSpace = 5.8;
            } else if (i == 17) {
                columnSpace = 6.75;
            } else if (i == 18) {
                columnSpace = 7.7;
            } else if (i == 19) {
                columnSpace = 8.7;
            } else if (i == 20) {
                columnSpace = 9.65;
            }

            this.temp_label = new cjs.Text(text, "16px 'Myriad Pro'");
            this.temp_label.setTransform(-9 + (columnSpace * 45), 65);
            ToBeAdded.push(this.temp_label);
        }

        this.Rect_1 = new cjs.Shape();
        this.Rect_1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(493, 5.5, 39, 82, 10);
        this.Rect_1.setTransform(0, 0);

        this.Sideimg = new lib.Sideimg();
        this.Sideimg.setTransform(463, 5);

        this.addChild(this.Rect_1, this.roundRect1, this.text, this.text_1, this.Sideimg);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 590, 80);

    (lib.Symbol5 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(5, 7, 245, 119, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text(" Vilket tal är det?", "16px 'Myriad Pro'");
        this.text.setTransform(24, 30);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(10, 30);

        this.instance = new lib.p26_2();
        this.instance.setTransform(19, 16, 0.47, 0.47);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(32, 115).lineTo(88, 115);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(32, 115).lineTo(88, 115);
        this.hrLine_2.setTransform(132, 0);

        this.addChild(this.roundRect1, this.instance, this.text, this.text_1, this.hrLine_1, this.hrLine_2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 250, 130);

    (lib.Symbol6 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#FFFFFF').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 246, 119, 10);
        this.roundRect1.setTransform(0, 7);

        this.text = new cjs.Text(" Rita med egyptiska talsymboler.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 30);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 30);

        this.Rect = new cjs.Shape();
        this.Rect.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 229, 49, 0);
        this.Rect.setTransform(8, 39);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(56, 0);
        this.hrLine_1.setTransform(92, 115);

        this.text_2 = new cjs.Text("18", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_2.setTransform(100, 115);

        this.Rect_1 = new cjs.Shape();
        this.Rect_1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 39, 82, 10);
        this.Rect_1.setTransform(238, 29);

        this.Sideimg = new lib.Sideimg();
        this.Sideimg.setTransform(208, 29);

        this.addChild(this.Rect_1, this.roundRect1, this.Rect, this.hrLine_1, this.text, this.text_1, this.text_2, this.Sideimg);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 250, 130);

    (lib.Symbol7 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(5, 10, 245, 120, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text(" Vad kostar de tillsammans?", "16px 'Myriad Pro'");
        this.text.setTransform(24, 27);

        this.text_1 = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(10, 27);

        this.instance = new lib.p26_3();
        this.instance.setTransform(10, 15, 0.47, 0.47);

        this.text_2 = new cjs.Text("10 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_2.setTransform(85, 57);
        this.text_2.skewX = -20;
        this.text_2.skewY = -20;

        this.text_3 = new cjs.Text("9 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_3.setTransform(142, 57)
        this.text_3.skewX = 20;
        this.text_3.skewY = 20;

        this.Rect = new cjs.Shape();
        this.Rect.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(29, 98, 200, 23, 0);
        this.Rect.setTransform(0, 0);

        var lineArr = [];
        var lineX = [11, 31, 51, 71, 91, 111, 131, 151, 171];
        var lineY = [98, 98, 98, 98, 98, 98, 98, 98, 98];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 23);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.addChild(this.roundRect1, this.instance, this.text, this.text_1, this.text_2, this.text_3, this.Rect);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 250, 130);

    (lib.Symbol8 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#FFFFFF').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 246, 120, 10);
        this.roundRect1.setTransform(0, 10);

        this.text = new cjs.Text(" Räkna ut prisskillnaden.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 27);

        this.text_1 = new cjs.Text("7.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(5, 27);

        this.text_2 = new cjs.Text("18 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_2.setTransform(38, 47);
        this.text_2.skewX = 20;
        this.text_2.skewY = 20;

        this.text_3 = new cjs.Text("7 kr", "bold 16px 'UusiTekstausMajema'", "#000000");
        this.text_3.setTransform(194, 63)
        this.text_3.skewX = -20;
        this.text_3.skewY = -20;

        this.instance = new lib.p26_4();
        this.instance.setTransform(19, 8, 0.47, 0.47);

        this.Rect = new cjs.Shape();
        this.Rect.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 200, 23, 0);
        this.Rect.setTransform(25, 98);

        var lineArr = [];
        var lineX = [5, 25, 45, 65, 85, 105, 125, 145, 165];
        var lineY = [98, 98, 98, 98, 98, 98, 98, 98, 98];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 23);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.Rect_1 = new cjs.Shape();
        this.Rect_1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 39, 82, 10);
        this.Rect_1.setTransform(238, 29);

        this.Sideimg = new lib.Sideimg();
        this.Sideimg.setTransform(208, 29);

        this.addChild(this.Rect_1, this.roundRect1, this.instance, this.text, this.text_1, this.text_2, this.text_3, this.Rect, this.Sideimg);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 250, 130);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(294, 45, 1, 1, 0, 0, 0, 255.8, 0);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294, 202, 1, 1, 0, 0, 0, 255.8, 0);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(294, 288, 1, 1, 0, 0, 0, 255.8, 0);

        this.v4 = new lib.Symbol5();
        this.v4.setTransform(294, 375, 1, 1, 0, 0, 0, 255.8, 0);

        this.v5 = new lib.Symbol6();
        this.v5.setTransform(550, 375, 1, 1, 0, 0, 0, 255.8, 0);

        this.v6 = new lib.Symbol7();
        this.v6.setTransform(294, 502, 1, 1, 0, 0, 0, 255.8, 0);

        this.v7 = new lib.Symbol8();
        this.v7.setTransform(550, 502, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.v2, this.v3,this.v5, this.v4, this.v7, this.v6, this.other);

        // this.addChild(this.v1, this.v2,this.v4, this.v5, this.v6, this.v7, this.other);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
