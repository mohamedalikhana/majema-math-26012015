(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p131_1.png",
            id: "p131_1"
        }, {
            src: "images/p131_2.png",
            id: "p131_2"
        }]
    };

    (lib.p131_1 = function() {
        this.initialize(img.p131_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p131_2 = function() {
        this.initialize(img.p131_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("131", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(552, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.instance = new lib.p131_1();
        this.instance.setTransform(404, 32, 0.62, 0.62);

        this.text_1 = new cjs.Text("Du kan visa talet", "14px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(438, 41);

        this.text_2 = new cjs.Text("11 med pengar.", "14px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(441, 57);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text,
            this.instance, this.text_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Fyll i det som saknas.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(-2, 9);

        this.text_2 = new cjs.Text("Siffror", "16px 'Myriad Pro'", "#FBAA34");
        this.text_2.lineHeight = 25;
        this.text_2.setTransform(50, 37);
        this.text_3 = new cjs.Text("Centimo", "16px 'Myriad Pro'", "#FBAA34");
        this.text_3.lineHeight = 25;
        this.text_3.setTransform(220, 37);
        this.text_4 = new cjs.Text("Pengar", "16px 'Myriad Pro'", "#FBAA34");
        this.text_4.lineHeight = 25;
        this.text_4.setTransform(401, 37);

        this.text_5 = new cjs.Text("1", "72px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 80;
        this.text_5.setTransform(18, 80);
        this.text_6 = new cjs.Text("2", "72px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 80;
        this.text_6.setTransform(92, 80);

        var ToBeAdded = [];
        var arryPos = ['64', '187', '310', '433'];
        for (var row = 0; row < arryPos.length; row++) {
            var yPos = arryPos[row];
            for (var col = 0; col < 4; col++) {
                var colSpace = col;

                var temp_text1 = new cjs.Text("tiotal", "14px 'Myriad Pro'");
                temp_text1.lineHeight = -4;
                temp_text1.setTransform(19 + (colSpace * 177), yPos);
                var temp_text2 = new cjs.Text("ental", "14px 'Myriad Pro'");
                temp_text2.lineHeight = -4;
                temp_text2.setTransform(96 + (colSpace * 177), yPos);
                if (col != 3) {
                    ToBeAdded.push(temp_text1, temp_text2);
                }

            }
        }

        // row-1
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FBAA34').drawRoundRect(351, 37, 152, 113, 5);
        this.roundRect1.setTransform(0, 25);
        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(-175, 25);
        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(-350, 25);
        // row-2
        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(0, 148);
        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(-175, 148);
        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(-350, 148);
        // row-3
        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 271);
        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(-175, 271);
        this.roundRect9 = this.roundRect1.clone(true);
        this.roundRect9.setTransform(-350, 271);
        // row-4
        this.roundRect10 = this.roundRect1.clone(true);
        this.roundRect10.setTransform(0, 394);
        this.roundRect11 = this.roundRect1.clone(true);
        this.roundRect11.setTransform(-175, 394);
        this.roundRect12 = this.roundRect1.clone(true);
        this.roundRect12.setTransform(-350, 394);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#FBAA34").setStrokeStyle(0.7).moveTo(427, 62).lineTo(427, 175).moveTo(351, 79).lineTo(503, 79)
            .moveTo(252, 62).lineTo(252, 175).moveTo(176, 79).lineTo(328, 79)
            .moveTo(77, 62).lineTo(77, 175).moveTo(1, 79).lineTo(153, 79)
            .moveTo(427, 185).lineTo(427, 298).moveTo(351, 202).lineTo(503, 202)
            .moveTo(252, 185).lineTo(252, 298).moveTo(176, 202).lineTo(328, 202)
            .moveTo(77, 185).lineTo(77, 298).moveTo(1, 202).lineTo(153, 202)
            .moveTo(427, 309).lineTo(427, 422).moveTo(351, 326).lineTo(503, 326)
            .moveTo(252, 309).lineTo(252, 422).moveTo(176, 326).lineTo(328, 326)
            .moveTo(77, 309).lineTo(77, 422).moveTo(1, 326).lineTo(153, 326)
            .moveTo(427, 431).lineTo(427, 544).moveTo(351, 448).lineTo(503, 448)
            .moveTo(252, 431).lineTo(252, 544).moveTo(176, 448).lineTo(328, 448)
            .moveTo(77, 431).lineTo(77, 544).moveTo(1, 448).lineTo(153, 448);
        this.Line_1.setTransform(0, 0);
        // row-1
        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f("#20B14A").s("#000000").setStrokeStyle(0.7).moveTo(209, 83).lineTo(209, 171).lineTo(218, 171).lineTo(218, 83).lineTo(209, 83)
            .moveTo(209, 91.6).lineTo(218, 91.6).moveTo(209,100.1).lineTo(218, 100.1).moveTo(209, 108.6).lineTo(218, 108.6).moveTo(209, 117.1).lineTo(218, 117.1)
            .moveTo(209, 125.6).lineTo(218, 125.6).moveTo(209, 134.1).lineTo(218, 134.1).moveTo(209, 143.6).lineTo(218, 143.6).moveTo(209, 153.1).lineTo(218, 153.1)
            .moveTo(209, 162.7).lineTo(218, 162.7);
        this.Line_2.setTransform(0, 0);
        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(285, 153.1).lineTo(285, 171).lineTo(294, 171).lineTo(294, 153.1).lineTo(285, 153.1)
            .moveTo(285, 153.1).lineTo(294, 153.1).moveTo(285, 162.7).lineTo(294, 162.7);
        this.Line_3.setTransform(0, 0);
        // row-2
        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f("#20B14A").s("#000000").setStrokeStyle(0.7).moveTo(209, 206).lineTo(209, 294).lineTo(218, 294).lineTo(218, 206).lineTo(209, 206)
            .moveTo(209, 214.6).lineTo(218, 214.6).moveTo(209,223.1).lineTo(218, 223.1).moveTo(209, 231.6).lineTo(218, 231.6).moveTo(209, 240.1).lineTo(218, 240.1)
            .moveTo(209, 248.6).lineTo(218, 248.6).moveTo(209, 257.1).lineTo(218, 257.1).moveTo(209, 266.6).lineTo(218, 266.6).moveTo(209, 276.1).lineTo(218, 276.1)
            .moveTo(209, 285.7).lineTo(218, 285.7);
        this.Line_4.setTransform(0, 0);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(285, 257.1).lineTo(285, 294).lineTo(294, 294).lineTo(294, 257.1).lineTo(285, 257.1)
            .moveTo(285, 266.6).lineTo(294, 266.6).moveTo(285, 276.1).lineTo(294, 276.1).moveTo(285, 285.7).lineTo(294, 285.7);
        this.Line_5.setTransform(0, 0);

        this.instance = new lib.p131_2();
        this.instance.setTransform(370, 329, 0.62, 0.62);

        this.dot_line1 = new cjs.Shape();
        var temp;
        for (var i = 35; i < 548; i++) {
            if (i == 35) {
                this.dot_line1.graphics.f("#949599").s("#949599").ss(0.5).arc(165, i, 0.5, 0, 1 * Math.PI);
                temp = i + 3;
            } else if (i == temp) {
                this.dot_line1.graphics.f("#949599").s("#949599").ss(0.5).arc(165, i, 0.5, 0, 1 * Math.PI);
                temp = i + 3;
            }
        }
        this.dot_line1.setTransform(0, 0);

        this.dot_line2 = new cjs.Shape();
        var temp;
        for (var i = 35; i < 548; i++) {
            if (i == 35) {
                this.dot_line2.graphics.f("#949599").s("#949599").ss(0.5).arc(340, i, 0.5, 0, 1 * Math.PI);
                temp = i + 3;
            } else if (i == temp) {
                this.dot_line2.graphics.f("#949599").s("#949599").ss(0.5).arc(340, i, 0.5, 0, 1 * Math.PI);
                temp = i + 3;
            }
        }
        this.dot_line2.setTransform(0, 0);

        this.addChild(this.text_1, this.text_2, this.roundRect1, this.roundRect2, this.roundRect3,
            this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.roundRect9,
            this.roundRect10, this.roundRect11, this.roundRect12, this.Line_1, this.text_3, this.text_4);
        this.addChild(this.instance, this.text_5, this.text_6, this.Line_2,this.Line_3,this.Line_4,this.Line_5, this.dot_line1,this.dot_line2);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 508);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310, 265, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
