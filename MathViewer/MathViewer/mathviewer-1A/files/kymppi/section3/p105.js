(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p105_1.png",
            id: "p105_1"
        }]
    };

    (lib.p105_1 = function() {
        this.initialize(img.p105_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("105", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(533, 650);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(560, 660.8);

        this.instance = new lib.p105_1();
        this.instance.setTransform(29, 13, 0.73, 0.72);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(451 + (columnSpace * 33), 32, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text(" Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20, 1);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 223, 349, 10);
        this.roundRect1.setTransform(0, 17);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(272, 10, 239, 349, 10);
        this.roundRect2.setTransform(0, 17);

        var arrxPos = ['20', '137', '291', '406'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                var fColor = "";
                var Colpadding = 22;
                if (i < 2) {
                    if (i == 1 && column == 4) {
                        continue;
                    }
                    fColor = "#DA2129"; //red
                } else {
                    Colpadding = 21.5;
                    fColor = "#0095DA"; //blue
                }
                for (var row = 0; row < 1; row++) {
                    this.shape_group1.graphics.f(fColor).s("#6E6E70").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * Colpadding), 51 + (row * 23), 9.3, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 10; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.8).drawRect(99 + (columnSpace * 258), 75 + (row * 29), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var ToBeAdded = [];
        arrxPos = [];
        arrxPos = ['127', '417'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "=   " + (i + 9).toString();

            for (var j = 0; j < 10; j++) {
                var rowSpace = j;

                if (j == 6) {
                    rowSpace = 6.04;
                } else if (j == 7) {
                    rowSpace = 7.04;
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 80 + (rowSpace * 29));
                ToBeAdded.push(temp_text);
            }
        }

        for (var j = 0; j < 10; j++) {
            var rowSpace = j;

            if (j == 6) {
                rowSpace = 6.04;
            } else if (j == 7) {
                rowSpace = 7.04;
            }
            var temp_Line = new cjs.Shape();
            temp_Line.graphics.s("#949599").ss(1).moveTo(379, 99 + (rowSpace * 29)).lineTo(409, 99 + (rowSpace * 29));
            temp_Line.setTransform(0, 0);

            ToBeAdded.push(temp_Line);
        }


        arrxPos = [];
        arrxPos = ['64.5', '345.5'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "";

            for (var j = 0; j < 10; j++) {
                var rowSpace = j;

                if (j == 0) {
                    text = "0   +";
                } else if (j == 1) {
                    text = "9   +";
                } else if (j == 2) {
                    text = "8   +";
                } else if (j == 3) {
                    text = "6   +";
                } else if (j == 4) {
                    text = (i == 0) ? "4   +" : "7   +";
                } else if (j == 5) {
                    text = (i == 0) ? "7   +" : "5   +";
                } else if (j == 6) {
                    rowSpace = 6.04;
                    text = (i == 0) ? "5   +" : "3   +";
                } else if (j == 7) {
                    rowSpace = 7.04;
                    text = (i == 0) ? "3   +" : "4   +";
                } else if (j == 8) {
                    text = "2   +";
                } else if (j == 9) {
                    text = "1   +";
                    if (i == 1) {
                        xPos = 343;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 80 + (rowSpace * 29));
                ToBeAdded.push(temp_text);
            }
        }

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.shape_group1, this.textbox_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(628.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(313, 478, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
