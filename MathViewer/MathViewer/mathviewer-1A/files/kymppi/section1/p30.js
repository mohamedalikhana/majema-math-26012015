(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p30_1.png",
            id: "p30_1"
        }]
    };

    // symbols:

    (lib.p30_1 = function() {
        this.initialize(img.p30_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("30", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(38, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00B0CA").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Fortsätt mönstret.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(22, 29);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#00B0CA");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(4, 29);

        this.text_2 = new cjs.Text("Kluring", "18px 'MyriadPro-Semibold'", "#00B0CA");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(-1, 0);

        this.instance = new lib.p30_1();
        this.instance.setTransform(40, 41, 0.44, 0.44);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 3, 514, 218, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text_2, this.text, this.text_1, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1.5, 0, 514, 346, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(25, 8);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#00B0CA");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(6, 8);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('#ffffff').s("#818284").ss(0.8).drawRect(28, 34, 180, 299);
        this.rect1.setTransform(0, 0);

        this.rect2 = new cjs.Shape();
        this.rect2.graphics.f('#ffffff').s("#818284").ss(0.8).drawRect(277, 34, 180, 299);
        this.rect2.setTransform(0, 0);

        var ToBeAdded = [];
        for (var col = 0; col < 9; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temptext=null;
                temptext = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
                temptext.lineHeight = -4;
                temptext.setTransform(42 + (colSpace * 18.8), 44.3 + (rowSpace * 21.1));
                ToBeAdded.push(temptext);
            }
        }

        for (var col = 0; col < 9; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temptext=null;
                temptext = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
                temptext.lineHeight = -4;
                temptext.setTransform(291 + (colSpace * 18.8), 44.3 + (rowSpace * 21.1));
                ToBeAdded.push(temptext);
            }
        }
        // block 1
        this.Line_1 = new cjs.Shape(); // green
        this.Line_1.graphics.f('#20B14A').s("#949599").ss(1.2).moveTo(44, 134).lineTo(194, 134).lineTo(176, 155).lineTo(156, 155).lineTo(138, 177)
            .lineTo(100, 177).lineTo(81, 155).lineTo(63, 155).lineTo(44, 134);
        this.Line_1.setTransform(0, 0);
        this.Line_2 = new cjs.Shape(); // red
        this.Line_2.graphics.f('#DA2129').s("#949599").ss(1.2).moveTo(101, 134).lineTo(138, 134).lineTo(138, 156).lineTo(101, 156).lineTo(101, 134)
            .moveTo(63, 134).lineTo(63, 50).lineTo(82, 50).lineTo(82, 70).lineTo(63, 70)
            .moveTo(176, 135).lineTo(176, 71).lineTo(195, 71).lineTo(195, 91).lineTo(176, 91);
        this.Line_2.setTransform(0, 0);
        this.Line_3 = new cjs.Shape(); // yellow
        this.Line_3.graphics.f('#FFF679').s("#949599").ss(1.2).moveTo(81, 134).lineTo(119, 50).lineTo(156, 134).lineTo(81, 134)
            .moveTo(119, 134).lineTo(119, 50);
        this.Line_3.setTransform(0, 0);
            
        // block 2
        this.Line_4 = new cjs.Shape(); // yellow
        this.Line_4.graphics.f('#FFF679').s("#949599").ss(1.2).moveTo(293, 113).lineTo(330, 93).lineTo(330, 134).lineTo(293, 113)
            .moveTo(407, 113.5).lineTo(443, 92.5).lineTo(443, 70).lineTo(407, 113.5)
            .moveTo(407, 113.5).lineTo(443, 134.5).lineTo(443, 155).lineTo(407, 113.5);
        this.Line_4.setTransform(0, 0);        
        this.Line_5 = new cjs.Shape(); // red
        this.Line_5.graphics.f('#DA2129').s("#949599").ss(1.2).moveTo(330, 92.5).lineTo(387, 92.5).lineTo(407, 113.5).lineTo(330, 113.5).lineTo(330, 92.5)
            .moveTo(387, 134.5).lineTo(387, 175.5).lineTo(350, 134.5).lineTo(387, 134.5)
            .moveTo(407, 113.5).lineTo(443, 92.5).lineTo(443, 134.5).lineTo(407, 113.5);
        this.Line_5.setTransform(0, 0);
        this.Line_6 = new cjs.Shape(); // BLUE
        this.Line_6.graphics.f('#0095DA').s("#949599").ss(1.2).moveTo(330, 134.5).lineTo(387, 134.5).lineTo(407, 113.5).lineTo(330, 113.5).lineTo(330, 92.5)
            .moveTo(387, 93).lineTo(387, 50).lineTo(350, 92).lineTo(387, 92);
        this.Line_6.setTransform(0, 0);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.s("#818284").ss(0.8).moveTo(28, 187).lineTo(208, 187).moveTo(277, 187).lineTo(457, 187);
        this.Line_7.setTransform(0, 0);

        this.Line_prac_1 = new cjs.Shape();
        this.Line_prac_1.graphics.s("#818284").ss(1.2).moveTo(44, 282).lineTo(63, 282).moveTo(44, 282).lineTo(63, 303)
            .moveTo(100, 241).lineTo(119, 198).lineTo(138, 241)
            .moveTo(330, 239.5).lineTo(293, 260).lineTo(330, 282);
        this.Line_prac_1.setTransform(0, 0);    

        this.text_dot_1 = new cjs.Text(".", "48px 'Myriad Pro'");
        this.text_dot_1.lineHeight = 77;
        this.text_dot_1.setTransform(305.5, 75.5);
        this.text_dot_2 = new cjs.Text(".", "48px 'Myriad Pro'");
        this.text_dot_2.lineHeight = 77;
        this.text_dot_2.setTransform(305.5, 224);

        this.addChild(this.roundRect1, this.text, this.text_1, this.rect1, this.rect2);
        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7, this.Line_prac_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.text_dot_1, this.text_dot_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 320.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(295.3, 87, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295.3, 327, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
