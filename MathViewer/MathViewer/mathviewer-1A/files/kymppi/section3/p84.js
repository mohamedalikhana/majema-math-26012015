(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p84_2.png",
            id: "p84_2"
        }, {
            src: "images/p84_1.png",
            id: "p84_1"
        }]
    };

    // symbols:
    (lib.p84_2 = function() {
        this.initialize(img.p84_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p84_1 = function() {
        this.initialize(img.p84_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("84", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(38, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Skriv talet som saknas så att svaret stämmer.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 1);

        var arrVal = ['6', '7', '8'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 1) {
                colSpace = 1.01;
            }
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(79 + (colSpace * 173.5), 56);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['3  +', '4  +', '5  +'];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 1) {
                colSpace = 0.965;
            } else if (index == 2) {
                colSpace = 1.93;
            }
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(67 + (colSpace * 180), 93);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['2  +', '5  +', '6  +'];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 1) {
                colSpace = 0.965;
            } else if (index == 2) {
                colSpace = 1.93;
            }
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(67 + (colSpace * 180), 122);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['4  +', '3  +', '7  +'];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 1) {
                colSpace = 0.965;
            } else if (index == 2) {
                colSpace = 1.93;
            }
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(67 + (colSpace * 180), 152);
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(100 + (columnSpace * 174), 87 + (row * 30), 20, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        arrVal = []
        arrVal = ['+  1', '+  7', '+  3'];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 1) {
                colSpace = 0.965;
            } else if (index == 2) {
                colSpace = 1.93;
            }
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(82 + (colSpace * 180), 183);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['+  6', '+  2', '+  2'];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 1) {
                colSpace = 0.965;
            } else if (index == 2) {
                colSpace = 1.93;
            }
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(82 + (colSpace * 180), 213);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['+  4', '+  1', '+  4'];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            if (index == 1) {
                colSpace = 0.965;
            } else if (index == 2) {
                colSpace = 1.92;
            }
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(82 + (colSpace * 180), 243);
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(55 + (columnSpace * 174), 177 + (row * 30), 20, 24);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.instance = new lib.p84_1();
        this.instance.setTransform(0, 20, 0.24, 0.24);

        this.instance1 = new lib.p84_1();
        this.instance1.setTransform(175, 20, 0.24, 0.24);

        this.instance2 = new lib.p84_1();
        this.instance2.setTransform(347, 20, 0.24, 0.24);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FFFFFF').drawRoundRect(0, 0, ((2107 * 0.5) - 10) * 0.5, 522 * 0.5, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.textbox_group2);
        this.addChild(this.instance, this.instance1, this.instance2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("Hitta vägen med rätt svar.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p84_2();
        this.instance.setTransform(22, 25, 0.312, 0.31);

        this.label1 = new cjs.Text("3 + 3", "16px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(50, 43);
        this.label2 = new cjs.Text("4 + 1", "16px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(129, 43);
        this.label3 = new cjs.Text("4 + 3", "16px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(208, 43);
        this.label4 = new cjs.Text("6 + 2", "16px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(288, 43);
        this.label5 = new cjs.Text("7 + 0", "16px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(366, 43);

        this.label6 = new cjs.Text("2 + 4", "16px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(50, 87);
        this.label7 = new cjs.Text("5 + 2", "16px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(129, 87);
        this.label8 = new cjs.Text("4 + 4", "16px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(208, 87);
        this.label9 = new cjs.Text("2 + 5", "16px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(288, 87);
        this.label10 = new cjs.Text("1 + 7", "16px 'Myriad Pro'");
        this.label10.lineHeight = 30;
        this.label10.setTransform(366, 87);

        this.label11 = new cjs.Text("3 + 4", "16px 'Myriad Pro'");
        this.label11.lineHeight = 30;
        this.label11.setTransform(50, 132);
        this.label12 = new cjs.Text("3 + 5", "16px 'Myriad Pro'");
        this.label12.lineHeight = 30;
        this.label12.setTransform(129, 132);
        this.label13 = new cjs.Text("3 + 3", "16px 'Myriad Pro'");
        this.label13.lineHeight = 30;
        this.label13.setTransform(208, 132);
        this.label14 = new cjs.Text("4 + 2", "16px 'Myriad Pro'");
        this.label14.lineHeight = 30;
        this.label14.setTransform(288, 132);
        this.label15 = new cjs.Text("1 + 5", "16px 'Myriad Pro'");
        this.label15.lineHeight = 30;
        this.label15.setTransform(366, 132);

        this.label16 = new cjs.Text("5 + 3", "16px 'Myriad Pro'");
        this.label16.lineHeight = 30;
        this.label16.setTransform(50, 177);
        this.label17 = new cjs.Text("6 + 1", "16px 'Myriad Pro'");
        this.label17.lineHeight = 30;
        this.label17.setTransform(129, 177);
        this.label18 = new cjs.Text("2 + 4", "16px 'Myriad Pro'");
        this.label18.lineHeight = 30;
        this.label18.setTransform(208, 177);
        this.label19 = new cjs.Text("3 + 2", "16px 'Myriad Pro'");
        this.label19.lineHeight = 30;
        this.label19.setTransform(288, 177);
        this.label20 = new cjs.Text("1 + 4", "16px 'Myriad Pro'");
        this.label20.lineHeight = 30;
        this.label20.setTransform(366, 177);

        this.label21 = new cjs.Text("2 + 5", "16px 'Myriad Pro'");
        this.label21.lineHeight = 30;
        this.label21.setTransform(50, 221);
        this.label22 = new cjs.Text("2 + 6", "16px 'Myriad Pro'");
        this.label22.lineHeight = 30;
        this.label22.setTransform(129, 221);
        this.label23 = new cjs.Text("0 + 6", "16px 'Myriad Pro'");
        this.label23.lineHeight = 30;
        this.label23.setTransform(208, 221);
        this.label24 = new cjs.Text("7 + 1", "16px 'Myriad Pro'");
        this.label24.lineHeight = 30;
        this.label24.setTransform(288, 221);
        this.label25 = new cjs.Text("3 + 4", "16px 'Myriad Pro'");
        this.label25.lineHeight = 30;
        this.label25.setTransform(366, 221);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#ffffff').drawRoundRect(0, 0, 460, 285, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text, this.text_1, this.instance);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label6, this.label7, this.label8, this.label9, this.label10);
        this.addChild(this.label11, this.label12, this.label13, this.label14, this.label15);
        this.addChild(this.label16, this.label17, this.label18, this.label19, this.label20);
        this.addChild(this.label21, this.label22, this.label23, this.label24, this.label25);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 290);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(289.3, 95.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(294.3, 367.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
