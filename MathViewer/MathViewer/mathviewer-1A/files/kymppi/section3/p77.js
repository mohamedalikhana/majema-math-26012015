(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p77_1.png",
            id: "p77_1"
        }]
    };

    (lib.p77_1 = function() {
        this.initialize(img.p77_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("77", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.instance = new lib.p77_1();
        this.instance.setTransform(52, 50, 0.786, 0.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(468 + (columnSpace * 33), 28, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Använd talen och skriv 2 olika additioner.", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(24, 1);

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(4, 0);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#C7C8CA").ss(1).drawRoundRect(5, 27, 515, 347, 10);
        this.round_Rect1.setTransform(0, 0);

        var arrTxtbox = ['110', '147', '275', '312'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding = 40,
            yPos,
            rectWidth = 110,
            rectHeight = 23,
            boxWidth = 22,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 3;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 40;
                if (i == 1) {
                    padding = 50;
                } else if (i == 2) {
                    padding = 53;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.5).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.5).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        arrTxtbox = [];
        arrTxtbox = ['65', '230'];

        this.textbox_group2 = new cjs.Shape();
        for (var i = 0; i < arrTxtbox.length; i++) {
            var yPos = parseInt(arrTxtbox[i]);
            var fColor, sColor;

            if (i == 0) {
                fColor = '#D8E9C0';
                sColor = '#88C240';
            } else {
                fColor = '#B4E3ED';
                sColor = '#00B0CA';
            }

            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) {
                    if (column == 2 && row == 2) {
                        continue;
                    }
                    this.textbox_group2.graphics.f(fColor).s(sColor).ss(0.7).drawRect(40 + (columnSpace * 43), yPos, 23, 24);
                }
            }
        }
        this.textbox_group2.setTransform(0, 0);

        arrTxtbox = [];
        arrTxtbox = ['65', '230'];

        this.textbox_group3 = new cjs.Shape();
        for (var i = 0; i < arrTxtbox.length; i++) {
            var yPos = parseInt(arrTxtbox[i]);
            var fColor, sColor;

            if (i == 0) {
                fColor = '#FDCDB0';
                sColor = '#E76234';
            } else {
                fColor = '#F3C2C9';
                sColor = '#D93B71';
            }

            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) {
                    if (column == 2 && row == 2) {
                        continue;
                    }
                    this.textbox_group3.graphics.f(fColor).s(sColor).ss(0.7).drawRect(210 + (columnSpace * 43), yPos, 23, 24);
                }
            }
        }
        this.textbox_group3.setTransform(0, 0);

        arrTxtbox = [];
        arrTxtbox = ['65', '230'];

        this.textbox_group4 = new cjs.Shape();
        for (var i = 0; i < arrTxtbox.length; i++) {
            var yPos = parseInt(arrTxtbox[i]);
            var fColor, sColor;

            if (i == 0) {
                fColor = '#D5D5EB';
                sColor = '#807FBC';

            } else {
                fColor = '#ACD6F3';
                sColor = '#807FBC';
            }

            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) {
                    if (column == 2 && row == 2) {
                        continue;
                    }
                    this.textbox_group4.graphics.f(fColor).s(sColor).ss(0.7).drawRect(378 + (columnSpace * 43), yPos, 23, 24);
                }
            }
        }
        this.textbox_group4.setTransform(0, 0);

        var arrVal = ['3', '2', '5'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {

            this.tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            this.tempLabel.lineHeight = 1;
            this.tempLabel.setTransform(47 + (index * 43), 71);
            ToBeAdded.push(this.tempLabel);
        }

        arrVal = [];
        arrVal = ['5', '7', '2'];

        for (var index = 0; index < arrVal.length; index++) {

            this.tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            this.tempLabel.lineHeight = 1;
            this.tempLabel.setTransform(47 + (index * 43), 236);
            ToBeAdded.push(this.tempLabel);
        }

        arrVal = [];
        arrVal = ['4', '2', '6'];
        for (var index = 0; index < arrVal.length; index++) {

            this.tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            this.tempLabel.lineHeight = 1;
            this.tempLabel.setTransform(217 + (index * 43), 71);
            ToBeAdded.push(this.tempLabel);
        }

        arrVal = [];
        arrVal = ['7', '3', '4'];

        for (var index = 0; index < arrVal.length; index++) {

            this.tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            this.tempLabel.lineHeight = 1;
            this.tempLabel.setTransform(217 + (index * 43), 236);
            ToBeAdded.push(this.tempLabel);
        }

        arrVal = [];
        arrVal = ['1', '6', '7'];
        for (var index = 0; index < arrVal.length; index++) {

            this.tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            this.tempLabel.lineHeight = 1;
            this.tempLabel.setTransform(384 + (index * 43), 71);
            ToBeAdded.push(this.tempLabel);
        }

        arrVal = [];
        arrVal = ['1', '4', '3'];

        for (var index = 0; index < arrVal.length; index++) {

            this.tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            this.tempLabel.lineHeight = 1;
            this.tempLabel.setTransform(384 + (index * 43), 236);
            ToBeAdded.push(this.tempLabel);
        }

        this.text_1 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(40, 100.5);

        this.text_2 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(64, 100.5);

        this.text_3 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(84, 100.5);

        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(108, 100.5);

        this.text_5 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(130, 100.5);

        this.hrRule = new cjs.Shape();
        this.hrRule.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(40, 200).lineTo(490, 200);
        this.hrRule.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(180, 58).lineTo(180, 185).moveTo(349, 58).lineTo(349, 185).moveTo(180, 223).lineTo(180, 350).moveTo(349, 223).lineTo(349, 350);
        this.Line_1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text, this.round_Rect1, this.textbox_group1, this.textbox_group2, this.textbox_group3, this.textbox_group4);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.hrRule, this.Line_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(610.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(306, 480, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
