(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };


    // symbols:

    (lib.Symbol22 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Talet 3", "24px 'MyriadPro-Semibold'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(105.5, 24);

        this.text_1 = new cjs.Text("4", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(67.5, 21);

        this.text_2 = new cjs.Text("15", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(555, 648);

        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 0 till 4", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(392, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(48, 23.5, 1.1, 1);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.addChild(this.shape_1, this.shape, this.pageBottomText, this.text_2, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("tre", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(46.9, 81.9);

        this.text_1 = new cjs.Text("3", "bold 80px 'UusiTekstausMajema'");
        this.text_1.lineHeight = 96;
        this.text_1.setTransform(38.1, 4);
        // arc above num
        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f("#000000").s().p("AgegVIA9AVIg9AXg");
        this.shape_42.setTransform(58.2, 23.5);
        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AgsAhQAVgfAZgPQAZgQAXAA");
        this.shape_43.setTransform(51.6, 26.5);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#000000").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape.setTransform(433.3, 57.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D7172F").s().p("AhmALIAAgVIDOAAIAAAVg");
        this.shape_1.setTransform(443.8, 57.6);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(433.4, 43.4);

        this.text_2 = new cjs.Text("12", "10px 'Myriad Pro'");
        this.text_2.lineHeight = 12;
        this.text_2.setTransform(426.2, 24.3);

        this.text_3 = new cjs.Text("11", "10px 'Myriad Pro'");
        this.text_3.lineHeight = 12;
        this.text_3.setTransform(412.5, 28.5);

        this.text_4 = new cjs.Text("10", "10px 'Myriad Pro'");
        this.text_4.lineHeight = 12;
        this.text_4.setTransform(402.5, 37.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C6CBCC").s().p("AkPBPQhxhuAAigIAfAAQAACTBoBmQBoBnCRAAQCSAABohnQBohmAAiTIAfAAQAACfhxBvQhxBxifAAQieAAhxhxg");
        this.shape_3.setTransform(433.3, 76.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#C6CBCC").s().p("AFiDAQAAiThohmQhohniSAAQiRAAhoBnQhoBmAACTIgfAAQAAifBxhvQBxhxCeAAQCfAABxBxQBxBvAACfg");
        this.shape_4.setTransform(433.3, 38.4);

        this.text_5 = new cjs.Text("9", "10px 'Myriad Pro'");
        this.text_5.lineHeight = 12;
        this.text_5.setTransform(398.9, 51.8);

        this.text_6 = new cjs.Text("8", "10px 'Myriad Pro'");
        this.text_6.lineHeight = 12;
        this.text_6.setTransform(403.6, 66.3);

        this.text_7 = new cjs.Text("7", "10px 'Myriad Pro'");
        this.text_7.lineHeight = 12;
        this.text_7.setTransform(413.5, 76.4);

        this.text_8 = new cjs.Text("6", "10px 'Myriad Pro'");
        this.text_8.lineHeight = 12;
        this.text_8.setTransform(429, 81.1);

        this.text_9 = new cjs.Text("5", "10px 'Myriad Pro'");
        this.text_9.lineHeight = 12;
        this.text_9.setTransform(443.3, 76.8);

        this.text_10 = new cjs.Text("4", "10px 'Myriad Pro'");
        this.text_10.lineHeight = 12;
        this.text_10.setTransform(454.4, 66.6);

        this.text_11 = new cjs.Text("3", "10px 'Myriad Pro'");
        this.text_11.lineHeight = 12;
        this.text_11.setTransform(459, 52.5);

        this.text_12 = new cjs.Text("2", "10px 'Myriad Pro'");
        this.text_12.lineHeight = 12;
        this.text_12.setTransform(454.2, 37.9);

        this.text_13 = new cjs.Text("1", "10px 'Myriad Pro'");
        this.text_13.lineHeight = 12;
        this.text_13.setTransform(443.4, 28.2);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAgKIAAAV");
        this.shape_5.setTransform(433.4, 23.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEgIIAJAR");
        this.shape_6.setTransform(416.2, 27.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJgEIATAJ");
        this.shape_7.setTransform(403.7, 40.5);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgKAAIAVAA");
        this.shape_8.setTransform(399.1, 57.6);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgJAFIATgJ");
        this.shape_9.setTransform(403.7, 74.8);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AgEAJIAJgR");
        this.shape_10.setTransform(416.2, 87.3);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAAALIAAgV");
        this.shape_11.setTransform(433.4, 91.9);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFAJIgJgR");
        this.shape_12.setTransform(450.5, 87.3);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJAFIgRgJ");
        this.shape_13.setTransform(463, 74.8);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AALAAIgVAA");
        this.shape_14.setTransform(467.6, 57.7);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAJgEIgRAJ");
        this.shape_15.setTransform(463, 40.5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.4, 0, 0, 4).p("AAFgIIgJAR");
        this.shape_16.setTransform(450.5, 27.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AFiAAQAACShoBoQhoBoiSAAQiRAAhohoQhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRg");
        this.shape_17.setTransform(433.3, 57.6);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("Aj5D6QhohoAAiSQAAiRBohoQBohoCRAAQCSAABoBoQBoBoAACRQAACShoBoQhoBoiSAAQiRAAhohog");
        this.shape_18.setTransform(433.3, 57.6);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#000000").ss(1.1, 0, 0, 4).p("AGBAAQAACfhxBxQhxBxifAAQieAAhxhxQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeg");
        this.shape_19.setTransform(433.3, 57.6);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AkPEQQhxhxAAifQAAieBxhxQBxhxCeAAQCfAABxBxQBxBxAACeQAACfhxBxQhxBxifAAQieAAhxhxg");
        this.shape_20.setTransform(433.3, 57.6);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_21.setTransform(238.3, 28.3);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_22.setTransform(238.3, 28.3);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAhAAAZgYQAYgYAAgiQAAghgYgYQgZgYghAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_23.setTransform(216.4, 28.3);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAYgYAhAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQghAAgYgYg");
        this.shape_24.setTransform(216.4, 28.3);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABSQAiAAAYgYQAYgYAAgiQAAghgYgYQgYgYgiAAQghAAgYAYQgYAYAAAhQAAAiAYAYQAYAYAhAAg");
        this.shape_25.setTransform(192.5, 28.3);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#0089CA").s().p("Ag5A6QgYgZAAghQAAghAYgYQAZgYAgAAQAiAAAYAYQAYAYAAAhQAAAhgYAZQgYAYgiAAQggAAgZgYg");
        this.shape_26.setTransform(192.5, 28.3);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("Ao1AAIRrAA");
        this.shape_27.setTransform(237.9, 39.6);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_28.setTransform(272.7, 39.8);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_29.setTransform(249.5, 39.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_30.setTransform(226.9, 39.8);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAjaIAAG1");
        this.shape_31.setTransform(204.4, 39.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AoIjfQgLAAgMAFQgWAMAAAcIAAFlIAFAXQAMAWAcAAIQRAAIAXgFQAWgMAAgcIAAllQAAgLgFgLQgMgXgcAAg");
        this.shape_32.setTransform(238.2, 39.6);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AoIDgQgcAAgMgWIgFgXIAAllQAAgcAWgMQAMgFALAAIQRAAQAcAAAMAXQAFALAAALIAAFlQAAAcgWAMIgXAFg");
        this.shape_33.setTransform(238.2, 39.6);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAHAAAEgEQAFgFAAgGQAAgFgFgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_34.setTransform(248.3, 75);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#000000").s().p("AgKALQgFgFABgGQgBgFAFgFQAFgEAFAAQAGAAAFAEQAFAFgBAFQABAGgFAFQgEAEgHAAQgFAAgFgEg");
        this.shape_35.setTransform(248.3, 75);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_36.setTransform(219.5, 75);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_37.setTransform(219.5, 75);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAAAPQAGAAAFgEQAEgFAAgGQAAgFgEgFQgFgEgGAAQgFAAgFAEQgEAFAAAFQAAAGAEAFQAFAEAFAAg");
        this.shape_38.setTransform(191.2, 75);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#000000").s().p("AgKALQgEgFAAgGQAAgFAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgFAAgFgEg");
        this.shape_39.setTransform(191.2, 75);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f("#000000").s().p("AgegWIA9AWIg9AXg");
        this.shape_40.setTransform(268.6, 74.9);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AmoAAINSAA");
        this.shape_41.setTransform(224.4, 74.9);

        this.text_14 = new cjs.Text("3", "bold 16px 'Myriad Pro'", "#0089CA");
        this.text_14.lineHeight = 19;
        this.text_14.setTransform(241.6, 75.6);

        this.text_15 = new cjs.Text("2", "bold 16px 'Myriad Pro'");
        this.text_15.lineHeight = 19;
        this.text_15.setTransform(212.5, 75.6);

        this.text_16 = new cjs.Text("1", "bold 16px 'Myriad Pro'");
        this.text_16.lineHeight = 19;
        this.text_16.setTransform(185, 75.6);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gHZQgdAAgcAOQg5AdAABHIAALQIACARQAEAWAIARQAdA5BHAAMBL/AAAIASgCQAVgEASgIQA5gdAAhGIAArQQAAgdgOgcQgdg5hHAAg");
        this.shape_44.setTransform(254.6, 56.9);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFF173").s().p("Egl/AHaQhHAAgdg5QgIgRgEgWIgCgRIAArQQAAhHA5gdQAcgOAdAAMBL/AAAQBHAAAdA5QAOAcAAAdIAALQQAABGg5AdQgSAIgVAEIgSACg");
        this.shape_45.setTransform(254.6, 56.9);

        var textArr = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(0.6).f("#000000").s("#000000").moveTo(130, 66).lineTo(330.5, 66).moveTo(330.5, 66).lineTo(330.5, 64).lineTo(337, 66).lineTo(330.5, 68).lineTo(330.5, 66);
        var numberLineLimit = 3
        for (var dot = 0; dot < 11; dot++) {
            var strokeColor = "#000000";
            if (numberLineLimit === dot) {
                strokeColor = "#00B2CA";
            }
            this.numberLine.graphics.f("#000000").ss(0.6).s(strokeColor).moveTo(130 + (20 * dot), 61).lineTo(130 + (20 * dot), 71);
        }

        this.numberLine.graphics.f("#00B2CA").ss(3.5).s("#00B2CA").moveTo(129, 66).lineTo(130 + (20 * numberLineLimit), 66);

        for (var dot = 0; dot < 11; dot++) {
            var temptext = null;
            if (numberLineLimit === dot) {
                temptext = new cjs.Text("" + dot, "bold 12px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + dot, "12px 'Myriad Pro'");
            }
            temptext.lineHeight = -1;
            var colSpace = (dot == 10) ? 9.8 : dot;
            temptext.setTransform(127 + (20 * colSpace), 88);
            textArr.push(temptext);
        }
        this.numberLine.setTransform(0, 15);

        this.addChild(this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.shape_4, this.shape_3, this.text_4, this.text_3, this.text_2, this.shape_2, this.shape_1, this.shape, this.text_1, this.text, this.numberLine);
        for (var textEl = 0; textEl < textArr.length; textEl++) {
            this.addChild(textArr[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 511.3, 105.3);


    (lib.Symbol5 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Hur många?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 5.2);

        this.text_1 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_1.lineHeight = 20;
        this.text_1.setTransform(0, 4);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAoAcAcQAcAcAnAAg");
        this.shape.setTransform(317.6, 166.1);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_1.setTransform(317.6, 166.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_2.setTransform(437.5, 83.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_3.setTransform(437.5, 83.1);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_4.setTransform(437.9, 59.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_5.setTransform(437.9, 59.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_6.setTransform(52.3, 176.7);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_7.setTransform(52.3, 176.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_8.setTransform(36.3, 158.5);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_9.setTransform(36.3, 158.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_10.setTransform(182.7, 149.4);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_11.setTransform(182.7, 149.4);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_12.setTransform(170.7, 166.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgmAcgdQAdgcAmAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_13.setTransform(170.7, 166.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_14.setTransform(159.4, 184.8);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_15.setTransform(159.4, 184.8);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_16.setTransform(296.2, 177.1);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_17.setTransform(296.2, 177.1);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_18.setTransform(441.2, 168.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_19.setTransform(441.2, 168.4);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_20.setTransform(181.6, 74.1);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_21.setTransform(181.6, 74.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_22.setTransform(296.2, 154.3);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_23.setTransform(296.2, 154.3);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_24.setTransform(306.4, 58.6);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgmAcgdQAdgcAmAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_25.setTransform(306.4, 58.6);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_26.setTransform(292.2, 80.9);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_27.setTransform(292.2, 80.9);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_28.setTransform(319.1, 80.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_29.setTransform(319.1, 80.9);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_30.setTransform(31.8, 73.4);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_31.setTransform(31.8, 73.4);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#6C7373").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_32.setTransform(56.4, 73.4);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_33.setTransform(56.4, 73.4);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_34.setTransform(474.1, 169.9);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_35.setTransform(474.1, 169.9);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_36.setTransform(474, 72.2);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_37.setTransform(474, 72.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_38.setTransform(351.7, 169.9);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_39.setTransform(351.7, 169.9);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_40.setTransform(351.3, 73.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_41.setTransform(351.3, 73.5);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_42.setTransform(212.2, 169.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_43.setTransform(212.2, 169.4);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_44.setTransform(88, 169.4);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_45.setTransform(88, 169.4);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_46.setTransform(212.4, 73.2);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_47.setTransform(212.4, 73.2);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjcIi/AAg");
        this.shape_48.setTransform(88.5, 72.7);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_49.setTransform(88.5, 72.7);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgHAAgLACQgVAEgSAIQg5AdAABHIAAJtIACASQAEAVAIASQAdA5BHAAIPVAAQAdAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hHAAg");
        this.shape_50.setTransform(451.7, 165.7);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFFFFF").s().p("AnqGpQhHAAgdg5QgIgSgEgVIgCgSIAAptQAAhHA5gdQASgIAVgEQALgCAHAAIPVAAQBHAAAdA5QAOAcAAAdIAAJtQAABHg5AdQgcAOgdAAg");
        this.shape_51.setTransform(451.7, 165.7);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgdAAgcAOQg5AdAABHIAAJtIACASQAEAVAIASQAdA5BHAAIPVAAQAdAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hHAAg");
        this.shape_52.setTransform(451.7, 71.4);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#FFFFFF").s().p("AnqGpQhHAAgdg5QgIgSgEgVIgCgSIAAptQAAhHA5gdQAcgOAdAAIPVAAQBHAAAdA5QAOAcAAAdIAAJtQAABHg5AdQgcAOgdAAg");
        this.shape_53.setTransform(451.7, 71.4);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgHAAgLACQgVAEgSAIQg5AdAABHIAAJtIACASQAEAVAIASQAdA5BHAAIPVAAQAdAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hHAAg");
        this.shape_54.setTransform(192.3, 165.7);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#FFFFFF").s().p("AnqGpQhHAAgdg5QgIgSgEgVIgCgSIAAptQAAhHA5gdQASgIAVgEQALgCAHAAIPVAAQBHAAAdA5QAOAcAAAdIAAJtQAABHg5AdQgcAOgdAAg");
        this.shape_55.setTransform(192.3, 165.7);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgHAAgLACQgVAEgSAIQg5AdAABHIAAJtIACASQAEAVAIASQAdA5BHAAIPVAAQAdAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hHAAg");
        this.shape_56.setTransform(192.3, 71.4);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("AnqGpQhHAAgdg5QgIgSgEgVIgCgSIAAptQAAhHA5gdQASgIAVgEQALgCAHAAIPVAAQBHAAAdA5QAOAcAAAdIAAJtQAABHg5AdQgcAOgdAAg");
        this.shape_57.setTransform(192.3, 71.4);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgHAAgLACQgVAEgSAIQg5AdAABHIAAJtIADASQADAVAJASQAcA5BHAAIPWAAQAcAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hGAAg");
        this.shape_58.setTransform(321.9, 165.7);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#FFFFFF").s().p("AnrGpQhGAAgdg5QgIgSgDgVIgDgSIAAptQAAhHA5gdQASgIAVgEQAKgCAHAAIPWAAQBHAAAcA5QAOAcABAdIAAJtQgBBHg4AdQgcAOgdAAg");
        this.shape_59.setTransform(321.9, 165.7);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgHAAgLACQgVAEgSAIQg5AdAABHIAAJtIADASQADAVAJASQAcA5BHAAIPWAAQAcAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hGAAg");
        this.shape_60.setTransform(321.9, 71.4);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AnrGpQhGAAgdg5QgIgSgDgVIgDgSIAAptQAAhHA5gdQASgIAVgEQAKgCAHAAIPWAAQBHAAAcA5QAOAcABAdIAAJtQgBBHg4AdQgcAOgdAAg");
        this.shape_61.setTransform(321.9, 71.4);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgHAAgLACQgVAEgSAIQg5AdAABHIAAJtIACASQAEAVAIASQAdA5BHAAIPVAAQAdAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hHAAg");
        this.shape_62.setTransform(62.5, 165.7);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AnqGpQhHAAgdg5QgIgSgEgVIgCgSIAAptQAAhHA5gdQASgIAVgEQALgCAHAAIPVAAQBHAAAdA5QAOAcAAAdIAAJtQAABHg5AdQgcAOgdAAg");
        this.shape_63.setTransform(62.5, 165.7);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AnqmoQgHAAgLACQgVAEgSAIQg5AdAABHIAAJtIACASQAEAVAIASQAdA5BHAAIPVAAQAdAAAcgOQA5gdAAhHIAAptQAAgdgOgcQgdg5hHAAg");
        this.shape_64.setTransform(62.5, 71.4);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AnqGpQhHAAgdg5QgIgSgEgVIgCgSIAAptQAAhHA5gdQASgIAVgEQALgCAHAAIPVAAQBHAAAdA5QAOAcAAAdIAAJtQAABHg5AdQgcAOgdAAg");
        this.shape_65.setTransform(62.5, 71.4);

        this.addChild(this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_1, this.text, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.ra6, this.ra7, this.ra8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 209.2);


    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 2


        // Layer 1
        this.text = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text.lineHeight = 44;
        this.text.setTransform(42.5, 31.4);

        this.text_1 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 44;
        this.text_1.setTransform(289, 31.4);

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 44;
        this.text_2.setTransform(226.1, 31.4);

        this.text_3 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 44;
        this.text_3.setTransform(144, 31.4);

        this.text_4 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 44;
        this.text_4.setTransform(61.9, 31.4);

        this.text_5 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 44;
        this.text_5.setTransform(124.4, 31.4);

        this.text_6 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 44;
        this.text_6.setTransform(351, 31.4);

        this.text_7 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_7.lineHeight = 44;
        this.text_7.setTransform(472.6, 31.4);

        this.text_8 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_8.lineHeight = 44;
        this.text_8.setTransform(187, 31.4);

        this.text_9 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_9.lineHeight = 44;
        this.text_9.setTransform(23.2, 31.4);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape.setTransform(482.9, 64);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_1.setTransform(462.9, 64);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_2.setTransform(442.9, 64);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_3.setTransform(492.7, 52.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_4.setTransform(472.8, 52.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_5.setTransform(482.9, 41.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_6.setTransform(453, 52.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_7.setTransform(462.9, 41.3);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_8.setTransform(433.1, 52.6);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_9.setTransform(442.9, 41.3);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_10.setTransform(400.7, 64);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_11.setTransform(380.7, 64);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_12.setTransform(360.7, 64);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_13.setTransform(410.5, 52.6);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_14.setTransform(390.6, 52.6);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_15.setTransform(400.7, 41.3);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_16.setTransform(370.8, 52.6);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_17.setTransform(380.7, 41.3);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_18.setTransform(350.9, 52.6);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_19.setTransform(360.7, 41.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_20.setTransform(318.5, 64);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_21.setTransform(298.5, 64);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_22.setTransform(278.5, 64);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_23.setTransform(328.3, 52.6);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_24.setTransform(308.4, 52.6);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_25.setTransform(318.5, 41.3);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_26.setTransform(288.6, 52.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_27.setTransform(298.5, 41.3);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_28.setTransform(268.7, 52.6);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_29.setTransform(278.5, 41.3);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_30.setTransform(236.2, 64);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_31.setTransform(216.3, 64);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_32.setTransform(196.3, 64);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_33.setTransform(246, 52.6);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_34.setTransform(226.2, 52.6);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_35.setTransform(236.2, 41.3);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_36.setTransform(206.4, 52.6);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_37.setTransform(216.3, 41.3);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_38.setTransform(186.5, 52.6);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_39.setTransform(196.3, 41.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_40.setTransform(154, 64);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_41.setTransform(134.1, 64);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_42.setTransform(114.1, 64);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_43.setTransform(163.8, 52.6);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_44.setTransform(144, 52.6);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_45.setTransform(154, 41.3);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_46.setTransform(124.2, 52.6);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_47.setTransform(134.1, 41.3);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_48.setTransform(104.3, 52.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_49.setTransform(114.1, 41.3);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_50.setTransform(71.8, 64);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_51.setTransform(51.9, 64);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_52.setTransform(31.9, 64);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_53.setTransform(81.6, 52.6);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_54.setTransform(61.8, 52.6);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_55.setTransform(71.8, 41.3);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_56.setTransform(42, 52.6);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5).p("AhhAAIDEAA");
        this.shape_57.setTransform(51.9, 41.3);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_58.setTransform(22.1, 52.6);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_59.setTransform(31.9, 41.3);

        this.text_10 = new cjs.Text("Skriv talen som saknas.", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(19, 5);

        this.text_11 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_11.lineHeight = 20;
        this.text_11.setTransform(0, 4);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgDcQgHAAgLABQgVAEgSAJQg5AcAABHIAADXIACASQAEAVAIASQAdA5BHAAMBMJAAAIASgCQAVgEASgIQA5gdAAhHIAAjXQAAgcgOgdQgdg4hHAAg");
        this.shape_60.setTransform(257.1, 51.5);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("EgmEADeQhHAAgdg5QgIgSgEgVIgCgSIAAjXQAAhHA5gcQASgJAVgEQALgCAHAAMBMJAAAQBHABAdA4QAOAdAAAcIAADXQAABHg5AcQgSAJgVADIgSADg");
        this.shape_61.setTransform(257.1, 51.5);

        this.addChild(this.shape_61, this.shape_60, this.text_11, this.text_10, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 77.4);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text.lineHeight = 55;
        this.text.setTransform(214.8, 3.1);

        this.text_1 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_1.lineHeight = 55;
        this.text_1.setTransform(424.7, 3.1);

        this.text_2 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_2.lineHeight = 55;
        this.text_2.setTransform(177.3, 3.1);

        this.text_3 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_3.lineHeight = 55;
        this.text_3.setTransform(138.4, 3.1);

        this.text_4 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_4.lineHeight = 55;
        this.text_4.setTransform(349.2, 3.1);

        this.text_5 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_5.lineHeight = 55;
        this.text_5.setTransform(100.8, 3.1);

        this.text_6 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_6.lineHeight = 55;
        this.text_6.setTransform(62.5, 3.1);

        this.text_7 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_7.lineHeight = 46;
        this.text_7.setTransform(440.5, 132.5);

        this.text_8 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_8.lineHeight = 46;
        this.text_8.setTransform(440.5, 103.1);

        this.text_9 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_9.lineHeight = 46;
        this.text_9.setTransform(440.5, 72.3);

        this.text_10 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_10.lineHeight = 46;
        this.text_10.setTransform(440.5, 41.5);

        this.text_11 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_11.lineHeight = 46;
        this.text_11.setTransform(192, 132.5);

        this.text_12 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_12.lineHeight = 46;
        this.text_12.setTransform(192, 103.1);

        this.text_13 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_13.lineHeight = 46;
        this.text_13.setTransform(192, 72.3);

        this.text_14 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_14.lineHeight = 46;
        this.text_14.setTransform(192, 41.5);

        this.text_15 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_15.lineHeight = 46;
        this.text_15.setTransform(383.8, 132.5);

        this.text_16 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_16.lineHeight = 46;
        this.text_16.setTransform(383.8, 103.1);

        this.text_17 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_17.lineHeight = 46;
        this.text_17.setTransform(383.8, 72.3);

        this.text_18 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_18.lineHeight = 46;
        this.text_18.setTransform(383.8, 41.5);

        this.text_19 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_19.lineHeight = 46;
        this.text_19.setTransform(135.3, 132.5);

        this.text_20 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_20.lineHeight = 46;
        this.text_20.setTransform(135.3, 103.1);

        this.text_21 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_21.lineHeight = 46;
        this.text_21.setTransform(135.3, 72.3);

        this.text_22 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_22.lineHeight = 46;
        this.text_22.setTransform(135.3, 41.5);

        this.text_23 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_23.lineHeight = 46;
        this.text_23.setTransform(327.7, 132.5);

        this.text_24 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_24.lineHeight = 46;
        this.text_24.setTransform(327.7, 103.1);

        this.text_25 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_25.lineHeight = 46;
        this.text_25.setTransform(327.7, 72.3);

        this.text_26 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_26.lineHeight = 46;
        this.text_26.setTransform(327.7, 41.5);

        this.text_27 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_27.lineHeight = 46;
        this.text_27.setTransform(78.3, 132.5);

        this.text_28 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_28.lineHeight = 46;
        this.text_28.setTransform(78.3, 103.1);

        this.text_29 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_29.lineHeight = 46;
        this.text_29.setTransform(78.3, 72.3);

        this.text_30 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_30.lineHeight = 46;
        this.text_30.setTransform(78.3, 41.5);

        this.text_31 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_31.lineHeight = 46;
        this.text_31.setTransform(271.4, 132.5);

        this.text_32 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_32.lineHeight = 46;
        this.text_32.setTransform(271.4, 103.1);

        this.text_33 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_33.lineHeight = 46;
        this.text_33.setTransform(271.4, 72.3);

        this.text_34 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_34.lineHeight = 46;
        this.text_34.setTransform(271.4, 41.5);

        this.text_35 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_35.lineHeight = 46;
        this.text_35.setTransform(22, 132.5);

        this.text_36 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_36.lineHeight = 46;
        this.text_36.setTransform(22, 103.1);

        this.text_37 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_37.lineHeight = 46;
        this.text_37.setTransform(22, 72.3);

        this.text_38 = new cjs.Text("3", "bold 35.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_38.lineHeight = 46;
        this.text_38.setTransform(22, 41.5);

        this.text_39 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_39.lineHeight = 55;
        this.text_39.setTransform(273.7, 3.1);

        this.text_40 = new cjs.Text("3", "bold 45.5px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_40.lineHeight = 55;
        this.text_40.setTransform(24.3, 3.1);

        this.text_41 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_41.lineHeight = 12;
        this.text_41.setTransform(474.2, 140.4);

        this.text_42 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_42.lineHeight = 12;
        this.text_42.setTransform(474.2, 111);

        this.text_43 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_43.lineHeight = 12;
        this.text_43.setTransform(474.2, 79.9);

        this.text_44 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_44.lineHeight = 12;
        this.text_44.setTransform(474.2, 49.2);

        this.text_45 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_45.lineHeight = 12;
        this.text_45.setTransform(225.3, 140.4);

        this.text_46 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_46.lineHeight = 12;
        this.text_46.setTransform(225.3, 111);

        this.text_47 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_47.lineHeight = 12;
        this.text_47.setTransform(225.3, 79.9);

        this.text_48 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_48.lineHeight = 12;
        this.text_48.setTransform(225.3, 49.2);

        this.text_49 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_49.lineHeight = 12;
        this.text_49.setTransform(417.5, 140.4);

        this.text_50 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_50.lineHeight = 12;
        this.text_50.setTransform(417.5, 111);

        this.text_51 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_51.lineHeight = 12;
        this.text_51.setTransform(417.5, 79.9);

        this.text_52 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_52.lineHeight = 12;
        this.text_52.setTransform(417.5, 49.2);

        this.text_53 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_53.lineHeight = 12;
        this.text_53.setTransform(168.6, 140.4);

        this.text_54 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_54.lineHeight = 12;
        this.text_54.setTransform(168.6, 111);

        this.text_55 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_55.lineHeight = 12;
        this.text_55.setTransform(168.6, 79.9);

        this.text_56 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_56.lineHeight = 12;
        this.text_56.setTransform(168.6, 49.2);

        this.text_57 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_57.lineHeight = 12;
        this.text_57.setTransform(360.3, 140.4);

        this.text_58 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_58.lineHeight = 12;
        this.text_58.setTransform(360.3, 111);

        this.text_59 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_59.lineHeight = 12;
        this.text_59.setTransform(360.3, 79.9);

        this.text_60 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_60.lineHeight = 12;
        this.text_60.setTransform(360.3, 49.2);

        this.text_61 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_61.lineHeight = 12;
        this.text_61.setTransform(111.4, 140.4);

        this.text_62 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_62.lineHeight = 12;
        this.text_62.setTransform(111.4, 111);

        this.text_63 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_63.lineHeight = 12;
        this.text_63.setTransform(111.4, 79.9);

        this.text_64 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_64.lineHeight = 12;
        this.text_64.setTransform(111.4, 49.2);

        this.text_65 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_65.lineHeight = 12;
        this.text_65.setTransform(304.1, 140.4);

        this.text_66 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_66.lineHeight = 12;
        this.text_66.setTransform(304.1, 111);

        this.text_67 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_67.lineHeight = 12;
        this.text_67.setTransform(304.1, 79.9);

        this.text_68 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_68.lineHeight = 12;
        this.text_68.setTransform(304.1, 49.2);

        this.text_69 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_69.lineHeight = 12;
        this.text_69.setTransform(55.2, 140.4);

        this.text_70 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_70.lineHeight = 12;
        this.text_70.setTransform(55.2, 111);

        this.text_71 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_71.lineHeight = 12;
        this.text_71.setTransform(55.2, 79.9);

        this.text_72 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_72.lineHeight = 12;
        this.text_72.setTransform(55.2, 49.2);

        this.text_73 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_73.lineHeight = 12;
        this.text_73.setTransform(469.7, 13.5);

        this.text_74 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_74.lineHeight = 12;
        this.text_74.setTransform(392.6, 13.5);

        this.text_75 = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
        this.text_75.lineHeight = 12;
        this.text_75.setTransform(317.6, 13.5);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape.setTransform(474.9, 29.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_1.setTransform(474.9, 29.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_2.setTransform(226.2, 29.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_3.setTransform(226.2, 29.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_4.setTransform(436.8, 29.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_5.setTransform(436.8, 29.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_6.setTransform(188.2, 29.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_7.setTransform(188.2, 29.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_8.setTransform(397.8, 29.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_9.setTransform(397.8, 29.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_10.setTransform(149.2, 29.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_11.setTransform(149.2, 29.9);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_12.setTransform(360.8, 29.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_13.setTransform(360.8, 29.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiJCLIEUAAIAAkVIkUAAg");
        this.shape_14.setTransform(112.2, 29.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEUAAIAAEVg");
        this.shape_15.setTransform(112.2, 29.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_16.setTransform(322.8, 29.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_17.setTransform(322.8, 29.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_18.setTransform(74.2, 29.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_19.setTransform(74.2, 29.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_20.setTransform(479, 154.2);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_21.setTransform(479, 154.2);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_22.setTransform(230.3, 154.2);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_23.setTransform(230.3, 154.2);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_24.setTransform(450.6, 154.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_25.setTransform(450.6, 154.2);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_26.setTransform(202, 154.2);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_27.setTransform(202, 154.2);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_28.setTransform(422.3, 154.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_29.setTransform(422.3, 154.2);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_30.setTransform(173.7, 154.2);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_31.setTransform(173.7, 154.2);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_32.setTransform(393.9, 154.2);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_33.setTransform(393.9, 154.2);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_34.setTransform(145.3, 154.2);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_35.setTransform(145.3, 154.2);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_36.setTransform(365.6, 154.2);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_37.setTransform(365.6, 154.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_38.setTransform(117, 154.2);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_39.setTransform(117, 154.2);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_40.setTransform(337.2, 154.2);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_41.setTransform(337.2, 154.2);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_42.setTransform(88.6, 154.2);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_43.setTransform(88.6, 154.2);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_44.setTransform(308.9, 154.2);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_45.setTransform(308.9, 154.2);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_46.setTransform(60.3, 154.2);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_47.setTransform(60.3, 154.2);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_48.setTransform(280.5, 154.2);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_49.setTransform(280.5, 154.2);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_50.setTransform(31.9, 154.2);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_51.setTransform(31.9, 154.2);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_52.setTransform(479, 124.5);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_53.setTransform(479, 124.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_54.setTransform(230.3, 124.5);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_55.setTransform(230.3, 124.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_56.setTransform(450.6, 124.5);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_57.setTransform(450.6, 124.5);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_58.setTransform(202, 124.5);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_59.setTransform(202, 124.5);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_60.setTransform(422.3, 124.5);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_61.setTransform(422.3, 124.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_62.setTransform(173.7, 124.5);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_63.setTransform(173.7, 124.5);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_64.setTransform(393.9, 124.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_65.setTransform(393.9, 124.5);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_66.setTransform(145.3, 124.5);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_67.setTransform(145.3, 124.5);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_68.setTransform(365.6, 124.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_69.setTransform(365.6, 124.5);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_70.setTransform(117, 124.5);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_71.setTransform(117, 124.5);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_72.setTransform(337.2, 124.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_73.setTransform(337.2, 124.5);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_74.setTransform(88.6, 124.5);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_75.setTransform(88.6, 124.5);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_76.setTransform(308.9, 124.5);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_77.setTransform(308.9, 124.5);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_78.setTransform(60.3, 124.5);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_79.setTransform(60.3, 124.5);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_80.setTransform(280.5, 124.5);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_81.setTransform(280.5, 124.5);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjcIi/AAg");
        this.shape_82.setTransform(31.9, 124.5);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_83.setTransform(31.9, 124.5);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_84.setTransform(479, 93.9);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_85.setTransform(479, 93.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_86.setTransform(230.3, 93.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_87.setTransform(230.3, 93.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_88.setTransform(450.6, 93.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_89.setTransform(450.6, 93.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_90.setTransform(202, 93.9);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_91.setTransform(202, 93.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_92.setTransform(422.3, 93.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_93.setTransform(422.3, 93.9);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_94.setTransform(173.7, 93.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_95.setTransform(173.7, 93.9);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_96.setTransform(393.9, 93.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_97.setTransform(393.9, 93.9);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_98.setTransform(145.3, 93.9);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_99.setTransform(145.3, 93.9);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_100.setTransform(365.6, 93.9);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_101.setTransform(365.6, 93.9);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_102.setTransform(117, 93.9);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_103.setTransform(117, 93.9);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_104.setTransform(337.2, 93.9);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_105.setTransform(337.2, 93.9);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_106.setTransform(88.6, 93.9);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_107.setTransform(88.6, 93.9);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_108.setTransform(308.9, 93.9);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_109.setTransform(308.9, 93.9);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_110.setTransform(60.3, 93.9);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_111.setTransform(60.3, 93.9);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_112.setTransform(280.5, 93.9);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_113.setTransform(280.5, 93.9);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_114.setTransform(31.9, 93.9);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_115.setTransform(31.9, 93.9);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_116.setTransform(479, 63.2);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_117.setTransform(479, 63.2);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_118.setTransform(230.3, 63.2);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_119.setTransform(230.3, 63.2);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_120.setTransform(450.6, 63.2);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_121.setTransform(450.6, 63.2);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_122.setTransform(202, 63.2);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_123.setTransform(202, 63.2);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_124.setTransform(422.3, 63.2);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_125.setTransform(422.3, 63.2);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_126.setTransform(173.7, 63.2);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_127.setTransform(173.7, 63.2);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_128.setTransform(393.9, 63.2);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_129.setTransform(393.9, 63.2);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_130.setTransform(145.3, 63.2);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_131.setTransform(145.3, 63.2);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_132.setTransform(365.6, 63.2);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_133.setTransform(365.6, 63.2);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_134.setTransform(117, 63.2);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_135.setTransform(117, 63.2);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_136.setTransform(337.2, 63.2);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_137.setTransform(337.2, 63.2);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_138.setTransform(88.6, 63.2);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_139.setTransform(88.6, 63.2);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_140.setTransform(308.9, 63.2);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_141.setTransform(308.9, 63.2);

        this.shape_142 = new cjs.Shape();
        this.shape_142.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_142.setTransform(60.3, 63.2);

        this.shape_143 = new cjs.Shape();
        this.shape_143.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_143.setTransform(60.3, 63.2);

        this.shape_144 = new cjs.Shape();
        this.shape_144.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_144.setTransform(280.5, 63.2);

        this.shape_145 = new cjs.Shape();
        this.shape_145.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_145.setTransform(280.5, 63.2);

        this.shape_146 = new cjs.Shape();
        this.shape_146.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBuIC/AAIAAjbIi/AAg");
        this.shape_146.setTransform(31.9, 63.2);

        this.shape_147 = new cjs.Shape();
        this.shape_147.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
        this.shape_147.setTransform(31.9, 63.2);

        this.shape_148 = new cjs.Shape();
        this.shape_148.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_148.setTransform(284.8, 29.9);

        this.shape_149 = new cjs.Shape();
        this.shape_149.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_149.setTransform(284.8, 29.9);

        this.shape_150 = new cjs.Shape();
        this.shape_150.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AiKCLIEVAAIAAkVIkVAAg");
        this.shape_150.setTransform(36.2, 29.9);

        this.shape_151 = new cjs.Shape();
        this.shape_151.graphics.f("#FFFFFF").s().p("AiKCLIAAkVIEVAAIAAEVg");
        this.shape_151.setTransform(36.2, 29.9);

        this.shape_152 = new cjs.Shape();
        this.shape_152.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmEgNNQgHAAgLACQgVADgSAJQg5AcAABHIAAW5IACARQAEAWAIASQAdA4BHAAMBMJAAAIASgCQAVgDASgJQA5gcAAhHIAA25QAAgcgOgdQgdg4hHAAg");
        this.shape_152.setTransform(255.1, 89.6);

        this.shape_153 = new cjs.Shape();
        this.shape_153.graphics.f("#FFFFFF").s().p("EgmEANOQhHAAgdg4QgIgSgEgWIgCgRIAA25QAAhHA5gcQASgJAVgDQALgCAHAAMBMJAAAQBHAAAdA4QAOAdAAAcIAAW5QAABHg5AcQgSAJgVADIgSACg");
        this.shape_153.setTransform(255.1, 89.6);

        this.addChild(this.shape_153, this.shape_152, this.shape_151, this.shape_150, this.shape_149, this.shape_148, this.shape_147, this.shape_146, this.shape_145, this.shape_144, this.shape_143, this.shape_142, this.shape_141, this.shape_140, this.shape_139, this.shape_138, this.shape_137, this.shape_136, this.shape_135, this.shape_134, this.shape_133, this.shape_132, this.shape_131, this.shape_130, this.shape_129, this.shape_128, this.shape_127, this.shape_126, this.shape_125, this.shape_124, this.shape_123, this.shape_122, this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_75, this.text_74, this.text_73, this.text_72, this.text_71, this.text_70, this.text_69, this.text_68, this.text_67, this.text_66, this.text_65, this.text_64, this.text_63, this.text_62, this.text_61, this.text_60, this.text_59, this.text_58, this.text_57, this.text_56, this.text_55, this.text_54, this.text_53, this.text_52, this.text_51, this.text_50, this.text_49, this.text_48, this.text_47, this.text_46, this.text_45, this.text_44, this.text_43, this.text_42, this.text_41, this.text_40, this.text_39, this.text_38, this.text_37, this.text_36, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.ra1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 512.3, 178.9);


    // stage content:
    (lib.p15 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol22();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v4 = new lib.Symbol5();
        this.v4.setTransform(311.8, 531.2, 1, 1, 0, 0, 0, 256.3, 104.2);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(311.8, 383.4, 1, 1, 0, 0, 0, 256.3, 38.7);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(312.6, 250.1, 1, 1, 0, 0, 0, 255.1, 89.4);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(312.6, 100.8, 1, 1, 0, 0, 0, 254.6, 52.5);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
