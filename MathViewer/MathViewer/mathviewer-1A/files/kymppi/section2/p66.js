(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p66_1.png",
            id: "p66_1"
        }, {
            src: "images/p66_2.png",
            id: "p66_2"
        }]
    };

    (lib.p66_1 = function() {
        this.initialize(img.p66_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p66_2 = function() {
        this.initialize(img.p66_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("66", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(34.5, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#87C140").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28, 660.8);

        this.instance = new lib.p66_1();
        this.instance.setTransform(34, 32, 0.737, 0.737);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(452 + (columnSpace * 33), 26, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Hur många kycklingar gömmer sig?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 14);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 14);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 255.5, 88, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260.5, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 118);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260.5, 118);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 211);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260.5, 211);

        this.roundRect7 = this.roundRect1.clone(true);
        this.roundRect7.setTransform(0, 304);

        this.roundRect8 = this.roundRect1.clone(true);
        this.roundRect8.setTransform(260.5, 304);

        this.text = new cjs.Text("3  +           =  5", "16.2px 'Myriad Pro'");
        this.text.lineHeight = 30;
        this.text.setTransform(75, 100);

        this.text_1 = new cjs.Text("4  +           =  6", "16.2px 'Myriad Pro'");
        this.text_1.lineHeight = 30;
        this.text_1.setTransform(75, 193);

        this.text_2 = new cjs.Text("3  +           =  6", "16.2px 'Myriad Pro'");
        this.text_2.lineHeight = 30;
        this.text_2.setTransform(75, 286);

        this.text_3 = new cjs.Text("5  +           =  6", "16.2px 'Myriad Pro'");
        this.text_3.lineHeight = 30;
        this.text_3.setTransform(75, 381);

        this.text_4 = new cjs.Text("+  3  =  5", "16.2px 'Myriad Pro'");
        this.text_4.lineHeight = 30;
        this.text_4.setTransform(366, 100);

        this.text_5 = new cjs.Text("+  3  =  6", "16.2px 'Myriad Pro'");
        this.text_5.lineHeight = 30;
        this.text_5.setTransform(366, 193);

        this.text_6 = new cjs.Text("+  2  =  4", "16.2px 'Myriad Pro'");
        this.text_6.lineHeight = 30;
        this.text_6.setTransform(366, 286);

        this.text_7 = new cjs.Text("+  2  =  6", "16.2px 'Myriad Pro'");
        this.text_7.lineHeight = 30;
        this.text_7.setTransform(366, 381);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (row == 2) {
                    rowSpace = 1.99;
                } else if (row == 3) {
                    rowSpace = 2.99;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(111 + (columnSpace * 228), 95 + (rowSpace * 94), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.instance = new lib.p66_2();
        this.instance.setTransform(5, 41, 0.475, 0.46);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.text, this.text_1);
        this.addChild(this.instance, this.textbox_group1);
        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7);
        this.addChild(this.text_q1, this.text_q2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 400.2);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(297, 446, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
