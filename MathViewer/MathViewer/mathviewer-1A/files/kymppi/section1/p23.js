(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/s24_25_94.png",
            id: "s24_25_94"
        }, {
            src: "images/s24_25_95.png",
            id: "s24_25_95"
        }, ]
    };
    // symbols:
    (lib.s24_25_94 = function() {
        this.initialize(img.s24_25_94);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 29, 29);

    (lib.s24_25_95 = function() {
        this.initialize(img.s24_25_95);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 21, 21);

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Större än och mindre än", "24px 'MyriadPro-Semibold'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(107, 24.5);

        this.text_1 = new cjs.Text("7", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(69.5, 21.2);

        this.text_2 = new cjs.Text("23", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(555, 648);

        this.pageBottomText = new cjs.Text("förstå och kunna använda symbolerna > och <", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(360, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(49, 23.5, 1.12, 1);

        this.addChild(this.shape_1, this.shape, this.pageBottomText, this.text_2, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Symbolen för                       är <.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(263.9, 9);

        this.text_a = new cjs.Text("mindre än", "bold 16px 'Myriad Pro'");
        this.text_a.lineHeight = 19;
        this.text_a.setTransform(356.5, 9);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape.setTransform(428.6, 37.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_1.setTransform(273.9, 37.3);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_2.setTransform(413.8, 37.4);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_3.setTransform(398.6, 37.4);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_4.setTransform(383, 37.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_5.setTransform(367.6, 37.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_6.setTransform(351.9, 37.4);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_7.setTransform(335.9, 37.4);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_8.setTransform(320.4, 37.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_9.setTransform(304.9, 37.3);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_10.setTransform(289.3, 37.3);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgGgGABgIQgBgHAGgGQAGgGAHAAQAIAAAGAGQAFAGABAHQgBAIgFAGQgGAGgIAAQgHAAgGgGg");
        this.shape_11.setTransform(290.5, 43.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgIgJgBgMQABgLAIgKQAKgJALAAQANAAAIAJQAKAKgBALQABAMgKAJQgIAKgNgBQgLABgKgKg");
        this.shape_12.setTransform(290.5, 43.7);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_13.setTransform(306, 43.7);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgJgJABgMQgBgLAJgKQAJgJAMAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgMABgJgKg");
        this.shape_14.setTransform(306, 43.7);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgGAHAAQAIAAAGAGQAGAGgBAHQABAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_15.setTransform(321.4, 43.7);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgJgJAAgMQAAgLAJgKQAIgJAMAAQAMAAAKAJQAIAKABALQgBAMgIAJQgKAKgMgBQgMABgIgKg");
        this.shape_16.setTransform(321.4, 43.7);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgGgGABgIQgBgHAGgGQAGgGAHAAQAIAAAGAGQAFAGABAHQgBAIgFAGQgGAGgIAAQgHAAgGgGg");
        this.shape_17.setTransform(336.9, 43.7);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgIgJgBgMQABgLAIgKQAKgJALAAQANAAAIAJQAKAKgBALQABAMgKAJQgIAKgNgBQgLABgKgKg");
        this.shape_18.setTransform(336.9, 43.7);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_19.setTransform(352.3, 43.7);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgKgJAAgMQAAgLAKgKQAIgJAMAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgMABgIgKg");
        this.shape_20.setTransform(352.3, 43.7);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgGAHAAQAIAAAGAGQAGAGgBAHQABAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_21.setTransform(367.8, 43.7);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgJgJAAgMQAAgLAJgKQAIgJAMAAQAMAAAKAJQAIAKABALQgBAMgIAJQgKAKgMgBQgMABgIgKg");
        this.shape_22.setTransform(367.8, 43.7);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgGgGABgIQgBgHAGgGQAGgGAHAAQAIAAAGAGQAFAGAAAHQAAAIgFAGQgGAGgIAAQgHAAgGgGg");
        this.shape_23.setTransform(383.2, 43.7);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgIgJgBgMQABgLAIgKQAKgJALAAQANAAAIAJQAKAKgBALQABAMgKAJQgIAKgNgBQgLABgKgKg");
        this.shape_24.setTransform(383.2, 43.7);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_25.setTransform(398.7, 43.7);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgJgJAAgMQAAgLAJgKQAJgJAMAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgMABgJgKg");
        this.shape_26.setTransform(398.7, 43.7);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgGAHAAQAIAAAGAGQAGAGgBAHQABAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_27.setTransform(414.1, 43.7);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgJgJAAgMQAAgLAJgKQAIgJAMAAQAMAAAKAJQAIAKABALQgBAMgIAJQgKAKgMgBQgMABgIgKg");
        this.shape_28.setTransform(414.1, 43.7);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_29.setTransform(429.7, 43.7);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgJgJABgMQgBgLAJgKQAJgJAMAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgMABgJgKg");
        this.shape_30.setTransform(429.7, 43.7);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgGAHAAQAIAAAGAGQAGAGgBAHQABAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_31.setTransform(275.1, 43.7);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgJgJAAgMQAAgLAJgKQAIgJAMAAQAMAAAKAJQAIAKABALQgBAMgIAJQgKAKgMgBQgMABgIgKg");
        this.shape_32.setTransform(275.1, 43.7);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_33.setTransform(185.9, 37.4);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_34.setTransform(31.2, 37.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_35.setTransform(171.1, 37.4);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(155.9, 37.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_37.setTransform(140.3, 37.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(124.9, 37.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_39.setTransform(109.2, 37.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(93.2, 37.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(77.7, 37.3);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(62.2, 37.3);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(46.6, 37.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgGAHAAQAIAAAGAGQAGAGgBAHQABAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_44.setTransform(47.8, 43.7);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgJgJAAgMQAAgLAJgKQAIgJAMAAQAMAAAKAJQAIAKABALQgBAMgIAJQgKAKgMgBQgMABgIgKg");
        this.shape_45.setTransform(47.8, 43.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgGgGABgIQgBgHAGgGQAGgGAHAAQAIAAAGAGQAFAGABAHQgBAIgFAGQgGAGgIAAQgHAAgGgGg");
        this.shape_46.setTransform(63.3, 43.7);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgIgJgBgMQABgLAIgKQAKgJALAAQANAAAIAJQAKAKgBALQABAMgKAJQgIAKgNgBQgLABgKgKg");
        this.shape_47.setTransform(63.3, 43.7);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_48.setTransform(78.7, 43.7);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgKgJAAgMQAAgLAKgKQAIgJAMAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgMABgIgKg");
        this.shape_49.setTransform(78.7, 43.7);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgGAHAAQAIAAAGAGQAGAGgBAHQABAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_50.setTransform(94.2, 43.7);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgJgJAAgMQAAgLAJgKQAIgJAMAAQAMAAAKAJQAIAKABALQgBAMgIAJQgKAKgMgBQgMABgIgKg");
        this.shape_51.setTransform(94.2, 43.7);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgGgGABgIQgBgHAGgGQAGgGAHAAQAIAAAGAGQAFAGAAAHQAAAIgFAGQgGAGgIAAQgHAAgGgGg");
        this.shape_52.setTransform(109.6, 43.7);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgIgJgBgMQABgLAIgKQAKgJALAAQANAAAIAJQAKAKgBALQABAMgKAJQgIAKgNgBQgLABgKgKg");
        this.shape_53.setTransform(109.6, 43.7);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_54.setTransform(125.1, 43.7);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgJgJAAgMQAAgLAJgKQAKgJALAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgLABgKgKg");
        this.shape_55.setTransform(125.1, 43.7);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgGAHAAQAIAAAGAGQAGAGgBAHQABAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_56.setTransform(140.5, 43.7);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgUAVQgJgJAAgMQAAgLAJgKQAIgJAMAAQAMAAAKAJQAIAKABALQgBAMgIAJQgKAKgMgBQgMABgIgKg");
        this.shape_57.setTransform(140.5, 43.7);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgGgGABgIQgBgHAGgGQAGgGAHAAQAIAAAGAGQAFAGAAAHQAAAIgFAGQgGAGgIAAQgHAAgGgGg");
        this.shape_58.setTransform(156, 43.7);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgIgJgBgMQABgLAIgKQAKgJALAAQANAAAIAJQAKAKgBALQABAMgKAJQgIAKgNgBQgLABgKgKg");
        this.shape_59.setTransform(156, 43.7);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_60.setTransform(171.4, 43.7);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgJgJABgMQgBgLAJgKQAJgJAMAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgMABgJgKg");
        this.shape_61.setTransform(171.4, 43.7);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgGgGABgIQgBgHAGgGQAGgGAHAAQAIAAAGAGQAFAGABAHQgBAIgFAGQgGAGgIAAQgHAAgGgGg");
        this.shape_62.setTransform(187, 43.7);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgIgJgBgMQABgLAIgKQAKgJALAAQANAAAIAJQAKAKgBALQABAMgKAJQgIAKgNgBQgLABgKgKg");
        this.shape_63.setTransform(187, 43.7);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.bf(img.s24_25_95, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -2.4, -2.4)).s().p("AgNAOQgFgGgBgIQABgHAFgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
        this.shape_64.setTransform(32.4, 43.7);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.bf(img.s24_25_94, null, new cjs.Matrix2D(0.24, 0, 0, 0.24, -3.4, -3.4)).s().p("AgVAVQgJgJAAgMQAAgLAJgKQAKgJALAAQAMAAAKAJQAJAKAAALQAAAMgJAJQgKAKgMgBQgLABgKgKg");
        this.shape_65.setTransform(32.4, 43.7);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#000000").ss(0.1).p("AgZAXQgFgHAAgJQAAgJAHgIAgSgQQAIgGAKAAQALAAAIAGAAagIQAFAHAAAIQAAAJgFAH");
        this.shape_66.setTransform(32.4, 43);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAcgcAAgmQAAgmgcgbQgcgcgmAAQgmAAgbAcQgcAbAAAmQAAAmAcAcQAbAcAmAAg");
        this.shape_67.setTransform(400, 62);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f("#C51930").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAbgcAmAAQAmAAAcAcQAcAbAAAmQAAAmgcAcQgcAcgmAAQgmAAgbgcg");
        this.shape_68.setTransform(400, 62);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAbgcAAgmQAAgmgbgbQgcgcgmAAQgmAAgbAcQgcAbAAAmQAAAmAcAcQAbAcAmAAg");
        this.shape_69.setTransform(66, 62);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f("#C51930").s().p("AhBBCQgbgcAAgmQAAgmAbgbQAbgcAmAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQgmAAgbgcg");
        this.shape_70.setTransform(66, 62);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAbgbAAgnQAAglgbgcQgcgcgmAAQgmAAgbAcQgcAcAAAlQAAAnAcAbQAbAcAmAAg");
        this.shape_71.setTransform(416.1, 84.7);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f("#C51930").s().p("AhBBCQgbgbgBgnQABglAbgcQAcgcAlAAQAmAAAcAcQAbAcAAAlQAAAngbAbQgcAcgmAAQglAAgcgcg");
        this.shape_72.setTransform(416.1, 84.7);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAcgbAAgnQAAglgcgcQgcgcgmAAQgmAAgbAcQgcAcAAAlQAAAnAcAbQAbAcAmAAg");
        this.shape_73.setTransform(78.1, 84.7);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f("#C51930").s().p("AhBBCQgcgbAAgnQAAglAcgcQAbgcAmAAQAmAAAcAcQAcAcAAAlQAAAngcAbQgcAcgmAAQgmAAgbgcg");
        this.shape_74.setTransform(78.1, 84.7);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAcgbAAgnQAAglgcgcQgcgcgmAAQgmAAgbAcQgcAcAAAlQAAAnAcAbQAbAcAmAAg");
        this.shape_75.setTransform(321, 84.3);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAbgcAmAAQAmAAAcAcQAcAcAAAlQAAAngcAbQgcAcgmAAQgmAAgbgcg");
        this.shape_76.setTransform(321, 84.3);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgbAcAAAlQAAAnAbAbQAcAcAlAAg");
        this.shape_77.setTransform(169.7, 84.3);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAcgcAlAAQAnAAAbAcQAcAcgBAlQABAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_78.setTransform(169.7, 84.3);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgcAcAAAlQAAAnAcAbQAcAcAlAAg");
        this.shape_79.setTransform(388.3, 84.7);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f("#C51930").s().p("AhBBCQgcgbAAgnQAAglAcgcQAcgcAlAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_80.setTransform(388.3, 84.7);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgbAcAAAlQAAAnAbAbQAcAcAlAAg");
        this.shape_81.setTransform(50.4, 84.7);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f("#C51930").s().p("AhBBCQgbgbgBgnQABglAbgcQAcgcAlAAQAnAAAbAcQAcAcgBAlQABAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_82.setTransform(50.4, 84.7);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAbgbAAgnQAAglgbgcQgcgcgmAAQglAAgcAcQgbAcAAAlQAAAnAbAbQAcAcAlAAg");
        this.shape_83.setTransform(293.3, 84.3);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f("#0089CA").s().p("AhBBCQgbgbgBgnQABglAbgcQAcgcAlAAQAmAAAcAcQAbAcAAAlQAAAngbAbQgcAcgmAAQglAAgcgcg");
        this.shape_84.setTransform(293.3, 84.3);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgcAcAAAlQAAAnAcAbQAcAcAlAAg");
        this.shape_85.setTransform(141.9, 84.3);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAcgcAlAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_86.setTransform(141.9, 84.3);

        this.text_1 = new cjs.Text("2 är mindre än 3.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(298.8, 128.3);

        this.text_2 = new cjs.Text("3 är större än 2.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(59.4, 128.3);

        this.text_3 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(377.6, 98.7);

        this.text_4 = new cjs.Text("<", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(347.1, 98.7);

        this.text_5 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(317.9, 98.7);

        this.text_6 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(131.2, 98.7);

        this.text_7 = new cjs.Text(">", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(102.5, 98.7);

        this.text_8 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(75.1, 98.7);

        this.text_9 = new cjs.Text("Symbolen för                     är >.", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(19.3, 9);

        this.text_9a = new cjs.Text("större än", "bold 16px 'Myriad Pro'");
        this.text_9a.lineHeight = 19;
        this.text_9a.setTransform(112, 9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#959C9D").s().p("AtKHEQglAAgOgeQgFgJgCgLIgBgJIAAsRQAAglAegOQAPgIAOAAIaVAAQAlAAAOAeQAIAPAAAOIAAMRQAAAlgeAOQgJAFgLACIgJABgAtSm9QgIABgLAFQgbANAAAiIAAMTIABAGQABAJAFAKQANAbAiAAIaXAAIAGgBQAIgBALgFQAbgOAAghIAAsTIgBgGQgCgLgEgIQgOgbghAAI6XAAg");
        this.shape_87.setTransform(353.8, 81);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f("#FFFFFF").s().p("AtKHBQgjAAgOgcIgHgcIAAsRQAAgjAcgOQAOgHAOAAIaVAAQAjAAAOAcQAHAOAAAOIAAMRQAAAjgcAOIgcAHg");
        this.shape_88.setTransform(353.8, 81);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AtKnAIgcAHQgcAOAAAjIAAMRIAHAcQAOAcAjAAIaUAAIAdgHQAcgOAAgjIAAsRQAAgOgHgOQgOgcgkAAg");
        this.shape_89.setTransform(111.1, 81);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f("#FFFFFF").s().p("AtKHBQgjAAgOgcIgHgcIAAsRQAAgjAcgOIAcgHIaUAAQAkAAAOAcQAHAOAAAOIAAMRQAAAjgcAOIgdAHg");
        this.shape_90.setTransform(111.1, 81);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gL+QgdAAgcAOQg5AcAABHIAAUbIACARQAEAWAIASQAdA4BHAAMBL/AAAIASgCQAVgDASgJQA5gcAAhHIAA0bQAAgcgOgcQgdg5hHAAg");
        this.shape_91.setTransform(254.6, 76.8);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f("#FFF173").s().p("Egl/AL/QhHAAgdg4QgIgSgEgWIgCgRIAA0bQAAhHA5gcQAcgOAdAAMBL/AAAQBHAAAdA5QAOAcAAAcIAAUbQAABHg5AcQgSAJgVADIgSACg");
        this.shape_92.setTransform(254.6, 76.8);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f().s("#000000").ss(0.1).p("AAWABQgIAFgKAAQgHAAgIgFAgSgEQgBgCgBgB");
        this.shape_93.setTransform(31.9, 46.2);

        this.addChild(this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text);
        this.addChild(this.text_a, this.text_9a);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.3, 155.6);


    (lib.Symbol3 = function() {
        this.initialize();
        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#00A5C0").ss(0.5, 0, 0, 4).p("AhYBXICxAAIAAitIixAAg");
        this.shape.setTransform(302, 15);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#00A5C0").ss(0.5, 0, 0, 4).p("AhYBXICxAAIAAitIixAAg");
        this.shape_1.setTransform(178, 15);

        this.text = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(451.1, 188);

        this.text_1 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(281.3, 188);

        this.text_2 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(104.3, 188);

        this.text_3 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(451.1, 91);

        this.text_4 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(279.5, 91);

        this.text_5 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(104.3, 91);

        this.text_6 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(397.5, 188);

        this.text_7 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(397.5, 91);

        this.text_8 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(227.7, 188);

        this.text_9 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(227.7, 91);

        this.text_10 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(50.7, 188);

        this.text_11 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_11.lineHeight = 19;
        this.text_11.setTransform(50.7, 91);

        this.text_12 = new cjs.Text("Jämför. Skriv större än  >  eller mindre än  <  .", "16px 'Myriad Pro'");
        this.text_12.lineHeight = 19;
        this.text_12.setTransform(20, 6);

        this.text_13 = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_13.lineHeight = 20;
        this.text_13.setTransform(2, 5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_2.setTransform(311.3, 169.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_3.setTransform(311.3, 169.1);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_4.setTransform(478.2, 74.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_5.setTransform(478.2, 74.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_6.setTransform(286, 169.1);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_7.setTransform(286, 169.1);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_8.setTransform(452.9, 74.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_9.setTransform(452.9, 74.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_10.setTransform(289.7, 74.8);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_11.setTransform(289.7, 74.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_12.setTransform(298.1, 147.5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_13.setTransform(298.1, 147.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_14.setTransform(465, 52.5);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_15.setTransform(465, 52.5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_16.setTransform(480, 170.1);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgmAAgdgcg");
        this.shape_17.setTransform(480, 170.1);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_18.setTransform(455.2, 170.1);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_19.setTransform(455.2, 170.1);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_20.setTransform(112.5, 71.6);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgmAAgdgcg");
        this.shape_21.setTransform(112.5, 71.6);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_22.setTransform(110.5, 170.1);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgmAAgdgcg");
        this.shape_23.setTransform(110.5, 170.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_24.setTransform(480, 146.5);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgmAcgdQAdgcAmAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_25.setTransform(480, 146.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_26.setTransform(455.2, 146.5);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_27.setTransform(455.2, 146.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_28.setTransform(112.5, 47.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#C51930").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgmAAgdgcg");
        this.shape_29.setTransform(112.5, 47.9);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_30.setTransform(110.5, 146.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#C51930").s().p("AhDBEQgcgdAAgnQAAgmAcgdQAdgcAmAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_31.setTransform(110.5, 146.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_32.setTransform(234.7, 52.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_33.setTransform(234.7, 52.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_34.setTransform(43.7, 147.5);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgmAcgdQAdgcAmAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_35.setTransform(43.7, 147.5);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_36.setTransform(389.3, 147.5);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_37.setTransform(389.3, 147.5);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_38.setTransform(232.2, 147.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgmAcgdQAdgcAmAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_39.setTransform(232.2, 147.5);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgmAAgdAcQgcAdAAAmQAAAnAcAdQAcAcAnAAg");
        this.shape_40.setTransform(208.1, 147.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgmAcgdQAdgcAmAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_41.setTransform(208.1, 147.5);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgcAAgoQAAgmgcgdQgdgcgnAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_42.setTransform(56.9, 72.7);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_43.setTransform(56.9, 72.7);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgnAAgcAcQgcAdAAAmQAAAoAcAcQAcAcAnAAg");
        this.shape_44.setTransform(403.4, 75);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
        this.shape_45.setTransform(403.4, 75);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgcAAgoQAAgmgcgdQgcgcgoAAQgmAAgdAcQgcAdAAAmQAAAoAcAcQAdAcAmAAg");
        this.shape_46.setTransform(234.7, 75);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#0089CA").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAdgcAmAAQAoAAAcAcQAcAdAAAmQAAAogcAcQgcAcgoAAQgmAAgdgcg");
        this.shape_47.setTransform(234.7, 75);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAoAAAcgcQAcgdAAgnQAAgngcgcQgcgcgoAAQgmAAgdAcQgcAcAAAnQAAAnAcAdQAdAcAmAAg");
        this.shape_48.setTransform(58.7, 169.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAdgcAmAAQAoAAAcAcQAcAcAAAnQAAAngcAdQgcAcgoAAQgmAAgdgcg");
        this.shape_49.setTransform(58.7, 169.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_50.setTransform(404.3, 169.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_51.setTransform(404.3, 169.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_52.setTransform(30.7, 169.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_53.setTransform(30.7, 169.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_54.setTransform(376.3, 169.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_55.setTransform(376.3, 169.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_56.setTransform(232.2, 169.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_57.setTransform(232.2, 169.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABgQAnAAAdgcQAcgdAAgnQAAgngcgcQgdgcgnAAQgnAAgcAcQgcAcAAAnQAAAnAcAdQAcAcAnAAg");
        this.shape_58.setTransform(208.1, 169.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#0089CA").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
        this.shape_59.setTransform(208.1, 169.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_60.setTransform(429.5, 195.5);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_61.setTransform(429.5, 195.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_62.setTransform(429.5, 101.2);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_63.setTransform(429.5, 101.2);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_64.setTransform(260.8, 195.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_65.setTransform(260.8, 195.5);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_66.setTransform(260.8, 101.2);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_67.setTransform(260.8, 101.2);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_68.setTransform(82.9, 195.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_69.setTransform(82.9, 195.5);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AhfBvIC/AAIAAjdIi/AAg");
        this.shape_70.setTransform(82.9, 101.2);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#FFFFFF").s().p("AhfBvIAAjdIC/AAIAADdg");
        this.shape_71.setTransform(82.9, 101.2);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmwQgHAAgKACQgWAEgRAIQg5AdAABHIAAJ+IACARQADAWAJARQAcA5BHAAIWfAAQAcAAAdgOQA4gdAAhGIAAp+QAAgdgOgcQgcg5hHAAg");
        this.shape_72.setTransform(256.9, 74.7);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#FFFFFF").s().p("ArPGxQhHAAgcg5QgJgRgDgWIgCgRIAAp+QAAhHA5gdQARgIAWgEQAKgCAHAAIWfAAQBHAAAcA5QAOAcAAAdIAAJ+QAABGg4AdQgdAOgcAAg");
        this.shape_73.setTransform(256.9, 74.7);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmwQgHAAgKACQgWADgRAJQg5AcAABHIAAJ/IACARQADAVAJASQAcA5BHAAIWfAAQAcAAAdgOQA4gcAAhHIAAp/QAAgcgOgcQgcg5hHAAg");
        this.shape_74.setTransform(256.9, 167.7);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#FFFFFF").s().p("ArPGxQhHAAgcg5QgJgRgDgWIgCgRIAAp+QAAhIA5gcQARgIAWgEQAKgCAHAAIWfAAQBHAAAcA5QAOAcAAAdIAAJ+QAABGg4AdQgdAOgcAAg");
        this.shape_75.setTransform(256.9, 167.7);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmwQgHAAgLACQgVAEgSAIQg5AdAABHIAAJ+IACARQAEAWAIARQAdA5BHAAIWfAAQAcAAAdgOQA4gdAAhGIAAp+QAAgdgNgcQgdg5hHAAg");
        this.shape_76.setTransform(428.9, 74.7);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#FFFFFF").s().p("ArPGxQhHAAgcg5QgJgRgDgWIgDgRIAAp+QAAhHA6gdQARgIAVgEQALgCAHAAIWfAAQBHAAAcA5QAPAcAAAdIAAJ+QAABGg5AdQgcAOgdAAg");
        this.shape_77.setTransform(428.9, 74.7);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmwQgHAAgLACQgVADgSAJQg5AcAABHIAAJ/IACARQAEAVAIASQAdA5BHAAIWfAAQAcAAAdgOQA4gcAAhHIAAp/QAAgcgNgcQgdg5hHAAg");
        this.shape_78.setTransform(428.9, 167.7);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#FFFFFF").s().p("ArPGxQhHAAgcg5QgJgRgDgWIgDgRIAAp+QAAhIA6gcQARgIAVgEQALgCAHAAIWfAAQBHAAAcA5QAPAcAAAdIAAJ+QAABGg5AdQgcAOgdAAg");
        this.shape_79.setTransform(428.9, 167.7);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmwQgHAAgKACQgWAEgRAIQg5AdAABHIAAJ+IACARQADAWAJARQAcA5BHAAIWfAAQAdAAAcgOQA4gdAAhGIAAp+QAAgdgOgcQgcg5hHAAg");
        this.shape_80.setTransform(85.4, 74.7);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#FFFFFF").s().p("ArPGxQhHAAgcg5QgJgRgDgWIgCgRIAAp+QAAhHA5gdQARgIAWgEQAKgCAHAAIWfAAQBHAAAcA5QAOAcAAAdIAAJ+QAABGg4AdQgcAOgdAAg");
        this.shape_81.setTransform(85.4, 74.7);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("ArPmwQgHAAgKACQgWADgRAJQg5AcAABHIAAJ/IACARQADAVAJASQAcA5BHAAIWfAAQAdAAAcgOQA4gcAAhHIAAp/QAAgcgOgcQgcg5hHAAg");
        this.shape_82.setTransform(85.4, 167.7);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#FFFFFF").s().p("ArPGxQhHAAgcg5QgJgRgDgWIgCgRIAAp+QAAhIA5gcQARgIAWgEQAKgCAHAAIWfAAQBHAAAcA5QAOAcAAAdIAAJ+QAABGg4AdQgcAOgdAAg");
        this.shape_83.setTransform(85.4, 167.7);

        this.addChild(this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.text_13, this.text_12, this.text_11, this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_1, this.shape, this.ra1, this.ra2, this.ra3, this.ra4, this.ra5, this.ra6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 217.2);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1


        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape.setTransform(436.7, 137.1);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_1.setTransform(436.7, 137.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_2.setTransform(436.7, 129.7);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_3.setTransform(436.7, 129.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_4.setTransform(444.1, 133.2);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_5.setTransform(444.1, 133.2);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_6.setTransform(396.2, 137.1);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_7.setTransform(396.2, 137.1);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_8.setTransform(396.2, 129.7);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_9.setTransform(396.2, 129.7);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_10.setTransform(403.6, 133.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_11.setTransform(403.6, 133.2);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_12.setTransform(356.5, 137.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_13.setTransform(356.5, 137.1);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_14.setTransform(356.5, 129.7);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_15.setTransform(356.5, 129.7);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_16.setTransform(363.9, 133.2);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_17.setTransform(363.9, 133.2);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_18.setTransform(317.1, 137.1);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_19.setTransform(317.1, 137.1);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_20.setTransform(317.1, 129.7);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_21.setTransform(317.1, 129.7);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_22.setTransform(324.4, 133.2);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_23.setTransform(324.4, 133.2);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_24.setTransform(185.5, 93.3);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_25.setTransform(185.5, 93.3);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_26.setTransform(185.5, 85.8);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_27.setTransform(185.5, 85.8);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_28.setTransform(178.1, 89.3);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_29.setTransform(178.1, 89.3);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_30.setTransform(146.6, 93.3);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_31.setTransform(146.6, 93.3);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_32.setTransform(146.6, 85.8);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_33.setTransform(146.6, 85.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_34.setTransform(139.2, 89.3);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_35.setTransform(139.2, 89.3);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_36.setTransform(106.9, 93.3);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_37.setTransform(106.9, 93.3);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_38.setTransform(106.9, 85.8);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_39.setTransform(106.9, 85.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_40.setTransform(99.5, 89.3);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_41.setTransform(99.5, 89.3);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_42.setTransform(67.2, 93.3);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_43.setTransform(67.2, 93.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_44.setTransform(67.2, 85.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_45.setTransform(67.2, 85.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_46.setTransform(59.9, 89.3);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_47.setTransform(59.9, 89.3);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_48.setTransform(67.4, 173.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_49.setTransform(67.4, 173.5);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_50.setTransform(67.4, 166.1);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_51.setTransform(67.4, 166.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_52.setTransform(60, 169.5);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_53.setTransform(60, 169.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_54.setTransform(66.9, 137.6);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_55.setTransform(66.9, 137.6);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_56.setTransform(66.9, 130.2);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_57.setTransform(66.9, 130.2);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_58.setTransform(59.5, 133.6);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_59.setTransform(59.5, 133.6);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_60.setTransform(106.8, 174.1);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_61.setTransform(106.8, 174.1);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_62.setTransform(106.8, 166.7);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_63.setTransform(106.8, 166.7);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_64.setTransform(99.4, 170.2);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_65.setTransform(99.4, 170.2);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_66.setTransform(106.8, 137.6);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_67.setTransform(106.8, 137.6);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_68.setTransform(106.8, 130.2);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_69.setTransform(106.8, 130.2);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_70.setTransform(99.4, 133.6);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_71.setTransform(99.4, 133.6);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_72.setTransform(146.5, 174.1);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_73.setTransform(146.5, 174.1);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_74.setTransform(146.5, 166.7);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_75.setTransform(146.5, 166.7);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_76.setTransform(139.1, 170.2);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_77.setTransform(139.1, 170.2);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_78.setTransform(146.5, 137.6);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_79.setTransform(146.5, 137.6);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_80.setTransform(146.5, 130.2);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_81.setTransform(146.5, 130.2);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_82.setTransform(139.1, 133.6);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_83.setTransform(139.1, 133.6);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_84.setTransform(186.3, 174.1);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_85.setTransform(186.3, 174.1);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_86.setTransform(186.3, 166.7);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_87.setTransform(186.3, 166.7);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_88.setTransform(179, 170.2);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_89.setTransform(179, 170.2);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_90.setTransform(186.3, 137.6);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_91.setTransform(186.3, 137.6);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_92.setTransform(186.3, 130.2);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_93.setTransform(186.3, 130.2);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQgDAAAAgEQAAgDADAAQAEAAAAADQAAAEgEAAg");
        this.shape_94.setTransform(179, 133.6);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_95.setTransform(179, 133.6);

        this.text = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text.lineHeight = 37;
        this.text.setTransform(452, 148);

        this.text_1 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_1.lineHeight = 37;
        this.text_1.setTransform(412.4, 148);

        this.text_2 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_2.lineHeight = 37;
        this.text_2.setTransform(372.7, 148);

        this.text_3 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_3.lineHeight = 37;
        this.text_3.setTransform(333, 148);

        this.text_4 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_4.lineHeight = 37;
        this.text_4.setTransform(293.3, 148);

        this.text_5 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_5.lineHeight = 37;
        this.text_5.setTransform(194.1, 148);

        this.text_6 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_6.lineHeight = 37;
        this.text_6.setTransform(154.4, 148);

        this.text_7 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_7.lineHeight = 37;
        this.text_7.setTransform(114.8, 148);

        this.text_8 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_8.lineHeight = 37;
        this.text_8.setTransform(75.1, 148);

        this.text_9 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_9.lineHeight = 37;
        this.text_9.setTransform(35.4, 148);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_96.setTransform(202.2, 180.3);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_97.setTransform(182.2, 180.3);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_98.setTransform(162.4, 180.3);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_99.setTransform(142.6, 180.3);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_100.setTransform(122.7, 180.3);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_101.setTransform(102.9, 180.3);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_102.setTransform(83, 180.3);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_103.setTransform(63.2, 180.3);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_104.setTransform(43.2, 180.3);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_105.setTransform(212, 169);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_106.setTransform(192.2, 169);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_107.setTransform(202.2, 157.7);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_108.setTransform(172.3, 169);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_109.setTransform(182.2, 157.7);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_110.setTransform(152.5, 169);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_111.setTransform(162.4, 157.7);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_112.setTransform(132.7, 169);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_113.setTransform(142.6, 157.7);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_114.setTransform(112.8, 169);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_115.setTransform(122.7, 157.7);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_116.setTransform(93, 169);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_117.setTransform(102.9, 157.7);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_118.setTransform(73.1, 169);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_119.setTransform(83, 157.7);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_120.setTransform(53.3, 169);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_121.setTransform(63.2, 157.7);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_122.setTransform(33.5, 169);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_123.setTransform(43.2, 157.7);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_124.setTransform(460.2, 180.3);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDEAA");
        this.shape_125.setTransform(440.2, 180.3);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_126.setTransform(420.4, 180.3);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_127.setTransform(400.5, 180.3);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_128.setTransform(380.7, 180.3);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_129.setTransform(360.8, 180.3);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_130.setTransform(341, 180.3);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_131.setTransform(321.1, 180.3);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_132.setTransform(301.2, 180.3);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_133.setTransform(470, 169);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_134.setTransform(450.1, 169);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_135.setTransform(460.2, 157.7);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_136.setTransform(430.3, 169);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDEAA");
        this.shape_137.setTransform(440.2, 157.7);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_138.setTransform(410.5, 169);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_139.setTransform(420.4, 157.7);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_140.setTransform(390.6, 169);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_141.setTransform(400.5, 157.7);

        this.shape_142 = new cjs.Shape();
        this.shape_142.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_142.setTransform(370.8, 169);

        this.shape_143 = new cjs.Shape();
        this.shape_143.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_143.setTransform(380.7, 157.7);

        this.shape_144 = new cjs.Shape();
        this.shape_144.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_144.setTransform(350.9, 169);

        this.shape_145 = new cjs.Shape();
        this.shape_145.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_145.setTransform(360.8, 157.7);

        this.shape_146 = new cjs.Shape();
        this.shape_146.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_146.setTransform(331.1, 169);

        this.shape_147 = new cjs.Shape();
        this.shape_147.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_147.setTransform(341, 157.7);

        this.shape_148 = new cjs.Shape();
        this.shape_148.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_148.setTransform(311.2, 169);

        this.shape_149 = new cjs.Shape();
        this.shape_149.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_149.setTransform(321.1, 157.7);

        this.shape_150 = new cjs.Shape();
        this.shape_150.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_150.setTransform(291.4, 169);

        this.shape_151 = new cjs.Shape();
        this.shape_151.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_151.setTransform(301.2, 157.7);

        this.text_10 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_10.lineHeight = 37;
        this.text_10.setTransform(451.4, 111.5);

        this.text_11 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_11.lineHeight = 37;
        this.text_11.setTransform(411.7, 111.5);

        this.text_12 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_12.lineHeight = 37;
        this.text_12.setTransform(372, 111.5);

        this.text_13 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_13.lineHeight = 37;
        this.text_13.setTransform(332.3, 111.5);

        this.text_14 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_14.lineHeight = 37;
        this.text_14.setTransform(292.6, 111.5);

        this.text_15 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_15.lineHeight = 37;
        this.text_15.setTransform(194.1, 111.5);

        this.text_16 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_16.lineHeight = 37;
        this.text_16.setTransform(154.4, 111.5);

        this.text_17 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_17.lineHeight = 37;
        this.text_17.setTransform(114.8, 111.5);

        this.text_18 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_18.lineHeight = 37;
        this.text_18.setTransform(75.1, 111.5);

        this.text_19 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_19.lineHeight = 37;
        this.text_19.setTransform(35.4, 111.5);

        this.shape_152 = new cjs.Shape();
        this.shape_152.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_152.setTransform(459.5, 144.2);

        this.shape_153 = new cjs.Shape();
        this.shape_153.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_153.setTransform(439.5, 144.2);

        this.shape_154 = new cjs.Shape();
        this.shape_154.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_154.setTransform(419.7, 144.2);

        this.shape_155 = new cjs.Shape();
        this.shape_155.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_155.setTransform(399.8, 144.2);

        this.shape_156 = new cjs.Shape();
        this.shape_156.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_156.setTransform(380, 144.2);

        this.shape_157 = new cjs.Shape();
        this.shape_157.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_157.setTransform(360.1, 144.2);

        this.shape_158 = new cjs.Shape();
        this.shape_158.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_158.setTransform(340.3, 144.2);

        this.shape_159 = new cjs.Shape();
        this.shape_159.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_159.setTransform(320.5, 144.2);

        this.shape_160 = new cjs.Shape();
        this.shape_160.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_160.setTransform(300.5, 144.2);

        this.shape_161 = new cjs.Shape();
        this.shape_161.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_161.setTransform(469.3, 132.8);

        this.shape_162 = new cjs.Shape();
        this.shape_162.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_162.setTransform(449.5, 132.8);

        this.shape_163 = new cjs.Shape();
        this.shape_163.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_163.setTransform(459.5, 121.5);

        this.shape_164 = new cjs.Shape();
        this.shape_164.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_164.setTransform(429.6, 132.8);

        this.shape_165 = new cjs.Shape();
        this.shape_165.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_165.setTransform(439.5, 121.5);

        this.shape_166 = new cjs.Shape();
        this.shape_166.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_166.setTransform(409.8, 132.8);

        this.shape_167 = new cjs.Shape();
        this.shape_167.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_167.setTransform(419.7, 121.5);

        this.shape_168 = new cjs.Shape();
        this.shape_168.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_168.setTransform(389.9, 132.8);

        this.shape_169 = new cjs.Shape();
        this.shape_169.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_169.setTransform(399.8, 121.5);

        this.shape_170 = new cjs.Shape();
        this.shape_170.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_170.setTransform(370.1, 132.8);

        this.shape_171 = new cjs.Shape();
        this.shape_171.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_171.setTransform(380, 121.5);

        this.shape_172 = new cjs.Shape();
        this.shape_172.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_172.setTransform(350.2, 132.8);

        this.shape_173 = new cjs.Shape();
        this.shape_173.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_173.setTransform(360.1, 121.5);

        this.shape_174 = new cjs.Shape();
        this.shape_174.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_174.setTransform(330.4, 132.8);

        this.shape_175 = new cjs.Shape();
        this.shape_175.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_175.setTransform(340.3, 121.5);

        this.shape_176 = new cjs.Shape();
        this.shape_176.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_176.setTransform(310.6, 132.8);

        this.shape_177 = new cjs.Shape();
        this.shape_177.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_177.setTransform(320.5, 121.5);

        this.shape_178 = new cjs.Shape();
        this.shape_178.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_178.setTransform(290.7, 132.8);

        this.shape_179 = new cjs.Shape();
        this.shape_179.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_179.setTransform(300.5, 121.5);

        this.shape_180 = new cjs.Shape();
        this.shape_180.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_180.setTransform(202.2, 144.2);

        this.shape_181 = new cjs.Shape();
        this.shape_181.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_181.setTransform(182.2, 144.2);

        this.shape_182 = new cjs.Shape();
        this.shape_182.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_182.setTransform(162.4, 144.2);

        this.shape_183 = new cjs.Shape();
        this.shape_183.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_183.setTransform(142.6, 144.2);

        this.shape_184 = new cjs.Shape();
        this.shape_184.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_184.setTransform(122.7, 144.2);

        this.shape_185 = new cjs.Shape();
        this.shape_185.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_185.setTransform(102.9, 144.2);

        this.shape_186 = new cjs.Shape();
        this.shape_186.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_186.setTransform(83, 144.2);

        this.shape_187 = new cjs.Shape();
        this.shape_187.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_187.setTransform(63.2, 144.2);

        this.shape_188 = new cjs.Shape();
        this.shape_188.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_188.setTransform(43.2, 144.2);

        this.shape_189 = new cjs.Shape();
        this.shape_189.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_189.setTransform(212, 132.8);

        this.shape_190 = new cjs.Shape();
        this.shape_190.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_190.setTransform(192.2, 132.8);

        this.shape_191 = new cjs.Shape();
        this.shape_191.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_191.setTransform(202.2, 121.5);

        this.shape_192 = new cjs.Shape();
        this.shape_192.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_192.setTransform(172.3, 132.8);

        this.shape_193 = new cjs.Shape();
        this.shape_193.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_193.setTransform(182.2, 121.5);

        this.shape_194 = new cjs.Shape();
        this.shape_194.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_194.setTransform(152.5, 132.8);

        this.shape_195 = new cjs.Shape();
        this.shape_195.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_195.setTransform(162.4, 121.5);

        this.shape_196 = new cjs.Shape();
        this.shape_196.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_196.setTransform(132.7, 132.8);

        this.shape_197 = new cjs.Shape();
        this.shape_197.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_197.setTransform(142.6, 121.5);

        this.shape_198 = new cjs.Shape();
        this.shape_198.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_198.setTransform(112.8, 132.8);

        this.shape_199 = new cjs.Shape();
        this.shape_199.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_199.setTransform(122.7, 121.5);

        this.shape_200 = new cjs.Shape();
        this.shape_200.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_200.setTransform(93, 132.8);

        this.shape_201 = new cjs.Shape();
        this.shape_201.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_201.setTransform(102.9, 121.5);

        this.shape_202 = new cjs.Shape();
        this.shape_202.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_202.setTransform(73.1, 132.8);

        this.shape_203 = new cjs.Shape();
        this.shape_203.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_203.setTransform(83, 121.5);

        this.shape_204 = new cjs.Shape();
        this.shape_204.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_204.setTransform(53.3, 132.8);

        this.shape_205 = new cjs.Shape();
        this.shape_205.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_205.setTransform(63.2, 121.5);

        this.shape_206 = new cjs.Shape();
        this.shape_206.graphics.f().s("#959C9D").ss(0.5).p("AAABvIAAjc");
        this.shape_206.setTransform(33.5, 132.8);

        this.shape_207 = new cjs.Shape();
        this.shape_207.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_207.setTransform(43.2, 121.5);

        this.text_20 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_20.lineHeight = 37;
        this.text_20.setTransform(452, 67.5);

        this.text_21 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_21.lineHeight = 37;
        this.text_21.setTransform(412.4, 67.5);

        this.text_22 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_22.lineHeight = 37;
        this.text_22.setTransform(372.7, 67.5);

        this.text_23 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_23.lineHeight = 37;
        this.text_23.setTransform(333, 67.5);

        this.text_24 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_24.lineHeight = 37;
        this.text_24.setTransform(293.3, 67.5);

        this.text_25 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_25.lineHeight = 37;
        this.text_25.setTransform(194.1, 67.5);

        this.text_26 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_26.lineHeight = 37;
        this.text_26.setTransform(154.4, 67.5);

        this.text_27 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_27.lineHeight = 37;
        this.text_27.setTransform(114.8, 67.5);

        this.text_28 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_28.lineHeight = 37;
        this.text_28.setTransform(75.1, 67.5);

        this.text_29 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_29.lineHeight = 37;
        this.text_29.setTransform(35.4, 67.5);

        this.shape_208 = new cjs.Shape();
        this.shape_208.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_208.setTransform(202.2, 100);

        this.shape_209 = new cjs.Shape();
        this.shape_209.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_209.setTransform(182.2, 100);

        this.shape_210 = new cjs.Shape();
        this.shape_210.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_210.setTransform(162.4, 100);

        this.shape_211 = new cjs.Shape();
        this.shape_211.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_211.setTransform(142.6, 100);

        this.shape_212 = new cjs.Shape();
        this.shape_212.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_212.setTransform(122.7, 100);

        this.shape_213 = new cjs.Shape();
        this.shape_213.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_213.setTransform(102.9, 100);

        this.shape_214 = new cjs.Shape();
        this.shape_214.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_214.setTransform(83, 100);

        this.shape_215 = new cjs.Shape();
        this.shape_215.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_215.setTransform(63.2, 100);

        this.shape_216 = new cjs.Shape();
        this.shape_216.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_216.setTransform(43.2, 100);

        this.shape_217 = new cjs.Shape();
        this.shape_217.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_217.setTransform(212, 88.6);

        this.shape_218 = new cjs.Shape();
        this.shape_218.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_218.setTransform(192.2, 88.6);

        this.shape_219 = new cjs.Shape();
        this.shape_219.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_219.setTransform(202.2, 77.3);

        this.shape_220 = new cjs.Shape();
        this.shape_220.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_220.setTransform(172.3, 88.6);

        this.shape_221 = new cjs.Shape();
        this.shape_221.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_221.setTransform(182.2, 77.3);

        this.shape_222 = new cjs.Shape();
        this.shape_222.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_222.setTransform(152.5, 88.6);

        this.shape_223 = new cjs.Shape();
        this.shape_223.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_223.setTransform(162.4, 77.3);

        this.shape_224 = new cjs.Shape();
        this.shape_224.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_224.setTransform(132.7, 88.6);

        this.shape_225 = new cjs.Shape();
        this.shape_225.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_225.setTransform(142.6, 77.3);

        this.shape_226 = new cjs.Shape();
        this.shape_226.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_226.setTransform(112.8, 88.6);

        this.shape_227 = new cjs.Shape();
        this.shape_227.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_227.setTransform(122.7, 77.3);

        this.shape_228 = new cjs.Shape();
        this.shape_228.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_228.setTransform(93, 88.6);

        this.shape_229 = new cjs.Shape();
        this.shape_229.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_229.setTransform(102.9, 77.3);

        this.shape_230 = new cjs.Shape();
        this.shape_230.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_230.setTransform(73.1, 88.6);

        this.shape_231 = new cjs.Shape();
        this.shape_231.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_231.setTransform(83, 77.3);

        this.shape_232 = new cjs.Shape();
        this.shape_232.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_232.setTransform(53.3, 88.6);

        this.shape_233 = new cjs.Shape();
        this.shape_233.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_233.setTransform(63.2, 77.3);

        this.shape_234 = new cjs.Shape();
        this.shape_234.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_234.setTransform(33.5, 88.6);

        this.shape_235 = new cjs.Shape();
        this.shape_235.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_235.setTransform(43.2, 77.3);

        this.shape_236 = new cjs.Shape();
        this.shape_236.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_236.setTransform(460.2, 100);

        this.shape_237 = new cjs.Shape();
        this.shape_237.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDEAA");
        this.shape_237.setTransform(440.2, 100);

        this.shape_238 = new cjs.Shape();
        this.shape_238.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_238.setTransform(420.4, 100);

        this.shape_239 = new cjs.Shape();
        this.shape_239.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_239.setTransform(400.5, 100);

        this.shape_240 = new cjs.Shape();
        this.shape_240.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_240.setTransform(380.7, 100);

        this.shape_241 = new cjs.Shape();
        this.shape_241.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_241.setTransform(360.8, 100);

        this.shape_242 = new cjs.Shape();
        this.shape_242.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_242.setTransform(341, 100);

        this.shape_243 = new cjs.Shape();
        this.shape_243.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_243.setTransform(321.1, 100);

        this.shape_244 = new cjs.Shape();
        this.shape_244.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_244.setTransform(301.2, 100);

        this.shape_245 = new cjs.Shape();
        this.shape_245.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_245.setTransform(470, 88.6);

        this.shape_246 = new cjs.Shape();
        this.shape_246.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_246.setTransform(450.1, 88.6);

        this.shape_247 = new cjs.Shape();
        this.shape_247.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_247.setTransform(460.2, 77.3);

        this.shape_248 = new cjs.Shape();
        this.shape_248.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_248.setTransform(430.3, 88.6);

        this.shape_249 = new cjs.Shape();
        this.shape_249.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDEAA");
        this.shape_249.setTransform(440.2, 77.3);

        this.shape_250 = new cjs.Shape();
        this.shape_250.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_250.setTransform(410.5, 88.6);

        this.shape_251 = new cjs.Shape();
        this.shape_251.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_251.setTransform(420.4, 77.3);

        this.shape_252 = new cjs.Shape();
        this.shape_252.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_252.setTransform(390.6, 88.6);

        this.shape_253 = new cjs.Shape();
        this.shape_253.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_253.setTransform(400.5, 77.3);

        this.shape_254 = new cjs.Shape();
        this.shape_254.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_254.setTransform(370.8, 88.6);

        this.shape_255 = new cjs.Shape();
        this.shape_255.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_255.setTransform(380.7, 77.3);

        this.shape_256 = new cjs.Shape();
        this.shape_256.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_256.setTransform(350.9, 88.6);

        this.shape_257 = new cjs.Shape();
        this.shape_257.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_257.setTransform(360.8, 77.3);

        this.shape_258 = new cjs.Shape();
        this.shape_258.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_258.setTransform(331.1, 88.6);

        this.shape_259 = new cjs.Shape();
        this.shape_259.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_259.setTransform(341, 77.3);

        this.shape_260 = new cjs.Shape();
        this.shape_260.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_260.setTransform(311.2, 88.6);

        this.shape_261 = new cjs.Shape();
        this.shape_261.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_261.setTransform(321.1, 77.3);

        this.shape_262 = new cjs.Shape();
        this.shape_262.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjc");
        this.shape_262.setTransform(291.4, 88.6);

        this.shape_263 = new cjs.Shape();
        this.shape_263.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_263.setTransform(301.2, 77.3);

        this.text_30 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_30.lineHeight = 37;
        this.text_30.setTransform(451.4, 31.7);

        this.text_31 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_31.lineHeight = 37;
        this.text_31.setTransform(431.5, 31.7);

        this.text_32 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_32.lineHeight = 37;
        this.text_32.setTransform(411.7, 31.7);

        this.text_33 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_33.lineHeight = 37;
        this.text_33.setTransform(391.8, 31.7);

        this.text_34 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_34.lineHeight = 37;
        this.text_34.setTransform(372, 31.7);

        this.text_35 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_35.lineHeight = 37;
        this.text_35.setTransform(352.2, 31.7);

        this.text_36 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_36.lineHeight = 37;
        this.text_36.setTransform(332.3, 31.7);

        this.text_37 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_37.lineHeight = 37;
        this.text_37.setTransform(312.5, 31.7);

        this.text_38 = new cjs.Text(">", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_38.lineHeight = 37;
        this.text_38.setTransform(292.6, 31.7);

        this.text_39 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_39.lineHeight = 37;
        this.text_39.setTransform(194.1, 31.7);

        this.text_40 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_40.lineHeight = 37;
        this.text_40.setTransform(174.3, 31.7);

        this.text_41 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_41.lineHeight = 37;
        this.text_41.setTransform(154.4, 31.7);

        this.text_42 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_42.lineHeight = 37;
        this.text_42.setTransform(134.6, 31.7);

        this.text_43 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_43.lineHeight = 37;
        this.text_43.setTransform(114.8, 31.7);

        this.text_44 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_44.lineHeight = 37;
        this.text_44.setTransform(94.9, 31.7);

        this.text_45 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_45.lineHeight = 37;
        this.text_45.setTransform(75.1, 31.7);

        this.text_46 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_46.lineHeight = 37;
        this.text_46.setTransform(55.2, 31.7);

        this.text_47 = new cjs.Text("<", "bold 31px 'UusiTekstausMajema'", "#BCC1C2");
        this.text_47.lineHeight = 37;
        this.text_47.setTransform(35.4, 31.7);

        this.shape_264 = new cjs.Shape();
        this.shape_264.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_264.setTransform(459.5, 63.8);

        this.shape_265 = new cjs.Shape();
        this.shape_265.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_265.setTransform(439.5, 63.8);

        this.shape_266 = new cjs.Shape();
        this.shape_266.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_266.setTransform(419.7, 63.8);

        this.shape_267 = new cjs.Shape();
        this.shape_267.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_267.setTransform(399.8, 63.8);

        this.shape_268 = new cjs.Shape();
        this.shape_268.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_268.setTransform(380, 63.8);

        this.shape_269 = new cjs.Shape();
        this.shape_269.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_269.setTransform(360.1, 63.8);

        this.shape_270 = new cjs.Shape();
        this.shape_270.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_270.setTransform(340.3, 63.8);

        this.shape_271 = new cjs.Shape();
        this.shape_271.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_271.setTransform(320.5, 63.8);

        this.shape_272 = new cjs.Shape();
        this.shape_272.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_272.setTransform(300.5, 63.8);

        this.shape_273 = new cjs.Shape();
        this.shape_273.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_273.setTransform(469.3, 52.4);

        this.shape_274 = new cjs.Shape();
        this.shape_274.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_274.setTransform(449.5, 52.4);

        this.shape_275 = new cjs.Shape();
        this.shape_275.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_275.setTransform(459.5, 41.1);

        this.shape_276 = new cjs.Shape();
        this.shape_276.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_276.setTransform(429.6, 52.4);

        this.shape_277 = new cjs.Shape();
        this.shape_277.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_277.setTransform(439.5, 41.1);

        this.shape_278 = new cjs.Shape();
        this.shape_278.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_278.setTransform(409.8, 52.4);

        this.shape_279 = new cjs.Shape();
        this.shape_279.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_279.setTransform(419.7, 41.1);

        this.shape_280 = new cjs.Shape();
        this.shape_280.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_280.setTransform(389.9, 52.4);

        this.shape_281 = new cjs.Shape();
        this.shape_281.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_281.setTransform(399.8, 41.1);

        this.shape_282 = new cjs.Shape();
        this.shape_282.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_282.setTransform(370.1, 52.4);

        this.shape_283 = new cjs.Shape();
        this.shape_283.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_283.setTransform(380, 41.1);

        this.shape_284 = new cjs.Shape();
        this.shape_284.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_284.setTransform(350.2, 52.4);

        this.shape_285 = new cjs.Shape();
        this.shape_285.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_285.setTransform(360.1, 41.1);

        this.shape_286 = new cjs.Shape();
        this.shape_286.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_286.setTransform(330.4, 52.4);

        this.shape_287 = new cjs.Shape();
        this.shape_287.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_287.setTransform(340.3, 41.1);

        this.shape_288 = new cjs.Shape();
        this.shape_288.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_288.setTransform(310.6, 52.4);

        this.shape_289 = new cjs.Shape();
        this.shape_289.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_289.setTransform(320.5, 41.1);

        this.shape_290 = new cjs.Shape();
        this.shape_290.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_290.setTransform(290.7, 52.4);

        this.shape_291 = new cjs.Shape();
        this.shape_291.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_291.setTransform(300.5, 41.1);

        this.shape_292 = new cjs.Shape();
        this.shape_292.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_292.setTransform(202.2, 63.8);

        this.shape_293 = new cjs.Shape();
        this.shape_293.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_293.setTransform(182.2, 63.8);

        this.shape_294 = new cjs.Shape();
        this.shape_294.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_294.setTransform(162.4, 63.8);

        this.shape_295 = new cjs.Shape();
        this.shape_295.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_295.setTransform(142.6, 63.8);

        this.shape_296 = new cjs.Shape();
        this.shape_296.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_296.setTransform(122.7, 63.8);

        this.shape_297 = new cjs.Shape();
        this.shape_297.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_297.setTransform(102.9, 63.8);

        this.shape_298 = new cjs.Shape();
        this.shape_298.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_298.setTransform(83, 63.8);

        this.shape_299 = new cjs.Shape();
        this.shape_299.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_299.setTransform(63.2, 63.8);

        this.shape_300 = new cjs.Shape();
        this.shape_300.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_300.setTransform(43.2, 63.8);

        this.shape_301 = new cjs.Shape();
        this.shape_301.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_301.setTransform(212, 52.4);

        this.shape_302 = new cjs.Shape();
        this.shape_302.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_302.setTransform(192.2, 52.4);

        this.shape_303 = new cjs.Shape();
        this.shape_303.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_303.setTransform(202.2, 41.1);

        this.shape_304 = new cjs.Shape();
        this.shape_304.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_304.setTransform(172.3, 52.4);

        this.shape_305 = new cjs.Shape();
        this.shape_305.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_305.setTransform(182.2, 41.1);

        this.shape_306 = new cjs.Shape();
        this.shape_306.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_306.setTransform(152.5, 52.4);

        this.shape_307 = new cjs.Shape();
        this.shape_307.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_307.setTransform(162.4, 41.1);

        this.shape_308 = new cjs.Shape();
        this.shape_308.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_308.setTransform(132.7, 52.4);

        this.shape_309 = new cjs.Shape();
        this.shape_309.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_309.setTransform(142.6, 41.1);

        this.shape_310 = new cjs.Shape();
        this.shape_310.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_310.setTransform(112.8, 52.4);

        this.shape_311 = new cjs.Shape();
        this.shape_311.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_311.setTransform(122.7, 41.1);

        this.shape_312 = new cjs.Shape();
        this.shape_312.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_312.setTransform(93, 52.4);

        this.shape_313 = new cjs.Shape();
        this.shape_313.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_313.setTransform(102.9, 41.1);

        this.shape_314 = new cjs.Shape();
        this.shape_314.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_314.setTransform(73.1, 52.4);

        this.shape_315 = new cjs.Shape();
        this.shape_315.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_315.setTransform(83, 41.1);

        this.shape_316 = new cjs.Shape();
        this.shape_316.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_316.setTransform(53.3, 52.4);

        this.shape_317 = new cjs.Shape();
        this.shape_317.graphics.f().s("#959C9D").ss(0.5).p("AhiAAIDFAA");
        this.shape_317.setTransform(63.2, 41.1);

        this.shape_318 = new cjs.Shape();
        this.shape_318.graphics.f().s("#959C9D").ss(0.5).p("AAABuIAAjb");
        this.shape_318.setTransform(33.5, 52.4);

        this.shape_319 = new cjs.Shape();
        this.shape_319.graphics.f().s("#959C9D").ss(0.5).p("AhjAAIDHAA");
        this.shape_319.setTransform(43.2, 41.1);

        this.text_48 = new cjs.Text("Öva.", "16px 'Myriad Pro'");
        this.text_48.lineHeight = 19;
        this.text_48.setTransform(20, 6);

        this.text_49 = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text_49.lineHeight = 20;
        this.text_49.setTransform(2, 5);

        this.shape_320 = new cjs.Shape();
        this.shape_320.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_320.setTransform(436.5, 172.8);

        this.shape_321 = new cjs.Shape();
        this.shape_321.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_321.setTransform(436.5, 172.8);

        this.shape_322 = new cjs.Shape();
        this.shape_322.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_322.setTransform(436.5, 165.4);

        this.shape_323 = new cjs.Shape();
        this.shape_323.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_323.setTransform(436.5, 165.4);

        this.shape_324 = new cjs.Shape();
        this.shape_324.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_324.setTransform(443.8, 168.9);

        this.shape_325 = new cjs.Shape();
        this.shape_325.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_325.setTransform(443.8, 168.9);

        this.shape_326 = new cjs.Shape();
        this.shape_326.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_326.setTransform(396.8, 172.8);

        this.shape_327 = new cjs.Shape();
        this.shape_327.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_327.setTransform(396.8, 172.8);

        this.shape_328 = new cjs.Shape();
        this.shape_328.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_328.setTransform(396.8, 165.4);

        this.shape_329 = new cjs.Shape();
        this.shape_329.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_329.setTransform(396.8, 165.4);

        this.shape_330 = new cjs.Shape();
        this.shape_330.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_330.setTransform(404.1, 168.9);

        this.shape_331 = new cjs.Shape();
        this.shape_331.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_331.setTransform(404.1, 168.9);

        this.shape_332 = new cjs.Shape();
        this.shape_332.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_332.setTransform(357.1, 172.8);

        this.shape_333 = new cjs.Shape();
        this.shape_333.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_333.setTransform(357.1, 172.8);

        this.shape_334 = new cjs.Shape();
        this.shape_334.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_334.setTransform(357.1, 165.4);

        this.shape_335 = new cjs.Shape();
        this.shape_335.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_335.setTransform(357.1, 165.4);

        this.shape_336 = new cjs.Shape();
        this.shape_336.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_336.setTransform(364.5, 168.9);

        this.shape_337 = new cjs.Shape();
        this.shape_337.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_337.setTransform(364.5, 168.9);

        this.shape_338 = new cjs.Shape();
        this.shape_338.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_338.setTransform(317.6, 172.8);

        this.shape_339 = new cjs.Shape();
        this.shape_339.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_339.setTransform(317.6, 172.8);

        this.shape_340 = new cjs.Shape();
        this.shape_340.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_340.setTransform(317.6, 165.4);

        this.shape_341 = new cjs.Shape();
        this.shape_341.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_341.setTransform(317.6, 165.4);

        this.shape_342 = new cjs.Shape();
        this.shape_342.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_342.setTransform(325, 168.9);

        this.shape_343 = new cjs.Shape();
        this.shape_343.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_343.setTransform(325, 168.9);

        this.shape_344 = new cjs.Shape();
        this.shape_344.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_344.setTransform(437.5, 93.4);

        this.shape_345 = new cjs.Shape();
        this.shape_345.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_345.setTransform(437.5, 93.4);

        this.shape_346 = new cjs.Shape();
        this.shape_346.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_346.setTransform(437.5, 85.9);

        this.shape_347 = new cjs.Shape();
        this.shape_347.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_347.setTransform(437.5, 85.9);

        this.shape_348 = new cjs.Shape();
        this.shape_348.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_348.setTransform(444.8, 89.4);

        this.shape_349 = new cjs.Shape();
        this.shape_349.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_349.setTransform(444.8, 89.4);

        this.shape_350 = new cjs.Shape();
        this.shape_350.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_350.setTransform(398.6, 93.4);

        this.shape_351 = new cjs.Shape();
        this.shape_351.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_351.setTransform(398.6, 93.4);

        this.shape_352 = new cjs.Shape();
        this.shape_352.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_352.setTransform(398.6, 85.9);

        this.shape_353 = new cjs.Shape();
        this.shape_353.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_353.setTransform(398.6, 85.9);

        this.shape_354 = new cjs.Shape();
        this.shape_354.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_354.setTransform(405.9, 89.4);

        this.shape_355 = new cjs.Shape();
        this.shape_355.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_355.setTransform(405.9, 89.4);

        this.shape_356 = new cjs.Shape();
        this.shape_356.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_356.setTransform(358.1, 93.4);

        this.shape_357 = new cjs.Shape();
        this.shape_357.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_357.setTransform(358.1, 93.4);

        this.shape_358 = new cjs.Shape();
        this.shape_358.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_358.setTransform(358.1, 85.9);

        this.shape_359 = new cjs.Shape();
        this.shape_359.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_359.setTransform(358.1, 85.9);

        this.shape_360 = new cjs.Shape();
        this.shape_360.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_360.setTransform(365.5, 89.4);

        this.shape_361 = new cjs.Shape();
        this.shape_361.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_361.setTransform(365.5, 89.4);

        this.shape_362 = new cjs.Shape();
        this.shape_362.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_362.setTransform(319.2, 93.4);

        this.shape_363 = new cjs.Shape();
        this.shape_363.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_363.setTransform(319.2, 93.4);

        this.shape_364 = new cjs.Shape();
        this.shape_364.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_364.setTransform(319.2, 85.9);

        this.shape_365 = new cjs.Shape();
        this.shape_365.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_365.setTransform(319.2, 85.9);

        this.shape_366 = new cjs.Shape();
        this.shape_366.graphics.f().s("#A9AFB0").ss(1, 0, 0, 4).p("AAAAEQAEAAAAgEQAAgDgEAAQgDAAAAADQAAAEADAAg");
        this.shape_366.setTransform(326.5, 89.4);

        this.shape_367 = new cjs.Shape();
        this.shape_367.graphics.f("#A9AFB0").s().p("AgDAAQAAgDADAAQAEAAAAADQAAAEgEAAQgDAAAAgEg");
        this.shape_367.setTransform(326.5, 89.4);

        this.shape_368 = new cjs.Shape();
        this.shape_368.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("EgmCgM3QgHAAgKACQgWADgRAJQg5AcAABHIAAWMIACASQADAWAJARQAcA5BHAAMBMFAAAIARgCQAWgEASgIQA4gdAAhHIAA2MQAAgcgOgcQgcg5hHAAg");
        this.shape_368.setTransform(257.4, 110.4);

        this.shape_369 = new cjs.Shape();
        this.shape_369.graphics.f("#FFFFFF").s().p("EgmCAM4QhHAAgcg4QgJgTgDgVIgCgSIAA2MQAAhGA5gdQARgIAWgEQAKgCAHAAMBMFAAAQBHAAAcA5QAOAcAAAcIAAWMQAABHg4AdQgSAIgWAEIgRACg");
        this.shape_369.setTransform(257.4, 110.4);

        this.addChild(this.shape_369, this.shape_368, this.shape_367, this.shape_366, this.shape_365, this.shape_364, this.shape_363, this.shape_362, this.shape_361, this.shape_360, this.shape_359, this.shape_358, this.shape_357, this.shape_356, this.shape_355, this.shape_354, this.shape_353, this.shape_352, this.shape_351, this.shape_350, this.shape_349, this.shape_348, this.shape_347, this.shape_346, this.shape_345, this.shape_344, this.shape_343, this.shape_342, this.shape_341, this.shape_340, this.shape_339, this.shape_338, this.shape_337, this.shape_336, this.shape_335, this.shape_334, this.shape_333, this.shape_332, this.shape_331, this.shape_330, this.shape_329, this.shape_328, this.shape_327, this.shape_326, this.shape_325, this.shape_324, this.shape_323, this.shape_322, this.shape_321, this.shape_320, this.text_49, this.text_48, this.shape_319, this.shape_318, this.shape_317, this.shape_316, this.shape_315, this.shape_314, this.shape_313, this.shape_312, this.shape_311, this.shape_310, this.shape_309, this.shape_308, this.shape_307, this.shape_306, this.shape_305, this.shape_304, this.shape_303, this.shape_302, this.shape_301, this.shape_300, this.shape_299, this.shape_298, this.shape_297, this.shape_296, this.shape_295, this.shape_294, this.shape_293, this.shape_292, this.shape_291, this.shape_290, this.shape_289, this.shape_288, this.shape_287, this.shape_286, this.shape_285, this.shape_284, this.shape_283, this.shape_282, this.shape_281, this.shape_280, this.shape_279, this.shape_278, this.shape_277, this.shape_276, this.shape_275, this.shape_274, this.shape_273, this.shape_272, this.shape_271, this.shape_270, this.shape_269, this.shape_268, this.shape_267, this.shape_266, this.shape_265, this.shape_264, this.text_47, this.text_46, this.text_45, this.text_44, this.text_43, this.text_42, this.text_41, this.text_40, this.text_39, this.text_38, this.text_37, this.text_36, this.text_35, this.text_34, this.text_33, this.text_32, this.text_31, this.text_30, this.shape_263, this.shape_262, this.shape_261, this.shape_260, this.shape_259, this.shape_258, this.shape_257, this.shape_256, this.shape_255, this.shape_254, this.shape_253, this.shape_252, this.shape_251, this.shape_250, this.shape_249, this.shape_248, this.shape_247, this.shape_246, this.shape_245, this.shape_244, this.shape_243, this.shape_242, this.shape_241, this.shape_240, this.shape_239, this.shape_238, this.shape_237, this.shape_236, this.shape_235, this.shape_234, this.shape_233, this.shape_232, this.shape_231, this.shape_230, this.shape_229, this.shape_228, this.shape_227, this.shape_226, this.shape_225, this.shape_224, this.shape_223, this.shape_222, this.shape_221, this.shape_220, this.shape_219, this.shape_218, this.shape_217, this.shape_216, this.shape_215, this.shape_214, this.shape_213, this.shape_212, this.shape_211, this.shape_210, this.shape_209, this.shape_208, this.text_29, this.text_28, this.text_27, this.text_26, this.text_25, this.text_24, this.text_23, this.text_22, this.text_21, this.text_20, this.shape_207, this.shape_206, this.shape_205, this.shape_204, this.shape_203, this.shape_202, this.shape_201, this.shape_200, this.shape_199, this.shape_198, this.shape_197, this.shape_196, this.shape_195, this.shape_194, this.shape_193, this.shape_192, this.shape_191, this.shape_190, this.shape_189, this.shape_188, this.shape_187, this.shape_186, this.shape_185, this.shape_184, this.shape_183, this.shape_182, this.shape_181, this.shape_180, this.shape_179, this.shape_178, this.shape_177, this.shape_176, this.shape_175, this.shape_174, this.shape_173, this.shape_172, this.shape_171, this.shape_170, this.shape_169, this.shape_168, this.shape_167, this.shape_166, this.shape_165, this.shape_164, this.shape_163, this.shape_162, this.shape_161, this.shape_160, this.shape_159, this.shape_158, this.shape_157, this.shape_156, this.shape_155, this.shape_154, this.shape_153, this.shape_152, this.text_19, this.text_18, this.text_17, this.text_16, this.text_15, this.text_14, this.text_13, this.text_12, this.text_11, this.text_10, this.shape_151, this.shape_150, this.shape_149, this.shape_148, this.shape_147, this.shape_146, this.shape_145, this.shape_144, this.shape_143, this.shape_142, this.shape_141, this.shape_140, this.shape_139, this.shape_138, this.shape_137, this.shape_136, this.shape_135, this.shape_134, this.shape_133, this.shape_132, this.shape_131, this.shape_130, this.shape_129, this.shape_128, this.shape_127, this.shape_126, this.shape_125, this.shape_124, this.shape_123, this.shape_122, this.shape_121, this.shape_120, this.shape_119, this.shape_118, this.shape_117, this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.ra1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 193.8);


    // stage content:
    (lib.p23 = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(312.7, 532.8, 1, 1, 0, 0, 0, 256.3, 108.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(312.7, 320.5, 1, 1, 0, 0, 0, 256.3, 96.5);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(313, 134.5, 1, 1, 0, 0, 0, 254.6, 76.8);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
