(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p144_1.png",
            id: "p144_1"
        }]
    };
    // symbols:

    (lib.p144_1 = function() {
        this.initialize(img.p144_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("144", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(33.4, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 15, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape, this.textbox_group1, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Fortsätt mönstret.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(22, 31);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(4, 31);

        this.text_2 = new cjs.Text("Kluring", "18px 'Myriad Pro'", "#8490C8");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(-1, -2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 3, 514, 235, 10);
        this.roundRect1.setTransform(0, 20);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['65', '115', '164', '214'];

        var xPos = 30,
            padding,
            yPos,
            rectWidth = 425,
            rectHeight = 28.4,
            boxWidth = 28.4,
            boxHeight = 28.4,
            numberOfBoxes = 15,
            numberOfRects = 1;
        //drawRect(30,65,boxWidth*4,boxHeight)
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 7;
                if (i == 1) {
                    padding = padding + 35;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#000000").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);

                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#000000").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);

                }
            }
        }

        this.textbox_group1.setTransform(0, 0);

        this.textbox1 = new cjs.Shape();
        this.textbox1.graphics.f("#24B24B").ss().drawRect(30 + 7, 65, boxWidth * 4, boxHeight);
        this.textbox2 = new cjs.Shape();
        this.textbox2.graphics.f("#24B24B").ss().drawRect(30 + 7, 115, boxWidth * 5, boxHeight);
        this.textbox3 = new cjs.Shape();
        this.textbox3.graphics.f("#0094D9").ss().drawRect(30 + 7, 164, boxWidth * 4, boxHeight);
        this.textbox4 = new cjs.Shape();
        this.textbox4.graphics.f("#DB2128").ss().drawRect(30 + 7, 214, boxWidth * 4, boxHeight);
        //DB2128
        this.Line_1_1 = new cjs.Shape();
        this.Line_1_1.graphics.f("#FFF679").ss(0.5).moveTo(37, 65).lineTo(65.4, 93.4).lineTo(93.8, 65).lineTo(122.2, 93.4).lineTo(150.6, 65).lineTo(37, 65);
        this.Line_1_1.setTransform(0, 0);

        this.Line_2_1 = new cjs.Shape();
        this.Line_2_1.graphics.f("#FFF679").s("#000000").ss(0.5).moveTo(37, 115).lineTo(65.4, 143.4).lineTo(65.4, 115).lineTo(93.8, 143.4).lineTo(93.8, 115).lineTo(37, 115);
        this.Line_2_1.setTransform(0, 0);

        this.Line_2_2 = new cjs.Shape();
        this.Line_2_2.graphics.f("#FFF679").s("#000000").ss(0.5).moveTo(122.2, 115).lineTo(150.6, 143.4).lineTo(150.6, 115).lineTo(179, 143.4).lineTo(179, 115).lineTo(122.2, 115);
        this.Line_2_2.setTransform(0, 0);

        this.Line_2_3 = new cjs.Shape();
        this.Line_2_3.graphics.f("").s("#000000").ss(0.5).moveTo(93.8, 115).lineTo(122.2, 143.4);
        this.Line_2_3.setTransform(0, 0);

        this.Line_3_1 = new cjs.Shape();
        this.Line_3_1.graphics.f("#FFF679").s("#000000").ss(0.5).moveTo(37, 192.4).lineTo(65.4, 164).lineTo(65.4, 192.4).lineTo(37, 192.4);
        this.Line_3_1.setTransform(0, 0);

        this.Line_3_2 = new cjs.Shape();
        this.Line_3_2.graphics.f("#DA2129").s("#000000").ss(0.5).moveTo(65.4, 192.4).lineTo(93.8, 164).lineTo(93.8, 192.4).lineTo(65.4, 192.4);
        this.Line_3_2.setTransform(0, 0);

        this.Line_3_3 = new cjs.Shape();
        this.Line_3_3.graphics.f("#FFF679").s("#000000").ss(0.5).moveTo(93.8, 192.4).lineTo(122.2, 164).lineTo(122.2, 192.4).lineTo(93.8, 192.4);
        this.Line_3_3.setTransform(0, 0);

        this.Line_3_4 = new cjs.Shape();
        this.Line_3_4.graphics.f("#DA2129").s("#000000").ss(0.5).moveTo(122.2, 192.4).lineTo(150.6, 164).lineTo(150.6, 192.4).lineTo(122.2, 192.4);
        this.Line_3_4.setTransform(0, 0);


        this.Line_4_1 = new cjs.Shape();
        this.Line_4_1.graphics.f("#0095DA").s("#000000").ss(0.5).moveTo(37, 242.4).lineTo(65.4, 214).lineTo(93.8, 214).lineTo(65.4, 242.4).lineTo(37, 242.4);
        this.Line_4_1.setTransform(0, 0);

        this.Line_4_2 = new cjs.Shape();
        this.Line_4_2.graphics.f("#0095DA").s("#000000").ss(0.5).moveTo(93.8, 242.4).lineTo(122.2, 214).lineTo(150.6, 214).lineTo(122.2, 242.4).lineTo(93.8, 242.4);
        this.Line_4_2.setTransform(0, 0);


        this.addChild(this.roundRect1, this.text_2, this.text, this.text_1, this.instance, this.textbox1, this.textbox2, this.textbox3, this.textbox4, this.Line_1_1, this.Line_2_1, this.Line_2_2, this.Line_2_3, this.Line_3_1, this.Line_3_2, this.Line_3_3, this.Line_3_4, this.Line_4_1, this.Line_4_2, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 290);
    (lib.DotMatrix = function(columns, rows, columnSpace, rowSpace, showNumbers) {
        this.initialize();
        this.matrix = []
        for (var col = 0; col < columns; col++) {
            for (var row = 0; row < rows; row++) {
                var temp_text1 = new cjs.Shape();
                temp_text1.graphics.f("#A9AFB0").arc(0, 0, 1.5, 0, 2 * Math.PI, -1);
                temp_text1.setTransform((col * columnSpace), (row * rowSpace));



                this.matrix.push({
                    x: (col * columnSpace),
                    y: (row * rowSpace)
                });

                this.addChild(temp_text1);
                if (showNumbers) {
                    var text = new cjs.Text((this.matrix.length - 1) + "", "bold 10px 'Myriad Pro'", "#ff0000");
                    text.setTransform((col * columnSpace), (row * rowSpace));
                    this.addChild(text);
                }
            }
        }
        p.matrix = this.matrix;
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1.5, 0, 514, 320, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(25, 8);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(6, 8);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('#ffffff').s("#8390C8").ss(0.8).drawRect(58, 34, 142, 274);
        this.rect1.setTransform(0, 0);

        this.rect2 = new cjs.Shape();
        this.rect2.graphics.f('#ffffff').s("#8390C8").ss(0.8).drawRect(305, 34, 142, 274);
        this.rect2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#8390C8").ss(0.8).moveTo(58, 171).lineTo(200, 171);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.s("#8390C8").ss(0.8).moveTo(305, 171).lineTo(447, 171);
        this.Line_2.setTransform(0, 0);
        var matrixPosX1 = 70,
            matrixPosY1 = 44,
            matrixPosX2 = 318,
            matrixPosY2 = 44,
            matrixPosX3 = 70,
            matrixPosY3 = 181.3,
            matrixPosX4 = 318,
            matrixPosY4 = 181.3;
        this.matrix1 = new lib.DotMatrix(5, 5, 29, 29);
        this.matrix1.setTransform(matrixPosX1, matrixPosY1);
        this.matrix2 = new lib.DotMatrix(5, 5, 29, 29);
        this.matrix2.setTransform(matrixPosX2, matrixPosY2);
        this.matrix3 = new lib.DotMatrix(5, 5, 29, 29);
        this.matrix3.setTransform(matrixPosX3, matrixPosY3);
        this.matrix4 = new lib.DotMatrix(5, 5, 29, 29);
        this.matrix4.setTransform(matrixPosX4, matrixPosY4);
        //this.matrix1=new lib.DotMatrix(columns,rows,columnSpace,rowSpace);



        this.Line_1_1 = new cjs.Shape();
        this.Line_1_1.graphics.f('#BD8146').s("#7C7C7C").ss(0.8).moveTo(this.matrix1.matrix[0].x, this.matrix1.matrix[0].y).lineTo(this.matrix1.matrix[1].x, this.matrix1.matrix[1].y).lineTo(this.matrix1.matrix[7].x, this.matrix1.matrix[7].y).lineTo(this.matrix1.matrix[6].x, this.matrix1.matrix[6].y).lineTo(this.matrix1.matrix[5].x, this.matrix1.matrix[5].y).lineTo(this.matrix1.matrix[0].x, this.matrix1.matrix[0].y);
        this.Line_1_1.setTransform(matrixPosX1, matrixPosY1);

        this.Line_1_2 = new cjs.Shape();
        this.Line_1_2.graphics.f('#FFF579').s("#7C7C7C").ss(0.8).moveTo(this.matrix1.matrix[1].x, this.matrix1.matrix[1].y).lineTo(this.matrix1.matrix[2].x, this.matrix1.matrix[2].y).lineTo(this.matrix1.matrix[8].x, this.matrix1.matrix[8].y).lineTo(this.matrix1.matrix[14].x, this.matrix1.matrix[14].y).lineTo(this.matrix1.matrix[18].x, this.matrix1.matrix[18].y).lineTo(this.matrix1.matrix[22].x, this.matrix1.matrix[22].y).lineTo(this.matrix1.matrix[21].x, this.matrix1.matrix[21].y).lineTo(this.matrix1.matrix[17].x, this.matrix1.matrix[17].y).lineTo(this.matrix1.matrix[16].x, this.matrix1.matrix[16].y).lineTo(this.matrix1.matrix[15].x, this.matrix1.matrix[15].y).lineTo(this.matrix1.matrix[11].x, this.matrix1.matrix[11].y).lineTo(this.matrix1.matrix[5].x, this.matrix1.matrix[5].y)
            .lineTo(this.matrix1.matrix[6].x, this.matrix1.matrix[6].y).lineTo(this.matrix1.matrix[7].x, this.matrix1.matrix[7].y).lineTo(this.matrix1.matrix[1].x, this.matrix1.matrix[1].y);
        this.matrix1.children[7].visible = false;
        this.matrix1.children[17].visible = false;
        this.Line_1_2.setTransform(matrixPosX1, matrixPosY1);

        this.Line_1_3 = new cjs.Shape();
        this.Line_1_3.graphics.f('#BD8146').s("#7C7C7C").ss(0.8).moveTo(this.matrix1.matrix[15].x, this.matrix1.matrix[15].y).lineTo(this.matrix1.matrix[16].x, this.matrix1.matrix[16].y).lineTo(this.matrix1.matrix[17].x, this.matrix1.matrix[17].y).lineTo(this.matrix1.matrix[21].x, this.matrix1.matrix[21].y).lineTo(this.matrix1.matrix[20].x, this.matrix1.matrix[20].y).lineTo(this.matrix1.matrix[15].x, this.matrix1.matrix[15].y);
        this.Line_1_3.setTransform(matrixPosX1, matrixPosY1);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.s("#7C7C7C").ss(0.8).moveTo(this.matrix3.matrix[14].x, this.matrix3.matrix[14].y).lineTo(this.matrix3.matrix[18].x, this.matrix3.matrix[18].y).moveTo(this.matrix3.matrix[14].x, this.matrix3.matrix[14].y).lineTo(this.matrix3.matrix[8].x, this.matrix3.matrix[13].y);
        this.matrix3.children[7].visible = false;
        this.matrix3.children[17].visible = false;
        this.Line_3.setTransform(matrixPosX3, matrixPosY3);


        // FFF679
        this.Line_2_1 = new cjs.Shape();
        this.Line_2_1.graphics.f('#FFF679').s("#7C7C7C").ss(0.8).moveTo(this.matrix2.matrix[0].x, this.matrix2.matrix[0].y).lineTo(this.matrix2.matrix[1].x, this.matrix2.matrix[1].y).lineTo(this.matrix2.matrix[2].x, this.matrix2.matrix[2].y).lineTo(this.matrix2.matrix[7].x, this.matrix2.matrix[7].y).lineTo(this.matrix2.matrix[0].x, this.matrix2.matrix[0].y)

        .moveTo(this.matrix2.matrix[10].x, this.matrix2.matrix[10].y).lineTo(this.matrix2.matrix[15].x, this.matrix2.matrix[15].y).lineTo(this.matrix2.matrix[20].x, this.matrix2.matrix[20].y).lineTo(this.matrix2.matrix[11].x, this.matrix2.matrix[11].y).lineTo(this.matrix2.matrix[10].x, this.matrix2.matrix[10].y)

        .moveTo(this.matrix2.matrix[4].x, this.matrix2.matrix[4].y).lineTo(this.matrix2.matrix[9].x, this.matrix2.matrix[9].y).lineTo(this.matrix2.matrix[14].x, this.matrix2.matrix[14].y).lineTo(this.matrix2.matrix[13].x, this.matrix2.matrix[13].y).lineTo(this.matrix2.matrix[4].x, this.matrix2.matrix[4].y)

        .moveTo(this.matrix2.matrix[17].x, this.matrix2.matrix[17].y).lineTo(this.matrix2.matrix[22].x, this.matrix2.matrix[22].y).lineTo(this.matrix2.matrix[23].x, this.matrix2.matrix[23].y).lineTo(this.matrix2.matrix[24].x, this.matrix2.matrix[24].y).lineTo(this.matrix2.matrix[17].x, this.matrix2.matrix[17].y);
        this.Line_2_1.setTransform(matrixPosX2, matrixPosY2);


        // FFF679
        this.Line_2_2 = new cjs.Shape();
        this.Line_2_2.graphics.f('#DB2128').s("#7C7C7C").ss(0.8).moveTo(this.matrix2.matrix[0].x, this.matrix2.matrix[0].y).lineTo(this.matrix2.matrix[7].x, this.matrix2.matrix[7].y).lineTo(this.matrix2.matrix[11].x, this.matrix2.matrix[11].y).lineTo(this.matrix2.matrix[0].x, this.matrix2.matrix[0].y)

        .moveTo(this.matrix2.matrix[7].x, this.matrix2.matrix[7].y).lineTo(this.matrix2.matrix[4].x, this.matrix2.matrix[4].y).lineTo(this.matrix2.matrix[13].x, this.matrix2.matrix[13].y).lineTo(this.matrix2.matrix[7].x, this.matrix2.matrix[7].y)

        .moveTo(this.matrix2.matrix[13].x, this.matrix2.matrix[13].y).lineTo(this.matrix2.matrix[24].x, this.matrix2.matrix[24].y).lineTo(this.matrix2.matrix[17].x, this.matrix2.matrix[17].y).lineTo(this.matrix2.matrix[13].x, this.matrix2.matrix[13].y)

        .moveTo(this.matrix2.matrix[17].x, this.matrix2.matrix[17].y).lineTo(this.matrix2.matrix[20].x, this.matrix2.matrix[20].y).lineTo(this.matrix2.matrix[11].x, this.matrix2.matrix[11].y).lineTo(this.matrix2.matrix[17].x, this.matrix2.matrix[17].y);
        this.Line_2_2.setTransform(matrixPosX2, matrixPosY2);


        this.Line_2_3 = new cjs.Shape();
        this.Line_2_3.graphics.f('#0095DA').s("#7C7C7C").ss(0.8).drawRect(this.matrix2.matrix[0].x, this.matrix2.matrix[0].y, this.matrix2.matrix[24].x, this.matrix2.matrix[24].y);
        this.Line_2_3.setTransform(matrixPosX2, matrixPosY2);

        this.addChild(this.roundRect1, this.text, this.text_1, this.rect1, this.rect2);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.s("#7C7C7C").ss(0.8).moveTo(this.matrix4.matrix[10].x, this.matrix4.matrix[10].y).lineTo(this.matrix4.matrix[11].x, this.matrix4.matrix[11].y).lineTo(this.matrix4.matrix[17].x, this.matrix4.matrix[17].y).moveTo(this.matrix4.matrix[11].x, this.matrix4.matrix[11].y).lineTo(this.matrix4.matrix[7].x, this.matrix4.matrix[7].y);
        this.Line_4.setTransform(matrixPosX4, matrixPosY4);


        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#000000").s("#7C7C7C").ss(0.8, 0, 0, 3).arc(10, 10, 3, 0, 2 * Math.PI);
        this.shape_group1.setTransform(89.2, 92.2);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#000000").s("#7C7C7C").ss(0.8, 0, 0, 3).arc(10, 10, 3, 0, 2 * Math.PI);
        this.shape_group2.setTransform(147.2, 92.2);

        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f("#000000").s("#7C7C7C").ss(0.8).moveTo(this.matrix1.matrix[14].x, this.matrix1.matrix[14].y).arc(this.matrix1.matrix[14].x, this.matrix1.matrix[14].y, 10, 7 * Math.PI / 4, 5 * Math.PI / 4, -1);
        this.shape_group3.setTransform(matrixPosX1, matrixPosY1);

        this.shape_group4 = new cjs.Shape();
        this.shape_group4.graphics.f("#000000").s("#7C7C7C").ss(0.8, 0, 0, 3).arc(10, 10, 3, 0, 2 * Math.PI);
        this.shape_group4.setTransform(89.2, 229.2);

        this.shape_group5 = new cjs.Shape();
        this.shape_group5.graphics.f("#000000").s("#7C7C7C").ss(0.8, 0, 0, 3).arc(10, 10, 3, 0, 2 * Math.PI);
        this.shape_group5.setTransform(147.2, 229.2);


        this.shape_group6 = new cjs.Shape();
        this.shape_group6.graphics.f("#000000").s("#7C7C7C").ss(0.8).moveTo(this.matrix3.matrix[14].x, this.matrix3.matrix[14].y).arc(this.matrix1.matrix[14].x, this.matrix1.matrix[14].y, 10, 7 * Math.PI / 4, 5 * Math.PI / 4, -1);
        this.shape_group6.setTransform(matrixPosX3, matrixPosY3);
        this.addChild(this.Line_2_3, this.Line_2_1, this.Line_2_2);
        this.addChild(this.Line_1_2, this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5, this.shape_group6, this.Line_1_1, this.Line_1_3, this.matrix1, this.matrix2, this.matrix3, this.matrix4);


        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 320.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(292.3, 102, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(292.3, 354, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
