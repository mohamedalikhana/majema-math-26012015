(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p113_1.png",
            id: "p113_1"
        }, {
            src: "images/p113_2.png",
            id: "p113_2"
        }]
    };

    (lib.p113_1 = function() {
        this.initialize(img.p113_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p113_2 = function() {
        this.initialize(img.p113_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("39", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(67, 22);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#8490C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(56, 23.5, 1.09, 1);

        this.text_2 = new cjs.Text("Vad kostar det?", "24px 'MyriadPro-Semibold'", "#8490C8");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(113.5, 25);

        this.pageBottomText = new cjs.Text("kunna räkna addition och subtraktion med mynt upp till 10 kr", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(312, 651);

        this.text = new cjs.Text("113", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(560, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(587, 660.8);

        this.instance = new lib.p113_1();
        this.instance.setTransform(84, 52, 0.52, 0.542);

        this.addChild(this.instance, this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur mycket kostar leksakerna tillsammans?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.instance_2 = new lib.p113_2();
        this.instance_2.setTransform(18, 33, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 116, 10);
        this.roundRect1.setTransform(0, 17);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 17);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 139);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 139);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 261);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 261);

        var arrTxtbox = ['114', '236', '358'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 180,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 9,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 37;
                if (i == 1) {
                    padding = 59;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(37, 104);
        this.text_2 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(78.2, 104);
        this.text_3 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(97, 104);
        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(138, 104);
        this.text_5 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 35;
        this.text_5.setTransform(116, 109);
        this.text_6 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 35;
        this.text_6.setTransform(56, 109);
        // row-1
        this.text_7 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 13;
        this.text_7.setTransform(89, 66);
        this.text_7.skewX = -9;
        this.text_7.skewY = -9;
        this.text_8 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_8.lineHeight = 13;
        this.text_8.setTransform(194, 52);
        this.text_8.skewX = -13;
        this.text_8.skewY = -13;
        this.text_9 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 13;
        this.text_9.setTransform(298, 53);
        this.text_9.skewX = 9;
        this.text_9.skewY = 9;
        this.text_10 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_10.lineHeight = 13;
        this.text_10.setTransform(452, 54);
        this.text_10.skewX = -13;
        this.text_10.skewY = -13;
        // row-2
        this.text_11 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_11.lineHeight = 13;
        this.text_11.setTransform(32, 169);
        this.text_11.skewX = 9;
        this.text_11.skewY = 9;
        this.text_12 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_12.lineHeight = 13;
        this.text_12.setTransform(198, 176);
        this.text_12.skewX = -18;
        this.text_12.skewY = -18;
        this.text_13 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_13.lineHeight = 13;
        this.text_13.setTransform(336, 179);
        this.text_13.skewX = -20;
        this.text_13.skewY = -20;
        this.text_14 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_14.lineHeight = 13;
        this.text_14.setTransform(451, 168);
        this.text_14.skewX = 13;
        this.text_14.skewY = 13;
        // row-3
        this.text_15 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_15.lineHeight = 13;
        this.text_15.setTransform(91, 289);
        this.text_15.skewX = -9;
        this.text_15.skewY = -9;
        this.text_16 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_16.lineHeight = 13;
        this.text_16.setTransform(194, 307);
        this.text_16.skewX = 13;
        this.text_16.skewY = 13;
        this.text_17 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_17.lineHeight = 13;
        this.text_17.setTransform(296, 289);
        this.text_17.skewX = 9;
        this.text_17.skewY = 9;
        this.text_18 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_18.lineHeight = 13;
        this.text_18.setTransform(452, 291.5);
        this.text_18.skewX = -13;
        this.text_18.skewY = -13;

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14);
        this.addChild(this.text_15, this.text_16, this.text_17, this.text_18);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(308, 465, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
