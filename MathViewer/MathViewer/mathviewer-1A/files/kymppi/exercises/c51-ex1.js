var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c51_ex1_1.png",
            id: "c51_ex1_1"
        }, {
            src: "images/c51_ex1_2.png",
            id: "c51_ex1_2"
        }, {
            src: "images/c51_ex1_3.png",
            id: "c51_ex1_3"
        }, {
            src: "images/c51_ex1_4.png",
            id: "c51_ex1_4"
        }, {
            src: "images/c51_ex1_5.png",
            id: "c51_ex1_5"
        }]
    };

    var moveX = 715,
        moveY = 520;

    (lib.c51_ex1_1 = function() {
        this.initialize(img.c51_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c51_ex1_2 = function() {
        this.initialize(img.c51_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c51_ex1_3 = function() {
        this.initialize(img.c51_ex1_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c51_ex1_4 = function() {
        this.initialize(img.c51_ex1_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c51_ex1_5 = function() {
        this.initialize(img.c51_ex1_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Mot programmering 1", "bold 24px 'Myriad Pro'", "#D83770");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("51", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();
        this.img_roboMsg = new lib.c51_ex1_1();
        this.img_roboMsg.setTransform(920, 260);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7d7d7d").setStrokeStyle(2).moveTo(980, 230).lineTo(1000, 260).lineTo(1000, 230);
        this.Line_1.setTransform(0, 0);

        this.round_rect = new cjs.Shape();
        this.round_rect.graphics.f("#ffffff").s('#7d7d7d').ss(2).drawRoundRect(760, -80, 550, 260 + 50, 10);
        this.round_rect.setTransform(0, 0);

        this.img_robo = new lib.c51_ex1_2();
        this.img_robo.setTransform(100, 345);

        this.img_car = new lib.c51_ex1_3();
        this.img_car.setTransform(100, 120);
        this.img_car.visible = false;

        this.img_drum = new lib.c51_ex1_4();
        this.img_drum.setTransform(340, 120);
        this.img_drum.visible = false;

        this.img_ball = new lib.c51_ex1_5();
        this.img_ball.setTransform(470, 240);
        this.img_ball.visible = false;

        var ToBeAdded = [];
        for (var row = 0; row < 4; row++) {
            for (var col = 0; col < 4; col++) {
                var temp_rect = new cjs.Shape();
                temp_rect.graphics.f("#ffffff").s('#7d7d7d').drawRect(80, -30, 120, 120);
                temp_rect.setTransform(0 + (col * 120), 0 + (row * 120));
                ToBeAdded.push(temp_rect);
            }
        }

        this.round_rectRed = new cjs.Shape();
        this.round_rectRed.graphics.f("#ffffff").s('red').ss(2).drawRoundRect(80, 463, 479, 100, 10);
        this.round_rectRed.setTransform(0 + moveX, 0 - moveY);
        this.round_rectRed.visible = false;
        ToBeAdded.push(this.round_rectRed);

        this.arrow_box = [];
        for (var col = 0; col < 4; col++) {
            var temp_rect = new cjs.Shape();
            temp_rect.graphics.f("#ffffff").s('#7d7d7d').drawRect(100, 473, 80, 80);
            temp_rect.setTransform(0 + (col * 120) + moveX, 0 - moveY);
            temp_rect.visible = false;
            this.arrow_box.push(temp_rect);
            ToBeAdded.push(temp_rect);
        }

        this.arrowSet_1 = new cjs.Container();

        this.tempLine_1 = new cjs.Shape();
        this.tempLine_1.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(112, 514).lineTo(154, 514).lineTo(154, 508).lineTo(167, 514).lineTo(154, 520).lineTo(154, 514);
        this.tempLine_1.setTransform(0, 0);
        this.tempLine_2 = new cjs.Shape();
        this.tempLine_2.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(112, 514).lineTo(154, 514).lineTo(154, 508).lineTo(167, 514).lineTo(154, 520).lineTo(154, 514);
        this.tempLine_2.setTransform(120, 0);
        this.tempLine_3 = new cjs.Shape();
        this.tempLine_3.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(112, 514).lineTo(154, 514).lineTo(154, 508).lineTo(167, 514).lineTo(154, 520).lineTo(154, 514);
        this.tempLine_3.setTransform(240, 0);
        this.tempLine_4 = new cjs.Shape();
        this.tempLine_4.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(140, 542).lineTo(140, 500).lineTo(134, 500).lineTo(140, 487).lineTo(146, 500).lineTo(140, 500);
        this.tempLine_4.setTransform(360, 0);

        this.arrowSet_1.addChild(this.tempLine_1, this.tempLine_2, this.tempLine_3, this.tempLine_4);
        this.arrowSet_1.setTransform(moveX, 0 - moveY);
        this.arrowSet_1.visible = false;

        this.arrowSet_2 = new cjs.Container();

        this.tempLine_5 = new cjs.Shape();
        this.tempLine_5.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(140, 542).lineTo(140, 500).lineTo(134, 500).lineTo(140, 487).lineTo(146, 500).lineTo(140, 500);
        this.tempLine_5.setTransform(0, 0);
        this.tempLine_6 = new cjs.Shape();
        this.tempLine_6.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(140, 542).lineTo(140, 500).lineTo(134, 500).lineTo(140, 487).lineTo(146, 500).lineTo(140, 500);
        this.tempLine_6.setTransform(120, 0);
        this.tempLine_7 = new cjs.Shape();
        this.tempLine_7.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(167, 514).lineTo(125, 514).lineTo(125, 520).lineTo(112, 514).lineTo(125, 508).lineTo(125, 514);
        this.tempLine_7.setTransform(240, 0);
        this.tempLine_8 = new cjs.Shape();
        this.tempLine_8.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(167, 514).lineTo(125, 514).lineTo(125, 520).lineTo(112, 514).lineTo(125, 508).lineTo(125, 514);
        this.tempLine_8.setTransform(360, 0);

        this.arrowSet_2.addChild(this.tempLine_5, this.tempLine_6, this.tempLine_7, this.tempLine_8);
        this.arrowSet_2.setTransform(moveX, 0 - moveY);
        this.arrowSet_2.visible = false;

        this.instrTexts = [];
        this.instrTexts.push('Rutroboten.');
        this.instrTexts.push('Roboten styrs med \n\n\n hjälp av pilkoden.');
        this.instrTexts.push('   betyder ett steg \n\n\n åt höger.');
        this.instrTexts.push('  betyder ett steg \n\n\n uppåt.');
        this.instrTexts.push('   betyder ett steg \n\n\n åt vänster.');
        this.instrTexts.push('  betyder ett steg \n\n\n neråt.');
        this.instrTexts.push('Koden kan bestå \n\n\n av flera steg.');
        this.instrTexts.push('Till vilken leksak \n\n\n leder koden?');
        this.instrTexts.push('Koden ledde till \n\n\n bollen.');

        this.arrow_right = new cjs.Shape();
        this.arrow_right.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(790, 43).lineTo(832, 43).lineTo(832, 37).lineTo(845, 43).lineTo(832, 49).lineTo(832, 43);
        this.arrow_right.visible = false;
        this.arrow_right.setTransform(35, 0);

        this.arrow_up = new cjs.Shape();
        this.arrow_up.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(818, 71).lineTo(818, 29).lineTo(812, 29).lineTo(818, 16).lineTo(824, 29).lineTo(818, 29);
        this.arrow_up.visible = false;
        this.arrow_up.setTransform(45, 0);

        this.arrow_left = new cjs.Shape();
        this.arrow_left.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(845, 43).lineTo(803, 43).lineTo(803, 49).lineTo(790, 43).lineTo(803, 37).lineTo(803, 43);
        this.arrow_left.visible = false;
        this.arrow_left.setTransform(40, 0);

        this.arrow_down = new cjs.Shape();
        this.arrow_down.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(818, 14).lineTo(818, 56).lineTo(812, 56).lineTo(818, 69).lineTo(824, 56).lineTo(818, 56);
        this.arrow_down.visible = false;
        this.arrow_down.setTransform(45, 0);

        this.instrText1 = new cjs.Text(this.instrTexts[1], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(1030, 20);

        this.addChild(this.img_roboMsg, this.Line_1, this.round_rect, this.instrText1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }
        this.addChild(this.img_car, this.img_drum, this.img_ball, this.img_robo, this.arrowSet_1, this.arrowSet_2, this.arrow_right, this.arrow_up, this.arrow_left, this.arrow_down);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[2];
        this.stage0.arrow_right.visible = true;

        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x + 120,
                y: this.stage0.img_robo.y
            },
            wait: 1500,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[3];
        this.stage0.img_robo.x = this.stage0.img_robo.x + 120;
        this.stage0.arrow_up.visible = true;

        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y - 120
            },
            wait: 1500,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[4];
        this.stage0.img_robo.x = this.stage0.img_robo.x + 120;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 120;
        this.stage0.arrow_left.visible = true;

        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x - 120,
                y: this.stage0.img_robo.y
            },
            wait: 1500,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[5];
        this.stage0.img_robo.y = this.stage0.img_robo.y - 120;
        this.stage0.arrow_down.visible = true;

        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y + 120
            },
            wait: 1500,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage6 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[6];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_1.visible = true;
        this.stage0.round_rectRed.visible = true;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.round_rectRed,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage7 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_1.visible = true;
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_ball.visible = true;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage8 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_1.visible = true;
        this.stage0.arrowSet_1.children[0].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_1.children[0].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_ball.visible = true;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x + 120,
                y: this.stage0.img_robo.y
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage9 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 120;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_1.visible = true;
        this.stage0.arrowSet_1.children[1].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_1.children[1].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_ball.visible = true;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x + 120,
                y: this.stage0.img_robo.y
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage10 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_1.visible = true;
        this.stage0.arrowSet_1.children[2].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_1.children[2].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_ball.visible = true;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x + 120,
                y: this.stage0.img_robo.y
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage11 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 360;
        this.stage0.img_robo.scaleX = this.stage0.img_robo.scaleY = 0.9;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_1.visible = true;
        this.stage0.arrowSet_1.children[3].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_1.children[3].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.scaleX = this.stage0.img_ball.scaleY = 0.9;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x - 20,
                y: this.stage0.img_robo.y - 120
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_ball,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_ball.x,
                y: this.stage0.img_ball.y
            },
            positionTo: {
                x: this.stage0.img_ball.x + 35,
                y: this.stage0.img_ball.y
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage12 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 340;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 120;
        this.stage0.img_robo.scaleX = this.stage0.img_robo.scaleY = 0.9;
        this.stage0.instrText1.text = this.stage0.instrTexts[8];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_1.visible = true;
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.x = this.stage0.img_ball.x + 35;
        this.stage0.img_ball.scaleX = this.stage0.img_ball.scaleY = 0.9;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage13 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_2.visible = true;
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_drum.x = this.stage0.img_drum.x + 120;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.x = this.stage0.img_ball.x - 240;
        this.stage0.img_ball.y = this.stage0.img_ball.y - 240;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage14 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_2.visible = true;
        this.stage0.arrowSet_2.children[0].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_2.children[0].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_drum.x = this.stage0.img_drum.x + 120;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.x = this.stage0.img_ball.x - 240;
        this.stage0.img_ball.y = this.stage0.img_ball.y - 240;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y - 120
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage15 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 120;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_2.visible = true;
        this.stage0.arrowSet_2.children[1].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_2.children[1].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_drum.x = this.stage0.img_drum.x + 120;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.x = this.stage0.img_ball.x - 240;
        this.stage0.img_ball.y = this.stage0.img_ball.y - 240;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y - 120
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage16 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 240;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_2.visible = true;
        this.stage0.arrowSet_2.children[2].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_2.children[2].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_drum.x = this.stage0.img_drum.x + 120;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.x = this.stage0.img_ball.x - 240;
        this.stage0.img_ball.y = this.stage0.img_ball.y - 240;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x - 120,
                y: this.stage0.img_robo.y
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage17 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 120;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 240;
        this.stage0.img_robo.scaleX = this.stage0.img_robo.scaleY = 0.9;
        this.stage0.instrText1.text = this.stage0.instrTexts[7];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_2.visible = true;
        this.stage0.arrowSet_2.children[3].graphics._fillInstructions[0].params[1] = 'red';
        this.stage0.arrowSet_2.children[3].graphics._strokeInstructions[0].params[1] = 'red';
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_drum.x = this.stage0.img_drum.x + 120;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.x = this.stage0.img_ball.x - 240;
        this.stage0.img_ball.y = this.stage0.img_ball.y - 240;
        this.stage0.img_car.scaleX = this.stage0.img_car.scaleY = 0.9;
        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x - 95,
                y: this.stage0.img_robo.y - 20
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_car,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_car.x,
                y: this.stage0.img_car.y
            },
            positionTo: {
                x: this.stage0.img_car.x - 25,
                y: this.stage0.img_car.y + 33
            },
            wait: 2000,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage18 = function() {
        this.initialize();

        this.stage0 = new lib.Stage0();
        this.stage0.img_robo.x = this.stage0.img_robo.x + 25;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 260;
        this.stage0.img_robo.scaleX = this.stage0.img_robo.scaleY = 0.9;
        this.stage0.instrText1.text = this.stage0.instrTexts[8];
        this.stage0.instrText1.setTransform(1030, 80);
        this.stage0.arrowSet_2.visible = true;
        this.stage0.img_car.visible = true;
        this.stage0.img_drum.visible = true;
        this.stage0.img_drum.x = this.stage0.img_drum.x + 120;
        this.stage0.img_ball.visible = true;
        this.stage0.img_ball.x = this.stage0.img_ball.x - 240;
        this.stage0.img_ball.y = this.stage0.img_ball.y - 240;
        this.stage0.img_car.scaleX = this.stage0.img_car.scaleY = 0.9;
        this.stage0.img_car.x = this.stage0.img_car.x - 25;
        this.stage0.img_car.y = this.stage0.img_car.y + 33;

        var arrowBox = this.stage0.arrow_box;
        for (var i = 0; i < arrowBox.length; i++) {
            arrowBox[i].visible = true;
        };
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage5 = new lib.Stage5();
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage6 = new lib.Stage6();
        this.stage6.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage7 = new lib.Stage7();
        this.stage7.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage8 = new lib.Stage8();
        this.stage8.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage9 = new lib.Stage9();
        this.stage9.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage10 = new lib.Stage10();
        this.stage10.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage11 = new lib.Stage11();
        this.stage11.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage12 = new lib.Stage12();
        this.stage12.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage13 = new lib.Stage13();
        this.stage13.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage14 = new lib.Stage14();
        this.stage14.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage15 = new lib.Stage15();
        this.stage15.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage16 = new lib.Stage16();
        this.stage16.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage17 = new lib.Stage17();
        this.stage17.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage18 = new lib.Stage18();
        this.stage18.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7, this.stage8, this.stage9, this.stage10, this.stage11, this.stage12, this.stage13, this.stage14, this.stage15, this.stage16, this.stage17, this.stage18);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
