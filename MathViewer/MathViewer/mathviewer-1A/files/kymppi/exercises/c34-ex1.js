var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes
    var isSaage3 = false;

    var moveX = -20,
        moveY = -60,
        scaleX = 2,
        scaleY = 2;

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };


    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Räkna högt: 1, 2, 3 … 10.", "40px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 430);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.rectangles = new lib.rectangles();
        this.rectangles.setTransform(415 + 80 + moveX, 20 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.Texts = new lib.Texts();
        this.Texts.text_0.visible = true;
        this.Texts.setTransform(650 + 90 + moveX, 74 + 27 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.boxes = new lib.boxes('#FFFB3F');
        this.boxes.setTransform(650 + moveX, 71 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        this.addChild(this.rectangles, this.commentTexts, this.boxes, this.Texts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.rectangles = new lib.rectangles();
        this.rectangles.setTransform(415 + 80 + moveX, 20 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.Texts = new lib.Texts();
        this.Texts.text_0.visible = true;
        this.Texts.setTransform(662 + 90 + moveX, 82.5 + 27 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.boxes = new lib.boxes('#FFFB3F');
        this.boxes.rectBox1.visible = true;
        this.boxes.rectBox2.visible = true;
        this.boxes.rectBox3.visible = true;
        this.boxes.rectBox4.visible = true;
        this.boxes.rectBox5.visible = true;
        this.boxes.rectBox6.visible = true;
        this.boxes.rectBox7.visible = true;
        this.boxes.rectBox8.visible = true;
        this.boxes.rectBox9.visible = true;
        this.boxes.rectBox10.visible = true;
        this.boxes.setTransform(650 + 90 + moveX, 71 + 27 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        var thisStage = this;
        var interval;
        this.onNewExercise = function(e) {
            if (navType == 'prev') {
                interval = setInterval(function() {
                    thisStage.Texts.text_0.text = (navType == 'prev') ? "10" : "0";
                }, 20);
            } else {
                clearInterval(interval);
            }
        };

        this.tweens = [];
        var alphaTimeout = 0;
        this.tweens.push({
            ref: this.boxes.rectBox1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 900,
            alphaTimeout: alphaTimeout
        });

        for (var i = 0; i <= 10; i++) {
            this.tweens.push({
                ref: this.Texts.text_0,
                textRef: this.Texts.text_0,
                alphaFrom: 1,
                alphaTo: 1,
                wait: 900 * i,
                textFrom: "" + (i - 1) + "",
                textTo: "" + i + "",
                alphaTimeout: alphaTimeout,
                override: false
            });
        }

        this.tweens.push({
            ref: this.Texts.text_0,
            textRef: this.Texts.text_0,
            alphaFrom: 1,
            alphaTo: 1,
            wait: 9000,
            textFrom: "0",
            textTo: "0",
            alphaTimeout: alphaTimeout,
            override: false
        });
        this.tweens.push({
            ref: this.Texts.text_0,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.Texts.text_0.x,
                y: this.Texts.text_0.y
            },
            positionTo: {
                x: this.Texts.text_0.x - 2.2,
                y: this.Texts.text_0.y
            },
            wait: 9000,
            alphaTimeout: 0,
            override: false
        });

        this.tweens.push({
            ref: this.boxes.rectBox2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1800,
            alphaTimeout: alphaTimeout
        });
        this.tweens.push({
            ref: this.boxes.rectBox3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2700,
            alphaTimeout: alphaTimeout
        });
        this.tweens.push({
            ref: this.boxes.rectBox4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 3600,
            alphaTimeout: alphaTimeout
        });
        this.tweens.push({
            ref: this.boxes.rectBox5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 4500,
            alphaTimeout: alphaTimeout
        });

        this.tweens.push({
            ref: this.boxes.rectBox6,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 5400,
            alphaTimeout: alphaTimeout

        });
        this.tweens.push({
            ref: this.boxes.rectBox7,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 6300,
            alphaTimeout: alphaTimeout
        });
        this.tweens.push({
            ref: this.boxes.rectBox8,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 7200,
            alphaTimeout: alphaTimeout
        });
        this.tweens.push({
            ref: this.boxes.rectBox9,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 8100,
            alphaTimeout: alphaTimeout
        });
        this.tweens.push({
            ref: this.boxes.rectBox10,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 9000,
            alphaTimeout: alphaTimeout
        });

        p.tweens = this.tweens;
        this.addChild(this.rectangles, this.commentTexts, this.boxes, this.Texts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.rectangles = new lib.rectangles();
        this.rectangles.setTransform(415 + 80 + moveX, 20 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.Texts = new lib.Texts();
        this.Texts.text_0.visible = true;
        this.Texts.text_1.visible = true;
        this.Texts.setTransform(590 + 90 + moveX, 76 + 27 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.boxes = new lib.boxes('#FFFB3F');
        this.boxes.rectBox1.visible = true;
        this.boxes.rectBox2.visible = true;
        this.boxes.rectBox3.visible = true;
        this.boxes.rectBox4.visible = true;
        this.boxes.rectBox5.visible = true;
        this.boxes.rectBox6.visible = true;
        this.boxes.rectBox7.visible = true;
        this.boxes.rectBox8.visible = true;
        this.boxes.rectBox9.visible = true;
        this.boxes.rectBox10.visible = true;
        this.boxes.setTransform(650 + 90 + moveX, 71 + 27 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.boxes2 = new lib.boxes('#50BF1C');
        this.boxes2.rectBox1.visible = true;
        this.boxes2.rectBox2.visible = true;
        this.boxes2.rectBox3.visible = true;
        this.boxes2.rectBox4.visible = true;
        this.boxes2.rectBox5.visible = true;
        this.boxes2.rectBox6.visible = true;
        this.boxes2.rectBox7.visible = true;
        this.boxes2.rectBox8.visible = true;
        this.boxes2.rectBox9.visible = true;
        this.boxes2.rectBox10.visible = true;
        this.boxes2.setTransform(650 + 90 + moveX, 71 + 27 + moveY, 3.5 + scaleX, 4 + scaleY, 0, 0, 0, 0, 0);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        var thisStage = this;
        this.onNewExercise = function(e) {
            thisStage.Texts.text_0.text = "0";
            thisStage.commentTexts.hintText1.text = "Räkna högt: 1, 2, 3 … 10.";
            if (isSaage3 == false) {
                thisStage.Texts.text_0.x = thisStage.Texts.text_0.x + 17.2;
                thisStage.Texts.text_0.y = thisStage.Texts.text_0.y - 0.3;
                isSaage3 = true;
            }
            setTimeout(function() {
                thisStage.commentTexts.hintText1.text = "10 ental är ett tiotal.";
            }, 3200);
        };

        this.tweens = [];

        this.tweens.push({
            ref: this.boxes,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 650 + 70,
                y: 37.5
            },
            positionTo: {
                x: 650 + 70 - (30 * 2.5),
                y: 37.5
            },
            wait: 3700,
            alphaTimeout: 900,
            override: false
        });

        this.tweens.push({
            ref: this.boxes,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 3800,
            alphaTimeout: 1000,
            override: false
        });

        this.tweens.push({
            ref: this.boxes.rectBox6,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 0
            },
            positionTo: {
                x: 0,
                y: 3
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes.rectBox7,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 6
            },
            positionTo: {
                x: 0,
                y: 9
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes.rectBox8,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 12
            },
            positionTo: {
                x: 0,
                y: 15
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes.rectBox9,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 18
            },
            positionTo: {
                x: 0,
                y: 21
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes.rectBox10,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 24
            },
            positionTo: {
                x: 0,
                y: 27
            },
            wait: 1000,
            alphaTimeout: 2000
        });

        //green boxes
        this.tweens.push({
            ref: this.boxes2,
            alphaFrom: 0,
            alphaTo: 1,
            positionFrom: {
                x: 650 + 70 - (30 * 2.5),
                y: 37.5
            },
            positionTo: {
                x: 650 + 70 - (30 * 5.5),
                y: 37.5
            },
            wait: 4550,
            alphaTimeout: 1120,
            override: false
        });

        this.tweens.push({
            ref: this.boxes2.rectBox6,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 0
            },
            positionTo: {
                x: 0,
                y: 3
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes2.rectBox7,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 6
            },
            positionTo: {
                x: 0,
                y: 9
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes2.rectBox8,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 12
            },
            positionTo: {
                x: 0,
                y: 15
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes2.rectBox9,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 18
            },
            positionTo: {
                x: 0,
                y: 21
            },
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.boxes2.rectBox10,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 6.5,
                y: 24
            },
            positionTo: {
                x: 0,
                y: 27
            },
            wait: 1000,
            alphaTimeout: 2000
        });



        this.tweens.push({
            ref: this.Texts.text_1,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: 9,
                y: 33.5
            },
            positionTo: {
                x: -22,
                y: 33.5
            },
            wait: 3700,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.rectangles, this.commentTexts, this.boxes, this.Texts, this.boxes2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 10", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("34", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.rectangles = function() {
        this.initialize();
        //function () {
        var boxes = new createjs.Shape();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.5).drawRect(0, 0, 66, 63);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f('#DCFEFE').s('').ss('').drawRect(0, 0, 66, 9.5);
        this.roundRect2.setTransform(0, 0);

        this.hrRule1 = new cjs.Shape();
        this.hrRule1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(0, 9.5).lineTo(66, 9.5);
        this.hrRule1.setTransform(0, 0);

        this.hrRule2 = new cjs.Shape();
        this.hrRule2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(0, 45.3).lineTo(66, 45.3);
        this.hrRule2.setTransform(0, 0);

        this.hrRule3 = new cjs.Shape();
        this.hrRule3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(32.5, 0).lineTo(32.5, 63);
        this.hrRule3.setTransform(0, 0);

        this.questionText1 = new cjs.Text("tiotal", "bold 6px 'Myriad Pro'")
        this.questionText1.lineHeight = 19;
        this.questionText1.setTransform(7.5, 1);

        this.questionText2 = new cjs.Text("ental", "bold 6px 'Myriad Pro'")
        this.questionText2.lineHeight = 19;
        this.questionText2.setTransform(40, 1);

        boxes.setTransform(0, 0);
        this.addChild(boxes, this.roundRect2, this.roundRect1, this.hrRule1, this.hrRule2, this.hrRule3, this.questionText1, this.questionText2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    (lib.boxes = function(fillColor) {
        this.initialize();

        this.rectBox1 = new cjs.Shape();
        this.rectBox1.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox1.visible = false;
        this.rectBox1.setTransform(0, 0);

        this.rectBox2 = new cjs.Shape();
        this.rectBox2.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox2.visible = false;
        this.rectBox2.setTransform(0, 6);

        this.rectBox3 = new cjs.Shape();
        this.rectBox3.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox3.visible = false;
        this.rectBox3.setTransform(0, 12);

        this.rectBox4 = new cjs.Shape();
        this.rectBox4.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox4.visible = false;
        this.rectBox4.setTransform(0, 18);

        this.rectBox5 = new cjs.Shape();
        this.rectBox5.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox5.visible = false;
        this.rectBox5.setTransform(0, 24);

        this.rectBox6 = new cjs.Shape();
        this.rectBox6.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox6.visible = false;
        this.rectBox6.setTransform(6.5, 0);

        this.rectBox7 = new cjs.Shape();
        this.rectBox7.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox7.visible = false;
        this.rectBox7.setTransform(6.5, 6);

        this.rectBox8 = new cjs.Shape();
        this.rectBox8.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox8.visible = false;
        this.rectBox8.setTransform(6.5, 12);

        this.rectBox9 = new cjs.Shape();
        this.rectBox9.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox9.visible = false;
        this.rectBox9.setTransform(6.5, 18);

        this.rectBox10 = new cjs.Shape();
        this.rectBox10.graphics.f(fillColor).s("#000000").ss(0.3).drawRect(0, 0, 2.5, 3);
        this.rectBox10.visible = false;
        this.rectBox10.setTransform(6.5, 24);

        this.addChild(this.rectBox1, this.rectBox2, this.rectBox3, this.rectBox4, this.rectBox5, this.rectBox6, this.rectBox7, this.rectBox8, this.rectBox9, this.rectBox10, this.text_0,
            this.text_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.Texts = function() {
        this.initialize();

        this.text_0 = new cjs.Text("0", "16px 'Myriad Pro'");
        this.text_0.visible = false;
        this.text_0.lineHeight = 19;
        this.text_0.setTransform(-2, 32.5);

        this.text_1 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(-2, 32.5);

        this.addChild(this.text_0, this.text_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
