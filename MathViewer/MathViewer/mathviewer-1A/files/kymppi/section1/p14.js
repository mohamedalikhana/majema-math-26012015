(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1219,
	height: 678,
	fps: 20,
	color: "#FFFFFF",
	manifest: [
		{src:"images/s14_15_1.png", id:"s14_15_1"},
		{src:"images/s14_15_2.png", id:"s14_15_2"}
	]
};

// symbols:
(lib.s14_15_1 = function() {
	this.initialize(img.s14_15_1);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,756,367);


(lib.s14_15_2 = function() {
	this.initialize(img.s14_15_2);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,957,392);


(lib.s14_15_3 = function() {
	this.initialize(img.s14_15_3);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,186,393);


(lib.s14_15_36 = function() {
	this.initialize(img.s14_15_36);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,460,762);


(lib.s14_15_40 = function() {
	this.initialize(img.s14_15_40);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,11,267);


(lib.s14_15_54 = function() {
	this.initialize(img.s14_15_54);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,179,146);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.fontfix = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.text_2 = new cjs.Text("Rita 1 cirkel mer än antalet föremål.", "16px 'Myriad Pro'");
	this.text_2.lineHeight = 19;
	this.text_2.setTransform(0,6.1);

	this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A5C0");
	this.text.lineHeight = 20;
	this.text.setTransform(-20,5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2}]}).wait(1));

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.virtualBounds = new cjs.Rectangle(0,0,330.8,25.3);


(lib.Symbol21 = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("14", "13px 'Myriad Pro'", "#FFFFFF");
	this.text.lineHeight = 18;
	this.text.setTransform(37,6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
	this.shape.setTransform(31.1,18.4);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,1218.9,35.1);





(lib.Symbol10 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
	this.shape.setTransform(122.7,9.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
	this.shape_1.setTransform(94.9,9.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQApAAAcgcQAdgdAAgpQAAgogdgdQgcgcgpAAQgnAAgdAcQgdAdAAAoQAAApAdAdQAdAcAnAAg");
	this.shape_2.setTransform(94.8,38.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQAoAAAdgcQAdgdAAgpQAAgogdgdQgdgcgoAAQgoAAgcAcQgdAdAAAoQAAApAdAdQAcAcAoAAg");
	this.shape_3.setTransform(66,38.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
	this.shape_4.setTransform(66,9.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQAoAAAdgcQAdgdAAgpQAAgogdgdQgdgcgoAAQgoAAgcAcQgdAdAAAoQAAApAdAdQAcAcAoAAg");
	this.shape_5.setTransform(37.7,38.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
	this.shape_6.setTransform(37.7,9.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQApAAAcgcQAdgdAAgpQAAgogdgdQgcgcgpAAQgnAAgdAcQgdAdAAAoQAAApAdAdQAdAcAnAAg");
	this.shape_7.setTransform(9.9,38.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
	this.shape_8.setTransform(9.9,9.9);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(-1,-1,134.6,50.1);


(lib.Symbol9 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
	this.shape.setTransform(122.7,9.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAegdQAcgdAAgpQAAgngcgeQgegcgoAAQgoAAgdAcQgdAeAAAnQAAApAdAdQAdAdAoAAg");
	this.shape_1.setTransform(94.9,9.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
	this.shape_2.setTransform(66,9.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
	this.shape_3.setTransform(37.6,9.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQApAAAcgcQAdgdAAgpQAAgogdgdQgcgcgpAAQgnAAgdAcQgdAdAAAoQAAApAdAdQAdAcAnAAg");
	this.shape_4.setTransform(38.5,38.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQAoAAAdgcQAdgdAAgpQAAgogdgdQgdgcgoAAQgoAAgcAcQgdAdAAAoQAAApAdAdQAcAcAoAAg");
	this.shape_5.setTransform(9.9,38.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
	this.shape_6.setTransform(9.9,9.9);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(-1,-1,134.6,50.1);


(lib.Symbol8 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
	this.shape.setTransform(122.7,9.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
	this.shape_1.setTransform(94.9,9.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
	this.shape_2.setTransform(66,9.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgdgdgoAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
	this.shape_3.setTransform(67.9,37.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQAoAAAdgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgoAAgcAdQgdAdAAAnQAAApAdAcQAcAdAoAAg");
	this.shape_4.setTransform(37.7,37.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQAoAAAdgdQAdgdAAgpQAAgngdgeQgdgcgoAAQgoAAgcAcQgdAeAAAnQAAApAdAdQAcAdAoAAg");
	this.shape_5.setTransform(37.7,9.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABiQApAAAcgdQAdgcAAgpQAAgngdgdQgcgdgpAAQgnAAgdAdQgdAdAAAnQAAApAdAcQAdAdAnAAg");
	this.shape_6.setTransform(9.9,37.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#D74479").ss(1.5,0,0,4).p("AAABjQApAAAcgdQAdgdAAgpQAAgngdgeQgcgcgpAAQgnAAgdAcQgdAeAAAnQAAApAdAdQAdAdAnAAg");
	this.shape_7.setTransform(9.9,9.9);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(-1,-1,134.6,49.7);

(lib.Symbol2 = function() {
	this.initialize();

	

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape.setTransform(444.1,265.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_1.setTransform(415.7,265.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_2.setTransform(387.3,265.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_3.setTransform(359,265.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_4.setTransform(330.5,265.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_5.setTransform(458.2,251.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_6.setTransform(429.9,251.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_7.setTransform(444.1,236.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_8.setTransform(401.5,251.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_9.setTransform(415.7,236.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_10.setTransform(373.2,251.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_11.setTransform(387.3,236.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_12.setTransform(344.8,251.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_13.setTransform(359,236.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_14.setTransform(316.5,251.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_15.setTransform(330.5,236.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_16.setTransform(458.2,222.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_17.setTransform(429.9,222.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_18.setTransform(444.1,208.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_19.setTransform(401.5,222.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_20.setTransform(415.7,208.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_21.setTransform(373.2,222.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_22.setTransform(387.3,208.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_23.setTransform(344.8,222.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_24.setTransform(359,208.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_25.setTransform(316.5,222.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_26.setTransform(330.5,208.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_27.setTransform(181.5,265.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_28.setTransform(153.1,265.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_29.setTransform(124.7,265.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_30.setTransform(96.4,265.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_31.setTransform(67.9,265.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_32.setTransform(195.6,251.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_33.setTransform(167.3,251.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_34.setTransform(181.5,236.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_35.setTransform(138.9,251.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_36.setTransform(153.1,236.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_37.setTransform(110.6,251.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_38.setTransform(124.7,236.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_39.setTransform(82.2,251.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_40.setTransform(96.4,236.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_41.setTransform(53.9,251.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_42.setTransform(67.9,236.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_43.setTransform(195.6,222.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_44.setTransform(167.3,222.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_45.setTransform(181.5,208.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_46.setTransform(138.9,222.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_47.setTransform(153.1,208.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_48.setTransform(110.6,222.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_49.setTransform(124.7,208.6);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_50.setTransform(82.2,222.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_51.setTransform(96.4,208.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_52.setTransform(53.9,222.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_53.setTransform(67.9,208.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_54.setTransform(444.1,126.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_55.setTransform(415.7,126.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_56.setTransform(387.3,126.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_57.setTransform(359,126.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_58.setTransform(330.5,126.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_59.setTransform(458.2,112.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_60.setTransform(429.9,112.1);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_61.setTransform(444.1,97.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_62.setTransform(401.5,112.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_63.setTransform(415.7,97.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_64.setTransform(373.2,112.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_65.setTransform(387.3,97.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_66.setTransform(344.8,112.1);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_67.setTransform(359,97.9);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_68.setTransform(316.5,112.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_69.setTransform(330.5,97.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_70.setTransform(458.2,83.7);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_71.setTransform(429.9,83.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_72.setTransform(444.1,69.6);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_73.setTransform(401.5,83.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_74.setTransform(415.7,69.6);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_75.setTransform(373.2,83.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_76.setTransform(387.3,69.6);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_77.setTransform(344.8,83.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_78.setTransform(359,69.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_79.setTransform(316.5,83.7);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_80.setTransform(330.5,69.6);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_81.setTransform(181.5,126.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_82.setTransform(153.1,126.3);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_83.setTransform(124.7,126.3);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_84.setTransform(96.4,126.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_85.setTransform(67.9,126.3);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_86.setTransform(195.6,112.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_87.setTransform(167.3,112.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_88.setTransform(181.5,97.9);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_89.setTransform(138.9,112.1);

	this.instance = new lib.s14_15_2();
	this.instance.setTransform(13.4,5.4,0.5,0.5);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_90.setTransform(153.1,97.9);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_91.setTransform(110.6,112.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_92.setTransform(124.7,97.9);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_93.setTransform(82.2,112.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_94.setTransform(96.4,97.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_95.setTransform(53.9,112.1);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_96.setTransform(67.9,97.9);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_97.setTransform(195.6,83.7);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_98.setTransform(167.3,83.7);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_99.setTransform(181.5,69.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_100.setTransform(138.9,83.7);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_101.setTransform(153.1,69.6);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_102.setTransform(110.6,83.7);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_103.setTransform(124.7,69.6);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_104.setTransform(82.2,83.7);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_105.setTransform(96.4,69.6);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_106.setTransform(53.9,83.7);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_107.setTransform(67.9,69.6);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABIIAAQ6IACASQAEAVAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAAw6QAAgdgNgcQgdg5hHAAg");
	this.shape_108.setTransform(384.4,65.6);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgdg5QgIgRgEgWIgBgRIAAw6QAAhIA4gcQASgJAWgDQAKgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAdIAAQ6QAABHg5AcQgSAIgWAEIgRACg");
	this.shape_109.setTransform(384.4,65.6);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABIIAAQ6IACASQAEAVAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAAw6QAAgdgPgcQgcg5hHAAg");
	this.shape_110.setTransform(125.9,65.6);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgcg5QgJgRgEgWIgCgRIAAw6QAAhIA6gcQARgJAWgDQAKgCAHAAMAjxAAAQBHAAAcA5QAOAcAAAdIAAQ6QAABHg4AcQgdAOgcAAg");
	this.shape_111.setTransform(125.9,65.6);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABHIAAQ6IACASQAEAVAIASQAdA5BHAAMAjxAAAQAcAAAdgOQA4gdAAhHIAAw6QAAgcgNgcQgdg5hHAAg");
	this.shape_112.setTransform(384.4,205.8);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgdg5QgIgRgEgWIgBgRIAAw7QAAhHA4gcQASgJAWgDQAKgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAQ7QAABGg5AdQgcAOgdAAg");
	this.shape_113.setTransform(384.4,205.8);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVADgSAJQg5AcAABHIAAQ6IACASQAEAVAIASQAdA5BHAAMAjxAAAQAdAAAcgOQA5gdAAhHIAAw6QAAgcgPgcQgcg5hHAAg");
	this.shape_114.setTransform(125.9,205.8);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgcg5QgJgRgEgWIgCgRIAAw7QAAhHA6gcQARgJAWgDQAKgCAHAAMAjxAAAQBHAAAcA5QAOAcAAAcIAAQ7QAABGg4AdQgdAOgcAAg");
	this.shape_115.setTransform(125.9,205.8);

	this.addChild(this.shape_115,this.shape_114,this.shape_113,this.shape_112,this.shape_111,this.shape_110,this.shape_109,this.shape_108,this.shape_107,this.shape_106,this.shape_105,this.shape_104,this.shape_103,this.shape_102,this.shape_101,this.shape_100,this.shape_99,this.shape_98,this.shape_97,this.shape_96,this.shape_95,this.shape_94,this.shape_93,this.shape_92,this.shape_91,this.shape_90,this.instance,this.shape_89,this.shape_88,this.shape_87,this.shape_86,this.shape_85,this.shape_84,this.shape_83,this.shape_82,this.shape_81,this.shape_80,this.shape_79,this.shape_78,this.shape_77,this.shape_76,this.shape_75,this.shape_74,this.shape_73,this.shape_72,this.shape_71,this.shape_70,this.shape_69,this.shape_68,this.shape_67,this.shape_66,this.shape_65,this.shape_64,this.shape_63,this.shape_62,this.shape_61,this.shape_60,this.shape_59,this.shape_58,this.shape_57,this.shape_56,this.shape_55,this.shape_54,this.shape_53,this.shape_52,this.shape_51,this.shape_50,this.shape_49,this.shape_48,this.shape_47,this.shape_46,this.shape_45,this.shape_44,this.shape_43,this.shape_42,this.shape_41,this.shape_40,this.shape_39,this.shape_38,this.shape_37,this.shape_36,this.shape_35,this.shape_34,this.shape_33,this.shape_32,this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.ra1,this.ra2,this.ra3,this.ra4);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,550,220);


(lib.Symbol1 = function() {
	this.initialize();


	// Layer 1
	this.instance = new lib.s14_15_1();
	this.instance.setTransform(89.6,47.4,0.5,0.5);

	this.instance_1 = new lib.Symbol22();
	this.instance_1.setTransform(183.9,13.6,1,1,0,0,0,164.3,12.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6C7373").ss(1,0,0,4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
	this.shape.setTransform(98.7,117.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#6C7373").ss(1,0,0,4).p("AAABmQAqAAAegeQAegeAAgqQAAgpgegeQgegegqAAQgpAAgeAeQgeAeAAApQAAAqAeAeQAeAeApAAg");
	this.shape_1.setTransform(70,117.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_2.setTransform(446.1,299.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_3.setTransform(417.7,299.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_4.setTransform(389.3,299.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_5.setTransform(361,299.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_6.setTransform(332.5,299.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_7.setTransform(460.2,285);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_8.setTransform(431.9,285);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_9.setTransform(446.1,270.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_10.setTransform(403.5,285);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_11.setTransform(417.7,270.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_12.setTransform(375.2,285);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_13.setTransform(389.3,270.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_14.setTransform(346.8,285);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_15.setTransform(361,270.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_16.setTransform(318.5,285);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_17.setTransform(332.5,270.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_18.setTransform(460.2,256.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_19.setTransform(431.9,256.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_20.setTransform(446.1,242.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_21.setTransform(403.5,256.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_22.setTransform(417.7,242.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_23.setTransform(375.2,256.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_24.setTransform(389.3,242.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_25.setTransform(346.8,256.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_26.setTransform(361,242.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_27.setTransform(318.5,256.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_28.setTransform(332.5,242.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_29.setTransform(183.5,299.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_30.setTransform(155.1,299.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_31.setTransform(126.7,299.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_32.setTransform(98.4,299.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_33.setTransform(69.9,299.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_34.setTransform(197.6,285);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_35.setTransform(169.3,285);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_36.setTransform(183.5,270.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_37.setTransform(140.9,285);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_38.setTransform(155.1,270.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_39.setTransform(112.6,285);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_40.setTransform(126.7,270.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_41.setTransform(84.2,285);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_42.setTransform(98.4,270.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_43.setTransform(55.9,285);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_44.setTransform(69.9,270.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_45.setTransform(197.6,256.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_46.setTransform(169.3,256.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_47.setTransform(183.5,242.5);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_48.setTransform(140.9,256.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_49.setTransform(155.1,242.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_50.setTransform(112.6,256.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_51.setTransform(126.7,242.5);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_52.setTransform(84.2,256.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_53.setTransform(98.4,242.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_54.setTransform(55.9,256.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_55.setTransform(69.9,242.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_56.setTransform(446.1,159.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_57.setTransform(417.7,159.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_58.setTransform(389.3,159.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_59.setTransform(361,159.4);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_60.setTransform(332.5,159.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_61.setTransform(460.2,145.2);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_62.setTransform(431.9,145.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_63.setTransform(446.1,131.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_64.setTransform(403.5,145.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_65.setTransform(417.7,131.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_66.setTransform(375.2,145.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_67.setTransform(389.3,131.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_68.setTransform(346.8,145.2);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_69.setTransform(361,131.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_70.setTransform(318.5,145.2);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_71.setTransform(332.5,131.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_72.setTransform(460.2,116.9);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_73.setTransform(431.9,116.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_74.setTransform(446.1,102.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_75.setTransform(403.5,116.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_76.setTransform(417.7,102.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_77.setTransform(375.2,116.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_78.setTransform(389.3,102.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_79.setTransform(346.8,116.9);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_80.setTransform(361,102.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_81.setTransform(318.5,116.9);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_82.setTransform(332.5,102.7);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_83.setTransform(183.5,159.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_84.setTransform(155.1,159.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_85.setTransform(126.7,159.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_86.setTransform(98.4,159.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_87.setTransform(69.9,159.4);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_88.setTransform(197.6,145.2);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_89.setTransform(169.3,145.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_90.setTransform(183.5,131.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_91.setTransform(140.9,145.2);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_92.setTransform(155.1,131.1);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_93.setTransform(112.6,145.2);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_94.setTransform(126.7,131.1);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_95.setTransform(84.2,145.2);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_96.setTransform(98.4,131.1);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_97.setTransform(55.9,145.2);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_98.setTransform(69.9,131.1);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_99.setTransform(197.6,116.9);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_100.setTransform(169.3,116.9);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_101.setTransform(183.5,102.7);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_102.setTransform(140.9,116.9);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_103.setTransform(155.1,102.7);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_104.setTransform(112.6,116.9);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_105.setTransform(126.7,102.7);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_106.setTransform(84.2,116.9);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#959C9D").ss(0.4).p("AiMAAIEZAA");
	this.shape_107.setTransform(98.4,102.7);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#959C9D").ss(0.4).p("AAACLIAAkV");
	this.shape_108.setTransform(55.9,116.9);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#959C9D").ss(0.4).p("AiNAAIEbAA");
	this.shape_109.setTransform(69.9,102.7);

	// this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A5C0");
	// this.text.lineHeight = 20;
	// this.text.setTransform(0,5);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgdAAgcAOQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAAw6QAAgdgNgcQgdg5hHAAg");
	this.shape_110.setTransform(386.4,98.1);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgdg5QgIgRgEgWIgBgRIAAw7QAAhGA4gdQAcgOAdAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAQ7QAABGg5AdQgSAIgWAEIgRACg");
	this.shape_111.setTransform(386.4,98.1);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgdAAgcAOQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA5gcAAhHIAAw6QAAgdgPgcQgcg5hHAAg");
	this.shape_112.setTransform(127.9,98.1);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgcg5QgJgRgEgWIgCgRIAAw7QAAhGA6gdQAcgOAcAAMAjxAAAQBHAAAcA5QAOAcAAAcIAAQ7QAABGg4AdQgSAIgWAEIgRACg");
	this.shape_113.setTransform(127.9,98.1);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgdAAgcAOQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAIARgCQAWgDASgJQA4gcAAhHIAAw6QAAgdgNgcQgdg5hHAAg");
	this.shape_114.setTransform(386.4,238.3);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgdg4QgIgTgEgVIgBgSIAAw6QAAhGA4gdQAcgOAdAAMAjxAAAQBHAAAcA5QAPAcAAAcIAAQ6QAABIg5AcQgSAJgWADIgRACg");
	this.shape_115.setTransform(386.4,238.3);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("Ax4qOQgHAAgLACQgVAEgSAIQg5AdAABHIAAQ6IACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAAw6QAAgdgPgcQgcg5hHAAg");
	this.shape_116.setTransform(127.9,238.3);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFFFFF").s().p("Ax4KPQhHAAgcg4QgJgTgEgVIgCgSIAAw6QAAhGA6gdQARgIAWgEQAKgCAHAAMAjxAAAQBHAAAcA5QAOAcAAAcIAAQ6QAABIg4AcQgdAOgcAAg");
	this.shape_117.setTransform(127.9,238.3);

	this.addChild(this.shape_117,this.shape_116,this.shape_115,this.shape_114,this.shape_113,this.shape_112,this.shape_111,this.shape_110,this.text,this.shape_109,this.shape_108,this.shape_107,this.shape_106,this.shape_105,this.shape_104,this.shape_103,this.shape_102,this.shape_101,this.shape_100,this.shape_99,this.shape_98,this.shape_97,this.shape_96,this.shape_95,this.shape_94,this.shape_93,this.shape_92,this.shape_91,this.shape_90,this.shape_89,this.shape_88,this.shape_87,this.shape_86,this.shape_85,this.shape_84,this.shape_83,this.shape_82,this.shape_81,this.shape_80,this.shape_79,this.shape_78,this.shape_77,this.shape_76,this.shape_75,this.shape_74,this.shape_73,this.shape_72,this.shape_71,this.shape_70,this.shape_69,this.shape_68,this.shape_67,this.shape_66,this.shape_65,this.shape_64,this.shape_63,this.shape_62,this.shape_61,this.shape_60,this.shape_59,this.shape_58,this.shape_57,this.shape_56,this.shape_55,this.shape_54,this.shape_53,this.shape_52,this.shape_51,this.shape_50,this.shape_49,this.shape_48,this.shape_47,this.shape_46,this.shape_45,this.shape_44,this.shape_43,this.shape_42,this.shape_41,this.shape_40,this.shape_39,this.shape_38,this.shape_37,this.shape_36,this.shape_35,this.shape_34,this.shape_33,this.shape_32,this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_1,this.instance,this.ra1,this.ra2,this.ra3);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,304.8);


// stage content:
(lib.p14 = function() {
	this.initialize();

	// Layer 1
	this.other = new lib.Symbol21();
	this.other.setTransform(609.5,660.2,1,1,0,0,0,609.5,17.5);

	this.v3 = new lib.Symbol2();
	this.v3.setTransform(294.6,499.6,1,1,0,0,0,255.1,135.7);

	this.v2 = new lib.Symbol1();
	this.v2.setTransform(293.8,202.7,1,1,0,0,0,256.3,152);

	this.addChild(this.v2,this.v3,this.other);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(609.5,389.7,1218.9,627.1);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;