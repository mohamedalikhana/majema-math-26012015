(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };



    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("39", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(60, 42);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(52, 23.5);

        this.text_2 = new cjs.Text("Jämföra talen 0 till 50", "24px 'MyriadPro-Semibold'", "#00A3C4");
        this.text_2.setTransform(105, 42);


        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 0 till 50", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(395, 660);

        this.text = new cjs.Text("113", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(562, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(588, 660.8);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();
        // Block-1

        var ToBeAdded = [];
        var xpos = 35.9
        var xpos1 = 33.5
        for (var col = 0; col < 26; col++) {
            if (col > 12) {
                xpos = 97.9;
                xpos1 = 95.5
            }
            var shape_1 = new cjs.Shape();
            shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
            shape_1.setTransform(xpos + (col * 15), 16.4);

            var shape_2 = new cjs.Shape();
            shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
            shape_2.setTransform(xpos1 + (col * 15), 21.9);

            var shape_3 = new cjs.Shape();
            shape_3.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
            shape_3.setTransform(xpos1 + (col * 15), 21.9);
            ToBeAdded.push(shape_1, shape_2, shape_3)

        };


        this.roundRect2 = new cjs.Shape(); // white block
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(20, 15, 215, 135, 5);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape(); // white block
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(275, 15, 215, 135, 5);
        this.roundRect3.setTransform(0, 0);

        // Main yellow block
        this.roundRect1 = new cjs.Shape(); // white block
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 0, 515, 182, 10);
        this.roundRect1.setTransform(0, 0);

        var xpos = 0;
        for (var col = 0; col < 10; col++) {
            var Line_2 = new cjs.Shape();
            if (col > 7) {
                xpos = 235
            } else if (col > 4) {
                xpos = 190
            } else if (col > 1) {
                xpos = 65
            } else {
                xpos = 0
            }
            Line_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(45, 35).lineTo(45, 123).lineTo(54, 123).lineTo(54, 35).lineTo(45, 35)
                .moveTo(45, 43.6).lineTo(54, 43.6).moveTo(45, 52.1).lineTo(54, 52.1).moveTo(45, 60.6).lineTo(54, 60.6).moveTo(45, 69.1).lineTo(54, 69.1)
                .moveTo(45, 77.6).lineTo(54, 77.6).moveTo(45, 86.1).lineTo(54, 86.1).moveTo(45, 95.6).lineTo(54, 95.6).moveTo(45, 105.1).lineTo(54, 105.1)
                .moveTo(45, 114.7).lineTo(54, 114.7);
            Line_2.setTransform(xpos + (col * 15), 0);
            ToBeAdded.push(Line_2)
        }

        var xpos = 0;
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 7; col++) {
                var Line_3 = new cjs.Shape();
                if (col > 4) {
                    xpos = 284
                } else if (col > 3) {
                    xpos = 223
                } else if (col > 1) {
                    xpos = 80
                } else {
                    xpos = 0
                }

                if (col < 5 && row < 3) {
                    continue
                }
                if (col == 1 && row == 3) {
                    continue
                }
                if ((col == 2 || col == 3) && row < 4) {
                    continue
                }
                if (col == 4 && row == 3) {
                    continue
                }
                if (col == 6 && row == 0) {
                    continue
                }
                Line_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(79, 66.1).lineTo(79, 75)
                    .lineTo(87, 75).lineTo(87, 66.1).lineTo(79, 66.1)
                Line_3.setTransform(xpos + (col * 14), 0 + (row * 12));
                ToBeAdded.push(Line_3)
            }
        }




        var tmptxt = ""
        var xpos = 63
        var ypos = 170
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 2; col++) {
                if (col == 0 && row == 0) {
                    tmptxt = "23 är mindre än 32."
                } else if (col == 1 && row == 0) {
                    tmptxt = "31 är större än 29."
                } else if (col == 0 && row == 1) {
                    tmptxt = "23          <          32";
                    xpos = 70;
                    ypos = 132
                } else if (col == 1 && row == 1) {
                    tmptxt = "31          >          29";
                    xpos = 65;
                    ypos = 132
                } else {
                    xpos = 76;
                    ypos = 170
                }

                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText);
            }
        }
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.hrLine, this.hrLine1, this.vrLine1, this.Line_3)
        this.addChild(this.hrLine2, this.hrLine3, this.vrLine2)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 530.3, 123.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Jämför. Skriv > eller <.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(20, 0);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 515, 365, 10);
        this.roundRect1.setTransform(0, 10);

        var ToBeAdded = [];

        for (var row = 0; row < 2; row++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(7, 131 + (row * 122)).lineTo(505, 131 + (row * 122));
            ToBeAdded.push(hrLine)
        };

        var moveToypos = 13;
        var lineToypos = 123;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 1; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 135;
                    lineToypos = 248;
                } else if (row == 2) {
                    moveToypos = 257;
                    lineToypos = 370;
                } else {
                    moveToypos = 13;
                    lineToypos = 123;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(259 + (col * 170), moveToypos).lineTo(259 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }
        var yPos = 0
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 16; col++) {
                var xPos = 10;

                if (col > 8 && row == 0) {
                    xPos = 265;
                } else if (col > 4 && row == 0) {
                    xPos = 190;
                } else if (col > 2 && row == 0) {
                    xPos = 105
                } else if (col > 11 && row == 1) {
                    xPos = 220
                } else if (col > 8 && row == 1) {
                    xPos = 130
                } else if (col > 4 && row == 1) {
                    xPos = 60
                } else if (col > 12 && row == 2) {
                    xPos = 205
                } else if (col > 8 && row == 2) {
                    xPos = 125
                } else if (col > 4 && row == 2) {
                    xPos = 60
                }
                if (row == 0 && (col == 0 || col == 13 || col == 14 || col == 15)) {
                    continue;
                }
                if (row == 1) {
                    yPos = 8
                } else if (row == 2) {
                    yPos = 18
                } else {
                    yPos = 0
                }
                var greenSquareLine_2 = new cjs.Shape();
                greenSquareLine_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(25, 17).lineTo(25, 105).lineTo(34, 105).lineTo(34, 17).lineTo(25, 17)
                    .moveTo(25, 25.6).lineTo(34, 25.6).moveTo(25, 34.1).lineTo(34, 34.1).moveTo(25, 42.6).lineTo(34, 42.6).moveTo(25, 51.1).lineTo(34, 51.1)
                    .moveTo(25, 59.6).lineTo(34, 59.6).moveTo(25, 68.1).lineTo(34, 68.1).moveTo(25, 77.6).lineTo(34, 77.6).moveTo(25, 87.1).lineTo(34, 87.1)
                    .moveTo(25, 96.7).lineTo(34, 96.7);
                greenSquareLine_2.setTransform(xPos + (col * 15), yPos + (row * 112));
                ToBeAdded.push(greenSquareLine_2)
            }
        }
        //box 1 yellow boxes
        var xpos = 75
        var ypos = 0;
        for (var numOfrow = 0; numOfrow < 3; numOfrow++) {
            for (var row = 0; row < 5; row++) {
                for (var col = 0; col < 6; col++) {
                    var yellowSquareLine_3 = new cjs.Shape();
                    if (numOfrow == 1) {
                        ypos = 120
                    } else if (numOfrow == 2) {
                        ypos = 242
                    } else {
                        ypos = 0
                    }
                    if (numOfrow == 0 && col > 3) {
                        xpos = 289
                    } else if (numOfrow == 0 && col > 1) {
                        xpos = 172
                    } else if (numOfrow == 1) {
                        xpos = 273
                    } else if (numOfrow == 2 && col > 3) {
                        xpos = 408
                    } else if (numOfrow == 2 && col > 1) {
                        xpos = 311
                    } else if (numOfrow == 2 && col >= 0) {
                        xpos = 210
                    } else {
                        xpos = 75
                    }

                    if (numOfrow == 1 && col < 4) {
                        continue;
                    } else if (numOfrow == 1 && col == 5 && row == 0) {
                        continue
                    } else if (numOfrow == 2 && col == 1 && row == 0) {
                        continue
                    } else if (numOfrow == 2 && col == 2 && row < 2) {
                        continue
                    } else if (numOfrow == 2 && col == 3 && row < 3) {
                        continue
                    } else if (numOfrow == 2 && (col == 4 || col == 5) && row < 2) {
                        continue
                    } else if (numOfrow == 0 && col == 0 && row < 3) {
                        continue
                    } else if (numOfrow == 0 && col == 1 && row < 4) {
                        continue
                    } else if (numOfrow == 0 && col == 2 && row < 2) {
                        continue
                    } else if (numOfrow == 0 && col == 3 && row < 3) {
                        continue
                    } else if (numOfrow == 0 && (col == 4 || col == 5) && row < 4) {
                        continue
                    }

                    yellowSquareLine_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(6, 48.1).lineTo(6, 57).lineTo(14, 57).lineTo(14, 48.1).lineTo(6, 48.1)
                    yellowSquareLine_3.setTransform(xpos + (col * 14), ypos + (row * 12));
                    ToBeAdded.push(yellowSquareLine_3)
                }
            }
        }

        //Rectangle box and text
        var xpos = 120;
        var ypos = 100;
        var tmptxt = ""
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 260), ypos + (row * 122), 21, 23);
                ToBeAdded.push(tmp_Rect);
            }
        }
        //draw harizantial  lines
        var moveToxpos = 78
        var lineToxpos = 115
        var ypos = 123
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 4; col++) {
                var hrLine1 = new cjs.Shape();
                if (col > 1) {
                    moveToxpos = 202;
                    lineToxpos = 239
                } else {
                    moveToxpos = 78;
                    lineToxpos = 115
                }
                hrLine1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(moveToxpos + (col * 68), ypos + (row * 122))
                    .lineTo(lineToxpos + (col * 68), ypos + (row * 122));
                ToBeAdded.push(hrLine1)
            }
        }

        this.addChild(this.text_q1, this.text, this.roundRect1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 340);

    // stage content: 
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(304, 272, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol6();
        this.v2.setTransform(304, 480, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
