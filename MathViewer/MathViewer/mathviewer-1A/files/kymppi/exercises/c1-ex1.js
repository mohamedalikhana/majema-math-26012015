(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c1_ex1_1.png",
            id: "c1_ex1_1"
        }, {
            src: "images/c1_ex1_2.png",
            id: "c1_ex1_2"
        }, {
            src: "images/c1_ex1_3.png",
            id: "c1_ex1_3"
        }, {
            src: "images/c1_ex1_4.png",
            id: "c1_ex1_4"
        }, {
            src: "images/c1_ex1_5.png",
            id: "c1_ex1_5"
        }, {
            src: "images/c1_ex1_6.png",
            id: "c1_ex1_6"
        }, {
            src: "images/c1_ex1_7.png",
            id: "c1_ex1_7"
        }, {
            src: "images/c1_ex1_8.png",
            id: "c1_ex1_8"
        }, {
            src: "images/c1_ex1_9.png",
            id: "c1_ex1_9"
        }, {
            src: "images/c1_ex1_10.png",
            id: "c1_ex1_10"
        }]
    };


    (lib.c1_ex1_1 = function() {
        this.initialize(img.c1_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_2 = function() {
        this.initialize(img.c1_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_3 = function() {
        this.initialize(img.c1_ex1_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_4 = function() {
        this.initialize(img.c1_ex1_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_5 = function() {
        this.initialize(img.c1_ex1_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_6 = function() {
        this.initialize(img.c1_ex1_6);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_7 = function() {
        this.initialize(img.c1_ex1_7);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_8 = function() {
        this.initialize(img.c1_ex1_8);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_9 = function() {
        this.initialize(img.c1_ex1_9);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.c1_ex1_10 = function() {
        this.initialize(img.c1_ex1_10);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();

        var ToBeAdded = [];
        for (var col = 0; col < 2; col++) {
            var bigFrame = new cjs.Shape();
            bigFrame.graphics.f('#ffffff').s('#7d7d7d').ss(2).drawRoundRect(230, -60, 400, 370, 20);
            bigFrame.setTransform(0 + (col * 450), 0);
            ToBeAdded.push(bigFrame)
        }

        this.smallFrames = [];
        for (var col = 0; col < 10; col++) {
            var smallFrame = new cjs.Shape();
            smallFrame.graphics.f('#ffffff').s('#7d7d7d').ss(2).drawRoundRect(-80, 350, 120, 150, 7);
            smallFrame.setTransform(0 + (col * 150), 0);

            var xPos = (col == 9) ? smallFrame.x - 87 : smallFrame.x - 53;
            var yPos = smallFrame.y + 373;
            var num = new cjs.Text("" + (col + 1), "24px 'Myriad Pro'");
            num.lineHeight = 19;
            num.setTransform(xPos, yPos, 5, 5);

            var numFrame = new cjs.Container();
            numFrame.addChild(smallFrame, num)

            this.smallFrames.push(numFrame);
        }

        this.img_1 = new lib.c1_ex1_1();
        this.img_1.visible = false;
        this.img_1.setTransform(340, 40);

        this.img_2 = new lib.c1_ex1_2();
        this.img_2.visible = false;
        this.img_2.setTransform(280, 55);

        this.img_3 = new lib.c1_ex1_3();
        this.img_3.visible = false;
        this.img_3.setTransform(280, 8);

        this.img_4 = new lib.c1_ex1_4();
        this.img_4.visible = false;
        this.img_4.setTransform(275, 14);

        this.img_5 = new lib.c1_ex1_5();
        this.img_5.visible = false;
        this.img_5.setTransform(275, 10);

        this.img_6 = new lib.c1_ex1_6();
        this.img_6.visible = false;
        this.img_6.setTransform(245, 60);

        this.img_7 = new lib.c1_ex1_7();
        this.img_7.visible = false;
        this.img_7.setTransform(245, 30);

        this.img_8 = new lib.c1_ex1_8();
        this.img_8.visible = false;
        this.img_8.setTransform(240, 50);

        this.img_9 = new lib.c1_ex1_9();
        this.img_9.visible = false;
        this.img_9.setTransform(241, 36);

        this.img_10 = new lib.c1_ex1_10();
        this.img_10.visible = false;
        this.img_10.setTransform(240, 16);

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }

        this.addChild(this.img_1, this.img_2, this.img_3, this.img_4, this.img_5, this.img_6, this.img_7, this.img_8, this.img_9, this.img_10);

        for (var i = 0; i < this.smallFrames.length; i++) {
            this.addChild(this.smallFrames[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_1.visible = true;
        this.addChild(this.Stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_1.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[0],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[0].x,
                y: smallFrames[0].y
            },
            positionTo: {
                x: smallFrames[0].x + 900,
                y: smallFrames[0].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[0].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[0].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_2.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[1],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[1].x,
                y: smallFrames[1].y
            },
            positionTo: {
                x: smallFrames[1].x + 750,
                y: smallFrames[1].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[1].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[1].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_3.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[2],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[2].x,
                y: smallFrames[2].y
            },
            positionTo: {
                x: smallFrames[2].x + 600,
                y: smallFrames[2].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[2].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[2].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_4.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[3],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[3].x,
                y: smallFrames[3].y
            },
            positionTo: {
                x: smallFrames[3].x + 450,
                y: smallFrames[3].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[3].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[3].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage6 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_5.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[4],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[4].x,
                y: smallFrames[4].y
            },
            positionTo: {
                x: smallFrames[4].x + 300,
                y: smallFrames[4].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[4].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[4].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage7 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_6.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[5],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[5].x,
                y: smallFrames[5].y
            },
            positionTo: {
                x: smallFrames[5].x + 150,
                y: smallFrames[5].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[5].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[5].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage8 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_7.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[6],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[6].x,
                y: smallFrames[6].y
            },
            positionTo: {
                x: smallFrames[6].x,
                y: smallFrames[6].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[6].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[6].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage9 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_8.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[7],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[7].x,
                y: smallFrames[7].y
            },
            positionTo: {
                x: smallFrames[7].x - 150,
                y: smallFrames[7].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[7].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[7].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage10 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_9.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[8],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[8].x,
                y: smallFrames[8].y
            },
            positionTo: {
                x: smallFrames[8].x - 300,
                y: smallFrames[8].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[8].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[8].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage11 = function() {
        this.initialize();

        this.Stage0 = new lib.Stage0();
        this.Stage0.img_10.visible = true;
        var smallFrames = this.Stage0.smallFrames;
        this.addChild(this.Stage0);

        this.tweens = [];

        this.tweens.push({
            ref: smallFrames[9],
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: smallFrames[9].x,
                y: smallFrames[9].y
            },
            positionTo: {
                x: smallFrames[9].x - 450,
                y: smallFrames[9].y - 303
            },
            wait: 500,
            alphaTimeout: 4500
        });

        this.tweens.push({
            ref: smallFrames[9].children[1],
            alphaFrom: 1,
            alphaTo: 1,
            startColor: smallFrames[9].children[1],
            endColor: '#7233ff',
            wait: 6000,
            alphaTimeout: 0
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Räkna till 10", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("1", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage5 = new lib.Stage5();
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage6 = new lib.Stage6();
        this.stage6.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage7 = new lib.Stage7();
        this.stage7.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage8 = new lib.Stage8();
        this.stage8.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage9 = new lib.Stage9();
        this.stage9.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage10 = new lib.Stage10();
        this.stage10.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage11 = new lib.Stage11();
        this.stage11.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7, this.stage8, this.stage9, this.stage10, this.stage11);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
