var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }, {
            src: "../exercises/images/c3_ex9_1.png",
            id: "sprites"
        }]
    };

    lib.init_Magic_Wand();

    var iconProperties = {
        x: 850 + 200,
        y: 350 - 90,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 150,
        wrongY: 345 - 15
    };

    var objects = [];

    // (lib.tick = function() {
    //     this.initialize(img.tick);
    // }).prototype = p = new cjs.Bitmap();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.tick = function() {
        this.initialize();
        var tickMark = new createjs.Shape();
        tickMark.graphics.f('#61df7f').ss(4).moveTo(77, 5).quadraticCurveTo(82, 10, 84, 10).quadraticCurveTo(78, 5, 25.79, 147.32).quadraticCurveTo(12, 152, 10.5, 152.84).quadraticCurveTo(12, 152, 77, 5).moveTo(15.26, 66.03).drawCircle(15.26, 66.03, 10).moveTo(72.89, 85).drawCircle(72.89, 85, 10);
        tickMark.setTransform(0, 0, 2, 2);
        tickMark.alpha = 1;
        this.addChild(tickMark);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function generateImages(images, imageCount, X, Y, scaleX, scaleY, image) {
        X = 280, Y = 100;
        if (imageCount < 5) {
            //X=X+(100*((5-imageCount-1)));
            switch (imageCount) {
                case 1:
                    X = X + 200;
                    break;
                case 2:
                    X = X + 150;
                    break;
                case 3:
                    X = X + 100;
                    break;
                case 4:
                    X = X + 50;
                    break;
            }

        }
        for (var i = 0; i < imageCount; i++) {
            var iteration = i;
            if (5 <= i) {
                iteration = i - 5;
                Y = 200;
            }
            var tempImage = image.clone(true);
            tempImage.setTransform(X + (100 * iteration), Y, 0.5, 0.5);
            images.addChild(tempImage);
        }
        return images;
    }

    (lib.CommentText = function() {
        this.initialize();
        this.hintText1 = new cjs.Text("Svara tillsammans.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 420);
        this.hintText2 = new cjs.Text("Vi räknar de blå bollarna.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText2.textAlign = 'center';
        this.hintText2.lineHeight = 19;
        this.hintText2.visible = false;
        this.hintText2.setTransform(550, 400);
        this.hintText3 = new cjs.Text("Vi säger tillsammans: 3 är lika med 3.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText3.textAlign = 'center';
        this.hintText3.lineHeight = 19;
        this.hintText3.visible = false;
        this.hintText3.setTransform(550, 400);
        this.hintText4 = new cjs.Text("Hur kan vi se att det är lika många?", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText4.textAlign = 'center';
        this.hintText4.lineHeight = 19;
        this.hintText4.visible = false;
        this.hintText4.setTransform(550, 400);
        this.addChild(this.hintText1, this.hintText2, this.hintText3, this.hintText4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;

        var data = {
            images: [img.sprites],
            frames: {
                width: 170,
                height: 170
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        for (var i = 0; i < 6; i++) {
            var tempObject = new createjs.Sprite(spriteSheet);
            tempObject.gotoAndStop(i);
            //            this.addChild(tempObject);
            objects.push(tempObject);
        }

        this.questionText = new cjs.Text("Vilket tal? Läs högt.", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -50);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        //this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        var startX = 375;
        var startY = 125;

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;

        thisStage.currentAnswer = 0;

        this.dices = new cjs.Container();
        for (var i = 0; i < 10; i++) {
            var numVal = new cjs.Text("" + i, "50px 'Myriad Pro'")
            numVal.lineHeight = 19;
            numVal.textAlign = 'center';
            numVal.setTransform(663, 50, 3, 3);
            this.dices.addChild(numVal);
        }

        this.numTxt = new cjs.Text(" ", "150px 'Myriad Pro'")
        this.numTxt.lineHeight = 19;
        this.numTxt.textAlign = 'center';
        this.numTxt.visible = false;
        this.numTxt.setTransform(663, 235);

        this.textbox_1 = new cjs.Shape();
        this.textbox_1.graphics.f('#ffffff').s('#7d7d7d').ss(1.5).drawRoundRect(0, 0, 150, 150, 6);
        this.textbox_1.setTransform(587, 40);

        this.addChild(this.textbox_1, this.dices, this.commentTexts, this.numTxt);
        var myStage = this;

        exerciseCount = 0;
        var ranNums = [];
        objects = shuffle(objects);

        this.onNewExercise = function(e) {
            if (exerciseCount < 10) {
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                myStage.numTxt.visible = false;

                for (var i = 0; i < myStage.dices.children.length; i++) {
                    myStage.dices.children[i].visible = false;
                }
                if (exerciseCount == 0 || ranNums.length == 0) {
                    var nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                        i = nums.length,
                        j = 0;

                    while (i--) {
                        j = Math.floor(Math.random() * (i + 1));
                        ranNums.push(nums[j]);
                        nums.splice(j, 1);
                    }
                }
                myStage.dices.children[ranNums[ranNums.length - 1]].visible = true;
                myStage.answer = ranNums[ranNums.length - 1];
                ranNums.pop();

                new lib.Magic_Wand(myStage);

                if (exerciseCount > 0) {
                    $(btn_newExercise).hide();
                    $(btn_newExercise).addClass('disabled');
                    $(btn_answers).hide();
                    $(btn_answers).addClass('disabled');
                }

                exerciseCount = exerciseCount + 1;
            }
            if (typeof btn_newExercise != 'undefined') {
                $(btn_newExercise).hide();
            }
        };
        var showIncorrectAnswerTween = null;

        this.onCheckAnswer = function() {
            if (showIncorrectAnswerTween) {
                clearTimeout(showIncorrectAnswerTween);
            }
            var ans = myStage.answer;
            var text = '';
            if (ans == 0) {
                text = 'Noll';
            } else if (ans == 1) {
                text = 'Ett';
            } else if (ans == 2) {
                text = 'Två';
            } else if (ans == 3) {
                text = 'Tre';
            } else if (ans == 4) {
                text = 'Fyra';
            } else if (ans == 5) {
                text = 'Fem';
            } else if (ans == 6) {
                text = 'Sex';
            } else if (ans == 7) {
                text = 'Sju';
            } else if (ans == 8) {
                text = 'Åtta';
            } else if (ans == 9) {
                text = 'Nio';
            }

            myStage.numTxt.text = text;
            myStage.numTxt.visible = true;

            currentAnswers[1] = -1;

            if (exerciseCount < 10) {
                showIncorrectAnswerTween = setTimeout(function() {
                    $(btn_answers).hide();
                    $(btn_answers).addClass('disabled');
                    $(btn_newExercise).show();
                    $(btn_newExercise).removeClass('disabled');
                }, 2000);
            } else if (exerciseCount == 10) {
                $(btn_newExercise).hide();
                $(btn_newExercise).addClass('disabled');
                $(btn_answers).hide();
                $(btn_answers).addClass('disabled');
            }

        };

        this.onNewExercise();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Räkna till 10", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);

        this.text_1 = new cjs.Text("1", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.balls = function() {
        this.initialize();
        //function () {



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)



        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
