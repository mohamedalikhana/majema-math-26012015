var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/tick.png",
            id: "tick"
        }, {
            src: "../exercises/images/error.png",
            id: "error"
        }, {
            src: "images/c38_ex1_1.png",
            id: "wallet"
        }, {
            src: "../viovar/images/bat.png",
            id: "bat"
        }, {
            src: "../exercises/images/c41_ex1_s1_1.png",
            id: "kr_5"
        }, {
            src: "../exercises/images/c41_ex1_s1_2.png",
            id: "kr_2"
        }, {
            src: "../exercises/images/c41_ex1_s1_3.png",
            id: "kr_1"
        }]
    };

    lib.init_Magic_Wand();

    var iconProperties = {
        x: 850 + 200,
        y: 120,
        scaleX: 0.5,
        scaleY: 0.5,
        wrongX: 750 + 150,
        wrongY: 345 - 15
    };

    var objects = [];



    (lib.wallet = function() {
        this.initialize(img.wallet);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.bat = function() {
        this.initialize(img.bat);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.tick = function() {
        this.initialize(img.tick);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);
    (lib.error = function() {
        this.initialize(img.error);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 50, 50);

    (lib.kr_5 = function() {
        this.initialize(img.kr_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.kr_2 = function() {
        this.initialize(img.kr_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    (lib.kr_1 = function() {
        this.initialize(img.kr_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function generateImages(images, imageCount, X, Y, scaleX, scaleY, image) {
        X = 280, Y = 100;
        if (imageCount < 5) {
            //X=X+(100*((5-imageCount-1)));
            switch (imageCount) {
                case 1:
                    X = X + 200;
                    break;
                case 2:
                    X = X + 150;
                    break;
                case 3:
                    X = X + 100;
                    break;
                case 4:
                    X = X + 50;
                    break;
            }

        }
        for (var i = 0; i < imageCount; i++) {
            var iteration = i;
            if (5 <= i) {
                iteration = i - 5;
                Y = 200;
            }
            var tempImage = image.clone(true);
            tempImage.setTransform(X + (100 * iteration), Y, 0.5, 0.5);
            images.addChild(tempImage);
        }
        return images;
    }
    (lib.dice = function(count, properties, number) {
        this.initialize();
        //console.log(ballWidth);
        var ballWidth = properties.width;
        thisStage = this;
        this.square = new cjs.Shape();
        this.square.graphics.f("#ffffff").ss(0.5).s('black').drawRoundRect(0, 0, properties.width, properties.height, 6);
        ballWidth = (properties.width + properties.height) / 1.35;
        this.addChild(this.square);
        switch (count) {
            case 1:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 2:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 2 / 3, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;

            case 3:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 4:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 5:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 6:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);

                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case 9:
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width / 2, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height / 2, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                var dot = new cjs.Shape();
                dot.graphics.f('#000000').arc(properties.width * 3 / 4, properties.height * 3 / 4, ballWidth / 12, 0, 2 * Math.PI, 1);
                this.addChild(dot);
                break;
            case -1:
                this.number = new cjs.Text('' + number + ' kr', "55px 'Myriad Pro'", '#000000');
                this.number.textAlign = 'center';
                this.number.setTransform(57, 10)
                this.addChild(this.number);
                break;
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();
        this.hintText1 = new cjs.Text("Klicka på rätt antal kronor.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(660, 530);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
    (lib.Stage1 = function() {
        this.initialize();
        thisStage = this;

        // var data = {
        //     images: [img.sprites],
        //     frames: {
        //         width: 170,
        //         height: 170
        //     },
        //     animations: {
        //         trash: 0,
        //         male: 1,
        //         wait: 2,
        //         library: 3,
        //         female: 4,
        //         hanger: 5,
        //         stairs: 6,
        //         noparking: 7
        //     }
        // }

        // var spriteSheet = new createjs.SpriteSheet(data);
        // for (var i = 0; i < 6; i++) {
        //     var tempObject = new createjs.Sprite(spriteSheet);
        //     tempObject.gotoAndStop(i);
        //     //            this.addChild(tempObject);
        //     objects.push(tempObject);
        // }

        this.questionText = new cjs.Text("Hur många kronor?", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(663, -100);

        this.incorrectAnswer = new cjs.Text("Pröva igen!", "36px 'Myriad Pro'", '#FF0000')
        this.incorrectAnswer.lineHeight = 1;
        this.incorrectAnswer.textAlign = 'center';
        this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        //this.incorrectAnswer.setTransform(iconProperties.wrongX, iconProperties.wrongY);
        this.incorrectAnswer.visible = false;

        this.correctIcon = new lib.tick();
        this.correctIcon.visible = false;
        this.correctIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        this.incorrectIcon = new lib.error();
        this.incorrectIcon.visible = false;
        this.incorrectIcon.setTransform(iconProperties.x, iconProperties.y, iconProperties.scaleX, iconProperties.scaleY);
        thisStage.currentAnswer = undefined;

        this.addChild(this.questionText, this.incorrectAnswer, this.incorrectIcon, this.correctIcon);

        var startX = 375;
        var startY = 125;

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        var circles = [];
        var circleCount = 6;
        var startX = 45;
        thisStage.currentAnswer = 0;


        var boxesX = 30,
            boxesY = 410;
        var myStage = this;

        var boxes = [];
        var boxeCount = 7;
        for (var i = 0; i < boxeCount; i++) {
            var box = new lib.dice(-1, {
                width: 120,
                height: 65
            }, i + 5);

            box.setTransform(boxesX + (180 * i), boxesY, 1.4, 1.4);
            box.value = i + 5;
            myStage.addChild(box);
            boxes.push(box);
            box.addEventListener('click', checkAnswer);
        };

        function checkAnswer(e) {
            if (e.currentTarget.value == myStage.answer) {
                myStage.onAnswerCorrect(e.currentTarget);
            } else {
                myStage.onAnswerIncorrect(e.currentTarget);
            }
        }

        this.wallet = new lib.wallet();
        this.wallet.setTransform(330, -30, 1.6, 1.6);

        this.task1 = new cjs.Container();
        this.t1_kr_1_1 = new lib.kr_1();
        this.t1_kr_1_1.setTransform(680, 50, 0.38, 0.38);
        this.t1_kr_2_1 = new lib.kr_2();
        this.t1_kr_2_1.setTransform(480, 190, 0.38, 0.38);
        this.t1_kr_2_2 = new lib.kr_2();
        this.t1_kr_2_2.setTransform(670, 200, 0.38, 0.38);
        this.t1_kr_5_1 = new lib.kr_5();
        this.t1_kr_5_1.setTransform(480, 35, 0.38, 0.38);
        this.task1.addChild(this.t1_kr_1_1, this.t1_kr_2_2, this.t1_kr_2_1, this.t1_kr_5_1)
        this.task1.visible = false;

        this.task2 = new cjs.Container();
        this.t2_kr_1_1 = new lib.kr_1();
        this.t2_kr_1_1.setTransform(590, 60, 0.38, 0.38);
        this.t2_kr_5_1 = new lib.kr_5();
        this.t2_kr_5_1.setTransform(490, 180, 0.38, 0.38);
        this.t2_kr_5_2 = new lib.kr_5();
        this.t2_kr_5_2.setTransform(680, 185, 0.38, 0.38);
        this.task2.addChild(this.t2_kr_1_1, this.t2_kr_5_2, this.t2_kr_5_1);
        this.task2.visible = false;

        this.task3 = new cjs.Container();
        this.t3_kr_1_1 = new lib.kr_1();
        this.t3_kr_1_1.setTransform(500, 50, 0.38, 0.38);
        this.t3_kr_1_2 = new lib.kr_1();
        this.t3_kr_1_2.setTransform(680, 50, 0.38, 0.38);
        this.t3_kr_2_1 = new lib.kr_2();
        this.t3_kr_2_1.setTransform(490, 190, 0.38, 0.38);
        this.t3_kr_2_2 = new lib.kr_2();
        this.t3_kr_2_2.setTransform(680, 200, 0.38, 0.38);
        this.task3.addChild(this.t3_kr_1_1, this.t3_kr_1_2, this.t3_kr_2_1, this.t3_kr_2_2);
        this.task3.visible = false;

        this.task4 = new cjs.Container();
        this.t4_kr_1_1 = new lib.kr_1();
        this.t4_kr_1_1.setTransform(460, 53, 0.35, 0.35);
        this.t4_kr_1_2 = new lib.kr_1();
        this.t4_kr_1_2.setTransform(620, 58, 0.35, 0.35);
        this.t4_kr_1_3 = new lib.kr_1();
        this.t4_kr_1_3.setTransform(780, 58, 0.35, 0.35);
        this.t4_kr_2_1 = new lib.kr_2();
        this.t4_kr_2_1.setTransform(450, 183, 0.35, 0.35);
        this.t4_kr_2_2 = new lib.kr_2();
        this.t4_kr_2_2.setTransform(610, 188, 0.35, 0.35);
        this.t4_kr_2_3 = new lib.kr_2();
        this.t4_kr_2_3.setTransform(770, 188, 0.35, 0.35);
        this.task4.addChild(this.t4_kr_1_1, this.t4_kr_1_2, this.t4_kr_1_3, this.t4_kr_2_1, this.t4_kr_2_2, this.t4_kr_2_3);
        this.task4.visible = false;

        this.task5 = new cjs.Container();
        this.t5_kr_1_1 = new lib.kr_1();
        this.t5_kr_1_1.setTransform(460, 50, 0.38, 0.38);
        this.t5_kr_1_2 = new lib.kr_1();
        this.t5_kr_1_2.setTransform(635, 55, 0.38, 0.38);
        this.t5_kr_1_3 = new lib.kr_1();
        this.t5_kr_1_3.setTransform(460, 50, 0.38, 0.38);
        this.t5_kr_1_4 = new lib.kr_1();
        this.t5_kr_1_4.setTransform(635, 55, 0.38, 0.38);
        this.t5_kr_2_1 = new lib.kr_2();
        this.t5_kr_2_1.setTransform(535, 180, 0.38, 0.38);
        this.t5_kr_5_1 = new lib.kr_5();
        this.t5_kr_5_1.setTransform(715, 185, 0.38, 0.38);
        this.task5.addChild(this.t5_kr_1_1, this.t5_kr_1_2, this.t5_kr_1_3, this.t5_kr_1_4, this.t5_kr_2_1, this.t5_kr_5_1);
        this.task5.visible = true;

        myStage.addChild(this.commentTexts, this.wallet, this.task1, this.task2, this.task3, this.task4, this.task5);

        function InvisibleAllCoins() {
            myStage.task1.visible = false;
            myStage.task2.visible = false;
            myStage.task3.visible = false;
            myStage.task4.visible = false;
            myStage.task5.visible = false;
        }

        exerciseCount = 0;
        // objects = shuffle(objects);

        this.onNewExercise = function(e) {
            if (exerciseCount < 5) {
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                // InvisibleAllCoins();
                var answers = [10, 11, 6, 9, 11];

                myStage.answer = answers[exerciseCount];

                // if (exerciseCount == 0) {
                //     myStage.task1.visible = true;
                // } else if (exerciseCount == 1) {
                //     myStage.task2.visible = true;
                // } else if (exerciseCount == 2) {
                //     myStage.task3.visible = true;
                // } else if (exerciseCount == 3) {
                //     myStage.task4.visible = true;
                // } else if (exerciseCount == 4) {
                //     myStage.task5.visible = true;
                // }

                // new lib.Magic_Wand(myStage, false);

                exerciseCount = exerciseCount + 1;
            }
            if (typeof btn_newExercise != 'undefined') {
                $(btn_newExercise).hide();
            }
        }
        var showIncorrectAnswerTween = null;
        var currentTimeoutAnimation = null;
        this.onAnswerCorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = true;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            if (exerciseCount < 5) {
                currentTimeoutAnimation = setTimeout(function() {
                    $(btn_newExercise).removeClass('disabled');
                    $(btn_newExercise).show();
                }, 1000)
            }
            stage.update();
        };
        this.onAnswerIncorrect = function(box) {
            cjs.Tween.removeAllTweens();
            if (currentTimeoutAnimation) {
                clearTimeout(currentTimeoutAnimation);
            }
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = false;
            for (var i = 0; i < boxes.length; i++) {
                boxes[i].number.color = "#000000";
            };
            box.number.color = "#ff0000";
            $(btn_newExercise).hide();
            stage.update();
            showIncorrectAnswerTween = cjs.Tween.get(box.number).wait(4000).call(function(e) {
                box.number.color = "#000000";
                myStage.correctIcon.visible = false;
                myStage.incorrectIcon.visible = false;
                myStage.incorrectAnswer.visible = false;

                stage.update();
            });
        };
        this.onCheckAnswer = function() {
            myStage.correctIcon.visible = true;
            myStage.incorrectIcon.visible = false;
            myStage.incorrectAnswer.visible = false;
            // for (var i = 0; i < circles.length; i++) {
            //     var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
            //         resetFillStyle[1] = "#ffffff";
            // }
            var answered = false;
            var selectedAnswers = [];
            for (var i = 0; i < myStage.dices.children.length; i++) {
                var resetFillStyle = myStage.dices.children[i].square.graphics._fillInstructions[0].params;
                if (resetFillStyle[1] === "#ffff00") {
                    selectedAnswers.push(myStage.dices.children[i]);
                }
            }
            for (var i = 0; i < selectedAnswers.length; i++) {
                if (selectedAnswers[i].value === myStage.answer) {
                    currentAnswers[1] = myStage.answer;
                    continue;
                } else {
                    currentAnswers[1] = 0;
                    break;
                }
            }

        };
        this.onIncorrectAnswer = function() {
            myStage.correctIcon.visible = false;
            myStage.incorrectIcon.visible = true;
            myStage.incorrectAnswer.visible = true;
            // for (var i = 0; i < circles.length; i++) {
            //     var resetFillStyle = circles[i].graphics._fillInstructions[0].params;
            //         resetFillStyle[1] = "#ffffff";
            // }
            cjs.Tween.get(myStage.dices).wait(500).call(function(e) {
                for (var i = 0; i < myStage.dices.children.length; i++) {
                    var resetFillStyle = myStage.dices.children[i].square.graphics._fillInstructions[0].params;
                    if (resetFillStyle[1] === "#ffff00") {
                        resetFillStyle[1] = "#ffffff";
                    }
                }
                stage.update();
            });

        };

        this.onNewExercise();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 11", "bold 24px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);

        this.text_1 = new cjs.Text("44", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(48, 0);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);




    (lib.balls = function() {
        this.initialize();
        //function () {



    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)



        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
