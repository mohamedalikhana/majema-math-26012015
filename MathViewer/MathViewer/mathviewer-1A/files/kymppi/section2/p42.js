(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p42_1.png",
            id: "p42_1"
        }, {
            src: "images/p42_2.png",
            id: "p42_2"
        }]
    };

    // symbols:

    (lib.p42_1 = function() {
        this.initialize(img.p42_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p42_2 = function() {
        this.initialize(img.p42_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageNoLeft = new cjs.Text("42", "13px 'Myriad Pro'", "#FFFFFF");
        this.pageNoLeft.lineHeight = 18;
        this.pageNoLeft.setTransform(39, 648);

        this.pageNoLeftBackground = new cjs.Shape();
        this.pageNoLeftBackground.graphics.f("#7AC729").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.pageNoLeftBackground.setTransform(31.1, 660.8);

        this.pageTitle = new cjs.Text("Vi skriver addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 19);

        this.chapterNumber = new cjs.Text("13", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(50.7, 15.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);
        this.instance = new lib.p42_1();
        this.instance.setTransform(0, 2, 0.502, 0.502);
        this.addChild(this.instance,this.pageNoLeftBackground, this.pageNoLeft);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol3 = function() {
        this.initialize();
        this.instance = new lib.p42_2();
        this.instance.setTransform(0, 26, 0.37, 0.37);
        this.text = new cjs.Text("Hur många fiskar är det tillsammans?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 5);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 5);
        var textToBeDisplayed1 = ['2', '+', '1', '=', '3'];

        this.textBoxGroup1 = new cjs.Shape();

        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 120, 20, 21);
        }
        this.textBoxGroup1.setTransform(0, 0);

        var textToBeDisplayed2 = ['3', '+', '1', '=', '4'];
        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 120, 20, 21);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 243, 20, 21);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 243, 20, 21);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 366, 20, 21);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 366, 20, 21);
        }
        this.textBoxGroup6.setTransform(0, 0);
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((1394 * 0.5) - 10) * 0.365, ((1004 * (1 / 3)) - 16) * 0.365, 10);
        this.roundRect1.setTransform(3, 31);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1394 * 0.5) + 8) * 0.37, 31);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 154);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1394 * 0.5) + 8) * 0.37, 154);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(3, 277);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((1394 * 0.5) + 8) * 0.37, 277);

        this.addChild(   this.instance,this.roundRect1,this.roundRect2,this.roundRect3,this.roundRect4, this.roundRect5, this.roundRect6, this.textBoxGroup1, this.textBoxGroup2, this.textBoxGroup3, this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6, this.text, this.text_1);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol3();
        this.v1.setTransform(294, 278, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1,this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
