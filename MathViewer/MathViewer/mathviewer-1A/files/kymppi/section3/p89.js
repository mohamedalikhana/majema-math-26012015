(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p89_1.png",
            id: "p89_1"
        }, {
            src: "images/p89_2.png",
            id: "p89_2"
        }, {
            src: "images/p89_3.png",
            id: "p89_3"
        }, {
            src: "images/p89_4.png",
            id: "p89_4"
        }, {
            src: "images/p89_5.png",
            id: "p89_5"
        }, {
            src: "images/p89_6.png",
            id: "p89_6"
        }, {
            src: "images/p89_7.png",
            id: "p89_7"
        }]
    };

    (lib.p89_1 = function() {
        this.initialize(img.p89_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p89_2 = function() {
        this.initialize(img.p89_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p89_3 = function() {
        this.initialize(img.p89_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p89_4 = function() {
        this.initialize(img.p89_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p89_5 = function() {
        this.initialize(img.p89_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p89_6 = function() {
        this.initialize(img.p89_6);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p89_7 = function() {
        this.initialize(img.p89_7);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("89", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Fortsätt mönstret.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 18; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(2 + (columnSpace * 28), 31 + (row * 28), 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 18; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(2 + (columnSpace * 28), 137 + (row * 28), 28, 28);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.instance_1 = new lib.p89_1();
        this.instance_1.setTransform(-3, 222, 0.378, 0.39);
        // block -1
        this.instance_2 = new lib.p89_2();
        this.instance_2.setTransform(2.3, 31.49, 0.370, 0.361);
        this.instance_3 = new lib.p89_2();
        this.instance_3.setTransform(58.3, 31.49, 0.370, 0.361);
        this.instance_4 = new lib.p89_2();
        this.instance_4.setTransform(114.4, 31.49, 0.370, 0.361);

        this.instance_5 = new lib.p89_2();
        this.instance_5.setTransform(2.3, 87.49, 0.370, 0.361);
        this.instance_6 = new lib.p89_2();
        this.instance_6.setTransform(58.3, 87.49, 0.370, 0.361);
        this.instance_7 = new lib.p89_2();
        this.instance_7.setTransform(114.4, 87.49, 0.370, 0.361);

        this.instance_8 = new lib.p89_3();
        this.instance_8.setTransform(30.1, 59.49, 0.370, 0.361);
        this.instance_9 = new lib.p89_3();
        this.instance_9.setTransform(86.1, 59.49, 0.370, 0.361);
        this.instance_10 = new lib.p89_3();
        this.instance_10.setTransform(142.1, 59.49, 0.370, 0.361);

        //block -2
        this.instance_11 = new lib.p89_4();
        this.instance_11.setTransform(2.3, 137.49, 0.371, 0.363);
        this.instance_11a = new lib.p89_5();
        this.instance_11a.setTransform(30.3, 138.5, 0.371, 0.363);
        this.instance_12 = new lib.p89_4();
        this.instance_12.setTransform(58.3, 137.49, 0.371, 0.363);
        this.instance_12a = new lib.p89_5();
        this.instance_12a.setTransform(86.3, 138.5, 0.371, 0.363);
        this.instance_13 = new lib.p89_4();
        this.instance_13.setTransform(114.4, 137.49, 0.371, 0.363);

        this.instance_14 = new lib.p89_6();
        this.instance_14.setTransform(2.6, 193.467, 0.371, 0.363);
        this.instance_14a = new lib.p89_7();
        this.instance_14a.setTransform(30.3, 193.4, 0.371, 0.363);
        this.instance_15 = new lib.p89_6();
        this.instance_15.setTransform(58.6, 193.467, 0.371, 0.363);
        this.instance_15a = new lib.p89_7();
        this.instance_15a.setTransform(86.3, 193.4, 0.371, 0.363);
        this.instance_16 = new lib.p89_6();
        this.instance_16.setTransform(114.7, 193.467, 0.371, 0.363);

        this.addChild(this.text_1, this.textbox_group1, this.textbox_group2, this.instance_1, this.instance_2,this.instance_3, this.instance_4);
        this.addChild(this.instance_5,this.instance_6,this.instance_7,this.instance_8,this.instance_9,this.instance_10);
        this.addChild(this.instance_11,this.instance_11a,this.instance_12,this.instance_12a,this.instance_13,this.instance_14,this.instance_15,this.instance_16);
        this.addChild(this.instance_14,this.instance_14a,this.instance_15,this.instance_15a,this.instance_16);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 300);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Gör början på egna mönster. Låt en kamrat fortsätta.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 7.6);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 18; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(2 + (columnSpace * 28), 37 + (row * 28), 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 18; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(2 + (columnSpace * 28), 143 + (row * 28), 28, 28);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.addChild(this.text_1, this.textbox_group1, this.textbox_group2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 540, 200);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(307.8, 270, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(307.8, 570, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
