(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p122_1.png",
            id: "p122_1"
        },{
            src: "images/p122_2.png",
            id: "p122_2"
        }]
    };

    (lib.p122_1 = function() {
        this.initialize(img.p122_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p122_2 = function() {
        this.initialize(img.p122_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("122", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(33, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(28.5, 660.8);

        this.instance = new lib.p122_1();
        this.instance.setTransform(-3, -2, 0.537, 0.551);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(451.9 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Räkna ut prisskillnaden.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(19, 0);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 13, 254, 125, 10);
        this.roundRect1.setTransform(0, 10);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(258, 10);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 142);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(258, 142);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 274);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(258, 274);

        this.instance = new lib.p122_2();
        this.instance.setTransform(16, 31, 0.47, 0.47);

        this.textbox_group1 = new cjs.Shape();
        var arryPos = ['119','251','383'];
        var xPos = 37,
            padding,
            yPos,
            rectWidth = 180,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 9,
            numberOfRects = 2;
        for (var k = 0; k < arryPos.length; k++) {
            yPos = parseInt(arryPos[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 0;
                if (i == 1) {
                    padding = padding + 39;
                }
                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);
        // row-1
        this.text_1 = new cjs.Text("8 kr", "13px 'Myriad Pro'");
        this.text_1.lineHeight = 15;
        this.text_1.setTransform(87, 51);
        this.text_1.skewX=-24;
        this.text_1.skewY=-24;
        this.text_2 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_2.lineHeight = 15;
        this.text_2.setTransform(177, 38.5);
        this.text_2.skewX=23;
        this.text_2.skewY=23;
        this.text_3 = new cjs.Text("8 kr", "13px 'Myriad Pro'");
        this.text_3.lineHeight = 15;
        this.text_3.setTransform(324, 46);
        this.text_3.skewX=-24;
        this.text_3.skewY=-24;
        this.text_4 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_4.lineHeight = 15;
        this.text_4.setTransform(432, 38);
        this.text_4.skewX=24;
        this.text_4.skewY=24;
        // row-2
        this.text_5 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_5.lineHeight = 15;
        this.text_5.setTransform(45, 169);
        this.text_5.skewX=9;
        this.text_5.skewY=9;
        this.text_6 = new cjs.Text("9 kr", "13px 'Myriad Pro'");
        this.text_6.lineHeight = 15;
        this.text_6.setTransform(210, 179);
        this.text_6.skewX=-9;
        this.text_6.skewY=-9;
        this.text_7 = new cjs.Text("7 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 15;
        this.text_7.setTransform(322, 179);
        this.text_7.skewX=-30;
        this.text_7.skewY=-30;
        this.text_8 = new cjs.Text("9 kr", "13px 'Myriad Pro'");
        this.text_8.lineHeight = 15;
        this.text_8.setTransform(464, 180);
        this.text_8.skewX=-9;
        this.text_8.skewY=-9;
        // row-3
        this.text_9 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 15;
        this.text_9.setTransform(91, 318);
        this.text_9.skewX=-9;
        this.text_9.skewY=-9;
        this.text_10 = new cjs.Text("10 kr", "13px 'Myriad Pro'");
        this.text_10.lineHeight = 15;
        this.text_10.setTransform(198.5, 305.5);
        this.text_10.skewX=-9;
        this.text_10.skewY=-9;
        this.text_11 = new cjs.Text("7 kr", "13px 'Myriad Pro'");
        this.text_11.lineHeight = 15;
        this.text_11.setTransform(333.5, 330.5);
        this.text_11.skewX=-9;
        this.text_11.skewY=-9;
        this.text_12 = new cjs.Text("10 kr", "13px 'Myriad Pro'");
        this.text_12.lineHeight = 15;
        this.text_12.setTransform(400.5, 315.5);
        this.text_12.skewX=10;
        this.text_12.skewY=10;
        
        this.addChild(this.text_q1, this.text_q2);
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);   
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(612.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(297, 442, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
