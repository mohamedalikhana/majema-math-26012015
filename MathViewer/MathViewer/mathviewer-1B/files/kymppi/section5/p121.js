(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };



    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("42", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(70, 43);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(58, 23.5,1.14,1);

        this.text_2 = new cjs.Text("Jämföra talen 0 till 100", "24px 'MyriadPro-Semibold'", "#00A3C4");
        this.text_2.setTransform(115, 42);

        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 0 till 100", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(395, 660);

        this.text = new cjs.Text("121", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(562, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(588, 660.8);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();
        // Block-1

        var ToBeAdded = [];
        var xpos = 22.9
        var xpos1 = 24.5
        for (var col = 0; col < 28; col++) {
            if (col > 13) {
                xpos = 67.9;
                xpos1 = 69.5
            }
            var shape_1 = new cjs.Shape();
            shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
            shape_1.setTransform(xpos + (col * 15.5), 16.4);

            var shape_2 = new cjs.Shape();
            shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
            shape_2.setTransform(xpos1 + (col * 15.5), 21.2);

            var shape_3 = new cjs.Shape();
            shape_3.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
            shape_3.setTransform(xpos1 + (col * 15.5), 21.2);
            ToBeAdded.push(shape_3, shape_2, shape_1)

        };


        this.roundRect2 = new cjs.Shape(); // white block
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(8, 15, 235, 130, 5);
        this.roundRect2.setTransform(2, 0);

        this.roundRect3 = new cjs.Shape(); // white block
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(272, 15, 235, 130, 5);
        this.roundRect3.setTransform(2, 0);

        // Main yellow block
        this.roundRect1 = new cjs.Shape(); // yellow box
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 0, 515, 175, 10);
        this.roundRect1.setTransform(0, 0);

        var xpos = 0;
        for (var col = 0; col < 22; col++) {
            var Line_2 = new cjs.Shape();
            if (col > 16) {
                xpos = 145
            } else if (col > 15) {
                xpos = 119
            } else if (col > 10) {
                xpos = 114
            } else if (col > 9) {
                xpos = 41
            } else if (col > 4) {
                xpos = 37
            } else {
                xpos = -0
            }

            Line_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(20, 31).lineTo(20, 119).lineTo(29, 119).lineTo(29, 31).lineTo(20, 31)
                .moveTo(20, 39.6).lineTo(29, 39.6).moveTo(20, 48.1).lineTo(29, 48.1).moveTo(20, 56.6).lineTo(29, 56.6).moveTo(20, 65.1).lineTo(29, 65.1)
                .moveTo(20, 73.6).lineTo(29, 73.6).moveTo(20, 82.1).lineTo(29, 82.1).moveTo(20, 91.6).lineTo(29, 91.6).moveTo(20, 101.1).lineTo(29, 101.1)
                .moveTo(20, 110.7).lineTo(29, 110.7);
            Line_2.setTransform(xpos + (col * 13.7), 0);
            ToBeAdded.push(Line_2)
        }

        var xpos = 0;
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 6; col++) {
                var yelowLine = new cjs.Shape();
                if (col > 3) {
                    xpos = 326
                } else if (col > 1) {
                    xpos = 95
                } else {
                    xpos = -3
                }

                if (col < 3 && row < 2) {
                    continue
                }
                if (col == 3 && row < 3) {
                    continue
                }
                if (col == 5 && row == 0) {
                    continue
                }

                yelowLine.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(93, 62.1).lineTo(93, 71)
                    .lineTo(101, 71).lineTo(101, 62.1).lineTo(93, 62.1)
                yelowLine.setTransform(xpos + (col * 13), 0 + (row * 12));
                ToBeAdded.push(yelowLine)
            }
        }

        var tmptxt = ""
        var xpos = 60
        var ypos = 168
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 2; col++) {
                if (col == 0 && row == 0) {
                    tmptxt = "56 är mindre än 65."
                } else if (col == 1 && row == 0) {
                    tmptxt = "  60 är större än 59."
                } else if (col == 0 && row == 1) {
                    tmptxt = "56            <           65";
                    xpos = 55;
                    ypos = 130
                } else if (col == 1 && row == 1) {
                    tmptxt = "60              >             59";
                    xpos = 56;
                    ypos = 130
                } else {
                    xpos = 60;
                    ypos = 168
                }

                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform(xpos + (col * 258), ypos + (row * 10));
                ToBeAdded.push(tempText);
            }
        }
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.hrLine, this.hrLine1, this.vrLine1, this.Line_3)
        this.addChild(this.hrLine2, this.hrLine3, this.vrLine2)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    // p.virtualBounds = new cjs.Rectangle(1, 1, 530.3, 123.3);
       p.virtualBounds = new cjs.Rectangle(1, 1, 515, 153.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Jämför. Skriv > eller <.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(20, 0);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 515, 371, 10);
        this.roundRect1.setTransform(0, 10);

        var ToBeAdded = [];

        for (var row = 0; row < 2; row++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(7, 133 + (row * 122)).lineTo(510, 133 + (row * 125));
            ToBeAdded.push(hrLine)
        };

        var moveToypos = 15;
        var lineToypos = 127;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 1; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 136;
                    lineToypos = 251;
                } else if (row == 2) {
                    moveToypos = 259;
                    lineToypos = 374;
                } else {
                    moveToypos = 15;
                    lineToypos = 127;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(259 + (col * 170), moveToypos).lineTo(259 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }
        var yPos = 0
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 22; col++) {
                var xPos = 3;

                if (col > 13 && row == 0) {
                    xPos = 205
                } else if (col > 8 && row == 0) {
                    xPos = 135
                } else if (col > 4 && row == 0) {
                    xPos = 90
                } else if (col > 19 && row == 1) {
                    xPos = 183
                } else if (col > 14 && row == 1) {
                    xPos = 176
                } else if (col > 9 && row == 1) {
                    xPos = 120
                } else if (col > 4 && row == 1) {
                    xPos = 85
                } else if (col > 20 && row == 2) {
                    xPos = 170
                } else if (col > 15 && row == 2) {
                    xPos = 160
                } else if (col > 10 && row == 2) {
                    xPos = 105
                } else if (col > 5 && row == 2) {
                    xPos = 57
                } else if (col > 4 && row == 2) {
                    xPos = 10
                } else {
                    xPos = 3
                }
                if (col > 18 && row == 0) {
                    continue
                }

                var greenSquareLine_2 = new cjs.Shape();
                greenSquareLine_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(10, 15).lineTo(10, 103).lineTo(19, 103).lineTo(19, 15).lineTo(10, 15)
                    .moveTo(10, 23.6).lineTo(19, 23.6).moveTo(10, 32.1).lineTo(19, 32.1).moveTo(10, 40.6).lineTo(19, 40.6).moveTo(10, 49.1).lineTo(19, 49.1)
                    .moveTo(10, 57.6).lineTo(19, 57.6).moveTo(10, 66.1).lineTo(19, 66.1).moveTo(10, 75.6).lineTo(19, 75.6).moveTo(10, 85.1).lineTo(19, 85.1)
                    .moveTo(10, 94.7).lineTo(19, 94.7);
                greenSquareLine_2.setTransform(xPos + (col * 14.3), yPos + (row * 123.5));
                ToBeAdded.push(greenSquareLine_2)
            }
        }
        //box 1 yellow boxes
        var xpos = 80
        var ypos = 0;
        for (var numOfrow = 0; numOfrow < 3; numOfrow++) {
            for (var row = 0; row < 5; row++) {
                for (var col = 0; col < 5; col++) {
                    var yellowSquareLine_3 = new cjs.Shape();
                    if (numOfrow == 1) {
                        ypos = 124
                    } else if (numOfrow == 2) {
                        ypos = 247
                    } else {
                        ypos = 0
                    }

                    if(numOfrow==0 && col==1){xpos=210} 
                        else if(numOfrow==0 && col==2){xpos=315} 
                            else if(numOfrow==0 && col==3){xpos=440} 
                                else if(numOfrow==2 && col==1){xpos=205} 
                        else if(numOfrow==2 && (col==2 || col==3) ){xpos=312} 
                            else if(numOfrow==2 && col==4){xpos=433} 
                                else if(numOfrow==2 && col==0){xpos=100} 
                        else {xpos=80}

                    if (numOfrow == 0 && (col == 0 || col == 1 || col == 3) && row < 2) {
                        continue
                    }
                    if (numOfrow == 0 && col == 2 && row < 3) {
                        continue
                    }
                    if (numOfrow == 2 && (col == 2 || col == 1 || col == 3) && row < 2) {
                        continue
                    }
                    if (numOfrow == 2 && col == 0 && row < 3) {
                        continue
                    }
                    if (numOfrow == 1 && col > 0) {
                        continue
                    }
                    if (numOfrow == 0 && col == 4) {
                        continue
                    }

                    yellowSquareLine_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(6, 46.1).lineTo(6, 55).lineTo(14, 55).lineTo(14, 46.1).lineTo(6, 46.1)
                    yellowSquareLine_3.setTransform(xpos + (col * 14), ypos + (row * 12));
                    ToBeAdded.push(yellowSquareLine_3)
                }
            }
        }

        //Rectangle box 
        var xpos = 120;
        var ypos = 108;
        var tmptxt = ""
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 2; column++) {
                var columnSpace = column;
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 260), ypos + (row * 124), 21, 20);
                ToBeAdded.push(tmp_Rect);
            }
        }
        //draw harizantial  lines
        var moveToxpos = 78
        var lineToxpos = 115
        var ypos = 127
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 4; col++) {
                var hrLine1 = new cjs.Shape();
                if (col > 1) {
                    moveToxpos = 202;
                    lineToxpos = 239
                } else {
                    moveToxpos = 78;
                    lineToxpos = 115
                }
                hrLine1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(moveToxpos + (col * 68), ypos + (row * 124))
                    .lineTo(lineToxpos + (col * 68), ypos + (row * 124));
                ToBeAdded.push(hrLine1)
            }
        }

        this.addChild(this.text_q1, this.text, this.roundRect1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 340);
     p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 350);

    // stage content: 
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310, 272, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol6();
        this.v2.setTransform(310, 475, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
