(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

            src: "images/p133_1.png",
            id: "p133_1"
        }]
    };

    (lib.p133_1 = function() {
        this.initialize(img.p133_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("133", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(455 + (columnSpace * 32), 17, 27, 27);
            }
        }
        this.textbox_group1.setTransform(15, 10);

        this.instance = new lib.p133_1();
        this.instance.setTransform(47, 0, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.instance, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 195, 10);
        this.roundRect1.setTransform(0, 10);

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(15, 0);
        this.arrow.setTransform(447, 37.5, 0.3, 0.3);
        this.arrow.rotation = -50;

        for (var i = 0; i < 21; i++) {
            var height = 7;
            height_settransform = 30
            var colspace = 19.5;
            var icount = i + 20;

            if (icount % 10 == 0) {
                height = 11;
                height_settransform = 26;
            } else if (number == 24 || number == 34) {
                height = 11;
                height_settransform = 26;
            }

            var vline = new cjs.Shape();

            if (icount % 10 == 0) {
                vline.graphics.f("#0CA3CB").s('#0CA3CB').ss(1).drawRect(0, 0, 1, height);
            } else if (icount == 25 || icount == 35) {
                vline.graphics.f("#000000").s('#000000').ss(1).drawRect(0, 0, 1, height);
            } else {
                vline.graphics.f("#000000").s('#000000').ss(0.7).drawRect(0, 0, 1, height);
            }
            vline.setTransform(48 + i * colspace, height_settransform);

            this.addChild(vline);

            var text;
            var ispace = 19.5;
            var number = i + 20;
            if (number > 99) {
                ispace = 19.35;
            }
            if (number % 10 == 0) {
                text = new cjs.Text(number + "", "bold 13px 'Myriad Pro'", "#0CA3CB");
            } else if (number == 25 || number == 35) {
                text = new cjs.Text(number + "", "bold 13px 'Myriad Pro'", "#000000");
            } else {

                text = new cjs.Text(number + "", "13px 'Myriad Pro'", "#000000");
            }

            text.setTransform(42 + i * ispace, 54);
            this.addChild(text);

        };

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(424, 0);
        this.linescale.setTransform(33, 37);
        this.addChild(this.linescale, this.arrow);


        var lineArr = [];
        var lineX = [79, 245, 424, 79, 245, 424, 79, 245, 424, 79, 245, 424, 79, 245, 424];
        var lineY = [48, 48, 48, 75, 75, 75, 102, 102, 102, 128, 128, 128, 154, 154, 154];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(42, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("20  +  3  =", "16px 'Myriad Pro'");
        this.label1.setTransform(22, 83);
        this.label2 = new cjs.Text("30  +  2  =", "16px 'Myriad Pro'");
        this.label2.setTransform(22, 108);
        this.label3 = new cjs.Text("20  +  5  =", "16px 'Myriad Pro'");
        this.label3.setTransform(22, 135);
        this.label4 = new cjs.Text("30  +  6  =", "16px 'Myriad Pro'");
        this.label4.setTransform(22, 162);
        this.label5 = new cjs.Text("20  +  8  =", "16px 'Myriad Pro'");
        this.label5.setTransform(22, 189);

        this.label6 = new cjs.Text("21  +  1  =", "16px 'Myriad Pro'");
        this.label6.setTransform(187, 83);
        this.label7 = new cjs.Text("22  +  1  =", "16px 'Myriad Pro'");
        this.label7.setTransform(187, 108);
        this.label8 = new cjs.Text("23  +  2  =", "16px 'Myriad Pro'");
        this.label8.setTransform(187, 135);
        this.label9 = new cjs.Text("24  +  2  =", "16px 'Myriad Pro'");
        this.label9.setTransform(187, 162);
        this.label10 = new cjs.Text("25  +  3  =", "16px 'Myriad Pro'");
        this.label10.setTransform(187, 189);

        this.label11 = new cjs.Text("32  +  1  =", "16px 'Myriad Pro'");
        this.label11.setTransform(357, 83);
        this.label12 = new cjs.Text("33  +  2  =", "16px 'Myriad Pro'");
        this.label12.setTransform(357, 108);
        this.label13 = new cjs.Text("34  +  1  =", "16px 'Myriad Pro'");
        this.label13.setTransform(357, 135);
        this.label14 = new cjs.Text("35  +  2  =", "16px 'Myriad Pro'");
        this.label14.setTransform(357, 162);
        this.label15 = new cjs.Text("36  +  3  =", "16px 'Myriad Pro'");
        this.label15.setTransform(357, 189);

        this.addChild(this.text, this.text_1, this.roundRect1, this.label1, this.label2, this.label3, this.label4, this.label5,
            this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 525.3, 205);

    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 2   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Addera.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 510, 195, 10);
        this.roundRect1.setTransform(0, 10);

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(15, 0);
        this.arrow.setTransform(447, 37.5, 0.3, 0.3);
        this.arrow.rotation = -50;

        for (var i = 0; i < 21; i++) {
            var height = 7;
            height_settransform = 30
            var colspace = 19.5;
            var icount = i + 80;

            if (icount % 10 == 0) {
                height = 11;
                height_settransform = 26;
            } else if (number == 84 || number == 94) {
                height = 11;
                height_settransform = 26;
            }

            var vline = new cjs.Shape();

            if (icount % 10 == 0) {
                vline.graphics.f("#0CA3CB").s('#0CA3CB').ss(1).drawRect(0, 0, 1, height);
            } else if (icount == 85 || icount == 95) {
                vline.graphics.f("#000000").s('#000000').ss(1).drawRect(0, 0, 1, height);
            } else {
                vline.graphics.f("#000000").s('#000000').ss(0.7).drawRect(0, 0, 1, height);
            }
            vline.setTransform(48 + i * colspace, height_settransform);

            this.addChild(vline);

            var text;
            var ispace = 19.5;
            var number = i + 80;
            if (number > 99) {
                ispace = 19.35;
            }
            if (number % 10 == 0) {
                text = new cjs.Text(number + "", "bold 13px 'Myriad Pro'", "#0CA3CB");
            } else if (number == 85 || number == 95) {
                text = new cjs.Text(number + "", "bold 13px 'Myriad Pro'", "#000000");
            } else {

                text = new cjs.Text(number + "", "13px 'Myriad Pro'", "#000000");
            }

            text.setTransform(42 + i * ispace, 54);
            this.addChild(text);

        };

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(1).moveTo(0, 0).lineTo(424, 0);
        this.linescale.setTransform(33, 37);
        this.addChild(this.linescale, this.arrow);

        var lineArr = [];
        var lineX = [79, 245, 424, 79, 245, 424, 79, 245, 424, 79, 245, 424, 79, 245, 424];
        var lineY = [48, 48, 48, 75, 75, 75, 102, 102, 102, 128, 128, 128, 154, 154, 154];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(9, 40).lineTo(42, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.label1 = new cjs.Text("80  +  4  =", "16px 'Myriad Pro'");
        this.label1.setTransform(22, 83);
        this.label2 = new cjs.Text("80  +  5  =", "16px 'Myriad Pro'");
        this.label2.setTransform(22, 108);
        this.label3 = new cjs.Text("80  +  7  =", "16px 'Myriad Pro'");
        this.label3.setTransform(22, 135);
        this.label4 = new cjs.Text("90  +  3  =", "16px 'Myriad Pro'");
        this.label4.setTransform(22, 162);
        this.label5 = new cjs.Text("90  +  9  =", "16px 'Myriad Pro'");
        this.label5.setTransform(22, 189);

        this.label6 = new cjs.Text("81  +  1  =", "16px 'Myriad Pro'");
        this.label6.setTransform(187, 83);
        this.label7 = new cjs.Text("82  +  2  =", "16px 'Myriad Pro'");
        this.label7.setTransform(187, 108);
        this.label8 = new cjs.Text("83  +  3  =", "16px 'Myriad Pro'");
        this.label8.setTransform(187, 135);
        this.label9 = new cjs.Text("85  +  2  =", "16px 'Myriad Pro'");
        this.label9.setTransform(187, 162);
        this.label10 = new cjs.Text("86  +  3  =", "16px 'Myriad Pro'");
        this.label10.setTransform(187, 189);

        this.label11 = new cjs.Text("91  +  2  =", "16px 'Myriad Pro'");
        this.label11.setTransform(357, 83);
        this.label12 = new cjs.Text("91  +  4  =", "16px 'Myriad Pro'");
        this.label12.setTransform(357, 108);
        this.label13 = new cjs.Text("92  +  2  =", "16px 'Myriad Pro'");
        this.label13.setTransform(357, 135);
        this.label14 = new cjs.Text("92  +  3  =", "16px 'Myriad Pro'");
        this.label14.setTransform(357, 162);
        this.label15 = new cjs.Text("93  +  4  =", "16px 'Myriad Pro'");
        this.label15.setTransform(357, 189);

        this.addChild(this.text, this.text_1, this.roundRect1, this.label1, this.label2, this.label3, this.label4, this.label5,
            this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 525.3, 205);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(309, 239, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309, 470, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.other, this.v1, this.v2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
