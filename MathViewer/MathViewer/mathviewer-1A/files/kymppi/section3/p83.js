(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p83_1.png",
            id: "p83_1"
        }]
    };

    (lib.p83_1 = function() {
        this.initialize(img.p83_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("83", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(555, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.instance = new lib.p83_1();
        this.instance.setTransform(52, 34, 0.829, 0.779);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(473 + (columnSpace * 32), 26 , 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape_2, this.shape_1, this.shape, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(5, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.instance_2 = new lib.Symbol5();
        this.instance_2.setTransform(193, 13.6, 1, 1, 0, 0, 0, 178.6, 12.6);

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(5, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 25, 510, 240, 10);
        this.roundRect1.setTransform(0, 0);

        var colGrp = ['18', '188', '358'];

        this.shape_group1 = new cjs.Shape();
        for (var colIndex = 0; colIndex < colGrp.length; colIndex++) {
            var rowStartVal = parseInt(colGrp[colIndex]);

            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    this.shape_group1.graphics.f("#FFF795").s("#000000").ss(0.8, 0, 0, 4).arc(rowStartVal + (columnSpace * 19), 50 + (row * 75), 8.2, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        var rowGrp = ['123', '293', '464'];

        this.shape_group2 = new cjs.Shape();
        for (var rowIndex = 0; rowIndex < rowGrp.length; rowIndex++) {
            var colStartVal = parseInt(rowGrp[rowIndex]);

            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                for (var row = 0; row < 3; row++) {
                    this.shape_group2.graphics.f("#FFF795").s("#000000").ss(0.8, 0, 0, 4).arc(colStartVal + (columnSpace * 19), 50 + (row * 75), 8.2, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            if(column==2){
                columnSpace=1.958;
            }
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(109 + (columnSpace * 174), 72 + (row * 75), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("8  –  1   =  ", "16.3px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(40, 77);
        this.label2 = new cjs.Text("8  –  4   =  ", "16.3px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(40, 151);
        this.label3 = new cjs.Text("8  –  7   =  ", "16.3px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(40, 227);

        this.label4 = new cjs.Text("8  –  0   =  ", "16.3px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(214, 77);
        this.label5 = new cjs.Text("8  –  2   =  ", "16.3px 'Myriad Pro'");
        this.label5.lineHeight = 30;
        this.label5.setTransform(214, 151);
        this.label6 = new cjs.Text("8  –  8   =  ", "16.3px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(214, 227);

        this.label7 = new cjs.Text("8  –  3   =  ", "16.3px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(378, 77);
        this.label8 = new cjs.Text("8  –  5   =  ", "16.3px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(378, 151);
        this.label9 = new cjs.Text("8  –  6   =  ", "16.3px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(378, 227);

        this.hrRule = new cjs.Shape();
        this.hrRule.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 104.5).lineTo(507, 104.5);
        this.hrRule.setTransform(0, 0);

        this.hrRule_2 = new cjs.Shape();
        this.hrRule_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(10, 182).lineTo(507, 182);
        this.hrRule_2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(174.8, 33).lineTo(174.8, 100);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(345, 33).lineTo(345, 100);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(174.8, 110).lineTo(174.8, 178);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(345, 110).lineTo(345, 178);
        this.Line_4.setTransform(0, 0);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(174.8, 187).lineTo(174.8, 262);
        this.Line_5.setTransform(0, 0);

        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(345, 187).lineTo(345, 262);
        this.Line_6.setTransform(0, 0);

        this.addChild(this.roundRect1, this.shape_group1, this.shape_group2, this.textbox_group1, this.hrRule, this.hrRule_2);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9);

        this.addChild(this.instance_2, this.text, this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Räkna.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 5);
        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(5, 5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 31, 510, 132, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(112 + (columnSpace * 154), 40 + (row * 28.5), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("7  +  1  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 30;
        this.label1.setTransform(45, 43);
        this.label2 = new cjs.Text("5  +  1  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 30;
        this.label2.setTransform(45, 72);
        this.label3 = new cjs.Text("6  +  2  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 30;
        this.label3.setTransform(45, 101);
        this.label4 = new cjs.Text("5  +  3  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 30;
        this.label4.setTransform(45, 130);

        this.label6 = new cjs.Text("4  +  4  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 30;
        this.label6.setTransform(200, 43);
        this.label7 = new cjs.Text("2  +  6  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 30;
        this.label7.setTransform(200, 72);
        this.label8 = new cjs.Text("3  +  4  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 30;
        this.label8.setTransform(200, 101);
        this.label9 = new cjs.Text("3  +  5  =  ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 30;
        this.label9.setTransform(200, 130);

        this.label11 = new cjs.Text("8  –  2  =  ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 30;
        this.label11.setTransform(355, 43);
        this.label12 = new cjs.Text("8  –  4  =  ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 30;
        this.label12.setTransform(355, 72);
        this.label13 = new cjs.Text("8  –  6  =  ", "16px 'Myriad Pro'");
        this.label13.lineHeight = 30;
        this.label13.setTransform(355, 101);
        this.label14 = new cjs.Text("8  –  7  =  ", "16px 'Myriad Pro'");
        this.label14.lineHeight = 30;
        this.label14.setTransform(355, 130);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label6, this.label7, this.label8, this.label9, this.label11, this.label12, this.label13, this.label14);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(309.5, 420, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(309.5, 472, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
