(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p144_1.png",
            id: "p144_1"
        }, {
            src: "images/p144_2.png",
            id: "p144_2"
        }]
    };

    (lib.p144_1 = function() {
        this.initialize(img.p144_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p144_2 = function() {
        this.initialize(img.p144_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Miniräknaren", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.setTransform(200, 73);

        this.text_1 = new cjs.Text("50", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(40, 43);

        this.text_3 = new cjs.Text("kunna använda en miniräknare", "9px 'Myriad Pro'", "#FAAA33");
        this.text_3.setTransform(90, 658);

        this.text_4 = new cjs.Text("144", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(32, 659);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(74, 649.7, 155, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p144_1();
        this.instance.setTransform(60, 0, 0.47, 0.47);

        this.text_1 = new cjs.Text("Räkna med miniräknaren. Skriv vilka knappar du tryckte på.", "16px 'Myriad Pro'");
        this.text_1.setTransform(4, 145);

        this.text_2 = new cjs.Text("Tryck", "14px 'Myriad Pro");
        this.text_2.setTransform(197, 57);

        this.tbox = new cjs.Shape();
        this.tbox.graphics.f("#F69C9C").s("#000000").ss(1).drawRect(0, 0, 15, 15, 0);
        this.tbox.setTransform(235, 41);

        this.text_3 = new cjs.Text("C", "14px 'Myriad Pro");
        this.text_3.setTransform(235, 53);

        this.text_4 = new cjs.Text("när du", "14px 'Myriad Pro");
        this.text_4.setTransform(252, 57);

        this.text_5 = new cjs.Text("vill börja om.", "14px 'Myriad Pro");
        this.text_5.setTransform(205, 74);

        this.instance_1 = new lib.p144_2();
        this.instance_1.setTransform(200, 170, 0.46, 0.47);

        this.round_Rect = new cjs.Shape();
        this.round_Rect.graphics.f("").s("#B7B7B7").ss(1).drawRoundRect(0, 0, 107, 29, 7);
        this.round_Rect.setTransform(398, 170);

        this.round_Rect1 = this.round_Rect.clone(true);
        this.round_Rect1.setTransform(398, 206);

        this.round_Rect2 = this.round_Rect.clone(true);
        this.round_Rect2.setTransform(398, 242);

        this.round_Rect3 = this.round_Rect.clone(true);
        this.round_Rect3.setTransform(398, 278);

        this.round_Rect4 = this.round_Rect.clone(true);
        this.round_Rect4.setTransform(398, 314);


        var TArr = [];

        var TxtArr = ['6', '3', '7', '4', '28', '6', '61', '9', '30', '40'];

        var TxtlineX = [48, 70, 48, 70, 39, 70, 39, 70, 30, 61];
        var TxtlineY = [190, 190, 225, 225, 261, 261, 301, 301, 336, 336];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var SymArr = [];
        var symblArr = ['+', '=', '–', '=', '–', '=', '+', '=', '+', '=']

        var symbllineX = [57, 81, 58, 81, 58, 81, 58, 81, 49, 80];
        var symbllineY = [189, 189, 224, 224, 260, 260, 300, 300, 335, 335];

        for (var i = 0; i < symbllineX.length; i++) {
            var symbltxt = symblArr[i];
            this.temp_label = new cjs.Text(symbltxt, "16px 'Myriad Pro'");
            this.temp_label.setTransform(symbllineX[i], symbllineY[i]);
            SymArr.push(this.temp_label);
        }

        var hrlineArr = [];
        var hrlineX = [98, 98, 98, 98, 98];
        var hrlineY = [190, 225, 261, 301, 336];

        for (var i = 0; i < hrlineX.length; i++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#9D9D9C").setStrokeStyle(1).moveTo(0, 0).lineTo(70, 0);
            hrLine_1.setTransform(hrlineX[i], hrlineY[i]);
            hrlineArr.push(hrLine_1);
        }

        this.addChild(this.instance, this.instance_1, this.text_1, this.text_2, this.tbox, this.text_3, this.text_4, this.text_5,
            this.round_Rect, this.round_Rect1, this.round_Rect2, this.round_Rect3, this.round_Rect4);

        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }

        for (var i = 0; i < SymArr.length; i++) {
            this.addChild(SymArr[i]);
        }
        for (var i = 0; i < hrlineArr.length; i++) {
            this.addChild(hrlineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 350);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Välj egna uppgifter och räkna med miniräknaren.", "16px 'Myriad Pro'");
        this.text_1.setTransform(4, 0);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 499, 192, 10);
        this.round_Rect1.setTransform(4, 15);

        this.addChild(this.text_1, this.round_Rect1);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 573.3, 205);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(302, 235, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(302, 605, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
