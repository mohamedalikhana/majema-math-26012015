(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p111_1.png",
            id: "p111_1"
        }]
    };

    (lib.p111_1 = function() {
        this.initialize(img.p111_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();
        this.text = new cjs.Text("111", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(551, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.instance = new lib.p111_1();
        this.instance.setTransform(52, 0, 0.469, 0.46);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(473 + (columnSpace * 32), 24, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();


        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Skriv talen som saknas på tallinjen.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 198, 10);
        this.roundRect1.setTransform(0, 0);
        //draw arrow line
        var ToBeAdded = [];
        var lineToypos = 35
        var ypos = 0;       
        for (var row = 0; row < 3; row++) {
            var rowSpace = row;
            for (var col = 0; col < 11; col++) {
                if (row == 1) {
                    ypos = 63
                } else if (row == 2) {
                    ypos = 123
                } else {
                    ypos = 0
                }
                var temp_hrLine1 = new cjs.Shape();
                temp_hrLine1.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.1).moveTo(25, (43 + ypos)).lineTo(485, (43 + ypos))
                    .moveTo(485, (43 + ypos)).lineTo(483, (39 + ypos)).lineTo(491, (43 + ypos)).lineTo(483, (47 + ypos))
                if (col == 0 || col == 5 || col == 10) {
                    lineToypos = 30
                } else {
                    lineToypos = 35
                }
                var temp_vrLine1 = new cjs.Shape();
                temp_vrLine1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40 + (col * 41.5), (43 + ypos))
                    .lineTo(40 + (col * 41.5), (lineToypos + ypos));
                ToBeAdded.push(temp_hrLine1, temp_vrLine1)
            }
        };


        var xpos = 65;
        var ypos = 50;
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 8; column++) {
                var columnSpace = column;
                if (column > 3 && row == 0) {
                    xpos = 110
                } else if (column > 2 && row == 1) {
                    xpos = 110
                } else if (column > 4 && row == 2) {
                    xpos = 110
                } else {
                    xpos = 65
                }
                if (row == 1) {
                    ypos = 78
                } else if (row == 2) {
                    ypos = 105
                } else(ypos = 50)
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 41), ypos + (row * 35), 33, 23);
                ToBeAdded.push(tmp_Rect);
            }
        }

        //set Number  "16px 'Myriad Pro'"
        var tmpNum = [20, 25, 30, 30, 34, 40, 40, 46, 50]
        var j = 0
        var fontColor = "#00ABDC"
        var fontname = "16px 'MyriadPro-Semibold'"
        var xpos = 30
        var ypos = 60
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 3; col++) {
                if (col == 1 && row == 0) {
                    xpos = 240;
                    fontColor = "#000000"
                } else if (col == 2) {
                    xpos = 447;
                    fontColor = "#00ABDC"
                     fontname = "16px 'MyriadPro-Semibold'"
                } else if (col == 1 && row == 1) {
                    xpos = 198;
                    fontColor = "#000000"
                    fontname="16px 'Myriad Pro'"
                } else if (col == 1 && row == 2) {
                    xpos = 280;
                    fontColor = "#000000"
                    fontname="16px 'Myriad Pro'"
                } else {
                    xpos = 30;
                    fontColor = "#00ABDC"
                    fontname = "16px 'MyriadPro-Semibold'"
                }
                var tempText = new cjs.Text(tmpNum[j], fontname, fontColor);
                tempText.setTransform(xpos, ypos + (row * 62));
                ToBeAdded.push(tempText);
                j++
            }
        }


        this.addChild(this.roundRect1, this.text, this.text_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Subtrahera med 1.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 160, 10);
        this.roundRect1.setTransform(0, 0);

        var ToBeAdded = [];
        var xpos = 15;
        var ypos = 30;
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 4; row++) {
            for (var column = 0; column < 12; column++) {
                var columnSpace = column;
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 39.5), ypos + (row * 35), 39.5, 24);
                ToBeAdded.push(tmp_Rect);
            }
        }

        var tmpNum = ""
        var j = 0;
        var xpos = 0;
        var ypos = 52.7
        for (var row = 0; row < 4; row++) {
            for (var column = 0; column < 12; column++) {
                columnSpace = column
                if (row == 0 && column == 9) {
                    xpos = 255;
                    tmpNum = 18
                } else if (row == 0 && column == 10) {
                    xpos = 295;
                    tmpNum = 19
                } else if (row == 0 && column == 11) {
                    xpos = 336;
                    tmpNum = 20
                } else if (row == 1 && column == 9) {
                    xpos = 255;
                    tmpNum = 28;
                    ypos = 88
                } else if (row == 1 && column == 10) {
                    xpos = 295;
                    tmpNum = 29;
                    ypos = 88
                } else if (row == 1 && column == 11) {
                    xpos = 336;
                    tmpNum = 30;
                    ypos = 88
                } else if (row == 2 && column == 10) {
                    xpos = 295;
                    tmpNum = 39;
                    ypos = 122.7
                } else if (row == 2 && column == 11) {
                    xpos = 336;
                    tmpNum = 40;
                    ypos = 122.7
                } else if (row == 3 && column == 10) {
                    xpos = 295;
                    tmpNum = 49;
                    ypos = 157.7
                } else if (row == 3 && column == 11) {
                    xpos = 336;
                    tmpNum = 50;
                    ypos = 157.7
                } else {
                    tmpNum = ""
                }
                var tempText = new cjs.Text(tmpNum, "36px 'UusiTekstausMajema'", "#6C7373");
                tempText.setTransform(xpos + 117, ypos);
                ToBeAdded.push(tempText);
            }
        }

        // harizantal Line

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.6).moveTo(490, 22).lineTo(330, 22)
            .moveTo(334, 26).lineTo(330, 22).lineTo(334, 18)
        this.hrLine_1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.hrLine_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(312.5, 443, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(312.5, 462, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
