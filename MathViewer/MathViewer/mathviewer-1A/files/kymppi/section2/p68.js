(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p68_1.png",
            id: "p68_1"
        },{
            src: "images/p68_2.png",
            id: "p68_2"
        },{
            src: "images/p68_3.png",
            id: "p68_3"
        },{
            src: "images/p68_4.png",
            id: "p68_4"
        }]
    };

    (lib.p68_1 = function() {
        this.initialize(img.p68_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p68_2 = function() {
        this.initialize(img.p68_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p68_3 = function() {
        this.initialize(img.p68_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p68_4 = function() {
        this.initialize(img.p68_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol11 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("68", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(34.5, 648);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#87C140").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31, 660.8);

        this.text_3 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#87C140");
        this.text_3.lineHeight = 29;
        this.text_3.setTransform(96, 28);

        this.text_4 = new cjs.Text("23", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.lineHeight = 34;
        this.text_4.setTransform(48, 24);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#87C140").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(44.5, 23.5,1.02,1.1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 145, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 33, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p68_1();
        this.instance.setTransform(503, 38, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Räkna.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {

                if (column == 1) {
                    columnSpace = 0.69;
                } else if (column == 2) {
                    columnSpace = 1.33;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.8).drawRect(105 + (columnSpace * 245), 29 + (row * 28), 20, 23);
            }
        }   
        this.textbox_group1.setTransform(0, 0);

        var ToBeAdded = [];
        var arrxPos = [];
        
        arrxPos = [];
        arrxPos = ['30.5', '202', '358'];
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            var text = "";

            for (var j = 0; j < 4; j++) {
                var rowSpace = j;

                if (i == 0) {
                    if (j == 0) {
                        text = "3   +   2   =";
                    } else if (j == 1) {
                        text = "1   +   4   =";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "3   +   3   =";
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "2   +   4   =";
                    }
                } else if (i == 1) {
                    if (j == 0) {
                        text = "5   –   4   =";
                    } else if (j == 1) {
                        text = "6   –   1   =";
                    } else if (j == 2) {
                        text = "6   –   3   =";
                        rowSpace = 2.05;
                    } else if (j == 3) {
                        text = "6   –   4   =";
                        rowSpace = 3.045;
                    }
                } else if (i == 2) {
                    if (j == 0) {
                        text = "5   –   0   =";
                    } else if (j == 1) {
                        xPos=356.5;
                        text = "6   +   0   =";
                    } else if (j == 2) {
                        rowSpace = 2.05;
                        text = "5   –   5   =";
                        xPos=358;
                    } else if (j == 3) {
                        rowSpace = 3.045;
                        text = "6   –   6   =";
                        xPos=356.5;
                    }
                }
                var temp_text = new cjs.Text(text, "15.5px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(xPos, 33 + (rowSpace * 27));
                ToBeAdded.push(temp_text);
            }
        }

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1, this.shape_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 132, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 25, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p68_1();
        this.instance.setTransform(503, 30, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Det kommer 2 fåglar till.", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        this.instance_2 = new lib.p68_2();
        this.instance_2.setTransform(19, 25, 0.49, 0.49);

        var arrTxtbox = ['103'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 100,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 70;
                if (i == 1) {
                    padding = 113;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);        

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(248, 34).lineTo(248, 127);
        this.Line_1.setTransform(0, 0);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1);
        this.addChild(this.instance_2, this.Line_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 131, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 25, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p68_1();
        this.instance.setTransform(503, 30, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Hur många finns kvar?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 11);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 11);

        this.instance_2 = new lib.p68_3();
        this.instance_2.setTransform(8, 29, 0.48, 0.48);

        var arrTxtbox = ['101'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 100,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 70;
                if (i == 1) {
                    padding = 113;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);        

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(248, 28).lineTo(248, 117);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("Jag äter 3.", "14px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(114, 49);

        this.text_2 = new cjs.Text("Jag äter", "14px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(373, 39);

        this.text_3 = new cjs.Text("hälften.", "14px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(373, 54);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1);
        this.addChild(this.instance_2, this.Line_1, this.text_1, this.text_2, this.text_3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol4 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 129, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 25, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p68_1();
        this.instance.setTransform(503, 30, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Hur många kaniner gömmer sig?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(23, 6);

        this.text_q2 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_q2.lineHeight = 27;
        this.text_q2.setTransform(4, 6);

        this.instance_2 = new lib.p68_4();
        this.instance_2.setTransform(25, 24, 0.48, 0.48);

        var arrTxtbox = ['101'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 20,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 1,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 112;
                if (i == 1) {
                    padding = 151;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);        

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.s("#949599").ss(1).moveTo(248, 28).lineTo(248, 124);
        this.Line_1.setTransform(0, 0);

        this.text_1 = new cjs.Text("3  +           =  6", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(77, 105);

        this.text_2 = new cjs.Text("+  1  =  6", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(350, 105);

        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.textbox_group1);
        this.addChild(this.instance_2, this.Line_1, this.text_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 551.3, 110);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol11();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(293, 138, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(293, 287, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.v3 = new lib.Symbol3();
        this.v3.setTransform(293, 423, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v4 = new lib.Symbol4();
        this.v4.setTransform(293, 559, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
