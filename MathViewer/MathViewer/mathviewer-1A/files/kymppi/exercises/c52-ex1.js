var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c51_ex1_1.png",
            id: "c51_ex1_1"
        }, {
            src: "images/c51_ex1_2.png",
            id: "c51_ex1_2"
        }, {
            src: "images/c52_ex1_1.png",
            id: "c52_ex1_1"
        }, {
            src: "images/c51_ex1_5.png",
            id: "c51_ex1_5"
        }]
    };

    var moveX = 715,
        moveY = 520;



    (lib.c51_ex1_1 = function() {
        this.initialize(img.c51_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c51_ex1_2 = function() {
        this.initialize(img.c51_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c52_ex1_1 = function() {
        this.initialize(img.c52_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c51_ex1_5 = function() {
        this.initialize(img.c51_ex1_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Mot programmering 2", "bold 24px 'Myriad Pro'", "#D83770");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("52", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();
        this.img_roboMsg = new lib.c51_ex1_1();
        this.img_roboMsg.setTransform(920, 260);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7d7d7d").setStrokeStyle(2).moveTo(980, 230).lineTo(1000, 260).lineTo(1000, 230);
        this.Line_1.setTransform(0, 0);

        this.round_rect = new cjs.Shape();
        this.round_rect.graphics.f("#ffffff").s('#7d7d7d').ss(2).drawRoundRect(760, -80, 550, 260 + 50, 10);
        this.round_rect.setTransform(0, 0);

        this.img_robo = new lib.c51_ex1_2();
        this.img_robo.setTransform(220, 345);

        this.img_toy = new lib.c52_ex1_1();
        this.img_toy.setTransform(460, 120);
        this.img_toy.visible = false;

        this.img_ball = new lib.c51_ex1_5();
        this.img_ball.setTransform(230, 240);
        this.img_ball.visible = false;

        var ToBeAdded = [];
        this.grid = [];
        for (var row = 0; row < 4; row++) {
            for (var col = 0; col < 4; col++) {
                var temp_rect = new cjs.Shape();
                temp_rect.graphics.f("#ffffff").s('#7d7d7d').ss(1.5).drawRect(80, -30, 120, 120);
                temp_rect.setTransform(0 + (col * 120), 0 + (row * 120));
                this.grid.push(temp_rect);
                ToBeAdded.push(temp_rect);
            }
        }

        this.arrow_box = [];
        for (var col = 0; col < 4; col++) {
            var temp_rect = new cjs.Shape();
            temp_rect.graphics.f("#ffffff").s('#7d7d7d').drawRect(100, 473, 80, 80);
            temp_rect.setTransform(0 + (col * 120) + moveX, 0 - moveY);
            temp_rect.visible = true;
            this.arrow_box.push(temp_rect);
            ToBeAdded.push(temp_rect);
        }

        this.tempLine_1 = new cjs.Shape();
        this.tempLine_1.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(140, 542).lineTo(140, 500).lineTo(134, 500).lineTo(140, 487).lineTo(146, 500).lineTo(140, 500);
        this.tempLine_1.setTransform(0 + moveX, 0 - moveY);
        this.tempLine_1.visible = false;

        this.tempLine_2 = new cjs.Shape();
        this.tempLine_2.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(140, 542).lineTo(140, 500).lineTo(134, 500).lineTo(140, 487).lineTo(146, 500).lineTo(140, 500);
        this.tempLine_2.setTransform(120 + moveX, 0 - moveY);
        this.tempLine_2.visible = false;

        this.tempLine_3 = new cjs.Shape();
        this.tempLine_3.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(112, 514).lineTo(154, 514).lineTo(154, 508).lineTo(167, 514).lineTo(154, 520).lineTo(154, 514);
        this.tempLine_3.setTransform(240 + moveX, 0 - moveY);
        this.tempLine_3.visible = false;

        this.tempLine_4 = new cjs.Shape();
        this.tempLine_4.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(112, 514).lineTo(154, 514).lineTo(154, 508).lineTo(167, 514).lineTo(154, 520).lineTo(154, 514);
        this.tempLine_4.setTransform(360 + moveX, 0 - moveY);
        this.tempLine_4.visible = false;

        this.tempLine_5 = new cjs.Shape();
        this.tempLine_5.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(140, 485).lineTo(140, 527).lineTo(134, 527).lineTo(140, 540).lineTo(146, 527).lineTo(140, 527);
        this.tempLine_5.setTransform(0 + moveX, 0 - moveY);
        this.tempLine_5.visible = false;

        this.tempLine_6 = new cjs.Shape();
        this.tempLine_6.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(167, 514).lineTo(125, 514).lineTo(125, 520).lineTo(112, 514).lineTo(125, 508).lineTo(125, 514);
        this.tempLine_6.setTransform(120 + moveX, 0 - moveY);
        this.tempLine_6.visible = false;

        this.tempLine_7 = new cjs.Shape();
        this.tempLine_7.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(167, 514).lineTo(125, 514).lineTo(125, 520).lineTo(112, 514).lineTo(125, 508).lineTo(125, 514);
        this.tempLine_7.setTransform(240 + moveX, 0 - moveY);
        this.tempLine_7.visible = false;

        this.tempLine_8 = new cjs.Shape();
        this.tempLine_8.graphics.beginStroke("#000000").f('#000000').setStrokeStyle(2).moveTo(140, 485).lineTo(140, 527).lineTo(134, 527).lineTo(140, 540).lineTo(146, 527).lineTo(140, 527);
        this.tempLine_8.setTransform(360 + moveX, 0 - moveY);
        this.tempLine_8.visible = false;

        this.instrTexts = [];
        this.instrTexts.push('Rutroboten.');
        this.instrTexts.push('Vilken pilkod behöver \n\n\n roboten nu?');
        this.instrTexts.push('Ett steg uppåt.');
        this.instrTexts.push('Ett steg åt höger.');
        this.instrTexts.push('Ett steg neråt.');
        this.instrTexts.push('Ett steg åt vänster.');

        this.instrText1 = new cjs.Text(this.instrTexts[1], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(1030, 105);

        this.addChild(this.img_roboMsg, this.Line_1, this.round_rect, this.instrText1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }
        this.addChild(this.img_toy, this.img_ball, this.img_robo);
        this.addChild(this.tempLine_1, this.tempLine_2, this.tempLine_3, this.tempLine_4);
        this.addChild(this.tempLine_5, this.tempLine_6, this.tempLine_7, this.tempLine_8);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.y = this.stage0.instrText1.y - 25;
        this.stage0.instrText1.text = this.stage0.instrTexts[1];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 5 || i == 6 || i == 7 || i == 9 || i == 13) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_toy.visible = true;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[2];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 5 || i == 6 || i == 7 || i == 9 || i == 13) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_toy.visible = true;
        this.stage0.tempLine_1.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y - 120
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[2];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 5 || i == 6 || i == 7 || i == 9 || i == 13) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_toy.visible = true;
        this.stage0.tempLine_1.visible = true;
        this.stage0.tempLine_2.visible = true;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 120;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y - 120
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[3];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 5 || i == 6 || i == 7 || i == 9 || i == 13) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_toy.visible = true;
        this.stage0.tempLine_1.visible = true;
        this.stage0.tempLine_2.visible = true;
        this.stage0.tempLine_3.visible = true;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 240;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x + 120,
                y: this.stage0.img_robo.y
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[3];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 5 || i == 6 || i == 7 || i == 9 || i == 13) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_toy.visible = true;
        this.stage0.tempLine_1.visible = true;
        this.stage0.tempLine_2.visible = true;
        this.stage0.tempLine_3.visible = true;
        this.stage0.tempLine_4.visible = true;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 240;
        this.stage0.img_robo.x = this.stage0.img_robo.x + 120;
        this.stage0.img_robo.scaleX = this.stage0.img_robo.scaleY = 0.9;
        this.stage0.img_toy.scaleX = this.stage0.img_toy.scaleY = 0.9;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x + 104,
                y: this.stage0.img_robo.y + 20
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        this.tweens.push({
            ref: this.stage0.img_toy,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_toy.x,
                y: this.stage0.img_toy.y
            },
            positionTo: {
                x: this.stage0.img_toy.x + 22,
                y: this.stage0.img_toy.y - 30
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage6 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.y = this.stage0.instrText1.y - 25;
        this.stage0.instrText1.text = this.stage0.instrTexts[1];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 3 || i == 6 || i == 7 || i == 9 || i == 5) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_ball.visible = true;
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 360;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage7 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[4];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 3 || i == 6 || i == 7 || i == 9 || i == 5) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_ball.visible = true;
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 360;
        this.stage0.tempLine_5.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y + 120
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage8 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[5];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 3 || i == 6 || i == 7 || i == 9 || i == 5) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_ball.visible = true;
        this.stage0.img_robo.x = this.stage0.img_robo.x + 240;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 240;
        this.stage0.tempLine_5.visible = true;
        this.stage0.tempLine_6.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_6,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x - 120,
                y: this.stage0.img_robo.y
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage9 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[5];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 3 || i == 6 || i == 7 || i == 9 || i == 5) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_ball.visible = true;
        this.stage0.img_robo.x = this.stage0.img_robo.x + 120;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 240;
        this.stage0.tempLine_5.visible = true;
        this.stage0.tempLine_6.visible = true;
        this.stage0.tempLine_7.visible = true;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_7,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x - 120,
                y: this.stage0.img_robo.y
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage10 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.text = this.stage0.instrTexts[4];
        for (var i = 0; i < this.stage0.grid.length; i++) {
            if (i == 3 || i == 6 || i == 7 || i == 9 || i == 5) {
                continue;
            }
            this.stage0.grid[i].graphics._fillInstructions[0].params[1] = '#67FF2A';
        };
        this.stage0.img_ball.visible = true;
        this.stage0.img_robo.y = this.stage0.img_robo.y - 240;
        this.stage0.tempLine_5.visible = true;
        this.stage0.tempLine_6.visible = true;
        this.stage0.tempLine_7.visible = true;
        this.stage0.tempLine_8.visible = true;
        this.stage0.img_robo.scaleX = this.stage0.img_robo.scaleY = 0.9;
        this.stage0.img_ball.scaleX = this.stage0.img_ball.scaleY = 0.9;
        this.addChild(this.stage0);

        this.tweens = [];

        this.tweens.push({
            ref: this.stage0.tempLine_8,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_robo,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_robo.x,
                y: this.stage0.img_robo.y
            },
            positionTo: {
                x: this.stage0.img_robo.x + 22,
                y: this.stage0.img_robo.y + 107
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        this.tweens.push({
            ref: this.stage0.img_ball,
            alphaFrom: 1,
            alphaTo: 1,
            positionFrom: {
                x: this.stage0.img_ball.x,
                y: this.stage0.img_ball.y
            },
            positionTo: {
                x: this.stage0.img_ball.x - 28,
                y: this.stage0.img_ball.y + 37
            },
            wait: 3500,
            alphaTimeout: 3000,
        });

        p.tweens = this.tweens;

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage5 = new lib.Stage5();
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage6 = new lib.Stage6();
        this.stage6.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage7 = new lib.Stage7();
        this.stage7.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage8 = new lib.Stage8();
        this.stage8.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage9 = new lib.Stage9();
        this.stage9.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage10 = new lib.Stage10();
        this.stage10.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7, this.stage8, this.stage9, this.stage10);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
