(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };



    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("41", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(57, 42);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(52, 23.5);

        this.text_2 = new cjs.Text("Talen 50 till 100", "24px 'MyriadPro-Semibold'", "#00A3C4");
        this.text_2.setTransform(103, 42);


        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 50 till 100", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(75, 656);

        this.text = new cjs.Text("118", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(44, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(39.5, 660.8, 1, 1.1);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();
        // Block-1

        var ToBeAdded = [];
        var xpos = 35.9
        var xpos1 = 37.4
        for (var col = 0; col < 24; col++) {
            if (col > 11) {
                xpos = 97.9;
                xpos1 = 99.4
            }
            var shape_1 = new cjs.Shape();
            shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
            shape_1.setTransform(xpos + (col * 16), 16.4);

            var shape_2 = new cjs.Shape();
            shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
            shape_2.setTransform(xpos1 + (col * 16), 21.5);

            var shape_3 = new cjs.Shape();
            shape_3.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
            shape_3.setTransform(xpos1 + (col * 16), 21.5);
            ToBeAdded.push( shape_3, shape_2,shape_1)
             // ToBeAdded.push(shape_1, shape_2, shape_3)

        };

        this.roundRect2 = new cjs.Shape(); // white block
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(25, 15, 205, 165, 5);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape(); // white block
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(278, 15, 205, 165, 5);
        this.roundRect3.setTransform(0, 0);

        // Main yellow block
        this.roundRect1 = new cjs.Shape(); // yellow box
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 1, 509, 205, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape(); // inner box
        this.roundRect4.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(35, 33, 183, 140, 0);
        this.roundRect4.setTransform(0, 0);

        this.roundRect5 = this.roundRect4.clone(true)
        this.roundRect5.setTransform(255, 0);

        var hrLine = new cjs.Shape();
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 2; col++) {
                hrLine.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).moveTo(35 + (col * 255), 50 + (row * 104)).lineTo(218 + (col * 255), 50 + (row * 104))
                ToBeAdded.push(hrLine)
            }
        };

        var vrLine1 = new cjs.Shape();

        for (var col = 0; col < 2; col++) {
            vrLine1.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).moveTo(147 + (col * 255), 33).lineTo(147 + (col * 255), 172)
            ToBeAdded.push(vrLine1)
        }

        var xpos = 0
        for (var col = 0; col < 11; col++) {
            var greenSquareLine_2 = new cjs.Shape();
            greenSquareLine_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(45, 60).lineTo(45, 148).lineTo(54, 148).lineTo(54, 60).lineTo(45, 60)
                .moveTo(45, 68.6).lineTo(54, 68.6).moveTo(45, 77.1).lineTo(54, 77.1).moveTo(45, 85.6).lineTo(54, 85.6).moveTo(45, 94.1).lineTo(54, 94.1)
                .moveTo(45, 102.6).lineTo(54, 102.6).moveTo(45, 111.1).lineTo(54, 111.1).moveTo(45, 120.6).lineTo(54, 120.6).moveTo(45, 130.1).lineTo(54, 130.1)
                .moveTo(45, 139.7).lineTo(54, 139.7);
            if (col > 9) {
                xpos = 193
            } else if (col > 4) {
                xpos = 180
            } else {
                xpos = 0
            }
            greenSquareLine_2.setTransform(xpos + (col * 14.5), 0);
            ToBeAdded.push(greenSquareLine_2)
        }

        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 2; col++) {
                var yellowSquareLine_3 = new cjs.Shape();
                if (row == 0 && col == 1) {
                    continue;
                }
                yellowSquareLine_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(143, 125.1).lineTo(143, 134).lineTo(151, 134).lineTo(151, 125.1).lineTo(143, 125.1)
                yellowSquareLine_3.setTransform(30+ (col * 13), 0 + (row * 13));
                ToBeAdded.push(yellowSquareLine_3)
            }
        }





        var tmpNum = "";
        var xpos = 75;
        var tmpxpos = 85;
        var fntSize = ""
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 4; col++) {
                if ((col == 0 || col == 2) && row == 0) {
                    tmpNum = "tiotal";
                    fntSize = "14px 'Myriad Pro'"
                }
                if ((col == 1 || col == 3) && row == 0) {
                    tmpNum = "ental";
                    fntSize = "14px 'Myriad Pro'"
                }
                if (col == 0 && row == 1) {
                    tmpNum = "5";
                    fntSize = "17px 'MyriadPro-Semibold'"
                }
                if (col == 1 && row == 1) {
                    tmpNum = " 3";
                    fntSize = "17px 'MyriadPro-Semibold'"
                }
                if (col == 2 && row == 1) {
                    tmpNum = "6";
                    fntSize = "17px 'MyriadPro-Semibold'"
                }
                if (col == 3 && row == 1) {
                    tmpNum = "0";
                    fntSize = "17px 'MyriadPro-Semibold'"
                }
                if (col == 1) {
                    xpos = 83
                } else if (col == 3) {
                    xpos = 167
                } else if (col > 1) {
                    xpos = 160
                } else if (row == 1 ) {
                    xpos = 80;
                    tmpxpos = 88
                } else {
                    xpos = 75;
                    tmpxpos = 85
                }
                var tempText = new cjs.Text(tmpNum, fntSize);
                tempText.setTransform(xpos + (col * tmpxpos), 46 + (row * 123));
                ToBeAdded.push(tempText);
            }
        }

        var tmptxt = ""
        for (var col = 0; col < 2; col++) {
            if (col == 0) {
                tmptxt = "femtiotre"
            } else {
                tmptxt = "sextio"
            }
            var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
            tempText.setTransform(100 + (col * 262), 198);
            ToBeAdded.push(tempText);
        }
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.hrLine, this.hrLine1, this.vrLine1, this.Line_3)
        this.addChild(this.hrLine2, this.hrLine3, this.vrLine2)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 530.3, 123.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Skriv talet.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 510, 337, 10);
        this.roundRect1.setTransform(0, 10);

        var ToBeAdded = [];

        for (var row = 0; row < 2; row++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(7, 122 + (row * 111)).lineTo(505, 122 + (row * 111));
            ToBeAdded.push(hrLine)
        };

        var moveToypos = 13;
        var lineToypos = 118;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 1; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 125;
                    lineToypos = 229;
                } else if (row == 2) {
                    moveToypos = 236;
                    lineToypos = 343;
                } else {
                    moveToypos = 13;
                    lineToypos = 118;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(255 + (col * 170), moveToypos).lineTo(255 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }

        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 16; col++) {
                var xPos = 0;
                if (col > 9 && row == 0) {
                    xPos = 193;
                } else if (col > 4 && row == 0) {
                    xPos = 180;
                } else if (col > 5 && row == 0) {
                    xPos = 105
                } else if (col > 11 && row == 1) {
                    xPos = 163
                } else if (col > 6 && row == 1) {
                    xPos = 150
                } else if (col > 4 && row == 1) {
                    xPos = 13
                } else if (col > 12 && row == 2) {
                    xPos = 148
                } else if (col > 7 && row == 2) {
                    xPos = 135
                } else if (col > 4 && row == 2) {
                    xPos = 13
                }

                if ((row == 0 && col > 10) || (row == 1 && col > 13)) {
                    continue;
                }
                var greenSquareLine_2 = new cjs.Shape();
                greenSquareLine_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(25, 20).lineTo(25, 108).lineTo(34, 108).lineTo(34, 20).lineTo(25, 20)
                    .moveTo(25, 28.6).lineTo(34, 28.6).moveTo(25, 37.1).lineTo(34, 37.1).moveTo(25, 45.6).lineTo(34, 45.6).moveTo(25, 54.1).lineTo(34, 54.1)
                    .moveTo(25, 62.6).lineTo(34, 62.6).moveTo(25, 71.1).lineTo(34, 71.1).moveTo(25, 80.6).lineTo(34, 80.6).moveTo(25, 90.1).lineTo(34, 90.1)
                    .moveTo(25, 99.7).lineTo(34, 99.7);
                greenSquareLine_2.setTransform(xPos + (col * 15), 0 + (row * 112));
                ToBeAdded.push(greenSquareLine_2)
            }
        }
        //box 1 yellow boxes
        //box 1 yellow boxes
        var xpos = 97
        var ypos = 3;
        for (var numOfrow = 0; numOfrow < 3; numOfrow++) {
            for (var row = 0; row < 5; row++) {
                for (var col = 0; col < 3; col++) {
                    var yellowSquareLine_3 = new cjs.Shape();
                    if (numOfrow == 1) {
                        ypos = 115
                    } else if (numOfrow == 2) {
                        ypos = 227
                    } else {
                        ypos = 3
                    }
                    if (numOfrow == 0 && col > 0) {
                        xpos = 365
                    } else if (numOfrow == 1) {
                        xpos = 393
                    } else if (numOfrow == 2 && col >= 0) {
                        xpos = 408
                    } else {
                        xpos = 97
                    }


                    if (numOfrow == 2 && col > 1) {
                        continue
                    } else if (numOfrow == 2 && row < 1 && col == 1) {
                        continue
                    } else if (numOfrow == 1 && col > 0) {
                        continue;
                    } else if (numOfrow == 1 && row < 2) {
                        continue
                    } else if (numOfrow == 0 && col == 0 && row < 2) {
                        continue
                    } else if (numOfrow == 0 && col == 2 && row < 1) {
                        continue
                    }

                    yellowSquareLine_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(6, 48.1).lineTo(6, 57).lineTo(14, 57).lineTo(14, 48.1).lineTo(6, 48.1)
                    yellowSquareLine_3.setTransform(xpos + (col * 14), ypos + (row * 12));
                    ToBeAdded.push(yellowSquareLine_3)
                }
            }
        }


        //Rectangle box and text
        var xpos = 188;
        var ypos = 85;
        var tmptxt = ""
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 4; column++) {
                var columnSpace = column;
                if (column > 1) {
                    xpos = 398
                } else {
                    xpos = 188
                }
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 112), 21, 23);
                if (column == 1 || column == 3 || column == 5) {
                    tmptxt = 'e'
                } else {
                    tmptxt = 't'
                }
                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform((xpos + 5) + (columnSpace * 22), (ypos - 5) + (row * 112));
                ToBeAdded.push(tmp_Rect, tempText);
            }
        }

        this.addChild(this.text_q1, this.text, this.roundRect1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 340);

    // stage content: 
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(299, 272, 1, 1, 0, 0, 0, 256.3, 217.9);
        this.v2 = new lib.Symbol6();
        this.v2.setTransform(299, 505, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
