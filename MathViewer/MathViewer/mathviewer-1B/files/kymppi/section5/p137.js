(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

            src: "images/p137_1.png",
            id: "p137_1"
        }, {
            src: "images/p137_2.png",
            id: "p137_2"
        }]
    };

    (lib.p137_1 = function() {
        this.initialize(img.p137_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p137_2 = function() {
        this.initialize(img.p137_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text = new cjs.Text("137", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Hur många guldmynt finns kvar?", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_2.setTransform(16, 139);

        this.text_3 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_3.setTransform(37, 139);

        this.text_4 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_4.setTransform(57, 139);

        this.text_5 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_5.setTransform(77, 139);

        this.text_6 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_6.setTransform(98, 139);

        this.instance = new lib.p137_1();
        this.instance.setTransform(4, 20, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 252, 132, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 154);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(261, 15);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(261, 154);

        this.rectgroup = new cjs.Container();

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#878787').drawRoundRect(0, 0, 140, 24, 0);
        this.Rect1.setTransform(0, 0);
        this.rectgroup.addChild(this.Rect1);

        var startX = 0,
            startY = 0;
        var colspace = 140 / 7,
            rowspace = 0;
        var cols = 7,
            rows = 1;

        for (var col = 0; col < cols; col++) {
            for (var row = 0; row < rows; row++) {
                var vrLine_1 = new cjs.Shape();
                vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 24);
                vrLine_1.setTransform(startX + (colspace * col), startY + (rowspace * row));

                this.rectgroup.addChild(vrLine_1);
            }
        }
        this.rectgroup.setTransform(17, 116);

        this.rectgroup2 = this.rectgroup.clone(true);
        this.rectgroup2.setTransform(280, 116);

        this.rectgroup3 = this.rectgroup.clone(true);
        this.rectgroup3.setTransform(17, 255);

        this.rectgroup4 = this.rectgroup.clone(true);
        this.rectgroup4.setTransform(280, 255);

        var TArr = ['36', 'guld-', 'mynt', '29', 'guld-', 'mynt', '48', 'guld-', 'mynt', '57', 'guld-', 'mynt'];
        var TxtArr = [];
        var TxtlineX = [39, 64, 64, 39, 64, 64, 294, 319, 319, 294, 319, 319];
        var TxtlineY = [89, 79, 93, 229, 220, 233, 89, 79, 93, 229, 220, 233];

        for (var i = 0; i < TxtlineX.length; i++) {
            if (i == 0 | i == 3 || i == 6 || i == 9) {
                this.temp_label = new cjs.Text(TArr[i], "18px 'Myriad Pro'", "#000000");
            } else {
                this.temp_label = new cjs.Text(TArr[i], "14px 'Myriad Pro'", "#000000");
            }

            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(this.temp_label);
        }

        var TxArr = ['Jag tar', '4 mynt.', 'Jag tar', '4 mynt.', 'Jag tar-', '7 mynt.', 'Jag tar', '7 mynt.'];
        var TArr = [];
        var TxlineX = [163, 160, 423, 420, 163, 160, 423, 420];
        var TxlineY = [45, 63, 45, 63, 185, 203, 185, 203];

        for (var i = 0; i < TxlineX.length; i++) {

            this.temp_label1 = new cjs.Text(TxArr[i], "16px 'Myriad Pro'", "#000000");
            this.temp_label1.setTransform(TxlineX[i], TxlineY[i]);
            TArr.push(this.temp_label1);
        }

        this.addChild(this.text, this.text_1, this.instance, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,
            this.rectgroup, this.rectgroup2, this.rectgroup3, this.rectgroup4, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }
        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 513.3, 285);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Manuels och Miras kort är lika mycket värda.", "16px 'Myriad Pro'");
        this.text.setTransform(14, 0);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#08B5DF");
        this.text_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text_2.setTransform(0, 22);

        this.instance = new lib.p137_2();
        this.instance.setTransform(45, 48, 0.46, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 252, 93, 10);
        this.roundRect1.setTransform(0, 36);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(261, 36);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 135);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(261, 135);

        var TxtBoxArr = [];
        var boxcount = 24;
        var TxtBox_X = [29, 56, 83, 140, 167, 194, 283, 310, 337, 395, 422, 449,
            29, 56, 83, 140, 167, 194, 283, 310, 337, 395, 422, 449
        ];
        var TxtBox_Y = [95, 92, 95, 95, 92, 95, 95, 92, 95, 95, 92, 95,
            189, 186, 189, 189, 186, 189, 189, 186, 189, 189, 186, 189
        ];
        for (var col = 0; col < boxcount; col++) {

            var boxfillcolor = "#FFFFFF";
            var boxborder = "#706F6F";
            var boxheight = 22,
                boxwidth = 20.5;

            if (col == 5 || col == 11 || col == 17 || col == 23) {
                boxfillcolor = "#FFFFFF";
                boxborder = "#706F6F";
                boxheight = 22, boxwidth = 33;
            } else if (col > 17) {
                boxfillcolor = "#FBD3B9";
                boxborder = "#EB5B1B";
                boxheight = 22;
                boxwidth = 20.5;
            } else if (col > 11) {
                boxfillcolor = "#C4E5F0";
                boxborder = "#00A3C4";
                boxheight = 22;
                boxwidth = 20.5;
            } else if (col > 5) {
                boxfillcolor = "#E1EBC5";
                boxborder = "#90BD22";
                boxheight = 22;
                boxwidth = 20.5;
            } else if (col < 5) {
                boxfillcolor = "#DFDDF0";
                boxborder = "#958FC5";
                boxheight = 22;
                boxwidth = 20.5;
            } else {
                boxfillcolor = "#FFFFFF";
                boxborder = "#706F6F";
                boxheight = 22;
                boxwidth = 20.5;
            }

            var TxtBox_1 = new cjs.Shape();
            TxtBox_1.graphics.f(boxfillcolor).s(boxborder).ss(0.7).drawRoundRect(0, 0, boxwidth, boxheight, 0);
            TxtBox_1.setTransform(TxtBox_X[col], TxtBox_Y[col]);
            TxtBoxArr.push(TxtBox_1);
        }

        var TxtArr = ['20', '30', '40', '10', '50', '10', '10', '70', '20', '30',
            '20', '50', '30', '10', '30', '40', '10', '50', '20', '10'
        ];
        var TxtlineX = [31, 58, 85, 142, 169, 285, 312, 339, 397, 424,
            31, 58, 85, 142, 169, 285, 312, 339, 397, 424
        ];
        var TxtlineY = [111, 108, 111, 111, 108, 111, 108, 111, 111, 108,
            205, 202, 205, 205, 202, 205, 202, 205, 205, 202
        ];
        var textBox_Nos = [];
        for (var i = 0; i < TxtlineX.length; i++) {
            var temp_label3 = new cjs.Text(TxtArr[i] + '', "16px 'Myriad Pro'", "#000000");
            temp_label3.setTransform(TxtlineX[i], TxtlineY[i]);
            textBox_Nos.push(temp_label3);
        }

        this.addChild(this.text, this.text_1, this.text_2, this.instance, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        for (var i = 0; i < TxtBoxArr.length; i++) {
            this.addChild(TxtBoxArr[i]);
        }
        for (var i = 0; i < textBox_Nos.length; i++) {
            this.addChild(textBox_Nos[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 529, 220);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(310, 107, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(310, 463, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
