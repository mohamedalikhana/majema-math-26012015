var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };

    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.Stage1 = function() {
        this.initialize();

        this.instrText = new cjs.Text("Vilket tal saknas?", "40px 'Myriad Pro'");
        this.instrText.lineHeight = 19;
        this.instrText.textAlign = 'center';
        this.instrText.setTransform(550 + 120, -40);

        this.addBall = new lib.insertBall_1(5);

        this.answers = new lib.answers();
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(285 + 115, 0 + 100, 3, 3, 0, 0, 0, 0, 0);

        this.textbox_group1 = new cjs.Shape();
        this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(1).drawRect(536 + 115, 171 + 100, 60, 70);
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.addBall, this.instrText, this.textbox_group1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage1 = new lib.Stage1();

        this.stage1.answers.text_3.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.stage1.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;

        this.addChild(this.stage1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();
        this.stage2 = new lib.Stage2();

        this.answers = new lib.answers();
        this.answers.text_10.visible = true;
        this.answers.text_9.visible = true;
        this.answers.text_7.visible = true;
        this.answers.text_6.visible = true;
        this.answers.setTransform(285 + 115, 90 + 100, 3, 3, 0, 0, 0, 0, 0);

        this.textbox_group1 = new cjs.Shape();
        this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(1).drawRect(536 + 115, 276 + 100, 60, 70);
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.stage2, this.textbox_group1, this.answers);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();

        this.stage3 = new lib.Stage3();

        this.stage3.answers.text_8.visible = true;

        this.tweens = [];

        this.tweens.push({
            ref: this.stage3.answers.text_8,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;

        this.addChild(this.stage3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talkamrater - Studerar", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("36", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.insertBall_1 = function(count) {
        this.initialize();
        var ball = new lib.c2_ex1_2();

        var ToBeAdded = [];
        for (var column = 0; column < count; column++) {
            var columnSpace = column;
            var redball = ball.clone(true);
            redball.setTransform(300 + (columnSpace * 150), 65);
            ToBeAdded.push(redball);
        }

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("1", "26px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(35, 55);

        this.text_2 = new cjs.Text("+", "26px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(60, 55);

        this.text_3 = new cjs.Text("4", "26px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(85, 55);

        this.text_4 = new cjs.Text("=", "26px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(110, 55);

        this.text_5 = new cjs.Text("5", "26px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(135, 55);

        this.text_6 = new cjs.Text("3", "26px 'Myriad Pro'");
        this.text_6.visible = false;
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(35, 60);

        this.text_7 = new cjs.Text("+", "26px 'Myriad Pro'");
        this.text_7.visible = false;
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(60, 60);

        this.text_8 = new cjs.Text("2", "26px 'Myriad Pro'");
        this.text_8.visible = false;
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(85, 60);

        this.text_9 = new cjs.Text("=", "26px 'Myriad Pro'");
        this.text_9.visible = false;
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(110, 60);

        this.text_10 = new cjs.Text("5", "26px 'Myriad Pro'");
        this.text_10.visible = false;
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(135, 60);

        this.addChild(this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
