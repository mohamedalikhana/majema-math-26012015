(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p47_1.png",
            id: "p47_1"
        }, {
            src: "images/p47_2.png",
            id: "p47_2"
        }, {
            src: "images/p47_3.png",
            id: "p47_3"
        }, {
            src: "images/p47_4.png",
            id: "p47_4"
        }]
    };

    (lib.p47_1 = function() {
        this.initialize(img.p47_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p47_2 = function() {
        this.initialize(img.p47_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p47_3 = function() {
        this.initialize(img.p47_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p47_4 = function() {
        this.initialize(img.p47_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol4 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("47", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);


        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);


        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Rita färdigt bilden så att den blir symmetrisk.", "16px 'Myriad Pro'");
        this.text_1.setTransform(1.3, 0);

        this.instance = new lib.p47_1();
        this.instance.setTransform(0, 20, 0.46, 0.46);
// horizontal dashed line
        var ToBeAdded = [];
        for (var col = 0; col < 38; col++) {
            var colSpace = col;
            var Line_1 = new cjs.Shape();
            Line_1.graphics.beginStroke("#706F6F").setStrokeStyle(0.7).moveTo(180, 160).lineTo(184, 160);
            Line_1.setTransform(52 + (colSpace * 7), 0);
            ToBeAdded.push(Line_1);
        }

// vertical dashed line
        for (var col = 0; col < 32; col++) {
            var colSpace = col;
            var VRLine_1 = new cjs.Shape();
            VRLine_1.graphics.beginStroke("#706F6F").setStrokeStyle(0.7).moveTo(57, 7).lineTo(57, 11);
            VRLine_1.setTransform(52, 10 + (colSpace * 7));
            ToBeAdded.push(VRLine_1);
        }

        this.addChild(this.instance, this.text_1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 246);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Måla färdigt bilden", "16px 'Myriad Pro'");
        this.text_1.setTransform(1.3, 7.6);

        this.text_2 = new cjs.Text("så att den blir symmetrisk.", "16px 'Myriad Pro'");
        this.text_2.setTransform(1.3, 29);

        this.text_3 = new cjs.Text("Måla en egen symmetrisk bild.", "16px 'Myriad Pro'");
        this.text_3.setTransform(265, 30);

        //draw rectangle boxes
        this.textbox_group1 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var col = 0; col < 8; col++) {
            for (var row = 0; row < 6; row++) {

                this.textbox_group1.graphics.s("#000000").ss(0.3).drawRect((1 + Xpos) + (col * 29), (57 + Ypos) + (row * 34), 29, 34);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        var Ypos = 0;
        var Xpos = 0;
        for (var col = 0; col < 8; col++) {
            for (var row = 0; row < 6; row++) {

                this.textbox_group2.graphics.s("#000000").ss(0.3).drawRect((265 + Xpos) + (col * 29), (57 + Ypos) + (row * 34), 29, 34);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        var arrVal = ['28', '57.6', '86.5'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var xVal = parseInt(arrVal[index]);

            this.instanceRed = new lib.p47_2();
            this.instanceRed.setTransform(xVal, 191.5, 0.45, 0.425);
            ToBeAdded.push(this.instanceRed);
        }

        var arrVal1 = ['59', '88.5'];
        for (var index = 0; index < arrVal1.length; index++) {
            var xVal = parseInt(arrVal1[index]);
            this.instancegreen = new lib.p47_3();
            this.instancegreen.setTransform(xVal, 124.2, 0.44, 0.425);
            ToBeAdded.push(this.instancegreen);
        }

        this.instanceblue = new lib.p47_4();
        this.instanceblue.setTransform(86.5, 88.7, 0.44, 0.425);

        this.instanceblue1 = new lib.p47_4();
        this.instanceblue1.setTransform(86.5, 156.7, 0.44, 0.425);

// Draw Two vertical  dashed lines
        var numberOfLines = ['107', '371'];
        var ypos = 35;
        for (var indexLine = 0; indexLine < numberOfLines.length; indexLine++) {
            for (var col = 0; col < 34; col++) {
                var colSpace = col;
                var Line_3 = new cjs.Shape();
                Line_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.7).moveTo(10, ypos).lineTo(10, ypos + (4));
                Line_3.setTransform(numberOfLines[indexLine], 9 + (colSpace * 7));
                ToBeAdded.push(Line_3);
            }
        }


        this.addChild(this.text_1, this.text_2, this.text_3, this.textbox_group1, this.textbox_group2, this.instanceblue, this.instanceblue1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 265);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310.8, 245, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(310.8, 527, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
