(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p79_1.png",
            id: "p79_1"
        }, {
            src: "images/p79_2.png",
            id: "p79_2"
        }, {
            src: "images/p79_3.png",
            id: "p79_3"
        }]
    };

    (lib.p79_1 = function() {
        this.initialize(img.p79_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p79_2 = function() {
        this.initialize(img.p79_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p79_3 = function() {
        this.initialize(img.p79_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text_2 = new cjs.Text("79", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(553, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_3 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#958FC5");
        this.text_3.setTransform(118, 42);

        this.text_4 = new cjs.Text("27", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(65, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#958FC5").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(52, 23.5, 1.2, 1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 148, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 31, 48, 81, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p79_1();
        this.instance.setTransform(503, 35, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Subtrahera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(23, 20);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.setTransform(4, 20);

        var ToBeAdded = [];
        var tmpNum = ["11  –  4  =", "12  –  3  =", "13  –  5  =", "14  –  7  =", "15  –  8  =", "16  –  7  =", "17  –  8  =", "18  –  9  =", "12  –  5  –  5  =", "14  –  8  –  3  =", "16  –  9  –  4  =", "17  –  8  –  8  ="]
        var i = 0
        var tmprow = 29;
        var tmpcol = 155
        var temp_Line1 = new cjs.Shape();
        for (var col = 0; col < 3; col++) {
            columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                tempText.setTransform(20 + (155 * columnSpace), 47 + (29 * rowSpace));
                if (col == 2) {
                    tmpcol = 170
                } else {
                    tmpcol = 155
                }
                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(87 + (columnSpace * tmpcol), 52 + (rowSpace * tmprow)).lineTo(120 + (columnSpace * tmpcol), 52 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)
                i++;

            };

        };




        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 551.3, 143.6);
    // p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 127, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 20, 48, 83, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p79_1();
        this.instance.setTransform(503, 25, 0.4, 0.4);

        this.text_q1 = new cjs.Text("Fortsätt talföljden. Skriv regeln.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(23, 20);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.setTransform(4, 20);

        var ToBeAdded = [];
        var tmpNum = ['0', '5', '10', '', '', '20', '18', '16']
        var i = 0
        var xpos = 20;
        var txtXpos = 30
            // Set number in box.
        for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
            for (var col = 0; col < 5; col++) {
                var columnSpace = col
                if (numOfcol == 1) {
                    xpos = 265;
                    txtXpos = 272
                } else {
                    xpos = 20;
                    txtXpos = 30
                }
                var tmp_Rect = new cjs.Shape();
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 45), 50, 28, 24);
                var tempText = new cjs.Text(tmpNum[i], "16px 'Myriad Pro'");
                if (col == 2 && numOfcol == 0) {
                    txtXpos = txtXpos - 5
                }
                tempText.setTransform(txtXpos + (45 * columnSpace), 67);
                ToBeAdded.push(tmp_Rect, tempText);
                i++
            }
        }

        for (var col = 0; col < 2; col++) {
            columnSpace = col;
            var tempText = new cjs.Text("Regel:", "12px 'Myriad Pro'");
            tempText.setTransform(64 + (246 * columnSpace), 102);
            var temp_Line1 = new cjs.Shape();
            temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(98 + (columnSpace * 246), 102).lineTo(183 + (columnSpace * 246), 102);
            ToBeAdded.push(tempText, temp_Line1)
        };
        //Arrow Shape Loop column 1
        this.shape_group2 = new cjs.Shape();
        this.hrLine_1 = new cjs.Shape();
        var arcYpos = 53.5
        var arrowYpos = 41.7
        var xpos = 58
        var arrowxpos = 67.5
        var arrowxpos1 = 63.5
        for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
            for (var col = 0; col < 4; col++) {
                var colSpace = col;
                if (numOfcol == 1) {
                    xpos = 302;
                    arrowxpos = 311.5;
                    arrowxpos1 = 306.5
                } else {
                    xpos = 58;
                    arrowxpos = 67.5;
                    arrowxpos1 = 62.5
                }
                this.shape_group2.graphics.f("#FFFFFF").s("#000000").ss(0.4, 0, 0, 4).arc(xpos + (colSpace * 45), arcYpos, 32, 4.3, 4.99)
                this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(arrowxpos + (colSpace * 45), arrowYpos).lineTo(arrowxpos + (colSpace * 45), (arrowYpos + 5)).lineTo(arrowxpos1 + (colSpace * 45), (arrowYpos + 5))
            }
        }
        this.shape_group2.setTransform(0, 0, 1, 2);
        this.hrLine_1.setTransform(0, 0);


        this.addChild(this.roundRect2, this.instance, this.roundRect1, this.text_q1, this.text_q2, this.hrLine_1, this.shape_group2);;
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 551.3, 143.6);

    (lib.Symbol4 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 145, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 35, 48, 82, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p79_1();
        this.instance.setTransform(503, 40, 0.4, 0.4);

        this.instance_2 = new lib.p79_2();
        this.instance_2.setTransform(20, 27, 0.46, 0.46);

        this.text_q1 = new cjs.Text("Hur många körsbär finns kvar?", "16px 'Myriad Pro'");
        this.text_q1.setTransform(23, 20);

        this.text_q2 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q2.setTransform(4, 20);

        this.Text_1 = new cjs.Text("14", "16px 'Myriad Pro'");
        this.Text_1.setTransform(27, 80);

        this.Text_2 = new cjs.Text("16", "16px 'Myriad Pro'");
        this.Text_2.setTransform(277, 82);

        this.vrLine = new cjs.Shape();
        this.vrLine.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(250, 45).lineTo(250, 135)
        this.vrLine.setTransform(0, 0);
        var ToBeAdded = [];
        var xpos = 43;
        var ypos = 105;
        var tmp_Rect = new cjs.Shape();
        for (var numOfcol = 0; numOfcol < 2; numOfcol++) {

            for (var column = 0; column < 6; column++) {
                var columnSpace = column;
                if (numOfcol == 1) {
                    xpos = 310
                } else {
                    xpos = 43
                }

                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos, 21, 23);
                ToBeAdded.push(tmp_Rect);
            }
        }

        var tmptxt = ""
        var ypos = 60
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            if (column == 0) {
                tmptxt = "Jag tar 6 körsbär."
            } else {
                tmptxt = "Jag tar 8 körsbär."
            }
            var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
            if (column == 1) {
                ypos = 51
            } else {
                ypos = 60
            }
            tempText.setTransform(99 + (250 * colSpace), ypos);
            ToBeAdded.push(tempText);
        }


        this.addChild(this.roundRect2, this.roundRect1, this.instance_2, this.instance, this.text_q1, this.text_q2, this.vrLine, this.Text_1, this.Text_2);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 551.3, 143.6);


    (lib.Symbol5 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 499, 140, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(480, 35, 48, 82, 10);
        this.roundRect2.setTransform(0, 0);

        this.instance = new lib.p79_1();
        this.instance.setTransform(503, 40, 0.4, 0.4);

        this.instance_2 = new lib.p79_3();
        this.instance_2.setTransform(10, 30, 0.47, 0.46);

        this.text_q1 = new cjs.Text("Hur många poäng fick Mira?", "16px 'Myriad Pro'");

        this.text_q1.setTransform(23, 20);

        this.text_q2 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");

        this.text_q2.setTransform(4, 20);

        this.vrLine = new cjs.Shape();
        this.vrLine.graphics.beginStroke("#000000").setStrokeStyle(0.7).moveTo(250, 33).lineTo(250, 126)
        this.vrLine.setTransform(0, 0);

        var ToBeAdded = [];
        var xpos = 56;
        var ypos = 98;
        var tmp_Rect = new cjs.Shape();
        for (var numOfcol = 0; numOfcol < 2; numOfcol++) {

            for (var column = 0; column < 6; column++) {
                var columnSpace = column;
                if (numOfcol == 1) {
                    xpos = 310
                } else {
                    xpos = 56
                }

                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 19.5), ypos, 20, 22);
                ToBeAdded.push(tmp_Rect);
            }
        }
        var tmpValtop = "Jag fick";
        var tmpValbottom = "";
        var ypos = 55;
        var xposTop = 27;
        var xposBotm = 20;
        var tmpxpos = 145


        for (var column = 0; column < 4; column++) {
            var colSpace = column;
            var tempText = new cjs.Text(tmpValtop, "16px 'Myriad Pro'");
            if (column == 1) {
                xposTop = 40;
                ypos = 53
            } else if (column == 3) {
                xposTop = 25;
                ypos = 53
            } else {
                xposTop = 30;
                ypos = 55
            }
            tempText.setTransform(xposTop + (125 * colSpace), ypos);
            if (column == 0) {
                tmpValbottom = "13 poäng."
            } else if (column == 1) {
                tmpValbottom = "6 färre."
            } else if (column == 2) {
                tmpValbottom = "8 poäng."
            } else if (column == 3) {
                tmpValbottom = "6 fler."
            }
            var tempText1 = new cjs.Text(tmpValbottom, "16px 'Myriad Pro'");
            if (column == 1) {
                xposBotm = 22
            } else if (column == 2) {
                tmpxpos = 127
            } else if (column == 3) {
                tmpxpos = 130
            } else {
                xposBotm = 20;
                tmpxpos = 145
            }
            tempText1.setTransform(xposBotm + (tmpxpos * colSpace), (ypos + 17));
            ToBeAdded.push(tempText, tempText1);

        }
        this.addChild(this.roundRect2, this.roundRect1, this.instance, this.instance_2, this.text_q1, this.text_q2, this.vrLine)


        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 551.3, 143.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(312, 109, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(312, 264, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(312, 398, 0.98, 0.98, 0, 0, 0, 254.6, 53.4);

        this.v4 = new lib.Symbol5();
        this.v4.setTransform(312, 550, 0.98, 0.98, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
