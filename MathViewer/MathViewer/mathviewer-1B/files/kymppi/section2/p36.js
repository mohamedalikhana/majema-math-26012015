(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p36_1.png",
            id: "p36_1"
        }, {
            src: "images/p36_2.png",
            id: "p36_2"
        }, {
            src: "images/p36_3.png",
            id: "p36_3"
        }, {
            src: "images/p36_4.png",
            id: "p36_4"
        }, {
            src: "images/p36_5.png",
            id: "p36_5"
        }, {
            src: "images/p36_6.png",
            id: "p36_6"
        }]
    };

    // symbols:

    (lib.p36_1 = function() {
        this.initialize(img.p36_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p36_2 = function() {
        this.initialize(img.p36_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p36_3 = function() {
        this.initialize(img.p36_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p36_4 = function() {
        this.initialize(img.p36_4);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p36_5 = function() {
        this.initialize(img.p36_5);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.p36_6 = function() {
        this.initialize(img.p36_6);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("36", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(42, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.5).moveTo(30, 0).lineTo(475, 0);
        this.Line_1.setTransform(10, 20);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 55;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 65;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 75;
                } else {
                    fillcolor = "#008BD2";
                    xpos = 85;
                }

                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 20 + (row * 19), 7.9, 0, 2 * Math.PI);

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(132, 23);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_3.setTransform(238, 23);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#ffffff");
        this.text_4.setTransform(349, 23);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#ffffff");
        this.text_5.setTransform(458.5, 23);

        // Layer 1
        this.text = new cjs.Text("Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 50);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(10, 50);

        this.instance = new lib.p36_1();
        this.instance.setTransform(362, 55, 0.46, 0.46);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 505, 130, 10);
        this.roundRect1.setTransform(10, 60);

        var tmptextArr = ['3', '2', '5', '4', '6', '8', '7', '9'];
        var ToBeAdded = [];
        var i = 0;
        this.hrLine_1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (i < tmptextArr.length) {
                    var tempText = new cjs.Text("9 " + " + " + tmptextArr[i] + " = ", "16px 'Myriad Pro'");
                    tempText.setTransform(68 + (160 * colSpace), 86 + (28 * rowSpace));
                    this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(118 + (160 * colSpace), 90 + (28 * rowSpace)).lineTo(165 + (160 * colSpace), 90 + (28 * rowSpace));
                    this.hrLine_1.setTransform(0, 0);
                    ToBeAdded.push(tempText, this.hrLine_1);
                    i++;

                }

            }
        }



        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5);
        this.addChild(this.hrLine_1, this.hrLine_2, this.hrLine_3);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 107.1);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 505, 130, 10);
        this.roundRect1.setTransform(10, 20);

        this.text = new cjs.Text("Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 10);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(10, 10);

        this.instance = new lib.p36_2();
        this.instance.setTransform(363, 40, 0.46, 0.46);


        var tmptextArr = ['3', '5', '8', '10', '4', '7', '6', '9'];
        var ToBeAdded = [];
        var i = 0;
        this.hrLine_1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (i < tmptextArr.length) {
                    var tempText = new cjs.Text("8 " + " +  " + tmptextArr[i] + "  = ", "16px 'Myriad Pro'");
                    if (i == 3) {
                        var tempText = new cjs.Text("8 " + " + " + tmptextArr[i] + " = ", "16px 'Myriad Pro'");
                    }
                    tempText.setTransform(68 + (160 * colSpace), 45 + (28 * rowSpace));
                    this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(127 + (160 * colSpace), 46 + (28 * rowSpace)).lineTo(173 + (160 * colSpace), 46 + (28 * rowSpace));
                    this.hrLine_1.setTransform(0, 0);
                    ToBeAdded.push(tempText, this.hrLine_1);
                    i++;

                }

            }
        }


        this.addChild(this.roundRect1, this.text, this.text_1, this.instance, this.hrLine_1, this.hrLine_2, this.hrLine_3);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 70);

    (lib.Symbol4 = function() {
        this.initialize();



        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(10, 20, 250, 207, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(256, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#FBBD99').drawRoundRect(23, 28, 170, 120, 10);
        this.roundRect3.setTransform(25, 5);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s('#FBBD99').drawRoundRect(23, 28, 170, 120, 10);
        this.roundRect4.setTransform(278, 5);

        this.text = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.setTransform(29, 10);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(10, 10);

        this.instance = new lib.p36_3();
        this.instance.setTransform(313, 35, 0.46, 0.46);

        this.instance1 = new lib.p36_4();
        this.instance1.setTransform(108, 35, 0.46, 0.46);

        this.instance2 = new lib.p36_5();
        this.instance2.setTransform(282, 155, 0.46, 0.46);

        this.instance3 = new lib.p36_6();
        this.instance3.setTransform(40, 155, 0.46, 0.46);

        this.hrLine_2 = new cjs.Shape();
        var ToBeAdded = [];
        var i = 0;
        var tmptextArr1 = ['9', '8', '9', '8'];
        var tmptextArr2 = ['13', '15', '15', '13'];
        for (var row = 0; row < 4; row++) {
            var rowSpace = row;
            if (i < tmptextArr1.length) {
                var tempText1 = new cjs.Text(tmptextArr1[i] + "  +             =  " + tmptextArr2[i], "16px 'Myriad Pro'");
                tempText1.setTransform(20 + (20 * 3), 58 + (28 * rowSpace));
            }
            ToBeAdded.push(tempText1, this.hrLine_2);
            i++;
        };

        this.hrLine_3 = new cjs.Shape();

        var j = 0;
        var tmptextArr3 = ['9', '8', ' ', ' '];
        var tmptextArr4 = ['14', ' ', ' ', '18'];
        for (var row = 0; row < 4; row++) {
            var rowSpace = row;
            if (j < tmptextArr3.length) {
                var tempText2 = new cjs.Text(tmptextArr3[j] + "  +             =  " + tmptextArr4[j], "16px 'Myriad Pro'");
                tempText2.setTransform(250 + (30 * 3), 58 + (28 * rowSpace));
            }
            if (row == 2 || row == 3) {
                var tempText2 = new cjs.Text(tmptextArr3[j] + "   +             =  " + tmptextArr4[j], "16px 'Myriad Pro'");
                tempText2.setTransform(250.5 + (30 * 3), 58 + (28 * rowSpace));
            }

            ToBeAdded.push(tempText2, this.hrLine_3);
            j++;
        };

        var k = 0;
        this.hrLine_5 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 2; row++) {
                var rowSpace = row;
                if (i < 8) {
                    var tempText = new cjs.Text(" = ", "16px 'Myriad Pro'");
                    tempText.setTransform(72 + (95 * colSpace), 180 + (28 * rowSpace));
                    if (column == 1) {
                        this.hrLine_5.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(110 + (75 * colSpace), 180 + (28 * rowSpace)).lineTo(138 + (75 * colSpace), 180 + (28 * rowSpace));
                    } else {
                        this.hrLine_5.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(90 + (75 * colSpace), 180 + (28 * rowSpace)).lineTo(115 + (75 * colSpace), 180 + (28 * rowSpace));
                    }
                    this.hrLine_5.setTransform(0, 0);
                    ToBeAdded.push(tempText, this.hrLine_5);
                    k++;

                }

            }
        }

        var l = 0;
        this.hrLine_6 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 2; row++) {
                var rowSpace = row;
                if (l < 8) {
                    var tempText = new cjs.Text(" = ", "16px 'Myriad Pro'");
                    tempText.setTransform(315 + (115 * colSpace), 180 + (28 * rowSpace));
                    if (column == 1) {
                        this.hrLine_6.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(370 + (75 * colSpace), 180 + (28 * rowSpace)).lineTo(400 + (75 * colSpace), 180 + (28 * rowSpace));
                    } else {
                        this.hrLine_6.graphics.beginStroke("#818284").setStrokeStyle(0.6).moveTo(330 + (75 * colSpace), 180 + (28 * rowSpace)).lineTo(360 + (75 * colSpace), 180 + (28 * rowSpace));
                    }
                    this.hrLine_6.setTransform(0, 0);
                    ToBeAdded.push(tempText, this.hrLine_6);
                    l++;

                }

            }
        }


        this.addChild(this.text, this.text_1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4);
        this.addChild(this.instance, this.instance1, this.instance2, this.instance3);
        this.addChild(this.hrLine_1, this.hrLine_2, this.hrLine_3)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 250.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(292.3, 88, 1, 1, 0, 0, 0, 254.6, 40);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(292.3, 288, 1, 1, 0, 0, 0, 255.8, 38);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(292.3, 450, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
