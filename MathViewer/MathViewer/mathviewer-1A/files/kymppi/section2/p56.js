(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p56_1.png",
            id: "p56_1"
        }, {
            src: "images/p56_2.png",
            id: "p56_2"
        }]
    };

    // symbols:
    (lib.p56_1 = function() {
        this.initialize(img.p56_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p56_2 = function() {
        this.initialize(img.p56_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("56", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(38, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#88C13F").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("Måla cirkeln om svaret stämmer.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 3);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#88C13F");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 3);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 8; row++) {
                if (row == 0 && column == 0) {
                    this.shape_group1.graphics.f("#C2C2C2").s("#000000").ss(0.5, 0, 0, 4).arc(120 + (columnSpace * 180), 105 + (row * 29), 11, 0, 2 * Math.PI);
                } else {
                    this.shape_group1.graphics.f("#fff").s("#000000").ss(0.5, 0, 0, 4).arc(120 + (columnSpace * 180), 105 + (row * 29), 11, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.Line_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 9; row++) {
                this.Line_group1.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(25 + (columnSpace * 180), 90 + (row * 29)).lineTo(150 + (columnSpace * 180), 90 + (row * 29));
            }
        }
        this.Line_group1.setTransform(0, 0);



        var arrVal = ['0', '1', '2'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            if (index == 2) {
                index = 2.01;
            }
            tempLabel.setTransform(80 + (index * 180), 61);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['1 – 1', '1 – 1', '3 – 2'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 98);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['1 – 0', '2 – 1', '3 – 1'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 128);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['2 – 0', '3 – 2', '2 – 2'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 155);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['2 – 2', '2 – 2', '4 – 2'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 185);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['3 – 3', '4 – 2', '4 – 1'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 213);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['4 – 0', '4 – 3', '5 – 3'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 245);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['4 – 3', '5 – 4', '4 – 4'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 273);
            ToBeAdded.push(tempLabel);
        }

        arrVal = []
        arrVal = ['4 – 4', '5 – 3', '2 – 0'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 180), 300);
            ToBeAdded.push(tempLabel);
        }

        this.instance = new lib.p56_2();
        this.instance.setTransform(1, 25, 0.24, 0.24);

        this.instance1 = new lib.p56_2();
        this.instance1.setTransform(181, 25, 0.24, 0.24);

        this.instance2 = new lib.p56_2();
        this.instance2.setTransform(363, 25, 0.24, 0.24);

        this.Line1 = new cjs.Shape();
        this.Line1.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(25, 90).lineTo(25, 322);
        this.Line1.setTransform(0, 0);

        this.Line2 = new cjs.Shape();
        this.Line2.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(150, 90).lineTo(150, 322);
        this.Line2.setTransform(0, 0);

        this.Line3 = new cjs.Shape();
        this.Line3.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(205, 90).lineTo(205, 322);
        this.Line3.setTransform(0, 0);

        this.Line4 = new cjs.Shape();
        this.Line4.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(330, 90).lineTo(330, 322);
        this.Line4.setTransform(0, 0);

        this.Line5 = new cjs.Shape();
        this.Line5.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(385, 90).lineTo(385, 322);
        this.Line5.setTransform(0, 0);

        this.Line6 = new cjs.Shape();
        this.Line6.graphics.beginStroke("#FC0404").f("#DF3333").setStrokeStyle(0.5).moveTo(510, 90).lineTo(510, 322);
        this.Line6.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FFFFFF').drawRoundRect(0, 0, ((2150 * 0.5) - 10) * 0.5, 630 * 0.5, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text, this.text_1, this.shape_group1, this.Line_group1);
        this.addChild(this.instance, this.instance1, this.instance2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

        this.addChild(this.Line1, this.Line2, this.Line3, this.Line4, this.Line5, this.Line6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p56_1();
        this.instance.setTransform(-12, 20, 0.2, 0.2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1037 * 0.5) - 10) * 0.5, 430 * 0.5, 10);
        this.roundRect1.setTransform(0, 20);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1037 * 0.5) + 10) * 0.5, 20);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#BBD992').drawRoundRect(30, 12, ((800 * 0.5) - 10) * 0.5, 230 * 0.5, 10);
        this.roundRect3.setTransform(0, 20);

        this.roundRect4 = this.roundRect3.clone(true);
        this.roundRect4.setTransform(((800 * 0.5) + 128) * 0.5, 20);

        this.text = new cjs.Text("Vad är svamparna värda?", "16px 'Myriad Pro'");
        this.text.lineHeight = 22;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#88C13F");
        this.text_1.lineHeight = 30;
        this.text_1.setTransform(0, 0);

        this.label1 = new cjs.Text("+              =     2", "18px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(88, 47);
        this.label2 = new cjs.Text("+              =     4", "18px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(88, 77);
        this.label3 = new cjs.Text("+              =     ", "18px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(88, 107);

        this.label4 = new cjs.Text("4", "18px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(320, 47);
        this.label5 = new cjs.Text("–", "18px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(351, 47);
        this.label6 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(412, 47);
        this.label7 = new cjs.Text("–", "18px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(351, 77);
        this.label8 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(412, 77);
        this.label9 = new cjs.Text("–", "18px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(351, 107);
        this.label10 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(412, 107);

        this.label11 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(58, 171);
        this.label12 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(165, 171);
        this.label13 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(58, 200);

        this.label14 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(335, 171);
        this.label15 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label15.lineHeight = 19;
        this.label15.setTransform(443, 171);
        this.label16 = new cjs.Text("=", "18px 'Myriad Pro'");
        this.label16.lineHeight = 19;
        this.label16.setTransform(335, 202);



        this.textbox1 = new cjs.Shape();
        this.textbox1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(77, 169, 22, 24);
        this.textbox1.setTransform(0, 0);
        this.textbox2 = new cjs.Shape();
        this.textbox2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(77, 199, 22, 24);
        this.textbox2.setTransform(0, 0);
        this.textbox3 = new cjs.Shape();
        this.textbox3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(185, 169, 22, 24);
        this.textbox3.setTransform(0, 0);

        this.textbox4 = new cjs.Shape();
        this.textbox4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(355, 169, 22, 24);
        this.textbox4.setTransform(0, 0);
        this.textbox5 = new cjs.Shape();
        this.textbox5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(461, 169, 22, 24);
        this.textbox5.setTransform(0, 0);
        this.textbox6 = new cjs.Shape();
        this.textbox6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(355, 201, 22, 24);
        this.textbox6.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.text,
            this.text_1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15, this.label16,
            this.textbox1, this.textbox2, this.textbox3, this.textbox4, this.textbox5, this.textbox6);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296.6, 438.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(291.6, 97, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
