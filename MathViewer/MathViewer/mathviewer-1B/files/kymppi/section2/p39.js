(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p39_1.png",
            id: "p39_1"
        }, {
            src: "images/p39_2.png",
            id: "p39_2"
        }]
    };

    // symbols:
    (lib.p39_1 = function() {
        this.initialize(img.p39_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p39_2 = function() {
        this.initialize(img.p39_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("39", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Byt ut figurerna mot tal. Addera.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 507, 230, 10);
        this.roundRect1.setTransform(0, 12);

        this.text_2 = new cjs.Text("= 9", "16px 'Myriad Pro'");
        this.text_2.setTransform(75, 47);

        this.text_3 = new cjs.Text("= 8", "16px 'Myriad Pro'");
        this.text_3.setTransform(162, 47);

        this.text_4 = new cjs.Text("= 7", "16px 'Myriad Pro'");
        this.text_4.setTransform(245, 47);

        this.text_5 = new cjs.Text("= 6", "16px 'Myriad Pro'");
        this.text_5.setTransform(328, 47);

        this.text_6 = new cjs.Text("= 5", "16px 'Myriad Pro'");
        this.text_6.setTransform(410, 47);

        this.text_7 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_7.setTransform(108, 90);

        this.text_8 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_8.setTransform(85, 173);

        this.text_9 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_9.setTransform(133, 173);

        this.text_10 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_10.setTransform(342, 173);

        this.text_11 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_11.setTransform(393, 173);

        this.text_12 = new cjs.Text("+", "16px 'Myriad Pro'");
        this.text_12.setTransform(367, 91);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.7).moveTo(20, 140).lineTo(460, 140);
        this.Line_1.setTransform(10, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.7).moveTo(230, 65).lineTo(230, 135);
        this.Line_2.setTransform(10, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.7).moveTo(230, 147).lineTo(230, 230);
        this.Line_3.setTransform(10, 0);

        this.instance = new lib.p39_1();
        this.instance.setTransform(45, 15, 0.46, 0.46);

        this.roundRect = new cjs.Shape()
        this.roundRect.graphics.f("#ffffff").s('#EB5B1B').drawRoundRect(25, 22, 450, 25, 1);
        this.roundRect.setTransform(0, 10);

        var arrTxtbox = ['106', '192'];
        this.textbox_group1 = new cjs.Shape();
        var xPos = 10,
            padding,
            yPos,
            rectWidth = 120,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 6,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 52.5;
                if (i == 1) {
                    padding = 90.5;

                }
                if (i == 1 && k == 1) {
                    numberOfBoxes = 8;
                    rectWidth = 160;
                    var rectStartPosX = 40;
                } else if (i == 0 && k == 1) {
                    numberOfBoxes = 8;
                    rectWidth = 160;
                    var rectStartPosX = 297;
                } else {
                    numberOfBoxes = 6;
                    rectWidth = 120;
                    var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                }
                this.textbox_group1.graphics.s("#707070").ss(0.6).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);

                for (var j = 1; j < numberOfBoxes; j++) {

                    this.textbox_group1.graphics.s("#707070").ss(0.6).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.roundRect);
        this.addChild(this.instance, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12)
        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 250);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p39_2();
        this.instance.setTransform(23, 37, 0.46, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(10, 25, 507, 259, 10);
        this.roundRect1.setTransform(0, 10);

        this.text = new cjs.Text("Mössorna finns i 4 färger. Halsdukarna finns i 3 färger.", "16px 'Myriad Pro'");
        this.text.setTransform(29, 0);

        this.text_2 = new cjs.Text("Måla alla olika sätt som Leo kan klä sig.", "16px 'Myriad Pro'");
        this.text_2.setTransform(29, 20);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.setTransform(10, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(306.5, 380.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(315.3, 120.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
