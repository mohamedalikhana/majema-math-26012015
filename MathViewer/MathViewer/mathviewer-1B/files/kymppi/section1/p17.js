(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p17_1.png",
            id: "p17_1"
        }]
    };

    (lib.p17_1 = function() {
        this.initialize(img.p17_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {

        this.text = new cjs.Text("17", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 658);


        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#90BD22").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);


        this.addChild(this.shape_1, this.text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1

        this.text = new cjs.Text("Skriv en talfamilj.", "16px 'Myriad Pro'");
        this.text.setTransform(21, 0);

        this.text_1 = new cjs.Text("7.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(2, 0);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(5, 0, 512, 295, 10);
        this.roundRect1.setTransform(0, 13);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 100).lineTo(500, 100);
        this.shape_group2.setTransform(0, 59);

        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f('#FFFFFF').s("#000000").setStrokeStyle(0.7).moveTo(20, 11).lineTo(20, 261);
        this.shape_group3.setTransform(240, 25);


        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 6; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 5 && row == 5) {
                    continue;
                }
                if (column < 3) {
                    this.textbox_group1.graphics.f('#E1EBC5').s("#90BD22").ss(0.8).drawRect(80 + (columnSpace * 40), 28, 28, 22);
                } else if (column < 6) {
                    this.textbox_group1.graphics.f('#E1EBC5').s("#90BD22").ss(0.8).drawRect(212 + (columnSpace * 40), 28, 28, 22);
                }
            }
        }
        this.textbox_group1.setTransform(0, 11);


        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 6; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 5 && row == 5) {
                    continue;
                }
                if (column < 3) {
                    this.textbox_group2.graphics.f('#E1EBC5').s("#90BD22").ss(0.8).drawRect(80 + (columnSpace * 40), 37, 28, 22);
                } else if (column < 6) {
                    this.textbox_group2.graphics.f('#E1EBC5').s("#90BD22").ss(0.8).drawRect(212 + (columnSpace * 40), 37, 28, 22);
                }
            }
        }
        this.textbox_group2.setTransform(0, 151);


        this.textbox_group3 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 5 && row == 5) {
                    continue;
                }
                if (column < 1) {
                    this.textbox_group3.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(20 + (columnSpace * 260), 20, 109, 23);
                } else if (column < 2) {
                    this.textbox_group3.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(97 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 3) {
                    this.textbox_group3.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(192 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 4) {
                    this.textbox_group3.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(270 + (columnSpace * 40), 20, 110, 23);
                }
            }
        }
        this.textbox_group3.setTransform(0, 61);


        var lineArr1 = [];
        var lineX = [-6, 10.5, 26, 41.5, 57.5, 74, 112, 129, 145, 161, 177, 193, 248, 263.5, 279.5, 295.5, 311.5, 327.5, 366, 381.5, 397.5, 413.5, 429.5, 445.5];
        var lineY = [26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26]
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 55).lineTo(40, 78);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr1.push(hrLine_1);
        }

        this.textbox_group4 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 5 && row == 5) {
                    continue;
                }
                if (column < 1) {
                    this.textbox_group4.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(20 + (columnSpace * 260), 20, 109, 23);
                } else if (column < 2) {
                    this.textbox_group4.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(97 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 3) {
                    this.textbox_group4.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(192 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 4) {
                    this.textbox_group4.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(270 + (columnSpace * 40), 20, 110, 23);
                }
            }
        }
        this.textbox_group4.setTransform(0, 94);

        var lineArr2 = [];
        var lineX = [-6, 10.5, 26, 41.5, 57.5, 74, 112, 129, 145, 161, 177, 193, 248, 263.5, 279.5, 295.5, 311.5, 327.5, 366, 381.5, 397.5, 413.5, 429.5, 445.5];
        var lineY = [59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59]
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 55).lineTo(40, 78);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr2.push(hrLine_1);
        }

        this.textbox_group5 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 5 && row == 5) {
                    continue;
                }
                if (column < 1) {
                    this.textbox_group5.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(20 + (columnSpace * 260), 20, 109, 23);
                } else if (column < 2) {
                    this.textbox_group5.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(97 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 3) {
                    this.textbox_group5.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(192 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 4) {
                    this.textbox_group5.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(270 + (columnSpace * 40), 20, 110, 23);
                }
            }
        }
        this.textbox_group5.setTransform(0, 211);

        var lineArr3 = [];
        var lineX = [-6, 10.5, 26, 41.5, 57.5, 74, 112, 129, 145, 161, 177, 193, 248, 263.5, 279.5, 295.5, 311.5, 327.5, 366, 381.5, 397.5, 413.5, 429.5, 445.5];
        var lineY = [176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 176]
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 55).lineTo(40, 78);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr3.push(hrLine_1);
        }


        this.textbox_group6 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 5 && row == 5) {
                    continue;
                }
                if (column < 1) {
                    this.textbox_group6.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(20 + (columnSpace * 260), 20, 109, 23);
                } else if (column < 2) {
                    this.textbox_group6.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(97 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 3) {
                    this.textbox_group6.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(192 + (columnSpace * 40), 20, 110, 23);
                } else if (column < 4) {
                    this.textbox_group6.graphics.f('').s("#6F6F6E").ss(0.8).drawRect(270 + (columnSpace * 40), 20, 110, 23);
                }
            }
        }
        this.textbox_group6.setTransform(0, 243);

        var lineArr4 = [];
        var lineX = [-6, 10.5, 26, 41.5, 57.5, 74, 112, 129, 145, 161, 177, 193, 248, 263.5, 279.5, 295.5, 311.5, 327.5, 366, 381.5, 397.5, 413.5, 429.5, 445.5];
        var lineY = [208, 208, 208, 208, 208, 208, 208, 208, 208, 208,208, 208, 208, 208, 208, 208, 208, 208, 208, 208, 208, 208, 208, 208, 208]
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 55).lineTo(40, 78);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr4.push(hrLine_1);
        }

        this.text_2 = new cjs.Text("20", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(84, 54)

        this.text_3 = new cjs.Text("8", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(128, 54);

        this.text_4 = new cjs.Text("12", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(163, 54);

        this.text_5 = new cjs.Text("20", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(84, 202);

        this.text_6 = new cjs.Text("4", "16px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(128, 202);

        this.text_24 = new cjs.Text("16", "16px 'Myriad Pro'", "#000000");
        this.text_24.setTransform(163, 202);

        this.text_7 = new cjs.Text("20", "16px 'Myriad Pro'", "#000000");
        this.text_7.setTransform(335, 54)

        this.text_8 = new cjs.Text("13", "16px 'Myriad Pro'", "#000000");
        this.text_8.setTransform(375, 54);

        this.text_9 = new cjs.Text("7", "16px 'Myriad Pro'", "#000000");
        this.text_9.setTransform(420, 54);

        this.text_10 = new cjs.Text("20", "16px 'Myriad Pro'", "#000000");
        this.text_10.setTransform(335, 202);

        this.text_11 = new cjs.Text("11", "16px 'Myriad Pro'", "#000000");
        this.text_11.setTransform(375, 202);

        this.text_12 = new cjs.Text("9", "16px 'Myriad Pro'", "#000000");
        this.text_12.setTransform(420, 202);


        this.text_13 = new cjs.Text("1", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_13.setTransform(17.5, 103)

        this.text_14 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_14.setTransform(33, 103);

        this.text_15 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_15.setTransform(50, 103);

        this.text_16 = new cjs.Text("8", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_16.setTransform(17.5, 136);

        this.text_17 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_17.setTransform(34, 135);

        this.text_18 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_18.setTransform(135, 103)

        this.text_19 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_19.setTransform(151, 103);

        this.text_20 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_20.setTransform(168.5, 103);

        this.text_21 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_21.setTransform(135, 136);

        this.text_22 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_22.setTransform(152, 136);

        this.text_23 = new cjs.Text("–", "36px 'UusiTekstausMajema'", "#6F6F6E");
        this.text_23.setTransform(168.5, 134);

        this.addChild(this.text, this.text_1, this.roundRect1, this.shape_group2, this.shape_group3, this.textbox_group1,
            this.textbox_group2, this.textbox_group3, this.textbox_group4, this.textbox_group5, this.textbox_group6);

        this.addChild(this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10,
            this.text_11, this.text_12, this.text_13, this.text_14, this.text_15, this.text_16, this.text_17, this.text_18, this.text_19, this.text_20,
            this.text_21, this.text_22, this.text_23, this.text_24);

        for (var i = 0; i < lineArr1.length; i++) {
            this.addChild(lineArr1[i]);
        }
        for (var i = 0; i < lineArr2.length; i++) {
            this.addChild(lineArr2[i]);
        }

        for (var i = 0; i < lineArr3.length; i++) {
            this.addChild(lineArr3[i]);
        }
        for (var i = 0; i < lineArr4.length; i++) {
            this.addChild(lineArr4[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 310);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p17_1();
        this.instance.setTransform(3, 10, 0.47, 0.47);

        this.text = new cjs.Text("Vad kostar pärlan?", "16px 'Myriad Pro'");
        this.text.setTransform(23, 10);

        this.text_1 = new cjs.Text("8.", "bold 17px 'Myriad Pro'", "#90BD22");
        this.text_1.setTransform(4, 10);

        //outer rect

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(7, 0, 253, 99, 10);
        this.roundRect1.setTransform(0, 22);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#7d7d7d').drawRoundRect(7, 53, 253, 106, 10);
        this.roundRect2.setTransform(0, 75);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("").s('#7d7d7d').drawRoundRect(265, 0, 253, 99, 10);
        this.roundRect3.setTransform(0, 22);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("").s('#7d7d7d').drawRoundRect(265, 103, 253, 106, 10);
        this.roundRect4.setTransform(0, 25);

        //inner green rect

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("").s('#90BD22').drawRoundRect(15, 10, 135, 76, 10);
        this.roundRect5.setTransform(5, 25);

        this.roundRect6 = new cjs.Shape();
        this.roundRect6.graphics.f("").s('#90BD22').drawRoundRect(15, 67, 135, 80, 10);
        this.roundRect6.setTransform(0, 75);

        this.roundRect7 = new cjs.Shape();
        this.roundRect7.graphics.f("").s('#90BD22').drawRoundRect(275, 15, 135, 75, 10);
        this.roundRect7.setTransform(0, 20);

        this.roundRect8 = new cjs.Shape();
        this.roundRect8.graphics.f("").s('#90BD22').drawRoundRect(275, 67, 135, 80, 10);
        this.roundRect8.setTransform(0, 75);



        this.text_3 = new cjs.Text("16 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(25, 53);

        this.text_4 = new cjs.Text("16 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(25, 160);

        this.text_5 = new cjs.Text("18 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(283, 53);

        this.text_6 = new cjs.Text("19 kr tillsammans", "16px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(283, 160);


        this.text_7 = new cjs.Text("5 kr", "16px 'Myriad Pro'");
        this.text_7.setTransform(203, 57);
        this.text_7.skewX = 9;
        this.text_7.skewY = 9;


        this.text_8 = new cjs.Text("5 kr", "16px 'Myriad Pro'");
        this.text_8.setTransform(203, 170);
        this.text_8.skewX = 9;
        this.text_8.skewY = 9;


        this.text_9 = new cjs.Text("4 kr", "16px 'Myriad Pro'");
        this.text_9.setTransform(466, 59);
        this.text_9.skewX = 9;
        this.text_9.skewY = 9;

        this.text_10 = new cjs.Text("2 kr", "16px 'Myriad Pro'");
        this.text_10.setTransform(465, 167);
        this.text_10.skewX = 9;
        this.text_10.skewY = 9;


        this.addChild(this.text, this.text_1, this.text_2, this.instance, this.roundRect1, this.roundRect2, this.roundRect3,
            this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.roundRect8, this.text_3, this.text_4,
            this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 230);


    (lib.p6 = function() {
        this.initialize();


        this.v1 = new lib.Symbol2();
        this.v1.setTransform(307, 125, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(307, 440, 1, 1, 0, 0, 0, 255.8, 38);

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
