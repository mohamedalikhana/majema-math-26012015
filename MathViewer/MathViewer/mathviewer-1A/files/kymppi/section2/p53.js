(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p53_1.png",
            id: "p53_1"
        }, {
            src: "images/p53_2.png",
            id: "p53_2"
        }]
    };

    // symbols:

    (lib.p53_1 = function() {
        this.initialize(img.p53_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p53_2 = function() {
        this.initialize(img.p53_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol1 = function() {
        this.initialize();

        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageNoRight = new cjs.Text("53", "12px 'Myriad Pro'", "#FFFFFF");
        this.pageNoRight.lineHeight = 18;
        this.pageNoRight.setTransform(555, 648);
        this.pageNoRightBackground = new cjs.Shape();
        this.pageNoRightBackground.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.pageNoRightBackground.setTransform(579, 660.8);

        this.pageTitle = new cjs.Text("Vi lär oss addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 19);

        this.chapterNumber = new cjs.Text("12", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(50.7, 15.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoRightBackground, this.pageNoRight);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p53_1();
        this.instance.setTransform(0, 26, 0.49, 0.49);

        this.text = new cjs.Text("Hur många bär finns kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("En fågel äter 1 bär.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(65, 35);

        this.text_3 = new cjs.Text("En fågel äter 1 bär.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(315, 35);

        this.text_4 = new cjs.Text("En fågel äter 2 bär.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(65, 178);

        this.text_5 = new cjs.Text("En fågel äter 2 bär.", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(315, 178);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1038 * 0.5) - 10) * 0.49, ((568 * 0.5) - 2) * 0.49, 10);
        this.roundRect1.setTransform(3, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1038 * 0.5) + 10) * 0.49, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 170);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1038 * 0.5) + 10) * 0.49, 170);

        var textToBeDisplayed1 = ['3', '–', '1', '=', ' '];
 
        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(74 + (i * 20), 130, 20, 23);
            this.labelTemp1 = new cjs.Text(textToBeDisplayed1[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp1.lineHeight = 34;
            this.labelTemp1.setTransform(77 + (i * 20), 121);
            textToBeDisplayed1[i] = this.labelTemp1;
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(330 + (i * 20), 130, 20, 23);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(74 + (i * 20), 275, 20, 23);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(330 + (i * 20), 275, 20, 23);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance,
            this.text, this.text_1,this.text_2,this.text_3,this.text_4,this.text_5, this.textBoxGroup1, this.textBoxGroup2,this.textBoxGroup3,
             this.textBoxGroup4);
   
         for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed1[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 300);


    (lib.Symbol3 = function() {
        this.initialize();
        this.instance = new lib.p53_2();
        this.instance.setTransform(0, 26, 0.37, 0.37);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1038 * 0.5) - 10) * 0.49, ((568 * 0.5) - 2) * 0.49, 10);
        this.roundRect1.setTransform(3, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1038 * 0.5) + 10) * 0.49, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 170);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1038 * 0.5) + 10) * 0.49, 170);      
 
        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(76 + (i * 20), 130, 20, 23);           
        }
        this.textBoxGroup1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(334 + (i * 20), 130, 20, 23);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(76 + (i * 20), 275, 20, 23);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(334 + (i * 20), 275, 20, 23);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.text_1 = new cjs.Text("En fågel äter 3 bär.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(65, 35);

        this.text_2 = new cjs.Text("En fågel äter 3 bär.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(315, 35);

        this.text_3 = new cjs.Text("En fågel äter 4 bär.", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(65, 179);

        this.text_4 = new cjs.Text("En fågel äter 4 bär.", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(315, 179);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance,
            this.textBoxGroup1,this.textBoxGroup2,this.textBoxGroup3,this.textBoxGroup4,this.text_1
            ,this.text_2,this.text_3,this.text_4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 300);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(314.5, 91, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(315.5, 365.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
