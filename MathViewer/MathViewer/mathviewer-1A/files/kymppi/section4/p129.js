(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p129_1.png",
            id: "p129_1"
        }, {
            src: "images/p129_2.png",
            id: "p129_2"
        }]
    };

    (lib.p129_1 = function() {
        this.initialize(img.p129_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p129_2 = function() {
        this.initialize(img.p129_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
          
        this.text = new cjs.Text("129", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(560, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(587.3, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

     
    (lib.Symbol2 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Vad kostar sakerna tillsammans?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(20, 1);

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 0);

        this.instance_2 = new lib.p129_1();
        this.instance_2.setTransform(5, 28, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 252.5, 97, 10);
        this.roundRect1.setTransform(0, 16);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(256.5, 16);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 117);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(256.5, 117);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 218);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(256.5, 218);

        var arrTxtbox = ['95', '195', '296'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding,
            yPos,
            rectWidth = 200,
            rectHeight = 23,
            boxWidth = 20,
            boxHeight = 23,
            numberOfBoxes = 10,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 28;
                if (i == 1) {
                    padding = 42;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 40;
        this.text_1.setTransform(27, 86);
        this.text_2 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 40;
        this.text_2.setTransform(68.2, 86);
        this.text_3 = new cjs.Text("6", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 40;
        this.text_3.setTransform(87, 86);
        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 40;
        this.text_4.setTransform(128, 86);
        this.text_5 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 30;
        this.text_5.setTransform(106, 91);
        this.text_6 = new cjs.Text("kr", "31px 'UusiTekstausMajema'", "#6C7373");
        this.text_6.lineHeight = 30;
        this.text_6.setTransform(46, 91);

        // // row-1
        this.text_7 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 15;
        this.text_7.setTransform(93, 44.5);
        this.text_7.skewX = 4;
        this.text_7.skewY = 4;
        this.text_8 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_8.lineHeight = 15;
        this.text_8.setTransform(199, 46);
        this.text_8.skewX = 5;
        this.text_8.skewY = 5;
        this.text_9 = new cjs.Text("7 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 15;
        this.text_9.setTransform(337, 41.5);
        this.text_9.skewX = -20;
        this.text_9.skewY = -20;
        this.text_10 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_10.lineHeight = 15;
        this.text_10.setTransform(465, 42);
        this.text_10.skewX = 5;
        this.text_10.skewY = 5;
        // // row-2
        this.text_11 = new cjs.Text("10 kr", "13px 'Myriad Pro'");
        this.text_11.lineHeight = 15;
        this.text_11.setTransform(99, 150.5);
        this.text_11.skewX = 4;
        this.text_11.skewY = 4;
        this.text_12 = new cjs.Text("1 kr", "13px 'Myriad Pro'");
        this.text_12.lineHeight = 15;
        this.text_12.setTransform(202.5, 154);
        this.text_12.skewX = 5;
        this.text_12.skewY = 5;
        this.text_13 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_13.lineHeight = 15;
        this.text_13.setTransform(343, 152);
        this.text_13.skewX = 4;
        this.text_13.skewY = 4;
        this.text_14 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_14.lineHeight = 15;
        this.text_14.setTransform(463, 146);
        this.text_14.skewX = 4;
        this.text_14.skewY = 4;
        // // row-3
        this.text_15 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_15.lineHeight = 15;
        this.text_15.setTransform(108, 244);
        this.text_15.skewX = 4;
        this.text_15.skewY = 4;
        this.text_16 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_16.lineHeight = 15;
        this.text_16.setTransform(216.5, 243);
        this.text_16.skewX = 4;
        this.text_16.skewY = 4;
        this.text_17 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_17.lineHeight = 15;
        this.text_17.setTransform(348, 251);
        this.text_17.skewX = 4;
        this.text_17.skewY = 4;
        this.text_18 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_18.lineHeight = 15;
        this.text_18.setTransform(473, 242);
        this.text_18.skewX = 5;
        this.text_18.skewY = 5;

        this.addChild(this.text_q1, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14);
        this.addChild(this.text_15, this.text_16, this.text_17, this.text_18);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

     (lib.Symbol3 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Sakerna kostar 11 kr tillsammans. Vad kostar bollarna?", "16px 'Myriad Pro'");
        this.text_q1.lineHeight = 19;
        this.text_q1.setTransform(20, 22);   

        this.text = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#8490C8");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 21);

        this.instance_2 = new lib.p129_2();
        this.instance_2.setTransform(0, 45, 0.38, 0.38);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 252.5, 97, 10);
        this.roundRect1.setTransform(0, 37);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(256.5, 37);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 138);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(256.5, 138);

        
        // // row-1
        this.text_7 = new cjs.Text("7 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 15;
        this.text_7.setTransform(73, 58.5);
        this.text_7.skewX = -7;
        this.text_7.skewY = -5;      
        this.text_9 = new cjs.Text("3 kr", "13px 'Myriad Pro'");
        this.text_9.lineHeight = 15;
        this.text_9.setTransform(312, 62.5);
        this.text_9.skewX = -12;
        this.text_9.skewY = -12;       
        // // row-2
        this.text_11 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_11.lineHeight = 15;
        this.text_11.setTransform(78, 176.5);
        this.text_11.skewX = -22;
        this.text_11.skewY = -30;        
        this.text_13 = new cjs.Text("9 kr", "13px 'Myriad Pro'");
        this.text_13.lineHeight = 15;
        this.text_13.setTransform(334, 165);
        this.text_13.skewX = -15;
        this.text_13.skewY = -15;
       
        

        this.addChild(this.text_q1, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6);
        this.addChild(this.text_7, this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14);
        this.addChild(this.text_15, this.text_16, this.text_17, this.text_18);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

       
        this.v2 = new lib.Symbol2();
        this.v2.setTransform(314, 275, 1, 1, 0, 0, 0, 256.3, 217.9);

         this.v3 = new lib.Symbol3();
        this.v3.setTransform(314, 605, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v3,this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
