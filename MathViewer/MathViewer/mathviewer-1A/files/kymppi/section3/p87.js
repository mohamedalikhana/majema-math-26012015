(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p87_1.png",
            id: "p87_1"
        }]
    };

    // symbols:
    (lib.p87_1 = function() {
        this.initialize(img.p87_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("87", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);


        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Skriv talet som saknas så att svaret stämmer.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 156, 293, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(172, 25);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(343, 0, 167, 293, 10);
        this.roundRect3.setTransform(0, 25);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f("#F1662B").beginStroke("#000000").setStrokeStyle(0.8).moveTo(66, 66).lineTo(66, 30).lineTo(91, 50).lineTo(66, 60);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f("#F1662B").beginStroke("#000000").setStrokeStyle(0.8).moveTo(239, 66).lineTo(239, 30).lineTo(264, 50).lineTo(239, 60);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.f("#F1662B").beginStroke("#000000").setStrokeStyle(0.8).moveTo(415, 66).lineTo(415, 30).lineTo(440, 50).lineTo(415, 60);
        this.Line_3.setTransform(0, 0);

        this.text1 = new cjs.Text("5", "15px 'Myriad Pro'", "#FFFFFF");
        this.text1.lineHeight = 19;
        this.text1.setTransform(68, 41);
        this.text2 = new cjs.Text("6", "15px 'Myriad Pro'", "#FFFFFF");
        this.text2.lineHeight = 19;
        this.text2.setTransform(241, 41);
        this.text3 = new cjs.Text("7", "15px 'Myriad Pro'", "#FFFFFF");
        this.text3.lineHeight = 19;
        this.text3.setTransform(417, 41);


        var arrxPos = ['40', '194', '366'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);

            for (var column = 0; column < 5; column++) {
                var columnSpace = column;
                for (var row = 0; row < 1; row++) { // yellow ball
                    this.shape_group1.graphics.f("#FFF679").s("#000000").ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * 20), 82 + (row * 23), 8.5, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.shape_group2 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) { // yellow ball
                if (column == 1) {
                    columnSpace = 8.6;
                } else if (column == 2) {
                    columnSpace = 9.6;
                }
                this.shape_group2.graphics.f("#FFF679").s("#000000").ss(0.8, 0, 0, 4).arc(301 + (columnSpace * 20), 82 + (row * 23), 8.5, 0, 2 * Math.PI);
            }
        }
        this.shape_group2.setTransform(0, 0);

        this.label1 = new cjs.Text("1   +   1   +", "16px 'Myriad Pro'");
        this.label1.lineHeight = 28;
        this.label1.setTransform(24, 106);
        this.label2 = new cjs.Text("1   +   2   +", "16px 'Myriad Pro'");
        this.label2.lineHeight = 28;
        this.label2.setTransform(24, 138);
        this.label3 = new cjs.Text("1   +   0   +", "16px 'Myriad Pro'");
        this.label3.lineHeight = 28;
        this.label3.setTransform(24, 168);
        this.label4 = new cjs.Text("2   +   0   +", "16px 'Myriad Pro'");
        this.label4.lineHeight = 28;
        this.label4.setTransform(24, 198);
        this.label5 = new cjs.Text("3   +   1   +", "16px 'Myriad Pro'");
        this.label5.lineHeight = 28;
        this.label5.setTransform(24, 229);
        this.label6 = new cjs.Text("3   +   0   +", "16px 'Myriad Pro'");
        this.label6.lineHeight = 28;
        this.label6.setTransform(24, 259);
        this.label7 = new cjs.Text("3   +   2   +", "16px 'Myriad Pro'");
        this.label7.lineHeight = 28;
        this.label7.setTransform(24, 290);

        this.label8 = new cjs.Text("1   +   1   +", "16px 'Myriad Pro'");
        this.label8.lineHeight = 28;
        this.label8.setTransform(197, 106);
        this.label9 = new cjs.Text("1   +   2   +", "16px 'Myriad Pro'");
        this.label9.lineHeight = 28;
        this.label9.setTransform(197, 138);
        this.label10 = new cjs.Text("1   +   0   +", "16px 'Myriad Pro'");
        this.label10.lineHeight = 28;
        this.label10.setTransform(197, 168);
        this.label11 = new cjs.Text("1   +   3   +", "16px 'Myriad Pro'");
        this.label11.lineHeight = 28;
        this.label11.setTransform(197, 198);
        this.label12 = new cjs.Text("2   +   2   +", "16px 'Myriad Pro'");
        this.label12.lineHeight = 28;
        this.label12.setTransform(197, 229);
        this.label13 = new cjs.Text("2   +   3   +", "16px 'Myriad Pro'");
        this.label13.lineHeight = 28;
        this.label13.setTransform(197, 259);
        this.label14 = new cjs.Text("2   +   4   +", "16px 'Myriad Pro'");
        this.label14.lineHeight = 28;
        this.label14.setTransform(197, 290);

        this.label15 = new cjs.Text("1   +   2   +", "16px 'Myriad Pro'");
        this.label15.lineHeight = 28;
        this.label15.setTransform(376, 106);
        this.label16 = new cjs.Text("1   +   0   +", "16px 'Myriad Pro'");
        this.label16.lineHeight = 28;
        this.label16.setTransform(376, 138);
        this.label17 = new cjs.Text("2   +   3   +", "16px 'Myriad Pro'");
        this.label17.lineHeight = 28;
        this.label17.setTransform(376, 168);
        this.label18 = new cjs.Text("3   +   3   +", "16px 'Myriad Pro'");
        this.label18.lineHeight = 28;
        this.label18.setTransform(376, 198);
        this.label19 = new cjs.Text("4   +   1   +", "16px 'Myriad Pro'");
        this.label19.lineHeight = 28;
        this.label19.setTransform(376, 229);
        this.label20 = new cjs.Text("5   +   0   +", "16px 'Myriad Pro'");
        this.label20.lineHeight = 28;
        this.label20.setTransform(376, 259);
        this.label21 = new cjs.Text("5   +   1   +", "16px 'Myriad Pro'");
        this.label21.lineHeight = 28;
        this.label21.setTransform(376, 290);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 7; row++) {
                if (column == 1) {
                    columnSpace = 0.677;
                } else if (column == 2) {
                    columnSpace = 1.368;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(101 + (columnSpace * 258), 102 + (row * 30.5), 20, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.text, this.text_1, this.Line_1, this.Line_2, this.Line_3);
        this.addChild(this.shape_group1, this.shape_group2, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7);
        this.addChild(this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14);
        this.addChild(this.label15, this.label16, this.label17, this.label18, this.label19, this.label20, this.label21, this.text1, this.text2, this.text3);
        this.addChild(this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p87_1();
        this.instance.setTransform(334, 40, 0.48, 0.48);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 511.1, 248, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Jämför. Skriv >, < eller =.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 2);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 2);

        this.label1 = new cjs.Text("3   +   1", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(32, 39);
        this.label1a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label1a.lineHeight = 19;
        this.label1a.setTransform(110, 39);
        this.label2 = new cjs.Text("3   –   1", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(32, 69);
        this.label2a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label2a.lineHeight = 19;
        this.label2a.setTransform(110, 69);
        this.label3 = new cjs.Text("3   +   0", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(32, 98.5);
        this.label3a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label3a.lineHeight = 19;
        this.label3a.setTransform(110, 98.5);
        this.label4 = new cjs.Text("3   –   0", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(32, 129);
        this.label4a = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label4a.lineHeight = 19;
        this.label4a.setTransform(110, 129);
        this.label5 = new cjs.Text("5   +   1", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(74, 157);
        this.label5a = new cjs.Text("5", "16px 'Myriad Pro'");
        this.label5a.lineHeight = 19;
        this.label5a.setTransform(34, 157);
        this.label6 = new cjs.Text("5   +   0", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(74, 188);
        this.label6a = new cjs.Text("5", "16px 'Myriad Pro'");
        this.label6a.lineHeight = 19;
        this.label6a.setTransform(34, 188);
        this.label7 = new cjs.Text("5   –   1", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19; 
        this.label7.setTransform(74, 218);
        this.label7a = new cjs.Text("5", "16px 'Myriad Pro'");
        this.label7a.lineHeight = 19; 
        this.label7a.setTransform(34, 218);
        this.label8 = new cjs.Text("5   –   0", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(74, 247);
        this.label8a = new cjs.Text("5", "16px 'Myriad Pro'");
        this.label8a.lineHeight = 19;
        this.label8a.setTransform(34, 247);

        this.label9 = new cjs.Text("4   +   1", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(207, 39);
        this.label9a = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label9a.lineHeight = 19;
        this.label9a.setTransform(285, 39);
        this.label10 = new cjs.Text("4   +   0", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(207, 69);
        this.label10a = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label10a.lineHeight = 19;
        this.label10a.setTransform(285, 69);
        this.label11 = new cjs.Text("4   –   1", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(207, 98.5);
        this.label11a = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label11a.lineHeight = 19;
        this.label11a.setTransform(285, 98.5);
        this.label12 = new cjs.Text("4   –   4", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(207, 129);
        this.label12a = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label12a.lineHeight = 19;
        this.label12a.setTransform(285, 129);
        this.label13 = new cjs.Text("6   –   1", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(249, 157);
        this.label13a = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label13a.lineHeight = 19;
        this.label13a.setTransform(209, 157);
        this.label14 = new cjs.Text("6   –   0", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(249, 188);
        this.label14a = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label14a.lineHeight = 19;
        this.label14a.setTransform(209, 188);
        this.label15 = new cjs.Text("6   +   1", "16px 'Myriad Pro'");
        this.label15.lineHeight = 19; 
        this.label15.setTransform(249, 218);
        this.label15a = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label15a.lineHeight = 19; 
        this.label15a.setTransform(209, 218);
        this.label16 = new cjs.Text("6   +   0", "16px 'Myriad Pro'");
        this.label16.lineHeight = 19;
        this.label16.setTransform(249, 247);
        this.label16a = new cjs.Text("6", "16px 'Myriad Pro'");
        this.label16a.lineHeight = 19;
        this.label16a.setTransform(209, 247);

        this.label17 = new cjs.Text("Kom ihåg!", "16px 'Myriad Pro'");
        this.label17.lineHeight = 19;
        this.label17.setTransform(369, 58);
        this.label18 = new cjs.Text("3 > 2", "16px 'Myriad Pro'");
        this.label18.lineHeight = 19;
        this.label18.setTransform(386, 81);
        this.label19 = new cjs.Text("2 < 3", "16px 'Myriad Pro'");
        this.label19.lineHeight = 19;
        this.label19.setTransform(386, 102);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 1) {
                    columnSpace = 0.677;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(87 + (columnSpace * 258), 33.5 + (row * 29.5), 20, 24);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 1) {
                    columnSpace = 0.677;
                }
                this.textbox_group2.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(51 + (columnSpace * 258), 153.5 + (row * 29.5), 20, 24);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.instance);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
        this.addChild(this.label1a, this.label2a, this.label3a, this.label4a, this.label5a, this.label6a, this.label7a, this.label8a);
        this.addChild(this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15, this.label16);
        this.addChild(this.label9a, this.label10a, this.label11a, this.label12a, this.label13a, this.label14a, this.label15a, this.label16a);
        this.addChild(this.label17, this.label18, this.label19);
        this.addChild(this.textbox_group1,this.textbox_group2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 250);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(312.3, 401.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(312.3, 88.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
