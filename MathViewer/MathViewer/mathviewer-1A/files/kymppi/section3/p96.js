(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p96_1.png",
            id: "p96_1"
        }, {
            src: "images/p96_2.png",
            id: "p96_2"
        }]
    };

    (lib.p96_1 = function() {
        this.initialize(img.p96_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p96_2 = function() {
        this.initialize(img.p96_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol16 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("33", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(56, 22.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(52.6, 23.5);

        this.text_2 = new cjs.Text("Addera och subtrahera", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_2.lineHeight = 29;
        this.text_2.setTransform(102.5, 25.5);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med addition och subtraktion 0 till 10", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(80, 651);

        this.text = new cjs.Text("96", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(43.4, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(40.1, 660.8);

        this.instance = new lib.p96_1();
        this.instance.setTransform(0, 49, 0.457, 0.405);

        this.addChild(this.instance, this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();

        var adjustX = 5;
        var adjustY = 10;

        this.text_q2 = new cjs.Text("Det kommer 2 fåglar till.", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(20 - adjustX, 2 - adjustY);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 20;
        this.text.setTransform(0 - adjustX, 1 - adjustY);

        this.instance_2 = new lib.p96_2();
        this.instance_2.setTransform(-2 - adjustX, 33 - adjustY, 0.40, 0.40);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 113, 10);
        this.roundRect1.setTransform(0 - adjustX, 17 - adjustY);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260 - adjustX, 17 - adjustY);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0 - adjustX, 135 - adjustY);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260 - adjustX, 135 - adjustY);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0 - adjustX, 253 - adjustY);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260 - adjustX, 253 - adjustY);

        var arrTxtbox = ['109', '227', '345'];

        this.textbox_group1 = new cjs.Shape();
        var xPos = 0,
            padding = 40,
            yPos,
            rectWidth = 110,
            rectHeight = 23,
            boxWidth = 22,
            boxHeight = 23,
            numberOfBoxes = 5,
            numberOfRects = 2;
        for (var k = 0; k < arrTxtbox.length; k++) {
            yPos = parseInt(arrTxtbox[k]);

            for (var i = 0; i < numberOfRects; i++) {
                padding = 72;
                if (i == 1) {
                    padding = 111;
                }

                var rectStartPosX = xPos + (padding * (i + 1)) + (i * rectWidth);
                this.textbox_group1.graphics.s("#707070").ss(0.8).drawRect(rectStartPosX, yPos, rectWidth, rectHeight);
                for (var j = 1; j < numberOfBoxes; j++) {
                    this.textbox_group1.graphics.s("#707070").ss(0.8).moveTo(rectStartPosX + (boxWidth * j), yPos).lineTo(rectStartPosX + (boxWidth * j), yPos + boxHeight);
                }
            }
        }
        this.textbox_group1.setTransform(0 - adjustX, 0 - adjustY);

        this.text_1 = new cjs.Text("5", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_1.lineHeight = 45;
        this.text_1.setTransform(73 - adjustX, 99 - adjustY);

        this.text_2 = new cjs.Text("+", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_2.lineHeight = 45;
        this.text_2.setTransform(97 - adjustX, 99 - adjustY);

        this.text_3 = new cjs.Text("2", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_3.lineHeight = 45;
        this.text_3.setTransform(117 - adjustX, 99 - adjustY);

        this.text_4 = new cjs.Text("=", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_4.lineHeight = 45;
        this.text_4.setTransform(141 - adjustX, 99 - adjustY);

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4);
        this.addChild(this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 506, 362);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(295, 486, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
