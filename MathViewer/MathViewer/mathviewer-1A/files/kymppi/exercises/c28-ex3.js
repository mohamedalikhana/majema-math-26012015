var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };



    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 8", "bold 24px 'Myriad Pro'", "#F1662B");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("28", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    
    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Räkna högt: 0, 1, 2 … 8.", "24px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550 + 110, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        var numberLineX = 60;
        scaleX = scaleY = 4;
        this.numberLine = new lib.NumberLine(9, 37.5, "circle");
        this.numberLine.setTransform(numberLineX, 20, scaleX, scaleY);
        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.numberLine, this.commentTexts);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        var numberLineX = 60;
        scaleX = scaleY = 4;
        this.numberLine = new lib.NumberLine(9, 37.5, "circle");
        this.numberLine.setTransform(numberLineX, 20, scaleX, scaleY);
        this.tweens = [];
        var startColor = '#000000';
        var endColor = '#000000';
        for (var textEl = 0; textEl < this.numberLine.texts.length; textEl++) {
            this.numberLine.texts[textEl].setTransform(numberLineX + 5 + (this.numberLine.texts[textEl].x * scaleX), 20 + (this.numberLine.texts[textEl].y * scaleY), scaleX, scaleY);
            this.addChild(this.numberLine.texts[textEl]);
            if (this.numberLine.texts.length === (textEl + 1)) {
                startColor = '#00B2CA';
                endColor = '#00B2CA';
            } else {
                // startColor = '#00B2CA';
                endColor = '#000000';
            }
            this.tweens.push({
                ref: this.numberLine.texts[textEl],
                alphaFrom: 0,
                alphaTo: 1,
                wait: 850 * (textEl + 1),
                alphaTimeout: 1500,
                startColor: startColor,
                // endColor: endColor
            });
        }
        p.tweens = this.tweens;
        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.numberLine, this.commentTexts);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();

        rectangles = new lib.animation1();
        rectangles.setTransform(190, 20, 3, 3, 0, 0, 0, 0, 0);

        this.commentTexts = new lib.CommentText();
        this.commentTexts.hintText1.visible = true;
        this.addChild(this.rectangles, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.NumberLine = function(dots, spacingX, numberLineType) {
        this.initialize();
        var startPosX = -3,
            startPosY = 10,
            spacingY = 1;
        extraSpaceRatio = 1 / 2, numberLineType = "circle"; //bar|circle
        var arrowStartX = startPosX + (spacingX * dots) - (spacingX * extraSpaceRatio);
        var arrowWidth = 5,
            arrowHeight = 5;
        var textArr = [];
        this.texts = [];
        this.numberLine = new cjs.Shape();
        this.numberLine.graphics.ss(1.5).f("#000000").s("#000000").moveTo(startPosX - (spacingX * extraSpaceRatio), startPosY).lineTo(arrowStartX, startPosY);
        this.numberLine.graphics.ss(1.5).moveTo(arrowStartX, startPosY);
        this.numberLine.graphics.lineTo(arrowStartX, startPosY - (arrowHeight / 2));
        this.numberLine.graphics.lineTo(arrowStartX + arrowWidth, startPosY);
        this.numberLine.graphics.lineTo(arrowStartX, startPosY + (arrowHeight / 2));
        this.numberLine.graphics.lineTo(arrowStartX, startPosY);
        var numberLineLimit = dots;
        for (var dot = 0; dot < dots; dot++) {
            var strokeColor = "#000000";
            var temptext = null;
            if (numberLineLimit === (dot + 1)) {
                //strokeColor = "#00B2CA";
                temptext = new cjs.Text("" + (dot), "bold 16px 'Myriad Pro'", '#00B2CA');
            } else {
                temptext = new cjs.Text("" + (dot), "16px 'Myriad Pro'");
            }
            //this.numberLine.graphics.f("#000000").ss(1.5).s(strokeColor).moveTo(130 + (20 * dot), 55).lineTo(130 + (20 * dot), 65);
            // this.numberLine.graphics.f("#000000").ss(1.5).s(strokeColor).arc(startPosX + (spacingX * dot), startPosY, 2, 0, Math.PI * 2, -1);
            this.numberLine.graphics.f("#000000").ss(1.5).s(strokeColor).rect(startPosX + (spacingX * dot), startPosY - 5, 0.4, 10);
            temptext.alpha = 0;
            temptext.setTransform(startPosX - 5 + (spacingX * dot), startPosY + 15);
            textArr.push(temptext);
            this.texts.push(temptext);
        }
        this.numberLine.setTransform(0, 0, 1, 1, 0, 0, 0, 0, 0);
        // this.tweens = [];
        // for (var textEl = 0; textEl < textArr.length; textEl++) {
        //     this.addChild(textArr[textEl]);
        //     this.tweens.push({
        //         ref: textArr[textEl],
        //         alphaFrom: 0,
        //         alphaTo: 1,
        //         wait: 1000*(textEl+1),
        //         alphaTimeout: 2000
        //     });
        // }

        p.tweens = this.texts;
        this.addChild(this.numberLine);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
