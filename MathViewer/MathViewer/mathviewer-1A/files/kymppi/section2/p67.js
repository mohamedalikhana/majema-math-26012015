(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p67_1.png",
            id: "p67_1"
        }]
    };

    // symbols:
    (lib.p67_1 = function() {
        this.initialize(img.p67_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("67", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#87C140").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Hur många får gömmer sig?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 1);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 1);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 252, 107, 10);
        this.roundRect1.setTransform(0, 25);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(257, 25);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 137);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(257, 137);

        this.instance = new lib.p67_1();
        this.instance.setTransform(5, 27, 0.48, 0.48);

        this.label1 = new cjs.Text("2  +           =  6", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(78, 105);
        this.label2 = new cjs.Text("1  +           =  6", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(338.5, 105)
        this.label3 = new cjs.Text("+  2  =  6", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(101, 218);
        this.label4 = new cjs.Text("+  1  =  6", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(363, 218);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                var xPos = (row == 0) ? 113 : 75;
                if (column == 1) {
                    columnSpace = 0.677;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(xPos + (columnSpace * 385), 101 + (row * 112), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect4, this.text, this.text_1);
        this.addChild(this.label1, this.label2, this.label3, this.label4);
        this.addChild(this.instance, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol2 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 510, 132, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 2);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 2);

        this.label1 = new cjs.Text("4  +           =  5", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(32, 39);
        this.label2 = new cjs.Text("2  +           =  5", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(32, 69);
        this.label3 = new cjs.Text("3  +           =  5", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(32, 98.5);
        this.label4 = new cjs.Text("1  +           =  5", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(32, 129);

        this.label5 = new cjs.Text("+  2  =  5", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(213, 69);
        this.label6 = new cjs.Text("+  1  =  5", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(213, 98.5);
        this.label7 = new cjs.Text("+  4  =  5", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(213, 129);

        this.label8 = new cjs.Text("0  +           =  5", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(341, 39);
        this.label9 = new cjs.Text("5  +           =  5", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(341, 69);
        this.label10 = new cjs.Text("+  3  =  5", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(371.5, 98.5);
        this.label11 = new cjs.Text("+  0  =  5", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(371.5, 129);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 5; column++) {
            var columnSpace;
            if(column==1){
                columnSpace=24;
            } else{
                columnSpace=25;
            }
            this.shape_group1.graphics.f('#0095DA').s("#000000").ss(0.8, 0, 0, 4).arc(185 + (column * columnSpace), 45, 9.5, 0, 2 * Math.PI);
        }
        this.shape_group1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 2) {
                    columnSpace = 2.33;
                } else if (column == 3) {
                    columnSpace = 2.59;
                }

                if (column == 1 && row == 0) {
                    continue;
                } else if (column == 2) {
                    if (row == 0 || row == 1) {
                        continue;
                    }
                } else if (column == 3) {
                    if (row == 2 || row == 3) {
                        continue;
                    }
                }

                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(68 + (columnSpace * 119), 33.5 + (row * 29.5), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.instance);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
        this.addChild(this.label9, this.label10, this.label11);

        this.addChild(this.textbox_group1, this.shape_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 150);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 510, 132, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Skriv talet som saknas.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 2);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 2);

        this.label1 = new cjs.Text("5  +           =  6", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(32, 39);
        this.label2 = new cjs.Text("3  +           =  6", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(32, 69);
        this.label3 = new cjs.Text("4  +           =  6", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(32, 98.5);
        this.label4 = new cjs.Text("2  +           =  6", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(32, 129);

        this.label5 = new cjs.Text("+  1  =  6", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(213, 69);
        this.label6 = new cjs.Text("+  4  =  6", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(213, 98.5);
        this.label7 = new cjs.Text("+  3  =  6", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(213, 129);

        this.label8 = new cjs.Text("0  +           =  6", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(355, 39);
        this.label9 = new cjs.Text("6  +           =  6", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(355, 69);
        this.label10 = new cjs.Text("+  2  =  6", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(385.5, 98.5);
        this.label11 = new cjs.Text("+  5  =  6", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(385.5, 129);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 6; column++) {
            var columnSpace;
            if(column==1){
                columnSpace=24;
            } else if(column==5){
                columnSpace=27;
            } else{
                columnSpace=25;
            }
            this.shape_group1.graphics.f('#0095DA').s("#000000").ss(0.8, 0, 0, 4).arc(169 + (column * columnSpace), 45, 9.5, 0, 2 * Math.PI);
        }
        this.shape_group1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                if (column == 2) {
                    columnSpace = 2.44;
                } else if (column == 3) {
                    columnSpace = 2.71;
                }

                if (column == 1 && row == 0) {
                    continue;
                } else if (column == 2) {
                    if (row == 0 || row == 1) {
                        continue;
                    }
                } else if (column == 3) {
                    if (row == 2 || row == 3) {
                        continue;
                    }
                }

                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(68 + (columnSpace * 119), 33.5 + (row * 29.5), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.instance);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8);
        this.addChild(this.label9, this.label10, this.label11);

        this.addChild(this.textbox_group1, this.shape_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 150);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(312.3, 110.7, 1, 1, 0, 0, 0, 254.6, 53.5);
        this.v2 = new lib.Symbol2();
        this.v2.setTransform(312.3, 347, 1, 1, 0, 0, 0, 255.8, 38);
        this.v3 = new lib.Symbol3();
        this.v3.setTransform(312.3, 515, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
