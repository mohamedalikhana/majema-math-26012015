(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p87_1.png",
            id: "p87_1"
        }]
    };

    (lib.p87_1 = function() {
        this.initialize(img.p87_1);
    }).prototype = p = new cjs.Bitmap()
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("87", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(11, 7);

        this.text_1 = new cjs.Text(" Rita visare.", "16px 'Myriad Pro'");
        this.text_1.setTransform(24, 7);

        this.text_2 = new cjs.Text("Nu", "bold 16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(52, 37);

        this.text_3 = new cjs.Text("Om 1 timme", "bold 16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(140, 37);

        this.text_4 = new cjs.Text("Nu", "bold 16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(300, 37);

        this.text_5 = new cjs.Text("Om en halvtimme", "bold 16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(385, 37);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 259, 263, 10);
        this.roundRect1.setTransform(10, 17);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 259, 263, 10);
        this.roundRect2.setTransform(279, 17);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(258, 40);
        this.hrLine_1.setTransform(10, 6);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(252, 40);
        this.hrLine_2.setTransform(13, 86);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(252, 40);
        this.hrLine_3.setTransform(13, 159);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(258, 40);
        this.hrLine_4.setTransform(279, 6);

        this.hrLine_5 = new cjs.Shape();
        this.hrLine_5.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(252, 40);
        this.hrLine_5.setTransform(282, 86);

        this.hrLine_6 = new cjs.Shape();
        this.hrLine_6.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(252, 40);
        this.hrLine_6.setTransform(282, 159);

        this.vrLine_1 = new cjs.Shape();
        this.vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 259);
        this.vrLine_1.setTransform(76, 17);

        this.vrLine_2 = new cjs.Shape();
        this.vrLine_2.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(40, 0).lineTo(40, 259);
        this.vrLine_2.setTransform(332, 17);

        //1-st

        this.Symbolclock_1 = new lib.Symboldotclock();
        this.Symbolclock_1.setTransform(63, 89, 0.7, 0.7);

        this.Symbolclock_2 = new lib.Symboldotclock();
        this.Symbolclock_2.setTransform(63, 163, 0.7, 0.7);

        this.Symbolclock_3 = new lib.Symboldotclock();
        this.Symbolclock_3.setTransform(63, 240, 0.7, 0.7);

        this.Symbolclock_4 = new lib.Symboldottedclock();
        this.Symbolclock_4.setTransform(188, 89, 0.7, 0.7);

        this.Symbolclock_5 = new lib.Symboldottedclock();
        this.Symbolclock_5.setTransform(188, 163, 0.7, 0.7);

        this.Symbolclock_6 = new lib.Symboldottedclock();
        this.Symbolclock_6.setTransform(188, 240, 0.7, 0.7);

        //2-nd

        this.Symbolclock_7 = new lib.Symboldotclock();
        this.Symbolclock_7.setTransform(315, 89, 0.7, 0.7);

        this.Symbolclock_8 = new lib.Symboldotclock();
        this.Symbolclock_8.setTransform(315, 163, 0.7, 0.7);

        this.Symbolclock_9 = new lib.Symboldotclock();
        this.Symbolclock_9.setTransform(315, 240, 0.7, 0.7);

        this.Symbolclock_10 = new lib.Symboldottedclock();
        this.Symbolclock_10.setTransform(443, 89, 0.7, 0.7);

        this.Symbolclock_11 = new lib.Symboldottedclock();
        this.Symbolclock_11.setTransform(443, 163, 0.7, 0.7);

        this.Symbolclock_12 = new lib.Symboldottedclock();
        this.Symbolclock_12.setTransform(443, 240, 0.7, 0.7);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(60, 95, 1.1, 0.6, 390);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(63, 78, 1.1, 0.8);
        this.text_dot1 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot1.setTransform(57.2, 87.8);

        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(55, 165, 1.1, 0.6, 440);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(63, 173.5, 1.1, 0.8);
        this.text_dot2 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot2.setTransform(57.2, 162);

        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(69, 245, 1.1, 0.6, 133);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(63, 250.5, 1.1, 0.8);
        this.text_dot3 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot3.setTransform(57.2, 239);

        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(322, 89, 1.1, 0.6, 90);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(314.9, 78, 1.1, 0.8);
        this.text_dot4 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot4.setTransform(309.3, 87.8);

        this.shape_9 = new cjs.Shape(); // clock handle red
        this.shape_9.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_9.setTransform(309, 157, 1.1, 0.6, 500);
        this.shape_10 = new cjs.Shape(); // clock handle blue
        this.shape_10.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_10.setTransform(314.9, 173.5, 1.1, 0.8);
        this.text_dot5 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot5.setTransform(309.3, 162.8);

        this.shape_11 = new cjs.Shape(); // clock handle red
        this.shape_11.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_11.setTransform(315, 248, 1.1, 0.6, 180);
        this.shape_12 = new cjs.Shape(); // clock handle blue
        this.shape_12.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_12.setTransform(314.9, 230.5, 1.1, 0.8);
        this.text_dot6 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot6.setTransform(309.3, 239.5);

        this.addChild(this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.roundRect1, this.roundRect2,
            this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.hrLine_5, this.hrLine_6,
            this.vrLine_1, this.vrLine_2, this.Symbolclock, this.Symbolclock_1, this.Symbolclock_2, this.Symbolclock_3, this.Symbolclock_4, this.Symbolclock_5,
            this.Symbolclock_6, this.Symbolclock_7, this.Symbolclock_8, this.Symbolclock_9, this.Symbolclock_10, this.Symbolclock_11, this.Symbolclock_12,
            this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10,
            this.shape_11, this.shape_12, this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4, this.text_dot5, this.text_dot6);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 580.3, 275);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Skriv klockslagen.", "16px 'Myriad Pro'");
        this.text.setTransform(23, 0);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(10, 0);

        this.text_2 = new cjs.Text("Jag är tillbaka", "16px 'Myriad Pro'", "#000000");
        this.text_2.setTransform(143, 36);

        this.text_3 = new cjs.Text("om 3 timmar.", "16px 'Myriad Pro'", "#000000");
        this.text_3.setTransform(143, 54);

        this.text_4 = new cjs.Text("Jag är tillbaka", "16px 'Myriad Pro'", "#000000");
        this.text_4.setTransform(143, 173);

        this.text_5 = new cjs.Text("om 3 timmar.", "16px 'Myriad Pro'", "#000000");
        this.text_5.setTransform(143, 190);

        this.text_6 = new cjs.Text("Jag är tillbaka", "16px 'Myriad Pro'", "#000000");
        this.text_6.setTransform(413, 36);

        this.text_7 = new cjs.Text("om 4 timmar.", "16px 'Myriad Pro'", "#000000");
        this.text_7.setTransform(413, 54);

        this.text_8 = new cjs.Text("Jag är tillbaka", "16px 'Myriad Pro'", "#000000");
        this.text_8.setTransform(413, 173);

        this.text_9 = new cjs.Text("om 2 timmar.", "16px 'Myriad Pro'", "#000000");
        this.text_9.setTransform(413, 190);

        this.text_10 = new cjs.Text("Då är", "16px 'Myriad Pro'", "#000000");
        this.text_10.setTransform(158, 98);

        this.text_11 = new cjs.Text("klockan", "16px 'Myriad Pro'", "#000000");
        this.text_11.setTransform(158, 122);

        this.text_12 = new cjs.Text("Då är klockan", "16px 'Myriad Pro'", "#000000");
        this.text_12.setTransform(158, 235);

        this.text_13 = new cjs.Text("Då är", "16px 'Myriad Pro'", "#000000");
        this.text_13.setTransform(423, 99);

        this.text_14 = new cjs.Text("klockan", "16px 'Myriad Pro'", "#000000");
        this.text_14.setTransform(423, 123);

        this.text_15 = new cjs.Text("Då är klockan", "16px 'Myriad Pro'", "#000000");
        this.text_15.setTransform(423, 236);

        this.text_16 = new cjs.Text("halv", "16px 'Myriad Pro'", "#000000");
        this.text_16.setTransform(36, 258);

        this.text_17 = new cjs.Text("halv", "16px 'Myriad Pro'", "#000000");
        this.text_17.setTransform(158, 258);

        this.text_18 = new cjs.Text("halv", "16px 'Myriad Pro'", "#000000");
        this.text_18.setTransform(300, 258);

        this.text_19 = new cjs.Text("halv", "16px 'Myriad Pro'", "#000000");
        this.text_19.setTransform(423, 258);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 259, 128, 10);
        this.roundRect1.setTransform(10, 10);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 259, 128, 10);
        this.roundRect2.setTransform(10, 145);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 259, 128, 10);
        this.roundRect3.setTransform(278, 10);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 259, 128, 10);
        this.roundRect4.setTransform(278, 145);

        this.Symbolclock_1 = new lib.Symboldotclock();
        this.Symbolclock_1.setTransform(70, 70, 0.8, 0.8);

        this.Symbolclock_2 = new lib.Symboldotclock();
        this.Symbolclock_2.setTransform(58, 205, 0.8, 0.8);

        this.Symbolclock_3 = new lib.Symboldotclock();
        this.Symbolclock_3.setTransform(333, 70, 0.8, 0.8);

        this.Symbolclock_4 = new lib.Symboldotclock();
        this.Symbolclock_4.setTransform(321, 205, 0.8, 0.8);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(77, 74, 1.1, 0.6, 298);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(69.5, 57.5, 1.1, 0.9);
        this.text_dot1 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot1.setTransform(64, 68.8);

        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(65, 203, 1.1, 0.6, 73);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(58, 218, 1.1, 0.8);
        this.text_dot2 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot2.setTransform(52.5, 204);      

        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(323, 70, 1.1, 0.6, 450);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(333, 57, 1.1, 0.9);
        this.text_dot3 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot3.setTransform(327, 68.8);

        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(315, 210.5, 1.1, 0.6, 227);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(321, 217, 1.1, 0.8);
        this.text_dot4 = new cjs.Text(".", "33px 'Myriad Pro'");
        this.text_dot4.setTransform(315.5, 204);

        var TextArr = [];
        var TextX = [67, 329, 48, 310];
        var TextY = [28, 28, 163, 163];

        for (var row = 0; row < TextX.length; row++) {

            var text_1 = new cjs.Text("Nu", "16px 'Myriad Pro'", "#000000");
            text_1.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_1);
        }

        var lineArr = [];
        var lineX = [55, 212, 317, 477, 66, 187, 330, 455];
        var lineY = [85, 85, 85, 85, 220, 220, 220, 220];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_3 = new cjs.Shape();
            hrLine_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(30, 40);
            hrLine_3.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_3);
        }

        var DotArr = [];
        var DotX = [85, 242, 348, 507, 97, 217, 360, 486];
        var DotY = [124, 124, 124, 124, 258, 258, 258, 258];

        for (var row = 0; row < DotX.length; row++) {

            var text_1 = new cjs.Text(".", "16px 'Myriad Pro'", "#9D9D9C");
            text_1.setTransform(DotX[row], DotY[row]);
            DotArr.push(text_1);
        }

        this.instance = new lib.p87_1();
        this.instance.setTransform(83, 0, 0.48, 0.48);

        this.addChild(this.text, this.instance, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7,
            this.text_8, this.text_9, this.text_10, this.text_11, this.text_12, this.text_13, this.text_14, this.text_15, this.text_16, this.text_17,
            this.text_18, this.text_19, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.Symbolclock_1, this.Symbolclock_2,
            this.Symbolclock_3, this.Symbolclock_4, this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6,this.shape_7,this.shape_8,
            this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4);

        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < DotArr.length; i++) {
            this.addChild(DotArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 580.3, 270);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(287, 95, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(287, 417, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
