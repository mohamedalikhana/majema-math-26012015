(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p72_1.png",
            id: "p72_1"
        }]
    };

    (lib.p72_1 = function() {
        this.initialize(img.p72_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("72", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(40, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 661);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(460 + (columnSpace * 33), 29, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.textbox_group1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(4, 0);

        this.text_1 = new cjs.Text("Hur många fler körsbär är det i burken än på fatet?", "16px 'Myriad Pro'");
        this.text_1.setTransform(23, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 253, 108, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true)
        this.roundRect2.setTransform(259, 15);

        this.roundRect3 = this.roundRect1.clone(true)
        this.roundRect3.setTransform(0, 131);

        this.roundRect4 = this.roundRect1.clone(true)
        this.roundRect4.setTransform(259, 131);

        this.roundRect5 = this.roundRect1.clone(true)
        this.roundRect5.setTransform(0, 247);

        this.roundRect6 = this.roundRect1.clone(true)
        this.roundRect6.setTransform(259, 247);

        this.instance = new lib.p72_1();
        this.instance.setTransform(35, 23, 0.46, 0.46);

        var ToBeAdded = [];
        var xpos = 65;
        var ypos = 92;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 3; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 208
            } else if (numOfrow == 2) {
                ypos = 325
            } else {
                ypos = 92
            }

            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 6; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 323
                        } else {
                            xpos = 65
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 32), 21, 23);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }

        var tmpNum = [11, 13, 16]       
        var tmpVal = "";
        var ypos = 65;
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                if (row == 0) {
                    tmpVal = tmpNum[0]
                }
                if (row == 1) {
                    tmpVal = tmpNum[1]
                }
                if (row == 2) {
                    tmpVal = tmpNum[2]
                }
                var tempText1 = new cjs.Text(tmpVal, "16px 'Myriad Pro'");
                if (row == 1 && column == 0) {
                    ypos = 66
                }  else {
                    ypos = 65
                }
                tempText1.setTransform(46 + (259 * colSpace), ypos + (118 * rowSpace));
                ToBeAdded.push(tempText1);
                
            }
        }

        var tmpNum = ['1', '1', '–', '6', '=']
        var j = 0;
        for (var col = 0; col <5; col++) { 
            columnSpace=col;
            var tempText = new cjs.Text(tmpNum[j], "36px 'UusiTekstausMajema'", "#6C7373");
            tempText.setTransform(68 + (columnSpace * 22), 114);
            ToBeAdded.push(tempText);
            j++
        }

        this.addChild(this.text, this.text_1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.instance)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 350);

    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text(" Subtrahera.", "16px 'Myriad Pro'");
        this.text.setTransform(18, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(4, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 12, 511, 168, 10);
        this.roundRect1.setTransform(0, 0);


        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#7D7D7D").setStrokeStyle(0.6).moveTo(25, 0).lineTo(473, 0);
        this.Line_1.setTransform(5, 36);

        this.shape_group1 = new cjs.Shape();
        var fillcolor = "";
        var xpos = 0;
        for (var column = 0; column < 20; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {

                if (column < 5) {
                    fillcolor = "#D51317";
                    xpos = 46;
                } else if (column < 10) {
                    fillcolor = "#008BD2";
                    xpos = 56;
                } else if (column < 15) {
                    fillcolor = "#D51317";
                    xpos = 66;
                } else {
                    fillcolor = "#008BD2";
                    xpos = 76;
                }

                this.shape_group1.graphics.f(fillcolor).s("#878787").ss(0.8, 0, 0, 4).arc(xpos + (columnSpace * 20), 36 + (row * 19), 7.9, 0, 2 * Math.PI);

            }

        }
        this.shape_group1.setTransform(3, 0);


        this.text_2 = new cjs.Text("5", "13px 'Myriad Pro'", "#ffffff");
        this.text_2.setTransform(123, 39);

        this.text_3 = new cjs.Text("10", "13px 'Myriad Pro'", "#ffffff");
        this.text_3.setTransform(229, 39);

        this.text_4 = new cjs.Text("15", "13px 'Myriad Pro'", "#ffffff");
        this.text_4.setTransform(340, 39);

        this.text_5 = new cjs.Text("20", "13px 'Myriad Pro'", "#ffffff");
        this.text_5.setTransform(450, 39);

        var ToBeAdded = [];
        var arryVal = [6, 8, 7, 9];
        var i = 12;
        var tmpTxt = "";
        var temp_Line1 = new cjs.Shape();
        var tmprow = 28;
        var tmpcol = 161;
        for (var col = 0; col < 3; col++) {
            if (col >= 1) {
                i++
            }
            var columnSpace = col;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var tempText = new cjs.Text(i + " – " + arryVal[row] + "  =", "16px 'Myriad Pro'");
                tempText.setTransform(40 + (161 * columnSpace), 75 + (28 * rowSpace));

                temp_Line1.graphics.s("#9D9D9C").ss(.7).moveTo(99 + (columnSpace * tmpcol), 78 + (rowSpace * tmprow)).lineTo(133 + (columnSpace * tmpcol), 78 + (rowSpace * tmprow));
                ToBeAdded.push(tempText, temp_Line1)

            };
        };


        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1);
        this.addChild(this.Line_1, this.shape_group1, this.text_2, this.text_3, this.text_4, this.text_5)

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 552.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(300, 284, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300, 455, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
