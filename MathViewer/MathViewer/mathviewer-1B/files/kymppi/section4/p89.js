(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p89_1.png",
            id: "p89_1"
        }, {
            src: "images/p89_2.png",
            id: "p89_2"
        }]
    };

    (lib.p89_1 = function() {
        this.initialize(img.p89_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p89_2 = function() {
        this.initialize(img.p89_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol1 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("89", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.setTransform(555, 655);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);

        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();

        this.instance = new lib.p89_1();
        this.instance.setTransform(0, 9, 0.47, 0.47);

        this.instance1 = new lib.p89_2();
        this.instance1.setTransform(365, 113, 0.47, 0.47);

        this.text = new cjs.Text("Hur lång tid tar det? Kryssa och skriv.", "16px 'Myriad Pro'");
        this.text.setTransform(84, 56);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("").s("#FAAA33").ss(1).drawRoundRect(0, 0, 501, 487, 7);
        this.round_Rect1.setTransform(34, 92);

        this.shape_group1 = new cjs.Shape();
        this.shape_group1.graphics.f("#FFFFFF").s("#FAAA33").ss(0.8, 0, 0, 4).arc(0, 0, 23, 0, 2 * Math.PI);
        this.shape_group1.setTransform(374, 72, 2, 1.25);

        this.shape_group2 = new cjs.Shape();
        this.shape_group2.graphics.f("#FFFFFF").s("#FAAA33").ss(0.8, 0, 0, 4).arc(0, 0, 23, 0, 2 * Math.PI);
        this.shape_group2.setTransform(482, 72, 2, 1.25);

        var lineArr = [];
        var lineX = [50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50];
        var lineY = [72, 111.1, 150.2, 189.3, 228.4, 267.5, 306.6, 345.7, 384.8, 423.9, 463];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_3 = new cjs.Shape();
            hrLine_3.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.5).moveTo(0, 40).lineTo(470, 40);
            hrLine_3.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_3);
        }

        var TextArr = [];
        var TArr = ['äta en glass', 'gå på bio', 'borsta tänderna', 'ha lunchrast', 'gå på kalas', 'rita en katt', 'läsa'];
        var TextX = [52, 52, 52, 52, 52, 52, 52];
        var TextY = [140, 177, 216, 256, 294, 333, 372];

        for (var row = 0; row < TextX.length; row++) {

            var text_5 = new cjs.Text(TArr[row], "16px 'Myriad Pro'", "#000000");
            text_5.setTransform(TextX[row], TextY[row]);
            TextArr.push(text_5);
        }

        this.text_1 = new cjs.Text("MER än", "14px 'Myriad Pro'");
        this.text_1.setTransform(350, 70);

        this.text_2 = new cjs.Text("1 timme", "14px 'Myriad Pro'");
        this.text_2.setTransform(348, 85);

        this.text_3 = new cjs.Text("MINDRE än", "14px 'Myriad Pro'");
        this.text_3.setTransform(447, 70);

        this.text_4 = new cjs.Text("1 timme", "14px 'Myriad Pro'");
        this.text_4.setTransform(455, 85);

        this.text_6 = new cjs.Text("Jag kan sova hur", "14px 'Myriad Pro'");
        this.text_6.setTransform(227, 525);

        this.text_7 = new cjs.Text("länge som helst …", "14px 'Myriad Pro'");
        this.text_7.setTransform(221, 543);


        this.addChild(this.round_Rect1, this.instance, this.shape_group1, this.shape_group2, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5,
            this.text_6, this.text_7, this.instance1);

        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < TextArr.length; i++) {
            this.addChild(TextArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 580);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(283, 230, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
