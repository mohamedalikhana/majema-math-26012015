(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{

            src: "images/p140_1.png",
            id: "p140_1"
        }, {
            src: "images/p140_2.png",
            id: "p140_2"
        }]
    };

    (lib.p140_1 = function() {
        this.initialize(img.p140_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p140_2 = function() {
        this.initialize(img.p140_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("140", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(33, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Hur mycket kostar sakerna tillsammans?", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.instance = new lib.p140_1();
        this.instance.setTransform(13, 21, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 250, 123, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 145);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(254, 15);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(254, 145);

        var lineArr = [];
        var lineX = [167, 420, 167, 420];
        var lineY = [84, 84, 214, 214];
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(57, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var TxtArr = [];
        var TxtlineX = [212, 463, 212, 463];
        var TxtlineY = [122, 122, 252, 252];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text("kr", "bold 20px 'UusiTekstausMajema'", "9D9C9C");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(this.temp_label);
        }

        this.text_3 = new cjs.Text("40 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_3.setTransform(27, 45);
        this.text_3.skewX = 20;
        this.text_3.skewY = 20;

        this.text_4 = new cjs.Text("30 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_4.setTransform(180, 55);
        this.text_4.skewX = 0;
        this.text_4.skewY = 0;

        this.text_5 = new cjs.Text("60 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_5.setTransform(330, 65);
        this.text_5.skewX = 10;
        this.text_5.skewY = 10;

        this.text_6 = new cjs.Text("30 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_6.setTransform(400, 87);
        this.text_6.skewX = -30;
        this.text_6.skewY = -30;

        this.text_7 = new cjs.Text("40 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_7.setTransform(85, 198);
        this.text_7.skewX = -20;
        this.text_7.skewY = -20;

        this.text_8 = new cjs.Text("40 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_8.setTransform(150, 174);
        this.text_8.skewX = 10;
        this.text_8.skewY = 10;

        this.text_9 = new cjs.Text("50 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_9.setTransform(323, 185);
        this.text_9.skewX = -10;
        this.text_9.skewY = -10;

        this.text_10 = new cjs.Text("40 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_10.setTransform(417, 190);
        this.text_10.skewX = -10;
        this.text_10.skewY = -10;

        this.addChild(this.text, this.text_1, this.instance, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,
            this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 520, 270);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Hur många kan du köpa av varje?", "16px 'Myriad Pro'");
        this.text.setTransform(14, 0);

        this.text_1 = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p140_2();
        this.instance.setTransform(-10, 13, 0.46, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 87, 115, 10);
        this.roundRect1.setTransform(0, 15);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(0, 139);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("").s('#7d7d7d').drawRoundRect(0, 0, 414, 117, 10);
        this.roundRect3.setTransform(90, 15);

        this.roundRect4 = this.roundRect3.clone(true);
        this.roundRect4.setTransform(90, 139);

        var lineArr = [];
        var lineX = [114, 221, 325, 424, 114, 221, 325, 424];
        var lineY = [78, 78, 78, 78, 201, 201, 201, 201];
        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(57, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        var vrlineArr = [];
        var vrlineX = [194, 302, 405, 194, 302, 405];
        var vrlineY = [27, 27, 27, 150, 150, 150];
        for (var row = 0; row < vrlineX.length; row++) {
            var vrLine_1 = new cjs.Shape();
            vrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(0, 97);
            vrLine_1.setTransform(vrlineX[row], vrlineY[row]);
            vrlineArr.push(vrLine_1);
        }

        var TxtArr = [];
        var TxtlineX = [159, 265, 369, 467, 159, 265, 369, 465];
        var TxtlineY = [116, 116, 116, 116, 238, 238, 238, 238];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text("st", "bold 20px 'UusiTekstausMajema'", "9D9C9C");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TxtArr.push(this.temp_label);
        }

        this.text_3 = new cjs.Text("40 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_3.setTransform(155, 48);
        this.text_3.skewX = -30;
        this.text_3.skewY = -30;

        this.text_4 = new cjs.Text("20 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_4.setTransform(257, 63);
        this.text_4.skewX = -10;
        this.text_4.skewY = -10;

        this.text_5 = new cjs.Text("50 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_5.setTransform(328, 45);
        this.text_5.skewX = 10;
        this.text_5.skewY = 10;

        this.text_6 = new cjs.Text("10 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_6.setTransform(410, 47);
        this.text_6.skewX = 10;
        this.text_6.skewY = 10;

        this.text_7 = new cjs.Text("40 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_7.setTransform(155, 172);
        this.text_7.skewX = -30;
        this.text_7.skewY = -30;

        this.text_8 = new cjs.Text("20 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_8.setTransform(257, 187);
        this.text_8.skewX = -10;
        this.text_8.skewY = -10;

        this.text_9 = new cjs.Text("50 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_9.setTransform(331, 167);
        this.text_9.skewX = 10;
        this.text_9.skewY = 10;

        this.text_10 = new cjs.Text("10 kr", "bold 16px 'UusiTekstausMajema'");
        this.text_10.setTransform(410, 170);
        this.text_10.skewX = 10;
        this.text_10.skewY = 10;

        this.addChild(this.text, this.text_1, this.instance, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4,
            this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10);
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }
        for (var i = 0; i < vrlineArr.length; i++) {
            this.addChild(vrlineArr[i]);
        }
        for (var i = 0; i < TxtArr.length; i++) {
            this.addChild(TxtArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 520, 260);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(295, 107, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295, 433, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
