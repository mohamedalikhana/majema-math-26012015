(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };

    // symbols:

    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("80", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(38, 660);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(453 + (columnSpace * 33), 12, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1, 20, 520, 307, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text.setTransform(22, 40);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(4, 40);

        this.text_2 = new cjs.Text("Kluring", "18px 'MyriadPro-Semibold'", "#958FC5");
        this.text_2.setTransform(0, 11);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('#ffffff').s("#958FC5").ss(0.8).drawRect(87, 50, 120, 263);
        this.rect1.setTransform(0, 0);

        this.rect2 = new cjs.Shape();
        this.rect2.graphics.f('#ffffff').s("#958FC5").ss(0.8).drawRect(295, 50, 120, 263);
        this.rect2.setTransform(10, 0);

        var ToBeAdded = [];
        for (var col = 0; col < 7; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");

                temp_text.setTransform(95 + (colSpace * 16.5), 64 + (rowSpace * 18.5));
                ToBeAdded.push(temp_text);
            }
        }

        for (var col = 0; col < 7; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");

                temp_text.setTransform(312 + (colSpace * 16.5), 64 + (rowSpace * 18.5));
                ToBeAdded.push(temp_text);
            }
        }

        this.centerLine = new cjs.Shape();
        this.centerLine.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(87, 181).lineTo(207.5, 181)
        this.centerLine.setTransform(0, 0);

        this.centerLine1 = new cjs.Shape();
        this.centerLine1.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(295, 181).lineTo(415.5, 181)
        this.centerLine1.setTransform(10, 0);

        this.centerGreenLine = new cjs.Shape();
        this.centerGreenLine.graphics.f('#13A538').s("#706F6F").ss(1.2).moveTo(146.3, 135).lineTo(146.3, 174)
            .moveTo(146, 173).lineTo(196.3, 154.3).lineTo(196.2, 134.5).lineTo(178, 136)
            .moveTo(146, 173).lineTo(96, 154).lineTo(96.3, 135).lineTo(113.5, 135)
        this.centerGreenLine.setTransform(0, 0);

        this.yellowLine = new cjs.Shape();
        this.yellowLine.graphics.f('#FFF374').s("#706F6F").ss(1.2).moveTo(140, 135).lineTo(172.7, 117).lineTo(172.7, 60)
            .lineTo(155, 81.4).lineTo(138.2, 60).lineTo(122, 81.4).lineTo(105.5, 60).lineTo(105.5, 117).lineTo(140, 135)
        this.yellowLine.setTransform(7, 0);

        this.blueLine_1 = new cjs.Shape(); // box 1 blue  shape2 008BD2
        this.blueLine_1.graphics.f('#008BD2').s("#706F6F").ss(1.2).moveTo(305, 82).lineTo(324, 62)
            .lineTo(340, 62).lineTo(340, 118).lineTo(375, 118).lineTo(375, 174).lineTo(340, 174).lineTo(324, 153.5).lineTo(324, 82).lineTo(305, 82)
        this.blueLine_1.setTransform(7, 0);

        this.yellowLine_1 = new cjs.Shape();
        this.yellowLine_1.graphics.f('#FFF374').s("#706F6F").ss(1.2).moveTo(362, 118).lineTo(397, 79.5)
            .lineTo(415, 79.5).lineTo(381, 118)
        this.yellowLine_1.setTransform(0, 0);

        this.blueLine_2 = new cjs.Shape();
        this.blueLine_2.graphics.f('#008BD2').s("#706F6F").ss(1.2).moveTo(398, 98.5).lineTo(414, 98.5).lineTo(382, 135).lineTo(382, 118)
        this.blueLine_2.setTransform(0, 0);

        this.redLine_2 = new cjs.Shape();
        this.redLine_2.graphics.f('#D51317').s("#706F6F").ss(1.2).moveTo(398, 117.5).lineTo(414, 117.5).lineTo(382, 155).lineTo(382, 135)
        this.redLine_2.setTransform(0, 0);

        this.yellowLine_2 = new cjs.Shape();
        this.yellowLine_2.graphics.f('#FFF374').s("#706F6F").ss(1.2).moveTo(398, 135).lineTo(414, 135).lineTo(382, 173).lineTo(382, 155)
        this.yellowLine_2.setTransform(0, 0);

        this.textEye = new cjs.Text("•", "bold 18px 'MyriadPro-Semibold'", "#000000");
        this.textEye.setTransform(325, 76);

        this.textEye1 = new cjs.Text("•", "bold 18px 'MyriadPro-Semibold'", "#000000");
        this.textEye1.setTransform(325, 204);

        this.blockLine_1 = new cjs.Shape();
        this.blockLine_1.graphics.f('#ffffff').s("#706F6F").ss(1.2).moveTo(330, 209.2).lineTo(314, 209.2).lineTo(331, 190)
        this.blockLine_1.setTransform(0, 0);



        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.rect1, this.rect2, this.centerLine, this.centerLine1);
        this.addChild(this.centerGreenLine, this.yellowLine, this.blueLine_1, this.yellowLine_1, this.blueLine_2, this.redLine_2, this.yellowLine_2, this.textEye)
        this.addChild(this.blockLine_1, this.textEye1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 510.3, 340.9);

    (lib.Symbol3 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Måla en likadan figur.", "16px 'Myriad Pro'");
        this.text.setTransform(25, 22);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(6, 22);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1.5, 3, 520, 267, 10);
        this.roundRect1.setTransform(0, 0);

        this.blueLine = new cjs.Shape();
        this.blueLine.graphics.f('#54B0E4').s("#000000").ss(1.2).moveTo(30, 90).lineTo(50, 40)
            .lineTo(90, 40).lineTo(70, 90).lineTo(90, 135).lineTo(50, 135).lineTo(30, 90)
        this.blueLine.setTransform(7, 0);

        this.yellowLine = new cjs.Shape();
        this.yellowLine.graphics.f('#FFDD00').s("#000000").ss(1.2).moveTo(45, 215).lineTo(55, 190)
            .lineTo(110, 190).lineTo(120, 215).lineTo(110, 240).lineTo(57, 240).lineTo(45, 215)
        this.yellowLine.setTransform(0, 0);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('#ffffff').s("#000000").ss(0.8).drawRect(130, 40, 90, 100);
        this.rect1.setTransform(0, 0);

        this.hrLine = new cjs.Shape()

        for (var row = 0; row < 3; row++) {
            this.hrLine.graphics.f('#ffffff').s("#000000").ss(0.5).moveTo(130, 65 + (row * 25)).lineTo(220, 65 + (row * 25))

        }
        this.hrLine.setTransform(0, 0)


        this.hrLine3 = new cjs.Shape()
        this.hrLine3.graphics.s("#000000").ss(0.5).moveTo(130, 140).lineTo(190, 40)
            .lineTo(220, 90).lineTo(190, 140).lineTo(130, 40)
            .moveTo(220, 40).lineTo(160, 140).lineTo(130, 90).lineTo(160, 40).lineTo(220, 140)
        this.hrLine3.setTransform(0, 0)

        this.box1 = new cjs.Container();
        this.box1.addChild(this.rect1, this.hrLine, this.hrLine3)
        this.box1.setTransform(0, 0)

        this.box2 = this.box1.clone(true)
        this.box2.setTransform(255, 0)

        this.box3 = this.box1.clone(true)
        this.box3.setTransform(0, 123)

        this.box4 = this.box1.clone(true)
        this.box4.setTransform(255, 123)

        this.redLine = new cjs.Shape()
        this.redLine.graphics.f('#EA5342').s("#000000").ss(0.5).moveTo(290, 40).lineTo(315, 40)
            .lineTo(330, 65).lineTo(350, 40).lineTo(375, 40).lineTo(345, 90).lineTo(375, 140)
            .lineTo(350, 140).lineTo(330, 115).lineTo(315, 140).lineTo(290, 140)
            .lineTo(315, 90).lineTo(290, 40)
        this.redLine.setTransform(0, 0)

        this.greenLine = new cjs.Shape()
        this.greenLine.graphics.f('#8FC472').s("#000000").ss(0.5).moveTo(290, 212).lineTo(315, 165)
            .lineTo(330, 190).lineTo(345, 165).lineTo(370, 212).lineTo(345, 262).lineTo(330, 240).lineTo(315, 262).lineTo(290, 212)
        this.greenLine.setTransform(0, 0)


        this.addChild(this.roundRect1, this.text, this.text_1, this.blueLine, this.yellowLine)
        this.addChild(this.box1, this.box2, this.box3, this.box4, this.redLine, this.greenLine)
    }).prototype = p = new cjs.Container();
    0, 0, 512.3, 320.9
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 240);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297.3, 85, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(297.3, 405, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
