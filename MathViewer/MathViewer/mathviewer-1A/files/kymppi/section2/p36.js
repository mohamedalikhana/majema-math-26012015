(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p36_1.png",
            id: "p36_1"
        }, {
            src: "images/p36_2.png",
            id: "p36_2"
        }]
    };

    (lib.p36_1 = function() {
        this.initialize(img.p36_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p36_2 = function() {
        this.initialize(img.p36_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.customShape1=function(fCTriangle,fCRectangle,text,x,y,scaleX,scaleY)
    {
        var shapeWithText={shape:null,text:null};
        var customShape = new cjs.Shape();
        customShape.graphics.f(fCTriangle).s('#7d7d7d').ss(3.5).moveTo(50, 0).lineTo(70, 0).lineTo(100, 25).lineTo(70, 50).lineTo(50, 50).lineTo(50, 0);
        customShape.graphics.f(fCRectangle).s('#7d7d7d').ss(3.5).moveTo(50, 0).lineTo(0, 0).lineTo(0, 50).lineTo(50, 50);
        var text = new cjs.Text(text, "16px 'Myriad Pro'");
        text.lineHeight = 16;
        text.setTransform(x+3, y+3);
        customShape.setTransform(x, y,scaleX,scaleY);
        shapeWithText.shape=customShape.clone(true);
        shapeWithText.text=text;
        return shapeWithText;
    }).prototype = p = new cjs.Container();


    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("36", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(39, 646);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 658.8,1,1.1);        

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p36_1();
        this.instance.setTransform(100, 18, 0.705, 0.705);
        this.text = new cjs.Text("Måla.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 10);
        
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1037) - 10) * 0.5, 568 * 0.5, 10);
        this.roundRect1.setTransform(0, 34);


        this.customShape1=lib.customShape1("#ffffff","#ffffff","0",27,67+(28*0),0.38,0.38);
        this.customShape2=lib.customShape1("#0099cc","#ffffff","1",27,67+(28*1),0.38,0.38);
        this.customShape3=lib.customShape1("#cc3333","#ffffff","2",27,67+(28*2),0.38,0.38);
        this.customShape4=lib.customShape1("#ffff66","#ffffff","3",27,67+(28*3),0.38,0.38);
        this.customShape5=lib.customShape1("#339933","#ffffff","4",27,67+(28*4),0.38,0.38);
        this.customShape6=lib.customShape1("#cc9933","#ffffff","5",27,67+(28*5),0.38,0.38);


        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 10);
        this.textbox_group1 = new cjs.Shape();
        this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(45, 260, 20, 21).drawRect(145, 260, 20, 21).drawRect(245, 260, 20, 21).drawRect(345, 260, 20, 21).drawRect(438, 260, 20, 21);
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1,this.instance, this.text, this.text_1,this.customShape1.shape,this.customShape1.text,this.customShape2.shape,this.customShape2.text,this.customShape3.shape,this.customShape3.text,this.customShape4.shape,this.customShape4.text,this.customShape5.shape,this.customShape5.text,this.customShape6.shape,this.customShape6.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500.3, 380.2);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p36_2();
        this.instance.setTransform(0, 20, 0.6, 0.6);
        //868*365
        this.text = new cjs.Text("Det är 5 bollar. Hur många bollar är i lådan?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);
        
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((868*0.5) - 3) * 0.59, ((365*0.5)-15) * 0.59, 10);
        this.roundRect1.setTransform(0, 25);
        
        this.roundRect2 =this.roundRect1.clone(true);
        //this.roundRect2.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((868*0.5) - 10) * 0.6, ((365*0.5)-10) * 0.6, 10);
        this.roundRect2.setTransform(((868*0.5) + 5) * 0.6, 25);

        this.roundRect3 =this.roundRect1.clone(true);
        //this.roundRect2.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((868*0.5) - 10) * 0.6, ((365*0.5)-10) * 0.6, 10);
        this.roundRect3.setTransform(0, 135);
        this.roundRect4 =this.roundRect1.clone(true);
        //this.roundRect2.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((868*0.5) - 10) * 0.6, ((365*0.5)-10) * 0.6, 10);
        this.roundRect4.setTransform(((868*0.5) + 5) * 0.6, 135);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            if (column > 1) {
                columnSpace = column + 3.1;
            }
            for (var row = 0; row < 3; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(160 + (columnSpace * 50), 70 + (row * 68), 20, 21);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textBoxGroup2 = new cjs.Shape();
        this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(60, 80, 20, 21).drawRect(60, 195, 20, 21).drawRect(315, 80, 20, 21).drawRect(315, 195, 20, 21);
        this.textBoxGroup2.setTransform(0, 0);
        this.addChild(this.roundRect1,this.roundRect2,this.roundRect3,this.roundRect4,this.instance, this.text, this.text_1, this.textBoxGroup2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75.9);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300.3, 428.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(300.3, 95, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
