(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p71_1.png",
            id: "p71_1"
        }, {
            src: "images/p71_2.png",
            id: "p71_2"
        }]
    };

    (lib.p71_1 = function() {
        this.initialize(img.p71_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p71_2 = function() {
        this.initialize(img.p71_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("24", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(67, 42);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#958FC5").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(56, 23.5, 1.09, 1);

        this.text_2 = new cjs.Text("Vi övar", "24px 'MyriadPro-Semibold'", "#958FC5");
        this.text_2.setTransform(113.5, 42);

        this.text_3 = new cjs.Text("kunna lösa uppgifter i subtraktion 0 till 20, med tiotalsövergång", "9px 'Myriad Pro'");
        this.text_3.setTransform(312, 655);

        this.text = new cjs.Text("71", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(565, 656);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(588, 660.8);

        this.instance = new lib.p71_1();
        this.instance.setTransform(128, 68, 0.46, 0.46);

        this.addChild(this.instance, this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.text_3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Hur många körsbär finns kvar?", "16px 'Myriad Pro'");
        this.text_q2.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#958FC5");
        this.text.setTransform(0, 0);

        this.instance_2 = new lib.p71_2();
        this.instance_2.setTransform(18, 23, 0.46, 0.46);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 10, 255, 116, 10);
        this.roundRect1.setTransform(0, 5);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(260, 5);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(0, 127);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(260, 127);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(0, 249);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(260, 249);

        var ToBeAdded = [];
        var xpos = 20;
        var ypos = 97;
        var tmp_Rect = new cjs.Shape();
        for (var numOfrow = 0; numOfrow < 3; numOfrow++) {
            if (numOfrow == 1) {
                ypos = 218
            } else if (numOfrow == 2) {
                ypos = 338
            } else {
                ypos = 97
            }

            for (var numOfcol = 0; numOfcol < 2; numOfcol++) {
                for (var row = 0; row < 1; row++) {
                    for (var column = 0; column < 6; column++) {
                        var columnSpace = column;
                        if (numOfcol == 1) {
                            xpos = 278
                        } else {
                            xpos = 20
                        }

                        tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 32), 21, 23);
                        ToBeAdded.push(tmp_Rect);
                    }
                }
            }
        }

        var tmptxt = ["Jag tar 7 körsbär.", "Jag tar 8 körsbär.", "Jag tar 9 körsbär."]
        var tmpNum = [12, 14, 15]
        var i = 0;
        var tmpVal = "";
        var ypos = 72;
        for (var column = 0; column < 2; column++) {
            var colSpace = column;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                if (column == 1) {
                    i = 2
                }
                if (column==0 && row==2){i=1}
                if (row == 0) {
                    tmpVal = tmpNum[0]
                }
                if (row == 1) {
                    tmpVal = tmpNum[1]
                }
                if (row == 2) {
                    tmpVal = tmpNum[2]
                }

                var tempText = new cjs.Text(tmptxt[i], "16px 'Myriad Pro'");
                tempText.setTransform(95 + (260 * colSpace), 50 + (120 * rowSpace));
                var tempText1 = new cjs.Text(tmpVal, "16px 'Myriad Pro'");
                if (row == 1 && column == 0) {
                    ypos = 70
                } else if (row == 0 && column == 1) {
                    ypos = 68
                } else if (row == 2 && column == 1) {
                    ypos = 69
                } else {
                    ypos = 72
                }
                tempText1.setTransform(30 + (260 * colSpace), ypos + (120 * rowSpace));
                ToBeAdded.push(tempText, tempText1);
                i++;
            }
        }

        var tmpNum = ['1', '2', '–', '7', '=']
        var j = 0;
        for (var col = 0; col <5; col++) { 
            columnSpace=col;
            var tempText = new cjs.Text(tmpNum[j], "36px 'UusiTekstausMajema'", "#6C7373");
            tempText.setTransform(25 + (columnSpace * 21), 119);
            ToBeAdded.push(tempText);
            j++
        }

        this.addChild(this.text_q2, this.text, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6);
        this.addChild(this.instance_2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(308, 480, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
