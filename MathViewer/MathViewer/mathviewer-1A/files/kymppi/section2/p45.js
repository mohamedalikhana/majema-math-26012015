(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p45_1.png",
            id: "p45_1"
        }, {
            src: "images/p45_2.png",
            id: "p45_2"
        }]
    };
    // symbols:
    (lib.p45_1 = function() {
        this.initialize(img.p45_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p45_2 = function() {
        this.initialize(img.p45_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("45", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500.3, 500);


    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p45_1();
        this.instance.setTransform(0, 20, 0.60, 0.60);
        this.text = new cjs.Text("Hitta vägen med rätt svar.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(22, -4.5);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(3, -4.5);      

        this.labelR11 = new cjs.Text("2 + 1", "16px 'Myriad Pro'");
        this.labelR11.lineHeight = 19;
        this.labelR11.setTransform(67, 49);
        this.labelR12 = new cjs.Text("1 + 4", "16px 'Myriad Pro'");
        this.labelR12.lineHeight = 19;
        this.labelR12.setTransform(67, 97);
        this.labelR13 = new cjs.Text("2 + 2", "16px 'Myriad Pro'");
        this.labelR13.lineHeight = 19;
        this.labelR13.setTransform(67, 145);
        this.labelR14 = new cjs.Text("1 + 3", "16px 'Myriad Pro'");
        this.labelR14.lineHeight = 19;
        this.labelR14.setTransform(67, 193);
        this.labelR15 = new cjs.Text("2 + 3", "16px 'Myriad Pro'");
        this.labelR15.lineHeight = 19;
        this.labelR15.setTransform(67, 241);

        this.labelR21 = new cjs.Text("4 + 1", "16px 'Myriad Pro'");
        this.labelR21.lineHeight = 19;
        this.labelR21.setTransform(157, 49);
        this.labelR22 = new cjs.Text("2 + 3", "16px 'Myriad Pro'");
        this.labelR22.lineHeight = 19;
        this.labelR22.setTransform(157, 97);
        this.labelR23 = new cjs.Text("0 + 4", "16px 'Myriad Pro'");
        this.labelR23.lineHeight = 19;
        this.labelR23.setTransform(157, 145);
        this.labelR24 = new cjs.Text("1 + 1", "16px 'Myriad Pro'");
        this.labelR24.lineHeight = 19;
        this.labelR24.setTransform(157, 193);
        this.labelR25 = new cjs.Text("1 + 3", "16px 'Myriad Pro'");
        this.labelR25.lineHeight = 19;
        this.labelR25.setTransform(157, 241);

        this.labelR31 = new cjs.Text("0 + 5", "16px 'Myriad Pro'");
        this.labelR31.lineHeight = 19;
        this.labelR31.setTransform(252, 49);
        this.labelR32 = new cjs.Text("1 + 2", "16px 'Myriad Pro'");
        this.labelR32.lineHeight = 19;
        this.labelR32.setTransform(252, 97);
        this.labelR33 = new cjs.Text("3 + 1", "16px 'Myriad Pro'");
        this.labelR33.lineHeight = 19;
        this.labelR33.setTransform(252, 145);
        this.labelR34 = new cjs.Text("4 + 0", "16px 'Myriad Pro'");
        this.labelR34.lineHeight = 19;
        this.labelR34.setTransform(252, 193);
        this.labelR35 = new cjs.Text("2 + 2", "16px 'Myriad Pro'");
        this.labelR35.lineHeight = 19;
        this.labelR35.setTransform(252, 241);

        this.labelR41 = new cjs.Text("3 + 2", "16px 'Myriad Pro'");
        this.labelR41.lineHeight = 19;
        this.labelR41.setTransform(341, 49);
        this.labelR42 = new cjs.Text("5 + 0", "16px 'Myriad Pro'");
        this.labelR42.lineHeight = 19;
        this.labelR42.setTransform(341, 97);
        this.labelR43 = new cjs.Text("1 + 0", "16px 'Myriad Pro'");
        this.labelR43.lineHeight = 19;
        this.labelR43.setTransform(341, 145);
        this.labelR44 = new cjs.Text("3 + 2", "16px 'Myriad Pro'");
        this.labelR44.lineHeight = 19;
        this.labelR44.setTransform(341, 193);
        this.labelR45 = new cjs.Text("4 + 1", "16px 'Myriad Pro'");
        this.labelR45.lineHeight = 19;
        this.labelR45.setTransform(341, 241);

        this.labelR51 = new cjs.Text("0 + 1", "16px 'Myriad Pro'");
        this.labelR51.lineHeight = 19;
        this.labelR51.setTransform(436, 49);
        this.labelR52 = new cjs.Text("2 + 3", "16px 'Myriad Pro'");
        this.labelR52.lineHeight = 19;
        this.labelR52.setTransform(436, 97);
        this.labelR53 = new cjs.Text("1 + 4", "16px 'Myriad Pro'");
        this.labelR53.lineHeight = 19;
        this.labelR53.setTransform(436, 145);
        this.labelR54 = new cjs.Text("0 + 5", "16px 'Myriad Pro'");
        this.labelR54.lineHeight = 19;
        this.labelR54.setTransform(436, 193);
        this.labelR55 = new cjs.Text("3 + 1", "16px 'Myriad Pro'");
        this.labelR55.lineHeight = 19;
        this.labelR55.setTransform(436, 241);


        this.addChild(this.instance, this.text, this.text_1,this.labelR11,this.labelR12,this.labelR13,this.labelR14,
            this.labelR15,this.labelR21,this.labelR22,this.labelR23,this.labelR24,this.labelR25,this.labelR31,this.labelR32,
            this.labelR33,this.labelR34,this.labelR35,this.labelR41,this.labelR42,this.labelR43,this.labelR44,this.labelR45,
            this.labelR51,this.labelR52,this.labelR53,this.labelR54,this.labelR55);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p45_2();
        this.instance.setTransform(-2, 24, 0.74, 0.74);

        this.text = new cjs.Text("Mira och Manuel får samma resultat. Rita prickar.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(24, 6);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(5, 6);

        this.img_Rect1 = new cjs.Shape();
        this.img_Rect1.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 30, 252, 91.5, 10);
        this.img_Rect1.setTransform(0, 0);

        this.img_Rect2 = new cjs.Shape();
        this.img_Rect2.graphics.s("#D0CECE").ss(1).drawRoundRect(264, 30, 252, 91.5, 10);
        this.img_Rect2.setTransform(0, 0);

        this.img_Rect3 = new cjs.Shape();
        this.img_Rect3.graphics.s("#D0CECE").ss(1).drawRoundRect(6, 130.5, 252, 91.5, 10);
        this.img_Rect3.setTransform(0, 0);

        this.img_Rect4 = new cjs.Shape();
        this.img_Rect4.graphics.s("#D0CECE").ss(1).drawRoundRect(264, 130.5, 252, 91.5, 10);
        this.img_Rect4.setTransform(0, 0);
       
        this.addChild(this.instance, this.text, this.text_1,this.img_Rect1,this.img_Rect2,
            this.img_Rect3,this.img_Rect4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);       

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(310, 120, 1, 1, 0, 0, 0, 254.6, 53.5);

         this.v2 = new lib.Symbol3();
        this.v2.setTransform(307.3, 451, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
