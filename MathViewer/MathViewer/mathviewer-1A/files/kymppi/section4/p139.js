(function(lib, img, cjs) {

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p139_1.png",
            id: "p139_1"
        }, {
            src: "images/p139_2.png",
            id: "p139_2"
        }]
    };

    (lib.p139_1 = function() {
        this.initialize(img.p139_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p139_2 = function() {
        this.initialize(img.p139_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);




    (lib.Symbol4 = function() {
        this.initialize();

        this.text_4 = new cjs.Text("139", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(552, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(580, 23).lineTo(0, 23).moveTo(586, 30).lineTo(586, 660).moveTo(590, 656).lineTo(0, 656);
        this.shape_3.setTransform(0, 0);
        this.shapeArc1 = new cjs.Shape();
        this.shapeArc1.graphics.s("#FAAA33").ss(2).arc(576, 33, 10, 3 * Math.PI / 2, 0);
        this.shapeArc1.setTransform(0, 0);
        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(587, 0, 23, 660).drawRect(0, 0, 610, 23).drawRect(0, 657, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.addChild(this.shape_4, this.shape_3, this.shapeArc1, this.shape_2, this.shape, this.text_Rect, this.text_4, this.text_3, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.instance = new lib.p139_1();
        this.instance.setTransform(0, 0, 0.62, 0.62);

        this.instance_2 = new lib.p139_2();
        this.instance_2.setTransform(47, 333, 0.44, 0.44);

        this.hrRule1 = new cjs.Shape();
        this.hrRule1.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(23, 46).lineTo(23, 330);
        this.hrRule1.setTransform(1, 0);

        this.hrRule2 = new cjs.Shape();
        this.hrRule2.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(23, 46).lineTo(23, 330);
        this.hrRule2.setTransform(1, 0);

        //Arrow
        this.hrRule3 = new cjs.Shape();
        this.hrRule3.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(23.5, 45.5).lineTo(27, 51);
        this.hrRule3.setTransform(1, 0);

        this.hrRule4 = new cjs.Shape();
        this.hrRule4.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(23, 45).lineTo(19, 50.5);
        this.hrRule4.setTransform(1, 0);

        this.hrRule5 = new cjs.Shape();
        this.hrRule5.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(23.5, 45.5).lineTo(27, 51);
        this.hrRule5.setTransform(1, 0);

        this.hrRule6 = new cjs.Shape();
        this.hrRule6.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(23, 45).lineTo(19, 50.5);
        this.hrRule6.setTransform(1, 0);
        //Arrow End

        //Arrow
        this.hrRule7 = new cjs.Shape();
        this.hrRule7.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(521.5, 326).lineTo(525, 330);
        this.hrRule7.setTransform(1, 0);

        this.hrRule8 = new cjs.Shape();
        this.hrRule8.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(521.5, 326).lineTo(525, 330);
        this.hrRule8.setTransform(1, 0);

        this.hrRule9 = new cjs.Shape();
        this.hrRule9.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(521, 333.5).lineTo(525, 330);
        this.hrRule9.setTransform(1, 0);

        this.hrRule10 = new cjs.Shape();
        this.hrRule10.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(521, 333.5).lineTo(525, 330);
        this.hrRule10.setTransform(1, 0);
        //Arrow 


        this.hrRule11 = new cjs.Shape();
        this.hrRule11.graphics.f("#959595").beginStroke("#959595").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(520, 50);
        this.hrRule11.setTransform(5, 10);


        var lineArr = [];
        for (var i = 0; i < 16; i++) {
            if (i == 0) {
                this.hrRuleNew = new cjs.Shape();
                this.hrRuleNew.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(520, 50);
                this.hrRuleNew.setTransform(5, 10 + (i * 18));
                lineArr.push(this.hrRuleNew);
            } else {

                this.hrRuleNew = new cjs.Shape();
                this.hrRuleNew.graphics.f("#B2B2B2").beginStroke("#B2B2B2").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(520, 50);
                this.hrRuleNew.setTransform(5, 10 + (i * 18));
                lineArr.push(this.hrRuleNew);
            }
            if (i == 5 || i == 10 || i == 15) {
                this.hrRuleNew = new cjs.Shape();
                this.hrRuleNew.graphics.f("#000000").beginStroke("#000000").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(520, 50);
                this.hrRuleNew.setTransform(5, 10 + (i * 18));
                lineArr.push(this.hrRuleNew);
            }

            if (!((i == 4) || (i == 9) || (i == 14) || (i == 15))) {
                this.hrRule15 = new cjs.Shape();
                this.hrRule15.graphics.f("#646464").beginStroke("#646464").setStrokeStyle(0.5).moveTo(14.6, 60).lineTo(42, 60);
                this.hrRule15.setTransform(36, 18 + (i * 18));
                lineArr.push(this.hrRule15);

                this.hrRule15 = new cjs.Shape();
                this.hrRule15.graphics.f("#646464").beginStroke("#646464").setStrokeStyle(0.5).moveTo(14.6, 60).lineTo(42, 60);
                this.hrRule15.setTransform(121, 18 + (i * 18));
                lineArr.push(this.hrRule15);

                this.hrRule15 = new cjs.Shape();
                this.hrRule15.graphics.f("#646464").beginStroke("#646464").setStrokeStyle(0.5).moveTo(14.6, 60).lineTo(43, 60);
                this.hrRule15.setTransform(206, 18 + (i * 18));
                lineArr.push(this.hrRule15);

                this.hrRule15 = new cjs.Shape();
                this.hrRule15.graphics.f("#646464").beginStroke("#646464").setStrokeStyle(0.5).moveTo(14.6, 60).lineTo(42, 60);
                this.hrRule15.setTransform(292, 18 + (i * 18));
                lineArr.push(this.hrRule15);

                this.hrRule15 = new cjs.Shape();
                this.hrRule15.graphics.f("#646464").beginStroke("#646464").setStrokeStyle(0.5).moveTo(14.6, 60).lineTo(43, 60);
                this.hrRule15.setTransform(377, 18 + (i * 18));
                lineArr.push(this.hrRule15);

                this.hrRule15 = new cjs.Shape();
                this.hrRule15.graphics.f("#646464").beginStroke("#646464").setStrokeStyle(0.5).moveTo(14.6, 60).lineTo(42.3, 60);
                this.hrRule15.setTransform(463, 18 + (i * 18));
                lineArr.push(this.hrRule15);
            }


        }


        for (var i = 0; i < 17; i++) {
            if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14) {
                continue;
            }
            this.hrRule16 = new cjs.Shape();
            this.hrRule16.graphics.f("#646464").beginStroke("#646464").setStrokeStyle(0.6).moveTo(50, 60).lineTo(50, 330);
            this.hrRule16.setTransform(0 + (i * 28.5), 0);
            lineArr.push(this.hrRule16);
        }

        this.numbertext = new cjs.Text("15", "12px 'Myriad Pro'");
        this.numbertext.lineHeight = -1;
        this.numbertext.setTransform(5, 54.5);
        lineArr.push(this.numbertext);
        this.numbertext = new cjs.Text("10", "12px 'Myriad Pro'");
        this.numbertext.lineHeight = -1;
        this.numbertext.setTransform(5, 144);
        lineArr.push(this.numbertext);
        this.numbertext = new cjs.Text("5", "12px 'Myriad Pro'");
        this.numbertext.lineHeight = -1;
        this.numbertext.setTransform(11, 234);
        lineArr.push(this.numbertext);
        this.numbertext = new cjs.Text("0", "12px 'Myriad Pro'");
        this.numbertext.lineHeight = 19;
        this.numbertext.setTransform(11, 324);
        lineArr.push(this.numbertext);

        this.addChild(this.instance, this.instance_2, this.hrRule1, this.hrRule2, this.hrRule3, this.hrRule4, this.hrRule5, this.hrRule6, this.hrRule7, this.hrRule8, this.hrRule9, this.hrRule10, this.hrRule11,
            this.hrRuleNew);

        for (var j = 0; j < lineArr.length; j++) {
            this.addChild(lineArr[j]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 360);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("Skriv 2 frågor där svaret finns i diagrammet.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(1, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FBAA34').drawRoundRect(2, 5, 508, 177, 10);
        this.roundRect1.setTransform(0, 20);

        this.text_2 = new cjs.Text("Ställ dina frågor till en kamrat.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(1, 210);

        this.hrRuleNew = new cjs.Shape();
        this.hrRuleNew.graphics.f("#959595").beginStroke("#959595").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(473, 50);
        this.hrRuleNew.setTransform(7, 14);

        this.hrRuleNew1 = new cjs.Shape();
        this.hrRuleNew1.graphics.f("#959595").beginStroke("#959595").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(473, 50);
        this.hrRuleNew1.setTransform(7, 53);

        this.hrRuleNew2 = new cjs.Shape();
        this.hrRuleNew2.graphics.f("#959595").beginStroke("#959595").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(473, 50);
        this.hrRuleNew2.setTransform(7, 94);

        this.hrRuleNew3 = new cjs.Shape();
        this.hrRuleNew3.graphics.f("#959595").beginStroke("#959595").setStrokeStyle(0.5).moveTo(19.5, 50).lineTo(473, 50);
        this.hrRuleNew3.setTransform(7, 133);

        this.addChild(this.text_1, this.roundRect1, this.text_2, this.hrRuleNew, this.hrRuleNew1, this.hrRuleNew2, this.hrRuleNew3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 220);

    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310, 210, 1, 1, 0, 0, 0, 278.3, 176.6);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(309, 530, 1, 1, 0, 0, 0, 256.3, 120.6);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
