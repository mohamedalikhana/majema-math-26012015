(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p114_1.png",
            id: "p114_1"
        }]
    };

    (lib.p114_1 = function() {
        this.initialize(img.p114_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("114", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 661);

        this.instance = new lib.p114_1();
        this.instance.setTransform(44, 0, 0.465, 0.465);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(460 + (columnSpace * 32), 27, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);
        //draw arrow line
        var ToBeAdded = [];
        var lineToypos = 36
        var ypos = 136;
        var temp_hrLine1 = new cjs.Shape();
        var temp_vrLine1 = new cjs.Shape();
        for (var col = 0; col < 51; col++) {
            temp_hrLine1.graphics.s("#000000").f("#000000").ss(0.1).moveTo(60, (43 + ypos)).lineTo(520, (43 + ypos))
                .moveTo(520, (43 + ypos)).lineTo(517, (39 + ypos)).lineTo(526, (43 + ypos)).lineTo(517, (47 + ypos))
            if (col == 0 || col == 5 || col == 10 || col == 15 || col == 20 || col == 25 || col == 30 || col == 35 || col == 40 || col == 45 || col == 50) {
                lineToypos = 33
            } else {
                lineToypos = 36
            }
            temp_vrLine1.graphics.s("#000000").ss(0.5).moveTo(70 + (col * 8.7), (43 + ypos))
                .lineTo(70 + (col * 8.7), (lineToypos + ypos));
            ToBeAdded.push(temp_hrLine1, temp_vrLine1)
        }
        //set number below arrow
        var fontColor = ""
        var bottomNum = [27, 30, 35, 40, 45, 50]

        var j = 0;
        var xpos = 66
        var tmpxpos = 43
        for (var col = 0; col < 11; col++) {
            if (col == 0 || col == 2 || col == 4 || col == 6 || col == 8 || col == 10) {
                fontColor = "#00ABDC"
            } else {
                fontColor = "#000000"
            }
            if (col > 5) {
                tmpxpos = 43
            }else if (col > 1) {
                tmpxpos = 42.3}

             else {
                tmpxpos = 43
            }
            var txtNum = new cjs.Text(j, "16px 'MyriadPro-Semibold'", fontColor);
            txtNum.setTransform(xpos + (col * tmpxpos), 167 + (row * 30));
            ToBeAdded.push(txtNum)
            j = j + 5;
        }


        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol6 = function() {
        this.initialize();


        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Jämför. Skriv > eller <.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 102, 10);
        this.roundRect1.setTransform(0, 0);


        var ToBeAdded = [];
        var xpos = 80;
        var ypos = 25;
        var tmpnum = [20, 30, 42, 20, 36, 44, 25, 33, 40]
        var tmpnum1 = [19, 33, 44, 21, 33, 43, 33, 39, 50]
        var i = 0
        var j = 0
        var xpos = 58
        var txtAftrRect = 85
        var txtBfrRect = 38
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                var tmp_Rect = new cjs.Shape();
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 160), ypos + (row * 29), 21, 23);
                if (column == 1) {
                    txtBfrRect = 195;
                    txtAftrRect = 245
                } else if (column == 2) {
                    txtBfrRect = 355;
                    txtAftrRect = 405
                } else {
                    txtBfrRect = 38;
                    txtAftrRect = 85
                }
                var tempText = new cjs.Text(tmpnum[i], "16px 'Myriad Pro'");
                tempText.setTransform(txtBfrRect, 40 + (row * 30));
                var tempText1 = new cjs.Text(tmpnum1[j], "16px 'Myriad Pro'");
                tempText1.setTransform(txtAftrRect, 40 + (row * 30));
                ToBeAdded.push(tmp_Rect, tempText, tempText1);
                i++
                j++
            }
        }


        this.addChild(this.roundRect1, this.roundRect2, this.text, this.text_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 236.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text(" Skriv talen i storleksordning.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 260, 10);
        this.roundRect1.setTransform(0, 0);
        //harizantal 2 lines
        var ToBeAdded = [];
        for (var row = 0; row < 2; row++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(7, 98 + (row * 87)).lineTo(505, 98 + (row * 87));
            ToBeAdded.push(hrLine)
        };

        //Vertical lines
        var moveToypos = 17;
        var lineToypos = 95;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 2; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 103;
                    lineToypos = 180;
                } else if (row == 2) {
                    moveToypos = 190;
                    lineToypos = 268;
                } else {
                    moveToypos = 17;
                    lineToypos = 95;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(170 + (col * 170), moveToypos).lineTo(170 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }

        //color Rect box
        var xpos = 30;
        var ypos = 25;
        fColorBlue = '#C4E5F0';
        sColorBlue = '#00A3C4';
        fColorPink = '#F3CAD3';
        sColorPink = '#958FC5';
        fColorGreen = '#E1EBC5';
        sColorGreen = '#90BD22';
        fColorOrng = '#FBD3B9';
        sColorOrng = '#EB5B1B';
        fColorViolet = '#DFDDF0';
        sColorViolet = '#958FC5';
        var fColor = "";
        var sColor = "";
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 9; column++) {
                var columnSpace = column;
                if (column > 5) {
                    xpos = 120
                } else if (column > 2) {
                    xpos = 75
                } else {
                    xpos = 30
                }
                if (row == 0 && column > 5) {
                    fColor = fColorGreen;
                    sColor = sColorGreen
                } else if (row == 0 && column > 2) {
                    fColor = fColorBlue;
                    sColor = sColorBlue
                } else if (row == 0 && column < 3) {
                    fColor = fColorPink;
                    sColor = sColorPink
                } else if (row == 1 && column > 5) {
                    fColor = fColorPink;
                    sColor = sColorPink
                } else if (row == 1 && column > 2) {
                    fColor = fColorOrng;
                    sColor = sColorOrng
                } else if (row == 1 && column < 3) {
                    fColor = fColorViolet;
                    sColor = sColorViolet
                } else if (row == 2 && column > 5) {
                    fColor = fColorOrng;
                    sColor = sColorOrng
                } else if (row == 2 && column > 2) {
                    fColor = fColorGreen;
                    sColor = sColorGreen
                } else if (row == 2 && column < 3) {
                    fColor = fColorBlue;
                    sColor = sColorBlue
                }
                tmp_Rect.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xpos + (columnSpace * 42), ypos + (row * 85), 23, 24);
                ToBeAdded.push(tmp_Rect);
            }
        }

        //Draw small lines
        var moveToxpos = 30
        var lintToxpos = 60

        var hrLine1 = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 9; col++) {
                if (col > 5) {
                    moveToxpos = 100;
                    lintToxpos = 130
                } else if (col > 2) {
                    moveToxpos = 70;
                    lintToxpos = 100
                } else {
                    moveToxpos = 30;
                    lintToxpos = 60
                }
                hrLine1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(moveToxpos + (col * 46), 90 + (row * 87)).lineTo(lintToxpos + (col * 46), 90 + (row * 87));
                ToBeAdded.push(hrLine1)
            }
        }

//set numbers
        var tmpnum = [9,19,10,15,5,10,22,20,2,33,30,3,23,32,20,33,23,43,40,4,44,30,50,40,45,50,40]        
        var i = 0     
        var xpos = 33             
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 9; column++) {
                var columnSpace = column;  
                var tempText = new cjs.Text(tmpnum[i], "16px 'Myriad Pro'");
                if(row==0 && column==0){xpos=35}
                    else if(row==2 && column==1){xpos=38}
                        else if(column>5){xpos=126}
                            else if(column>2){xpos=80}  
                    else {xpos=33}

                         if (row==0 && column==4){xpos=85}
                       if(row==0 && column==8){xpos=130}
                        if(row==1 && column==2){xpos=38}
                        if(column>6){xpos1=90}else {xpos1=65}                           
                
                tempText.setTransform(xpos+(column*41.5), 42 + (row * 85));
                // var tempText1 = new cjs.Text("<", "16px 'Myriad Pro'");
                // tempText1.setTransform(xpos1+(column*45), 83 + (row * 87));
                ToBeAdded.push(tempText);
                i++               
            }
        }
        var xpos1=65 
        for(var row=0;row<3;row++){
            for(col=0;col<6;col++){
                if(col>3){xpos1=225}
                else if(col>1){xpos1=150}
                    else{xpos1=65}
                 var tempText1 = new cjs.Text("<", "16px 'Myriad Pro'");               
                tempText1.setTransform(xpos1+(col*45), 83 + (row * 87));
                ToBeAdded.push(tempText1);
            }
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.Line_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(300.5, 440, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300.5, 365, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
