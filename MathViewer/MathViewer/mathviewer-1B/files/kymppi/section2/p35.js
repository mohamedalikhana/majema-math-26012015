(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p35_1.png",
            id: "p35_1"
        }, {
            src: "images/p35_2.png",
            id: "p35_2"
        }]
    };

    (lib.p35_1 = function() {
        this.initialize(img.p35_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p35_2 = function() {
        this.initialize(img.p35_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("35", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(555, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.instance = new lib.p35_1();
        this.instance.setTransform(37, 33, 0.466, 0.466);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(455 + (columnSpace * 33), 28, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Rita cirklar och addera.", "16px 'Myriad Pro'");
        this.text_q2.setTransform(24, 1);

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#F1662B");
        this.text.setTransform(4, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 15, 252.7, 86, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 15, 252.7, 86, 10);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 106, 252.7, 86, 10);
        this.roundRect3.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 106, 252.7, 86, 10);
        this.roundRect4.setTransform(0, 0);

        this.roundRect5 = new cjs.Shape();
        this.roundRect5.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 197, 252.7, 86, 10);
        this.roundRect5.setTransform(0, 0);

        this.roundRect6 = new cjs.Shape();
        this.roundRect6.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 197, 252.7, 86, 10);
        this.roundRect6.setTransform(0, 0);

        this.roundRect7 = new cjs.Shape();
        this.roundRect7.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 288, 252.7, 86, 10);
        this.roundRect7.setTransform(0, 0);

        // draw first recangle boxs right and left

        this.textbox_group1 = new cjs.Shape();
        this.textbox_group2 = new cjs.Shape();
        this.textbox_group3 = new cjs.Shape();
        this.textbox_group4 = new cjs.Shape();
        var xPos = 15,
            ypos = 28.5,
            yPos1 = 48.5
        rectWidth = 100,
            rectHeight = 40.5,
            boxWidth = 20,
            boxHeight = 20,
            numberOfBoxes = 24
        this.textbox_group1.graphics.s("#707070").ss(0.4).drawRect(xPos, ypos, rectWidth, rectHeight);
        this.textbox_group3.graphics.s("#707070").ss(0.4).moveTo(xPos, yPos1).lineTo(115, yPos1);

        for (var hrLine = 0; hrLine < 3; hrLine++) {
            this.textbox_group4.graphics.s("#707070").ss(0.4).moveTo(135, ypos + (boxHeight * hrLine)).lineTo(235, ypos + (boxHeight * hrLine));
            this.textbox_group4.graphics.s("#707070").ss(0.4).moveTo(272, ypos + (boxHeight * hrLine)).lineTo(372, ypos + (boxHeight * hrLine));
            this.textbox_group4.graphics.s("#707070").ss(0.4).moveTo(392, ypos + (boxHeight * hrLine)).lineTo(492, ypos + (boxHeight * hrLine));
        }

        for (var j = 1; j < numberOfBoxes; j++) {
            if (j > 11) {
                xPos = 32,
                    ypos = 28.5,
                    yPos1 = 48.5

            }

            this.textbox_group1.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), ypos).lineTo(xPos + (boxWidth * j), ypos + boxHeight);
            this.textbox_group2.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), yPos1).lineTo(xPos + (boxWidth * j), yPos1 + boxHeight);

        }
        this.textbox_group1.setTransform(0, 0);

        // draw second recangle boxs right and left

        this.textbox_group5 = new cjs.Shape();
        this.textbox_group6 = new cjs.Shape();
        this.textbox_group7 = new cjs.Shape();
        this.textbox_group8 = new cjs.Shape();
        var xPos = 15,
            ypos = 118.5,
            yPos1 = 138.5
        rectWidth = 100,
            rectHeight = 40.5,
            boxWidth = 20,
            boxHeight = 20,
            numberOfBoxes = 24
        this.textbox_group5.graphics.s("#707070").ss(0.4).drawRect(xPos, ypos, rectWidth, rectHeight);
        this.textbox_group7.graphics.s("#707070").ss(0.4).moveTo(xPos, yPos1).lineTo(115, yPos1);

        for (var hrLine = 0; hrLine < 3; hrLine++) {
            this.textbox_group8.graphics.s("#707070").ss(0.4).moveTo(135, ypos + (boxHeight * hrLine)).lineTo(235, ypos + (boxHeight * hrLine));
            this.textbox_group8.graphics.s("#707070").ss(0.4).moveTo(272, ypos + (boxHeight * hrLine)).lineTo(372, ypos + (boxHeight * hrLine));
            this.textbox_group8.graphics.s("#707070").ss(0.4).moveTo(392, ypos + (boxHeight * hrLine)).lineTo(492, ypos + (boxHeight * hrLine));
        }

        for (var j = 1; j < numberOfBoxes; j++) {
            if (j > 11) {
                xPos = 32,
                    ypos = 118.5,
                    yPos1 = 138.5

            }

            this.textbox_group5.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), ypos).lineTo(xPos + (boxWidth * j), ypos + boxHeight);
            this.textbox_group6.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), yPos1).lineTo(xPos + (boxWidth * j), yPos1 + boxHeight);

        }
        this.textbox_group5.setTransform(0, 0);

        // draw third recangle boxs right and left

        this.textbox_group9 = new cjs.Shape();
        this.textbox_group10 = new cjs.Shape();
        this.textbox_group11 = new cjs.Shape();
        this.textbox_group12 = new cjs.Shape();
        var xPos = 15,
            ypos = 208.5,
            yPos1 = 228.5
        rectWidth = 100,
            rectHeight = 40.5,
            boxWidth = 20,
            boxHeight = 20,
            numberOfBoxes = 24
        this.textbox_group9.graphics.s("#707070").ss(0.4).drawRect(xPos, ypos, rectWidth, rectHeight);
        this.textbox_group11.graphics.s("#707070").ss(0.4).moveTo(xPos, yPos1).lineTo(115, yPos1);

        for (var hrLine = 0; hrLine < 3; hrLine++) {
            this.textbox_group12.graphics.s("#707070").ss(0.4).moveTo(135, ypos + (boxHeight * hrLine)).lineTo(235, ypos + (boxHeight * hrLine));
            this.textbox_group12.graphics.s("#707070").ss(0.4).moveTo(272, ypos + (boxHeight * hrLine)).lineTo(372, ypos + (boxHeight * hrLine));
            this.textbox_group12.graphics.s("#707070").ss(0.4).moveTo(392, ypos + (boxHeight * hrLine)).lineTo(492, ypos + (boxHeight * hrLine));
        }

        for (var j = 1; j < numberOfBoxes; j++) {
            if (j > 11) {
                xPos = 32,
                    ypos = 208.5,
                    yPos1 = 228.5

            }

            this.textbox_group9.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), ypos).lineTo(xPos + (boxWidth * j), ypos + boxHeight);
            this.textbox_group10.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), yPos1).lineTo(xPos + (boxWidth * j), yPos1 + boxHeight);

        }
        this.textbox_group5.setTransform(0, 0);

        // draw fourth recangle boxs right and left

        this.textbox_group13 = new cjs.Shape();
        this.textbox_group14 = new cjs.Shape();
        this.textbox_group15 = new cjs.Shape();
        this.textbox_group16 = new cjs.Shape();
        var xPos = 15,
            ypos = 298.5,
            yPos1 = 318.5
        rectWidth = 100,
            rectHeight = 40.5,
            boxWidth = 20,
            boxHeight = 20,
            numberOfBoxes = 12
        this.textbox_group13.graphics.s("#707070").ss(0.4).drawRect(xPos, ypos, rectWidth, rectHeight);
        this.textbox_group15.graphics.s("#707070").ss(0.4).moveTo(xPos, yPos1).lineTo(115, yPos1);

        for (var hrLine = 0; hrLine < 3; hrLine++) {
            this.textbox_group16.graphics.s("#707070").ss(0.4).moveTo(135, ypos + (boxHeight * hrLine)).lineTo(235, ypos + (boxHeight * hrLine));

        }

        for (var j = 1; j < numberOfBoxes; j++) {
            if (j > 11) {
                xPos = 32,
                    ypos = 298.5,
                    yPos1 = 318.5

            }

            this.textbox_group13.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), ypos).lineTo(xPos + (boxWidth * j), ypos + boxHeight);
            this.textbox_group14.graphics.s("#707070").ss(0.4).moveTo(xPos + (boxWidth * j), yPos1).lineTo(xPos + (boxWidth * j), yPos1 + boxHeight);

        }
        this.textbox_group5.setTransform(0, 0);

        //Draw Balla left

        this.shape_group1 = new cjs.Shape();
        var xpos = 25;
        var ypos = 38;
        var fillcolor = "";
        var ssVal = 0.5;

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 8; row++) {
                if (row == 1) {
                    xpos = 25;
                    ypos = 44;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 25;
                    ypos = 38;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#FFFFFF";
                    ssVal = 0.8;
                }
                if (row == 1 && column == 3) {
                    fillcolor = "#FFFFFF";
                    ssVal = 0.8;
                }
                if (row == 2) {
                    ypos = 100;
                    fillcolor = "#D51317";
                }
                if (row == 3) {
                    ypos = 107;
                    fillcolor = "#D51317";
                }
                if (row == 4) {
                    ypos = 162;
                    fillcolor = "#D51317";
                }
                if (row == 5) {
                    ypos = 169;
                    fillcolor = "#D51317";
                }
                if (row == 6) {
                    ypos = 224;
                    fillcolor = "#D51317";
                }
                if (row == 7) {
                    ypos = 231;
                    fillcolor = "#D51317";
                }
                if (row == 3 && column >= 3) {
                    continue;
                }
                if (row == 5 && column >= 3) {
                    continue;
                }
                if (row == 7 && column >= 3) {
                    continue;
                }


                this.shape_group1.graphics.f(fillcolor).s("#6E6E70").ss(ssVal, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group1.setTransform(0, 0);
        //Draw Balla right
        this.shape_group2 = new cjs.Shape();
        var xpos = 282;
        var ypos = 38;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 6; row++) {
                if (row == 1) {
                    xpos = 282;
                    ypos = 44;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 282;
                    ypos = 38;
                    fillcolor = "#D51317";
                }


                if (row == 2) {
                    ypos = 100;
                    fillcolor = "#D51317";
                }
                if (row == 3) {
                    ypos = 107;
                    fillcolor = "#D51317";
                }
                if (row == 4) {
                    ypos = 162;
                    fillcolor = "#D51317";
                }
                if (row == 5) {
                    ypos = 169;
                    fillcolor = "#D51317";
                }
                if (row == 6) {
                    ypos = 224;
                    fillcolor = "#D51317";
                }

                if (row == 1 && column >= 3) {
                    continue;
                }
                if (row == 3 && column >= 3) {
                    continue;
                }
                if (row == 5 && column >= 3) {
                    continue;
                }



                this.shape_group2.graphics.f(fillcolor).s("#6E6E70").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group2.setTransform(0, 0);

        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f("#FFFFFF").s("#6E6E70").ss(0.5, 0, 0, 4).arc(145, 38, 7, 0, 2 * Math.PI);
        this.shape_group3.setTransform(0, 0);


        this.instance = new lib.p35_2();
        this.instance.setTransform(280, 293, 0.466, 0.466);

        this.text_1 = new cjs.Text("Ser du att", "16px 'Myriad Pro'");
        this.text_1.setTransform(325, 315);

        this.text_2 = new cjs.Text("8 + 9 = 9 + 8? ", "16px 'Myriad Pro'");
        this.text_2.setTransform(315, 335);

        this.text_3 = new cjs.Text("8 + 3 =", "16px 'Myriad Pro'");
        this.text_3.setTransform(80, 87);
        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.s("##707070").ss(0.4).moveTo(130, 91).lineTo(170, 91);


        this.text_4 = new cjs.Text("8 + 5 =", "16px 'Myriad Pro'");
        this.text_4.setTransform(338, 87);
        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.s("##707070").ss(0.4).moveTo(388, 91).lineTo(430, 91);

        this.text_5 = new cjs.Text("8 + 4 =", "16px 'Myriad Pro'");
        this.text_5.setTransform(80, 178);
        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.s("##707070").ss(0.4).moveTo(130, 183).lineTo(170, 183);

        this.text_6 = new cjs.Text("8 + 6 =", "16px 'Myriad Pro'");
        this.text_6.setTransform(338, 178);
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.s("##707070").ss(0.4).moveTo(388, 183).lineTo(430, 183);

        this.text_7 = new cjs.Text("8 + 8 =", "16px 'Myriad Pro'");
        this.text_7.setTransform(80, 268);
        this.hrLine_5 = new cjs.Shape();
        this.hrLine_5.graphics.s("##707070").ss(0.4).moveTo(130, 275).lineTo(170, 275);

        this.text_8 = new cjs.Text("8 + 7 =", "16px 'Myriad Pro'");
        this.text_8.setTransform(338, 268);
        this.hrLine_6 = new cjs.Shape();
        this.hrLine_6.graphics.s("##707070").ss(0.4).moveTo(388, 275).lineTo(430, 275);

        this.text_9 = new cjs.Text("8 + 9 =", "16px 'Myriad Pro'");
        this.text_9.setTransform(80, 358);
        this.hrLine_7 = new cjs.Shape();
        this.hrLine_7.graphics.s("##707070").ss(0.4).moveTo(130, 367).lineTo(170, 367);




        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.roundRect7, this.text_q1, this.text_q2);
        this.addChild(this.instance, this.text_1, this.text_2, this.text, this.textbox_group1, this.textbox_group2, this.textbox_group3, this.textbox_group4);
        this.addChild(this.textbox_group5, this.textbox_group6, this.textbox_group7, this.textbox_group8, this.textbox_group9, this.textbox_group10, this.textbox_group11, this.textbox_group12);
        this.addChild(this.textbox_group13, this.textbox_group14, this.textbox_group15, this.textbox_group16, this.shape_group1, this.shape_group2, this.shape_group3);
        this.addChild(this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9);
        this.addChild(this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4, this.hrLine_5, this.hrLine_6, this.hrLine_7);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 370.6);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(610.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(306, 467, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
