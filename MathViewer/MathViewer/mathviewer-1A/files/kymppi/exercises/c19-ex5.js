var extras = function() {};

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 30,
        color: "#FFFFFF",
        isExercise: true,
        // ishideButtons: true,
        manifest: [{
            src: "../exercises/images/pencil.png",
            id: "pen"
        }, {
            src: "../exercises/images/player-buttons.png",
            id: "icons"
        }]
    };
    var transformX = transformY = 0.184;
    var moveX = 480 + 70,
        moveY = 6;
    var startX = ((739.0000) * transformX) + moveX,
        startY = ((0) * transformY) + moveY;
    var endX = ((120) * transformX) + moveX,
        endY = ((1060) * transformY) + moveY;
    var pencil = null;

    var startX2 = ((0) * transformX) + moveX,
        startY2 = ((0) * transformY) + moveY,
        endX2 = ((0) * transformX) + moveX,
        endY2 = ((0) * transformY) + moveY;
    var altStartX2 = startX,
        altStartY2 = startY,
        altEndX2 = endX,
        altEndY2 = endY;
    var bar = {
        x: startX,
        y: startY,
        oldx: startX,
        oldy: startY
    };
    var bar2 = {
        x: startX2,
        y: startY2,
        oldx: startX2,
        oldy: startY2
    };
    var pencil = null;
    var points = [
        new createjs.Point(startX, startY),

        new createjs.Point(((739.0000) * transformX) + moveX, ((0.3333) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((0.6667) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((1.0000) * transformY) + moveY),

        new createjs.Point(((738.3334) * transformX) + moveX, ((1.0000) * transformY) + moveY),
        new createjs.Point(((737.6666) * transformX) + moveX, ((1.0000) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((1.0000) * transformY) + moveY),

        new createjs.Point(((736.6667) * transformX) + moveX, ((1.6666) * transformY) + moveY),
        new createjs.Point(((736.3333) * transformX) + moveX, ((2.3334) * transformY) + moveY),
        new createjs.Point(((736.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((735.3334) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((734.6666) * transformX) + moveX, ((3.0000) * transformY) + moveY),
        new createjs.Point(((734.0000) * transformX) + moveX, ((3.0000) * transformY) + moveY),

        new createjs.Point(((733.3334) * transformX) + moveX, ((3.9999) * transformY) + moveY),
        new createjs.Point(((732.6666) * transformX) + moveX, ((5.0001) * transformY) + moveY),
        new createjs.Point(((732.0000) * transformX) + moveX, ((6.0000) * transformY) + moveY),

        new createjs.Point(((731.3334) * transformX) + moveX, ((6.0000) * transformY) + moveY),
        new createjs.Point(((730.6666) * transformX) + moveX, ((6.0000) * transformY) + moveY),
        new createjs.Point(((730.0000) * transformX) + moveX, ((6.0000) * transformY) + moveY),

        new createjs.Point(((729.3334) * transformX) + moveX, ((6.9999) * transformY) + moveY),
        new createjs.Point(((728.6666) * transformX) + moveX, ((8.0001) * transformY) + moveY),
        new createjs.Point(((728.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((727.3334) * transformX) + moveX, ((9.0000) * transformY) + moveY),
        new createjs.Point(((726.6666) * transformX) + moveX, ((9.0000) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((9.0000) * transformY) + moveY),

        new createjs.Point(((725.3334) * transformX) + moveX, ((9.9999) * transformY) + moveY),
        new createjs.Point(((724.6666) * transformX) + moveX, ((11.0001) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((723.3334) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((722.6666) * transformX) + moveX, ((12.0000) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((12.0000) * transformY) + moveY),

        new createjs.Point(((721.3334) * transformX) + moveX, ((12.9999) * transformY) + moveY),
        new createjs.Point(((720.6666) * transformX) + moveX, ((14.0001) * transformY) + moveY),
        new createjs.Point(((720.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((719.3334) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((718.6666) * transformX) + moveX, ((15.0000) * transformY) + moveY),
        new createjs.Point(((718.0000) * transformX) + moveX, ((15.0000) * transformY) + moveY),

        new createjs.Point(((717.3334) * transformX) + moveX, ((15.9999) * transformY) + moveY),
        new createjs.Point(((716.6666) * transformX) + moveX, ((17.0001) * transformY) + moveY),
        new createjs.Point(((716.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((715.3334) * transformX) + moveX, ((18.0000) * transformY) + moveY),
        new createjs.Point(((714.6666) * transformX) + moveX, ((18.0000) * transformY) + moveY),
        new createjs.Point(((714.0000) * transformX) + moveX, ((18.0000) * transformY) + moveY),

        new createjs.Point(((713.3334) * transformX) + moveX, ((18.9999) * transformY) + moveY),
        new createjs.Point(((712.6666) * transformX) + moveX, ((20.0001) * transformY) + moveY),
        new createjs.Point(((712.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((711.3334) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((710.6666) * transformX) + moveX, ((21.0000) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((21.0000) * transformY) + moveY),

        new createjs.Point(((709.3334) * transformX) + moveX, ((21.9999) * transformY) + moveY),
        new createjs.Point(((708.6666) * transformX) + moveX, ((23.0001) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((24.0000) * transformY) + moveY),

        new createjs.Point(((707.3334) * transformX) + moveX, ((24.0000) * transformY) + moveY),
        new createjs.Point(((706.6666) * transformX) + moveX, ((24.0000) * transformY) + moveY),
        new createjs.Point(((706.0000) * transformX) + moveX, ((24.0000) * transformY) + moveY),

        new createjs.Point(((705.3334) * transformX) + moveX, ((24.9999) * transformY) + moveY),
        new createjs.Point(((704.6666) * transformX) + moveX, ((26.0001) * transformY) + moveY),
        new createjs.Point(((704.0000) * transformX) + moveX, ((27.0000) * transformY) + moveY),

        new createjs.Point(((703.3334) * transformX) + moveX, ((27.0000) * transformY) + moveY),
        new createjs.Point(((702.6666) * transformX) + moveX, ((27.0000) * transformY) + moveY),
        new createjs.Point(((702.0000) * transformX) + moveX, ((27.0000) * transformY) + moveY),

        new createjs.Point(((701.0001) * transformX) + moveX, ((28.3332) * transformY) + moveY),
        new createjs.Point(((699.9999) * transformX) + moveX, ((29.6668) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((31.0000) * transformY) + moveY),

        new createjs.Point(((698.0001) * transformX) + moveX, ((31.3333) * transformY) + moveY),
        new createjs.Point(((696.9999) * transformX) + moveX, ((31.6667) * transformY) + moveY),
        new createjs.Point(((696.0000) * transformX) + moveX, ((32.0000) * transformY) + moveY),

        new createjs.Point(((695.6667) * transformX) + moveX, ((32.6666) * transformY) + moveY),
        new createjs.Point(((695.3333) * transformX) + moveX, ((33.3334) * transformY) + moveY),
        new createjs.Point(((695.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((694.3334) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((693.6666) * transformX) + moveX, ((34.0000) * transformY) + moveY),
        new createjs.Point(((693.0000) * transformX) + moveX, ((34.0000) * transformY) + moveY),

        new createjs.Point(((692.6667) * transformX) + moveX, ((34.6666) * transformY) + moveY),
        new createjs.Point(((692.3333) * transformX) + moveX, ((35.3334) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((36.0000) * transformY) + moveY),

        new createjs.Point(((691.6667) * transformX) + moveX, ((36.0000) * transformY) + moveY),
        new createjs.Point(((691.3333) * transformX) + moveX, ((36.0000) * transformY) + moveY),
        new createjs.Point(((691.0000) * transformX) + moveX, ((36.0000) * transformY) + moveY),

        new createjs.Point(((690.6667) * transformX) + moveX, ((36.6666) * transformY) + moveY),
        new createjs.Point(((690.3333) * transformX) + moveX, ((37.3334) * transformY) + moveY),
        new createjs.Point(((690.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((689.3334) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((688.6666) * transformX) + moveX, ((38.0000) * transformY) + moveY),
        new createjs.Point(((688.0000) * transformX) + moveX, ((38.0000) * transformY) + moveY),

        new createjs.Point(((687.0001) * transformX) + moveX, ((39.3332) * transformY) + moveY),
        new createjs.Point(((685.9999) * transformX) + moveX, ((40.6668) * transformY) + moveY),
        new createjs.Point(((685.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((684.3334) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((683.6666) * transformX) + moveX, ((42.0000) * transformY) + moveY),
        new createjs.Point(((683.0000) * transformX) + moveX, ((42.0000) * transformY) + moveY),

        new createjs.Point(((682.6667) * transformX) + moveX, ((42.6666) * transformY) + moveY),
        new createjs.Point(((682.3333) * transformX) + moveX, ((43.3334) * transformY) + moveY),
        new createjs.Point(((682.0000) * transformX) + moveX, ((44.0000) * transformY) + moveY),

        new createjs.Point(((681.6667) * transformX) + moveX, ((44.0000) * transformY) + moveY),
        new createjs.Point(((681.3333) * transformX) + moveX, ((44.0000) * transformY) + moveY),
        new createjs.Point(((681.0000) * transformX) + moveX, ((44.0000) * transformY) + moveY),

        new createjs.Point(((680.6667) * transformX) + moveX, ((44.6666) * transformY) + moveY),
        new createjs.Point(((680.3333) * transformX) + moveX, ((45.3334) * transformY) + moveY),
        new createjs.Point(((680.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((679.3334) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((678.6666) * transformX) + moveX, ((46.0000) * transformY) + moveY),
        new createjs.Point(((678.0000) * transformX) + moveX, ((46.0000) * transformY) + moveY),

        new createjs.Point(((676.6668) * transformX) + moveX, ((47.6665) * transformY) + moveY),
        new createjs.Point(((675.3332) * transformX) + moveX, ((49.3335) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((51.0000) * transformY) + moveY),

        new createjs.Point(((673.3334) * transformX) + moveX, ((51.0000) * transformY) + moveY),
        new createjs.Point(((672.6666) * transformX) + moveX, ((51.0000) * transformY) + moveY),
        new createjs.Point(((672.0000) * transformX) + moveX, ((51.0000) * transformY) + moveY),

        new createjs.Point(((671.0001) * transformX) + moveX, ((52.3332) * transformY) + moveY),
        new createjs.Point(((669.9999) * transformX) + moveX, ((53.6668) * transformY) + moveY),
        new createjs.Point(((669.0000) * transformX) + moveX, ((55.0000) * transformY) + moveY),

        new createjs.Point(((668.3334) * transformX) + moveX, ((55.0000) * transformY) + moveY),
        new createjs.Point(((667.6666) * transformX) + moveX, ((55.0000) * transformY) + moveY),
        new createjs.Point(((667.0000) * transformX) + moveX, ((55.0000) * transformY) + moveY),

        new createjs.Point(((665.6668) * transformX) + moveX, ((56.6665) * transformY) + moveY),
        new createjs.Point(((664.3332) * transformX) + moveX, ((58.3335) * transformY) + moveY),
        new createjs.Point(((663.0000) * transformX) + moveX, ((60.0000) * transformY) + moveY),

        new createjs.Point(((662.3334) * transformX) + moveX, ((60.0000) * transformY) + moveY),
        new createjs.Point(((661.6666) * transformX) + moveX, ((60.0000) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((60.0000) * transformY) + moveY),

        new createjs.Point(((659.3335) * transformX) + moveX, ((61.9998) * transformY) + moveY),
        new createjs.Point(((657.6665) * transformX) + moveX, ((64.0002) * transformY) + moveY),
        new createjs.Point(((656.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((655.3334) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((654.6666) * transformX) + moveX, ((66.0000) * transformY) + moveY),
        new createjs.Point(((654.0000) * transformX) + moveX, ((66.0000) * transformY) + moveY),

        new createjs.Point(((652.3335) * transformX) + moveX, ((67.9998) * transformY) + moveY),
        new createjs.Point(((650.6665) * transformX) + moveX, ((70.0002) * transformY) + moveY),
        new createjs.Point(((649.0000) * transformX) + moveX, ((72.0000) * transformY) + moveY),

        new createjs.Point(((648.3334) * transformX) + moveX, ((72.0000) * transformY) + moveY),
        new createjs.Point(((647.6666) * transformX) + moveX, ((72.0000) * transformY) + moveY),
        new createjs.Point(((647.0000) * transformX) + moveX, ((72.0000) * transformY) + moveY),

        new createjs.Point(((645.3335) * transformX) + moveX, ((73.9998) * transformY) + moveY),
        new createjs.Point(((643.6665) * transformX) + moveX, ((76.0002) * transformY) + moveY),
        new createjs.Point(((642.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((641.3334) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((640.6666) * transformX) + moveX, ((78.0000) * transformY) + moveY),
        new createjs.Point(((640.0000) * transformX) + moveX, ((78.0000) * transformY) + moveY),

        new createjs.Point(((638.0002) * transformX) + moveX, ((80.3331) * transformY) + moveY),
        new createjs.Point(((635.9998) * transformX) + moveX, ((82.6669) * transformY) + moveY),
        new createjs.Point(((634.0000) * transformX) + moveX, ((85.0000) * transformY) + moveY),

        new createjs.Point(((633.3334) * transformX) + moveX, ((85.0000) * transformY) + moveY),
        new createjs.Point(((632.6666) * transformX) + moveX, ((85.0000) * transformY) + moveY),
        new createjs.Point(((632.0000) * transformX) + moveX, ((85.0000) * transformY) + moveY),

        new createjs.Point(((629.3336) * transformX) + moveX, ((87.9997) * transformY) + moveY),
        new createjs.Point(((626.6664) * transformX) + moveX, ((91.0003) * transformY) + moveY),
        new createjs.Point(((624.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((623.3334) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((622.6666) * transformX) + moveX, ((94.0000) * transformY) + moveY),
        new createjs.Point(((622.0000) * transformX) + moveX, ((94.0000) * transformY) + moveY),

        new createjs.Point(((619.0003) * transformX) + moveX, ((97.3330) * transformY) + moveY),
        new createjs.Point(((615.9997) * transformX) + moveX, ((100.6670) * transformY) + moveY),
        new createjs.Point(((613.0000) * transformX) + moveX, ((104.0000) * transformY) + moveY),

        new createjs.Point(((612.3334) * transformX) + moveX, ((104.0000) * transformY) + moveY),
        new createjs.Point(((611.6666) * transformX) + moveX, ((104.0000) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((104.0000) * transformY) + moveY),

        new createjs.Point(((607.0004) * transformX) + moveX, ((108.3329) * transformY) + moveY),
        new createjs.Point(((602.9996) * transformX) + moveX, ((112.6671) * transformY) + moveY),
        new createjs.Point(((599.0000) * transformX) + moveX, ((117.0000) * transformY) + moveY),

        new createjs.Point(((598.6667) * transformX) + moveX, ((117.0000) * transformY) + moveY),
        new createjs.Point(((598.3333) * transformX) + moveX, ((117.0000) * transformY) + moveY),
        new createjs.Point(((598.0000) * transformX) + moveX, ((117.0000) * transformY) + moveY),

        new createjs.Point(((597.6667) * transformX) + moveX, ((117.6666) * transformY) + moveY),
        new createjs.Point(((597.3333) * transformX) + moveX, ((118.3334) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((119.0000) * transformY) + moveY),

        new createjs.Point(((596.3334) * transformX) + moveX, ((119.0000) * transformY) + moveY),
        new createjs.Point(((595.6666) * transformX) + moveX, ((119.0000) * transformY) + moveY),
        new createjs.Point(((595.0000) * transformX) + moveX, ((119.0000) * transformY) + moveY),

        new createjs.Point(((587.6674) * transformX) + moveX, ((126.6659) * transformY) + moveY),
        new createjs.Point(((580.3326) * transformX) + moveX, ((134.3341) * transformY) + moveY),
        new createjs.Point(((573.0000) * transformX) + moveX, ((142.0000) * transformY) + moveY),

        new createjs.Point(((562.8311) * transformX) + moveX, ((152.1707) * transformY) + moveY),
        new createjs.Point(((551.5371) * transformX) + moveX, ((161.1953) * transformY) + moveY),
        new createjs.Point(((543.0000) * transformX) + moveX, ((173.0000) * transformY) + moveY),

        new createjs.Point(((540.0003) * transformX) + moveX, ((175.6664) * transformY) + moveY),
        new createjs.Point(((536.9997) * transformX) + moveX, ((178.3336) * transformY) + moveY),
        new createjs.Point(((534.0000) * transformX) + moveX, ((181.0000) * transformY) + moveY),

        new createjs.Point(((532.6668) * transformX) + moveX, ((182.6665) * transformY) + moveY),
        new createjs.Point(((531.3332) * transformX) + moveX, ((184.3335) * transformY) + moveY),
        new createjs.Point(((530.0000) * transformX) + moveX, ((186.0000) * transformY) + moveY),

        new createjs.Point(((529.3334) * transformX) + moveX, ((186.3333) * transformY) + moveY),
        new createjs.Point(((528.6666) * transformX) + moveX, ((186.6667) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((187.0000) * transformY) + moveY),

        new createjs.Point(((527.6667) * transformX) + moveX, ((187.9999) * transformY) + moveY),
        new createjs.Point(((527.3333) * transformX) + moveX, ((189.0001) * transformY) + moveY),
        new createjs.Point(((527.0000) * transformX) + moveX, ((190.0000) * transformY) + moveY),

        new createjs.Point(((523.6670) * transformX) + moveX, ((192.9997) * transformY) + moveY),
        new createjs.Point(((520.3330) * transformX) + moveX, ((196.0003) * transformY) + moveY),
        new createjs.Point(((517.0000) * transformX) + moveX, ((199.0000) * transformY) + moveY),

        new createjs.Point(((517.0000) * transformX) + moveX, ((199.6666) * transformY) + moveY),
        new createjs.Point(((517.0000) * transformX) + moveX, ((200.3334) * transformY) + moveY),
        new createjs.Point(((517.0000) * transformX) + moveX, ((201.0000) * transformY) + moveY),

        new createjs.Point(((514.0003) * transformX) + moveX, ((203.6664) * transformY) + moveY),
        new createjs.Point(((510.9997) * transformX) + moveX, ((206.3336) * transformY) + moveY),
        new createjs.Point(((508.0000) * transformX) + moveX, ((209.0000) * transformY) + moveY),

        new createjs.Point(((508.0000) * transformX) + moveX, ((209.6666) * transformY) + moveY),
        new createjs.Point(((508.0000) * transformX) + moveX, ((210.3334) * transformY) + moveY),
        new createjs.Point(((508.0000) * transformX) + moveX, ((211.0000) * transformY) + moveY),

        new createjs.Point(((505.6669) * transformX) + moveX, ((212.9998) * transformY) + moveY),
        new createjs.Point(((503.3331) * transformX) + moveX, ((215.0002) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((217.0000) * transformY) + moveY),

        new createjs.Point(((501.0000) * transformX) + moveX, ((217.6666) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((218.3334) * transformY) + moveY),
        new createjs.Point(((501.0000) * transformX) + moveX, ((219.0000) * transformY) + moveY),

        new createjs.Point(((498.6669) * transformX) + moveX, ((220.9998) * transformY) + moveY),
        new createjs.Point(((496.3331) * transformX) + moveX, ((223.0002) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((225.0000) * transformY) + moveY),

        new createjs.Point(((494.0000) * transformX) + moveX, ((225.6666) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((226.3334) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((227.0000) * transformY) + moveY),

        new createjs.Point(((492.0002) * transformX) + moveX, ((228.6665) * transformY) + moveY),
        new createjs.Point(((489.9998) * transformX) + moveX, ((230.3335) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((232.0000) * transformY) + moveY),

        new createjs.Point(((488.0000) * transformX) + moveX, ((232.6666) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((233.3334) * transformY) + moveY),
        new createjs.Point(((488.0000) * transformX) + moveX, ((234.0000) * transformY) + moveY),

        new createjs.Point(((486.0002) * transformX) + moveX, ((235.6665) * transformY) + moveY),
        new createjs.Point(((483.9998) * transformX) + moveX, ((237.3335) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((239.0000) * transformY) + moveY),

        new createjs.Point(((482.0000) * transformX) + moveX, ((239.6666) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((240.3334) * transformY) + moveY),
        new createjs.Point(((482.0000) * transformX) + moveX, ((241.0000) * transformY) + moveY),

        new createjs.Point(((480.3335) * transformX) + moveX, ((242.3332) * transformY) + moveY),
        new createjs.Point(((478.6665) * transformX) + moveX, ((243.6668) * transformY) + moveY),
        new createjs.Point(((477.0000) * transformX) + moveX, ((245.0000) * transformY) + moveY),

        new createjs.Point(((477.0000) * transformX) + moveX, ((245.6666) * transformY) + moveY),
        new createjs.Point(((477.0000) * transformX) + moveX, ((246.3334) * transformY) + moveY),
        new createjs.Point(((477.0000) * transformX) + moveX, ((247.0000) * transformY) + moveY),

        new createjs.Point(((476.3334) * transformX) + moveX, ((247.3333) * transformY) + moveY),
        new createjs.Point(((475.6666) * transformX) + moveX, ((247.6667) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((248.0000) * transformY) + moveY),

        new createjs.Point(((474.3334) * transformX) + moveX, ((248.9999) * transformY) + moveY),
        new createjs.Point(((473.6666) * transformX) + moveX, ((250.0001) * transformY) + moveY),
        new createjs.Point(((473.0000) * transformX) + moveX, ((251.0000) * transformY) + moveY),

        new createjs.Point(((472.6667) * transformX) + moveX, ((251.0000) * transformY) + moveY),
        new createjs.Point(((472.3333) * transformX) + moveX, ((251.0000) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((251.0000) * transformY) + moveY),

        new createjs.Point(((472.0000) * transformX) + moveX, ((251.6666) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((252.3334) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((253.0000) * transformY) + moveY),

        new createjs.Point(((470.3335) * transformX) + moveX, ((254.3332) * transformY) + moveY),
        new createjs.Point(((468.6665) * transformX) + moveX, ((255.6668) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((257.0000) * transformY) + moveY),

        new createjs.Point(((467.0000) * transformX) + moveX, ((257.6666) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((258.3334) * transformY) + moveY),
        new createjs.Point(((467.0000) * transformX) + moveX, ((259.0000) * transformY) + moveY),

        new createjs.Point(((466.3334) * transformX) + moveX, ((259.3333) * transformY) + moveY),
        new createjs.Point(((465.6666) * transformX) + moveX, ((259.6667) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((260.0000) * transformY) + moveY),

        new createjs.Point(((465.0000) * transformX) + moveX, ((260.3333) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((260.6667) * transformY) + moveY),
        new createjs.Point(((465.0000) * transformX) + moveX, ((261.0000) * transformY) + moveY),

        new createjs.Point(((464.3334) * transformX) + moveX, ((261.3333) * transformY) + moveY),
        new createjs.Point(((463.6666) * transformX) + moveX, ((261.6667) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((262.0000) * transformY) + moveY),

        new createjs.Point(((463.0000) * transformX) + moveX, ((262.6666) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((263.3334) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((264.0000) * transformY) + moveY),

        new createjs.Point(((461.3335) * transformX) + moveX, ((265.3332) * transformY) + moveY),
        new createjs.Point(((459.6665) * transformX) + moveX, ((266.6668) * transformY) + moveY),
        new createjs.Point(((458.0000) * transformX) + moveX, ((268.0000) * transformY) + moveY),

        new createjs.Point(((458.0000) * transformX) + moveX, ((268.6666) * transformY) + moveY),
        new createjs.Point(((458.0000) * transformX) + moveX, ((269.3334) * transformY) + moveY),
        new createjs.Point(((458.0000) * transformX) + moveX, ((270.0000) * transformY) + moveY),

        new createjs.Point(((456.6668) * transformX) + moveX, ((270.9999) * transformY) + moveY),
        new createjs.Point(((455.3332) * transformX) + moveX, ((272.0001) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((273.0000) * transformY) + moveY),

        new createjs.Point(((454.0000) * transformX) + moveX, ((273.6666) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((274.3334) * transformY) + moveY),
        new createjs.Point(((454.0000) * transformX) + moveX, ((275.0000) * transformY) + moveY),

        new createjs.Point(((453.3334) * transformX) + moveX, ((275.3333) * transformY) + moveY),
        new createjs.Point(((452.6666) * transformX) + moveX, ((275.6667) * transformY) + moveY),
        new createjs.Point(((452.0000) * transformX) + moveX, ((276.0000) * transformY) + moveY),

        new createjs.Point(((452.0000) * transformX) + moveX, ((276.3333) * transformY) + moveY),
        new createjs.Point(((452.0000) * transformX) + moveX, ((276.6667) * transformY) + moveY),
        new createjs.Point(((452.0000) * transformX) + moveX, ((277.0000) * transformY) + moveY),

        new createjs.Point(((451.3334) * transformX) + moveX, ((277.3333) * transformY) + moveY),
        new createjs.Point(((450.6666) * transformX) + moveX, ((277.6667) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((278.0000) * transformY) + moveY),

        new createjs.Point(((450.0000) * transformX) + moveX, ((278.6666) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((279.3334) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((280.0000) * transformY) + moveY),

        new createjs.Point(((449.3334) * transformX) + moveX, ((280.3333) * transformY) + moveY),
        new createjs.Point(((448.6666) * transformX) + moveX, ((280.6667) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((281.0000) * transformY) + moveY),

        new createjs.Point(((447.6667) * transformX) + moveX, ((281.9999) * transformY) + moveY),
        new createjs.Point(((447.3333) * transformX) + moveX, ((283.0001) * transformY) + moveY),
        new createjs.Point(((447.0000) * transformX) + moveX, ((284.0000) * transformY) + moveY),

        new createjs.Point(((445.6668) * transformX) + moveX, ((284.9999) * transformY) + moveY),
        new createjs.Point(((444.3332) * transformX) + moveX, ((286.0001) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((287.0000) * transformY) + moveY),

        new createjs.Point(((443.0000) * transformX) + moveX, ((287.6666) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((288.3334) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((289.0000) * transformY) + moveY),

        new createjs.Point(((441.6668) * transformX) + moveX, ((289.9999) * transformY) + moveY),
        new createjs.Point(((440.3332) * transformX) + moveX, ((291.0001) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((292.0000) * transformY) + moveY),

        new createjs.Point(((439.0000) * transformX) + moveX, ((292.6666) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((293.3334) * transformY) + moveY),
        new createjs.Point(((439.0000) * transformX) + moveX, ((294.0000) * transformY) + moveY),

        new createjs.Point(((438.0001) * transformX) + moveX, ((294.6666) * transformY) + moveY),
        new createjs.Point(((436.9999) * transformX) + moveX, ((295.3334) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((296.0000) * transformY) + moveY),

        new createjs.Point(((436.0000) * transformX) + moveX, ((296.6666) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((297.3334) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((298.0000) * transformY) + moveY),

        new createjs.Point(((435.0001) * transformX) + moveX, ((298.6666) * transformY) + moveY),
        new createjs.Point(((433.9999) * transformX) + moveX, ((299.3334) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((300.0000) * transformY) + moveY),

        new createjs.Point(((433.0000) * transformX) + moveX, ((300.6666) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((301.3334) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((302.0000) * transformY) + moveY),

        new createjs.Point(((431.6668) * transformX) + moveX, ((302.9999) * transformY) + moveY),
        new createjs.Point(((430.3332) * transformX) + moveX, ((304.0001) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((305.0000) * transformY) + moveY),

        new createjs.Point(((429.0000) * transformX) + moveX, ((305.6666) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((306.3334) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((307.0000) * transformY) + moveY),

        new createjs.Point(((428.0001) * transformX) + moveX, ((307.6666) * transformY) + moveY),
        new createjs.Point(((426.9999) * transformX) + moveX, ((308.3334) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((309.0000) * transformY) + moveY),

        new createjs.Point(((426.0000) * transformX) + moveX, ((309.6666) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((310.3334) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((311.0000) * transformY) + moveY),

        new createjs.Point(((425.0001) * transformX) + moveX, ((311.6666) * transformY) + moveY),
        new createjs.Point(((423.9999) * transformX) + moveX, ((312.3334) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((313.0000) * transformY) + moveY),

        new createjs.Point(((423.0000) * transformX) + moveX, ((313.6666) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((314.3334) * transformY) + moveY),
        new createjs.Point(((423.0000) * transformX) + moveX, ((315.0000) * transformY) + moveY),

        new createjs.Point(((422.0001) * transformX) + moveX, ((315.6666) * transformY) + moveY),
        new createjs.Point(((420.9999) * transformX) + moveX, ((316.3334) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((317.0000) * transformY) + moveY),

        new createjs.Point(((420.0000) * transformX) + moveX, ((317.6666) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((318.3334) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((319.0000) * transformY) + moveY),

        new createjs.Point(((419.0001) * transformX) + moveX, ((319.6666) * transformY) + moveY),
        new createjs.Point(((417.9999) * transformX) + moveX, ((320.3334) * transformY) + moveY),
        new createjs.Point(((417.0000) * transformX) + moveX, ((321.0000) * transformY) + moveY),

        new createjs.Point(((416.6667) * transformX) + moveX, ((321.9999) * transformY) + moveY),
        new createjs.Point(((416.3333) * transformX) + moveX, ((323.0001) * transformY) + moveY),
        new createjs.Point(((416.0000) * transformX) + moveX, ((324.0000) * transformY) + moveY),

        new createjs.Point(((415.3334) * transformX) + moveX, ((324.3333) * transformY) + moveY),
        new createjs.Point(((414.6666) * transformX) + moveX, ((324.6667) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((325.0000) * transformY) + moveY),

        new createjs.Point(((414.0000) * transformX) + moveX, ((325.6666) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((326.3334) * transformY) + moveY),
        new createjs.Point(((414.0000) * transformX) + moveX, ((327.0000) * transformY) + moveY),

        new createjs.Point(((413.3334) * transformX) + moveX, ((327.3333) * transformY) + moveY),
        new createjs.Point(((412.6666) * transformX) + moveX, ((327.6667) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((328.0000) * transformY) + moveY),

        new createjs.Point(((412.0000) * transformX) + moveX, ((328.6666) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((329.3334) * transformY) + moveY),
        new createjs.Point(((412.0000) * transformX) + moveX, ((330.0000) * transformY) + moveY),

        new createjs.Point(((411.3334) * transformX) + moveX, ((330.3333) * transformY) + moveY),
        new createjs.Point(((410.6666) * transformX) + moveX, ((330.6667) * transformY) + moveY),
        new createjs.Point(((410.0000) * transformX) + moveX, ((331.0000) * transformY) + moveY),

        new createjs.Point(((409.6667) * transformX) + moveX, ((331.9999) * transformY) + moveY),
        new createjs.Point(((409.3333) * transformX) + moveX, ((333.0001) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((334.0000) * transformY) + moveY),

        new createjs.Point(((408.0001) * transformX) + moveX, ((334.6666) * transformY) + moveY),
        new createjs.Point(((406.9999) * transformX) + moveX, ((335.3334) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((336.0000) * transformY) + moveY),

        new createjs.Point(((406.0000) * transformX) + moveX, ((336.6666) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((337.3334) * transformY) + moveY),
        new createjs.Point(((406.0000) * transformX) + moveX, ((338.0000) * transformY) + moveY),

        new createjs.Point(((405.3334) * transformX) + moveX, ((338.3333) * transformY) + moveY),
        new createjs.Point(((404.6666) * transformX) + moveX, ((338.6667) * transformY) + moveY),
        new createjs.Point(((404.0000) * transformX) + moveX, ((339.0000) * transformY) + moveY),

        new createjs.Point(((404.0000) * transformX) + moveX, ((339.6666) * transformY) + moveY),
        new createjs.Point(((404.0000) * transformX) + moveX, ((340.3334) * transformY) + moveY),
        new createjs.Point(((404.0000) * transformX) + moveX, ((341.0000) * transformY) + moveY),

        new createjs.Point(((403.0001) * transformX) + moveX, ((341.6666) * transformY) + moveY),
        new createjs.Point(((401.9999) * transformX) + moveX, ((342.3334) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((343.0000) * transformY) + moveY),

        new createjs.Point(((401.0000) * transformX) + moveX, ((343.6666) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((344.3334) * transformY) + moveY),
        new createjs.Point(((401.0000) * transformX) + moveX, ((345.0000) * transformY) + moveY),

        new createjs.Point(((400.3334) * transformX) + moveX, ((345.3333) * transformY) + moveY),
        new createjs.Point(((399.6666) * transformX) + moveX, ((345.6667) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((346.0000) * transformY) + moveY),

        new createjs.Point(((399.0000) * transformX) + moveX, ((346.6666) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((347.3334) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((348.0000) * transformY) + moveY),

        new createjs.Point(((398.0001) * transformX) + moveX, ((348.6666) * transformY) + moveY),
        new createjs.Point(((396.9999) * transformX) + moveX, ((349.3334) * transformY) + moveY),
        new createjs.Point(((396.0000) * transformX) + moveX, ((350.0000) * transformY) + moveY),

        new createjs.Point(((396.0000) * transformX) + moveX, ((350.6666) * transformY) + moveY),
        new createjs.Point(((396.0000) * transformX) + moveX, ((351.3334) * transformY) + moveY),
        new createjs.Point(((396.0000) * transformX) + moveX, ((352.0000) * transformY) + moveY),

        new createjs.Point(((395.3334) * transformX) + moveX, ((352.3333) * transformY) + moveY),
        new createjs.Point(((394.6666) * transformX) + moveX, ((352.6667) * transformY) + moveY),
        new createjs.Point(((394.0000) * transformX) + moveX, ((353.0000) * transformY) + moveY),

        new createjs.Point(((394.0000) * transformX) + moveX, ((353.6666) * transformY) + moveY),
        new createjs.Point(((394.0000) * transformX) + moveX, ((354.3334) * transformY) + moveY),
        new createjs.Point(((394.0000) * transformX) + moveX, ((355.0000) * transformY) + moveY),

        new createjs.Point(((393.0001) * transformX) + moveX, ((355.6666) * transformY) + moveY),
        new createjs.Point(((391.9999) * transformX) + moveX, ((356.3334) * transformY) + moveY),
        new createjs.Point(((391.0000) * transformX) + moveX, ((357.0000) * transformY) + moveY),

        new createjs.Point(((391.0000) * transformX) + moveX, ((357.6666) * transformY) + moveY),
        new createjs.Point(((391.0000) * transformX) + moveX, ((358.3334) * transformY) + moveY),
        new createjs.Point(((391.0000) * transformX) + moveX, ((359.0000) * transformY) + moveY),

        new createjs.Point(((390.6667) * transformX) + moveX, ((359.0000) * transformY) + moveY),
        new createjs.Point(((390.3333) * transformX) + moveX, ((359.0000) * transformY) + moveY),
        new createjs.Point(((390.0000) * transformX) + moveX, ((359.0000) * transformY) + moveY),

        new createjs.Point(((389.6667) * transformX) + moveX, ((359.9999) * transformY) + moveY),
        new createjs.Point(((389.3333) * transformX) + moveX, ((361.0001) * transformY) + moveY),
        new createjs.Point(((389.0000) * transformX) + moveX, ((362.0000) * transformY) + moveY),

        new createjs.Point(((388.3334) * transformX) + moveX, ((362.3333) * transformY) + moveY),
        new createjs.Point(((387.6666) * transformX) + moveX, ((362.6667) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((363.0000) * transformY) + moveY),

        new createjs.Point(((387.0000) * transformX) + moveX, ((363.6666) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((364.3334) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((365.0000) * transformY) + moveY),

        new createjs.Point(((386.0001) * transformX) + moveX, ((365.6666) * transformY) + moveY),
        new createjs.Point(((384.9999) * transformX) + moveX, ((366.3334) * transformY) + moveY),
        new createjs.Point(((384.0000) * transformX) + moveX, ((367.0000) * transformY) + moveY),

        new createjs.Point(((384.0000) * transformX) + moveX, ((367.6666) * transformY) + moveY),
        new createjs.Point(((384.0000) * transformX) + moveX, ((368.3334) * transformY) + moveY),
        new createjs.Point(((384.0000) * transformX) + moveX, ((369.0000) * transformY) + moveY),

        new createjs.Point(((383.3334) * transformX) + moveX, ((369.3333) * transformY) + moveY),
        new createjs.Point(((382.6666) * transformX) + moveX, ((369.6667) * transformY) + moveY),
        new createjs.Point(((382.0000) * transformX) + moveX, ((370.0000) * transformY) + moveY),

        new createjs.Point(((381.6667) * transformX) + moveX, ((370.9999) * transformY) + moveY),
        new createjs.Point(((381.3333) * transformX) + moveX, ((372.0001) * transformY) + moveY),
        new createjs.Point(((381.0000) * transformX) + moveX, ((373.0000) * transformY) + moveY),

        new createjs.Point(((380.6667) * transformX) + moveX, ((373.0000) * transformY) + moveY),
        new createjs.Point(((380.3333) * transformX) + moveX, ((373.0000) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((373.0000) * transformY) + moveY),

        new createjs.Point(((380.0000) * transformX) + moveX, ((373.6666) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((374.3334) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((375.0000) * transformY) + moveY),

        new createjs.Point(((379.3334) * transformX) + moveX, ((375.3333) * transformY) + moveY),
        new createjs.Point(((378.6666) * transformX) + moveX, ((375.6667) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((376.0000) * transformY) + moveY),

        new createjs.Point(((378.0000) * transformX) + moveX, ((376.6666) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((377.3334) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((378.0000) * transformY) + moveY),

        new createjs.Point(((377.3334) * transformX) + moveX, ((378.3333) * transformY) + moveY),
        new createjs.Point(((376.6666) * transformX) + moveX, ((378.6667) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((379.0000) * transformY) + moveY),

        new createjs.Point(((376.0000) * transformX) + moveX, ((379.6666) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((380.3334) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((381.0000) * transformY) + moveY),

        new createjs.Point(((375.0001) * transformX) + moveX, ((381.6666) * transformY) + moveY),
        new createjs.Point(((373.9999) * transformX) + moveX, ((382.3334) * transformY) + moveY),
        new createjs.Point(((373.0000) * transformX) + moveX, ((383.0000) * transformY) + moveY),

        new createjs.Point(((373.0000) * transformX) + moveX, ((383.6666) * transformY) + moveY),
        new createjs.Point(((373.0000) * transformX) + moveX, ((384.3334) * transformY) + moveY),
        new createjs.Point(((373.0000) * transformX) + moveX, ((385.0000) * transformY) + moveY),

        new createjs.Point(((372.3334) * transformX) + moveX, ((385.3333) * transformY) + moveY),
        new createjs.Point(((371.6666) * transformX) + moveX, ((385.6667) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((386.0000) * transformY) + moveY),

        new createjs.Point(((370.6667) * transformX) + moveX, ((386.9999) * transformY) + moveY),
        new createjs.Point(((370.3333) * transformX) + moveX, ((388.0001) * transformY) + moveY),
        new createjs.Point(((370.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((369.6667) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((369.3333) * transformX) + moveX, ((389.0000) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((389.0000) * transformY) + moveY),

        new createjs.Point(((369.0000) * transformX) + moveX, ((389.6666) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((390.3334) * transformY) + moveY),
        new createjs.Point(((369.0000) * transformX) + moveX, ((391.0000) * transformY) + moveY),

        new createjs.Point(((368.3334) * transformX) + moveX, ((391.3333) * transformY) + moveY),
        new createjs.Point(((367.6666) * transformX) + moveX, ((391.6667) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((392.0000) * transformY) + moveY),

        new createjs.Point(((367.0000) * transformX) + moveX, ((392.6666) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((393.3334) * transformY) + moveY),
        new createjs.Point(((367.0000) * transformX) + moveX, ((394.0000) * transformY) + moveY),

        new createjs.Point(((366.3334) * transformX) + moveX, ((394.3333) * transformY) + moveY),
        new createjs.Point(((365.6666) * transformX) + moveX, ((394.6667) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((395.0000) * transformY) + moveY),

        new createjs.Point(((365.0000) * transformX) + moveX, ((395.6666) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((396.3334) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((397.0000) * transformY) + moveY),

        new createjs.Point(((364.3334) * transformX) + moveX, ((397.3333) * transformY) + moveY),
        new createjs.Point(((363.6666) * transformX) + moveX, ((397.6667) * transformY) + moveY),
        new createjs.Point(((363.0000) * transformX) + moveX, ((398.0000) * transformY) + moveY),

        new createjs.Point(((363.0000) * transformX) + moveX, ((398.6666) * transformY) + moveY),
        new createjs.Point(((363.0000) * transformX) + moveX, ((399.3334) * transformY) + moveY),
        new createjs.Point(((363.0000) * transformX) + moveX, ((400.0000) * transformY) + moveY),

        new createjs.Point(((362.3334) * transformX) + moveX, ((400.3333) * transformY) + moveY),
        new createjs.Point(((361.6666) * transformX) + moveX, ((400.6667) * transformY) + moveY),
        new createjs.Point(((361.0000) * transformX) + moveX, ((401.0000) * transformY) + moveY),

        new createjs.Point(((361.0000) * transformX) + moveX, ((401.6666) * transformY) + moveY),
        new createjs.Point(((361.0000) * transformX) + moveX, ((402.3334) * transformY) + moveY),
        new createjs.Point(((361.0000) * transformX) + moveX, ((403.0000) * transformY) + moveY),

        new createjs.Point(((360.3334) * transformX) + moveX, ((403.3333) * transformY) + moveY),
        new createjs.Point(((359.6666) * transformX) + moveX, ((403.6667) * transformY) + moveY),
        new createjs.Point(((359.0000) * transformX) + moveX, ((404.0000) * transformY) + moveY),

        new createjs.Point(((358.6667) * transformX) + moveX, ((405.3332) * transformY) + moveY),
        new createjs.Point(((358.3333) * transformX) + moveX, ((406.6668) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((408.0000) * transformY) + moveY),

        new createjs.Point(((357.3334) * transformX) + moveX, ((408.3333) * transformY) + moveY),
        new createjs.Point(((356.6666) * transformX) + moveX, ((408.6667) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((409.0000) * transformY) + moveY),

        new createjs.Point(((356.0000) * transformX) + moveX, ((409.6666) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((410.3334) * transformY) + moveY),
        new createjs.Point(((356.0000) * transformX) + moveX, ((411.0000) * transformY) + moveY),

        new createjs.Point(((355.6667) * transformX) + moveX, ((411.0000) * transformY) + moveY),
        new createjs.Point(((355.3333) * transformX) + moveX, ((411.0000) * transformY) + moveY),
        new createjs.Point(((355.0000) * transformX) + moveX, ((411.0000) * transformY) + moveY),

        new createjs.Point(((354.6667) * transformX) + moveX, ((411.9999) * transformY) + moveY),
        new createjs.Point(((354.3333) * transformX) + moveX, ((413.0001) * transformY) + moveY),
        new createjs.Point(((354.0000) * transformX) + moveX, ((414.0000) * transformY) + moveY),

        new createjs.Point(((353.3334) * transformX) + moveX, ((414.3333) * transformY) + moveY),
        new createjs.Point(((352.6666) * transformX) + moveX, ((414.6667) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((415.0000) * transformY) + moveY),

        new createjs.Point(((352.0000) * transformX) + moveX, ((415.6666) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((416.3334) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((417.0000) * transformY) + moveY),

        new createjs.Point(((351.3334) * transformX) + moveX, ((417.3333) * transformY) + moveY),
        new createjs.Point(((350.6666) * transformX) + moveX, ((417.6667) * transformY) + moveY),
        new createjs.Point(((350.0000) * transformX) + moveX, ((418.0000) * transformY) + moveY),

        new createjs.Point(((349.6667) * transformX) + moveX, ((418.9999) * transformY) + moveY),
        new createjs.Point(((349.3333) * transformX) + moveX, ((420.0001) * transformY) + moveY),
        new createjs.Point(((349.0000) * transformX) + moveX, ((421.0000) * transformY) + moveY),

        new createjs.Point(((348.6667) * transformX) + moveX, ((421.0000) * transformY) + moveY),
        new createjs.Point(((348.3333) * transformX) + moveX, ((421.0000) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((421.0000) * transformY) + moveY),

        new createjs.Point(((347.6667) * transformX) + moveX, ((422.3332) * transformY) + moveY),
        new createjs.Point(((347.3333) * transformX) + moveX, ((423.6668) * transformY) + moveY),
        new createjs.Point(((347.0000) * transformX) + moveX, ((425.0000) * transformY) + moveY),

        new createjs.Point(((346.3334) * transformX) + moveX, ((425.3333) * transformY) + moveY),
        new createjs.Point(((345.6666) * transformX) + moveX, ((425.6667) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((426.0000) * transformY) + moveY),

        new createjs.Point(((345.0000) * transformX) + moveX, ((426.6666) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((427.3334) * transformY) + moveY),
        new createjs.Point(((345.0000) * transformX) + moveX, ((428.0000) * transformY) + moveY),

        new createjs.Point(((344.3334) * transformX) + moveX, ((428.3333) * transformY) + moveY),
        new createjs.Point(((343.6666) * transformX) + moveX, ((428.6667) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((429.0000) * transformY) + moveY),

        new createjs.Point(((343.0000) * transformX) + moveX, ((429.6666) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((430.3334) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((431.0000) * transformY) + moveY),

        new createjs.Point(((342.3334) * transformX) + moveX, ((431.3333) * transformY) + moveY),
        new createjs.Point(((341.6666) * transformX) + moveX, ((431.6667) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((432.0000) * transformY) + moveY),

        new createjs.Point(((340.6667) * transformX) + moveX, ((433.3332) * transformY) + moveY),
        new createjs.Point(((340.3333) * transformX) + moveX, ((434.6668) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((436.0000) * transformY) + moveY),

        new createjs.Point(((339.6667) * transformX) + moveX, ((436.0000) * transformY) + moveY),
        new createjs.Point(((339.3333) * transformX) + moveX, ((436.0000) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((436.0000) * transformY) + moveY),

        new createjs.Point(((338.6667) * transformX) + moveX, ((436.9999) * transformY) + moveY),
        new createjs.Point(((338.3333) * transformX) + moveX, ((438.0001) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((439.0000) * transformY) + moveY),

        new createjs.Point(((337.3334) * transformX) + moveX, ((439.3333) * transformY) + moveY),
        new createjs.Point(((336.6666) * transformX) + moveX, ((439.6667) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((440.0000) * transformY) + moveY),

        new createjs.Point(((336.0000) * transformX) + moveX, ((440.6666) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((441.3334) * transformY) + moveY),
        new createjs.Point(((336.0000) * transformX) + moveX, ((442.0000) * transformY) + moveY),

        new createjs.Point(((335.6667) * transformX) + moveX, ((442.0000) * transformY) + moveY),
        new createjs.Point(((335.3333) * transformX) + moveX, ((442.0000) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((442.0000) * transformY) + moveY),

        new createjs.Point(((335.0000) * transformX) + moveX, ((442.6666) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((443.3334) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((444.0000) * transformY) + moveY),

        new createjs.Point(((334.3334) * transformX) + moveX, ((444.3333) * transformY) + moveY),
        new createjs.Point(((333.6666) * transformX) + moveX, ((444.6667) * transformY) + moveY),
        new createjs.Point(((333.0000) * transformX) + moveX, ((445.0000) * transformY) + moveY),

        new createjs.Point(((332.6667) * transformX) + moveX, ((446.3332) * transformY) + moveY),
        new createjs.Point(((332.3333) * transformX) + moveX, ((447.6668) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((449.0000) * transformY) + moveY),

        new createjs.Point(((331.6667) * transformX) + moveX, ((449.0000) * transformY) + moveY),
        new createjs.Point(((331.3333) * transformX) + moveX, ((449.0000) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((449.0000) * transformY) + moveY),

        new createjs.Point(((330.6667) * transformX) + moveX, ((449.9999) * transformY) + moveY),
        new createjs.Point(((330.3333) * transformX) + moveX, ((451.0001) * transformY) + moveY),
        new createjs.Point(((330.0000) * transformX) + moveX, ((452.0000) * transformY) + moveY),

        new createjs.Point(((329.3334) * transformX) + moveX, ((452.3333) * transformY) + moveY),
        new createjs.Point(((328.6666) * transformX) + moveX, ((452.6667) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((453.0000) * transformY) + moveY),

        new createjs.Point(((328.0000) * transformX) + moveX, ((453.6666) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((454.3334) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((327.6667) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((327.3333) * transformX) + moveX, ((455.0000) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((455.0000) * transformY) + moveY),

        new createjs.Point(((327.0000) * transformX) + moveX, ((455.6666) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((456.3334) * transformY) + moveY),
        new createjs.Point(((327.0000) * transformX) + moveX, ((457.0000) * transformY) + moveY),

        new createjs.Point(((326.3334) * transformX) + moveX, ((457.3333) * transformY) + moveY),
        new createjs.Point(((325.6666) * transformX) + moveX, ((457.6667) * transformY) + moveY),
        new createjs.Point(((325.0000) * transformX) + moveX, ((458.0000) * transformY) + moveY),

        new createjs.Point(((324.6667) * transformX) + moveX, ((459.3332) * transformY) + moveY),
        new createjs.Point(((324.3333) * transformX) + moveX, ((460.6668) * transformY) + moveY),
        new createjs.Point(((324.0000) * transformX) + moveX, ((462.0000) * transformY) + moveY),

        new createjs.Point(((323.3334) * transformX) + moveX, ((462.3333) * transformY) + moveY),
        new createjs.Point(((322.6666) * transformX) + moveX, ((462.6667) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((463.0000) * transformY) + moveY),

        new createjs.Point(((321.6667) * transformX) + moveX, ((464.3332) * transformY) + moveY),
        new createjs.Point(((321.3333) * transformX) + moveX, ((465.6668) * transformY) + moveY),
        new createjs.Point(((321.0000) * transformX) + moveX, ((467.0000) * transformY) + moveY),

        new createjs.Point(((320.3334) * transformX) + moveX, ((467.3333) * transformY) + moveY),
        new createjs.Point(((319.6666) * transformX) + moveX, ((467.6667) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((468.0000) * transformY) + moveY),

        new createjs.Point(((318.6667) * transformX) + moveX, ((469.3332) * transformY) + moveY),
        new createjs.Point(((318.3333) * transformX) + moveX, ((470.6668) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((472.0000) * transformY) + moveY),

        new createjs.Point(((317.3334) * transformX) + moveX, ((472.3333) * transformY) + moveY),
        new createjs.Point(((316.6666) * transformX) + moveX, ((472.6667) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((473.0000) * transformY) + moveY),

        new createjs.Point(((315.6667) * transformX) + moveX, ((474.3332) * transformY) + moveY),
        new createjs.Point(((315.3333) * transformX) + moveX, ((475.6668) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((477.0000) * transformY) + moveY),

        new createjs.Point(((314.3334) * transformX) + moveX, ((477.3333) * transformY) + moveY),
        new createjs.Point(((313.6666) * transformX) + moveX, ((477.6667) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((478.0000) * transformY) + moveY),

        new createjs.Point(((312.3334) * transformX) + moveX, ((479.9998) * transformY) + moveY),
        new createjs.Point(((311.6666) * transformX) + moveX, ((482.0002) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((484.0000) * transformY) + moveY),

        new createjs.Point(((310.3334) * transformX) + moveX, ((484.3333) * transformY) + moveY),
        new createjs.Point(((309.6666) * transformX) + moveX, ((484.6667) * transformY) + moveY),
        new createjs.Point(((309.0000) * transformX) + moveX, ((485.0000) * transformY) + moveY),

        new createjs.Point(((308.6667) * transformX) + moveX, ((486.3332) * transformY) + moveY),
        new createjs.Point(((308.3333) * transformX) + moveX, ((487.6668) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((489.0000) * transformY) + moveY),

        new createjs.Point(((307.3334) * transformX) + moveX, ((489.3333) * transformY) + moveY),
        new createjs.Point(((306.6666) * transformX) + moveX, ((489.6667) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((490.0000) * transformY) + moveY),

        new createjs.Point(((305.3334) * transformX) + moveX, ((491.9998) * transformY) + moveY),
        new createjs.Point(((304.6666) * transformX) + moveX, ((494.0002) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((496.0000) * transformY) + moveY),

        new createjs.Point(((303.3334) * transformX) + moveX, ((496.3333) * transformY) + moveY),
        new createjs.Point(((302.6666) * transformX) + moveX, ((496.6667) * transformY) + moveY),
        new createjs.Point(((302.0000) * transformX) + moveX, ((497.0000) * transformY) + moveY),

        new createjs.Point(((301.3334) * transformX) + moveX, ((498.9998) * transformY) + moveY),
        new createjs.Point(((300.6666) * transformX) + moveX, ((501.0002) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((503.0000) * transformY) + moveY),

        new createjs.Point(((299.3334) * transformX) + moveX, ((503.3333) * transformY) + moveY),
        new createjs.Point(((298.6666) * transformX) + moveX, ((503.6667) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((504.0000) * transformY) + moveY),

        new createjs.Point(((298.0000) * transformX) + moveX, ((504.6666) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((505.3334) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((297.6667) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((297.3333) * transformX) + moveX, ((506.0000) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((506.0000) * transformY) + moveY),

        new createjs.Point(((297.0000) * transformX) + moveX, ((506.6666) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((507.3334) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((296.6667) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((296.3333) * transformX) + moveX, ((508.0000) * transformY) + moveY),
        new createjs.Point(((296.0000) * transformX) + moveX, ((508.0000) * transformY) + moveY),

        new createjs.Point(((296.0000) * transformX) + moveX, ((508.6666) * transformY) + moveY),
        new createjs.Point(((296.0000) * transformX) + moveX, ((509.3334) * transformY) + moveY),
        new createjs.Point(((296.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((295.6667) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((295.3333) * transformX) + moveX, ((510.0000) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((510.0000) * transformY) + moveY),

        new createjs.Point(((295.0000) * transformX) + moveX, ((510.6666) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((511.3334) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((512.0000) * transformY) + moveY),

        new createjs.Point(((294.3334) * transformX) + moveX, ((512.3333) * transformY) + moveY),
        new createjs.Point(((293.6666) * transformX) + moveX, ((512.6667) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((513.0000) * transformY) + moveY),

        new createjs.Point(((292.3334) * transformX) + moveX, ((514.9998) * transformY) + moveY),
        new createjs.Point(((291.6666) * transformX) + moveX, ((517.0002) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((519.0000) * transformY) + moveY),

        new createjs.Point(((290.3334) * transformX) + moveX, ((519.3333) * transformY) + moveY),
        new createjs.Point(((289.6666) * transformX) + moveX, ((519.6667) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((520.0000) * transformY) + moveY),

        new createjs.Point(((289.0000) * transformX) + moveX, ((520.6666) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((521.3334) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((522.0000) * transformY) + moveY),

        new createjs.Point(((288.6667) * transformX) + moveX, ((522.0000) * transformY) + moveY),
        new createjs.Point(((288.3333) * transformX) + moveX, ((522.0000) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((522.0000) * transformY) + moveY),

        new createjs.Point(((288.0000) * transformX) + moveX, ((522.6666) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((523.3334) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((524.0000) * transformY) + moveY),

        new createjs.Point(((287.6667) * transformX) + moveX, ((524.0000) * transformY) + moveY),
        new createjs.Point(((287.3333) * transformX) + moveX, ((524.0000) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((524.0000) * transformY) + moveY),

        new createjs.Point(((287.0000) * transformX) + moveX, ((524.6666) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((525.3334) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((526.0000) * transformY) + moveY),

        new createjs.Point(((286.6667) * transformX) + moveX, ((526.0000) * transformY) + moveY),
        new createjs.Point(((286.3333) * transformX) + moveX, ((526.0000) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((526.0000) * transformY) + moveY),

        new createjs.Point(((286.0000) * transformX) + moveX, ((526.6666) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((527.3334) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((528.0000) * transformY) + moveY),

        new createjs.Point(((285.6667) * transformX) + moveX, ((528.0000) * transformY) + moveY),
        new createjs.Point(((285.3333) * transformX) + moveX, ((528.0000) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((528.0000) * transformY) + moveY),

        new createjs.Point(((285.0000) * transformX) + moveX, ((528.6666) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((529.3334) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((530.0000) * transformY) + moveY),

        new createjs.Point(((284.3334) * transformX) + moveX, ((530.3333) * transformY) + moveY),
        new createjs.Point(((283.6666) * transformX) + moveX, ((530.6667) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((531.0000) * transformY) + moveY),

        new createjs.Point(((283.0000) * transformX) + moveX, ((531.6666) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((532.3334) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((533.0000) * transformY) + moveY),

        new createjs.Point(((282.6667) * transformX) + moveX, ((533.0000) * transformY) + moveY),
        new createjs.Point(((282.3333) * transformX) + moveX, ((533.0000) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((533.0000) * transformY) + moveY),

        new createjs.Point(((282.0000) * transformX) + moveX, ((533.6666) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((534.3334) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((535.0000) * transformY) + moveY),

        new createjs.Point(((281.6667) * transformX) + moveX, ((535.0000) * transformY) + moveY),
        new createjs.Point(((281.3333) * transformX) + moveX, ((535.0000) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((535.0000) * transformY) + moveY),

        new createjs.Point(((281.0000) * transformX) + moveX, ((535.6666) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((536.3334) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((537.0000) * transformY) + moveY),

        new createjs.Point(((280.6667) * transformX) + moveX, ((537.0000) * transformY) + moveY),
        new createjs.Point(((280.3333) * transformX) + moveX, ((537.0000) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((537.0000) * transformY) + moveY),

        new createjs.Point(((280.0000) * transformX) + moveX, ((537.6666) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((538.3334) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((539.0000) * transformY) + moveY),

        new createjs.Point(((279.6667) * transformX) + moveX, ((539.0000) * transformY) + moveY),
        new createjs.Point(((279.3333) * transformX) + moveX, ((539.0000) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((539.0000) * transformY) + moveY),

        new createjs.Point(((278.6667) * transformX) + moveX, ((540.3332) * transformY) + moveY),
        new createjs.Point(((278.3333) * transformX) + moveX, ((541.6668) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((543.0000) * transformY) + moveY),

        new createjs.Point(((277.3334) * transformX) + moveX, ((543.3333) * transformY) + moveY),
        new createjs.Point(((276.6666) * transformX) + moveX, ((543.6667) * transformY) + moveY),
        new createjs.Point(((276.0000) * transformX) + moveX, ((544.0000) * transformY) + moveY),

        new createjs.Point(((273.6669) * transformX) + moveX, ((549.3328) * transformY) + moveY),
        new createjs.Point(((271.3331) * transformX) + moveX, ((554.6672) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((560.0000) * transformY) + moveY),

        new createjs.Point(((268.3334) * transformX) + moveX, ((560.3333) * transformY) + moveY),
        new createjs.Point(((267.6666) * transformX) + moveX, ((560.6667) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((561.0000) * transformY) + moveY),

        new createjs.Point(((267.0000) * transformX) + moveX, ((561.6666) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((562.3334) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((563.0000) * transformY) + moveY),

        new createjs.Point(((266.6667) * transformX) + moveX, ((563.0000) * transformY) + moveY),
        new createjs.Point(((266.3333) * transformX) + moveX, ((563.0000) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((563.0000) * transformY) + moveY),

        new createjs.Point(((266.0000) * transformX) + moveX, ((563.6666) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((564.3334) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((565.0000) * transformY) + moveY),

        new createjs.Point(((265.6667) * transformX) + moveX, ((565.0000) * transformY) + moveY),
        new createjs.Point(((265.3333) * transformX) + moveX, ((565.0000) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((565.0000) * transformY) + moveY),

        new createjs.Point(((265.0000) * transformX) + moveX, ((565.6666) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((566.3334) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((567.0000) * transformY) + moveY),

        new createjs.Point(((264.6667) * transformX) + moveX, ((567.0000) * transformY) + moveY),
        new createjs.Point(((264.3333) * transformX) + moveX, ((567.0000) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((567.0000) * transformY) + moveY),

        new createjs.Point(((264.0000) * transformX) + moveX, ((567.6666) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((568.3334) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((569.0000) * transformY) + moveY),

        new createjs.Point(((263.6667) * transformX) + moveX, ((569.0000) * transformY) + moveY),
        new createjs.Point(((263.3333) * transformX) + moveX, ((569.0000) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((569.0000) * transformY) + moveY),

        new createjs.Point(((262.3334) * transformX) + moveX, ((570.9998) * transformY) + moveY),
        new createjs.Point(((261.6666) * transformX) + moveX, ((573.0002) * transformY) + moveY),
        new createjs.Point(((261.0000) * transformX) + moveX, ((575.0000) * transformY) + moveY),

        new createjs.Point(((260.6667) * transformX) + moveX, ((575.0000) * transformY) + moveY),
        new createjs.Point(((260.3333) * transformX) + moveX, ((575.0000) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((575.0000) * transformY) + moveY),

        new createjs.Point(((259.6667) * transformX) + moveX, ((576.3332) * transformY) + moveY),
        new createjs.Point(((259.3333) * transformX) + moveX, ((577.6668) * transformY) + moveY),
        new createjs.Point(((259.0000) * transformX) + moveX, ((579.0000) * transformY) + moveY),

        new createjs.Point(((258.6667) * transformX) + moveX, ((579.0000) * transformY) + moveY),
        new createjs.Point(((258.3333) * transformX) + moveX, ((579.0000) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((579.0000) * transformY) + moveY),

        new createjs.Point(((257.6667) * transformX) + moveX, ((580.3332) * transformY) + moveY),
        new createjs.Point(((257.3333) * transformX) + moveX, ((581.6668) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((583.0000) * transformY) + moveY),

        new createjs.Point(((256.6667) * transformX) + moveX, ((583.0000) * transformY) + moveY),
        new createjs.Point(((256.3333) * transformX) + moveX, ((583.0000) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((583.0000) * transformY) + moveY),

        new createjs.Point(((256.0000) * transformX) + moveX, ((583.6666) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((584.3334) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((585.0000) * transformY) + moveY),

        new createjs.Point(((255.6667) * transformX) + moveX, ((585.0000) * transformY) + moveY),
        new createjs.Point(((255.3333) * transformX) + moveX, ((585.0000) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((585.0000) * transformY) + moveY),

        new createjs.Point(((255.0000) * transformX) + moveX, ((585.6666) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((586.3334) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((587.0000) * transformY) + moveY),

        new createjs.Point(((254.6667) * transformX) + moveX, ((587.0000) * transformY) + moveY),
        new createjs.Point(((254.3333) * transformX) + moveX, ((587.0000) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((587.0000) * transformY) + moveY),

        new createjs.Point(((254.0000) * transformX) + moveX, ((587.6666) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((588.3334) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((589.0000) * transformY) + moveY),

        new createjs.Point(((253.6667) * transformX) + moveX, ((589.0000) * transformY) + moveY),
        new createjs.Point(((253.3333) * transformX) + moveX, ((589.0000) * transformY) + moveY),
        new createjs.Point(((253.0000) * transformX) + moveX, ((589.0000) * transformY) + moveY),

        new createjs.Point(((253.0000) * transformX) + moveX, ((589.6666) * transformY) + moveY),
        new createjs.Point(((253.0000) * transformX) + moveX, ((590.3334) * transformY) + moveY),
        new createjs.Point(((253.0000) * transformX) + moveX, ((591.0000) * transformY) + moveY),

        new createjs.Point(((252.6667) * transformX) + moveX, ((591.0000) * transformY) + moveY),
        new createjs.Point(((252.3333) * transformX) + moveX, ((591.0000) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((591.0000) * transformY) + moveY),

        new createjs.Point(((252.0000) * transformX) + moveX, ((591.6666) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((592.3334) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((593.0000) * transformY) + moveY),

        new createjs.Point(((251.6667) * transformX) + moveX, ((593.0000) * transformY) + moveY),
        new createjs.Point(((251.3333) * transformX) + moveX, ((593.0000) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((593.0000) * transformY) + moveY),

        new createjs.Point(((250.3334) * transformX) + moveX, ((594.9998) * transformY) + moveY),
        new createjs.Point(((249.6666) * transformX) + moveX, ((597.0002) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((599.0000) * transformY) + moveY),

        new createjs.Point(((248.6667) * transformX) + moveX, ((599.0000) * transformY) + moveY),
        new createjs.Point(((248.3333) * transformX) + moveX, ((599.0000) * transformY) + moveY),
        new createjs.Point(((248.0000) * transformX) + moveX, ((599.0000) * transformY) + moveY),

        new createjs.Point(((247.6667) * transformX) + moveX, ((600.3332) * transformY) + moveY),
        new createjs.Point(((247.3333) * transformX) + moveX, ((601.6668) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((603.0000) * transformY) + moveY),

        new createjs.Point(((246.6667) * transformX) + moveX, ((603.0000) * transformY) + moveY),
        new createjs.Point(((246.3333) * transformX) + moveX, ((603.0000) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((603.0000) * transformY) + moveY),

        new createjs.Point(((246.0000) * transformX) + moveX, ((603.6666) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((604.3334) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((605.0000) * transformY) + moveY),

        new createjs.Point(((245.6667) * transformX) + moveX, ((605.0000) * transformY) + moveY),
        new createjs.Point(((245.3333) * transformX) + moveX, ((605.0000) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((605.0000) * transformY) + moveY),

        new createjs.Point(((245.0000) * transformX) + moveX, ((605.6666) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((606.3334) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((607.0000) * transformY) + moveY),

        new createjs.Point(((244.6667) * transformX) + moveX, ((607.0000) * transformY) + moveY),
        new createjs.Point(((244.3333) * transformX) + moveX, ((607.0000) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((607.0000) * transformY) + moveY),

        new createjs.Point(((244.0000) * transformX) + moveX, ((607.6666) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((608.3334) * transformY) + moveY),
        new createjs.Point(((244.0000) * transformX) + moveX, ((609.0000) * transformY) + moveY),

        new createjs.Point(((243.6667) * transformX) + moveX, ((609.0000) * transformY) + moveY),
        new createjs.Point(((243.3333) * transformX) + moveX, ((609.0000) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((609.0000) * transformY) + moveY),

        new createjs.Point(((243.0000) * transformX) + moveX, ((609.6666) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((610.3334) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((611.0000) * transformY) + moveY),

        new createjs.Point(((242.6667) * transformX) + moveX, ((611.0000) * transformY) + moveY),
        new createjs.Point(((242.3333) * transformX) + moveX, ((611.0000) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((611.0000) * transformY) + moveY),

        new createjs.Point(((242.0000) * transformX) + moveX, ((611.6666) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((612.3334) * transformY) + moveY),
        new createjs.Point(((242.0000) * transformX) + moveX, ((613.0000) * transformY) + moveY),

        new createjs.Point(((241.6667) * transformX) + moveX, ((613.0000) * transformY) + moveY),
        new createjs.Point(((241.3333) * transformX) + moveX, ((613.0000) * transformY) + moveY),
        new createjs.Point(((241.0000) * transformX) + moveX, ((613.0000) * transformY) + moveY),

        new createjs.Point(((240.6667) * transformX) + moveX, ((614.3332) * transformY) + moveY),
        new createjs.Point(((240.3333) * transformX) + moveX, ((615.6668) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((617.0000) * transformY) + moveY),

        new createjs.Point(((239.6667) * transformX) + moveX, ((617.0000) * transformY) + moveY),
        new createjs.Point(((239.3333) * transformX) + moveX, ((617.0000) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((617.0000) * transformY) + moveY),

        new createjs.Point(((239.0000) * transformX) + moveX, ((617.6666) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((618.3334) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((619.0000) * transformY) + moveY),

        new createjs.Point(((238.6667) * transformX) + moveX, ((619.0000) * transformY) + moveY),
        new createjs.Point(((238.3333) * transformX) + moveX, ((619.0000) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((619.0000) * transformY) + moveY),

        new createjs.Point(((238.0000) * transformX) + moveX, ((619.9999) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((621.0001) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((622.0000) * transformY) + moveY),

        new createjs.Point(((237.6667) * transformX) + moveX, ((622.0000) * transformY) + moveY),
        new createjs.Point(((237.3333) * transformX) + moveX, ((622.0000) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((622.0000) * transformY) + moveY),

        new createjs.Point(((236.6667) * transformX) + moveX, ((623.3332) * transformY) + moveY),
        new createjs.Point(((236.3333) * transformX) + moveX, ((624.6668) * transformY) + moveY),
        new createjs.Point(((236.0000) * transformX) + moveX, ((626.0000) * transformY) + moveY),

        new createjs.Point(((235.6667) * transformX) + moveX, ((626.0000) * transformY) + moveY),
        new createjs.Point(((235.3333) * transformX) + moveX, ((626.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((626.0000) * transformY) + moveY),

        new createjs.Point(((235.0000) * transformX) + moveX, ((626.6666) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((627.3334) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((628.0000) * transformY) + moveY),

        new createjs.Point(((234.6667) * transformX) + moveX, ((628.0000) * transformY) + moveY),
        new createjs.Point(((234.3333) * transformX) + moveX, ((628.0000) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((628.0000) * transformY) + moveY),

        new createjs.Point(((234.0000) * transformX) + moveX, ((628.6666) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((629.3334) * transformY) + moveY),
        new createjs.Point(((234.0000) * transformX) + moveX, ((630.0000) * transformY) + moveY),

        new createjs.Point(((233.6667) * transformX) + moveX, ((630.0000) * transformY) + moveY),
        new createjs.Point(((233.3333) * transformX) + moveX, ((630.0000) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((630.0000) * transformY) + moveY),

        new createjs.Point(((232.3334) * transformX) + moveX, ((631.9998) * transformY) + moveY),
        new createjs.Point(((231.6666) * transformX) + moveX, ((634.0002) * transformY) + moveY),
        new createjs.Point(((231.0000) * transformX) + moveX, ((636.0000) * transformY) + moveY),

        new createjs.Point(((230.6667) * transformX) + moveX, ((636.0000) * transformY) + moveY),
        new createjs.Point(((230.3333) * transformX) + moveX, ((636.0000) * transformY) + moveY),
        new createjs.Point(((230.0000) * transformX) + moveX, ((636.0000) * transformY) + moveY),

        new createjs.Point(((230.0000) * transformX) + moveX, ((636.9999) * transformY) + moveY),
        new createjs.Point(((230.0000) * transformX) + moveX, ((638.0001) * transformY) + moveY),
        new createjs.Point(((230.0000) * transformX) + moveX, ((639.0000) * transformY) + moveY),

        new createjs.Point(((229.6667) * transformX) + moveX, ((639.0000) * transformY) + moveY),
        new createjs.Point(((229.3333) * transformX) + moveX, ((639.0000) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((639.0000) * transformY) + moveY),

        new createjs.Point(((229.0000) * transformX) + moveX, ((639.6666) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((640.3334) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((641.0000) * transformY) + moveY),

        new createjs.Point(((228.6667) * transformX) + moveX, ((641.0000) * transformY) + moveY),
        new createjs.Point(((228.3333) * transformX) + moveX, ((641.0000) * transformY) + moveY),
        new createjs.Point(((228.0000) * transformX) + moveX, ((641.0000) * transformY) + moveY),

        new createjs.Point(((228.0000) * transformX) + moveX, ((641.6666) * transformY) + moveY),
        new createjs.Point(((228.0000) * transformX) + moveX, ((642.3334) * transformY) + moveY),
        new createjs.Point(((228.0000) * transformX) + moveX, ((643.0000) * transformY) + moveY),

        new createjs.Point(((227.6667) * transformX) + moveX, ((643.0000) * transformY) + moveY),
        new createjs.Point(((227.3333) * transformX) + moveX, ((643.0000) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((643.0000) * transformY) + moveY),

        new createjs.Point(((226.3334) * transformX) + moveX, ((644.9998) * transformY) + moveY),
        new createjs.Point(((225.6666) * transformX) + moveX, ((647.0002) * transformY) + moveY),
        new createjs.Point(((225.0000) * transformX) + moveX, ((649.0000) * transformY) + moveY),

        new createjs.Point(((224.6667) * transformX) + moveX, ((649.0000) * transformY) + moveY),
        new createjs.Point(((224.3333) * transformX) + moveX, ((649.0000) * transformY) + moveY),
        new createjs.Point(((224.0000) * transformX) + moveX, ((649.0000) * transformY) + moveY),

        new createjs.Point(((223.0001) * transformX) + moveX, ((651.9997) * transformY) + moveY),
        new createjs.Point(((221.9999) * transformX) + moveX, ((655.0003) * transformY) + moveY),
        new createjs.Point(((221.0000) * transformX) + moveX, ((658.0000) * transformY) + moveY),

        new createjs.Point(((220.6667) * transformX) + moveX, ((658.0000) * transformY) + moveY),
        new createjs.Point(((220.3333) * transformX) + moveX, ((658.0000) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((658.0000) * transformY) + moveY),

        new createjs.Point(((220.0000) * transformX) + moveX, ((658.6666) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((659.3334) * transformY) + moveY),
        new createjs.Point(((220.0000) * transformX) + moveX, ((660.0000) * transformY) + moveY),

        new createjs.Point(((219.6667) * transformX) + moveX, ((660.0000) * transformY) + moveY),
        new createjs.Point(((219.3333) * transformX) + moveX, ((660.0000) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((660.0000) * transformY) + moveY),

        new createjs.Point(((219.0000) * transformX) + moveX, ((660.9999) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((662.0001) * transformY) + moveY),
        new createjs.Point(((219.0000) * transformX) + moveX, ((663.0000) * transformY) + moveY),

        new createjs.Point(((218.6667) * transformX) + moveX, ((663.0000) * transformY) + moveY),
        new createjs.Point(((218.3333) * transformX) + moveX, ((663.0000) * transformY) + moveY),
        new createjs.Point(((218.0000) * transformX) + moveX, ((663.0000) * transformY) + moveY),

        new createjs.Point(((218.0000) * transformX) + moveX, ((663.6666) * transformY) + moveY),
        new createjs.Point(((218.0000) * transformX) + moveX, ((664.3334) * transformY) + moveY),
        new createjs.Point(((218.0000) * transformX) + moveX, ((665.0000) * transformY) + moveY),

        new createjs.Point(((217.6667) * transformX) + moveX, ((665.0000) * transformY) + moveY),
        new createjs.Point(((217.3333) * transformX) + moveX, ((665.0000) * transformY) + moveY),
        new createjs.Point(((217.0000) * transformX) + moveX, ((665.0000) * transformY) + moveY),

        new createjs.Point(((216.6667) * transformX) + moveX, ((666.3332) * transformY) + moveY),
        new createjs.Point(((216.3333) * transformX) + moveX, ((667.6668) * transformY) + moveY),
        new createjs.Point(((216.0000) * transformX) + moveX, ((669.0000) * transformY) + moveY),

        new createjs.Point(((215.6667) * transformX) + moveX, ((669.0000) * transformY) + moveY),
        new createjs.Point(((215.3333) * transformX) + moveX, ((669.0000) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((669.0000) * transformY) + moveY),

        new createjs.Point(((215.0000) * transformX) + moveX, ((669.9999) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((671.0001) * transformY) + moveY),
        new createjs.Point(((215.0000) * transformX) + moveX, ((672.0000) * transformY) + moveY),

        new createjs.Point(((214.6667) * transformX) + moveX, ((672.0000) * transformY) + moveY),
        new createjs.Point(((214.3333) * transformX) + moveX, ((672.0000) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((672.0000) * transformY) + moveY),

        new createjs.Point(((214.0000) * transformX) + moveX, ((672.6666) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((673.3334) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((674.0000) * transformY) + moveY),

        new createjs.Point(((213.6667) * transformX) + moveX, ((674.0000) * transformY) + moveY),
        new createjs.Point(((213.3333) * transformX) + moveX, ((674.0000) * transformY) + moveY),
        new createjs.Point(((213.0000) * transformX) + moveX, ((674.0000) * transformY) + moveY),

        new createjs.Point(((212.6667) * transformX) + moveX, ((675.3332) * transformY) + moveY),
        new createjs.Point(((212.3333) * transformX) + moveX, ((676.6668) * transformY) + moveY),
        new createjs.Point(((212.0000) * transformX) + moveX, ((678.0000) * transformY) + moveY),

        new createjs.Point(((211.6667) * transformX) + moveX, ((678.0000) * transformY) + moveY),
        new createjs.Point(((211.3333) * transformX) + moveX, ((678.0000) * transformY) + moveY),
        new createjs.Point(((211.0000) * transformX) + moveX, ((678.0000) * transformY) + moveY),

        new createjs.Point(((211.0000) * transformX) + moveX, ((678.9999) * transformY) + moveY),
        new createjs.Point(((211.0000) * transformX) + moveX, ((680.0001) * transformY) + moveY),
        new createjs.Point(((211.0000) * transformX) + moveX, ((681.0000) * transformY) + moveY),

        new createjs.Point(((210.6667) * transformX) + moveX, ((681.0000) * transformY) + moveY),
        new createjs.Point(((210.3333) * transformX) + moveX, ((681.0000) * transformY) + moveY),
        new createjs.Point(((210.0000) * transformX) + moveX, ((681.0000) * transformY) + moveY),

        new createjs.Point(((209.6667) * transformX) + moveX, ((682.3332) * transformY) + moveY),
        new createjs.Point(((209.3333) * transformX) + moveX, ((683.6668) * transformY) + moveY),
        new createjs.Point(((209.0000) * transformX) + moveX, ((685.0000) * transformY) + moveY),

        new createjs.Point(((208.6667) * transformX) + moveX, ((685.0000) * transformY) + moveY),
        new createjs.Point(((208.3333) * transformX) + moveX, ((685.0000) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((685.0000) * transformY) + moveY),

        new createjs.Point(((208.0000) * transformX) + moveX, ((685.9999) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((687.0001) * transformY) + moveY),
        new createjs.Point(((208.0000) * transformX) + moveX, ((688.0000) * transformY) + moveY),

        new createjs.Point(((207.6667) * transformX) + moveX, ((688.0000) * transformY) + moveY),
        new createjs.Point(((207.3333) * transformX) + moveX, ((688.0000) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((688.0000) * transformY) + moveY),

        new createjs.Point(((206.6667) * transformX) + moveX, ((689.3332) * transformY) + moveY),
        new createjs.Point(((206.3333) * transformX) + moveX, ((690.6668) * transformY) + moveY),
        new createjs.Point(((206.0000) * transformX) + moveX, ((692.0000) * transformY) + moveY),

        new createjs.Point(((205.6667) * transformX) + moveX, ((692.0000) * transformY) + moveY),
        new createjs.Point(((205.3333) * transformX) + moveX, ((692.0000) * transformY) + moveY),
        new createjs.Point(((205.0000) * transformX) + moveX, ((692.0000) * transformY) + moveY),

        new createjs.Point(((205.0000) * transformX) + moveX, ((692.9999) * transformY) + moveY),
        new createjs.Point(((205.0000) * transformX) + moveX, ((694.0001) * transformY) + moveY),
        new createjs.Point(((205.0000) * transformX) + moveX, ((695.0000) * transformY) + moveY),

        new createjs.Point(((204.6667) * transformX) + moveX, ((695.0000) * transformY) + moveY),
        new createjs.Point(((204.3333) * transformX) + moveX, ((695.0000) * transformY) + moveY),
        new createjs.Point(((204.0000) * transformX) + moveX, ((695.0000) * transformY) + moveY),

        new createjs.Point(((203.6667) * transformX) + moveX, ((696.3332) * transformY) + moveY),
        new createjs.Point(((203.3333) * transformX) + moveX, ((697.6668) * transformY) + moveY),
        new createjs.Point(((203.0000) * transformX) + moveX, ((699.0000) * transformY) + moveY),

        new createjs.Point(((202.6667) * transformX) + moveX, ((699.0000) * transformY) + moveY),
        new createjs.Point(((202.3333) * transformX) + moveX, ((699.0000) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((699.0000) * transformY) + moveY),

        new createjs.Point(((202.0000) * transformX) + moveX, ((699.9999) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((701.0001) * transformY) + moveY),
        new createjs.Point(((202.0000) * transformX) + moveX, ((702.0000) * transformY) + moveY),

        new createjs.Point(((201.6667) * transformX) + moveX, ((702.0000) * transformY) + moveY),
        new createjs.Point(((201.3333) * transformX) + moveX, ((702.0000) * transformY) + moveY),
        new createjs.Point(((201.0000) * transformX) + moveX, ((702.0000) * transformY) + moveY),

        new createjs.Point(((200.6667) * transformX) + moveX, ((703.3332) * transformY) + moveY),
        new createjs.Point(((200.3333) * transformX) + moveX, ((704.6668) * transformY) + moveY),
        new createjs.Point(((200.0000) * transformX) + moveX, ((706.0000) * transformY) + moveY),

        new createjs.Point(((199.6667) * transformX) + moveX, ((706.0000) * transformY) + moveY),
        new createjs.Point(((199.3333) * transformX) + moveX, ((706.0000) * transformY) + moveY),
        new createjs.Point(((199.0000) * transformX) + moveX, ((706.0000) * transformY) + moveY),

        new createjs.Point(((199.0000) * transformX) + moveX, ((706.9999) * transformY) + moveY),
        new createjs.Point(((199.0000) * transformX) + moveX, ((708.0001) * transformY) + moveY),
        new createjs.Point(((199.0000) * transformX) + moveX, ((709.0000) * transformY) + moveY),

        new createjs.Point(((198.6667) * transformX) + moveX, ((709.0000) * transformY) + moveY),
        new createjs.Point(((198.3333) * transformX) + moveX, ((709.0000) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((709.0000) * transformY) + moveY),

        new createjs.Point(((198.0000) * transformX) + moveX, ((709.6666) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((710.3334) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((711.0000) * transformY) + moveY),

        new createjs.Point(((197.6667) * transformX) + moveX, ((711.0000) * transformY) + moveY),
        new createjs.Point(((197.3333) * transformX) + moveX, ((711.0000) * transformY) + moveY),
        new createjs.Point(((197.0000) * transformX) + moveX, ((711.0000) * transformY) + moveY),

        new createjs.Point(((196.6667) * transformX) + moveX, ((712.6665) * transformY) + moveY),
        new createjs.Point(((196.3333) * transformX) + moveX, ((714.3335) * transformY) + moveY),
        new createjs.Point(((196.0000) * transformX) + moveX, ((716.0000) * transformY) + moveY),

        new createjs.Point(((195.6667) * transformX) + moveX, ((716.0000) * transformY) + moveY),
        new createjs.Point(((195.3333) * transformX) + moveX, ((716.0000) * transformY) + moveY),
        new createjs.Point(((195.0000) * transformX) + moveX, ((716.0000) * transformY) + moveY),

        new createjs.Point(((195.0000) * transformX) + moveX, ((716.9999) * transformY) + moveY),
        new createjs.Point(((195.0000) * transformX) + moveX, ((718.0001) * transformY) + moveY),
        new createjs.Point(((195.0000) * transformX) + moveX, ((719.0000) * transformY) + moveY),

        new createjs.Point(((194.6667) * transformX) + moveX, ((719.0000) * transformY) + moveY),
        new createjs.Point(((194.3333) * transformX) + moveX, ((719.0000) * transformY) + moveY),
        new createjs.Point(((194.0000) * transformX) + moveX, ((719.0000) * transformY) + moveY),

        new createjs.Point(((193.6667) * transformX) + moveX, ((720.3332) * transformY) + moveY),
        new createjs.Point(((193.3333) * transformX) + moveX, ((721.6668) * transformY) + moveY),
        new createjs.Point(((193.0000) * transformX) + moveX, ((723.0000) * transformY) + moveY),

        new createjs.Point(((192.6667) * transformX) + moveX, ((723.0000) * transformY) + moveY),
        new createjs.Point(((192.3333) * transformX) + moveX, ((723.0000) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((723.0000) * transformY) + moveY),

        new createjs.Point(((192.0000) * transformX) + moveX, ((723.9999) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((725.0001) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((726.0000) * transformY) + moveY),

        new createjs.Point(((191.6667) * transformX) + moveX, ((726.0000) * transformY) + moveY),
        new createjs.Point(((191.3333) * transformX) + moveX, ((726.0000) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((726.0000) * transformY) + moveY),

        new createjs.Point(((191.0000) * transformX) + moveX, ((726.6666) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((727.3334) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((728.0000) * transformY) + moveY),

        new createjs.Point(((190.6667) * transformX) + moveX, ((728.0000) * transformY) + moveY),
        new createjs.Point(((190.3333) * transformX) + moveX, ((728.0000) * transformY) + moveY),
        new createjs.Point(((190.0000) * transformX) + moveX, ((728.0000) * transformY) + moveY),

        new createjs.Point(((189.6667) * transformX) + moveX, ((729.6665) * transformY) + moveY),
        new createjs.Point(((189.3333) * transformX) + moveX, ((731.3335) * transformY) + moveY),
        new createjs.Point(((189.0000) * transformX) + moveX, ((733.0000) * transformY) + moveY),

        new createjs.Point(((188.6667) * transformX) + moveX, ((733.0000) * transformY) + moveY),
        new createjs.Point(((188.3333) * transformX) + moveX, ((733.0000) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((733.0000) * transformY) + moveY),

        new createjs.Point(((188.0000) * transformX) + moveX, ((733.9999) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((735.0001) * transformY) + moveY),
        new createjs.Point(((188.0000) * transformX) + moveX, ((736.0000) * transformY) + moveY),

        new createjs.Point(((187.6667) * transformX) + moveX, ((736.0000) * transformY) + moveY),
        new createjs.Point(((187.3333) * transformX) + moveX, ((736.0000) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((736.0000) * transformY) + moveY),

        new createjs.Point(((187.0000) * transformX) + moveX, ((736.6666) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((737.3334) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((738.0000) * transformY) + moveY),

        new createjs.Point(((186.6667) * transformX) + moveX, ((738.0000) * transformY) + moveY),
        new createjs.Point(((186.3333) * transformX) + moveX, ((738.0000) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((738.0000) * transformY) + moveY),

        new createjs.Point(((186.0000) * transformX) + moveX, ((738.9999) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((740.0001) * transformY) + moveY),
        new createjs.Point(((186.0000) * transformX) + moveX, ((741.0000) * transformY) + moveY),

        new createjs.Point(((185.6667) * transformX) + moveX, ((741.0000) * transformY) + moveY),
        new createjs.Point(((185.3333) * transformX) + moveX, ((741.0000) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((741.0000) * transformY) + moveY),

        new createjs.Point(((185.0000) * transformX) + moveX, ((741.6666) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((742.3334) * transformY) + moveY),
        new createjs.Point(((185.0000) * transformX) + moveX, ((743.0000) * transformY) + moveY),

        new createjs.Point(((184.6667) * transformX) + moveX, ((743.0000) * transformY) + moveY),
        new createjs.Point(((184.3333) * transformX) + moveX, ((743.0000) * transformY) + moveY),
        new createjs.Point(((184.0000) * transformX) + moveX, ((743.0000) * transformY) + moveY),

        new createjs.Point(((183.6667) * transformX) + moveX, ((744.9998) * transformY) + moveY),
        new createjs.Point(((183.3333) * transformX) + moveX, ((747.0002) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((749.0000) * transformY) + moveY),

        new createjs.Point(((182.6667) * transformX) + moveX, ((749.0000) * transformY) + moveY),
        new createjs.Point(((182.3333) * transformX) + moveX, ((749.0000) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((749.0000) * transformY) + moveY),

        new createjs.Point(((182.0000) * transformX) + moveX, ((749.6666) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((750.3334) * transformY) + moveY),
        new createjs.Point(((182.0000) * transformX) + moveX, ((751.0000) * transformY) + moveY),

        new createjs.Point(((181.6667) * transformX) + moveX, ((751.0000) * transformY) + moveY),
        new createjs.Point(((181.3333) * transformX) + moveX, ((751.0000) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((751.0000) * transformY) + moveY),

        new createjs.Point(((181.0000) * transformX) + moveX, ((751.9999) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((753.0001) * transformY) + moveY),
        new createjs.Point(((181.0000) * transformX) + moveX, ((754.0000) * transformY) + moveY),

        new createjs.Point(((180.6667) * transformX) + moveX, ((754.0000) * transformY) + moveY),
        new createjs.Point(((180.3333) * transformX) + moveX, ((754.0000) * transformY) + moveY),
        new createjs.Point(((180.0000) * transformX) + moveX, ((754.0000) * transformY) + moveY),

        new createjs.Point(((180.0000) * transformX) + moveX, ((754.6666) * transformY) + moveY),
        new createjs.Point(((180.0000) * transformX) + moveX, ((755.3334) * transformY) + moveY),
        new createjs.Point(((180.0000) * transformX) + moveX, ((756.0000) * transformY) + moveY),

        new createjs.Point(((179.6667) * transformX) + moveX, ((756.0000) * transformY) + moveY),
        new createjs.Point(((179.3333) * transformX) + moveX, ((756.0000) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((756.0000) * transformY) + moveY),

        new createjs.Point(((179.0000) * transformX) + moveX, ((756.9999) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((758.0001) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((759.0000) * transformY) + moveY),

        new createjs.Point(((178.6667) * transformX) + moveX, ((759.0000) * transformY) + moveY),
        new createjs.Point(((178.3333) * transformX) + moveX, ((759.0000) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((759.0000) * transformY) + moveY),

        new createjs.Point(((178.0000) * transformX) + moveX, ((759.9999) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((761.0001) * transformY) + moveY),
        new createjs.Point(((178.0000) * transformX) + moveX, ((762.0000) * transformY) + moveY),

        new createjs.Point(((177.6667) * transformX) + moveX, ((762.0000) * transformY) + moveY),
        new createjs.Point(((177.3333) * transformX) + moveX, ((762.0000) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((762.0000) * transformY) + moveY),

        new createjs.Point(((177.0000) * transformX) + moveX, ((762.6666) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((763.3334) * transformY) + moveY),
        new createjs.Point(((177.0000) * transformX) + moveX, ((764.0000) * transformY) + moveY),

        new createjs.Point(((176.6667) * transformX) + moveX, ((764.0000) * transformY) + moveY),
        new createjs.Point(((176.3333) * transformX) + moveX, ((764.0000) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((764.0000) * transformY) + moveY),

        new createjs.Point(((176.0000) * transformX) + moveX, ((764.9999) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((766.0001) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((767.0000) * transformY) + moveY),

        new createjs.Point(((175.6667) * transformX) + moveX, ((767.0000) * transformY) + moveY),
        new createjs.Point(((175.3333) * transformX) + moveX, ((767.0000) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((767.0000) * transformY) + moveY),

        new createjs.Point(((175.0000) * transformX) + moveX, ((767.6666) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((768.3334) * transformY) + moveY),
        new createjs.Point(((175.0000) * transformX) + moveX, ((769.0000) * transformY) + moveY),

        new createjs.Point(((174.6667) * transformX) + moveX, ((769.0000) * transformY) + moveY),
        new createjs.Point(((174.3333) * transformX) + moveX, ((769.0000) * transformY) + moveY),
        new createjs.Point(((174.0000) * transformX) + moveX, ((769.0000) * transformY) + moveY),

        new createjs.Point(((173.6667) * transformX) + moveX, ((770.9998) * transformY) + moveY),
        new createjs.Point(((173.3333) * transformX) + moveX, ((773.0002) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),

        new createjs.Point(((172.6667) * transformX) + moveX, ((775.0000) * transformY) + moveY),
        new createjs.Point(((172.3333) * transformX) + moveX, ((775.0000) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),

        new createjs.Point(((172.0000) * transformX) + moveX, ((775.6666) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((776.3334) * transformY) + moveY),
        new createjs.Point(((172.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((171.6667) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((171.3333) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((170.3334) * transformX) + moveX, ((779.9997) * transformY) + moveY),
        new createjs.Point(((169.6666) * transformX) + moveX, ((783.0003) * transformY) + moveY),
        new createjs.Point(((169.0000) * transformX) + moveX, ((786.0000) * transformY) + moveY),

        new createjs.Point(((155.1687) * transformX) + moveX, ((817.6206) * transformY) + moveY),
        new createjs.Point(((146.0634) * transformX) + moveX, ((853.6495) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((887.0000) * transformY) + moveY),

        new createjs.Point(((134.0000) * transformX) + moveX, ((888.3332) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((889.6668) * transformY) + moveY),
        new createjs.Point(((134.0000) * transformX) + moveX, ((891.0000) * transformY) + moveY),

        new createjs.Point(((129.3493) * transformX) + moveX, ((903.7866) * transformY) + moveY),
        new createjs.Point(((125.6219) * transformX) + moveX, ((919.0969) * transformY) + moveY),
        new createjs.Point(((121.0000) * transformX) + moveX, ((932.0000) * transformY) + moveY),

        new createjs.Point(((119.6668) * transformX) + moveX, ((938.3327) * transformY) + moveY),
        new createjs.Point(((118.3332) * transformX) + moveX, ((944.6673) * transformY) + moveY),
        new createjs.Point(((117.0000) * transformX) + moveX, ((951.0000) * transformY) + moveY),

        new createjs.Point(((110.4709) * transformX) + moveX, ((970.0716) * transformY) + moveY),
        new createjs.Point(((107.2799) * transformX) + moveX, ((992.5300) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1012.0000) * transformY) + moveY),

        new createjs.Point(((101.0000) * transformX) + moveX, ((1013.6665) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1015.3335) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),

        new createjs.Point(((100.6667) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((100.3333) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),

        new createjs.Point(((99.6667) * transformX) + moveX, ((1019.9997) * transformY) + moveY),
        new createjs.Point(((99.3333) * transformX) + moveX, ((1023.0003) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1026.0000) * transformY) + moveY),

        new createjs.Point(((98.6667) * transformX) + moveX, ((1026.0000) * transformY) + moveY),
        new createjs.Point(((98.3333) * transformX) + moveX, ((1026.0000) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1026.0000) * transformY) + moveY),

        new createjs.Point(((98.0000) * transformX) + moveX, ((1027.3332) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1028.6668) * transformY) + moveY),
        new createjs.Point(((98.0000) * transformX) + moveX, ((1030.0000) * transformY) + moveY),

        new createjs.Point(((97.6667) * transformX) + moveX, ((1030.0000) * transformY) + moveY),
        new createjs.Point(((97.3333) * transformX) + moveX, ((1030.0000) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1030.0000) * transformY) + moveY),

        new createjs.Point(((97.0000) * transformX) + moveX, ((1031.6665) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1033.3335) * transformY) + moveY),
        new createjs.Point(((97.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),

        new createjs.Point(((96.6667) * transformX) + moveX, ((1035.0000) * transformY) + moveY),
        new createjs.Point(((96.3333) * transformX) + moveX, ((1035.0000) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),

        new createjs.Point(((96.0000) * transformX) + moveX, ((1036.6665) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((1038.3335) * transformY) + moveY),
        new createjs.Point(((96.0000) * transformX) + moveX, ((1040.0000) * transformY) + moveY),

        new createjs.Point(((91.0656) * transformX) + moveX, ((1055.6713) * transformY) + moveY),
        new createjs.Point(((89.8430) * transformX) + moveX, ((1075.0432) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1091.0000) * transformY) + moveY),

        new createjs.Point(((85.0000) * transformX) + moveX, ((1092.9998) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1095.0002) * transformY) + moveY),
        new createjs.Point(((85.0000) * transformX) + moveX, ((1097.0000) * transformY) + moveY),

        new createjs.Point(((83.4391) * transformX) + moveX, ((1102.1940) * transformY) + moveY),
        new createjs.Point(((82.5539) * transformX) + moveX, ((1109.8034) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1115.0000) * transformY) + moveY),

        new createjs.Point(((81.0000) * transformX) + moveX, ((1116.9998) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1119.0002) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1121.0000) * transformY) + moveY),

        new createjs.Point(((79.3767) * transformX) + moveX, ((1126.4142) * transformY) + moveY),
        new createjs.Point(((78.5958) * transformX) + moveX, ((1134.5755) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1140.0000) * transformY) + moveY),

        new createjs.Point(((76.6667) * transformX) + moveX, ((1144.6662) * transformY) + moveY),
        new createjs.Point(((76.3333) * transformX) + moveX, ((1149.3338) * transformY) + moveY),
        new createjs.Point(((76.0000) * transformX) + moveX, ((1154.0000) * transformY) + moveY),

        new createjs.Point(((72.5951) * transformX) + moveX, ((1165.6491) * transformY) + moveY),
        new createjs.Point(((73.3727) * transformX) + moveX, ((1181.2405) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1193.0000) * transformY) + moveY),

        new createjs.Point(((70.0000) * transformX) + moveX, ((1195.9997) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1199.0003) * transformY) + moveY),
        new createjs.Point(((70.0000) * transformX) + moveX, ((1202.0000) * transformY) + moveY),

        new createjs.Point(((69.6667) * transformX) + moveX, ((1202.0000) * transformY) + moveY),
        new createjs.Point(((69.3333) * transformX) + moveX, ((1202.0000) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((1202.0000) * transformY) + moveY),

        new createjs.Point(((69.0000) * transformX) + moveX, ((1204.9997) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((1208.0003) * transformY) + moveY),
        new createjs.Point(((69.0000) * transformX) + moveX, ((1211.0000) * transformY) + moveY),

        new createjs.Point(((68.6667) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((68.3333) * transformX) + moveX, ((1211.0000) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1211.0000) * transformY) + moveY),

        new createjs.Point(((68.0000) * transformX) + moveX, ((1214.3330) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1217.6670) * transformY) + moveY),
        new createjs.Point(((68.0000) * transformX) + moveX, ((1221.0000) * transformY) + moveY),

        new createjs.Point(((67.6667) * transformX) + moveX, ((1221.0000) * transformY) + moveY),
        new createjs.Point(((67.3333) * transformX) + moveX, ((1221.0000) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1221.0000) * transformY) + moveY),

        new createjs.Point(((67.0000) * transformX) + moveX, ((1224.6663) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1228.3337) * transformY) + moveY),
        new createjs.Point(((67.0000) * transformX) + moveX, ((1232.0000) * transformY) + moveY),

        new createjs.Point(((66.6667) * transformX) + moveX, ((1232.0000) * transformY) + moveY),
        new createjs.Point(((66.3333) * transformX) + moveX, ((1232.0000) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1232.0000) * transformY) + moveY),

        new createjs.Point(((66.0000) * transformX) + moveX, ((1235.6663) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1239.3337) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1243.0000) * transformY) + moveY),

        new createjs.Point(((65.6667) * transformX) + moveX, ((1243.0000) * transformY) + moveY),
        new createjs.Point(((65.3333) * transformX) + moveX, ((1243.0000) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1243.0000) * transformY) + moveY),

        new createjs.Point(((65.0000) * transformX) + moveX, ((1246.6663) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1250.3337) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1254.0000) * transformY) + moveY),

        new createjs.Point(((65.0000) * transformX) + moveX, ((1254.3333) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1254.6667) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1255.0000) * transformY) + moveY),

        new createjs.Point(((64.6667) * transformX) + moveX, ((1255.0000) * transformY) + moveY),
        new createjs.Point(((64.3333) * transformX) + moveX, ((1255.0000) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1255.0000) * transformY) + moveY),

        new createjs.Point(((64.0000) * transformX) + moveX, ((1259.6662) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1264.3338) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1269.0000) * transformY) + moveY),

        new createjs.Point(((63.6667) * transformX) + moveX, ((1269.0000) * transformY) + moveY),
        new createjs.Point(((63.3333) * transformX) + moveX, ((1269.0000) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1269.0000) * transformY) + moveY),

        new createjs.Point(((62.6667) * transformX) + moveX, ((1280.9988) * transformY) + moveY),
        new createjs.Point(((62.3333) * transformX) + moveX, ((1293.0012) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1305.0000) * transformY) + moveY),

        new createjs.Point(((59.8799) * transformX) + moveX, ((1312.5912) * transformY) + moveY),
        new createjs.Point(((62.4886) * transformX) + moveX, ((1323.9684) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1333.0000) * transformY) + moveY),

        new createjs.Point(((59.3846) * transformX) + moveX, ((1342.8013) * transformY) + moveY),
        new createjs.Point(((60.0025) * transformX) + moveX, ((1356.5664) * transformY) + moveY),
        new createjs.Point(((60.0000) * transformX) + moveX, ((1368.0000) * transformY) + moveY),

        new createjs.Point(((59.9980) * transformX) + moveX, ((1377.1996) * transformY) + moveY),
        new createjs.Point(((58.8844) * transformX) + moveX, ((1389.5115) * transformY) + moveY),
        new createjs.Point(((61.0000) * transformX) + moveX, ((1397.0000) * transformY) + moveY),

        new createjs.Point(((62.7109) * transformX) + moveX, ((1403.0561) * transformY) + moveY),
        new createjs.Point(((60.4580) * transformX) + moveX, ((1412.5534) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1418.0000) * transformY) + moveY),

        new createjs.Point(((62.0000) * transformX) + moveX, ((1422.6662) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1427.3338) * transformY) + moveY),
        new createjs.Point(((62.0000) * transformX) + moveX, ((1432.0000) * transformY) + moveY),

        new createjs.Point(((62.3333) * transformX) + moveX, ((1432.0000) * transformY) + moveY),
        new createjs.Point(((62.6667) * transformX) + moveX, ((1432.0000) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1432.0000) * transformY) + moveY),

        new createjs.Point(((63.0000) * transformX) + moveX, ((1435.9996) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1440.0004) * transformY) + moveY),
        new createjs.Point(((63.0000) * transformX) + moveX, ((1444.0000) * transformY) + moveY),

        new createjs.Point(((63.3333) * transformX) + moveX, ((1444.0000) * transformY) + moveY),
        new createjs.Point(((63.6667) * transformX) + moveX, ((1444.0000) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1444.0000) * transformY) + moveY),

        new createjs.Point(((64.0000) * transformX) + moveX, ((1447.3330) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1450.6670) * transformY) + moveY),
        new createjs.Point(((64.0000) * transformX) + moveX, ((1454.0000) * transformY) + moveY),

        new createjs.Point(((64.3333) * transformX) + moveX, ((1454.0000) * transformY) + moveY),
        new createjs.Point(((64.6667) * transformX) + moveX, ((1454.0000) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1454.0000) * transformY) + moveY),

        new createjs.Point(((65.0000) * transformX) + moveX, ((1456.6664) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1459.3336) * transformY) + moveY),
        new createjs.Point(((65.0000) * transformX) + moveX, ((1462.0000) * transformY) + moveY),

        new createjs.Point(((65.3333) * transformX) + moveX, ((1462.0000) * transformY) + moveY),
        new createjs.Point(((65.6667) * transformX) + moveX, ((1462.0000) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1462.0000) * transformY) + moveY),

        new createjs.Point(((66.0000) * transformX) + moveX, ((1464.9997) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1468.0003) * transformY) + moveY),
        new createjs.Point(((66.0000) * transformX) + moveX, ((1471.0000) * transformY) + moveY),

        new createjs.Point(((68.9239) * transformX) + moveX, ((1481.0254) * transformY) + moveY),
        new createjs.Point(((69.0564) * transformX) + moveX, ((1494.1642) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1504.0000) * transformY) + moveY),

        new createjs.Point(((72.0000) * transformX) + moveX, ((1505.9998) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1508.0002) * transformY) + moveY),
        new createjs.Point(((72.0000) * transformX) + moveX, ((1510.0000) * transformY) + moveY),

        new createjs.Point(((73.9318) * transformX) + moveX, ((1516.3826) * transformY) + moveY),
        new createjs.Point(((75.0470) * transformX) + moveX, ((1524.7730) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1531.0000) * transformY) + moveY),

        new createjs.Point(((77.0000) * transformX) + moveX, ((1532.6665) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1534.3335) * transformY) + moveY),
        new createjs.Point(((77.0000) * transformX) + moveX, ((1536.0000) * transformY) + moveY),

        new createjs.Point(((77.3333) * transformX) + moveX, ((1536.0000) * transformY) + moveY),
        new createjs.Point(((77.6667) * transformX) + moveX, ((1536.0000) * transformY) + moveY),
        new createjs.Point(((78.0000) * transformX) + moveX, ((1536.0000) * transformY) + moveY),

        new createjs.Point(((78.3333) * transformX) + moveX, ((1538.9997) * transformY) + moveY),
        new createjs.Point(((78.6667) * transformX) + moveX, ((1542.0003) * transformY) + moveY),
        new createjs.Point(((79.0000) * transformX) + moveX, ((1545.0000) * transformY) + moveY),

        new createjs.Point(((79.3333) * transformX) + moveX, ((1545.0000) * transformY) + moveY),
        new createjs.Point(((79.6667) * transformX) + moveX, ((1545.0000) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1545.0000) * transformY) + moveY),

        new createjs.Point(((80.0000) * transformX) + moveX, ((1546.3332) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1547.6668) * transformY) + moveY),
        new createjs.Point(((80.0000) * transformX) + moveX, ((1549.0000) * transformY) + moveY),

        new createjs.Point(((80.3333) * transformX) + moveX, ((1549.0000) * transformY) + moveY),
        new createjs.Point(((80.6667) * transformX) + moveX, ((1549.0000) * transformY) + moveY),
        new createjs.Point(((81.0000) * transformX) + moveX, ((1549.0000) * transformY) + moveY),

        new createjs.Point(((81.3333) * transformX) + moveX, ((1551.9997) * transformY) + moveY),
        new createjs.Point(((81.6667) * transformX) + moveX, ((1555.0003) * transformY) + moveY),
        new createjs.Point(((82.0000) * transformX) + moveX, ((1558.0000) * transformY) + moveY),

        new createjs.Point(((88.2367) * transformX) + moveX, ((1576.2436) * transformY) + moveY),
        new createjs.Point(((92.0830) * transformX) + moveX, ((1596.6557) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1614.0000) * transformY) + moveY),

        new createjs.Point(((99.0000) * transformX) + moveX, ((1614.9999) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1616.0001) * transformY) + moveY),
        new createjs.Point(((99.0000) * transformX) + moveX, ((1617.0000) * transformY) + moveY),

        new createjs.Point(((99.3333) * transformX) + moveX, ((1617.0000) * transformY) + moveY),
        new createjs.Point(((99.6667) * transformX) + moveX, ((1617.0000) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((1617.0000) * transformY) + moveY),

        new createjs.Point(((100.0000) * transformX) + moveX, ((1617.9999) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((1619.0001) * transformY) + moveY),
        new createjs.Point(((100.0000) * transformX) + moveX, ((1620.0000) * transformY) + moveY),

        new createjs.Point(((100.3333) * transformX) + moveX, ((1620.0000) * transformY) + moveY),
        new createjs.Point(((100.6667) * transformX) + moveX, ((1620.0000) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1620.0000) * transformY) + moveY),

        new createjs.Point(((101.0000) * transformX) + moveX, ((1620.9999) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1622.0001) * transformY) + moveY),
        new createjs.Point(((101.0000) * transformX) + moveX, ((1623.0000) * transformY) + moveY),

        new createjs.Point(((101.3333) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((101.6667) * transformX) + moveX, ((1623.0000) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1623.0000) * transformY) + moveY),

        new createjs.Point(((102.0000) * transformX) + moveX, ((1623.9999) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1625.0001) * transformY) + moveY),
        new createjs.Point(((102.0000) * transformX) + moveX, ((1626.0000) * transformY) + moveY),

        new createjs.Point(((102.3333) * transformX) + moveX, ((1626.0000) * transformY) + moveY),
        new createjs.Point(((102.6667) * transformX) + moveX, ((1626.0000) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((1626.0000) * transformY) + moveY),

        new createjs.Point(((103.0000) * transformX) + moveX, ((1626.9999) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((1628.0001) * transformY) + moveY),
        new createjs.Point(((103.0000) * transformX) + moveX, ((1629.0000) * transformY) + moveY),

        new createjs.Point(((114.3540) * transformX) + moveX, ((1654.9780) * transformY) + moveY),
        new createjs.Point(((123.5737) * transformX) + moveX, ((1683.1157) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((1706.0000) * transformY) + moveY),

        new createjs.Point(((138.0000) * transformX) + moveX, ((1706.6666) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((1707.3334) * transformY) + moveY),
        new createjs.Point(((138.0000) * transformX) + moveX, ((1708.0000) * transformY) + moveY),

        new createjs.Point(((138.3333) * transformX) + moveX, ((1708.0000) * transformY) + moveY),
        new createjs.Point(((138.6667) * transformX) + moveX, ((1708.0000) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((1708.0000) * transformY) + moveY),

        new createjs.Point(((139.0000) * transformX) + moveX, ((1708.6666) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((1709.3334) * transformY) + moveY),
        new createjs.Point(((139.0000) * transformX) + moveX, ((1710.0000) * transformY) + moveY),

        new createjs.Point(((139.3333) * transformX) + moveX, ((1710.0000) * transformY) + moveY),
        new createjs.Point(((139.6667) * transformX) + moveX, ((1710.0000) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1710.0000) * transformY) + moveY),

        new createjs.Point(((140.0000) * transformX) + moveX, ((1710.6666) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1711.3334) * transformY) + moveY),
        new createjs.Point(((140.0000) * transformX) + moveX, ((1712.0000) * transformY) + moveY),

        new createjs.Point(((140.3333) * transformX) + moveX, ((1712.0000) * transformY) + moveY),
        new createjs.Point(((140.6667) * transformX) + moveX, ((1712.0000) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((1712.0000) * transformY) + moveY),

        new createjs.Point(((141.0000) * transformX) + moveX, ((1712.6666) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((1713.3334) * transformY) + moveY),
        new createjs.Point(((141.0000) * transformX) + moveX, ((1714.0000) * transformY) + moveY),

        new createjs.Point(((141.3333) * transformX) + moveX, ((1714.0000) * transformY) + moveY),
        new createjs.Point(((141.6667) * transformX) + moveX, ((1714.0000) * transformY) + moveY),
        new createjs.Point(((142.0000) * transformX) + moveX, ((1714.0000) * transformY) + moveY),

        new createjs.Point(((142.3333) * transformX) + moveX, ((1715.3332) * transformY) + moveY),
        new createjs.Point(((142.6667) * transformX) + moveX, ((1716.6668) * transformY) + moveY),
        new createjs.Point(((143.0000) * transformX) + moveX, ((1718.0000) * transformY) + moveY),

        new createjs.Point(((143.6666) * transformX) + moveX, ((1718.3333) * transformY) + moveY),
        new createjs.Point(((144.3334) * transformX) + moveX, ((1718.6667) * transformY) + moveY),
        new createjs.Point(((145.0000) * transformX) + moveX, ((1719.0000) * transformY) + moveY),

        new createjs.Point(((145.6666) * transformX) + moveX, ((1720.9998) * transformY) + moveY),
        new createjs.Point(((146.3334) * transformX) + moveX, ((1723.0002) * transformY) + moveY),
        new createjs.Point(((147.0000) * transformX) + moveX, ((1725.0000) * transformY) + moveY),

        new createjs.Point(((147.6666) * transformX) + moveX, ((1725.3333) * transformY) + moveY),
        new createjs.Point(((148.3334) * transformX) + moveX, ((1725.6667) * transformY) + moveY),
        new createjs.Point(((149.0000) * transformX) + moveX, ((1726.0000) * transformY) + moveY),

        new createjs.Point(((149.3333) * transformX) + moveX, ((1727.3332) * transformY) + moveY),
        new createjs.Point(((149.6667) * transformX) + moveX, ((1728.6668) * transformY) + moveY),
        new createjs.Point(((150.0000) * transformX) + moveX, ((1730.0000) * transformY) + moveY),

        new createjs.Point(((150.6666) * transformX) + moveX, ((1730.3333) * transformY) + moveY),
        new createjs.Point(((151.3334) * transformX) + moveX, ((1730.6667) * transformY) + moveY),
        new createjs.Point(((152.0000) * transformX) + moveX, ((1731.0000) * transformY) + moveY),

        new createjs.Point(((152.3333) * transformX) + moveX, ((1732.3332) * transformY) + moveY),
        new createjs.Point(((152.6667) * transformX) + moveX, ((1733.6668) * transformY) + moveY),
        new createjs.Point(((153.0000) * transformX) + moveX, ((1735.0000) * transformY) + moveY),

        new createjs.Point(((153.6666) * transformX) + moveX, ((1735.3333) * transformY) + moveY),
        new createjs.Point(((154.3334) * transformX) + moveX, ((1735.6667) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((1736.0000) * transformY) + moveY),

        new createjs.Point(((155.0000) * transformX) + moveX, ((1736.6666) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((1737.3334) * transformY) + moveY),
        new createjs.Point(((155.0000) * transformX) + moveX, ((1738.0000) * transformY) + moveY),

        new createjs.Point(((155.6666) * transformX) + moveX, ((1738.3333) * transformY) + moveY),
        new createjs.Point(((156.3334) * transformX) + moveX, ((1738.6667) * transformY) + moveY),
        new createjs.Point(((157.0000) * transformX) + moveX, ((1739.0000) * transformY) + moveY),

        new createjs.Point(((157.0000) * transformX) + moveX, ((1739.6666) * transformY) + moveY),
        new createjs.Point(((157.0000) * transformX) + moveX, ((1740.3334) * transformY) + moveY),
        new createjs.Point(((157.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),

        new createjs.Point(((157.3333) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((157.6667) * transformX) + moveX, ((1741.0000) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1741.0000) * transformY) + moveY),

        new createjs.Point(((158.0000) * transformX) + moveX, ((1741.6666) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1742.3334) * transformY) + moveY),
        new createjs.Point(((158.0000) * transformX) + moveX, ((1743.0000) * transformY) + moveY),

        new createjs.Point(((158.6666) * transformX) + moveX, ((1743.3333) * transformY) + moveY),
        new createjs.Point(((159.3334) * transformX) + moveX, ((1743.6667) * transformY) + moveY),
        new createjs.Point(((160.0000) * transformX) + moveX, ((1744.0000) * transformY) + moveY),

        new createjs.Point(((160.0000) * transformX) + moveX, ((1744.6666) * transformY) + moveY),
        new createjs.Point(((160.0000) * transformX) + moveX, ((1745.3334) * transformY) + moveY),
        new createjs.Point(((160.0000) * transformX) + moveX, ((1746.0000) * transformY) + moveY),

        new createjs.Point(((160.6666) * transformX) + moveX, ((1746.3333) * transformY) + moveY),
        new createjs.Point(((161.3334) * transformX) + moveX, ((1746.6667) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((1747.0000) * transformY) + moveY),

        new createjs.Point(((162.0000) * transformX) + moveX, ((1747.6666) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((1748.3334) * transformY) + moveY),
        new createjs.Point(((162.0000) * transformX) + moveX, ((1749.0000) * transformY) + moveY),

        new createjs.Point(((162.3333) * transformX) + moveX, ((1749.0000) * transformY) + moveY),
        new createjs.Point(((162.6667) * transformX) + moveX, ((1749.0000) * transformY) + moveY),
        new createjs.Point(((163.0000) * transformX) + moveX, ((1749.0000) * transformY) + moveY),

        new createjs.Point(((163.3333) * transformX) + moveX, ((1749.9999) * transformY) + moveY),
        new createjs.Point(((163.6667) * transformX) + moveX, ((1751.0001) * transformY) + moveY),
        new createjs.Point(((164.0000) * transformX) + moveX, ((1752.0000) * transformY) + moveY),

        new createjs.Point(((164.6666) * transformX) + moveX, ((1752.3333) * transformY) + moveY),
        new createjs.Point(((165.3334) * transformX) + moveX, ((1752.6667) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((1753.0000) * transformY) + moveY),

        new createjs.Point(((166.0000) * transformX) + moveX, ((1753.6666) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((1754.3334) * transformY) + moveY),
        new createjs.Point(((166.0000) * transformX) + moveX, ((1755.0000) * transformY) + moveY),

        new createjs.Point(((166.6666) * transformX) + moveX, ((1755.3333) * transformY) + moveY),
        new createjs.Point(((167.3334) * transformX) + moveX, ((1755.6667) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1756.0000) * transformY) + moveY),

        new createjs.Point(((168.0000) * transformX) + moveX, ((1756.6666) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1757.3334) * transformY) + moveY),
        new createjs.Point(((168.0000) * transformX) + moveX, ((1758.0000) * transformY) + moveY),

        new createjs.Point(((168.9999) * transformX) + moveX, ((1758.6666) * transformY) + moveY),
        new createjs.Point(((170.0001) * transformX) + moveX, ((1759.3334) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((1760.0000) * transformY) + moveY),

        new createjs.Point(((171.0000) * transformX) + moveX, ((1760.6666) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((1761.3334) * transformY) + moveY),
        new createjs.Point(((171.0000) * transformX) + moveX, ((1762.0000) * transformY) + moveY),

        new createjs.Point(((171.6666) * transformX) + moveX, ((1762.3333) * transformY) + moveY),
        new createjs.Point(((172.3334) * transformX) + moveX, ((1762.6667) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((1763.0000) * transformY) + moveY),

        new createjs.Point(((173.0000) * transformX) + moveX, ((1763.6666) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((1764.3334) * transformY) + moveY),
        new createjs.Point(((173.0000) * transformX) + moveX, ((1765.0000) * transformY) + moveY),

        new createjs.Point(((173.9999) * transformX) + moveX, ((1765.6666) * transformY) + moveY),
        new createjs.Point(((175.0001) * transformX) + moveX, ((1766.3334) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((1767.0000) * transformY) + moveY),

        new createjs.Point(((176.0000) * transformX) + moveX, ((1767.6666) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((1768.3334) * transformY) + moveY),
        new createjs.Point(((176.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),

        new createjs.Point(((176.9999) * transformX) + moveX, ((1769.6666) * transformY) + moveY),
        new createjs.Point(((178.0001) * transformX) + moveX, ((1770.3334) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((1771.0000) * transformY) + moveY),

        new createjs.Point(((179.0000) * transformX) + moveX, ((1771.6666) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((1772.3334) * transformY) + moveY),
        new createjs.Point(((179.0000) * transformX) + moveX, ((1773.0000) * transformY) + moveY),

        new createjs.Point(((180.3332) * transformX) + moveX, ((1773.9999) * transformY) + moveY),
        new createjs.Point(((181.6668) * transformX) + moveX, ((1775.0001) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((1776.0000) * transformY) + moveY),

        new createjs.Point(((183.0000) * transformX) + moveX, ((1776.6666) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((1777.3334) * transformY) + moveY),
        new createjs.Point(((183.0000) * transformX) + moveX, ((1778.0000) * transformY) + moveY),

        new createjs.Point(((184.3332) * transformX) + moveX, ((1778.9999) * transformY) + moveY),
        new createjs.Point(((185.6668) * transformX) + moveX, ((1780.0001) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((1781.0000) * transformY) + moveY),

        new createjs.Point(((187.0000) * transformX) + moveX, ((1781.6666) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((1782.3334) * transformY) + moveY),
        new createjs.Point(((187.0000) * transformX) + moveX, ((1783.0000) * transformY) + moveY),

        new createjs.Point(((188.6665) * transformX) + moveX, ((1784.3332) * transformY) + moveY),
        new createjs.Point(((190.3335) * transformX) + moveX, ((1785.6668) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((1787.0000) * transformY) + moveY),

        new createjs.Point(((192.0000) * transformX) + moveX, ((1787.6666) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((1788.3334) * transformY) + moveY),
        new createjs.Point(((192.0000) * transformX) + moveX, ((1789.0000) * transformY) + moveY),

        new createjs.Point(((193.9998) * transformX) + moveX, ((1790.6665) * transformY) + moveY),
        new createjs.Point(((196.0002) * transformX) + moveX, ((1792.3335) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((1794.0000) * transformY) + moveY),

        new createjs.Point(((198.0000) * transformX) + moveX, ((1794.6666) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((1795.3334) * transformY) + moveY),
        new createjs.Point(((198.0000) * transformX) + moveX, ((1796.0000) * transformY) + moveY),

        new createjs.Point(((203.3328) * transformX) + moveX, ((1800.9995) * transformY) + moveY),
        new createjs.Point(((208.6672) * transformX) + moveX, ((1806.0005) * transformY) + moveY),
        new createjs.Point(((214.0000) * transformX) + moveX, ((1811.0000) * transformY) + moveY),

        new createjs.Point(((217.6663) * transformX) + moveX, ((1814.9996) * transformY) + moveY),
        new createjs.Point(((221.3337) * transformX) + moveX, ((1819.0004) * transformY) + moveY),
        new createjs.Point(((225.0000) * transformX) + moveX, ((1823.0000) * transformY) + moveY),

        new createjs.Point(((225.6666) * transformX) + moveX, ((1823.0000) * transformY) + moveY),
        new createjs.Point(((226.3334) * transformX) + moveX, ((1823.0000) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((1823.0000) * transformY) + moveY),

        new createjs.Point(((228.9998) * transformX) + moveX, ((1825.3331) * transformY) + moveY),
        new createjs.Point(((231.0002) * transformX) + moveX, ((1827.6669) * transformY) + moveY),
        new createjs.Point(((233.0000) * transformX) + moveX, ((1830.0000) * transformY) + moveY),

        new createjs.Point(((233.6666) * transformX) + moveX, ((1830.0000) * transformY) + moveY),
        new createjs.Point(((234.3334) * transformX) + moveX, ((1830.0000) * transformY) + moveY),
        new createjs.Point(((235.0000) * transformX) + moveX, ((1830.0000) * transformY) + moveY),

        new createjs.Point(((235.9999) * transformX) + moveX, ((1831.3332) * transformY) + moveY),
        new createjs.Point(((237.0001) * transformX) + moveX, ((1832.6668) * transformY) + moveY),
        new createjs.Point(((238.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),

        new createjs.Point(((238.6666) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((239.3334) * transformX) + moveX, ((1834.0000) * transformY) + moveY),
        new createjs.Point(((240.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),

        new createjs.Point(((240.9999) * transformX) + moveX, ((1835.3332) * transformY) + moveY),
        new createjs.Point(((242.0001) * transformX) + moveX, ((1836.6668) * transformY) + moveY),
        new createjs.Point(((243.0000) * transformX) + moveX, ((1838.0000) * transformY) + moveY),

        new createjs.Point(((243.9999) * transformX) + moveX, ((1838.3333) * transformY) + moveY),
        new createjs.Point(((245.0001) * transformX) + moveX, ((1838.6667) * transformY) + moveY),
        new createjs.Point(((246.0000) * transformX) + moveX, ((1839.0000) * transformY) + moveY),

        new createjs.Point(((246.3333) * transformX) + moveX, ((1839.6666) * transformY) + moveY),
        new createjs.Point(((246.6667) * transformX) + moveX, ((1840.3334) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),

        new createjs.Point(((247.6666) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((248.3334) * transformX) + moveX, ((1841.0000) * transformY) + moveY),
        new createjs.Point(((249.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),

        new createjs.Point(((249.6666) * transformX) + moveX, ((1841.9999) * transformY) + moveY),
        new createjs.Point(((250.3334) * transformX) + moveX, ((1843.0001) * transformY) + moveY),
        new createjs.Point(((251.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),

        new createjs.Point(((251.6666) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((252.3334) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((253.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),

        new createjs.Point(((253.3333) * transformX) + moveX, ((1844.6666) * transformY) + moveY),
        new createjs.Point(((253.6667) * transformX) + moveX, ((1845.3334) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((254.6666) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((255.3334) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((256.3333) * transformX) + moveX, ((1846.6666) * transformY) + moveY),
        new createjs.Point(((256.6667) * transformX) + moveX, ((1847.3334) * transformY) + moveY),
        new createjs.Point(((257.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((257.6666) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((258.3334) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((259.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((259.3333) * transformX) + moveX, ((1848.6666) * transformY) + moveY),
        new createjs.Point(((259.6667) * transformX) + moveX, ((1849.3334) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((260.6666) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((261.3334) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((262.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((262.3333) * transformX) + moveX, ((1850.6666) * transformY) + moveY),
        new createjs.Point(((262.6667) * transformX) + moveX, ((1851.3334) * transformY) + moveY),
        new createjs.Point(((263.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),

        new createjs.Point(((263.6666) * transformX) + moveX, ((1852.0000) * transformY) + moveY),
        new createjs.Point(((264.3334) * transformX) + moveX, ((1852.0000) * transformY) + moveY),
        new createjs.Point(((265.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),

        new createjs.Point(((265.3333) * transformX) + moveX, ((1852.6666) * transformY) + moveY),
        new createjs.Point(((265.6667) * transformX) + moveX, ((1853.3334) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((266.6666) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((267.3334) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((268.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((268.3333) * transformX) + moveX, ((1854.6666) * transformY) + moveY),
        new createjs.Point(((268.6667) * transformX) + moveX, ((1855.3334) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),

        new createjs.Point(((270.3332) * transformX) + moveX, ((1856.3333) * transformY) + moveY),
        new createjs.Point(((271.6668) * transformX) + moveX, ((1856.6667) * transformY) + moveY),
        new createjs.Point(((273.0000) * transformX) + moveX, ((1857.0000) * transformY) + moveY),

        new createjs.Point(((273.3333) * transformX) + moveX, ((1857.6666) * transformY) + moveY),
        new createjs.Point(((273.6667) * transformX) + moveX, ((1858.3334) * transformY) + moveY),
        new createjs.Point(((274.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),

        new createjs.Point(((275.3332) * transformX) + moveX, ((1859.3333) * transformY) + moveY),
        new createjs.Point(((276.6668) * transformX) + moveX, ((1859.6667) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),

        new createjs.Point(((278.3333) * transformX) + moveX, ((1860.6666) * transformY) + moveY),
        new createjs.Point(((278.6667) * transformX) + moveX, ((1861.3334) * transformY) + moveY),
        new createjs.Point(((279.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),

        new createjs.Point(((279.6666) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((280.3334) * transformX) + moveX, ((1862.0000) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),

        new createjs.Point(((281.0000) * transformX) + moveX, ((1862.3333) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((1862.6667) * transformY) + moveY),
        new createjs.Point(((281.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),

        new createjs.Point(((281.6666) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((282.3334) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),

        new createjs.Point(((283.0000) * transformX) + moveX, ((1863.3333) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((1863.6667) * transformY) + moveY),
        new createjs.Point(((283.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),

        new createjs.Point(((283.6666) * transformX) + moveX, ((1864.0000) * transformY) + moveY),
        new createjs.Point(((284.3334) * transformX) + moveX, ((1864.0000) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),

        new createjs.Point(((285.0000) * transformX) + moveX, ((1864.3333) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((1864.6667) * transformY) + moveY),
        new createjs.Point(((285.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),

        new createjs.Point(((285.6666) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((286.3334) * transformX) + moveX, ((1865.0000) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),

        new createjs.Point(((287.0000) * transformX) + moveX, ((1865.3333) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((1865.6667) * transformY) + moveY),
        new createjs.Point(((287.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),

        new createjs.Point(((287.6666) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((288.3334) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),

        new createjs.Point(((289.0000) * transformX) + moveX, ((1866.3333) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((1866.6667) * transformY) + moveY),
        new createjs.Point(((289.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),

        new createjs.Point(((289.6666) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((290.3334) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),

        new createjs.Point(((291.0000) * transformX) + moveX, ((1867.3333) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1867.6667) * transformY) + moveY),
        new createjs.Point(((291.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((291.6666) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((292.3334) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((293.0000) * transformX) + moveX, ((1868.3333) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((1868.6667) * transformY) + moveY),
        new createjs.Point(((293.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),

        new createjs.Point(((293.6666) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((294.3334) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),

        new createjs.Point(((295.0000) * transformX) + moveX, ((1869.3333) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((1869.6667) * transformY) + moveY),
        new createjs.Point(((295.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),

        new createjs.Point(((295.6666) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((296.3334) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),

        new createjs.Point(((297.0000) * transformX) + moveX, ((1870.3333) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1870.6667) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),

        new createjs.Point(((297.6666) * transformX) + moveX, ((1871.0000) * transformY) + moveY),
        new createjs.Point(((298.3334) * transformX) + moveX, ((1871.0000) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),

        new createjs.Point(((299.0000) * transformX) + moveX, ((1871.3333) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((1871.6667) * transformY) + moveY),
        new createjs.Point(((299.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),

        new createjs.Point(((300.9998) * transformX) + moveX, ((1872.6666) * transformY) + moveY),
        new createjs.Point(((303.0002) * transformX) + moveX, ((1873.3334) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1874.0000) * transformY) + moveY),

        new createjs.Point(((305.0000) * transformX) + moveX, ((1874.3333) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1874.6667) * transformY) + moveY),
        new createjs.Point(((305.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),

        new createjs.Point(((305.9999) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((307.0001) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),

        new createjs.Point(((308.0000) * transformX) + moveX, ((1875.3333) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((1875.6667) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),

        new createjs.Point(((308.6666) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((309.3334) * transformX) + moveX, ((1876.0000) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),

        new createjs.Point(((310.0000) * transformX) + moveX, ((1876.3333) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((1876.6667) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),

        new createjs.Point(((310.9999) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((312.0001) * transformX) + moveX, ((1877.0000) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((1877.0000) * transformY) + moveY),

        new createjs.Point(((313.0000) * transformX) + moveX, ((1877.3333) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((1877.6667) * transformY) + moveY),
        new createjs.Point(((313.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),

        new createjs.Point(((313.6666) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((314.3334) * transformX) + moveX, ((1878.0000) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),

        new createjs.Point(((315.0000) * transformX) + moveX, ((1878.3333) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((1878.6667) * transformY) + moveY),
        new createjs.Point(((315.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),

        new createjs.Point(((315.9999) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((317.0001) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),

        new createjs.Point(((318.0000) * transformX) + moveX, ((1879.3333) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((1879.6667) * transformY) + moveY),
        new createjs.Point(((318.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((318.6666) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((319.3334) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((320.0000) * transformX) + moveX, ((1880.3333) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1880.6667) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((1881.0000) * transformY) + moveY),

        new createjs.Point(((321.9998) * transformX) + moveX, ((1881.3333) * transformY) + moveY),
        new createjs.Point(((324.0002) * transformX) + moveX, ((1881.6667) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((1882.0000) * transformY) + moveY),

        new createjs.Point(((326.0000) * transformX) + moveX, ((1882.3333) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((1882.6667) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),

        new createjs.Point(((326.9999) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((328.0001) * transformX) + moveX, ((1883.0000) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),

        new createjs.Point(((329.0000) * transformX) + moveX, ((1883.3333) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1883.6667) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((1884.0000) * transformY) + moveY),

        new createjs.Point(((329.9999) * transformX) + moveX, ((1884.0000) * transformY) + moveY),
        new createjs.Point(((331.0001) * transformX) + moveX, ((1884.0000) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1884.0000) * transformY) + moveY),

        new createjs.Point(((332.0000) * transformX) + moveX, ((1884.3333) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1884.6667) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((1885.0000) * transformY) + moveY),

        new createjs.Point(((332.9999) * transformX) + moveX, ((1885.0000) * transformY) + moveY),
        new createjs.Point(((334.0001) * transformX) + moveX, ((1885.0000) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((1885.0000) * transformY) + moveY),

        new createjs.Point(((335.0000) * transformX) + moveX, ((1885.3333) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((1885.6667) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((1886.0000) * transformY) + moveY),

        new createjs.Point(((335.9999) * transformX) + moveX, ((1886.0000) * transformY) + moveY),
        new createjs.Point(((337.0001) * transformX) + moveX, ((1886.0000) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((1886.0000) * transformY) + moveY),

        new createjs.Point(((338.0000) * transformX) + moveX, ((1886.3333) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((1886.6667) * transformY) + moveY),
        new createjs.Point(((338.0000) * transformX) + moveX, ((1887.0000) * transformY) + moveY),

        new createjs.Point(((338.9999) * transformX) + moveX, ((1887.0000) * transformY) + moveY),
        new createjs.Point(((340.0001) * transformX) + moveX, ((1887.0000) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((1887.0000) * transformY) + moveY),

        new createjs.Point(((341.0000) * transformX) + moveX, ((1887.3333) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((1887.6667) * transformY) + moveY),
        new createjs.Point(((341.0000) * transformX) + moveX, ((1888.0000) * transformY) + moveY),

        new createjs.Point(((355.6652) * transformX) + moveX, ((1890.9997) * transformY) + moveY),
        new createjs.Point(((370.3348) * transformX) + moveX, ((1894.0003) * transformY) + moveY),
        new createjs.Point(((385.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),

        new createjs.Point(((392.3326) * transformX) + moveX, ((1897.3333) * transformY) + moveY),
        new createjs.Point(((399.6674) * transformX) + moveX, ((1897.6667) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((1898.0000) * transformY) + moveY),

        new createjs.Point(((407.0000) * transformX) + moveX, ((1898.3333) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((1898.6667) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),

        new createjs.Point(((411.3329) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((415.6671) * transformX) + moveX, ((1899.0000) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((1899.0000) * transformY) + moveY),

        new createjs.Point(((420.0000) * transformX) + moveX, ((1899.3333) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((1899.6667) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((1900.0000) * transformY) + moveY),

        new createjs.Point(((425.9033) * transformX) + moveX, ((1901.6702) * transformY) + moveY),
        new createjs.Point(((441.9532) * transformX) + moveX, ((1899.1465) * transformY) + moveY),
        new createjs.Point(((446.0000) * transformX) + moveX, ((1898.0000) * transformY) + moveY),

        new createjs.Point(((451.9994) * transformX) + moveX, ((1898.0000) * transformY) + moveY),
        new createjs.Point(((458.0006) * transformX) + moveX, ((1898.0000) * transformY) + moveY),
        new createjs.Point(((464.0000) * transformX) + moveX, ((1898.0000) * transformY) + moveY),

        new createjs.Point(((464.0000) * transformX) + moveX, ((1897.6667) * transformY) + moveY),
        new createjs.Point(((464.0000) * transformX) + moveX, ((1897.3333) * transformY) + moveY),
        new createjs.Point(((464.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),

        new createjs.Point(((467.9996) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((472.0004) * transformX) + moveX, ((1897.0000) * transformY) + moveY),
        new createjs.Point(((476.0000) * transformX) + moveX, ((1897.0000) * transformY) + moveY),

        new createjs.Point(((485.0388) * transformX) + moveX, ((1894.3946) * transformY) + moveY),
        new createjs.Point(((497.1932) * transformX) + moveX, ((1894.6337) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((1892.0000) * transformY) + moveY),

        new createjs.Point(((507.9998) * transformX) + moveX, ((1892.0000) * transformY) + moveY),
        new createjs.Point(((510.0002) * transformX) + moveX, ((1892.0000) * transformY) + moveY),
        new createjs.Point(((512.0000) * transformX) + moveX, ((1892.0000) * transformY) + moveY),

        new createjs.Point(((520.6914) * transformX) + moveX, ((1889.3360) * transformY) + moveY),
        new createjs.Point(((531.5960) * transformX) + moveX, ((1887.8697) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((1885.0000) * transformY) + moveY),

        new createjs.Point(((541.3332) * transformX) + moveX, ((1885.0000) * transformY) + moveY),
        new createjs.Point(((542.6668) * transformX) + moveX, ((1885.0000) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1885.0000) * transformY) + moveY),

        new createjs.Point(((544.0000) * transformX) + moveX, ((1884.6667) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1884.3333) * transformY) + moveY),
        new createjs.Point(((544.0000) * transformX) + moveX, ((1884.0000) * transformY) + moveY),

        new createjs.Point(((546.3331) * transformX) + moveX, ((1883.6667) * transformY) + moveY),
        new createjs.Point(((548.6669) * transformX) + moveX, ((1883.3333) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1883.0000) * transformY) + moveY),

        new createjs.Point(((551.0000) * transformX) + moveX, ((1882.6667) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1882.3333) * transformY) + moveY),
        new createjs.Point(((551.0000) * transformX) + moveX, ((1882.0000) * transformY) + moveY),

        new createjs.Point(((553.3331) * transformX) + moveX, ((1881.6667) * transformY) + moveY),
        new createjs.Point(((555.6669) * transformX) + moveX, ((1881.3333) * transformY) + moveY),
        new createjs.Point(((558.0000) * transformX) + moveX, ((1881.0000) * transformY) + moveY),

        new createjs.Point(((558.0000) * transformX) + moveX, ((1880.6667) * transformY) + moveY),
        new createjs.Point(((558.0000) * transformX) + moveX, ((1880.3333) * transformY) + moveY),
        new createjs.Point(((558.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((558.9999) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((560.0001) * transformX) + moveX, ((1880.0000) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1880.0000) * transformY) + moveY),

        new createjs.Point(((561.0000) * transformX) + moveX, ((1879.6667) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1879.3333) * transformY) + moveY),
        new createjs.Point(((561.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),

        new createjs.Point(((561.9999) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((563.0001) * transformX) + moveX, ((1879.0000) * transformY) + moveY),
        new createjs.Point(((564.0000) * transformX) + moveX, ((1879.0000) * transformY) + moveY),

        new createjs.Point(((564.0000) * transformX) + moveX, ((1878.6667) * transformY) + moveY),
        new createjs.Point(((564.0000) * transformX) + moveX, ((1878.3333) * transformY) + moveY),
        new createjs.Point(((564.0000) * transformX) + moveX, ((1878.0000) * transformY) + moveY),

        new createjs.Point(((567.3330) * transformX) + moveX, ((1877.3334) * transformY) + moveY),
        new createjs.Point(((570.6670) * transformX) + moveX, ((1876.6666) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((1876.0000) * transformY) + moveY),

        new createjs.Point(((574.0000) * transformX) + moveX, ((1875.6667) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((1875.3333) * transformY) + moveY),
        new createjs.Point(((574.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),

        new createjs.Point(((574.6666) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((575.3334) * transformX) + moveX, ((1875.0000) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((1875.0000) * transformY) + moveY),

        new createjs.Point(((576.0000) * transformX) + moveX, ((1874.6667) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((1874.3333) * transformY) + moveY),
        new createjs.Point(((576.0000) * transformX) + moveX, ((1874.0000) * transformY) + moveY),

        new createjs.Point(((578.9997) * transformX) + moveX, ((1873.3334) * transformY) + moveY),
        new createjs.Point(((582.0003) * transformX) + moveX, ((1872.6666) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1872.0000) * transformY) + moveY),

        new createjs.Point(((585.0000) * transformX) + moveX, ((1871.6667) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1871.3333) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),

        new createjs.Point(((585.6666) * transformX) + moveX, ((1871.0000) * transformY) + moveY),
        new createjs.Point(((586.3334) * transformX) + moveX, ((1871.0000) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1871.0000) * transformY) + moveY),

        new createjs.Point(((587.0000) * transformX) + moveX, ((1870.6667) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1870.3333) * transformY) + moveY),
        new createjs.Point(((587.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),

        new createjs.Point(((587.9999) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((589.0001) * transformX) + moveX, ((1870.0000) * transformY) + moveY),
        new createjs.Point(((590.0000) * transformX) + moveX, ((1870.0000) * transformY) + moveY),

        new createjs.Point(((590.0000) * transformX) + moveX, ((1869.6667) * transformY) + moveY),
        new createjs.Point(((590.0000) * transformX) + moveX, ((1869.3333) * transformY) + moveY),
        new createjs.Point(((590.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),

        new createjs.Point(((590.6666) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((591.3334) * transformX) + moveX, ((1869.0000) * transformY) + moveY),
        new createjs.Point(((592.0000) * transformX) + moveX, ((1869.0000) * transformY) + moveY),

        new createjs.Point(((592.0000) * transformX) + moveX, ((1868.6667) * transformY) + moveY),
        new createjs.Point(((592.0000) * transformX) + moveX, ((1868.3333) * transformY) + moveY),
        new createjs.Point(((592.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((592.9999) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((594.0001) * transformX) + moveX, ((1868.0000) * transformY) + moveY),
        new createjs.Point(((595.0000) * transformX) + moveX, ((1868.0000) * transformY) + moveY),

        new createjs.Point(((595.0000) * transformX) + moveX, ((1867.6667) * transformY) + moveY),
        new createjs.Point(((595.0000) * transformX) + moveX, ((1867.3333) * transformY) + moveY),
        new createjs.Point(((595.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),

        new createjs.Point(((595.6666) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((596.3334) * transformX) + moveX, ((1867.0000) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1867.0000) * transformY) + moveY),

        new createjs.Point(((597.0000) * transformX) + moveX, ((1866.6667) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1866.3333) * transformY) + moveY),
        new createjs.Point(((597.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),

        new createjs.Point(((597.9999) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((599.0001) * transformX) + moveX, ((1866.0000) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1866.0000) * transformY) + moveY),

        new createjs.Point(((600.0000) * transformX) + moveX, ((1865.6667) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1865.3333) * transformY) + moveY),
        new createjs.Point(((600.0000) * transformX) + moveX, ((1865.0000) * transformY) + moveY),

        new createjs.Point(((601.3332) * transformX) + moveX, ((1864.6667) * transformY) + moveY),
        new createjs.Point(((602.6668) * transformX) + moveX, ((1864.3333) * transformY) + moveY),
        new createjs.Point(((604.0000) * transformX) + moveX, ((1864.0000) * transformY) + moveY),

        new createjs.Point(((604.0000) * transformX) + moveX, ((1863.6667) * transformY) + moveY),
        new createjs.Point(((604.0000) * transformX) + moveX, ((1863.3333) * transformY) + moveY),
        new createjs.Point(((604.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),

        new createjs.Point(((604.9999) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((606.0001) * transformX) + moveX, ((1863.0000) * transformY) + moveY),
        new createjs.Point(((607.0000) * transformX) + moveX, ((1863.0000) * transformY) + moveY),

        new createjs.Point(((607.0000) * transformX) + moveX, ((1862.6667) * transformY) + moveY),
        new createjs.Point(((607.0000) * transformX) + moveX, ((1862.3333) * transformY) + moveY),
        new createjs.Point(((607.0000) * transformX) + moveX, ((1862.0000) * transformY) + moveY),

        new createjs.Point(((608.3332) * transformX) + moveX, ((1861.6667) * transformY) + moveY),
        new createjs.Point(((609.6668) * transformX) + moveX, ((1861.3333) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1861.0000) * transformY) + moveY),

        new createjs.Point(((611.0000) * transformX) + moveX, ((1860.6667) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1860.3333) * transformY) + moveY),
        new createjs.Point(((611.0000) * transformX) + moveX, ((1860.0000) * transformY) + moveY),

        new createjs.Point(((612.6665) * transformX) + moveX, ((1859.6667) * transformY) + moveY),
        new createjs.Point(((614.3335) * transformX) + moveX, ((1859.3333) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((1859.0000) * transformY) + moveY),

        new createjs.Point(((616.0000) * transformX) + moveX, ((1858.6667) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((1858.3333) * transformY) + moveY),
        new createjs.Point(((616.0000) * transformX) + moveX, ((1858.0000) * transformY) + moveY),

        new createjs.Point(((617.9998) * transformX) + moveX, ((1857.3334) * transformY) + moveY),
        new createjs.Point(((620.0002) * transformX) + moveX, ((1856.6666) * transformY) + moveY),
        new createjs.Point(((622.0000) * transformX) + moveX, ((1856.0000) * transformY) + moveY),

        new createjs.Point(((622.0000) * transformX) + moveX, ((1855.6667) * transformY) + moveY),
        new createjs.Point(((622.0000) * transformX) + moveX, ((1855.3333) * transformY) + moveY),
        new createjs.Point(((622.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),

        new createjs.Point(((622.6666) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((623.3334) * transformX) + moveX, ((1855.0000) * transformY) + moveY),
        new createjs.Point(((624.0000) * transformX) + moveX, ((1855.0000) * transformY) + moveY),

        new createjs.Point(((624.0000) * transformX) + moveX, ((1854.6667) * transformY) + moveY),
        new createjs.Point(((624.0000) * transformX) + moveX, ((1854.3333) * transformY) + moveY),
        new createjs.Point(((624.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((624.6666) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((625.3334) * transformX) + moveX, ((1854.0000) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((1854.0000) * transformY) + moveY),

        new createjs.Point(((626.0000) * transformX) + moveX, ((1853.6667) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((1853.3333) * transformY) + moveY),
        new createjs.Point(((626.0000) * transformX) + moveX, ((1853.0000) * transformY) + moveY),

        new createjs.Point(((626.6666) * transformX) + moveX, ((1853.0000) * transformY) + moveY),
        new createjs.Point(((627.3334) * transformX) + moveX, ((1853.0000) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((1853.0000) * transformY) + moveY),

        new createjs.Point(((628.0000) * transformX) + moveX, ((1852.6667) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((1852.3333) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),

        new createjs.Point(((628.6666) * transformX) + moveX, ((1852.0000) * transformY) + moveY),
        new createjs.Point(((629.3334) * transformX) + moveX, ((1852.0000) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((1852.0000) * transformY) + moveY),

        new createjs.Point(((630.0000) * transformX) + moveX, ((1851.6667) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((1851.3333) * transformY) + moveY),
        new createjs.Point(((630.0000) * transformX) + moveX, ((1851.0000) * transformY) + moveY),

        new createjs.Point(((630.6666) * transformX) + moveX, ((1851.0000) * transformY) + moveY),
        new createjs.Point(((631.3334) * transformX) + moveX, ((1851.0000) * transformY) + moveY),
        new createjs.Point(((632.0000) * transformX) + moveX, ((1851.0000) * transformY) + moveY),

        new createjs.Point(((632.0000) * transformX) + moveX, ((1850.6667) * transformY) + moveY),
        new createjs.Point(((632.0000) * transformX) + moveX, ((1850.3333) * transformY) + moveY),
        new createjs.Point(((632.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((632.6666) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((633.3334) * transformX) + moveX, ((1850.0000) * transformY) + moveY),
        new createjs.Point(((634.0000) * transformX) + moveX, ((1850.0000) * transformY) + moveY),

        new createjs.Point(((634.3333) * transformX) + moveX, ((1849.3334) * transformY) + moveY),
        new createjs.Point(((634.6667) * transformX) + moveX, ((1848.6666) * transformY) + moveY),
        new createjs.Point(((635.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((635.6666) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((636.3334) * transformX) + moveX, ((1848.0000) * transformY) + moveY),
        new createjs.Point(((637.0000) * transformX) + moveX, ((1848.0000) * transformY) + moveY),

        new createjs.Point(((637.0000) * transformX) + moveX, ((1847.6667) * transformY) + moveY),
        new createjs.Point(((637.0000) * transformX) + moveX, ((1847.3333) * transformY) + moveY),
        new createjs.Point(((637.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),

        new createjs.Point(((637.6666) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((638.3334) * transformX) + moveX, ((1847.0000) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((1847.0000) * transformY) + moveY),

        new createjs.Point(((639.0000) * transformX) + moveX, ((1846.6667) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((1846.3333) * transformY) + moveY),
        new createjs.Point(((639.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((639.6666) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((640.3334) * transformX) + moveX, ((1846.0000) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((1846.0000) * transformY) + moveY),

        new createjs.Point(((641.0000) * transformX) + moveX, ((1845.6667) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((1845.3333) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),

        new createjs.Point(((641.6666) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((642.3334) * transformX) + moveX, ((1845.0000) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((1845.0000) * transformY) + moveY),

        new createjs.Point(((643.0000) * transformX) + moveX, ((1844.6667) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((1844.3333) * transformY) + moveY),
        new createjs.Point(((643.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),

        new createjs.Point(((643.6666) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((644.3334) * transformX) + moveX, ((1844.0000) * transformY) + moveY),
        new createjs.Point(((645.0000) * transformX) + moveX, ((1844.0000) * transformY) + moveY),

        new createjs.Point(((645.3333) * transformX) + moveX, ((1843.3334) * transformY) + moveY),
        new createjs.Point(((645.6667) * transformX) + moveX, ((1842.6666) * transformY) + moveY),
        new createjs.Point(((646.0000) * transformX) + moveX, ((1842.0000) * transformY) + moveY),

        new createjs.Point(((647.3332) * transformX) + moveX, ((1841.6667) * transformY) + moveY),
        new createjs.Point(((648.6668) * transformX) + moveX, ((1841.3333) * transformY) + moveY),
        new createjs.Point(((650.0000) * transformX) + moveX, ((1841.0000) * transformY) + moveY),

        new createjs.Point(((650.3333) * transformX) + moveX, ((1840.3334) * transformY) + moveY),
        new createjs.Point(((650.6667) * transformX) + moveX, ((1839.6666) * transformY) + moveY),
        new createjs.Point(((651.0000) * transformX) + moveX, ((1839.0000) * transformY) + moveY),

        new createjs.Point(((652.3332) * transformX) + moveX, ((1838.6667) * transformY) + moveY),
        new createjs.Point(((653.6668) * transformX) + moveX, ((1838.3333) * transformY) + moveY),
        new createjs.Point(((655.0000) * transformX) + moveX, ((1838.0000) * transformY) + moveY),

        new createjs.Point(((655.3333) * transformX) + moveX, ((1837.3334) * transformY) + moveY),
        new createjs.Point(((655.6667) * transformX) + moveX, ((1836.6666) * transformY) + moveY),
        new createjs.Point(((656.0000) * transformX) + moveX, ((1836.0000) * transformY) + moveY),

        new createjs.Point(((657.3332) * transformX) + moveX, ((1835.6667) * transformY) + moveY),
        new createjs.Point(((658.6668) * transformX) + moveX, ((1835.3333) * transformY) + moveY),
        new createjs.Point(((660.0000) * transformX) + moveX, ((1835.0000) * transformY) + moveY),

        new createjs.Point(((660.0000) * transformX) + moveX, ((1834.6667) * transformY) + moveY),
        new createjs.Point(((660.0000) * transformX) + moveX, ((1834.3333) * transformY) + moveY),
        new createjs.Point(((660.0000) * transformX) + moveX, ((1834.0000) * transformY) + moveY),

        new createjs.Point(((660.9999) * transformX) + moveX, ((1833.6667) * transformY) + moveY),
        new createjs.Point(((662.0001) * transformX) + moveX, ((1833.3333) * transformY) + moveY),
        new createjs.Point(((663.0000) * transformX) + moveX, ((1833.0000) * transformY) + moveY),

        new createjs.Point(((663.3333) * transformX) + moveX, ((1832.3334) * transformY) + moveY),
        new createjs.Point(((663.6667) * transformX) + moveX, ((1831.6666) * transformY) + moveY),
        new createjs.Point(((664.0000) * transformX) + moveX, ((1831.0000) * transformY) + moveY),

        new createjs.Point(((665.3332) * transformX) + moveX, ((1830.6667) * transformY) + moveY),
        new createjs.Point(((666.6668) * transformX) + moveX, ((1830.3333) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((1830.0000) * transformY) + moveY),

        new createjs.Point(((668.0000) * transformX) + moveX, ((1829.6667) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((1829.3333) * transformY) + moveY),
        new createjs.Point(((668.0000) * transformX) + moveX, ((1829.0000) * transformY) + moveY),

        new createjs.Point(((668.9999) * transformX) + moveX, ((1828.6667) * transformY) + moveY),
        new createjs.Point(((670.0001) * transformX) + moveX, ((1828.3333) * transformY) + moveY),
        new createjs.Point(((671.0000) * transformX) + moveX, ((1828.0000) * transformY) + moveY),

        new createjs.Point(((671.3333) * transformX) + moveX, ((1827.3334) * transformY) + moveY),
        new createjs.Point(((671.6667) * transformX) + moveX, ((1826.6666) * transformY) + moveY),
        new createjs.Point(((672.0000) * transformX) + moveX, ((1826.0000) * transformY) + moveY),

        new createjs.Point(((672.6666) * transformX) + moveX, ((1826.0000) * transformY) + moveY),
        new createjs.Point(((673.3334) * transformX) + moveX, ((1826.0000) * transformY) + moveY),
        new createjs.Point(((674.0000) * transformX) + moveX, ((1826.0000) * transformY) + moveY),

        new createjs.Point(((674.6666) * transformX) + moveX, ((1825.0001) * transformY) + moveY),
        new createjs.Point(((675.3334) * transformX) + moveX, ((1823.9999) * transformY) + moveY),
        new createjs.Point(((676.0000) * transformX) + moveX, ((1823.0000) * transformY) + moveY),

        new createjs.Point(((676.6666) * transformX) + moveX, ((1823.0000) * transformY) + moveY),
        new createjs.Point(((677.3334) * transformX) + moveX, ((1823.0000) * transformY) + moveY),
        new createjs.Point(((678.0000) * transformX) + moveX, ((1823.0000) * transformY) + moveY),

        new createjs.Point(((678.3333) * transformX) + moveX, ((1822.3334) * transformY) + moveY),
        new createjs.Point(((678.6667) * transformX) + moveX, ((1821.6666) * transformY) + moveY),
        new createjs.Point(((679.0000) * transformX) + moveX, ((1821.0000) * transformY) + moveY),

        new createjs.Point(((679.6666) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((680.3334) * transformX) + moveX, ((1821.0000) * transformY) + moveY),
        new createjs.Point(((681.0000) * transformX) + moveX, ((1821.0000) * transformY) + moveY),

        new createjs.Point(((681.3333) * transformX) + moveX, ((1820.3334) * transformY) + moveY),
        new createjs.Point(((681.6667) * transformX) + moveX, ((1819.6666) * transformY) + moveY),
        new createjs.Point(((682.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),

        new createjs.Point(((682.6666) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((683.3334) * transformX) + moveX, ((1819.0000) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((1819.0000) * transformY) + moveY),

        new createjs.Point(((684.6666) * transformX) + moveX, ((1818.0001) * transformY) + moveY),
        new createjs.Point(((685.3334) * transformX) + moveX, ((1816.9999) * transformY) + moveY),
        new createjs.Point(((686.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),

        new createjs.Point(((686.6666) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((687.3334) * transformX) + moveX, ((1816.0000) * transformY) + moveY),
        new createjs.Point(((688.0000) * transformX) + moveX, ((1816.0000) * transformY) + moveY),

        new createjs.Point(((688.6666) * transformX) + moveX, ((1815.0001) * transformY) + moveY),
        new createjs.Point(((689.3334) * transformX) + moveX, ((1813.9999) * transformY) + moveY),
        new createjs.Point(((690.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),

        new createjs.Point(((690.6666) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((691.3334) * transformX) + moveX, ((1813.0000) * transformY) + moveY),
        new createjs.Point(((692.0000) * transformX) + moveX, ((1813.0000) * transformY) + moveY),

        new createjs.Point(((692.6666) * transformX) + moveX, ((1812.0001) * transformY) + moveY),
        new createjs.Point(((693.3334) * transformX) + moveX, ((1810.9999) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((694.6666) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((695.3334) * transformX) + moveX, ((1810.0000) * transformY) + moveY),
        new createjs.Point(((696.0000) * transformX) + moveX, ((1810.0000) * transformY) + moveY),

        new createjs.Point(((696.6666) * transformX) + moveX, ((1809.0001) * transformY) + moveY),
        new createjs.Point(((697.3334) * transformX) + moveX, ((1807.9999) * transformY) + moveY),
        new createjs.Point(((698.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((698.6666) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((699.3334) * transformX) + moveX, ((1807.0000) * transformY) + moveY),
        new createjs.Point(((700.0000) * transformX) + moveX, ((1807.0000) * transformY) + moveY),

        new createjs.Point(((700.9999) * transformX) + moveX, ((1805.6668) * transformY) + moveY),
        new createjs.Point(((702.0001) * transformX) + moveX, ((1804.3332) * transformY) + moveY),
        new createjs.Point(((703.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),

        new createjs.Point(((703.6666) * transformX) + moveX, ((1803.0000) * transformY) + moveY),
        new createjs.Point(((704.3334) * transformX) + moveX, ((1803.0000) * transformY) + moveY),
        new createjs.Point(((705.0000) * transformX) + moveX, ((1803.0000) * transformY) + moveY),

        new createjs.Point(((706.3332) * transformX) + moveX, ((1801.3335) * transformY) + moveY),
        new createjs.Point(((707.6668) * transformX) + moveX, ((1799.6665) * transformY) + moveY),
        new createjs.Point(((709.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),

        new createjs.Point(((709.6666) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((710.3334) * transformX) + moveX, ((1798.0000) * transformY) + moveY),
        new createjs.Point(((711.0000) * transformX) + moveX, ((1798.0000) * transformY) + moveY),

        new createjs.Point(((712.3332) * transformX) + moveX, ((1796.3335) * transformY) + moveY),
        new createjs.Point(((713.6668) * transformX) + moveX, ((1794.6665) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((1793.0000) * transformY) + moveY),

        new createjs.Point(((715.6666) * transformX) + moveX, ((1793.0000) * transformY) + moveY),
        new createjs.Point(((716.3334) * transformX) + moveX, ((1793.0000) * transformY) + moveY),
        new createjs.Point(((717.0000) * transformX) + moveX, ((1793.0000) * transformY) + moveY),

        new createjs.Point(((719.3331) * transformX) + moveX, ((1790.3336) * transformY) + moveY),
        new createjs.Point(((721.6669) * transformX) + moveX, ((1787.6664) * transformY) + moveY),
        new createjs.Point(((724.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((724.6666) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((725.3334) * transformX) + moveX, ((1785.0000) * transformY) + moveY),
        new createjs.Point(((726.0000) * transformX) + moveX, ((1785.0000) * transformY) + moveY),

        new createjs.Point(((730.9995) * transformX) + moveX, ((1779.6672) * transformY) + moveY),
        new createjs.Point(((736.0005) * transformX) + moveX, ((1774.3328) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((1769.0000) * transformY) + moveY),

        new createjs.Point(((749.7793) * transformX) + moveX, ((1760.2167) * transformY) + moveY),
        new createjs.Point(((758.6000) * transformX) + moveX, ((1752.2392) * transformY) + moveY),
        new createjs.Point(((766.0000) * transformX) + moveX, ((1742.0000) * transformY) + moveY),

        new createjs.Point(((767.9998) * transformX) + moveX, ((1740.3335) * transformY) + moveY),
        new createjs.Point(((770.0002) * transformX) + moveX, ((1738.6665) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((1737.0000) * transformY) + moveY),

        new createjs.Point(((772.3333) * transformX) + moveX, ((1736.0001) * transformY) + moveY),
        new createjs.Point(((772.6667) * transformX) + moveX, ((1734.9999) * transformY) + moveY),
        new createjs.Point(((773.0000) * transformX) + moveX, ((1734.0000) * transformY) + moveY),

        new createjs.Point(((773.9999) * transformX) + moveX, ((1733.3334) * transformY) + moveY),
        new createjs.Point(((775.0001) * transformX) + moveX, ((1732.6666) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((1732.0000) * transformY) + moveY),

        new createjs.Point(((776.0000) * transformX) + moveX, ((1731.3334) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((1730.6666) * transformY) + moveY),
        new createjs.Point(((776.0000) * transformX) + moveX, ((1730.0000) * transformY) + moveY),

        new createjs.Point(((777.6665) * transformX) + moveX, ((1728.6668) * transformY) + moveY),
        new createjs.Point(((779.3335) * transformX) + moveX, ((1727.3332) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1726.0000) * transformY) + moveY),

        new createjs.Point(((781.0000) * transformX) + moveX, ((1725.3334) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1724.6666) * transformY) + moveY),
        new createjs.Point(((781.0000) * transformX) + moveX, ((1724.0000) * transformY) + moveY),

        new createjs.Point(((781.6666) * transformX) + moveX, ((1723.6667) * transformY) + moveY),
        new createjs.Point(((782.3334) * transformX) + moveX, ((1723.3333) * transformY) + moveY),
        new createjs.Point(((783.0000) * transformX) + moveX, ((1723.0000) * transformY) + moveY),

        new createjs.Point(((783.3333) * transformX) + moveX, ((1722.0001) * transformY) + moveY),
        new createjs.Point(((783.6667) * transformX) + moveX, ((1720.9999) * transformY) + moveY),
        new createjs.Point(((784.0000) * transformX) + moveX, ((1720.0000) * transformY) + moveY),

        new createjs.Point(((785.3332) * transformX) + moveX, ((1719.0001) * transformY) + moveY),
        new createjs.Point(((786.6668) * transformX) + moveX, ((1717.9999) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1717.0000) * transformY) + moveY),

        new createjs.Point(((788.0000) * transformX) + moveX, ((1716.3334) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1715.6666) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((1715.0000) * transformY) + moveY),

        new createjs.Point(((788.9999) * transformX) + moveX, ((1714.3334) * transformY) + moveY),
        new createjs.Point(((790.0001) * transformX) + moveX, ((1713.6666) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1713.0000) * transformY) + moveY),

        new createjs.Point(((791.0000) * transformX) + moveX, ((1712.3334) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1711.6666) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((1711.0000) * transformY) + moveY),

        new createjs.Point(((791.9999) * transformX) + moveX, ((1710.3334) * transformY) + moveY),
        new createjs.Point(((793.0001) * transformX) + moveX, ((1709.6666) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1709.0000) * transformY) + moveY),

        new createjs.Point(((794.0000) * transformX) + moveX, ((1708.3334) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1707.6666) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((1707.0000) * transformY) + moveY),

        new createjs.Point(((794.9999) * transformX) + moveX, ((1706.3334) * transformY) + moveY),
        new createjs.Point(((796.0001) * transformX) + moveX, ((1705.6666) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1705.0000) * transformY) + moveY),

        new createjs.Point(((797.0000) * transformX) + moveX, ((1704.3334) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1703.6666) * transformY) + moveY),
        new createjs.Point(((797.0000) * transformX) + moveX, ((1703.0000) * transformY) + moveY),

        new createjs.Point(((797.6666) * transformX) + moveX, ((1702.6667) * transformY) + moveY),
        new createjs.Point(((798.3334) * transformX) + moveX, ((1702.3333) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1702.0000) * transformY) + moveY),

        new createjs.Point(((799.0000) * transformX) + moveX, ((1701.3334) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1700.6666) * transformY) + moveY),
        new createjs.Point(((799.0000) * transformX) + moveX, ((1700.0000) * transformY) + moveY),

        new createjs.Point(((799.9999) * transformX) + moveX, ((1699.3334) * transformY) + moveY),
        new createjs.Point(((801.0001) * transformX) + moveX, ((1698.6666) * transformY) + moveY),
        new createjs.Point(((802.0000) * transformX) + moveX, ((1698.0000) * transformY) + moveY),

        new createjs.Point(((802.0000) * transformX) + moveX, ((1697.3334) * transformY) + moveY),
        new createjs.Point(((802.0000) * transformX) + moveX, ((1696.6666) * transformY) + moveY),
        new createjs.Point(((802.0000) * transformX) + moveX, ((1696.0000) * transformY) + moveY),

        new createjs.Point(((802.6666) * transformX) + moveX, ((1695.6667) * transformY) + moveY),
        new createjs.Point(((803.3334) * transformX) + moveX, ((1695.3333) * transformY) + moveY),
        new createjs.Point(((804.0000) * transformX) + moveX, ((1695.0000) * transformY) + moveY),

        new createjs.Point(((804.3333) * transformX) + moveX, ((1694.0001) * transformY) + moveY),
        new createjs.Point(((804.6667) * transformX) + moveX, ((1692.9999) * transformY) + moveY),
        new createjs.Point(((805.0000) * transformX) + moveX, ((1692.0000) * transformY) + moveY),

        new createjs.Point(((805.3333) * transformX) + moveX, ((1692.0000) * transformY) + moveY),
        new createjs.Point(((805.6667) * transformX) + moveX, ((1692.0000) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1692.0000) * transformY) + moveY),

        new createjs.Point(((806.0000) * transformX) + moveX, ((1691.3334) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1690.6666) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((1690.0000) * transformY) + moveY),

        new createjs.Point(((806.6666) * transformX) + moveX, ((1689.6667) * transformY) + moveY),
        new createjs.Point(((807.3334) * transformX) + moveX, ((1689.3333) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1689.0000) * transformY) + moveY),

        new createjs.Point(((808.0000) * transformX) + moveX, ((1688.3334) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1687.6666) * transformY) + moveY),
        new createjs.Point(((808.0000) * transformX) + moveX, ((1687.0000) * transformY) + moveY),

        new createjs.Point(((808.9999) * transformX) + moveX, ((1686.3334) * transformY) + moveY),
        new createjs.Point(((810.0001) * transformX) + moveX, ((1685.6666) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1685.0000) * transformY) + moveY),

        new createjs.Point(((811.0000) * transformX) + moveX, ((1684.3334) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1683.6666) * transformY) + moveY),
        new createjs.Point(((811.0000) * transformX) + moveX, ((1683.0000) * transformY) + moveY),

        new createjs.Point(((811.6666) * transformX) + moveX, ((1682.6667) * transformY) + moveY),
        new createjs.Point(((812.3334) * transformX) + moveX, ((1682.3333) * transformY) + moveY),
        new createjs.Point(((813.0000) * transformX) + moveX, ((1682.0000) * transformY) + moveY),

        new createjs.Point(((813.3333) * transformX) + moveX, ((1680.6668) * transformY) + moveY),
        new createjs.Point(((813.6667) * transformX) + moveX, ((1679.3332) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((1678.0000) * transformY) + moveY),

        new createjs.Point(((814.6666) * transformX) + moveX, ((1677.6667) * transformY) + moveY),
        new createjs.Point(((815.3334) * transformX) + moveX, ((1677.3333) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((1677.0000) * transformY) + moveY),

        new createjs.Point(((816.0000) * transformX) + moveX, ((1676.3334) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((1675.6666) * transformY) + moveY),
        new createjs.Point(((816.0000) * transformX) + moveX, ((1675.0000) * transformY) + moveY),

        new createjs.Point(((816.6666) * transformX) + moveX, ((1674.6667) * transformY) + moveY),
        new createjs.Point(((817.3334) * transformX) + moveX, ((1674.3333) * transformY) + moveY),
        new createjs.Point(((818.0000) * transformX) + moveX, ((1674.0000) * transformY) + moveY),

        new createjs.Point(((818.3333) * transformX) + moveX, ((1673.0001) * transformY) + moveY),
        new createjs.Point(((818.6667) * transformX) + moveX, ((1671.9999) * transformY) + moveY),
        new createjs.Point(((819.0000) * transformX) + moveX, ((1671.0000) * transformY) + moveY),

        new createjs.Point(((819.3333) * transformX) + moveX, ((1671.0000) * transformY) + moveY),
        new createjs.Point(((819.6667) * transformX) + moveX, ((1671.0000) * transformY) + moveY),
        new createjs.Point(((820.0000) * transformX) + moveX, ((1671.0000) * transformY) + moveY),

        new createjs.Point(((820.3333) * transformX) + moveX, ((1669.6668) * transformY) + moveY),
        new createjs.Point(((820.6667) * transformX) + moveX, ((1668.3332) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((1667.0000) * transformY) + moveY),

        new createjs.Point(((821.3333) * transformX) + moveX, ((1667.0000) * transformY) + moveY),
        new createjs.Point(((821.6667) * transformX) + moveX, ((1667.0000) * transformY) + moveY),
        new createjs.Point(((822.0000) * transformX) + moveX, ((1667.0000) * transformY) + moveY),

        new createjs.Point(((822.3333) * transformX) + moveX, ((1666.0001) * transformY) + moveY),
        new createjs.Point(((822.6667) * transformX) + moveX, ((1664.9999) * transformY) + moveY),
        new createjs.Point(((823.0000) * transformX) + moveX, ((1664.0000) * transformY) + moveY),

        new createjs.Point(((823.6666) * transformX) + moveX, ((1663.6667) * transformY) + moveY),
        new createjs.Point(((824.3334) * transformX) + moveX, ((1663.3333) * transformY) + moveY),
        new createjs.Point(((825.0000) * transformX) + moveX, ((1663.0000) * transformY) + moveY),

        new createjs.Point(((825.0000) * transformX) + moveX, ((1662.3334) * transformY) + moveY),
        new createjs.Point(((825.0000) * transformX) + moveX, ((1661.6666) * transformY) + moveY),
        new createjs.Point(((825.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),

        new createjs.Point(((825.3333) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((825.6667) * transformX) + moveX, ((1661.0000) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1661.0000) * transformY) + moveY),

        new createjs.Point(((826.0000) * transformX) + moveX, ((1660.3334) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1659.6666) * transformY) + moveY),
        new createjs.Point(((826.0000) * transformX) + moveX, ((1659.0000) * transformY) + moveY),

        new createjs.Point(((826.6666) * transformX) + moveX, ((1658.6667) * transformY) + moveY),
        new createjs.Point(((827.3334) * transformX) + moveX, ((1658.3333) * transformY) + moveY),
        new createjs.Point(((828.0000) * transformX) + moveX, ((1658.0000) * transformY) + moveY),

        new createjs.Point(((828.3333) * transformX) + moveX, ((1656.6668) * transformY) + moveY),
        new createjs.Point(((828.6667) * transformX) + moveX, ((1655.3332) * transformY) + moveY),
        new createjs.Point(((829.0000) * transformX) + moveX, ((1654.0000) * transformY) + moveY),

        new createjs.Point(((829.6666) * transformX) + moveX, ((1653.6667) * transformY) + moveY),
        new createjs.Point(((830.3334) * transformX) + moveX, ((1653.3333) * transformY) + moveY),
        new createjs.Point(((831.0000) * transformX) + moveX, ((1653.0000) * transformY) + moveY),

        new createjs.Point(((831.6666) * transformX) + moveX, ((1651.0002) * transformY) + moveY),
        new createjs.Point(((832.3334) * transformX) + moveX, ((1648.9998) * transformY) + moveY),
        new createjs.Point(((833.0000) * transformX) + moveX, ((1647.0000) * transformY) + moveY),

        new createjs.Point(((833.6666) * transformX) + moveX, ((1646.6667) * transformY) + moveY),
        new createjs.Point(((834.3334) * transformX) + moveX, ((1646.3333) * transformY) + moveY),
        new createjs.Point(((835.0000) * transformX) + moveX, ((1646.0000) * transformY) + moveY),

        new createjs.Point(((835.6666) * transformX) + moveX, ((1644.0002) * transformY) + moveY),
        new createjs.Point(((836.3334) * transformX) + moveX, ((1641.9998) * transformY) + moveY),
        new createjs.Point(((837.0000) * transformX) + moveX, ((1640.0000) * transformY) + moveY),

        new createjs.Point(((837.6666) * transformX) + moveX, ((1639.6667) * transformY) + moveY),
        new createjs.Point(((838.3334) * transformX) + moveX, ((1639.3333) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1639.0000) * transformY) + moveY),

        new createjs.Point(((839.0000) * transformX) + moveX, ((1638.3334) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1637.6666) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((1637.0000) * transformY) + moveY),

        new createjs.Point(((839.3333) * transformX) + moveX, ((1637.0000) * transformY) + moveY),
        new createjs.Point(((839.6667) * transformX) + moveX, ((1637.0000) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1637.0000) * transformY) + moveY),

        new createjs.Point(((840.0000) * transformX) + moveX, ((1636.3334) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1635.6666) * transformY) + moveY),
        new createjs.Point(((840.0000) * transformX) + moveX, ((1635.0000) * transformY) + moveY),

        new createjs.Point(((840.3333) * transformX) + moveX, ((1635.0000) * transformY) + moveY),
        new createjs.Point(((840.6667) * transformX) + moveX, ((1635.0000) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1635.0000) * transformY) + moveY),

        new createjs.Point(((841.0000) * transformX) + moveX, ((1634.3334) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1633.6666) * transformY) + moveY),
        new createjs.Point(((841.0000) * transformX) + moveX, ((1633.0000) * transformY) + moveY),

        new createjs.Point(((841.3333) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((841.6667) * transformX) + moveX, ((1633.0000) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1633.0000) * transformY) + moveY),

        new createjs.Point(((842.0000) * transformX) + moveX, ((1632.3334) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1631.6666) * transformY) + moveY),
        new createjs.Point(((842.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),

        new createjs.Point(((842.3333) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((842.6667) * transformX) + moveX, ((1631.0000) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1631.0000) * transformY) + moveY),

        new createjs.Point(((843.0000) * transformX) + moveX, ((1630.3334) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1629.6666) * transformY) + moveY),
        new createjs.Point(((843.0000) * transformX) + moveX, ((1629.0000) * transformY) + moveY),

        new createjs.Point(((843.6666) * transformX) + moveX, ((1628.6667) * transformY) + moveY),
        new createjs.Point(((844.3334) * transformX) + moveX, ((1628.3333) * transformY) + moveY),
        new createjs.Point(((845.0000) * transformX) + moveX, ((1628.0000) * transformY) + moveY),

        new createjs.Point(((845.3333) * transformX) + moveX, ((1626.6668) * transformY) + moveY),
        new createjs.Point(((845.6667) * transformX) + moveX, ((1625.3332) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((1624.0000) * transformY) + moveY),

        new createjs.Point(((846.3333) * transformX) + moveX, ((1624.0000) * transformY) + moveY),
        new createjs.Point(((846.6667) * transformX) + moveX, ((1624.0000) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1624.0000) * transformY) + moveY),

        new createjs.Point(((847.0000) * transformX) + moveX, ((1623.3334) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1622.6666) * transformY) + moveY),
        new createjs.Point(((847.0000) * transformX) + moveX, ((1622.0000) * transformY) + moveY),

        new createjs.Point(((847.3333) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((847.6667) * transformX) + moveX, ((1622.0000) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((1622.0000) * transformY) + moveY),

        new createjs.Point(((848.0000) * transformX) + moveX, ((1621.3334) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((1620.6666) * transformY) + moveY),
        new createjs.Point(((848.0000) * transformX) + moveX, ((1620.0000) * transformY) + moveY),

        new createjs.Point(((848.3333) * transformX) + moveX, ((1620.0000) * transformY) + moveY),
        new createjs.Point(((848.6667) * transformX) + moveX, ((1620.0000) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1620.0000) * transformY) + moveY),

        new createjs.Point(((849.0000) * transformX) + moveX, ((1619.3334) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1618.6666) * transformY) + moveY),
        new createjs.Point(((849.0000) * transformX) + moveX, ((1618.0000) * transformY) + moveY),

        new createjs.Point(((849.3333) * transformX) + moveX, ((1618.0000) * transformY) + moveY),
        new createjs.Point(((849.6667) * transformX) + moveX, ((1618.0000) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1618.0000) * transformY) + moveY),

        new createjs.Point(((850.0000) * transformX) + moveX, ((1617.3334) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1616.6666) * transformY) + moveY),
        new createjs.Point(((850.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),

        new createjs.Point(((850.3333) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((850.6667) * transformX) + moveX, ((1616.0000) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((1616.0000) * transformY) + moveY),

        new createjs.Point(((851.3333) * transformX) + moveX, ((1614.6668) * transformY) + moveY),
        new createjs.Point(((851.6667) * transformX) + moveX, ((1613.3332) * transformY) + moveY),
        new createjs.Point(((852.0000) * transformX) + moveX, ((1612.0000) * transformY) + moveY),

        new createjs.Point(((852.3333) * transformX) + moveX, ((1612.0000) * transformY) + moveY),
        new createjs.Point(((852.6667) * transformX) + moveX, ((1612.0000) * transformY) + moveY),
        new createjs.Point(((853.0000) * transformX) + moveX, ((1612.0000) * transformY) + moveY),

        new createjs.Point(((853.3333) * transformX) + moveX, ((1610.6668) * transformY) + moveY),
        new createjs.Point(((853.6667) * transformX) + moveX, ((1609.3332) * transformY) + moveY),
        new createjs.Point(((854.0000) * transformX) + moveX, ((1608.0000) * transformY) + moveY),

        new createjs.Point(((854.3333) * transformX) + moveX, ((1608.0000) * transformY) + moveY),
        new createjs.Point(((854.6667) * transformX) + moveX, ((1608.0000) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1608.0000) * transformY) + moveY),

        new createjs.Point(((855.0000) * transformX) + moveX, ((1607.3334) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1606.6666) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((1606.0000) * transformY) + moveY),

        new createjs.Point(((855.3333) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((855.6667) * transformX) + moveX, ((1606.0000) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1606.0000) * transformY) + moveY),

        new createjs.Point(((856.0000) * transformX) + moveX, ((1605.3334) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1604.6666) * transformY) + moveY),
        new createjs.Point(((856.0000) * transformX) + moveX, ((1604.0000) * transformY) + moveY),

        new createjs.Point(((856.3333) * transformX) + moveX, ((1604.0000) * transformY) + moveY),
        new createjs.Point(((856.6667) * transformX) + moveX, ((1604.0000) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1604.0000) * transformY) + moveY),

        new createjs.Point(((857.0000) * transformX) + moveX, ((1603.3334) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1602.6666) * transformY) + moveY),
        new createjs.Point(((857.0000) * transformX) + moveX, ((1602.0000) * transformY) + moveY),

        new createjs.Point(((857.3333) * transformX) + moveX, ((1602.0000) * transformY) + moveY),
        new createjs.Point(((857.6667) * transformX) + moveX, ((1602.0000) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1602.0000) * transformY) + moveY),

        new createjs.Point(((858.0000) * transformX) + moveX, ((1601.3334) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1600.6666) * transformY) + moveY),
        new createjs.Point(((858.0000) * transformX) + moveX, ((1600.0000) * transformY) + moveY),

        new createjs.Point(((858.3333) * transformX) + moveX, ((1600.0000) * transformY) + moveY),
        new createjs.Point(((858.6667) * transformX) + moveX, ((1600.0000) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1600.0000) * transformY) + moveY),

        new createjs.Point(((859.0000) * transformX) + moveX, ((1599.3334) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1598.6666) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((1598.0000) * transformY) + moveY),

        new createjs.Point(((859.3333) * transformX) + moveX, ((1598.0000) * transformY) + moveY),
        new createjs.Point(((859.6667) * transformX) + moveX, ((1598.0000) * transformY) + moveY),
        new createjs.Point(((860.0000) * transformX) + moveX, ((1598.0000) * transformY) + moveY),

        new createjs.Point(((860.3333) * transformX) + moveX, ((1596.6668) * transformY) + moveY),
        new createjs.Point(((860.6667) * transformX) + moveX, ((1595.3332) * transformY) + moveY),
        new createjs.Point(((861.0000) * transformX) + moveX, ((1594.0000) * transformY) + moveY),

        new createjs.Point(((861.3333) * transformX) + moveX, ((1594.0000) * transformY) + moveY),
        new createjs.Point(((861.6667) * transformX) + moveX, ((1594.0000) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1594.0000) * transformY) + moveY),

        new createjs.Point(((862.0000) * transformX) + moveX, ((1593.3334) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1592.6666) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((1592.0000) * transformY) + moveY),

        new createjs.Point(((862.3333) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((862.6667) * transformX) + moveX, ((1592.0000) * transformY) + moveY),
        new createjs.Point(((863.0000) * transformX) + moveX, ((1592.0000) * transformY) + moveY),

        new createjs.Point(((863.3333) * transformX) + moveX, ((1590.3335) * transformY) + moveY),
        new createjs.Point(((863.6667) * transformX) + moveX, ((1588.6665) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((1587.0000) * transformY) + moveY),

        new createjs.Point(((864.3333) * transformX) + moveX, ((1587.0000) * transformY) + moveY),
        new createjs.Point(((864.6667) * transformX) + moveX, ((1587.0000) * transformY) + moveY),
        new createjs.Point(((865.0000) * transformX) + moveX, ((1587.0000) * transformY) + moveY),

        new createjs.Point(((865.6666) * transformX) + moveX, ((1585.0002) * transformY) + moveY),
        new createjs.Point(((866.3334) * transformX) + moveX, ((1582.9998) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((1581.0000) * transformY) + moveY),

        new createjs.Point(((867.3333) * transformX) + moveX, ((1581.0000) * transformY) + moveY),
        new createjs.Point(((867.6667) * transformX) + moveX, ((1581.0000) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1581.0000) * transformY) + moveY),

        new createjs.Point(((868.0000) * transformX) + moveX, ((1580.0001) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1578.9999) * transformY) + moveY),
        new createjs.Point(((868.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),

        new createjs.Point(((868.3333) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((868.6667) * transformX) + moveX, ((1578.0000) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1578.0000) * transformY) + moveY),

        new createjs.Point(((869.0000) * transformX) + moveX, ((1577.3334) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1576.6666) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((1576.0000) * transformY) + moveY),

        new createjs.Point(((869.3333) * transformX) + moveX, ((1576.0000) * transformY) + moveY),
        new createjs.Point(((869.6667) * transformX) + moveX, ((1576.0000) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((1576.0000) * transformY) + moveY),

        new createjs.Point(((870.3333) * transformX) + moveX, ((1574.6668) * transformY) + moveY),
        new createjs.Point(((870.6667) * transformX) + moveX, ((1573.3332) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((1572.0000) * transformY) + moveY),

        new createjs.Point(((871.3333) * transformX) + moveX, ((1572.0000) * transformY) + moveY),
        new createjs.Point(((871.6667) * transformX) + moveX, ((1572.0000) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1572.0000) * transformY) + moveY),

        new createjs.Point(((872.0000) * transformX) + moveX, ((1571.0001) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1569.9999) * transformY) + moveY),
        new createjs.Point(((872.0000) * transformX) + moveX, ((1569.0000) * transformY) + moveY),

        new createjs.Point(((872.3333) * transformX) + moveX, ((1569.0000) * transformY) + moveY),
        new createjs.Point(((872.6667) * transformX) + moveX, ((1569.0000) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((1569.0000) * transformY) + moveY),

        new createjs.Point(((873.3333) * transformX) + moveX, ((1567.6668) * transformY) + moveY),
        new createjs.Point(((873.6667) * transformX) + moveX, ((1566.3332) * transformY) + moveY),
        new createjs.Point(((874.0000) * transformX) + moveX, ((1565.0000) * transformY) + moveY),

        new createjs.Point(((874.3333) * transformX) + moveX, ((1565.0000) * transformY) + moveY),
        new createjs.Point(((874.6667) * transformX) + moveX, ((1565.0000) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((1565.0000) * transformY) + moveY),

        new createjs.Point(((876.9998) * transformX) + moveX, ((1559.0006) * transformY) + moveY),
        new createjs.Point(((879.0002) * transformX) + moveX, ((1552.9994) * transformY) + moveY),
        new createjs.Point(((881.0000) * transformX) + moveX, ((1547.0000) * transformY) + moveY),

        new createjs.Point(((881.3333) * transformX) + moveX, ((1547.0000) * transformY) + moveY),
        new createjs.Point(((881.6667) * transformX) + moveX, ((1547.0000) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1547.0000) * transformY) + moveY),

        new createjs.Point(((882.0000) * transformX) + moveX, ((1546.3334) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1545.6666) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((1545.0000) * transformY) + moveY),

        new createjs.Point(((882.3333) * transformX) + moveX, ((1545.0000) * transformY) + moveY),
        new createjs.Point(((882.6667) * transformX) + moveX, ((1545.0000) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1545.0000) * transformY) + moveY),

        new createjs.Point(((883.0000) * transformX) + moveX, ((1544.0001) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1542.9999) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((1542.0000) * transformY) + moveY),

        new createjs.Point(((883.3333) * transformX) + moveX, ((1542.0000) * transformY) + moveY),
        new createjs.Point(((883.6667) * transformX) + moveX, ((1542.0000) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1542.0000) * transformY) + moveY),

        new createjs.Point(((884.0000) * transformX) + moveX, ((1541.3334) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1540.6666) * transformY) + moveY),
        new createjs.Point(((884.0000) * transformX) + moveX, ((1540.0000) * transformY) + moveY),

        new createjs.Point(((884.3333) * transformX) + moveX, ((1540.0000) * transformY) + moveY),
        new createjs.Point(((884.6667) * transformX) + moveX, ((1540.0000) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((1540.0000) * transformY) + moveY),

        new createjs.Point(((885.3333) * transformX) + moveX, ((1538.0002) * transformY) + moveY),
        new createjs.Point(((885.6667) * transformX) + moveX, ((1535.9998) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((1534.0000) * transformY) + moveY),

        new createjs.Point(((886.3333) * transformX) + moveX, ((1534.0000) * transformY) + moveY),
        new createjs.Point(((886.6667) * transformX) + moveX, ((1534.0000) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1534.0000) * transformY) + moveY),

        new createjs.Point(((887.0000) * transformX) + moveX, ((1533.3334) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1532.6666) * transformY) + moveY),
        new createjs.Point(((887.0000) * transformX) + moveX, ((1532.0000) * transformY) + moveY),

        new createjs.Point(((887.3333) * transformX) + moveX, ((1532.0000) * transformY) + moveY),
        new createjs.Point(((887.6667) * transformX) + moveX, ((1532.0000) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((1532.0000) * transformY) + moveY),

        new createjs.Point(((888.3333) * transformX) + moveX, ((1530.0002) * transformY) + moveY),
        new createjs.Point(((888.6667) * transformX) + moveX, ((1527.9998) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((1526.0000) * transformY) + moveY),

        new createjs.Point(((889.3333) * transformX) + moveX, ((1526.0000) * transformY) + moveY),
        new createjs.Point(((889.6667) * transformX) + moveX, ((1526.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1526.0000) * transformY) + moveY),

        new createjs.Point(((890.0000) * transformX) + moveX, ((1525.3334) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1524.6666) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((1524.0000) * transformY) + moveY),

        new createjs.Point(((890.3333) * transformX) + moveX, ((1524.0000) * transformY) + moveY),
        new createjs.Point(((890.6667) * transformX) + moveX, ((1524.0000) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((1524.0000) * transformY) + moveY),

        new createjs.Point(((892.9998) * transformX) + moveX, ((1517.0007) * transformY) + moveY),
        new createjs.Point(((895.0002) * transformX) + moveX, ((1509.9993) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((1503.0000) * transformY) + moveY),

        new createjs.Point(((901.6990) * transformX) + moveX, ((1491.2125) * transformY) + moveY),
        new createjs.Point(((905.6243) * transformX) + moveX, ((1477.1768) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((1465.0000) * transformY) + moveY),

        new createjs.Point(((910.3333) * transformX) + moveX, ((1462.3336) * transformY) + moveY),
        new createjs.Point(((910.6667) * transformX) + moveX, ((1459.6664) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((1457.0000) * transformY) + moveY),

        new createjs.Point(((911.3333) * transformX) + moveX, ((1457.0000) * transformY) + moveY),
        new createjs.Point(((911.6667) * transformX) + moveX, ((1457.0000) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((1457.0000) * transformY) + moveY),

        new createjs.Point(((912.0000) * transformX) + moveX, ((1456.0001) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((1454.9999) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((1454.0000) * transformY) + moveY),

        new createjs.Point(((912.3333) * transformX) + moveX, ((1454.0000) * transformY) + moveY),
        new createjs.Point(((912.6667) * transformX) + moveX, ((1454.0000) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((1454.0000) * transformY) + moveY),

        new createjs.Point(((913.0000) * transformX) + moveX, ((1452.6668) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((1451.3332) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((1450.0000) * transformY) + moveY),

        new createjs.Point(((913.3333) * transformX) + moveX, ((1450.0000) * transformY) + moveY),
        new createjs.Point(((913.6667) * transformX) + moveX, ((1450.0000) * transformY) + moveY),
        new createjs.Point(((914.0000) * transformX) + moveX, ((1450.0000) * transformY) + moveY),

        new createjs.Point(((914.0000) * transformX) + moveX, ((1448.6668) * transformY) + moveY),
        new createjs.Point(((914.0000) * transformX) + moveX, ((1447.3332) * transformY) + moveY),
        new createjs.Point(((914.0000) * transformX) + moveX, ((1446.0000) * transformY) + moveY),

        new createjs.Point(((914.3333) * transformX) + moveX, ((1446.0000) * transformY) + moveY),
        new createjs.Point(((914.6667) * transformX) + moveX, ((1446.0000) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((1446.0000) * transformY) + moveY),

        new createjs.Point(((915.3333) * transformX) + moveX, ((1443.6669) * transformY) + moveY),
        new createjs.Point(((915.6667) * transformX) + moveX, ((1441.3331) * transformY) + moveY),
        new createjs.Point(((916.0000) * transformX) + moveX, ((1439.0000) * transformY) + moveY),

        new createjs.Point(((916.3333) * transformX) + moveX, ((1439.0000) * transformY) + moveY),
        new createjs.Point(((916.6667) * transformX) + moveX, ((1439.0000) * transformY) + moveY),
        new createjs.Point(((917.0000) * transformX) + moveX, ((1439.0000) * transformY) + moveY),

        new createjs.Point(((917.6666) * transformX) + moveX, ((1434.6671) * transformY) + moveY),
        new createjs.Point(((918.3334) * transformX) + moveX, ((1430.3329) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),

        new createjs.Point(((919.3333) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((919.6667) * transformX) + moveX, ((1426.0000) * transformY) + moveY),
        new createjs.Point(((920.0000) * transformX) + moveX, ((1426.0000) * transformY) + moveY),

        new createjs.Point(((920.0000) * transformX) + moveX, ((1424.6668) * transformY) + moveY),
        new createjs.Point(((920.0000) * transformX) + moveX, ((1423.3332) * transformY) + moveY),
        new createjs.Point(((920.0000) * transformX) + moveX, ((1422.0000) * transformY) + moveY),

        new createjs.Point(((920.3333) * transformX) + moveX, ((1422.0000) * transformY) + moveY),
        new createjs.Point(((920.6667) * transformX) + moveX, ((1422.0000) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((1422.0000) * transformY) + moveY),

        new createjs.Point(((921.0000) * transformX) + moveX, ((1420.6668) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((1419.3332) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((1418.0000) * transformY) + moveY),

        new createjs.Point(((921.3333) * transformX) + moveX, ((1418.0000) * transformY) + moveY),
        new createjs.Point(((921.6667) * transformX) + moveX, ((1418.0000) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((1418.0000) * transformY) + moveY),

        new createjs.Point(((922.0000) * transformX) + moveX, ((1416.6668) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((1415.3332) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((1414.0000) * transformY) + moveY),

        new createjs.Point(((922.3333) * transformX) + moveX, ((1414.0000) * transformY) + moveY),
        new createjs.Point(((922.6667) * transformX) + moveX, ((1414.0000) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((1414.0000) * transformY) + moveY),

        new createjs.Point(((923.3333) * transformX) + moveX, ((1411.0003) * transformY) + moveY),
        new createjs.Point(((923.6667) * transformX) + moveX, ((1407.9997) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((1405.0000) * transformY) + moveY),

        new createjs.Point(((924.3333) * transformX) + moveX, ((1405.0000) * transformY) + moveY),
        new createjs.Point(((924.6667) * transformX) + moveX, ((1405.0000) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((1405.0000) * transformY) + moveY),

        new createjs.Point(((925.3333) * transformX) + moveX, ((1401.6670) * transformY) + moveY),
        new createjs.Point(((925.6667) * transformX) + moveX, ((1398.3330) * transformY) + moveY),
        new createjs.Point(((926.0000) * transformX) + moveX, ((1395.0000) * transformY) + moveY),

        new createjs.Point(((928.9468) * transformX) + moveX, ((1385.5976) * transformY) + moveY),
        new createjs.Point(((930.1102) * transformX) + moveX, ((1373.5309) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((1364.0000) * transformY) + moveY),

        new createjs.Point(((933.0000) * transformX) + moveX, ((1362.0002) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((1359.9998) * transformY) + moveY),
        new createjs.Point(((933.0000) * transformX) + moveX, ((1358.0000) * transformY) + moveY),

        new createjs.Point(((936.7049) * transformX) + moveX, ((1345.6026) * transformY) + moveY),
        new createjs.Point(((936.3482) * transformX) + moveX, ((1329.6757) * transformY) + moveY),
        new createjs.Point(((940.0000) * transformX) + moveX, ((1317.0000) * transformY) + moveY),

        new createjs.Point(((940.0000) * transformX) + moveX, ((1314.3336) * transformY) + moveY),
        new createjs.Point(((940.0000) * transformX) + moveX, ((1311.6664) * transformY) + moveY),
        new createjs.Point(((940.0000) * transformX) + moveX, ((1309.0000) * transformY) + moveY),

        new createjs.Point(((940.3333) * transformX) + moveX, ((1309.0000) * transformY) + moveY),
        new createjs.Point(((940.6667) * transformX) + moveX, ((1309.0000) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((1309.0000) * transformY) + moveY),

        new createjs.Point(((941.0000) * transformX) + moveX, ((1306.0003) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((1302.9997) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((1300.0000) * transformY) + moveY),

        new createjs.Point(((941.3333) * transformX) + moveX, ((1300.0000) * transformY) + moveY),
        new createjs.Point(((941.6667) * transformX) + moveX, ((1300.0000) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((1300.0000) * transformY) + moveY),

        new createjs.Point(((942.3333) * transformX) + moveX, ((1293.3340) * transformY) + moveY),
        new createjs.Point(((942.6667) * transformX) + moveX, ((1286.6660) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((1280.0000) * transformY) + moveY),

        new createjs.Point(((943.3333) * transformX) + moveX, ((1280.0000) * transformY) + moveY),
        new createjs.Point(((943.6667) * transformX) + moveX, ((1280.0000) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((1280.0000) * transformY) + moveY),

        new createjs.Point(((944.0000) * transformX) + moveX, ((1276.0004) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((1271.9996) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((1268.0000) * transformY) + moveY),

        new createjs.Point(((944.3333) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((944.6667) * transformX) + moveX, ((1268.0000) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1268.0000) * transformY) + moveY),

        new createjs.Point(((945.0000) * transformX) + moveX, ((1267.0001) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1265.9999) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1265.0000) * transformY) + moveY),

        new createjs.Point(((945.0000) * transformX) + moveX, ((1261.3337) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1257.6663) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1254.0000) * transformY) + moveY),

        new createjs.Point(((946.3581) * transformX) + moveX, ((1249.1304) * transformY) + moveY),
        new createjs.Point(((944.4708) * transformX) + moveX, ((1240.5109) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((1235.0000) * transformY) + moveY),

        new createjs.Point(((948.9116) * transformX) + moveX, ((1224.5069) * transformY) + moveY),
        new createjs.Point(((946.9966) * transformX) + moveX, ((1205.5578) * transformY) + moveY),
        new createjs.Point(((947.0000) * transformX) + moveX, ((1193.0000) * transformY) + moveY),

        new createjs.Point(((947.0029) * transformX) + moveX, ((1182.3312) * transformY) + moveY),
        new createjs.Point(((948.5184) * transformX) + moveX, ((1167.8920) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((1159.0000) * transformY) + moveY),

        new createjs.Point(((946.0000) * transformX) + moveX, ((1154.6671) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((1150.3329) * transformY) + moveY),
        new createjs.Point(((946.0000) * transformX) + moveX, ((1146.0000) * transformY) + moveY),

        new createjs.Point(((945.6667) * transformX) + moveX, ((1146.0000) * transformY) + moveY),
        new createjs.Point(((945.3333) * transformX) + moveX, ((1146.0000) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1146.0000) * transformY) + moveY),

        new createjs.Point(((945.0000) * transformX) + moveX, ((1142.3337) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1138.6663) * transformY) + moveY),
        new createjs.Point(((945.0000) * transformX) + moveX, ((1135.0000) * transformY) + moveY),

        new createjs.Point(((944.6667) * transformX) + moveX, ((1135.0000) * transformY) + moveY),
        new createjs.Point(((944.3333) * transformX) + moveX, ((1135.0000) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((1135.0000) * transformY) + moveY),

        new createjs.Point(((944.0000) * transformX) + moveX, ((1132.0003) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((1128.9997) * transformY) + moveY),
        new createjs.Point(((944.0000) * transformX) + moveX, ((1126.0000) * transformY) + moveY),

        new createjs.Point(((943.6667) * transformX) + moveX, ((1126.0000) * transformY) + moveY),
        new createjs.Point(((943.3333) * transformX) + moveX, ((1126.0000) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((1126.0000) * transformY) + moveY),

        new createjs.Point(((943.0000) * transformX) + moveX, ((1123.6669) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((1121.3331) * transformY) + moveY),
        new createjs.Point(((943.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),

        new createjs.Point(((942.6667) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((942.3333) * transformX) + moveX, ((1119.0000) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((1119.0000) * transformY) + moveY),

        new createjs.Point(((942.0000) * transformX) + moveX, ((1116.6669) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((1114.3331) * transformY) + moveY),
        new createjs.Point(((942.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),

        new createjs.Point(((941.6667) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((941.3333) * transformX) + moveX, ((1112.0000) * transformY) + moveY),
        new createjs.Point(((941.0000) * transformX) + moveX, ((1112.0000) * transformY) + moveY),

        new createjs.Point(((940.6667) * transformX) + moveX, ((1107.6671) * transformY) + moveY),
        new createjs.Point(((940.3333) * transformX) + moveX, ((1103.3329) * transformY) + moveY),
        new createjs.Point(((940.0000) * transformX) + moveX, ((1099.0000) * transformY) + moveY),

        new createjs.Point(((937.7736) * transformX) + moveX, ((1091.7363) * transformY) + moveY),
        new createjs.Point(((936.2809) * transformX) + moveX, ((1082.0830) * transformY) + moveY),
        new createjs.Point(((934.0000) * transformX) + moveX, ((1075.0000) * transformY) + moveY),

        new createjs.Point(((933.3334) * transformX) + moveX, ((1070.6671) * transformY) + moveY),
        new createjs.Point(((932.6666) * transformX) + moveX, ((1066.3329) * transformY) + moveY),
        new createjs.Point(((932.0000) * transformX) + moveX, ((1062.0000) * transformY) + moveY),

        new createjs.Point(((931.6667) * transformX) + moveX, ((1062.0000) * transformY) + moveY),
        new createjs.Point(((931.3333) * transformX) + moveX, ((1062.0000) * transformY) + moveY),
        new createjs.Point(((931.0000) * transformX) + moveX, ((1062.0000) * transformY) + moveY),

        new createjs.Point(((930.3334) * transformX) + moveX, ((1058.3337) * transformY) + moveY),
        new createjs.Point(((929.6666) * transformX) + moveX, ((1054.6663) * transformY) + moveY),
        new createjs.Point(((929.0000) * transformX) + moveX, ((1051.0000) * transformY) + moveY),

        new createjs.Point(((928.6667) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((928.3333) * transformX) + moveX, ((1051.0000) * transformY) + moveY),
        new createjs.Point(((928.0000) * transformX) + moveX, ((1051.0000) * transformY) + moveY),

        new createjs.Point(((927.6667) * transformX) + moveX, ((1048.6669) * transformY) + moveY),
        new createjs.Point(((927.3333) * transformX) + moveX, ((1046.3331) * transformY) + moveY),
        new createjs.Point(((927.0000) * transformX) + moveX, ((1044.0000) * transformY) + moveY),

        new createjs.Point(((926.6667) * transformX) + moveX, ((1044.0000) * transformY) + moveY),
        new createjs.Point(((926.3333) * transformX) + moveX, ((1044.0000) * transformY) + moveY),
        new createjs.Point(((926.0000) * transformX) + moveX, ((1044.0000) * transformY) + moveY),

        new createjs.Point(((926.0000) * transformX) + moveX, ((1043.0001) * transformY) + moveY),
        new createjs.Point(((926.0000) * transformX) + moveX, ((1041.9999) * transformY) + moveY),
        new createjs.Point(((926.0000) * transformX) + moveX, ((1041.0000) * transformY) + moveY),

        new createjs.Point(((925.6667) * transformX) + moveX, ((1041.0000) * transformY) + moveY),
        new createjs.Point(((925.3333) * transformX) + moveX, ((1041.0000) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((1041.0000) * transformY) + moveY),

        new createjs.Point(((925.0000) * transformX) + moveX, ((1040.0001) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((1038.9999) * transformY) + moveY),
        new createjs.Point(((925.0000) * transformX) + moveX, ((1038.0000) * transformY) + moveY),

        new createjs.Point(((924.6667) * transformX) + moveX, ((1038.0000) * transformY) + moveY),
        new createjs.Point(((924.3333) * transformX) + moveX, ((1038.0000) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((1038.0000) * transformY) + moveY),

        new createjs.Point(((924.0000) * transformX) + moveX, ((1037.0001) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((1035.9999) * transformY) + moveY),
        new createjs.Point(((924.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),

        new createjs.Point(((923.6667) * transformX) + moveX, ((1035.0000) * transformY) + moveY),
        new createjs.Point(((923.3333) * transformX) + moveX, ((1035.0000) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((1035.0000) * transformY) + moveY),

        new createjs.Point(((923.0000) * transformX) + moveX, ((1034.0001) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((1032.9999) * transformY) + moveY),
        new createjs.Point(((923.0000) * transformX) + moveX, ((1032.0000) * transformY) + moveY),

        new createjs.Point(((922.6667) * transformX) + moveX, ((1032.0000) * transformY) + moveY),
        new createjs.Point(((922.3333) * transformX) + moveX, ((1032.0000) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((1032.0000) * transformY) + moveY),

        new createjs.Point(((922.0000) * transformX) + moveX, ((1031.0001) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((1029.9999) * transformY) + moveY),
        new createjs.Point(((922.0000) * transformX) + moveX, ((1029.0000) * transformY) + moveY),

        new createjs.Point(((921.6667) * transformX) + moveX, ((1029.0000) * transformY) + moveY),
        new createjs.Point(((921.3333) * transformX) + moveX, ((1029.0000) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((1029.0000) * transformY) + moveY),

        new createjs.Point(((921.0000) * transformX) + moveX, ((1028.0001) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((1026.9999) * transformY) + moveY),
        new createjs.Point(((921.0000) * transformX) + moveX, ((1026.0000) * transformY) + moveY),

        new createjs.Point(((920.6667) * transformX) + moveX, ((1026.0000) * transformY) + moveY),
        new createjs.Point(((920.3333) * transformX) + moveX, ((1026.0000) * transformY) + moveY),
        new createjs.Point(((920.0000) * transformX) + moveX, ((1026.0000) * transformY) + moveY),

        new createjs.Point(((920.0000) * transformX) + moveX, ((1025.0001) * transformY) + moveY),
        new createjs.Point(((920.0000) * transformX) + moveX, ((1023.9999) * transformY) + moveY),
        new createjs.Point(((920.0000) * transformX) + moveX, ((1023.0000) * transformY) + moveY),

        new createjs.Point(((919.6667) * transformX) + moveX, ((1023.0000) * transformY) + moveY),
        new createjs.Point(((919.3333) * transformX) + moveX, ((1023.0000) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((1023.0000) * transformY) + moveY),

        new createjs.Point(((919.0000) * transformX) + moveX, ((1022.0001) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((1020.9999) * transformY) + moveY),
        new createjs.Point(((919.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),

        new createjs.Point(((918.6667) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((918.3333) * transformX) + moveX, ((1020.0000) * transformY) + moveY),
        new createjs.Point(((918.0000) * transformX) + moveX, ((1020.0000) * transformY) + moveY),

        new createjs.Point(((918.0000) * transformX) + moveX, ((1019.0001) * transformY) + moveY),
        new createjs.Point(((918.0000) * transformX) + moveX, ((1017.9999) * transformY) + moveY),
        new createjs.Point(((918.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),

        new createjs.Point(((917.6667) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((917.3333) * transformX) + moveX, ((1017.0000) * transformY) + moveY),
        new createjs.Point(((917.0000) * transformX) + moveX, ((1017.0000) * transformY) + moveY),

        new createjs.Point(((917.0000) * transformX) + moveX, ((1016.3334) * transformY) + moveY),
        new createjs.Point(((917.0000) * transformX) + moveX, ((1015.6666) * transformY) + moveY),
        new createjs.Point(((917.0000) * transformX) + moveX, ((1015.0000) * transformY) + moveY),

        new createjs.Point(((916.6667) * transformX) + moveX, ((1015.0000) * transformY) + moveY),
        new createjs.Point(((916.3333) * transformX) + moveX, ((1015.0000) * transformY) + moveY),
        new createjs.Point(((916.0000) * transformX) + moveX, ((1015.0000) * transformY) + moveY),

        new createjs.Point(((915.6667) * transformX) + moveX, ((1013.0002) * transformY) + moveY),
        new createjs.Point(((915.3333) * transformX) + moveX, ((1010.9998) * transformY) + moveY),
        new createjs.Point(((915.0000) * transformX) + moveX, ((1009.0000) * transformY) + moveY),

        new createjs.Point(((914.6667) * transformX) + moveX, ((1009.0000) * transformY) + moveY),
        new createjs.Point(((914.3333) * transformX) + moveX, ((1009.0000) * transformY) + moveY),
        new createjs.Point(((914.0000) * transformX) + moveX, ((1009.0000) * transformY) + moveY),

        new createjs.Point(((913.6667) * transformX) + moveX, ((1007.3335) * transformY) + moveY),
        new createjs.Point(((913.3333) * transformX) + moveX, ((1005.6665) * transformY) + moveY),
        new createjs.Point(((913.0000) * transformX) + moveX, ((1004.0000) * transformY) + moveY),

        new createjs.Point(((912.6667) * transformX) + moveX, ((1004.0000) * transformY) + moveY),
        new createjs.Point(((912.3333) * transformX) + moveX, ((1004.0000) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((1004.0000) * transformY) + moveY),

        new createjs.Point(((912.0000) * transformX) + moveX, ((1003.3334) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((1002.6666) * transformY) + moveY),
        new createjs.Point(((912.0000) * transformX) + moveX, ((1002.0000) * transformY) + moveY),

        new createjs.Point(((911.6667) * transformX) + moveX, ((1002.0000) * transformY) + moveY),
        new createjs.Point(((911.3333) * transformX) + moveX, ((1002.0000) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((1002.0000) * transformY) + moveY),

        new createjs.Point(((911.0000) * transformX) + moveX, ((1001.0001) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((999.9999) * transformY) + moveY),
        new createjs.Point(((911.0000) * transformX) + moveX, ((999.0000) * transformY) + moveY),

        new createjs.Point(((910.6667) * transformX) + moveX, ((999.0000) * transformY) + moveY),
        new createjs.Point(((910.3333) * transformX) + moveX, ((999.0000) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((999.0000) * transformY) + moveY),

        new createjs.Point(((910.0000) * transformX) + moveX, ((998.3334) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((997.6666) * transformY) + moveY),
        new createjs.Point(((910.0000) * transformX) + moveX, ((997.0000) * transformY) + moveY),

        new createjs.Point(((909.6667) * transformX) + moveX, ((997.0000) * transformY) + moveY),
        new createjs.Point(((909.3333) * transformX) + moveX, ((997.0000) * transformY) + moveY),
        new createjs.Point(((909.0000) * transformX) + moveX, ((997.0000) * transformY) + moveY),

        new createjs.Point(((908.6667) * transformX) + moveX, ((995.3335) * transformY) + moveY),
        new createjs.Point(((908.3333) * transformX) + moveX, ((993.6665) * transformY) + moveY),
        new createjs.Point(((908.0000) * transformX) + moveX, ((992.0000) * transformY) + moveY),

        new createjs.Point(((907.6667) * transformX) + moveX, ((992.0000) * transformY) + moveY),
        new createjs.Point(((907.3333) * transformX) + moveX, ((992.0000) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((992.0000) * transformY) + moveY),

        new createjs.Point(((907.0000) * transformX) + moveX, ((991.3334) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((990.6666) * transformY) + moveY),
        new createjs.Point(((907.0000) * transformX) + moveX, ((990.0000) * transformY) + moveY),

        new createjs.Point(((906.6667) * transformX) + moveX, ((990.0000) * transformY) + moveY),
        new createjs.Point(((906.3333) * transformX) + moveX, ((990.0000) * transformY) + moveY),
        new createjs.Point(((906.0000) * transformX) + moveX, ((990.0000) * transformY) + moveY),

        new createjs.Point(((905.3334) * transformX) + moveX, ((988.0002) * transformY) + moveY),
        new createjs.Point(((904.6666) * transformX) + moveX, ((985.9998) * transformY) + moveY),
        new createjs.Point(((904.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),

        new createjs.Point(((903.6667) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((903.3333) * transformX) + moveX, ((984.0000) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((984.0000) * transformY) + moveY),

        new createjs.Point(((903.0000) * transformX) + moveX, ((983.0001) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((981.9999) * transformY) + moveY),
        new createjs.Point(((903.0000) * transformX) + moveX, ((981.0000) * transformY) + moveY),

        new createjs.Point(((902.6667) * transformX) + moveX, ((981.0000) * transformY) + moveY),
        new createjs.Point(((902.3333) * transformX) + moveX, ((981.0000) * transformY) + moveY),
        new createjs.Point(((902.0000) * transformX) + moveX, ((981.0000) * transformY) + moveY),

        new createjs.Point(((901.3334) * transformX) + moveX, ((979.0002) * transformY) + moveY),
        new createjs.Point(((900.6666) * transformX) + moveX, ((976.9998) * transformY) + moveY),
        new createjs.Point(((900.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),

        new createjs.Point(((899.6667) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((899.3333) * transformX) + moveX, ((975.0000) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),

        new createjs.Point(((899.0000) * transformX) + moveX, ((974.3334) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((973.6666) * transformY) + moveY),
        new createjs.Point(((899.0000) * transformX) + moveX, ((973.0000) * transformY) + moveY),

        new createjs.Point(((898.6667) * transformX) + moveX, ((973.0000) * transformY) + moveY),
        new createjs.Point(((898.3333) * transformX) + moveX, ((973.0000) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((973.0000) * transformY) + moveY),

        new createjs.Point(((898.0000) * transformX) + moveX, ((972.3334) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((971.6666) * transformY) + moveY),
        new createjs.Point(((898.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),

        new createjs.Point(((897.6667) * transformX) + moveX, ((971.0000) * transformY) + moveY),
        new createjs.Point(((897.3333) * transformX) + moveX, ((971.0000) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((971.0000) * transformY) + moveY),

        new createjs.Point(((897.0000) * transformX) + moveX, ((970.3334) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((969.6666) * transformY) + moveY),
        new createjs.Point(((897.0000) * transformX) + moveX, ((969.0000) * transformY) + moveY),

        new createjs.Point(((896.6667) * transformX) + moveX, ((969.0000) * transformY) + moveY),
        new createjs.Point(((896.3333) * transformX) + moveX, ((969.0000) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((969.0000) * transformY) + moveY),

        new createjs.Point(((896.0000) * transformX) + moveX, ((968.3334) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((967.6666) * transformY) + moveY),
        new createjs.Point(((896.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),

        new createjs.Point(((895.6667) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((895.3333) * transformX) + moveX, ((967.0000) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((967.0000) * transformY) + moveY),

        new createjs.Point(((895.0000) * transformX) + moveX, ((966.3334) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((965.6666) * transformY) + moveY),
        new createjs.Point(((895.0000) * transformX) + moveX, ((965.0000) * transformY) + moveY),

        new createjs.Point(((894.6667) * transformX) + moveX, ((965.0000) * transformY) + moveY),
        new createjs.Point(((894.3333) * transformX) + moveX, ((965.0000) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((965.0000) * transformY) + moveY),

        new createjs.Point(((894.0000) * transformX) + moveX, ((964.3334) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((963.6666) * transformY) + moveY),
        new createjs.Point(((894.0000) * transformX) + moveX, ((963.0000) * transformY) + moveY),

        new createjs.Point(((893.3334) * transformX) + moveX, ((962.6667) * transformY) + moveY),
        new createjs.Point(((892.6666) * transformX) + moveX, ((962.3333) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((962.0000) * transformY) + moveY),

        new createjs.Point(((892.0000) * transformX) + moveX, ((961.3334) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((960.6666) * transformY) + moveY),
        new createjs.Point(((892.0000) * transformX) + moveX, ((960.0000) * transformY) + moveY),

        new createjs.Point(((891.6667) * transformX) + moveX, ((960.0000) * transformY) + moveY),
        new createjs.Point(((891.3333) * transformX) + moveX, ((960.0000) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((960.0000) * transformY) + moveY),

        new createjs.Point(((891.0000) * transformX) + moveX, ((959.3334) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((958.6666) * transformY) + moveY),
        new createjs.Point(((891.0000) * transformX) + moveX, ((958.0000) * transformY) + moveY),

        new createjs.Point(((890.6667) * transformX) + moveX, ((958.0000) * transformY) + moveY),
        new createjs.Point(((890.3333) * transformX) + moveX, ((958.0000) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((958.0000) * transformY) + moveY),

        new createjs.Point(((890.0000) * transformX) + moveX, ((957.3334) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((956.6666) * transformY) + moveY),
        new createjs.Point(((890.0000) * transformX) + moveX, ((956.0000) * transformY) + moveY),

        new createjs.Point(((889.6667) * transformX) + moveX, ((956.0000) * transformY) + moveY),
        new createjs.Point(((889.3333) * transformX) + moveX, ((956.0000) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((956.0000) * transformY) + moveY),

        new createjs.Point(((889.0000) * transformX) + moveX, ((955.3334) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((954.6666) * transformY) + moveY),
        new createjs.Point(((889.0000) * transformX) + moveX, ((954.0000) * transformY) + moveY),

        new createjs.Point(((888.6667) * transformX) + moveX, ((954.0000) * transformY) + moveY),
        new createjs.Point(((888.3333) * transformX) + moveX, ((954.0000) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((954.0000) * transformY) + moveY),

        new createjs.Point(((888.0000) * transformX) + moveX, ((953.3334) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((952.6666) * transformY) + moveY),
        new createjs.Point(((888.0000) * transformX) + moveX, ((952.0000) * transformY) + moveY),

        new createjs.Point(((887.3334) * transformX) + moveX, ((951.6667) * transformY) + moveY),
        new createjs.Point(((886.6666) * transformX) + moveX, ((951.3333) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((951.0000) * transformY) + moveY),

        new createjs.Point(((886.0000) * transformX) + moveX, ((950.3334) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((949.6666) * transformY) + moveY),
        new createjs.Point(((886.0000) * transformX) + moveX, ((949.0000) * transformY) + moveY),

        new createjs.Point(((885.6667) * transformX) + moveX, ((949.0000) * transformY) + moveY),
        new createjs.Point(((885.3333) * transformX) + moveX, ((949.0000) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((949.0000) * transformY) + moveY),

        new createjs.Point(((885.0000) * transformX) + moveX, ((948.3334) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((947.6666) * transformY) + moveY),
        new createjs.Point(((885.0000) * transformX) + moveX, ((947.0000) * transformY) + moveY),

        new createjs.Point(((884.3334) * transformX) + moveX, ((946.6667) * transformY) + moveY),
        new createjs.Point(((883.6666) * transformX) + moveX, ((946.3333) * transformY) + moveY),
        new createjs.Point(((883.0000) * transformX) + moveX, ((946.0000) * transformY) + moveY),

        new createjs.Point(((882.6667) * transformX) + moveX, ((944.6668) * transformY) + moveY),
        new createjs.Point(((882.3333) * transformX) + moveX, ((943.3332) * transformY) + moveY),
        new createjs.Point(((882.0000) * transformX) + moveX, ((942.0000) * transformY) + moveY),

        new createjs.Point(((881.3334) * transformX) + moveX, ((941.6667) * transformY) + moveY),
        new createjs.Point(((880.6666) * transformX) + moveX, ((941.3333) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((941.0000) * transformY) + moveY),

        new createjs.Point(((880.0000) * transformX) + moveX, ((940.3334) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((939.6666) * transformY) + moveY),
        new createjs.Point(((880.0000) * transformX) + moveX, ((939.0000) * transformY) + moveY),

        new createjs.Point(((879.6667) * transformX) + moveX, ((939.0000) * transformY) + moveY),
        new createjs.Point(((879.3333) * transformX) + moveX, ((939.0000) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((939.0000) * transformY) + moveY),

        new createjs.Point(((879.0000) * transformX) + moveX, ((938.3334) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((937.6666) * transformY) + moveY),
        new createjs.Point(((879.0000) * transformX) + moveX, ((937.0000) * transformY) + moveY),

        new createjs.Point(((878.3334) * transformX) + moveX, ((936.6667) * transformY) + moveY),
        new createjs.Point(((877.6666) * transformX) + moveX, ((936.3333) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((936.0000) * transformY) + moveY),

        new createjs.Point(((877.0000) * transformX) + moveX, ((935.3334) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((934.6666) * transformY) + moveY),
        new createjs.Point(((877.0000) * transformX) + moveX, ((934.0000) * transformY) + moveY),

        new createjs.Point(((876.6667) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((876.3333) * transformX) + moveX, ((934.0000) * transformY) + moveY),
        new createjs.Point(((876.0000) * transformX) + moveX, ((934.0000) * transformY) + moveY),

        new createjs.Point(((875.6667) * transformX) + moveX, ((933.0001) * transformY) + moveY),
        new createjs.Point(((875.3333) * transformX) + moveX, ((931.9999) * transformY) + moveY),
        new createjs.Point(((875.0000) * transformX) + moveX, ((931.0000) * transformY) + moveY),

        new createjs.Point(((874.3334) * transformX) + moveX, ((930.6667) * transformY) + moveY),
        new createjs.Point(((873.6666) * transformX) + moveX, ((930.3333) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((930.0000) * transformY) + moveY),

        new createjs.Point(((873.0000) * transformX) + moveX, ((929.3334) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((928.6666) * transformY) + moveY),
        new createjs.Point(((873.0000) * transformX) + moveX, ((928.0000) * transformY) + moveY),

        new createjs.Point(((872.3334) * transformX) + moveX, ((927.6667) * transformY) + moveY),
        new createjs.Point(((871.6666) * transformX) + moveX, ((927.3333) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((927.0000) * transformY) + moveY),

        new createjs.Point(((871.0000) * transformX) + moveX, ((926.3334) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((925.6666) * transformY) + moveY),
        new createjs.Point(((871.0000) * transformX) + moveX, ((925.0000) * transformY) + moveY),

        new createjs.Point(((870.6667) * transformX) + moveX, ((925.0000) * transformY) + moveY),
        new createjs.Point(((870.3333) * transformX) + moveX, ((925.0000) * transformY) + moveY),
        new createjs.Point(((870.0000) * transformX) + moveX, ((925.0000) * transformY) + moveY),

        new createjs.Point(((869.6667) * transformX) + moveX, ((924.0001) * transformY) + moveY),
        new createjs.Point(((869.3333) * transformX) + moveX, ((922.9999) * transformY) + moveY),
        new createjs.Point(((869.0000) * transformX) + moveX, ((922.0000) * transformY) + moveY),

        new createjs.Point(((868.3334) * transformX) + moveX, ((921.6667) * transformY) + moveY),
        new createjs.Point(((867.6666) * transformX) + moveX, ((921.3333) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((921.0000) * transformY) + moveY),

        new createjs.Point(((867.0000) * transformX) + moveX, ((920.3334) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((919.6666) * transformY) + moveY),
        new createjs.Point(((867.0000) * transformX) + moveX, ((919.0000) * transformY) + moveY),

        new createjs.Point(((866.0001) * transformX) + moveX, ((918.3334) * transformY) + moveY),
        new createjs.Point(((864.9999) * transformX) + moveX, ((917.6666) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((917.0000) * transformY) + moveY),

        new createjs.Point(((864.0000) * transformX) + moveX, ((916.3334) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((915.6666) * transformY) + moveY),
        new createjs.Point(((864.0000) * transformX) + moveX, ((915.0000) * transformY) + moveY),

        new createjs.Point(((863.3334) * transformX) + moveX, ((914.6667) * transformY) + moveY),
        new createjs.Point(((862.6666) * transformX) + moveX, ((914.3333) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((914.0000) * transformY) + moveY),

        new createjs.Point(((862.0000) * transformX) + moveX, ((913.3334) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((912.6666) * transformY) + moveY),
        new createjs.Point(((862.0000) * transformX) + moveX, ((912.0000) * transformY) + moveY),

        new createjs.Point(((861.0001) * transformX) + moveX, ((911.3334) * transformY) + moveY),
        new createjs.Point(((859.9999) * transformX) + moveX, ((910.6666) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((910.0000) * transformY) + moveY),

        new createjs.Point(((859.0000) * transformX) + moveX, ((909.3334) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((908.6666) * transformY) + moveY),
        new createjs.Point(((859.0000) * transformX) + moveX, ((908.0000) * transformY) + moveY),

        new createjs.Point(((857.6668) * transformX) + moveX, ((907.0001) * transformY) + moveY),
        new createjs.Point(((856.3332) * transformX) + moveX, ((905.9999) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((905.0000) * transformY) + moveY),

        new createjs.Point(((855.0000) * transformX) + moveX, ((904.3334) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((903.6666) * transformY) + moveY),
        new createjs.Point(((855.0000) * transformX) + moveX, ((903.0000) * transformY) + moveY),

        new createjs.Point(((853.6668) * transformX) + moveX, ((902.0001) * transformY) + moveY),
        new createjs.Point(((852.3332) * transformX) + moveX, ((900.9999) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((900.0000) * transformY) + moveY),

        new createjs.Point(((851.0000) * transformX) + moveX, ((899.3334) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((898.6666) * transformY) + moveY),
        new createjs.Point(((851.0000) * transformX) + moveX, ((898.0000) * transformY) + moveY),

        new createjs.Point(((849.3335) * transformX) + moveX, ((896.6668) * transformY) + moveY),
        new createjs.Point(((847.6665) * transformX) + moveX, ((895.3332) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((894.0000) * transformY) + moveY),

        new createjs.Point(((846.0000) * transformX) + moveX, ((893.3334) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((892.6666) * transformY) + moveY),
        new createjs.Point(((846.0000) * transformX) + moveX, ((892.0000) * transformY) + moveY),

        new createjs.Point(((843.6669) * transformX) + moveX, ((890.0002) * transformY) + moveY),
        new createjs.Point(((841.3331) * transformX) + moveX, ((887.9998) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((886.0000) * transformY) + moveY),

        new createjs.Point(((839.0000) * transformX) + moveX, ((885.3334) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((884.6666) * transformY) + moveY),
        new createjs.Point(((839.0000) * transformX) + moveX, ((884.0000) * transformY) + moveY),

        new createjs.Point(((833.0006) * transformX) + moveX, ((878.3339) * transformY) + moveY),
        new createjs.Point(((826.9994) * transformX) + moveX, ((872.6661) * transformY) + moveY),
        new createjs.Point(((821.0000) * transformX) + moveX, ((867.0000) * transformY) + moveY),

        new createjs.Point(((818.6669) * transformX) + moveX, ((864.3336) * transformY) + moveY),
        new createjs.Point(((816.3331) * transformX) + moveX, ((861.6664) * transformY) + moveY),
        new createjs.Point(((814.0000) * transformX) + moveX, ((859.0000) * transformY) + moveY),

        new createjs.Point(((813.3334) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((812.6666) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((812.0000) * transformX) + moveX, ((859.0000) * transformY) + moveY),

        new createjs.Point(((810.0002) * transformX) + moveX, ((856.6669) * transformY) + moveY),
        new createjs.Point(((807.9998) * transformX) + moveX, ((854.3331) * transformY) + moveY),
        new createjs.Point(((806.0000) * transformX) + moveX, ((852.0000) * transformY) + moveY),

        new createjs.Point(((805.3334) * transformX) + moveX, ((852.0000) * transformY) + moveY),
        new createjs.Point(((804.6666) * transformX) + moveX, ((852.0000) * transformY) + moveY),
        new createjs.Point(((804.0000) * transformX) + moveX, ((852.0000) * transformY) + moveY),

        new createjs.Point(((802.6668) * transformX) + moveX, ((850.3335) * transformY) + moveY),
        new createjs.Point(((801.3332) * transformX) + moveX, ((848.6665) * transformY) + moveY),
        new createjs.Point(((800.0000) * transformX) + moveX, ((847.0000) * transformY) + moveY),

        new createjs.Point(((799.3334) * transformX) + moveX, ((847.0000) * transformY) + moveY),
        new createjs.Point(((798.6666) * transformX) + moveX, ((847.0000) * transformY) + moveY),
        new createjs.Point(((798.0000) * transformX) + moveX, ((847.0000) * transformY) + moveY),

        new createjs.Point(((797.3334) * transformX) + moveX, ((846.0001) * transformY) + moveY),
        new createjs.Point(((796.6666) * transformX) + moveX, ((844.9999) * transformY) + moveY),
        new createjs.Point(((796.0000) * transformX) + moveX, ((844.0000) * transformY) + moveY),

        new createjs.Point(((795.3334) * transformX) + moveX, ((844.0000) * transformY) + moveY),
        new createjs.Point(((794.6666) * transformX) + moveX, ((844.0000) * transformY) + moveY),
        new createjs.Point(((794.0000) * transformX) + moveX, ((844.0000) * transformY) + moveY),

        new createjs.Point(((793.0001) * transformX) + moveX, ((842.6668) * transformY) + moveY),
        new createjs.Point(((791.9999) * transformX) + moveX, ((841.3332) * transformY) + moveY),
        new createjs.Point(((791.0000) * transformX) + moveX, ((840.0000) * transformY) + moveY),

        new createjs.Point(((790.0001) * transformX) + moveX, ((839.6667) * transformY) + moveY),
        new createjs.Point(((788.9999) * transformX) + moveX, ((839.3333) * transformY) + moveY),
        new createjs.Point(((788.0000) * transformX) + moveX, ((839.0000) * transformY) + moveY),

        new createjs.Point(((787.6667) * transformX) + moveX, ((838.3334) * transformY) + moveY),
        new createjs.Point(((787.3333) * transformX) + moveX, ((837.6666) * transformY) + moveY),
        new createjs.Point(((787.0000) * transformX) + moveX, ((837.0000) * transformY) + moveY),

        new createjs.Point(((786.3334) * transformX) + moveX, ((837.0000) * transformY) + moveY),
        new createjs.Point(((785.6666) * transformX) + moveX, ((837.0000) * transformY) + moveY),
        new createjs.Point(((785.0000) * transformX) + moveX, ((837.0000) * transformY) + moveY),

        new createjs.Point(((784.6667) * transformX) + moveX, ((836.3334) * transformY) + moveY),
        new createjs.Point(((784.3333) * transformX) + moveX, ((835.6666) * transformY) + moveY),
        new createjs.Point(((784.0000) * transformX) + moveX, ((835.0000) * transformY) + moveY),

        new createjs.Point(((783.3334) * transformX) + moveX, ((835.0000) * transformY) + moveY),
        new createjs.Point(((782.6666) * transformX) + moveX, ((835.0000) * transformY) + moveY),
        new createjs.Point(((782.0000) * transformX) + moveX, ((835.0000) * transformY) + moveY),

        new createjs.Point(((781.3334) * transformX) + moveX, ((834.0001) * transformY) + moveY),
        new createjs.Point(((780.6666) * transformX) + moveX, ((832.9999) * transformY) + moveY),
        new createjs.Point(((780.0000) * transformX) + moveX, ((832.0000) * transformY) + moveY),

        new createjs.Point(((779.3334) * transformX) + moveX, ((832.0000) * transformY) + moveY),
        new createjs.Point(((778.6666) * transformX) + moveX, ((832.0000) * transformY) + moveY),
        new createjs.Point(((778.0000) * transformX) + moveX, ((832.0000) * transformY) + moveY),

        new createjs.Point(((777.6667) * transformX) + moveX, ((831.3334) * transformY) + moveY),
        new createjs.Point(((777.3333) * transformX) + moveX, ((830.6666) * transformY) + moveY),
        new createjs.Point(((777.0000) * transformX) + moveX, ((830.0000) * transformY) + moveY),

        new createjs.Point(((776.0001) * transformX) + moveX, ((829.6667) * transformY) + moveY),
        new createjs.Point(((774.9999) * transformX) + moveX, ((829.3333) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((829.0000) * transformY) + moveY),

        new createjs.Point(((774.0000) * transformX) + moveX, ((828.6667) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((828.3333) * transformY) + moveY),
        new createjs.Point(((774.0000) * transformX) + moveX, ((828.0000) * transformY) + moveY),

        new createjs.Point(((773.3334) * transformX) + moveX, ((828.0000) * transformY) + moveY),
        new createjs.Point(((772.6666) * transformX) + moveX, ((828.0000) * transformY) + moveY),
        new createjs.Point(((772.0000) * transformX) + moveX, ((828.0000) * transformY) + moveY),

        new createjs.Point(((771.6667) * transformX) + moveX, ((827.3334) * transformY) + moveY),
        new createjs.Point(((771.3333) * transformX) + moveX, ((826.6666) * transformY) + moveY),
        new createjs.Point(((771.0000) * transformX) + moveX, ((826.0000) * transformY) + moveY),

        new createjs.Point(((769.6668) * transformX) + moveX, ((825.6667) * transformY) + moveY),
        new createjs.Point(((768.3332) * transformX) + moveX, ((825.3333) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((825.0000) * transformY) + moveY),

        new createjs.Point(((767.0000) * transformX) + moveX, ((824.6667) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((824.3333) * transformY) + moveY),
        new createjs.Point(((767.0000) * transformX) + moveX, ((824.0000) * transformY) + moveY),

        new createjs.Point(((766.0001) * transformX) + moveX, ((823.6667) * transformY) + moveY),
        new createjs.Point(((764.9999) * transformX) + moveX, ((823.3333) * transformY) + moveY),
        new createjs.Point(((764.0000) * transformX) + moveX, ((823.0000) * transformY) + moveY),

        new createjs.Point(((763.6667) * transformX) + moveX, ((822.3334) * transformY) + moveY),
        new createjs.Point(((763.3333) * transformX) + moveX, ((821.6666) * transformY) + moveY),
        new createjs.Point(((763.0000) * transformX) + moveX, ((821.0000) * transformY) + moveY),

        new createjs.Point(((761.6668) * transformX) + moveX, ((820.6667) * transformY) + moveY),
        new createjs.Point(((760.3332) * transformX) + moveX, ((820.3333) * transformY) + moveY),
        new createjs.Point(((759.0000) * transformX) + moveX, ((820.0000) * transformY) + moveY),

        new createjs.Point(((758.6667) * transformX) + moveX, ((819.3334) * transformY) + moveY),
        new createjs.Point(((758.3333) * transformX) + moveX, ((818.6666) * transformY) + moveY),
        new createjs.Point(((758.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((756.0002) * transformX) + moveX, ((817.3334) * transformY) + moveY),
        new createjs.Point(((753.9998) * transformX) + moveX, ((816.6666) * transformY) + moveY),
        new createjs.Point(((752.0000) * transformX) + moveX, ((816.0000) * transformY) + moveY),

        new createjs.Point(((751.6667) * transformX) + moveX, ((815.3334) * transformY) + moveY),
        new createjs.Point(((751.3333) * transformX) + moveX, ((814.6666) * transformY) + moveY),
        new createjs.Point(((751.0000) * transformX) + moveX, ((814.0000) * transformY) + moveY),

        new createjs.Point(((750.3334) * transformX) + moveX, ((814.0000) * transformY) + moveY),
        new createjs.Point(((749.6666) * transformX) + moveX, ((814.0000) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((814.0000) * transformY) + moveY),

        new createjs.Point(((749.0000) * transformX) + moveX, ((813.6667) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((813.3333) * transformY) + moveY),
        new createjs.Point(((749.0000) * transformX) + moveX, ((813.0000) * transformY) + moveY),

        new createjs.Point(((748.3334) * transformX) + moveX, ((813.0000) * transformY) + moveY),
        new createjs.Point(((747.6666) * transformX) + moveX, ((813.0000) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((813.0000) * transformY) + moveY),

        new createjs.Point(((747.0000) * transformX) + moveX, ((812.6667) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((812.3333) * transformY) + moveY),
        new createjs.Point(((747.0000) * transformX) + moveX, ((812.0000) * transformY) + moveY),

        new createjs.Point(((746.3334) * transformX) + moveX, ((812.0000) * transformY) + moveY),
        new createjs.Point(((745.6666) * transformX) + moveX, ((812.0000) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((812.0000) * transformY) + moveY),

        new createjs.Point(((745.0000) * transformX) + moveX, ((811.6667) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((811.3333) * transformY) + moveY),
        new createjs.Point(((745.0000) * transformX) + moveX, ((811.0000) * transformY) + moveY),

        new createjs.Point(((744.3334) * transformX) + moveX, ((811.0000) * transformY) + moveY),
        new createjs.Point(((743.6666) * transformX) + moveX, ((811.0000) * transformY) + moveY),
        new createjs.Point(((743.0000) * transformX) + moveX, ((811.0000) * transformY) + moveY),

        new createjs.Point(((743.0000) * transformX) + moveX, ((810.6667) * transformY) + moveY),
        new createjs.Point(((743.0000) * transformX) + moveX, ((810.3333) * transformY) + moveY),
        new createjs.Point(((743.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),

        new createjs.Point(((742.3334) * transformX) + moveX, ((810.0000) * transformY) + moveY),
        new createjs.Point(((741.6666) * transformX) + moveX, ((810.0000) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),

        new createjs.Point(((741.0000) * transformX) + moveX, ((809.6667) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((809.3333) * transformY) + moveY),
        new createjs.Point(((741.0000) * transformX) + moveX, ((809.0000) * transformY) + moveY),

        new createjs.Point(((740.3334) * transformX) + moveX, ((809.0000) * transformY) + moveY),
        new createjs.Point(((739.6666) * transformX) + moveX, ((809.0000) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((809.0000) * transformY) + moveY),

        new createjs.Point(((739.0000) * transformX) + moveX, ((808.6667) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((808.3333) * transformY) + moveY),
        new createjs.Point(((739.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((738.3334) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((737.6666) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((737.0000) * transformX) + moveX, ((807.6667) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((807.3333) * transformY) + moveY),
        new createjs.Point(((737.0000) * transformX) + moveX, ((807.0000) * transformY) + moveY),

        new createjs.Point(((736.3334) * transformX) + moveX, ((807.0000) * transformY) + moveY),
        new createjs.Point(((735.6666) * transformX) + moveX, ((807.0000) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((807.0000) * transformY) + moveY),

        new createjs.Point(((735.0000) * transformX) + moveX, ((806.6667) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((806.3333) * transformY) + moveY),
        new createjs.Point(((735.0000) * transformX) + moveX, ((806.0000) * transformY) + moveY),

        new createjs.Point(((734.3334) * transformX) + moveX, ((806.0000) * transformY) + moveY),
        new createjs.Point(((733.6666) * transformX) + moveX, ((806.0000) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((806.0000) * transformY) + moveY),

        new createjs.Point(((733.0000) * transformX) + moveX, ((805.6667) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((805.3333) * transformY) + moveY),
        new createjs.Point(((733.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),

        new createjs.Point(((731.0002) * transformX) + moveX, ((804.3334) * transformY) + moveY),
        new createjs.Point(((728.9998) * transformX) + moveX, ((803.6666) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((803.0000) * transformY) + moveY),

        new createjs.Point(((727.0000) * transformX) + moveX, ((802.6667) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((802.3333) * transformY) + moveY),
        new createjs.Point(((727.0000) * transformX) + moveX, ((802.0000) * transformY) + moveY),

        new createjs.Point(((725.3335) * transformX) + moveX, ((801.6667) * transformY) + moveY),
        new createjs.Point(((723.6665) * transformX) + moveX, ((801.3333) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((801.0000) * transformY) + moveY),

        new createjs.Point(((722.0000) * transformX) + moveX, ((800.6667) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((800.3333) * transformY) + moveY),
        new createjs.Point(((722.0000) * transformX) + moveX, ((800.0000) * transformY) + moveY),

        new createjs.Point(((720.6668) * transformX) + moveX, ((799.6667) * transformY) + moveY),
        new createjs.Point(((719.3332) * transformX) + moveX, ((799.3333) * transformY) + moveY),
        new createjs.Point(((718.0000) * transformX) + moveX, ((799.0000) * transformY) + moveY),

        new createjs.Point(((718.0000) * transformX) + moveX, ((798.6667) * transformY) + moveY),
        new createjs.Point(((718.0000) * transformX) + moveX, ((798.3333) * transformY) + moveY),
        new createjs.Point(((718.0000) * transformX) + moveX, ((798.0000) * transformY) + moveY),

        new createjs.Point(((717.0001) * transformX) + moveX, ((798.0000) * transformY) + moveY),
        new createjs.Point(((715.9999) * transformX) + moveX, ((798.0000) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((798.0000) * transformY) + moveY),

        new createjs.Point(((715.0000) * transformX) + moveX, ((797.6667) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((797.3333) * transformY) + moveY),
        new createjs.Point(((715.0000) * transformX) + moveX, ((797.0000) * transformY) + moveY),

        new createjs.Point(((714.3334) * transformX) + moveX, ((797.0000) * transformY) + moveY),
        new createjs.Point(((713.6666) * transformX) + moveX, ((797.0000) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((797.0000) * transformY) + moveY),

        new createjs.Point(((713.0000) * transformX) + moveX, ((796.6667) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((796.3333) * transformY) + moveY),
        new createjs.Point(((713.0000) * transformX) + moveX, ((796.0000) * transformY) + moveY),

        new createjs.Point(((712.0001) * transformX) + moveX, ((796.0000) * transformY) + moveY),
        new createjs.Point(((710.9999) * transformX) + moveX, ((796.0000) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((796.0000) * transformY) + moveY),

        new createjs.Point(((710.0000) * transformX) + moveX, ((795.6667) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((795.3333) * transformY) + moveY),
        new createjs.Point(((710.0000) * transformX) + moveX, ((795.0000) * transformY) + moveY),

        new createjs.Point(((709.3334) * transformX) + moveX, ((795.0000) * transformY) + moveY),
        new createjs.Point(((708.6666) * transformX) + moveX, ((795.0000) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((795.0000) * transformY) + moveY),

        new createjs.Point(((708.0000) * transformX) + moveX, ((794.6667) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((794.3333) * transformY) + moveY),
        new createjs.Point(((708.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),

        new createjs.Point(((705.0003) * transformX) + moveX, ((793.3334) * transformY) + moveY),
        new createjs.Point(((701.9997) * transformX) + moveX, ((792.6666) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((792.0000) * transformY) + moveY),

        new createjs.Point(((699.0000) * transformX) + moveX, ((791.6667) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((791.3333) * transformY) + moveY),
        new createjs.Point(((699.0000) * transformX) + moveX, ((791.0000) * transformY) + moveY),

        new createjs.Point(((698.3334) * transformX) + moveX, ((791.0000) * transformY) + moveY),
        new createjs.Point(((697.6666) * transformX) + moveX, ((791.0000) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((791.0000) * transformY) + moveY),

        new createjs.Point(((697.0000) * transformX) + moveX, ((790.6667) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((790.3333) * transformY) + moveY),
        new createjs.Point(((697.0000) * transformX) + moveX, ((790.0000) * transformY) + moveY),

        new createjs.Point(((696.0001) * transformX) + moveX, ((790.0000) * transformY) + moveY),
        new createjs.Point(((694.9999) * transformX) + moveX, ((790.0000) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((790.0000) * transformY) + moveY),

        new createjs.Point(((694.0000) * transformX) + moveX, ((789.6667) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((789.3333) * transformY) + moveY),
        new createjs.Point(((694.0000) * transformX) + moveX, ((789.0000) * transformY) + moveY),

        new createjs.Point(((692.6668) * transformX) + moveX, ((789.0000) * transformY) + moveY),
        new createjs.Point(((691.3332) * transformX) + moveX, ((789.0000) * transformY) + moveY),
        new createjs.Point(((690.0000) * transformX) + moveX, ((789.0000) * transformY) + moveY),

        new createjs.Point(((690.0000) * transformX) + moveX, ((788.6667) * transformY) + moveY),
        new createjs.Point(((690.0000) * transformX) + moveX, ((788.3333) * transformY) + moveY),
        new createjs.Point(((690.0000) * transformX) + moveX, ((788.0000) * transformY) + moveY),

        new createjs.Point(((689.0001) * transformX) + moveX, ((788.0000) * transformY) + moveY),
        new createjs.Point(((687.9999) * transformX) + moveX, ((788.0000) * transformY) + moveY),
        new createjs.Point(((687.0000) * transformX) + moveX, ((788.0000) * transformY) + moveY),

        new createjs.Point(((687.0000) * transformX) + moveX, ((787.6667) * transformY) + moveY),
        new createjs.Point(((687.0000) * transformX) + moveX, ((787.3333) * transformY) + moveY),
        new createjs.Point(((687.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),

        new createjs.Point(((686.0001) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((684.9999) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),

        new createjs.Point(((684.0000) * transformX) + moveX, ((786.6667) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((786.3333) * transformY) + moveY),
        new createjs.Point(((684.0000) * transformX) + moveX, ((786.0000) * transformY) + moveY),

        new createjs.Point(((683.0001) * transformX) + moveX, ((786.0000) * transformY) + moveY),
        new createjs.Point(((681.9999) * transformX) + moveX, ((786.0000) * transformY) + moveY),
        new createjs.Point(((681.0000) * transformX) + moveX, ((786.0000) * transformY) + moveY),

        new createjs.Point(((681.0000) * transformX) + moveX, ((785.6667) * transformY) + moveY),
        new createjs.Point(((681.0000) * transformX) + moveX, ((785.3333) * transformY) + moveY),
        new createjs.Point(((681.0000) * transformX) + moveX, ((785.0000) * transformY) + moveY),

        new createjs.Point(((678.3336) * transformX) + moveX, ((784.6667) * transformY) + moveY),
        new createjs.Point(((675.6664) * transformX) + moveX, ((784.3333) * transformY) + moveY),
        new createjs.Point(((673.0000) * transformX) + moveX, ((784.0000) * transformY) + moveY),

        new createjs.Point(((673.0000) * transformX) + moveX, ((783.6667) * transformY) + moveY),
        new createjs.Point(((673.0000) * transformX) + moveX, ((783.3333) * transformY) + moveY),
        new createjs.Point(((673.0000) * transformX) + moveX, ((783.0000) * transformY) + moveY),

        new createjs.Point(((672.0001) * transformX) + moveX, ((783.0000) * transformY) + moveY),
        new createjs.Point(((670.9999) * transformX) + moveX, ((783.0000) * transformY) + moveY),
        new createjs.Point(((670.0000) * transformX) + moveX, ((783.0000) * transformY) + moveY),

        new createjs.Point(((670.0000) * transformX) + moveX, ((782.6667) * transformY) + moveY),
        new createjs.Point(((670.0000) * transformX) + moveX, ((782.3333) * transformY) + moveY),
        new createjs.Point(((670.0000) * transformX) + moveX, ((782.0000) * transformY) + moveY),

        new createjs.Point(((667.0003) * transformX) + moveX, ((781.6667) * transformY) + moveY),
        new createjs.Point(((663.9997) * transformX) + moveX, ((781.3333) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((781.0000) * transformY) + moveY),

        new createjs.Point(((661.0000) * transformX) + moveX, ((780.6667) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((780.3333) * transformY) + moveY),
        new createjs.Point(((661.0000) * transformX) + moveX, ((780.0000) * transformY) + moveY),

        new createjs.Point(((659.6668) * transformX) + moveX, ((780.0000) * transformY) + moveY),
        new createjs.Point(((658.3332) * transformX) + moveX, ((780.0000) * transformY) + moveY),
        new createjs.Point(((657.0000) * transformX) + moveX, ((780.0000) * transformY) + moveY),

        new createjs.Point(((657.0000) * transformX) + moveX, ((779.6667) * transformY) + moveY),
        new createjs.Point(((657.0000) * transformX) + moveX, ((779.3333) * transformY) + moveY),
        new createjs.Point(((657.0000) * transformX) + moveX, ((779.0000) * transformY) + moveY),

        new createjs.Point(((655.3335) * transformX) + moveX, ((779.0000) * transformY) + moveY),
        new createjs.Point(((653.6665) * transformX) + moveX, ((779.0000) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((779.0000) * transformY) + moveY),

        new createjs.Point(((652.0000) * transformX) + moveX, ((778.6667) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((778.3333) * transformY) + moveY),
        new createjs.Point(((652.0000) * transformX) + moveX, ((778.0000) * transformY) + moveY),

        new createjs.Point(((650.3335) * transformX) + moveX, ((778.0000) * transformY) + moveY),
        new createjs.Point(((648.6665) * transformX) + moveX, ((778.0000) * transformY) + moveY),
        new createjs.Point(((647.0000) * transformX) + moveX, ((778.0000) * transformY) + moveY),

        new createjs.Point(((647.0000) * transformX) + moveX, ((777.6667) * transformY) + moveY),
        new createjs.Point(((647.0000) * transformX) + moveX, ((777.3333) * transformY) + moveY),
        new createjs.Point(((647.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((645.0002) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((642.9998) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((641.0000) * transformX) + moveX, ((776.6667) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((776.3333) * transformY) + moveY),
        new createjs.Point(((641.0000) * transformX) + moveX, ((776.0000) * transformY) + moveY),

        new createjs.Point(((639.0002) * transformX) + moveX, ((776.0000) * transformY) + moveY),
        new createjs.Point(((636.9998) * transformX) + moveX, ((776.0000) * transformY) + moveY),
        new createjs.Point(((635.0000) * transformX) + moveX, ((776.0000) * transformY) + moveY),

        new createjs.Point(((635.0000) * transformX) + moveX, ((775.6667) * transformY) + moveY),
        new createjs.Point(((635.0000) * transformX) + moveX, ((775.3333) * transformY) + moveY),
        new createjs.Point(((635.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),

        new createjs.Point(((632.6669) * transformX) + moveX, ((775.0000) * transformY) + moveY),
        new createjs.Point(((630.3331) * transformX) + moveX, ((775.0000) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),

        new createjs.Point(((628.0000) * transformX) + moveX, ((774.6667) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((774.3333) * transformY) + moveY),
        new createjs.Point(((628.0000) * transformX) + moveX, ((774.0000) * transformY) + moveY),

        new createjs.Point(((625.3336) * transformX) + moveX, ((774.0000) * transformY) + moveY),
        new createjs.Point(((622.6664) * transformX) + moveX, ((774.0000) * transformY) + moveY),
        new createjs.Point(((620.0000) * transformX) + moveX, ((774.0000) * transformY) + moveY),

        new createjs.Point(((620.0000) * transformX) + moveX, ((773.6667) * transformY) + moveY),
        new createjs.Point(((620.0000) * transformX) + moveX, ((773.3333) * transformY) + moveY),
        new createjs.Point(((620.0000) * transformX) + moveX, ((773.0000) * transformY) + moveY),

        new createjs.Point(((616.6670) * transformX) + moveX, ((773.0000) * transformY) + moveY),
        new createjs.Point(((613.3330) * transformX) + moveX, ((773.0000) * transformY) + moveY),
        new createjs.Point(((610.0000) * transformX) + moveX, ((773.0000) * transformY) + moveY),

        new createjs.Point(((610.0000) * transformX) + moveX, ((772.6667) * transformY) + moveY),
        new createjs.Point(((610.0000) * transformX) + moveX, ((772.3333) * transformY) + moveY),
        new createjs.Point(((610.0000) * transformX) + moveX, ((772.0000) * transformY) + moveY),

        new createjs.Point(((605.0005) * transformX) + moveX, ((772.0000) * transformY) + moveY),
        new createjs.Point(((599.9995) * transformX) + moveX, ((772.0000) * transformY) + moveY),
        new createjs.Point(((595.0000) * transformX) + moveX, ((772.0000) * transformY) + moveY),

        new createjs.Point(((595.0000) * transformX) + moveX, ((771.6667) * transformY) + moveY),
        new createjs.Point(((595.0000) * transformX) + moveX, ((771.3333) * transformY) + moveY),
        new createjs.Point(((595.0000) * transformX) + moveX, ((771.0000) * transformY) + moveY),

        new createjs.Point(((591.6670) * transformX) + moveX, ((771.0000) * transformY) + moveY),
        new createjs.Point(((588.3330) * transformX) + moveX, ((771.0000) * transformY) + moveY),
        new createjs.Point(((585.0000) * transformX) + moveX, ((771.0000) * transformY) + moveY),

        new createjs.Point(((576.6889) * transformX) + moveX, ((770.9932) * transformY) + moveY),
        new createjs.Point(((565.6374) * transformX) + moveX, ((770.1172) * transformY) + moveY),
        new createjs.Point(((559.0000) * transformX) + moveX, ((772.0000) * transformY) + moveY),

        new createjs.Point(((552.6673) * transformX) + moveX, ((772.3333) * transformY) + moveY),
        new createjs.Point(((546.3327) * transformX) + moveX, ((772.6667) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((773.0000) * transformY) + moveY),

        new createjs.Point(((540.0000) * transformX) + moveX, ((773.3333) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((773.6667) * transformY) + moveY),
        new createjs.Point(((540.0000) * transformX) + moveX, ((774.0000) * transformY) + moveY),

        new createjs.Point(((536.0004) * transformX) + moveX, ((774.3333) * transformY) + moveY),
        new createjs.Point(((531.9996) * transformX) + moveX, ((774.6667) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((775.0000) * transformY) + moveY),

        new createjs.Point(((528.0000) * transformX) + moveX, ((775.3333) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((775.6667) * transformY) + moveY),
        new createjs.Point(((528.0000) * transformX) + moveX, ((776.0000) * transformY) + moveY),

        new createjs.Point(((526.3335) * transformX) + moveX, ((776.0000) * transformY) + moveY),
        new createjs.Point(((524.6665) * transformX) + moveX, ((776.0000) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((776.0000) * transformY) + moveY),

        new createjs.Point(((523.0000) * transformX) + moveX, ((776.3333) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((776.6667) * transformY) + moveY),
        new createjs.Point(((523.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((521.3335) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((519.6665) * transformX) + moveX, ((777.0000) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((777.0000) * transformY) + moveY),

        new createjs.Point(((518.0000) * transformX) + moveX, ((777.3333) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((777.6667) * transformY) + moveY),
        new createjs.Point(((518.0000) * transformX) + moveX, ((778.0000) * transformY) + moveY),

        new createjs.Point(((516.6668) * transformX) + moveX, ((778.0000) * transformY) + moveY),
        new createjs.Point(((515.3332) * transformX) + moveX, ((778.0000) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((778.0000) * transformY) + moveY),

        new createjs.Point(((514.0000) * transformX) + moveX, ((778.3333) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((778.6667) * transformY) + moveY),
        new createjs.Point(((514.0000) * transformX) + moveX, ((779.0000) * transformY) + moveY),

        new createjs.Point(((512.6668) * transformX) + moveX, ((779.0000) * transformY) + moveY),
        new createjs.Point(((511.3332) * transformX) + moveX, ((779.0000) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((779.0000) * transformY) + moveY),

        new createjs.Point(((510.0000) * transformX) + moveX, ((779.3333) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((779.6667) * transformY) + moveY),
        new createjs.Point(((510.0000) * transformX) + moveX, ((780.0000) * transformY) + moveY),

        new createjs.Point(((508.6668) * transformX) + moveX, ((780.0000) * transformY) + moveY),
        new createjs.Point(((507.3332) * transformX) + moveX, ((780.0000) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((780.0000) * transformY) + moveY),

        new createjs.Point(((506.0000) * transformX) + moveX, ((780.3333) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((780.6667) * transformY) + moveY),
        new createjs.Point(((506.0000) * transformX) + moveX, ((781.0000) * transformY) + moveY),

        new createjs.Point(((504.6668) * transformX) + moveX, ((781.0000) * transformY) + moveY),
        new createjs.Point(((503.3332) * transformX) + moveX, ((781.0000) * transformY) + moveY),
        new createjs.Point(((502.0000) * transformX) + moveX, ((781.0000) * transformY) + moveY),

        new createjs.Point(((502.0000) * transformX) + moveX, ((781.3333) * transformY) + moveY),
        new createjs.Point(((502.0000) * transformX) + moveX, ((781.6667) * transformY) + moveY),
        new createjs.Point(((502.0000) * transformX) + moveX, ((782.0000) * transformY) + moveY),

        new createjs.Point(((500.6668) * transformX) + moveX, ((782.0000) * transformY) + moveY),
        new createjs.Point(((499.3332) * transformX) + moveX, ((782.0000) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((782.0000) * transformY) + moveY),

        new createjs.Point(((498.0000) * transformX) + moveX, ((782.3333) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((782.6667) * transformY) + moveY),
        new createjs.Point(((498.0000) * transformX) + moveX, ((783.0000) * transformY) + moveY),

        new createjs.Point(((496.6668) * transformX) + moveX, ((783.0000) * transformY) + moveY),
        new createjs.Point(((495.3332) * transformX) + moveX, ((783.0000) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((783.0000) * transformY) + moveY),

        new createjs.Point(((494.0000) * transformX) + moveX, ((783.3333) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((783.6667) * transformY) + moveY),
        new createjs.Point(((494.0000) * transformX) + moveX, ((784.0000) * transformY) + moveY),

        new createjs.Point(((490.6670) * transformX) + moveX, ((784.6666) * transformY) + moveY),
        new createjs.Point(((487.3330) * transformX) + moveX, ((785.3334) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((786.0000) * transformY) + moveY),

        new createjs.Point(((484.0000) * transformX) + moveX, ((786.3333) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((786.6667) * transformY) + moveY),
        new createjs.Point(((484.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),

        new createjs.Point(((483.0001) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((481.9999) * transformX) + moveX, ((787.0000) * transformY) + moveY),
        new createjs.Point(((481.0000) * transformX) + moveX, ((787.0000) * transformY) + moveY),

        new createjs.Point(((481.0000) * transformX) + moveX, ((787.3333) * transformY) + moveY),
        new createjs.Point(((481.0000) * transformX) + moveX, ((787.6667) * transformY) + moveY),
        new createjs.Point(((481.0000) * transformX) + moveX, ((788.0000) * transformY) + moveY),

        new createjs.Point(((480.0001) * transformX) + moveX, ((788.0000) * transformY) + moveY),
        new createjs.Point(((478.9999) * transformX) + moveX, ((788.0000) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((788.0000) * transformY) + moveY),

        new createjs.Point(((478.0000) * transformX) + moveX, ((788.3333) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((788.6667) * transformY) + moveY),
        new createjs.Point(((478.0000) * transformX) + moveX, ((789.0000) * transformY) + moveY),

        new createjs.Point(((477.0001) * transformX) + moveX, ((789.0000) * transformY) + moveY),
        new createjs.Point(((475.9999) * transformX) + moveX, ((789.0000) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((789.0000) * transformY) + moveY),

        new createjs.Point(((475.0000) * transformX) + moveX, ((789.3333) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((789.6667) * transformY) + moveY),
        new createjs.Point(((475.0000) * transformX) + moveX, ((790.0000) * transformY) + moveY),

        new createjs.Point(((474.0001) * transformX) + moveX, ((790.0000) * transformY) + moveY),
        new createjs.Point(((472.9999) * transformX) + moveX, ((790.0000) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((790.0000) * transformY) + moveY),

        new createjs.Point(((472.0000) * transformX) + moveX, ((790.3333) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((790.6667) * transformY) + moveY),
        new createjs.Point(((472.0000) * transformX) + moveX, ((791.0000) * transformY) + moveY),

        new createjs.Point(((469.0003) * transformX) + moveX, ((791.6666) * transformY) + moveY),
        new createjs.Point(((465.9997) * transformX) + moveX, ((792.3334) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((793.0000) * transformY) + moveY),

        new createjs.Point(((463.0000) * transformX) + moveX, ((793.3333) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((793.6667) * transformY) + moveY),
        new createjs.Point(((463.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),

        new createjs.Point(((462.3334) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((461.6666) * transformX) + moveX, ((794.0000) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((794.0000) * transformY) + moveY),

        new createjs.Point(((461.0000) * transformX) + moveX, ((794.3333) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((794.6667) * transformY) + moveY),
        new createjs.Point(((461.0000) * transformX) + moveX, ((795.0000) * transformY) + moveY),

        new createjs.Point(((459.0002) * transformX) + moveX, ((795.3333) * transformY) + moveY),
        new createjs.Point(((456.9998) * transformX) + moveX, ((795.6667) * transformY) + moveY),
        new createjs.Point(((455.0000) * transformX) + moveX, ((796.0000) * transformY) + moveY),

        new createjs.Point(((455.0000) * transformX) + moveX, ((796.3333) * transformY) + moveY),
        new createjs.Point(((455.0000) * transformX) + moveX, ((796.6667) * transformY) + moveY),
        new createjs.Point(((455.0000) * transformX) + moveX, ((797.0000) * transformY) + moveY),

        new createjs.Point(((453.3335) * transformX) + moveX, ((797.3333) * transformY) + moveY),
        new createjs.Point(((451.6665) * transformX) + moveX, ((797.6667) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((798.0000) * transformY) + moveY),

        new createjs.Point(((450.0000) * transformX) + moveX, ((798.3333) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((798.6667) * transformY) + moveY),
        new createjs.Point(((450.0000) * transformX) + moveX, ((799.0000) * transformY) + moveY),

        new createjs.Point(((449.3334) * transformX) + moveX, ((799.0000) * transformY) + moveY),
        new createjs.Point(((448.6666) * transformX) + moveX, ((799.0000) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((799.0000) * transformY) + moveY),

        new createjs.Point(((448.0000) * transformX) + moveX, ((799.3333) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((799.6667) * transformY) + moveY),
        new createjs.Point(((448.0000) * transformX) + moveX, ((800.0000) * transformY) + moveY),

        new createjs.Point(((447.0001) * transformX) + moveX, ((800.0000) * transformY) + moveY),
        new createjs.Point(((445.9999) * transformX) + moveX, ((800.0000) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((800.0000) * transformY) + moveY),

        new createjs.Point(((445.0000) * transformX) + moveX, ((800.3333) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((800.6667) * transformY) + moveY),
        new createjs.Point(((445.0000) * transformX) + moveX, ((801.0000) * transformY) + moveY),

        new createjs.Point(((444.3334) * transformX) + moveX, ((801.0000) * transformY) + moveY),
        new createjs.Point(((443.6666) * transformX) + moveX, ((801.0000) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((801.0000) * transformY) + moveY),

        new createjs.Point(((443.0000) * transformX) + moveX, ((801.3333) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((801.6667) * transformY) + moveY),
        new createjs.Point(((443.0000) * transformX) + moveX, ((802.0000) * transformY) + moveY),

        new createjs.Point(((442.0001) * transformX) + moveX, ((802.0000) * transformY) + moveY),
        new createjs.Point(((440.9999) * transformX) + moveX, ((802.0000) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((802.0000) * transformY) + moveY),

        new createjs.Point(((440.0000) * transformX) + moveX, ((802.3333) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((802.6667) * transformY) + moveY),
        new createjs.Point(((440.0000) * transformX) + moveX, ((803.0000) * transformY) + moveY),

        new createjs.Point(((438.6668) * transformX) + moveX, ((803.3333) * transformY) + moveY),
        new createjs.Point(((437.3332) * transformX) + moveX, ((803.6667) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((804.0000) * transformY) + moveY),

        new createjs.Point(((436.0000) * transformX) + moveX, ((804.3333) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((804.6667) * transformY) + moveY),
        new createjs.Point(((436.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),

        new createjs.Point(((435.0001) * transformX) + moveX, ((805.0000) * transformY) + moveY),
        new createjs.Point(((433.9999) * transformX) + moveX, ((805.0000) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((805.0000) * transformY) + moveY),

        new createjs.Point(((433.0000) * transformX) + moveX, ((805.3333) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((805.6667) * transformY) + moveY),
        new createjs.Point(((433.0000) * transformX) + moveX, ((806.0000) * transformY) + moveY),

        new createjs.Point(((431.6668) * transformX) + moveX, ((806.3333) * transformY) + moveY),
        new createjs.Point(((430.3332) * transformX) + moveX, ((806.6667) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((807.0000) * transformY) + moveY),

        new createjs.Point(((429.0000) * transformX) + moveX, ((807.3333) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((807.6667) * transformY) + moveY),
        new createjs.Point(((429.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((428.0001) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((426.9999) * transformX) + moveX, ((808.0000) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((808.0000) * transformY) + moveY),

        new createjs.Point(((426.0000) * transformX) + moveX, ((808.3333) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((808.6667) * transformY) + moveY),
        new createjs.Point(((426.0000) * transformX) + moveX, ((809.0000) * transformY) + moveY),

        new createjs.Point(((424.6668) * transformX) + moveX, ((809.3333) * transformY) + moveY),
        new createjs.Point(((423.3332) * transformX) + moveX, ((809.6667) * transformY) + moveY),
        new createjs.Point(((422.0000) * transformX) + moveX, ((810.0000) * transformY) + moveY),

        new createjs.Point(((422.0000) * transformX) + moveX, ((810.3333) * transformY) + moveY),
        new createjs.Point(((422.0000) * transformX) + moveX, ((810.6667) * transformY) + moveY),
        new createjs.Point(((422.0000) * transformX) + moveX, ((811.0000) * transformY) + moveY),

        new createjs.Point(((421.3334) * transformX) + moveX, ((811.0000) * transformY) + moveY),
        new createjs.Point(((420.6666) * transformX) + moveX, ((811.0000) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((811.0000) * transformY) + moveY),

        new createjs.Point(((420.0000) * transformX) + moveX, ((811.3333) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((811.6667) * transformY) + moveY),
        new createjs.Point(((420.0000) * transformX) + moveX, ((812.0000) * transformY) + moveY),

        new createjs.Point(((419.3334) * transformX) + moveX, ((812.0000) * transformY) + moveY),
        new createjs.Point(((418.6666) * transformX) + moveX, ((812.0000) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((812.0000) * transformY) + moveY),

        new createjs.Point(((418.0000) * transformX) + moveX, ((812.3333) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((812.6667) * transformY) + moveY),
        new createjs.Point(((418.0000) * transformX) + moveX, ((813.0000) * transformY) + moveY),

        new createjs.Point(((417.0001) * transformX) + moveX, ((813.0000) * transformY) + moveY),
        new createjs.Point(((415.9999) * transformX) + moveX, ((813.0000) * transformY) + moveY),
        new createjs.Point(((415.0000) * transformX) + moveX, ((813.0000) * transformY) + moveY),

        new createjs.Point(((415.0000) * transformX) + moveX, ((813.3333) * transformY) + moveY),
        new createjs.Point(((415.0000) * transformX) + moveX, ((813.6667) * transformY) + moveY),
        new createjs.Point(((415.0000) * transformX) + moveX, ((814.0000) * transformY) + moveY),

        new createjs.Point(((413.0002) * transformX) + moveX, ((814.6666) * transformY) + moveY),
        new createjs.Point(((410.9998) * transformX) + moveX, ((815.3334) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((816.0000) * transformY) + moveY),

        new createjs.Point(((409.0000) * transformX) + moveX, ((816.3333) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((816.6667) * transformY) + moveY),
        new createjs.Point(((409.0000) * transformX) + moveX, ((817.0000) * transformY) + moveY),

        new createjs.Point(((408.3334) * transformX) + moveX, ((817.0000) * transformY) + moveY),
        new createjs.Point(((407.6666) * transformX) + moveX, ((817.0000) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((817.0000) * transformY) + moveY),

        new createjs.Point(((407.0000) * transformX) + moveX, ((817.3333) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((817.6667) * transformY) + moveY),
        new createjs.Point(((407.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((406.3334) * transformX) + moveX, ((818.0000) * transformY) + moveY),
        new createjs.Point(((405.6666) * transformX) + moveX, ((818.0000) * transformY) + moveY),
        new createjs.Point(((405.0000) * transformX) + moveX, ((818.0000) * transformY) + moveY),

        new createjs.Point(((405.0000) * transformX) + moveX, ((818.3333) * transformY) + moveY),
        new createjs.Point(((405.0000) * transformX) + moveX, ((818.6667) * transformY) + moveY),
        new createjs.Point(((405.0000) * transformX) + moveX, ((819.0000) * transformY) + moveY),

        new createjs.Point(((404.3334) * transformX) + moveX, ((819.0000) * transformY) + moveY),
        new createjs.Point(((403.6666) * transformX) + moveX, ((819.0000) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((819.0000) * transformY) + moveY),

        new createjs.Point(((403.0000) * transformX) + moveX, ((819.3333) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((819.6667) * transformY) + moveY),
        new createjs.Point(((403.0000) * transformX) + moveX, ((820.0000) * transformY) + moveY),

        new createjs.Point(((401.6668) * transformX) + moveX, ((820.3333) * transformY) + moveY),
        new createjs.Point(((400.3332) * transformX) + moveX, ((820.6667) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((821.0000) * transformY) + moveY),

        new createjs.Point(((399.0000) * transformX) + moveX, ((821.3333) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((821.6667) * transformY) + moveY),
        new createjs.Point(((399.0000) * transformX) + moveX, ((822.0000) * transformY) + moveY),

        new createjs.Point(((397.6668) * transformX) + moveX, ((822.3333) * transformY) + moveY),
        new createjs.Point(((396.3332) * transformX) + moveX, ((822.6667) * transformY) + moveY),
        new createjs.Point(((395.0000) * transformX) + moveX, ((823.0000) * transformY) + moveY),

        new createjs.Point(((395.0000) * transformX) + moveX, ((823.3333) * transformY) + moveY),
        new createjs.Point(((395.0000) * transformX) + moveX, ((823.6667) * transformY) + moveY),
        new createjs.Point(((395.0000) * transformX) + moveX, ((824.0000) * transformY) + moveY),

        new createjs.Point(((394.3334) * transformX) + moveX, ((824.0000) * transformY) + moveY),
        new createjs.Point(((393.6666) * transformX) + moveX, ((824.0000) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((824.0000) * transformY) + moveY),

        new createjs.Point(((393.0000) * transformX) + moveX, ((824.3333) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((824.6667) * transformY) + moveY),
        new createjs.Point(((393.0000) * transformX) + moveX, ((825.0000) * transformY) + moveY),

        new createjs.Point(((392.3334) * transformX) + moveX, ((825.0000) * transformY) + moveY),
        new createjs.Point(((391.6666) * transformX) + moveX, ((825.0000) * transformY) + moveY),
        new createjs.Point(((391.0000) * transformX) + moveX, ((825.0000) * transformY) + moveY),

        new createjs.Point(((391.0000) * transformX) + moveX, ((825.3333) * transformY) + moveY),
        new createjs.Point(((391.0000) * transformX) + moveX, ((825.6667) * transformY) + moveY),
        new createjs.Point(((391.0000) * transformX) + moveX, ((826.0000) * transformY) + moveY),

        new createjs.Point(((390.3334) * transformX) + moveX, ((826.0000) * transformY) + moveY),
        new createjs.Point(((389.6666) * transformX) + moveX, ((826.0000) * transformY) + moveY),
        new createjs.Point(((389.0000) * transformX) + moveX, ((826.0000) * transformY) + moveY),

        new createjs.Point(((389.0000) * transformX) + moveX, ((826.3333) * transformY) + moveY),
        new createjs.Point(((389.0000) * transformX) + moveX, ((826.6667) * transformY) + moveY),
        new createjs.Point(((389.0000) * transformX) + moveX, ((827.0000) * transformY) + moveY),

        new createjs.Point(((388.3334) * transformX) + moveX, ((827.0000) * transformY) + moveY),
        new createjs.Point(((387.6666) * transformX) + moveX, ((827.0000) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((827.0000) * transformY) + moveY),

        new createjs.Point(((387.0000) * transformX) + moveX, ((827.3333) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((827.6667) * transformY) + moveY),
        new createjs.Point(((387.0000) * transformX) + moveX, ((828.0000) * transformY) + moveY),

        new createjs.Point(((385.6668) * transformX) + moveX, ((828.3333) * transformY) + moveY),
        new createjs.Point(((384.3332) * transformX) + moveX, ((828.6667) * transformY) + moveY),
        new createjs.Point(((383.0000) * transformX) + moveX, ((829.0000) * transformY) + moveY),

        new createjs.Point(((382.6667) * transformX) + moveX, ((829.6666) * transformY) + moveY),
        new createjs.Point(((382.3333) * transformX) + moveX, ((830.3334) * transformY) + moveY),
        new createjs.Point(((382.0000) * transformX) + moveX, ((831.0000) * transformY) + moveY),

        new createjs.Point(((381.3334) * transformX) + moveX, ((831.0000) * transformY) + moveY),
        new createjs.Point(((380.6666) * transformX) + moveX, ((831.0000) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((831.0000) * transformY) + moveY),

        new createjs.Point(((380.0000) * transformX) + moveX, ((831.3333) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((831.6667) * transformY) + moveY),
        new createjs.Point(((380.0000) * transformX) + moveX, ((832.0000) * transformY) + moveY),

        new createjs.Point(((379.3334) * transformX) + moveX, ((832.0000) * transformY) + moveY),
        new createjs.Point(((378.6666) * transformX) + moveX, ((832.0000) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((832.0000) * transformY) + moveY),

        new createjs.Point(((378.0000) * transformX) + moveX, ((832.3333) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((832.6667) * transformY) + moveY),
        new createjs.Point(((378.0000) * transformX) + moveX, ((833.0000) * transformY) + moveY),

        new createjs.Point(((377.3334) * transformX) + moveX, ((833.0000) * transformY) + moveY),
        new createjs.Point(((376.6666) * transformX) + moveX, ((833.0000) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((833.0000) * transformY) + moveY),

        new createjs.Point(((376.0000) * transformX) + moveX, ((833.3333) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((833.6667) * transformY) + moveY),
        new createjs.Point(((376.0000) * transformX) + moveX, ((834.0000) * transformY) + moveY),

        new createjs.Point(((375.3334) * transformX) + moveX, ((834.0000) * transformY) + moveY),
        new createjs.Point(((374.6666) * transformX) + moveX, ((834.0000) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((834.0000) * transformY) + moveY),

        new createjs.Point(((374.0000) * transformX) + moveX, ((834.3333) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((834.6667) * transformY) + moveY),
        new createjs.Point(((374.0000) * transformX) + moveX, ((835.0000) * transformY) + moveY),

        new createjs.Point(((373.3334) * transformX) + moveX, ((835.0000) * transformY) + moveY),
        new createjs.Point(((372.6666) * transformX) + moveX, ((835.0000) * transformY) + moveY),
        new createjs.Point(((372.0000) * transformX) + moveX, ((835.0000) * transformY) + moveY),

        new createjs.Point(((371.6667) * transformX) + moveX, ((835.6666) * transformY) + moveY),
        new createjs.Point(((371.3333) * transformX) + moveX, ((836.3334) * transformY) + moveY),
        new createjs.Point(((371.0000) * transformX) + moveX, ((837.0000) * transformY) + moveY),

        new createjs.Point(((369.0002) * transformX) + moveX, ((837.6666) * transformY) + moveY),
        new createjs.Point(((366.9998) * transformX) + moveX, ((838.3334) * transformY) + moveY),
        new createjs.Point(((365.0000) * transformX) + moveX, ((839.0000) * transformY) + moveY),

        new createjs.Point(((364.6667) * transformX) + moveX, ((839.6666) * transformY) + moveY),
        new createjs.Point(((364.3333) * transformX) + moveX, ((840.3334) * transformY) + moveY),
        new createjs.Point(((364.0000) * transformX) + moveX, ((841.0000) * transformY) + moveY),

        new createjs.Point(((362.0002) * transformX) + moveX, ((841.6666) * transformY) + moveY),
        new createjs.Point(((359.9998) * transformX) + moveX, ((842.3334) * transformY) + moveY),
        new createjs.Point(((358.0000) * transformX) + moveX, ((843.0000) * transformY) + moveY),

        new createjs.Point(((357.6667) * transformX) + moveX, ((843.6666) * transformY) + moveY),
        new createjs.Point(((357.3333) * transformX) + moveX, ((844.3334) * transformY) + moveY),
        new createjs.Point(((357.0000) * transformX) + moveX, ((845.0000) * transformY) + moveY),

        new createjs.Point(((355.6668) * transformX) + moveX, ((845.3333) * transformY) + moveY),
        new createjs.Point(((354.3332) * transformX) + moveX, ((845.6667) * transformY) + moveY),
        new createjs.Point(((353.0000) * transformX) + moveX, ((846.0000) * transformY) + moveY),

        new createjs.Point(((352.6667) * transformX) + moveX, ((846.6666) * transformY) + moveY),
        new createjs.Point(((352.3333) * transformX) + moveX, ((847.3334) * transformY) + moveY),
        new createjs.Point(((352.0000) * transformX) + moveX, ((848.0000) * transformY) + moveY),

        new createjs.Point(((350.6668) * transformX) + moveX, ((848.3333) * transformY) + moveY),
        new createjs.Point(((349.3332) * transformX) + moveX, ((848.6667) * transformY) + moveY),
        new createjs.Point(((348.0000) * transformX) + moveX, ((849.0000) * transformY) + moveY),

        new createjs.Point(((347.6667) * transformX) + moveX, ((849.6666) * transformY) + moveY),
        new createjs.Point(((347.3333) * transformX) + moveX, ((850.3334) * transformY) + moveY),
        new createjs.Point(((347.0000) * transformX) + moveX, ((851.0000) * transformY) + moveY),

        new createjs.Point(((345.6668) * transformX) + moveX, ((851.3333) * transformY) + moveY),
        new createjs.Point(((344.3332) * transformX) + moveX, ((851.6667) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((852.0000) * transformY) + moveY),

        new createjs.Point(((343.0000) * transformX) + moveX, ((852.3333) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((852.6667) * transformY) + moveY),
        new createjs.Point(((343.0000) * transformX) + moveX, ((853.0000) * transformY) + moveY),

        new createjs.Point(((342.0001) * transformX) + moveX, ((853.3333) * transformY) + moveY),
        new createjs.Point(((340.9999) * transformX) + moveX, ((853.6667) * transformY) + moveY),
        new createjs.Point(((340.0000) * transformX) + moveX, ((854.0000) * transformY) + moveY),

        new createjs.Point(((339.6667) * transformX) + moveX, ((854.6666) * transformY) + moveY),
        new createjs.Point(((339.3333) * transformX) + moveX, ((855.3334) * transformY) + moveY),
        new createjs.Point(((339.0000) * transformX) + moveX, ((856.0000) * transformY) + moveY),

        new createjs.Point(((337.6668) * transformX) + moveX, ((856.3333) * transformY) + moveY),
        new createjs.Point(((336.3332) * transformX) + moveX, ((856.6667) * transformY) + moveY),
        new createjs.Point(((335.0000) * transformX) + moveX, ((857.0000) * transformY) + moveY),

        new createjs.Point(((334.6667) * transformX) + moveX, ((857.6666) * transformY) + moveY),
        new createjs.Point(((334.3333) * transformX) + moveX, ((858.3334) * transformY) + moveY),
        new createjs.Point(((334.0000) * transformX) + moveX, ((859.0000) * transformY) + moveY),

        new createjs.Point(((333.3334) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((332.6666) * transformX) + moveX, ((859.0000) * transformY) + moveY),
        new createjs.Point(((332.0000) * transformX) + moveX, ((859.0000) * transformY) + moveY),

        new createjs.Point(((331.6667) * transformX) + moveX, ((859.6666) * transformY) + moveY),
        new createjs.Point(((331.3333) * transformX) + moveX, ((860.3334) * transformY) + moveY),
        new createjs.Point(((331.0000) * transformX) + moveX, ((861.0000) * transformY) + moveY),

        new createjs.Point(((330.3334) * transformX) + moveX, ((861.0000) * transformY) + moveY),
        new createjs.Point(((329.6666) * transformX) + moveX, ((861.0000) * transformY) + moveY),
        new createjs.Point(((329.0000) * transformX) + moveX, ((861.0000) * transformY) + moveY),

        new createjs.Point(((328.6667) * transformX) + moveX, ((861.6666) * transformY) + moveY),
        new createjs.Point(((328.3333) * transformX) + moveX, ((862.3334) * transformY) + moveY),
        new createjs.Point(((328.0000) * transformX) + moveX, ((863.0000) * transformY) + moveY),

        new createjs.Point(((327.3334) * transformX) + moveX, ((863.0000) * transformY) + moveY),
        new createjs.Point(((326.6666) * transformX) + moveX, ((863.0000) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((863.0000) * transformY) + moveY),

        new createjs.Point(((326.0000) * transformX) + moveX, ((863.3333) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((863.6667) * transformY) + moveY),
        new createjs.Point(((326.0000) * transformX) + moveX, ((864.0000) * transformY) + moveY),

        new createjs.Point(((325.0001) * transformX) + moveX, ((864.3333) * transformY) + moveY),
        new createjs.Point(((323.9999) * transformX) + moveX, ((864.6667) * transformY) + moveY),
        new createjs.Point(((323.0000) * transformX) + moveX, ((865.0000) * transformY) + moveY),

        new createjs.Point(((322.6667) * transformX) + moveX, ((865.6666) * transformY) + moveY),
        new createjs.Point(((322.3333) * transformX) + moveX, ((866.3334) * transformY) + moveY),
        new createjs.Point(((322.0000) * transformX) + moveX, ((867.0000) * transformY) + moveY),

        new createjs.Point(((321.3334) * transformX) + moveX, ((867.0000) * transformY) + moveY),
        new createjs.Point(((320.6666) * transformX) + moveX, ((867.0000) * transformY) + moveY),
        new createjs.Point(((320.0000) * transformX) + moveX, ((867.0000) * transformY) + moveY),

        new createjs.Point(((319.6667) * transformX) + moveX, ((867.6666) * transformY) + moveY),
        new createjs.Point(((319.3333) * transformX) + moveX, ((868.3334) * transformY) + moveY),
        new createjs.Point(((319.0000) * transformX) + moveX, ((869.0000) * transformY) + moveY),

        new createjs.Point(((318.3334) * transformX) + moveX, ((869.0000) * transformY) + moveY),
        new createjs.Point(((317.6666) * transformX) + moveX, ((869.0000) * transformY) + moveY),
        new createjs.Point(((317.0000) * transformX) + moveX, ((869.0000) * transformY) + moveY),

        new createjs.Point(((316.6667) * transformX) + moveX, ((869.6666) * transformY) + moveY),
        new createjs.Point(((316.3333) * transformX) + moveX, ((870.3334) * transformY) + moveY),
        new createjs.Point(((316.0000) * transformX) + moveX, ((871.0000) * transformY) + moveY),

        new createjs.Point(((315.3334) * transformX) + moveX, ((871.0000) * transformY) + moveY),
        new createjs.Point(((314.6666) * transformX) + moveX, ((871.0000) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((871.0000) * transformY) + moveY),

        new createjs.Point(((314.0000) * transformX) + moveX, ((871.3333) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((871.6667) * transformY) + moveY),
        new createjs.Point(((314.0000) * transformX) + moveX, ((872.0000) * transformY) + moveY),

        new createjs.Point(((313.0001) * transformX) + moveX, ((872.3333) * transformY) + moveY),
        new createjs.Point(((311.9999) * transformX) + moveX, ((872.6667) * transformY) + moveY),
        new createjs.Point(((311.0000) * transformX) + moveX, ((873.0000) * transformY) + moveY),

        new createjs.Point(((310.6667) * transformX) + moveX, ((873.6666) * transformY) + moveY),
        new createjs.Point(((310.3333) * transformX) + moveX, ((874.3334) * transformY) + moveY),
        new createjs.Point(((310.0000) * transformX) + moveX, ((875.0000) * transformY) + moveY),

        new createjs.Point(((309.3334) * transformX) + moveX, ((875.0000) * transformY) + moveY),
        new createjs.Point(((308.6666) * transformX) + moveX, ((875.0000) * transformY) + moveY),
        new createjs.Point(((308.0000) * transformX) + moveX, ((875.0000) * transformY) + moveY),

        new createjs.Point(((307.3334) * transformX) + moveX, ((875.9999) * transformY) + moveY),
        new createjs.Point(((306.6666) * transformX) + moveX, ((877.0001) * transformY) + moveY),
        new createjs.Point(((306.0000) * transformX) + moveX, ((878.0000) * transformY) + moveY),

        new createjs.Point(((305.3334) * transformX) + moveX, ((878.0000) * transformY) + moveY),
        new createjs.Point(((304.6666) * transformX) + moveX, ((878.0000) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((878.0000) * transformY) + moveY),

        new createjs.Point(((304.0000) * transformX) + moveX, ((878.3333) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((878.6667) * transformY) + moveY),
        new createjs.Point(((304.0000) * transformX) + moveX, ((879.0000) * transformY) + moveY),

        new createjs.Point(((303.0001) * transformX) + moveX, ((879.3333) * transformY) + moveY),
        new createjs.Point(((301.9999) * transformX) + moveX, ((879.6667) * transformY) + moveY),
        new createjs.Point(((301.0000) * transformX) + moveX, ((880.0000) * transformY) + moveY),

        new createjs.Point(((300.6667) * transformX) + moveX, ((880.6666) * transformY) + moveY),
        new createjs.Point(((300.3333) * transformX) + moveX, ((881.3334) * transformY) + moveY),
        new createjs.Point(((300.0000) * transformX) + moveX, ((882.0000) * transformY) + moveY),

        new createjs.Point(((299.3334) * transformX) + moveX, ((882.0000) * transformY) + moveY),
        new createjs.Point(((298.6666) * transformX) + moveX, ((882.0000) * transformY) + moveY),
        new createjs.Point(((298.0000) * transformX) + moveX, ((882.0000) * transformY) + moveY),

        new createjs.Point(((297.6667) * transformX) + moveX, ((882.6666) * transformY) + moveY),
        new createjs.Point(((297.3333) * transformX) + moveX, ((883.3334) * transformY) + moveY),
        new createjs.Point(((297.0000) * transformX) + moveX, ((884.0000) * transformY) + moveY),

        new createjs.Point(((296.0001) * transformX) + moveX, ((884.3333) * transformY) + moveY),
        new createjs.Point(((294.9999) * transformX) + moveX, ((884.6667) * transformY) + moveY),
        new createjs.Point(((294.0000) * transformX) + moveX, ((885.0000) * transformY) + moveY),

        new createjs.Point(((293.3334) * transformX) + moveX, ((885.9999) * transformY) + moveY),
        new createjs.Point(((292.6666) * transformX) + moveX, ((887.0001) * transformY) + moveY),
        new createjs.Point(((292.0000) * transformX) + moveX, ((888.0000) * transformY) + moveY),

        new createjs.Point(((291.3334) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((290.6666) * transformX) + moveX, ((888.0000) * transformY) + moveY),
        new createjs.Point(((290.0000) * transformX) + moveX, ((888.0000) * transformY) + moveY),

        new createjs.Point(((289.3334) * transformX) + moveX, ((888.9999) * transformY) + moveY),
        new createjs.Point(((288.6666) * transformX) + moveX, ((890.0001) * transformY) + moveY),
        new createjs.Point(((288.0000) * transformX) + moveX, ((891.0000) * transformY) + moveY),

        new createjs.Point(((287.3334) * transformX) + moveX, ((891.0000) * transformY) + moveY),
        new createjs.Point(((286.6666) * transformX) + moveX, ((891.0000) * transformY) + moveY),
        new createjs.Point(((286.0000) * transformX) + moveX, ((891.0000) * transformY) + moveY),

        new createjs.Point(((285.3334) * transformX) + moveX, ((891.9999) * transformY) + moveY),
        new createjs.Point(((284.6666) * transformX) + moveX, ((893.0001) * transformY) + moveY),
        new createjs.Point(((284.0000) * transformX) + moveX, ((894.0000) * transformY) + moveY),

        new createjs.Point(((283.3334) * transformX) + moveX, ((894.0000) * transformY) + moveY),
        new createjs.Point(((282.6666) * transformX) + moveX, ((894.0000) * transformY) + moveY),
        new createjs.Point(((282.0000) * transformX) + moveX, ((894.0000) * transformY) + moveY),

        new createjs.Point(((281.3334) * transformX) + moveX, ((894.9999) * transformY) + moveY),
        new createjs.Point(((280.6666) * transformX) + moveX, ((896.0001) * transformY) + moveY),
        new createjs.Point(((280.0000) * transformX) + moveX, ((897.0000) * transformY) + moveY),

        new createjs.Point(((279.3334) * transformX) + moveX, ((897.0000) * transformY) + moveY),
        new createjs.Point(((278.6666) * transformX) + moveX, ((897.0000) * transformY) + moveY),
        new createjs.Point(((278.0000) * transformX) + moveX, ((897.0000) * transformY) + moveY),

        new createjs.Point(((277.0001) * transformX) + moveX, ((898.3332) * transformY) + moveY),
        new createjs.Point(((275.9999) * transformX) + moveX, ((899.6668) * transformY) + moveY),
        new createjs.Point(((275.0000) * transformX) + moveX, ((901.0000) * transformY) + moveY),

        new createjs.Point(((274.0001) * transformX) + moveX, ((901.3333) * transformY) + moveY),
        new createjs.Point(((272.9999) * transformX) + moveX, ((901.6667) * transformY) + moveY),
        new createjs.Point(((272.0000) * transformX) + moveX, ((902.0000) * transformY) + moveY),

        new createjs.Point(((271.6667) * transformX) + moveX, ((902.6666) * transformY) + moveY),
        new createjs.Point(((271.3333) * transformX) + moveX, ((903.3334) * transformY) + moveY),
        new createjs.Point(((271.0000) * transformX) + moveX, ((904.0000) * transformY) + moveY),

        new createjs.Point(((270.3334) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((269.6666) * transformX) + moveX, ((904.0000) * transformY) + moveY),
        new createjs.Point(((269.0000) * transformX) + moveX, ((904.0000) * transformY) + moveY),

        new createjs.Point(((268.6667) * transformX) + moveX, ((904.6666) * transformY) + moveY),
        new createjs.Point(((268.3333) * transformX) + moveX, ((905.3334) * transformY) + moveY),
        new createjs.Point(((268.0000) * transformX) + moveX, ((906.0000) * transformY) + moveY),

        new createjs.Point(((267.6667) * transformX) + moveX, ((906.0000) * transformY) + moveY),
        new createjs.Point(((267.3333) * transformX) + moveX, ((906.0000) * transformY) + moveY),
        new createjs.Point(((267.0000) * transformX) + moveX, ((906.0000) * transformY) + moveY),

        new createjs.Point(((266.6667) * transformX) + moveX, ((906.6666) * transformY) + moveY),
        new createjs.Point(((266.3333) * transformX) + moveX, ((907.3334) * transformY) + moveY),
        new createjs.Point(((266.0000) * transformX) + moveX, ((908.0000) * transformY) + moveY),

        new createjs.Point(((265.3334) * transformX) + moveX, ((908.0000) * transformY) + moveY),
        new createjs.Point(((264.6666) * transformX) + moveX, ((908.0000) * transformY) + moveY),
        new createjs.Point(((264.0000) * transformX) + moveX, ((908.0000) * transformY) + moveY),

        new createjs.Point(((262.6668) * transformX) + moveX, ((909.6665) * transformY) + moveY),
        new createjs.Point(((261.3332) * transformX) + moveX, ((911.3335) * transformY) + moveY),
        new createjs.Point(((260.0000) * transformX) + moveX, ((913.0000) * transformY) + moveY),

        new createjs.Point(((259.3334) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((258.6666) * transformX) + moveX, ((913.0000) * transformY) + moveY),
        new createjs.Point(((258.0000) * transformX) + moveX, ((913.0000) * transformY) + moveY),

        new createjs.Point(((257.3334) * transformX) + moveX, ((913.9999) * transformY) + moveY),
        new createjs.Point(((256.6666) * transformX) + moveX, ((915.0001) * transformY) + moveY),
        new createjs.Point(((256.0000) * transformX) + moveX, ((916.0000) * transformY) + moveY),

        new createjs.Point(((255.6667) * transformX) + moveX, ((916.0000) * transformY) + moveY),
        new createjs.Point(((255.3333) * transformX) + moveX, ((916.0000) * transformY) + moveY),
        new createjs.Point(((255.0000) * transformX) + moveX, ((916.0000) * transformY) + moveY),

        new createjs.Point(((254.6667) * transformX) + moveX, ((916.6666) * transformY) + moveY),
        new createjs.Point(((254.3333) * transformX) + moveX, ((917.3334) * transformY) + moveY),
        new createjs.Point(((254.0000) * transformX) + moveX, ((918.0000) * transformY) + moveY),

        new createjs.Point(((253.3334) * transformX) + moveX, ((918.0000) * transformY) + moveY),
        new createjs.Point(((252.6666) * transformX) + moveX, ((918.0000) * transformY) + moveY),
        new createjs.Point(((252.0000) * transformX) + moveX, ((918.0000) * transformY) + moveY),

        new createjs.Point(((250.3335) * transformX) + moveX, ((919.9998) * transformY) + moveY),
        new createjs.Point(((248.6665) * transformX) + moveX, ((922.0002) * transformY) + moveY),
        new createjs.Point(((247.0000) * transformX) + moveX, ((924.0000) * transformY) + moveY),

        new createjs.Point(((246.3334) * transformX) + moveX, ((924.0000) * transformY) + moveY),
        new createjs.Point(((245.6666) * transformX) + moveX, ((924.0000) * transformY) + moveY),
        new createjs.Point(((245.0000) * transformX) + moveX, ((924.0000) * transformY) + moveY),

        new createjs.Point(((243.0002) * transformX) + moveX, ((926.3331) * transformY) + moveY),
        new createjs.Point(((240.9998) * transformX) + moveX, ((928.6669) * transformY) + moveY),
        new createjs.Point(((239.0000) * transformX) + moveX, ((931.0000) * transformY) + moveY),

        new createjs.Point(((238.3334) * transformX) + moveX, ((931.0000) * transformY) + moveY),
        new createjs.Point(((237.6666) * transformX) + moveX, ((931.0000) * transformY) + moveY),
        new createjs.Point(((237.0000) * transformX) + moveX, ((931.0000) * transformY) + moveY),

        new createjs.Point(((234.3336) * transformX) + moveX, ((933.9997) * transformY) + moveY),
        new createjs.Point(((231.6664) * transformX) + moveX, ((937.0003) * transformY) + moveY),
        new createjs.Point(((229.0000) * transformX) + moveX, ((940.0000) * transformY) + moveY),

        new createjs.Point(((228.3334) * transformX) + moveX, ((940.0000) * transformY) + moveY),
        new createjs.Point(((227.6666) * transformX) + moveX, ((940.0000) * transformY) + moveY),
        new createjs.Point(((227.0000) * transformX) + moveX, ((940.0000) * transformY) + moveY),

        new createjs.Point(((225.3335) * transformX) + moveX, ((941.9998) * transformY) + moveY),
        new createjs.Point(((223.6665) * transformX) + moveX, ((944.0002) * transformY) + moveY),
        new createjs.Point(((222.0000) * transformX) + moveX, ((946.0000) * transformY) + moveY),

        new createjs.Point(((217.0005) * transformX) + moveX, ((950.6662) * transformY) + moveY),
        new createjs.Point(((211.9995) * transformX) + moveX, ((955.3338) * transformY) + moveY),
        new createjs.Point(((207.0000) * transformX) + moveX, ((960.0000) * transformY) + moveY),

        new createjs.Point(((203.2573) * transformX) + moveX, ((963.7431) * transformY) + moveY),
        new createjs.Point(((196.0993) * transformX) + moveX, ((973.6029) * transformY) + moveY),
        new createjs.Point(((191.0000) * transformX) + moveX, ((975.0000) * transformY) + moveY),

        new createjs.Point(endX, endY)
    ];

    var points2 = [
        new createjs.Point(startX2, startY2),
        new createjs.Point(endX2, endY2)
    ];
    var points1Final = points;

    var points2Final = points2;
    var motionPaths = [],
        motionPathsFinal = [];
    var motionPath = getMotionPathFromPoints(points);
    //console.log(motionPath);
    var motionPath2 = getMotionPathFromPoints(points2);
    motionPaths.push(motionPath, motionPath2);
    motionPathsFinal.push(getMotionPathFromPoints(points1Final), getMotionPathFromPoints(points2Final));
    (lib.pen = function() {
        this.initialize(img.pen);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.icons = function() {
        this.initialize(img.icons);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);

        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Talet 6", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("19", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Vi räknar tillsammans: 1, 2.", "16px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550, 400);

        this.addChild(this.hintText1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.EndCharacter = function() {
        this.initialize();
        thisStage = this;
        var barFull = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };
        for (var m = 0; m < motionPathsFinal.length; m++) {

            for (var i = 2; i < motionPathsFinal[m].length; i += 2) {
                var motionPathTemp = motionPathsFinal[m];
                //motionPath[i].x, motionPath[i].y
                var round = new cjs.Shape();
                round.graphics
                    .setStrokeStyle(10, 'round', 'round')
                    .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
                    .curveTo(motionPathTemp[i - 2], motionPathTemp[i - 1], motionPathTemp[i], motionPathTemp[i + 1])
                    .endStroke();
                thisStage.addChild(round);

            };
        }
        // for (var i = 2; i < motionPath2.length; i += 2) {

        //     //motionPath[i].x, motionPath[i].y
        //     var round = new cjs.Shape();
        //     round.graphics
        //         .setStrokeStyle(10, 'round', 'round')
        //         .beginStroke("#000") //.moveTo(bar.oldx, bar.oldy).lineTo(bar.x, bar.y)
        //         .curveTo(motionPath2[i - 2], motionPath2[i - 1], motionPath2[i], motionPath2[i + 1])
        //         .endStroke();
        //     thisStage.addChild(round);

        // };
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    var currentMotionStep = 1;
    // stage content:
    (lib.drawPoints = function(mode, startPosition, loop) {
        loop = false;
        this.initialize(mode, startPosition, loop, {
            "start": 0,
            "end": 1
        }, true);
        thisStage = this;
        thisStage.pencil = new lib.pen();
        thisStage.pencil.regY = 0;
        thisStage.pencil.setTransform(0, 0, 0.8, 0.8);

        thisStage.tempElements = [];

        thisStage.addChild(thisStage.pencil);

        this.timeline.addTween(cjs.Tween.get(bar).wait(20).to({
            guide: {
                path: motionPath
            }
        }, 60)).on('change', (function(event) {

            if (currentMotionStep == 1) {
                bar = drawCharacter(thisStage, bar, true, event);
            }
        }));

        this.timeline.addTween(cjs.Tween.get(bar2).wait(40).to({
            guide: {
                path: motionPath2
            }
        }, 20).wait(50).call(function() {
            gotoLastAndStop(Stage1);
            Stage1.shape.visible = false;
        })).on('change', (function(event) {
            if (currentMotionStep == 2) {
                bar2 = drawCharacter(thisStage, bar2, true, event);
            }
        }));



    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-126.9, 130, 123, 123);
    var currentStepChanged = false;

    function drawCharacter(thisStage, thisbar, isVisible, event) {
        //console.log(currentMotionStep)
        var oldStep = currentMotionStep;
        if (currentStepChanged) {
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = false;
        }
        if (thisbar.x == endX && thisbar.y == endY) {
            currentMotionStep = 2;
            thisbar.x = startX2;
            thisbar.y = startY2;
            thisbar.oldx = startX2;
            thisbar.oldy = startY2;
            currentStepChanged = true;
        }
        if (thisbar.x == endX2 && thisbar.y == endY2) {
            currentMotionStep = 1;
            thisbar.oldx = startX;
            thisbar.oldy = startY;
            thisbar.x = startX;
            thisbar.y = startY;
        }
        if (oldStep == currentMotionStep) {
            if (thisbar.x === startX && thisbar.y === startY) {
                thisbar.oldx = startX;
                thisbar.oldy = startY;
                thisbar.x = startX;
                thisbar.y = startY;
            }
            if ((thisbar.x === startX && thisbar.y === startY) || (thisbar.x === endX2 && thisbar.y === endY2)) {
                //thisStage.timeline.stop();
                for (var i = 0; i < thisStage.tempElements.length; i++) {
                    var e = thisStage.tempElements[i];

                    if (e) {
                        e.object.visible = false;
                        //thisStage.tempElements.pop(e);
                        thisStage.removeChild(e.object)
                    }
                }
                thisStage.tempElements = [];
                //thisStage.timeline.play();
            }

            var round = new cjs.Shape();

            round.graphics
                .setStrokeStyle(10, 'round', 'round')
                .beginStroke("#000") //.moveTo(thisbar.oldx, thisbar.oldy).lineTo(thisbar.x, thisbar.y)
                .curveTo(thisbar.oldx, thisbar.oldy, thisbar.x, thisbar.y)
                .endStroke();
            round.visible = isVisible;
            thisbar.oldx = thisbar.x;
            thisbar.oldy = thisbar.y;

            if (thisbar.x === endX && thisbar.y === endY) {
                thisbar.x = startX2;
                thisbar.y = startY2;
                thisbar.oldx = thisbar.x;
                thisbar.oldy = thisbar.y;
            } else {
                thisStage.addChild(round);
            }
            
            if (thisbar.x == 561.592 && thisbar.y == 270.34223434142854) { // finished plotting 1/3 line
                Stage1.shape.visible = false;
            } else if (thisbar.x == startX && thisbar.y == startY) { // plot 1st point
                Stage1.shape.visible = true;
            }

            thisStage.pencil.x = thisbar.x, thisStage.pencil.y = thisbar.y - 145;
            ////console.log(thisStage.pencil.x, thisStage.pencil.y)
            thisStage.removeChild(thisStage.pencil);
            thisStage.addChild(thisStage.pencil);
            pencil = this.pencil;

            // thisStage.addChild(round);
            thisStage.tempElements.push({
                "object": round,
                "expired": false
            });
        }

        return thisbar;
    }

    var Stage1;
    (lib.Stage1 = function() {
        this.initialize();
        var thisStage = this;
        Stage1 = this;
        thisStage.buttonShadow = new cjs.Shadow("#000000", 0, 0, 2);
        // var measuredFramerate=createjs.Ticker.getMeasureFPS();

        thisStage.rectangle = new createjs.Shape();
        thisStage.rectangle.graphics.f('').s('#00B4EA').ss(1.5).drawRect(0, 0, 68 * 5, 68 * 5.32)
        thisStage.rectangle.setTransform(450 + 30, 0);

        thisStage.shape = new createjs.Shape();
        thisStage.shape.graphics.beginFill("#ff00ff").drawCircle(startX, startY, 15);
        thisStage.circleShadow = new cjs.Shadow("#ff0000", 0, 0, 5);
        thisStage.shape.shadow = thisStage.circleShadow;
        thisStage.circleShadow.blur = 0;
        createjs.Tween.get(thisStage.circleShadow).to({
            blur: 50
        }, 500).wait(100).to({
            blur: 0
        }, 500).to({
            blur: 50
        }, 500).wait(10).to({
            blur: 5
        }, 500);
        var data = {
            images: [img.icons],
            frames: {
                width: 40,
                height: 40
            },
            animations: {
                trash: 0,
                male: 1,
                wait: 2,
                library: 3,
                female: 4,
                hanger: 5,
                stairs: 6,
                noparking: 7
            }
        }

        var spriteSheet = new createjs.SpriteSheet(data);
        thisStage.previous = new createjs.Sprite(spriteSheet);
        thisStage.previous.x = 98 + 450;
        thisStage.previous.y = 68 * 3.2 + 5 + 145;
        thisStage.previous.shadow = thisStage.buttonShadow.clone();

        thisStage.pause = thisStage.previous.clone();
        thisStage.pause.gotoAndStop(1);
        thisStage.pause.x = 98 + 450 + 44;
        thisStage.pause.y = 68 * 3.2 + 5 + 145;
        thisStage.pause.shadow = thisStage.buttonShadow.clone();

        thisStage.play = thisStage.previous.clone();
        thisStage.play.gotoAndStop(3);
        thisStage.play.x = 98 + 450 + 44;
        thisStage.play.y = 68 * 3.2 + 5 + 145;
        thisStage.play.visible = false;
        thisStage.play.shadow = thisStage.buttonShadow.clone();

        thisStage.next = thisStage.previous.clone();
        thisStage.next.gotoAndStop(2);
        thisStage.next.x = 98 + 450 + 44 * 2;
        thisStage.next.y = 68 * 3.2 + 5 + 145;
        thisStage.next.shadow = thisStage.buttonShadow.clone();
        thisStage.currentSpeed = 100;

        thisStage.speed = thisStage.previous.clone();
        thisStage.speed.gotoAndStop(4);
        thisStage.speed.setTransform(98 + 450 + 44 * 3, 68 * 3.2 + 5 + 145, 1.8, 1)
        thisStage.speed.shadow = thisStage.buttonShadow.clone();

        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF");
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145)

        var bar = {
            x: startX,
            y: startY,
            oldx: startX,
            oldy: startY
        };

        thisStage.tempElements = [];

        this.addChild(this.rectangle, thisStage.shape, thisStage.previousRect, thisStage.previousText, thisStage.previous, thisStage.pause, thisStage.play, thisStage.next, thisStage.speed, thisStage.speedText);
        this.endCharacter = new lib.EndCharacter();

        thisStage.movie = new lib.drawPoints();
        thisStage.movie.setTransform(0, 0);
        this.addChild(thisStage.movie);

        createjs.Tween.get(bar).setPaused(false);
        thisStage.paused = false;
        thisStage.pause.addEventListener("click", function(evt) {
            pause();
        });
        thisStage.play.addEventListener("click", function(evt) {
            thisStage.removeChild(thisStage.endCharacter);
            thisStage.play.visible = false;
            thisStage.pause.visible = true;
            thisStage.paused = false;
            thisStage.movie.play();
        });
        thisStage.previous.addEventListener("click", function(evt) {
            Stage1.shape.visible = true;
            gotoFirst(thisStage);
        });
        thisStage.next.addEventListener("click", function(evt) {
            gotoLast(thisStage);
            Stage1.shape.visible = false;
        });

        thisStage.speed.addEventListener("click", function(evt) {
            modifySpeed(thisStage);

        });
        pause();
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    function pause() {
        Stage1.removeChild(Stage1.endCharacter);
        Stage1.pause.visible = false;
        Stage1.play.visible = true;
        Stage1.paused = true;
        Stage1.movie.stop();
    }

    function gotoFirst(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = false;
        thisStage.pause.visible = true;
        thisStage.paused = false;

        thisStage.movie.gotoAndStop("start");
        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];
            if (e) {
                e.object.visible = false;
                //thisStage.tempElements.pop(e);
                thisStage.movie.removeChild(e.object)
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.movie.gotoAndPlay("start");
        currentMotionStep = 1;
        // bar.x=startX;
        // bar.y=startY;
        // bar.oldx=startX;
        // bar.oldy=startY;
        // thisStage.movie.gotoAndPlay("start");
    }


    function gotoLast(thisStage) {
        if (pencil) {
            pencil.visible = false;
            pencil.parent.removeChild(pencil);
        }
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);

        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        // thisStage.pencil=thisStage.movie.pencil;
        // thisStage.addChild(thisStage.pencil);
        if (thisStage.pencil) {
            thisStage.removeChild(thisStage.pencil);
        }
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function gotoLastAndStop(thisStage) {
        thisStage.removeChild(thisStage.endCharacter);
        thisStage.play.visible = true;
        thisStage.pause.visible = false;
        thisStage.movie.gotoAndStop(0);
        thisStage.movie.pencil.x = motionPath[motionPath.length - 2];
        thisStage.movie.pencil.y = motionPath[motionPath.length - 1] - 145;
        thisStage.movie.removeChild(thisStage.movie.pencil);


        for (var i = 0; i < thisStage.movie.tempElements.length; i++) {
            var e = thisStage.movie.tempElements[i];

            if (e) {
                e.object.visible = false;
                thisStage.movie.removeChild(e.object);
            }
        }
        thisStage.movie.tempElements = [];
        thisStage.addChild(thisStage.endCharacter);
        thisStage.removeChild(this.movie);
        thisStage.pencil = thisStage.movie.pencil;
        thisStage.addChild(thisStage.pencil);
        currentMotionStep = 1;
        //thisStage.addChild(this.movie);
        //thisStage.movie.addChild(thisStage.movie.pencil);
    }

    function modifySpeed(thisStage) {
        thisStage.removeChild(thisStage.speedText);
        if (thisStage.currentSpeed == 20) {
            thisStage.currentSpeed = 100;
        } else {
            thisStage.currentSpeed = thisStage.currentSpeed - 20;
        }
        createjs.Ticker.setFPS(thisStage.currentSpeed * lib.properties.fps / 100);
        thisStage.speedText = new cjs.Text("Hastighet: \n" + thisStage.currentSpeed + "%", "12px 'Myriad Pro'", "#FFFFFF")
        thisStage.speedText.textAlign = "center";
        thisStage.speedText.lineHeight = 15;
        thisStage.speedText.setTransform(98 + 442.5 + 44 * 4, 66 * 3.2 + 15 + 2 + 145);
        thisStage.addChild(thisStage.speedText);
    }
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
