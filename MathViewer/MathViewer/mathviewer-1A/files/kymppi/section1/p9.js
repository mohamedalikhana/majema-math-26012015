(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p9_1.png",
            id: "p9_1"
        }, {
            src: "images/p9_2.png",
            id: "p9_2"
        }]
    };

    var decodePath = function(str) {
        var instructions = [cjs.Graphics._ctx.moveTo, cjs.Graphics._ctx.lineTo, cjs.Graphics._ctx.quadraticCurveTo, cjs.Graphics._ctx.bezierCurveTo, cjs.Graphics._ctx.closePath];
        var paramCount = [2, 2, 4, 6, 0];
        var i = 0,
            l = str.length;
        var params = [];
        var x = 0,
            y = 0;
        var base64 = cjs.Graphics.BASE_64;

        while (i < l) {
            var c = str.charAt(i);
            var n = base64[c];
            var fi = n >> 3; // highest order bits 1-3 code for operation.
            var f = instructions[fi];
            // check that we have a valid instruction & that the unused bits are empty:
            if (!f || (n & 3)) {
                throw ("bad path data (@" + i + "): " + c);
            }
            var pl = paramCount[fi];
            if (!fi) {
                x = y = 0;
            } // move operations reset the position.
            params.length = 0;
            i++;
            var charCount = (n >> 2 & 1) + 2; // 4th header bit indicates number size for this operation.
            for (var p = 0; p < pl; p++) {
                var num = base64[str.charAt(i)];
                var sign = (num >> 5) ? -1 : 1;
                num = ((num & 31) << 6) | (base64[str.charAt(i + 1)]);
                if (charCount == 3) {
                    num = (num << 6) | (base64[str.charAt(i + 2)]);
                }
                num = sign * num / 10;
                if (p % 2) {
                    x = (num += x);
                } else {
                    y = (num += y);
                }
                params[p] = num;
                i += charCount;
            }
            //f.apply(this,params);
            //console.log(f + "" + params)
        }
        return this;
    };

    // symbols:

    (lib.p9_1 = function() {
        this.initialize(img.p9_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 963, 307);


    (lib.p9_2 = function() {
        this.initialize(img.p9_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 955, 389);


    (lib.Symbol24 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_1 = new cjs.Text("Ringa in lika många som i bilden.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 22;
        this.text_1.setTransform(0, 4.1);


        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_1
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 330.6, 24.7);


    (lib.Symbol22 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text = new cjs.Text(".", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(309.6, 0);

        this.text_1 = new cjs.Text("=", "bold 16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(297.6, 0);

        this.text_2 = new cjs.Text("är ", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(297.6, 0);

        this.text_3 = new cjs.Text("är lika med ", "bold 16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(214.2, 0);

        this.text_4 = new cjs.Text("eller ", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(126.2, 0);

        this.text_5 = new cjs.Text("lika många ", "bold 16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(99.9, 0);

        this.text_6 = new cjs.Text("Symbolen för ", "16px 'Myriad Pro'");
        this.text_6.lineHeight = 19;

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_6
            }, {
                t: this.text_5
            }, {
                t: this.text_4
            }, {
                t: this.text_3
            }, {
                t: this.text_2
            }, {
                t: this.text_1
            }, {
                t: this.text
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 319.8, 24.7);


    (lib.Symbol19 = function() {
        this.initialize();

        decodePath("AAAmkIAANJ");

        // Layer 1
        this.text = new cjs.Text("Lika många – är lika med", "24px 'MyriadPro-Semibold'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(105.5, 23);

        this.text_1 = new cjs.Text("2", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(67, 21);

        this.text_2 = new cjs.Text("9", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 15;
        this.text_2.setTransform(558, 648);

        this.pageBottomText = new cjs.Text("förstå och kunna använda symbolen =", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(392, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(48, 23.5, 1.1, 1);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A5C0").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.instance = new lib.p9_2();
        this.instance.setTransform(0, 167.4, 0.64, 0.64);

        this.addChild(this.instance, this.shape_1, this.shape, this.pageBottomText, this.text_2, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);



    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p9_1();
        this.instance.setTransform(13.6, 53.9, 0.5, 0.5);

        this.instance_1 = new lib.Symbol24();
        this.instance_1.setTransform(181.6, 12.8, 1, 1, 0, 0, 0, 162.6, 12.4);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 20;
        this.text.setTransform(0, 3);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4m2QgHAAgLACQgVAEgSAIQg5AdAABHIAAKKIACARQAEAWAIARQAdA5BHAAMAjxAAAQAcAAAdgOQA4gdAAhGIAAqKQAAgdgNgcQgdg5hHAAg");
        this.shape.setTransform(386.4, 74.3);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("Ax4G3QhHAAgdg5QgIgRgEgWIgBgRIAAqKQAAhHA4gdQASgIAWgEQAKgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAdIAAKKQAABGg5AdQgcAOgdAAg");
        this.shape_1.setTransform(386.4, 74.3);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4m2QgHAAgLACQgVAEgSAIQg5AdAABHIAAKKIACARQAEAWAIARQAdA5BHAAMAjxAAAQAdAAAcgOQA5gdAAhGIAAqKQAAgdgPgcQgcg5hHAAg");
        this.shape_2.setTransform(128.4, 74.3);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("Ax4G3QhHAAgdg5QgIgRgDgWIgCgRIAAqKQAAhHA4gdQASgIAVgEQALgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAdIAAKKQAABGg5AdQgcAOgdAAg");
        this.shape_3.setTransform(128.4, 74.3);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4m2QgHAAgLACQgVAEgSAIQg5AdAABHIAAKKIACARQAEAWAIASQAdA4BHAAMAjxAAAQAdAAAcgOQA5gcAAhHIAAqKQAAgdgPgcQgcg5hHAAg");
        this.shape_4.setTransform(128.4, 169.5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("Ax4G3QhHAAgdg4QgIgTgDgVIgCgSIAAqJQAAhHA4gdQASgJAVgDQALgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAdIAAKJQAABIg5AcQgcAOgdAAg");
        this.shape_5.setTransform(128.4, 169.5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("Ax4m2QgHAAgLACQgVAEgSAIQg5AdAABHIAAKKIACARQAEAWAIASQAdA4BHAAMAjxAAAQAcAAAdgOQA4gcAAhHIAAqKQAAgdgNgcQgdg5hHAAg");
        this.shape_6.setTransform(386.4, 169.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("Ax4G3QhHAAgdg4QgIgTgEgVIgBgSIAAqJQAAhHA4gdQASgJAWgDQAKgCAHAAMAjxAAAQBHAAAcA5QAPAcAAAdIAAKJQAABIg5AcQgcAOgdAAg");
        this.shape_7.setTransform(386.4, 169.5);

        this.addChild(this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text, this.instance_1, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 214.4);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.Symbol22();
        this.instance.setTransform(179.2, 15.4, 1, 1, 0, 0, 0, 159.9, 12.4);

        this.text = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(387.9, 109.9);

        this.text_1 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(364.8, 109.9);

        this.text_2 = new cjs.Text("6", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(343.2, 109.9);

        this.text_3 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(134.9, 109.9);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(111.2, 109.9);

        this.text_5 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(88.7, 109.9);

        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape.setTransform(445.1, 80.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_1.setTransform(445.1, 80.6);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape_2.setTransform(343.8, 94.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_3.setTransform(343.8, 94.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgcAcAAAlQAAAnAcAbQAcAcAlAAg");
        this.shape_4.setTransform(458.1, 60.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAcgcAlAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_5.setTransform(458.1, 60.6);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgcAcAAAlQAAAnAcAbQAcAcAlAAg");
        this.shape_6.setTransform(343.8, 67.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAcgcAlAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_7.setTransform(343.8, 67.6);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape_8.setTransform(162.2, 71.8);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#C51930").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_9.setTransform(162.2, 71.8);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAcgbAAgnQAAglgcgcQgcgcgmAAQgmAAgbAcQgcAcAAAlQAAAnAcAbQAbAcAmAAg");
        this.shape_10.setTransform(90, 89.6);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#C51930").s().p("AhBBCQgcgbAAgnQAAglAcgcQAbgcAmAAQAmAAAcAcQAcAcAAAlQAAAngcAbQgcAcgmAAQgmAAgbgcg");
        this.shape_11.setTransform(90, 89.6);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape_12.setTransform(429.8, 99);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_13.setTransform(429.8, 99);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAmAcAcQAcAcAlAAg");
        this.shape_14.setTransform(315.4, 94.6);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQglAAgcgcg");
        this.shape_15.setTransform(315.4, 94.6);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgcAcAAAlQAAAnAcAbQAcAcAlAAg");
        this.shape_16.setTransform(429.8, 60.6);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAcgcAlAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_17.setTransform(429.8, 60.6);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgcAcAAAlQAAAnAcAbQAcAcAlAAg");
        this.shape_18.setTransform(315.4, 67.6);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAcgcAlAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_19.setTransform(315.4, 67.6);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAgmgcgbQgbgcgnAAQglAAgcAcQgcAbAAAmQAAAnAcAbQAcAcAlAAg");
        this.shape_20.setTransform(176.8, 94.1);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#C51930").s().p("AhBBCQgcgbAAgnQAAgmAcgbQAcgcAlAAQAnAAAbAcQAcAbAAAmQAAAngcAbQgbAcgnAAQglAAgcgcg");
        this.shape_21.setTransform(176.8, 94.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAcgbAAgnQAAglgcgcQgcgcgmAAQgmAAgbAcQgcAcAAAlQAAAnAcAbQAbAcAmAAg");
        this.shape_22.setTransform(64.6, 89.6);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#C51930").s().p("AhBBCQgcgbAAgnQAAglAcgcQAbgcAmAAQAmAAAcAcQAcAcAAAlQAAAngcAbQgcAcgmAAQgmAAgbgcg");
        this.shape_23.setTransform(64.6, 89.6);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAcgcAAgmQAAgmgcgbQgcgcgmAAQgmAAgbAcQgcAbAAAmQAAAmAcAcQAbAcAmAAg");
        this.shape_24.setTransform(414, 80.6);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAbgcAmAAQAmAAAcAcQAcAbAAAmQAAAmgcAcQgcAcgmAAQgmAAgbgcg");
        this.shape_25.setTransform(414, 80.6);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgcAAgmQAAgmgcgbQgbgcgnAAQgmAAgbAcQgcAbAAAmQAAAmAcAcQAbAcAmAAg");
        this.shape_26.setTransform(285.7, 94.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#0089CA").s().p("AhBBCQgcgcAAgmQAAgmAcgbQAbgcAmAAQAnAAAbAcQAcAbAAAmQAAAmgcAcQgbAcgnAAQgmAAgbgcg");
        this.shape_27.setTransform(285.7, 94.6);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAmAAAcgcQAcgbAAgnQAAglgcgcQgcgcgmAAQgmAAgbAcQgcAcAAAlQAAAnAcAbQAbAcAmAAg");
        this.shape_28.setTransform(400, 60.6);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAbgcAmAAQAmAAAcAcQAcAcAAAlQAAAngcAbQgcAcgmAAQgmAAgbgcg");
        this.shape_29.setTransform(400, 60.6);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQgmAAgbAcQgcAcAAAlQAAAnAcAbQAbAcAmAAg");
        this.shape_30.setTransform(285.7, 67.6);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#0089CA").s().p("AhBBCQgcgbAAgnQAAglAcgcQAbgcAmAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQgmAAgbgcg");
        this.shape_31.setTransform(285.7, 67.6);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAgmgcgbQgbgcgnAAQgmAAgbAcQgcAbAAAmQAAAnAcAbQAbAcAmAAg");
        this.shape_32.setTransform(147.1, 94.1);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#C51930").s().p("AhBBCQgcgbAAgnQAAgmAcgbQAbgcAmAAQAnAAAbAcQAcAbAAAmQAAAngcAbQgbAcgnAAQgmAAgbgcg");
        this.shape_33.setTransform(147.1, 94.1);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#000000").ss(0.5, 0, 0, 4).p("AAABeQAnAAAbgcQAcgbAAgnQAAglgcgcQgbgcgnAAQglAAgcAcQgbAcAAAlQAAAnAbAbQAcAcAlAAg");
        this.shape_34.setTransform(40.9, 89.6);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#C51930").s().p("AhBBCQgbgbAAgnQAAglAbgcQAbgcAmAAQAnAAAbAcQAcAcAAAlQAAAngcAbQgbAcgnAAQgmAAgbgcg");
        this.shape_35.setTransform(40.9, 89.6);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_36.setTransform(185.9, 37.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_37.setTransform(171.1, 37.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_38.setTransform(155.9, 37.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_39.setTransform(140.3, 37.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_40.setTransform(124.9, 37.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_41.setTransform(109.2, 37.4);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_42.setTransform(93.2, 37.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_43.setTransform(77.7, 37.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_44.setTransform(62.2, 37.3);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_45.setTransform(46.6, 37.3);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_46.setTransform(31.2, 37.3);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_47.setTransform(187.2, 43.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_48.setTransform(187.2, 43.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_49.setTransform(172.5, 43.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_50.setTransform(172.5, 43.9);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_51.setTransform(94.5, 43.9);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_52.setTransform(94.5, 43.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_53.setTransform(157.4, 43.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_54.setTransform(157.4, 43.9);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_55.setTransform(79.3, 43.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_56.setTransform(79.3, 43.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_57.setTransform(141.7, 43.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_58.setTransform(141.7, 43.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_59.setTransform(63.6, 43.9);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_60.setTransform(63.6, 43.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_61.setTransform(126.2, 43.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_62.setTransform(126.2, 43.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_63.setTransform(48.2, 43.9);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_64.setTransform(48.2, 43.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_65.setTransform(110.5, 43.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_66.setTransform(110.5, 43.9);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_67.setTransform(32.5, 43.9);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_68.setTransform(32.5, 43.9);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AtKnnIgcAHQgcANAAAkIAANfIAHAcQAOAdAjAAIaUAAIAdgHQAcgOAAgkIAAtfQAAgOgHgOQgOgcgkAAg");
        this.shape_69.setTransform(111.1, 84.9);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f("#FFFFFF").s().p("AtKHpQgjgBgOgbIgHgdIAAtfQAAgkAcgOIAcgHIaUAAQAkAAAOAdQAHAOAAAOIAANfQAAAkgcANIgdAIg");
        this.shape_70.setTransform(111.1, 84.9);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_71.setTransform(473.2, 37.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_72.setTransform(427.9, 37.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_73.setTransform(458.5, 37.4);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_74.setTransform(413.2, 37.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_75.setTransform(443.3, 37.4);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_76.setTransform(398, 37.4);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_77.setTransform(382.4, 37.4);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_78.setTransform(367, 37.4);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_79.setTransform(351.3, 37.4);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_80.setTransform(335.3, 37.4);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_81.setTransform(319.8, 37.3);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_82.setTransform(304.3, 37.3);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_83.setTransform(288.7, 37.3);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_84.setTransform(273.3, 37.3);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_85.setTransform(474.6, 43.9);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_86.setTransform(474.6, 43.9);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_87.setTransform(429.3, 43.9);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_88.setTransform(429.3, 43.9);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_89.setTransform(459.8, 43.9);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_90.setTransform(459.8, 43.9);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_91.setTransform(414.6, 43.9);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_92.setTransform(414.6, 43.9);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_93.setTransform(336.5, 43.9);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_94.setTransform(336.5, 43.9);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_95.setTransform(444.7, 43.9);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_96.setTransform(444.7, 43.9);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_97.setTransform(399.4, 43.9);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_98.setTransform(399.4, 43.9);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_99.setTransform(321.4, 43.9);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_100.setTransform(321.4, 43.9);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_101.setTransform(383.7, 43.9);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_102.setTransform(383.7, 43.9);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_103.setTransform(305.7, 43.9);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_104.setTransform(305.7, 43.9);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_105.setTransform(368.2, 43.9);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_106.setTransform(368.2, 43.9);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_107.setTransform(290.2, 43.9);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_108.setTransform(290.2, 43.9);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_109.setTransform(352.6, 43.9);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_110.setTransform(352.6, 43.9);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_111.setTransform(274.5, 43.9);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_112.setTransform(274.5, 43.9);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AwXnnQgOAAgPAHQgcANAAAkIAANfIAHAcQAPAdAjAAMAgvAAAIAdgHQAcgOAAgkIAAtfQAAgOgHgOQgOgcgkAAg");
        this.shape_113.setTransform(373.8, 84.9);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.f("#FFFFFF").s().p("AwXHpQgjgBgPgbIgHgdIAAtfQAAgkAcgOQAPgGAOgBMAgwAAAQAjAAAOAdQAHAOAAAOIAANfQAAAkgcANIgcAIg");
        this.shape_114.setTransform(373.8, 84.9);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.f().s("#FFF173").ss(1.5, 0, 0, 4).p("Egl/gLDQgdAAgcAOQg5AdAABHIAASjIACARQAEAWAIASQAdA5BHAAMBL/AAAIASgCQAVgEASgIQA5gdAAhHIAAyjQAAgdgOgcQgdg5hHAAg");
        this.shape_115.setTransform(254.6, 70.8);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f("#FFF173").s().p("Egl/ALEQhHAAgdg5QgIgSgEgWIgCgRIAAyjQAAhHA5gcQAcgPAdAAMBL/AAAQBHAAAdA6QAOAbAAAdIAASjQAABHg5AcQgSAJgVADIgSADg");
        this.shape_116.setTransform(254.6, 70.8);

        this.addChild(this.shape_116, this.shape_115, this.shape_114, this.shape_113, this.shape_112, this.shape_111, this.shape_110, this.shape_109, this.shape_108, this.shape_107, this.shape_106, this.shape_105, this.shape_104, this.shape_103, this.shape_102, this.shape_101, this.shape_100, this.shape_99, this.shape_98, this.shape_97, this.shape_96, this.shape_95, this.shape_94, this.shape_93, this.shape_92, this.shape_91, this.shape_90, this.shape_89, this.shape_88, this.shape_87, this.shape_86, this.shape_85, this.shape_84, this.shape_83, this.shape_82, this.shape_81, this.shape_80, this.shape_79, this.shape_78, this.shape_77, this.shape_76, this.shape_75, this.shape_74, this.shape_73, this.shape_72, this.shape_71, this.shape_70, this.shape_69, this.shape_68, this.shape_67, this.shape_66, this.shape_65, this.shape_64, this.shape_63, this.shape_62, this.shape_61, this.shape_60, this.shape_59, this.shape_58, this.shape_57, this.shape_56, this.shape_55, this.shape_54, this.shape_53, this.shape_52, this.shape_51, this.shape_50, this.shape_49, this.shape_48, this.shape_47, this.shape_46, this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.shape_40, this.shape_39, this.shape_38, this.shape_37, this.shape_36, this.shape_35, this.shape_34, this.shape_33, this.shape_32, this.shape_31, this.shape_30, this.shape_29, this.shape_28, this.shape_27, this.shape_26, this.shape_25, this.shape_24, this.shape_23, this.shape_22, this.shape_21, this.shape_20, this.shape_19, this.shape_18, this.shape_17, this.shape_16, this.shape_15, this.shape_14, this.shape_13, this.shape_12, this.shape_11, this.shape_10, this.shape_9, this.shape_8, this.shape_7, this.shape_6, this.shape_5, this.shape_4, this.shape_3, this.shape_2, this.shape_1, this.shape, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1, this.text, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 511.3, 143.6);


    // stage content:
    (lib.p9 = function() {
        this.initialize();

        // Layer 1
        this.v2 = new lib.Symbol2();
        this.v2.setTransform(309.8, 528, 1, 1, 0, 0, 0, 256.3, 106.8);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(310.1, 129, 1, 1, 0, 0, 0, 254.6, 70.8);

        this.other = new lib.Symbol19();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.addChild(this.other, this.v1, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
