(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
          
        }]
    }; 

    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1      

        this.text_1 = new cjs.Text("106", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(33, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D6326A").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.dotMatrixRect = function() {
        this.initialize();

        var width = 252,
            height = 160;
        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('').s("#7d7d7d").ss(0.8).drawRoundRect(0, 0, width, height, 10);
        this.rect1.setTransform(0, 0);

        var ToBeAdded = [];
        var colSpace = 28.3;
        var rowSpace = 28.5;
        var startX = 28;
        var startY = 36;
        var dotCount = 0;
        for (var col = 0; col < 8; col++) {

            for (var row = 0; row < 5; row++) {

                var dot = null;
                dot = new cjs.Shape();
                dot.graphics.f('#878787').ss().s().drawCircle(0, 0, 1.25);
                dot.setTransform(startX + (col * colSpace), startY + (row * rowSpace));
                dot.id = dotCount;
                dotCount = dotCount + 1;

                ToBeAdded.push(dot);
            }
        }

        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);

        }
        this.dots = ToBeAdded;
        this.addChild(this.rect1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 600, 160);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text(" Följ instruktionen och rita en triangel.", "16px 'Myriad Pro'");
        this.text_1.setTransform(14, 0);

        this.text_2 = new cjs.Text("1 av triangelns sidor är 3 cm.", "15px 'Myriad Pro'");
        this.text_2.setTransform(37, 32);

        this.text_3 = new cjs.Text("1 av triangelns sidor är 5 cm.", "15px 'Myriad Pro'");
        this.text_3.setTransform(292, 32);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(0, 13);

        this.rectimg_1 = this.rectimg.clone(true);
        this.rectimg_1.setTransform(256, 13);


        this.addChild(this.text, this.text_1, this.rectimg, this.rectimg_1, this.text_2, this.text_3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 519.3, 170);


    (lib.Symbol3 = function() {

        this.initialize();

        this.text = new cjs.Text(" Följ instruktionen och rita en fyrhörning.", "16px 'Myriad Pro'");
        this.text.setTransform(14, 0);

        this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("1 av fyrhörningens sidor är 4 cm.", "15px 'Myriad Pro'");
        this.text_2.setTransform(37, 32);

        this.text_3 = new cjs.Text("2 av fyrhörningens sidor är 3 cm.", "15px 'Myriad Pro'");
        this.text_3.setTransform(292, 32);

        this.rectimg = new lib.dotMatrixRect();
        this.rectimg.setTransform(0, 13);

        this.rectimg_1 = this.rectimg.clone(true);
        this.rectimg_1.setTransform(256, 13);

        this.addChild(this.text, this.text_1, this.rectimg, this.rectimg_1, this.text_2, this.text_3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 170);

    (lib.Symbol4 = function() {

        this.initialize();

        this.text = new cjs.Text(" Hitta mönstret. Rita och måla 3 former till.", "16px 'Myriad Pro'");
        this.text.setTransform(14, 0);

        this.text_1 = new cjs.Text("6.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 508, 59, 10);
        this.roundRect1.setTransform(0, 12);

        //first row

        this.triangle = new cjs.Shape();
        this.triangle.graphics.f("#13A538").s("#706F6F").ss(1.2).moveTo(20, 44).lineTo(51, 44).lineTo(35, 18).lineTo(20, 44);
        this.triangle.setTransform(0, 10);

        this.circle = new cjs.Shape();
        this.circle.graphics.f("#FFF374").s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 13, 0, 2 * Math.PI);
        this.circle.setTransform(75, 41);

        this.circle1 = this.circle.clone(true);
        this.circle1.setTransform(113, 41);

        this.triangle1 = this.triangle.clone(true);
        this.triangle1.setTransform(115, 10);

        this.circle2 = this.circle.clone(true);
        this.circle2.setTransform(189, 41);

        this.circle3 = this.circle.clone(true);
        this.circle3.setTransform(227, 41);

        this.triangle2 = this.triangle.clone(true);
        this.triangle2.setTransform(230, 10);

        this.circle4 = this.circle.clone(true);
        this.circle4.setTransform(306, 41);

        this.Rectsquare = new cjs.Shape();
        this.Rectsquare.graphics.f("#FFFFFF").s('#706F6F').drawRoundRect(0, 0, 45, 40, 10);
        this.Rectsquare.setTransform(339, 22);

        this.Rectsquare1 = this.Rectsquare.clone(true);
        this.Rectsquare1.setTransform(393, 22);

        this.Rectsquare2 = this.Rectsquare.clone(true);
        this.Rectsquare2.setTransform(445, 22);

        this.addChild(this.text, this.text_1, this.roundRect1, this.roundRect2, this.instance, this.instance_1,
            this.triangle, this.triangle1, this.triangle2, this.circle, this.circle1, this.circle2, this.circle3, this.circle4,
            this.Rectsquare, this.Rectsquare1, this.Rectsquare2);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f('').s("#7D7D7D").ss(0.7).drawRoundRect(0, 0, 508, 59, 10);
        this.roundRect2.setTransform(0, 79);  

        //2nd row     

        this.circle_1 = new cjs.Shape();
        this.circle_1.graphics.f("#FFF374").s("#706F6F").ss(0.8, 0, 0, 4).arc(0, 0, 13, 0, 2 * Math.PI);
        this.circle_1.setTransform(31, 112);

        this.triangle_1 = new cjs.Shape();
        this.triangle_1.graphics.f("#D51317").s("#706F6F").ss(1.2).moveTo(20, 44).lineTo(51, 44).lineTo(35, 18).lineTo(20, 44);
        this.triangle_1.setTransform(33, 80);

        this.square_1 = new cjs.Shape();
        this.square_1.graphics.f("#008BD2").s('#7d7d7d').drawRect(0, 0, 27, 27, 0);
        this.square_1.setTransform(97, 98);

        this.Rectsquare_1 = new cjs.Shape();
        this.Rectsquare_1.graphics.f("#FFFFFF").s('#706F6F').drawRoundRect(0, 0, 45, 40, 10);
        this.Rectsquare_1.setTransform(337, 90);

        this.circle_2 = this.circle_1.clone(true);
        this.circle_2.setTransform(145, 112);

        this.triangle_2 = this.triangle_1.clone(true);
        this.triangle_2.setTransform(145, 80);

        this.square_2 = this.square_1.clone(true);
        this.square_2.setTransform(209, 98);

        this.circle_3 = this.circle_1.clone(true);
        this.circle_3.setTransform(263, 112);

        this.triangle_3 = this.triangle_1.clone(true);
        this.triangle_3.setTransform(265, 80);

        this.Rectsquare_2 = this.Rectsquare_1.clone(true);
        this.Rectsquare_2.setTransform(392, 90);

        this.Rectsquare_3 = this.Rectsquare_1.clone(true);
        this.Rectsquare_3.setTransform(445, 90);

        this.addChild(this.roundRect2, this.instance_1, this.triangle_1, this.triangle_2, this.triangle_3, this.circle_1, this.circle_2, this.circle_3,
            this.square_1, this.square_2, this.Rectsquare_1, this.Rectsquare_2, this.Rectsquare_3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 519.3, 137);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(295, 120, 1, 1, 0, 0, 0, 255.8, 38);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(295, 335, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(295, 552, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.other, this.v1, this.v2, this.v3);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
