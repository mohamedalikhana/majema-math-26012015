(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p51_1.png",
            id: "p51_1"
        }, {
            src: "images/p51_2.png",
            id: "p51_2"
        }]
    };

    (lib.p51_1 = function() {
        this.initialize(img.p51_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 350, 350);

    (lib.p51_2 = function() {
        this.initialize(img.p51_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);


    (lib.Symbol1 = function() {
        this.initialize();


        this.pageTitle = new cjs.Text("Vi skriver subtraktion", "24px 'MyriadPro-Semibold'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(110.5, 24.5);

        this.text_1 = new cjs.Text("17", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(62.5, 21.6);

        this.text_2 = new cjs.Text("51", "12px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 18;
        this.text_2.setTransform(555, 648);

        this.pageBottomText = new cjs.Text("kunna lösa uppgifter med subtraktion 0 till 6", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'right';
        this.pageBottomText.setTransform(370, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#88C13F").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(50, 23.5, 1.15, 1);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#88C13F").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_2.setTransform(0, 0);

        this.addChild(this.pageTitle, this.shape_2, this.shape_1, this.shape, this.text_1, this.text_2,
            this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p51_1();
        this.instance.setTransform(43, 63, 0.2, 0.2);

        this.label1 = new cjs.Text("Det är 3 fåglar.", "16px 'Myriad Pro'");
        this.label1.lineHeight = 34;
        this.label1.setTransform(40, 33);

        this.label5 = new cjs.Text("1 flyger iväg.", "16px 'Myriad Pro'");
        this.label5.lineHeight = 34;
        this.label5.setTransform(40, 54);

        this.label6 = new cjs.Text("2 stannar kvar.", "16px 'Myriad Pro'");
        this.label6.lineHeight = 34;
        this.label6.setTransform(40, 72);

        this.label2 = new cjs.Text("Det är 4 kulor.", "16px 'Myriad Pro'");
        this.label2.lineHeight = 34;
        this.label2.setTransform(308, 33);

        this.label7 = new cjs.Text("3 tas bort.", "16px 'Myriad Pro'");
        this.label7.lineHeight = 34;
        this.label7.setTransform(308, 54);

        this.label8 = new cjs.Text("1 är kvar.", "16px 'Myriad Pro'");
        this.label8.lineHeight = 34;
        this.label8.setTransform(308, 72);

        this.label3 = new cjs.Text("3 minus 1 är lika med 2.", "16px 'Myriad Pro'");
        this.label3.lineHeight = 34;
        this.label3.setTransform(40, 167);
        this.label4 = new cjs.Text("4 minus 3 är lika med 1.", "16px 'Myriad Pro'");
        this.label4.lineHeight = 34;
        this.label4.setTransform(308, 167);
        var textToBeDisplayed1 = ['3', '–', '1', '=', '2'];

        this.textBoxGroup1 = new cjs.Shape();

        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(53 + (i * 20), 140, 20, 22);
            this.labelTemp1 = new cjs.Text(textToBeDisplayed1[i], "bold 35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp1.lineHeight = 34;
            this.labelTemp1.setTransform(55 + (i * 20), 131);
            textToBeDisplayed1[i] = this.labelTemp1;
        }
        this.textBoxGroup1.setTransform(0, 0);

        var textToBeDisplayed2 = ['4', '–', '3', '=', '1'];
        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(317 + (i * 20), 140, 20, 22);
            this.labelTemp2 = new cjs.Text(textToBeDisplayed2[i], "bold 35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp2.lineHeight = 34;
            this.labelTemp2.setTransform(320 + (i * 20), 131);
            textToBeDisplayed2[i] = this.labelTemp2;
        }
        this.textBoxGroup2.setTransform(0, 0);
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#fff779").s('#7d7d7d').drawRoundRect(0, 0, ((1038) - 20) * 0.50, ((321) - 0) * 0.50, 10);
        this.roundRect1.setTransform(0, 27);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#DB2128").s("#616161").ss(0.5, 0, 0, 4).arc(0, 0, 10.7, 0, 2 * Math.PI);
        this.shape_1.setTransform(325, 118);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#DB2128").s("#616161").ss(0.5, 0, 0, 4).arc(0, 0, 10.7, 0, 2 * Math.PI);
        this.shape_2.setTransform(352.5, 118);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#DB2128").s("#616161").ss(0.5, 0, 0, 4).arc(0, 0, 10.7, 0, 2 * Math.PI);
        this.shape_3.setTransform(379.5, 118);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#DB2128").s("#616161").ss(0.5, 0, 0, 4).arc(0, 0, 10.7, 0, 2 * Math.PI);
        this.shape_4.setTransform(406.5, 118);

        this.line_1 = new cjs.Shape();
        this.line_1.graphics.s("#616161").ss(1.5).moveTo(8.7 * 2, -1).lineTo(3, 10.7 * 2 + 7);
        this.line_1.setTransform(352.5 - 10.7, 118 - 10.7 - 3);

        this.line_2 = new cjs.Shape();
        this.line_2.graphics.s("#616161").ss(1.5).moveTo(8.7 * 2, -1).lineTo(3, 10.7 * 2 + 7);
        this.line_2.setTransform(379.5 - 10.7, 118 - 10.7 - 3);

        this.line_3 = new cjs.Shape();
        this.line_3.graphics.s("#616161").ss(1.5).moveTo(8.7 * 2, -1).lineTo(3, 10.7 * 2 + 7);
        this.line_3.setTransform(406.5 - 10.7, 118 - 10.7 - 3);


        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.textBoxGroup1,
            this.textBoxGroup2, this.label1, this.label2, this.label3, this.label4, this.shape_1, this.shape_2, this.shape_3,
            this.shape_4, this.label5, this.label6, this.label7, this.label8, this.line_1, this.line_2, this.line_3);
        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed1[i]);
        }
        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed2[i]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75);


    (lib.Symbol3 = function() {
        this.initialize();
        this.instance = new lib.p51_2();
        this.instance.setTransform(0, 26, 0.38, 0.38);
        this.text = new cjs.Text("1 av fåglarna flyger iväg. Hur många stannar kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 6);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(0, 6);

        var textToBeDisplayed1 = ['2', '–', '1', '=', ' '];
        this.textBoxGroup1 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 122, 20, 23);
            this.labelTemp1 = new cjs.Text(textToBeDisplayed1[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp1.lineHeight = 40;
            this.labelTemp1.setTransform(78 + (i * 20), 112.5);
            textToBeDisplayed1[i] = this.labelTemp1;
        }
        this.textBoxGroup1.setTransform(0, 0);


        var textToBeDisplayed2 = ['3', '–', '1', '=', ' '];
        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 122, 20, 23);
            this.labelTemp2 = new cjs.Text(textToBeDisplayed2[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp2.lineHeight = 40;
            this.labelTemp2.setTransform(338 + (i * 20), 112.5);
            textToBeDisplayed2[i] = this.labelTemp2;
        }
        this.textBoxGroup2.setTransform(0, 0);


        var textToBeDisplayed3 = ['4', '–', '1', '=', ' '];
        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 251.5, 20, 23);
            this.labelTemp3 = new cjs.Text(textToBeDisplayed3[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp3.lineHeight = 40;
            this.labelTemp3.setTransform(78 + (i * 20), 242.5);
            textToBeDisplayed3[i] = this.labelTemp3;
        }
        this.textBoxGroup3.setTransform(0, 0);


        var textToBeDisplayed4 = ['1', '–', '1', '=', ' '];
        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 251.5, 20, 23);
            this.labelTemp4 = new cjs.Text(textToBeDisplayed4[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp4.lineHeight = 40;
            this.labelTemp4.setTransform(338 + (i * 20), 242.5);
            textToBeDisplayed4[i] = this.labelTemp4;
        }
        this.textBoxGroup4.setTransform(0, 0);

        var textToBeDisplayed5 = ['5', '–', '1', '=', ' '];
        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 381, 20, 23);
            this.labelTemp5 = new cjs.Text(textToBeDisplayed5[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp5.lineHeight = 40;
            this.labelTemp5.setTransform(78 + (i * 20), 372);
            textToBeDisplayed5[i] = this.labelTemp5;
        }
        this.textBoxGroup5.setTransform(0, 0);


        var textToBeDisplayed6 = ['3', '–', '1', '=', ' '];
        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 381, 20, 23);
            this.labelTemp6 = new cjs.Text(textToBeDisplayed6[i], "35px 'UusiTekstausMajema'", "#6C6D70");
            this.labelTemp6.lineHeight = 40;
            this.labelTemp6.setTransform(338 + (i * 20), 372);
            textToBeDisplayed6[i] = this.labelTemp6;
        }
        this.textBoxGroup6.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((1044 * 0.5) - 10) * 0.49, ((789 * (1 / 3)) - 15) * 0.49, 10);
        this.roundRect1.setTransform(3, 31);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1044 * 0.5) + 8) * 0.49, 31);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 160);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1044 * 0.5) + 8) * 0.49, 160);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(3, 289);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((1044 * 0.5) + 8) * 0.49, 289);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5,
            this.roundRect6, this.instance, this.text, this.text_1, this.textBoxGroup1,
            this.textBoxGroup2, this.textBoxGroup3, this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6);
        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed1[i]);
        }

        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed2[i]);
        }

        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed3[i]);
        }

        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed4[i]);
        }

        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed5[i]);
        }

        for (var i = 0; i < 5; i++) {
            this.addChild(textToBeDisplayed6[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);



    (lib.p51 = function() {
        this.initialize();

        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(313, 264, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(313, 83, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v2, this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
