(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p148_1.png",
            id: "p148_1"
        }]
    };

    // symbols:
    (lib.p148_1 = function() {
        this.initialize(img.p148_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("148", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(35, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("Lista ut meddelandet.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#D83770");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 510, 275, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 4; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                var fcolor = (row == 3) ? "#F5D5DA" : "#ffffff";

                this.textbox_group1.graphics.f(fcolor).s("#D83770").ss(0.8).drawRect(55 + (columnSpace * 28), 41.5 + (rowSpace * 28), 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;

                this.textbox_group2.graphics.f('#F5D5DA').s("#D83770").ss(0.8).drawRect(21 + (columnSpace * 28), 41.5 + (rowSpace * 28), 34, 28);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        var arrxPos = ['13', '140', '264'];
        this.textbox_group3 = new cjs.Shape();
        for (var i = 0; i < arrxPos.length; i++) {
            var xPos = parseInt(arrxPos[i]);
            for (var column = 0; column < 6; column++) {
                if ((i == 0 && column > 2) || (i == 1 && column > 2)) {
                    continue;
                }
                var columnSpace = column;
                this.textbox_group3.graphics.f("#ffffff").s("#949599").ss(0.8).drawRect(xPos + (columnSpace * 33.5), 200, 19.5, 22);
            }
        }
        this.textbox_group3.setTransform(0, 0);

        this.textbox_group4 = new cjs.Shape();
        for (var column = 0; column < 11; column++) {
            var columnSpace = column;
            this.textbox_group4.graphics.f("#ffffff").s("#949599").ss(0.8).drawRect(13 + (columnSpace * 33.5), 263, 19.5, 22);
        }
        this.textbox_group4.setTransform(0, 0);

        //row-1
        this.label1 = new cjs.Text("H", "16px 'Myriad Pro'");
        this.label1.lineHeight = 28;
        this.label1.setTransform(62, 49);
        this.label2 = new cjs.Text("N", "16px 'Myriad Pro'");
        this.label2.lineHeight = 28;
        this.label2.setTransform(90, 49);
        this.label3 = new cjs.Text("P", "16px 'Myriad Pro'");
        this.label3.lineHeight = 28;
        this.label3.setTransform(118, 49);
        this.label4 = new cjs.Text("T", "16px 'Myriad Pro'");
        this.label4.lineHeight = 28;
        this.label4.setTransform(147, 49);
        //row-2
        this.label5 = new cjs.Text("V", "16px 'Myriad Pro'");
        this.label5.lineHeight = 28;
        this.label5.setTransform(62, 76);
        this.label6 = new cjs.Text("O", "16px 'Myriad Pro'");
        this.label6.lineHeight = 28;
        this.label6.setTransform(90, 76);
        this.label7 = new cjs.Text("M", "16px 'Myriad Pro'");
        this.label7.lineHeight = 28;
        this.label7.setTransform(118, 76);
        this.label8 = new cjs.Text("A", "16px 'Myriad Pro'");
        this.label8.lineHeight = 28;
        this.label8.setTransform(147, 76);
        //row-3
        this.label9 = new cjs.Text("K", "16px 'Myriad Pro'");
        this.label9.lineHeight = 28;
        this.label9.setTransform(62, 104);
        this.label10 = new cjs.Text("R", "16px 'Myriad Pro'");
        this.label10.lineHeight = 28;
        this.label10.setTransform(90, 104);
        this.label11 = new cjs.Text("L", "16px 'Myriad Pro'");
        this.label11.lineHeight = 28;
        this.label11.setTransform(118, 104);
        this.label12 = new cjs.Text("E", "16px 'Myriad Pro'");
        this.label12.lineHeight = 28;
        this.label12.setTransform(147, 104);
        //row-4
        this.label13 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.label13.lineHeight = 28;
        this.label13.setTransform(62, 132.1);
        this.label14 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.label14.lineHeight = 28;
        this.label14.setTransform(90, 132.1);
        this.label15 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label15.lineHeight = 28;
        this.label15.setTransform(118, 132.1);
        this.label16 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label16.lineHeight = 28;
        this.label16.setTransform(147, 132.1);

        this.shape_tri1 = new cjs.Shape(); // yellow triangle
        this.shape_tri1.graphics.s("#000000").f("#FFF459").ss(0.8).moveTo(29, 64).lineTo(48, 64).lineTo(38.5, 47).lineTo(29, 64);
        this.shape_tri1.setTransform(0, 0);
        this.shape_sqr1 = new cjs.Shape(); // red square
        this.shape_sqr1.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(29, 74.8).lineTo(46.5, 74.8).lineTo(46.5, 92.3).lineTo(29, 92.3).lineTo(29, 74.8);
        this.shape_sqr1.setTransform(0, 0);
        this.shape_circ1 = new cjs.Shape(); // blue circle
        this.shape_circ1.graphics.f("#1CB0E7").s("#000000").ss(0.8, 0, 0, 4).arc(38, 112, 9, 0, 2 * Math.PI);
        this.shape_circ1.setTransform(0, 0);

        var ToBeAdded = [];
        var tri_xPos = ['122', '252', '420', '-2', '66', '100'];
        var tri_yPos = ['145', '145', '145', '208', '208', '208'];

        for (var i = 0; i < tri_xPos.length; i++) {
            var xPos = tri_xPos[i];
            var yPos = tri_yPos[i];

            var tempshape_tri = this.shape_tri1.clone(true);
            tempshape_tri.setTransform(xPos, yPos, 0.75, 0.75);
            ToBeAdded.push(tempshape_tri);
        }

        var sqr_xPos = ['-2', '66', '159', '287', '384', '134', '234', '300'];
        var sqr_yPos = ['124', '124', '124', '124', '124', '188', '188', '188'];

        for (var i = 0; i < sqr_xPos.length; i++) {
            var xPos = sqr_xPos[i];
            var yPos = sqr_yPos[i];

            var tempshape_sqr = this.shape_sqr1.clone(true);
            tempshape_sqr.setTransform(xPos, yPos, 0.75, 0.75);
            ToBeAdded.push(tempshape_sqr);
        }

        var circ_xPos = ['33.5', '195', '318', '352', '33.5', '166', '200', '267', '336'];
        var circ_yPos = ['102.5', '102.5', '102.5', '102.5', '166.5', '166.5', '166.5', '166.5', '166.5'];

        for (var i = 0; i < circ_xPos.length; i++) {
            var xPos = circ_xPos[i];
            var yPos = circ_yPos[i];

            var tempshape_circ = this.shape_circ1.clone(true);
            tempshape_circ.setTransform(xPos, yPos, 0.75, 0.75);
            ToBeAdded.push(tempshape_circ);
        }
        //row-1
        this.label17 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.label17.lineHeight = 28;
        this.label17.setTransform(7.5, 178);
        this.label18 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.label18.lineHeight = 28;
        this.label18.setTransform(131, 178);
        this.label19 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label19.lineHeight = 28;
        this.label19.setTransform(43, 178);
        this.label20 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label20.lineHeight = 28;
        this.label20.setTransform(168, 178);
        this.label21 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label21.lineHeight = 28;
        this.label21.setTransform(295, 178);
        this.label22 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label22.lineHeight = 28;
        this.label22.setTransform(394, 178);
        this.label23 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label23.lineHeight = 28;
        this.label23.setTransform(429.5, 178);
        this.label24 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label24.lineHeight = 28;
        this.label24.setTransform(74, 178);
        this.label25 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label25.lineHeight = 28;
        this.label25.setTransform(327.5, 178);
        this.label26 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label26.lineHeight = 28;
        this.label26.setTransform(361.5, 178);
        this.label27 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.label27.lineHeight = 28;
        this.label27.setTransform(203, 178);
        this.label28 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.label28.lineHeight = 28;
        this.label28.setTransform(262, 178);

        //row-1
        this.label29 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label29.lineHeight = 28;
        this.label29.setTransform(8, 242.5);
        this.label31 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label31.lineHeight = 28;
        this.label31.setTransform(43, 242.5);
        this.label36 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label36.lineHeight = 28;
        this.label36.setTransform(77, 242.5);
        this.label30 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.label30.lineHeight = 28;
        this.label30.setTransform(110.5, 242.5);
        this.label32 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label32.lineHeight = 28;
        this.label32.setTransform(142.5, 242.5);
        this.label39 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.label39.lineHeight = 28;
        this.label39.setTransform(175.5, 242.5);
        this.label33 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.label33.lineHeight = 28;
        this.label33.setTransform(210, 242.5);
        this.label37 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.label37.lineHeight = 28;
        this.label37.setTransform(242, 242.5);
        this.label38 = new cjs.Text("1", "16px 'Myriad Pro'");
        this.label38.lineHeight = 28;
        this.label38.setTransform(277, 242.5);
        this.label34 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.label34.lineHeight = 28;
        this.label34.setTransform(308, 242.5);
        this.label35 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.label35.lineHeight = 28;
        this.label35.setTransform(345, 242.5);
        this.label40 = new cjs.Text("?", "16px 'Myriad Pro'");
        this.label40.lineHeight = 28;
        this.label40.setTransform(370, 266);

        this.label41 = new cjs.Text("V", "bold 28px 'UusiTekstausMajema'", "#6D6E70");
        this.label41.lineHeight = 36;
        this.label41.setTransform(12, 196);

        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.textbox_group2, this.textbox_group3, this.textbox_group4, this.shape_tri1, this.shape_sqr1, this.shape_circ1);
        this.addChild(this.label1, this.label2, this.label3, this.label4, this.label5);
        this.addChild(this.label6, this.label7, this.label8, this.label9, this.label10);
        this.addChild(this.label11, this.label12, this.label13, this.label14, this.label15);
        this.addChild(this.label16, this.label17, this.label18, this.label19, this.label20);
        this.addChild(this.label21, this.label22, this.label23, this.label24, this.label25, this.label26, this.label27, this.label28, this.label29, this.label30, this.label31, this.label32, this.label33, this.label34, this.label35, this.label36, this.label37, this.label38, this.label39, this.label40,this.label41);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 5, 510, 235, 10);
        this.roundRect1.setTransform(0, 20);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(30, 153, 77, 92, 7);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = this.roundRect2.clone(true);
        this.roundRect3.setTransform(115, 0);

        this.roundRect4 = this.roundRect2.clone(true);
        this.roundRect4.setTransform(254, 0);
        this.roundRect5 = this.roundRect2.clone(true);
        this.roundRect5.setTransform(370, 0);

        this.text = new cjs.Text("Rita figurerna på rätt plats. Måla.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#D83770");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.instance = new lib.p148_1();
        this.instance.setTransform(40, 129, 0.47, 0.47);

        this.shape_ellipse1 = new cjs.Shape(); // ellipse
        this.shape_ellipse1.graphics.f("#ffffff").s("#D83770").ss(0.8, 0, 0, 4).arc(62, 70, 37, 0, 2 * Math.PI, false);
        this.shape_ellipse1.setTransform(0, 0, 2, 1.1);
        this.shape_ellipse2 = this.shape_ellipse1.clone(true);
        this.shape_ellipse2.setTransform(260, 0, 2, 1.1);

        this.line_arrow1 = new cjs.Shape();
        this.line_arrow1.graphics.s("#D83770").f("#D83770").ss(0.9).moveTo(87, 112).lineTo(69, 145).lineTo(72, 144).lineTo(67, 149).lineTo(68, 142).lineTo(69, 145);
        this.line_arrow1.setTransform(0, 0);

        this.line_arrow2 = this.line_arrow1.clone(true);
        this.line_arrow2.setTransform(262, 0);

        this.line_arrow3 = this.line_arrow1.clone(true);
        this.line_arrow3.setTransform(277, 128);
        this.line_arrow3.skewX = -58;
        this.line_arrow3.skewY = -58;
        this.line_arrow4 = this.line_arrow1.clone(true);
        this.line_arrow4.setTransform(15, 128);
        this.line_arrow4.skewX = -58;
        this.line_arrow4.skewY = -58;

        this.shape_tri1 = new cjs.Shape(); // yellow triangle
        this.shape_tri1.graphics.s("#000000").f("#FFF459").ss(0.8).moveTo(411, 73).lineTo(430, 73).lineTo(420.5, 56).lineTo(411, 73);
        this.shape_tri1.setTransform(0, 0);
        this.shape_tri2 = new cjs.Shape(); // red triangle
        this.shape_tri2.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(360, 100).lineTo(379, 100).lineTo(369.5, 83).lineTo(360, 100);
        this.shape_tri2.setTransform(0, 0);
        this.shape_tri3 = new cjs.Shape(); // red triangle
        this.shape_tri3.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(107, 105).lineTo(126, 105).lineTo(116.5, 88).lineTo(107, 105);
        this.shape_tri3.setTransform(0, 0);
        this.shape_tri4 = new cjs.Shape(); // blue triangle
        this.shape_tri4.graphics.s("#000000").f("#42B7E9").ss(0.8).moveTo(167, 78).lineTo(186, 78).lineTo(176.5, 61).lineTo(167, 78);
        this.shape_tri4.setTransform(0, 0);
        this.shape_tri5 = new cjs.Shape(); // white triangle
        this.shape_tri5.graphics.s("#000000").f("#ffffff").ss(0.8).moveTo(306, 146).lineTo(325, 146).lineTo(315.5, 129).lineTo(306, 146);
        this.shape_tri5.setTransform(0, 0);

        this.shape_sqr1 = new cjs.Shape(); // red square
        this.shape_sqr1.graphics.s("#000000").f("#E7544E").ss(0.8).moveTo(76, 79.8).lineTo(92.5, 79.8).lineTo(92.5, 96.3).lineTo(76, 96.3).lineTo(76, 79.8);
        this.shape_sqr1.setTransform(0, 0);
        this.shape_sqr2 = new cjs.Shape(); // blue square
        this.shape_sqr2.graphics.s("#000000").f("#42B7E9").ss(0.8).moveTo(138, 82.8).lineTo(154.5, 82.8).lineTo(154.5, 99.3).lineTo(138, 99.3).lineTo(138, 82.8);
        this.shape_sqr2.setTransform(0, 0);
        this.shape_sqr3 = new cjs.Shape(); // blue square
        this.shape_sqr3.graphics.s("#000000").f("#42B7E9").ss(0.8).moveTo(342, 55.8).lineTo(358.5, 55.8).lineTo(358.5, 72.3).lineTo(342, 72.3).lineTo(342, 55.8);
        this.shape_sqr3.setTransform(0, 0);
        this.shape_sqr4 = new cjs.Shape(); // yellow square
        this.shape_sqr4.graphics.s("#000000").f("#FFF459").ss(0.8).moveTo(394, 85.8).lineTo(410.5, 85.8).lineTo(410.5, 102.3).lineTo(394, 102.3).lineTo(394, 85.8);
        this.shape_sqr4.setTransform(0, 0);
        this.shape_sqr5 = new cjs.Shape(); // white square
        this.shape_sqr5.graphics.s("#000000").f("#ffffff").ss(0.8).moveTo(444, 129.8).lineTo(460.5, 129.8).lineTo(460.5, 146.3).lineTo(444, 146.3).lineTo(444, 129.8);
        this.shape_sqr5.setTransform(0, 0);

        this.shape_circ1 = new cjs.Shape(); // blue circle
        this.shape_circ1.graphics.f("#1CB0E7").s("#000000").ss(0.8, 0, 0, 4).arc(86, 61, 8.6, 0, 2 * Math.PI);
        this.shape_circ1.setTransform(0, 0);
        this.shape_circ2 = new cjs.Shape(); // red circle
        this.shape_circ2.graphics.f("#E7544E").s("#000000").ss(0.8, 0, 0, 4).arc(115, 67, 8.6, 0, 2 * Math.PI);
        this.shape_circ2.setTransform(0, 0);
        this.shape_circ3 = new cjs.Shape(); // yellow circle
        this.shape_circ3.graphics.f("#FFF459").s("#000000").ss(0.8, 0, 0, 4).arc(148, 58, 8.6, 0, 2 * Math.PI);
        this.shape_circ3.setTransform(0, 0);
        this.shape_circ4 = new cjs.Shape(); // red circle
        this.shape_circ4.graphics.f("#E7544E").s("#000000").ss(0.8, 0, 0, 4).arc(387, 64, 8.6, 0, 2 * Math.PI);
        this.shape_circ4.setTransform(0, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.s("#949599").ss(0.9).moveTo(256, 35).lineTo(256, 246);
        this.line1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.shape_ellipse1, this.shape_ellipse2, this.shape_circ1,this.shape_circ2,this.shape_circ3,this.shape_circ4, this.instance,this.line1);
        this.addChild(this.shape_tri1, this.shape_tri2, this.shape_tri3, this.shape_tri4, this.shape_tri5, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.shape_sqr1, this.shape_sqr2, this.shape_sqr3, this.shape_sqr4,this.shape_sqr5);
        this.addChild(this.line_arrow1, this.line_arrow2, this.line_arrow3, this.line_arrow4)
            
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296, 105, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(297, 413, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
