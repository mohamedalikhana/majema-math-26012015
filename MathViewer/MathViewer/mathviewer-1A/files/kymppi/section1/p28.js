(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1219,
	height: 678,
	fps: 20,
	color: "#FFFFFF",
	manifest: [
		{src:"images/s30_31_1.png", id:"s30_31_1"},
		{src:"images/s30_31_2.png", id:"s30_31_2"},
		{src:"images/s30_31_24.png", id:"s30_31_24"},
		{src:"images/s30_31_3.png", id:"s30_31_3"},
		{src:"images/s30_31_37.png", id:"s30_31_37"}
	]
};
// symbols:
(lib.s30_31_1 = function() {
	this.initialize(img.s30_31_1);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,995,567);


(lib.s30_31_2 = function() {
	this.initialize(img.s30_31_2);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,969,404);


(lib.s30_31_24 = function() {
	this.initialize(img.s30_31_24);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,179,146);


(lib.s30_31_3 = function() {
	this.initialize(img.s30_31_3);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,915,359);


(lib.s30_31_37 = function() {
	this.initialize(img.s30_31_37);
}).prototype = p = new cjs.Bitmap();
p.virtualBounds = new cjs.Rectangle(0,0,208,193);


//Page Number
(lib.Symbol31 = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("28", "13px 'Myriad Pro'", "#FFFFFF");
	this.text.lineHeight = 18;
	this.text.setTransform(37.4,648);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A5C0").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
	this.shape.setTransform(31.1,660.8);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,642.4,1218.9,35.1);

(lib.Symbol2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("EgmoAAAMBNRAAA");
	this.shape.setTransform(257.3,96.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_1.setTransform(428.5,78.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_2.setTransform(428.5,78.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_3.setTransform(371.1,78.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_4.setTransform(371.1,78.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_5.setTransform(318.6,78.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_6.setTransform(318.6,78.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_7.setTransform(264.5,78.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_8.setTransform(264.5,78.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_9.setTransform(212.3,78.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_10.setTransform(212.3,78.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_11.setTransform(163.4,78.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_12.setTransform(163.4,78.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_13.setTransform(113.1,78.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_14.setTransform(113.1,78.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("AhfBuIC/AAIAAjbIi/AAg");
	this.shape_15.setTransform(66.9,78.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AhfBuIAAjbIC/AAIAADbg");
	this.shape_16.setTransform(66.9,78.6);

	this.text = new cjs.Text(" Det är 4 bollar. Hur många bollar är i lådan?", "16px 'Myriad Pro'");
	this.text.lineHeight = 19;
	this.text.setTransform(14.4,14.2);

	this.text_1 = new cjs.Text("5.", "bold 17px 'Myriad Pro'", "#00A5C0");
	this.text_1.lineHeight = 20;
	this.text_1.setTransform(0,13);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("EgmEgQiQgHAAgLACQgVAEgSAIQg5AdAABHIAAdiIACARQAEAWAIASQAdA4BHAAMBMJAAAQAdAAAcgOQA5gcAAhHIAA9iQAAgdgOgcQgdg5hHAAg");
	this.shape_17.setTransform(257.1,134.7);

	this.instance = new lib.s30_31_2();
	this.instance.setTransform(0,34.5,0.531,0.531);

	this.addChild(this.instance,this.text_1,this.text,this.ra1,this.ra2,this.ra3,this.ra4,this.ra5,this.ra6,this.ra7,this.ra8);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,241.7);


(lib.Symbol1 = function() {
	this.initialize();	

	// Layer 1
	this.text = new cjs.Text("4", "18px 'Myriad Pro'");
	this.text.lineHeight = 24;
	this.text.setTransform(64.1,230.5);

	this.text_1 = new cjs.Text("4", "18px 'Myriad Pro'");
	this.text_1.lineHeight = 24;
	this.text_1.setTransform(64.1,134.4);

	this.text_2 = new cjs.Text("4", "18px 'Myriad Pro'");
	this.text_2.lineHeight = 24;
	this.text_2.setTransform(188.7,134.4);

	this.text_3 = new cjs.Text("4", "18px 'Myriad Pro'");
	this.text_3.lineHeight = 24;
	this.text_3.setTransform(314.4,134.4);

	this.text_4 = new cjs.Text("4", "18px 'Myriad Pro'");
	this.text_4.lineHeight = 24;
	this.text_4.setTransform(439.3,134.4);

	this.text_5 = new cjs.Text("3", "18px 'Myriad Pro'");
	this.text_5.lineHeight = 24;
	this.text_5.setTransform(438.3,38.1);

	this.text_6 = new cjs.Text("3", "18px 'Myriad Pro'");
	this.text_6.lineHeight = 24;
	this.text_6.setTransform(314.6,38.1);

	this.text_7 = new cjs.Text("3", "18px 'Myriad Pro'");
	this.text_7.lineHeight = 24;
	this.text_7.setTransform(188.8,38.1);

	this.text_8 = new cjs.Text("3", "18px 'Myriad Pro'");
	this.text_8.lineHeight = 24;
	this.text_8.setTransform(62.7,38.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.5,0,0,4).p("AhQBPIChAAIAAicIihAAg");
	this.shape.setTransform(69.8,49.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhQBOIAAibIChAAIAACbg");
	this.shape_1.setTransform(69.8,49.4);

	this.text_9 = new cjs.Text("3", "36px 'UusiTekstausMajema'", "#6C7373");
	this.text_9.lineHeight = 43;
	this.text_9.setTransform(95.8,35.4);

	this.text_10 = new cjs.Text("0", "36px 'UusiTekstausMajema'", "#6C7373");
	this.text_10.lineHeight = 43;
	this.text_10.setTransform(22.4,35.4);

	this.text_11 = new cjs.Text(" Hur många bollar är det på varje sida?", "16px 'Myriad Pro'");
	this.text_11.lineHeight = 19;
	this.text_11.setTransform(13.5,6.2);

	this.text_12 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#00A5C0");
	this.text_12.lineHeight = 27;
	this.text_12.setTransform(0,6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#959C9D").ss(0.5,0,0,4).p("EgmEgXPQgdAAgcAOQg5AdAABHMAAAAq7IACASQAEAVAIASQAdA5BHAAMBMJAAAIASgCQAVgEASgIQA5gdAAhHMAAAgq7QAAgdgOgcQgdg5hHAAg");
	this.shape_2.setTransform(257.1,177.3);

	this.instance = new lib.s30_31_1();
	this.instance.setTransform(8.2,39.2,0.5,0.5);

	this.addChild(this.instance,this.shape_2,this.text_12,this.text_11,this.text_10,this.text_9,this.shape_1,this.shape,this.text_8,this.text_7,this.text_6,this.text_5,this.text_4,this.text_3,this.text_2,this.text_1,this.text,this.ra1,this.ra2,this.ra3,this.ra4,this.ra5,this.ra6,this.ra7,this.ra8,this.ra9,this.ra10,this.ra11,this.ra12,this.ra13,this.ra14,this.ra15,this.ra16);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(0,0,513.3,327.1);


// stage content:
(lib.p28 = function() {
	this.initialize();

	// Layer 1
	this.other = new lib.Symbol31();
	this.other.setTransform(609.5,339,1,1,0,0,0,609.5,338.7);

	// this.v4 = new lib.Symbol4();
	// this.v4.setTransform(925.7,471.2,1,1,0,0,0,262.4,172.2);

	// this.v3 = new lib.Symbol3();
	// this.v3.setTransform(920.4,169.2,1,1,0,0,0,256.3,118.3);

	this.v2 = new lib.Symbol2();
	this.v2.setTransform(295,510.1,1,1,0,0,0,256.3,120.5);

	this.v1 = new lib.Symbol1();
	this.v1.setTransform(295.4,214.3,1,1,0,0,0,256.3,163.2);

	this.addChild(this.v1,this.v2,this.other);
}).prototype = p = new cjs.Container();
p.virtualBounds = new cjs.Rectangle(609.5,389.9,1218.9,626.9);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;