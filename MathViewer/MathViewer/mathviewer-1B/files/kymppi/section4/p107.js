(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p95_1.png",
            id: "p95_1"
        }, {
            src: "images/p107_1.png",
            id: "p107_1"
        }, {
            src: "images/p107_2.png",
            id: "p107_2"

        }]
    };

    (lib.p95_1 = function() {
        this.initialize(img.p95_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.p107_1 = function() {
        this.initialize(img.p107_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p107_2 = function() {
        this.initialize(img.p107_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);
    // symbols:

    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1     

        this.text_1 = new cjs.Text("107", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_1.setTransform(555, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#D6326A").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(579, 660.8);

        this.text_2 = new cjs.Text("Testa dina kunskaper", "24px 'MyriadPro-Semibold'", "#D6326A");
        this.text_2.setTransform(110, 42);

        this.text_3 = new cjs.Text("37", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_3.setTransform(55, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#D6326A").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(49, 23.5, 1.15, 1);

        this.addChild(this.shape_2, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.drawcat = function() {
        this.initialize();

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("").s("#7d7d7d").ss(1).drawRoundRect(0, 0, 50, 85, 15);
        this.roundRect2.setTransform(-17, 0);

        this.instance = new lib.p107_2();
        this.instance.setTransform(0, 0, 0.47, 0.47);

        this.addChild(this.roundRect2, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    (lib.Symbol2 = function() {
        this.initialize();

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(5, 23);

        this.text_1 = new cjs.Text(" Vad är klockan?", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 23);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s("#7d7d7d").ss(1).drawRoundRect(0, 0, 480, 140, 15);
        this.roundRect1.setTransform(0, 1);

        this.Symbolclock_1 = new lib.Symboldotclock();
        this.Symbolclock_1.setTransform(60, 73, 0.83, 0.83);

        this.Symbolclock_2 = new lib.Symboldotclock();
        this.Symbolclock_2.setTransform(175, 73, 0.83, 0.83);

        this.Symbolclock_3 = new lib.Symboldotclock();
        this.Symbolclock_3.setTransform(290, 73, 0.83, 0.83);

        this.Symbolclock_4 = new lib.Symboldotclock();
        this.Symbolclock_4.setTransform(405, 73, 0.83, 0.83);

        this.shape_1 = new cjs.Shape(); // clock handle red
        this.shape_1.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_1.setTransform(60.5, 82.5, 1.2, 0.7, 180);
        this.shape_2 = new cjs.Shape(); // clock handle blue
        this.shape_2.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_2.setTransform(60.5, 59.1, 1.2, 0.9);
        this.text_dot1 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot1.setTransform(54.5, 71.8);

        this.shape_3 = new cjs.Shape(); // clock handle red
        this.shape_3.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_3.setTransform(183, 75, 1.2, 0.7, 100);
        this.shape_4 = new cjs.Shape(); // clock handle blue
        this.shape_4.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_4.setTransform(175.5, 85.5, 1.2, 0.9);
        this.text_dot2 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot2.setTransform(169, 71.8);

        this.shape_5 = new cjs.Shape(); // clock handle red
        this.shape_5.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_5.setTransform(293, 82, 1.2, 0.7, 163);
        this.shape_6 = new cjs.Shape(); // clock handle blue
        this.shape_6.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_6.setTransform(290.5, 85.5, 1.2, 0.9);
        this.text_dot3 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot3.setTransform(284, 71.8);

        this.shape_7 = new cjs.Shape(); // clock handle red
        this.shape_7.graphics.f("#D7172F").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_7.setTransform(403.2, 64, 1.2, 0.7, 169);
        this.shape_8 = new cjs.Shape(); // clock handle blue
        this.shape_8.graphics.f("#0066A6").s().p("AgKCOIAAkbIAVAAIAAEbg");
        this.shape_8.setTransform(405.5, 85.5, 1.2, 0.9);
        this.text_dot4 = new cjs.Text(".", "37px 'Myriad Pro'");
        this.text_dot4.setTransform(399, 71.8);


        var TArr = [];
        var TxtlineX = [146, 260, 374];
        var TxtlineY = [125, 125, 124];

        for (var i = 0; i < TxtlineX.length; i++) {
            this.temp_label = new cjs.Text("halv", "15px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var lineArr = [];
        var lineX = [44, 178, 292, 407];
        var lineY = [87, 87, 87, 87];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(29, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 29);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.Symbolclock_1, this.Symbolclock_2, this.Symbolclock_2, this.Symbolclock_3, this.Symbolclock_4,
            this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8,
            this.text_dot1, this.text_dot2, this.text_dot3, this.text_dot4);

        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    //Part 1
    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(5, 43);

        this.text_1 = new cjs.Text(" Mät och räkna ut hur långt myran ska gå.", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 43);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s('#7d7d7d').drawRoundRect(0, 0, 480, 112, 10);
        this.roundRect1.setTransform(0, 20);

        this.instance = new lib.p95_1();
        this.instance.setTransform(6, 75, 0.465, 0.47);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f('').s("#706F6F").ss(1.5).moveTo(22, 55).lineTo(26, 61).lineTo(24, 58).lineTo(100, 20).lineTo(340, 60).lineTo(280, 15).lineTo(278, 18).lineTo(282, 12);
        this.Line_1.setTransform(15, 50);

        var TArr = [];
        var TxtArr = ['cm', 'cm', 'cm', 'cm', 'hela:'];

        var TxtlineX = [102, 242, 339, 434, 365];
        var TxtlineY = [114, 114, 62, 102, 101];

        for (var i = 0; i < TxtlineX.length; i++) {
            var txt = TxtArr[i];
            this.temp_label = new cjs.Text(txt, "15px 'Myriad Pro'");
            this.temp_label.setTransform(TxtlineX[i], TxtlineY[i]);
            TArr.push(this.temp_label);
        }

        var lineArr = [];
        var lineX = [70, 210, 307, 402];
        var lineY = [75, 75, 23, 63];

        for (var row = 0; row < lineX.length; row++) {
            var hrLine_1 = new cjs.Shape();
            hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 40).lineTo(29, 40);
            hrLine_1.setTransform(lineX[row], lineY[row]);
            lineArr.push(hrLine_1);
        }

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 33);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.instance, this.Line_1);

        for (var i = 0; i < TArr.length; i++) {
            this.addChild(TArr[i]);
        }
        for (var i = 0; i < lineArr.length; i++) {
            this.addChild(lineArr[i]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("3.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(5, 43);

        this.text_1 = new cjs.Text(" Rita formerna där de passar. Måla.", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 43);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s('#7d7d7d').drawRoundRect(0, 0, 480, 150, 10);
        this.roundRect1.setTransform(0, 20);

        this.Rect1 = new cjs.Shape();
        this.Rect1.graphics.f("").s('#878787').drawRoundRect(0, 0, 145, 101, 10);
        this.Rect1.setTransform(10, 55);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {

                if (column == 0 && row == 0) {
                    this.shape_group1.graphics.f('#008BD2').s("#706F6F").ss(1.2).moveTo(24, 52).lineTo(56, 52).lineTo(40, 25).lineTo(24, 52);
                } else if (column == 1 && row == 0) {
                    this.shape_group1.graphics.f('#FFF374').s("#706F6F").ss(0.7).drawRect(68, 25, 27, 27, 0);
                } else if (column == 2 && row == 0) {
                    this.shape_group1.graphics.f("#D51317").s("#706F6F").ss(0.8, 0, 0, 4).arc(82 + (columnSpace * 20.5), 40 + (row * 19), 14, 0, 2 * Math.PI);
                } else if (column == 0 && row == 1) {
                    this.shape_group1.graphics.f("#FFF374").s("#706F6F").ss(0.8, 0, 0, 4).arc(38 + (columnSpace * 20.5), 65 + (row * 19), 14, 0, 2 * Math.PI);
                } else if (column == 1 && row == 1) {
                    this.shape_group1.graphics.f('#D51317').s("#706F6F").ss(1.2).moveTo(64, 95).lineTo(96, 95).lineTo(80, 67).lineTo(64, 95);
                } else {
                    this.shape_group1.graphics.f('#008BD2').s("#706F6F").ss(0.7).drawRect(109, 69, 27, 27, 0);
                }

            }

        }
        this.shape_group1.setTransform(0, 47);

        this.Rect2 = new cjs.Shape();
        this.Rect2.graphics.f("").s('#878787').drawRoundRect(0, 0, 145, 101, 10);
        this.Rect2.setTransform(163.5, 55);

        this.text_2 = new cjs.Text("fyrhörningar", "16px 'Myriad Pro'");
        this.text_2.setTransform(195, 77);

        this.Rect3 = new cjs.Shape();
        this.Rect3.graphics.f("").s('#878787').drawRoundRect(0, 0, 145, 101, 10);
        this.Rect3.setTransform(317, 55);

        this.text_3 = new cjs.Text("trianglar", "16px 'Myriad Pro'");
        this.text_3.setTransform(368, 77);

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 50);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.Rect1, this.Rect2, this.Rect3, this.shape_group1, this.text_2, this.text_3);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    (lib.drawScale = function(scaleLength, paddingLeft, paddingRight, cropRight) {
        this.initialize();
        var scaleHeight = 126.8;

        var corners = {
            TL: 10,
            TR: 10,
            BR: 10,
            BL: 10
        };

        if (cropRight) {
            corners.TR = 0;
            corners.BR = 0;
            paddingRight = 10;
        }

        this.metricscale = new cjs.Shape();
        this.metricscale.graphics.f("#E2F3FD").s("#959C9D").ss(1).drawRoundRectComplex(0, 0, paddingLeft + (scaleLength * 100) + paddingRight, scaleHeight, corners.TL, corners.TR, corners.BR, corners.BL);
        if (cropRight) {
            this.metricscale.graphics.s('#ffffff').drawRect(paddingLeft + (scaleLength * 100) + paddingRight - 1, 1, 1 * 2, scaleHeight - 1 * 2);
        }
        this.metricscale.setTransform(0, 0);
        this.addChild(this.metricscale);

        for (var i = 0; i <= scaleLength * 10; i++) {
            var height = 31.7;
            if (i % 5 == 0) {
                height = 63.4;
            }

            var round = new cjs.Shape();
            round.graphics.f("#000000").s('#7d7d7d').drawRect(0, 0, 1, height);
            round.setTransform(25 + i * 10, 0);
            this.addChild(round, text);
            if (i % 10 == 0) {
                var ispace = 16;
                var number = i / 10;
                if (number > 9) {
                    ispace = 8;
                }
                var text = new cjs.Text(number + "", "36px 'Myriad Pro'", "#000000");
                text.setTransform(ispace + i * 10, height + (height * 50 / 100));
                this.addChild(text);
            }
        };

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 320.3, 143.6);

    (lib.Symbol5 = function() {
        this.initialize();

        // Layer 1   

        this.text = new cjs.Text("4.", "bold 17px 'Myriad Pro'", "#D6326A");
        this.text.setTransform(5, 43);

        this.text_1 = new cjs.Text(" Hur lång är pennan?", "16px 'Myriad Pro'");
        this.text_1.setTransform(20, 43);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#FFFFFF").s('#7d7d7d').drawRoundRect(0, 0, 480, 150, 10);
        this.roundRect1.setTransform(0, 20);

        this.instance = new lib.p107_1();
        this.instance.setTransform(142, 55, 0.465, 0.47);

        var scaleLength = 12.5;
        var paddingLeft = 25,
            paddingRight = 25;
        var cropRight = false;
        var bigScale1 = new lib.drawScale(scaleLength, paddingLeft, paddingRight, cropRight);
        bigScale1.setTransform(25, 90, 0.284, 0.36);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(0, 0).lineTo(28, 0);
        this.hrLine_1.setTransform(415, 135);

        this.text_2 = new cjs.Text("cm", "16px 'Myriad Pro'");
        this.text_2.setTransform(443, 133);

        this.sidebar = new lib.drawcat()
        this.sidebar.setTransform(485, 50);

        this.addChild(this.sidebar, this.roundRect1, this.text, this.text_1, this.instance, bigScale1, this.hrLine_1, this.text_2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 535, 170);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(305, 108, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305, 238, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(305, 360, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v4 = new lib.Symbol5();
        this.v4.setTransform(305, 520, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.addChild(this.v1, this.v2, this.v3, this.v4, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
