(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p108_1.png",
            id: "p108_1"
        }]
    };

    // symbols:

    (lib.p108_1 = function() {
        this.initialize(img.p108_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("108", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(33.4, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(451 + (columnSpace * 33), 12, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape, this.textbox_group1, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Fortsätt mönstret.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(22, 29);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(4, 29);

        this.text_2 = new cjs.Text("Kluring", "18px 'MyriadPro-Semibold'", "#F1662B");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(-1, 0);

        this.instance = new lib.p108_1();
        this.instance.setTransform(48, 25, 0.69, 0.69);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 3, 514, 240, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text_2, this.text, this.text_1, this.instance);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1.5, 0, 514, 320, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(25, 8);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(6, 8);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('#ffffff').s("#F1652B").ss(0.8).drawRect(28, 34, 180, 274);
        this.rect1.setTransform(0, 0);

        this.rect2 = new cjs.Shape();
        this.rect2.graphics.f('#ffffff').s("#F1652B").ss(0.8).drawRect(277, 34, 180, 274);
        this.rect2.setTransform(0, 0);

        var ToBeAdded = [];
        for (var col = 0; col < 9; col++) {
            var colSpace = col;
            for (var row = 0; row < 12; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
                temp_text.lineHeight = -7;
                temp_text.setTransform(42 + (colSpace * 18.8), 44 + (rowSpace * 22.1));
                ToBeAdded.push(temp_text);
            }
        }

        for (var col = 0; col < 9; col++) {
            var colSpace = col;
            for (var row = 0; row < 12; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
                temp_text.lineHeight = -7;
                temp_text.setTransform(291 + (colSpace * 18.8), 44 + (rowSpace * 22.1));
                ToBeAdded.push(temp_text);
            }
        }

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.f('#0095DA').s("#949599").ss(1.2).moveTo(44, 139).lineTo(44, 72).lineTo(82, 116).lineTo(158, 116).lineTo(157, 116).lineTo(157, 50)
            .lineTo(175.5, 50).lineTo(175, 139).lineTo(156, 160.5).lineTo(63, 160.5).lineTo(44, 139);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape();
        this.Line_2.graphics.f('#FFF679').s("#949599").ss(1.2).moveTo(175, 50).lineTo(194.5, 72).lineTo(175, 72);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape();
        this.Line_3.graphics.s("#949599").ss(1.2).moveTo(44, 270).lineTo(44, 203).lineTo(82, 250);
        this.Line_3.setTransform(0, 0);

        this.Line_4 = new cjs.Shape();
        this.Line_4.graphics.f('#C8C9CB').s("#949599").ss(1.2).moveTo(293, 160).lineTo(293, 72).lineTo(311, 50).lineTo(311, 72).lineTo(293, 94).lineTo(388, 94)
            .lineTo(406, 116).lineTo(406, 160.5).lineTo(386, 138).lineTo(368, 160.5).lineTo(368, 138).lineTo(330.5, 138).lineTo(330.5, 160.5).lineTo(311, 138).lineTo(293, 160);
        this.Line_4.setTransform(0, 0);

        this.Line_5 = new cjs.Shape();
        this.Line_5.graphics.f('#FFF679').s("#949599").ss(1.2).moveTo(387, 94).lineTo(387, 50).lineTo(405, 72).lineTo(425, 72).lineTo(443, 50)
            .lineTo(443, 94).lineTo(425, 116).lineTo(405, 116).lineTo(387, 94);
        this.Line_5.setTransform(0, 0);

        this.Line_6 = new cjs.Shape();
        this.Line_6.graphics.s("#949599").ss(1.2).moveTo(293, 292).lineTo(293, 204).lineTo(311.5, 183).lineTo(311.5, 204);
        this.Line_6.setTransform(0, 0);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.s("#F1652B").ss(0.8).moveTo(28, 171).lineTo(208, 171).moveTo(277, 171).lineTo(457, 171);
        this.Line_7.setTransform(0, 0);

        this.text_dot_1 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_1.lineHeight = 55;
        this.text_dot_1.setTransform(159.5, 32);
        this.text_dot_2 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_2.lineHeight = 55;
        this.text_dot_2.setTransform(159.5, 163.5);

        this.text_dot_3 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_3.lineHeight = 55;
        this.text_dot_3.setTransform(399.5, 64);
        this.text_dot_4 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_4.lineHeight = 55;
        this.text_dot_4.setTransform(399.5, 197);
        this.text_dot_5 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_5.lineHeight = 55;
        this.text_dot_5.setTransform(418, 64);
        this.text_dot_6 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_6.lineHeight = 55;
        this.text_dot_6.setTransform(418, 197);
        this.text_dot_7 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_7.lineHeight = 55;
        this.text_dot_7.setTransform(409.5, 74);
        this.text_dot_8 = new cjs.Text(".", "38px 'Myriad Pro'");
        this.text_dot_8.lineHeight = 55;
        this.text_dot_8.setTransform(409.5, 208);

        this.addChild(this.roundRect1, this.text, this.text_1, this.rect1, this.rect2);
        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.text_dot_1, this.text_dot_2, this.text_dot_3, this.text_dot_4,this.text_dot_5, this.text_dot_6, this.text_dot_7, this.text_dot_8);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 320.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(292.3, 88, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(292.3, 354, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
