(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p34_1.png",
            id: "p34_1"
        }]
    };

    (lib.p34_1 = function() {
        this.initialize(img.p34_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol3 = function() {
        this.initialize();

        // Layer 1
        this.text_1 = new cjs.Text("kunna lösa uppgifter i addition 0 till 20, med tiotalsövergång", "9px 'Myriad Pro'");
        this.text_1.setTransform(71, 659);

        this.text_2 = new cjs.Text("34", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_2.setTransform(37.5, 658);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_1.setTransform(31.1, 660.2);

        this.text_3 = new cjs.Text("Addera till talen 9 och 8", "24px 'MyriadPro-Semibold'", "#F1662B");
        this.text_3.setTransform(95.5, 45);

        this.text_4 = new cjs.Text("11", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_4.setTransform(52, 42);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#F1662B").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_2.setTransform(43.6, 23.5, 1, 1.1);

        this.addChild(this.shape_2, this.text_4, this.text_3, this.shape_1, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();


        // Block-1
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_1.setTransform(255.9, 18.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_2.setTransform(240.1, 18.4);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
        this.shape_3.setTransform(225.9, 18.4);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAAKATQALASgHAUQgIAYgbAM");
        this.shape_4.setTransform(210.3, 18.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_5.setTransform(195.9, 18.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_6.setTransform(180.2, 18.4);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQASAAALATQAKASgHAUQgIAYgbAM");
        this.shape_7.setTransform(165.2, 18.4);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_8.setTransform(150.7, 18.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_9.setTransform(135.2, 18.3);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_10.setTransform(120.6, 18.3);
        //-----------------------------
        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_11.setTransform(105.6, 18.3);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_12.setTransform(90.6, 18.3);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_13.setTransform(75, 18.3);
        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_14.setTransform(60.6, 18.3);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AglgVQAVgaAUABQATAAAKATQAKASgHAUQgIAYgbAM");
        this.shape_15.setTransform(45.6, 18.3);


        //------------------------------------

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_16.setTransform(255.2, 26.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_17.setTransform(255.2, 26.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_18.setTransform(240.5, 26.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_19.setTransform(240.5, 26.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_20.setTransform(225.5, 26.9);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_21.setTransform(225.5, 26.9);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_22.setTransform(210.4, 26.9);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_23.setTransform(210.4, 26.9);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_24.setTransform(195.3, 26.9);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_25.setTransform(195.3, 26.9);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_26.setTransform(180.7, 26.9);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_27.setTransform(180.7, 26.9);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_28.setTransform(165.6, 26.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_29.setTransform(165.6, 26.9);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_30.setTransform(150.2, 26.9);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_31.setTransform(150.2, 26.9);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAIAIAKAAg");
        this.shape_32.setTransform(135.2, 26.9);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
        this.shape_33.setTransform(135.2, 26.9);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_34.setTransform(120.5, 26.9);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_35.setTransform(120.5, 26.9);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_36.setTransform(105.5, 26.9);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_37.setTransform(105.5, 26.9);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_38.setTransform(90.5, 26.9);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_39.setTransform(90.5, 26.9);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_40.setTransform(75.5, 26.9);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_41.setTransform(75.5, 26.9);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_42.setTransform(60.5, 26.9);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_43.setTransform(60.5, 26.9);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
        this.shape_44.setTransform(45.5, 26.9);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgJAAgJgIg");
        this.shape_45.setTransform(45.5, 26.9);

        // white block
        var shadow = new cjs.Shadow("#7d7d7d", 0, -1, 4);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(28, 19, 245, 96, 5);
        this.shape_69.shadow = shadow.clone(true);
        this.shape_69.setTransform(0, 0);

        //Main yellow block

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.f("#FFF173").s("").ss(0.5, 0, 0, 4).drawRoundRect(0, 0, 509, 134, 10);
        this.shape_116.shadow = shadow.clone(true);
        this.shape_116.setTransform(0, 0);

        //draw boxs1  

        this.textbox_group1 = new cjs.Shape();
        this.textbox_group2 = new cjs.Shape();
        this.textbox_group3 = new cjs.Shape();
        this.textbox_group4 = new cjs.Shape();
        var xPos = 40,
            ypos = 42.5,
            yPos1 = 62.5
        rectWidth = 100,
            rectHeight = 40.5,
            boxWidth = 20,
            boxHeight = 20,
            numberOfBoxes = 12

        this.textbox_group1.graphics.s("##707070").ss(0.4).drawRect(xPos, ypos, rectWidth, rectHeight);
        this.textbox_group3.graphics.s("##707070").ss(0.4).moveTo(xPos, yPos1).lineTo(140, yPos1);

        for (var i = 0; i < 3; i++) {
            this.textbox_group4.graphics.s("##707070").ss(0.4).moveTo(160, 42.5 + (boxHeight * i)).lineTo(260, 42.5 + (boxHeight * i));
        }

        for (var j = 1; j < numberOfBoxes; j++) {

            this.textbox_group1.graphics.s("##707070").ss(0.4).moveTo(xPos + (boxWidth * j), ypos).lineTo(xPos + (boxWidth * j), ypos + boxHeight);
            this.textbox_group2.graphics.s("##707070").ss(0.4).moveTo(xPos + (boxWidth * j), yPos1).lineTo(xPos + (boxWidth * j), yPos1 + boxHeight);

        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("9 + 4 = 13", "14px 'Myriad Pro'");
        this.text_1.setTransform(100, 105);

        this.instance = new lib.p34_1();
        this.instance.setTransform(320, 10, 0.48, 0.48);

        this.text_2 = new cjs.Text("Fyll den första", "14px 'Myriad Pro'");
        this.text_2.setTransform(350, 45);

        this.text_3 = new cjs.Text("tiolådan först.", "14px 'Myriad Pro'");
        this.text_3.setTransform(350, 65);

        //Draw Balls left

        this.shape_group1 = new cjs.Shape();
        var xpos = 50;
        var ypos = 52;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    ypos = 58;
                    xpos = 50;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    ypos = 52;
                    xpos = 50;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#008BD2";
                }

                this.shape_group1.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group1.setTransform(0, 0);
        //Draw Balls right
        this.shape_group2 = new cjs.Shape();
        var Rfillcolor = "#008BD2";
        var Rxpos = 170;
        var Rypos = 52;
        for (var Rcolumn = 0; Rcolumn < 3; Rcolumn++) {
            this.shape_group2.graphics.f(Rfillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(Rxpos + (Rcolumn * 20), Rypos + (0 * 14), 7, 0, 2 * Math.PI);
        }


        this.addChild(this.shape_116, this.shape_69, this.shape_1, this.shape_2, this.shape_3, this.shape_4, this.shape_5, this.shape_6, this.shape_7, this.shape_8, this.shape_9, this.shape_10);
        this.addChild(this.shape_11, this.shape_12, this.shape_13, this.shape_14, this.shape_15, this.shape_16, this.shape_17, this.shape_18, this.shape_19, this.shape_20);
        this.addChild(this.shape_21, this.shape_22, this.shape_23, this.shape_24, this.shape_25, this.shape_26, this.shape_27, this.shape_28, this.shape_29, this.shape_30);
        this.addChild(this.shape_31, this.shape_32, this.shape_33, this.shape_34, this.shape_35, this.shape_36, this.shape_37, this.shape_38, this.shape_39, this.shape_40);
        this.addChild(this.shape_45, this.shape_44, this.shape_43, this.shape_42, this.shape_41, this.text_1);
        this.addChild(this.shape_group1, this.shape_group2);
        this.addChild(this.instance, this.text_2, this.text_3, this.textbox_group1, this.textbox_group2, this.textbox_group3, this.textbox_group4);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, -1, 550, 143.6);

    (lib.Symbol2 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Addera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(19, 20);

        this.text_q2 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(0, 20);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 30, 252.7, 86, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 30, 252.7, 86, 10);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 120, 252.7, 86, 10);
        this.roundRect3.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 120, 252.7, 86, 10);
        this.roundRect4.setTransform(0, 0);


        this.textbox_group1 = new cjs.Shape();
        this.textbox_group2 = new cjs.Shape();
        this.textbox_group3 = new cjs.Shape();
        this.textbox_group4 = new cjs.Shape();
        var xPos = 15,
            ypos = 42.5,
            yPos1 = 62.5
        rectWidth = 100,
            rectHeight = 40.5,
            boxWidth = 20,
            boxHeight = 20,
            numberOfBoxes = 24
        this.textbox_group1.graphics.s("##707070").ss(0.4).drawRect(xPos, ypos, rectWidth, rectHeight);
        this.textbox_group3.graphics.s("##707070").ss(0.4).moveTo(xPos, yPos1).lineTo(115, yPos1);

        for (var hrLine = 0; hrLine < 3; hrLine++) {
            this.textbox_group4.graphics.s("##707070").ss(0.4).moveTo(135, 42.5 + (boxHeight * hrLine)).lineTo(235, 42.5 + (boxHeight * hrLine));
            this.textbox_group4.graphics.s("##707070").ss(0.4).moveTo(272, 42.5 + (boxHeight * hrLine)).lineTo(372, 42.5 + (boxHeight * hrLine));
            this.textbox_group4.graphics.s("##707070").ss(0.4).moveTo(392, 42.5 + (boxHeight * hrLine)).lineTo(492, 42.5 + (boxHeight * hrLine));
        }

        for (var j = 1; j < numberOfBoxes; j++) {
            if (j > 11) {
                xPos = 32,
                    ypos = 42.5,
                    yPos1 = 62.5

            }

            this.textbox_group1.graphics.s("##707070").ss(0.4).moveTo(xPos + (boxWidth * j), ypos).lineTo(xPos + (boxWidth * j), ypos + boxHeight);
            this.textbox_group2.graphics.s("##707070").ss(0.4).moveTo(xPos + (boxWidth * j), yPos1).lineTo(xPos + (boxWidth * j), yPos1 + boxHeight);

        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("9 + 2 =", "16px 'Myriad Pro'");
        this.text_1.setTransform(77, 102);

        this.text_2 = new cjs.Text("9 + 3 =", "16px 'Myriad Pro'");
        this.text_2.setTransform(338, 102);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.s("##707070").ss(0.4).moveTo(390, 107.5).lineTo(430, 107.5);
        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.s("##707070").ss(0.4).moveTo(130, 107.5).lineTo(178, 107.5);

        //Draw balls Left
        this.shape_group1 = new cjs.Shape();
        var xpos = 25;
        var ypos = 52;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 25;
                    ypos = 58;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 25;
                    ypos = 52;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#008BD2";
                }

                this.shape_group1.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group1.setTransform(0, 0);

        //Draw balls Left
        this.shape_group2 = new cjs.Shape();
        var xpos = 282;
        var ypos = 52;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 282;
                    ypos = 58;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 282;
                    ypos = 52;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#008BD2";
                }

                this.shape_group2.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group2.setTransform(0, 0);

        //Draw Balls right
        var Rfillcolor = "#008BD2";
        this.shape_group3 = new cjs.Shape();
        this.shape_group3.graphics.f(Rfillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(145, 52, 7, 0, 2 * Math.PI);
        this.shape_group3.setTransform(0, 0);

        var Rxpos = 402;
        var Rypos = 52;
        this.shape_group4 = new cjs.Shape();
        for (var Rcolumn = 0; Rcolumn < 2; Rcolumn++) {
            this.shape_group4.graphics.f(Rfillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(Rxpos + (Rcolumn * 20), Rypos + (0 * 14), 7, 0, 2 * Math.PI);

        }
        this.shape_group4.setTransform(0, 0);
        //--------------------------------------------------------Box 2-----------------------------------------

        this.textbox_group5 = new cjs.Shape();
        this.textbox_group6 = new cjs.Shape();
        this.textbox_group7 = new cjs.Shape();
        this.textbox_group8 = new cjs.Shape();
        var xPos1 = 15,
            ypos1 = 132.5,
            yPos2 = 152.5
        rectWidth1 = 100,
            rectHeight1 = 40.5,
            boxWidth1 = 20,
            boxHeight1 = 20,
            numberOfBoxes1 = 24
        this.textbox_group5.graphics.s("##707070").ss(0.4).drawRect(xPos1, ypos1, rectWidth1, rectHeight1);
        this.textbox_group7.graphics.s("##707070").ss(0.4).moveTo(xPos1, yPos2).lineTo(115, yPos2);

        for (var hrLine1 = 0; hrLine1 < 3; hrLine1++) {
            this.textbox_group8.graphics.s("##707070").ss(0.4).moveTo(135, 132.5 + (boxHeight1 * hrLine1)).lineTo(235, 132.5 + (boxHeight1 * hrLine1));
            this.textbox_group8.graphics.s("##707070").ss(0.4).moveTo(272, 132.5 + (boxHeight1 * hrLine1)).lineTo(372, 132.5 + (boxHeight1 * hrLine1));
            this.textbox_group8.graphics.s("##707070").ss(0.4).moveTo(392, 132.5 + (boxHeight1 * hrLine1)).lineTo(492, 132.5 + (boxHeight1 * hrLine1));
        }

        for (var j = 1; j < numberOfBoxes1; j++) {
            if (j > 11) {
                xPos1 = 32,
                    ypos1 = 132.5,
                    yPos2 = 152.5

            }

            this.textbox_group5.graphics.s("##707070").ss(0.4).moveTo(xPos1 + (boxWidth1 * j), ypos1).lineTo(xPos1 + (boxWidth1 * j), ypos1 + boxHeight1);
            this.textbox_group6.graphics.s("##707070").ss(0.4).moveTo(xPos1 + (boxWidth1 * j), yPos2).lineTo(xPos1 + (boxWidth1 * j), yPos2 + boxHeight1);

        }
        this.textbox_group5.setTransform(0, 0);

        this.text_3 = new cjs.Text("9 + 5 =", "16px 'Myriad Pro'");
        this.text_3.setTransform(77, 192);

        this.text_4 = new cjs.Text("9 + 4 =", "16px 'Myriad Pro'");
        this.text_4.setTransform(338, 192);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.s("##707070").ss(0.4).moveTo(390, 197.5).lineTo(430, 197.5);
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.s("##707070").ss(0.4).moveTo(130, 197.5).lineTo(178, 197.5);

        //Draw balls Left
        this.shape_group5 = new cjs.Shape();
        var xpos = 25;
        var ypos = 142;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 25;
                    ypos = 148;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 25;
                    ypos = 142;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#008BD2";
                }

                this.shape_group5.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group5.setTransform(0, 0);

        //Draw balls Left
        this.shape_group6 = new cjs.Shape();
        var xpos = 282;
        var ypos = 142;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 282;
                    ypos = 148;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 282;
                    ypos = 142;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#008BD2";
                }

                this.shape_group6.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group6.setTransform(0, 0);

        //Draw Balls right
        var Rfillcolor = "#008BD2";
        this.shape_group7 = new cjs.Shape();
        var Rxpos = 145;
        var Rypos = 142;
        for (var Rcolumn1 = 0; Rcolumn1 < 4; Rcolumn1++) {
            this.shape_group7.graphics.f(Rfillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(Rxpos + (Rcolumn1 * 20), Rypos + (0 * 14), 7, 0, 2 * Math.PI);
        }
        this.shape_group7.setTransform(0, 0);

        var Rxpos = 402;
        var Rypos = 142;
        this.shape_group8 = new cjs.Shape();
        for (var Rcolumn = 0; Rcolumn < 3; Rcolumn++) {
            this.shape_group8.graphics.f(Rfillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(Rxpos + (Rcolumn * 20), Rypos + (0 * 14), 7, 0, 2 * Math.PI);

        }
        this.shape_group8.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.text_q1, this.text_q2, this.text_1, this.text_2, this.text_3, this.text_4);
        this.addChild(this.textbox_group1, this.textbox_group2, this.textbox_group3, this.textbox_group4, this.textbox_group5, this.textbox_group6, this.textbox_group7, this.textbox_group8)
        this.addChild(this.hrLine_1, this.hrLine_2, this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5, this.shape_group6, this.shape_group7, this.shape_group8)
        this.addChild(this.hrLine_3, this.hrLine_4)
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 300);

    (lib.Symbol4 = function() {
        this.initialize();

        // Layer 1
        this.text_q1 = new cjs.Text("Rita cirklar och addera.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(19, 20);

        this.text_q2 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_q2.setTransform(0, 20);


        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 30, 252.7, 86, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 30, 252.7, 86, 10);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(0, 120, 252.7, 86, 10);
        this.roundRect3.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s("#7d7d7d").ss(0.5, 0, 0, 4).drawRoundRect(257, 120, 252.7, 86, 10);
        this.roundRect4.setTransform(0, 0);


        this.textbox_group1 = new cjs.Shape();
        this.textbox_group2 = new cjs.Shape();
        this.textbox_group3 = new cjs.Shape();
        this.textbox_group4 = new cjs.Shape();
        var xPos = 15,
            ypos = 42.5,
            yPos1 = 62.5
        rectWidth = 100,
            rectHeight = 40.5,
            boxWidth = 20,
            boxHeight = 20,
            numberOfBoxes = 24
        this.textbox_group1.graphics.s("##707070").ss(0.4).drawRect(xPos, ypos, rectWidth, rectHeight);
        this.textbox_group3.graphics.s("##707070").ss(0.4).moveTo(xPos, yPos1).lineTo(115, yPos1);

        for (var hrLine = 0; hrLine < 3; hrLine++) {
            this.textbox_group4.graphics.s("##707070").ss(0.4).moveTo(135, 42.5 + (boxHeight * hrLine)).lineTo(235, 42.5 + (boxHeight * hrLine));
            this.textbox_group4.graphics.s("##707070").ss(0.4).moveTo(272, 42.5 + (boxHeight * hrLine)).lineTo(372, 42.5 + (boxHeight * hrLine));
            this.textbox_group4.graphics.s("##707070").ss(0.4).moveTo(392, 42.5 + (boxHeight * hrLine)).lineTo(492, 42.5 + (boxHeight * hrLine));
        }

        for (var j = 1; j < numberOfBoxes; j++) {
            if (j > 11) {
                xPos = 32,
                    ypos = 42.5,
                    yPos1 = 62.5

            }

            this.textbox_group1.graphics.s("##707070").ss(0.4).moveTo(xPos + (boxWidth * j), ypos).lineTo(xPos + (boxWidth * j), ypos + boxHeight);
            this.textbox_group2.graphics.s("##707070").ss(0.4).moveTo(xPos + (boxWidth * j), yPos1).lineTo(xPos + (boxWidth * j), yPos1 + boxHeight);

        }
        this.textbox_group1.setTransform(0, 0);

        this.text_1 = new cjs.Text("9 + 6 =", "16px 'Myriad Pro'");
        this.text_1.setTransform(77, 102);

        this.text_2 = new cjs.Text("9 + 7 =", "16px 'Myriad Pro'");
        this.text_2.setTransform(338, 102);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.s("##707070").ss(0.4).moveTo(390, 107.5).lineTo(430, 107.5);
        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.s("##707070").ss(0.4).moveTo(130, 107.5).lineTo(178, 107.5);

        //Draw balls Left
        this.shape_group1 = new cjs.Shape();
        var xpos = 25;
        var ypos = 52;
        var fillcolor = "";
        var ssVal = 0.5;

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 25;
                    ypos = 58;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 25;
                    ypos = 52;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    fillcolor = "#FFFFFF";
                    var ssVal = 0.8;
                }

                this.shape_group1.graphics.f(fillcolor).s("#706F6F").ss(ssVal, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group1.setTransform(0, 0);

        //Draw balls Left
        this.shape_group2 = new cjs.Shape();
        var xpos = 282;
        var ypos = 52;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 282;
                    ypos = 58;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 282;
                    ypos = 52;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    continue;
                }

                this.shape_group2.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group2.setTransform(0, 0);

        //Draw Balls right
        var Rfillcolor = "#FFFFFF";
        var Rxpos = 145;
        var Rypos = 52;
        this.shape_group4 = new cjs.Shape();
        for (var Rcolumn = 0; Rcolumn < 5; Rcolumn++) {
            this.shape_group4.graphics.f(Rfillcolor).s("#706F6F").ss(0.8, 0, 0, 4).arc(Rxpos + (Rcolumn * 20), Rypos + (0 * 14), 7, 0, 2 * Math.PI);

        }
        this.shape_group4.setTransform(0, 0);
        //--------------------------------------------------------Box 2-----------------------------------------

        this.textbox_group5 = new cjs.Shape();
        this.textbox_group6 = new cjs.Shape();
        this.textbox_group7 = new cjs.Shape();
        this.textbox_group8 = new cjs.Shape();
        var xPos1 = 15,
            ypos1 = 132.5,
            yPos2 = 152.5
        rectWidth1 = 100,
            rectHeight1 = 40.5,
            boxWidth1 = 20,
            boxHeight1 = 20,
            numberOfBoxes1 = 24
        this.textbox_group5.graphics.s("##707070").ss(0.4).drawRect(xPos1, ypos1, rectWidth1, rectHeight1);
        this.textbox_group7.graphics.s("##707070").ss(0.4).moveTo(xPos1, yPos2).lineTo(115, yPos2);

        for (var hrLine1 = 0; hrLine1 < 3; hrLine1++) {
            this.textbox_group8.graphics.s("##707070").ss(0.4).moveTo(135, 132.5 + (boxHeight1 * hrLine1)).lineTo(235, 132.5 + (boxHeight1 * hrLine1));
            this.textbox_group8.graphics.s("##707070").ss(0.4).moveTo(272, 132.5 + (boxHeight1 * hrLine1)).lineTo(372, 132.5 + (boxHeight1 * hrLine1));
            this.textbox_group8.graphics.s("##707070").ss(0.4).moveTo(392, 132.5 + (boxHeight1 * hrLine1)).lineTo(492, 132.5 + (boxHeight1 * hrLine1));
        }

        for (var j = 1; j < numberOfBoxes1; j++) {
            if (j > 11) {
                xPos1 = 32,
                    ypos1 = 132.5,
                    yPos2 = 152.5

            }

            this.textbox_group5.graphics.s("##707070").ss(0.4).moveTo(xPos1 + (boxWidth1 * j), ypos1).lineTo(xPos1 + (boxWidth1 * j), ypos1 + boxHeight1);
            this.textbox_group6.graphics.s("##707070").ss(0.4).moveTo(xPos1 + (boxWidth1 * j), yPos2).lineTo(xPos1 + (boxWidth1 * j), yPos2 + boxHeight1);

        }
        this.textbox_group5.setTransform(0, 0);

        this.text_3 = new cjs.Text("9 + 9 =", "16px 'Myriad Pro'");
        this.text_3.setTransform(77, 192);

        this.text_4 = new cjs.Text("9 + 8 =", "16px 'Myriad Pro'");
        this.text_4.setTransform(338, 192);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.s("##707070").ss(0.4).moveTo(390, 197.5).lineTo(430, 197.5);
        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.s("##707070").ss(0.4).moveTo(130, 197.5).lineTo(178, 197.5);

        //Draw balls Left
        this.shape_group5 = new cjs.Shape();
        var xpos = 25;
        var ypos = 142;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 25;
                    ypos = 148;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 25;
                    ypos = 142;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    continue;
                }

                this.shape_group5.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group5.setTransform(0, 0);

        //Draw balls Left
        this.shape_group6 = new cjs.Shape();
        var xpos = 282;
        var ypos = 142;
        var fillcolor = "";

        for (var column = 0; column < 5; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1) {
                    xpos = 282;
                    ypos = 148;
                    fillcolor = "#D51317";
                }
                if (row == 0) {
                    xpos = 282;
                    ypos = 142;
                    fillcolor = "#D51317";
                }
                if (row == 1 && column == 4) {
                    continue;
                }

                this.shape_group6.graphics.f(fillcolor).s("#706F6F").ss(0.5, 0, 0, 4).arc(xpos + (columnSpace * 20), ypos + (row * 14), 7, 0, 2 * Math.PI);
            }

        }
        this.shape_group6.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.text_q1, this.text_q2, this.text_1, this.text_2, this.text_3, this.text_4);
        this.addChild(this.textbox_group1, this.textbox_group2, this.textbox_group3, this.textbox_group4, this.textbox_group5, this.textbox_group6, this.textbox_group7, this.textbox_group8)
        this.addChild(this.hrLine_1, this.hrLine_2, this.shape_group1, this.shape_group2, this.shape_group3, this.shape_group4, this.shape_group5, this.shape_group6, this.shape_group7, this.shape_group8)
        this.addChild(this.hrLine_3, this.hrLine_4)
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 300);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol3();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(295, 114, 1, 1, 0, 0, 0, 254.6, 53.4);

        this.v2 = new lib.Symbol2();
        this.v2.setTransform(295, 260, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v3 = new lib.Symbol4();
        this.v3.setTransform(295, 470, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.v3, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
