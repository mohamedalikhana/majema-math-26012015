(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };

    // symbols:

    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("69", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#87C140").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#707070").ss(0.8).drawRect(468 + (columnSpace * 33), 12, 28, 28);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape, this.textbox_group1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("Vad händer sedan? Fortsätt mönstret.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(22, 29);

        this.text_1 = new cjs.Text("1.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(4, 29);

        this.text_2 = new cjs.Text("Kluring", "18px 'MyriadPro-Semibold'", "#87C140");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(-1, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 3, 508, 223.5, 10);
        this.roundRect1.setTransform(0, 20);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 6; column++) {
            var columnSpace = column;
            for (var row = 0; row < 3; row++) {
                var rowSpace = row;
                this.textbox_group1.graphics.f('#ffffff').s("#545456").ss(0.8).drawRect(38 + (columnSpace * 76), 51 + (rowSpace * 66), 55, 55);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        var arryPos = ['57', '233'];
        this.shape_group1 = new cjs.Shape();
        for (var i = 0; i < arryPos.length; i++) {
            var yPos = parseInt(arryPos[i]);
            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                var fColor = "";
                var xPos = (i == 0) ? 65 : 42.5;

                if (i == 0) {
                    fColor = "#DA2129"; //red
                    yPos = (column == 1) ? 101 : parseInt(arryPos[i]);
                } else if (i == 1) {
                    fColor = "#20B14A"; //green
                    columnSpace = (column == 2) ? 2.56 : column;
                    yPos = (column == 0) ? 189 : parseInt(arryPos[i]);
                }
                for (var row = 0; row < 1; row++) {
                    this.shape_group1.graphics.f(fColor).s(fColor).ss(0.8, 0, 0, 4).arc(xPos + (columnSpace * 76.5), yPos + (row * 23), 4.5, 0, 2 * Math.PI);
                }
            }
        }
        this.shape_group1.setTransform(0, 0);

        var ToBeAdded = [];
        for (var col = 0; col < 3; col++) {
            var colSpace = col;
            if(col==2){
                colSpace = 2.04;
            }
            var temp_text = new cjs.Text("•", "bold 11px 'Myriad Pro'", "#000000");
            temp_text.lineHeight = 12;
            temp_text.setTransform(64 + (colSpace * 75), 140);
            ToBeAdded.push(temp_text);
        }

        this.Line_1 = new cjs.Shape(); 
        this.Line_1.graphics.s("#000000").ss(1).moveTo(66.5, 144).lineTo(91, 120).lineTo(91, 125).
            moveTo(91, 120).lineTo(86, 120);
        this.Line_1.setTransform(0, 0);

        this.Line_2 = new cjs.Shape(); 
        this.Line_2.graphics.s("#000000").ss(1).moveTo(140.5, 145).lineTo(167, 170).lineTo(167, 165)
            .moveTo(167, 170).lineTo(162, 170);
        this.Line_2.setTransform(0, 0);

        this.Line_3 = new cjs.Shape(); 
        this.Line_3.graphics.s("#000000").ss(1).moveTo(219, 145).lineTo(192, 170).lineTo(192, 165)
            .moveTo(192, 170).lineTo(197, 170);
        this.Line_3.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text_2, this.text, this.text_1, this.textbox_group1, this.shape_group1, this.Line_1);
        this.addChild(this.Line_2,this.Line_3);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 490, 290);

    (lib.Symbol3 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(1.5, 0, 508, 346, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Rita och måla likadant.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(25, 8);

        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#87C140");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(6, 8);

        this.rect1 = new cjs.Shape();
        this.rect1.graphics.f('#ffffff').s("#818284").ss(0.8).drawRect(42, 34, 180, 299);
        this.rect1.setTransform(0, 0);

        this.rect2 = new cjs.Shape();
        this.rect2.graphics.f('#ffffff').s("#818284").ss(0.8).drawRect(291, 34, 180, 299);
        this.rect2.setTransform(0, 0);

        var ToBeAdded = [];
        for (var col = 0; col < 9; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
                temp_text.lineHeight = -1;
                temp_text.setTransform(56 + (colSpace * 18.8), 44 + (rowSpace * 21.1));
                ToBeAdded.push(temp_text);
            }
        }

        for (var col = 0; col < 9; col++) {
            var colSpace = col;
            for (var row = 0; row < 14; row++) {
                var rowSpace = row;
                var temp_text = new cjs.Text("•", "bold 10px 'Myriad Pro'", "#A9AFB0");
                temp_text.lineHeight = -1;
                temp_text.setTransform(305 + (colSpace * 18.8), 44 + (rowSpace * 21.1));
                ToBeAdded.push(temp_text);
            }
        }
        // block 1
        this.Line_1 = new cjs.Shape(); // red
        this.Line_1.graphics.f('#DA2129').s("#949599").ss(1.2).moveTo(76, 176.5).lineTo(191, 176.5).lineTo(191, 113).lineTo(152, 113).lineTo(152, 135)
            .lineTo(114, 135).lineTo(114, 113).lineTo(114, 92).lineTo(76, 92).lineTo(76, 176.5);
        this.Line_1.setTransform(0, 0);
        this.Line_2 = new cjs.Shape(); // blue
        this.Line_2.graphics.f('#0095DA').s("#949599").ss(1.2).moveTo(57, 92).lineTo(134, 92).lineTo(95, 49).lineTo(57, 92)
            .moveTo(132, 113).lineTo(209, 113).lineTo(170, 70).lineTo(132, 113)
            .moveTo(114, 176.5).lineTo(152, 176.5).lineTo(152, 156.5).lineTo(133, 134).lineTo(114, 156.5).lineTo(114, 176.5);
        this.Line_2.setTransform(0, 0);
        this.Line_3 = new cjs.Shape(); // yellow
        this.Line_3.graphics.f('#FFF679').s("#949599").ss(1.2).moveTo(94, 113).lineTo(113, 113).lineTo(113, 134).lineTo(95, 134).lineTo(95, 113)
            .moveTo(170, 134).lineTo(190, 134).lineTo(190, 155).lineTo(171, 155).lineTo(171, 134);
        this.Line_3.setTransform(0, 0);

        // block 2
        this.Line_4 = new cjs.Shape(); // yellow
        this.Line_4.graphics.f('#FFF679').s("#949599").ss(1.2).moveTo(307, 71).lineTo(307, 176.5).lineTo(457, 176.5).lineTo(457, 71)
            .lineTo(420, 71).lineTo(420, 113).lineTo(401, 113).lineTo(401, 71).lineTo(363, 71).lineTo(363, 113).lineTo(344.5, 113)
            .lineTo(344.5, 71).lineTo(307, 71);
        this.Line_4.setTransform(0, 0);
        this.Line_5 = new cjs.Shape(); // red
        this.Line_5.graphics.f('#DA2129').s("#949599").ss(1.2).moveTo(307, 92).lineTo(344, 92).lineTo(325, 113).lineTo(307, 92)
            .moveTo(364, 92).lineTo(400, 92).lineTo(382, 113).lineTo(364, 92)
            .moveTo(420, 92).lineTo(456, 92).lineTo(438, 113).lineTo(420, 92)
            .moveTo(307, 70).lineTo(343, 70).lineTo(325, 49).lineTo(307, 70)
            .moveTo(364, 70).lineTo(400, 70).lineTo(382, 49).lineTo(364, 70)
            .moveTo(420, 70).lineTo(456, 70).lineTo(438, 49).lineTo(420, 70);
        this.Line_5.setTransform(0, 0);
        this.Line_6 = new cjs.Shape(); // BLUE
        this.Line_6.graphics.f('#0095DA').s("#949599").ss(1.2).moveTo(307, 134).lineTo(344, 134).lineTo(325, 155).lineTo(307, 134)
            .moveTo(420, 134).lineTo(456, 134).lineTo(438, 155).lineTo(420, 134)
            .moveTo(363.5, 176.5).lineTo(363.5, 134).lineTo(401, 134).lineTo(401, 176.5).lineTo(363.5, 176.5)
            .moveTo(382, 176.5).lineTo(382, 134);
        this.Line_6.setTransform(0, 0);

        this.Line_7 = new cjs.Shape();
        this.Line_7.graphics.s("#818284").ss(0.8).moveTo(42, 189).lineTo(222, 189).moveTo(291, 189).lineTo(471, 189);
        this.Line_7.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.rect1, this.rect2);
        this.addChild(this.Line_1, this.Line_2, this.Line_3, this.Line_4, this.Line_5, this.Line_6, this.Line_7);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 320.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(315.3, 87, 1, 1, 0, 0, 0, 254.6, 50);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(315.3, 327, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
