(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../section1/images/c2_ex1_1.png",
            id: "c2_ex1_1"
        }, {
            src: "../section1/images/c2_ex1_2.png",
            id: "c2_ex1_2"
        }]
    };


    (lib.c2_ex1_1 = function() {
        this.initialize(img.c2_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);


    (lib.c2_ex1_2 = function() {
        this.initialize(img.c2_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);
    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.CommentText1 = function() {
        this.initialize();


        this.hintText1 = new cjs.Text("Räkna de röda bollarna.", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550 + 130, 470);
        this.hintText2 = new cjs.Text("Räkna de blå bollarna.", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText2.textAlign = 'center';
        this.hintText2.lineHeight = 19;
        this.hintText2.visible = false;
        this.hintText2.setTransform(550 + 130, 470);
        this.hintText3 = new cjs.Text("Sägs högt: 3 är lika stort som 3", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText3.textAlign = 'center';
        this.hintText3.lineHeight = 19;
        this.hintText3.visible = false;
        this.hintText3.setTransform(550 + 150, 470);
        this.hintText4 = new cjs.Text("Hur betecknas det?", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText4.textAlign = 'center';
        this.hintText4.lineHeight = 19;
        this.hintText4.visible = false;
        this.hintText4.setTransform(550 + 150, 470);
        this.addChild(this.hintText1, this.hintText2, this.hintText3, this.hintText4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.CommentText2 = function() {
        this.initialize();

        this.hintText1 = new cjs.Text("Räkna de röda bollarna.", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText1.textAlign = 'center';
        this.hintText1.lineHeight = 19;
        this.hintText1.visible = false;
        this.hintText1.setTransform(550 + 130, 470);
        this.hintText2 = new cjs.Text("Räkna de blå bollarna.", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText2.textAlign = 'center';
        this.hintText2.lineHeight = 19;
        this.hintText2.visible = false;
        this.hintText2.setTransform(550 + 130, 470);
        this.hintText3 = new cjs.Text("Sägs högt: 2 är lika stort som 2", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText3.textAlign = 'center';
        this.hintText3.lineHeight = 19;
        this.hintText3.visible = false;
        this.hintText3.setTransform(550 + 150, 470);
        this.hintText4 = new cjs.Text("Hur betecknas det?", "25px 'Myriad Pro'", "#00B4EA")
        this.hintText4.textAlign = 'center';
        this.hintText4.lineHeight = 19;
        this.hintText4.visible = false;
        this.hintText4.setTransform(550 + 150, 470);
        this.addChild(this.hintText1, this.hintText2, this.hintText3, this.hintText4);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Hur många bollar?", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 130, -40);

        this.balls = new lib.balls1();
        this.balls.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.textFields = new lib.textFields();
        this.textFields.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);
        this.addChild(this.balls, this.textFields, this.questionText);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage2 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Hur många bollar?", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 130, -40);

        this.balls = new lib.balls1();
        this.balls.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.textFields = new lib.textFields();
        this.textFields.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.answers = new lib.answers1();
        this.answers.text_3.visible = true;
        this.answers.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        // this.commentTexts = new lib.CommentText1();
        // this.commentTexts.hintText1.visible = true;


        this.addChild(this.balls, this.textFields, this.questionText, this.answers, this.commentTexts);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage3 = function() {
        this.initialize();

        var questionText = new cjs.Text("Hur många bollar?", "40px 'Myriad Pro'")
        questionText.lineHeight = 19;
        questionText.textAlign = 'center';
        questionText.setTransform(550 + 130, -40);

        this.balls = new lib.balls1();
        this.balls.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.textFields = new lib.textFields();
        this.textFields.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.answers = new lib.answers1();
        this.answers.text_3.visible = true;
        this.answers.text_1.visible = true;
        this.answers.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        // this.commentTexts = new lib.CommentText1();
        // this.commentTexts.hintText2.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: questionText,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 0,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;

        this.addChild(this.balls, this.textFields, questionText, this.answers, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Hur många bollar?", "40px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 130, -40);

        this.balls = new lib.balls1();
        this.balls.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.textFields = new lib.textFields();
        this.textFields.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.answers = new lib.answers1();
        this.answers.text_3.visible = true;
        this.answers.text_1.visible = true;
        this.answers.text_2.visible = true;
        this.answers.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        // this.commentTexts = new lib.CommentText1();
        // this.commentTexts.hintText3.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;

        this.addChild(this.balls, this.textFields, this.answers, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();

        this.questionText = new cjs.Text("Är det lika många röda och blå  bollar?", "36px 'Myriad Pro'")
        this.questionText.lineHeight = 19;
        this.questionText.textAlign = 'center';
        this.questionText.setTransform(550 + 100, -40);

        this.answerText = new cjs.Text("3 är lika med 3.", "36px 'Myriad Pro'")
        this.answerText.lineHeight = 19;
        this.answerText.textAlign = 'center';
        this.answerText.setTransform(550 + 150, 340 + 47);

        this.balls = new lib.balls1();
        this.balls.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        this.textFields = new lib.textFields();
        this.textFields.setTransform(190, -20, 4, 4, 0, 0, 0, 0, 0);

        var answers = new lib.answers1();
        answers.text_3.visible = true;
        answers.text_2.visible = true;
        answers.text_1.visible = true;
        answers.setTransform(197, -15, 4, 4, 0, 0, 0, 0, 0);

        // this.commentTexts = new lib.CommentText1();
        // this.commentTexts.hintText4.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.answerText,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.balls, this.textFields, this.answerText, answers, this.commentTexts);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Lika många – är lika med", "bold 24px 'Myriad Pro'", "#00A5C0");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("2", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(55.7, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A5C0").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.balls1 = function() {
        this.initialize();

        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();

        this.shape4 = this.blueBall.clone(true);
        this.shape4.setTransform(152.2, 16.8, 0.17, 0.17);
        this.shape5 = this.blueBall.clone(true);
        this.shape5.setTransform(166.8, 39.1, 0.17, 0.17);
        this.shape6 = this.blueBall.clone(true);
        this.shape6.setTransform(137.1, 39.1, 0.17, 0.17);

        this.shape1 = this.redBall.clone(true);
        this.shape1.setTransform(100, 39.6, 0.17, 0.17);
        this.shape2 = this.redBall.clone(true);
        this.shape2.setTransform(74.6, 39.6, 0.17, 0.17);
        this.shape3 = this.redBall.clone(true);
        this.shape3.setTransform(50.9, 39.6, 0.17, 0.17);

        this.addChild(this.shape1, this.shape2, this.shape3, this.shape4, this.shape5, this.shape6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);
    (lib.balls2 = function() {
        this.initialize();

        this.blueBall = new lib.c2_ex1_1();
        this.redBall = new lib.c2_ex1_2();

        this.shape5 = this.blueBall.clone(true);
        this.shape5.setTransform(166.8, 39.1, 0.17, 0.17);
        this.shape6 = this.blueBall.clone(true);
        this.shape6.setTransform(137.1, 39.1, 0.17, 0.17);

        this.shape1 = this.redBall.clone(true);
        this.shape1.setTransform(91.8, 39.6, 0.17, 0.17);
        this.shape2 = this.redBall.clone(true);
        this.shape2.setTransform(62.1, 39.6, 0.17, 0.17);

        this.addChild(this.shape1, this.shape2, this.shape5, this.shape6);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.textFields = function() {
        this.initialize();

        this.textbox_1 = new cjs.Shape();
        this.textbox_1.graphics.f('#ffffff').s('#000000').ss(0.7).drawRoundRect(0, 0, 20, 20, 2);
        this.textbox_1.setTransform(146.9 + 6, 59.9 + 10);


        this.textbox_3 = new cjs.Shape();
        this.textbox_3.graphics.f('#ffffff').s('#000000').ss(0.7).drawRoundRect(0, 0, 20, 20, 2);
        this.textbox_3.setTransform(69.7 + 6, 59.9 + 10);

        this.addChild(this.textbox_3, this.textbox_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    (lib.answers1 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(146.9 + 10, 59.9 + 12);

        this.text_2 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(116.9, 59.9 + 12);

        this.text_3 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(69.7 + 10, 59.9 + 12);

        this.addChild(this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);
    (lib.answers2 = function() {
        this.initialize();

        this.text_1 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(146.9 + 10, 59.9 + 12);

        this.text_2 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(116.9, 59.9 + 12);

        this.text_3 = new cjs.Text("2", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(69.7 + 10, 59.9 + 12);

        this.addChild(this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);
    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7, this.stage8, this.stage9, this.stage10);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
