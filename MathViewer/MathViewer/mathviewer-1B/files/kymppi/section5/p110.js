(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: []
    };



    (lib.Symbol16 = function() {
        this.initialize();
        // Layer 1
        this.text_1 = new cjs.Text("38", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.setTransform(57, 42);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#00A3C4").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape_1.setTransform(52, 23.5);

        this.text_2 = new cjs.Text("Talen 0 till 50", "24px 'MyriadPro-Semibold'", "#00A3C4");
        this.text_2.setTransform(103, 42);


        this.pageBottomText = new cjs.Text("förstå och kunna använda talen 0 till 50", "9px 'Myriad Pro'");
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(75, 656);

        this.text = new cjs.Text("110", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(44, 657);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(39.5, 660.8, 1, 1.1);

        this.addChild(this.shape, this.text, this.shape_1, this.text_1, this.text_2, this.pageBottomText);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();
        // Block-1

        var ToBeAdded = [];
        for (var col = 0; col < 12; col++) {
            var shape_1 = new cjs.Shape();
            shape_1.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
            shape_1.setTransform(33.9 + (col * 15), 16.4);

            var shape_2 = new cjs.Shape();
            shape_2.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
            shape_2.setTransform(31.5 + (col * 15), 21.9);

            var shape_3 = new cjs.Shape();
            shape_3.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
            shape_3.setTransform(31.5 + (col * 15), 21.9);
            ToBeAdded.push(shape_1, shape_2, shape_3)

        };

        for (var col = 0; col < 12; col++) {

            var shape_4 = new cjs.Shape();
            shape_4.graphics.f().s("#818888").ss(1.5, 1, 0, 4).p("AgmgVQAWgaATABQATAAALATQAKASgHAUQgIAYgbAM");
            shape_4.setTransform(287 + (col * 15), 16.4);

            var shape_5 = new cjs.Shape();
            shape_5.graphics.f().s("#959C9D").ss(0.5, 0, 0, 4).p("AAAAbQALAAAIgIQAIgIAAgLQAAgKgIgIQgIgIgLAAQgKAAgIAIQgIAIAAAKQAAALAIAIQAJAIAJAAg");
            shape_5.setTransform(284 + (col * 15), 21.9);

            var shape_6 = new cjs.Shape();
            shape_6.graphics.f("#FFF173").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
            shape_6.setTransform(284 + (col * 15), 21.9);
            ToBeAdded.push(shape_4, shape_5, shape_6)

        }

        this.roundRect2 = new cjs.Shape(); // white block
        this.roundRect2.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(20, 15, 190, 165, 5);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = new cjs.Shape(); // white block
        this.roundRect3.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(275, 15, 190, 165, 5);
        this.roundRect3.setTransform(0, 0);

        // Main yellow block
        this.roundRect1 = new cjs.Shape(); // white block
        this.roundRect1.graphics.f("#FFF173").s("#959C9D").ss(1).drawRoundRect(0, 1, 509, 210, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect4 = new cjs.Shape(); // white block
        this.roundRect4.graphics.f("#ffffff").s("#959C9D").ss(1).drawRoundRect(40, 35, 150, 137, 0);
        this.roundRect4.setTransform(0, 0);

        this.roundRect5 = this.roundRect4.clone(true)
        this.roundRect5.setTransform(255, 0);

        this.hrLine = new cjs.Shape();
        this.hrLine.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(40, 152).lineTo(190, 152)
        this.hrLine.setTransform(0, 0);

        this.hrLine1 = new cjs.Shape();
        this.hrLine1.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(40, 53).lineTo(190, 53)
        this.hrLine1.setTransform(0, 0);

        this.vrLine1 = new cjs.Shape();
        this.vrLine1.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(115, 35).lineTo(115, 172)
        this.vrLine1.setTransform(0, 0);


        for (var col = 0; col < 2; col++) {
            var Line_2 = new cjs.Shape();
            Line_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(65, 60).lineTo(65, 148).lineTo(74, 148).lineTo(74, 60).lineTo(65, 60)
                .moveTo(65, 68.6).lineTo(74, 68.6).moveTo(65, 77.1).lineTo(74, 77.1).moveTo(65, 85.6).lineTo(74, 85.6).moveTo(65, 94.1).lineTo(74, 94.1)
                .moveTo(65, 102.6).lineTo(74, 102.6).moveTo(65, 111.1).lineTo(74, 111.1).moveTo(65, 120.6).lineTo(74, 120.6).moveTo(65, 130.1).lineTo(74, 130.1)
                .moveTo(65, 139.7).lineTo(74, 139.7);
            Line_2.setTransform(0 + (col * 15), 0);
            ToBeAdded.push(Line_2)
        }

        for (var col = 0; col < 2; col++) {
            for (var row = 0; row < 2; row++) {
                var Line_3 = new cjs.Shape();
                if (row == 0 && col == 1) {
                    continue;
                }
                Line_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(143, 127.1).lineTo(143, 136).lineTo(151, 136).lineTo(151, 127.1).lineTo(143, 127.1)
                Line_3.setTransform(0 + (col * 14), 0 + (row * 12));
                ToBeAdded.push(Line_3)
            }
        }

        // Block-2

        this.hrLine2 = new cjs.Shape();
        this.hrLine2.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(295, 152).lineTo(445.5, 152)
        this.hrLine2.setTransform(0, 0);

        this.hrLine3 = new cjs.Shape();
        this.hrLine3.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(295, 53).lineTo(445.5, 53)
        this.hrLine3.setTransform(0, 0);

        this.vrLine2 = new cjs.Shape();
        this.vrLine2.graphics.f('#ffffff').s("#958FC5").ss(0.7).moveTo(370, 35).lineTo(370, 172)
        this.vrLine2.setTransform(0, 0);

        for (var col = 0; col < 3; col++) {
            var greenSquareLine_2 = new cjs.Shape();
            greenSquareLine_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(65, 60).lineTo(65, 148).lineTo(74, 148).lineTo(74, 60).lineTo(65, 60)
                .moveTo(65, 68.6).lineTo(74, 68.6).moveTo(65, 77.1).lineTo(74, 77.1).moveTo(65, 85.6).lineTo(74, 85.6).moveTo(65, 94.1).lineTo(74, 94.1)
                .moveTo(65, 102.6).lineTo(74, 102.6).moveTo(65, 111.1).lineTo(74, 111.1).moveTo(65, 120.6).lineTo(74, 120.6).moveTo(65, 130.1).lineTo(74, 130.1)
                .moveTo(65, 139.7).lineTo(74, 139.7);
            greenSquareLine_2.setTransform(247 + (col * 15), 0);
            ToBeAdded.push(greenSquareLine_2)
        }

        for (var col = 0; col < 2; col++) {
            var yellowSquareLine_3 = new cjs.Shape();
            if (row == 0 && col == 1) {
                continue;
            }
            yellowSquareLine_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(143, 127.1).lineTo(143, 136).lineTo(151, 136).lineTo(151, 127.1).lineTo(143, 127.1)
            yellowSquareLine_3.setTransform(253 + (col * 14), 10);
            ToBeAdded.push(yellowSquareLine_3)
        }

        var tmpNum = "";
        var xpos = 62;
        var tmpxpos = 72;
        var fntSize = ""
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 4; col++) {
                if ((col == 0 || col == 2) && row == 0) {
                    tmpNum = "tiotal";
                    fntSize = "14px 'Myriad Pro'"
                }
                if ((col == 1 || col == 3) && row == 0) {
                    tmpNum = "ental";
                    fntSize = "14px 'Myriad Pro'"
                }
                if ((col == 0 || col == 3) && row == 1) {
                    tmpNum = "2";
                    fntSize = "17px 'MyriadPro-Semibold'"
                }
                if ((col == 1 || col == 2) && row == 1) {
                    tmpNum = "3";
                    fntSize = "17px 'MyriadPro-Semibold'"
                }
                if (col > 1) {
                    xpos = 170
                } else if (row == 1) {
                    xpos = 70;
                    tmpxpos = 78
                } else {
                    xpos = 62;
                    tmpxpos = 72
                }
                var tempText = new cjs.Text(tmpNum, fntSize);
                tempText.setTransform(xpos + (col * tmpxpos), 50 + (row * 117));
                ToBeAdded.push(tempText);
            }
        }

        var tmptxt = ""
        for (var col = 0; col < 2; col++) {
            if (col == 0) {
                tmptxt = "tjugotre"
            } else {
                tmptxt = "trettiotvå"
            }
            var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
            tempText.setTransform(76 + (col * 254), 200);
            ToBeAdded.push(tempText);
        }
        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5, this.hrLine, this.hrLine1, this.vrLine1, this.Line_3)
        this.addChild(this.hrLine2, this.hrLine3, this.vrLine2)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(1, 1, 530.3, 123.3);

    (lib.Symbol6 = function() {
        this.initialize();

        this.text_q1 = new cjs.Text("Skriv talet.", "16px 'Myriad Pro'");
        this.text_q1.setTransform(20, 1);

        this.text = new cjs.Text("1.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 510, 333, 10);
        this.roundRect1.setTransform(0, 10);

        var ToBeAdded = [];

        for (var row = 0; row < 2; row++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(7, 120 + (row * 111)).lineTo(505, 120 + (row * 111));
            ToBeAdded.push(hrLine)
        };

        var moveToypos = 13;
        var lineToypos = 115;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 2; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 125;
                    lineToypos = 227;
                } else if (row == 2) {
                    moveToypos = 235;
                    lineToypos = 342;
                } else {
                    moveToypos = 13;
                    lineToypos = 115;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(165 + (col * 170), moveToypos).lineTo(165 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }

        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 12; col++) {

                var xPos = 0;

                if (col > 6) {
                    xPos = 225;
                } else if (col > 2) {
                    xPos = 115;
                }

                if (row == 0 && (col == 2 || col == 5 || col == 6 || col == 9 || col == 10 || col == 11)) {
                    continue;
                } else if (row == 2 && col == 11) {
                    continue;
                }


                var greenSquareLine_2 = new cjs.Shape();
                // if(col>1){continue;}
                greenSquareLine_2.graphics.f("#1A8943").s("#000000").setStrokeStyle(0.7).moveTo(25, 20).lineTo(25, 108).lineTo(34, 108).lineTo(34, 20).lineTo(25, 20)
                    .moveTo(25, 28.6).lineTo(34, 28.6).moveTo(25, 37.1).lineTo(34, 37.1).moveTo(25, 45.6).lineTo(34, 45.6).moveTo(25, 54.1).lineTo(34, 54.1)
                    .moveTo(25, 62.6).lineTo(34, 62.6).moveTo(25, 71.1).lineTo(34, 71.1).moveTo(25, 80.6).lineTo(34, 80.6).moveTo(25, 90.1).lineTo(34, 90.1)
                    .moveTo(25, 99.7).lineTo(34, 99.7);
                greenSquareLine_2.setTransform(xPos + (col * 15), 0 + (row * 112));
                ToBeAdded.push(greenSquareLine_2)
            }
        }
        //box 1 yellow boxes
        var xpos = 75
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 4; col++) {
                var yellowSquareLine_3 = new cjs.Shape();
                if (col > 1) {
                    xpos = 217
                } else {
                    xpos = 75
                }
                if (col < 1 && row < 2) {
                    continue
                } else if (col == 1 && row < 3) {
                    continue
                } else if (col == 3 && row < 1) {
                    continue
                }
                yellowSquareLine_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(143, 50.1).lineTo(143, 59)
                    .lineTo(151, 59).lineTo(151, 50.1).lineTo(143, 50.1)
                yellowSquareLine_3.setTransform(xpos + (col * 14), 0 + (row * 12));
                ToBeAdded.push(yellowSquareLine_3)
            }
        }
        //box 2 yellow boxes
        var xpos = 15
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 5; col++) {
                var yellowSquareLine_3 = new cjs.Shape();
                if (col > 2) {
                    xpos = 317
                } else if (col > 0) {
                    xpos = 175
                } else(xpos = 15)

                if (col < 1 && row < 4) {
                    continue
                } else if (col == 1 && row < 2) {
                    continue
                } else if (col == 2 && row < 3) {
                    continue
                } else if (col == 4 && row < 1) {
                    continue
                }
                yellowSquareLine_3.graphics.f("#FFF679").s("#000000").setStrokeStyle(0.7).moveTo(57, 275.1).lineTo(57, 284)
                    .lineTo(65, 284).lineTo(65, 275.1).lineTo(57, 275.1)
                yellowSquareLine_3.setTransform(xpos + (col * 14), 0 + (row * 12));
                ToBeAdded.push(yellowSquareLine_3)
            }
        }

        //Rectangle box and text
        var xpos = 107;
        var ypos = 85;
        var tmptxt = ""
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 6; column++) {
                var columnSpace = column;
                if (column > 3) {
                    xpos = 367
                } else if (column > 1) {
                    xpos = 235
                } else {
                    xpos = 107
                }
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 21.4), ypos + (row * 112), 21, 23);
                if (column == 1 || column == 3 || column == 5) {
                    tmptxt = 'e'
                } else {
                    tmptxt = 't'
                }
                var tempText = new cjs.Text(tmptxt, "16px 'Myriad Pro'");
                tempText.setTransform((xpos + 5) + (columnSpace * 22), (ypos - 5) + (row * 112));
                ToBeAdded.push(tmp_Rect, tempText);
            }
        }

        this.addChild(this.text_q1, this.text, this.roundRect1)
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 340);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(601, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(299, 272, 1, 1, 0, 0, 0, 256.3, 217.9);
        this.v2 = new lib.Symbol6();
        this.v2.setTransform(299, 510, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
