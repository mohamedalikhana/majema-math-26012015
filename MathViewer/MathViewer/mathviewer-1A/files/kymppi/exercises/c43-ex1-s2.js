var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "images/c43_ex1_s1_1.png",
            id: "c43_ex1_s1_1"
        }, {
            src: "images/c43_ex1_s1_2.png",
            id: "c43_ex1_s1_2"
        }]
    };


    (lib.c43_ex1_s1_1 = function() {
        this.initialize(img.c43_ex1_s1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c43_ex1_s1_2 = function() {
        this.initialize(img.c43_ex1_s1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



    (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Ett tal saknas", "bold 24px 'Myriad Pro'", "#8390C8");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("43", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8390C8").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();

        var transformXY = 1;
        this.img_bread_1 = new lib.c43_ex1_s1_1();
        this.img_bread_1.setTransform(-30, 70, transformXY, transformXY)
        this.img_outer_1 = new lib.c43_ex1_s1_2();
        this.img_outer_1.setTransform(-30, 70, transformXY, transformXY)

        this.img_bread_2 = new lib.c43_ex1_s1_1();
        this.img_bread_2.setTransform(170, 70, transformXY, transformXY)
        this.img_outer_2 = new lib.c43_ex1_s1_2();
        this.img_outer_2.setTransform(170, 70, transformXY, transformXY)

        this.img_bread_3 = new lib.c43_ex1_s1_1();
        this.img_bread_3.setTransform(370, 70, transformXY, transformXY)
        this.img_outer_3 = new lib.c43_ex1_s1_2();
        this.img_outer_3.setTransform(370, 70, transformXY, transformXY)

        this.img_bread_4 = new lib.c43_ex1_s1_1();
        this.img_bread_4.setTransform(570, 70, transformXY, transformXY)

        this.img_bread_5 = new lib.c43_ex1_s1_1();
        this.img_bread_5.setTransform(770, 70, transformXY, transformXY)

        this.img_bread_6 = new lib.c43_ex1_s1_1();
        this.img_bread_6.setTransform(970, 70, transformXY, transformXY)

        this.img_bread_7 = new lib.c43_ex1_s1_1();
        this.img_bread_7.setTransform(1170, 70, transformXY, transformXY)

        this.outer_rect = new cjs.Shape();
        this.outer_rect.graphics.f('#ffffff').s("#949599").ss(0.9).drawRoundRect(-50, 50, 1400, 170, 10);
        this.outer_rect.setTransform(0, 0);

        this.answers = new lib.answers();
        // this.answers.text_4.visible = true;
        // this.answers.text_3.visible = true;
        // this.answers.text_2.visible = true;
        // this.answers.text_1.visible = true;
        this.answers.setTransform(-65, 300, 6, 6, 0, 0, 0, 0, 0);


        this.instrTexts = [];
        this.instrTexts.push('Manuel har 7 pepparkakor.');
        this.instrTexts.push('Manuel äter några av sina pepparkakor.');
        this.instrTexts.push('Hur många pepparkakor har Manuel ätit?');


        this.instrText1_a = new cjs.Text(this.instrTexts[0], "40px 'Myriad Pro'");
        this.instrText1_a.lineHeight = 19;
        this.instrText1_a.textAlign = 'center';
        this.instrText1_a.setTransform(550 + 120, -40);

        this.instrText1_b = new cjs.Text(this.instrTexts[1], "40px 'Myriad Pro'");
        this.instrText1_b.lineHeight = 19;
        this.instrText1_b.textAlign = 'center';
        this.instrText1_b.setTransform(550 + 120, -40);
        this.instrText1_b.visible = false;

        this.instrText1_c = new cjs.Text(this.instrTexts[2], "40px 'Myriad Pro'");
        this.instrText1_c.lineHeight = 19;
        this.instrText1_c.textAlign = 'center';
        this.instrText1_c.setTransform(550 + 120, -40);
        this.instrText1_c.visible = false;

        this.instrText2 = new cjs.Text("Gå tillsammans med klassen igenom hur många pepparkakor som finns.", "20px 'Myriad Pro'", "#155EAE")
        this.instrText2.lineHeight = 19;
        this.instrText2.textAlign = 'center';
        this.instrText2.visible = false;
        this.instrText2.setTransform(550 + 150, 470);

        this.addChild(this.outer_rect, this.img_outer_1, this.img_outer_2, this.img_outer_3, this.img_bread_1, this.img_bread_2, this.img_bread_3,
            this.img_bread_4, this.img_bread_5, this.img_bread_6, this.img_bread_7,
            this.answers, this.instrText1_a, this.instrText1_b, this.instrText1_c, this.instrText2);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1_b.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText1_a,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 300,
            alphaTimeout: 1500,
        });
        this.tweens.push({
            ref: this.stage0.instrText1_b,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2600,
            alphaTimeout: 2000,
        });

        this.tweens.push({
            ref: this.stage0.img_bread_1,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 5000,
            alphaTimeout: 2500,
        });
        this.tweens.push({
            ref: this.stage0.img_bread_2,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 5000,
            alphaTimeout: 2500,
        });
        this.tweens.push({
            ref: this.stage0.img_bread_3,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 5000,
            alphaTimeout: 2500,
        });

        p.tweens = this.tweens;

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);


    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1_a.visible = false;
        this.stage0.instrText1_b.visible = true;
        this.stage0.img_bread_1.visible = false;
        this.stage0.img_bread_2.visible = false;
        this.stage0.img_bread_3.visible = false;

        this.answers = this.stage0.answers;
        this.answers.textbox_group1.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.textbox_group1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1_a.visible = false;
        this.stage0.instrText1_b.visible = true;
        this.stage0.instrText1_c.visible = true;
        this.stage0.img_bread_1.visible = false;
        this.stage0.img_bread_2.visible = false;
        this.stage0.img_bread_3.visible = false;

        this.answers = this.stage0.answers;
        this.answers.textbox_group1.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText1_b,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 300,
            alphaTimeout: 1500,
        });
        this.tweens.push({
            ref: this.stage0.instrText1_c,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2600,
            alphaTimeout: 2000,
        });

        p.tweens = this.tweens;

        this.addChild(this.stage0);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1_a.visible = false;
        this.stage0.instrText1_c.visible = true;
        this.stage0.img_bread_1.visible = false;
        this.stage0.img_bread_2.visible = false;
        this.stage0.img_bread_3.visible = false;

        this.answers = this.stage0.answers;
        this.answers.textbox_group1.visible = true;
        this.answers.text_5.visible = true;
        this.answers.text_4.visible = true;
        this.answers.text_3.visible = true;
        this.answers.text_2.visible = true;
        this.answers.text_1.visible = true;

        this.tweens = [];
        this.tweens.push({
            ref: this.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("7", "16px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(85, 0);

        this.text_2 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(100, 0);

        this.text_3 = new cjs.Text("3", "16px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(115, 0);

        this.text_4 = new cjs.Text("=", "16px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(130, 0);

        this.text_5 = new cjs.Text("4", "16px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(145, 0);

        this.textbox_group1 = new cjs.Shape();
        this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(0.6).drawRect(115, 0, 15, 15);
        this.textbox_group1.setTransform(0, 0);
        this.textbox_group1.visible = false;

        this.addChild(this.textbox_group1, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);


    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)
        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(0, 0, 1, 1, 0, 0, 0)



        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
