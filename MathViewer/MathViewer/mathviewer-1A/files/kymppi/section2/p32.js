(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p32_1.png",
            id: "p32_1"
        }]
    };

    (lib.p32_1 = function() {
        this.initialize(img.p32_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Hälften", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(236, 35);

        this.text_1 = new cjs.Text("10", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 21);

        this.text_2 = new cjs.Text("5", "bold 20px 'Myriad Pro'", "#FFFFFF");
        this.text_2.lineHeight = 24;
        this.text_2.setTransform(1165.3, 642.4);

        this.pageBottomText = new cjs.Text("förstå och kunna använda begreppet hälften", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(80.5, 654.7);

        this.text_4 = new cjs.Text("32", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 15;
        this.text_4.setTransform(39, 651);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FAAA33").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape_1.setTransform(1188, 660.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(31.1, 660.8);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 20).lineTo(610, 20).moveTo(20, 20).lineTo(20, 660).moveTo(20, 660).lineTo(610, 660);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 20, 660).drawRect(0, 0, 610, 20).drawRect(0, 660, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(70, 653.7, 195, 12, 20);
        this.text_Rect.setTransform(0, 0);



        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4, this.pageBottomText, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text_48 = new cjs.Text("Leo har hälften så många bär som Mira. Rita Leos bär. Skriv antalet.", "16px 'Myriad Pro'");
        this.text_48.lineHeight = 19;
        this.text_48.setTransform(0, 1.6);

        this.round_Rect1 = new cjs.Shape();
        this.round_Rect1.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(0, 50, 520, 482, 10);
        this.round_Rect1.setTransform(0, 0);

        this.line1 = new cjs.Shape();
        this.line1.graphics.f("#ffffff").s("#BDBEC0").ss(1).moveTo(20, 90).lineTo(495, 90).moveTo(20, 170).lineTo(495, 170).moveTo(20, 260).lineTo(495, 260).moveTo(20, 350).lineTo(495, 350).moveTo(20, 430).lineTo(495, 430);
        this.line1.setTransform(0, 0);



        this.line2 = new cjs.Shape();
        //.moveTo(250,60).lineTo(250,520)
        for (var i = 55; i < 530; i++) {
            if (i % 5 == 0) {
                this.line2.graphics.f("#BDBEC0").s("#BDBEC0").ss(0.5).arc(250, i, 0.5, 0, 2 * Math.PI);
            }
        }
        this.line2.setTransform(0, 0);

        this.instance = new lib.p32_1();
        this.instance.setTransform(-20, 12, 1, 1);

        this.textbox_1 = new cjs.Shape();
        this.textbox_1.graphics.s("#7D7D7D").ss(1).drawRect(208, 133, 23, 25);
        this.textbox_1.setTransform(0, 0);

        this.textbox_2 = new cjs.Shape();
        this.textbox_2.graphics.s("#7D7D7D").ss(1).drawRect(460, 133, 23, 25);
        this.textbox_2.setTransform(0, 0);

        this.textbox_3 = new cjs.Shape();
        this.textbox_3.graphics.s("#7D7D7D").ss(1).drawRect(208, 223, 23, 25);
        this.textbox_3.setTransform(0, 0);

        this.textbox_4 = new cjs.Shape();
        this.textbox_4.graphics.s("#7D7D7D").ss(1).drawRect(460, 223, 23, 25);
        this.textbox_4.setTransform(0, 0);

        this.textbox_5 = new cjs.Shape();
        this.textbox_5.graphics.s("#7D7D7D").ss(1).drawRect(208, 315, 23, 25);
        this.textbox_5.setTransform(0, 0);

        this.textbox_6 = new cjs.Shape();
        this.textbox_6.graphics.s("#7D7D7D").ss(1).drawRect(460, 315, 23, 25);
        this.textbox_6.setTransform(0, 0);

        this.textbox_7 = new cjs.Shape();
        this.textbox_7.graphics.s("#7D7D7D").ss(1).drawRect(208, 395, 23, 25);
        this.textbox_7.setTransform(0, 0);

        this.textbox_8 = new cjs.Shape();
        this.textbox_8.graphics.s("#7D7D7D").ss(1).drawRect(460, 395, 23, 25);
        this.textbox_8.setTransform(0, 0);

        this.textbox_9 = new cjs.Shape();
        this.textbox_9.graphics.s("#7D7D7D").ss(1).drawRect(208, 497, 23, 25);
        this.textbox_9.setTransform(0, 0);

        this.textbox_10 = new cjs.Shape();
        this.textbox_10.graphics.s("#7D7D7D").ss(1).drawRect(460, 497, 23, 25);
        this.textbox_10.setTransform(0, 0);

        this.text_5 = new cjs.Text("4", "36px 'UusiTekstausMajema'", "#6C7373");
        this.text_5.lineHeight = 40;
        this.text_5.setTransform(208, 125);

        this.addChild(this.round_Rect1, this.line1, this.text_48, this.instance, this.textbox_1, this.textbox_2,
            this.textbox_3, this.textbox_4, this.textbox_5, this.textbox_6, this.textbox_7, this.textbox_8,
            this.textbox_9, this.textbox_10, this.text_5, this.line2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513.3, 550);


    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);
        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 270, 1, 1, 0, 0, 0, 256.3, 173.6);
        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
