(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p122_1.png",
            id: "p122_1"
        }]
    };

    (lib.p122_1 = function() {
        this.initialize(img.p122_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.Symbol16 = function() {
        this.initialize();

        this.text = new cjs.Text("122", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(35, 658);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#00A3C4").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(30, 661);

        this.instance = new lib.p122_1();
        this.instance.setTransform(155, 25, 0.46, 0.46);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 1; row++) {
                if (column == 2 && row == 2) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(450 + (columnSpace * 32), 40, 27, 27);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.instance, this.shape, this.text, this.textbox_group1);

        this.numberLine1 = new lib.NumberLineType1();
        this.numberLine1.setTransform(9,0);

        this.addChild(this.numberLine1)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.TextBox = function(number, color) {
        this.initialize();
        i = number / 5;
        if (i % 10 == 0) {
            var text = new cjs.Text(number + "", " bold 16px 'Myriad Pro'", color);
        } else {
            var text = new cjs.Text(number + "", "16px 'Myriad Pro'", color);
        }
        text.textAlign = 'center';

        this.addChild(text);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);

    (lib.NumberLineType1 = function(width, count) {
        this.initialize();

        this.arrow = new cjs.Shape();
        this.arrow.graphics.f('#000000').s("#000000").ss(1).arc(0, 0, 15, 0, Math.PI * 1.5 / 3, false).lineTo(30, 30).lineTo(15, 0);
        this.arrow.setTransform(512, 175.5, 0.3, 0.3);
        this.arrow.rotation = -50;
        var positionArray = [76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 78, 0, 78, 0, 78, 0, 78, 0, 80, 0]
        for (var i = 0; i < 21; i++) {
            var height = 7;
            lineY = 168
            var colspace = 21.2;
            var icount = i;

            if (icount % 2 == 0) {
                height = 10;
                lineY = 165;
            }

            var vline = new cjs.Shape();

            if (icount % 10 == 0) {
                vline.graphics.f("#0CA3CB").s('#0CA3CB').ss(1.2).drawRect(0, 0, 0.2, height);
            } else {
                vline.graphics.f("#000000").s('#000000').ss(0.5).drawRect(0, 0, 0.1, height);
            }
            vline.setTransform(76 + i * colspace, lineY);

            this.addChild(vline);

            var text;
            var ispace = 21;
            var number = i * 5;

            if (i % 10 == 0) {
                text = new lib.TextBox(number, "#0CA3CB");
                text.setTransform(positionArray[i] + i * ispace, 192);
                this.addChild(text);
            } else if (number % 10 == 0) {
                text = new lib.TextBox(number, "#000000");
                text.setTransform(positionArray[i] + i * ispace, 192);
                this.addChild(text);
            }
        };

        this.linescale = new cjs.Shape();
        this.linescale.graphics.f("#000000").s("#000000").ss(0.7).moveTo(0, 0).lineTo(455, 0);
        this.linescale.setTransform(61, 175);

        this.addChild(this.linescale, this.arrow);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(-1, 0, 525.3, 170);

    (lib.Symbol6 = function() {
        this.initialize();


        this.text = new cjs.Text("2.", "bold 17px 'Myriad Pro'", "#00A3C4");
        this.text.setTransform(0, 0);

        this.text_1 = new cjs.Text("Jämför. Skriv > eller <.", "16px 'Myriad Pro'");
        this.text_1.setTransform(19, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 130, 10);
        this.roundRect1.setTransform(0, 0);


        var ToBeAdded = [];
        var xpos = 75;
        var ypos = 25;
        var tmpnum = [50, 80, 68, 70, 80, 98, 60, 90, 93, 70, 90, 100]
        var tmpnum1 = [60, 79, 86, 60, 82, 89, 70, 89, 95, 50, 91, 99]
        var i = 0
        var j = 0
        var xpos = 58
        var txtAftrRect = 85
        var txtBfrRect = 38
        for (var row = 0; row < 4; row++) {
            for (var column = 0; column < 3; column++) {
                var columnSpace = column;
                var tmp_Rect = new cjs.Shape();
                tmp_Rect.graphics.f('#ffffff').s("#9D9D9C").ss(0.7).drawRect(xpos + (columnSpace * 160), ypos + (row * 29), 21, 23);
                if (column == 1) {
                    txtBfrRect = 192;
                    txtAftrRect = 245
                } else if (column == 2) {
                    txtBfrRect = 352;
                    txtAftrRect = 405
                } else {
                    txtBfrRect = 35;
                    txtAftrRect = 85
                }
                if (i == 11) {
                    txtBfrRect = 348
                }
                var tempText = new cjs.Text(tmpnum[i], "16px 'Myriad Pro'");
                tempText.setTransform(txtBfrRect, 40 + (row * 30));
                var tempText1 = new cjs.Text(tmpnum1[j], "16px 'Myriad Pro'");
                tempText1.setTransform(txtAftrRect, 40 + (row * 30));
                ToBeAdded.push(tmp_Rect, tempText, tempText1);
                i++
                j++
            }
        }


        this.addChild(this.roundRect1, this.roundRect2, this.text, this.text_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 536.3, 76.6);

    (lib.Symbol3 = function() {
        this.initialize();

        this.text = new cjs.Text("Skriv talen i storleksordning.", "16px 'Myriad Pro'");
        this.text.setTransform(19, 0);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#00A3C4");
        this.text_1.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(0, 13, 510, 235, 10);
        this.roundRect1.setTransform(0, 0);
        //harizantal 2 lines
        var ToBeAdded = [];
        for (var row = 0; row < 2; row++) {
            var hrLine = new cjs.Shape();
            hrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(7, 93 + (row * 77)).lineTo(505, 93 + (row * 77));
            ToBeAdded.push(hrLine)
        };

        //Vertical lines
        var moveToypos = 17;
        var lineToypos = 90;
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 2; col++) {
                var vrLine = new cjs.Shape();
                if (row == 1) {
                    moveToypos = 97;
                    lineToypos = 167;
                } else if (row == 2) {
                    moveToypos = 175;
                    lineToypos = 245;
                } else {
                    moveToypos = 17;
                    lineToypos = 90;
                }
                vrLine.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(170 + (col * 170), moveToypos).lineTo(170 + (col * 170), lineToypos);
                ToBeAdded.push(vrLine)
            }
        }

        //color Rect box
        var xpos = 30;
        var ypos = 25;
        fColorBlue = '#C4E5F0';
        sColorBlue = '#00A3C4';
        fColorPink = '#F3CAD3';
        sColorPink = '#958FC5';
        fColorGreen = '#E1EBC5';
        sColorGreen = '#90BD22';
        fColorOrng = '#FBD3B9';
        sColorOrng = '#EB5B1B';
        fColorViolet = '#DFDDF0';
        sColorViolet = '#958FC5';
        var fColor = "";
        var sColor = "";
        var tmp_Rect = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 9; column++) {
                var columnSpace = column;
                if (column > 5) {
                    xpos = 120
                } else if (column > 2) {
                    xpos = 75
                } else {
                    xpos = 30
                }
                if (row == 0 && column > 5) {
                    fColor = fColorGreen;
                    sColor = sColorGreen
                } else if (row == 0 && column > 2) {
                    fColor = fColorBlue;
                    sColor = sColorBlue
                } else if (row == 0 && column < 3) {
                    fColor = fColorPink;
                    sColor = sColorPink
                } else if (row == 1 && column > 5) {
                    fColor = fColorPink;
                    sColor = sColorPink
                } else if (row == 1 && column > 2) {
                    fColor = fColorOrng;
                    sColor = sColorOrng
                } else if (row == 1 && column < 3) {
                    fColor = fColorViolet;
                    sColor = sColorViolet
                } else if (row == 2 && column > 5) {
                    fColor = fColorOrng;
                    sColor = sColorOrng
                } else if (row == 2 && column > 2) {
                    fColor = fColorGreen;
                    sColor = sColorGreen
                } else if (row == 2 && column < 3) {
                    fColor = fColorBlue;
                    sColor = sColorBlue
                }
                tmp_Rect.graphics.f(fColor).s(sColor).ss(0.7).drawRect(xpos + (columnSpace * 42), ypos + (row * 79), 23, 24);
                ToBeAdded.push(tmp_Rect);
            }
        }

        //Draw small lines
        var moveToxpos = 30
        var lintToxpos = 67

        var hrLine1 = new cjs.Shape();
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 9; col++) {
                if (col > 5) {
                    moveToxpos = 21;
                    lintToxpos = 58
                } else if (col > 2) {
                    moveToxpos = 17;
                    lintToxpos = 54
                } else {
                    moveToxpos = 14;
                    lintToxpos = 51
                }
                hrLine1.graphics.beginStroke("#9D9D9C").setStrokeStyle(0.8).moveTo(moveToxpos + (col * 55), 82 + (row * 78)).lineTo(lintToxpos + (col * 55), 82 + (row * 78));
                ToBeAdded.push(hrLine1)
            }
        }

        //set numbers
        var tmpnum = [60, 40, 50, 70, 30, 90, 54, 51, 59, 63, 36, 60, 72, 27, 60, 53, 93, 73, 80, 8, 88, 100, 10, 11, 19, 9, 99]
        var i = 0
        var xpos = 33
        for (var row = 0; row < 3; row++) {
            for (var column = 0; column < 9; column++) {
                var columnSpace = column;
                var tempText = new cjs.Text(tmpnum[i], "16px 'Myriad Pro'");
                if (column > 5) {
                    xpos = 124
                } else if (column > 2) {
                    xpos = 78
                } else {
                    xpos = 33
                }

                if (row == 2 && column == 1) {
                    xpos = 37
                }
                if (row == 2 && column == 3) {
                    xpos = 73
                }
                if (row == 2 && column == 7) {
                    xpos = 127
                }

                tempText.setTransform(xpos + (column * 42), 42 + (row * 80));
                ToBeAdded.push(tempText);
                i++
            }
        }
        var xpos1 = 65
        for (var row = 0; row < 3; row++) {
            for (col = 0; col < 6; col++) {
                if (col == 5) {
                    xpos1 = 220
                } else if (col ==4) {
                    xpos1 = 210
                }else if (col ==3) {
                    xpos1 = 142
                } else if(col==2){
                    xpos1 = 132
                }else if(col==1){
                    xpos1 = 65
                }else {
                    xpos1 = 57
                }
                var tempText1 = new cjs.Text("<", "16px 'Myriad Pro'");
                tempText1.setTransform(xpos1 + (col * 45), 75 + (row * 79));
                ToBeAdded.push(tempText1);
            }
        }

        this.addChild(this.roundRect1, this.text, this.text_1, this.Line_1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 533.3, 50);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol16();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol6();
        this.v1.setTransform(300.5, 440, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(300.5, 390, 1, 1, 0, 0, 0, 255.8, 0);

        this.addChild(this.v1, this.other, this.v2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
