var page_to_activate = 0;
var sectionCount = 0;
var pageCount = 0;
var table_of_contents = true;
var tools = true;
var table_of_contentsObj, toolsObj, extraObj;
var panel_links = [];

function init() {
    mathviewer.initFrames();
    mathviewer.initMaterializeComponents();
    mathviewer.compatiblitySetup();
    var d, a, b, e;
    d = $('#toolbar .features')[0];
    var image = document.createElement('img');
    image.src = "lib/images/1AB/BG_matteMeny_1B.png";
    image.style.height = "100%";
    image.style.width = "100%";
    $('#toolbar').append(image);
    a = mathviewer.addToolbarItem('', 'waves-effect waves-light vilar', 'tab1', 'modalViLar', popupFunctions, true, true);
    $(a).append($('<img src="lib/images/1AB/ViLar1.png" data-hover-on="lib/images/1AB/ViLar2.png" data-hover-off="lib/images/1AB/ViLar1.png"  alt="" class="hand" width="276" height="220" border="0" >'));

    $(d).append(a);


    a = mathviewer.addToolbarItem('', 'waves-effect waves-light viovar', 'tab1', 'modalViOvar', popupFunctions, true, true);
    $(a).append($('<img src="lib/images/1AB/viOvar1.png" data-hover-on="lib/images/1AB/viOvar2.png" data-hover-off="lib/images/1AB/viOvar1.png"  alt="" class="hand" width="276" height="150" border="0">'));
    $(d).append(a);

    a = mathviewer.addToolbarItem('', 'hand matteladan', 'tab1', 'modalMatteladan', false, true);
    $(a).append($('<img src="lib/images/1AB/matteladan.png" data-hover-on="lib/images/1AB/matteladan2.png" data-hover-off="lib/images/1AB/matteladan.png" alt="" class="hand dropdown-button" data-activates="dropdown1" width="276" height="150" border="0">'));
    $(a).css('overflow', 'visible');
    $(a).css('z-index', '2');
    $(d).append(a);

    a = mathviewer.addToolbarItem('', 'waves-effect waves-light lyssna', 'tab1', 'modalLyssna', popupFunctions, true, true);
    $(a).append($('<img src="lib/images/1AB/Lyssna1.png" data-hover-on="lib/images/1AB/Lyssna2.png" data-hover-off="lib/images/1AB/Lyssna1.png"  alt="" class="hand" width="276" height="150" border="0">'));
    $(d).append(a);

    a = mathviewer.addToolbarItem('', '', '', '', navigation_functions, false);
    $(a).append($(''));
    $(a).append($('<img src="lib/images/1AB/tom1.png" width="276" height="123" alt="">'));
    //$(d).append(a);

    a = mathviewer.addToolbarItem('', '', '', '', navigation_functions, false);
    $(a).append($('<img src="lib/images/1AB/pilLeft.png" class="btn_navigate_prev hand" alt="" name="pilLeft" width="276" height="150" border="0" id="pilLeft" style="width:50%;">'));
    $(a).append($('<img src="lib/images/1AB/pilRight.png" class="btn_navigate_next hand"   alt="" name="pilRight" width="276" height="150" border="0" id="pilRight" style="width:50%;">'));
    $(d).append(a);

    a = mathviewer.addToolbarItem('', '', '', '', navigation_functions, false);
    $(a).append($(''));
    $(a).append($('<img src="lib/images/1AB/tom2.png" width="276" height="95" alt="">'));
    // $(d).append(a);

    a = mathviewer.addToolbarItem('', '', '', '', navigation_functions, false);
    $(a).append($('<img src="lib/images/1AB/innehall.png" class="btn_home hand"  width="276" height="72" alt="">'));
    $(d).append(a);

    a = mathviewer.addToolbarItem('', '', '', '', navigation_functions, false);
    $(a).append($(''));
    $(a).append($('<img src="lib/images/1AB/LoggaUt.png" class="hand" width="276" height="77" alt="">'));
    $(d).append(a);

    a = mathviewer.addToolbarItem('', '', '', '', navigation_functions, false);
    $(a).append($(''));
    $(a).append($('<img src="lib/images/1AB/tom3.png" width="276" height="123" alt="">'));
    //$(d).append(a);


    if (!table_of_contents) {
        $('.btn_toc').addClass('disabled');
        $('.btn_toc').hide();
        $('.seperator_table_of_contents').hide();
    } else {
        $('#toolbar').hide();
    }



    for (a = 0; a < pageInfo.length; a++) {
        if (pageInfo[a].preload == true || $('.page').length === 0)
            $('#toolbar').before('<div class="page"></div>');
    }


    if (!$('html').hasClass('d2l_fixed')) {
        window.addEventListener('orientationchange', updateProperties);
        window.onresize = updateProperties;
    }
    updateProperties();

    setNavigations();

    page_to_activate = 0;

    if (getVariable('page') !== 'false') {
        downloadPage(getVariable('page'));
    } else {
        downloadPage(pageInfo[0].pages[sectionCount][pageCount]);
    }
    mathviewer.postRenderFunctions();
}

function setNavigations() {
    sectionCount = 0;
    pageCount = 0;

    checkNavigations();
}

function navigation_functions(e) {
    var btn = e.target;

    if (btn.style.opacity) {
        if (btn.style.opacity < 1)
            return;
    }

    if ($(btn).hasClass('btn_toc'))
        openCloseTOC();
    else if ($(btn).hasClass('btn_home'))
        navigate_FrontPage();
    else if ($(btn).hasClass('btn_navigate_prev'))
        navigate_prev();
    else if ($(btn).hasClass('btn_navigate_next'))
        navigate_next();


}

function navigate_FrontPage() {
    var sectionUrl = 'section=' + sectionCount;

    if (sectionCount > 0 || pageCount > 0) {
        sectionCount = 0;
        pageCount = 0;
    } else
        return;


    if (pageInfo[0].pages[sectionCount][pageCount].indexOf('?') === -1)
        sectionUrl = '?' + sectionUrl;
    else
        sectionUrl = '&' + sectionUrl;


    downloadPage(pageInfo[0].pages[sectionCount][pageCount], sectionUrl);
}

function navigate_prev() {
    if ($('.btn_navigate_prev').hasClass('disabled'))
        return;

    if (pageCount > 1 && pageInfo[0].pages[sectionCount][pageCount] === pageInfo[0].pages[sectionCount][pageCount - 1]) {
        pageCount -= 2;
    } else if (pageCount > 0) {
        pageCount--;
    } else if (sectionCount > 0) {
        sectionCount--;
        pageCount = pageInfo[0].pages[sectionCount].length - 1;
    } else
        return;


    downloadPage(pageInfo[0].pages[sectionCount][pageCount]);
}

function navigate_next() {
    if ($('.btn_navigate_next').hasClass('disabled'))
        return;

    if (pageCount < pageInfo[0].pages[sectionCount].length - 2 && pageInfo[0].pages[sectionCount][pageCount] === pageInfo[0].pages[sectionCount][pageCount + 1]) {
        pageCount += 2;
    } else if (pageCount < pageInfo[0].pages[sectionCount].length - 1) {
        pageCount++;
    } else if (sectionCount < pageInfo[0].pages.length - 1) {
        sectionCount++;
        pageCount = 0;
    } else
        return;

    downloadPage(pageInfo[0].pages[sectionCount][pageCount]);
}

function checkNavigations() {
    $('.btn_navigate_prev').removeClass('disabled');
    $('.btn_navigate_next').removeClass('disabled');

    if (sectionCount == 0 && pageCount == 0)
        $('.btn_navigate_prev').addClass('disabled');
    if (sectionCount == pageInfo[0].pages.length - 1 && pageCount == pageInfo[0].pages[sectionCount].length - 1)
        $('.btn_navigate_next').addClass('disabled');
}

function downloadPage(file, variables) {
    closeExtras();

    openCloseTOC(false);

    variables = variables || '';

    var a, aa;

    sectionCount = -1;
    pageCount = -1;
    for (a = 0; a < pageInfo[0].pages.length; a++) {
        for (aa = 0; aa < pageInfo[0].pages[a].length; aa++) {
            if (file == pageInfo[0].pages[a][aa]) {
                sectionCount = a;
                pageCount = aa;
                break;
            }
        }
        if (sectionCount > -1)
            break;
    }

    if (sectionCount == -1 || pageCount == -1) {
        alert('File not found');
        sectionCount = pageCount = 0;
    }
    preloaderOn();
    $('.btn_home').removeClass('selected');
    if (sectionCount === 0 && pageCount === 0) {
        $('.btn_home').addClass('selected');
    }

    var pagelinks = [];
    $.each(pageInfo, function(a, obj) {
        if (obj.pages[sectionCount][pageCount] != undefined && obj.pages[sectionCount][pageCount] != 'undefined') {
            pagelinks.push({
                o: obj.title,
                t: (obj.pages[sectionCount][pageCount].indexOf('files') > -1 ? '' : obj.folder + '/') + obj.pages[sectionCount][pageCount].replace('/index.html', obj.end + '/index.html'),
                k: 'page' + a
            });
        }
    });
    if (pagelinks.length > 1)
        addToolBarLinks('top', pagelinks);


    if (window.toolbarLinksTop)
        addToolBarLinks('top', window.toolbarLinksTop);
    if (window.toolbarLinksBottom)
        addToolBarLinks('bottom', window.toolbarLinksBottom);

    if (pageInfo[0].preload === true) {

        $.each(pageInfo, function(a, obj) {
            var page = $('.page').eq(a);
            $(page).html('<iframe class="pageContent" src="" />');
            if (obj.pages[sectionCount][pageCount] != undefined && obj.pages[sectionCount][pageCount] != 'undefined' && obj.pages[sectionCount][pageCount] != '') {
                $(page).html('<iframe class="pageContent" src="' + (obj.pages[sectionCount][pageCount].indexOf('files') > -1 ? '' : obj.folder + '/') + obj.pages[sectionCount][pageCount].replace('/index.html', obj.end + '/index.html') + variables + '" />');
            } else {
                $(page).hide();
                $(page).html('<iframe class="pageContent" src="" />');
                page_to_activate = 0;
            }
        });
    } else {
        if (typeof pageInfo[page_to_activate].pages[sectionCount] == 'string')
            page_to_activate = 0;
        else if (pageInfo[page_to_activate].pages[sectionCount][pageCount] != undefined && pageInfo[page_to_activate].pages[sectionCount][pageCount] != 'undefined' && pageInfo[page_to_activate].pages[sectionCount][pageCount] != '')
            page_to_activate = 0;
        downloadPageactivate(variables);
    }

    try {
        window.history.pushState('', 'html', getURL() + 'index.html?page=' + pageInfo[0].pages[sectionCount][pageCount]);
    } catch (e) {}

    viewPage(page_to_activate);

    checkNavigations();
}

function downloadPageactivate(variables) {
    variables = variables || '';

    preloaderOn();

    $('.page').html('<iframe class="pageContent" src="' + (pageInfo[page_to_activate].pages[sectionCount][pageCount].indexOf('files') > -1 ? '' : pageInfo[page_to_activate].folder + '/') + pageInfo[page_to_activate].pages[sectionCount][pageCount].replace('/index.html', pageInfo[page_to_activate].end + '/index.html') + variables + '" />');
}

function pageloaded(win) {
    var allloaded = true;

    $.each($('.page'), function(a, page) {
        try {
            var item = $(page).find('.pageContent')[0];
            if ($(item).attr('src') != undefined && $(item).attr('src') != 'undefined' && $(item).attr('src') != '') {
                var item = $(page).find('.pageContent')[0];
                if (item.contentWindow.loaded === true) {
                    if (a === page_to_activate || pageInfo[0].preload !== true) {
                        try {
                            $($('.page').eq(a).find('.pageContent')[0].contentWindow).focus();
                        } catch (e) {}

                        setTimeout(function() {
                            $(page).show();
                            if (typeof item.contentWindow.activate)
                                item.contentWindow.activate(true);
                        }, 50);
                    } else {
                        setTimeout(function() {
                            $(page).hide();
                            if (item.contentWindow.activate)
                                item.contentWindow.activate(false);
                        }, 50);
                    }
                } else
                    allloaded = false;
            } else
                $(page).hide();
        } catch (e) {}
    });

    if (allloaded)
        preloaderOff();
}

function iframeloaded(win) {
    if (extraObj) {
        if (win === extraObj.win()) {
            extraloaded();
            return;
        }
    }
    pageloaded(win);
}


function viewPage(s) {
    page_to_activate = s;


    $('.page').show();

    if (pageInfo[0].preload === true) {

        $.each($('.page'), function(a, page) {
            try {
                var item = $(page).find('.pageContent')[0];
                if (item === undefined)
                    $(page).hide();
                else if (item.contentWindow.loaded === true) {
                    if (a !== page_to_activate) {
                        $(page).hide();

                        item.contentWindow.activate(false);
                    } else {
                        item.contentWindow.activate(true);
                    }
                }
            } catch (e) {}
        });

        try {
            $('.page').css('z-index', '-1');
            $('.page').eq(page_to_activate).css('z-index', '');
            $($('.page').eq(page_to_activate).find('.pageContent')[0].contentWindow).focus();
        } catch (e) {}
    }

    $.each($('.toolbarLink'), function(a, item) {
        if ($(item).attr('pagebtn') !== undefined) {
            $(item).removeClass('selected');
            $(item).css('background-color', '');
            if ($(item).attr('pagebtn') === ('page' + page_to_activate)) {
                $(item).addClass('selected');


            }
        }
    })

    updateProperties();
}


function openInstructions() {

}


function openCloseTOC(bool) {
    if (bool == false || table_of_contentsObj) {
        if (table_of_contentsObj) {
            table_of_contentsObj.close();
            table_of_contentsObj = undefined;
        }
    } else
        table_of_contentsObj = new TableOfContents();
}




function addToolBarLinks(toolbarPosition, arr) {
    if (arr == undefined)
        return;

    var bar, btns, btn;

    if (toolbarPosition == 'top')
        bar = $('#toolbar')[0];
    else
        bar = $('#toolbar_bottom')[0];





    btns = $(bar).find('.toolbarLinks')[0];
    if (btns === undefined) {
        btns = document.createElement('div');
        $(btns).addClass('toolbarLinks');
        $(bar).append(btns);
    } else {

        $.each(arr, function(a, item) {
            var o = item.o.stripTags();
            $('.toolbarLink_' + o).css('display', 'none');
        });
    }

    $.each(arr, function(a, item) {
        panel_links.push(item.t);

        btn = document.createElement('a');
        if (item.type != undefined) {
            if (item.type === 'popup')
                $(btn).html(item.o);
            else if (item.type === 'dd')
                $(btn).html(item.o + '<div class="arrow">></div>');
        } else
            $(btn).html(item.o);

        $(btn).attr('pagebtn', item.k);
        $(btn).attr('l_index', panel_links.length - 1);
        $(btn).attr('type', (item.type == undefined ? '' : item.type));
        $(btn).addClass('toolbarLink');
        $(btn).addClass('toolbarLink_' + item.o.stripTags());
        $(btns).append(btn);
        $(btn).on('click', linkFunctions);
        new NoClickDelay(btn);
    });


    function linkFunctions(e) {
        var btn = e.currentTarget;
        var i = parseFloat($(btn).attr('l_index'));

        if ($(btn).attr('pagebtn') != undefined) {
            viewPage(parseFloat($(btn).attr('pagebtn').substr(4)));
            if (pageInfo[0].preload !== true)
                downloadPageactivate();
        } else {
            if ($(btn).attr('type') === 'popup') {
                openPopup(panel_links[i]);
            } else if ($(btn).attr('type') === 'dd') {
                dropdownMenu(btn, panel_links[i], $(btn).attr('type'));
            } else {
                $('.toolbarLink:not([pagebtn])').removeClass('selected');
                $('.toolbarLink[l_index="' + i + '"]').addClass('selected');



                extraOpen(panel_links[i]);
            }
        }
    }
}

function removeToolbarLinks() {
    $('.toolbarLinks').remove();

    panel_links = [];

    removePageMenu();
}


function updateProperties() {
    var m = screenProperties();

    var pageX, pageY, pageW, pageH;
    var page = $('.page .pageContent');
    page.removeClass('toc');
    var istableOfContents = false;
    var currentPage = getVariable('page');
    if (currentPage && currentPage.indexOf('toc') > -1) {
        istableOfContents = true;
        page.addClass('toc');
    }
    if (m.width <= 910 || m.height <= 540) {
        $('html').addClass('mobile');
        pageX = 0;
        pageY = $('#toolbar_Mini').height();
        pageW = m.width - pageX;
        pageH = m.height - pageY;
        var isiPad = (navigator.userAgent.match(/iPad/i) != null);
        if (isiPad) { //sathish
            pageX = 120;
            pageY = 20;
            pageW = 600;
            pageH = 570;
            $('#toolbar').show();
        }
    } else {
        $('html').removeClass('mobile');

        //Mohamed
        pageX = 0;

        if (!istableOfContents) {
            $('#toolbar').show();
            pageX = $('#toolbar').width();
        } else {
            $('#toolbar').hide();
        }

        pageY = 20;
        pageW = m.width - pageX;
        pageH = m.height - pageY * 2;


    }
    $('.overlay').hide();
    $('.page').css('width', pageW + 'px');
    $('.page').css('height', pageH + 'px');
    $('.page').css('left', pageX + 'px');
    $('.page').css('top', pageY + 'px');
    if (istableOfContents) {
        $('#toolbar').hide();
        $('.overlay').show();
        $('.page').css('width', "100%");
        $('.page').css('height', "100%");
        $('.page').css('left', 0 + 'px');
        $('.page').css('top', 0 + 'px');
    }
    screenPropertiesUpdated = false;

    $.each($('.page'), function(a, page) {
        var item = $(page).find('.pageContent')[0];
        try {
            if (typeof item.contentWindow.updateProperties != 'undefined' && (a === page_to_activate || pageInfo[0].preload !== true)) {
                item.contentWindow.updateProperties();

                screenPropertiesUpdated = true;
            }
        } catch (e) {}
    });



    if (extraObj)
        extraObj.updateProperties();



    if (table_of_contentsObj)
        table_of_contentsObj.updateProperties();



    if (toolsObj)
        toolsObj.updateProperties();
}

function screenProperties() {
    var w = window.innerWidth ? window.innerWidth : $(window).width();
    var h = window.innerHeight ? window.innerHeight : $(window).height();

    var focusInput, focusWindow;
    if (document.activeElement) {
        if ($(document.activeElement).is('input') || $(document.activeElement).is('textarea') || $(document.activeElement).is('*[contenteditable="true"]')) {
            focusInput = document.activeElement;
            focusWindow = window;
        }
    }
    $.each($('iframe'), function(a, iframe) {
        try {
            var elem = $(iframe)[0].contentWindow.document.activeElement;
            if (elem) {
                if ($(elem).is('input') || $(elem).is('textarea') || $(elem).is('*[contenteditable="true"]')) {
                    focusInput = elem;
                    focusWindow = $(iframe)[0].contentWindow;
                }
            }
        } catch (e) {}
    });
    $('html').css({
        'overflow-y': '',
        '-webkit-overflow-scrolling': '',
        '-ms-touch-action': '',
        'touch-action': ''
    });
    if (focusInput) {
        if (window.__last_properties.focusInput != focusInput) {
            if (typeof focusWindow.focusInputOut == 'function')
                focusWindow.focusInputOut(focusInput, window);
        }
        $('html').css({
            'overflow-y': 'auto',
            '-webkit-overflow-scrolling': 'auto',
            '-ms-touch-action': 'auto',
            'touch-action': 'auto'
        });
        if (window.__last_properties) {
            w = window.__last_properties.w;
            h = window.__last_properties.h;
        }
    }
    window.__last_properties = {
        w: w,
        h: h,
        focusInput: focusInput
    };



    return {
        width: w,
        height: h
    };
}

function focusInputOut(ip, win) {
    $(ip).one('focusout', doIt);

    function doIt() {
        setTimeout(win.updateProperties, 500);
    }
}

function iframeProperties(win) {
    var m = screenProperties();
    $.each($('iframe'), function(a, item) {
        try {
            if (item.contentWindow === win) {
                var $parent = $(item).parent();
                m.width = parseFloat($parent.css('width').replace('px', ''));
                m.height = parseFloat($parent.css('height').replace('px', ''));
            }
        } catch (e) {}
    });
    return m;
}

function pageProperties() {
    var m = screenProperties();
    var $page = $('.page').eq(0);
    m.width = parseFloat($page.css('width').replace('px', ''));
    m.height = parseFloat($page.css('height').replace('px', ''));
    return m;
}

function extrasProperties() {
    var m = screenProperties();
    if (extraObj)
        m = extraObj.properties();
    return m;
}


function trace(str) {
    var t = $('#trace')[0];
    if (t === undefined) {
        t = document.createElement('div');
        $(t).attr('id', 'trace');
        $(t).css({
            'color': '#000000',
            'position': 'absolute',
            'left': '0',
            'top': '0',
            'z-index': '1000'
        });
    }
    $('body').append(t);
    $(t).html(str.toString());
}


function extraOpen(obj, index, title) {
    if (!extraObj)
        extraObj = new Extra();

    extraObj.open(obj, index, title);

    $('.page').hide();
}

function closeExtras() {
    if (!extraObj)
        return;

    extraObj.close();
    extraObj = undefined;

    $('.toolbarLink:not([pagebtn])').removeClass('selected');
    $('.toolbarLink:not([pagebtn])').css('background-color', '');

    viewPage(page_to_activate);
}

function extraloaded() {
    if (!extraObj)
        return;

    extraObj.loaded();
}


function openPopup(file) {
    window.open(file, 'popup_addnl_material', 'scrollbars=yes,menubar=no,width=920,height=600,resizable=yes,toolbar=no,location=no,status=no');
}

function preloaderOn() {
    var preloader = $('#preloader')[0];
    $(preloader).css('display', 'inline');

    if (extraObj) {
        var y = parseFloat(($('.extra').css('top')).replace('px', ''));
        y += $('.extra').height() / 2;
        y += -10;
        $(preloader).css('top', y + 'px');
    } else
        $(preloader).css('top', '50%');
    $(preloader).css('z-index', '10000');
}

function preloaderOff() {
    var preloader = $('#preloader')[0];
    $(preloader).css('display', 'none');
}

function getURL() {
    var tempUrl = window.location.href.split('?')[0];
    var tempInt = tempUrl.lastIndexOf('/') + 1;
    tempUrl = tempUrl.substring(0, tempInt);
    return tempUrl;
}

function getVariable(str) {
    var a, tempArr = window.location.href.split('?');
    if (tempArr.length == 1)
        return 'false';
    tempArr = tempArr[1].split('&');
    for (a = 0; a < tempArr.length; a++) {
        if (tempArr[a].indexOf(str + '=') > -1)
            return tempArr[a].split(str + '=')[1];
    }
    return 'false';
}

function setColor(j, t) {
    var color = '';
    var aa = 0;
    var j = j == undefined ? sectionCount : j;
    var t = t || '';
    $.each(pages, function(a, obj) {
        if (obj.a === 0) {
            if (aa === j) {
                if (t == 'background')
                    color = obj.backgroundcolor;
                else
                    color = obj.color;
                return false;
            }
            aa++;
        }
    });
    return color;
}

String.prototype.replaceAt = function(index, character) {
    return this.substr(0, index) + character + this.substr(index + 1);
}
String.prototype.stripTags = function() {
    var orgStr = this;
    var newStr = orgStr.replace(/(<([^>]+)>)/ig, '');
    return newStr;
}


function NoClickDelay(el) {
    this.element = typeof el == 'object' ? el : document.getElementById(el);
    if (window.Touch)
        this.element.addEventListener('touchstart', this, false);
}
NoClickDelay.prototype = {
    handleEvent: function(e) {
        switch (e.type) {
            case 'touchstart':
                this.onTouchStart(e);
                break;
            case 'touchmove':
                this.onTouchMove(e);
                break;
            case 'touchend':
                this.onTouchEnd(e);
                break;
        }
    },
    onTouchStart: function(e) {
        e.preventDefault();
        this.moved = false;
        this.x = e.targetTouches[0].clientX;
        this.y = e.targetTouches[0].clientY;

        this.theTarget = document.elementFromPoint(this.x, this.y);
        if (this.theTarget.nodeType == 3) this.theTarget = theTarget.parentNode;
        this.theTarget.className += ' hover';
        this.element.addEventListener('touchmove', this, false);
        this.element.addEventListener('touchend', this, false);

        var theEvent = document.createEvent('MouseEvents');
        theEvent.initEvent('mousedown', true, true);
        this.theTarget.dispatchEvent(theEvent);
    },
    onTouchMove: function(e) {
        var x = e.targetTouches[0].clientX;
        var y = e.targetTouches[0].clientY;
        if (Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2)) > 50) {
            this.moved = true;
            this.theTarget.className = this.theTarget.className.replace(/ hover/gi, '');
        } else {
            if (this.moved == true) {
                this.moved = false;
                this.theTarget.className += ' hover';
            }
        }
    },
    onTouchEnd: function(e) {
        this.element.removeEventListener('touchmove', this, false);
        this.element.removeEventListener('touchend', this, false);
        if (!this.moved && this.theTarget) {
            this.theTarget.className = this.theTarget.className.replace(/ hover/gi, '');
            var theEvent = document.createEvent('MouseEvents');
            theEvent.initEvent('click', true, true);
            this.theTarget.dispatchEvent(theEvent);

            theEvent = document.createEvent('MouseEvents');
            theEvent.initEvent('mouseup', true, true);
            this.theTarget.dispatchEvent(theEvent);
        }
        this.theTarget = undefined;
    }
};
NoClickDelay.prototype.offset = function() {
    var _x = _y = 0;
    var obj = document;

    if (obj.offsetParent) {
        do {
            _x += obj.offsetLeft;
            _y += obj.offsetTop;
        } while (obj = obj.offsetParent);
    }
    return {
        x: _x,
        y: _y
    };
}
$(document).ready(init);
