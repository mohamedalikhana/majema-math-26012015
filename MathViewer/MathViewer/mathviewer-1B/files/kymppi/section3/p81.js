(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p81_1.png",
            id: "p81_1"
        }, {
            src: "images/p81_2.png",
            id: "p81_2"
        }, {
            src: "images/p81_3.png",
            id: "p81_3"
        }]
    };

    (lib.p81_1 = function() {
        this.initialize(img.p81_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p81_2 = function() {
        this.initialize(img.p81_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);

    (lib.p81_3 = function() {
        this.initialize(img.p81_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 658, 248);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("81", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.setTransform(552, 656);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#958FC5").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol2 = function() {
        this.initialize();
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#B3B3B3').drawRoundRect(9, 42, 523, 547, 13);
        this.roundRect1.setTransform(0, 0);

        this.instance_2 = new lib.p81_1();
        this.instance_2.setTransform(40, 224, 0.46, 0.46);

        this.text = new cjs.Text("Vattenhjulen", "18px 'MyriadPro-Semibold'", "#958FC5");
        this.text.setTransform(7, 13);
        this.text_1 = new cjs.Text("Spel för 2 eller fler.", "16px 'Myriad Pro'", "#958FC5");
        this.text_1.setTransform(7, 33);

        this.instance = new lib.p81_3();
        this.instance.setTransform(423, 2, 0.4, 0.4);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#958FC5').drawRoundRect(338, 3, 190, 33, 6);
        this.roundRect2.setTransform(0, 0);

        this.text_7 = new cjs.Text("Ni behöver", "16px 'Myriad Pro'");
        this.text_7.setTransform(346, 24);

        this.mailRect = new cjs.Shape();
        this.mailRect.graphics.f("#ffffff").s('#000000').drawRoundRect(380, 495, 70, 70, 0)
        this.mailRect.setTransform(0, 0);

        this.label1 = new cjs.Text("MÅL", "18px 'Myriad Pro'");
        this.label1.setTransform(395, 520);

        this.label3 = new cjs.Text("7", "18px 'MyriadPro-Semibold'");
        this.label3.setTransform(405, 545);

        this.startRect = new cjs.Shape();
        this.startRect.graphics.f("#ffffff").s('#000000').drawRoundRect(27, 233, 60, 25, 0)
        this.startRect.setTransform(0, 0);

        this.label2 = new cjs.Text("START", "18px 'Myriad Pro'");
        this.label2.setTransform(30, 250);

        this.text_2 = new cjs.Text("• Slå 2 tärningar.", "16px 'Myriad Pro'");
        this.text_2.setTransform(15, 70);

        this.text_3 = new cjs.Text("• Subtrahera tärningstalen från talet 13,", "16px 'Myriad Pro'");
        this.text_3.setTransform(15, 92);

        this.text_4 = new cjs.Text("13 –", "16px 'Myriad Pro'");
        this.text_4.setTransform(24, 114);

        this.text_11 = new cjs.Text("–", "16px 'Myriad Pro'");
        this.text_11.setTransform(77, 114);

        this.text_12 = new cjs.Text("= 6 .", "16px 'Myriad Pro'");
        this.text_12.setTransform(110, 114);

        this.instance3 = new lib.p81_2();
        this.instance3.setTransform(57, 100.5, 0.4, 0.43);

        this.text_5 = new cjs.Text("• Gå till nästa hjul om svaret finns där.", "16px 'Myriad Pro'");
        this.text_5.setTransform(15, 140);

        this.text_6 = new cjs.Text("• Om inte står du kvar.", "16px 'Myriad Pro'");
        this.text_6.setTransform(15, 162);

        this.text_9 = new cjs.Text("• Målrutan kan du bara nå när du får svaret 7.", "16px 'Myriad Pro'");
        this.text_9.setTransform(15, 184);

        this.text_10 = new cjs.Text("• Den som först når MÅL vinner.", "16px 'Myriad Pro'");
        this.text_10.setTransform(15, 206);
        // Assign numbers in bule color
        this.numtext_1 = new cjs.Text("7", "18px 'Myriad Pro'");
        this.numtext_1.setTransform(110, 299);
        this.numtext_2 = new cjs.Text("3", "18px 'Myriad Pro'");
        this.numtext_2.setTransform(165, 299);
        this.numtext_3 = new cjs.Text("9", "18px 'Myriad Pro'");
        this.numtext_3.setTransform(110, 265);
        this.numtext_4 = new cjs.Text("1", "18px 'Myriad Pro'");
        this.numtext_4.setTransform(165, 265);
        this.numtext_5 = new cjs.Text("5", "18px 'Myriad Pro'");
        this.numtext_5.setTransform(138, 312);
        this.numtext_32 = new cjs.Text("11", "18px 'Myriad Pro'");
        this.numtext_32.setTransform(135, 255);

        this.numtext_6 = new cjs.Text("3", "18px 'Myriad Pro'");
        this.numtext_6.setTransform(245, 299);
        this.numtext_7 = new cjs.Text("1", "18px 'Myriad Pro'");
        this.numtext_7.setTransform(300, 299);
        this.numtext_8 = new cjs.Text("4", "18px 'Myriad Pro'");
        this.numtext_8.setTransform(245, 265);
        this.numtext_9 = new cjs.Text("6", "18px 'Myriad Pro'");
        this.numtext_9.setTransform(300, 265);
        this.numtext_10 = new cjs.Text("2", "18px 'Myriad Pro'");
        this.numtext_10.setTransform(272, 312);
        this.numtext_33 = new cjs.Text("5", "18px 'Myriad Pro'");
        this.numtext_33.setTransform(272, 255);

        this.numtext_11 = new cjs.Text("9", "18px 'Myriad Pro'");
        this.numtext_11.setTransform(375, 299);
        this.numtext_12 = new cjs.Text("11", "18px 'Myriad Pro'");
        this.numtext_12.setTransform(427, 299);
        this.numtext_13 = new cjs.Text("8", "18px 'Myriad Pro'");
        this.numtext_13.setTransform(375, 265);
        this.numtext_14 = new cjs.Text("6", "18px 'Myriad Pro'");
        this.numtext_14.setTransform(427, 265);
        this.numtext_15 = new cjs.Text("10", "18px 'Myriad Pro'");
        this.numtext_15.setTransform(400, 312);
        this.numtext_34 = new cjs.Text("7", "18px 'Myriad Pro'");
        this.numtext_34.setTransform(402, 255);

        this.numtext_16 = new cjs.Text("10", "18px 'Myriad Pro'");
        this.numtext_16.setTransform(172, 412);
        this.numtext_17 = new cjs.Text("6", "18px 'Myriad Pro'");
        this.numtext_17.setTransform(232, 412);
        this.numtext_18 = new cjs.Text("11", "18px 'Myriad Pro'");
        this.numtext_18.setTransform(173, 380);
        this.numtext_19 = new cjs.Text("4", "18px 'Myriad Pro'");
        this.numtext_19.setTransform(232, 380);
        this.numtext_20 = new cjs.Text("8", "18px 'Myriad Pro'");
        this.numtext_20.setTransform(205, 425);
        this.numtext_35 = new cjs.Text("2", "18px 'Myriad Pro'");
        this.numtext_35.setTransform(205, 370);

        this.numtext_21 = new cjs.Text("7", "18px 'Myriad Pro'");
        this.numtext_21.setTransform(312, 412);
        this.numtext_22 = new cjs.Text("3", "18px 'Myriad Pro'");
        this.numtext_22.setTransform(365, 412);
        this.numtext_23 = new cjs.Text("9", "18px 'Myriad Pro'");
        this.numtext_23.setTransform(312, 380);
        this.numtext_24 = new cjs.Text("1", "18px 'Myriad Pro'");
        this.numtext_24.setTransform(365, 380);
        this.numtext_25 = new cjs.Text("5", "18px 'Myriad Pro'");
        this.numtext_25.setTransform(337, 425);
        this.numtext_36 = new cjs.Text("11", "18px 'Myriad Pro'");
        this.numtext_36.setTransform(332, 370);

        this.numtext_26 = new cjs.Text("8", "18px 'Myriad Pro'");
        this.numtext_26.setTransform(245, 528);
        this.numtext_27 = new cjs.Text("4", "18px 'Myriad Pro'");
        this.numtext_27.setTransform(300, 528);
        this.numtext_28 = new cjs.Text("10", "18px 'Myriad Pro'");
        this.numtext_28.setTransform(240, 497);
        this.numtext_29 = new cjs.Text("2", "18px 'Myriad Pro'");
        this.numtext_29.setTransform(299.5, 495);
        this.numtext_30 = new cjs.Text("6", "18px 'Myriad Pro'");
        this.numtext_30.setTransform(270, 540);
        this.numtext_37 = new cjs.Text("11", "18px 'Myriad Pro'");
        this.numtext_37.setTransform(267, 485);



        this.addChild(this.roundRect2, this.roundRect1, this.instance_2, this.instance3, this.startRect, this.mailRect);
        this.addChild(this.text_1, this.label1, this.label2, this.label3, this.text_11, this.text_12);
        this.addChild(this.text, this.text_2, this.instance, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8, this.text_9, this.text_10);
        this.addChild(this.numtext_1, this.numtext_2, this.numtext_3, this.numtext_4, this.numtext_5)
        this.addChild(this.numtext_6, this.numtext_7, this.numtext_8, this.numtext_9, this.numtext_10, this.numtext_11, this.numtext_12, this.numtext_13, this.numtext_14, this.numtext_15)
        this.addChild(this.numtext_16, this.numtext_17, this.numtext_18, this.numtext_19, this.numtext_20, this.numtext_21, this.numtext_22, this.numtext_23, this.numtext_24, this.numtext_25)
        this.addChild(this.numtext_26, this.numtext_27, this.numtext_28, this.numtext_29, this.numtext_30, this.numtext_31)
        this.addChild(this.numtext_32, this.numtext_33, this.numtext_34, this.numtext_35, this.numtext_36, this.numtext_37)

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 513, 532);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol1();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol2();
        this.v1.setTransform(296, 265, 1, 1, 0, 0, 0, 256.3, 217.9);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
