(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p41_1.png",
            id: "p41_1"
        }, {
            src: "images/p41_2.png",
            id: "p41_2"
        }]
    };

    // symbols:

    (lib.p41_1 = function() {
        this.initialize(img.p41_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p41_2 = function() {
        this.initialize(img.p41_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();


        this.top_textbox_group = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_group.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(465 + (i * 32), 33, 27, 27);
        }
        this.top_textbox_group.setTransform(0, 0);

        this.pageNoRight = new cjs.Text("41", "12px 'Myriad Pro'", "#FFFFFF");
        this.pageNoRight.lineHeight = 18;
        this.pageNoRight.setTransform(554, 648);
        this.pageNoRightBackground = new cjs.Shape();
        this.pageNoRightBackground.graphics.f("#7AC729").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.pageNoRightBackground.setTransform(579, 660.8);

        this.pageTitle = new cjs.Text("Vi skriver addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.pageTitle.lineHeight = 29;
        this.pageTitle.setTransform(95.5, 19);

        this.chapterNumber = new cjs.Text("13", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.chapterNumber.lineHeight = 34;
        this.chapterNumber.setTransform(50.7, 15.1);

        this.chapterNumberBackground = new cjs.Shape();
        this.chapterNumberBackground.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.chapterNumberBackground.setTransform(43.6, 23.5);

        this.addChild(this.pageNoRightBackground, this.pageNoRight, this.top_textbox_group);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p41_1();
        this.instance.setTransform(0, 26, 0.37, 0.37);
        this.text = new cjs.Text("Hur många fiskar är det tillsammans?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(23, 7);
        this.text_1 = new cjs.Text("2.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(4, 7);
        var textToBeDisplayed1 = ['2', '+', '1', '=', '3'];

        this.textBoxGroup1 = new cjs.Shape();

        for (var i = 0; i < 5; i++) {
            this.textBoxGroup1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 120, 20, 21);
        }
        this.textBoxGroup1.setTransform(0, 0);

        var textToBeDisplayed2 = ['3', '+', '1', '=', '4'];
        this.textBoxGroup2 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 120, 20, 21);
        }
        this.textBoxGroup2.setTransform(0, 0);

        this.textBoxGroup3 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup3.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 243, 20, 21);
        }
        this.textBoxGroup3.setTransform(0, 0);

        this.textBoxGroup4 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup4.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 243, 20, 21);
        }
        this.textBoxGroup4.setTransform(0, 0);

        this.textBoxGroup5 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup5.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(335 + (i * 20), 366, 20, 21);
        }
        this.textBoxGroup5.setTransform(0, 0);

        this.textBoxGroup6 = new cjs.Shape();
        for (var i = 0; i < 5; i++) {
            this.textBoxGroup6.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(75 + (i * 20), 366, 20, 21);
        }
        this.textBoxGroup6.setTransform(0, 0);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.s('#7d7d7d').drawRoundRect(0, 0, ((1394 * 0.5) - 10) * 0.365, ((1004 * (1 / 3)) - 16) * 0.365, 10);
        this.roundRect1.setTransform(3, 31);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1394 * 0.5) + 8) * 0.37, 31);

        this.roundRect3 = this.roundRect1.clone(true);
        this.roundRect3.setTransform(3, 154);

        this.roundRect4 = this.roundRect1.clone(true);
        this.roundRect4.setTransform(((1394 * 0.5) + 8) * 0.37, 154);

        this.roundRect5 = this.roundRect1.clone(true);
        this.roundRect5.setTransform(3, 277);

        this.roundRect6 = this.roundRect1.clone(true);
        this.roundRect6.setTransform(((1394 * 0.5) + 8) * 0.37, 277);

        this.addChild(this.roundRect1,this.roundRect2,  this.roundRect3, this.roundRect4, this.roundRect5, this.roundRect6, this.instance, this.textBoxGroup1, this.textBoxGroup2, this.textBoxGroup3, this.textBoxGroup4, this.textBoxGroup5, this.textBoxGroup6, this.text, this.text_1);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 380);


    (lib.Symbol3 = function() {
        this.initialize();
        this.text = new cjs.Text("Addera.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(23, 7.5);
        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#7AC729");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(4, 7.5);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRoundRect(5, 29, 510, 157, 10);
        this.roundRect1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 5; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(126 + (columnSpace * 150), 40 + (row * 29), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.label1 = new cjs.Text("1  +  1  =  ", "16px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(60, 43);
        this.label2 = new cjs.Text("1  +  0  =  ", "16px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(60, 73);
        this.label3 = new cjs.Text("1  +  2  =  ", "16px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(60, 103);
        this.label4 = new cjs.Text("1  +  3  =  ", "16px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(60, 133);
        this.label5 = new cjs.Text("1  +  4  =  ", "16px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(60, 162);


        this.label6 = new cjs.Text("2  +  0  =  ", "16px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(210, 43);
        this.label7 = new cjs.Text("2  +  1  =  ", "16px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(210, 73);
        this.label8 = new cjs.Text("2  +  3  =  ", "16px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(210, 103);
        this.label9 = new cjs.Text("0  +  2  =  ", "16px 'Myriad Pro'");
        this.label9.lineHeight = 19;
        this.label9.setTransform(210, 133);
        this.label10 = new cjs.Text("0  +  3  =  ", "16px 'Myriad Pro'");
        this.label10.lineHeight = 19;
        this.label10.setTransform(210, 162);

        this.label11 = new cjs.Text("3  +  0  =  ", "16px 'Myriad Pro'");
        this.label11.lineHeight = 19;
        this.label11.setTransform(360, 43);
        this.label12 = new cjs.Text("3  +  1  =  ", "16px 'Myriad Pro'");
        this.label12.lineHeight = 19;
        this.label12.setTransform(360, 73);
        this.label13 = new cjs.Text("3  +  2  =  ", "16px 'Myriad Pro'");
        this.label13.lineHeight = 19;
        this.label13.setTransform(360, 103);
        this.label14 = new cjs.Text("4  +  1  =  ", "16px 'Myriad Pro'");
        this.label14.lineHeight = 19;
        this.label14.setTransform(360, 133);
        this.label15 = new cjs.Text("4  +  0  =  ", "16px 'Myriad Pro'");
        this.label15.lineHeight = 19;
        this.label15.setTransform(360, 162);
        this.addChild(this.roundRect1, this.text, this.text_1, this.textbox_group1, this.label1, this.label2, this.label3, this.label4, this.label5, this.label6, this.label7, this.label8, this.label9, this.label10, this.label11, this.label12, this.label13, this.label14, this.label15);


    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75);




    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305.3, 487, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(305.3, 106, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
