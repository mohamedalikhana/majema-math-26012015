(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p123_1.png",
            id: "p123_1"
        }, {
            src: "images/p123_2.png",
            id: "p123_2"
        }]
    };

    // symbols:
    (lib.p123_1 = function() {
        this.initialize(img.p123_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p123_2 = function() {
        this.initialize(img.p123_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();
        // Layer 1
        this.text = new cjs.Text("123", "12px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(552, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#8490C8").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579.3, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();
        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, 517, 345, 10);
        this.roundRect1.setTransform(0, 25);

        this.text = new cjs.Text("Hur många kronor är kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 2);

        this.text_q1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 2);

        this.instance = new lib.p123_1();
        this.instance.setTransform(10, 58, 0.47, 0.47);

        this.text_h2 = new cjs.Text("Kronor från början", "15px 'MyriadPro-Semibold'");
        this.text_h2.lineHeight = 19;
        this.text_h2.setTransform(31, 35);

        this.text_h3 = new cjs.Text("Handlar", "15px 'MyriadPro-Semibold'");
        this.text_h3.lineHeight = 19;
        this.text_h3.setTransform(240, 35);

        this.text_h4 = new cjs.Text("Kronor kvar", "15px 'MyriadPro-Semibold'");
        this.text_h4.lineHeight = 19;
        this.text_h4.setTransform(415, 35);

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(0, 55.5).lineTo(517, 55.5).moveTo(0, 133.5).lineTo(517, 133.5)
            .moveTo(0, 210.5).lineTo(517, 210.5).moveTo(0, 290.5).lineTo(517, 290.5);
        this.hrLine_1.setTransform(0, 0);

        this.hrLine_2 = new cjs.Shape();
        this.hrLine_2.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(184, 25).lineTo(184, 370).moveTo(392, 25).lineTo(392, 370);
        this.hrLine_2.setTransform(0, 0);

        this.hrLine_3 = new cjs.Shape();
        this.hrLine_3.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(430, 115).lineTo(480, 115).moveTo(430, 192).lineTo(480, 192)
            .moveTo(430, 272).lineTo(480, 272).moveTo(430, 351).lineTo(480, 351);
        this.hrLine_3.setTransform(0, 0);

        this.hrLine_4 = new cjs.Shape();
        this.hrLine_4.graphics.beginStroke("#818284").setStrokeStyle(0.8).moveTo(131, 124).lineTo(173, 124).moveTo(131, 201).lineTo(173, 201)
            .moveTo(131, 281).lineTo(173, 281).moveTo(131, 360).lineTo(173, 360);
        this.hrLine_4.setTransform(0, 0);

        // row-1
        this.text_1 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_1.lineHeight = 15;
        this.text_1.setTransform(199, 68);
        this.text_1.skewX = 12;
        this.text_1.skewY = 12;
        this.text_2 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_2.lineHeight = 15;
        this.text_2.setTransform(344, 75.5);
        this.text_2.skewX = -23;
        this.text_2.skewY = -23;
        // row-2
        this.text_3 = new cjs.Text("6 kr", "13px 'Myriad Pro'");
        this.text_3.lineHeight = 15;
        this.text_3.setTransform(229, 147);
        this.text_3.skewX = -13;
        this.text_3.skewY = -13;
        this.text_4 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_4.lineHeight = 15;
        this.text_4.setTransform(344, 153);
        this.text_4.skewX = -24;
        this.text_4.skewY = -24;
        // row-3
        this.text_5 = new cjs.Text("4 kr", "13px 'Myriad Pro'");
        this.text_5.lineHeight = 15;
        this.text_5.setTransform(210, 217);
        this.text_5.skewX = 9;
        this.text_5.skewY = 9;
        this.text_6 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_6.lineHeight = 15;
        this.text_6.setTransform(346, 228);
        this.text_6.skewX = -24;
        this.text_6.skewY = -24;
        // row-4
        this.text_7 = new cjs.Text("5 kr", "13px 'Myriad Pro'");
        this.text_7.lineHeight = 15;
        this.text_7.setTransform(236, 298);
        this.text_7.skewX = 9;
        this.text_7.skewY = 9;
        this.text_8 = new cjs.Text("2 kr", "13px 'Myriad Pro'");
        this.text_8.lineHeight = 15;
        this.text_8.setTransform(349, 307.5);
        this.text_8.skewX = -24;
        this.text_8.skewY = -24;

        this.addChild(this.roundRect1, this.text, this.text_q1, this.instance, this.hrLine_1, this.hrLine_2, this.hrLine_3, this.hrLine_4);
        this.addChild(this.text_h2, this.text_h3, this.text_h4);
        this.addChild(this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.text_6, this.text_7, this.text_8);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 350);


    (lib.Symbol3 = function() {
        this.initialize();

        this.text_q2 = new cjs.Text("Vad kostar pärlan?", "16px 'Myriad Pro'");
        this.text_q2.lineHeight = 19;
        this.text_q2.setTransform(19, 1);

        this.text_q1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#8490C8");
        this.text_q1.lineHeight = 27;
        this.text_q1.setTransform(0, 1);

        this.instance = new lib.p123_2();
        this.instance.setTransform(8, 30, 0.47, 0.47);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 24, 517, 160, 10);
        this.roundRect1.setTransform(0, 0);

        this.roundRect2 = new cjs.Shape();
        this.roundRect2.graphics.f("#ffffff").s('#B5BBDF').drawRoundRect(12, 35, 132, 64, 7);
        this.roundRect2.setTransform(0, 0);

        this.roundRect3 = this.roundRect2.clone(true);
        this.roundRect3.setTransform(0, 75);

        this.roundRect4 = new cjs.Shape();
        this.roundRect4.graphics.f("#ffffff").s('#B5BBDF').drawRoundRect(12, 35, 147, 64, 7);
        this.roundRect4.setTransform(251, 0);

        this.roundRect5 = this.roundRect4.clone(true);
        this.roundRect5.setTransform(251, 75);

        // row-1
        this.text_1 = new cjs.Text("7 kr tillsammans", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 23;
        this.text_1.setTransform(21, 35.5);
        this.text_2 = new cjs.Text("10 kr tillsammans", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 23;
        this.text_2.setTransform(280.5, 35.5);
        // row-2
        this.text_3 = new cjs.Text("10 kr tillsammans", "16px 'Myriad Pro'");
        this.text_3.lineHeight = 23;
        this.text_3.setTransform(21, 112);
        this.text_4 = new cjs.Text("10 kr tillsammans", "16px 'Myriad Pro'");
        this.text_4.lineHeight = 23;
        this.text_4.setTransform(280.5, 112);

        var ToBeAdded = [];
        var arrYPos = ['43.7', '122'];
        for (var col = 0; col < 2; col++) {
            var columnSpace = col;
            for (var j = 0; j < arrYPos.length; j++) {
                var yPos = parseInt(arrYPos[j]);
                if (col == 1 && j == 1) {
                    columnSpace = 0.99;
                    yPos=120;
                }
                var text = "3 kr";
                if (col == 1) {
                    text = (j == 0) ? "4 kr" : "2 kr";
                }
                var temp_text = new cjs.Text(text, "16px 'Myriad Pro'");
                temp_text.lineHeight = -1;
                temp_text.setTransform(203 + (columnSpace * 264), yPos);
                ToBeAdded.push(temp_text);
            }
        }

        this.hrLine_1 = new cjs.Shape();
        this.hrLine_1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(10, 104.3).lineTo(509, 104.3)
            .moveTo(253, 28).lineTo(253, 100).moveTo(253, 108).lineTo(253, 180);
        this.hrLine_1.setTransform(0, 0);

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 4; row++) {
                var rowSpace = row;
                if (row == 2) {
                    rowSpace = 2.41;
                } else if (row == 3) {
                    rowSpace = 3.42;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#6D6E70").ss(0.8).drawRect(199 + (columnSpace * 263), 39 + (rowSpace * 32.5), 34, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.text_q2, this.text_q1, this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.roundRect5);
        this.addChild(this.instance, this.textbox_group1, this.text_1, this.text_2, this.text_3, this.text_4, this.hrLine_1);
        for (var i = 0; i < ToBeAdded.length; i++) {
            this.addChild(ToBeAdded[i])
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 180);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();
        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(306, 490, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(306, 114.7, 1, 1, 0, 0, 0, 255.8, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
