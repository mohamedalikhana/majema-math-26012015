(function(lib, img, cjs) {

    // Basic implementation that could be added to CreateJS.Graphics
    (cjs.Graphics.Dash = function(instr) {
        if (instr == null) {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec = function(ctx) {
        ctx.setLineDash(this.instr);
    };
    cjs.Graphics.prototype.dash = function(instr) {
        return this.append(new cjs.Graphics.Dash(instr));
    }

    var p;
    lib.properties = {
        width: 610,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p138_1.png",
            id: "p138_1"
        }, {
            src: "images/p138_2.png",
            id: "p138_2"
        }, {
            src: "images/p138_3.png",
            id: "p138_3"
        }]
    };

    (lib.p138_1 = function() {
        this.initialize(img.p138_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p138_2 = function() {
        this.initialize(img.p138_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);

    (lib.p138_3 = function() {
        this.initialize(img.p138_3);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 633, 542);



    (lib.Symbol4 = function() {
        this.initialize();

        this.text = new cjs.Text("Eget stapeldiagram", "bold 36px 'Epic Awesomeness'", "#FAAA33");
        this.text.lineHeight = 29;
        this.text.setTransform(141, 37);

        this.text_1 = new cjs.Text("48", "28px 'MyriadPro-Semibold'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(42, 22);

        this.text_2 = new cjs.Text("Använd tabellen och fyll i diagrammet på nästa sida.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(50.5, 620);

        this.pageBottomText = new cjs.Text("kunna läsa av och göra eget stapeldiagram", "9px 'Myriad Pro'", "#FAAA33");
        this.pageBottomText.lineHeight = 11;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(85, 651);

        this.text_4 = new cjs.Text("138", "13px 'Myriad Pro'", "#FFFFFF");
        this.text_4.lineHeight = 18;
        this.text_4.setTransform(38, 649);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FAAA33").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 23.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FAAA33").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape_2.setTransform(33.1, 660.8, 1.05, 1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.s("#FAAA33").ss(2).moveTo(20, 24).lineTo(610, 24).moveTo(24, 20).lineTo(24, 660).moveTo(20, 656).lineTo(610, 656);
        this.shape_3.setTransform(0, 0);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#fef3e2").drawRect(0, 0, 24, 660).drawRect(0, 0, 610, 24).drawRect(0, 656, 610, 20);
        this.shape_4.setTransform(0, 0);

        this.text_Rect = new cjs.Shape();
        this.text_Rect.graphics.f("#ffffff").s("#FAAA33").ss(1).drawRoundRect(78, 649.7, 180, 12, 20);
        this.text_Rect.setTransform(0, 0);

        this.instance_2 = new lib.p138_2();
        this.instance_2.setTransform(516, 48, 0.63, 0.63);

        this.addChild(this.shape_4, this.shape_3, this.shape_2, this.shape, this.text_Rect, this.text_4,
            this.pageBottomText, this.text_2, this.text_1, this.text, this.instance_2);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);

    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FBAA34').drawRoundRect(2, 5, 511, 476.2, 10);
        this.roundRect1.setTransform(8, 45);

        this.text_1 = new cjs.Text("Slå tärningen 30 gånger. Skriv 1 streck i tabellen efter varje slag.", "16px 'Myriad Pro'");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(60, 17);

        this.instance = new lib.p138_1();
        this.instance.setTransform(8, 0, 0.62, 0.62);

        this.instance_2 = new lib.p138_3();
        this.instance_2.setTransform(7, 48, 0.46, 0.46);

        var hrRule = [];

        for (var i = 0; i < 5; i++) {
            this.hrRule1 = new cjs.Shape();
            this.hrRule1.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(10, 127).lineTo(520, 127);
            this.hrRule1.setTransform(0, 3 + (i * 79.3));
            hrRule.push(this.hrRule1);
        }

        this.hrRule2 = new cjs.Shape();
        this.hrRule2.graphics.beginStroke("#949599").setStrokeStyle(0.8).moveTo(100, 50).lineTo(100, 526);
        this.hrRule2.setTransform(1, 0);
        hrRule.push(this.hrRule2);

        this.addChild(this.text_1, this.instance, this.roundRect1, this.instance_2);
        for (var txtAdd = 0; txtAdd < hrRule.length; txtAdd++) {
            this.addChild(hrRule[txtAdd]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500, 535);



    (lib.pageLib = function() {
        this.initialize();
        this.other = new lib.Symbol4();
        this.other.setTransform(609.5, 339, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.8, 255, 1, 1, 0, 0, 0, 256.3, 173.6);

        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339.3, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
