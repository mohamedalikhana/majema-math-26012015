var extras = function() {};
(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        isExercise: true,
        manifest: [{
            src: "../exercises/images/c13_ex1_1.png",
            id: "c13_ex1_1"
        }, {
            src: "../exercises/images/c13_ex1_2.png",
            id: "c13_ex1_2"
        }]
    };

    (lib.c13_ex1_1 = function() {
        this.initialize(img.c13_ex1_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    (lib.c13_ex1_2 = function() {
        this.initialize(img.c13_ex1_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 121, 121);

    //Static Content
    (lib.Basement = function() {
        this.initialize();
        this.header = new lib.exerciseTitle();
        this.header.setTransform(0, -30, 1, 1, 0, 0, 0, 0, 0);
        this.addChild(this.header);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage0 = function() {
        this.initialize();
        this.instrTexts = [];
        this.instrTexts.push('Hur många?');
        this.instrTexts.push('Hur många kommer?');
        this.instrTexts.push('Hur många barn är det tillsammans?');

        this.instrText1 = new cjs.Text(this.instrTexts[0], "40px 'Myriad Pro'");
        this.instrText1.lineHeight = 19;
        this.instrText1.textAlign = 'center';
        this.instrText1.setTransform(550 + 120, -40);
        this.instrText1.visible = false;

        this.instrText2 = this.instrText1.clone(true);
        this.instrText2.text = this.instrTexts[1];
        this.instrText3 = this.instrText1.clone(true);
        this.instrText3.text = this.instrTexts[2];

        this.finalText = new cjs.Text("                       och                      är    4.", "bold 36px'Myriad Pro'");
        this.finalText.lineHeight = 19;
        this.finalText.textAlign = 'left';
        this.finalText.setTransform(470, 30);
        this.finalText.visible = false;

        this.answer1 = new cjs.Text("3", "bold 36px'Myriad Pro'");
        this.answer1.lineHeight = 19;
        this.answer1.textAlign = 'left';
        this.answer1.setTransform(470, 30);
        this.answer1.visible = false;

        this.answer2 = new cjs.Text("1", "bold 36px'Myriad Pro'");
        this.answer2.lineHeight = 19;
        this.answer2.textAlign = 'left';
        this.answer2.setTransform(810, 30);
        this.answer2.visible = false;

        this.children1 = new lib.c13_ex1_1();
        this.children1.setTransform(375, 100, 1.2, 1.2);
        this.children1.visible = true;

        this.children2 = new lib.c13_ex1_2();
        this.children2.setTransform(780, 100, 1.2, 1.2);
        this.children2.visible = false;
        this.beneathText = new cjs.Text("3 plus 1 är lika med 4.", "36px'Myriad Pro'");
        this.beneathText.lineHeight = 19;
        this.beneathText.textAlign = 'center';
        this.beneathText.setTransform(550 + 120, 420);
        this.beneathText.visible = false;

        this.answers = new lib.answers();
        // this.answers.text_5.visible = true;
        // this.answers.text_4.visible = true;
        // this.answers.text_2.visible = true;
        // this.answers.text_1.visible = true;
        // this.answers.text_3.visible = true;
        this.answers.setTransform(285 + 115, 0 + 100, 3, 3, 0, 0, 0, 0, 0);

        // this.textbox_group1 = new cjs.Shape();
        // this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(1).drawRect(536 + 115, 171 + 100, 60, 70);
        // this.textbox_group1.setTransform(0, 0);

        this.addChild(this.children1, this.children2, this.instrText1, this.instrText2, this.instrText3, this.answer1, this.answer2, this.finalText, this.textbox_group1, this.answers, this.beneathText);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage1 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.Stage2 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.instrText1.visible = true;
        this.stage0.answer1.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.answer1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    (lib.Stage3 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();
        this.stage0.answer1.visible = true;
        this.stage0.children2.visible = true;
        this.stage0.instrText1.visible = true;
        this.stage0.instrText2.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText1,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 1500
        });

        this.tweens.push({
            ref: this.stage0.instrText2,
            alphaFrom: 0,
            alphaTo: 1,
            wait:2800,
            alphaTimeout: 2000
        });

        this.tweens.push({
            ref: this.stage0.children2,
            alphaFrom: 0,
            alphaTo: 1,
            wait:2800,
            alphaTimeout: 2000
        });

        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage4 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.stage0.answer1.visible = true;
        this.stage0.answer2.visible = true;
        this.stage0.children2.visible = true;
        this.stage0.instrText2.visible = true;
        this.stage0.instrText3.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.instrText2,
            alphaFrom: 1,
            alphaTo: 0,
            wait: 500,
            alphaTimeout: 1500
        });

        this.tweens.push({
            ref: this.stage0.instrText3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2800,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.answer2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2800,
            alphaTimeout: 2000
        });
        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage5 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.stage0.answer1.visible = true;
        this.stage0.answer2.visible = true;
        this.stage0.children2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.finalText.visible = true;
        this.tweens = [];
        this.tweens.push({
            ref: this.stage0.finalText,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });


        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage6 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.stage0.answer1.visible = true;
        this.stage0.answer2.visible = true;
        this.stage0.children2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.finalText.visible = true;
        this.stage0.answers.text_4.visible = true;
        this.stage0.answers.text_2.visible = true;
        this.stage0.answers.text_1.visible = true;
        this.stage0.answers.text_3.visible = true;
        this.tweens = [];
     
        this.tweens.push({
            ref: this.stage0.answers.text_1,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.answers.text_2,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1000,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.answers.text_3,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 1500,
            alphaTimeout: 2000
        });
        this.tweens.push({
            ref: this.stage0.answers.text_4,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 2000,
            alphaTimeout: 2000
        });




        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage7 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.stage0.answer1.visible = true;
        this.stage0.answer2.visible = true;
        this.stage0.children2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.finalText.visible = true;
        this.stage0.answers.text_5.visible = true;
        this.stage0.answers.text_4.visible = true;
        this.stage0.answers.text_2.visible = true;
        this.stage0.answers.text_1.visible = true;
        this.stage0.answers.text_3.visible = true;
        this.tweens = [];
      
        this.tweens.push({
            ref: this.stage0.answers.text_5,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        




        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.Stage8 = function() {
        this.initialize();
        this.stage0 = new lib.Stage0();

        this.stage0.answer1.visible = true;
        this.stage0.answer2.visible = true;
        this.stage0.children2.visible = true;
        this.stage0.instrText3.visible = true;
        this.stage0.finalText.visible = true;
        this.stage0.answers.text_5.visible = true;
        this.stage0.answers.text_4.visible = true;
        this.stage0.answers.text_2.visible = true;
        this.stage0.answers.text_1.visible = true;
        this.stage0.answers.text_3.visible = true;
        this.stage0.beneathText.visible=true;
        this.tweens = [];
      
        this.tweens.push({
            ref: this.stage0.beneathText,
            alphaFrom: 0,
            alphaTo: 1,
            wait: 500,
            alphaTimeout: 2000
        });
        
        p.tweens = this.tweens;
        this.addChild(this.stage0);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);
    // (lib.Stage2 = function() {
    //     this.initialize();
    //     this.stage1 = new lib.Stage1();

    //     this.stage1.answers.text_3.visible = true;

    //     this.tweens = [];
    //     this.tweens.push({
    //         ref: this.stage1.answers.text_3,
    //         alphaFrom: 0,
    //         alphaTo: 1,
    //         wait: 500,
    //         alphaTimeout: 2000
    //     });
    //     p.tweens = this.tweens;

    //     this.addChild(this.stage1);

    // }).prototype = p = new cjs.Container();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // (lib.Stage3 = function() {
    //     this.initialize();
    //     this.stage2 = new lib.Stage2();

    //     this.answers = new lib.answers();
    //     this.answers.text_10.visible = true;
    //     this.answers.text_9.visible = true;
    //     this.answers.text_7.visible = true;
    //     this.answers.text_6.visible = true;
    //     this.answers.setTransform(285 + 115, 90 + 100, 3, 3, 0, 0, 0, 0, 0);

    //     this.textbox_group1 = new cjs.Shape();
    //     this.textbox_group1.graphics.f('#ffffff').s("#949599").ss(1).drawRect(536 + 115, 276 + 100, 60, 70);
    //     this.textbox_group1.setTransform(0, 0);

    //     this.addChild(this.stage2, this.textbox_group1, this.answers);

    // }).prototype = p = new cjs.Container();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    // (lib.Stage4 = function() {
    //     this.initialize();

    //     this.stage3 = new lib.Stage3();

    //     this.stage3.answers.text_8.visible = true;

    //     this.tweens = [];

    //     this.tweens.push({
    //         ref: this.stage3.answers.text_8,
    //         alphaFrom: 0,
    //         alphaTo: 1,
    //         wait: 500,
    //         alphaTimeout: 2000
    //     });

    //     p.tweens = this.tweens;

    //     this.addChild(this.stage3);

    // }).prototype = p = new cjs.Container();
    // p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);



 (lib.exerciseTitle = function() {
        this.initialize();
        this.text = new cjs.Text("Vi skriver addition", "bold 24px 'Myriad Pro'", "#7AC729");
        this.text.lineHeight = 29;
        this.text.setTransform(96.5, 3.9);
        this.text_1 = new cjs.Text("13", "bold 28px 'Myriad Pro'", "#FFFFFF");
        this.text_1.lineHeight = 34;
        this.text_1.setTransform(46, 0);
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#7AC729").s().p("AmyDqIAAnTINlAAIAAGbQAAAjgcAOIgcAHg");
        this.shape.setTransform(43.6, 9.5);

        this.addChild(this.shape, this.text_1, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 650);

    (lib.answers = function() {
        this.initialize();

        this.text_1 = new cjs.Text("3", "26px 'Myriad Pro'");
        this.text_1.visible = false;
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(35, 55);

        this.text_2 = new cjs.Text("+", "26px 'Myriad Pro'");
        this.text_2.visible = false;
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(60, 55);

        this.text_3 = new cjs.Text("1", "26px 'Myriad Pro'");
        this.text_3.visible = false;
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(85, 55);

        this.text_4 = new cjs.Text("=", "26px 'Myriad Pro'");
        this.text_4.visible = false;
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(110, 55);

        this.text_5 = new cjs.Text("4", "26px 'Myriad Pro'");
        this.text_5.visible = false;
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(135, 55);

        this.text_6 = new cjs.Text("3", "26px 'Myriad Pro'");
        this.text_6.visible = false;
        this.text_6.lineHeight = 19;
        this.text_6.setTransform(35, 60);

        this.text_7 = new cjs.Text("+", "26px 'Myriad Pro'");
        this.text_7.visible = false;
        this.text_7.lineHeight = 19;
        this.text_7.setTransform(60, 60);

        this.text_8 = new cjs.Text("2", "26px 'Myriad Pro'");
        this.text_8.visible = false;
        this.text_8.lineHeight = 19;
        this.text_8.setTransform(85, 60);

        this.text_9 = new cjs.Text("=", "26px 'Myriad Pro'");
        this.text_9.visible = false;
        this.text_9.lineHeight = 19;
        this.text_9.setTransform(110, 60);

        this.text_10 = new cjs.Text("5", "26px 'Myriad Pro'");
        this.text_10.visible = false;
        this.text_10.lineHeight = 19;
        this.text_10.setTransform(135, 60);

        this.addChild(this.text_10, this.text_9, this.text_8, this.text_7, this.text_6, this.text_5, this.text_4, this.text_3, this.text_2, this.text_1);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 511.3, 143.6);

    // stage content:
    (lib.exercise = function() {
        this.initialize();
        this.other = new lib.Basement();
        this.other.setTransform(0, 40, 1, 1);

        this.stage1 = new lib.Stage1();
        this.stage1.visible = true;
        this.stage1.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage2 = new lib.Stage2();
        this.stage2.visible = false;
        this.stage2.setTransform(100, 100, 1, 1, 0, 0, 0)
        this.stage3 = new lib.Stage3();
        this.stage3.visible = false;
        this.stage3.setTransform(100, 100, 1, 1, 0, 0, 0)
        this.stage4 = new lib.Stage4();
        this.stage4.visible = false;
        this.stage4.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage5 = new lib.Stage5();
        this.stage5.visible = false;
        this.stage5.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage6 = new lib.Stage6();
        this.stage6.visible = false;
        this.stage6.setTransform(100, 100, 1, 1, 0, 0, 0)

        this.stage7 = new lib.Stage7();
        this.stage7.visible = false;
        this.stage7.setTransform(100, 100, 1, 1, 0, 0, 0)
        this.stage8 = new lib.Stage8();
        this.stage8.visible = false;
        this.stage8.setTransform(100, 100, 1, 1, 0, 0, 0)

        // this.stage3 = new lib.Stage3();
        // this.stage3.visible = false;
        // this.stage3.setTransform(100, 100, 1, 1, 0, 0, 0)

        // this.stage4 = new lib.Stage4();
        // this.stage4.visible = false;
        // this.stage4.setTransform(0, 0, 1, 1, 0, 0, 0)

        this.addChild(this.other, this.stage1, this.stage2, this.stage3, this.stage4, this.stage5, this.stage6, this.stage7, this.stage8);

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 610.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});


var lib, images, createjs;
