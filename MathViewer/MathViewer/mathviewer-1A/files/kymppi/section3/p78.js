(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p78_2.png",
            id: "p78_2"
        }, {
            src: "images/p78_1.png",
            id: "p78_1"
        }]
    };

    // symbols:

    (lib.p78_2 = function() {
        this.initialize(img.p78_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);


    (lib.p78_1 = function() {
        this.initialize(img.p78_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.customShape1 = function(fCTriangle, fCRectangle, text, x, y, scaleX, scaleY) {
        var shapeWithText = {
            shape: null,
            text: null
        };
        var customShape = new cjs.Shape();
        customShape.graphics.f(fCTriangle).s('#7d7d7d').ss(3.5).moveTo(50, 0).lineTo(70, 0).lineTo(100, 25).lineTo(70, 50).lineTo(50, 50).lineTo(50, 0);
        customShape.graphics.f(fCRectangle).s('#7d7d7d').ss(3.5).moveTo(50, 0).lineTo(0, 0).lineTo(0, 50).lineTo(50, 50);
        var text = new cjs.Text(text, "16px 'Myriad Pro'");
        text.lineHeight = -1;
        text.setTransform(x + 3, y + 3);
        customShape.setTransform(x, y, scaleX, scaleY);
        shapeWithText.shape = customShape.clone(true);
        shapeWithText.text = text;
        return shapeWithText;
    }).prototype = p = new cjs.Container();


    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("78", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(37.4, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.top_textbox_1 = new cjs.Shape();
        for (var i = 0; i < 3; i++) {
            this.top_textbox_1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(480 + (i * 30), 20, 25, 25);
        }
        this.top_textbox_1.setTransform(0, 0);

        this.addChild(this.shape, this.text, this.customShape);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        // Layer 1
        this.instance = new lib.p78_2();
        this.instance.setTransform(-12, 55, 0.244, 0.244);

        this.text = new cjs.Text("Räkna och måla.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 12);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 12);

        this.customShape4 = lib.customShape1("#ffff66", "#ffffff", " 5", 23, 37, 0.38, 0.38);
        this.customShape3 = lib.customShape1("#cc3333", "#ffffff", " 6", 80, 37, 0.38, 0.38);
        this.customShape2 = lib.customShape1("#0099cc", "#ffffff", " 7", 137, 37, 0.38, 0.38);
        this.customShape5 = lib.customShape1("#339933", "#ffffff", " 8", 194, 37, 0.38, 0.38);

        this.label_1 = new cjs.Text("1 + 2 + 2", "14.5px 'Myriad Pro'");
        this.label_1.lineHeight = 19;
        this.label_1.setTransform(35, 110);
        this.label_1.skewY=10;
        this.label_1.skewX=10;

        this.label_2 = new cjs.Text("2 + 2 + 2", "14.5px 'Myriad Pro'");
        this.label_2.lineHeight = 19;
        this.label_2.setTransform(31, 137);
        this.label_2.skewY=10;
        this.label_2.skewX=10;

        this.label_3 = new cjs.Text("1 + 2 + 3", "14.5px 'Myriad Pro'");
        this.label_3.lineHeight = 19;
        this.label_3.setTransform(0, 158);
        this.label_3.skewY=16;
        this.label_3.skewX=16;

        this.label_4 = new cjs.Text("1 + 4 + 1", "14.5px 'Myriad Pro'");
        this.label_4.lineHeight = 19;
        this.label_4.setTransform(62, 175);
        this.label_4.skewY=-8;
        this.label_4.skewX=-8;

        this.label_5 = new cjs.Text("0 + 2 + 3", "14.5px 'Myriad Pro'");
        this.label_5.lineHeight = 19;
        this.label_5.setTransform(138, 178);
        this.label_5.skewY=-8;
        this.label_5.skewX=-8;

        this.label_6 = new cjs.Text("2 + 3 + 2", "14.5px 'Myriad Pro'");
        this.label_6.lineHeight = 19;
        this.label_6.setTransform(149, 203);
        this.label_6.skewY=-5;
        this.label_6.skewX=-5;

        this.label_7 = new cjs.Text("0 + 6 + 1", "14.5px 'Myriad Pro'");
        this.label_7.lineHeight = 19;
        this.label_7.setTransform(153, 231);
        this.label_7.skewY=-5;
        this.label_7.skewX=-5;

        this.label_8 = new cjs.Text("2 + 3 + 1", "14.5px 'Myriad Pro'");
        this.label_8.lineHeight = 19;
        this.label_8.setTransform(238, 129);

        this.label_9 = new cjs.Text("2 + 3 + 2", "14.5px 'Myriad Pro'");
        this.label_9.lineHeight = 19;
        this.label_9.setTransform(240, 156);

        this.label_10 = new cjs.Text("5 + 1 + 1", "14.5px 'Myriad Pro'");
        this.label_10.lineHeight = 19;
        this.label_10.setTransform(216, 178);
        this.label_10.skewY=8;
        this.label_10.skewX=8;

        this.label_11 = new cjs.Text("2 + 4 + 1", "14.5px 'Myriad Pro'");
        this.label_11.lineHeight = 19;
        this.label_11.setTransform(274, 188);
        this.label_11.skewY=-19;
        this.label_11.skewX=-19;

        this.label_12 = new cjs.Text("4 + 2 + 2", "14.5px 'Myriad Pro'");
        this.label_12.lineHeight = 19;
        this.label_12.setTransform(346, 162);

        this.label_13 = new cjs.Text("2 + 2 + 1", "14.5px 'Myriad Pro'");
        this.label_13.lineHeight = 19;
        this.label_13.setTransform(346, 190);

        this.label_14 = new cjs.Text("3 + 3 + 2", "14.5px 'Myriad Pro'");
        this.label_14.lineHeight = 19;
        this.label_14.setTransform(340, 220);

        this.label_15 = new cjs.Text("2 + 1 + 3", "14.5px 'Myriad Pro'");
        this.label_15.lineHeight = 19;
        this.label_15.setTransform(437, 110);
        this.label_15.skewY=-9;
        this.label_15.skewX=-9;

        this.label_16 = new cjs.Text("3 + 3 + 1", "14.5px 'Myriad Pro'");
        this.label_16.lineHeight = 19;
        this.label_16.setTransform(441, 136);
        this.label_16.skewY=-9;
        this.label_16.skewX=-9;

        this.label_17 = new cjs.Text("4 + 2 + 2", "14.5px 'Myriad Pro'");
        this.label_17.lineHeight = 19;
        this.label_17.setTransform(416, 163);
        this.label_17.skewY=4;
        this.label_17.skewX=4;

        this.label_18 = new cjs.Text("4 + 3 + 1", "14.5px 'Myriad Pro'");
        this.label_18.lineHeight = 19;
        this.label_18.setTransform(476, 163);
        this.label_18.skewY=-16;
        this.label_18.skewX=-16;


        this.addChild(this.instance, this.text, this.text_1, this.customShape2.shape, this.customShape2.text, this.customShape3.shape, this.customShape3.text, this.customShape4.shape, this.customShape4.text, this.customShape5.shape, this.customShape5.text);
        this.addChild(this.label_1, this.label_2, this.label_3, this.label_4, this.label_5,this.label_6,this.label_7,this.label_8,this.label_9,this.label_10,this.label_11);
        this.addChild(this.label_12, this.label_13,this.label_14,this.label_15,this.label_16,this.label_17,this.label_18);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 500.3, 320.2);

    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p78_1();
        this.instance.setTransform(0, 25, 0.2, 0.2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 5, ((2161 * 0.5) - 10) * 0.5, 485 * 0.5, 10);
        this.roundRect1.setTransform(0, 23);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#BBD992').drawRoundRect(40, 43, ((750 * 0.5) - 10) * 0.5, 238 * 0.5, 10);
        this.roundRect3.setTransform(0, 20);

        this.roundRect4 = this.roundRect3.clone(true);
        this.roundRect4.setTransform(((839 * 0.5) + 128) * 0.5, 20);

        this.text = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 3);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 3);

        this.label1 = new cjs.Text("6     –", "18px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(344, 75);

        this.label2 = new cjs.Text("+", "18px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(372, 150);

        this.label3 = new cjs.Text("+", "18px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(372, 110);

        this.label4 = new cjs.Text("+", "18px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(102, 150);

        this.label5 = new cjs.Text("4", "18px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(187, 74);

        this.label6 = new cjs.Text("6", "18px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(187, 150);

        this.label7 = new cjs.Text("3", "18px 'Myriad Pro'");
        this.label7.lineHeight = 19;
        this.label7.setTransform(130, 111);

        this.label8 = new cjs.Text("5", "18px 'Myriad Pro'");
        this.label8.lineHeight = 19;
        this.label8.setTransform(458, 111);

        var arrEqual = ['75', '113', '150'];
        var ToBeAdded = [];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = 5;
            tempLabel.setTransform(430, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['75', '113', '150'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = 5;
            tempLabel.setTransform(160, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['208', '238'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = 5;
            tempLabel.setTransform(72, row);
            ToBeAdded.push(tempLabel);
        }


        arrEqual = [];
        arrEqual = ['207', '237'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = 5;
            tempLabel.setTransform(356, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['75', '112'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("+", "18px 'Myriad Pro'");
            tempLabel.lineHeight = 5;
            tempLabel.setTransform(104, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['183', '459'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = 5;
            tempLabel.setTransform(row, 204);
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1 && column == 1) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(88 + (columnSpace * 109), 202 + (row * 32), 20, 23);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1 && column == 1) {
                    continue;
                }
                this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(373 + (columnSpace * 104), 202 + (row * 32), 20, 23);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#949599").setStrokeStyle(0.5).moveTo(268, 62).lineTo(268, 266);
        this.Line_1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect3, this.roundRect4, this.text, this.text_1, this.label1, this.label2, this.label3);
        this.addChild(this.label4, this.label5, this.label6, this.textbox_group1, this.textbox_group2, this.label7, this.label8, this.Line_1);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 280.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(296.3, 403, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(296.3, 90, 1, 1, 0, 0, 0, 254.6, 50);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
