(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p73_1.png",
            id: "p73_1"
        }, {
            src: "images/p73_2.png",
            id: "p73_2"
        }]
    };

    // symbols:
    (lib.p73_1 = function() {
        this.initialize(img.p73_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.p73_2 = function() {
        this.initialize(img.p73_2);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("73", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(555, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("AkzCmIAAkSQAAgkAcgOQAOgHAOAAIIvAAIAAFLg");
        this.shape.setTransform(579, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.text = new cjs.Text("Måla cirkeln om svaret stämmer.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 4);

        this.text_1 = new cjs.Text("5.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 4);

        this.shape_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 8; row++) {
                this.shape_group1.graphics.f("#fff").s("#000000").ss(0.5, 0, 0, 4).arc(120 + (columnSpace * 176), 105 + (row * 29), 11, 0, 2 * Math.PI);
            }
        }
        this.shape_group1.setTransform(0, 0);

        this.Line_group1 = new cjs.Shape();
        for (var column = 0; column < 3; column++) {
            var columnSpace = column;
            for (var row = 0; row < 9; row++) {
                this.Line_group1.graphics.beginStroke("#000000").setStrokeStyle(0.5).moveTo(25 + (columnSpace * 176), 90 + (row * 29)).lineTo(150 + (columnSpace * 176), 90 + (row * 29));
            }
        }
        this.Line_group1.setTransform(0, 0);

        var arrVal=['2','6','7'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(80 + (index * 176), 61 );
            ToBeAdded.push(tempLabel);
        }

        arrVal=[]
        arrVal=['1 + 2','1 + 5','3 + 3'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 98 );
            ToBeAdded.push(tempLabel);
        }

        arrVal=[]
        arrVal=['2 + 0','2 + 4','4 + 3'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 128 );
            ToBeAdded.push(tempLabel);
        }        

        arrVal=[]
        arrVal=['2 – 0','3 + 4','7 – 1'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 155 );
            ToBeAdded.push(tempLabel);
        }

        arrVal=[]
        arrVal=['4 – 2','2 + 3','7 – 0'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 185 );
            ToBeAdded.push(tempLabel);
        }

        arrVal=[]
        arrVal=['6 – 3','4 + 2','6 – 1'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 213 );
            ToBeAdded.push(tempLabel);
        }

        arrVal=[]
        arrVal=['5 – 3','7 – 1','6 + 1'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 245 );
            ToBeAdded.push(tempLabel);
        }

        arrVal=[]
        arrVal=['7 – 5','7 – 3','2 + 4'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 273 );
            ToBeAdded.push(tempLabel);
        }

        arrVal=[]
        arrVal=['6 – 4','4 + 3','1 + 6'];

        for (var index = 0; index < arrVal.length; index++) {

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(40 + (index * 176), 300 );
            ToBeAdded.push(tempLabel);
        }
        
        this.instance = new lib.p73_2();
        this.instance.setTransform(1, 25, 0.24, 0.24);

        this.instance1 = new lib.p73_2();
        this.instance1.setTransform(177, 25, 0.24, 0.24);

        this.instance2 = new lib.p73_2();
        this.instance2.setTransform(353, 25, 0.24, 0.24);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#FFFFFF').drawRoundRect(0, 0, ((2150 * 0.5) - 10) * 0.5, 630 * 0.5, 10);
        this.roundRect1.setTransform(0, 20);

        this.addChild(this.roundRect1, this.text, this.text_1, this.shape_group1, this.Line_group1);
        this.addChild(this.instance,this.instance1, this.instance2);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530.3, 320);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p73_1();
        this.instance.setTransform(-12, 20, 0.2, 0.2);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 0, ((1037 * 0.5) - 10) * 0.5, 422 * 0.5, 10);
        this.roundRect1.setTransform(0, 23);

        this.roundRect2 = this.roundRect1.clone(true);
        this.roundRect2.setTransform(((1037 * 0.5) + 10) * 0.5, 23);

        this.roundRect3 = new cjs.Shape();
        this.roundRect3.graphics.f("#ffffff").s('#BBD992').drawRoundRect(30, 12, ((800 * 0.5) - 10) * 0.5, 230 * 0.5, 10);
        this.roundRect3.setTransform(0, 20);

        this.roundRect4 = this.roundRect3.clone(true);
        this.roundRect4.setTransform(((800 * 0.5) + 128) * 0.5, 20);

        this.text = new cjs.Text("Vad är figurerna värda?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, -4);

        this.text_1 = new cjs.Text("6.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, -4);

        this.label1 = new cjs.Text("6      –  ", "18px 'Myriad Pro'");
        this.label1.lineHeight = 19;
        this.label1.setTransform(333, 45);

        this.label2 = new cjs.Text("4      –  ", "18px 'Myriad Pro'");
        this.label2.lineHeight = 19;
        this.label2.setTransform(333, 75);

        this.label3 = new cjs.Text("+", "18px 'Myriad Pro'");
        this.label3.lineHeight = 19;
        this.label3.setTransform(365, 101);

        this.label4 = new cjs.Text("–", "18px 'Myriad Pro'");
        this.label4.lineHeight = 19;
        this.label4.setTransform(91, 101);

        this.label5 = new cjs.Text("6", "18px 'Myriad Pro'");
        this.label5.lineHeight = 19;
        this.label5.setTransform(177, 45);

        this.label6 = new cjs.Text("4", "18px 'Myriad Pro'");
        this.label6.lineHeight = 19;
        this.label6.setTransform(177, 75);

        var arrEqual = ['45', '75', '101'];
        var ToBeAdded = [];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(423, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['45', '75', '101'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(149, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['160', '190'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(61, row);
            ToBeAdded.push(tempLabel);
        }


        arrEqual = [];
        arrEqual = ['160', '190'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(345, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['45', '75'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("+", "18px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(90, row);
            ToBeAdded.push(tempLabel);
        }

        arrEqual = [];
        arrEqual = ['181', '457'];

        for (var index = 0; index < arrEqual.length; index++) {
            var row = parseInt(arrEqual[index]);

            var tempLabel = new cjs.Text("=", "18px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(row, 160);
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1 && column == 1) {
                    continue;
                }
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(80 + (columnSpace * 117), 157 + (row * 32), 22, 25);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.textbox_group2 = new cjs.Shape();
        for (var column = 0; column < 2; column++) {
            var columnSpace = column;
            for (var row = 0; row < 2; row++) {
                if (row == 1 && column == 1) {
                    continue;
                }
                this.textbox_group2.graphics.f('#ffffff').s("#7D7D7D").ss(0.7).drawRect(365 + (columnSpace * 112), 157 + (row * 32), 22, 25);
            }
        }
        this.textbox_group2.setTransform(0, 0);

        this.addChild(this.roundRect1, this.roundRect2, this.roundRect3, this.roundRect4, this.instance, this.text, this.text_1, this.label1, this.label2, this.label3);
        this.addChild(this.label4, this.label5, this.label6, this.textbox_group1, this.textbox_group2);

        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 512.3, 75.9);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(305.3, 438.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(300.3, 95, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
