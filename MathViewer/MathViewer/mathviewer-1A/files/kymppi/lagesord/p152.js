(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 619,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p152_1.png",
            id: "p152_1"
        }]
    };

    // symbols:
    (lib.p152_1 = function() {
        this.initialize(img.p152_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 560, 297);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("152", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(35, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D83770").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31.1, 660.8);

        this.text_1 = new cjs.Text("Lägesord", "24px 'MyriadPro-Semibold'", "#D83770");
        this.text_1.lineHeight = 19;
        this.text_1.setTransform(40, 50);

        this.text_2 = new cjs.Text("Till läraren:", "bold 11px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(40.5, 596);

        this.text_3 = new cjs.Text("Ställ frågor av typen ”Vad finns det på nedersta hyllan i mitten?”,", "11px 'Myriad Pro'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(100, 596);

        this.text_4 = new cjs.Text("”Vad finns det ovanpå byrån?”, ”Vem smyger under bordet?” och ”Var är suddet?” .", "11px 'Myriad Pro'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(40.5, 611);

        this.text_5 = new cjs.Text("Använd orden framför, bakom, ovanför, nedanför, under, på, i, i mitten, bredvid.", "11px 'Myriad Pro'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(40.5, 626);

        this.pageBottomText = new cjs.Text("kunna olika lägesord", "9px 'Myriad Pro'");
        this.pageBottomText.lineHeight = 19;
        this.pageBottomText.pos = 'left';
        this.pageBottomText.setTransform(69, 651);

        this.instance = new lib.p152_1();
        this.instance.setTransform(33, 85, 0.47, 0.47);

        this.addChild(this.shape, this.text, this.text_1, this.text_2, this.text_3, this.text_4, this.text_5, this.pageBottomText, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 677.5);


    //Title Text
    (lib.Symbol5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.fontfix = true;
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 1
        this.text_2 = new cjs.Text(" Färglägg halsbandet efter instruktionen.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(6, 0);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.text_2
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.virtualBounds = new cjs.Rectangle(0, 0, 341.3, 25.3);

    (lib.Symbol1 = function() {
        this.initialize();

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 0, 0);


    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(0, 0, 1, 1, 0, 0, 0, 254.6, 50);



        this.addChild(this.v1, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
