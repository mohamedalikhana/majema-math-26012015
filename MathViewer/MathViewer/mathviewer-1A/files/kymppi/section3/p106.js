(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // library properties:
    lib.properties = {
        width: 1219,
        height: 678,
        fps: 20,
        color: "#FFFFFF",
        manifest: [{
            src: "images/p106_1.png",
            id: "p106_1"
        }]
    };

    (lib.p106_1 = function() {
        this.initialize(img.p106_1);
    }).prototype = p = new cjs.Bitmap();
    p.virtualBounds = new cjs.Rectangle(0, 0, 398, 572);

    (lib.Symbol33 = function() {
        this.initialize();

        // Layer 1
        this.text = new cjs.Text("106", "13px 'Myriad Pro'", "#FFFFFF");
        this.text.lineHeight = 18;
        this.text.setTransform(35, 648);

        this.shape = new cjs.Shape();
        this.shape.graphics.f("#F1662B").s().p("Ak2CmIAAlLII0AAQAjAAAOAcQAIAPgBAOIAAESg");
        this.shape.setTransform(31, 660.8);

        this.addChild(this.shape, this.text);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 1218.9, 677.5);


    (lib.Symbol1 = function() {
        this.initialize();

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 520.5, 280, 10);
        this.roundRect1.setTransform(0, 0);

        this.text = new cjs.Text("Hur många äpplen är kvar?", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_1 = new cjs.Text("3.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.hrRule_1 = new cjs.Shape();
        this.hrRule_1.graphics.beginStroke("#707070").setStrokeStyle(0.8).moveTo(0, 69.5).lineTo(520.5, 69.5).moveTo(0, 116).lineTo(520.5, 116).moveTo(0, 164).lineTo(520.5, 164)
            .moveTo(0, 211).lineTo(520.5, 211).moveTo(0, 258).lineTo(520.5, 258);
        this.hrRule_1.setTransform(0, 0);

        this.Line_1 = new cjs.Shape();
        this.Line_1.graphics.beginStroke("#707070").setStrokeStyle(0.8).moveTo(150, 26).lineTo(150, 306).moveTo(264, 26).lineTo(264, 306).moveTo(376, 26).lineTo(376, 306);
        this.Line_1.setTransform(0, 0);

        this.text_2 = new cjs.Text("Äpplen från början", "15px 'MyriadPro-Semibold'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(11, 41);

        this.text_3 = new cjs.Text("Läggs till", "15px 'MyriadPro-Semibold'");
        this.text_3.lineHeight = 19;
        this.text_3.setTransform(173, 41);

        this.text_4 = new cjs.Text("Äts upp", "15px 'MyriadPro-Semibold'");
        this.text_4.lineHeight = 19;
        this.text_4.setTransform(292, 41);

        this.text_5 = new cjs.Text("Är kvar till slut", "15px 'MyriadPro-Semibold'");
        this.text_5.lineHeight = 19;
        this.text_5.setTransform(395, 41);

        var arrVal = ['3', '2', '4', '1', '2', '2', '4', '3', '1', '4', '5 äpplen', '3 äpplen', '6 äpplen', '5 äpplen', '8 äpplen'];
        var ToBeAdded = [];

        for (var index = 0; index < arrVal.length; index++) {
            var colSpace = index;
            var yPos = 87;
            if (index == 1) {
                colSpace = 0.65;
                yPos = 87;
            } else if (index == 2) {
                colSpace = 0;
                yPos = 134;
            } else if (index == 3) {
                colSpace = 0.65;
                yPos = 134;
            } else if (index == 4) {
                colSpace = 0;
                yPos = 181;
            } else if (index == 5) {
                colSpace = 0.65;
                yPos = 181;
            } else if (index == 6) {
                colSpace = 0;
                yPos = 228;
            } else if (index == 7) {
                colSpace = 0.65;
                yPos = 228;
            } else if (index == 8) {
                colSpace = 0;
                yPos = 276;
            } else if (index == 9) {
                colSpace = 0.65;
                yPos = 276;
            } else if (index == 10) {
                colSpace = -0.89;
                yPos = 87;
            } else if (index == 11) {
                colSpace = -0.89;
                yPos = 134;
            } else if (index == 12) {
                colSpace = -0.89;
                yPos = 181;
            } else if (index == 13) {
                colSpace = -0.89;
                yPos = 228;
            } else if (index == 14) {
                colSpace = -0.89;
                yPos = 276;
            }

            var tempLabel = new cjs.Text(arrVal[index], "16px 'Myriad Pro'");
            tempLabel.lineHeight = -1;
            tempLabel.setTransform(200 + (colSpace * 173.5), yPos);
            ToBeAdded.push(tempLabel);
        }

        this.textbox_group1 = new cjs.Shape();
        for (var column = 0; column < 1; column++) {
            for (var row = 0; row < 5; row++) {
                this.textbox_group1.graphics.f('#ffffff').s("#7D7D7D").ss(0.8).drawRect(435, 81 + (row * 47), 19, 22);
            }
        }
        this.textbox_group1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.hrRule_1);
        this.addChild(this.Line_1, this.text_2, this.text_3, this.text_4, this.text_5, this.textbox_group1);
        for (var textEl = 0; textEl < ToBeAdded.length; textEl++) {
            this.addChild(ToBeAdded[textEl]);
        }

    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 530, 325.9);


    (lib.Symbol3 = function() {
        this.initialize();

        this.instance = new lib.p106_1();
        this.instance.setTransform(8, 50, 0.382, 0.37);

        this.roundRect1 = new cjs.Shape();
        this.roundRect1.graphics.f("#ffffff").s('#7d7d7d').drawRoundRect(0, 26, 522, 205, 10);
        this.roundRect1.setTransform(0, 20);

        this.text = new cjs.Text("Mössorna finns i 4 färger. Halsdukarna finns i 2 färger.", "16px 'Myriad Pro'");
        this.text.lineHeight = 19;
        this.text.setTransform(19, 0);

        this.text_2 = new cjs.Text("Måla alla olika sätt som Leo kan klä sig.", "16px 'Myriad Pro'");
        this.text_2.lineHeight = 19;
        this.text_2.setTransform(19, 20);

        this.text_1 = new cjs.Text("4.", "bold 16px 'Myriad Pro'", "#F1662B");
        this.text_1.lineHeight = 27;
        this.text_1.setTransform(0, 0);

        this.addChild(this.roundRect1, this.text, this.text_1, this.text_2, this.instance);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(0, 0, 550, 200);

    // stage content:
    (lib.pageLib = function() {
        this.initialize();

        // Layer 1
        this.other = new lib.Symbol33();
        this.other.setTransform(609.5, 338.7, 1, 1, 0, 0, 0, 609.5, 338.7);

        this.v1 = new lib.Symbol1();
        this.v1.setTransform(297.5, 100.7, 1, 1, 0, 0, 0, 254.6, 53.5);

        this.v2 = new lib.Symbol3();
        this.v2.setTransform(297.5, 421.6, 1, 1, 0, 0, 0, 255.8, 38);

        this.addChild(this.v1, this.v2, this.other);
    }).prototype = p = new cjs.Container();
    p.virtualBounds = new cjs.Rectangle(609.5, 339, 1218.9, 677.5);

})(lib = lib || {}, images = images || {}, createjs = createjs || {});
var lib, images, createjs;
